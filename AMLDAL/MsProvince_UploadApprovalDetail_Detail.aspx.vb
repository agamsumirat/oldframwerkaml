#Region "Imports..."
Option Explicit On
Imports Sahassa.AML.BLL
Imports Sahassa.AML.BLL.ValidateBLL
Imports Sahassa.AML.BLL.AuditTrailBLL
Imports Sahassa.AML.Entities
Imports Sahassa.AML.Data
Imports Sahassa.AML.BLL.DataType
Imports Sahassa.AML.Commonly
Imports Sahassa.AML
Imports System.Collections.Generic
#End Region

Partial Class MsProvince_UploadApprovalDetail_Detail
    Inherits Parent

#Region "Function"

    Sub HideControl()
        ImageCancel.Visible = False
    End Sub

    Sub ChangeMultiView(ByVal index As Integer)
        MtvMsUser.ActiveViewIndex = index
    End Sub

#End Region

#Region "events..."

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Dim LngId As Long = 0
        Using ObjMsProvince_ApprovalDetail As TList(Of MsProvince_ApprovalDetail) = DataRepository.MsProvince_ApprovalDetailProvider.GetPaged("Pk_MsProvince_Approval_Detail_Id = " & parID, "", 0, Integer.MaxValue, 0)
            If ObjMsProvince_ApprovalDetail.Count > 0 Then
                LngId = ObjMsProvince_ApprovalDetail(0).Fk_MsProvince_Approval_Id
            End If
        End Using
        Response.Redirect("MsProvince_UploadApprovalDetail_View.aspx?ID=" & LngId)
    End Sub





    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                'AuditTrailBLL.InsertAuditTrailUserAccess(SessionCurrentPage)
                ListmapingNew = New TList(Of MappingMsProvinceNCBSPPATK_Approval_Detail)
                ListmapingOld = New TList(Of MappingMsProvinceNCBSPPATK)
                LoadData()
            Catch ex As Exception
                LogError(ex)
                CvalHandleErr.IsValid = False
                CvalHandleErr.ErrorMessage = ex.Message
            End Try
        End If

    End Sub

    Private Sub LoadData()
        Dim PkObject As String
        'Load new data
        Using ObjMsProvince_ApprovalDetail As MsProvince_ApprovalDetail = DataRepository.MsProvince_ApprovalDetailProvider.GetPaged(MsProvince_ApprovalDetailColumn.Pk_MsProvince_Approval_Detail_Id.ToString & _
            "=" & _
            parID, "", 0, 1, Nothing)(0)
            With ObjMsProvince_ApprovalDetail
                SafeDefaultValue = "-"
                txtIdProvincenew.Text = Safe(.IdProvince)
                txtCodenew.Text = Safe(.Code)
                txtNamanew.Text = Safe(.Nama)
                txtDescriptionnew.Text = Safe(.Description)

                'other info
                Dim Omsuser As User
                Omsuser = DataRepository.UserProvider.GetByUserID(.CreatedBy)
                If Omsuser IsNot Nothing Then
                    lblCreatedByNew.Text = Omsuser.UserName
                Else
                    lblCreatedByNew.Text = SafeDefaultValue
                End If
                lblCreatedDateNew.Text = FormatDate(.CreatedDate)
                Omsuser = DataRepository.UserProvider.GetByUserID(.LastUpdatedBy)
                If Omsuser IsNot Nothing Then
                    lblUpdatedbyNew.Text = Omsuser.UserName
                Else
                    lblUpdatedbyNew.Text = SafeDefaultValue
                End If
                lblUpdatedDateNew.Text = FormatDate(.LastUpdatedDate)
                'txtActivationNew.Text = SafeActiveInactive(.Activation)
                Dim L_objMappingMsProvinceNCBSPPATK_Approval_Detail As TList(Of MappingMsProvinceNCBSPPATK_Approval_Detail)
                L_objMappingMsProvinceNCBSPPATK_Approval_Detail = DataRepository.MappingMsProvinceNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsProvinceNCBSPPATK_Approval_DetailColumn.IDProvince.ToString & "=" & CStr(ObjMsProvince_ApprovalDetail.IdProvince), "", 0, Integer.MaxValue, Nothing)

                'Add mapping
                ListmapingNew.AddRange(L_objMappingMsProvinceNCBSPPATK_Approval_Detail)

            End With
            PkObject = ObjMsProvince_ApprovalDetail.IdProvince
        End Using



        'Load Old Data
        Using objMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(PkObject)
            If objMsProvince Is Nothing Then
                lamaOld.Visible = False
                lama.Visible = False
                Return
            End If

            With objMsProvince
                SafeDefaultValue = "-"
                txtIdProvinceOld.Text = Safe(.IdProvince)
                txtCodeOld.Text = Safe(.Code)
                txtNamaOld.Text = Safe(.Nama)
                txtDescriptionOld.Text = Safe(.Description)


                'other info
                Dim omsuser As User
                Omsuser = DataRepository.UserProvider.GetByUserID(.CreatedBy)
                If omsuser IsNot Nothing Then
                    lblCreatedByOld.Text = omsuser.UserName
                Else
                    lblCreatedByOld.Text = SafeDefaultValue
                End If
                lblCreatedDateOld.Text = FormatDate(.CreatedDate)
                Omsuser = DataRepository.UserProvider.GetByUserID(.LastUpdatedBy)
                If omsuser IsNot Nothing Then
                    lblUpdatedbyOld.Text = omsuser.UserName
                Else
                    lblUpdatedbyOld.Text = SafeDefaultValue
                End If
                lblUpdatedDateOld.Text = FormatDate(.LastUpdatedDate)
                Dim l_objMappingMsProvinceNCBSPPATK As TList(Of MappingMsProvinceNCBSPPATK)
                'txtNamaOld.Text = Safe(.Nama)
                l_objMappingMsProvinceNCBSPPATK = DataRepository.MappingMsProvinceNCBSPPATKProvider.GetPaged(MappingMsProvinceNCBSPPATKColumn.Pk_MsProvince_Id.ToString & _
                   "=" & _
                   PkObject, "", 0, Integer.MaxValue, Nothing)

                ListmapingOld.AddRange(l_objMappingMsProvinceNCBSPPATK)
            End With
        End Using
    End Sub






    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            BindListMapping()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub BindListMapping()
        'bind new value
        LBMappingNew.DataSource = ListMappingDisplayNew '(0).PK_MappingMsBentukBidangUsahaNCBSPPATK_Id
        LBMappingNew.DataBind()
        'Bind OldValue
        LBmappingOld.DataSource = ListMappingDisplayOld
        LBmappingOld.DataBind()
    End Sub

#End Region

#Region "Property..."

    Public ReadOnly Property parID() As String
        Get
            Return Request.Item("ID")
        End Get
    End Property
    ''' <summary>
    ''' Menyimpan Item untuk mapping New sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ListmapingNew() As TList(Of MappingMsProvinceNCBSPPATK_Approval_Detail)
        Get
            Return Session("ListmapingNew.data")
        End Get
        Set(ByVal value As TList(Of MappingMsProvinceNCBSPPATK_Approval_Detail))
            Session("ListmapingNew.data") = value
        End Set
    End Property

    ''' <summary>
    ''' Menyimpan Item untuk mapping Old sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ListmapingOld() As TList(Of MappingMsProvinceNCBSPPATK)
        Get
            Return Session("ListmapingOld.data")
        End Get
        Set(ByVal value As TList(Of MappingMsProvinceNCBSPPATK))
            Session("ListmapingOld.data") = value
        End Set
    End Property

    ''' <summary>
    ''' List yang tampil di ListBox New
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplayNew() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingMsProvinceNCBSPPATK_Approval_Detail In ListmapingNew.FindAllDistinct("IDProvinceNCBS")
                Temp.Add(i.IDProvinceNCBS.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property

    ''' <summary>
    ''' List yang tampil di ListBox Old
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplayOld() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingMsProvinceNCBSPPATK In ListmapingOld.FindAllDistinct("Pk_MsProvinceNCBS_Id")
                Temp.Add(i.Pk_MsProvinceNCBS_Id.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property
#End Region



End Class



