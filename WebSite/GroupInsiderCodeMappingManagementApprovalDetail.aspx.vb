Imports System.Runtime.Serialization
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Partial Class GroupInsiderCodeMappingManagementApprovalDetail
    Inherits Parent
#Region " Property "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get confirm type
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property ParamType() As Sahassa.AML.Commonly.TypeConfirm
        Get
            Return Me.Request.Params("TypeConfirm")
        End Get
    End Property

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get param pk Group management
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    07/06/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property ParamGroupId() As Int32
        Get
            Return Me.Request.Params("GroupID")
        End Get
    End Property

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get param pk Group management
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    07/06/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property ParamGroupName() As String
        Get
            Return Me.Request.Params("GroupName")
        End Get
    End Property

    ''' <summary>
    ''' get user id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetUserID(ByVal pkPendingApprovalID As Int64) As String
        Get
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.GroupInsiderCodeMapping_PendingApprovalTableAdapter
                'Return AccessPending.GetGroupInsiderCodeMapping_PendingApprovalUserID(Me.ParamPkPendingApproval)
                Return AccessPending.GetGroupInsiderCodeMapping_PendingApprovalUserID(pkPendingApprovalID)
            End Using
        End Get
    End Property

    Private Property OldInsiderCodes() As ArrayList
        Get
            Return IIf(ViewState("OldInsiderCodes") Is Nothing, New ArrayList, ViewState("OldInsiderCodes"))
        End Get
        Set(ByVal value As ArrayList)
            ViewState("OldInsiderCodes") = value
        End Set
    End Property

    Private Property PendingApprovalIDs() As ArrayList
        Get
            Return IIf(ViewState("PendingApprovalIDs") Is Nothing, New ArrayList, ViewState("PendingApprovalIDs"))
        End Get
        Set(ByVal value As ArrayList)
            ViewState("PendingApprovalIDs") = value
        End Set
    End Property

    Private Property PendingInsiderCodes() As ListItemCollection
        Get
            Return IIf(Session("PendingInsiderCodes") Is Nothing, New ListItemCollection, Session("PendingInsiderCodes"))
        End Get
        Set(ByVal value As ListItemCollection)
            Session("PendingInsiderCodes") = value
        End Set
    End Property
#End Region

#Region " Load "
    ''' <summary>
    ''' Load Informasi Detail Group Insider Code Mapping Approval utk GroupID tsb
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub LoadGroupInsiderCodeMappingDetails()
        Try
            Me.LabelTitle.Text = "Activity: Group Insider Code Mapping"

            Me.LabelGroupId.Text = Me.ParamGroupId
            Me.LabelGroupName.Text = Me.ParamGroupName

            Dim i As Integer

            'Ambil informasi dari tabel GroupInsiderCodeMapping_PendingApproval untuk GroupID tsb
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.GroupInsiderCodeMapping_PendingApprovalTableAdapter
                Using objTable As AMLDAL.AMLDataSet.GroupInsiderCodeMapping_PendingApprovalDataTable = AccessPending.GetGroupInsiderCodeMappingPendingApprovalData(Me.ParamGroupId)
                    If objTable.Rows.Count = 0 Then
                        Throw New Exception("Cannot load Group Insider Code Mapping data for the following Group : " & Me.ParamGroupName & " because it is no longer waiting for approval.")
                    Else
                        Dim arrTarget As New ArrayList

                        For Each objRow As AMLDAL.AMLDataSet.GroupInsiderCodeMapping_PendingApprovalRow In objTable
                            'Ambil informasi PendingApprovalUserID dan PendingApprovalEntryDate dari row pertama
                            If i = 0 Then
                                Me.LabelCreatedBy.Text = objRow.PendingApprovalUserID
                                LabelEntryDate.Text = String.Format("{0:dd-MMM-yyyy HH:mm}", CType(objRow.PendingApprovalEntryDate, Date))

                                i += 1
                            End If
                            arrTarget.Add(objRow.PendingApprovalID)
                        Next

                        Me.PendingApprovalIDs = arrTarget
                    End If
                End Using
            End Using

            'Ambil informasi InsiderCodes lama untuk GroupID tsb
            Using AccessInsiderCodes As New AMLDAL.AMLDataSetTableAdapters.GroupInsiderCodeMappingTableAdapter
                Using objTable As AMLDAL.AMLDataSet.GroupInsiderCodeMappingDataTable = AccessInsiderCodes.GetDataByGroupId(Me.ParamGroupId)
                    If objTable.Rows.Count = 0 Then
                        'Berarti Penambahan GroupInsiderCode
                        Me.OldInsiderCodes = Nothing
                    Else
                        Dim arrTarget As New ArrayList

                        'Ambil semua InsiderCode yg dimapping ke GroupID tsb
                        For Each objRow As AMLDAL.AMLDataSet.GroupInsiderCodeMappingRow In objTable
                            Dim InsiderCodeId As String = objRow.InsiderCodeId

                            arrTarget.Add(InsiderCodeId)
                            Me.ListBoxInsiderCodesOld.Items.Add(InsiderCodeId)
                        Next

                        Me.OldInsiderCodes = arrTarget
                    End If
                End Using
            End Using

            Dim PendingInsiderCodes_ As New ListItemCollection

            'Ambil informasi InsiderCodes yg ada dlm tabel GroupInsiderCodeMapping_Approval
            Using AccessApproval As New AMLDAL.AMLDataSetTableAdapters.GroupInsiderCodeMapping_ApprovalTableAdapter
                Dim PendingApprovalIDs_ As ArrayList = Me.PendingApprovalIDs

                For i = 0 To PendingApprovalIDs_.Count - 1
                    Dim objTable As Data.DataTable = AccessApproval.GetGroupInsiderCodeMappingApprovalDataByPendingApprovalID(PendingApprovalIDs_(i))

                    If objTable.Rows.Count > 0 Then
                        Dim objRow As AMLDAL.AMLDataSet.GroupInsiderCodeMapping_ApprovalRow = objTable.Rows(0)
                        Dim item As New ListItem(objRow.InsiderCodeId, objRow.ModeID)
                        PendingInsiderCodes_.Add(item)
                    End If
                Next

                Me.PendingInsiderCodes = PendingInsiderCodes_
            End Using

            'Tampilkan informasi InsiderCodes To-Be (dlm bagian New Values)
            Dim AddedInsiderCodes As New ArrayList
            Dim DeletedInsiderCodes As New ArrayList

            For i = 0 To PendingInsiderCodes_.Count - 1
                If PendingInsiderCodes_.Item(i).Value = "1" Then
                    AddedInsiderCodes.Add(PendingInsiderCodes_(i).Text)
                ElseIf PendingInsiderCodes_(i).Value = "3" Then
                    DeletedInsiderCodes.Add(PendingInsiderCodes_(i).Text)
                End If
            Next

            Dim NewInsiderCodes As ArrayList = Me.OldInsiderCodes
            For i = 0 To AddedInsiderCodes.Count - 1
                If NewInsiderCodes.Contains(AddedInsiderCodes(i)) = False Then
                    NewInsiderCodes.Add(AddedInsiderCodes(i))
                End If
            Next

            For i = 0 To DeletedInsiderCodes.Count - 1
                If NewInsiderCodes.Contains(DeletedInsiderCodes(i)) Then
                    NewInsiderCodes.Remove(DeletedInsiderCodes(i))
                End If
            Next

            For i = 0 To NewInsiderCodes.Count - 1
                Me.ListBoxInsiderCodesNew.Items.Add(NewInsiderCodes(i))
            Next
        Catch
            Throw
        End Try

    End Sub
#End Region

#Region " Accept "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' AcceptGroupInsiderCodeMapping
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    '''     [Hendry]    18/06/2007 Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptGroupInsiderCodeMapping()
        Dim oSQLTrans As SqlTransaction = Nothing

        Try
            'Using TranScope As New Transactions.TransactionScope
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.GroupInsiderCodeMapping_PendingApprovalTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessPending, Data.IsolationLevel.ReadUncommitted)
                Using objTable As AMLDAL.AMLDataSet.GroupInsiderCodeMapping_PendingApprovalDataTable = AccessPending.GetGroupInsiderCodeMappingPendingApprovalData(Me.ParamGroupId)
                    If objTable.Rows.Count > 0 Then
                        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                            Using AccessApproval As New AMLDAL.AMLDataSetTableAdapters.GroupInsiderCodeMapping_ApprovalTableAdapter
                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessApproval, oSQLTrans)
                                Using AccessGroupInsiderCodeMapping As New AMLDAL.AMLDataSetTableAdapters.GroupInsiderCodeMappingTableAdapter
                                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessGroupInsiderCodeMapping, oSQLTrans)
                                    Dim i As Integer

                                    Dim PendingApprovalIDs_ As ArrayList = Me.PendingApprovalIDs

                                    For i = 0 To PendingApprovalIDs_.Count - 1
                                        Dim ApprovalTable As Data.DataTable = AccessApproval.GetGroupInsiderCodeMappingApprovalDataByPendingApprovalID(PendingApprovalIDs_(i))
                                        Dim ApprovalRow As AMLDAL.AMLDataSet.GroupInsiderCodeMapping_ApprovalRow = ApprovalTable.Rows(0)

                                        'Periksa apakah pasangan GroupID-InsiderCodeID tsb sdh ada dlm tabel GroupInsiderCodeMapping atau belum
                                        Dim MatchCount As Int32 = AccessGroupInsiderCodeMapping.CheckGroupInsiderCodeMapping(ApprovalRow.GroupId, ApprovalRow.InsiderCodeId)

                                        If ApprovalRow.ModeID = 1 Then
                                            'MatchCount = 0 berarti pasangan GroupID-InsiderCodeID tsb blm ada dlm tabel GroupInsiderCodeMapping dan bisa ditambahkan
                                            If MatchCount = 0 Then
                                                AccessGroupInsiderCodeMapping.Insert(ApprovalRow.GroupId, ApprovalRow.InsiderCodeId, ApprovalRow.CreatedDate)
                                                AccessAudit.Insert(Now, Me.GetUserID(PendingApprovalIDs_(i)), Sahassa.AML.Commonly.SessionUserId, "GroupInsiderCodeMapping", "GroupId", "Add", "", ApprovalRow.GroupId, "Accepted")
                                                AccessAudit.Insert(Now, Me.GetUserID(PendingApprovalIDs_(i)), Sahassa.AML.Commonly.SessionUserId, "GroupInsiderCodeMapping", "InsiderCodeId", "Add", "", ApprovalRow.InsiderCodeId, "Accepted")
                                                AccessAudit.Insert(Now, Me.GetUserID(PendingApprovalIDs_(i)), Sahassa.AML.Commonly.SessionUserId, "GroupInsiderCodeMapping", "CreatedDate", "Add", "", ApprovalRow.CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                                            End If
                                        ElseIf ApprovalRow.ModeID = 3 Then
                                            'MatchCount > 0 berarti pasangan GroupID-InsiderCodeID tsb msh ada dlm tabel GroupInsiderCodeMapping dan bisa dihapus
                                            If MatchCount > 0 Then
                                                AccessGroupInsiderCodeMapping.DeleteGroupInsiderCodeMapping(ApprovalRow.GroupId, ApprovalRow.InsiderCodeId)
                                                AccessAudit.Insert(Now, Me.GetUserID(PendingApprovalIDs_(i)), Sahassa.AML.Commonly.SessionUserId, "GroupInsiderCodeMapping", "GroupId", "Delete", ApprovalRow.GroupId, "", "Accepted")
                                                AccessAudit.Insert(Now, Me.GetUserID(PendingApprovalIDs_(i)), Sahassa.AML.Commonly.SessionUserId, "GroupInsiderCodeMapping", "InsiderCodeId", "Delete", ApprovalRow.InsiderCodeId, "", "Accepted")
                                                AccessAudit.Insert(Now, Me.GetUserID(PendingApprovalIDs_(i)), Sahassa.AML.Commonly.SessionUserId, "GroupInsiderCodeMapping", "CreatedDate", "Delete", ApprovalRow.CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "", "Accepted")
                                            End If
                                        End If

                                        AccessPending.DeleteGroupInsiderCodeMapping_PendingApproval(PendingApprovalIDs_(i))
                                        AccessApproval.DeleteGroupInsiderCodeMapping_Approval(PendingApprovalIDs_(i))
                                    Next

                                    oSQLTrans.Commit()
                                End Using
                            End Using
                        End Using
                    Else
                        Throw New Exception("Operation failed. The following Group: " & Me.LabelGroupName.Text & " is no longer in the approval table.")
                    End If
                End Using
            End Using
            'End Using
        Catch
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()

            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If

        End Try
    End Sub
#End Region

#Region " Reject "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' RejectGroupInsiderCodeMapping 
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    '''     [Hendry]    18/06/2007 Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectGroupInsiderCodeMapping()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            'Using TranScope As New Transactions.TransactionScope
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.GroupInsiderCodeMapping_PendingApprovalTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessPending, Data.IsolationLevel.ReadUncommitted)
                Using objTable As AMLDAL.AMLDataSet.GroupInsiderCodeMapping_PendingApprovalDataTable = AccessPending.GetGroupInsiderCodeMappingPendingApprovalData(Me.ParamGroupId)
                    If objTable.Rows.Count > 0 Then
                        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                            Using AccessApproval As New AMLDAL.AMLDataSetTableAdapters.GroupInsiderCodeMapping_ApprovalTableAdapter
                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessApproval, oSQLTrans)
                                Dim i As Integer

                                Dim PendingApprovalIDs_ As ArrayList = Me.PendingApprovalIDs

                                For i = 0 To PendingApprovalIDs_.Count - 1
                                    Dim ApprovalTable As Data.DataTable = AccessApproval.GetGroupInsiderCodeMappingApprovalDataByPendingApprovalID(PendingApprovalIDs_(i))
                                    Dim ApprovalRow As AMLDAL.AMLDataSet.GroupInsiderCodeMapping_ApprovalRow = ApprovalTable.Rows(0)

                                    If ApprovalRow.ModeID = 1 Then
                                        AccessAudit.Insert(Now, Me.GetUserID(PendingApprovalIDs_(i)), Sahassa.AML.Commonly.SessionUserId, "GroupInsiderCodeMapping", "GroupId", "Add", "", ApprovalRow.GroupId, "Rejected")
                                        AccessAudit.Insert(Now, Me.GetUserID(PendingApprovalIDs_(i)), Sahassa.AML.Commonly.SessionUserId, "GroupInsiderCodeMapping", "InsiderCodeId", "Add", "", ApprovalRow.InsiderCodeId, "Rejected")
                                        AccessAudit.Insert(Now, Me.GetUserID(PendingApprovalIDs_(i)), Sahassa.AML.Commonly.SessionUserId, "GroupInsiderCodeMapping", "CreatedDate", "Add", "", ApprovalRow.CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")
                                    ElseIf ApprovalRow.ModeID = 3 Then
                                        AccessAudit.Insert(Now, Me.GetUserID(PendingApprovalIDs_(i)), Sahassa.AML.Commonly.SessionUserId, "GroupInsiderCodeMapping", "GroupId", "Delete", ApprovalRow.GroupId, "", "Rejected")
                                        AccessAudit.Insert(Now, Me.GetUserID(PendingApprovalIDs_(i)), Sahassa.AML.Commonly.SessionUserId, "GroupInsiderCodeMapping", "InsiderCodeId", "Delete", ApprovalRow.InsiderCodeId, "", "Rejected")
                                        AccessAudit.Insert(Now, Me.GetUserID(PendingApprovalIDs_(i)), Sahassa.AML.Commonly.SessionUserId, "GroupInsiderCodeMapping", "CreatedDate", "Delete", ApprovalRow.CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "", "Rejected")
                                    End If

                                    AccessPending.DeleteGroupInsiderCodeMapping_PendingApproval(PendingApprovalIDs_(i))
                                    AccessApproval.DeleteGroupInsiderCodeMapping_Approval(PendingApprovalIDs_(i))
                                Next

                                oSQLTrans.Commit()
                            End Using
                        End Using
                    Else
                        Throw New Exception("Operation failed. The following Group: " & Me.LabelGroupName.Text & " is no longer in the approval table.")
                    End If
                End Using
            End Using
            'End Using

        Catch
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub
#End Region

    ''' <summary>
    ''' page load event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.ImageAccept.Attributes.Add("onclick", "javascript: if(confirm('Do you want to accept this activity ?')== false) return false;")
                Me.ImageReject.Attributes.Add("onclick", "javascript: if(confirm('Do you want to reject this activity ?')== false) return false;")
                Me.LoadGroupInsiderCodeMappingDetails()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    '    Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' accept button handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click
        Try
            Me.AcceptGroupInsiderCodeMapping()

            Sahassa.AML.Commonly.SessionIntendedPage = "GroupInsiderCodeMappingManagementApproval.aspx"

            Me.Response.Redirect("GroupInsiderCodeMappingManagementApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' reject button handlert
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click
        Try
            RejectGroupInsiderCodeMapping()

            Sahassa.AML.Commonly.SessionIntendedPage = "GroupInsiderCodeMappingManagementApproval.aspx"

            Me.Response.Redirect("GroupInsiderCodeMappingManagementApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' back button handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "GroupInsiderCodeMappingManagementApproval.aspx"

        Me.Response.Redirect("GroupInsiderCodeMappingManagementApproval.aspx", False)
    End Sub
End Class