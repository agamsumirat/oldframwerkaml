Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper


Partial Class GroupAdd
    Inherits Parent

    ''' <summary>
    ''' get id for focues
    ''' </summary>
    ''' <value></value>
    ''' <returns>element control id</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property GetFocusID() As String
        Get
            Return Me.TextGroupName.ClientID
        End Get
    End Property

    ''' <summary>
    ''' cancel handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "GroupView.aspx"
        Me.Response.Redirect("GroupView.aspx", False)
    End Sub
    ''' <summary>
    ''' insert by su
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub InsertGroupBySU()
        Try
            Dim GroupDescription As String
            If Me.TextGroupDescription.Text.Length <= 255 Then
                GroupDescription = Me.TextGroupDescription.Text
            Else
                GroupDescription = Me.TextGroupDescription.Text.Substring(0, 255)
            End If

            Using AccessGroup As New AMLDAL.AMLDataSetTableAdapters.GroupTableAdapter
                AccessGroup.Insert(Me.TextGroupName.Text, GroupDescription, Now, True)
            End Using
        Catch
            Throw
        End Try
    End Sub

    Private Sub InsertAuditTrail()
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(3)
            Dim GroupDescription As String
            If Me.TextGroupDescription.Text.Length <= 255 Then
                GroupDescription = Me.TextGroupDescription.Text
            Else
                GroupDescription = Me.TextGroupDescription.Text.Substring(0, 255)
            End If

            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Group", "GroupName", "Add", "", Me.TextGroupName.Text, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Group", "GroupDescription", "Add", "", GroupDescription, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Group", "GroupCreatedDate", "Add", "", Now.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
            End Using
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' update handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim oTrans As SqlTransaction = Nothing

        Page.Validate()

        If Page.IsValid Then
            Try
                Dim GroupName As String = Trim(Me.TextGroupName.Text)

                'Periksa apakah Nama Group tersebut sudah ada dalam tabel Group atau belum
                Using AccessGroup As New AMLDAL.AMLDataSetTableAdapters.GroupTableAdapter
                    Dim counter As Int32 = AccessGroup.CountMatchingGroup(GroupName)

                    'Counter = 0 berarti Nama Group tersebut belum pernah ada dalam tabel Group
                    If counter = 0 Then
                        'Periksa apakah Nama Group tersebut sudah ada dalam tabel Group_Approval atau belum
                        Using AccessGroupApproval As New AMLDAL.AMLDataSetTableAdapters.Group_ApprovalTableAdapter
                            oTrans = BeginTransaction(AccessGroupApproval, IsolationLevel.ReadUncommitted)
                            counter = AccessGroupApproval.CountGroupApprovalByGroupName(GroupName)

                            'Counter = 0 berarti Group tersebut statusnya tidak dalam pending approval dan boleh ditambahkan dlm tabel Group_Approval
                            If counter = 0 Then
                                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                                    Me.InsertGroupBySU()
                                    Me.InsertAuditTrail()
                                    Me.LblSuccess.Text = "Success to Insert New Group."
                                    Me.LblSuccess.Visible = True
                                Else
                                    Dim GroupDescription As String
                                    If Me.TextGroupDescription.Text.Length <= 255 Then
                                        GroupDescription = Me.TextGroupDescription.Text
                                    Else
                                        GroupDescription = Me.TextGroupDescription.Text.Substring(0, 255)
                                    End If

                                    Dim GroupPendingApprovalID As Int64

                                    'Using TransScope As New Transactions.TransactionScope
                                    Using AccessGroupPendingApproval As New AMLDAL.AMLDataSetTableAdapters.Group_PendingApprovalTableAdapter
                                        SetTransaction(AccessGroupPendingApproval, oTrans)
                                        'Tambahkan ke dalam tabel Group_PendingApproval dengan ModeID = 1 (Add) 
                                        GroupPendingApprovalID = AccessGroupPendingApproval.InsertGroup_PendingApproval(Sahassa.AML.Commonly.SessionUserId, Now, "Group Add", 1)

                                        'Tambahkan ke dalam tabel Group_Approval dengan ModeID = 1 (Add) 
                                        AccessGroupApproval.Insert(GroupPendingApprovalID, 1, Nothing, GroupName, GroupDescription, Now, Nothing, Nothing, Nothing, Nothing, False, Nothing)

                                        oTrans.Commit()

                                        Dim MessagePendingID As Integer = 8201 'MessagePendingID 8201 = Group Add 

                                        Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & GroupName

                                        Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & GroupName, False)
                                    End Using
                                    'End Using
                                End If
                            Else
                                Throw New Exception("Cannot add the following Group: '" & GroupName & "' because it is currently waiting for approval.")
                            End If
                        End Using
                    Else
                        Throw New Exception("Cannot add the following Group: '" & GroupName & "' because that Group Name already exists in the database.")
                    End If
                End Using
            Catch ex As Exception
                If Not oTrans Is Nothing Then oTrans.Rollback()
                Me.cvalPageError.IsValid = False
                Me.cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            Finally
                If Not oTrans Is Nothing Then
                    oTrans.Dispose()
                    oTrans = Nothing
                End If

            End Try

        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

            'Using Transcope As New Transactions.TransactionScope
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                '    Transcope.Complete()
            End Using
            'End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class