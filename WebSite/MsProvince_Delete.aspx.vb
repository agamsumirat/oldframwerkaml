#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports Sahassa.AML
#End Region

Partial Class MsProvince_Delete
Inherits Parent

#Region "Function"

	Sub ChangeMultiView(index As Integer)
		MtvMsUser.ActiveViewIndex = index
	End Sub

	Private Sub BindListMapping()
		LBMapping.DataSource = Listmaping
		LBMapping.DataTextField = "Nama"
		LBMapping.DataValueField = "IdProvinceNCBS"
		LBMapping.DataBind()
	End Sub
  

#End Region

#Region "events..."

	Public ReadOnly Property parID As String
		Get
			Return Request.Item("ID")
		End Get
	End Property

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Not Page.IsPostBack Then
			Try
                'AuditTrailBLL.InsertAuditTrailUserAccess(SessionCurrentPage)
				Listmaping = New TList(Of MsProvinceNCBS)
				LoadData()
			Catch ex As Exception
				LogError(ex)
				CvalPageErr.IsValid = False
				CvalPageErr.ErrorMessage = ex.Message
			End Try
		End If

	End Sub

	Private Sub LoadData()
		Dim ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetPaged(MsProvinceColumn.IdProvince.ToString & "=" & parID, "", 0, 1, Nothing)(0)
		 If  ObjMsProvince Is Nothing Then Throw New Exception("Data Not Found")
		With ObjMsProvince
			SafeDefaultValue = "-"
			 lblIdProvince.Text = .IdProvince
lblCode.Text = Safe(.Code)
lblNama.Text = Safe(.Nama)
lblDescription.Text = Safe(.Description)

			Dim L_objMappingMsProvinceNCBSPPATK As TList(Of MappingMsProvinceNCBSPPATK)
            L_objMappingMsProvinceNCBSPPATK = DataRepository.MappingMsProvinceNCBSPPATKProvider.GetPaged(MappingMsProvinceNCBSPPATKColumn.Pk_MsProvince_Id.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)

			For Each i As MappingMsProvinceNCBSPPATK In L_objMappingMsProvinceNCBSPPATK
				Dim TempObj As MsProvinceNCBS = DataRepository.MsProvinceNCBSProvider.GetByIdProvinceNCBS(i.PK_MappingMsProvinceNCBSPPATK_Id)
				  If TempObj Is Nothing Then Continue For
				Listmaping.Add(TempObj)
			Next
		End With
	End Sub

	Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
		Try
			BindListMapping()
		Catch ex As Exception
			LogError(ex)
			CvalPageErr.IsValid = False
			CvalPageErr.ErrorMessage = ex.Message
		End Try
	End Sub

#End Region

#Region "Property..."

	''' <summary>
	''' Menyimpan Item untuk mapping sementara
	''' </summary>
	''' <value></value>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Property Listmaping As TList(Of MsProvinceNCBS)
		Get
			Return Session("Listmaping.data")
		End Get
		Set(value As TList(Of MsProvinceNCBS))
			Session("Listmaping.data") = value
		End Set
	End Property

#End Region

#Region "events..."
	Protected Sub ImageCancel_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
		Response.Redirect("MsProvince_View.aspx")
	End Sub

	Protected Sub imgOk_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles imgOk.Click
		Try
			Page.Validate("handle")
			If Page.IsValid Then

				' =========== Insert Header Approval
				Dim KeyHeaderApproval As Integer
				Using ObjMsProvince_Approval As New MsProvince_Approval
					With ObjMsProvince_Approval
						.FK_MsMode_Id = 3
                        FillOrNothing(.Fk_MsUser_Id, SessionPkUserId)
						FillOrNothing(.RequestedDate, Date.Now)
                        .IsUpload = False
					End With
					DataRepository.MsProvince_ApprovalProvider.Save(ObjMsProvince_Approval)
					KeyHeaderApproval = ObjMsProvince_Approval.PK_MsProvince_Approval_Id
				End Using
				'============ Insert Detail Approval
				Using objMsProvince_ApprovalDetail As New MsProvince_ApprovalDetail()
					With objMsProvince_ApprovalDetail
						Dim ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(parID)
						FillOrNothing(.IdProvince, lblIdProvince.Text,true,Oint)
FillOrNothing(.Code, lblCode.Text,true,Ovarchar)
FillOrNothing(.Nama, lblNama.Text,true,Ovarchar)
FillOrNothing(.Description, lblDescription.Text,true,Ovarchar)

						FillOrNothing(.IdProvince, ObjMsProvince.IdProvince)
						FillOrNothing(.Activation, ObjMsProvince.Activation)
						FillOrNothing(.CreatedDate, ObjMsProvince.CreatedDate)
						FillOrNothing(.CreatedBy, ObjMsProvince.CreatedBy)
						FillOrNothing(.FK_MsProvince_Approval_Id, KeyHeaderApproval)
					End With
					DataRepository.MsProvince_ApprovalDetailProvider.Save(objMsProvince_ApprovalDetail)
                    Dim keyHeaderApprovalDetail As Integer = objMsProvince_ApprovalDetail.Pk_MsProvince_Approval_Detail_Id

					'========= Insert mapping item 
					Dim LobjMappingMsProvinceNCBSPPATK_Approval_Detail As New TList(Of MappingMsProvinceNCBSPPATK_Approval_Detail)

                    Dim L_ObjMappingMsProvinceNCBSPPATK As TList(Of MappingMsProvinceNCBSPPATK) = DataRepository.MappingMsProvinceNCBSPPATKProvider.GetPaged(MappingMsProvinceNCBSPPATKColumn.Pk_MsProvince_Id.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)

                    For Each objMappingMsProvinceNCBSPPATK As MappingMsProvinceNCBSPPATK In L_ObjMappingMsProvinceNCBSPPATK
                        Dim objMappingMsProvinceNCBSPPATK_Approval_Detail As New MappingMsProvinceNCBSPPATK_Approval_Detail
                        With objMappingMsProvinceNCBSPPATK_Approval_Detail
                            FillOrNothing(.IDProvince, objMappingMsProvinceNCBSPPATK.PK_MappingMsProvinceNCBSPPATK_Id)
                            FillOrNothing(.IDProvinceNCBS, objMappingMsProvinceNCBSPPATK.Pk_MsProvinceNCBS_Id)
                            FillOrNothing(.PK_MsProvince_Approval_Id, KeyHeaderApproval)
                            FillOrNothing(.PK_MappingMsProvinceNCBSPPATK_Id, objMappingMsProvinceNCBSPPATK.PK_MappingMsProvinceNCBSPPATK_Id)
                            LobjMappingMsProvinceNCBSPPATK_Approval_Detail.Add(objMappingMsProvinceNCBSPPATK_Approval_Detail)
                        End With
                        DataRepository.MappingMsProvinceNCBSPPATK_Approval_DetailProvider.Save(LobjMappingMsProvinceNCBSPPATK_Approval_Detail)
                    Next
					'session Maping item Clear
					Listmaping.Clear()
                    Me.imgOk.Visible = False
                    Me.ImageCancel.Visible = False
					LblConfirmation.Text = "Data has been Delete and waiting for approval"
					MtvMsUser.ActiveViewIndex = 1
				End Using
			End If
		Catch ex As Exception
			LogError(ex)
			CvalPageErr.IsValid = False
			CvalPageErr.ErrorMessage = ex.Message
		End Try

	End Sub

	Protected Sub ImgBtnAdd_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAdd.Click
		Response.Redirect("MsProvince_View.aspx")
	End Sub

#End Region
	
End Class



