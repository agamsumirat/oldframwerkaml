Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper


Partial Class VerificationListCategoryDelete
    Inherits Parent
    ''' <summary>
    ''' get user id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetCategoryId() As String
        Get
            Return Me.Request.Params("CategoryID")
        End Get
    End Property

    ''' <summary>
    ''' cancel handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "VerificationListCategoryView.aspx"
        Me.Response.Redirect("VerificationListCategoryView.aspx", False)
    End Sub

    Private Sub InsertAuditTrail()
        Try
            Dim CategoryID_Old As Int32 = ViewState("CategoryID_Old")
            Dim CategoryName_Old As String = ViewState("CategoryName_Old")
            Dim CategoryDescription_Old As String = ViewState("CategoryDescription_Old")
            Dim CategoryCreatedDate_Old As DateTime = ViewState("CategoryCreatedDate_Old")
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(4)
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "VerificationListCategory", "CategoryID", "Delete", "", CategoryID_Old, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "VerificationListCategory", "CategoryName", "Delete", "", CategoryName_Old, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "VerificationListCategory", "CategoryDescription", "Delete", "", CategoryDescription_Old, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "VerificationListCategory", "CategoryCreatedDate", "Delete", "", CategoryCreatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
            End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub DeleteVerificationListCategory()
        Try
            Using AccessDeleteVerificationListCategory As New AMLDAL.AMLDataSetTableAdapters.VerificationListCategoryTableAdapter
                AccessDeleteVerificationListCategory.DeleteVerificationListCategory(CInt(Me.GetCategoryId))
            End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' update handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim oSQLTrans As SqlTransaction = Nothing
        Page.Validate()

        If Page.IsValid Then
            Try
                'Buat variabel untuk menampung nilai-nilai baru
                Dim CategoryID As String = Me.LabelCategoryID.Text
                Dim CategoryName As String = Me.TextCategoryName.Text
                Dim CategoryDescription As String = Me.TextCategoryDescription.Text

                'Periksa apakah CategoryName tsb sdh ada dalam tabel VerificationListCategory atau belum
                Using AccessCategory As New AMLDAL.AMLDataSetTableAdapters.VerificationListCategoryTableAdapter
                    Dim counter As Int32 = AccessCategory.CountMatchingCategory(CategoryName)

                    'Counter > 0 berarti CategoryName tersebut masih ada dalam tabel VerificationListCategory dan bisa didelete
                    If counter > 0 Then
                        'Periksa apakah CategoryName tsb dalam status pending approval atau tidak
                        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.VerificationListCategory_ApprovalTableAdapter
                            oSQLTrans = BeginTransaction(AccessPending, IsolationLevel.ReadUncommitted)
                            counter = AccessPending.CountVerificationListCategoryApproval(CategoryID)

                            'Counter = 0 berarti CategoryName tersebut tidak dalam status pending approval
                            If counter = 0 Then
                                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                                    Me.InsertAuditTrail()
                                    Me.DeleteVerificationListCategory()
                                    Me.LblSuccess.Visible = True
                                    Me.LblSuccess.Text = "Success to Delete Verification List Category."
                                Else
                                    'Buat variabel untuk menampung nilai-nilai lama
                                    Dim CategoryID_Old As Int32 = ViewState("CategoryID_Old")
                                    Dim Category_Old As String = ViewState("CategoryName_Old")
                                    Dim CategoryDescription_Old As String = ViewState("CategoryDescription_Old")
                                    Dim CategoryCreatedDate_Old As DateTime = ViewState("CategoryCreatedDate_Old")

                                    'variabel untuk menampung nilai identity dari tabel VerificationListCategory_PendingApprovalID
                                    Dim CategoryPendingApprovalID As Int64
                                    
                                    'Using TransScope As New Transactions.TransactionScope
                                    Using AccessCategoryPendingApproval As New AMLDAL.AMLDataSetTableAdapters.VerificationListCategory_PendingApprovalTableAdapter
                                        SetTransaction(AccessCategoryPendingApproval, oSQLTrans)
                                        'Tambahkan ke dalam tabel VerificationListCategory_PendingApproval dengan ModeID = 3 (Delete)
                                        CategoryPendingApprovalID = AccessCategoryPendingApproval.InsertVerificationListCategory_PendingApproval(Sahassa.AML.Commonly.SessionUserId, Now, "Verification List Category Delete", 3)

                                        'Tambahkan ke dalam tabel VerificationListCategory_Approval dengan ModeID = 3 (Delete) 
                                        AccessPending.Insert(CategoryPendingApprovalID, 3, CategoryID, CategoryName, CategoryDescription, Now, CategoryID_Old, Category_Old, CategoryDescription_Old, CategoryCreatedDate_Old)

                                        oSQLTrans.Commit()

                                        Dim MessagePendingID As Integer = 8903 'MessagePendingID 8903 = VerificationListCategory Delete 

                                        Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & CategoryName
                                        Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & CategoryName, False)
                                    End Using
                                    'End Using
                                End If
                            Else
                                Throw New Exception("Cannot delete the following Verification List Category: '" & CategoryName & "' because it is currently waiting for approval.")
                            End If
                        End Using
                    Else 'Counter = 0 berarti CategoryName tersebut tidak ada dalam tabel VerificationListCategory
                        Throw New Exception("Cannot delete the following Verification List Category: '" & CategoryName & "' because that Category Name does not exist in the database anymore.")
                    End If
                End Using
            Catch ex As Exception
                If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
                Me.cvalPageError.IsValid = False
                Me.cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            Finally
                If Not oSQLTrans Is Nothing Then
                    oSQLTrans.Dispose()
                    oSQLTrans = Nothing
                End If
            End Try
        End If
    End Sub

    ''' <summary>
    ''' filledit data
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillEditData()
        Using AccessCategory As New AMLDAL.AMLDataSetTableAdapters.VerificationListCategoryTableAdapter
            Using TableCategory As AMLDAL.AMLDataSet.VerificationListCategoryDataTable = AccessCategory.GetDataByCategoryID(Me.GetCategoryId)
                If TableCategory.Rows.Count > 0 Then
                    Dim TableRowCategory As AMLDAL.AMLDataSet.VerificationListCategoryRow = TableCategory.Rows(0)


                    Dim CategoryName = TableRowCategory.CategoryName

                    If (Me.GetCategoryId = 1) OrElse (Me.GetCategoryId = 2) OrElse (Me.GetCategoryId = 3) OrElse (Me.GetCategoryId = 4) OrElse (Me.GetCategoryId = 5) Then
                        Throw New Exception("The following Category : '" & CategoryName & "' cannot be deleted.")
                    Else
                        ViewState("CategoryID_Old") = TableRowCategory.CategoryID
                        Me.LabelCategoryID.Text = ViewState("CategoryID_Old")

                        ViewState("CategoryName_Old") = CategoryName
                        Me.TextCategoryName.Text = ViewState("CategoryName_Old")

                        ViewState("CategoryDescription_Old") = TableRowCategory.CategoryDescription
                        Me.TextCategoryDescription.Text = ViewState("CategoryDescription_Old")

                        ViewState("CategoryCreatedDate_Old") = TableRowCategory.CategoryCreatedDate
                    End If
                End If
            End Using
        End Using
    End Sub

    ''' <summary>
    ''' page load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.FillEditData()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    '        Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

End Class