Imports Sahassa.aml.Commonly
Imports SahassaNettier.Entities
Partial Class SuspiciusPersonDelete
    Inherits Parent


    Public ReadOnly Property PKSuspiciusPersonID() As Integer
        Get
            Dim temp As String = Request.Params("PKSuspiciusPersonID")
            Dim intresult As Integer
            If Integer.TryParse(temp, intresult) Then
                Return intresult
            Else
                Throw New Exception("PKSuspiciusPersonId must numeric")
            End If
        End Get
    End Property


    Public ReadOnly Property ObjSuspiciusPersonDelete() As SuspiciusPerson
        Get
            If Session("SuspiciusPersonDelete.ObjSuspiciusPersonDelete") Is Nothing Then
                Session("SuspiciusPersonDelete.ObjSuspiciusPersonDelete") = AMLBLL.SuspiciusPersonBLL.GetSuspiciusPersonByPk(Me.PKSuspiciusPersonID)
            End If
            Return Session("SuspiciusPersonDelete.ObjSuspiciusPersonDelete")
        End Get
    End Property

    Sub ClearSession()
        Session("SuspiciusPersonDelete.ObjSuspiciusPersonDelete") = Nothing
    End Sub

    Sub LoadDataSuspiciusPersonDelete()
        If Me.ObjSuspiciusPersonDelete Is Nothing Then
            Throw New Exception("Data Suspicius Person not Exist or Already Deleted.")
        Else
            With ObjSuspiciusPersonDelete

                lblName.Text = .Nama
                lblTglLahir.Text = .TanggalLahir.GetValueOrDefault(GetDefaultDate).ToString("dd-MMM-yyyy")
                lblTempatLahir.Text = .TempatLahir
                lblIdentityNo.Text = .NoIdentitas
                lblAddress.Text = .Alamat
                lblKecamatanKelurahan.Text = .KecamatanKeluarahan
                lblPhoneNumber.Text = .NoTelp
                lblJob.Text = .Pekerjaan
                lblworkplace.Text = .AlamatTempatKerja
                lblnpwp.Text = .NPWP
                lbldescription.Text = .Description
                lblcreatedby.Text = .UseridCreator

            End With


        End If
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If Not Me.IsPostBack Then
                ClearSession()
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)


                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                End Using

                LoadDataSuspiciusPersonDelete()
            End If

        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click

        Try
            Response.Redirect("SuspiciusPersonView.aspx", False)

        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click

        Try
            If Me.ObjSuspiciusPersonDelete Is Nothing Then
                Throw New Exception("Data Suspicius already deleted.")
            Else
                If SessionPkUserId = 1 Then
                    'delete superuser

                    AMLBLL.SuspiciusPersonBLL.DeleteSuspiciusPerson(ObjSuspiciusPersonDelete)
                    Response.Redirect("SuspiciusPersonView.aspx", False)
                Else
                    'delete approval
                    Using ojNewSuspiciusApprovaldetail As New Suspicius_ApprovalDetail
                        With ojNewSuspiciusApprovaldetail


                            .ModeID = 23
                            .PK_SuspiciusPerson_ID = ObjSuspiciusPersonDelete.PK_SuspiciusPerson_ID
                            .Nama = ObjSuspiciusPersonDelete.Nama
                            .TanggalLahir = ObjSuspiciusPersonDelete.TanggalLahir
                            .TempatLahir = ObjSuspiciusPersonDelete.TanggalLahir
                            .NoIdentitas = ObjSuspiciusPersonDelete.NoIdentitas
                            .Alamat = ObjSuspiciusPersonDelete.Alamat
                            .KecamatanKeluarahan = ObjSuspiciusPersonDelete.KecamatanKeluarahan
                            .NoTelp = ObjSuspiciusPersonDelete.NoTelp
                            .Pekerjaan = ObjSuspiciusPersonDelete.Pekerjaan
                            .AlamatTempatKerja = ObjSuspiciusPersonDelete.AlamatTempatKerja
                            .NPWP = ObjSuspiciusPersonDelete.NPWP
                            .Description = ObjSuspiciusPersonDelete.Description
                            .CreatedDate = ObjSuspiciusPersonDelete.CreatedDate
                            .LastUpdateDate = ObjSuspiciusPersonDelete.LastUpdateDate
                            .UseridCreator = ObjSuspiciusPersonDelete.UseridCreator                            
                        End With
                        AMLBLL.SuspiciusPersonBLL.DeleteApprovalSuspiciusPerson(ojNewSuspiciusApprovaldetail)
                        Dim MessagePendingID As Integer = 89803 'MessagePendingID 8201 = Group Add 

                        Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & lblName.Text.Trim

                        Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & lblName.Text.Trim, False)

                    End Using

                End If
            End If

        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub
End Class
