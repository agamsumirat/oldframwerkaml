#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports System.Collections.Generic
#End Region

Partial Class MsCurrency_edit
    Inherits Parent

#Region "Function"

    Private Sub LoadData()
        Dim ObjMsCurrency As MsCurrency = DataRepository.MsCurrencyProvider.GetPaged(MsCurrencyColumn.IdCurrency.ToString & "=" & parID, "", 0, 1, Nothing)(0)
        If ObjMsCurrency Is Nothing Then Throw New Exception("Data Not Found")
        With ObjMsCurrency
            SafeDefaultValue = "-"
            txtIdCurrency.Text = Safe(.IdCurrency)
            txtCode.Text = Safe(.Code)
            txtName.Text = Safe(.Name)

            chkactivation.checked = SafeBoolean(.Activation)
            Dim L_objMappingMsCurrencyNCBSPPATK As TList(Of MappingMsCurrencyNCBSPPATK)
            L_objMappingMsCurrencyNCBSPPATK = DataRepository.MappingMsCurrencyNCBSPPATKProvider.GetPaged(MappingMsCurrencyNCBSPPATKColumn.IdCurrency.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)


            Listmaping.AddRange(L_objMappingMsCurrencyNCBSPPATK)

        End With
    End Sub

    Private Sub BindListMapping()
        LBMapping.DataSource = ListMappingDisplay
        LBMapping.DataBind()
    End Sub

    ''' <summary>
    ''' Validasi men-handle seluruh syarat2 data valid
    ''' data yang lolos dari validasi dipastikan lolos ke database 
    ''' karena menggunakan FillorNothing
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ValidasiControl()
        '======================== Validasi textbox :  ID canot Null ====================================================
        If ObjectAntiNull(txtIdCurrency.Text) = False Then Throw New Exception("ID  must be filled  ")
        If Not IsNumeric(Me.txtIdCurrency.Text) Then
            Throw New Exception("ID must be in number format")
        End If
        '======================== Validasi textbox :  Code canot Null ====================================================
        If ObjectAntiNull(txtCode.Text) = False Then Throw New Exception("Code  must be filled  ")
        '======================== Validasi textbox :  Name canot Null ====================================================
        If ObjectAntiNull(txtName.Text) = False Then Throw New Exception("Name  must be filled  ")

        If DataRepository.MsCurrencyProvider.GetPaged("IDCurrency = '" & txtIdCurrency.Text & "' AND IDCurrency <> '" & parID & "'", "", 0, Integer.MaxValue, 0).Count > 0 Then
            Throw New Exception("ID already exist in the database")
        End If

        If DataRepository.MsCurrency_ApprovalDetailProvider.GetPaged(MsCurrency_ApprovalDetailColumn.IdCurrency.ToString & "='" & txtIdCurrency.Text & "' AND IDCurrency <> '" & parID & "'", "", 0, 1, Nothing).Count > 0 Then
            Throw New Exception("Data exist in List Waiting Approval")
        End If
        'If LBMapping.Items.Count = 0 Then Throw New Exception("data tidak punya list Mapping")
    End Sub

#End Region

#Region "events..."

    Protected Sub imgBrowse_click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBrowse.Click
        Try
            If Not Get_DataFromPicker Is Nothing Then
                Listmaping.AddRange(Get_DataFromPicker)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgDeleteItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgDeleteItem.Click
        Try
            If LBMapping.SelectedIndex < 0 Then
                Throw New Exception("No data Selected")
            End If
            Listmaping.RemoveAt(LBMapping.SelectedIndex)

        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
                Listmaping = New TList(Of MappingMsCurrencyNCBSPPATK)
                LoadData()
            Catch ex As Exception
                LogError(ex)
                CvalPageErr.IsValid = False
                CvalPageErr.ErrorMessage = ex.Message
            End Try
        End If

    End Sub

    Protected Sub ImgBtnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnSave.Click
        Try
            Page.Validate("handle")
            If Page.IsValid Then
                ValidasiControl()
                ' =========== Insert Header Approval
                Dim KeyHeaderApproval As Integer
                Using ObjMsCurrency_Approval As New MsCurrency_Approval
                    With ObjMsCurrency_Approval
                        FillOrNothing(.FK_MsMode_Id, 2, True, oInt)
                        FillOrNothing(.RequestedBy, SessionPkUserId)
                        FillOrNothing(.RequestedDate, Date.Now)
                        .IsUpload = False
                    End With
                    DataRepository.MsCurrency_ApprovalProvider.Save(ObjMsCurrency_Approval)
                    KeyHeaderApproval = ObjMsCurrency_Approval.PK_MsCurrencyPPATK_Approval_Id
                End Using
                '============ Insert Detail Approval
                Using objMsCurrency_ApprovalDetail As New MsCurrency_ApprovalDetail()
                    With objMsCurrency_ApprovalDetail
                        Dim ObjMsCurrency As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(parID)
                        FillOrNothing(.IdCurrency, ObjMsCurrency.IdCurrency)
                        FillOrNothing(.Name, ObjMsCurrency.Name)
                        FillOrNothing(.Activation, ObjMsCurrency.Activation)
                        FillOrNothing(.CreatedDate, ObjMsCurrency.CreatedDate)
                        FillOrNothing(.CreatedBy, ObjMsCurrency.CreatedBy)
                        FillOrNothing(.PK_MsCurrency_Approval_id, KeyHeaderApproval)

                        '==== Edit data =============
                        FillOrNothing(.IdCurrency, txtIdCurrency.Text, True, oInt)
                        FillOrNothing(.Code, txtCode.Text, True, Ovarchar)
                        FillOrNothing(.Name, txtName.Text, True, Ovarchar)

                        FillOrNothing(.Activation, chkActivation.checked, True, oBit)
                        FillOrNothing(.LastUpdatedBy, SessionUserId)
                        FillOrNothing(.LastUpdatedDate, Date.Now)

                    End With
                    DataRepository.MsCurrency_ApprovalDetailProvider.Save(objMsCurrency_ApprovalDetail)
                    Dim keyHeaderApprovalDetail As Integer = objMsCurrency_ApprovalDetail.PK_MsCurrency_Approval_detail_id
                    '========= Insert mapping item 
                    Dim LobjMappingMsCurrencyNCBSPPATK_Approval_Detail As New TList(Of MappingMsCurrencyNCBSPPATK_Approval_Detail)
                    For Each objMappingMsCurrencyNCBSPPATK As MappingMsCurrencyNCBSPPATK In Listmaping
                        Dim Mapping As New MappingMsCurrencyNCBSPPATK_Approval_Detail
                        With Mapping
                            FillOrNothing(.Pk_MsCurrency_Id, objMsCurrency_ApprovalDetail.IdCurrency)
                            FillOrNothing(.Pk_MsCurrencyPPATK_Id, objMappingMsCurrencyNCBSPPATK.IdCurrencyNCBS)
                            FillOrNothing(.PK_MappingMsCurrencyNCBSPPATK_Approval_Id, KeyHeaderApproval)
                            FillOrNothing(.Nama, objMappingMsCurrencyNCBSPPATK.Nama)
                            FillOrNothing(.PK_MappingMsCurrencyNCBSPPATK_Approval_Detail_Id, keyHeaderApprovalDetail)
                            LobjMappingMsCurrencyNCBSPPATK_Approval_Detail.Add(Mapping)
                        End With
                    Next
                    DataRepository.MappingMsCurrencyNCBSPPATK_Approval_DetailProvider.Save(LobjMappingMsCurrencyNCBSPPATK_Approval_Detail)
                    'session Maping item Clear
                    Listmaping.Clear()
                    ImgBtnSave.Visible = False
                    ImgBackAdd.Visible = False
                    LblConfirmation.Text = "Data has been added and waiting for approval"
                    MtvMsUser.ActiveViewIndex = 1
                End Using
            End If
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub



    Protected Sub ImgBackAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBackAdd.Click
        Response.Redirect("MsCurrency_View.aspx")
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            BindListMapping()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBack.Click
        Response.Redirect("MsCurrency_View.aspx")
    End Sub
#End Region

#Region "Property..."
    Public ReadOnly Property parID() As String
        Get
            Return Request.Item("ID")
        End Get
    End Property
    ''' <summary>
    ''' Mengambil Data dari Picker
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Get_DataFromPicker() As TList(Of MappingMsCurrencyNCBSPPATK)
        Get
            Dim Temp As New TList(Of MappingMsCurrencyNCBSPPATK)
            Dim L_MsCurrencyNCBS As TList(Of MsCurrencyNCBS)
            Try
                If Session("PickerMsCurrencyNCBS.Data") Is Nothing Then
                    Throw New Exception
                End If
                L_MsCurrencyNCBS = Session("PickerMsCurrencyNCBS.Data")
                For Each i As MsCurrencyNCBS In L_MsCurrencyNCBS
                    Dim Tempmapping As New MappingMsCurrencyNCBSPPATK
                    With Tempmapping
                        .IdCurrencyNCBS = i.IdCurrencyNCBS
                        .Nama = i.Description
                    End With
                    Temp.Add(Tempmapping)
                Next
            Catch ex As Exception
                L_MsCurrencyNCBS = Nothing
            End Try
            Return Temp
        End Get
    End Property
    ''' <summary>
    ''' Menyimpan Item untuk mapping sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Listmaping() As TList(Of MappingMsCurrencyNCBSPPATK)
        Get
            Return Session("Listmaping.data")
        End Get
        Set(ByVal value As TList(Of MappingMsCurrencyNCBSPPATK))
            Session("Listmaping.data") = value
        End Set
    End Property
    ''' <summary>
    ''' List yang tampil di ListBox
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplay() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingMsCurrencyNCBSPPATK In Listmaping.FindAllDistinct("IdCurrencyNCBS")
                Temp.Add(i.IdCurrencyNCBS.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property
#End Region

End Class



