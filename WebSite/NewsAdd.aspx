<%@ Page Language="VB" MasterPageFile="~/masterpage.master" AutoEventWireup="false"
    CodeFile="NewsAdd.aspx.vb" Inherits="NewsAdd" Title="News Add" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    <table id="title" border="0" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
        height="72" style="border-top-style: none; border-right-style: none; border-left-style: none;
        border-bottom-style: none" width="100%">
        <tr>
            <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>News - Add New&nbsp;
                    <hr />
                </strong>
                <asp:Label ID="LblSuccess" runat="server" CssClass="validationok" Visible="False"
                    Width="100%"></asp:Label>
            </td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" colspan="4" style="border-top-style: none; border-right-style: none;
                border-left-style: none; height: 24px; border-bottom-style: none">
                <span style="color: #ff0000">* Required</span>
            </td>
        </tr>
    </table>

    <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
        border="2" height="72">
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
                <%--<asp:RequiredFieldValidator ID="RequiredFieldNewsTitle" runat="server" ErrorMessage="News Title is required"
                    ControlToValidate="TextNewsTitle">*</asp:RequiredFieldValidator>--%>
            </td>
            <td width="20%" bgcolor="#ffffff" style="height: 24px">
                Title
            </td>
            <td width="5" bgcolor="#ffffff" style="height: 24px">
                :
            </td>
            <td width="80%" bgcolor="#ffffff" style="height: 24px">
            <strong><span style="color: #ff0000">*</span></strong>
            <CKEditor:CKEditorControl ID="TextNewsTitleCK" runat="server" MaxLength="255" Toolbar="Basic"></CKEditor:CKEditorControl>
                <asp:TextBox ID="TextNewsTitle" runat="server" CssClass="textBox" MaxLength="100"
                    Width="500px" Visible="False"></asp:TextBox>                
                <asp:Label ID="lblJudul" runat="server"></asp:Label>
            </td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" rowspan="1" style="width: 24px; height: 24px; border-top-style: none;
                border-right-style: none; border-left-style: none; border-bottom-style: none;">
                <asp:RequiredFieldValidator ID="RequiredfieldStartDate" runat="server" ControlToValidate="TextStartDate"
                    ErrorMessage="Start Date cannot be blank">*</asp:RequiredFieldValidator><br />
            </td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px">
                Start Date
            </td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px">
                :
            </td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px">
                <asp:TextBox ID="TextStartDate" runat="server" CssClass="textBox" MaxLength="20"></asp:TextBox>
                <strong><span style="color: #ff0000">*
                    <input id="cmdStartDatePicker" runat="server" name="popUpCalc" style="border-right: #ffffff 0px solid;
                        border-top: #ffffff 0px solid; font-size: 11px; border-left: #ffffff 0px solid;
                        border-bottom: #ffffff 0px solid; height: 17px; background-image: url(Script/Calendar/cal.gif);
                        width: 16px;" title="Click to show calendar" type="button" />&nbsp;</span></strong>
            </td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" rowspan="1" style="width: 24px; height: 24px; border-top-style: none;
                border-right-style: none; border-left-style: none; border-bottom-style: none;">
                <asp:RequiredFieldValidator ID="RequiredfieldEndDate" runat="server" ControlToValidate="TextEndDate"
                    ErrorMessage="End Date cannot be blank">*</asp:RequiredFieldValidator><br />
            </td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px">
                End Date
            </td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px">
                :
            </td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px">
                <asp:TextBox ID="TextEndDate" runat="server" CssClass="textBox" MaxLength="20"></asp:TextBox>
                <strong><span style="color: #ff0000">*
                    <input id="cmdEndDatePicker" runat="server" name="popUpCalc" style="border-right: #ffffff 0px solid;
                        border-top: #ffffff 0px solid; font-size: 11px; border-left: #ffffff 0px solid;
                        border-bottom: #ffffff 0px solid; height: 17px; background-image: url(Script/Calendar/cal.gif);
                        width: 16px;" title="Click to show calendar" type="button" />&nbsp;</span></strong>
            </td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; height: 24px;">
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" style="height: 24px">
                News Summary<br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" style="height: 24px">
                :<br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" style="height: 24px">
                &nbsp;<strong><span style="color: #ff0000">*
                    <CKEditor:CKEditorControl ID="TextNewsSummary" runat="server" MaxLength="255" Toolbar="Basic"></CKEditor:CKEditorControl></span></strong>
            </td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" style="height: 24px">
                News Content<br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" style="height: 24px">
                :<br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" style="height: 24px">
                <CKEditor:CKEditorControl ID="TextNewsContent" runat="server" Toolbar="Basic" MaxLength="8000"></CKEditor:CKEditorControl>
                <strong><span style="color: #ff0000">*</span></strong>
            </td>
        </tr>
        <tr>
         <td bgcolor="#ffffff" style="width: 24px; height: 24px">
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td nowrap="noWrap" width="1%" bgcolor="#ffffff">
                Attachment
            </td>
            <td width="1"bgcolor="#ffffff">
                :
            </td>
            <td width="99%" bgcolor="#ffffff">
                <asp:FileUpload ID="FileUpload1" runat="server" />
                <asp:Button ID="ButtonAdd" runat="server" Text="Add" ValidationGroup="FillInvestigation" />
                <br />
                <br />
                <asp:ListBox ID="ListAttachment" runat="server" Width="400px"></asp:ListBox>
                <asp:Button ID="ButtonDelete" runat="server" Text="Delete" />
            </td>
        </tr>        
        <tr class="formText" bgcolor="#dddddd" height="30">
            <td style="width: 24px">
                <img height="15" src="images/arrow.gif" width="15">
            </td>
            <td colspan="3" style="height: 9px">
                <table cellspacing="0" cellpadding="3" border="0">
                    <tr>
                        <td>
                            <asp:ImageButton ID="ImageSave" runat="server" CausesValidation="True" SkinID="AddButton">
                            </asp:ImageButton>
                        </td>
                        <td>
                            <asp:ImageButton ID="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton">
                            </asp:ImageButton>
                        </td>
                    </tr>
                </table>
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="none"></asp:CustomValidator>
            </td>
        </tr>
    </table>
</asp:Content>
