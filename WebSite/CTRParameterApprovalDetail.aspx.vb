Imports AMLDAL
Imports System.Data.SqlClient
Imports Sahassa.AML.TableAdapterHelper
Partial Class CTRParameterApprovalDetail
    Inherits Parent

    Public Enum EnCTRParameterType
        TransactionAmmount = 1
        CTREmailTo
        CTREmailCC
        CTREmailBCC
        CTREmailSubject
        CTREmailBody
        CTRPIC
    End Enum
    Private ReadOnly Property PKPendingID() As Long
        Get
            Dim temp As String
            temp = Request.Params("PKPendingID")
            If Not IsNumeric(temp) Then
                Try
                    Throw New Exception("PK Pending is not valid")
                Catch ex As Exception
                    cvalPageErr.IsValid = False
                    cvalPageErr.ErrorMessage = ex.Message
                End Try
            Else
                Return temp
            End If
        End Get
    End Property


    Private _oRowCTRApprovalDetailEmailBody As AMLDAL.CTRParameter.CTRParameter_ApprovalIDRow
    Private ReadOnly Property oRowCTRApprovalDetailEmailBody() As AMLDAL.CTRParameter.CTRParameter_ApprovalIDRow
        Get
            Try
                Using adapter As New AMLDAL.CTRParameterTableAdapters.CTRParameter_ApprovalIDTableAdapter
                    Dim orows() As CTRParameter.CTRParameter_ApprovalIDRow = adapter.GetDataByFK(Me.PKPendingID).Select("PK_CTRParameterId=" & EnCTRParameterType.CTREmailBody)
                    If orows.Length > 0 Then
                        _oRowCTRApprovalDetailEmailBody = orows(0)
                        Return _oRowCTRApprovalDetailEmailBody
                    Else
                        _oRowCTRApprovalDetailEmailBody = Nothing
                        Return _oRowCTRApprovalDetailEmailBody
                    End If

                End Using
            Catch ex As Exception
                LogError(ex)
                Throw
            End Try

        End Get
    End Property
    Private _oRowCTRApprovalDetailEmailSubject As AMLDAL.CTRParameter.CTRParameter_ApprovalIDRow
    Private ReadOnly Property oRowCTRApprovalDetailEmailSubject() As AMLDAL.CTRParameter.CTRParameter_ApprovalIDRow
        Get
            Try
                Using adapter As New AMLDAL.CTRParameterTableAdapters.CTRParameter_ApprovalIDTableAdapter
                    Dim orows() As CTRParameter.CTRParameter_ApprovalIDRow = adapter.GetDataByFK(Me.PKPendingID).Select("PK_CTRParameterId=" & EnCTRParameterType.CTREmailSubject)
                    If orows.Length > 0 Then
                        _oRowCTRApprovalDetailEmailSubject = orows(0)
                        Return _oRowCTRApprovalDetailEmailSubject
                    Else
                        _oRowCTRApprovalDetailEmailSubject = Nothing
                        Return _oRowCTRApprovalDetailEmailSubject
                    End If

                End Using
            Catch ex As Exception
                LogError(ex)
                Throw
            End Try

        End Get
    End Property
    Private _oRowCTRApprovalDetailEmailBCC As AMLDAL.CTRParameter.CTRParameter_ApprovalIDRow
    Private ReadOnly Property oRowCTRApprovalDetailEmailBCC() As AMLDAL.CTRParameter.CTRParameter_ApprovalIDRow
        Get
            Try
                Using adapter As New AMLDAL.CTRParameterTableAdapters.CTRParameter_ApprovalIDTableAdapter
                    Dim orows() As CTRParameter.CTRParameter_ApprovalIDRow = adapter.GetDataByFK(Me.PKPendingID).Select("PK_CTRParameterId=" & EnCTRParameterType.CTREmailBCC)
                    If orows.Length > 0 Then
                        _oRowCTRApprovalDetailEmailBCC = orows(0)
                        Return _oRowCTRApprovalDetailEmailBCC
                    Else
                        _oRowCTRApprovalDetailEmailBCC = Nothing
                        Return _oRowCTRApprovalDetailEmailBCC
                    End If

                End Using
            Catch ex As Exception
                LogError(ex)
                Throw
            End Try

        End Get
    End Property
    Private _oRowCTRApprovalDetailEmailCC As AMLDAL.CTRParameter.CTRParameter_ApprovalIDRow
    Private ReadOnly Property oRowCTRApprovalDetailEmailCC() As AMLDAL.CTRParameter.CTRParameter_ApprovalIDRow
        Get
            Try
                Using adapter As New AMLDAL.CTRParameterTableAdapters.CTRParameter_ApprovalIDTableAdapter
                    Dim orows() As CTRParameter.CTRParameter_ApprovalIDRow = adapter.GetDataByFK(Me.PKPendingID).Select("PK_CTRParameterId=" & EnCTRParameterType.CTREmailCC)
                    If orows.Length > 0 Then
                        _oRowCTRApprovalDetailEmailCC = orows(0)
                        Return _oRowCTRApprovalDetailEmailCC
                    Else
                        _oRowCTRApprovalDetailEmailCC = Nothing
                        Return _oRowCTRApprovalDetailEmailCC
                    End If

                End Using
            Catch ex As Exception
                LogError(ex)
                Throw
            End Try

        End Get
    End Property
    Private _oRowCTRApprovalDetailEmailTo As AMLDAL.CTRParameter.CTRParameter_ApprovalIDRow
    Private ReadOnly Property oRowCTRApprovalDetailEmailTo() As AMLDAL.CTRParameter.CTRParameter_ApprovalIDRow
        Get
            Try
                Using adapter As New AMLDAL.CTRParameterTableAdapters.CTRParameter_ApprovalIDTableAdapter
                    Dim orows() As CTRParameter.CTRParameter_ApprovalIDRow = adapter.GetDataByFK(Me.PKPendingID).Select("PK_CTRParameterId=" & EnCTRParameterType.CTREmailTo)
                    If orows.Length > 0 Then
                        _oRowCTRApprovalDetailEmailTo = orows(0)
                        Return _oRowCTRApprovalDetailEmailTo
                    Else
                        _oRowCTRApprovalDetailEmailTo = Nothing
                        Return _oRowCTRApprovalDetailEmailTo
                    End If

                End Using
            Catch ex As Exception
                LogError(ex)
                Throw
            End Try

        End Get
    End Property
    Private _oRowCTRApprovalDetailAmmount As AMLDAL.CTRParameter.CTRParameter_ApprovalIDRow
    Private ReadOnly Property oRowCTRApprovalDetailAmmount() As AMLDAL.CTRParameter.CTRParameter_ApprovalIDRow
        Get
            Try
                Using adapter As New AMLDAL.CTRParameterTableAdapters.CTRParameter_ApprovalIDTableAdapter
                    Dim orows() As CTRParameter.CTRParameter_ApprovalIDRow = adapter.GetDataByFK(Me.PKPendingID).Select("PK_CTRParameterId=" & EnCTRParameterType.TransactionAmmount)
                    If orows.Length > 0 Then
                        _oRowCTRApprovalDetailAmmount = orows(0)
                        Return _oRowCTRApprovalDetailAmmount
                    Else
                        _oRowCTRApprovalDetailAmmount = Nothing
                        Return _oRowCTRApprovalDetailAmmount
                    End If

                End Using
            Catch ex As Exception
                LogError(ex)
                Throw
            End Try

        End Get
    End Property

    Private _oRowCTRApprovalDetailPIC As AMLDAL.CTRParameter.CTRParameter_ApprovalIDRow
    Private ReadOnly Property oRowCTRApprovalDetailPIC() As AMLDAL.CTRParameter.CTRParameter_ApprovalIDRow
        Get
            Try
                Using adapter As New AMLDAL.CTRParameterTableAdapters.CTRParameter_ApprovalIDTableAdapter
                    Dim orows() As CTRParameter.CTRParameter_ApprovalIDRow = adapter.GetDataByFK(Me.PKPendingID).Select("PK_CTRParameterId=" & EnCTRParameterType.CTRPIC)
                    If orows.Length > 0 Then
                        _oRowCTRApprovalDetailPIC = orows(0)
                        Return _oRowCTRApprovalDetailPIC
                    Else
                        _oRowCTRApprovalDetailPIC = Nothing
                        Return _oRowCTRApprovalDetailPIC
                    End If

                End Using
            Catch ex As Exception
                LogError(ex)
                Throw
            End Try

        End Get
    End Property

    Private _oRowCTRPendingApproval As AMLDAL.CTRParameter.CTRParameter_PendingApprovalRow
    Private ReadOnly Property oRowCTRPendingApproval() As AMLDAL.CTRParameter.CTRParameter_PendingApprovalRow
        Get
            Try
                Using adapter As New CTRParameterTableAdapters.CTRParameter_PendingApprovalTableAdapter
                    Using otable As CTRParameter.CTRParameter_PendingApprovalDataTable = adapter.GetDataFK(Me.PKPendingID)
                        If otable.Rows.Count > 0 Then
                            _oRowCTRPendingApproval = otable.Rows(0)
                            Return _oRowCTRPendingApproval
                        Else
                            _oRowCTRPendingApproval = Nothing
                            Return _oRowCTRPendingApproval
                        End If

                    End Using
                End Using
            Catch ex As Exception
                LogError(ex)
                Throw
            End Try
        End Get
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.ImageAccept.Attributes.Add("onclick", "javascript: if(confirm('Do you want to accept this activity ?')== false) return false;")
            Me.ImageReject.Attributes.Add("onclick", "javascript: if(confirm('Do you want to reject this activity ?')== false) return false;")
            Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)


            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)


            End Using
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImageAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click
        Dim oTrans As SqlTransaction = Nothing
        Try
            If Not oRowCTRApprovalDetailAmmount Is Nothing Then
                Using adapter As New CTRParameterTableAdapters.CTRParameterTableAdapter
                    oTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(adapter)
                    adapter.UpdateCTRValueByPK(oRowCTRApprovalDetailAmmount.CTRParameterValue, oRowCTRApprovalDetailAmmount.PK_CTRParameterId)
                    If Not oRowCTRApprovalDetailEmailTo Is Nothing Then
                        adapter.UpdateCTRValueByPK(oRowCTRApprovalDetailEmailTo.CTRParameterValue, oRowCTRApprovalDetailEmailTo.PK_CTRParameterId)
                    End If
                    If Not oRowCTRApprovalDetailEmailCC Is Nothing Then
                        adapter.UpdateCTRValueByPK(oRowCTRApprovalDetailEmailCC.CTRParameterValue, oRowCTRApprovalDetailEmailCC.PK_CTRParameterId)
                    End If
                    If Not oRowCTRApprovalDetailEmailBCC Is Nothing Then
                        adapter.UpdateCTRValueByPK(oRowCTRApprovalDetailEmailBCC.CTRParameterValue, oRowCTRApprovalDetailEmailBCC.PK_CTRParameterId)
                    End If
                    If Not oRowCTRApprovalDetailEmailSubject Is Nothing Then
                        adapter.UpdateCTRValueByPK(oRowCTRApprovalDetailEmailSubject.CTRParameterValue, oRowCTRApprovalDetailEmailSubject.PK_CTRParameterId)
                    End If
                    If Not oRowCTRApprovalDetailEmailBody Is Nothing Then
                        adapter.UpdateCTRValueByPK(oRowCTRApprovalDetailEmailBody.CTRParameterValue, oRowCTRApprovalDetailEmailBody.PK_CTRParameterId)
                    End If
                    If Not oRowCTRApprovalDetailPIC Is Nothing Then
                        adapter.UpdateCTRValueByPK(oRowCTRApprovalDetailPIC.CTRParameterValue, oRowCTRApprovalDetailEmailBody.PK_CTRParameterId)
                    End If
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                        SetTransaction(AccessAudit, oTrans)
                        AccessAudit.Insert(Now, oRowCTRPendingApproval.CTRParameter_PendingApprovalUserID, Sahassa.AML.Commonly.SessionUserId, "CTR Parameter", "CTRParameterName", "Edit", oRowCTRApprovalDetailAmmount.CTRParameterName_Old, oRowCTRApprovalDetailAmmount.CTRParameterName, "Accepted")
                        AccessAudit.Insert(Now, oRowCTRPendingApproval.CTRParameter_PendingApprovalUserID, Sahassa.AML.Commonly.SessionUserId, "CTR Parameter", "CTRParameterValue", "Edit", oRowCTRApprovalDetailAmmount.CTRParameterValue_Old, oRowCTRApprovalDetailAmmount.CTRParameterValue, "Accepted")
                        AccessAudit.Insert(Now, oRowCTRPendingApproval.CTRParameter_PendingApprovalUserID, Sahassa.AML.Commonly.SessionUserId, "CTR Parameter", "CTRParameterName", "Edit", oRowCTRApprovalDetailEmailTo.CTRParameterName_Old, oRowCTRApprovalDetailEmailTo.CTRParameterName, "Accepted")
                        AccessAudit.Insert(Now, oRowCTRPendingApproval.CTRParameter_PendingApprovalUserID, Sahassa.AML.Commonly.SessionUserId, "CTR Parameter", "CTRParameterValue", "Edit", oRowCTRApprovalDetailEmailTo.CTRParameterValue_Old, oRowCTRApprovalDetailEmailTo.CTRParameterValue, "Accepted")
                        AccessAudit.Insert(Now, oRowCTRPendingApproval.CTRParameter_PendingApprovalUserID, Sahassa.AML.Commonly.SessionUserId, "CTR Parameter", "CTRParameterName", "Edit", oRowCTRApprovalDetailEmailCC.CTRParameterName_Old, oRowCTRApprovalDetailEmailCC.CTRParameterName, "Accepted")
                        AccessAudit.Insert(Now, oRowCTRPendingApproval.CTRParameter_PendingApprovalUserID, Sahassa.AML.Commonly.SessionUserId, "CTR Parameter", "CTRParameterValue", "Edit", oRowCTRApprovalDetailEmailCC.CTRParameterValue_Old, oRowCTRApprovalDetailEmailCC.CTRParameterValue, "Accepted")
                        AccessAudit.Insert(Now, oRowCTRPendingApproval.CTRParameter_PendingApprovalUserID, Sahassa.AML.Commonly.SessionUserId, "CTR Parameter", "CTRParameterName", "Edit", oRowCTRApprovalDetailEmailBCC.CTRParameterName_Old, oRowCTRApprovalDetailEmailBCC.CTRParameterName, "Accepted")
                        AccessAudit.Insert(Now, oRowCTRPendingApproval.CTRParameter_PendingApprovalUserID, Sahassa.AML.Commonly.SessionUserId, "CTR Parameter", "CTRParameterValue", "Edit", oRowCTRApprovalDetailEmailBCC.CTRParameterValue_Old, oRowCTRApprovalDetailEmailBCC.CTRParameterValue, "Accepted")
                        AccessAudit.Insert(Now, oRowCTRPendingApproval.CTRParameter_PendingApprovalUserID, Sahassa.AML.Commonly.SessionUserId, "CTR Parameter", "CTRParameterName", "Edit", oRowCTRApprovalDetailEmailSubject.CTRParameterName_Old, oRowCTRApprovalDetailEmailSubject.CTRParameterName, "Accepted")
                        AccessAudit.Insert(Now, oRowCTRPendingApproval.CTRParameter_PendingApprovalUserID, Sahassa.AML.Commonly.SessionUserId, "CTR Parameter", "CTRParameterValue", "Edit", oRowCTRApprovalDetailEmailSubject.CTRParameterValue_Old, oRowCTRApprovalDetailEmailSubject.CTRParameterValue, "Accepted")
                        AccessAudit.Insert(Now, oRowCTRPendingApproval.CTRParameter_PendingApprovalUserID, Sahassa.AML.Commonly.SessionUserId, "CTR Parameter", "CTRParameterName", "Edit", oRowCTRApprovalDetailEmailBody.CTRParameterName_Old, oRowCTRApprovalDetailEmailBody.CTRParameterName, "Accepted")
                        AccessAudit.Insert(Now, oRowCTRPendingApproval.CTRParameter_PendingApprovalUserID, Sahassa.AML.Commonly.SessionUserId, "CTR Parameter", "CTRParameterValue", "Edit", oRowCTRApprovalDetailEmailBody.CTRParameterValue_Old, oRowCTRApprovalDetailEmailBody.CTRParameterValue, "Accepted")
                        AccessAudit.Insert(Now, oRowCTRPendingApproval.CTRParameter_PendingApprovalUserID, Sahassa.AML.Commonly.SessionUserId, "CTR Parameter", "CTRParameterName", "Edit", oRowCTRApprovalDetailPIC.CTRParameterName_Old, oRowCTRApprovalDetailPIC.CTRParameterName, "Accepted")
                        AccessAudit.Insert(Now, oRowCTRPendingApproval.CTRParameter_PendingApprovalUserID, Sahassa.AML.Commonly.SessionUserId, "CTR Parameter", "CTRParameterValue", "Edit", oRowCTRApprovalDetailPIC.CTRParameterValue_Old, oRowCTRApprovalDetailPIC.CTRParameterValue, "Accepted")

                    End Using
                    'delete detail
                    Using adapterDetail As New CTRParameterTableAdapters.CTRParameter_ApprovalIDTableAdapter
                        SetTransaction(adapterDetail, oTrans)
                        adapterDetail.DeleteByFKPending(Me.PKPendingID)
                    End Using
                    Using adapterMaster As New CTRParameterTableAdapters.CTRParameter_PendingApprovalTableAdapter
                        SetTransaction(adapterMaster, oTrans)
                        adapterMaster.DeleteByPK(Me.PKPendingID)
                    End Using
                    oTrans.Commit()
                End Using
            End If
            Sahassa.AML.Commonly.SessionIntendedPage = "CTRParameterApproval.aspx"
            Me.Response.Redirect("CTRParameterApproval.aspx", False)

        Catch ex As Exception
            If Not oTrans Is Nothing Then
                oTrans.Rollback()
            End If
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        Finally
            If Not oTrans Is Nothing Then
                oTrans.Dispose()
            End If
        End Try
    End Sub

    Protected Sub ImageReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click
        Dim otrans As SqlTransaction = Nothing
        Try
            'insert audittrail
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                otrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAudit)
                AccessAudit.Insert(Now, oRowCTRPendingApproval.CTRParameter_PendingApprovalUserID, Sahassa.AML.Commonly.SessionUserId, "CTR Parameter", "CTRParameterName", "Edit", oRowCTRApprovalDetailAmmount.CTRParameterName_Old, oRowCTRApprovalDetailAmmount.CTRParameterName, "Rejected")
                AccessAudit.Insert(Now, oRowCTRPendingApproval.CTRParameter_PendingApprovalUserID, Sahassa.AML.Commonly.SessionUserId, "CTR Parameter", "CTRParameterValue", "Edit", oRowCTRApprovalDetailAmmount.CTRParameterValue_Old, oRowCTRApprovalDetailAmmount.CTRParameterValue, "Rejected")
                AccessAudit.Insert(Now, oRowCTRPendingApproval.CTRParameter_PendingApprovalUserID, Sahassa.AML.Commonly.SessionUserId, "CTR Parameter", "CTRParameterName", "Edit", oRowCTRApprovalDetailEmailTo.CTRParameterName_Old, oRowCTRApprovalDetailEmailTo.CTRParameterName, "Rejected")
                AccessAudit.Insert(Now, oRowCTRPendingApproval.CTRParameter_PendingApprovalUserID, Sahassa.AML.Commonly.SessionUserId, "CTR Parameter", "CTRParameterValue", "Edit", oRowCTRApprovalDetailEmailTo.CTRParameterValue_Old, oRowCTRApprovalDetailEmailTo.CTRParameterValue, "Rejected")
                AccessAudit.Insert(Now, oRowCTRPendingApproval.CTRParameter_PendingApprovalUserID, Sahassa.AML.Commonly.SessionUserId, "CTR Parameter", "CTRParameterName", "Edit", oRowCTRApprovalDetailEmailCC.CTRParameterName_Old, oRowCTRApprovalDetailEmailCC.CTRParameterName, "Rejected")
                AccessAudit.Insert(Now, oRowCTRPendingApproval.CTRParameter_PendingApprovalUserID, Sahassa.AML.Commonly.SessionUserId, "CTR Parameter", "CTRParameterValue", "Edit", oRowCTRApprovalDetailEmailCC.CTRParameterValue_Old, oRowCTRApprovalDetailEmailCC.CTRParameterValue, "Rejected")
                AccessAudit.Insert(Now, oRowCTRPendingApproval.CTRParameter_PendingApprovalUserID, Sahassa.AML.Commonly.SessionUserId, "CTR Parameter", "CTRParameterName", "Edit", oRowCTRApprovalDetailEmailBCC.CTRParameterName_Old, oRowCTRApprovalDetailEmailBCC.CTRParameterName, "Rejected")
                AccessAudit.Insert(Now, oRowCTRPendingApproval.CTRParameter_PendingApprovalUserID, Sahassa.AML.Commonly.SessionUserId, "CTR Parameter", "CTRParameterValue", "Edit", oRowCTRApprovalDetailEmailBCC.CTRParameterValue_Old, oRowCTRApprovalDetailEmailBCC.CTRParameterValue, "Rejected")
                AccessAudit.Insert(Now, oRowCTRPendingApproval.CTRParameter_PendingApprovalUserID, Sahassa.AML.Commonly.SessionUserId, "CTR Parameter", "CTRParameterName", "Edit", oRowCTRApprovalDetailEmailSubject.CTRParameterName_Old, oRowCTRApprovalDetailEmailSubject.CTRParameterName, "Rejected")
                AccessAudit.Insert(Now, oRowCTRPendingApproval.CTRParameter_PendingApprovalUserID, Sahassa.AML.Commonly.SessionUserId, "CTR Parameter", "CTRParameterValue", "Edit", oRowCTRApprovalDetailEmailSubject.CTRParameterValue_Old, oRowCTRApprovalDetailEmailSubject.CTRParameterValue, "Rejected")
                AccessAudit.Insert(Now, oRowCTRPendingApproval.CTRParameter_PendingApprovalUserID, Sahassa.AML.Commonly.SessionUserId, "CTR Parameter", "CTRParameterName", "Edit", oRowCTRApprovalDetailEmailBody.CTRParameterName_Old, oRowCTRApprovalDetailEmailBody.CTRParameterName, "Rejected")
                AccessAudit.Insert(Now, oRowCTRPendingApproval.CTRParameter_PendingApprovalUserID, Sahassa.AML.Commonly.SessionUserId, "CTR Parameter", "CTRParameterValue", "Edit", oRowCTRApprovalDetailEmailBody.CTRParameterValue_Old, oRowCTRApprovalDetailEmailBody.CTRParameterValue, "Rejected")
                AccessAudit.Insert(Now, oRowCTRPendingApproval.CTRParameter_PendingApprovalUserID, Sahassa.AML.Commonly.SessionUserId, "CTR Parameter", "CTRParameterName", "Edit", oRowCTRApprovalDetailPIC.CTRParameterName_Old, oRowCTRApprovalDetailPIC.CTRParameterName, "Rejected")
                AccessAudit.Insert(Now, oRowCTRPendingApproval.CTRParameter_PendingApprovalUserID, Sahassa.AML.Commonly.SessionUserId, "CTR Parameter", "CTRParameterValue", "Edit", oRowCTRApprovalDetailPIC.CTRParameterValue_Old, oRowCTRApprovalDetailPIC.CTRParameterValue, "Rejected")


                'delete detail
                Using adapter As New CTRParameterTableAdapters.CTRParameter_ApprovalIDTableAdapter
                    SetTransaction(adapter, otrans)
                    adapter.DeleteByFKPending(Me.PKPendingID)
                End Using
                Using adapter As New CTRParameterTableAdapters.CTRParameter_PendingApprovalTableAdapter
                    SetTransaction(adapter, otrans)
                    adapter.DeleteByPK(Me.PKPendingID)
                End Using
                'delete master
                otrans.Commit()
            End Using
            Sahassa.AML.Commonly.SessionIntendedPage = "CTRParameterApproval.aspx"
            Me.Response.Redirect("CTRParameterApproval.aspx", False)
        Catch ex As Exception
            If Not otrans Is Nothing Then
                otrans.Rollback()
            End If
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        Finally

            If Not otrans Is Nothing Then
                otrans.Dispose()
            End If
        End Try
    End Sub

    Protected Sub ImageBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "CTRParameterApproval.aspx"

        Me.Response.Redirect("CTRParameterApproval.aspx", False)
    End Sub
    Private Sub LoadData()
        Try
            If Not oRowCTRApprovalDetailAmmount Is Nothing Then
                If Not oRowCTRApprovalDetailAmmount.IsCTRParameterValueNull Then
                    LabelTransactionAmmountNew.Text = Convert.ToDecimal(oRowCTRApprovalDetailAmmount.CTRParameterValue).ToString("###,###.00")
                End If
                If Not oRowCTRApprovalDetailAmmount.IsCTRParameterValue_OldNull Then
                    LabelTransactionAmmountOld.Text = Convert.ToDecimal(oRowCTRApprovalDetailAmmount.CTRParameterValue_Old).ToString("###,###.00")
                End If
            End If


            If Not oRowCTRApprovalDetailEmailTo Is Nothing Then
                If Not oRowCTRApprovalDetailEmailTo.IsCTRParameterValueNull Then
                    LabelCTREmailToNew.Text = oRowCTRApprovalDetailEmailTo.CTRParameterValue
                End If
                If Not oRowCTRApprovalDetailEmailTo.IsCTRParameterValue_OldNull Then
                    LabelCTREmailToOld.Text = oRowCTRApprovalDetailEmailTo.CTRParameterValue_Old
                End If
            End If

            If Not oRowCTRApprovalDetailEmailCC Is Nothing Then
                If Not oRowCTRApprovalDetailEmailCC.IsCTRParameterValueNull Then
                    LabelCTREmailCCNew.Text = oRowCTRApprovalDetailEmailCC.CTRParameterValue
                End If

                If Not oRowCTRApprovalDetailEmailCC.IsCTRParameterValue_OldNull Then
                    LabelCTREmailCCOld.Text = oRowCTRApprovalDetailEmailCC.CTRParameterValue_Old
                End If
            End If

            If Not oRowCTRApprovalDetailEmailBCC Is Nothing Then
                If Not oRowCTRApprovalDetailEmailBCC.IsCTRParameterValueNull Then
                    LabelCTREmailBCCNew.Text = oRowCTRApprovalDetailEmailBCC.CTRParameterValue
                End If

                If Not oRowCTRApprovalDetailEmailBCC.IsCTRParameterValue_OldNull Then
                    LabelCTREmailBCCOld.Text = oRowCTRApprovalDetailEmailBCC.CTRParameterValue_Old
                End If
            End If
            If Not oRowCTRApprovalDetailEmailSubject Is Nothing Then
                If Not oRowCTRApprovalDetailEmailSubject.IsCTRParameterValueNull Then
                    LabelCTREmailSubjectNew.Text = oRowCTRApprovalDetailEmailSubject.CTRParameterValue
                End If

                If Not oRowCTRApprovalDetailEmailSubject.IsCTRParameterValue_OldNull Then
                    LabelCTREmailSubjectOld.Text = oRowCTRApprovalDetailEmailSubject.CTRParameterValue_Old
                End If
            End If

            If Not oRowCTRApprovalDetailEmailBody Is Nothing Then
                If Not oRowCTRApprovalDetailEmailBody.IsCTRParameterValueNull Then
                    LabelCTREmailBodyNew.Text = oRowCTRApprovalDetailEmailBody.CTRParameterValue
                End If

                If Not oRowCTRApprovalDetailEmailBody.IsCTRParameterValue_OldNull Then
                    LabelCTREmailBodyOld.Text = oRowCTRApprovalDetailEmailBody.CTRParameterValue_Old
                End If
            End If

            If Not oRowCTRApprovalDetailPIC Is Nothing Then
                If Not oRowCTRApprovalDetailPIC.IsCTRParameterValueNull Then
                    LabelCTRPICNew.Text = oRowCTRApprovalDetailPIC.CTRParameterValue
                End If

                If Not oRowCTRApprovalDetailPIC.IsCTRParameterValue_OldNull Then
                    LabelCTRPICOld.Text = oRowCTRApprovalDetailPIC.CTRParameterValue_Old
                End If
            End If
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            If Not IsPostBack Then
                LabelTitle.Text = "Activity : Edit CTR Parameter"
                LoadData()
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
End Class
