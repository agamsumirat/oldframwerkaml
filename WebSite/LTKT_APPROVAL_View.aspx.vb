#Region "Imports..."
Imports System.IO
Imports System.Data
Imports Sahassanettier.Data
Imports Sahassanettier.Entities
Imports Sahassanettier.Services
Imports System.Collections.Generic
Imports AMLBLL.ValidateBLL
Imports System
Imports Sahassa.AML.Commonly
#End Region
Partial Class LTKT_APPROVAL_View
    Inherits Parent

#Region "Member"
    Private ReadOnly BindGridFromExcel As Boolean
#End Region

#Region "Property"

    Public ReadOnly Property SetnGetUserID() As String
        Get
            'Return SessionNIK
            Return SessionUserId
        End Get


    End Property
    Public Property SetRequestedDate() As String
        Get
            If Not Session("LTKT_APPROVAL.SetRequestedDate") Is Nothing Then
                Return CStr(Session("LTKT_APPROVAL.SetRequestedDate"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("LTKT_APPROVAL.SetRequestedDate") = value
        End Set
    End Property
    Public Property SetRequestedDate2() As String
        Get
            If Not Session("LTKT_APPROVAL.SetRequestedDate2") Is Nothing Then
                Return CStr(Session("LTKT_APPROVAL.SetRequestedDate2"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("LTKT_APPROVAL.SetRequestedDate2") = value
        End Set
    End Property
    Public Property SetMsUser_StaffName() As String
        Get
            If Not Session("LTKT_APPROVAL.SetMsUser_StaffName") Is Nothing Then
                Return CStr(Session("LTKT_APPROVAL.SetMsUser_StaffName"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("LTKT_APPROVAL.SetMsUser_StaffName") = value
        End Set
    End Property


    Private Property SetnGetSort() As String
        Get
            Return CType(IIf(Session("LTKT_APPROVAL.Sort") Is Nothing, "Pk_LTKT_APPROVAL_Id  asc", Session("LTKT_APPROVAL.Sort")), String)
        End Get
        Set(ByVal Value As String)
            Session("LTKT_APPROVAL.Sort") = Value
        End Set
    End Property
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return CType(IIf(Session("LTKT_APPROVAL.SelectedItem") Is Nothing, New ArrayList, Session("LTKT_APPROVAL.SelectedItem")), ArrayList)
        End Get
        Set(ByVal value As ArrayList)
            Session("LTKT_APPROVAL.SelectedItem") = value
        End Set
    End Property
    Private Property SetnGetRowTotal() As Int32
        Get
            Return CType(IIf(Session("LTKT_APPROVAL.RowTotal") Is Nothing, 0, Session("LTKT_APPROVAL.RowTotal")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("LTKT_APPROVAL.RowTotal") = Value
        End Set
    End Property
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return iTotalPages(SetnGetRowTotal)
        End Get
    End Property
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return CType(IIf(Session("LTKT_APPROVAL.CurrentPage") Is Nothing, 0, Session("LTKT_APPROVAL.CurrentPage")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("LTKT_APPROVAL.CurrentPage") = Value
        End Set
    End Property
    Public ReadOnly Property SetnGetBindTable() As VList(Of VW_LTKTAPPROVALVIEW)
        Get
            Dim strWhereClause(-1) As String
            Dim strAllWhereClause As String = ""
            If SetnGetCurrentPage > GetPageTotal - 1 And GetPageTotal - 1 > 0 Then
                SetnGetCurrentPage = GetPageTotal - 1
            End If

            If SetRequestedDate.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = vw_LTKTApprovalViewColumn.RequestedDate.ToString & " between '" & CToDate2(SetRequestedDate.Trim).ToString("yyyy-MM-dd") & " 00:00:00' and '" & CToDate2(SetRequestedDate2.Trim).ToString("yyyy-MM-dd") & " 23:59:00'"
            End If

            If SetMsUser_StaffName.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = vw_LTKTApprovalViewColumn.UserName.ToString & " like '%" & SetMsUser_StaffName.Trim.Replace("'", "''") & "%'"
            End If


            'TODO lepas lagi kalo sudah projeknya disatukan


            strAllWhereClause = String.Join(" and ", strWhereClause)

            Return DataRepository.vw_LTKTApprovalViewProvider.GetPaged(strAllWhereClause, SetnGetSort, SetnGetCurrentPage, GetDisplayedTotalRow, SetnGetRowTotal)


        End Get

    End Property


#End Region

#Region "Function"

    Private Sub BindSelected()
        Dim Rows As New ArrayList

        For Each IdDIN As Int64 In SetnGetSelectedItem
            Using rowData As VList(Of vw_LTKTApprovalView) = DataRepository.vw_LTKTApprovalViewProvider.GetPaged("Pk_LTKT_APPROVAL_Id = " & IdDIN & "", "", 0, Integer.MaxValue, 0)
                If rowData.Count > 0 Then
                    Rows.Add(rowData(0))
                End If
            End Using
        Next

        GridMsUserView.DataSource = Rows
        GridMsUserView.AllowPaging = False
        GridMsUserView.DataBind()
        For i As Integer = 0 To GridMsUserView.Items.Count - 1
            For y As Integer = 0 To GridMsUserView.Columns.Count - 1
                GridMsUserView.Items(i).Cells(y).Attributes.Add("class", "text")
            Next
        Next

        HideButtonGrid()

    End Sub
    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In GridMsUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = CType(gridRow.FindControl("CheckBoxExporttoExcel"), CheckBox)
                Dim PKMsUserId As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = SetnGetSelectedItem
                If ArrTarget.Contains(PKMsUserId) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(PKMsUserId)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(PKMsUserId)
                End If
                SetnGetSelectedItem = ArrTarget
            End If
        Next

    End Sub
    Private Sub SettingControlSearching()
        txtRequestedDate.Text = SetRequestedDate
        txtMsUser_StaffName.Text = SetMsUser_StaffName


    End Sub
    Private Sub SettingPropertySearching()
        SetRequestedDate = txtRequestedDate.Text.Trim
        SetMsUser_StaffName = txtMsUser_StaffName.Text.Trim
        SetRequestedDate2 = txtRequestedDate2.Text.Trim
    End Sub

    Private Sub ClearThisPageSessions()
        Clearproperty()
        LblMessage.Visible = False
        LblMessage.Text = ""
        SetnGetRowTotal = Nothing
        SetnGetSort = Nothing
        SetnGetCurrentPage = 0
    End Sub
    Private Sub Clearproperty()
        SetRequestedDate = Nothing
        SetMsUser_StaffName = Nothing
        SetRequestedDate2 = Nothing
    End Sub
    Private Sub popDateControl()
        '================================================= poppup for  Requested Date =========================================================================
        PopRequestedDate.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & txtRequestedDate.ClientID & "'), 'dd-mm-yyyy')")
        PopRequestedDate2.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & txtRequestedDate2.ClientID & "'), 'dd-mm-yyyy')")
    End Sub
    Private Sub BindComboBox()

    End Sub

    Private Sub bindgrid()
        SettingControlSearching()
        GridMsUserView.DataSource = SetnGetBindTable
        GridMsUserView.CurrentPageIndex = SetnGetCurrentPage
        GridMsUserView.VirtualItemCount = SetnGetRowTotal
        GridMsUserView.DataBind()
        'SettingConfigConfiguration()
        If SetnGetRowTotal > 0 Then
            LabelNoRecordFound.Visible = False
        Else
            LabelNoRecordFound.Visible = True
        End If
    End Sub
    'Private Sub SettingConfigConfiguration()
    '	Dim ObjMappingModuleActionList As TList(Of MappingMsShsModuleMsShsAction) = Nothing
    '	Dim bAllowAdd As Boolean
    '	Dim bAllowEdit As Boolean = False
    '	Dim bAllowDelete As Boolean = False
    '	Dim bAlllowDeleteOperation As Boolean = False
    '	Try
    '		Using ObjSecurityConfigurationBll As New SahassaBLL.SecurityConfigurationBll
    '			ObjMappingModuleActionList = ObjSecurityConfigurationBll.LoadConfiguration(CInt(SahassaCommonly.Commonly.SessionGroupMenuId), MsShsModuleList.FORMTKT_APPROVAL)
    '			For Each ObjMappingModuleAction As MappingMsShsModuleMsShsAction In ObjMappingModuleActionList
    '				If ObjMappingModuleAction.FK_ShsModule_id.GetValueOrDefault(0) = MsShsModuleList.FORMTKT_APPROVAL Then
    '					If ObjMappingModuleAction.FK_ShsAction_id.GetValueOrDefault(0) = MsShsActionList.Add Then
    '						bAllowAdd = True
    '						Exit For
    '					End If
    '				End If
    '			Next
    '			For Each ObjMappingModuleAction As MappingMsShsModuleMsShsAction In ObjMappingModuleActionList
    '				If ObjMappingModuleAction.FK_ShsModule_id.GetValueOrDefault(0) = MsShsModuleList.FORMTKT_APPROVAL Then
    '					If ObjMappingModuleAction.FK_ShsAction_id.GetValueOrDefault(0) = MsShsActionList.Edit Then
    '						bAllowEdit = True
    '						Exit For
    '					End If
    '				End If
    '			Next
    '			For Each ObjMappingModuleAction As MappingMsShsModuleMsShsAction In ObjMappingModuleActionList
    '				If ObjMappingModuleAction.FK_ShsModule_id.GetValueOrDefault(0) = MsShsModuleList.FORMTKT_APPROVAL Then
    '					If ObjMappingModuleAction.FK_ShsAction_id.GetValueOrDefault(0) = MsShsActionList.Delete Then
    '						bAllowDelete = True
    '						Exit For
    '					End If
    '				End If
    '			Next
    '			For Each ObjMappingModuleAction As MappingMsShsModuleMsShsAction In ObjMappingModuleActionList
    '				If ObjMappingModuleAction.FK_ShsModule_id.GetValueOrDefault(0) = MsShsModuleList.FORMTKT_APPROVAL Then
    '					If ObjMappingModuleAction.FK_ShsAction_id.GetValueOrDefault(0) = MsShsActionList.DeleteOperation Then
    '						bAlllowDeleteOperation = True
    '						Exit For
    '					End If
    '				End If
    '			Next

    '			If bAllowAdd Then
    '				LinkButtonAddNew.Visible = True
    '			Else
    '				LinkButtonAddNew.Visible = False
    '			End If

    '			GridMsUserView.Columns(GridMsUserView.Columns.Count - 3).Visible = bAllowEdit
    '			GridMsUserView.Columns(GridMsUserView.Columns.Count - 2).Visible = bAlllowDeleteOperation
    '			GridMsUserView.Columns(GridMsUserView.Columns.Count - 1).Visible = bAllowDelete

    '		End Using
    '	Catch ex As Exception
    '		LogError(ex)
    '		CvalPageErr.IsValid = False
    '		CvalPageErr.ErrorMessage = ex.Message
    '	End Try

    'End Sub
#End Region

#Region "events..."

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            popDateControl()
            BindComboBox()
            LblMessage.Text = ""
            LblMessage.Visible = False
            If Not Page.IsPostBack Then
                ClearThisPageSessions()
            End If

        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : SetnGetCurrentPage = 0
                Case "Prev" : SetnGetCurrentPage -= 1
                Case "Next" : SetnGetCurrentPage += 1
                Case "Last" : SetnGetCurrentPage = GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
            CollectSelected()

        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    Protected Sub ImgBtnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnSearch.Click

        Try
            SettingPropertySearching()

        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub
    Private Sub SetInfoNavigate()
        PageCurrentPage.Text = (SetnGetCurrentPage + 1).ToString
        PageTotalPages.Text = GetPageTotal.ToString
        PageTotalRows.Text = SetnGetRowTotal.ToString
        LinkButtonNext.Enabled = (Not SetnGetCurrentPage + 1 = GetPageTotal) AndAlso GetPageTotal <> 0
        LinkButtonLast.Enabled = (Not SetnGetCurrentPage + 1 = GetPageTotal) AndAlso GetPageTotal <> 0
        First.Enabled = Not SetnGetCurrentPage = 0
        LinkButtonPrevious.Enabled = Not SetnGetCurrentPage = 0
    End Sub
    Private Sub SetCheckedAll()
        Dim i As Int16 = 0
        Dim TotalRow As Int16 = 0
        For Each gridRow As DataGridItem In GridMsUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = CType(gridRow.FindControl("CheckBoxExporttoExcel"), CheckBox)
                If chkBox.Checked Then
                    i = CType(i + 1, Int16)
                End If
                TotalRow = CType(TotalRow + 1, Int16)
            End If
        Next
        If TotalRow = 0 Then
            'If i = totalrow Then
            CheckBoxSelectAll.Checked = False
            'End If
        Else
            If i = TotalRow Then
                CheckBoxSelectAll.Checked = True
            Else
                CheckBoxSelectAll.Checked = False
            End If
        End If
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
        
            bindgrid()
            SetInfoNavigate()
            SetCheckedAll()

        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImgBtnClearSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnClearSearch.Click
        Try
            Clearproperty()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        'For Each gridRow As DataGridItem In GridMsUserView.Items
        '    If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
        '        Dim pkid As String = gridRow.Cells(1).Text
        '        Dim ArrTarget As ArrayList = SetnGetSelectedItem
        '        If SetnGetSelectedItem.Contains(pkid) Then
        '            If CheckBoxSelectAll.Checked Then
        '                If Not ArrTarget.Contains(pkid) Then
        '                    ArrTarget.Add(pkid)
        '                End If
        '            Else
        '                ArrTarget.Remove(pkid)
        '            End If
        '        Else
        '            If CheckBoxSelectAll.Checked Then
        '                If Not ArrTarget.Contains(pkid) Then
        '                    ArrTarget.Add(pkid)
        '                End If
        '            End If
        '        End If
        '        SetnGetSelectedItem = ArrTarget
        '    End If
        'Next

        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        For Each gridRow As DataGridItem In Me.GridMsUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(1).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItem.Contains(StrKey) = False Then
                        ArrTarget.Add(StrKey)
                    End If
                Else
                    If Me.SetnGetSelectedItem.Contains(StrKey) Then
                        ArrTarget.Remove(StrKey)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItem = ArrTarget

    End Sub

    Protected Sub GridMsUserView_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridMsUserView.EditCommand
        Dim PKMsUserApprovalid As Integer
        Try
            PKMsUserApprovalid = CInt(e.Item.Cells(1).Text)
            Response.Redirect("UserApprovalDetail.aspx?PKMsUserApprovalID=" & PKMsUserApprovalid, False)
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub GridMsUserView_ItemCommand(source As Object, e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridMsUserView.ItemCommand
        If e.CommandName = "detail" Then
            Response.Redirect("LTKT_APPROVALDETAIL_View.aspx?ID=" & e.CommandArgument)
        End If
    End Sub

    Protected Sub GridMsUserView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridMsUserView.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Dim chkBox As CheckBox = CType(e.Item.FindControl("CheckBoxExporttoExcel"), CheckBox)
                chkBox.Checked = SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)
                If BindGridFromExcel = True Then
                    e.Item.Cells(2).Text = CStr((e.Item.ItemIndex + 1))
                Else
                    e.Item.Cells(2).Text = CStr((e.Item.ItemIndex + 1) + (SetnGetCurrentPage * GetDisplayedTotalRow))
                End If

                If e.Item.Cells(GridMsUserView.Columns.Count - 5).Text = "C" Then
                    e.Item.Cells(GridMsUserView.Columns.Count - 2).Enabled = False
                End If

                Dim lnkDetail As LinkButton = CType(e.Item.FindControl("lnkDetail"), LinkButton)
                lnkDetail.CommandName = "detail"

                Dim objVw As vw_LTKTApprovalView = CType(e.Item.DataItem, vw_LTKTApprovalView)
                lnkDetail.CommandArgument = objVw.PK_LTKT_Approval_Id
            End If
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub
    Protected Sub GridMsUserView_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridMsUserView.SortCommand
        Dim GridUser As DataGrid = CType(source, DataGrid)
        Try
            SetnGetSort = ChangeSortCommand(e.SortExpression)
            GridUser.Columns(IndexSort(GridUser, e.SortExpression)).SortExpression = SetnGetSort

        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub HideButtonGrid()
        With GridMsUserView
            .Columns(.Columns.Count - 4).Visible = False
            .Columns(.Columns.Count - 3).Visible = False
            .Columns(.Columns.Count - 2).Visible = False
            .Columns(.Columns.Count - 1).Visible = False
        End With
    End Sub
    Private Sub BindSelectedAll()
        Try
            GridMsUserView.DataSource = DataRepository.vw_LTKTApprovalViewProvider.GetAll
            GridMsUserView.AllowPaging = False
            GridMsUserView.DataBind()

            For i As Integer = 0 To GridMsUserView.Items.Count - 1
                For y As Integer = 0 To GridMsUserView.Columns.Count - 1
                    GridMsUserView.Items(i).Cells(y).Attributes.Add("class", "text")
                Next
            Next
            HideButtonGrid()
        Catch
            Throw
        End Try


    End Sub
    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                    'Skip
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub
    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click

        Try
            CollectSelected()
            Const strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            BindSelected()
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=UserApproval.xls")
            Response.Charset = ""
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            EnableViewState = False
            Using stringWrite As System.IO.StringWriter = New System.IO.StringWriter()
                Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
                ClearControls(GridMsUserView)
                GridMsUserView.RenderControl(htmlWrite)
                Response.Write(strStyle)
                Response.Write(stringWrite.ToString())
            End Using
            Response.End()

        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            Me.CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) AndAlso (CInt(Me.TextGoToPage.Text) > 0) Then
                If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                    Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                Else
                    Throw New Exception("EXECPTION_PAGENUMBER_GREATER_TOTALPAGENUMBER")
                End If
            Else
                Throw New Exception("EXECPTION_PAGENUMBER_NUMERICPOSITIF")
            End If
            CollectSelected()

        Catch ex As Exception
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    Protected Sub CvalHandleErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles CvalHandleErr.PreRender
        If CvalHandleErr.IsValid = False Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
        End If
    End Sub


    Protected Sub CvalPageErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles CvalPageErr.PreRender
        If CvalPageErr.IsValid = False Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
        End If
    End Sub

    Protected Sub lnkExportAllData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExportAllData.Click

        Try

            Const strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            BindSelectedAll()
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=UserApprovalAll.xls")
            Response.Charset = ""
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            EnableViewState = False
            Using stringWrite As System.IO.StringWriter = New System.IO.StringWriter()
                Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
                ClearControls(GridMsUserView)
                GridMsUserView.RenderControl(htmlWrite)
                Response.Write(strStyle)
                Response.Write(stringWrite.ToString())
            End Using
            Response.End()


        Catch ex As Exception
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub


#End Region


End Class

