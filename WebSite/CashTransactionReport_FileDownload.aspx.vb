
Partial Class CashTransactionReport_FileDownload
    Inherits System.Web.UI.Page

    'Protected Sub page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    '    Response.Clear()
    '    Dim filename As New String("")
    '    Dim counter = 0
    '    Using AccessToNextFileNumber As New AMLDAL.CTRReportTableAdapters.pNextFileNameGetterTableAdapter
    '        Using DataTableNextFileNumber As AMLDAL.CTRReport.pNextFileNameGetterDataTable = AccessToNextFileNumber.GetNextFileCounter
    '            counter = Convert.ToInt16(DataTableNextFileNumber.Rows(0).Item(0).ToString.Trim)
    '            filename = "C" & Now.Year.ToString.Substring(2, 2) & Now.Month.ToString & Now.Day.ToString & DataTableNextFileNumber.Rows(0).Item(0).ToString & ".txt"
    '        End Using
    '    End Using

    '    Response.AddHeader("content-disposition", "attachment;filename=" & filename)
    '    Response.ContentType = "text/plain"
    '    Dim sw_write As System.IO.TextWriter
    '    sw_write = Response.Output
    '    sw_write.WriteLine("A01:HR")
    '    sw_write.WriteLine(":A02:022")
    '    sw_write.WriteLine(String.Format(":A03:{0}", filename))
    '    sw_write.WriteLine(String.Format("=B01:{0}", "CE"))
    '    sw_write.WriteLine(String.Format(":B02:{0}", "N")) 'New Report
    '    sw_write.WriteLine(String.Format(":B03:{0}", ""))
    '    'Reporting Branch Name
    '    Using AccessToBranchName As New AMLDAL.CTRReportTableAdapters.pBranchNameGetterTableAdapter
    '        Using DataTableGetBranchName As AMLDAL.CTRReport.pBranchNameGetterDataTable = AccessToBranchName.GetBranchNameData(Request.QueryString("key"))
    '            If DataTableGetBranchName.Rows.Count > 0 Then
    '                sw_write.WriteLine(String.Format("=M01:{0}", DataTableGetBranchName.Rows(0).Item(0).ToString.Trim))
    '                'Reporting Branch Address
    '                sw_write.WriteLine(String.Format(":M02:{0}", DataTableGetBranchName.Rows(0).Item(1).ToString.Trim))
    '            Else
    '                sw_write.WriteLine("=M01:")
    '                sw_write.WriteLine(":M02:")
    '            End If
    '        End Using
    '    End Using

    '    'Reporting Officer Name
    '    Using AccessToReportingOfficer As New AMLDAL.CTRReportTableAdapters.pReportingOfficerGetterTableAdapter
    '        Using DataTableGetReportingOfficer As AMLDAL.CTRReport.pReportingOfficerGetterDataTable = AccessToReportingOfficer.GetReportingOfficerName(Request.QueryString("key"))
    '            If DataTableGetReportingOfficer.Rows.Count > 0 Then
    '                sw_write.WriteLine(String.Format(":M03:{0}", DataTableGetReportingOfficer.Rows(0).Item(0).ToString.Trim))
    '            Else
    '                sw_write.WriteLine(":M03:")
    '            End If
    '        End Using
    '    End Using

    '    'M04: TransactionDate
    '    Using AccessToTransactionData As New AMLDAL.CTRReportTableAdapters.pTransactionTypeGetterTableAdapter
    '        Using DataTableGetTransactionData As AMLDAL.CTRReport.pTransactionTypeGetterDataTable = AccessToTransactionData.GetTransactionData(Request.QueryString("key"))
    '            If DataTableGetTransactionData.Rows.Count > 0 Then
    '                If DataTableGetTransactionData.Rows(0).Item(0).ToString.Length > 0 Then
    '                    sw_write.WriteLine(String.Format(":M04:{0}", DataTableGetTransactionData.Rows(0).Item(0).ToString.Trim.Substring(0, 10)))
    '                Else
    '                    sw_write.WriteLine(":M04:")
    '                End If
    '                If DataTableGetTransactionData.Rows(0).Item(1).ToString.Length > 0 Then
    '                    sw_write.WriteLine(String.Format(":M04:{0}", DataTableGetTransactionData.Rows(0).Item(1).ToString.Trim.Substring(0, 10)))
    '                End If
    '                If DataTableGetTransactionData.Rows(0).Item(2).ToString.Length > 0 Then
    '                    sw_write.WriteLine(String.Format(":M04:{0}", DataTableGetTransactionData.Rows(0).Item(2).ToString.Trim.Substring(0, 10)))
    '                End If

    '                'Transaction Type Code : DP=Deposit, WD=Withdrawal
    '                If DataTableGetTransactionData.Rows(0).Item(3).ToString.Trim = "D" Then
    '                    sw_write.WriteLine(":M05:DP")
    '                    sw_write.WriteLine(":M06:Deposit")
    '                Else
    '                    sw_write.WriteLine(":M05:WD")
    '                    sw_write.WriteLine(":M06:Withdrawal")
    '                End If

    '                sw_write.WriteLine(String.Format(":M07:{0}", DataTableGetTransactionData.Rows(0).Item(4).ToString.Trim)) 'Cash Transaction Amount
    '                sw_write.WriteLine(String.Format("=N01:{0}", DataTableGetTransactionData.Rows(0).Item(5).ToString.Trim)) 'Foreign Currency Code
    '                sw_write.WriteLine(String.Format(":N02:{0}", DataTableGetTransactionData.Rows(0).Item(6).ToString.Trim)) 'Foreign Currency Amount
    '                sw_write.WriteLine(String.Format(":N03:{0}", DataTableGetTransactionData.Rows(0).Item(7).ToString.Trim)) 'Foreign Currency Rate
    '            Else
    '                sw_write.WriteLine(":M04:")
    '                sw_write.WriteLine(":M05:")
    '                sw_write.WriteLine(":M06:")
    '                sw_write.WriteLine(":M07:")
    '                sw_write.WriteLine("=N01:")
    '                sw_write.WriteLine(":N02:")
    '                sw_write.WriteLine(":N03:")
    '            End If
    '        End Using
    '    End Using
    '    'TODO: Cross Check Party Role Code apakah P=Person at the counter atau Q=Walk In Customer
    '    sw_write.WriteLine("=P01:P")

    '    'Name of Party
    '    Using AccessToPartyName As New AMLDAL.CTRReportTableAdapters.pAccountNameGetterTableAdapter
    '        Using DataTablePartyName As AMLDAL.CTRReport.pAccountNameGetterDataTable = AccessToPartyName.GetAccountName(Request.QueryString("key"))
    '            If DataTablePartyName.Rows.Count > 0 Then
    '                sw_write.WriteLine(String.Format(":P02:{0}", DataTablePartyName.Rows(0).Item(0)))
    '            Else
    '                sw_write.WriteLine(":P02:")
    '            End If
    '        End Using
    '    End Using

    '    'NPWP
    '    Using AccessToNPWP As New AMLDAL.CTRReportTableAdapters.pNPWPGetterTableAdapter
    '        Using DataTableNPWP As AMLDAL.CTRReport.pNPWPGetterDataTable = AccessToNPWP.GetNPWP(Request.QueryString("key"))
    '            If DataTableNPWP.Rows.Count > 0 Then
    '                sw_write.WriteLine(String.Format(":P03:{0}", DataTableNPWP.Rows(0).Item(0)))
    '            Else
    '                sw_write.WriteLine(":P03:")
    '            End If
    '        End Using
    '    End Using

    '    'Party Address
    '    Using AccessToAddress As New AMLDAL.CTRReportTableAdapters.pAddressGetterTableAdapter
    '        Using DataTableAddress As AMLDAL.CTRReport.pAddressGetterDataTable = AccessToAddress.GetAddress(Request.QueryString("key"))
    '            If DataTableAddress.Rows.Count > 0 Then
    '                sw_write.WriteLine(String.Format(":P04:{0}" & vbNewLine & "{1}" & "{2}" & "{3}", DataTableAddress.Rows(0).Item(0).ToString.Trim, DataTableAddress.Rows(0).Item(1).ToString.Trim, DataTableAddress.Rows(0).Item(2).ToString.Trim, DataTableAddress.Rows(0).Item(3).ToString.Trim))
    '            Else
    '                sw_write.WriteLine(":P04:")
    '            End If
    '        End Using
    '    End Using

    '    sw_write.WriteLine(":P05:") 'City
    '    sw_write.WriteLine(":P06:") 'Province

    '    'Business Description
    '    Using AccessToBusinessDescription As New AMLDAL.CTRReportTableAdapters.pBusinessGetterTableAdapter
    '        Using DataTableBusinessDescription As AMLDAL.CTRReport.pBusinessGetterDataTable = AccessToBusinessDescription.GetBusiness(Request.QueryString("key"))
    '            If DataTableBusinessDescription.Rows.Count > 0 Then
    '                sw_write.WriteLine(String.Format(":P07:{0}", DataTableBusinessDescription.Rows(0).Item(1).ToString.Trim))
    '            Else
    '                sw_write.WriteLine(":P07:")
    '            End If
    '        End Using
    '    End Using

    '    'DOB
    '    Using AccessToDOB As New AMLDAL.CTRReportTableAdapters.pDOBGetterTableAdapter
    '        Using DataTableDOB As AMLDAL.CTRReport.pDOBGetterDataTable = AccessToDOB.GetDOB(Request.QueryString("key"))
    '            If DataTableDOB.Rows.Count > 0 Then
    '                sw_write.WriteLine(String.Format(":P08:{0}", DataTableDOB.Rows(0).Item(0).ToString.Trim.Substring(0, 10)))
    '            Else
    '                sw_write.WriteLine(":P08:")
    '            End If
    '        End Using
    '    End Using

    '    'Note: if P01=P Then AccountNumber exists
    '    'Account Number
    '    Using AccessToAccountNumber As New AMLDAL.CTRReportTableAdapters.pAccountNumberGetterTableAdapter
    '        Using DataTableAccountNumber As AMLDAL.CTRReport.pAccountNumberGetterDataTable = AccessToAccountNumber.GetAccountNumber(Request.QueryString("key"))
    '            If DataTableAccountNumber.Rows.Count > 0 Then
    '                sw_write.WriteLine(String.Format(":P09:{0}", DataTableAccountNumber.Rows(0).Item(0).ToString.Trim))
    '            Else
    '                sw_write.WriteLine(":P09:")
    '            End If
    '        End Using
    '    End Using

    '    'Account Type
    '    'Code:B=Credit card Account, C=Cheque Account, L=Loan Account, M=Multiple Account Table, O=Other Account, S=Savings Account, T=Term Deposit Account
    '    Using AccessToAccountType As New AMLDAL.CTRReportTableAdapters.pAccountTypeGetterTableAdapter
    '        Using DataTableAccountType As AMLDAL.CTRReport.pAccountTypeGetterDataTable = AccessToAccountType.GetAccountType(Request.QueryString("key"))
    '            If DataTableAccountType.Rows.Count > 0 Then
    '                If DataTableAccountType.Rows(0).Item(0).ToString.Trim = "D" Then
    '                    sw_write.WriteLine(":P10:C")
    '                    sw_write.WriteLine(":P11:C") 'Account Title
    '                    sw_write.WriteLine(":P12:Giro-Cheque Account") 'Account Description
    '                ElseIf DataTableAccountType.Rows(0).Item(0).ToString.Trim = "L" Then
    '                    sw_write.WriteLine(":P10:L")
    '                    sw_write.WriteLine(":P11:L") 'Account Title
    '                    sw_write.WriteLine(":P12:Loan Account") 'Account Description
    '                ElseIf DataTableAccountType.Rows(0).Item(0).ToString.Trim = "S" Then
    '                    sw_write.WriteLine(":P10:S")
    '                    sw_write.WriteLine(":P11:S") 'Account Title
    '                    sw_write.WriteLine(":P12:Savings Account") 'Account Description
    '                ElseIf DataTableAccountType.Rows(0).Item(0).ToString.Trim = "T" Then
    '                    sw_write.WriteLine(":P10:T")
    '                    sw_write.WriteLine(":P11:T") 'Account Title
    '                    sw_write.WriteLine(":P12:Term Deposit Account") 'Account Description
    '                ElseIf DataTableAccountType.Rows(0).Item(0).ToString.Trim = "Z" Then
    '                    sw_write.WriteLine(":P10:B")
    '                    sw_write.WriteLine(":P11:B") 'Account Title
    '                    sw_write.WriteLine(":P12:Credit Card Account") 'Account Description
    '                Else
    '                    sw_write.WriteLine(":P10:O")
    '                    sw_write.WriteLine(":P11:O") 'Account Title
    '                    sw_write.WriteLine(":P12:Other Account") 'Account Description
    '                End If
    '            Else
    '                sw_write.WriteLine(":P10:")
    '                sw_write.WriteLine(":P11:")
    '                sw_write.WriteLine(":P12:")
    '            End If
    '        End Using
    '    End Using

    '    'Request.QueryString("key") Type Code: K=KTP, P=Passport, S=SIM, M=KIMS/KITAS/KITAP, U=Kartu Pelajar, L=Lainnya
    '    Using AccessToIDType As New AMLDAL.CTRReportTableAdapters.pIdentityTypeGetterTableAdapter
    '        Using DataTableIDType As AMLDAL.CTRReport.pIdentityTypeGetterDataTable = AccessToIDType.GetIdentityType(Request.QueryString("key"))
    '            If DataTableIDType.Rows.Count > 0 Then
    '                If DataTableIDType.Rows(0).Item(0).ToString.Trim = "KT" Then
    '                    sw_write.WriteLine("=Q01:K") 'KTP
    '                ElseIf DataTableIDType.Rows(0).Item(0).ToString.Trim = "PP" Then
    '                    sw_write.WriteLine("=Q01:P") 'Passport
    '                ElseIf DataTableIDType.Rows(0).Item(0).ToString.Trim = "SM" Then
    '                    sw_write.WriteLine("=Q01:S") 'SIM
    '                ElseIf DataTableIDType.Rows(0).Item(0).ToString.Trim = "KI" Then
    '                    sw_write.WriteLine("=Q01:M") 'KIMS/KITAS/KITAP
    '                ElseIf DataTableIDType.Rows(0).Item(0).ToString.Trim = "KP" Then
    '                    sw_write.WriteLine("=Q01:U") 'Kartu Pelajar
    '                ElseIf DataTableIDType.Rows(0).Item(0).ToString.Trim = "NA" Then
    '                    sw_write.WriteLine("=Q01:") 'Not Available
    '                Else
    '                    sw_write.WriteLine("=Q01:L") 'Lainnya
    '                End If
    '            Else
    '                sw_write.WriteLine("=Q01:")
    '            End If
    '        End Using
    '    End Using

    '    'Request.QueryString("key") Number
    '    Using AccessToIDNumber As New AMLDAL.CTRReportTableAdapters.pIdentityNumberGetterTableAdapter
    '        Using DataTableIDNumber As AMLDAL.CTRReport.pIdentityNumberGetterDataTable = AccessToIDNumber.GetIdentityNumber(Request.QueryString("key"))
    '            If DataTableIDNumber.Rows.Count > 0 Then
    '                sw_write.WriteLine(String.Format(":Q02:{0}", DataTableIDNumber.Rows(0).Item(0).ToString.Trim))
    '            Else
    '                sw_write.WriteLine(":Q02:")
    '            End If
    '        End Using
    '    End Using

    '    sw_write.WriteLine("=U01:") 'Financial Institution Name
    '    sw_write.WriteLine(":U02:") 'Financial Institution Address
    '    sw_write.WriteLine(":U03:") 'Account Number
    '    sw_write.WriteLine(":U04:") 'Recipients Name
    '    sw_write.WriteLine("=V02:") 'Additional Cash Dealer Text
    '    sw_write.WriteLine("=Z01:TR")
    '    Using AccessToTransactionNumber As New AMLDAL.CTRReportTableAdapters.pTransactionCounterTableAdapter
    '        Using DataTableTransactionNumber As AMLDAL.CTRReport.pTransactionCounterDataTable = AccessToTransactionNumber.GetTransactionCounter(Request.QueryString("key"))
    '            sw_write.WriteLine(String.Format(":Z02:{0}", DataTableTransactionNumber.Rows(0).Item(0).ToString.Trim)) 'No. of CTR Reports in this File
    '        End Using
    '    End Using

    '    sw_write.WriteLine("=")

    '    Using AccessToSetFileNumber As New AMLDAL.CTRReportTableAdapters.filename_SetterTableAdapter
    '        Using DataTableSetFileNumber As AMLDAL.CTRReport.filename_SetterDataTable = AccessToSetFileNumber.SetFileNameCounter
    '            'setting filename into database
    '        End Using
    '    End Using

    '    Response.End()
    'End Sub

    Protected Sub page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Clear()
        Dim filename As New String("")
        Dim counter = 0
        Using AccessToNextFileNumber As New AMLDAL.CTRReportTableAdapters.pNextFileNameGetterTableAdapter
            Using DataTableNextFileNumber As AMLDAL.CTRReport.pNextFileNameGetterDataTable = AccessToNextFileNumber.GetNextFileCounter
                counter = Convert.ToInt16(DataTableNextFileNumber.Rows(0).Item(0).ToString.Trim)
                filename = "C" & Now.Year.ToString.Substring(2, 2) & Now.Month.ToString & Now.Day.ToString & DataTableNextFileNumber.Rows(0).Item(0).ToString & ".txt"
            End Using
        End Using

        Response.AddHeader("content-disposition", "attachment;filename=" & filename)
        Response.ContentType = "text/plain"
        Dim sw_write As System.IO.TextWriter
        sw_write = Response.Output
        sw_write.WriteLine("A01:HR")
        sw_write.WriteLine(":A02:022")
        sw_write.WriteLine(String.Format(":A03:{0}", filename))
        sw_write.WriteLine(String.Format("=B01:{0}", "CE"))
        sw_write.WriteLine(String.Format(":B02:{0}", "N")) 'New Report
        sw_write.WriteLine(String.Format(":B03:{0}", ""))
        'Reporting Branch Name
        Using AccessToBranchName As New AMLDAL.CTRReportTableAdapters.pBranchNameGetterTableAdapter
            Using DataTableGetBranchName As AMLDAL.CTRReport.pBranchNameGetterDataTable = AccessToBranchName.GetBranchNameData(Request.QueryString("key"))
                If DataTableGetBranchName.Rows.Count > 0 Then
                    sw_write.WriteLine(String.Format("=M01:{0}", DataTableGetBranchName.Rows(0).Item(0).ToString.Trim))
                    'Reporting Branch Address
                    sw_write.WriteLine(String.Format(":M02:{0}", DataTableGetBranchName.Rows(0).Item(1).ToString.Trim))
                Else
                    sw_write.WriteLine("=M01:")
                    sw_write.WriteLine(":M02:")
                End If
            End Using
        End Using

        'Reporting Officer Name
        Using AccessToReportingOfficer As New AMLDAL.CTRReportTableAdapters.pReportingOfficerGetterTableAdapter
            Using DataTableGetReportingOfficer As AMLDAL.CTRReport.pReportingOfficerGetterDataTable = AccessToReportingOfficer.GetReportingOfficerName(Request.QueryString("key"))
                If DataTableGetReportingOfficer.Rows.Count > 0 Then
                    sw_write.WriteLine(String.Format(":M03:{0}", DataTableGetReportingOfficer.Rows(0).Item(0).ToString.Trim))
                Else
                    sw_write.WriteLine(":M03:")
                End If
            End Using
        End Using

        'M04: TransactionDate
        Using AccessToTransactionData As New AMLDAL.CTRReportTableAdapters.pTransactionTypeGetterTableAdapter
            Using DataTableGetTransactionData As AMLDAL.CTRReport.pTransactionTypeGetterDataTable = AccessToTransactionData.GetTransactionData(Request.QueryString("key"))
                If DataTableGetTransactionData.Rows.Count > 0 Then
                    If DataTableGetTransactionData.Rows(0).Item(0).ToString.Length > 0 Then
                        sw_write.WriteLine(String.Format(":M04:{0}", DataTableGetTransactionData.Rows(0).Item(0).ToString.Trim.Substring(0, 10)))
                    Else
                        sw_write.WriteLine(":M04:")
                    End If
                    If DataTableGetTransactionData.Rows(0).Item(1).ToString.Length > 0 Then
                        sw_write.WriteLine(String.Format(":M04:{0}", DataTableGetTransactionData.Rows(0).Item(1).ToString.Trim.Substring(0, 10)))
                    End If
                    If DataTableGetTransactionData.Rows(0).Item(2).ToString.Length > 0 Then
                        sw_write.WriteLine(String.Format(":M04:{0}", DataTableGetTransactionData.Rows(0).Item(2).ToString.Trim.Substring(0, 10)))
                    End If

                    'Transaction Type Code : DP=Deposit, WD=Withdrawal
                    If DataTableGetTransactionData.Rows(0).Item(3).ToString.Trim = "D" Then
                        sw_write.WriteLine(":M05:DP")
                        sw_write.WriteLine(":M06:Deposit")
                    Else
                        sw_write.WriteLine(":M05:WD")
                        sw_write.WriteLine(":M06:Withdrawal")
                    End If

                    sw_write.WriteLine(String.Format(":M07:{0}", DataTableGetTransactionData.Rows(0).Item(4).ToString.Trim)) 'Cash Transaction Amount
                    sw_write.WriteLine(String.Format("=N01:{0}", DataTableGetTransactionData.Rows(0).Item(5).ToString.Trim)) 'Foreign Currency Code
                    sw_write.WriteLine(String.Format(":N02:{0}", DataTableGetTransactionData.Rows(0).Item(6).ToString.Trim)) 'Foreign Currency Amount
                    sw_write.WriteLine(String.Format(":N03:{0}", DataTableGetTransactionData.Rows(0).Item(7).ToString.Trim)) 'Foreign Currency Rate
                Else
                    sw_write.WriteLine(":M04:")
                    sw_write.WriteLine(":M05:")
                    sw_write.WriteLine(":M06:")
                    sw_write.WriteLine(":M07:")
                    sw_write.WriteLine("=N01:")
                    sw_write.WriteLine(":N02:")
                    sw_write.WriteLine(":N03:")
                End If
            End Using
        End Using
        'TODO: Cross Check Party Role Code apakah P=Person at the counter atau Q=Walk In Customer
        sw_write.WriteLine("=P01:P")

        'Name of Party
        Using AccessToPartyName As New AMLDAL.CTRReportTableAdapters.pAccountNameGetterTableAdapter
            Using DataTablePartyName As AMLDAL.CTRReport.pAccountNameGetterDataTable = AccessToPartyName.GetAccountName(Request.QueryString("key"))
                If DataTablePartyName.Rows.Count > 0 Then
                    sw_write.WriteLine(String.Format(":P02:{0}", DataTablePartyName.Rows(0).Item(0)))
                Else
                    sw_write.WriteLine(":P02:")
                End If
            End Using
        End Using

        'NPWP
        Using AccessToNPWP As New AMLDAL.CTRReportTableAdapters.pNPWPGetterTableAdapter
            Using DataTableNPWP As AMLDAL.CTRReport.pNPWPGetterDataTable = AccessToNPWP.GetNPWP(Request.QueryString("key"))
                If DataTableNPWP.Rows.Count > 0 Then
                    sw_write.WriteLine(String.Format(":P03:{0}", DataTableNPWP.Rows(0).Item(0)))
                Else
                    sw_write.WriteLine(":P03:")
                End If
            End Using
        End Using

        'Party Address
        Using AccessToAddress As New AMLDAL.CTRReportTableAdapters.pAddressGetterTableAdapter
            Using DataTableAddress As AMLDAL.CTRReport.pAddressGetterDataTable = AccessToAddress.GetAddress(Request.QueryString("key"))
                If DataTableAddress.Rows.Count > 0 Then
                    sw_write.WriteLine(String.Format(":P04:{0}" & vbNewLine & "{1}" & "{2}" & "{3}", DataTableAddress.Rows(0).Item(0).ToString.Trim, DataTableAddress.Rows(0).Item(1).ToString.Trim, DataTableAddress.Rows(0).Item(2).ToString.Trim, DataTableAddress.Rows(0).Item(3).ToString.Trim))
                Else
                    sw_write.WriteLine(":P04:")
                End If
            End Using
        End Using

        sw_write.WriteLine(":P05:") 'City
        sw_write.WriteLine(":P06:") 'Province

        'Business Description
        Using AccessToBusinessDescription As New AMLDAL.CTRReportTableAdapters.pBusinessGetterTableAdapter
            Using DataTableBusinessDescription As AMLDAL.CTRReport.pBusinessGetterDataTable = AccessToBusinessDescription.GetBusiness(Request.QueryString("key"))
                If DataTableBusinessDescription.Rows.Count > 0 Then
                    sw_write.WriteLine(String.Format(":P07:{0}", DataTableBusinessDescription.Rows(0).Item(1).ToString.Trim))
                Else
                    sw_write.WriteLine(":P07:")
                End If
            End Using
        End Using

        'DOB
        Using AccessToDOB As New AMLDAL.CTRReportTableAdapters.pDOBGetterTableAdapter
            Using DataTableDOB As AMLDAL.CTRReport.pDOBGetterDataTable = AccessToDOB.GetDOB(Request.QueryString("key"))
                If DataTableDOB.Rows.Count > 0 Then
                    sw_write.WriteLine(String.Format(":P08:{0}", DataTableDOB.Rows(0).Item(0).ToString.Trim.Substring(0, 10)))
                Else
                    sw_write.WriteLine(":P08:")
                End If
            End Using
        End Using

        'Note: if P01=P Then AccountNumber exists
        'Account Number
        Using AccessToAccountNumber As New AMLDAL.CTRReportTableAdapters.pAccountNumberGetterTableAdapter
            Using DataTableAccountNumber As AMLDAL.CTRReport.pAccountNumberGetterDataTable = AccessToAccountNumber.GetAccountNumber(Request.QueryString("key"))
                If DataTableAccountNumber.Rows.Count > 0 Then
                    sw_write.WriteLine(String.Format(":P09:{0}", DataTableAccountNumber.Rows(0).Item(0).ToString.Trim))
                Else
                    sw_write.WriteLine(":P09:")
                End If
            End Using
        End Using

        'Account Type
        'Code:B=Credit card Account, C=Cheque Account, L=Loan Account, M=Multiple Account Table, O=Other Account, S=Savings Account, T=Term Deposit Account
        Using AccessToAccountType As New AMLDAL.CTRReportTableAdapters.pAccountTypeGetterTableAdapter
            Using DataTableAccountType As AMLDAL.CTRReport.pAccountTypeGetterDataTable = AccessToAccountType.GetAccountType(Request.QueryString("key"))
                If DataTableAccountType.Rows.Count > 0 Then
                    If DataTableAccountType.Rows(0).Item(0).ToString.Trim = "D" Then
                        sw_write.WriteLine(":P10:C")
                        sw_write.WriteLine(":P11:C") 'Account Title
                        sw_write.WriteLine(":P12:Giro-Cheque Account") 'Account Description
                    ElseIf DataTableAccountType.Rows(0).Item(0).ToString.Trim = "L" Then
                        sw_write.WriteLine(":P10:L")
                        sw_write.WriteLine(":P11:L") 'Account Title
                        sw_write.WriteLine(":P12:Loan Account") 'Account Description
                    ElseIf DataTableAccountType.Rows(0).Item(0).ToString.Trim = "S" Then
                        sw_write.WriteLine(":P10:S")
                        sw_write.WriteLine(":P11:S") 'Account Title
                        sw_write.WriteLine(":P12:Savings Account") 'Account Description
                    ElseIf DataTableAccountType.Rows(0).Item(0).ToString.Trim = "T" Then
                        sw_write.WriteLine(":P10:T")
                        sw_write.WriteLine(":P11:T") 'Account Title
                        sw_write.WriteLine(":P12:Term Deposit Account") 'Account Description
                    ElseIf DataTableAccountType.Rows(0).Item(0).ToString.Trim = "Z" Then
                        sw_write.WriteLine(":P10:B")
                        sw_write.WriteLine(":P11:B") 'Account Title
                        sw_write.WriteLine(":P12:Credit Card Account") 'Account Description
                    Else
                        sw_write.WriteLine(":P10:O")
                        sw_write.WriteLine(":P11:O") 'Account Title
                        sw_write.WriteLine(":P12:Other Account") 'Account Description
                    End If
                Else
                    sw_write.WriteLine(":P10:")
                    sw_write.WriteLine(":P11:")
                    sw_write.WriteLine(":P12:")
                End If
            End Using
        End Using

        'Request.QueryString("key") Type Code: K=KTP, P=Passport, S=SIM, M=KIMS/KITAS/KITAP, U=Kartu Pelajar, L=Lainnya
        Using AccessToIDType As New AMLDAL.CTRReportTableAdapters.pIdentityTypeGetterTableAdapter
            Using DataTableIDType As AMLDAL.CTRReport.pIdentityTypeGetterDataTable = AccessToIDType.GetIdentityType(Request.QueryString("key"))
                If DataTableIDType.Rows.Count > 0 Then
                    If DataTableIDType.Rows(0).Item(0).ToString.Trim = "KT" Then
                        sw_write.WriteLine("=Q01:K") 'KTP
                    ElseIf DataTableIDType.Rows(0).Item(0).ToString.Trim = "PP" Then
                        sw_write.WriteLine("=Q01:P") 'Passport
                    ElseIf DataTableIDType.Rows(0).Item(0).ToString.Trim = "SM" Then
                        sw_write.WriteLine("=Q01:S") 'SIM
                    ElseIf DataTableIDType.Rows(0).Item(0).ToString.Trim = "KI" Then
                        sw_write.WriteLine("=Q01:M") 'KIMS/KITAS/KITAP
                    ElseIf DataTableIDType.Rows(0).Item(0).ToString.Trim = "KP" Then
                        sw_write.WriteLine("=Q01:U") 'Kartu Pelajar
                    ElseIf DataTableIDType.Rows(0).Item(0).ToString.Trim = "NA" Then
                        sw_write.WriteLine("=Q01:") 'Not Available
                    Else
                        sw_write.WriteLine("=Q01:L") 'Lainnya
                    End If
                Else
                    sw_write.WriteLine("=Q01:")
                End If
            End Using
        End Using

        'Request.QueryString("key") Number
        Using AccessToIDNumber As New AMLDAL.CTRReportTableAdapters.pIdentityNumberGetterTableAdapter
            Using DataTableIDNumber As AMLDAL.CTRReport.pIdentityNumberGetterDataTable = AccessToIDNumber.GetIdentityNumber(Request.QueryString("key"))
                If DataTableIDNumber.Rows.Count > 0 Then
                    sw_write.WriteLine(String.Format(":Q02:{0}", DataTableIDNumber.Rows(0).Item(0).ToString.Trim))
                Else
                    sw_write.WriteLine(":Q02:")
                End If
            End Using
        End Using

        sw_write.WriteLine("=U01:") 'Financial Institution Name
        sw_write.WriteLine(":U02:") 'Financial Institution Address
        sw_write.WriteLine(":U03:") 'Account Number
        sw_write.WriteLine(":U04:") 'Recipients Name
        sw_write.WriteLine("=V02:") 'Additional Cash Dealer Text
        sw_write.WriteLine("=Z01:TR")
        Using AccessToTransactionNumber As New AMLDAL.CTRReportTableAdapters.pTransactionCounterTableAdapter
            Using DataTableTransactionNumber As AMLDAL.CTRReport.pTransactionCounterDataTable = AccessToTransactionNumber.GetTransactionCounter(Request.QueryString("key"))
                sw_write.WriteLine(String.Format(":Z02:{0}", DataTableTransactionNumber.Rows(0).Item(0).ToString.Trim)) 'No. of CTR Reports in this File
            End Using
        End Using

        sw_write.WriteLine("=")

        Using AccessToSetFileNumber As New AMLDAL.CTRReportTableAdapters.filename_SetterTableAdapter
            Using DataTableSetFileNumber As AMLDAL.CTRReport.filename_SetterDataTable = AccessToSetFileNumber.SetFileNameCounter
                'setting filename into database
            End Using
        End Using

        Response.End()
    End Sub

End Class

