<%@ Page Language="VB" AutoEventWireup="false" CodeFile="MsCurrency_UploadApproval_View.aspx.vb"
	Inherits="MsCurrency_UploadAPPROVAL_View" MasterPageFile="~/MasterPage.master" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
	<script src="script/popcalendar.js"></script>
	<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td>
				<img src="Images/blank.gif" width="5" height="1" />
			</td>
			<td>
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr>
						<td>
							&nbsp;
						</td>
						<td width="99%" bgcolor="#FFFFFF">
							<img src="Images/blank.gif" width="1" height="1" />
						</td>
						<td bgcolor="#FFFFFF">
							<img src="Images/blank.gif" width="1" height="1" />
						</td>
					</tr>
				</table>
			</td>
			<td>
				<img src="Images/blank.gif" width="5" height="1" />
			</td>
		</tr>
		<tr>
			<td>
			</td>
			<td id="tdcontent" height="99%" valign="top" bgcolor="#FFFFFF">
				
					<table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
						<tr>
							<td>
								<img src="Images/blank.gif" width="20" height="100%" />
							</td>
							<td class="divcontentinside" bgcolor="#FFFFFF">
								&nbsp;
								<ajax:AjaxPanel ID="AjaxPanel5" runat="server" Width="100%" meta:resourcekey="AjaxPanel5Resource1">
									<table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
										height="72" style="border-top-style: none; border-right-style: none; border-left-style: none;
										border-bottom-style: none" bgcolor="#dddddd" width="100%">
										<tr bgcolor="#ffffff">
											<td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
												border-right-style: none; border-left-style: none; border-bottom-style: none">
												<strong>
													<img src="Images/dot_title.gif" width="17" height="17">
													<asp:Label ID="Label7" runat="server" Text="Master Currency Upload Approval View"></asp:Label>
													<hr />
												</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<ajax:AjaxPanel ID="AjxMessage" runat="server"
													meta:resourcekey="AjxMessageResource1">
													<asp:Label ID="LblMessage" background="images/validationbground.gif" runat="server"
														CssClass="validationok" Width="100%" meta:resourcekey="LblMessageResource1"></asp:Label>
												</ajax:AjaxPanel>
											</td>
										</tr>
									</table>
									<asp:Panel ID="Panel1" runat="server" DefaultButton="ImgBtnSearch" Width="100%" meta:resourcekey="Panel1Resource1">
										<table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
											border="2">
											<tr>
												<td colspan="2" background="images/search-bar-background.gif" height="18" valign="middle"
													width="100%" align="right" style="text-align: left">
													<table cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td style="height: 16px">
																<strong>Search box</strong> &nbsp;
															</td>
															<td style="height: 16px">
																<a href="#" onclick="javascript:UpdateSearchBox()" title="click to minimize or maximize">
																	<img id="searchimage" src="images/search-bar-minimize.gif" border="0" style="width: 19px;
																		height: 14px"></a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr id="searchbox">
												<td colspan="2" valign="top" width="98%" bgcolor="#ffffff">
													<table cellpadding="0" width="100%" border="0">
														<tr>
															<td valign="middle" nowrap>
																<ajax:AjaxPanel ID="AjaxPanel1" runat="server" Width="885px" meta:resourcekey="AjaxPanel1Resource1">
																	<table style="height: 100%">
																		<tr>
																			<td colspan="3" style="height: 7px">
																			</td>
																		</tr>
																		<tr>
																			<td nowrap style="height: 26px; width: 107px;">
																				<asp:Label ID="LabelMsUser_StaffName" runat="server" Text="User"></asp:Label>
																			</td>
																			<td style="width: 19px;">
																				:
																			</td>
																			<td style="height: 26px; width: 765px;">
																				<asp:TextBox ID="txtMsUser_StaffName" runat="server" CssClass="searcheditbox" TabIndex="2"
																					Width="308px"></asp:TextBox>
																			</td>
																		</tr>
																		<tr>
																			<td nowrap style="height: 26px; width: 107px;">
																				<asp:Label ID="LabelRequestedDate" runat="server" Text="Requested Date"></asp:Label>
																			</td>
																			<td style="width: 19px;">
																				:
																			</td>
																			<td style="height: 26px; width: 765px;">
																				<asp:TextBox ID="txtRequestedDate1" runat="server" CssClass="textBox" MaxLength="1000"
																					TabIndex="2" Width="120px" ToolTip="RequestedDate"></asp:TextBox>
																				<input id="PopRequestedDate1" runat="server" style="border-right: #ffffff 0px solid;
																					border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
																					border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
																					height: 17px" title="Click to show calendar" type="button" />
																				&nbsp; s/d <span defaultbutton="ImgBtnSearch">
																					<asp:TextBox ID="txtRequestedDate2" runat="server" CssClass="textBox" MaxLength="1000"
																						TabIndex="2" ToolTip="RequestedDate" Width="120px"></asp:TextBox>
																					<input id="PopRequestedDate2" runat="server" style="border-right: #ffffff 0px solid;
																						border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
																						border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
																						height: 17px" title="Click to show calendar" type="button" />
																				</span>
																			</td>
																		</tr>
																		
																		<tr>
																			<td colspan="3">
																			</td>
																		</tr>
																		<tr>
																			<td colspan="3">
																				<asp:ImageButton ID="ImgBtnSearch" TabIndex="3" runat="server" 
																					ImageUrl="~/Images/button/search.gif" meta:resourcekey="ImgBtnSearchResource1">
																				</asp:ImageButton>
																				<asp:ImageButton ID="ImgBtnClearSearch" runat="server" ImageUrl="~/Images/button/clearsearch.gif"
																					meta:resourcekey="ImgBtnClearSearchResource1" />
																			</td>
																		</tr>
																	</table>
																	&nbsp;&nbsp;&nbsp;
																</ajax:AjaxPanel>
															</td>
														</tr>
													</table>
													<ajax:AjaxPanel ID="AjaxPanel3" runat="server" meta:resourcekey="AjaxPanel3Resource1">
														<asp:CustomValidator ID="CvalPageErr" runat="server" Display="None" meta:resourcekey="CvalPageErrResource1"></asp:CustomValidator></ajax:AjaxPanel>
													<ajax:AjaxPanel ID="Ajaxpanel2" runat="server" meta:resourcekey="Ajaxpanel2Resource1">
														<asp:CustomValidator ID="CvalHandleErr" runat="server" ValidationGroup="handle" Display="None"
															meta:resourcekey="CvalHandleErrResource1"></asp:CustomValidator></ajax:AjaxPanel>
												</td>
											</tr>
										</table>
									</asp:Panel>
									<table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
										border="2">
										<tr>
											<td bgcolor="#ffffff">
												<ajax:AjaxPanel ID="AjaxPanel4" runat="server" meta:resourcekey="AjaxPanel4Resource1">
                                                <asp:DataGrid ID="GV" runat="server" AllowCustomPaging="True" AutoGenerateColumns="False"
                                                    Font-Size="XX-Small" BackColor="White" CellPadding="4" BorderWidth="1px" BorderStyle="None"
                                                    AllowPaging="True" Width="100%" GridLines="Vertical" AllowSorting="True" BorderColor="#DEDFDE"
                                                    ForeColor="Black">
                                                    <FooterStyle BackColor="#CCCC99"></FooterStyle>
                                                    <SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#CE5D5A"></SelectedItemStyle>
                                                    <AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
                                                    <ItemStyle BackColor="#F7F7DE"></ItemStyle>
                                                    <HeaderStyle Font-Size="XX-Small" Font-Bold="True" ForeColor="White" BackColor="#6B696B">
                                                    </HeaderStyle>
                                                    <PagerStyle Visible="False" HorizontalAlign="Right" ForeColor="Black" BackColor="#F7F7DE"
                                                        Mode="NumericPages"></PagerStyle>
														<Columns>
															<asp:TemplateColumn>
																<ItemTemplate>
																	<asp:CheckBox ID="CheckBoxExporttoExcel" runat="server" meta:resourcekey="CheckBoxExporttoExcelResource1">
																	</asp:CheckBox>
																</ItemTemplate>
																<HeaderStyle Width="10px" />
															</asp:TemplateColumn>
															<asp:BoundColumn DataField="PK_MsCurrencyPPATK_Approval_Id" Visible="False"></asp:BoundColumn>
															<asp:BoundColumn HeaderText="No">
																<HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
																	Font-Underline="False" ForeColor="White" />
															</asp:BoundColumn>
															<asp:BoundColumn DataField="UserName" HeaderText="User" SortExpression="UserName  asc">
															</asp:BoundColumn>
															<asp:BoundColumn DataField="RequestedDate" HeaderText="Requested Date" SortExpression="RequestedDate desc"
																DataFormatString="{0:dd-MM-yyyy}"></asp:BoundColumn>
														
															<asp:HyperLinkColumn Text="Detail" DataNavigateUrlField="PK_MsCurrencyPPATK_Approval_Id"
																DataNavigateUrlFormatString="MsCurrency_UploadApprovalDetail_View.aspx?ID={0}">
															</asp:HyperLinkColumn>
														</Columns>
													</asp:DataGrid>
													<br />
													<br />
													<asp:Label ID="LabelNoRecordFound" runat="server" Text="No record match with your criteria"
														CssClass="text" Visible="False" meta:resourcekey="LabelNoRecordFoundResource1"></asp:Label></ajax:AjaxPanel>
											</td>
										</tr>
										<tr>
											<td style="background-color: #ffffff">
												<table cellpadding="0" cellspacing="0" border="0" width="100%">
													<tr>
														<td nowrap>
															<ajax:AjaxPanel ID="AjaxPanel6" runat="server" meta:resourcekey="AjaxPanel6Resource1">
																&nbsp;<asp:CheckBox ID="CheckBoxSelectAll" runat="server" Text="Select All" AutoPostBack="True"
																	meta:resourcekey="CheckBoxSelectAllResource1" />
																&nbsp;
															</ajax:AjaxPanel>
														</td>
														<td style="width: 92%">
															&nbsp;&nbsp;<asp:LinkButton ID="LinkButtonExportExcel" ajaxcall="none" OnClientClick="aspnetForm.encoding = 'multipart/form-data';"
																runat="server" meta:resourcekey="LinkButtonExportExcelResource1">Export 
				to Excel</asp:LinkButton>&nbsp;
															<asp:LinkButton ID="lnkExportAllData" runat="server" ajaxcall="none" OnClientClick="aspnetForm.encoding = 'multipart/form-data';"
																Text="Export All to Excel" meta:resourcekey="lnkExportAllDataResource1"></asp:LinkButton>
														</td>
														<td width="99%" style="text-align: right">
															&nbsp;
														</td>
														<td align="right" nowrap>
															&nbsp;
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td bgcolor="#ffffff">
												<table id="Table3" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
													bgcolor="#ffffff" border="2">
													<tr class="regtext" align="center" bgcolor="#dddddd">
														<td valign="top" align="left" width="50%" bgcolor="#ffffff" >
															Page&nbsp;<ajax:AjaxPanel ID="AjaxPanel7" runat="server" meta:resourcekey="AjaxPanel7Resource1"><asp:Label
																ID="PageCurrentPage" runat="server" CssClass="regtext" meta:resourcekey="PageCurrentPageResource1">0</asp:Label>&nbsp;of&nbsp;
																<asp:Label ID="PageTotalPages" runat="server" CssClass="regtext" meta:resourcekey="PageTotalPagesResource1">0</asp:Label></ajax:AjaxPanel>
														</td>
														<td valign="top" align="right" width="50%" bgcolor="#ffffff" >
															Total Records&nbsp;
															<ajax:AjaxPanel ID="AjaxPanel8" runat="server" meta:resourcekey="AjaxPanel8Resource1">
																<asp:Label ID="PageTotalRows" runat="server" meta:resourcekey="PageTotalRowsResource1">0</asp:Label></ajax:AjaxPanel>
														</td>
													</tr>
												</table>
												<table id="Table4" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
													bgcolor="#ffffff" border="2">
													<tr bgcolor="#ffffff">
														<td class="regtext" valign="middle" align="left" colspan="11" height="7">
															<hr color="#f40101" noshade size="1">
														</td>
													</tr>
													<tr>
														<td class="regtext" valign="middle" align="left" width="63" bgcolor="#ffffff">
															Go to page
														</td>
														<td class="regtext" valign="middle" align="left" width="5" bgcolor="#ffffff">
															<font face="Verdana, Arial, Helvetica, sans-serif" size="1">
																<ajax:AjaxPanel ID="AjaxPanel9" runat="server" meta:resourcekey="AjaxPanel9Resource1">
																	<asp:TextBox ID="TextGoToPage" runat="server" Width="38px" CssClass="searcheditbox"
																		meta:resourcekey="TextGoToPageResource1"></asp:TextBox></ajax:AjaxPanel>
															</font>
														</td>
														<td class="regtext" valign="middle" align="left" bgcolor="#ffffff">
															<ajax:AjaxPanel ID="AjaxPanel10" runat="server" meta:resourcekey="AjaxPanel10Resource1">
																<asp:ImageButton ID="ImageButtonGo" runat="server"  ImageUrl="~/Images/button/go.gif"
																	meta:resourcekey="ImageButtonGoResource1"></asp:ImageButton></ajax:AjaxPanel>
														</td>
														<td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
															<img height="5" src="images/first.gif" width="6">
														</td>
														<td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff">
															<ajax:AjaxPanel ID="AjaxPanel11" runat="server" meta:resourcekey="AjaxPanel11Resource1">
																<asp:LinkButton ID="First" runat="server" CssClass="regtext" CommandName="First"
																	OnCommand="PageNavigate" meta:resourcekey="LinkButtonFirstResource1">First</asp:LinkButton></ajax:AjaxPanel>
														</td>
														<td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff">
															<img height="5" src="images/prev.gif" width="6">
														</td>
														<td class="regtext" valign="middle" align="right" width="14" bgcolor="#ffffff">
															<ajax:AjaxPanel ID="AjaxPanel12" runat="server" meta:resourcekey="AjaxPanel12Resource1">
																<asp:LinkButton ID="LinkButtonPrevious" runat="server" CssClass="regtext" CommandName="Prev"
																	OnCommand="PageNavigate" meta:resourcekey="LinkButtonPreviousResource1">Previous</asp:LinkButton></ajax:AjaxPanel>
														</td>
														<td class="regtext" valign="middle" align="right" width="60" bgcolor="#ffffff">
															<ajax:AjaxPanel ID="AjaxPanel13" runat="server" meta:resourcekey="AjaxPanel13Resource1">
																<a class="pageNav" href="#">
																	<asp:LinkButton ID="LinkButtonNext" runat="server" CssClass="regtext" CommandName="Next"
																		OnCommand="PageNavigate" meta:resourcekey="LinkButtonNextResource1">Next</asp:LinkButton></a></ajax:AjaxPanel>
														</td>
														<td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
															<img height="5" src="images/next.gif" width="6">
														</td>
														<td class="regtext" valign="middle" align="left" width="25" bgcolor="#ffffff">
															<ajax:AjaxPanel ID="AjaxPanel16" runat="server" meta:resourcekey="AjaxPanel16Resource1">
																<asp:LinkButton ID="LinkButtonLast" runat="server" CssClass="regtext" CommandName="Last"
																	OnCommand="PageNavigate" meta:resourcekey="LinkButtonLastResource1">Last</asp:LinkButton></ajax:AjaxPanel>
														</td>
														<td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
															<img height="5" src="images/last.gif" width="6">
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</ajax:AjaxPanel>
							</td>
						</tr>
					</table>
				
			</td>
		</tr>
		<tr>
			<td>
			</td>
			<td>
				<ajax:AjaxPanel ID="AjaxPanel14" runat="server" meta:resourcekey="AjaxPanel14Resource1">
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tr>
							<td align="left" background="Images/button-bground.gif" valign="middle">
								<img height="1" src="Images/blank.gif" width="5" />
							</td>
							<td align="left" background="Images/button-bground.gif" valign="middle">
								&nbsp;
							</td>
							<td background="Images/button-bground.gif">
								&nbsp;
							</td>
							<td background="Images/button-bground.gif">
								&nbsp;
							</td>
							<td background="Images/button-bground.gif">
								&nbsp;
							</td>
							<td background="Images/button-bground.gif" width="99%">
								<img height="1" src="Images/blank.gif" width="1" />
							</td>
							<td>
								
							</td>
						</tr>
					</table>
					<asp:CustomValidator ID="CustomValidator1" runat="server" Display="None" ValidationGroup="handle"
						meta:resourcekey="CustomValidator1Resource1"></asp:CustomValidator><asp:CustomValidator
							ID="CustomValidator2" runat="server" Display="None" meta:resourcekey="CustomValidator2Resource1"></asp:CustomValidator></ajax:AjaxPanel>
			</td>
		</tr>
	</table>
</asp:Content>


