Imports System.Data
Imports System.Data.SqlClient
Imports AML2015Nettier.data
Imports AML2015Nettier.Entities
Partial Class CTRExemptionListDelete
    '    Inherits System.Web.UI.Page
    Inherits Parent



    Public ReadOnly Property CTRExemptionListId() As Integer
        Get
            Return Session("CTRExemptionListDeleteID")
        End Get

    End Property



    Public Property ObjCTRExemptionList() As CTRExemptionList
        Get
            If Session("CTRExemptionList.ObjCTRExemptionList") Is Nothing Then
                Session("CTRExemptionList.ObjCTRExemptionList") = AMLBLL.CTRExemptionListBLL.GetCTRExemptionListByPk(Me.CTRExemptionListId)
                Return CType(Session("CTRExemptionList.ObjCTRExemptionList"), CTRExemptionList)
            Else
                Return CType(Session("CTRExemptionList.ObjCTRExemptionList"), CTRExemptionList)
            End If
        End Get
        Set(ByVal value As CTRExemptionList)
            Session("CTRExemptionList.ObjCTRExemptionList") = value
        End Set
    End Property



    Private Sub InsertAuditTrail(ByRef OTrans As SqlTransaction)
        Try
            Using AccessGroup As New AMLDAL.CTRExemptionListTableAdapters.CTRExemptionListTableAdapter
                Using dt As AMLDAL.CTRExemptionList.CTRExemptionListDataTable = AccessGroup.GetData2(Session("CTRExemptionId"))
                    Dim rowData As AMLDAL.CTRExemptionList.CTRExemptionListRow = dt.Rows(0)
                    Sahassa.AML.AuditTrailAlert.AuditTrailChecking(4)
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, OTrans)
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "CTRExemption", "CTRExemptionListID", "Delete", "", rowData.CTRExemptionListId, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "CTRExemption", "AccountNo", "Delete", "", rowData.AccountNo, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "CTRExemption", "Description", "Delete", "", rowData.Description, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "CTRExemption", "CreatedDate", "Delete", "", rowData.CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "CTRExemption", "AddedBy", "Delete", "", rowData.AddedBy, "Accepted")
                    End Using
                End Using
            End Using
        Catch
            Throw
        End Try
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "CTRExemptionListView.aspx"
        Me.Response.Redirect("CTRExemptionListView.aspx", False)
    End Sub

    Private Sub DeleteCTRExemptionBySU()
        Dim Trans As SqlTransaction = Nothing
        Try
            Using AccessCTRExemptionAdd As New AMLDAL.CTRExemptionListTableAdapters.CTRExemptionListTableAdapter
                Trans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessCTRExemptionAdd)
                Dim CTRExemptionID As Long = Me.LblCTRExemptionListID.Text
                AccessCTRExemptionAdd.DeleteCTRExemption(CTRExemptionID)
                Me.InsertAuditTrail(Trans)
                Trans.Commit()
            End Using
        Catch ex As Exception
            If Not Trans Is Nothing Then Trans.Rollback()
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        Finally
            If Not Trans Is Nothing Then
                Trans.Dispose()
            End If
        End Try
    End Sub

    Function IsDataValid() As Boolean
        Dim strErrorMessage As New StringBuilder

        'cek data masih ada di exemptioinlist
        Dim strdata As String
        If ObjCTRExemptionList.FK_CTRExemptionType_ID.GetValueOrDefault(0) = 1 Then
            strdata = ObjCTRExemptionList.CIFNo
        Else
            strdata = ObjCTRExemptionList.AccountNo
        End If
        If Not AMLBLL.CTRExemptionListBLL.IsExistinctrexemptionList(ObjCTRExemptionList.FK_CTRExemptionType_ID.GetValueOrDefault(0), strdata) Then
            strErrorMessage.Append("Can not delete account no or cif no" & strdata & " because it is already not exist in database anymore.")
        End If


        'cek data masih  ada di approval ngak
        If AMLBLL.CTRExemptionListBLL.IsExistinctrexemptionListApproval(ObjCTRExemptionList.FK_CTRExemptionType_ID.GetValueOrDefault(0), strdata) Then
            strErrorMessage.Append("Can not delete account no or cif no" & strdata & " because it is already exist pending approval.")
        End If
        If strErrorMessage.ToString.Trim.Length > 0 Then
            Throw New Exception(strErrorMessage.ToString)
        Else
            Return True
        End If

    End Function

    Enum Exemptiontype
        CIF = 1
        Account
    End Enum
    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click


        Try

            If IsDataValid Then
                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                    AMLBLL.CTRExemptionListBLL.DeleteBySu(Me.ObjCTRExemptionList.CTRExemptionListId)
                Else

                    Using objCTRExemptionList_PendingApproval As New CTRExemptionList_PendingApproval
                        With objCTRExemptionList_PendingApproval

                            If ObjCTRExemptionList.FK_CTRExemptionType_ID.GetValueOrDefault(0) = Exemptiontype.CIF Then
                                .CTRPendingApproval_CIFNo = ObjCTRExemptionList.CIFNo
                                .CTRPendingApproval_AccountNo = ""
                            Else
                                .CTRPendingApproval_CIFNo = ""
                                .CTRPendingApproval_AccountNo = ObjCTRExemptionList.AccountNo

                            End If

                            .CTRPendingApproval_CreatedDate = Now
                            .CTRPendingApproval_ModeID = 3
                            .CTRPendingApproval_Category = "Delete"
                            .CTRPendingApproval_CreatedBy = Sahassa.AML.Commonly.SessionUserId
                        End With



                        Using objCTRExemptionList_Approval As New CTRExemptionList_Approval
                            With objCTRExemptionList_Approval

                                '.CTRPendingApproval_ID = item.CTRPendingApproval_ID
                                .ModeID = 3
                                .FK_CTRExemptionType_ID = ObjCTRExemptionList.FK_CTRExemptionType_ID.GetValueOrDefault(0)
                                If ObjCTRExemptionList.FK_CTRExemptionType_ID.GetValueOrDefault(0) = Exemptiontype.CIF Then

                                    .CIFNo = ObjCTRExemptionList.CIFNo
                                    .AccountNo = ""
                                Else

                                    .CIFNo = ""
                                    .AccountNo = ObjCTRExemptionList.AccountNo

                                End If

                                .CustomerName = ObjCTRExemptionList.CustomerName
                                .[Description] = TxtDescription.Text.Trim
                                .CreatedDate = Now
                                .AddedBy = Sahassa.AML.Commonly.SessionUserId
                            End With

                            AMLBLL.CTRExemptionListBLL.SaveCtrExemptionListPendingApproval(objCTRExemptionList_PendingApproval, objCTRExemptionList_Approval, 3)
                            Dim MessagePendingID As Integer = 81302 'MessagePendingID 81302 = CTRExemptionList Delete 
                            Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & Session("CTRAccountNo")
                            Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & Session("CTRAccountNo"), False)

                        End Using
                    End Using
                End If

            End If


        Catch ex As Exception
            LogError(ex)
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try

        'Page.Validate()

        'If Page.IsValid Then
        '    Dim Trans As SqlTransaction = Nothing
        '    Try
        '        'Periksa apakah Account tsb masih ada dalam tabel CTRExemptionList atau belum
        '        Using CTRExemption As New AMLDAL.CTRExemptionListTableAdapters.CTRExemptionListTableAdapter
        '            Using CTRPendingAdapter As New AMLDAL.CTRExemptionListTableAdapters.CTRPending_SelectCommandTableAdapter
        '                Dim counter As Integer = CTRExemption.GetCTRExemptionCount(Session("CTRAccountNo"))

        '                'Counter > 0 berarti Account tersebut masih ada dalam tabel CTRExemptionList dan bisa didelete
        '                If counter > 0 Then

        '                    'Periksa apakah Account tsb dalam status pending approval atau tidak
        '                    Using AccessPending As New AMLDAL.CTRExemptionListTableAdapters.CTRApprovalTableAdapter
        '                        counter = CTRPendingAdapter.GetCTRPendingApproval_Count(Session("CTRAccountNo"))

        '                        'Counter = 0 berarti Account tersebut tidak dalam status pending approval
        '                        If counter = 0 Then
        '                            If Sahassa.AML.Commonly.SessionPkUserId = 1 Then

        '                                Me.DeleteCTRExemptionBySU()

        '                                Sahassa.AML.Commonly.SessionIntendedPage = "CTRExemptionListView.aspx"
        '                                Me.Response.Redirect("CTRExemptionListView.aspx", False)
        '                            Else
        '                                'variabel untuk menampung nilai identity dari tabel GroupPendingApprovalID
        '                                Dim CTRPending_Id As Int64

        '                                'Tambahkan ke dalam tabel CTRExemption_PendingApproval dengan ModeID = 3 (Delete)
        '                                Trans = Sahassa.AML.TableAdapterHelper.BeginTransaction(CTRPendingAdapter)
        '                                CTRPending_Id = CTRPendingAdapter.CTRExemptionList_PendingApprovalInsert(Session("CTRAccountNo"), Session("CTRCreatedDate"), 3, "Delete", Sahassa.AML.Commonly.SessionUserId)
        '                                'CTRPendingAdapter.Insert(Session("CTRAccountNo"), Session("CTRCreatedDate"), 3, "Delete", Session("CTRAddedBy"))


        '                                'Using AccessCTRExemption As New AMLDAL.CTRExemptionListTableAdapters.CTRPending_SelectCommandTableAdapter
        '                                '    Using TableCTRExemption As AMLDAL.CTRExemptionList.CTRPending_SelectCommandDataTable = AccessCTRExemption.GetData
        '                                '        Dim TableRowCTRExemption As AMLDAL.CTRExemptionList.CTRPending_SelectCommandRow = TableCTRExemption.Rows(TableCTRExemption.Rows.Count - 1)
        '                                '        CTRPending_Id = TableRowCTRExemption.CTRPendingApproval_Id
        '                                '    End Using
        '                                'End Using

        '                                'Tambahkan ke dalam tabel CTRExemption_Approval dengan ModeID = 3 (Delete) 
        '                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, Trans)
        '                                AccessPending.Insert(CTRPending_Id, 3, Session("CTRAccountNo"), Session("CTRAccountName"), Session("CTRDescription"), Session("CTRCreatedDate"), Session("CTRAddedBy"))

        '                                Trans.Commit()
        '                                Dim MessagePendingID As Integer = 81302 'MessagePendingID 81302 = CTRExemptionList Delete 
        '                                Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & Session("CTRAccountNo")
        '                                Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & Session("CTRAccountNo"), False)
        '                            End If
        '                        Else
        '                            Throw New Exception("Cannot delete the following Account Number: '" & Session("CTRAccountNo") & "' because it is currently waiting for approval.")
        '                        End If

        '                    End Using
        '                Else  'Counter = 0 berarti GroupName tersebut tidak ada dalam tabel Group
        '                    Throw New Exception("Cannot delete the following CTR List: '" & "' because that CTR List does not exist in the database anymore.")
        '                End If
        '            End Using
        '        End Using
        '    Catch tex As Threading.ThreadAbortException
        '        'ignore
        '    Catch ex As Exception
        '        Me.cvalPageError.IsValid = False
        '        Me.cvalPageError.ErrorMessage = ex.Message
        '        LogError(ex)
        '        If Not Trans Is Nothing Then
        '            Trans.Rollback()
        '        End If
        '    Finally
        '        If Not Trans Is Nothing Then
        '            Trans.Dispose()
        '            Trans = Nothing
        '        End If
        '    End Try
        'End If
    End Sub

    Private Sub FillEditData()

        If Not ObjCTRExemptionList Is Nothing Then
            Me.TxtAccountNo.Text = ObjCTRExemptionList.AccountNo
            Me.TxtAccountName.Text = ObjCTRExemptionList.CustomerName
            Me.TxtDescription.Text = ObjCTRExemptionList.Description
            Me.TxtAddedBy.Text = ObjCTRExemptionList.AddedBy
            Me.TxtCIFNo.Text = ObjCTRExemptionList.CIFNo
        End If

        'Using AccessCTRExemption As New AMLDAL.CTRExemptionListTableAdapters.CTRExemptionListTableAdapter
        '    Using TableCTRExemption As AMLDAL.CTRExemptionList.CTRExemptionListDataTable = AccessCTRExemption.GetData2(Session("CTRExemptionListDeleteID"))
        '        If TableCTRExemption.Rows.Count > 0 Then
        '            Dim TableRowCTRExemption As AMLDAL.CTRExemptionList.CTRExemptionListRow = TableCTRExemption.Rows(0)
        '            Session("CTRExemptionId") = TableRowCTRExemption.CTRExemptionListId
        '            Me.LblCTRExemptionListID.Text = Session("CTRExemptionId")
        '            Session("CTRAccountNo") = TableRowCTRExemption.AccountNo
        '            Me.TxtAccountNo.Text = Session("CTRAccountNo")
        '            Session("CTRAccountName") = TableRowCTRExemption.CustomerName
        '            Me.TxtAccountName.Text = Session("CTRAccountName")
        '            Session("CTRDescription") = TableRowCTRExemption.Description
        '            Me.TxtDescription.Text = Session("CTRDescription")
        '            Session("CTRAddedBy") = TableRowCTRExemption.AddedBy
        '            Me.TxtAddedBy.Text = Session("CTRAddedBy")
        '            Session("CTRCreatedDate") = TableRowCTRExemption.CreatedDate
        '        End If
        '    End Using
        'End Using
    End Sub

    Sub ClearData()
        Session("CTRExemptionList.ObjCTRExemptionList") = Nothing

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim Trans As SqlTransaction = Nothing
        Try
            If Not Me.IsPostBack Then
                Cleardata()
                Me.FillEditData()
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Trans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAudit)
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                    Trans.Commit()
                End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            Trans.Rollback()
        Finally
            If Not Trans Is Nothing Then
                Trans.Dispose()
            End If
        End Try
    End Sub
End Class