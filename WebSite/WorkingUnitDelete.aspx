<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="WorkingUnitDelete.aspx.vb" Inherits="WorkingUnitDelete" title="Working Unit Delete" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
	<table id="title"  border="2" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr>
            <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Working Unit - Delete&nbsp;
                    <hr />
                </strong>
                <asp:Label ID="LblSuccess" runat="server" CssClass="validationok" Visible="False"
                    Width="100%"></asp:Label></td>
        </tr>
       <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; border-top-style: none; border-right-style: none;
                border-left-style: none; height: 24px; border-bottom-style: none">
                <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/validationsign_animate.gif" /></td>
            <td bgcolor="#ffffff" colspan="3" style="border-top-style: none; border-right-style: none;
                border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>
                The following Working Unit will be deleted :</strong></td>
        </tr>
    </table>	
	<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2" height="72" id="TABLE1">
                     <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; border-top-style: none; border-right-style: none;
                border-left-style: none; height: 24px; text-align: center; border-bottom-style: none">
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none;
                height: 24px; border-bottom-style: none" width="23%">
                Working Unit ID</td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none;
                height: 24px; border-bottom-style: none">
                :</td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none;
                height: 24px; border-bottom-style: none" width="80%">
                <asp:Label ID="LabelWorkingUnitID" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; text-align: center; border-bottom-style: none;">
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" ControlToValidate="TextWorkingUnitName"
                    Display="Dynamic" ErrorMessage="Working Unit Name cannot be blank">*</asp:RequiredFieldValidator></td>
            <td bgcolor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;" width="23%">
                Working Unit Name</td>
            <td bgcolor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;" width="80%">
                <asp:TextBox ID="TextWorkingUnitName" runat="server" CssClass="textBox" MaxLength="50"
                    ReadOnly="True" Width="190px"></asp:TextBox></td>
        </tr>
		<tr class="formText">
			<td bgColor="#ffffff" style="width: 24px; height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; text-align: center; border-bottom-style: none;">&nbsp;<asp:requiredfieldvalidator id="RequiredFieldValidator2" runat="server" ErrorMessage="Level Type Name cannot be blank" Display="Dynamic"
					ControlToValidate="TextLevelTypeName">*</asp:requiredfieldvalidator></td>
			<td width="23%" bgColor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
                Level Type Name</td>
			<td bgColor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">:</td>
			<td width="77%" bgColor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
                <asp:TextBox ID="TextLevelTypeName" runat="server" CssClass="textBox" MaxLength="50"
                    ReadOnly="True" Width="190px"></asp:TextBox></td>
		</tr>
        <tr class="formText">
            <td bgcolor="#ffffff" rowspan="1" style="width: 24px; height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; text-align: center; border-bottom-style: none;">
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" ControlToValidate="TextWorkingUnitParentName"
                    Display="Dynamic" ErrorMessage="Working Unit Parent cannot be blank">*</asp:RequiredFieldValidator></td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
                Working Unit Parent</td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
                :</td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
                <asp:TextBox ID="TextWorkingUnitParentName" runat="server" CssClass="textBox" MaxLength="50" ReadOnly="True" Width="189px"></asp:TextBox>
            </td>
        </tr>
		<tr class="formText">
			<td bgColor="#ffffff" style="width: 24px; height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; text-align: center; border-bottom-style: none;" rowspan="6">&nbsp;</td>
			<td bgColor="#ffffff" rowspan="6" style="height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
                Description<br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" rowspan="6" style="height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
                :<br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" rowspan="6" style="height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
                <asp:textbox id="TextWorkingUnitDescription" runat="server" CssClass="textBox" MaxLength="255" Width="200px" Height="63px" TextMode="MultiLine" ReadOnly="True"></asp:textbox></td>
		</tr>
		<tr class="formText">
		</tr>
		<tr class="formText">
		</tr>
		<tr class="formText">
		</tr>
		<tr class="formText">
		</tr>
		<tr class="formText">
		</tr>
		<tr class="formText" bgColor="#dddddd" height="30">
			<td style="width: 24px"><IMG height="15" src="images/arrow.gif" width="15"></td>
			<td colSpan="3" style="height: 9px">
				<table cellSpacing="0" cellPadding="3" border="0">
					<tr>
						<td><asp:imagebutton id="ImageUpdate" runat="server" CausesValidation="True" SkinID="DeleteButton"></asp:imagebutton></td>
						<td><asp:imagebutton id="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton"></asp:imagebutton></td>
					</tr>
				</table>               
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
		</tr>
	</table>
	
</asp:Content>

