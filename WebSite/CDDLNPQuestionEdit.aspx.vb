﻿Imports CDDLNPNettier.Data
Imports CDDLNPNettier.Entities
Imports AMLBLL
Imports System.Data

Partial Class CDDLNPQuestionEdit
    Inherits Parent

    Private ReadOnly Property GetPK_CDDQuestion_ID() As Int32
        Get
            Return Request.QueryString("ID")
        End Get
    End Property
    Private Property QuestionDetailDataTable() As Data.DataTable
        Get
            Dim dt As Data.DataTable = DataRepository.CDD_QuestionDetailProvider.GetPaged(CDD_QuestionDetailColumn.FK_CDD_Question_ID.ToString & " = " & GetPK_CDDQuestion_ID, "", 0, Integer.MaxValue, 0).ToDataSet(False).Tables(0)
            dt = CType(IIf(Session("CDDQuestionDetail.Detail") Is Nothing, dt, Session("CDDQuestionDetail.Detail")), Data.DataTable)
            Session("CDDQuestionDetail.Detail") = dt
            Return dt
        End Get
        Set(ByVal value As Data.DataTable)
            Session("CDDQuestionDetail.Detail") = value
        End Set
    End Property
    Private Sub LoadQuestionDetail()
        Me.GridViewCDDQuestionDetail.DataSource = QuestionDetailDataTable
        GridViewCDDQuestionDetail.DataBind()
    End Sub
    Private Property SetnGetCurrentPageReceiver() As Integer
        Get
            Dim result As Integer = 0
            If Not (Session("CDDQuestionDetail.CurrentPageReceiver") Is Nothing) Then
                result = CType(Session("CDDQuestionDetail.CurrentPageReceiver"), Integer)
            End If

            Return result
        End Get
        Set(ByVal Value As Integer)
            Session("CDDQuestionDetail.CurrentPageReceiver") = Value
        End Set
    End Property
    Public Property getQuestionDetailTempPK() As Integer
        Get
            Return IIf(Session("CDDQuestionDetail.QuestionDetailTempPK") Is Nothing, Nothing, Session("CDDQuestionDetail.QuestionDetailTempPK"))
        End Get
        Set(ByVal value As Integer)
            Session("CDDQuestionDetail.QuestionDetailTempPK") = value
        End Set

    End Property
    Property Edit_index() As Integer
        Get
            Return IIf(Session("CDDQuestionDetail.index") Is Nothing, -1, Session("CDDQuestionDetail.index"))
        End Get
        Set(ByVal Value As Integer)
            Session("CDDQuestionDetail.index") = Value
        End Set
    End Property

    Private Property SetnGetBindTable() As TList(Of CDD_QuestionDetail_ApprovalDetail)
        Get
            Return CType(Session("CDDQuestionDetail"), TList(Of CDD_QuestionDetail_ApprovalDetail))
        End Get
        Set(ByVal value As TList(Of CDD_QuestionDetail_ApprovalDetail))
            Session("CDDQuestionDetail") = value
        End Set
    End Property
    Sub clearSession()
        Session("CDDQuestionDetail.QuestionDetailTempPK") = Nothing
        QuestionDetailDataTable = Nothing
    End Sub
    Private Sub LoadData()
        Using objCDDQuestion As TList(Of CDD_Question) = DataRepository.CDD_QuestionProvider.GetPaged("PK_CDD_Question_ID=" & GetPK_CDDQuestion_ID, "", 0, 1, 0)
            If objCDDQuestion.Count > 0 Then
                Me.txtQuestion.Text = objCDDQuestion(0).CDD_Question.ToString
                Me.ddlQuestionType.SelectedValue = objCDDQuestion(0).FK_CDD_QuestionType_ID.GetValueOrDefault(0)
                Select Case ddlQuestionType.SelectedValue
                    Case 3, 4, 7
                        RowQuestionDetail.Visible = True
                        LoadQuestionDetail()
                    Case Else
                        RowQuestionDetail.Visible = False
                End Select
                Me.chkIsRequired.Checked = objCDDQuestion(0).IsRequired.GetValueOrDefault(0)
                Me.ddlSequence.SelectedValue = objCDDQuestion(0).SortNo.GetValueOrDefault(0)
                Me.hfParentQuestionID.Value = objCDDQuestion(0).FK_Parent_Question_ID.GetValueOrDefault(0)
                Using objCDDQuestionParent As TList(Of CDD_Question) = DataRepository.CDD_QuestionProvider.GetPaged("PK_CDD_Question_ID=" & Me.hfParentQuestionID.Value, "", 0, 1, 0)
                    If objCDDQuestionParent.Count > 0 Then
                        Me.lblParentQuestion.Text = objCDDQuestionParent(0).CDD_Question
                    End If
                End Using
            End If
        End Using
    End Sub

    Private Sub DisableFunction()
        ''Disable Function yang question-nya di-hardcode
        ' 1 - Nama Nasabah (Nama nasabah merupakan data mandatory untuk table CDDLNP)
        ' 2 - Jenis Nasabah (Jenis nasabah merupakan data mandatory untuk table CDDLNP)
        ' 3 - Sumber dana nasabah (Jika dari orang lain harus mengisi lembar beneficial owner)
        ' 4 - Apakah terkait nasabah existing? (Untuk jawaban ya menampilkan pertanyaan CIF, untuk jawaban tidak meng-hide pertanyaan CIF)
        ' 5 - CIF# (Terkait fungsi dari pertanyaan nomor 4)
        ' 7 - Apakah termasuk PEP? (PEP merupakan data mandatory untuk table CDDLNP, Jika jawaban ya AML rating High)
        ' 8 - Apakah termasuk mandatory EDD? (Jika jawaban ya AML rating High)
        '11 - Customer screening di AML Solution (Ada link yang dihardcode di pertanyaan ini untuk redirect ke customer screening)
        '86 - CIF# untuk nasabah existing? (CIF# merupakan data mandatory untuk table CDDLNP)

        Select Case GetPK_CDDQuestion_ID
            Case 1, 2, 3, 4, 5, 7, 8, 11, 86
                ddlQuestionType.Enabled = False
                chkIsRequired.Enabled = False
                ImageBrowse.Visible = False
                ImageRemove.Visible = False
                rowCommit.Visible = False
                rowValidation.Visible = False
                rowQuestionDetails.Visible = False
                Me.GridViewCDDQuestionDetail.Columns(GridViewCDDQuestionDetail.Columns.Count - 2).Visible = False
                Me.GridViewCDDQuestionDetail.Columns(GridViewCDDQuestionDetail.Columns.Count - 1).Visible = False

            Case Else
                ddlQuestionType.Enabled = True
                chkIsRequired.Enabled = True
                ImageBrowse.Visible = True
                ImageRemove.Visible = True
                rowCommit.Visible = True
                rowValidation.Visible = True
                rowQuestionDetails.Visible = True
                Me.GridViewCDDQuestionDetail.Columns(GridViewCDDQuestionDetail.Columns.Count - 2).Visible = True
                Me.GridViewCDDQuestionDetail.Columns(GridViewCDDQuestionDetail.Columns.Count - 1).Visible = True

        End Select

    End Sub

    Private Sub BindComboBox()
        ddlQuestionType.Items.Clear()
        For Each objQuestionType As CDD_QuestionType In DataRepository.CDD_QuestionTypeProvider.GetAll()
            ddlQuestionType.Items.Add(New ListItem(objQuestionType.QuestionTypeName, objQuestionType.PK_CDD_QuestionType_ID))
        Next

        ddlValidation.Items.Clear()
        For Each objValidation As CDD_VAlidation In DataRepository.CDD_VAlidationProvider.GetAll()
            ddlValidation.Items.Add(New ListItem(objValidation.CDD_VAlidationName, objValidation.PK_CDD_VAlidation_ID))
        Next

        ddlSequence.Items.Clear()
        ddlSequence.Items.Add(New ListItem("Last", 9999))
        For i As Int32 = 1 To DataRepository.CDD_QuestionProvider.GetTotalItems(String.Empty, 0)
            ddlSequence.Items.Add(New ListItem(i, i))
        Next
    End Sub

    Protected Sub CvalPageErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles cvalPageError.PreRender
        If cvalPageError.IsValid = False Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
        End If
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not Page.IsPostBack Then
                'SetnGetBindTable = New TList(Of CDD_QuestionDetail_ApprovalDetail)
                clearSession()
                BindComboBox()
                LoadData()
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
                DisableFunction()
            End If
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub

    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim objTransManager As New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)
        Dim objApproval As New CDD_Question_Approval
        Dim objCDDQuestion As New CDD_Question_ApprovalDetail
        Dim objCDDQuestionDetail As New CDD_QuestionDetail_ApprovalDetail

        Try
            objTransManager.BeginTransaction()

            If DataRepository.CDD_Question_ApprovalDetailProvider.GetTotalItems("PK_CDD_Question_ID=" & Me.GetPK_CDDQuestion_ID, 0) > 0 Then
                Throw New Exception("Question masih dalam pending approval")
            End If

            If txtQuestion.Text.Trim = String.Empty Then
                Throw New Exception("Question harus diisi")
            End If

            objApproval.RequestedBy = Sahassa.AML.Commonly.SessionPkUserId
            objApproval.RequestedDate = Date.Now
            objApproval.FK_MsMode_Id = 2
            objApproval.IsUpload = False
            DataRepository.CDD_Question_ApprovalProvider.Save(objTransManager, objApproval)


            objCDDQuestion.FK_CDD_Question_Approval_Id = objApproval.PK_CDD_Question_Approval_Id
            objCDDQuestion.CDD_Question = txtQuestion.Text.Trim
            objCDDQuestion.FK_CDD_QuestionType_ID = ddlQuestionType.SelectedValue
            objCDDQuestion.PK_CDD_Question_ID = GetPK_CDDQuestion_ID
            If hfParentQuestionID.Value <> String.Empty Then
                objCDDQuestion.FK_Parent_Question_ID = hfParentQuestionID.Value
            End If
            objCDDQuestion.IsRequired = chkIsRequired.Checked
            objCDDQuestion.SortNo = ddlSequence.SelectedValue
            DataRepository.CDD_Question_ApprovalDetailProvider.Save(objTransManager, objCDDQuestion) 'Save Question


            'For Each rowCDDQuestionDetail As CDD_QuestionDetail_ApprovalDetail In SetnGetBindTable
            '    rowCDDQuestionDetail.FK_CDD_Question_Approval_Id = objApproval.PK_CDD_Question_Approval_Id
            'Next
            Dim result As ArrayList
            Dim i As Integer
            Dim dt As Data.DataTable = QuestionDetailDataTable
            Dim rowCount As Integer = dt.Rows.Count

            If (rowCount > 0) Then
                result = New ArrayList
            End If

            For i = 0 To rowCount - 1
                objCDDQuestionDetail = New CDD_QuestionDetail_ApprovalDetail
                objCDDQuestionDetail.FK_CDD_Question_Approval_Id = objApproval.PK_CDD_Question_Approval_Id
                If dt.Rows(i)("PK_CDD_QuestionDetail_ID").ToString <> "" Then
                    objCDDQuestionDetail.PK_CDD_QuestionDetail_ID = dt.Rows(i)("PK_CDD_QuestionDetail_ID")
                End If
                objCDDQuestionDetail.QuestionDetail = dt.Rows(i)("QuestionDetail").ToString
                objCDDQuestionDetail.FK_CDD_Question_ID = objCDDQuestion.PK_CDD_Question_ID
                objCDDQuestionDetail.IsNeedValidation = dt.Rows(i)("IsNeedValidation")
                objCDDQuestionDetail.FK_CDD_VAlidation_ID = dt.Rows(i)("FK_CDD_VAlidation_ID").ToString
                DataRepository.CDD_QuestionDetail_ApprovalDetailProvider.Save(objTransManager, objCDDQuestionDetail)
            Next


            objTransManager.Commit()

            Me.Response.Redirect("CDDLNPMessage.aspx?ID=10121", False)

        Catch ex As Exception
            objTransManager.Rollback()
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        Finally
            If Not objTransManager Is Nothing Then
                objTransManager.Dispose()
                objTransManager = Nothing
            End If
            If Not objCDDQuestion Is Nothing Then
                objCDDQuestion.Dispose()
                objCDDQuestion = Nothing
            End If
            If Not objCDDQuestionDetail Is Nothing Then
                objCDDQuestionDetail.Dispose()
                objCDDQuestionDetail = Nothing
            End If
        End Try
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Me.Response.Redirect("CDDLNPQuestionView.aspx")
    End Sub

    Protected Sub ImageAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAdd.Click
        Try
            If txtQuestionDetail.Text.Trim = String.Empty Then
                Throw New Exception("Question Detail harus diisi")
                txtQuestionDetail.Focus()
            Else
                'SetnGetBindTable.Add(CDD_QuestionDetail_ApprovalDetail.CreateCDD_QuestionDetail_ApprovalDetail(Nothing, Nothing, Nothing, _
                '    txtQuestionDetail.Text.Trim, ddlValidation.SelectedValue > 0, ddlValidation.SelectedValue))

                Dim dr As Data.DataRow = Nothing

                dr = QuestionDetailDataTable.NewRow
                dr("QuestionDetail") = txtQuestionDetail.Text
                If ddlValidation.SelectedValue <> "" Then
                    dr("FK_CDD_VAlidation_ID") = ddlValidation.SelectedValue
                    If ddlValidation.SelectedValue > 0 Then
                        dr("IsNeedValidation") = True
                    Else
                        dr("IsNeedValidation") = False
                    End If
                End If
                QuestionDetailDataTable.Rows.Add(dr)
                LoadQuestionDetail()
                txtQuestionDetail.Focus()
                txtQuestionDetail.Text = String.Empty
                ddlValidation.SelectedValue = 0
            End If
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub

    Protected Sub ddlQuestionType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlQuestionType.SelectedIndexChanged
        Try
            Select Case ddlQuestionType.SelectedValue
                Case 3, 4, 7
                    RowQuestionDetail.Visible = True

                Case Else
                    RowQuestionDetail.Visible = False

            End Select
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub

    Protected Sub ImageEdit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageEdit.Click
        Try
            If txtQuestionDetail.Text.Trim = String.Empty Then
                Throw New Exception("Question Detail harus diisi")
                txtQuestionDetail.Focus()
            Else
                'SetnGetBindTable(Edit_index).QuestionDetail = txtQuestionDetail.Text
                'SetnGetBindTable(Edit_index).FK_CDD_VAlidation_ID = ddlValidation.SelectedValue

                Dim dr As Data.DataRow = Nothing

                dr = QuestionDetailDataTable.Rows(getQuestionDetailTempPK - 1)
                dr.BeginEdit()
                dr("QuestionDetail") = txtQuestionDetail.Text
                If ddlValidation.SelectedValue <> "" Then
                    dr("FK_CDD_VAlidation_ID") = ddlValidation.SelectedValue
                    If ddlValidation.SelectedValue > 0 Then
                        dr("IsNeedValidation") = True
                    Else
                        dr("IsNeedValidation") = False
                    End If
                End If

                QuestionDetailDataTable.AcceptChanges()

                getQuestionDetailTempPK = Nothing
                LoadQuestionDetail()
                txtQuestionDetail.Focus()
                txtQuestionDetail.Text = String.Empty
                ddlValidation.SelectedValue = 0
                ImageEdit.Visible = False
                ImageCancelEdit.Visible = False
                ImageAdd.Visible = True
            End If
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub

    Protected Sub ImageCancelEdit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancelEdit.Click
        Try
            txtQuestionDetail.Focus()
            txtQuestionDetail.Text = String.Empty
            ddlValidation.SelectedValue = 0
            ImageEdit.Visible = False
            ImageCancelEdit.Visible = False
            ImageAdd.Visible = True
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub

    Protected Sub GridViewCDDQuestionDetail_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridViewCDDQuestionDetail.ItemCommand
        Try
            Select Case e.CommandName.ToLower
                Case "edit"
                    Edit_index = e.Item.ItemIndex
                    'txtQuestionDetail.Text = SetnGetBindTable(Edit_index).QuestionDetail
                    'ddlValidation.SelectedValue = SetnGetBindTable(Edit_index).FK_CDD_VAlidation_ID
                    Dim dr As Data.DataRow = QuestionDetailDataTable.Rows(e.Item.ItemIndex + (SetnGetCurrentPageReceiver * Sahassa.AML.Commonly.GetDisplayedTotalRow))
                    getQuestionDetailTempPK = (e.Item.ItemIndex + (SetnGetCurrentPageReceiver * Sahassa.AML.Commonly.GetDisplayedTotalRow)) + 1
                    txtQuestionDetail.Text = dr("QuestionDetail").ToString
                    If dr("FK_CDD_VAlidation_ID").ToString.Length > 0 Then
                        ddlValidation.SelectedValue = dr("FK_CDD_VAlidation_ID")
                    End If


                    txtQuestionDetail.Focus()
                    ImageEdit.Visible = True
                    ImageCancelEdit.Visible = True
                    ImageAdd.Visible = False

                Case "delete"
                    'SetnGetBindTable.RemoveAt(e.Item.ItemIndex)
                    If Page.IsPostBack Then
                        QuestionDetailDataTable.Rows.RemoveAt(e.Item.ItemIndex + (SetnGetCurrentPageReceiver * Sahassa.AML.Commonly.GetDisplayedTotalRow))

                        LoadQuestionDetail()
                    End If
            End Select
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub

    Protected Sub GridViewCDDQuestionDetail_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridViewCDDQuestionDetail.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Using objValidation As CDD_VAlidation = DataRepository.CDD_VAlidationProvider.GetByPK_CDD_VAlidation_ID(e.Item.Cells(2).Text)
                    If objValidation IsNot Nothing Then
                        e.Item.Cells(3).Text = objValidation.CDD_VAlidationName
                    End If
                End Using
            End If
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Session("strPK_CDD_Question_ID") = "''"
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub

    Protected Sub ImageBrowse_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBrowse.Click
        Try
            If Session("PopupCDDQuestion.PK_CDD_Question_ID") IsNot Nothing Then
                lblParentQuestion.Text = Session("PopupCDDQuestion.CDD_Question").ToString.Replace("</br>", vbCrLf)
                hfParentQuestionID.Value = Session("PopupCDDQuestion.PK_CDD_Question_ID")
            End If
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub

    Protected Sub ImageRemove_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageRemove.Click
        Try
            lblParentQuestion.Text = String.Empty
            hfParentQuestionID.Value = String.Empty
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub
End Class