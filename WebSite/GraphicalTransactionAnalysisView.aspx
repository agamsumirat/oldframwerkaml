<%@ Page Language="VB" AutoEventWireup="false" CodeFile="GraphicalTransactionAnalysisView.aspx.vb" Inherits="GraphicalTransactionAnalysisView" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Graphical Transaction Analysis</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="theme/aml.css" rel="stylesheet" type="text/css">
	<link href="theme/popcalendar.css" type="text/css" rel="stylesheet">
	<script language="javascript" src="script/popcalendar.js"></script>
	<script src="script/x_load.js"></script>
	<script language="javascript">xInclude('script/x_core.js');</script>
	<script language="javascript" src="script/aml.js" ></script>
    <script language="javascript">
        if (document.layers) { // Netscape
            document.captureEvents(Event.MOUSEMOVE);
            document.onmousemove = captureMousePosition;
        } else if (document.all) { // Internet Explorer
            document.onmousemove = captureMousePosition;
        } else if (document.getElementById) { // Netcsape 6
            document.onmousemove = captureMousePosition;
        }
        // Global variables
        xMousePos = 0; // Horizontal position of the mouse on the screen
        yMousePos = 0; // Vertical position of the mouse on the screen
        xMousePosMax = 0; // Width of the page
        yMousePosMax = 0; // Height of the page

        function captureMousePosition(e) {
            if (document.layers) {

                xMousePos = e.pageX;
                yMousePos = e.pageY;
                xMousePosMax = window.innerWidth+window.pageXOffset;
                yMousePosMax = window.innerHeight+window.pageYOffset;
            } else if (document.all) {
                xMousePos = document.body.scrollLeft + (window.event?window.event.x:0);
                yMousePos = document.body.scrollTop + (window.event?window.event.y:0);
                //window.status ="xMousePos="+xMousePos+";"+"yMousePos="+yMousePos;
                xMousePosMax = document.body.clientWidth+document.body.scrollLeft;
                yMousePosMax = document.body.clientHeight+document.body.scrollTop;
            } else if (document.getElementById) {
                // Netscape 6 behaves the same as Netscape 4 in this regard
                xMousePos = e.pageX;
                yMousePos = e.pageY;
                xMousePosMax = window.innerWidth+window.pageXOffset;
                yMousePosMax = window.innerHeight+window.pageYOffset;
            }
           //window.status = "xMousePos=" + xMousePos + ", yMousePos=" + yMousePos + ", xMousePosMax=" + xMousePosMax + ", yMousePosMax=" + yMousePosMax + " - " + document.documentElement.scrollTop ;
        }    
    var SelectedAccountNo;
    function ShowMenu(StrAccountNo)
    {
        var scrOfX = 0, scrOfY = 0;

        if( typeof( window.pageYOffset ) == 'number' ) {
        //Netscape compliant
        scrOfY = window.pageYOffset;
        scrOfX = window.pageXOffset;
        } else if( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ) ) {
        //IE6 standards compliant mode
        scrOfY = document.documentElement.scrollTop;
        scrOfX = document.documentElement.scrollLeft;

        } else if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) ) {
        //DOM compliant
        scrOfY = document.body.scrollTop;
        scrOfX = document.body.scrollLeft;
        }

                              
        SelectedAccountNo=StrAccountNo;
//        xShow(xGetElementById("divContext"));
//        xMoveTo(xGetElementById("divContext"),window.event.clientX,window.event.clientY)
        var dtStartDate = xGetElementById('StartDateTextBox').value;
        var dtEndDate = xGetElementById('EndDateTextBox').value;
        var ele = xGetElementById("popupmenu");
        ele.src="GraphicalTransactionAnalysisMenu.aspx?StartDate=" + dtStartDate + "&EndDate="  + dtEndDate
        xShow(ele);
        xMoveTo(ele,xMousePos+scrOfX,yMousePos+scrOfY);
//        var aExpand = xGetElementById('aExpand');
//        aExpand.innerText="Expand";
//        if (dtStartDate && dtEndDate) {
//           aExpand.innerText="Expand (" + dtStartDate + " - " + dtEndDate + ")"; 
//        }
    }

    function HideMenu()
    {
        xHide("popupmenu");
    }

    function Expand()
    {
        xGetElementById('chkExpandLink').checked=true;
        xGetElementById('hidSelectedNode').value=SelectedAccountNo;
        xGetElementById('imgGo').click();
    }

    function ShowAccontInfo()
    {
		var StrAccountNo;
		StrAccountNo=SelectedAccountNo.substring(3, SelectedAccountNo.indexOf("_"))
        window.top.location.href="AccountInformationDetail.aspx?AccountNo=" + StrAccountNo;
    }

//    function CreateNewCase()
//    {
//    }
</script>
</head>

<body leftmargin="0" topmargin="0" scroll="yes" background="images/main-contentbg.gif">
<form id="form1" runat="server">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
  	  <td id="tdcontent" height="99%" valign="top">
  	  <div id="divcontent" class="divcontent" >
  	  <table cellpadding="0" cellspacing="0" border="0" width="100%">
	  	<tr>
			<td background="images/main-menu-bg-shadow.gif" height="10"></td>
		</tr>
		<tr>
			<td class="divcontentinside" style="height: 712px" valign="top">
			        <ajax:ajaxpanel id="a" runat="server"><asp:validationsummary id="ValidationSummary1" runat="server" HeaderText="There were errors on the page:" Width="100%" CssClass="validation" ></asp:validationsummary></ajax:ajaxpanel>                
			        
                    <table>
                        <tr>
                            <td>
                                <table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd" border="0">
                                    <tr bgcolor="#ffffff">
                                        <td>&nbsp;</td>
                                        <td title="Test">Transaction Date From</td>
                                        <td>:</td>
                                        <td nowrap>
                                            <asp:TextBox ID="StartDateTextBox" runat="server" CssClass="textBox" MaxLength="20"></asp:TextBox>
                                                <input id="cmdStartDatePicker" runat="server" name="popUpCalc" style="border-right: #ffffff 0px solid;
                                                    border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
                                                    border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
                                                    height: 17px" title="Click to show calendar" type="button" /></td>
                                        <td>
                                            &nbsp;To&nbsp;</td>
                                        <td nowrap><asp:TextBox ID="EndDateTextBox" runat="server" CssClass="textBox" MaxLength="20"></asp:TextBox> <input
                                                id="cmdEndDatePicker" runat="server" name="popUpCalc" style="border-right: #ffffff 0px solid;
                                                border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
                                                border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
                                                height: 17px" title="Click to show calendar" type="button" />
                                            <asp:CheckBox ID="chkExpandLink" runat="server" />
                                            <asp:HiddenField ID="hidSelectedNode" runat="server" />
                                        </td>
                                    </tr>
<%--                                    <tr bgcolor="#ffffff">
                                        <td>&nbsp;</td>
                                        <td valign="top" nowrap>
                                            Filter Auxiliary 
                                            <br />
                                            Transaction Code</td>
                                        <td>:</td>
                                        <td colspan="3" valign="top"><ajax:AjaxPanel ID="a2" runat="server"><table cellpadding="0" cellspacing="0" border="0"><tr><td style="height: 70px">
                                            <asp:ListBox ID="lstAllTrxCode" runat="server" CssClass="textBox" Rows="9" SelectionMode="Multiple" Width="250px"></asp:ListBox></td><td style="height: 70px" align="center">
                                                &nbsp;<asp:ImageButton ID="btnAdd" runat="server" ImageUrl="~/Images/button/add.gif" /><br />
                                                &nbsp;
                                                <asp:ImageButton ID="btnRemove" runat="server" ImageUrl="~/Images/button/delete.gif" />&nbsp;
                                            </td><td style="height: 70px">
                                            <asp:ListBox ID="lstSelectedTrxCode" runat="server" CssClass="textBox" Rows="9" SelectionMode="Multiple" Width="250px"></asp:ListBox></td></tr></table></ajax:AjaxPanel>
                                            </td>
                                    </tr> --%>                       
                                    <tr bgcolor="#ffffff">
                                        <td>&nbsp;</td>
                                        <td nowrap>Transaction Amount</td>
                                        <td>:</td>
                                        <td>
                                            <asp:TextBox ID="TransactionAmountFromTextBox" runat="server" CssClass="textBox" Width="130px"></asp:TextBox></td>
                                        <td>&nbsp;To&nbsp;</td>
                                        <td width="99%"><asp:TextBox ID="TransactionAmountToTextBox" runat="server" CssClass="textBox" Width="130px"></asp:TextBox></td>
                                        
                                    </tr>
                                    <tr bgcolor="#dddddd">
                                        <td><IMG height="15" src="images/arrow.gif" width="15"></td><td colspan="5"><asp:ImageButton ID="imgGo" ImageUrl="~/images/button/go.gif" runat="server" />
                                            <asp:ImageButton ID="ImageButtonClear" ImageUrl="~/Images/button/clearsearch.gif" runat="server" />
                                            <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top"><br /><br /><table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd" border="0">
<%--                                <tr bgcolor="#ffffff">
                                    <td nowrap="nowrap">Zoom: </td>
                                    <td nowrap="nowrap"><input type="text" size="3" class="textBox" id="txtZoom" onkeypress="javascript:return checkKey(event)" value="100" /> %</td>
                                    <td width="99%"><a href="javascript:zoom()"><img border="0" src="images/zoom.gif" /></a></td>
                                </tr>--%>
                                <tr><td colspan="3" nowrap="nowrap">
                                
                                     <table id="Table3" bgcolor="#ffffff" border="2" bordercolor="#ffffff" cellpadding="2"
                                    cellspacing="1" width="100%">
                                    <tr align="center" bgcolor="#dddddd" class="regtext">
                                        <td align="left" bgcolor="#ffffff" valign="top" width="50%">
                                            Page
                                            
                                                <asp:Label ID="PageCurrentPage" runat="server" CssClass="regtext">0</asp:Label>
                                                &nbsp;of&nbsp;
                                                <asp:Label ID="PageTotalPages" runat="server" CssClass="regtext">0</asp:Label>
                                            
                                        </td>
                                        <td align="right" bgcolor="#ffffff" valign="top" width="50%">
                                            Total Records&nbsp;
                                            
                                                <asp:Label ID="PageTotalRows" runat="server">0</asp:Label>
                                            
                                        </td>
                                    </tr>
                                </table>
                                <table id="Table4" bgcolor="#ffffff" border="2" bordercolor="#ffffff" cellpadding="2"
                                    cellspacing="1" width="100%">
                                    <tr bgcolor="#ffffff">
                                        <td align="left" class="regtext" colspan="11" height="7" valign="middle">
                                            <hr color="#f40101" noshade="noshade" size="1" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="63">
                                            Go to page</td>
                                        <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="5">
                                            <font face="Verdana, Arial, Helvetica, sans-serif" size="1">
                                                
                                                    <asp:TextBox ID="TextGoToPage" runat="server" CssClass="searcheditbox" Width="38px"></asp:TextBox>
                                                
                                            </font>
                                        </td>
                                        <td align="left" bgcolor="#ffffff" class="regtext" valign="middle">
                                            
                                                <asp:ImageButton ID="ImageButtonGo" runat="server" SkinID="GoButton" />
                                            
                                        </td>
                                        <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="6">
                                            <img height="5" src="images/first.gif" width="6" />
                                        </td>
                                        <td align="right" bgcolor="#ffffff" class="regtext" valign="middle" width="6">
                                            
                                                <asp:LinkButton ID="LinkButtonFirst" runat="server" CommandName="First" CssClass="regtext"
                                                    OnCommand="PageNavigate">First</asp:LinkButton>
                                            
                                        </td>
                                        <td align="right" bgcolor="#ffffff" class="regtext" valign="middle" width="6">
                                            <img height="5" src="images/prev.gif" width="6" /></td>
                                        <td align="right" bgcolor="#ffffff" class="regtext" valign="middle" width="14">
                                            
                                                <asp:LinkButton ID="LinkButtonPrevious" runat="server" CommandName="Prev" CssClass="regtext"
                                                    OnCommand="PageNavigate">Previous</asp:LinkButton>
                                            
                                        </td>
                                        <td align="right" bgcolor="#ffffff" class="regtext" valign="middle" width="60">
                                            
                                                <a class="pageNav" href="#">
                                                    <asp:LinkButton ID="LinkButtonNext" runat="server" CommandName="Next" CssClass="regtext"
                                                        OnCommand="PageNavigate">Next</asp:LinkButton>
                                                </a>
                                            
                                        </td>
                                        <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="6">
                                            <img height="5" src="images/next.gif" width="6" /></td>
                                        <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="25">
                                            
                                                <asp:LinkButton ID="LinkButtonLast" runat="server" CommandName="Last" CssClass="regtext"
                                                    OnCommand="PageNavigate">Last</asp:LinkButton>
                                            
                                        </td>
                                        <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="6">
                                            <img height="5" src="images/last.gif" width="6" /></td>
                                    </tr>
                                </table>
                                </td> </tr>
                                <tr bgcolor="#ffffff">
                                    <td colspan="3" nowrap="nowrap">Amount in million</td>
                                </tr>
                                <tr bgcolor="#ffffff">
                                    <td colspan="2" nowrap="nowrap">
                                        Account Legend</td>
                                    <td nowrap="nowrap">
                                        Auxiliary Transaction Code Legend</td>
                                </tr>
                                <tr bgcolor="#ffffff">
                                    <td nowrap="nowrap" colspan="2" style="height: 145px" valign="top">
                                        <asp:GridView ID="GridViewAccountLegend" runat="server" AutoGenerateColumns="False" BackColor="White"
                                            BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black"
                                            GridLines="Vertical">
                                            <FooterStyle BackColor="#CCCC99" />
                                            <Columns>
                                                <asp:BoundField DataField="AccountNo" HeaderText="Account No" />
                                                <asp:BoundField DataField="AccountName" HeaderText="Name" />
                                            </Columns>
                                            <RowStyle BackColor="#F7F7DE" />
                                            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                            <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                            <AlternatingRowStyle BackColor="White" />
                                        </asp:GridView>                                    </td>
                                    <td nowrap="nowrap" style="height: 145px" valign="top">
                                        <asp:GridView ID="GridViewAuxTransactionCodeLegend" runat="server" AutoGenerateColumns="False" BackColor="White"
                                            BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black"
                                            GridLines="Vertical">
                                            <FooterStyle BackColor="#CCCC99" />
                                            <Columns>
                                                <asp:BoundField DataField="TrxCode" HeaderText="Auxiliary Transaction Code" />
                                                <asp:BoundField DataField="TrxDesc" HeaderText="Auxiliary Transaction Description" />
                                            </Columns>
                                            <RowStyle BackColor="#F7F7DE" />
                                            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                            <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                            <AlternatingRowStyle BackColor="White" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr bgcolor="#ffffff">
                                    <td colspan="3">
                                    <IMG id="GraphicalTransactionAnalysisImage"  src="~/Images/nodata.gif" border="0"  runat=server visible="false">
                                    <embed src="./GraphicalTransactionAnalysis.aspx?.svg"  type="image/svg+xml" width="1000" height="1000" />
                                    <iframe id="popupmenu" src="" name="popupmenu" style="visibility:hidden; z-index:100;position:absolute;" width="280" height="90" scrolling="no" frameborder=no></iframe>                                    
                                    </td>
                                </tr>
                            </table>
<%--                            <div id="divContext" style="border: 1px solid blue; position: absolute; visibility:hidden;background-color:White;" >
                              <table>
                                <tr>
                                    <td align=right style="height: 15px"><a href="Javascript:HideMenu();">Close</a></td>
                                </tr>
                                <tr>
                                    <td style="height: 15px"><a href="Javascript:Expand();" id="aExpand">Expand</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><a href="Javascript:ShowAccontInfo();">Show Account Info</a></td>
                                </tr>                
                                <tr>
                                    <td><a href="Javascript:CreateNewCase();">Create New Case</a></td>
                                </tr>       
                              </table>  
                            </div>
--%>                            
                           
                            </td>
                        </tr>
                    </table></td>
		</tr>
	  </table></div></td>
  </tr>
</table></form><script>arrangeView();</script></body>
</html>



