Imports System.Data
Imports System.IO
Imports SahassaNettier.Data
Imports SahassaNettier.Entities

Partial Class DownloadAttachment
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Request.QueryString.Count > 0 Then
                If Request.QueryString("FromRiskScoring") = "Y" Then
                    If Request.QueryString("PKDownloadID") <> "" Then
                        Dim idRS As Integer = CInt(Request.QueryString("PKDownloadID"))

                        Dim ds As DataSet = Sahassa.AML.Commonly.ExecuteDataset("select * from RekapRiskScoringReportTable where PK_RekapRiskScoringReportTable_Id = " & idRS)
                        If ds.Tables(0) IsNot Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                            Dim strExtension As String = ""
                            If ds.Tables(0).Rows(0)(4).ToString.Trim.ToLower = "application/zip".ToLower Then
                                strExtension = ".zip"
                            End If
                            Response.AddHeader("content-disposition", "attachment; filename=" & ds.Tables(0).Rows(0)(1) & strExtension)
                            Response.Charset = ""
                            Me.EnableViewState = False
                            Response.ContentType = ds.Tables(0).Rows(0)(4).ToString.Trim
                            Response.BinaryWrite(ds.Tables(0).Rows(0)(2))
                            Response.End()
                        End If
                    End If
                ElseIf Request.QueryString("PKDownloadID") <> "" Then
                    Dim id As Integer = CInt(Request.QueryString("PKDownloadID"))

                    'Using objDA As New AMLDAL.UpdatingDataReportDSTableAdapters.UpdatingDataFilesDownloadTableAdapter
                    '    Using objDT As AMLDAL.UpdatingDataReportDS.UpdatingDataFilesDownloadDataTable = objDA.GetDataUpdatingDownloadByID(id)
                    '        If objDT IsNot Nothing AndAlso objDT.Rows.Count > 0 Then
                    '            Dim objRow As AMLDAL.UpdatingDataReportDS.UpdatingDataFilesDownloadRow = objDT.Rows(0)

                    '            Response.AddHeader("content-disposition", "attachment; filename=" & objRow.FileName)
                    '            Response.Charset = ""
                    '            Me.EnableViewState = False
                    '            Response.ContentType = objRow.ContentType
                    '            Response.BinaryWrite(objRow.Data)
                    '            Response.End()
                    '        End If
                    '    End Using
                    'End Using
                ElseIf Request.QueryString("FromSearching") = "Y" Then
                    Try
                        Dim strFile As String = ""
                        If Session("DownloadDataFiltered") IsNot Nothing AndAlso CStr(Session("DownloadDataFiltered")) <> String.Empty Then
                            strFile = CStr(Session("DownloadDataFiltered"))
                        End If
                        Response.AppendHeader("content-disposition", "attachment; filename=" & Path.GetFileName(strFile))
                        Response.ContentType = "application/zip"
                        Response.TransmitFile(strFile)
                        Response.End()
                    Catch ex As Exception

                    End Try
                ElseIf Request.QueryString("IsTemplate") = "Y" Then
                    Try
                        Dim strFile As String = ""
                        If Session("FileTemplatePath") IsNot Nothing AndAlso CStr(Session("FileTemplatePath")) <> String.Empty Then
                            strFile = CStr(Session("FileTemplatePath"))
                        End If
                        Response.AppendHeader("content-disposition", "attachment; filename=" & Path.GetFileName(strFile))
                        Response.ContentType = "application/ms-excel"
                        Response.TransmitFile(strFile)
                        Response.End()
                    Catch ex As Exception
                        Throw
                    End Try
                End If

                If Request.QueryString("IsFile") = "Y" Or Request.QueryString("IsFile") = "y" Then
                    Dim strFullPathFile As String = Request.QueryString("FileName")
                    Response.AppendHeader("content-disposition", "attachment; filename=" & Path.GetFileName(strFullPathFile))
                    Response.ContentType = "application/zip"
                    Response.TransmitFile(strFullPathFile)
                    Response.End()
                ElseIf Request.QueryString("IsBinary") = "Y" Or Request.QueryString("IsBinary") = "y" Then
                    Dim contentType As String = ""
                    Dim dataSource As String = Request.Params("DataSource")
                    Dim fileName As String = ""
                    Dim id As Integer = CInt(Request.Params("id"))
                    Dim buffer As Byte() = New Byte() {}

                    If Request.Params("ext").Trim() = "xls" Then
                        contentType = "application/vnd.ms-excel"
                    End If

                    If dataSource.ToLower = "wic" Then
                        Using obj As WICGeneratedFileXls = DataRepository.WICGeneratedFileXlsProvider.GetByPK_WICGeneratedFileXls_Id(id)
                            If obj IsNot Nothing Then
                                fileName = obj.CTRXlsFileName
                                buffer = obj.BinaryFileData
                            End If
                        End Using
                    Else
                        Using obj As LTKTGeneratedFileXls = DataRepository.LTKTGeneratedFileXlsProvider.GetByPK_LTKTGeneratedFileXls_Id(id)
                            If obj IsNot Nothing Then
                                fileName = obj.CTRXlsFileName
                                buffer = obj.BinaryFileData
                            End If
                        End Using
                    End If

                    Response.AddHeader("Content-type", contentType)
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName)
                    Response.BinaryWrite(buffer)
                    Response.Flush()
                    Response.End()
                End If
            End If
        End If
    End Sub
End Class
