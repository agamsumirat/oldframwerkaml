#Region "Imports..."
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Excel
Imports Sahassa.AML.Commonly
Imports Sahassanettier.Data
Imports Sahassanettier.Entities
Imports SahassaNettier.Data.SqlClient
Imports System.Data.SqlClient
Imports System
Imports System.Collections.Generic
Imports System.Data
Imports System.IO
Imports AMLBLL.DataType
#End Region

Partial Class MsBentukBidangUsaha_upload
    Inherits Parent

#Region "Structure..."

    Structure GrouP_MsBentukBidangUsaha_success
        Dim MsBentukBidangUsaha As MsBentukBidangUsaha_ApprovalDetail
        Dim L_MsBentukBidangUsahaNCBS As TList(Of MsBentukBidangUsahaNCBS_ApprovalDetail)

    End Structure

#End Region

#Region "Property umum"
    Private Property GroupTableSuccessData() As List(Of GrouP_MsBentukBidangUsaha_success)
        Get
            Return Session("GroupTableSuccessData")
        End Get
        Set(ByVal value As List(Of GrouP_MsBentukBidangUsaha_success))
            Session("GroupTableSuccessData") = value
        End Set
    End Property

    Private Property GroupTableAnomalyData() As List(Of AMLBLL.MsBentukBidangUsahaBll.DT_Group_MsBentukBidangUsaha)
        Get
            Return Session("GroupTableAnomalyData")
        End Get
        Set(ByVal value As List(Of AMLBLL.MsBentukBidangUsahaBll.DT_Group_MsBentukBidangUsaha))
            Session("GroupTableAnomalyData") = value
        End Set
    End Property

    Public ReadOnly Property GET_PKUserID() As Integer
        Get
            Return SessionPkUserId
        End Get
    End Property

    Public ReadOnly Property GET_UserName() As String
        Get
            Return SessionUserId
            'Return SessionNIK
        End Get
    End Property

    Private Property SetnGetDataTableForSuccessData() As TList(Of MsBentukBidangUsaha_ApprovalDetail)
        Get
            Return CType(IIf(Session("MsBentukBidangUsaha_upload.TableSuccessUpload") Is Nothing, Nothing, Session("MsBentukBidangUsaha_upload.TableSuccessUpload")), TList(Of MsBentukBidangUsaha_ApprovalDetail))
        End Get
        Set(ByVal value As TList(Of MsBentukBidangUsaha_ApprovalDetail))
            Session("MsBentukBidangUsaha_upload.TableSuccessUpload") = value
        End Set
    End Property

    Private Property SetnGetDataTableForAnomalyData() As Data.DataTable
        Get
            Return CType(IIf(Session("MsBentukBidangUsaha_upload.TableAnomalyUpload") Is Nothing, Nothing, Session("MsBentukBidangUsaha_upload.TableAnomalyUpload")), Data.DataTable)
        End Get
        Set(ByVal value As Data.DataTable)
            Session("MsBentukBidangUsaha_upload.TableAnomalyUpload") = value
        End Set
    End Property

    Private Property DSexcelTemp() As Data.DataSet
        Get
            Return CType(IIf(Session("MsBentukBidangUsaha_upload.DSexcelTemp") Is Nothing, Nothing, Session("MsBentukBidangUsaha_upload.DSexcelTemp")), DataSet)
        End Get
        Set(ByVal value As Data.DataSet)
            Session("MsBentukBidangUsaha_upload.DSexcelTemp") = value
        End Set
    End Property

#End Region

#Region "Property Tabel Temporary"
    Private Property DT_AllMsBentukBidangUsaha() As Data.DataTable
        Get
            Return CType(IIf(Session("MsBentukBidangUsaha_upload.DT_MsBentukBidangUsaha") Is Nothing, Nothing, Session("MsBentukBidangUsaha_upload.DT_MsBentukBidangUsaha")), Data.DataTable)
        End Get
        Set(ByVal value As Data.DataTable)
            Session("MsBentukBidangUsaha_upload.DT_MsBentukBidangUsaha") = value
        End Set
    End Property

    Private Property DT_AllMsBentukBidangUsahaNCBS() As Data.DataTable
        Get
            Return CType(IIf(Session("MsBentukBidangUsaha_upload.DT_MsBentukBidangUsahaNCBS") Is Nothing, Nothing, Session("MsBentukBidangUsaha_upload.DT_MsBentukBidangUsahaNCBS")), Data.DataTable)
        End Get
        Set(ByVal value As Data.DataTable)
            Session("MsBentukBidangUsaha_upload.DT_MsBentukBidangUsahaNCBS") = value
        End Set
    End Property

#End Region

#Region "Property Table Sukses Data"

    Private Property TableSuccessMsBentukBidangUsaha() As TList(Of MsBentukBidangUsaha_ApprovalDetail)
        Get
            Return Session("TableSuccessMsBentukBidangUsaha")
        End Get
        Set(ByVal value As TList(Of MsBentukBidangUsaha_ApprovalDetail))
            Session("TableSuccessMsBentukBidangUsaha") = value
        End Set
    End Property

    Private Property TableSuccessMsBentukBidangUsahaNCBS() As TList(Of MsBentukBidangUsahaNCBS)
        Get
            Return Session("TableSuccessMsBentukBidangUsahaNCBS")
        End Get
        Set(ByVal value As TList(Of MsBentukBidangUsahaNCBS))
            Session("TableSuccessMsBentukBidangUsahaNCBS") = value
        End Set
    End Property


#End Region

#Region "Property Table Anomaly Data"

    Private Property TableAnomalyMsBentukBidangUsaha() As Data.DataTable
        Get
            Return Session("TableAnomalyMsBentukBidangUsaha")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("TableAnomalyMsBentukBidangUsaha") = value
        End Set
    End Property

    Private Property TableAnomalyMsBentukBidangUsahaNCBS() As Data.DataTable
        Get
            Return Session("TableAnomalyMsBentukBidangUsahaNCBS")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("TableAnomalyMsBentukBidangUsahaNCBS") = value
        End Set
    End Property

#End Region

#Region "validasi.."

    Private Function ValidateInput(ByVal MsBentukBidangUsaha As AMLBLL.MsBentukBidangUsahaBll.DT_Group_MsBentukBidangUsaha) As List(Of String)
        Dim temp As New List(Of String)
        Dim msgError As New StringBuilder
        Dim errorline As New StringBuilder
        Try
            'validasi
            ValidateMsBentukBidangUsaha(MsBentukBidangUsaha.MsBentukBidangUsaha, msgError, errorline)
            If msgError.ToString = "" Then
                For Each ci As DataRow In MsBentukBidangUsaha.L_MsBentukBidangUsahaNCBS.Rows
                    'validasi NCBS
                    ValidateMsBentukBidangUsahaNCBS(ci, msgError, errorline)
                Next
            End If
            temp.Add(msgError.ToString)
            temp.Add(errorline.ToString)
        Catch ex As Exception
            msgError.Append(ex.Message)
            temp.Add(msgError.ToString)
            temp.Add(errorline.ToString)
        End Try
        Return temp
    End Function

    Private Shared Sub ValidateMsBentukBidangUsahaNCBS(ByVal DTR As DataRow, ByVal msgError As StringBuilder, ByVal errorLine As StringBuilder)
        Try

            '======================== Validasi textbox :  IdBentukBidangusahaNCBS canot Null (#)====================================================
            If ObjectAntiNull(DTR("IdBentukBidangusahaBank")) = False Then
                msgError.AppendLine(" &bull; IdBentukBidangusahaBank must be filled <BR/>")
                errorLine.AppendLine(" &bull; MsBentukBidangUsahaBank : Line " & DTR("NO") & "<BR/>")
            End If

            '======================== Validasi textbox :  BentukBidangUsahaNCBS canot Null (#)====================================================
            If ObjectAntiNull(DTR("BentukBidangUsahaBank")) = False Then
                msgError.AppendLine(" &bull; BentukBidangUsahaBank must be filled <BR/>")
                errorLine.AppendLine(" &bull; MsBentukBidangUsahaBank : Line " & DTR("NO") & "<BR/>")
            End If

        Catch ex As Exception
            msgError.AppendLine(" &bull;" & ex.Message & "<BR/>")
            errorLine.AppendLine(" &bull;MsBentukBidangUsahaBank : Line " & DTR("NO") & "<BR/>")
        End Try
    End Sub

    Private Shared Sub ValidateMsBentukBidangUsaha(ByVal DTR As DataRow, ByVal msgError As StringBuilder, ByVal errorline As StringBuilder)
        Try

            Using checkBlocking As TList(Of MsBentukBidangUsaha_ApprovalDetail) = DataRepository.MsBentukBidangUsaha_ApprovalDetailProvider.GetPaged("IdBentukBidangUsaha=" & DTR("IdBentukBidangUsaha"), "", 0, 1, Nothing)
                If checkBlocking.Count > 0 Then
                    msgError.AppendLine(" &bull;MsBentukBidangUsaha is waiting for approval <BR/>")
                    errorline.AppendLine(" &bull;MsBentukBidangUsaha : Line " & DTR("NO") & "<BR/>")
                End If
            End Using


        Catch ex As Exception
            msgError.AppendLine(" &bull;" & ex.Message & "<BR/>")
            errorline.AppendLine(" &bull;MsBentukBidangUsaha  : Line " & DTR("NO") & "<BR/>")
            LogError(ex)
        End Try
    End Sub

#End Region

#Region "Upload File"

    Public Function SaveFileImportToSave() As String
        Dim filePath As String
        Dim FileNameToGetData As String = ""
        Try
            If FileUploadKPI.PostedFile.ContentLength = 0 Then
                Return FileNameToGetData
            End If

            If Not FileUploadKPI.HasFile Then
                Return FileNameToGetData
            End If

            filePath = Path.Combine(Server.MapPath("Upload"), FileUploadKPI.FileName)

            FileNameToGetData = FileUploadKPI.FileName
            If System.IO.File.Exists(filePath) Then
                Dim bexist As Boolean = True
                Dim i As Integer = 1
                While bexist
                    filePath = Path.Combine(Server.MapPath("Upload"), FileUploadKPI.FileName.Split(CChar("."))(0) & "-" & i.ToString & "." & FileUploadKPI.FileName.Split(CChar("."))(1))
                    FileNameToGetData = FileUploadKPI.FileName.Split(CChar("."))(0) & "-" & i.ToString & "." & Me.FileUploadKPI.FileName.Split(CChar("."))(1)
                    If Not System.IO.File.Exists(filePath) Then
                        bexist = False
                        Exit While
                    End If
                    i = i + 1
                End While
            End If
            Me.FileUploadKPI.PostedFile.SaveAs(filePath)
            Session("Filepath") = filePath
            Return FileNameToGetData
        Catch
            Throw
        End Try
    End Function

    ReadOnly Property FilePath() As String
        Get
            Return Session("FilePath")
        End Get
    End Property

    Private Sub ProcessExcelFile(ByVal FullPathFileNameToExportExcel As String)

        Using stream As FileStream = File.Open(FullPathFileNameToExportExcel, FileMode.Open, FileAccess.Read)
            Using excelReader As Excel.IExcelDataReader = ExcelReaderFactory.CreateBinaryReader(stream)
                excelReader.IsFirstRowAsColumnNames = True
                DSexcelTemp = excelReader.AsDataSet()
                If DSexcelTemp.Tables("MsBentukBidangUsaha").Columns(0).ColumnName Like "*Column*" Then
                    DSexcelTemp.Tables("MsBentukBidangUsaha").Rows.Clear()
                End If

            End Using
        End Using

    End Sub


#End Region

#Region "Function..."

    Function getErrorNo(ByVal Id As String, ByVal nama As String) As String
        Dim temp As String = ""
        Try
            Dim DV As New DataView(DT_AllMsBentukBidangUsahaNCBS)
            'jika Null maka harus is null bukan kosong disini belum di handle
            DV.RowFilter = "IdBentukBidangUsaha = '" & Id & "' and BentukBidangUsaha = '" & nama & "'"

            Dim DT As Data.DataTable = DV.ToTable
            If DT.Rows.Count > 0 Then
                temp = Safe(DT.Rows(0)("NO"))
            End If
        Catch ex As Exception
            temp = ex.Message
        End Try

        Return temp
    End Function
    ''' <summary>
    ''' Merubah excel ke Object Databale sekaligus memecahnya
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ImportExcelTempToDTproperty()
        Dim DT As New Data.DataTable
        'DT_MsBentukBidangUsaha = DSexcelTemp.Tables("MsBentukBidangUsaha")
        DT = DSexcelTemp.Tables("MsBentukBidangUsaha")
        Dim DV As New DataView(DT)
        DT_AllMsBentukBidangUsaha = DV.ToTable(True, "IdBentukBidangUsaha", "BentukBidangUsaha", "activation")
        DT_AllMsBentukBidangUsahaNCBS = DSexcelTemp.Tables("MsBentukBidangUsaha")
        DT_AllMsBentukBidangUsaha.Columns.Add("NO")
        For Each i As DataRow In DT_AllMsBentukBidangUsaha.Rows
            Dim PK As String = Safe(i("IdBentukBidangUsaha"))
            Dim nama As String = Safe(i("BentukBidangUsaha"))
            i("NO") = getErrorNo(PK, nama)
        Next
    End Sub

    Private Sub ClearThisPageSessions()
        DT_AllMsBentukBidangUsaha = Nothing
        DT_AllMsBentukBidangUsahaNCBS = Nothing
        GroupTableSuccessData = Nothing
        GroupTableAnomalyData = Nothing
        DSexcelTemp = Nothing
        SetnGetDataTableForAnomalyData = Nothing
        SetnGetDataTableForSuccessData = Nothing
    End Sub


    Function ConvertDTtoMsBentukBidangUsahaNCBS(ByVal DT As Data.DataTable) As TList(Of MsBentukBidangUsahaNCBS_ApprovalDetail)
        Dim temp As New TList(Of MsBentukBidangUsahaNCBS_ApprovalDetail)
        For Each DTR As DataRow In DT.Rows
            Using MsBentukBidangUsahaNCBS As New MsBentukBidangUsahaNCBS_ApprovalDetail
                With MsBentukBidangUsahaNCBS
                    FillOrNothing(.PK_BentukBidangUsahaNCBS_ID, DTR("IdBentukBidangusahaBank"), True, oInt)
                    FillOrNothing(.BentukBidangUsahaNCBS, DTR("BentukBidangUsahaBank"), True, oString)
                    FillOrNothing(.Activation, 1, True, oBit)
                    FillOrNothing(.CreatedBy, GET_UserName, True, oString)
                    FillOrNothing(.CreatedDate, Now, True, oDate)
                    temp.Add(MsBentukBidangUsahaNCBS)
                End With
            End Using

        Next

        Return temp
    End Function



    Function ConvertDTtoMsBentukBidangUsaha(ByVal DTR As DataRow) As MsBentukBidangUsaha_ApprovalDetail
        Dim temp As New MsBentukBidangUsaha_ApprovalDetail
        Using MsBentukBidangUsaha As New MsBentukBidangUsaha_ApprovalDetail()
            With MsBentukBidangUsaha
                .IdBentukBidangUsaha = DTR("IdBentukBidangUsaha").ToString
                .BentukBidangUsaha = DTR("BentukBidangUsaha").ToString
                FillOrNothing(.Activation, DTR("Activation"), True, oBit)
                FillOrNothing(.CreatedBy, GET_UserName, True, oString)
                FillOrNothing(.CreatedDate, Now, True, oDate)
            End With
            temp = MsBentukBidangUsaha
        End Using

        Return temp
    End Function

    Function ConvertDTtoMsBentukBidangUsaha(ByVal DT As Data.DataTable) As TList(Of MsBentukBidangUsaha_ApprovalDetail)
        Dim temp As New TList(Of MsBentukBidangUsaha_ApprovalDetail)
        For Each DTR As DataRow In DT.Rows
            Using MsBentukBidangUsaha As New MsBentukBidangUsaha_ApprovalDetail()
                With MsBentukBidangUsaha
                    .IdBentukBidangUsaha = DTR("IdBentukBidangUsaha").ToString
                    .BentukBidangUsaha = DTR("BentukBidangUsaha").ToString
                    FillOrNothing(.Activation, DTR("Activation"), True, oBit)
                    FillOrNothing(.CreatedBy, GET_UserName, True, oString)
                    FillOrNothing(.CreatedDate, Now, True, oDate)
                    temp.Add(MsBentukBidangUsaha)
                End With
            End Using
        Next
        Return temp
    End Function

    Private Sub ChangeMultiView(ByVal index As Integer)
        MultiViewKPIOvertimeUpload.ActiveViewIndex = index
        If index = 0 Then
            Imgbtnsave.Visible = False
            Imgbtncancel.Visible = False
        ElseIf index = 1 Then
            Imgbtncancel.Visible = True
        ElseIf index = 2 Then
            Imgbtncancel.Visible = False
            Imgbtnsave.Visible = False
        End If
    End Sub

    Private Function GenerateGroupingMsBentukBidangUsaha() As List(Of AMLBLL.MsBentukBidangUsahaBll.DT_Group_MsBentukBidangUsaha)
        Dim Ltemp As New List(Of AMLBLL.MsBentukBidangUsahaBll.DT_Group_MsBentukBidangUsaha)
        For i As Integer = 0 To DT_AllMsBentukBidangUsaha.Rows.Count - 1
            Dim MsBentukBidangUsaha As New AMLBLL.MsBentukBidangUsahaBll.DT_Group_MsBentukBidangUsaha
            With MsBentukBidangUsaha
                .MsBentukBidangUsaha = DT_AllMsBentukBidangUsaha.Rows(i)
                .L_MsBentukBidangUsahaNCBS = DT_AllMsBentukBidangUsahaNCBS.Clone

                'Insert MsBentukBidangUsahaNCBS
                Dim MsBentukBidangUsaha_ID As String = Safe(.MsBentukBidangUsaha("IdBentukBidangUsaha"))
                Dim MsBentukBidangUsaha_nama As String = Safe(.MsBentukBidangUsaha("BentukBidangUsaha"))
                Dim MsBentukBidangUsaha_Activation As String = Safe(.MsBentukBidangUsaha("Activation"))
                If DT_AllMsBentukBidangUsahaNCBS.Rows.Count > 0 Then
                    Using DV As New DataView(DT_AllMsBentukBidangUsahaNCBS)
                        DV.RowFilter = "IdBentukBidangUsaha='" & MsBentukBidangUsaha_ID & "' and " & _
                                                  "BentukBidangUsaha='" & MsBentukBidangUsaha_nama & "' and " & _
                                                  "activation='" & MsBentukBidangUsaha_Activation & "'"
                        If DV.Count > 0 Then
                            Dim temp As Data.DataTable = DV.ToTable
                            For Each H As DataRow In temp.Rows
                                .L_MsBentukBidangUsahaNCBS.ImportRow(H)
                            Next
                        End If
                    End Using
                End If




            End With
            Ltemp.Add(MsBentukBidangUsaha)
        Next
        Return Ltemp
    End Function

    Public Sub BindGrid()
        lblValueFailedData.Text = SetnGetDataTableForAnomalyData.Rows.Count
        GridAnomalyList.DataSource = SetnGetDataTableForAnomalyData
        GridAnomalyList.DataBind()
        lblValueSuccessData.Text = SetnGetDataTableForSuccessData.Count
        GridSuccessList.DataSource = SetnGetDataTableForSuccessData
        lblValueuploadeddata.Text = DT_AllMsBentukBidangUsaha.Rows.Count
        GridSuccessList.DataBind()
    End Sub

#End Region

#Region "events..."


    Protected Sub ImgBtnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imgbtncancel.Click
        ClearThisPageSessions()
        ChangeMultiView(0)
        LinkButtonExportExcel.Visible = True
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Try
            If IsPostBack Then
                Return
            End If

            ClearThisPageSessions()
            SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
            ChangeMultiView(0)
        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub ImgBtnAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAdd.Click
        ChangeMultiView(0)
    End Sub

    Protected Sub GridAnomalyList_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles GridAnomalyList.PageIndexChanged
        GridAnomalyList.CurrentPageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub GridSuccessList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridSuccessList.ItemDataBound
        Try
            Dim CollCount As Integer = e.Item.Cells.Count - 1
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then

                'Show mapping item
                Dim StringMaping As New StringBuilder
                Dim DV As New DataView(DT_AllMsBentukBidangUsahaNCBS)
                Dim DT As New Data.DataTable
                DV.RowFilter = "IdBentukBidangUsaha='" & e.Item.Cells(0).Text & "' and " & "BentukBidangUsaha='" & e.Item.Cells(1).Text & "'"
                DT = DV.ToTable
                If DT.Rows.Count > 0 Then
                    Dim countField As Integer = 7
                    Dim lengthField As Integer = 0
                    For Each idx As DataRow In DT.Rows
                        With idx
                            If lengthField > countField Then
                                StringMaping.AppendLine(idx("BentukBidangUsahaBank") & "(" & idx("IdBentukBidangusahaBank") & ");")
                                lengthField = 0
                            Else
                                StringMaping.AppendLine(idx("BentukBidangUsahaBank") & "(" & idx("IdBentukBidangusahaBank") & ");")
                                lengthField += 1
                            End If

                        End With
                    Next
                Else
                    StringMaping.Append("-")
                End If
                e.Item.Cells(CollCount).Text = StringMaping.ToString

            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub GridSuccessList_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles GridSuccessList.PageIndexChanged
        Dim GridTarget As DataGrid = CType(source, DataGrid)
        Try
            GridSuccessList.CurrentPageIndex = e.NewPageIndex
            BindGrid()
        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgSaveSuccessData_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imgbtnsave.Click
        Try
            'File belum diupload
            If DT_AllMsBentukBidangUsaha Is Nothing Then
                Throw New Exception("Load data failed")
            End If

            'Save MsBentukBidangUsaha Approval (Header)
            Dim KeyApp As String
            Using Aproval As New MsBentukBidangUsaha_Approval()
                With Aproval
                    .RequestedBy = GET_PKUserID
                    .RequestedDate = Date.Now
                    .FK_MsMode_Id = 2 'Edit
                    .IsUpload = True
                End With
                DataRepository.MsBentukBidangUsaha_ApprovalProvider.Save(Aproval)
                'ambil key approval
                KeyApp = Aproval.PK_MsBentukBidangUsaha_Approval_Id
            End Using

            For k As Integer = 0 To GroupTableSuccessData.Count - 1

                Dim MsBentukBidangUsaha As GrouP_MsBentukBidangUsaha_success = GroupTableSuccessData(k)


                With MsBentukBidangUsaha
                    'save MsBentukBidangUsaha approval detil
                    .MsBentukBidangUsaha.FK_MsBentukBidangUsaha_Approval_Id = KeyApp
                    DataRepository.MsBentukBidangUsaha_ApprovalDetailProvider.Save(.MsBentukBidangUsaha)
                    'ambil key MsBentukBidangUsaha approval detil
                    Dim IdMsBentukBidangUsaha As String = .MsBentukBidangUsaha.IdBentukBidangUsaha
                    Dim L_MappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail As New TList(Of MappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail)
                    'save mapping dan NCBS
                    For Each OMsBentukBidangUsahaNCBS_ApprovalDetail As MsBentukBidangUsahaNCBS_ApprovalDetail In .L_MsBentukBidangUsahaNCBS
                        OMsBentukBidangUsahaNCBS_ApprovalDetail.PK_MsBentukBidangUsaha_Approval_Id = KeyApp
                        Using OMappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail As New MappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail
                            With OMappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail
                                FillOrNothing(.IdBentukBidangUsahaNCBS, OMsBentukBidangUsahaNCBS_ApprovalDetail.PK_BentukBidangUsahaNCBS_ID, True, oInt)
                                FillOrNothing(.IdBentukBidangUsaha, IdMsBentukBidangUsaha, True, oInt)
                                FillOrNothing(.PK_MsBentukBidangUsaha_Approval_Id, KeyApp, True, oInt)
                                FillOrNothing(.Nama, OMsBentukBidangUsahaNCBS_ApprovalDetail.BentukBidangUsahaNCBS, True, oString)
                            End With
                            L_MappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail.Add(OMappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail)
                        End Using
                    Next
                    'save maping dan NCBS
                    DataRepository.MappingMsBentukBidangUsahaNCBSPPATK_Approval_DetailProvider.Save(L_MappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail)
                    DataRepository.MsBentukBidangUsahaNCBS_ApprovalDetailProvider.Save(.L_MsBentukBidangUsahaNCBS)

                End With
            Next

            LblConfirmation.Text = "MsBentukBidangUsaha data has edited (" & SetnGetDataTableForSuccessData.Count & " Rows / MsBentukBidangUsaha)"
            ChangeMultiView(2)

        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgUpload_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgUpload.Click
        Dim CombinedFileName As String
        GroupTableSuccessData = New List(Of GrouP_MsBentukBidangUsaha_success)
        GroupTableAnomalyData = New List(Of AMLBLL.MsBentukBidangUsahaBll.DT_Group_MsBentukBidangUsaha)
        Try
            'validasi File
            If FileUploadKPI.FileName = "" Then
                Throw New Exception("Data required")
            End If

            If Path.GetExtension(FileUploadKPI.FileName) <> ".xls" Then
                Throw New Exception("Invalid File Upload Extension")
            End If

            CombinedFileName = SaveFileImportToSave()
            If String.Compare(CombinedFileName, "", False) <> 0 Then
                Try
                    ProcessExcelFile(FilePath)
                Catch ex As Exception
                    Throw New Exception("Invalid File Format")
                End Try
            Else
                Throw New Exception("Invalid File Format")
            End If

            'Import from Excell to property Global
            ImportExcelTempToDTproperty()

            Dim LG_MsBentukBidangUsaha As List(Of AMLBLL.MsBentukBidangUsahaBll.DT_Group_MsBentukBidangUsaha) = GenerateGroupingMsBentukBidangUsaha()
            'tambahkan kolom untuk tampilan di grid anomaly
            DT_AllMsBentukBidangUsaha.Columns.Add("Description")
            DT_AllMsBentukBidangUsaha.Columns.Add("errorline")

            'Membuat tempory untuk yang anomali dan yang sukses
            Dim DTnormal As Data.DataTable = DT_AllMsBentukBidangUsaha.Clone
            Dim DTAnomaly As Data.DataTable = DT_AllMsBentukBidangUsaha.Clone

            'proses pemilahan data anomaly dan data sukses melalui validasi 
            For i As Integer = 0 To LG_MsBentukBidangUsaha.Count - 1
                Dim Lerror As List(Of String) = ValidateInput(LG_MsBentukBidangUsaha(i))
                Dim MSG As String = Lerror(0)
                Dim LINE As String = Lerror(1)
                If MSG = "" Then
                    DTnormal.ImportRow(LG_MsBentukBidangUsaha(i).MsBentukBidangUsaha)
                    Dim GroupMsBentukBidangUsaha As New GrouP_MsBentukBidangUsaha_success
                    With GroupMsBentukBidangUsaha
                        'konversi dari datatable ke Object Nettier
                        .MsBentukBidangUsaha = ConvertDTtoMsBentukBidangUsaha(LG_MsBentukBidangUsaha(i).MsBentukBidangUsaha)
                        .L_MsBentukBidangUsahaNCBS = ConvertDTtoMsBentukBidangUsahaNCBS(LG_MsBentukBidangUsaha(i).L_MsBentukBidangUsahaNCBS)
                    End With
                    'masukan ke group sukses
                    GroupTableSuccessData.Add(GroupMsBentukBidangUsaha)
                Else
                    DT_AllMsBentukBidangUsaha.Rows(i)("Description") = MSG
                    DT_AllMsBentukBidangUsaha.Rows(i)("errorline") = LINE
                    DTAnomaly.ImportRow(DT_AllMsBentukBidangUsaha.Rows(i))
                    GroupTableAnomalyData.Add(LG_MsBentukBidangUsaha(i))
                End If

            Next

            SetnGetDataTableForSuccessData = ConvertDTtoMsBentukBidangUsaha(DTnormal)
            SetnGetDataTableForAnomalyData = DTAnomaly

            If DTAnomaly.Rows.Count > 0 Then
                Imgbtnsave.Visible = False
                Imgbtncancel.Visible = True
            Else
                Imgbtnsave.Visible = True
                Imgbtncancel.Visible = True
            End If

            BindGrid()
            If GridAnomalyList.Items.Count < 1 Then
                LinkButtonExportExcel.Visible = False
            Else
                LinkButtonExportExcel.Visible = True
            End If
            ChangeMultiView(1)
        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

#End Region

    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Dim Exp As New AMLBLL.MsBentukBidangUsahaBll
            Dim FolderPath As String = Server.MapPath("FolderTemplate")

            Dim buffer As Byte() = Exp.ExportDTtoExcel(GroupTableAnomalyData, FolderPath)
            Response.AddHeader("Content-type", "application/vnd.ms-excel")
            Response.AddHeader("Content-Disposition", "attachment; filename=MsBentukBidangUsahaUpload.xls")
            Response.BinaryWrite(buffer)
            Response.Flush()
            Response.End()

        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

        ' Verifies that the control is rendered 

    End Sub

    Protected Sub GridSuccessList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridSuccessList.SelectedIndexChanged

    End Sub
End Class





