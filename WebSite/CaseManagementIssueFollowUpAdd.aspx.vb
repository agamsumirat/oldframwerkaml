
Partial Class CaseManagementIssueFollowUpAdd
    Inherits Parent
    Private ReadOnly Property PK_MapCaseManagementIssueID() As String
        Get
            Try
                'cek apakah user yang sekarang sama dengan user yang membuat issue
                'jika sama maka redirect karena tidak berhak mengisi action issue yang dia buat sendiri
                Return Request.Params("PK_MapCaseManagementIssueID")
                Using adapter As New AMLDAL.CaseManagementTableAdapters.MapCaseManagementIssueTableAdapter
                    Using otable As AMLDAL.CaseManagement.MapCaseManagementIssueDataTable = adapter.GetMapCaseManagementIssueByPK(Request.Params("PK_MapCaseManagementIssueID"))
                        If otable.Rows.Count > 0 Then
                            If Not CType(otable.Rows(0), AMLDAL.CaseManagement.MapCaseManagementIssueRow).IsFK_IssueByIDNull Then
                                If CType(otable.Rows(0), AMLDAL.CaseManagement.MapCaseManagementIssueRow).FK_IssueByID = Sahassa.AML.Commonly.SessionPkUserId Then
                                    Response.Redirect("CaseManagementViewDetail.aspx?PK_CaseManagementID=" & Me.PK_MapCaseManagementID & "&SelectedIndex=2", False)
                                End If
                            End If
                        End If
                    End Using
                End Using

            Catch ex As Exception
                LogError(ex)
                Throw
            End Try
            
        End Get
    End Property
    Private ReadOnly Property PK_MapCaseManagementID() As String
        Get
            Return Request.Params("PK_MapCaseManagementID")

        End Get
    End Property

    Private _oRowCaseManagementIssue As AMLDAL.CaseManagement.MapCaseManagementIssueRow
    Private ReadOnly Property oRowCaseManagementIssue() As AMLDAL.CaseManagement.MapCaseManagementIssueRow
        Get
            If Not _oRowCaseManagementIssue Is Nothing Then
                Return _oRowCaseManagementIssue
            Else
                Using adapter As New AMLDAL.CaseManagementTableAdapters.MapCaseManagementIssueTableAdapter
                    Using otable As AMLDAL.CaseManagement.MapCaseManagementIssueDataTable = adapter.GetMapCaseManagementIssueByPK(Me.PK_MapCaseManagementIssueID)
                        If otable.Rows.Count > 0 Then
                            _oRowCaseManagementIssue = CType(otable.Rows(0), AMLDAL.CaseManagement.MapCaseManagementIssueRow)
                            Return _oRowCaseManagementIssue
                        Else
                            Return Nothing
                        End If
                    End Using
                End Using
            End If
        End Get
    End Property

    Private Sub LoadData()
        Try
            If Not oRowCaseManagementIssue Is Nothing Then
                If Not oRowCaseManagementIssue.IsIssueTitleNull Then
                    Me.LabelIssueTitle.Text = oRowCaseManagementIssue.IssueTitle
                End If
                If Not oRowCaseManagementIssue.IsDescriptionNull Then
                    Me.LabelDescription.Text = oRowCaseManagementIssue.Description
                End If
                If Not oRowCaseManagementIssue.IsIssueStatusNull Then
                    Me.LabelStatus.Text = oRowCaseManagementIssue.IssueStatus
                End If
                If Not oRowCaseManagementIssue.IsFK_IssueStatusIDNull Then
                    Me.ImageButtonAddAction.Visible = (oRowCaseManagementIssue.FK_IssueStatusID <> 2)
                End If
            End If
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)


            End Using
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        LoadData()
    End Sub

    Protected Sub GridViewAction_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridViewAction.RowDataBound
        'Try
        '    If e.Row.RowType = DataControlRowType.DataRow And GridViewAction.EditIndex <> e.Row.RowIndex Then
        '        If CType(e.Row.FindControl("LabelUserID"), Label).Text = Sahassa.AML.Commonly.SessionUserId Then
        '            CType(e.Row.FindControl("LinkEdit"), LinkButton).Visible = True
        '            CType(e.Row.FindControl("LinkDelete"), LinkButton).Visible = True
        '        Else
        '            CType(e.Row.FindControl("LinkEdit"), LinkButton).Visible = False
        '            CType(e.Row.FindControl("LinkDelete"), LinkButton).Visible = False
        '        End If
        '    End If
        'Catch ex As Exception
        '    LogError(ex)
        '    cvalPageError.IsValid = False
        '    cvalPageError.ErrorMessage = ex.Message
        'End Try
    End Sub

    Protected Sub GridViewAction_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GridViewAction.RowDeleting
        Try
            SqlDataSource1.DeleteParameters("PK_MapCaseManagementIssueFollowUp").DefaultValue = e.Keys(0).ToString

        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
    Protected Sub GridViewAction_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles GridViewAction.RowUpdating
        Dim StrAction As String
        Try
            StrAction = CType(GridViewAction.Rows(e.RowIndex).FindControl("textActiongrid"), TextBox).Text

            SqlDataSource1.UpdateParameters("Action").DefaultValue = StrAction
            SqlDataSource1.UpdateParameters("CreatedDate").DefaultValue = Now
            SqlDataSource1.UpdateParameters("PK_MapCaseManagementIssueFollowUp").DefaultValue = e.Keys(0).ToString
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub
    Protected Sub ImageButtonAddAction_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonAddAction.Click
        Try
            If Page.IsValid Then
                SqlDataSource1.InsertParameters("FK_MapCaseManagementIssueID").DefaultValue = Me.PK_MapCaseManagementIssueID
                SqlDataSource1.InsertParameters("Action").DefaultValue = Me.TextAction.Text.Trim
                SqlDataSource1.InsertParameters("CreatedDate").DefaultValue = Now
                SqlDataSource1.InsertParameters("FK_UserID").DefaultValue = Sahassa.AML.Commonly.SessionPkUserId
                SqlDataSource1.Insert()
            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImageBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click
        Response.Redirect("CaseManagementViewDetail.aspx?PK_CaseManagementID=" & Me.PK_MapCaseManagementID & "&SelectedIndex=2", False)
    End Sub
End Class
