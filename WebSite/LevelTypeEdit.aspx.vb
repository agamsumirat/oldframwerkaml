Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Partial Class LevelTypeEdit
    Inherits Parent

    ''' <summary>
    ''' get user id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetLevelTypeId() As String
        Get
            Return Me.Request.Params("LevelTypeID")
        End Get
    End Property

    ''' <summary>
    ''' cancel handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "LevelTypeView.aspx"

        Me.Response.Redirect("LevelTypeView.aspx", False)
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub UpdateLevelTypeBySU()
        Try
            Dim LevelTypeDescription As String
            If Me.TextLevelTypeDescription.Text.Length <= 255 Then
                LevelTypeDescription = Me.TextLevelTypeDescription.Text
            Else
                LevelTypeDescription = Me.TextLevelTypeDescription.Text.Substring(0, 255)
            End If

            Using AccessLevelType As New AMLDAL.AMLDataSetTableAdapters.LevelTypeTableAdapter
                AccessLevelType.UpdateLevelType(Me.GetLevelTypeId, Me.TextLevelTypeName.Text, LevelTypeDescription)
            End Using
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub InsertAuditTrail()
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(4)
            Using AccessLevelType As New AMLDAL.AMLDataSetTableAdapters.LevelTypeTableAdapter
                Using dt As Data.DataTable = AccessLevelType.GetDataByLevelTypeID(CInt(Me.GetLevelTypeId))
                    Dim row As AMLDAL.AMLDataSet.LevelTypeRow = dt.Rows(0)

                    Dim LevelTypeDescription As String
                    If Me.TextLevelTypeDescription.Text.Length <= 255 Then
                        LevelTypeDescription = Me.TextLevelTypeDescription.Text
                    Else
                        LevelTypeDescription = Me.TextLevelTypeDescription.Text.Substring(0, 255)
                    End If

                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "LevelType", "LevelTypeID", "Edit", row.LevelTypeID, row.LevelTypeID, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "LevelType", "LevelTypeName", "Edit", row.LevelTypeName, Me.TextLevelTypeName.Text, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "LevelType", "LevelTypeDesc", "Edit", row.LevelTypeDesc, LevelTypeDescription, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "LevelType", "LevelTypeCreatedDate", "Edit", row.LevelTypeCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), row.LevelTypeCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                    End Using
                End Using
            End Using
        Catch
            Throw
        End Try
    End Sub

    ''' <summary>
    ''' update handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageUpdate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageUpdate.Click
        Dim oSQLTrans As SqlTransaction = Nothing
        Page.Validate()

        If Page.IsValid Then
            Try
                'Buat variabel untuk menampung nilai-nilai baru
                Dim LevelTypeID As String = Me.LabelTypeID.Text
                Dim LevelTypeName As String = Trim(Me.TextLevelTypeName.Text)

                'Jika tidak ada perubahan LevelTypeName
                If LevelTypeName = ViewState("LevelTypeName_Old") Then
                    GoTo Add
                Else 'Jika ada perubahan LevelTypeName 

                    Dim counter As Int32
                    'Periksa apakah LevelTypeName yg baru tsb sdh ada dalam tabel LevelType atau belum
                    Using AccessLevelType As New AMLDAL.AMLDataSetTableAdapters.LevelTypeTableAdapter
                        counter = AccessLevelType.CountMatchingLevelType(LevelTypeName)
                    End Using

                    'Counter = 0 berarti LevelTypeName tersebut belum pernah ada dalam tabel LevelType
                    If counter = 0 Then
Add:
                        'Periksa apakah LevelTypeName tsb dalam status pending approval atau tidak
                        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.LevelType_ApprovalTableAdapter
                            counter = AccessPending.CountLevelTypeApprovalByLevelTypeName(LevelTypeName)

                            'Counter = 0 berarti LevelTypeName yg baru tsb tidak dalam status pending approval
                            If counter = 0 Then
                                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                                    Me.InsertAuditTrail()
                                    Me.UpdateLevelTypeBySU()
                                    Me.LblSuccess.Text = "Success to Update Level Type."
                                    Me.LblSuccess.Visible = True
                                Else
                                    Dim LevelTypeDescription As String
                                    If Me.TextLevelTypeDescription.Text.Length <= 255 Then
                                        LevelTypeDescription = Me.TextLevelTypeDescription.Text
                                    Else
                                        LevelTypeDescription = Me.TextLevelTypeDescription.Text.Substring(0, 255)
                                    End If

                                    'Buat variabel untuk menampung nilai-nilai lama
                                    Dim LevelTypeID_Old As Int32 = ViewState("LevelTypeID_Old")
                                    Dim LevelTypeName_Old As String = ViewState("LevelTypeName_Old")
                                    Dim LevelTypeDescription_Old As String = ViewState("LevelTypeDescription_Old")
                                    Dim LevelTypeCreatedDate_Old As DateTime = ViewState("LevelTypeCreatedDate_Old")

                                    'variabel untuk menampung nilai identity dari tabel LevelTypePendingApprovalID
                                    Dim LevelTypePendingApprovalID As Int64

                                    'Using TransScope As New Transactions.TransactionScope()
                                    Using AccessLevelTypePendingApproval As New AMLDAL.AMLDataSetTableAdapters.LevelType_PendingApprovalTableAdapter
                                        oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessLevelTypePendingApproval, Data.IsolationLevel.ReadUncommitted)
                                        'Tambahkan ke dalam tabel LevelType_PendingApproval dengan ModeID = 2 (Edit) 
                                        LevelTypePendingApprovalID = AccessLevelTypePendingApproval.InsertLevelType_PendingApproval(Sahassa.AML.Commonly.SessionUserId, Now, "Level Type Edit", 2)

                                        'Tambahkan ke dalam tabel LevelType_Approval dengan ModeID = 2 (Edit) 
                                        AccessPending.Insert(LevelTypePendingApprovalID, 2, LevelTypeID, LevelTypeName, LevelTypeDescription, Now, LevelTypeID_Old, LevelTypeName_Old, LevelTypeDescription_Old, LevelTypeCreatedDate_Old)

                                        oSQLTrans.Commit()
                                    End Using
                                    'End Using

                                    Dim MessagePendingID As Integer = 8402 'MessagePendingID 8402 = LevelType Edit 

                                    Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & LevelTypeName_Old

                                    Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & LevelTypeName_Old, False)
                                End If
                            Else  'Counter != 0 berarti LevelTypeName tersebut dalam status pending approval
                                Throw New Exception("Cannot edit the following Level Type: '" & LevelTypeName & "' because it is currently waiting for approval")
                            End If
                        End Using
                    Else 'Counter != 0 berarti LevelTypeName tersebut sudah ada dalam tabel LevelType
                        Throw New Exception("Cannot change to the following Level Type Name: '" & LevelTypeName & "' because that Level Type Name already exists in the database.")
                    End If
                End If
            Catch ex As Exception
                If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
                Me.cvalPageError.IsValid = False
                Me.cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            Finally
                If Not oSQLTrans Is Nothing Then
                    oSQLTrans.Dispose()
                    oSQLTrans = Nothing
                End If

            End Try
        End If
    End Sub

    ''' <summary>
    ''' filledit data
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillEditData()
        Try
            Using AccessLevelType As New AMLDAL.AMLDataSetTableAdapters.LevelTypeTableAdapter
                Using TableLevelType As AMLDAL.AMLDataSet.LevelTypeDataTable = AccessLevelType.GetDataByLevelTypeID(Me.GetLevelTypeId)
                    If TableLevelType.Rows.Count > 0 Then
                        Dim TableRowLevelType As AMLDAL.AMLDataSet.LevelTypeRow = TableLevelType.Rows(0)

                        ViewState("LevelTypeID_Old") = TableRowLevelType.LevelTypeID
                        Me.LabelTypeID.Text = ViewState("LevelTypeID_Old")

                        ViewState("LevelTypeName_Old") = TableRowLevelType.LevelTypeName
                        Me.TextLevelTypeName.Text = ViewState("LevelTypeName_Old")

                        ViewState("LevelTypeDescription_Old") = TableRowLevelType.LevelTypeDesc
                        Me.TextLevelTypeDescription.Text = ViewState("LevelTypeDescription_Old")

                        ViewState("LevelTypeCreatedDate_Old") = TableRowLevelType.LevelTypeCreatedDate
                    End If
                End Using
            End Using
        Catch
            Throw
        End Try
    End Sub


    ''' <summary>
    ''' page load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.FillEditData()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    '    Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class