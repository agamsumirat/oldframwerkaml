Imports System.Data.SqlClient
Partial Class AdvancedRulesApprovalDetail  
    Inherits Parent

#Region " Property "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get confirm type
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property ParamType() As Integer
        Get
            Return Me.Request.Params("TypeConfirm")
        End Get
    End Property

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get DataUpdatingParameterApprovalID
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    15/05/2007 Modified
    '''  [Hendra] 12:24 PM 10/7/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property AdvancedRulesPendingApprovalID() As Int64
        Get
            Dim Temp As String = String.Empty
            Temp = Me.Request.Params("AdvancedRulesPendingApprovalID")
            If Temp = "" OrElse Not IsNumeric(Temp) Then
                Throw New Exception("Pending Approval ID not valid or already deleted")
                'cek id yang direquest masih ada atau tidak di database kalau tidak ada maka throw error
            ElseIf IsNumeric(Temp) Then
                Using adapter As New AMLDAL.RulesAdvancedTableAdapters.RulesAdvancedPendingApprovalTableAdapter
                    Dim orows() As AMLDAL.RulesAdvanced.RulesAdvancedPendingApprovalRow = adapter.GetDataByPK(Temp).Select
                    If orows.Length = 0 Then
                        Throw New Exception("Pending Approval ID not exist  or already deleted")
                    Else
                        Return Temp
                    End If
                End Using
            End If
        End Get
    End Property

   

    ''' <summary>
    ''' get user id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetUserID() As String
        Get
            Using AccessPending As New AMLDAL.RulesAdvancedTableAdapters.RulesAdvancedPendingApprovalTableAdapter
                Dim orowPending() As AMLDAL.RulesAdvanced.RulesAdvancedPendingApprovalRow = AccessPending.GetDataByPK(Me.AdvancedRulesPendingApprovalID).Select
                If orowPending.Length > 0 Then
                    Return orowPending(0).RulesAdvancedUserID
                Else
                    Return ""
                End If
            End Using
        End Get
    End Property
    Private ReadOnly Property GetDetailAdvancedRules() As AMLDAL.RulesAdvanced.RulesAdvancedApprovalRow
        Get

            If Session("AdvancedRulesApprovalDetailRow") Is Nothing OrElse CType(Session("AdvancedRulesApprovalDetailRow"), AMLDAL.RulesAdvanced.RulesAdvancedApprovalRow).FK_RuleAdvancedPendingApprovalID <> AdvancedRulesPendingApprovalID Then
                Using adapter As New AMLDAL.RulesAdvancedTableAdapters.RulesAdvancedApprovalTableAdapter

                    Dim objDatarow As AMLDAL.RulesAdvanced.RulesAdvancedApprovalRow = adapter.GetDataByPendingApprovalID(Me.AdvancedRulesPendingApprovalID).Rows(0)
                    If Not objDatarow Is Nothing Then
                        Session("AdvancedRulesApprovalDetailRow") = objDatarow
                        Return Session("AdvancedRulesApprovalDetailRow")
                    Else
                        Throw New Exception("Pending Approval ID not exist or already deleted")
                    End If
                End Using
            ElseIf CType(Session("AdvancedRulesApprovalDetailRow"), AMLDAL.RulesAdvanced.RulesAdvancedApprovalRow).FK_RuleAdvancedPendingApprovalID = AdvancedRulesPendingApprovalID Then

                Return Session("AdvancedRulesApprovalDetailRow")
            End If

        End Get
    End Property
#End Region

    Private Sub AbandonSession()
        Session("AdvancedRulesApprovalDetailRow") = Nothing
    End Sub
#Region " Load "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' hide all
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    15/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub HandleTampilan()

        Select Case Me.ParamType
            Case TypeConfirm.Add

                Me.TableDetailAdd.Visible = True
                Me.TableDetailEdit.Visible = False
                Me.TableDetailDelete.Visible = False
            Case TypeConfirm.Edit
                Me.TableDetailAdd.Visible = False
                Me.TableDetailEdit.Visible = True
                Me.TableDetailDelete.Visible = False
            Case TypeConfirm.Delete
                Me.TableDetailAdd.Visible = False
                Me.TableDetailEdit.Visible = False
                Me.TableDetailDelete.Visible = True
            Case Else
                Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
        End Select


    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load LoadDataAdvancedRulesAdd
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Hendra] 1:04 PM 10/7/2007 created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub LoadDataAdvancedRulesAdd()
        Me.LabelTitle.Text = "Activity: Add Data Advanced Rules"
        If Not GetDetailAdvancedRules Is Nothing Then
            Me.LabelAdvancedRulesNameAdd.Text = GetDetailAdvancedRules.RulesAdvancedName
            Me.LabelDescriptionAdd.Text = GetDetailAdvancedRules.RulesAdvancedDescription
            Me.LabelEnabledAdd.Text = GetDetailAdvancedRules.Enabled
            Me.LabelCaseGroupingByAdd.Text = GetDetailAdvancedRules.CaseGroupingBy
            Me.LabelSQLExpressionAdd.Text = GetDetailAdvancedRules.Expression
        End If

    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load LoadDataAdvancedRulesEdit
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Hendra] 1:30 PM 10/7/2007
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub LoadDataAdvancedRulesEdit()
        Me.LabelTitle.Text = "Activity: Edit Data Advanced Rules"
        If Not GetDetailAdvancedRules Is Nothing Then
            Me.LabelAdvancedRulesNameEditOld.Text = GetDetailAdvancedRules.RulesAdvancedName_Old
            Me.LabelDescriptionEditOld.Text = GetDetailAdvancedRules.RulesAdvancedDescription_Old
            Me.LabelEnabledEditOld.Text = GetDetailAdvancedRules.Enabled_Old
            Me.LabelCaseGroupingByEditOld.Text = GetDetailAdvancedRules.CaseGroupingBy_Old
            Me.LabelSQLExpressionEditOld.Text = GetDetailAdvancedRules.Expression_Old
            Me.LabelAdvancedRulesNameEditNew.Text = GetDetailAdvancedRules.RulesAdvancedName
            Me.LabelDescriptionEditNew.Text = GetDetailAdvancedRules.RulesAdvancedDescription
            Me.LabelEnabledEditNew.Text = GetDetailAdvancedRules.Enabled
            Me.LabelCaseGroupingByEditNew.Text = GetDetailAdvancedRules.CaseGroupingBy
            Me.LabelSQLExpressionEditNew.Text = GetDetailAdvancedRules.Expression

        End If
    End Sub
    Private Sub LoadDataAdvancedRulesDelete()
        Me.LabelTitle.Text = "Activity: Delete Data Advanced Rules"
        If Not GetDetailAdvancedRules Is Nothing Then
            Me.LabelAdvancedRulesNameDelete.Text = GetDetailAdvancedRules.RulesAdvancedName
            Me.LabelDescriptionDelete.Text = GetDetailAdvancedRules.RulesAdvancedDescription
            Me.LabelEnabledDelete.Text = GetDetailAdvancedRules.Enabled
            Me.LabelCaseGroupingByDelete.Text = GetDetailAdvancedRules.CaseGroupingBy
            Me.LabelSQLExpressionDelete.Text = GetDetailAdvancedRules.Expression
        End If
    End Sub
#End Region

#Region " Accept "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' AcceptDataAdvancedRules add accept
    ''' </summary>
    ''' <remarks>
    ''' 1.insert data advanced rules ke table rulesadvance
    ''' 2.insert data ke audittrail
    ''' 3.delete data dari table rulesadvancedpendingapproval dan rulesadvanceapproval
    ''' 
    ''' </remarks>
    ''' <history>
    ''' 	[Hendra] 1:37 PM 10/7/2007
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptDataAdvancedRulesAdd()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try

            If Not Me.GetDetailAdvancedRules Is Nothing Then
                'Step 1


                Using adapter As New AMLDAL.RulesAdvancedTableAdapters.RulesAdvancedTableAdapter
                    oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(adapter)
                    adapter.Insert(GetDetailAdvancedRules.RulesAdvancedName, GetDetailAdvancedRules.RulesAdvancedDescription, GetDetailAdvancedRules.Enabled, GetDetailAdvancedRules.CaseGroupingBy, GetDetailAdvancedRules.Expression, Now, False)
                End Using
                'Step 2
                InsertAuditTrailAdd(oSQLTrans)
                'Step 3
                Using adapter As New AMLDAL.RulesAdvancedTableAdapters.RulesAdvancedApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(adapter, oSQLTrans)
                    adapter.DeleteRulesAdvancedByFKPendingID(Me.AdvancedRulesPendingApprovalID)
                End Using
                Using adapter As New AMLDAL.RulesAdvancedTableAdapters.RulesAdvancedPendingApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(adapter, oSQLTrans)
                    adapter.DeleteRulesAdvancedPendingApprovalByPK(Me.AdvancedRulesPendingApprovalID)
                End Using
                oSQLTrans.Commit()
                AbandonSession()
            End If
            Me.Response.Redirect("AdvancedRulesApproval.aspx", False)
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Rollback()

            End If

            LogError(ex)
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub
    Private Sub InsertAuditTrailAdd(ByRef osqltrans As SqlTransaction)
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(6)
            If Not Me.GetDetailAdvancedRules Is Nothing Then

                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, osqltrans)
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Advanced Rule Name", "Add", "", Me.GetDetailAdvancedRules.RulesAdvancedName, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Description", "Add", "", Me.GetDetailAdvancedRules.RulesAdvancedDescription, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Enabled", "Add", "", Me.GetDetailAdvancedRules.Enabled, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Case Grouping By", "Add", "", Me.GetDetailAdvancedRules.CaseGroupingBy, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Expression", "Add", "", Me.GetDetailAdvancedRules.Expression, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Advanced Rule Created Date", "Add", "", Now.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")


                End Using
            End If
        Catch
            Throw
        End Try
    End Sub
    Private Sub InsertAuditTrailEdit(ByRef oSQLTrans As SqlTransaction)
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(5)
            If Not Me.GetDetailAdvancedRules Is Nothing Then
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Advanced Rule Name", "Edit", Me.GetDetailAdvancedRules.RulesAdvancedName_Old, Me.GetDetailAdvancedRules.RulesAdvancedName, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Description", "Edit", Me.GetDetailAdvancedRules.RulesAdvancedDescription_Old, Me.GetDetailAdvancedRules.RulesAdvancedDescription, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Enabled", "Edit", Me.GetDetailAdvancedRules.Enabled_Old, Me.GetDetailAdvancedRules.Enabled, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Case Grouping By", "Edit", Me.GetDetailAdvancedRules.CaseGroupingBy_Old, Me.GetDetailAdvancedRules.CaseGroupingBy, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Expression", "Edit", Me.GetDetailAdvancedRules.Expression_Old, Me.GetDetailAdvancedRules.Expression, "Accepted")


                End Using
            End If
        Catch
            Throw
        End Try
    End Sub
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' DataUpdatingParameter edit accept
    ''' </summary>
    ''' <remarks>
    ''' Step
    ''' 1.Update Data RulesAdvanced
    ''' 2.Insert Data ke AuditTrail
    ''' 3.Delete Data dari table RulesAdvancedApproval dan RulesAdvancedPendingApproval
    ''' </remarks>
    ''' <history>
    ''' 	[Hendra] 10:22 PM 10/7/2007
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptDataAdvancedRulesEdit()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try

            'Step 1
            Using adapter As New AMLDAL.RulesAdvancedTableAdapters.RulesAdvancedTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(adapter)
                adapter.UpdateAdvancedRulesbyAdvanceRulesID(Me.GetDetailAdvancedRules.RulesAdvancedName, Me.GetDetailAdvancedRules.RulesAdvancedDescription, Me.GetDetailAdvancedRules.Enabled, Me.GetDetailAdvancedRules.CaseGroupingBy, Me.GetDetailAdvancedRules.Expression, Me.GetDetailAdvancedRules.RulesAdvancedId)
            End Using
            'Step2
            InsertAuditTrailEdit(oSQLTrans)
            'Step3
            Using adapter As New AMLDAL.RulesAdvancedTableAdapters.RulesAdvancedApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(adapter, oSQLTrans)
                adapter.DeleteRulesAdvancedByFKPendingID(Me.AdvancedRulesPendingApprovalID)
            End Using
            Using adapter As New AMLDAL.RulesAdvancedTableAdapters.RulesAdvancedPendingApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(adapter, oSQLTrans)
                adapter.DeleteRulesAdvancedPendingApprovalByPK(Me.AdvancedRulesPendingApprovalID)
            End Using
            oSQLTrans.Commit()
            AbandonSession()
            Me.Response.Redirect("AdvancedRulesApproval.aspx", False)
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Rollback()
            End If
            LogError(ex)
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try

    End Sub
    ''' <summary>
    ''' Accept Delete RulesAdvanced
    ''' </summary>
    ''' <remarks>
    ''' Step 
    ''' 1.Delete Data RulesAdvanced
    ''' 2.Insert AuditTrail
    ''' 3.Delete Data dari table RulesAdvancedApproval dan RulesAdvancedPendingApproval
    ''' </remarks>
    ''' <history>
    ''' [Hendra] 10:35 PM 10/7/2007
    ''' </history>
    Private Sub AcceptDataAdvancedRulesDelete()

        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            'Step 1
            Using adapter As New AMLDAL.RulesAdvancedTableAdapters.RulesAdvancedTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(adapter)

                adapter.DeleteRulesAdvancedByPK(Me.GetDetailAdvancedRules.RulesAdvancedId)
            End Using
            'Step 2
            InsertAuditTrailDelete(oSQLTrans)
            'Step3
            Using adapter As New AMLDAL.RulesAdvancedTableAdapters.RulesAdvancedApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(adapter, oSQLTrans)

                adapter.DeleteRulesAdvancedByFKPendingID(Me.AdvancedRulesPendingApprovalID)
            End Using
            Using adapter As New AMLDAL.RulesAdvancedTableAdapters.RulesAdvancedPendingApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(adapter, oSQLTrans)

                adapter.DeleteRulesAdvancedPendingApprovalByPK(Me.AdvancedRulesPendingApprovalID)
            End Using
            oSQLTrans.Commit()
            AbandonSession()
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Rollback()
            End If
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub
    Private Sub InsertAuditTrailDelete(ByRef osqltrans As SqlTransaction)
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(5)
            If Not Me.GetDetailAdvancedRules Is Nothing Then
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, osqltrans)
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Advanced Rule Name", "Delete", "", Me.GetDetailAdvancedRules.RulesAdvancedName, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Description", "Delete", "", Me.GetDetailAdvancedRules.RulesAdvancedDescription, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Enabled", "Delete", "", Me.GetDetailAdvancedRules.Enabled, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Case Grouping By", "Delete", "", Me.GetDetailAdvancedRules.CaseGroupingBy, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Expression", "Delete", "", Me.GetDetailAdvancedRules.Expression, "Accepted")


                End Using
            End If
        Catch
            Throw
        End Try
    End Sub
#End Region

#Region " Reject "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' reject DataRulesAdvanced add
    ''' </summary>
    ''' <remarks>
    ''' Step 
    ''' 1.Insert Audit Trail
    ''' 2.Delete Data dari table RulesAdvancedApproval dan RulesAdvancedPendingApproval
    ''' </remarks>
    ''' <history>
    ''' 	[Hendra] Created 10:44 PM 10/7/2007
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectDataRulesAdvancedAdd()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try

            Using adapter As New AMLDAL.RulesAdvancedTableAdapters.RulesAdvancedApprovalTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(adapter, Data.IsolationLevel.ReadUncommitted)
                adapter.DeleteRulesAdvancedByFKPendingID(Me.AdvancedRulesPendingApprovalID)
            End Using
            InsertAuditTrailRejectAdd(oSQLTrans)
            Using adapter As New AMLDAL.RulesAdvancedTableAdapters.RulesAdvancedPendingApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(adapter, oSQLTrans)
                adapter.DeleteRulesAdvancedPendingApprovalByPK(Me.AdvancedRulesPendingApprovalID)
            End Using

            oSQLTrans.Commit()
            AbandonSession()
            Me.Response.Redirect("AdvancedRulesApproval.aspx", False)

        Catch
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Rollback()
            End If
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub
    Private Sub InsertAuditTrailRejectAdd(ByRef oSQLTrans As SqlTransaction)
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(5)
            If Not Me.GetDetailAdvancedRules Is Nothing Then
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Advanced Rule Name", "Add", "", Me.GetDetailAdvancedRules.RulesAdvancedName, "Rejected")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Description", "Add", "", Me.GetDetailAdvancedRules.RulesAdvancedDescription, "Rejected")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Enabled", "Add", "", Me.GetDetailAdvancedRules.Enabled, "Rejected")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Case Grouping By", "Add", "", Me.GetDetailAdvancedRules.CaseGroupingBy, "Rejected")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Expression", "Add", "", Me.GetDetailAdvancedRules.Expression, "Rejected")
                End Using
            End If
        Catch ex As Exception
            Throw
        End Try


    End Sub
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' RejectDataRulesAdvancedRejectEdit reject edit
    ''' </summary>
    ''' <remarks>
    ''' Step 
    ''' 1.Insert AuditTrail
    ''' 2.Delete Data dari table RulesAdvancedApproval dan RulesAdvancedPendingApproval
    ''' </remarks>
    ''' <history>
    ''' 	[Hendra] 11:02 PM 10/7/2007
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectDataRulesAdvancedRejectEdit()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try


            'Step 2
            Using adapter As New AMLDAL.RulesAdvancedTableAdapters.RulesAdvancedApprovalTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(adapter)
                adapter.DeleteRulesAdvancedByFKPendingID(Me.AdvancedRulesPendingApprovalID)
            End Using
            InsertAuditTrailRejectEdit(oSQLTrans)
            Using adapter As New AMLDAL.RulesAdvancedTableAdapters.RulesAdvancedPendingApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(adapter, oSQLTrans)
                adapter.DeleteRulesAdvancedPendingApprovalByPK(Me.AdvancedRulesPendingApprovalID)
            End Using

            oSQLTrans.Commit()
            AbandonSession()
            Me.Response.Redirect("AdvancedRulesApproval.aspx", False)
        Catch
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Rollback()
            End If
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try

    End Sub
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' RejectDataRulesAdvancedRejectDelete reject Delete
    ''' </summary>
    ''' <remarks>
    ''' Step 
    ''' 1.Insert AuditTrail
    ''' 2.Delete Data dari table RulesAdvancedApproval dan RulesAdvancedPendingApproval
    ''' </remarks>
    ''' <history>
    ''' 	[Hendra] 11:02 PM 10/7/2007
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectDataRulesAdvancedRejectDelete()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try


            Using adapter As New AMLDAL.RulesAdvancedTableAdapters.RulesAdvancedApprovalTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(adapter)
                adapter.DeleteRulesAdvancedByFKPendingID(Me.AdvancedRulesPendingApprovalID)
            End Using
            InsertAuditTrailRejectDelete(oSQLTrans)
            Using adapter As New AMLDAL.RulesAdvancedTableAdapters.RulesAdvancedPendingApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(adapter, oSQLTrans)
                adapter.DeleteRulesAdvancedPendingApprovalByPK(Me.AdvancedRulesPendingApprovalID)
            End Using


            oSQLTrans.Commit()
            AbandonSession()
            Me.Response.Redirect("AdvancedRulesApproval.aspx", False)
        Catch
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Rollback()
            End If
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try

    End Sub

#End Region

    Private Sub InsertAuditTrailRejectEdit(ByRef oSQLTrans As SqlTransaction)
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(5)
            If Not Me.GetDetailAdvancedRules Is Nothing Then

                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Advanced Rule Name", "Edit", Me.GetDetailAdvancedRules.RulesAdvancedName_Old, Me.GetDetailAdvancedRules.RulesAdvancedName, "Rejected")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Description", "Edit", Me.GetDetailAdvancedRules.RulesAdvancedDescription_Old, Me.GetDetailAdvancedRules.RulesAdvancedDescription, "Rejected")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Enabled", "Edit", Me.GetDetailAdvancedRules.Enabled_Old, Me.GetDetailAdvancedRules.Enabled, "Rejected")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Case Grouping By", "Edit", Me.GetDetailAdvancedRules.CaseGroupingBy_Old, Me.GetDetailAdvancedRules.CaseGroupingBy, "Rejected")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Expression", "Edit", Me.GetDetailAdvancedRules.Expression_Old, Me.GetDetailAdvancedRules.Expression, "Rejected")
                End Using
            End If
        Catch ex As Exception
            Throw
        End Try


    End Sub
    Private Sub InsertAuditTrailRejectDelete(ByRef oSQLTrans As SqlTransaction)
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(5)
            If Not Me.GetDetailAdvancedRules Is Nothing Then
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Advanced Rule Name", "Delete", "", Me.GetDetailAdvancedRules.RulesAdvancedName, "Rejected")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Description", "Delete", "", Me.GetDetailAdvancedRules.RulesAdvancedDescription, "Rejected")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Enabled", "Delete", "", Me.GetDetailAdvancedRules.Enabled, "Rejected")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Case Grouping By", "Delete", "", Me.GetDetailAdvancedRules.CaseGroupingBy, "Rejected")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Expression", "Delete", "", Me.GetDetailAdvancedRules.Expression, "Rejected")
                End Using
            End If
        Catch ex As Exception
            Throw
        End Try


    End Sub





    ''' <summary>
    ''' page load event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.ImageAccept.Attributes.Add("onclick", "javascript: if(confirm('Do you want to accept this activity ?')== false) return false;")
                Me.ImageReject.Attributes.Add("onclick", "javascript: if(confirm('Do you want to reject this activity ?')== false) return false;")
                Me.HandleTampilan()
                Select Case Me.ParamType
                    Case TypeConfirm.Add
                        Me.LoadDataAdvancedRulesAdd()
                    Case TypeConfirm.Edit
                        Me.LoadDataAdvancedRulesEdit()
                    Case TypeConfirm.Delete
                        Me.LoadDataAdvancedRulesDelete()
                End Select

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)


                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                End Using
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' accept button handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click
        Try
            Select Case Me.ParamType
                Case TypeConfirm.Add
                    Me.AcceptDataAdvancedRulesAdd()
                Case TypeConfirm.Edit
                    Me.AcceptDataAdvancedRulesEdit()
                Case TypeConfirm.Delete
                    Me.AcceptDataAdvancedRulesDelete()
                Case Else
                    Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
            End Select

            Sahassa.AML.Commonly.SessionIntendedPage = "AdvancedRulesApproval.aspx"

            Me.Response.Redirect("AdvancedRulesApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' reject button handlert
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click
        Try
            Select Case Me.ParamType
                Case TypeConfirm.Add
                    Me.RejectDataRulesAdvancedAdd()
                Case TypeConfirm.Edit
                    Me.RejectDataRulesAdvancedRejectEdit()
                Case TypeConfirm.Delete
                    Me.RejectDataRulesAdvancedRejectDelete()
                Case Else
                    Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
            End Select

            Sahassa.AML.Commonly.SessionIntendedPage = "AdvancedRulesApproval.aspx"

            Me.Response.Redirect("AdvancedRulesApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' back button handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "AdvancedRulesApproval.aspx"
        AbandonSession()
        Me.Response.Redirect("AdvancedRulesApproval.aspx", False)
    End Sub
    Public Enum TypeConfirm
        Add = 1
        Edit
        Delete
    End Enum
End Class
