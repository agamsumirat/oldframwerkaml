Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports System.Data
Partial Class CTRFileUploadReferenceNumber
    Inherits Parent

#Region "Property"

    Public ReadOnly Property PKListOfGeneratedFile() As Integer
        Get
            Dim strtemp As String = Request.Params("PK")
            Dim intResult As Integer
            If Not Integer.TryParse(strtemp, intResult) Then
                Throw New Exception("PK Must Integer")
            End If
            Return intResult
        End Get
    End Property


    Public ReadOnly Property View_FkCustomerTypeId() As Int16
        Get
            Return Convert.ToInt16(Session("View_FkCustomerTypeId"))
        End Get
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Session("CTRFileUploadReferenceNumber.TransactionDate") = Nothing
                Session("CTRFileUploadReferenceNumber.TipeCustomer") = Nothing

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    '    Transcope.Complete()
                End Using

                Me.LoadData()
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    'TODO: ubah page ini
    Private Sub LoadData()
        Try
            Using ObjListvw_ListOfGeneratedFileCTR As VList(Of vw_ListOfGeneratedFileCTR) = DataRepository.vw_ListOfGeneratedFileCTRProvider.GetPaged("PK_ListOfGeneratedFileCTR_ID =" & Me.PKListOfGeneratedFile, "", 0, 1, 0)
                If Not ObjListvw_ListOfGeneratedFileCTR Is Nothing Then
                    Dim Objvw_ListOfGeneratedFileCTR As vw_ListOfGeneratedFileCTR = ObjListvw_ListOfGeneratedFileCTR.Item(0)
                    Me.LblTextFileName.Text = Objvw_ListOfGeneratedFileCTR.FileName.Trim()
                    'Me.LblTotalNumberofCIF.Text = Objvw_ListOfGeneratedFileCTR.CTRFile_TotalNumberofCIF.ToString

                    If CInt(Objvw_ListOfGeneratedFileCTR.FK_ReportType_ID.GetValueOrDefault(0)) = 1 Then
                        Me.LblTotalNumberofCIF.Text = Objvw_ListOfGeneratedFileCTR.TotalCIF.ToString()
                    Else
                        Me.LblTotalNumberofCIF.Text = Objvw_ListOfGeneratedFileCTR.TotalWIC.ToString()
                    End If

                    LblTotalAmountCashIn.Text = AMLBLL.ReportControlGeneratorBLL.GetTotalCashInByPKGeneratedID(Objvw_ListOfGeneratedFileCTR.PK_ListOfGeneratedFileCTR_ID)
                    LblTotalAmountCashOut.Text = AMLBLL.ReportControlGeneratorBLL.GetTotalCashoutByPKGeneratedID(Objvw_ListOfGeneratedFileCTR.PK_ListOfGeneratedFileCTR_ID)

                    'Me.LblTotalAmount.Text = "(IDR) " & Objvw_ListOfGeneratedFileCTR.CTRFile_TotalAmount.ToString("###,###0.00")
                    Me.LblCustomerType.Text = Objvw_ListOfGeneratedFileCTR.ReportFormatName
                    Me.LblReportType.Text = Objvw_ListOfGeneratedFileCTR.ReportTypeDesc
                    Session("View_FkCustomerTypeId") = Objvw_ListOfGeneratedFileCTR.FK_ReportType_ID

                    Session("CTRFileUploadReferenceNumber.TransactionDate") = Objvw_ListOfGeneratedFileCTR.TransactionDate
                    Session("CTRFileUploadReferenceNumber.TipeCustomer") = CInt(Objvw_ListOfGeneratedFileCTR.FK_ReportType_ID)
                End If
            End Using
        Catch
            Throw
        End Try
    End Sub


    Protected Sub ImgBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBack.Click
        Try
            Session("View_PkCTRFileId") = Nothing
            Session("View_FkCustomerTypeId") = Nothing
            Sahassa.AML.Commonly.SessionIntendedPage = "ListofGeneratedCTRFile.aspx"
            Me.Response.Redirect("ListofGeneratedCTRFile.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Protected Sub ImgUploadBasedonRefNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgUploadBasedonRefNo.Click
        Dim ArrConfirmationNumber As New ArrayList
        Dim ObjUploadRefNoTransManager As TransactionManager = Nothing


        Try
            If Not Me.FileUploadFileReferenceNumber.HasFile Then
                Throw New Exception("Please choose file to be uploaded.")
            End If
            ObjUploadRefNoTransManager = New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)
            ObjUploadRefNoTransManager.BeginTransaction(IsolationLevel.ReadUncommitted)



            ArrConfirmationNumber = Me.GetCTRUploadedConfirmationNumber()

            If Not ArrConfirmationNumber Is Nothing Then
                If ArrConfirmationNumber.Count > 0 Then
                    'cek dulu cif atau wic 
                    Select Case Me.View_FkCustomerTypeId
                        Case 1
                            'BDI Customer
                            If ArrConfirmationNumber.Count = Convert.ToInt32(Me.LblTotalNumberofCIF.Text) Then
                                ' Do Update PPATK Confirmation
                                For i As Integer = 0 To ArrConfirmationNumber.Count - 1
                                    Using ObjSQLCmd As New System.Data.SqlClient.SqlCommand
                                        ObjSQLCmd.CommandType = Data.CommandType.Text
                                        ObjSQLCmd.CommandText = "EXEC usp_UpdatePPATKReffNumberForLTKT '" & ArrConfirmationNumber(i).ToString & "'," & (i + 1) & ",'" & CDate(Session("CTRFileUploadReferenceNumber.TransactionDate").ToString()).ToString("yyyy-MM-dd") & "'"
                                        SahassaNettier.Data.Utility.ExecuteNonQuery(ObjUploadRefNoTransManager, ObjSQLCmd)
                                    End Using
                                Next

                                Using ObjSQLCmd As New System.Data.SqlClient.SqlCommand
                                    ObjSQLCmd.CommandType = Data.CommandType.Text
                                    ObjSQLCmd.CommandText = "EXEC usp_ListOfGeneratedCTRNoDone '" & Me.PKListOfGeneratedFile & "'"
                                    SahassaNettier.Data.Utility.ExecuteNonQuery(ObjUploadRefNoTransManager, ObjSQLCmd)
                                End Using
                            Else
                                Throw New Exception("Count Of Confirmation Number not same with count of CTR for file name '" & Me.LblTextFileName.Text & "'")
                            End If
                        Case 2
                            'WIC
                            If ArrConfirmationNumber.Count = Convert.ToInt32(Me.LblTotalNumberofCIF.Text) Then
                                ' Do Update PPATK Confirmation
                                For i As Integer = 0 To ArrConfirmationNumber.Count - 1
                                    Using ObjSQLCmd As New System.Data.SqlClient.SqlCommand
                                        ObjSQLCmd.CommandType = Data.CommandType.Text
                                        ObjSQLCmd.CommandText = "EXEC usp_UpdatePPATKReffNumberForWIC '" & ArrConfirmationNumber(i).ToString & "'," & (i + 1) & ",'" & CDate(Session("CTRFileUploadReferenceNumber.TransactionDate").ToString()).ToString("yyyy-MM-dd") & "'"
                                        SahassaNettier.Data.Utility.ExecuteNonQuery(ObjUploadRefNoTransManager, ObjSQLCmd)
                                    End Using
                                Next
                            Else
                                Throw New Exception("Count Of Confirmation Number not same with count of CTR for file name '" & Me.LblTextFileName.Text & "'")
                            End If
                    End Select
                    InsertAuditTrailUploadRefNo(ObjUploadRefNoTransManager, "Direct Upload Reference Number from PPATK without approval")

                    'Using ObjSQLCmd As New System.Data.SqlClient.SqlCommand
                    '    ObjSQLCmd.CommandType = Data.CommandType.Text
                    '    ObjSQLCmd.CommandText = "Update CTRFile set Fk_MsStatusUploadPPATK_Id=1 where Pk_CTRFile_Id=" & Me.View_PkCTRFileId
                    '    Sahassa.CTR_Web.Data.Utility.ExecuteNonQuery(ObjUploadRefNoTransManager, ObjSQLCmd)
                    'End Using

                    If ObjUploadRefNoTransManager.IsOpen Then
                        ObjUploadRefNoTransManager.Commit()
                    End If
                End If
            End If
            Me.LblSucces.Text = "Update PPATK Confirmation Number has done successfully."
            Me.LblSucces.Visible = True
        Catch ex As Exception
            If Not ObjUploadRefNoTransManager.IsOpen Then
                ObjUploadRefNoTransManager.Rollback()
            End If
            Me.LblSucces.Visible = False
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not ObjUploadRefNoTransManager Is Nothing Then
                ObjUploadRefNoTransManager.Dispose()
                ObjUploadRefNoTransManager = Nothing
            End If
            If Not ArrConfirmationNumber Is Nothing Then
                ArrConfirmationNumber = Nothing
            End If
        End Try
    End Sub
    Private Function GetCTRUploadedConfirmationNumber() As ArrayList
        Dim myStream As System.IO.Stream
        Dim ArrConfirmationNumber As String() = Nothing
        Dim ArrReturn As New ArrayList
        Dim Counter As Integer
        Dim StrConfirmationNumber As String = ""
        Dim StrConfirmationNumberDetail As String
        Dim IntStartCounter As Integer

        Try
            ' Initialize the stream to read the uploaded file.
            myStream = Me.FileUploadFileReferenceNumber.FileContent
            Using sr As IO.StreamReader = New IO.StreamReader(myStream)
                StrConfirmationNumber = sr.ReadToEnd
                sr.Close()
            End Using
            ArrConfirmationNumber = StrConfirmationNumber.Split(vbCrLf)
            If Not ArrConfirmationNumber Is Nothing Then
                If ArrConfirmationNumber.Length > 0 Then
                    ' Update 19 November 2009 by Johan cause changes in PPATK system
                    'Nama File	Order	No Referensi Internal PJK	No Referensi PPATK
                    'C080910001	       SR5082-C080910001-00023-20080919
                    'C080910001	00024	SR5082-C080910001-00024-20080919
                    'C080910001	00025	SR5082-C080910001-00025-20080919
                    'C080910001	00026	SR5082-C080910001-00026-20080919
                    'C080910001	00027	SR5082-C080910001-00027-20080919
                    'C080910001	00028	SR5082-C080910001-00028-20080919
                    'C080910001	00029	SR5082-C080910001-00029-20080919
                    'C080910001	00030	SR5082-C080910001-00030-20080919
                    'C080910001	00031	SR5082-C080910001-00031-20080919
                    'C080910001	00032	SR5082-C080910001-00032-20080919

                    IntStartCounter = 0
                    If Not System.Configuration.ConfigurationManager.AppSettings("CTRConfirmSkipFirstRow") Is Nothing Then
                        If System.Configuration.ConfigurationManager.AppSettings("CTRConfirmSkipFirstRow") Then
                            IntStartCounter = 1
                        End If
                    End If

                    For Counter = IntStartCounter To ArrConfirmationNumber.Length - 1
                        ' Update 19 November 2009 by Johan cause changes in PPATK system
                        StrConfirmationNumberDetail = ArrConfirmationNumber(Counter).Trim()
                        StrConfirmationNumberDetail = Right(StrConfirmationNumberDetail, 32)
                        If StrConfirmationNumberDetail <> "" Then
                            If Text.RegularExpressions.Regex.IsMatch(StrConfirmationNumberDetail, "^\w{4,4}\-\w{7,7}\-\d{6,6}\-\d{8,8}$") Then
                                ArrReturn.Add(StrConfirmationNumberDetail)
                            Else
                                Throw New Exception("Invalid Confirmation Number '" & StrConfirmationNumberDetail & "' at line " & Counter + 1 & ". Confirmation Number must be using following format 'XXXX-XXXXXXX-XXXXXX-XXXXXXXX' (28 Characters)")
                            End If
                        End If
                    Next
                End If
            End If
            Return ArrReturn
        Catch
            Throw
        End Try
    End Function
    Protected Sub cvalPageError_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles cvalPageError.PreRender
        If Me.cvalPageError.IsValid = False Then
            ' Sahassa.AML.Commonly.SessionErrorMessageCount = Sahassa.AML.Commonly.SessionErrorMessageCount + 1
            ClientScript.RegisterStartupScript(Page.GetType, "ErrorMessage" & Guid.NewGuid.ToString, "alert('There was uncompleteness in this page: \n" & Me.cvalPageError.ErrorMessage.Replace("'", "\'").Replace(vbCrLf, "\n").Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">") & "');", True)
        End If
    End Sub
#Region "Audit Trail"
    Private Sub InsertAuditTrailUploadRefNo(ByRef ValTransactionManager As TransactionManager, ByVal StatusApproval As String)
        Dim AuditTrailEntityId As Integer
        Dim ExecutionTime As DateTime
        Dim NIK As String
        Dim StaffName As String
        Dim AuditTrailDescription As String
        Dim PkAuditTrailMasterId As Int64
        Try
            AuditTrailEntityId = 15
            ExecutionTime = DateTime.Now
            NIK = Sahassa.AML.Commonly.SessionUserId
            StaffName = Sahassa.AML.Commonly.SessionUserId
            AuditTrailDescription = StatusApproval

            'Using ObjControlerAuditTrailMaster As New AuditTrailBLL()
            '    ObjControlerAuditTrailMaster.MasterSaveAdd(ValTransactionManager, AuditTrailEntityId, ExecutionTime, NIK, StaffName, AuditTrailDescription, PkAuditTrailMasterId)
            'End Using
            'Using ObjControlerAuditTrailDetail As New AuditTrailBLL
            '    ObjControlerAuditTrailDetail.DetailSaveAdd(ValTransactionManager, PkAuditTrailMasterId, "CTR Text File Name", "[N/A]", Me.LblTextFileName.Text)
            '    ObjControlerAuditTrailDetail.DetailSaveAdd(ValTransactionManager, PkAuditTrailMasterId, "Total Number of CIF", "[N/A]", Me.LblTotalNumberofCIF.Text)
            '    ' ObjControlerAuditTrailDetail.DetailSaveAdd(ValTransactionManager, PkAuditTrailMasterId, "Total Amount", "[N/A]", Me.LblTotalAmount.Text)
            '    ObjControlerAuditTrailDetail.DetailSaveAdd(ValTransactionManager, PkAuditTrailMasterId, "Customer Type", "[N/A]", Me.LblCustomerType.Text)
            '    ObjControlerAuditTrailDetail.DetailSaveAdd(ValTransactionManager, PkAuditTrailMasterId, "Report Type", "[N/A]", Me.LblReportType.Text)
            'End Using
        Catch
            Throw
        End Try
    End Sub
#End Region

    'Protected Sub LnkDownloadTemplate_Click(sender As Object, e As System.EventArgs) Handles LnkDownloadTemplate.Click
    '    Try

    '        Response.AddHeader("content-disposition", "attachment;filename=UploadRefNoCTR.xls")
    '        Response.Charset = ""
    '        Response.ContentType = "application/vnd.xls"
    '        Me.EnableViewState = False




    '        'If System.IO.Path.GetExtension(ObjAttachment.FileName).ToLower = ".zip" Then
    '        '    Response.ContentType = "application/octet-stream"
    '        'Else
    '        '    Response.ContentType = ObjAttachment.ContentType
    '        'End If
    '        Response.BinaryWrite(My.Computer.FileSystem.ReadAllBytes(Server.MapPath("~") & "\ExcelTemplate\UploadRefNoCTR.xls"))
    '        Response.End()
    '    Catch ex As Exception
    '        Me.cvalPageError.IsValid = False
    '        Me.cvalPageError.ErrorMessage = ex.Message
    '        LogError(ex)
    '    End Try
    'End Sub
End Class
