<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="UploadWorkFlowApprovalDetail.aspx.vb" Inherits="UploadWorkFlowApprovalDetail"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
    <table cellpadding="0" cellspacing="4" width="100%">
        <tr>
            <td align="left" valign="bottom">
                <img height="15" src="images/dot_title.gif" width="15" /></td>
            <td class="maintitle" valign="bottom" width="99%">
                <asp:Label ID="LabelTitle" runat="server"></asp:Label></td>
        </tr>
    </table>
    <table style="width: 100%">
        <tr>
            <td colspan="3" valign="top">
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                    EnableModelValidation="True">
                    <Columns>
                        <asp:TemplateField HeaderText="No">
                            <ItemTemplate>
                                <asp:Label ID="labelno" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="AccountOwnerID" HeaderText="AccountOwnerID" />
                        <asp:BoundField DataField="DescriptionCaseAlert" 
                            HeaderText="DescriptionCaseAlert" />
                        <asp:BoundField DataField="VIPCODE" HeaderText="VIPCODE" />
                        <asp:BoundField DataField="Segment" HeaderText="Segment" />

                        <asp:BoundField DataField="SBU" HeaderText="SBU" />
                        <asp:BoundField DataField="SUBSBU" HeaderText="SUBSBU" />
                        <asp:BoundField DataField="RM" HeaderText="RM" />

                        <asp:BoundField DataField="InsiderCode" HeaderText="InsiderCode" />
                        <asp:BoundField DataField="SerialNo" HeaderText="SerialNo" />
                        <asp:BoundField DataField="WorkFlowStep" HeaderText="WorkFlowStep" />
                        <asp:TemplateField HeaderText="Approvers">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Approvers") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="labelapprover" runat="server" Text='<%# Bind("Approvers") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="DueDate" HeaderText="DueDate" />
                        <asp:TemplateField HeaderText="NotifyOthers">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("NotifyOthers") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="labelnotifyothers" runat="server" Text='<%# Bind("NotifyOthers") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="SubjectEmail" HeaderText="SubjectEmail" />
                        <asp:BoundField DataField="BodyEmail" HeaderText="BodyEmail" />
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="3" valign="top">
                <asp:ImageButton ID="imgAccept" runat="server" ImageUrl="~/Images/button/accept.gif" />
                <asp:ImageButton ID="imgreject" runat="server" ImageUrl="~/Images/button/reject.gif" />
                <asp:ImageButton ID="imgCancel" runat="server" ImageUrl="~/Images/button/cancel.gif" />
                <asp:CustomValidator ID="CValPageError" runat="server" Display="None"></asp:CustomValidator></td>
        </tr>
    </table>
</asp:Content>

