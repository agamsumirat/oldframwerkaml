<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="LevelTypeEdit.aspx.vb" Inherits="LevelTypeEdit" title="Level Type Edit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
	<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2" height="72" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
        <tr class="formText">
            <td bgcolor="#ffffff" colspan="4" style="border-top-style: none; border-right-style: none;
                border-left-style: none; height: 24px; border-bottom-style: none">
                <strong style="font-size: 18px">Level Type - Edit<br />
                    <hr />
                </strong>
                <asp:Label ID="LblSuccess" runat="server" CssClass="validationok" Visible="False"
                    Width="100%"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" colspan="4" style="border-top-style: none; border-right-style: none;
                border-left-style: none; height: 24px; border-bottom-style: none"><span style="color: #ff0000">* Required</span></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;" width="20%">
                Level Type ID</td>
            <td bgcolor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;" width="5">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;" width="80%">
                <asp:Label ID="LabelTypeID" runat="server"></asp:Label></td>
        </tr>
		<tr class="formText">
			<td bgColor="#ffffff" style="width: 24px; height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorLevelTypeName" runat="server"
                    ControlToValidate="TextLevelTypeName" Display="Dynamic" ErrorMessage="Level Type Name is required">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidatorLevelTypeName" runat="server"
                    ControlToValidate="TextLevelTypeName" ErrorMessage="Level Type Name must starts with a letter then it can be followed by any letters, numbers or underscore"
                    ValidationExpression="[a-zA-Z](\w*\s*)*">*</asp:RegularExpressionValidator>&nbsp;</td>
			<td width="20%" bgColor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
                Level Type Name</td>
			<td width="5" bgColor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">:</td>
			<td width="80%" bgColor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;"><asp:textbox id="TextLevelTypeName" runat="server" CssClass="textBox" MaxLength="50" Width="200px" ></asp:textbox>
                <strong><span style="color: #ff0000">*</span></strong></td>
		</tr>
		<tr class="formText">
			<td bgColor="#ffffff" style="width: 24px; height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;" rowspan="6">&nbsp;</td>
			<td bgColor="#ffffff" rowspan="6" style="height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
                Description<br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" rowspan="6" style="height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
                :<br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" rowspan="6" style="height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
                <asp:textbox id="TextLevelTypeDescription" runat="server" CssClass="textBox" MaxLength="255" Width="200px" Height="63px" TextMode="MultiLine"></asp:textbox></td>
		</tr>
		<tr class="formText">
		</tr>
		<tr class="formText">
		</tr>
		<tr class="formText">
		</tr>
		<tr class="formText">
		</tr>
		<tr class="formText">
		</tr>
		<tr class="formText" bgColor="#dddddd" height="30">
			<td style="width: 24px"><IMG height="15" src="images/arrow.gif" width="15"></td>
			<td colSpan="3" style="height: 9px">
				<table cellSpacing="0" cellPadding="3" border="0">
					<tr>
						<td><asp:imagebutton id="ImageUpdate" runat="server" CausesValidation="True" SkinID="UpdateButton"></asp:imagebutton></td>
						<td><asp:imagebutton id="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton"></asp:imagebutton></td>
					</tr>
				</table>                
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
		</tr>
	</table>
	
</asp:Content>

