<%@ Page Language="VB" MasterPageFile="~/masterpage.master" AutoEventWireup="false" CodeFile="ProposalSTRDetail.aspx.vb" Inherits="ProposalSTRDetail" title="Proposal STR Detail" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
<ajax:AjaxPanel ID="PanelAccountInformationDetail" runat="server" Width="100%">
        <table id="TABLE1" bgcolor="#dddddd" border="2" bordercolor="#fffff" cellpadding="0"
            cellspacing="0" width="99%">
            <tr>
                <td style="width: 5px; height: 39px;">
                </td>
                <td style="  background-color: #ffffff; height: 39px;" valign="top">
                    <table width="100%">
                        <tr>
                            <td style="width: 1%">
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/dot_title.gif" /></td>
                            <td style="width: 100px">
                                <strong style="font-size: medium; width: 50%">
                                    <asp:Label ID="Label1" runat="server" CssClass="MainTitle" Text="STR Proposal"
                                        Width="400px"></asp:Label></strong></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 5px">
                </td>
                <td style="background-color: #ffffff; " valign="top">
                    <table cellpadding="0" cellspacing="0" width="99%">
                        <tr>
                            <td valign="top" >
                                &nbsp;<table class="tabContents">
                                    <tr>
                                        <td style=" height: 722px" >
                                            
                                                    <table class="TabArea" width="100%">
                                                        <tr id="Tr14" runat="server">
                                                        </tr>
                                                        <tr id="Tr15" runat="server">
                                                            <td id="TdDataGeneralAvailable" runat="server">
                                                                <table bgcolor="#dddddd" border="2" bordercolor="#ffffff" cellpadding="1" cellspacing="1" >
                                                                    
                                                                    <tr style="background-color: #ffffff">
                                                                        <td bgcolor="#ffffff" style="height: 24px" width="20%">
                                                                            I. Kasus Posisi</td>
                                                                    </tr> 
                                                                    <tr style="background-color: #ffffff">
                                                                        <td bgcolor="#ffffff" style="height: 24px" >
                                                                            &nbsp;<asp:TextBox ID="TextKasusPosisi" runat="server"  CssClass="searcheditbox" Height="50px" TextMode="MultiLine" Enabled="False" Width="700px"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextKasusPosisi"
                                                                                Display="Dynamic" ErrorMessage="Please Fill Kasus Polisi">*</asp:RequiredFieldValidator></td>
                                                                    </tr>

                                                                    <tr style="background-color: #ffffff">
                                                                        <td bgcolor="#ffffff" style="height: 24px" >
                                                                            II. Indikator Mencurigakan</td>
                                                                    </tr> 
                                                                    <tr style="background-color: #ffffff">
                                                                        <td bgcolor="#ffffff" style="height: 24px" >
                                                                            &nbsp;<asp:TextBox ID="TextIndikatorMencurigakan" runat="server"  CssClass="searcheditbox" Height="50px" TextMode="MultiLine" Enabled="False" Width="700px"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextIndikatorMencurigakan"
                                                                                Display="Dynamic" ErrorMessage="Please Fill Indicator Mencurigakan">*</asp:RequiredFieldValidator></td>
                                                                    </tr>

                                                                    <tr style="background-color: #ffffff">
                                                                        <td bgcolor="#ffffff" style="height: 24px" >
                                                                            III. Unsur Transaksi Keuangan Mencurigakan</td>
                                                                    </tr> 
                                                                    <tr style="background-color: #ffffff">
                                                                        <td bgcolor="#ffffff" style="height: 24px" >
                                                                            &nbsp;<asp:TextBox ID="TextUnsurTKM" runat="server"  CssClass="searcheditbox" Height="50px" TextMode="MultiLine" Enabled="False" Width="700px"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextUnsurTKM"
                                                                                Display="Dynamic" ErrorMessage="Please Fill Unsur Transaksi Keuangan Mencurigakan">*</asp:RequiredFieldValidator></td>
                                                                    </tr>

                                                                    <tr style="background-color: #ffffff">
                                                                        <td bgcolor="#ffffff" style="height: 24px" >
                                                                            IV. Lain-lain</td>
                                                                    </tr> 
                                                                    <tr style="background-color: #ffffff">
                                                                        <td bgcolor="#ffffff" style="height: 24px" >
                                                                            &nbsp;<asp:TextBox ID="TextLainLain" runat="server"  CssClass="searcheditbox" Height="50px" TextMode="MultiLine" Enabled="False" Width="700px"></asp:TextBox>
                                                                        </td>
                                                                    </tr>

                                                                    <tr style="background-color: #ffffff">
                                                                        <td bgcolor="#ffffff" style="height: 24px" >
                                                                            V. Kesimpulan</td>
                                                                    </tr> 
                                                                    <tr style="background-color: #ffffff">
                                                                        <td bgcolor="#ffffff" style="height: 24px" >
                                                                            &nbsp;<asp:TextBox ID="TextKesimpulan" runat="server"  CssClass="searcheditbox" Height="50px" TextMode="MultiLine" Enabled="False" Width="700px"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TextKesimpulan"
                                                                                Display="Dynamic" ErrorMessage="Please fill kesimpulan">*</asp:RequiredFieldValidator></td>
                                                                    </tr>
                                                                    <tr style="background-color: #ffffff">
                                                                        <td bgcolor="#ffffff" style="height: 24px" >
                                                                            &nbsp;</td>
                                                                    </tr>

                                                                    <tr style="background-color: #ffffff">
                                                                        <td bgcolor="#ffffff" style="height: 25px" >
                                                                            <asp:Panel ID="Panel1" runat="server" GroupingText="Workflow" Height="100%" Width="100%">
                                                                                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                                                                    ForeColor="Black" GridLines="Vertical" Width="100%" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px">
                                                                                    <FooterStyle BackColor="#CCCC99" />
                                                                                    <Columns>
                                                                                        <asp:BoundField DataField="ProposalSTREventTypeDescriptioin" HeaderText="EventType" />
                                                                                        <asp:BoundField DataField="CreatedDate" DataFormatString="{0:dd MMM yyyy HH:mm}"
                                                                                            HeaderText="Event Date" HtmlEncode="False" />
                                                                                        <asp:BoundField DataField="workflowstep" HeaderText="Workflow Step" />
                                                                                        <asp:BoundField DataField="userid" HeaderText="Approver" />
                                                                                        <asp:BoundField DataField="ProposalSTRResponseDescription" HeaderText="Response" />
                                                                                        <asp:BoundField DataField="Comment" HeaderText="Comment" />
                                                                                    </Columns>
                                                                                    <RowStyle BackColor="#F7F7DE" />
                                                                                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                                                                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                                                                    <AlternatingRowStyle BackColor="White" />
                                                                                </asp:GridView>
                                                                            </asp:Panel>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="background-color: #ffffff">
                                                                        <td bgcolor="#ffffff" style="height: 24px" >
                                                                            </td>
                                                                    </tr>

                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                             
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td style="width: 466px">
                    &nbsp;<asp:ImageButton ID="ImagePrint" runat="server" SkinID="PrintButton" />
                    <asp:ImageButton ID="ImageButtonBack" runat="server" SkinID="BackButton" />
                    <asp:CustomValidator ID="CValPageError" runat="server" Display="None"></asp:CustomValidator><br />
                </td>
            </tr>
        </table>
        
    </ajax:AjaxPanel>
</asp:Content>

