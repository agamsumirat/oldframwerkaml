#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports AMLBLL.AuditTrailBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports Sahassa.AML
Imports System.Collections.Generic
#End Region

Partial Class MsKotaKab_UploadApprovalDetail_Detail
    Inherits Parent

#Region "Function"

    Sub HideControl()
        ImageCancel.Visible = False
    End Sub

    Sub ChangeMultiView(ByVal index As Integer)
        MtvMsUser.ActiveViewIndex = index
    End Sub

#End Region

#Region "events..."

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Dim LngId As Long = 0
        Using ObjMsKotaKab_ApprovalDetail As TList(Of MsKotaKab_ApprovalDetail) = DataRepository.MsKotaKab_ApprovalDetailProvider.GetPaged("PK_MsKotaKab_ApprovalDetail_Id = " & parID, "", 0, Integer.MaxValue, 0)
            If ObjMsKotaKab_ApprovalDetail.Count > 0 Then
                LngId = ObjMsKotaKab_ApprovalDetail(0).FK_MsKotaKab_Approval_Id
            End If
        End Using
        Response.Redirect("MsKotaKab_UploadApprovalDetail_View.aspx?ID=" & LngId)
    End Sub




    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                'AuditTrailBLL.InsertAuditTrailUserAccess(SessionCurrentPage)
                ListmapingNew = New TList(Of MappingMsKotaKabNCBSPPATK_Approval_Detail)
                ListmapingOld = New TList(Of MappingMsKotaKabNCBSPPATK)
                LoadData()
            Catch ex As Exception
                LogError(ex)
                CvalPageErr.IsValid = False
                CvalPageErr.ErrorMessage = ex.Message
            End Try
        End If

    End Sub

    Private Sub LoadData()
        Dim PkObject As String
        'Load new data
        Using ObjMsKotaKab_ApprovalDetail As MsKotaKab_ApprovalDetail = DataRepository.MsKotaKab_ApprovalDetailProvider.GetPaged(MsKotaKab_ApprovalDetailColumn.PK_MsKotaKab_ApprovalDetail_Id.ToString & _
            "=" & _
            parID, "", 0, 1, Nothing)(0)
            With ObjMsKotaKab_ApprovalDetail
                SafeDefaultValue = "-"
                HFProvincenew.Value = Safe(.IDPropinsi)
                Dim OProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(.IDPropinsi.GetValueOrDefault)
                If OProvince IsNot Nothing Then LBSearchNamanew.Text = Safe(OProvince.Nama)

                txtIDKotaKabnew.Text = Safe(.IDKotaKab)
                txtNamaKotaKabnew.Text = Safe(.NamaKotaKab)

                'other info
                Dim Omsuser As TList(Of User)
                Omsuser = DataRepository.UserProvider.GetByUserID(.CreatedBy)
                If Omsuser.Count > 0 Then
                    lblCreatedByNew.Text = Omsuser(0).UserName
                Else
                    lblCreatedByNew.Text = SafeDefaultValue
                End If
                lblCreatedDateNew.Text = FormatDate(.CreatedDate)
                Omsuser = DataRepository.UserProvider.GetByUserID(.LastUpdatedBy)
                If Omsuser.Count > 0 Then
                    lblUpdatedbyNew.Text = Omsuser(0).UserName
                Else
                    lblUpdatedbyNew.Text = SafeDefaultValue
                End If
                lblUpdatedDateNew.Text = FormatDate(.LastUpdatedDate)
                'txtActivationNew.Text = SafeActiveInactive(.Activation)
                Dim L_objMappingMsKotaKabNCBSPPATK_Approval_Detail As TList(Of MappingMsKotaKabNCBSPPATK_Approval_Detail)
                L_objMappingMsKotaKabNCBSPPATK_Approval_Detail = DataRepository.MappingMsKotaKabNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsKotaKabNCBSPPATK_Approval_DetailColumn.IDKotaKab.ToString & _
                 "=" & _
                 ObjMsKotaKab_ApprovalDetail.IDKotaKab, "", 0, Integer.MaxValue, Nothing)

                'Add mapping
                ListmapingNew.AddRange(L_objMappingMsKotaKabNCBSPPATK_Approval_Detail)

            End With
            PkObject = ObjMsKotaKab_ApprovalDetail.IDKotaKab
        End Using



        'Load Old Data
        Using objMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(PkObject)
            If objMsKotaKab Is Nothing Then
                lamaOld.Visible = False
                lama.Visible = False
                Return
            End If

            With objMsKotaKab
                SafeDefaultValue = "-"
                HFProvinceOld.Value = Safe(.IDPropinsi)
                Dim OProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(.IDPropinsi.GetValueOrDefault)
                If OProvince IsNot Nothing Then LBSearchNamaOld.Text = Safe(OProvince.Nama)

                txtIDKotaKabOld.Text = Safe(.IDKotaKab)
                txtNamaKotaKabOld.Text = Safe(.NamaKotaKab)


                'other info
                Dim Omsuser As TList(Of User)
                Omsuser = DataRepository.UserProvider.GetByUserID(.CreatedBy)
                If Omsuser.Count > 0 Then
                    lblCreatedByOld.Text = Omsuser(0).UserName
                Else
                    lblCreatedByOld.Text = SafeDefaultValue
                End If
                lblCreatedDateNew.Text = FormatDate(.CreatedDate)
                Omsuser = DataRepository.UserProvider.GetByUserID(.LastUpdatedBy)
                If Omsuser.Count > 0 Then
                    lblUpdatedbyOld.Text = Omsuser(0).UserName
                Else
                    lblUpdatedbyOld.Text = SafeDefaultValue
                End If
                lblUpdatedDateOld.Text = FormatDate(.LastUpdatedDate)
                Dim l_objMappingMsKotaKabNCBSPPATK As TList(Of MappingMsKotaKabNCBSPPATK)
                'txtNamaKotaKabOld.Text = Safe(.NamaKotaKab)
                l_objMappingMsKotaKabNCBSPPATK = DataRepository.MappingMsKotaKabNCBSPPATKProvider.GetPaged(MappingMsKotaKabNCBSPPATKColumn.IdKotakab.ToString & _
                   "=" & _
                   PkObject, "", 0, Integer.MaxValue, Nothing)

                ListmapingOld.AddRange(l_objMappingMsKotaKabNCBSPPATK)
            End With
        End Using
    End Sub






    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            BindListMapping()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub BindListMapping()
        'bind new value
        LBMappingNew.DataSource = ListMappingDisplayNew '(0).PK_MappingMsBentukBidangUsahaNCBSPPATK_Id
        LBMappingNew.DataBind()
        'Bind OldValue
        LBmappingOld.DataSource = ListMappingDisplayOld
        LBmappingOld.DataBind()
    End Sub

#End Region

#Region "Property..."

    Public ReadOnly Property parID() As String
        Get
            Return Request.Item("ID")
        End Get
    End Property
    ''' <summary>
    ''' Menyimpan Item untuk mapping New sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ListmapingNew() As TList(Of MappingMsKotaKabNCBSPPATK_Approval_Detail)
        Get
            Return Session("ListmapingNew.data")
        End Get
        Set(ByVal value As TList(Of MappingMsKotaKabNCBSPPATK_Approval_Detail))
            Session("ListmapingNew.data") = value
        End Set
    End Property

    ''' <summary>
    ''' Menyimpan Item untuk mapping Old sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ListmapingOld() As TList(Of MappingMsKotaKabNCBSPPATK)
        Get
            Return Session("ListmapingOld.data")
        End Get
        Set(ByVal value As TList(Of MappingMsKotaKabNCBSPPATK))
            Session("ListmapingOld.data") = value
        End Set
    End Property

    ''' <summary>
    ''' List yang tampil di ListBox New
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplayNew() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingMsKotaKabNCBSPPATK_Approval_Detail In ListmapingNew.FindAllDistinct("IDKotaKabNCBS")
                Temp.Add(i.IDKotaKabNCBS.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property

    ''' <summary>
    ''' List yang tampil di ListBox Old
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplayOld() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingMsKotaKabNCBSPPATK In ListmapingOld.FindAllDistinct("IDKotaKabNCBS")
                Temp.Add(i.IdKotakabNCBS.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property
#End Region



End Class



