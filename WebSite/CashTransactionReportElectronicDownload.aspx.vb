Imports System.Configuration.ConfigurationManager
Partial Class CashTransactionReportElectronicDownload
    Inherits System.Web.UI.Page

    Private Function SplitByNumberOfCharacter(ByVal StrData As String, ByVal NumberOfCharacter As Integer) As String()
        Dim ArrStringReturn As String() = Nothing
        Dim NumberOfArray As Integer
        Dim Counter As Integer
        If StrData <> "" Then

            NumberOfArray = Math.Ceiling(StrData.Length / NumberOfCharacter)
            ReDim ArrStringReturn(NumberOfArray)
            For Counter = 0 To NumberOfArray - 1
                If (StrData.Length - (Counter * NumberOfCharacter)) >= NumberOfCharacter Then
                    ArrStringReturn(Counter) = StrData.Substring(Counter * NumberOfCharacter, NumberOfCharacter)
                Else
                    ArrStringReturn(Counter) = StrData.Substring(Counter * NumberOfCharacter)
                End If
            Next
        End If
        Return ArrStringReturn
    End Function

    Private Function SplitByNumberOfCharacterwithSpasi(ByVal StrData As String, ByVal NumberOfCharacter As Integer) As String()
        Dim ArrStringReturn As String() = Nothing
        Dim NumberOfArray As Integer
        Dim Counter As Integer
        If StrData <> "" Then

            NumberOfArray = Math.Ceiling(StrData.Length / NumberOfCharacter)
            ReDim ArrStringReturn(NumberOfArray)
            For Counter = 0 To NumberOfArray - 1
                If (StrData.Length - (Counter * NumberOfCharacter)) >= NumberOfCharacter Then
                    ArrStringReturn(Counter) = StrData.Substring(Counter * NumberOfCharacter, NumberOfCharacter)
                Else
                    ArrStringReturn(Counter) = StrData.Substring(Counter * NumberOfCharacter)
                End If
            Next
        End If
        Return ArrStringReturn
    End Function
    Private Function GenerateElectroniCTR(ByRef sw_write As System.IO.TextWriter, ByVal StrFileName As String, ByVal CreatedDate As DateTime, ByVal IsNewReport As Boolean) As Boolean
        Using CTRAdapter As New AMLDAL.CTRDataSetTableAdapters.CTRSelectTableAdapter
            Using CTRTable As New AMLDAL.CTRDataSet.CTRSelectDataTable
                CTRAdapter.FillByCreatedDate(CTRTable, CreatedDate)
                If Not CTRTable Is Nothing Then
                    If CTRTable.Rows.Count > 0 Then
                        Dim CTRTableRow As AMLDAL.CTRDataSet.CTRSelectRow
                        Dim CTRTransactionTableRow As AMLDAL.CTRDataSet.CTRTransactionSelectByFK_CTRIDRow

                        sw_write.WriteLine("A01:HR") ' TAG HEADER
                        sw_write.WriteLine(String.Format(":A02:{0}", AppSettings("CTRPJKCode"))) ' PJK Code Must be 6 Characters
                        sw_write.WriteLine(String.Format(":A03:{0}", StrFileName.Replace(".", ""))) ' Report file id must be 10 characters
                        Dim CTRCounter As Integer
                        CTRCounter = 1
                        For Each CTRTableRow In CTRTable.Rows
                            Using CTRTransactionAdapter As New AMLDAL.CTRDataSetTableAdapters.CTRTransactionSelectByFK_CTRIDTableAdapter
                                Using CTRTransactionTable As New AMLDAL.CTRDataSet.CTRTransactionSelectByFK_CTRIDDataTable
                                    CTRTransactionAdapter.FillByFK_CTRID(CTRTransactionTable, CTRTableRow.PK_CTRID)
                                    CTRTransactionTableRow = Nothing
                                    If Not CTRTransactionTable Is Nothing Then
                                        If CTRTransactionTable.Rows.Count > 0 Then
                                            CTRTransactionTableRow = CTRTransactionTable.Rows(0)
                                        End If
                                    End If
                                    sw_write.WriteLine(String.Format("=B01:{0}", "CE")) ' Report origin code 2an
                                    If IsNewReport Then
                                        sw_write.WriteLine(String.Format(":B02:{0}", "N")) 'Report type code 1a; N indicated New Report
                                        sw_write.WriteLine(String.Format(":B03:{0}", ""))
                                    Else
                                        sw_write.WriteLine(String.Format(":B02:{0}", "C")) 'Report type code 1a; C indicated Correction Report
                                        If Not CTRTableRow.IsPPATKConfirmationNoNull Then
                                            sw_write.WriteLine(String.Format(":B03:{0}", CTRTableRow.PPATKConfirmationNo))
                                        Else
                                            sw_write.WriteLine(String.Format(":B02:{0}", "N"))
                                            sw_write.WriteLine(String.Format(":B03:{0}", ""))
                                        End If
                                    End If
                                    '''''''''''''''''''''''''''''
                                    ' General Transaction Details
                                    '''''''''''''''''''''''''''''
                                    ' Reporting branch name
                                    If Not CTRTableRow.IsAccountOwnerNameNull Then
                                        sw_write.WriteLine(String.Format("=M01:{0}", CTRTableRow.AccountOwnerName.Trim().Replace(":", " ")))
                                    Else
                                        sw_write.WriteLine("=M01:")
                                    End If
                                    ' Reporting branch address
                                    If Not CTRTableRow.IsAccountOwnerDescriptionNull Then
                                        Dim ArrAccountOwnerDescription As String()
                                        ArrAccountOwnerDescription = SplitByNumberOfCharacter(CTRTableRow.AccountOwnerDescription.Trim().Replace(":", " "), 35)
                                        If Not ArrAccountOwnerDescription Is Nothing Then
                                            If ArrAccountOwnerDescription.Length > 0 Then
                                                sw_write.WriteLine(String.Format(":M02:{0}", ArrAccountOwnerDescription(0)))
                                                For Counter As Integer = 1 To ArrAccountOwnerDescription.Length - 1
                                                    If ArrAccountOwnerDescription(Counter) <> "" Then
                                                        sw_write.WriteLine(ArrAccountOwnerDescription(Counter))
                                                    End If
                                                Next
                                            End If
                                        End If
                                    Else
                                        sw_write.WriteLine(":M02:")
                                    End If
                                    ' Reporting Officer Name
                                    sw_write.WriteLine(String.Format(":M03:{0}", AppSettings("CTRReportingOfficer")))
                                    'Transaction date
                                    If Not CTRTableRow.IsCreatedDateNull Then
                                        sw_write.WriteLine(String.Format(":M04:{0}", CTRTableRow.CreatedDate.ToString("yyyyMMdd")))
                                    Else
                                        sw_write.WriteLine(":M04:")
                                    End If
                                    'Transaction type code : DP=Deposit, WD=Withdrawal
                                    'Transaction type description
                                    If Not CTRTransactionTableRow Is Nothing Then
                                        If Not CTRTransactionTableRow.IsDebitORCreditNull Then
                                            If CTRTransactionTableRow.DebitORCredit.ToUpper() = "C" Then
                                                sw_write.WriteLine(":M05:DP")
                                                sw_write.WriteLine(":M06:Deposit")
                                            Else
                                                sw_write.WriteLine(":M05:WD")
                                                sw_write.WriteLine(":M06:Withdrawal")
                                            End If
                                        Else
                                            sw_write.WriteLine(":M05:")
                                            sw_write.WriteLine(":M06:")
                                        End If
                                    Else
                                        sw_write.WriteLine(":M05:")
                                        sw_write.WriteLine(":M06:")
                                    End If
                                    'Total cash amount in Rupiah
                                    If Not CTRTableRow.IsTotalTransactionValueNull Then
                                        sw_write.WriteLine(String.Format(":M07:{0}", CTRTableRow.TotalTransactionValue.ToString("0.00")))
                                    Else
                                        sw_write.WriteLine(":M07:")
                                    End If
                                    '''''''''''''''''''''''''
                                    'Foreign Currency Details
                                    '''''''''''''''''''''''''
                                    If Not CTRTransactionTable Is Nothing Then
                                        If CTRTransactionTable.Rows.Count > 0 Then
                                            Dim CurrencyCTRTransactionTableRow As AMLDAL.CTRDataSet.CTRTransactionSelectByFK_CTRIDRow
                                            For Each CurrencyCTRTransactionTableRow In CTRTransactionTable.Rows
                                                If Not CurrencyCTRTransactionTableRow.IsCurrencyTypeNull Then
                                                    sw_write.WriteLine(String.Format("=N01:{0}", CurrencyCTRTransactionTableRow.CurrencyType.Trim()))
                                                End If
                                                If Not CurrencyCTRTransactionTableRow.IsTransactionAmountNull Then
                                                    sw_write.WriteLine(String.Format(":N02:{0}", CurrencyCTRTransactionTableRow.TransactionAmount.ToString("0.00")))
                                                End If
                                                If Not CurrencyCTRTransactionTableRow.IsTransactionExchangeRateNull Then
                                                    sw_write.WriteLine(String.Format(":N03:{0}", CurrencyCTRTransactionTableRow.TransactionExchangeRate.ToString("0.00")))
                                                End If
                                            Next
                                        End If
                                    End If
                                    '''''''''''''''''''''''''
                                    'Party Details
                                    '''''''''''''''''''''''''
                                    'P01 Party role code 1a
                                    'P Person conducting the transaction (A gent), i.e.the person(s) at the counter.
                                    'Q Person on whose behalf transaction is being conducted (Beneficial Owner/Walk In Customer).
                                    ' TODO: Update Party Role Code to support Walk In Customer
                                    Dim StrPartyRoleCode As String
                                    StrPartyRoleCode = "P"
                                    sw_write.WriteLine(String.Format("=P01:{0}", StrPartyRoleCode))
                                    'P02 Name 70x
                                    If Not CTRTableRow.IsCustomerNameNull Then
                                        sw_write.WriteLine(String.Format(":P02:{0}", CTRTableRow.CustomerName.Trim().Replace(":", " ")))
                                        'Else
                                        '    sw_write.WriteLine(":P02:")
                                    End If
                                    'P03 Tax Number 30x
                                    Using CTRCustomerIdentificationNoAdapter As New AMLDAL.CTRDataSetTableAdapters.CTRGetCustomerIDNumberByCIFNOTableAdapter
                                        Using CTRCustomerIdentificationNoTable As New AMLDAL.CTRDataSet.CTRGetCustomerIDNumberByCIFNODataTable
                                            CTRCustomerIdentificationNoAdapter.Fill(CTRCustomerIdentificationNoTable, CTRTableRow.CIFNo)
                                            If Not CTRCustomerIdentificationNoTable Is Nothing Then
                                                Dim CTRCustomerIdentificationNoTableRows As AMLDAL.CTRDataSet.CTRGetCustomerIDNumberByCIFNORow()
                                                CTRCustomerIdentificationNoTableRows = CTRCustomerIdentificationNoTable.Select("CFSSCD='NP'")
                                                If Not CTRCustomerIdentificationNoTableRows Is Nothing Then
                                                    If CTRCustomerIdentificationNoTableRows.Length > 0 Then
                                                        If Not CTRCustomerIdentificationNoTableRows(0).IsCFSSNONull Then
                                                            sw_write.WriteLine(String.Format(":P03:{0}", CTRCustomerIdentificationNoTableRows(0).CFSSNO.Trim().Replace(":", " ")))
                                                            'Else
                                                            '    sw_write.WriteLine(":P03:")
                                                        End If
                                                        'Else
                                                        '    sw_write.WriteLine(":P03:")
                                                    End If
                                                    'Else
                                                    '    sw_write.WriteLine(":P03:")
                                                End If
                                                'Else
                                                '    sw_write.WriteLine(":P03:")
                                            End If
                                        End Using
                                    End Using

                                    Using CTRCustomerAddressAdapter As New AMLDAL.CTRDataSetTableAdapters.CTRGetCustomerAddressByCIFNoTableAdapter
                                        Using CTRCustomerAddressTable As New AMLDAL.CTRDataSet.CTRGetCustomerAddressByCIFNoDataTable
                                            CTRCustomerAddressAdapter.Fill(CTRCustomerAddressTable, CTRTableRow.CIFNo)
                                            If Not CTRCustomerAddressTable Is Nothing AndAlso CTRCustomerAddressTable.Rows.Count > 0 Then
                                                Dim CTRCustomerAddressTableRow As AMLDAL.CTRDataSet.CTRGetCustomerAddressByCIFNoRow
                                                CTRCustomerAddressTableRow = CTRCustomerAddressTable.Rows(0)
                                                'P04 Address 4*35x
                                                Dim StrP04Address As String = ""

                                                If Not CTRCustomerAddressTableRow.IsCFNA2Null Then
                                                    If Not CTRCustomerAddressTableRow.IsCFNA3Null Then
                                                        StrP04Address = CTRCustomerAddressTableRow.CFNA2.Trim() & " " & CTRCustomerAddressTableRow.CFNA3.Trim()
                                                        StrP04Address = StrP04Address.Replace(":", " ")
                                                        Dim ArrStrP04Address As String()
                                                        ArrStrP04Address = SplitByNumberOfCharacter(StrP04Address, 35)
                                                        If Not ArrStrP04Address Is Nothing Then
                                                            If ArrStrP04Address.Length > 0 Then
                                                                sw_write.WriteLine(String.Format(":P04:{0}", ArrStrP04Address(0)))
                                                                For Counter As Integer = 1 To ArrStrP04Address.Length - 1
                                                                    If Not ArrStrP04Address(Counter) Is Nothing Then
                                                                        If ArrStrP04Address(Counter).Trim() <> "" Then
                                                                            sw_write.WriteLine(ArrStrP04Address(Counter))
                                                                        End If
                                                                    End If
                                                                Next
                                                            End If
                                                        End If
                                                    Else
                                                        sw_write.WriteLine(String.Format(":P04:{0}", CTRCustomerAddressTableRow.CFNA2.Trim().Replace(":", " ")))
                                                    End If
                                                Else
                                                    If Not CTRCustomerAddressTableRow.IsCFNA3Null Then
                                                        sw_write.WriteLine(String.Format(":P04:{0}", CTRCustomerAddressTableRow.CFNA3.Trim().Replace(":", " ")))
                                                        'Else
                                                        '    sw_write.WriteLine(":P04:")
                                                    End If
                                                End If
                                                'P05 City 50x
                                                If Not CTRCustomerAddressTableRow.IsCFNA4Null Then
                                                    sw_write.WriteLine(String.Format(":P05:{0}", CTRCustomerAddressTableRow.CFNA4.Trim().Replace(":", " ")))
                                                    'Else
                                                    '    sw_write.WriteLine(":P05:")
                                                End If
                                                'P06 Provence 50x
                                                If Not CTRCustomerAddressTableRow.IsCFNA5Null Then
                                                    If Not CTRCustomerAddressTableRow.IsCFZIPNull Then
                                                        sw_write.WriteLine(String.Format(":P06:{0} {1}", CTRCustomerAddressTableRow.CFNA5.Trim().Replace(":", " "), CTRCustomerAddressTableRow.CFZIP.Substring(0, 5).Replace(":", " ")))
                                                    Else
                                                        sw_write.WriteLine(String.Format(":P06:{0}", CTRCustomerAddressTableRow.CFNA5.Trim().Replace(":", " ")))
                                                    End If
                                                Else
                                                    If Not CTRCustomerAddressTableRow.IsCFZIPNull Then
                                                        sw_write.WriteLine(String.Format(":P06:{0}", CTRCustomerAddressTableRow.CFZIP.Substring(0, 5).Replace(":", " ")))
                                                        'Else
                                                        '    sw_write.WriteLine(":P06:")
                                                    End If
                                                End If
                                                'Else
                                                '    sw_write.WriteLine(":P04:")
                                                '    sw_write.WriteLine(":P05:")
                                                '    sw_write.WriteLine(":P06:")
                                            End If
                                        End Using
                                    End Using
                                    Using CTRCustomerInformationAdapter As New AMLDAL.CTRDataSetTableAdapters.CTRGetCustomerInformationByCIFNoTableAdapter
                                        Using CTRCustomerInformationTable As New AMLDAL.CTRDataSet.CTRGetCustomerInformationByCIFNoDataTable
                                            CTRCustomerInformationAdapter.Fill(CTRCustomerInformationTable, CTRTableRow.CIFNo)
                                            If Not CTRCustomerInformationTable Is Nothing AndAlso CTRCustomerInformationTable.Rows.Count > 0 Then
                                                Dim CTRCustomerInformationTableRow As AMLDAL.CTRDataSet.CTRGetCustomerInformationByCIFNoRow
                                                CTRCustomerInformationTableRow = CTRCustomerInformationTable.Rows(0)
                                                'P07 Business/occupation description 40x
                                                If Not CTRCustomerInformationTableRow.IsIndustryCodeDescriptionNull Then
                                                    sw_write.WriteLine(String.Format(":P07:{0}", CTRCustomerInformationTableRow.IndustryCodeDescription.Trim().Replace(":", " ")))
                                                    'Else
                                                    '    sw_write.WriteLine(":P07:")
                                                End If
                                                'P08 Date of birth ccyymmdd
                                                If Not CTRCustomerInformationTableRow.IsBirthDateNull Then
                                                    sw_write.WriteLine(String.Format(":P08:{0}", CTRCustomerInformationTableRow.BirthDate.ToString("yyyyMMdd")))
                                                    'Else
                                                    '    sw_write.WriteLine(":P08:")
                                                End If
                                                'Else
                                                '    sw_write.WriteLine(":P07:")
                                                '    sw_write.WriteLine(":P08:")
                                            End If
                                        End Using
                                    End Using
                                    If StrPartyRoleCode = "P" Then
                                        'P09 Account Number 35x
                                        If Not CTRTransactionTableRow.IsAccountNoNull Then
                                            sw_write.WriteLine(String.Format(":P09:{0}", CTRTransactionTableRow.AccountNo.ToString().Trim()))
                                        End If
                                        'B Credit card account
                                        'C Cheque account
                                        'L Loan account
                                        'M Multiple account table
                                        'O Other account
                                        'S Savings account
                                        'T Term deposit account
                                        'P10 Account Type Code 1a
                                        If Not CTRTransactionTableRow.IsAccountTypeNull Then
                                            Select Case CTRTransactionTableRow.AccountType
                                                Case "S"
                                                    sw_write.WriteLine(":P10:S")
                                                Case "T"
                                                    sw_write.WriteLine(":P10:T")
                                                Case Else
                                                    sw_write.WriteLine(":P10:O")
                                            End Select
                                        End If
                                        'P11 Account title 70x
                                        If Not CTRTransactionTableRow.IsAccountNameNull Then
                                            sw_write.WriteLine(String.Format(":P11:{0}", CTRTransactionTableRow.AccountName.Trim().Replace(":", " ")))
                                        End If
                                    End If
                                    ''''''''''''''''''''''''''''
                                    'Identification Type Details
                                    ''''''''''''''''''''''''''''
                                    Using CTRCustomerIdentificationNoAdapter As New AMLDAL.CTRDataSetTableAdapters.CTRGetCustomerIDNumberByCIFNOTableAdapter
                                        Using CTRCustomerIdentificationNoTable As New AMLDAL.CTRDataSet.CTRGetCustomerIDNumberByCIFNODataTable
                                            CTRCustomerIdentificationNoAdapter.Fill(CTRCustomerIdentificationNoTable, CTRTableRow.CIFNo)
                                            If Not CTRCustomerIdentificationNoTable Is Nothing Then
                                                Dim CTRCustomerIdentificationNoTableRow As AMLDAL.CTRDataSet.CTRGetCustomerIDNumberByCIFNORow
                                                For Each CTRCustomerIdentificationNoTableRow In CTRCustomerIdentificationNoTable.Rows
                                                    'Q01 Identification type code 1a
                                                    If Not CTRCustomerIdentificationNoTableRow.IsCFSSCDNull Then
                                                        Select Case CTRCustomerIdentificationNoTableRow.CFSSCD
                                                            Case "KT" ' KTP
                                                                sw_write.WriteLine("=Q01:K")
                                                            Case "PP" ' PASSPORT
                                                                sw_write.WriteLine("=Q01:P")
                                                            Case "SM" ' SIM
                                                                sw_write.WriteLine("=Q01:S")
                                                            Case "KI" ' KIMS/KITAS/KITAP
                                                                sw_write.WriteLine("=Q01:M")
                                                            Case "KP" ' Kartu Pelajar
                                                                sw_write.WriteLine("=Q01:U")
                                                            Case Else
                                                                sw_write.WriteLine("=Q01:L")
                                                        End Select
                                                    End If
                                                    'Q02 Identification number 25x
                                                    If Not CTRCustomerIdentificationNoTableRow.IsCFSSNONull Then
                                                        sw_write.WriteLine(String.Format(":Q02:{0}", CTRCustomerIdentificationNoTableRow.CFSSNO.Trim().Replace(":", " ")))
                                                    End If
                                                Next
                                            End If
                                        End Using
                                    End Using
                                    ''''''''''''''''''''''''''''
                                    'Other Account Involve
                                    ''''''''''''''''''''''''''''
                                    'O U01 Financial Institution Name 30x
                                    'sw_write.WriteLine("=U01:") 'Financial Institution Name
                                    'O U02 Financial Instituation Address 4*35x
                                    'sw_write.WriteLine(":U02:") 'Financial Institution Address
                                    'O U03 Account Number 35x
                                    'sw_write.WriteLine(":U03:") 'Account Number
                                    'O U04 Recipients Name 30x
                                    'sw_write.WriteLine(":U04:") 'Recipients Name

                                    ''''''''''''''''''''''''''''
                                    'Free format text details (V)
                                    'V02 Additional cash dealer text n*72x
                                    ''''''''''''''''''''''''''''
                                    If Not CTRTransactionTable Is Nothing Then
                                        If CTRTransactionTable.Rows.Count > 0 Then
                                            Dim AccountCTRTransactionTableRow As AMLDAL.CTRDataSet.CTRTransactionSelectByFK_CTRIDRow
                                            Dim ArrAccount As New ArrayList
                                            Dim StrAccount As String
                                            For Each AccountCTRTransactionTableRow In CTRTransactionTable.Rows
                                                If Not ArrAccount.Contains(AccountCTRTransactionTableRow.AccountNo) Then
                                                    ArrAccount.Add(AccountCTRTransactionTableRow.AccountNo)
                                                End If
                                            Next
                                            If ArrAccount.Count > 0 Then
                                                Dim StrJointAccountText As String = ""
                                                For Counter As Integer = 0 To ArrAccount.Count - 1
                                                    StrAccount = ArrAccount(Counter)
                                                    'Get Related Joint Account CIF
                                                    Using TaJointAccount As New AMLDAL.CTRDataSetTableAdapters.CTRJointAccountTableAdapter
                                                        Using DtJointAccount As New AMLDAL.CTRDataSet.CTRJointAccountDataTable
                                                            TaJointAccount.Fill(DtJointAccount, StrAccount)
                                                            If DtJointAccount.Rows.Count > 0 Then
                                                                Dim DrJointAccount As AMLDAL.CTRDataSet.CTRJointAccountRow
                                                                If StrJointAccountText = "" Then
                                                                    StrJointAccountText = "Account " & StrAccount & " is Joint Account of "
                                                                Else
                                                                    StrJointAccountText = StrJointAccountText & vbCrLf & "Account " & StrAccount & " is Joint Account of "
                                                                End If

                                                                For Each DrJointAccount In DtJointAccount.Rows
                                                                    If Not DrJointAccount.IsCustomerNameNull Then
                                                                        Dim strnama As String = DrJointAccount.CustomerName
                                                                        If strnama.Length > 72 Then
                                                                            StrJointAccountText = StrJointAccountText & vbCrLf & " " & strnama.Substring(0, 72)
                                                                            StrJointAccountText = StrJointAccountText & vbCrLf & " " & strnama.Substring(72, strnama.Length - 1)
                                                                        Else
                                                                            StrJointAccountText = StrJointAccountText & vbCrLf & " " & strnama
                                                                        End If



                                                                    End If
                                                                Next

                                                            End If
                                                        End Using
                                                    End Using
                                                Next
                                                If StrJointAccountText <> "" Then


                                                    sw_write.WriteLine(String.Format("=V02:{0}", StrJointAccountText)) 'Additional Cash Dealer Text

                                                    'Dim ArrJointAccountText As String()
                                                    'ArrJointAccountText = SplitByNumberOfCharacterwithSpasi(StrJointAccountText, 72)
                                                    'If Not ArrJointAccountText Is Nothing Then
                                                    '    If ArrJointAccountText.Length > 0 Then
                                                    '        sw_write.WriteLine(String.Format("=V02:{0}", ArrJointAccountText(0))) 'Additional Cash Dealer Text
                                                    '        For JointAccountCounter As Integer = 1 To ArrJointAccountText.Length - 1
                                                    '            If Not ArrJointAccountText(JointAccountCounter) Is Nothing Then
                                                    '                If ArrJointAccountText(JointAccountCounter).Trim() <> "" Then
                                                    '                    sw_write.WriteLine(ArrJointAccountText(JointAccountCounter))
                                                    '                End If
                                                    '            End If
                                                    '        Next
                                                    '    End If
                                                    'End If
                                                End If
                                            End If
                                        End If
                                    End If
                                    'sw_write.WriteLine("=V02:") 'Additional Cash Dealer Text

                                    ' Changed by Johan 10:07 AM 11/19/2008
                                    ' Cause changes in PPATK website
                                    If CTRCounter < CTRTable.Rows.Count Then
                                        sw_write.WriteLine("=")
                                    End If
                                    CTRCounter += 1
                                End Using
                            End Using
                        Next
                        ''''''''''''''''''''''''''''
                        'CTR File Trailer Record
                        ''''''''''''''''''''''''''''
                        'M Z01 Header code TR
                        sw_write.WriteLine("=Z01:TR")
                        'M Z02 No of CTR Reports in this file 6n
                        sw_write.WriteLine(String.Format(":Z02:{0}", CTRTable.Rows.Count))
                        ' = End of Block
                        sw_write.Write("=")
                    End If
                End If
            End Using
        End Using
        Return True
    End Function




    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim CTRCreatedDate As DateTime
        CTRCreatedDate = Me.Request.Params.Item("CreatedDate")
        Response.Clear()
        Dim StrFileName As String = ""
        Dim IsNewReport As Boolean = True
        Dim FileCounter As Integer = 1
        StrFileName = "C" & CTRCreatedDate.ToString("yy") & CTRCreatedDate.ToString("MM") & CTRCreatedDate.ToString("dd") & "." & FileCounter.ToString("000")
        Using CTRPPATKAdapter As New AMLDAL.CTRDataSetTableAdapters.CTRPPATKSelectTableAdapter
            Using CTRPPATKTable As New AMLDAL.CTRDataSet.CTRPPATKSelectDataTable
                CTRPPATKAdapter.FillByCreatedDate(CTRPPATKTable, CTRCreatedDate.ToString("yyyy-MM-dd"))
                If Not CTRPPATKTable Is Nothing AndAlso CTRPPATKTable.Rows.Count > 0 Then
                    Dim CTRPPATKTableRow As AMLDAL.CTRDataSet.CTRPPATKSelectRow
                    CTRPPATKTableRow = CTRPPATKTable.Rows(0)

                    Using CTRAdapter As New AMLDAL.CTRDataSetTableAdapters.CTRSelectTableAdapter
                        Using CTRTable As New AMLDAL.CTRDataSet.CTRSelectDataTable
                            CTRAdapter.FillByCreatedDate(CTRTable, CTRCreatedDate)
                            If Not CTRTable Is Nothing Then
                                If CTRTable.Rows.Count > 0 Then
                                    Dim CTRTableRow As AMLDAL.CTRDataSet.CTRSelectRow
                                    CTRTableRow = CTRTable.Rows(0)
                                    If Not CTRTableRow.IsPPATKConfirmationNoNull Then
                                        If CTRTableRow.PPATKConfirmationNo <> "" Then
                                            If Not CTRPPATKTableRow.IsCounterNull Then
                                                FileCounter = CTRPPATKTableRow.Counter
                                                FileCounter = FileCounter + 1
                                                IsNewReport = False
                                                StrFileName = "C" & CTRCreatedDate.ToString("yy") & CTRCreatedDate.ToString("MM") & CTRCreatedDate.ToString("dd") & "." & FileCounter.ToString("000")
                                                CTRPPATKTableRow.Counter = FileCounter
                                                CTRPPATKTableRow.FileName = StrFileName
                                                CTRPPATKTableRow.ReportedBy = Sahassa.AML.Commonly.SessionUserId
                                                CTRPPATKAdapter.Update(CTRPPATKTableRow)
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        End Using
                    End Using
                Else
                    FileCounter = 1
                    IsNewReport = True
                    StrFileName = "C" & CTRCreatedDate.ToString("yy") & CTRCreatedDate.ToString("MM") & CTRCreatedDate.ToString("dd") & "." & FileCounter.ToString("000")
                    CTRPPATKAdapter.Insert(CTRCreatedDate, "", Sahassa.AML.Commonly.SessionUserId, "", FileCounter, StrFileName)
                End If
            End Using
        End Using
        Response.AddHeader("content-disposition", "attachment;filename=" & StrFileName)
        Response.ContentType = "text/plain"
        Dim sw_write As System.IO.TextWriter
        sw_write = Response.Output
        Me.GenerateElectroniCTR(sw_write, StrFileName, CTRCreatedDate, IsNewReport)

        Response.End()
    End Sub
End Class
