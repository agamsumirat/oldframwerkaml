#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports Sahassa.AML
Imports System.Collections.Generic
#End Region

Partial Class MsKotaKab_APPROVAL_Detail
    Inherits Parent

#Region "Function"

    Sub HideControl()
        imgReject.Visible = False
        imgApprove.Visible = False
        ImageCancel.Visible = False
    End Sub

    Sub ChangeMultiView(ByVal index As Integer)
        MtvMsUser.ActiveViewIndex = index
    End Sub

#End Region

#Region "events..."

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Response.Redirect("MsKotaKab_Approval_View.aspx")
    End Sub

    Protected Sub imgReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgReject.Click
        Try
            Rejected()
            ChangeMultiView(1)
            LblConfirmation.Text = "Data has been rejected"
            HideControl()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgApprove_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgApprove.Click
        Try
            Using ObjMsKotaKab_Approval As MsKotaKab_Approval = DataRepository.MsKotaKab_ApprovalProvider.GetByPK_MsKotaKab_Approval_Id(parID)
                If ObjMsKotaKab_Approval.FK_MsMode_Id.GetValueOrDefault = 1 Then
                    Inserdata(ObjMsKotaKab_Approval)
                ElseIf ObjMsKotaKab_Approval.FK_MsMode_Id.GetValueOrDefault = 2 Then
                    UpdateData(ObjMsKotaKab_Approval)
                ElseIf ObjMsKotaKab_Approval.FK_MsMode_Id.GetValueOrDefault = 3 Then
                    DeleteData(ObjMsKotaKab_Approval)
                End If
            End Using
            ChangeMultiView(1)
            LblConfirmation.Text = "Data approved successful"
            HideControl()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                'AuditTrailBLL.InsertAuditTrailUserAccess(SessionCurrentPage)
                ListmapingNew = New TList(Of MappingMsKotaKabNCBSPPATK_Approval_Detail)
                ListmapingOld = New TList(Of MappingMsKotaKabNCBSPPATK)
                LoadData()
            Catch ex As Exception
                LogError(ex)
                CvalPageErr.IsValid = False
                CvalPageErr.ErrorMessage = ex.Message
            End Try
        End If

    End Sub

    Private Sub LoadData()
        Dim PkObject As String
        'Load new data
        Using ObjMsKotaKab_ApprovalDetail As MsKotaKab_ApprovalDetail = DataRepository.MsKotaKab_ApprovalDetailProvider.GetPaged(MsKotaKab_ApprovalDetailColumn.FK_MsKotaKab_Approval_Id.ToString & _
         "=" & _
         parID, "", 0, 1, Nothing)(0)
            Dim CekMode As MsKotaKab_Approval = DataRepository.MsKotaKab_ApprovalProvider.GetByPK_MsKotaKab_Approval_Id(ObjMsKotaKab_ApprovalDetail.FK_MsKotaKab_Approval_Id)
            Dim nama As String = CekMode.FK_MsMode_Id
            If nama = 1 Then
                baru.Visible = True
                lama.Visible = False
                baruNew.Visible = True
                lamaOld.Visible = False
            ElseIf nama = 2 Then
                baru.Visible = True
                lama.Visible = True
                baruNew.Visible = True
                lamaOld.Visible = True
            ElseIf nama = 3 Then
                baru.Visible = True
                lama.Visible = False
                baruNew.Visible = True
                lamaOld.Visible = False
            ElseIf nama = 4 Then
                baru.Visible = True
                lama.Visible = False
                baruNew.Visible = True
                lamaOld.Visible = False
            End If
            With ObjMsKotaKab_ApprovalDetail
                SafeDefaultValue = "-"
                HFProvinceNew.Value = Safe(.IDPropinsi)
                Dim OProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(.IDPropinsi.GetValueOrDefault)
                If OProvince IsNot Nothing Then LBSearchNamaNew.Text = Safe(OProvince.Nama)

                txtIDKotaKabnew.Text = Safe(.IDKotaKab)
                txtNamaKotaKabnew.Text = Safe(.NamaKotaKab)

                'other info
                Dim Omsuser As TList(Of User)
                Omsuser = DataRepository.UserProvider.GetPaged(UserColumn.UserID.ToString & "='" & .CreatedBy & "'", "", 0, Integer.MaxValue, 0)
                If Omsuser.Count > 0 Then
                    lblCreatedByNew.Text = Omsuser(0).UserName
                Else
                    lblCreatedByNew.Text = SafeDefaultValue
                End If
                lblCreatedDateNew.Text = FormatDate(.CreatedDate)
                Omsuser = DataRepository.UserProvider.GetPaged(UserColumn.UserID.ToString & "='" & .LastUpdatedBy & "'", "", 0, Integer.MaxValue, 0)
                If Omsuser.Count > 0 Then
                    lblUpdatedbyNew.Text = Omsuser(0).UserName
                Else
                    lblUpdatedbyNew.Text = SafeDefaultValue
                End If
                lblUpdatedDateNew.Text = FormatDate(.LastUpdatedDate)
                txtActivationNew.Text = SafeActiveInactive(.Activation)
                Dim L_objMappingMsKotaKabNCBSPPATK_Approval_Detail As TList(Of MappingMsKotaKabNCBSPPATK_Approval_Detail)
                L_objMappingMsKotaKabNCBSPPATK_Approval_Detail = DataRepository.MappingMsKotaKabNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsKotaKabNCBSPPATK_Approval_DetailColumn.PK_MsKotaKab_Approval_Id.ToString & _
                 "=" & _
                 parID, "", 0, Integer.MaxValue, Nothing)
                'Add mapping
                ListmapingNew.AddRange(L_objMappingMsKotaKabNCBSPPATK_Approval_Detail)

            End With
            PkObject = ObjMsKotaKab_ApprovalDetail.IDKotaKab
        End Using

        'Load Old Data
        Using ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(PkObject)
            If ObjMsKotaKab Is Nothing Then Return
            With ObjMsKotaKab
                SafeDefaultValue = "-"
                HFProvinceOld.Value = Safe(.IDPropinsi)
                Dim OProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(.IDPropinsi.GetValueOrDefault)
                If OProvince IsNot Nothing Then LBSearchNamaOld.Text = Safe(OProvince.Nama)

                txtIDKotaKabOld.Text = Safe(.IDKotaKab)
                txtNamaKotaKabOld.Text = Safe(.NamaKotaKab)

                'other info
                Dim Omsuser As TList(Of User)
                Omsuser = DataRepository.UserProvider.GetPaged(UserColumn.UserID.ToString & "='" & .CreatedBy & "'", "", 0, Integer.MaxValue, 0)
                If Omsuser.Count > 0 Then
                    lblCreatedByOld.Text = Omsuser(0).UserName
                Else
                    lblCreatedByOld.Text = SafeDefaultValue
                End If
                lblCreatedDateNew.Text = FormatDate(.CreatedDate)
                Omsuser = DataRepository.UserProvider.GetPaged(UserColumn.UserID.ToString & "='" & .LastUpdatedBy & "'", "", 0, Integer.MaxValue, 0)
                If Omsuser.Count > 0 Then
                    lblUpdatedbyOld.Text = Omsuser(0).UserName
                Else
                    lblUpdatedbyOld.Text = SafeDefaultValue
                End If
                lblUpdatedDateOld.Text = FormatDate(.LastUpdatedDate)
                Dim L_objMappingMsKotaKabNCBSPPATK As TList(Of MappingMsKotaKabNCBSPPATK)
                txtActivationOld.Text = SafeActiveInactive(.Activation)
                L_objMappingMsKotaKabNCBSPPATK = DataRepository.MappingMsKotaKabNCBSPPATKProvider.GetPaged(MappingMsKotaKabNCBSPPATKColumn.IdKotakab.ToString & _
                   "=" & _
                   PkObject, "", 0, Integer.MaxValue, Nothing)
                ListmapingOld.AddRange(L_objMappingMsKotaKabNCBSPPATK)
            End With
        End Using
    End Sub



    Protected Sub imgBtnConfirmation_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnConfirmation.Click
        Try
            Response.Redirect("MsKotaKab_approval_view.aspx")
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub


    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            BindListMapping()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub BindListMapping()
        'bind new value
        LBMappingNew.DataSource = ListMappingDisplayNew
        LBMappingNew.DataBind()
        'Bind OldValue
        LBmappingOld.DataSource = ListMappingDisplayOld
        LBmappingOld.DataBind()
    End Sub

#End Region

#Region "Property..."

    Public ReadOnly Property parID() As String
        Get
            Return Request.Item("ID")
        End Get
    End Property
    ''' <summary>
    ''' Menyimpan Item untuk mapping New sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ListmapingNew() As TList(Of MappingMsKotaKabNCBSPPATK_Approval_Detail)
        Get
            Return Session("ListmapingNew.data")
        End Get
        Set(ByVal value As TList(Of MappingMsKotaKabNCBSPPATK_Approval_Detail))
            Session("ListmapingNew.data") = value
        End Set
    End Property

    ''' <summary>
    ''' Menyimpan Item untuk mapping Old sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ListmapingOld() As TList(Of MappingMsKotaKabNCBSPPATK)
        Get
            Return Session("ListmapingOld.data")
        End Get
        Set(ByVal value As TList(Of MappingMsKotaKabNCBSPPATK))
            Session("ListmapingOld.data") = value
        End Set
    End Property

    ''' <summary>
    ''' List yang tampil di ListBox
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplayNew() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingMsKotaKabNCBSPPATK_Approval_Detail In ListmapingNew.FindAllDistinct("idKotakabNCBS")
                Temp.Add(i.IDKotaKabNCBS.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property

    ''' <summary>
    ''' List yang tampil di ListBox
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplayOld() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingMsKotaKabNCBSPPATK In ListmapingOld.FindAllDistinct("idKotakabNCBS")
                Temp.Add(i.IdKotakabNCBS.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property

#End Region

#Region "Action Approval..."

    Sub Rejected()
        Dim ObjMsKotaKab_Approval As MsKotaKab_Approval = DataRepository.MsKotaKab_ApprovalProvider.GetByPK_MsKotaKab_Approval_Id(parID)
        Dim ObjMsKotaKab_ApprovalDetail As MsKotaKab_ApprovalDetail = DataRepository.MsKotaKab_ApprovalDetailProvider.GetPaged(MsKotaKab_ApprovalDetailColumn.FK_MsKotaKab_Approval_Id.ToString & "=" & ObjMsKotaKab_Approval.PK_MsKotaKab_Approval_Id, "", 0, 1, Nothing)(0)
        Dim L_ObjMappingMsKotaKabNCBSPPATK_Approval_Detail As TList(Of MappingMsKotaKabNCBSPPATK_Approval_Detail) = DataRepository.MappingMsKotaKabNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsKotaKabNCBSPPATK_Approval_DetailColumn.PK_MsKotaKab_Approval_Id.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)
        DeleteApproval(ObjMsKotaKab_Approval, ObjMsKotaKab_ApprovalDetail, L_ObjMappingMsKotaKabNCBSPPATK_Approval_Detail)
    End Sub

    Sub DeleteApproval(ByRef objMsKotaKab_Approval As MsKotaKab_Approval, ByRef ObjMsKotaKab_ApprovalDetail As MsKotaKab_ApprovalDetail, ByRef L_ObjMappingMsKotaKabNCBSPPATK_Approval_Detail As TList(Of MappingMsKotaKabNCBSPPATK_Approval_Detail))
        DataRepository.MsKotaKab_ApprovalProvider.Delete(objMsKotaKab_Approval)
        DataRepository.MsKotaKab_ApprovalDetailProvider.Delete(ObjMsKotaKab_ApprovalDetail)
        DataRepository.MappingMsKotaKabNCBSPPATK_Approval_DetailProvider.Delete(L_ObjMappingMsKotaKabNCBSPPATK_Approval_Detail)
    End Sub

    ''' <summary>
    ''' Delete Data dari  asli dari approval
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub DeleteData(ByVal objMsKotaKab_Approval As MsKotaKab_Approval)
        '   '================= Delete Header ===========================================================
        Dim LObjMsKotaKab_ApprovalDetail As TList(Of MsKotaKab_ApprovalDetail) = DataRepository.MsKotaKab_ApprovalDetailProvider.GetPaged(MsKotaKab_ApprovalDetailColumn.FK_MsKotaKab_Approval_Id.ToString & "=" & objMsKotaKab_Approval.PK_MsKotaKab_Approval_Id, "", 0, 1, Nothing)
        If LObjMsKotaKab_ApprovalDetail.Count = 0 Then Throw New Exception("Data Not Found")
        Dim ObjMsKotaKab_ApprovalDetail As MsKotaKab_ApprovalDetail = LObjMsKotaKab_ApprovalDetail(0)
        Using ObjNewMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(ObjMsKotaKab_ApprovalDetail.IDKotaKab)
            DataRepository.MsKotaKabProvider.Delete(ObjNewMsKotaKab)
        End Using

        '======== Delete MAPPING =========================================
        Dim L_ObjMappingMsKotaKabNCBSPPATK_Approval_Detail As TList(Of MappingMsKotaKabNCBSPPATK_Approval_Detail) = DataRepository.MappingMsKotaKabNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsKotaKabNCBSPPATK_Approval_DetailColumn.PK_MsKotaKab_Approval_Id.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)


        For Each c As MappingMsKotaKabNCBSPPATK_Approval_Detail In L_ObjMappingMsKotaKabNCBSPPATK_Approval_Detail
            Using ObjNewMappingMsKotaKabNCBSPPATK As MappingMsKotaKabNCBSPPATK = DataRepository.MappingMsKotaKabNCBSPPATKProvider.GetByPK_MappingMsKotaKabNCBSPPATK_Id(c.PK_MappingMsKotaKabNCBSPPATK_Id)
                DataRepository.MappingMsKotaKabNCBSPPATKProvider.Delete(ObjNewMappingMsKotaKabNCBSPPATK)
            End Using
        Next

        '======== Delete Approval data ======================================
        DeleteApproval(objMsKotaKab_Approval, ObjMsKotaKab_ApprovalDetail, L_ObjMappingMsKotaKabNCBSPPATK_Approval_Detail)
    End Sub

    ''' <summary>
    ''' Update Data dari approval ke table asli 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub UpdateData(ByVal objMsKotaKab_Approval As MsKotaKab_Approval)
        'Dim objAuditTrailType As MsAuditTrailEntityType = DataRepository.MsAuditTrailEntityTypeProvider.GetByPk_MsAuditTrailEntityTypeId(18)
        '   '================= Update Header ===========================================================
        Dim LObjMsKotaKab_ApprovalDetail As TList(Of MsKotaKab_ApprovalDetail) = DataRepository.MsKotaKab_ApprovalDetailProvider.GetPaged(MsKotaKab_ApprovalDetailColumn.FK_MsKotaKab_Approval_Id.ToString & "=" & objMsKotaKab_Approval.PK_MsKotaKab_Approval_Id, "", 0, 1, Nothing)
        If LObjMsKotaKab_ApprovalDetail.Count = 0 Then Throw New Exception("Data Not Found")
        Dim ObjMsKotaKab_ApprovalDetail As MsKotaKab_ApprovalDetail = LObjMsKotaKab_ApprovalDetail(0)
        Dim ObjMsKotaKabNew As MsKotaKab
        Using ObjMsKotaKabOld As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(ObjMsKotaKab_ApprovalDetail.IDKotaKab)
            If ObjMsKotaKabOld Is Nothing Then Throw New Exception("Data Not Found")
            ObjMsKotaKabNew = ObjMsKotaKabOld.Clone
            With ObjMsKotaKabNew
                FillOrNothing(.NamaKotaKab, ObjMsKotaKab_ApprovalDetail.NamaKotaKab)
                FillOrNothing(.CreatedBy, ObjMsKotaKab_ApprovalDetail.CreatedBy)
                FillOrNothing(.CreatedDate, ObjMsKotaKab_ApprovalDetail.CreatedDate)
                FillOrNothing(.ApprovedBy, SessionUserId, True, oString)
                FillOrNothing(.ApprovedDate, Date.Now)
                .IDPropinsi = ObjMsKotaKab_ApprovalDetail.IDPropinsi
                .Activation = ObjMsKotaKab_ApprovalDetail.Activation
            End With
            DataRepository.MsKotaKabProvider.Save(ObjMsKotaKabNew)
            'Insert auditrail
            'AuditTrailBLL.InsertAuditTrail(AuditTrailOperation.Add, objAuditTrailType, Now(), Sahassa.CommonCTRWeb.Commonly.SessionNIK, Sahassa.CommonCTRWeb.Commonly.SessionStaffName, "MsKotaKab has Approved to Update data", ObjMsKotaKabNew, ObjMsKotaKabOld)
            '======== Update MAPPING =========================================
            'delete mapping item
            Using L_ObjMappingMsKotaKabNCBSPPATK As TList(Of MappingMsKotaKabNCBSPPATK) = DataRepository.MappingMsKotaKabNCBSPPATKProvider.GetPaged(MappingMsKotaKabNCBSPPATKColumn.IdKotakab.ToString & _
             "=" & _
             ObjMsKotaKabNew.IDKotaKab, "", 0, Integer.MaxValue, Nothing)
                DataRepository.MappingMsKotaKabNCBSPPATKProvider.Delete(L_ObjMappingMsKotaKabNCBSPPATK)
            End Using
            'Insert mapping item
            Dim L_ObjMappingMsKotaKabNCBSPPATK_Approval_Detail As TList(Of MappingMsKotaKabNCBSPPATK_Approval_Detail) = DataRepository.MappingMsKotaKabNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsKotaKabNCBSPPATK_Approval_DetailColumn.PK_MsKotaKab_Approval_Id.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)

            Dim L_ObjNewMappingMsKotaKabNCBSPPATK As New TList(Of MappingMsKotaKabNCBSPPATK)
            For Each c As MappingMsKotaKabNCBSPPATK_Approval_Detail In L_ObjMappingMsKotaKabNCBSPPATK_Approval_Detail
                Dim ObjNewMappingMsKotaKabNCBSPPATK As New MappingMsKotaKabNCBSPPATK '= DataRepository.MappingMsKotaKabNCBSPPATKProvider.GetByPK_MappingMsKotaKabNCBSPPATK_Id(c.PK_MappingMsKotaKabNCBSPPATK_Id.GetValueOrDefault)
                With ObjNewMappingMsKotaKabNCBSPPATK
                    FillOrNothing(.IdKotakab, ObjMsKotaKabNew.IDKotaKab)
                    FillOrNothing(.Nama, c.Nama)
                    FillOrNothing(.IdKotakabNCBS, c.IDKotaKabNCBS)
                    L_ObjNewMappingMsKotaKabNCBSPPATK.Add(ObjNewMappingMsKotaKabNCBSPPATK)
                End With
            Next
            DataRepository.MappingMsKotaKabNCBSPPATKProvider.Save(L_ObjNewMappingMsKotaKabNCBSPPATK)
            '======== Delete Approval data ======================================
            DeleteApproval(objMsKotaKab_Approval, ObjMsKotaKab_ApprovalDetail, L_ObjMappingMsKotaKabNCBSPPATK_Approval_Detail)
        End Using
    End Sub
    ''' <summary>
    ''' Insert Data dari approval ke table asli 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub Inserdata(ByVal objMsKotaKab_Approval As MsKotaKab_Approval)
        'Dim objAuditTrailType As MsAuditTrailEntityType = DataRepository.MsAuditTrailEntityTypeProvider.GetByPk_MsAuditTrailEntityTypeId(18)
        '   '================= Save Header ===========================================================
        Dim LObjMsKotaKab_ApprovalDetail As TList(Of MsKotaKab_ApprovalDetail) = DataRepository.MsKotaKab_ApprovalDetailProvider.GetPaged(MsKotaKab_ApprovalDetailColumn.FK_MsKotaKab_Approval_Id.ToString & "=" & objMsKotaKab_Approval.PK_MsKotaKab_Approval_Id, "", 0, 1, Nothing)
        'jika data tidak ada di database
        If LObjMsKotaKab_ApprovalDetail.Count = 0 Then Throw New Exception("Data Not Found")
        'binding approval ke table asli
        Dim ObjMsKotaKab_ApprovalDetail As MsKotaKab_ApprovalDetail = LObjMsKotaKab_ApprovalDetail(0)
        Using ObjNewMsKotaKab As New MsKotaKab()
            With ObjNewMsKotaKab
                FillOrNothing(.IDKotaKab, ObjMsKotaKab_ApprovalDetail.IDKotaKab)
                FillOrNothing(.NamaKotaKab, ObjMsKotaKab_ApprovalDetail.NamaKotaKab)
                FillOrNothing(.CreatedBy, ObjMsKotaKab_ApprovalDetail.CreatedBy)
                FillOrNothing(.CreatedDate, ObjMsKotaKab_ApprovalDetail.CreatedDate)
                FillOrNothing(.ApprovedBy, SessionUserId, True, oString)
                FillOrNothing(.ApprovedDate, Date.Now)
                .IDPropinsi = ObjMsKotaKab_ApprovalDetail.IDPropinsi
                .Activation = True
            End With
            DataRepository.MsKotaKabProvider.Save(ObjNewMsKotaKab)
            'Insert auditrail
            'AuditTrailBLL.InsertAuditTrail(AuditTrailOperation.Add, objAuditTrailType, Now(), Sahassa.CommonCTRWeb.Commonly.SessionNIK, Sahassa.CommonCTRWeb.Commonly.SessionStaffName, "MsKotaKab has Approved to Insert data", ObjNewMsKotaKab, Nothing)
            '======== Insert MAPPING =========================================
            Dim L_ObjMappingMsKotaKabNCBSPPATK_Approval_Detail As TList(Of MappingMsKotaKabNCBSPPATK_Approval_Detail) = DataRepository.MappingMsKotaKabNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsKotaKabNCBSPPATK_Approval_DetailColumn.PK_MsKotaKab_Approval_Id.ToString & _
             "=" & _
             parID, "", 0, Integer.MaxValue, Nothing)
            Dim L_ObjNewMappingMsKotaKabNCBSPPATK As New TList(Of MappingMsKotaKabNCBSPPATK)()
            For Each c As MappingMsKotaKabNCBSPPATK_Approval_Detail In L_ObjMappingMsKotaKabNCBSPPATK_Approval_Detail
                Dim ObjNewMappingMsKotaKabNCBSPPATK As New MappingMsKotaKabNCBSPPATK()
                With ObjNewMappingMsKotaKabNCBSPPATK
                    FillOrNothing(.IdKotakab, ObjNewMsKotaKab.IDKotaKab)
                    FillOrNothing(.Nama, c.Nama)
                    FillOrNothing(.IdKotakabNCBS, c.IDKotaKabNCBS)
                    L_ObjNewMappingMsKotaKabNCBSPPATK.Add(ObjNewMappingMsKotaKabNCBSPPATK)
                End With
            Next
            DataRepository.MappingMsKotaKabNCBSPPATKProvider.Save(L_ObjNewMappingMsKotaKabNCBSPPATK)
            '======== Delete Approval data ======================================
            DeleteApproval(objMsKotaKab_Approval, ObjMsKotaKab_ApprovalDetail, L_ObjMappingMsKotaKabNCBSPPATK_Approval_Detail)
        End Using
    End Sub

#End Region

End Class


