﻿Imports SahassaNettier.Entities
Imports SahassaNettier.Data
Imports AMLBLL

Partial Class InsiderCodeApprovalDetailView
    Inherits Parent

    ReadOnly Property GetPk_InsiderCodeApproval As Long
        Get
            If IsNumeric(Request.Params("InsiderCodeApprovalID")) Then
                If Not IsNothing(Session("InsiderCodeApprovalDetail.PK")) Then
                    Return CLng(Session("InsiderCodeApprovalDetail.PK"))
                Else
                    Session("InsiderCodeApprovalDetail.PK") = Request.Params("InsiderCodeApprovalID")
                    Return CLng(Session("InsiderCodeApprovalDetail.PK"))
                End If
            End If
            Return 0
        End Get
    End Property

    Sub clearSession()
        Session("InsiderCodeApprovalDetail.PK") = Nothing
    End Sub

    Sub LoadDataAll()
        Using objInsiderCodeApproval As InsiderCodeApproval = InsiderCodeBLL.getInsiderCodeApprovalByPK(GetPk_InsiderCodeApproval)
            If Not IsNothing(objInsiderCodeApproval) Then
                If objInsiderCodeApproval.FK_ModeID.GetValueOrDefault(0) = Sahassa.AML.Commonly.TypeMode.Add Then
                    LblAction.Text = "Add"
                ElseIf objInsiderCodeApproval.FK_ModeID.GetValueOrDefault(0) = Sahassa.AML.Commonly.TypeMode.Edit Then
                    LblAction.Text = "Edit"
                ElseIf objInsiderCodeApproval.FK_ModeID.GetValueOrDefault(0) = Sahassa.AML.Commonly.TypeMode.Delete Then
                    LblAction.Text = "Delete"
                ElseIf objInsiderCodeApproval.FK_ModeID.GetValueOrDefault(0) = Sahassa.AML.Commonly.TypeMode.Activation Then
                    LblAction.Text = "Activation"
                End If
            End If
            Using objUser As User = UserBLL.GetUserByPkUserID(objInsiderCodeApproval.FK_MsUserID)
                If Not IsNothing(objUser) Then
                    LblRequestBy.Text = objUser.UserName
                End If
            End Using
            LblRequestDate.Text = CDate(objInsiderCodeApproval.CreatedDate).ToString("dd-MM-yyyy")


            Using objInsiderCodeApprovalDetail As InsiderCodeApprovalDetail = InsiderCodeBLL.getInsiderCodeApprovalDetailByPKApproval(GetPk_InsiderCodeApproval)
                If LblAction.Text = "Add" Then
                    LoadNewData()
                ElseIf LblAction.Text = "Edit" Then
                    LoadNewData()
                    LoadOldData(objInsiderCodeApprovalDetail.PK_InsiderCode_ID)
                    compareDataByContainer(PanelNew, PanelOld)
                ElseIf LblAction.Text = "Delete" Then
                    LoadOldData(objInsiderCodeApprovalDetail.PK_InsiderCode_ID)
                ElseIf LblAction.Text = "Activation" Then
                    LoadNewData()
                    LoadOldData(objInsiderCodeApprovalDetail.PK_InsiderCode_ID)
                    compareDataByContainer(PanelNew, PanelOld)
                End If
            End Using


        End Using

    End Sub

    Sub LoadNewData()
        Using objInsiderCodeApprovalDetail As InsiderCodeApprovalDetail = InsiderCodeBLL.getInsiderCodeApprovalDetailByPKApproval(GetPk_InsiderCodeApproval)
            If Not IsNothing(objInsiderCodeApprovalDetail) Then
                txtInsiderCodeID.Text = objInsiderCodeApprovalDetail.InsiderCodeID.ToString
                txtInsiderCodeName.Text = objInsiderCodeApprovalDetail.InsiderCodeName.ToString
                txtActivation.Text = objInsiderCodeApprovalDetail.Activation.GetValueOrDefault(0).ToString
                PanelNew.Visible = True
            End If
        End Using
    End Sub

    Sub LoadOldData(ByVal PK_InsiderCode_ID As Long)
        Using objInsiderCode As InsiderCode = InsiderCodeBLL.getInsiderCodeByPk(PK_InsiderCode_ID)
            If Not IsNothing(objInsiderCode) Then
                old_txtInsiderCodeID.Text = objInsiderCode.InsiderCodeId.ToString
                old_txtInsiderCodeName.Text = objInsiderCode.InsiderCodeName.ToString
                old_txtActivation.Text = objInsiderCode.Activation.GetValueOrDefault(0).ToString
                PanelOld.Visible = True
            End If
        End Using
    End Sub


    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                clearSession()
                mtvApproval.ActiveViewIndex = 0

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using

                LoadDataAll()
            End If
        Catch ex As Exception
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImgBtnAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAccept.Click
        Try
            If LblAction.Text = "Add" Then
                If InsiderCodeBLL.acceptAddOrDelete(GetPk_InsiderCodeApproval, "Add") Then
                    mtvApproval.ActiveViewIndex = 1
                    lblMessage.Text = "Adding Data Approved"
                    AjaxPanel1.Visible = False
                End If
            ElseIf LblAction.Text = "Edit" Then
                If InsiderCodeBLL.acceptEditOrActivate(GetPk_InsiderCodeApproval, "Edit") Then
                    mtvApproval.ActiveViewIndex = 1
                    lblMessage.Text = "Editing Data Approved"
                    AjaxPanel1.Visible = False
                End If
            ElseIf LblAction.Text = "Delete" Then
                If InsiderCodeBLL.acceptAddOrDelete(GetPk_InsiderCodeApproval, "Delete") Then
                    mtvApproval.ActiveViewIndex = 1
                    lblMessage.Text = "Deleting Data Approved"
                    AjaxPanel1.Visible = False
                End If
            ElseIf LblAction.Text = "Activation" Then
                If InsiderCodeBLL.acceptEditOrActivate(GetPk_InsiderCodeApproval, "Activation") Then
                    mtvApproval.ActiveViewIndex = 1
                    lblMessage.Text = "Activation Data Approved"
                    AjaxPanel1.Visible = False
                End If
            End If
        Catch ex As Exception
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub ImgBtnReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnReject.Click
        Try
            If LblAction.Text = "Add" Then
                If InsiderCodeBLL.rejectAddOrDelete(GetPk_InsiderCodeApproval, "Add") Then
                    mtvApproval.ActiveViewIndex = 1
                    lblMessage.Text = "Adding Data Rejeceted"
                    AjaxPanel1.Visible = False
                End If
            ElseIf LblAction.Text = "Edit" Then
                If InsiderCodeBLL.rejectEditOrActivate(GetPk_InsiderCodeApproval, "Edit") Then
                    mtvApproval.ActiveViewIndex = 1
                    lblMessage.Text = "Editing Data Rejeceted"
                    AjaxPanel1.Visible = False
                End If
            ElseIf LblAction.Text = "Delete" Then
                If InsiderCodeBLL.rejectAddOrDelete(GetPk_InsiderCodeApproval, "Delete") Then
                    mtvApproval.ActiveViewIndex = 1
                    lblMessage.Text = "Deleting Data Rejeceted"
                    AjaxPanel1.Visible = False
                End If
            ElseIf LblAction.Text = "Activation" Then
                If InsiderCodeBLL.rejectEditOrActivate(GetPk_InsiderCodeApproval, "Activation") Then
                    mtvApproval.ActiveViewIndex = 1
                    lblMessage.Text = "Activation Data Rejeceted"
                    AjaxPanel1.Visible = False
                End If
            End If
        Catch ex As Exception
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImgBackAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBackAdd.Click
        Try
            Response.Redirect("InsiderCodeApproval.aspx")
        Catch ex As Exception
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub btnOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnOK.Click
        Try
            Response.Redirect("InsiderCodeApproval.aspx")
        Catch ex As Exception
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Sub compareDataByContainer(ByRef containerNew As Control, ByRef containerOld As Control)
        Dim index As Integer = 0
        For Each item As Control In containerNew.Controls
            If TypeOf (item) Is Label Then

                If CType(item, Label).Text.ToLower <> "old value" And CType(item, Label).Text.ToLower <> "new value" Then
                    If CType(item, Label).Text <> CType(containerOld.Controls(index), Label).Text Then
                        CType(item, Label).ForeColor = Drawing.Color.Red
                        CType(containerOld.Controls(index), Label).ForeColor = Drawing.Color.Red
                    End If
                End If
            End If
            index = index + 1
        Next
    End Sub
End Class
