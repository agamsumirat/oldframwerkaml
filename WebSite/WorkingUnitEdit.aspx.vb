Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Partial Class WorkingUnitEdit
    Inherits Parent

    ''' <summary>
    ''' get user id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetWorkingUnitId() As String
        Get
            Return Me.Request.Params("WorkingUnitID")
        End Get
    End Property

    ''' <summary>
    ''' cancel handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "WorkingUnitView.aspx"

        Me.Response.Redirect("WorkingUnitView.aspx", False)
    End Sub

    ''' <summary>
    ''' fill group
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillGroupLevelType()
        Using AccessGroup As New AMLDAL.AMLDataSetTableAdapters.LevelTypeTableAdapter
            Me.DropDownLevelTypeName.DataSource = AccessGroup.GetData
            Me.DropDownLevelTypeName.DataTextField = "LevelTypeName"
            Me.DropDownLevelTypeName.DataValueField = "LevelTypeID"
            Me.DropDownLevelTypeName.DataBind()
        End Using
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillGroupWorkingUnitParent()
        Using AccessGroup As New AMLDAL.AMLDataSetTableAdapters.WorkingUnitTableAdapter
            Me.DropDownWorkingUnitParent.DataSource = AccessGroup.GetData
            Me.DropDownWorkingUnitParent.DataTextField = "WorkingUnitName"
            Me.DropDownWorkingUnitParent.DataValueField = "WorkingUnitID"
            Me.DropDownWorkingUnitParent.DataBind()
        End Using
    End Sub

    ''' <summary>
    ''' Fill Initial data to be editted
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillEditData()
        Using AccessWorkingUnit As New AMLDAL.AMLDataSetTableAdapters.WorkingUnitTableAdapter
            Using TableWorkingUnit As AMLDAL.AMLDataSet.WorkingUnitDataTable = AccessWorkingUnit.GetDataByWorkingUnitID(Me.GetWorkingUnitId)
                If TableWorkingUnit.Rows.Count > 0 Then
                    Dim TableRowWorkingUnit As AMLDAL.AMLDataSet.WorkingUnitRow = TableWorkingUnit.Rows(0)

                    ViewState("WorkingUnitID_Old") = TableRowWorkingUnit.WorkingUnitID
                    Me.LabelWorkingUnitID.Text = ViewState("WorkingUnitID_Old")

                    ViewState("WorkingUnitName_Old") = TableRowWorkingUnit.WorkingUnitName
                    Me.TextWorkingUnitName.Text = ViewState("WorkingUnitName_Old")

                    ViewState("LevelTypeID_Old") = TableRowWorkingUnit.LevelTypeID
                    Me.DropDownLevelTypeName.SelectedValue = ViewState("LevelTypeID_Old")

                    ViewState("WorkingUnitParentID_Old") = TableRowWorkingUnit.WorkingUnitParentID
                    Me.DropDownWorkingUnitParent.SelectedValue = ViewState("WorkingUnitParentID_Old")

                    ViewState("WorkingUnitDesc_Old") = TableRowWorkingUnit.WorkingUnitDesc
                    Me.TextWorkingUnitDescription.Text = ViewState("WorkingUnitDesc_Old")

                    ViewState("WorkingUnitCreatedDate_Old") = TableRowWorkingUnit.WorkingUnitCreatedDate

                End If
            End Using
        End Using
    End Sub

    ''' <summary>
    ''' InsertWorkingUnitBySU
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub UpdateWorkingUnitBySU()
        Try
            Dim WorkingUnitDescription As String
            If Me.TextWorkingUnitDescription.Text.Length <= 255 Then
                WorkingUnitDescription = Me.TextWorkingUnitDescription.Text
            Else
                WorkingUnitDescription = Me.TextWorkingUnitDescription.Text.Substring(0, 255)
            End If

            Using AccessWorkingUnit As New AMLDAL.AMLDataSetTableAdapters.WorkingUnitTableAdapter
                AccessWorkingUnit.UpdateWorkingUnit(CInt(Me.GetWorkingUnitId), Me.TextWorkingUnitName.Text, CInt(Me.DropDownLevelTypeName.SelectedValue), CInt(Me.DropDownWorkingUnitParent.SelectedValue), WorkingUnitDescription)
            End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    ''' <summary>
    ''' InsertAuditTrail
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub InsertAuditTrail()
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(5)

            Dim WorkingUnitDescription As String
            If Me.TextWorkingUnitDescription.Text.Length <= 255 Then
                WorkingUnitDescription = Me.TextWorkingUnitDescription.Text
            Else
                WorkingUnitDescription = Me.TextWorkingUnitDescription.Text.Substring(0, 255)
            End If

            Using AccessWorkingUnit As New AMLDAL.AMLDataSetTableAdapters.WorkingUnitTableAdapter
                Using dt As Data.DataTable = AccessWorkingUnit.GetDataByWorkingUnitID(CInt(Me.GetWorkingUnitId))
                    Dim row As AMLDAL.AMLDataSet.WorkingUnitRow = dt.Rows(0)
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "WorkingUnitName", "Edit", row.WorkingUnitName, Me.TextWorkingUnitName.Text, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "LevelTypeID", "Edit", row.LevelTypeID, Me.DropDownLevelTypeName.SelectedItem.Text, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "WorkingUnitParentID", "Edit", row.WorkingUnitParentID, Me.DropDownWorkingUnitParent.SelectedItem.Text, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "WorkingUnitDesc", "Edit", row.WorkingUnitDesc, WorkingUnitDescription, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "WorkingUnitCreatedDate", "Edit", row.WorkingUnitCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), row.WorkingUnitCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageUpdate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageUpdate.Click
        Dim oSQLTrans As SQLTransaction = Nothing
        Page.Validate()

        If Page.IsValid Then
            Try
                'Buat variabel untuk menampung nilai-nilai baru
                Dim WorkingUnitID As Int64 = Me.LabelWorkingUnitID.Text
                Dim WorkingUnitName As String = Trim(Me.TextWorkingUnitName.Text)

                'Jika tidak ada perubahan WorkingUnitName
                If WorkingUnitName = Trim(ViewState("WorkingUnitName_Old")) Then
                    GoTo Add
                Else 'Jika ada perubahan WorkingUnitName 

                    Dim counter As Int32
                    'Periksa apakah WorkingUnitName yg baru tsb sdh ada dalam tabel WorkingUnit atau belum
                    Using AccessCategory As New AMLDAL.AMLDataSetTableAdapters.WorkingUnitTableAdapter
                        counter = AccessCategory.CountMatchingWorkingUnit(WorkingUnitName)
                    End Using

                    'Counter = 0 berarti WorkingUnitName tersebut belum pernah ada dalam tabel WorkingUnit
                    If counter = 0 Then
Add:
                        'Periksa apakah WorkingUnitName tsb dalam status pending approval atau tidak
                        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.WorkingUnit_ApprovalTableAdapter
                            oSQLTrans = BeginTransaction(AccessPending, IsolationLevel.ReadUncommitted)
                            counter = AccessPending.CountWorkingUnitApprovalByWorkingUnitName(WorkingUnitName)

                            'Counter = 0 berarti WorkingUnitName yg baru tsb tidak dalam status pending approval
                            If counter = 0 Then
                                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                                    Me.InsertAuditTrail()
                                    Me.UpdateWorkingUnitBySU()
                                    Me.LblSuccess.Text = "Success to Update Working Unit."
                                    Me.LblSuccess.Visible = True
                                Else
                                    Dim LevelTypeID As Int16 = Convert.ToInt16(Me.DropDownLevelTypeName.SelectedValue)
                                    Dim WorkingUnitParentID As Int16 = Convert.ToInt16(Me.DropDownWorkingUnitParent.SelectedValue)
                                    Dim WorkingUnitDescription As String = Me.TextWorkingUnitDescription.Text

                                    'Buat variabel untuk menampung nilai-nilai lama
                                    Dim WorkingUnitID_Old As Int64 = ViewState("WorkingUnitID_Old")
                                    Dim WorkingUnitName_Old As String = ViewState("WorkingUnitName_Old")
                                    Dim LevelTypeID_Old As Int16 = ViewState("LevelTypeID_Old")
                                    Dim WorkingUnitParentID_Old As Int16 = ViewState("WorkingUnitParentID_Old")
                                    Dim WorkingUnitDesc_Old As String = ViewState("WorkingUnitDesc_Old")
                                    Dim WorkingUnitCreatedDate_Old As DateTime = ViewState("WorkingUnitCreatedDate_Old")

                                    'variabel untuk menampung nilai identity dari tabel NewsPendingApprovalID
                                    Dim WorkingUnitPendingApprovalID As Int64

                                    'Using TransScope As New Transactions.TransactionScope
                                    Using AccessWorkingUnitPendingApproval As New AMLDAL.AMLDataSetTableAdapters.WorkingUnit_PendingApprovalTableAdapter
                                        SetTransaction(AccessWorkingUnitPendingApproval, oSQLTrans)
                                        'Tambahkan ke dalam tabel WorkingUnit_PendingApproval dengan ModeID = 2 (Edit) 
                                        WorkingUnitPendingApprovalID = AccessWorkingUnitPendingApproval.InsertWorkingUnit_PendingApproval(Sahassa.AML.Commonly.SessionUserId, Now, "WorkingUnit Edit", 2)

                                        'Tambahkan ke dalam tabel WorkingUnit_Approval dengan ModeID = 2 (Edit) 
                                        AccessPending.Insert(WorkingUnitPendingApprovalID, 2, WorkingUnitID, WorkingUnitName, LevelTypeID, WorkingUnitParentID, WorkingUnitDescription, Now, WorkingUnitID_Old, WorkingUnitName_Old, LevelTypeID_Old, WorkingUnitParentID_Old, WorkingUnitDesc_Old, WorkingUnitCreatedDate_Old)

                                        oSQLTrans.Commit()
                                        '    TransScope.Complete()
                                    End Using
                                    'End Using

                                    Dim MessagePendingID As Integer = 8502 'MessagePendingID 8502 = WorkingUnit Edit 

                                    Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & WorkingUnitName_Old

                                    Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & WorkingUnitName_Old, False)
                                End If
                            Else 'Counter != 0 berarti WorkingUnitName tersebut dalam status pending approval
                                Throw New Exception("Cannot edit the following Working Unit Name: '" & WorkingUnitName & "' because it is currently waiting for approval")
                            End If
                        End Using
                    Else 'Counter != 0 berarti WorkingUnitName tersebut sudah ada dalam tabel WorkingUnit
                        Throw New Exception("Cannot change to the following Working Unit Name: '" & WorkingUnitName & "' because that Working Unit Name already exists in the database.")
                    End If
                End If
            Catch ex As Exception
                If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
                Me.cvalPageError.IsValid = False
                Me.cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            Finally
                If Not oSQLTrans Is Nothing Then
                    oSQLTrans.Dispose()
                    oSQLTrans = Nothing
                End If
            End Try
        End If
    End Sub

    ''' <summary>
    ''' page load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.FillGroupLevelType()
                Me.FillGroupWorkingUnitParent()
                Me.FillEditData()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    '    Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

End Class