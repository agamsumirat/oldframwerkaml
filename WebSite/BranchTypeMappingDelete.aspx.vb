Imports System.Data.SqlClient
Partial Class BranchTypeMappingDelete
    Inherits Parent

    ''' <summary>
    ''' get user id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetBranchTypeMappingID() As String
        Get
            Return Me.Request.Params("BranchMappingID")
        End Get
    End Property
    Private _orowBranchTypeMapping As AMLDAL.MappingBranch.BranchTypeMappingRow
    Private ReadOnly Property orowBranchTypeMapping() As AMLDAL.MappingBranch.BranchTypeMappingRow
        Get
            If _orowBranchTypeMapping Is Nothing Then
                Using adapter As New AMLDAL.MappingBranchTableAdapters.BranchTypeMappingTableAdapter
                    Using otable As AMLDAL.MappingBranch.BranchTypeMappingDataTable = adapter.GetDataByPK(Me.GetBranchTypeMappingID)
                        If otable.Rows.Count > 0 Then
                            _orowBranchTypeMapping = otable.Rows(0)
                            Return _orowBranchTypeMapping
                        Else
                            _orowBranchTypeMapping = Nothing
                            Return _orowBranchTypeMapping
                        End If
                    End Using
                End Using
            Else
                Return _orowBranchTypeMapping

            End If
            
        End Get
    End Property
    ''' <summary>
    ''' cancel handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "BranchTypeMappingview.aspx"

        Me.Response.Redirect("BranchTypeMappingview.aspx", False)
    End Sub

    Private Function GetDataBranchType(ByVal branchtypeid As Long) As String
        Try
            Using adapter As New AMLDAL.MappingBranchTableAdapters.BranchTypeTableAdapter
                Using otable As AMLDAL.MappingBranch.BranchTypeDataTable = adapter.GetDataByPK(branchtypeid)
                    If otable.Rows.Count > 0 Then
                        Dim orow As AMLDAL.MappingBranch.BranchTypeRow = otable.Rows(0)
                        If Not orow.IsBranchTypeNameNull Then
                            Return orow.BranchTypeName
                        Else
                            Return ""
                        End If
                    Else
                        Return ""
                    End If

                End Using

            End Using
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Function
    Private Function GetDataBranch(ByVal branchid As Long) As String
        Try
            Using adapter As New AMLDAL.MappingBranchTableAdapters.JHDATATableAdapter
                Using otable As AMLDAL.MappingBranch.JHDATADataTable = adapter.GetDataByPK(branchid)
                    If otable.Rows.Count > 0 Then
                        Dim orow As AMLDAL.MappingBranch.JHDATARow = otable.Rows(0)
                        If Not orow.IsJDNAMENull Then
                            Return orow.JDNAME
                        Else
                            Return ""
                        End If
                    Else
                        Return ""
                    End If
                End Using
            End Using
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Function
    ''' <summary>
    ''' update handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageUpdate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageUpdate.Click
        Dim otrans As SqlTransaction = Nothing
        Dim PK As Long = 0
        Try
            If Page.IsValid AndAlso IsDataValid() Then
                Using adapter As New AMLDAL.MappingBranchTableAdapters.BranchTypeMapping_PendingApprovalTableAdapter
                    PK = adapter.InsertBranchTypeMappingPendingApproval(Sahassa.AML.Commonly.SessionUserId, Now, LabelBranchID.Text, "Delete", Sahassa.AML.Commonly.TypeMode.Delete)
                End Using
                Using adapter As New AMLDAL.MappingBranchTableAdapters.BranchTypeMappingApprovalTableAdapter
                    adapter.Insert(PK, orowBranchTypeMapping.BranchTypeMappingId, orowBranchTypeMapping.BranchTypeId, orowBranchTypeMapping.BranchId, Nothing, Nothing, Nothing)
                End Using
                Dim MessagePendingID As Integer = 82593 'MessagePendingID 8203 = Group Delete 

                Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & LabelBranchID.Text

                Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & LabelBranchID.Text, False)
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Function IsDataValid() As Boolean
        Try
            'cek apakah data yang mau didelete tidak boleh ada di pending approval
            Using adapter As New AMLDAL.MappingBranchTableAdapters.BranchTypeMapping_PendingApprovalTableAdapter
                Dim jml As Integer = 0
                jml = adapter.CountByBranchTypeIDName(orowBranchTypeMapping.BranchId & " - " & GetDataBranch(orowBranchTypeMapping.BranchId))
                If jml > 0 Then
                    cvalPageError.IsValid = False
                    cvalPageError.ErrorMessage = "Branch " & orowBranchTypeMapping.BranchId & " - " & GetDataBranch(orowBranchTypeMapping.BranchId) & " Already in waiting for approval."
                    Return False
                Else
                    Return True
                End If
            End Using

        Catch ex As Exception
            Return False
        End Try
    End Function
    ''' <summary>
    ''' Load data yang mau di delete
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub LoadData()
        Try
            If Not orowBranchTypeMapping Is Nothing Then
                If Not orowBranchTypeMapping.IsBranchIdNull Then
                    LabelBranchID.Text = orowBranchTypeMapping.BranchId & " - " & GetDataBranch(orowBranchTypeMapping.BranchId)
                End If

                If Not orowBranchTypeMapping.IsBranchTypeIdNull Then
                    LabelBranchType.Text = GetDataBranchType(orowBranchTypeMapping.BranchTypeId)
                End If
            End If

        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Sub


    ''' <summary>
    ''' page load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then

                LoadData()
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)


                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)


                End Using

            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

End Class
