<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="GroupInsiderCodeMappingManagementApprovalDetail.aspx.vb" Inherits="GroupInsiderCodeMappingManagementApprovalDetail" title="Group Insider Code Mapping Management Approval Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
	<table cellspacing="4" cellpadding="0" width="100%">
		<tr>
			<td vAlign="bottom" align="left"><IMG height="15" src="images/dot_title.gif" width="15"></td>
			<td class="maintitle" vAlign="bottom" width="99%"><asp:label id="LabelTitle" runat="server"></asp:label></td>
		</tr>
	</table>
    <TABLE id="TableDetail" bordercolor="#ffffff" cellspacing="1" cellpadding="2" 
	    bgColor="#dddddd" border="2" runat="server">
	    <TR class="formText">
		    <TD bgColor="#ffffff" style="width: 22px; height: 32px;">&nbsp;</TD>
		    <TD width="20%" bgColor="#ffffff" style="height: 32px">
                Created By</TD>
		    <TD bgColor="#ffffff" style="width: 44px; height: 32px;">:</TD>
		    <TD width="40%" bgColor="#ffffff" style="height: 32px" colspan="5"><asp:label id="LabelCreatedBy" runat="server"></asp:label></TD>
	    </TR>
	    <tr class="formText">
		    <td bgcolor="#ffffff" height="24" style="width: 22px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;"></td>
		    <td bgcolor="#ffffff">
                Entry Date</td>
		    <td bgcolor="#ffffff" style="width: 44px">:</td>
		    <td bgcolor="#ffffff" colspan="5">              
                <asp:Label ID="LabelEntryDate" runat="server"></asp:Label></td>
	    </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" height="24" style="width: 22px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff">
                Group ID</td>
            <td bgcolor="#ffffff" style="width: 44px">
                :</td>
            <td bgcolor="#ffffff" colspan="5">
                <asp:Label ID="LabelGroupId" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" height="24" style="width: 22px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff">
                Group Name</td>
            <td bgcolor="#ffffff" style="width: 44px">
                :</td>
            <td bgcolor="#ffffff" colspan="5">
                <asp:Label ID="LabelGroupName" runat="server"></asp:Label></td>
        </tr>
	    
	       <tr class="formText">
		    <td colspan="4" bgcolor="#ffffcc" style="height: 30px">&nbsp;Old Values</td>
		    <td colspan="4" bgcolor="#ffffcc" style="height: 30px">&nbsp;New Values</td>
	    </tr>
	    <tr class="formText">
		    <td bgcolor="#ffffff" style="width: 22px">&nbsp;</td>
		    <td bgcolor="#ffffff">
                Insider Code(s)<br />
                <br />
                <br />
                <br />
            </td>
		    <td bgcolor="#ffffff" style="width: 44px">:<br />
                <br />
                <br />
                <br />
                <br />
            </td>
		    <td bgcolor="#ffffff">
                <asp:ListBox ID="ListBoxInsiderCodesOld" runat="server" Width="144px"></asp:ListBox></td>
		    <td bgcolor="#ffffff">&nbsp;</td>
		    <td width="20%" bgcolor="#ffffff">
                Insider Code(s)<br />
                <br />
                <br />
            </td>
		    <td bgcolor="#ffffff">:<br />
                <br />
                <br />
                <br />
                <br />
            </td>
		    <td width="50%" bgcolor="#ffffff">
                <asp:ListBox ID="ListBoxInsiderCodesNew" runat="server" Width="144px"></asp:ListBox></td>
	    </tr>
	    
	    <TR class="formText" id="Button11" bgColor="#dddddd" height="30">
		    <TD style="width: 22px"><IMG height="15" src="images/arrow.gif" width="15"></TD>
		    <TD colSpan="7">
			    <TABLE cellSpacing="0" cellPadding="3" border="0">
				    <TR>
					    <TD><asp:imagebutton id="ImageAccept" runat="server" CausesValidation="False" SkinID="AcceptButton"></asp:imagebutton></TD>
					    <TD><asp:imagebutton id="ImageReject" runat="server" CausesValidation="False" SkinID="RejectButton"></asp:imagebutton></TD>
					    <td><asp:imagebutton id="ImageBack" runat="server" CausesValidation="False" SkinID="BackButton"></asp:imagebutton></td>
				    </TR>
			    </TABLE>
                <asp:CustomValidator ID="cvalPageErr" runat="server" Display="none"></asp:CustomValidator></TD>
	    </TR>
    </TABLE>
</asp:Content>