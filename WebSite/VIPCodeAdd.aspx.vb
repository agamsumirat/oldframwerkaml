﻿Imports SahassaNettier.Entities
Imports SahassaNettier.Data
Imports AMLBLL

Partial Class VIPCodeAdd
    Inherits Parent

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "VIPCodeView.aspx"

            Me.Response.Redirect("VIPCodeView.aspx", False)
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Function isValidData() As Boolean
        Try
            If txtVIPCodeCode.Text.Trim.Length = 0 Then
                Throw New Exception("VIPCode Code must be filled")
            End If
            If Not VIPCodeBLL.IsUniqueVIPCode(txtVIPCodeCode.Text) Then
                Throw New Exception("VIPCode Code " & txtVIPCodeCode.Text & " existed")
            End If
            Return True
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
            Return False
        End Try
    End Function

    Sub ClearControl()
        txtDescription.Text = ""
        txtVIPCodeCode.Text = ""
    End Sub

    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        If isValidData() Then
            Try

                Using objUser As User = AMLBLL.UserBLL.GetUserByPkUserID(Sahassa.AML.Commonly.SessionPkUserId)
                    If Not IsNothing(objUser) Then
                        Using objVIPCode As New VIPCode
                            objVIPCode.VIPCODE = txtVIPCodeCode.Text
                            objVIPCode.VIPCodeDescription = txtDescription.Text
                            objVIPCode.Activation = True
                            objVIPCode.CreatedDate = Now
                            objVIPCode.CreatedBy = objUser.UserName
                            objVIPCode.LastUpdateDate = Now
                            objVIPCode.LastUpdateBy = objUser.UserName



                            If Sahassa.AML.Commonly.SessionGroupName.ToLower = "superuser" Then
                                If VIPCodeBLL.SaveAdd(objVIPCode) Then
                                    lblMessage.Text = "Insert Data Success Click Ok to Add new data"
                                    mtvVIPCodeAdd.ActiveViewIndex = 1
                                End If
                            Else
                                If VIPCodeBLL.SaveAddApproval(objVIPCode) Then
                                    lblMessage.Text = "Data has been inserted to Approval Click Ok to Add new data"
                                    mtvVIPCodeAdd.ActiveViewIndex = 1
                                End If
                            End If
                        End Using
                    End If
                End Using

            Catch ex As Exception
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            End Try
        End If
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                mtvVIPCodeAdd.ActiveViewIndex = 0

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
            End If
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnOK.Click
        Try
            mtvVIPCodeAdd.ActiveViewIndex = 0
            ClearControl()
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBack.Click
        Try
            Response.Redirect("VIPCodeView.aspx")
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class


