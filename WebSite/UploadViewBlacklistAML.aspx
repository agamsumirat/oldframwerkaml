﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"    CodeFile="UploadViewBlacklistAML.aspx.vb" Inherits="UploadViewBlacklistAML" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    <div id="divcontent" class="divcontent">
        <%--<ajax:AjaxPanel ID="a" runat="server" meta:resourcekey="aResource1" 
                                        BackColor="White" BorderColor="White" Width="100%">
                                <asp:ValidationSummary ID="ValidationSummaryunhandle" runat="Server" CssClass="validation" meta:resourcekey="ValidationSummaryunhandleResource1" />
								<asp:ValidationSummary ID="ValidationSummaryhandle" runat="server"  
                                        CssClass="validationok" ForeColor="Black" ValidationGroup="handle" 
                                        meta:resourcekey="ValidationSummaryhandleResource1" BackColor="White" 
                                        Width="18001px"/>
						        </ajax:AjaxPanel>  --%>
        <ajax:AjaxPanel ID="AjaxPanel5" runat="server" Width="100%" meta:resourcekey="AjaxPanel5Resource1">
            <asp:MultiView ID="MultiViewKPIOvertimeUpload" runat="server" ActiveViewIndex="0">
                <asp:View ID="UploadBlacklistAml" runat="server">
                    <table width="100%" bgcolor="White">
                        <tr>
                            <td height="20px" valign="middle" style="width: 852px" bgcolor="White">
                                <table>
                                    <tr>
                                        <td valign="bottom" align="left" bgcolor="#ffffff" width="15">
                                            <img height="15" src="images/dot_title.gif" width="15">
                                        </td>
                                        <td class="maintitle" valign="bottom" bgcolor="#ffffff">
                                            <asp:Label ID="LabelTitle" runat="server">Upload Blacklist AML</asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="White">
                                <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                    bgcolor="#dddddd" border="2">
                                    <tr id="ModifyRETBDI">
                                        <td valign="top" width="100%" bgcolor="#ffffff" style="height: 132px" id="UploadExcel">
                                            <table cellspacing="4" cellpadding="0" width="100%" border="0">
                                                <tr>
                                                    <td valign="middle" width="100%" nowrap="">
                                                        <table style="width: 100%; height: 100%;">
                                                            <tr>
                                                                <td style="width: 82px; height: 18px">
                                                                </td>
                                                                <td style="width: 61px; height: 18px">
                                                                    &nbsp;
                                                                </td>
                                                                <td align="center" style="height: 18px">
                                                                    &nbsp;
                                                                </td>
                                                                <td style="width: 100%; height: 18px">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 82px; height: 18px">
                                                                </td>
                                                                <td style="width: 61px; height: 18px" align="left">
                                                                    <asp:Label ID="Label2" runat="server">File</asp:Label>
                                                                </td>
                                                                <td align="center" style="height: 18px">
                                                                    :
                                                                </td>
                                                                <td style="width: 100%; height: 18px">
                                                                    &nbsp;<asp:FileUpload ID="FileUploadAttachment" OnClientClick="aspnetForm.encoding = 'multipart/form-data';"
                                                                        ajaxcall="none" runat="server" CssClass="textbox" Width="400px" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 82px; height: 10px">
                                                                </td>
                                                                <td style="width: 61px; height: 10px">
                                                                    <asp:Label ID="Label3" runat="server"> </asp:Label>
                                                                </td>
                                                                <td align="center" style="height: 10px">
                                                                </td>
                                                                <td style="width: 100%; height: 10px">
                                                                    &nbsp;<asp:Label ID="Label36" runat="server" Text=""> </asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 82px; height: 21px">
                                                                </td>
                                                                <td style="width: 61px; height: 21px">
                                                                </td>
                                                                <td align="center" style="height: 21px">
                                                                </td>
                                                                <td style="width: 100%; height: 21px">
                                                                    <%--<ajax:AjaxPanel ID="AjaxPanel11" runat="server" Width="100%" meta:resourcekey="AjaxPanel11Resource1">--%>
                                                                    <asp:ImageButton ID="imgUpload" runat="server" OnClientClick="aspnetForm.encoding = 'multipart/form-data';"
                                                                        ajaxcall="none" ImageUrl="~/Images/button/upload.gif" CausesValidation="false"
                                                                        Style="height: 20px" />
                                                                    <%--</ajax:AjaxPanel> 	--%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 82px; height: 21px">
                                                                    &nbsp;</td>
                                                                <td style="width: 61px; height: 21px">
                                                                    &nbsp;</td>
                                                                <td align="center" style="height: 21px">
                                                                    &nbsp;</td>
                                                                <td style="width: 100%; height: 21px">
                                                                    <%--<asp:LinkButton ID="LnkDownloadTemplate" runat="server" ajaxcall="none">Download Template BlackListAML</asp:LinkButton>--%>
                                                                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/FolderTemplate/TemplateUploadBlackListAML.xls">Download Template</asp:HyperLink></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 852px;">
                                            <asp:CustomValidator ID="cvalPageErr" runat="server" Display="None"> </asp:CustomValidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:View>
                <asp:View ID="UploadBlacklisAMLSuccessData" runat="server">
                    <table align="center" bgcolor="#dddddd" border="2" bordercolor="#ffffff" cellpadding="2"
                        cellspacing="1" width="100%">
                        <tr id="Tr1">
                            <td id="Td1" bgcolor="#ffffff" style="height: 80px" valign="top" width="100%">
                                <table border="0" cellpadding="0" cellspacing="4" width="100%">
                                    <tr>
                                        <td style="width: 82px; height: 18px">
                                        </td>
                                        <td style="width: 1%; height: 18px; font-weight: bold">
                                            Total Success Data
                                        </td>
                                        <td align="center" style="width: 18px; height: 18px" width="100%">
                                            :
                                        </td>
                                        <td style="width: 100%; height: 18px">
                                            <asp:Label ID="lblValueSuccessData" runat="server">0</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 82px; height: 18px">
                                        </td>
                                        <td align="left" style="width: 1%; height: 18px; font-weight: bold">
                                            Total Failed Data
                                        </td>
                                        <td align="center" style="width: 18px; height: 18px" width="100%">
                                            :
                                        </td>
                                        <td style="width: 100%; height: 18px">
                                            <asp:Label ID="lblValueFailedData" runat="server">0</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 82px; height: 10px">
                                        </td>
                                        <td style="width: 1%; height: 10px; font-weight: bold">
                                            Total Uploaded data
                                        </td>
                                        <td align="center" style="width: 18px; height: 10px" width="100%">
                                            :
                                        </td>
                                        <td style="width: 100%; height: 10px">
                                            <asp:Label ID="lblValueuploadeddata" runat="server">0</asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <table id="tablesukses" width="99%" bgcolor="White">
                        <tr>
                            <td background="images/search-bar-background.gif" valign="middle" style="width: 99%;
                                height: 20px; border-right: gray thin solid; border-top: gray thin solid; border-left: gray thin solid;
                                border-bottom: gray thin solid;">
                                <table cellspacing="0" cellpadding="3" border="0" width="99%">
                                    <tr>
                                        <td valign="bottom" align="left" width="15">
                                            <img height="15" src="images/done.png" width="15">
                                        </td>
                                        <td class="maintitle" valign="bottom">
                                            <asp:Label ID="Label6" runat="server" Text="Success Data">
                                            </asp:Label>
                                            &nbsp;:&nbsp;<asp:Label ID="lbljumlahsukses" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100%;">
                                <table cellspacing="0" cellpadding="3" border="0" width="99%">
                                    <tr>
                                        <td bgcolor="#ffffff" style="width: 99%;">
                                            <asp:DataGrid ID="GridSuccessList" runat="server" CellPadding="4" AllowPaging="True"
                                                Width="100%" GridLines="None" AllowSorting="True" ForeColor="White" BorderColor="#003300"
                                                BorderStyle="Solid" AutoGenerateColumns="False" Font-Bold="False" Font-Italic="False"
                                                Font-Overline="False" Font-Strikeout="False" Font-Underline="False" 
                                                HorizontalAlign="Center" ShowFooter="True">
                                                <PagerStyle HorizontalAlign="Center" ForeColor="White" BackColor="#666666"></PagerStyle>
                                                <HeaderStyle Font-Bold="True" ForeColor="White" Wrap="False" BackColor="#1C5E55"
                                                    Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False"
                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <Columns>
                                                    <asp:BoundColumn HeaderText="No">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="PK_UploadBlacklistAml_V_Upload_id" 
                                                        HeaderText="PK_UploadBlacklistAml_V_Upload_id" 
                                                        SortExpression="PK_UploadBlacklistAml_V_Upload_id" Visible="False">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Name" HeaderText="Name" SortExpression="Name">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="VerificationListType" HeaderText="VerificationListType" 
                                                        SortExpression="VerificationListType">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn HeaderText="Category" DataField="Category">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Alias1" HeaderText="Alias1">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Alias2" HeaderText="Alias2">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Alias3" HeaderText="Alias3">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Alias4" HeaderText="Alias4">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Alias5" HeaderText="Alias5">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="DateOfBirth" HeaderText="DateOfBirth" 
                                                        SortExpression="DateOfBirth">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="BirthPlace" 
                                                        HeaderText="BirthPlace">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Address1" 
                                                        HeaderText="Address1">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Address2" HeaderText="Address2">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Address3" HeaderText="Address3">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Address4" HeaderText="Address4">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                            Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Address5" HeaderText="Address5">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                            Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="IDNumber1" HeaderText="IDNumber1">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                            Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="IDNumber2" HeaderText="IDNumber2">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                            Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="IDNumber3" HeaderText="IDNumber3">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                            Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="IDNumber4" HeaderText="IDNumber4">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                            Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="IDNumber5" HeaderText="IDNumber5">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                            Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="CustomRemarks1" HeaderText="CustomRemarks1">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                            Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="CustomRemarks2" HeaderText="CustomRemarks2">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                            Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="CustomRemarks3" HeaderText="CustomRemarks3">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                            Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="CustomRemarks4" HeaderText="CustomRemarks4">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                            Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="CustomRemarks5" HeaderText="CustomRemarks5">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                            Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                                                    </asp:BoundColumn>
                                                </Columns>
                                                <EditItemStyle BackColor="#7C6F57" />
                                                <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                                                <SelectedItemStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                                                <AlternatingItemStyle BackColor="White" />
                                                <ItemStyle BackColor="#E3EAEB" />
                                            </asp:DataGrid>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <table id="tableanomaly" bgcolor="White" style="width: 100%">
                        <tr>
                            <td background="images/search-bar-background.gif" valign="middle" style="width: 99%;
                                height: 20px; border-right: gray thin solid; border-top: gray thin solid; border-left: gray thin solid;
                                border-bottom: gray thin solid;" bgcolor="White" width="100%">
                                <table cellspacing="0" cellpadding="3" border="0" width="100%">
                                    <tr>
                                        <td valign="bottom" align="left" width="15">
                                            <img height="15" src="images/warning.png" width="15">
                                        </td>
                                        <td class="maintitle" valign="bottom">
                                            <asp:Label ID="Label4" runat="server" Text="Anomaly Data"></asp:Label>
                                            &nbsp;:&nbsp;<asp:Label ID="lbljumlahanomalydata" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100%;" bgcolor="White" width="100%">
                                <table cellspacing="0" cellpadding="3" border="0" width="100%">
                                    <tr>
                                        <td bgcolor="#ffffff" style="width: 99%;">
                                            <asp:DataGrid ID="GridAnomalyList" runat="server" CellPadding="4" Width="100%" GridLines="None"
                                                AllowSorting="True" ForeColor="#333333" AutoGenerateColumns="False" BorderColor="Maroon"
                                                BorderStyle="Solid" BackColor="White" AllowPaging="True">
                                                <PagerStyle HorizontalAlign="Center" ForeColor="#333333" BackColor="#FFCC66"></PagerStyle>
                                                <HeaderStyle Font-Bold="True" ForeColor="White" Wrap="False" BackColor="#990000" />
                                                <Columns>
                                                    <asp:BoundColumn DataField="Error_description" HeaderText="Description">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="RecordNumber" HeaderText="Error Line Number">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" />
                                                        <ItemStyle Width="20%" />
                                                    </asp:BoundColumn>
                                                </Columns>
                                                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                <SelectedItemStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                <AlternatingItemStyle BackColor="White" />
                                                <ItemStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                            </asp:DataGrid>
                                           
                                            <asp:Button ID="BtnSave" runat="server" Font-Names="Calibri" Height="20px" Text="Save"
                                                Width="51px" />
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <asp:Button ID="BtnCancel" runat="server" Font-Names="Calibri" Height="20px" Text="Cancel"
                                                Width="51px" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="White" width="100%">
                            </td>
                        </tr>
                    </table>
                </asp:View>
                <asp:View ID="VmBlacklistConfirmation" runat="server">
                    <table style="width: 100%" bordercolor="#ffffff" cellspacing="1" cellpadding="2"
                        width="100%" bgcolor="#dddddd" border="0">
                        <tr bgcolor="#ffffff">
                            <td colspan="2" align="center" style="height: 17px">
                                <asp:Label ID="LblConfirmation" runat="server" meta:resourcekey="LblConfirmationResource1"></asp:Label>
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="center" colspan="2">
                                <asp:ImageButton ID="ImgBtnAdd" runat="server" ImageUrl="~/images/button/Ok.gif"
                                    CausesValidation="False" meta:resourcekey="ImgBtnAddResource1" />
                            </td>
                        </tr>
                    </table>
                </asp:View>
            </asp:MultiView>
        </ajax:AjaxPanel>
        <ajax:AjaxPanel ID="AjaxPanel14" runat="server" meta:resourcekey="AjaxPanel14Resource1">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" background="Images/button-bground.gif" valign="middle">
                        <img height="1" src="Images/blank.gif" width="5" />
                    </td>
                    <td align="left" background="Images/button-bground.gif" valign="middle">
                        <img height="15" src="images/arrow.gif" width="15" />&nbsp;
                    </td>
                    <td background="Images/button-bground.gif" style="width: 5px" align="center" valign="middle">
                        <asp:ImageButton ID="Imgbtnsave" runat="server" ImageUrl="~/images/button/save.gif"
                            CausesValidation="False" meta:resourcekey="ImgBackAddResource1" Visible="False" />
                    </td>
                    <td background="Images/button-bground.gif">
                        <asp:ImageButton ID="Imgbtncancel" runat="server" ImageUrl="~/images/button/cancel.gif"
                            CausesValidation="False" meta:resourcekey="ImgBackAddResource1" Visible="False"
                            Style="margin-left: 4px" />
                    </td>
                    <td background="Images/button-bground.gif" width="99%">
                        <img height="1" src="Images/blank.gif" width="1" />
                    </td>
                    <td style="width: 25px">
                    </td>
                </tr>
            </table>
            <asp:CustomValidator ID="CvalHandleErr" runat="server" Display="None" ValidationGroup="handle"
                meta:resourcekey="CvalHandleErrResource1"></asp:CustomValidator>
            <asp:CustomValidator ID="CustomValidator1" runat="server" Display="None" meta:resourcekey="CvalPageErrResource1"> </asp:CustomValidator>
        </ajax:AjaxPanel>
    </div>
</asp:Content>
