Option Explicit On
Option Strict On
Imports EkaDataNettier.Entities
Imports EkaDataNettier.Data

Imports AMLBLL

Partial Class AuditTrailPotensialScreeningCustomer
    Inherits Parent

#Region "SetnGet Searching"
    Public Property SetnGetUserID() As String
        Get
            If Not Session("AuditTrailPotensialScreeningCustomer.SetnGetUserID") Is Nothing Then
                Return CStr(Session("AuditTrailPotensialScreeningCustomer.SetnGetUserID"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("AuditTrailPotensialScreeningCustomer.SetnGetUserID") = value
        End Set
    End Property
    Public Property SetnGetUserName() As String
        Get
            If Not Session("AuditTrailPotensialScreeningCustomer.SetnGetUserName") Is Nothing Then
                Return CStr(Session("AuditTrailPotensialScreeningCustomer.SetnGetUserName"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("AuditTrailPotensialScreeningCustomer.SetnGetUserName") = value
        End Set
    End Property
    Public Property SetnGetScreeningDate() As String
        Get
            If Not Session("AuditTrailPotensialScreeningCustomer.SetnGetScreeningDate") Is Nothing Then
                Return CStr(Session("AuditTrailPotensialScreeningCustomer.SetnGetScreeningDate"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("AuditTrailPotensialScreeningCustomer.SetnGetScreeningDate") = value
        End Set
    End Property
    Public Property SetnGetNama() As String
        Get
            If Not Session("AuditTrailPotensialScreeningCustomer.SetnGetNama") Is Nothing Then
                Return CStr(Session("AuditTrailPotensialScreeningCustomer.SetnGetNama"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("AuditTrailPotensialScreeningCustomer.SetnGetNama") = value
        End Set
    End Property
    Public Property SetnGetDateOfBirth() As String
        Get
            If Not Session("AuditTrailPotensialScreeningCustomer.SetnGetDateOfBirth") Is Nothing Then
                Return CStr(Session("AuditTrailPotensialScreeningCustomer.SetnGetDateOfBirth"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("AuditTrailPotensialScreeningCustomer.SetnGetDateOfBirth") = value
        End Set
    End Property
    Public Property SetnGetNationality() As String
        Get
            If Not Session("AuditTrailPotensialScreeningCustomer.Address") Is Nothing Then
                Return CStr(Session("AuditTrailPotensialScreeningCustomer.Address"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("AuditTrailPotensialScreeningCustomer.Address") = value
        End Set
    End Property
    Public Property SetnGetIDNumber() As String
        Get
            If Not Session("AuditTrailPotensialScreeningCustomer.IDNumber") Is Nothing Then
                Return CStr(Session("AuditTrailPotensialScreeningCustomer.IDNumber"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("AuditTrailPotensialScreeningCustomer.IDNumber") = value
        End Set
    End Property
    Public Property SetnGetSuspectMatch() As String
        Get
            If Not Session("AuditTrailPotensialScreeningCustomer.SuspectMatch") Is Nothing Then
                Return CStr(Session("AuditTrailPotensialScreeningCustomer.SuspectMatch"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("AuditTrailPotensialScreeningCustomer.SuspectMatch") = value
        End Set
    End Property
#End Region

#Region "SetGrid"
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return CType(IIf(Session("AuditTrailPotensialScreeningCustomer.SelectedItem") Is Nothing, New ArrayList, Session("AuditTrailPotensialScreeningCustomer.SelectedItem")), ArrayList)
        End Get
        Set(ByVal value As ArrayList)
            Session("AuditTrailPotensialScreeningCustomer.SelectedItem") = value
        End Set
    End Property
    Private Property SetnGetSort() As String
        Get
            Return CType(IIf(Session("AuditTrailPotensialScreeningCustomer.Sort") Is Nothing, "PK_AML_Screening_Customer_Request_Detail_ID  asc", Session("AuditTrailPotensialScreeningCustomer.Sort")), String)
        End Get
        Set(ByVal Value As String)
            Session("AuditTrailPotensialScreeningCustomer.Sort") = Value
        End Set
    End Property
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return CType(IIf(Session("AuditTrailPotensialScreeningCustomer.CurrentPage") Is Nothing, 0, Session("AuditTrailPotensialScreeningCustomer.CurrentPage")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("AuditTrailPotensialScreeningCustomer.CurrentPage") = Value
        End Set
    End Property
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    Private Property SetnGetRowTotal() As Int32
        Get
            Return CType(IIf(Session("AuditTrailPotensialScreeningCustomer.RowTotal") Is Nothing, 0, Session("AuditTrailPotensialScreeningCustomer.RowTotal")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("AuditTrailPotensialScreeningCustomer.RowTotal") = Value
        End Set
    End Property
    Public Property SetAndGetSearchingCriteria() As String
        Get
            Return CStr(IIf(Session("MsUserViewAppPageSearchCriteria") Is Nothing, "", Session("MsUserViewAppPageSearchCriteria")))
        End Get
        Set(ByVal value As String)
            Session("MsUserViewAppPageSearchCriteria") = value
        End Set
    End Property

    Public Property SetnGetBindTable() As VList(Of vw_AML_Screening_AuditTrail)
        Get
            Dim strWhereClause(-1) As String
            Dim strAllWhereClause As String = ""
            If Me.SetnGetCurrentPage > Me.GetPageTotal - 1 And Me.GetPageTotal - 1 > 0 Then
                Me.SetnGetCurrentPage = Me.GetPageTotal - 1
            End If

            If SetnGetUserID.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "UserID like '%" & SetnGetUserID.Trim.Replace("'", "''") & "%'"
            End If
            If SetnGetUserName.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "UserName like '%" & SetnGetUserName.Trim.Replace("'", "''") & "%'"
            End If
            'SetnGetScreeningDate
            If SetnGetScreeningDate.Length > 0 Then
                If Sahassa.AML.Commonly.IsDateValid("dd-MM-yyyy", SetnGetScreeningDate) Then
                    Dim tanggalScreen As String = Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", SetnGetScreeningDate).ToString("yyyy-MM-dd")
                    ReDim Preserve strWhereClause(strWhereClause.Length)
                    strWhereClause(strWhereClause.Length - 1) = vw_AML_Screening_AuditTrailColumn.CreatedDate.ToString & " between '" & tanggalScreen & " 00:00:00' and '" & tanggalScreen & " 23:59:59'"
                End If
            End If

            If SetnGetNama.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "Nama like '%" & SetnGetNama.Trim.Replace("'", "''") & "%'"
            End If
            'SetnGetDateOfBirth
            If SetnGetDateOfBirth.Length > 0 Then
                If Sahassa.AML.Commonly.IsDateValid("dd-MM-yyyy", SetnGetDateOfBirth) AndAlso Sahassa.AML.Commonly.IsDateValid("dd-MM-yyyy", SetnGetDateOfBirth) Then
                    Dim tanggalBirth As String = Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", SetnGetDateOfBirth).ToString("yyyy-MM-dd")
                    ReDim Preserve strWhereClause(strWhereClause.Length)
                    strWhereClause(strWhereClause.Length - 1) = vw_AML_Screening_AuditTrailColumn.DOB.ToString & " between '" & tanggalBirth & " 00:00:00' and '" & tanggalBirth & " 23:59:59'"
                End If
            End If
            If SetnGetNationality.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = vw_AML_Screening_AuditTrailColumn.Nationality.ToString & " like '%" & SetnGetNationality.Trim.Replace("'", "''") & "%'"
            End If

            If SetnGetSuspectMatch.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "SuspectMatch like '%" & SetnGetSuspectMatch.Trim.Replace("'", "''") & "%'"
            End If

            strAllWhereClause = String.Join(" and ", strWhereClause)
            If strAllWhereClause.Trim.Length > 0 Then
                strAllWhereClause += " and  UserID IN (select UserId from UserWorkingUnitAssignment Where WorkingUnitId in (select WorkingUnitId from UserWorkingUnitAssignment where UserId = '" & Sahassa.AML.Commonly.SessionUserId & "')) "
            Else
                strAllWhereClause += " UserID IN (select UserId from UserWorkingUnitAssignment Where WorkingUnitId in (select WorkingUnitId from UserWorkingUnitAssignment where UserId = '" & Sahassa.AML.Commonly.SessionUserId & "')) "
            End If
            Session("AuditTrailPotensialScreeningCustomer.Table") = DataRepository.vw_AML_Screening_AuditTrailProvider.GetPaged(strAllWhereClause, SetnGetSort, SetnGetCurrentPage, Sahassa.AML.Commonly.GetDisplayedTotalRow, Me.SetnGetRowTotal)

            Return CType(Session("AuditTrailPotensialScreeningCustomer.Table"), VList(Of vw_AML_Screening_AuditTrail))
        End Get
        Set(ByVal value As VList(Of vw_AML_Screening_AuditTrail))
            Session("AuditTrailPotensialScreeningCustomer.Table") = value
        End Set
    End Property

    Public Property SetnGetBindTableAll() As VList(Of vw_AML_Screening_AuditTrail)
        Get
            Dim strWhereClause(-1) As String
            Dim strAllWhereClause As String = ""
            If Me.SetnGetCurrentPage > Me.GetPageTotal - 1 And Me.GetPageTotal - 1 > 0 Then
                Me.SetnGetCurrentPage = Me.GetPageTotal - 1
            End If
            If SetnGetUserID.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "UserID like '%" & SetnGetUserID.Trim.Replace("'", "''") & "%'"
            End If
            If SetnGetUserName.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "UserName like '%" & SetnGetUserName.Trim.Replace("'", "''") & "%'"
            End If
            'SetnGetScreeningDate
            If SetnGetScreeningDate.Length > 0 Then
                If Sahassa.AML.Commonly.IsDateValid("dd-MM-yyyy", SetnGetScreeningDate) AndAlso Sahassa.AML.Commonly.IsDateValid("dd-MM-yyyy", SetnGetScreeningDate) Then
                    Dim tanggalScreen As String = Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", SetnGetScreeningDate).ToString("yyyy-MM-dd")
                    ReDim Preserve strWhereClause(strWhereClause.Length)
                    strWhereClause(strWhereClause.Length - 1) = vw_AML_Screening_AuditTrailColumn.CreatedDate.ToString & " between '" & tanggalScreen & " 00:00:00' and '" & tanggalScreen & " 23:59:59'"
                End If
            End If

            If SetnGetNama.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "Nama like '%" & SetnGetNama.Trim.Replace("'", "''") & "%'"
            End If
            'SetnGetDateOfBirth
            If SetnGetDateOfBirth.Length > 0 Then
                If Sahassa.AML.Commonly.IsDateValid("dd-MM-yyyy", SetnGetDateOfBirth) AndAlso Sahassa.AML.Commonly.IsDateValid("dd-MM-yyyy", SetnGetDateOfBirth) Then
                    Dim tanggalBirth As String = Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", SetnGetDateOfBirth).ToString("yyyy-MM-dd")
                    ReDim Preserve strWhereClause(strWhereClause.Length)
                    strWhereClause(strWhereClause.Length - 1) = vw_AML_Screening_AuditTrailColumn.DOB.ToString & " between '" & tanggalBirth & " 00:00:00' and '" & tanggalBirth & " 23:59:59'"
                End If
            End If
            If SetnGetNationality.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "Nationality like '%" & SetnGetNationality.Trim.Replace("'", "''") & "%'"
            End If

            If SetnGetSuspectMatch.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "SuspectMatch like '%" & SetnGetSuspectMatch.Trim.Replace("'", "''") & "%'"
            End If

            strAllWhereClause = String.Join(" and ", strWhereClause)

            If strAllWhereClause.Trim.Length > 0 Then
                strAllWhereClause += " and  UserID IN (select UserId from UserWorkingUnitAssignment Where WorkingUnitId in (select WorkingUnitId from UserWorkingUnitAssignment where UserId = '" & Sahassa.AML.Commonly.SessionUserId & "'))"
            Else
                strAllWhereClause += " UserID IN (select UserId from UserWorkingUnitAssignment Where WorkingUnitId in (select WorkingUnitId from UserWorkingUnitAssignment where UserId = '" & Sahassa.AML.Commonly.SessionUserId & "'))"
            End If

            'If strAllWhereClause.Trim.Length > 0 Then
            '    strAllWhereClause += " and FK_MsUserID <> " & Sahassa.AML.Commonly.SessionPkUserId & " AND FK_MsUserID IN (SELECT u.pkUserID FROM [User] u WHERE u.UserID IN(select UserId from UserWorkingUnitAssignment Where WorkingUnitId in (select WorkingUnitId from UserWorkingUnitAssignment where UserId = '" & Sahassa.AML.Commonly.SessionUserId & "')))"
            'Else
            '    strAllWhereClause += " FK_MsUserID <> " & Sahassa.AML.Commonly.SessionPkUserId & " AND FK_MsUserID IN (SELECT u.pkUserID FROM [User] u WHERE u.UserID IN(select UserId from UserWorkingUnitAssignment Where WorkingUnitId in (select WorkingUnitId from UserWorkingUnitAssignment where UserId = '" & Sahassa.AML.Commonly.SessionUserId & "')))"
            'End If
            Session("AuditTrailPotensialScreeningCustomer.TableALL") = DataRepository.vw_AML_Screening_AuditTrailProvider.GetPaged(strAllWhereClause, SetnGetSort, 0, Integer.MaxValue, 0)

            Return CType(Session("AuditTrailPotensialScreeningCustomer.TableALL"), VList(Of vw_AML_Screening_AuditTrail))
        End Get
        Set(ByVal value As VList(Of vw_AML_Screening_AuditTrail))
            Session("AuditTrailPotensialScreeningCustomer.Table") = value
        End Set
    End Property

#End Region


    Private Sub BindGrid()
        SettingControlSearching()
        Me.GridAuditTrailPotensialScreeningCustomer.DataSource = Me.SetnGetBindTable
        Me.GridAuditTrailPotensialScreeningCustomer.CurrentPageIndex = Me.SetnGetCurrentPage
        Me.GridAuditTrailPotensialScreeningCustomer.VirtualItemCount = Me.SetnGetRowTotal
        Me.GridAuditTrailPotensialScreeningCustomer.DataBind()

        If SetnGetRowTotal > 0 Then
            LabelNoRecordFound.Visible = False
        Else
            LabelNoRecordFound.Visible = True
        End If
    End Sub

    Private Sub ClearThisPageSessions()
        SetnGetUserID = Nothing
        SetnGetUserName = Nothing
        SetnGetScreeningDate = Nothing
        SetnGetNama = Nothing
        SetnGetDateOfBirth = Nothing
        SetnGetNationality = Nothing
        SetnGetIDNumber = Nothing
        SetnGetSuspectMatch = Nothing

        LblMessage.Visible = False
        LblMessage.Text = ""
        Me.SetnGetRowTotal = Nothing
        Me.SetnGetSort = Nothing
    End Sub

    Private Sub SettingPropertySearching()
        SetnGetUserID = TxtUserID.Text.Trim
        SetnGetUserName = TxtUserName.Text.Trim
        SetnGetScreeningDate = TxtEntryDateScreen.Text
        SetnGetNama = TxtNama.Text.Trim
        SetnGetDateOfBirth = TxtEntryDateBirth.Text
        SetnGetNationality = TxtNationality.Text.Trim

        SetnGetSuspectMatch = TxtSuspectMatch.Text.Trim
    End Sub

    Private Sub SettingControlSearching()
        TxtUserID.Text = SetnGetUserID
        TxtUserName.Text = SetnGetUserName
        TxtEntryDateScreen.Text = SetnGetScreeningDate
        TxtNama.Text = SetnGetNama
        TxtEntryDateBirth.Text = SetnGetDateOfBirth
        TxtNationality.Text = SetnGetNationality

        TxtSuspectMatch.Text = SetnGetSuspectMatch
    End Sub

    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In Me.GridAuditTrailPotensialScreeningCustomer.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = CType(gridRow.FindControl("CheckBoxExporttoExcel"), CheckBox)
                Dim PKAuditTrailPotensialScreeningCustomerID As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
                If ArrTarget.Contains(PKAuditTrailPotensialScreeningCustomerID) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(PKAuditTrailPotensialScreeningCustomerID)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(PKAuditTrailPotensialScreeningCustomerID)
                End If
                Me.SetnGetSelectedItem = ArrTarget
            End If
        Next
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            LblMessage.Text = ""
            LblMessage.Visible = False
            If Not Page.IsPostBack Then
                ClearThisPageSessions()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using

                Me.popUpEntryDateScreen.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TxtEntryDateScreen.ClientID & "'), 'dd-mm-yyyy')")
                Me.popUpEntryDateBirth.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TxtEntryDateBirth.ClientID & "'), 'dd-mm-yyyy')")

            End If
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
            CollectSelected()
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBtnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnSearch.Click
        Try
            SetnGetCurrentPage = 0
            SettingPropertySearching()
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub SetInfoNavigate()
        Me.PageCurrentPage.Text = (Me.SetnGetCurrentPage + 1).ToString
        Me.PageTotalPages.Text = Me.GetPageTotal.ToString
        Me.PageTotalRows.Text = Me.SetnGetRowTotal.ToString
        Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
        Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
        Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
        Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
            Me.SetInfoNavigate()
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBtnClearSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnClearSearch.Click
        Try
            SetnGetUserID = Nothing
            SetnGetUserName = Nothing
            SetnGetScreeningDate = Nothing
            SetnGetNama = Nothing
            SetnGetDateOfBirth = Nothing
            SetnGetNationality = Nothing
            SetnGetIDNumber = Nothing
            SetnGetSuspectMatch = Nothing

        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        'For Each gridRow As DataGridItem In Me.GridAuditTrailPotensialScreeningCustomer.Items
        '    If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
        '        Dim pkid As String = gridRow.Cells(1).Text
        '        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        '        If Me.SetnGetSelectedItem.Contains(pkid) Then
        '            If Me.CheckBoxSelectAll.Checked Then
        '                If Not ArrTarget.Contains(pkid) Then
        '                    ArrTarget.Add(pkid)
        '                End If
        '            Else
        '                ArrTarget.Remove(pkid)
        '            End If
        '        Else
        '            If Me.CheckBoxSelectAll.Checked Then
        '                If Not ArrTarget.Contains(pkid) Then
        '                    ArrTarget.Add(pkid)
        '                End If
        '            End If
        '        End If
        '        Me.SetnGetSelectedItem = ArrTarget
        '    End If
        'Next
        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        For Each gridRow As DataGridItem In Me.GridAuditTrailPotensialScreeningCustomer.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(1).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItem.Contains(StrKey) = False Then
                        ArrTarget.Add(StrKey)
                    End If
                Else
                    If Me.SetnGetSelectedItem.Contains(StrKey) Then
                        ArrTarget.Remove(StrKey)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItem = ArrTarget
    End Sub

    Public Sub BindGridView()
        GridAuditTrailPotensialScreeningCustomer.DataSource = Me.SetnGetBindTable
        GridAuditTrailPotensialScreeningCustomer.VirtualItemCount = Me.SetnGetRowTotal
        GridAuditTrailPotensialScreeningCustomer.DataBind()
    End Sub

    Protected Sub GridAuditTrailPotensialScreeningCustomer_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridAuditTrailPotensialScreeningCustomer.EditCommand
        Dim PKAuditTrailPotensialScreeningCustomerid As Integer
        Try
            PKAuditTrailPotensialScreeningCustomerid = CInt(e.Item.Cells(1).Text)
            Response.Redirect("AuditTrailPotensialScreeningCustomerDetail.aspx?PKAuditTrailPotensialScreeningCustomerid=" & PKAuditTrailPotensialScreeningCustomerid, False)
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub GridAuditTrailPotensialScreeningCustomer_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridAuditTrailPotensialScreeningCustomer.ItemCommand
        Dim BActivationNeedApproval As Boolean = False
        Dim PKAuditTrailPotensialScreeningCustomerid As Integer
        Dim strUserID As String 'uniq
        Dim objVeryvy As CheckBox = CType(e.Item.FindControl("Verify"), CheckBox)
        PKAuditTrailPotensialScreeningCustomerid = CType(e.Item.Cells(1).Text, Integer)
        strUserID = e.Item.Cells(2).Text

        Try
            If e.CommandName.ToLower = "verify" Then
                If objVeryvy.Checked = True Then
                    Using ObjAuditTrail_Potensial_Screening_Customer_Update As EkaDataNettier.Entities.AML_Screening_Customer_Request_Detail = DataRepository.AML_Screening_Customer_Request_DetailProvider.GetByPK_AML_Screening_Customer_Request_Detail_ID(PKAuditTrailPotensialScreeningCustomerid)
                        ObjAuditTrail_Potensial_Screening_Customer_Update.verify = False
                        ObjAuditTrail_Potensial_Screening_Customer_Update.verifyUser = Sahassa.AML.Commonly.SessionUserId
                        ObjAuditTrail_Potensial_Screening_Customer_Update.verifyDate = Nothing
                        DataRepository.AML_Screening_Customer_Request_DetailProvider.Save(ObjAuditTrail_Potensial_Screening_Customer_Update)
                        BindGrid()
                    End Using
                Else
                    Using ObjAuditTrail_Potensial_Screening_Customer_Update As EkaDataNettier.Entities.AML_Screening_Customer_Request_Detail = DataRepository.AML_Screening_Customer_Request_DetailProvider.GetByPK_AML_Screening_Customer_Request_Detail_ID(PKAuditTrailPotensialScreeningCustomerid)
                        ObjAuditTrail_Potensial_Screening_Customer_Update.verify = True
                        ObjAuditTrail_Potensial_Screening_Customer_Update.verifyUser = Sahassa.AML.Commonly.SessionUserId
                        ObjAuditTrail_Potensial_Screening_Customer_Update.verifyDate = Now
                        DataRepository.AML_Screening_Customer_Request_DetailProvider.Save(ObjAuditTrail_Potensial_Screening_Customer_Update)
                        BindGrid()
                    End Using
                End If
            End If

        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub GridAuditTrailPotensialScreeningCustomer_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridAuditTrailPotensialScreeningCustomer.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Header And e.Item.ItemType <> ListItemType.Footer Then
                Dim PK_AuditTrail_Potensial_Screening_Customer_ID As String = e.Item.Cells(1).Text
                Dim objcheckbox As CheckBox = CType(e.Item.FindControl("CheckBoxExporttoExcel"), CheckBox)
                objcheckbox.Checked = Me.SetnGetSelectedItem.Contains(PK_AuditTrail_Potensial_Screening_Customer_ID)

                Dim VerifyButton As CheckBox = CType(e.Item.Cells(11).FindControl("Verify"), CheckBox)
                Dim Verify As LinkButton = CType(e.Item.Cells(11).FindControl("lnkVerify"), LinkButton)

                Using ObjAuditTrail_Potensial_Screening_Customer As AML_Screening_Customer_Request_Detail = DataRepository.AML_Screening_Customer_Request_DetailProvider.GetByPK_AML_Screening_Customer_Request_Detail_ID(CLng(PK_AuditTrail_Potensial_Screening_Customer_ID))

                    Dim userid As String = e.Item.Cells(2).Text

                    If userid <> Sahassa.AML.Commonly.SessionUserId Then

                        If ObjAuditTrail_Potensial_Screening_Customer.verify.GetValueOrDefault(False) = True Then
                            VerifyButton.Checked = True
                            Verify.Enabled = False
                        Else
                            VerifyButton.Checked = False
                            Verify.Enabled = True
                        End If
                    Else
                        If ObjAuditTrail_Potensial_Screening_Customer.verify.GetValueOrDefault(False) = True Then
                            VerifyButton.Checked = True
                            Verify.Enabled = False
                        Else
                            VerifyButton.Checked = False
                            Verify.Enabled = False
                        End If

                    End If


                End Using

            End If
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub GridAuditTrailPotensialScreeningCustomer_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridAuditTrailPotensialScreeningCustomer.SortCommand
        Dim GridUser As DataGrid = CType(source, DataGrid)
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub BindSelectedAll()
        Try
            Me.GridAuditTrailPotensialScreeningCustomer.DataSource = SetnGetBindTableAll
            Me.GridAuditTrailPotensialScreeningCustomer.AllowPaging = False
            Me.GridAuditTrailPotensialScreeningCustomer.DataBind()

            For i As Integer = 0 To GridAuditTrailPotensialScreeningCustomer.Items.Count - 1
                For y As Integer = 0 To GridAuditTrailPotensialScreeningCustomer.Columns.Count - 1
                    GridAuditTrailPotensialScreeningCustomer.Items(i).Cells(y).Attributes.Add("class", "text")
                Next
            Next
            Me.GridAuditTrailPotensialScreeningCustomer.Columns(0).Visible = False
            Me.GridAuditTrailPotensialScreeningCustomer.Columns(1).Visible = False
            Me.GridAuditTrailPotensialScreeningCustomer.Columns(10).Visible = False
            Me.GridAuditTrailPotensialScreeningCustomer.Columns(11).Visible = False
        Catch
            Throw
        End Try
    End Sub

    Private Sub BindSelected()
        Dim Rows As New ArrayList
        Dim AllList As VList(Of vw_AML_Screening_AuditTrail) = DataRepository.vw_AML_Screening_AuditTrailProvider.GetPaged("", "", 0, Integer.MaxValue, 0)

        For Each IdPk As Long In Me.SetnGetSelectedItem
            Dim oList As VList(Of vw_AML_Screening_AuditTrail) = AllList.FindAll(vw_AML_Screening_AuditTrailColumn.PK_AML_Screening_Customer_Request_Detail_ID.ToString, IdPk)
            If oList.Count > 0 Then
                Rows.Add(oList(0))
            End If
        Next
        Me.GridAuditTrailPotensialScreeningCustomer.DataSource = Rows
        Me.GridAuditTrailPotensialScreeningCustomer.AllowPaging = False
        Me.GridAuditTrailPotensialScreeningCustomer.DataBind()
        For i As Integer = 0 To GridAuditTrailPotensialScreeningCustomer.Items.Count - 1
            For y As Integer = 0 To GridAuditTrailPotensialScreeningCustomer.Columns.Count - 1
                GridAuditTrailPotensialScreeningCustomer.Items(i).Cells(y).Attributes.Add("class", "text")
            Next
        Next

        Me.GridAuditTrailPotensialScreeningCustomer.Columns(0).Visible = False
        Me.GridAuditTrailPotensialScreeningCustomer.Columns(1).Visible = False
        Me.GridAuditTrailPotensialScreeningCustomer.Columns(10).Visible = False
        'Me.GridAuditTrailPotensialScreeningCustomer.Columns(11).Visible = False
    End Sub

    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub 'ClearControls

    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Me.CollectSelected()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelected()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=AuditTrailPotensialScreeningCustomer.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridAuditTrailPotensialScreeningCustomer)
            GridAuditTrailPotensialScreeningCustomer.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub lnkExportAllData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExportAllData.Click
        Try
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            BindSelectedAll()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=AuditTrailPotensialScreeningCustomerAll.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridAuditTrailPotensialScreeningCustomer)
            GridAuditTrailPotensialScreeningCustomer.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            Me.CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) And (CInt(Me.TextGoToPage.Text) > 0) Then
                If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                    Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                Else
                    Throw New Exception("Page number must be less than or equal to the total page count.")
                End If
            Else
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If
            CollectSelected()
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
End Class
