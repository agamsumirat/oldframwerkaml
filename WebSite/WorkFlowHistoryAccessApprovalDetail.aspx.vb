﻿Imports SahassaNettier.Entities
Imports AMLBLL
Imports SahassaNettier.Data
Imports System.Diagnostics

Partial Class WorkFlowHistoryAccessApprovalDetail
    Inherits Parent

    Public ReadOnly Property objPkPendingID() As Long
        Get
            Dim strtemp As String = Request.Params("PKWorkflowHistoryAccessApprovalID")
            Dim lngresult As Long
            If Long.TryParse(strtemp, lngresult) Then
                Return lngresult
            Else
                Throw New SahassaException("PKWorkflowHistoryAccessApprovalID is not Valid.")
            End If
        End Get
    End Property


    Public Property ObjTWorkflowHistoryAccessDetail() As TList(Of WorkflowHistoryAccessDetail)
        Get
            If Session("WorkFlowHistoryAccessApprovalDetail.ObjTWorkflowHistoryAccessDetail ") Is Nothing Then
                Session("WorkFlowHistoryAccessApprovalDetail.ObjTWorkflowHistoryAccessDetail ") = WorkFlowHistoryAccessBLL.GetTlistWorkflowHistoryAccessDetail(WorkflowHistoryAccessDetailColumn.FK_WorkflowHistoryAccess_ID.ToString & "=" & ObjTWorkflowHistoryAccess_ApprovalDetail(0).PK_WorkflowHistoryAccess_ID.GetValueOrDefault(0), "", 0, Integer.MaxValue, 0)
                Return CType(Session("WorkFlowHistoryAccessApprovalDetail.ObjTWorkflowHistoryAccessDetail "), TList(Of WorkflowHistoryAccessDetail))
            Else
                Return CType(Session("WorkFlowHistoryAccessApprovalDetail.ObjTWorkflowHistoryAccessDetail "), TList(Of WorkflowHistoryAccessDetail))
            End If
        End Get
        Set(value As TList(Of WorkflowHistoryAccessDetail))
            Session("WorkFlowHistoryAccessApprovalDetail.ObjTWorkflowHistoryAccessDetail ") = value
        End Set
    End Property


    Public Property ObjTWorkflowHistoryAccess_ApprovalDetail() As TList(Of WorkflowHistoryAccess_ApprovalDetail)
        Get
            If Session("WorkFlowHistoryAccessApprovalDetail.ObjTWorkflowHistoryAccess_ApprovalDetail") Is Nothing Then
                Session("WorkFlowHistoryAccessApprovalDetail.ObjTWorkflowHistoryAccess_ApprovalDetail") = WorkFlowHistoryAccessBLL.GetTlistWorkflowHistoryAccess_ApprovalDetail(WorkflowHistoryAccess_ApprovalDetailColumn.FK_WorkflowHistoryAccess_ApprovalID.ToString & "=" & objPkPendingID, "", 0, Integer.MaxValue, 0)
                Return CType(Session("WorkFlowHistoryAccessApprovalDetail.ObjTWorkflowHistoryAccess_ApprovalDetail"), TList(Of WorkflowHistoryAccess_ApprovalDetail))
            Else
                Return CType(Session("WorkFlowHistoryAccessApprovalDetail.ObjTWorkflowHistoryAccess_ApprovalDetail"), TList(Of WorkflowHistoryAccess_ApprovalDetail))
            End If
        End Get
        Set(value As TList(Of WorkflowHistoryAccess_ApprovalDetail))
            Session("WorkFlowHistoryAccessApprovalDetail.ObjTWorkflowHistoryAccess_ApprovalDetail") = value
        End Set
    End Property

    Public Property ObjWorkflowHistoryAccessDetail_ApprovalDetail() As TList(Of WorkflowHistoryAccessDetail_ApprovalDetail)
        Get
            If Session("WorkFlowHistoryAccessApprovalDetail.ObjWorkflowHistoryAccessDetail_ApprovalDetail") Is Nothing Then
                Session("WorkFlowHistoryAccessApprovalDetail.ObjWorkflowHistoryAccessDetail_ApprovalDetail") = WorkFlowHistoryAccessBLL.GetTlistWorkflowHistoryAccessDetail_ApprovalDetail(WorkflowHistoryAccessDetail_ApprovalDetailColumn.FK_WorkflowHistoryAccess_ApprovalID.ToString & "=" & Me.objPkPendingID, "", 0, Integer.MaxValue, 0)
                Return CType(Session("WorkFlowHistoryAccessApprovalDetail.ObjWorkflowHistoryAccessDetail_ApprovalDetail"), TList(Of WorkflowHistoryAccessDetail_ApprovalDetail))
            Else
                Return CType(Session("WorkFlowHistoryAccessApprovalDetail.ObjWorkflowHistoryAccessDetail_ApprovalDetail"), TList(Of WorkflowHistoryAccessDetail_ApprovalDetail))
            End If
        End Get
        Set(value As TList(Of WorkflowHistoryAccessDetail_ApprovalDetail))
            Session("WorkFlowHistoryAccessApprovalDetail.ObjWorkflowHistoryAccessDetail_ApprovalDetail") = value
        End Set
    End Property


    Public Property ObjWorkflowHistoryAccess_Approval() As WorkflowHistoryAccess_Approval
        Get
            If Session("WorkFlowHistoryAccessApprovalDetail.ObjWorkflowHistoryAccess_Approval ") Is Nothing Then
                Session("WorkFlowHistoryAccessApprovalDetail.ObjWorkflowHistoryAccess_Approval ") = WorkFlowHistoryAccessBLL.GetWorkflowHistoryAccess_ApprovalByPk(Me.objPkPendingID)
                Return CType(Session("WorkFlowHistoryAccessApprovalDetail.ObjWorkflowHistoryAccess_Approval "), WorkflowHistoryAccess_Approval)
            Else
                Return CType(Session("WorkFlowHistoryAccessApprovalDetail.ObjWorkflowHistoryAccess_Approval "), WorkflowHistoryAccess_Approval)
            End If
        End Get
        Set(value As WorkflowHistoryAccess_Approval)
            Session("WorkFlowHistoryAccessApprovalDetail.ObjWorkflowHistoryAccess_Approval ") = value
        End Set
    End Property




    'Public Property ObjDataUpdatingParameterAlert_Approval() As DataUpdatingParameterAlert_Approval
    '    Get
    '        If Session("DataUpdatingAlertApprovalDetail.ObjDataUpdatingParameterAlert_Approval") Is Nothing Then
    '            Session("DataUpdatingAlertApprovalDetail.ObjDataUpdatingParameterAlert_Approval") = DataUpdatingAlertBLL.GetDataUpdatingParameterAlert_ApprovalByPk(Me.objPkPendingID)
    '            Return CType(Session("DataUpdatingAlertApprovalDetail.ObjDataUpdatingParameterAlert_Approval"), DataUpdatingParameterAlert_Approval)
    '        Else
    '            Return CType(Session("DataUpdatingAlertApprovalDetail.ObjDataUpdatingParameterAlert_Approval"), DataUpdatingParameterAlert_Approval)
    '        End If
    '    End Get
    '    Set(value As DataUpdatingParameterAlert_Approval)
    '        Session("DataUpdatingAlertApprovalDetail.ObjDataUpdatingParameterAlert_Approval") = value
    '    End Set
    'End Property


    'Public Property ObjTDataUpdatingParameterAlert_ApprovalDetail() As TList(Of DataUpdatingParameterAlert_ApprovalDetail)
    '    Get
    '        If Session("StatementGeneratorAdd.ObjTDataUpdatingParameterAlert_ApprovalDetail") Is Nothing Then
    '            Session("StatementGeneratorAdd.ObjTDataUpdatingParameterAlert_ApprovalDetail") = DataUpdatingAlertBLL.GetTlistDataUpdatingParameterAlert_ApprovalDetail(DataUpdatingParameterAlert_ApprovalDetailColumn.FK_DataUpdatingParameterAlert_ApprovalID.ToString & "=" & Me.objPkPendingID, "", 0, Integer.MaxValue, 0)
    '            Return CType(Session("StatementGeneratorAdd.ObjTDataUpdatingParameterAlert_ApprovalDetail"), TList(Of DataUpdatingParameterAlert_ApprovalDetail))
    '        Else
    '            Return CType(Session("StatementGeneratorAdd.ObjTDataUpdatingParameterAlert_ApprovalDetail"), TList(Of DataUpdatingParameterAlert_ApprovalDetail))
    '        End If
    '    End Get
    '    Set(value As TList(Of DataUpdatingParameterAlert_ApprovalDetail))
    '        Session("StatementGeneratorAdd.ObjTDataUpdatingParameterAlert_ApprovalDetail") = value
    '    End Set
    'End Property

    Sub ClearSession()
        ObjTWorkflowHistoryAccessDetail = Nothing
        ObjTWorkflowHistoryAccess_ApprovalDetail = Nothing
        ObjWorkflowHistoryAccess_Approval = Nothing
        ObjWorkflowHistoryAccessDetail_ApprovalDetail = Nothing

    End Sub
    Sub LoadDataNew()

        If ObjTWorkflowHistoryAccess_ApprovalDetail.Count > 0 Then

            LblWorkflowStepNew.Text = ObjTWorkflowHistoryAccess_ApprovalDetail(0).WorkflowStep
        End If


        GrdAccessNew.DataSource = ObjWorkflowHistoryAccessDetail_ApprovalDetail
        GrdAccessNew.DataBind()

        
    End Sub
    Sub loaddataold()
        If ObjTWorkflowHistoryAccess_ApprovalDetail.Count > 0 Then

            LblWorkflowStepOld.Text = ObjTWorkflowHistoryAccess_ApprovalDetail(0).WorkflowStep_old
        End If
        GrdAccessOld.DataSource = ObjTWorkflowHistoryAccessDetail
        GrdAccessOld.DataBind()
    End Sub


    Sub LoadDataApproval()

        If Not ObjWorkflowHistoryAccess_Approval Is Nothing Then
            Using objUser As User = DataRepository.UserProvider.GetBypkUserID(ObjWorkflowHistoryAccess_Approval.FK_MsUserID.GetValueOrDefault(0))
                If Not objUser Is Nothing Then
                    LblRequestBy.Text = objUser.UserName
                End If
                LblRequestDate.Text = ObjWorkflowHistoryAccess_Approval.CreatedDate.GetValueOrDefault(Sahassa.AML.Commonly.GetDefaultDate).ToString("dd-mmm-yyyy").Replace("01-jan-0001", "")
                If ObjWorkflowHistoryAccess_Approval.FK_ModeID.GetValueOrDefault(0) = 1 Then
                    LblMode.Text = "add"
                ElseIf ObjWorkflowHistoryAccess_Approval.FK_ModeID.GetValueOrDefault(0) = 2 Then
                    LblMode.Text = "edit"
                ElseIf ObjWorkflowHistoryAccess_Approval.FK_ModeID.GetValueOrDefault(0) = 3 Then
                    LblMode.Text = "delete"
                End If

                Select Case ObjWorkflowHistoryAccess_Approval.FK_ModeID.GetValueOrDefault(0)
                    Case 1
                        PanelOld.Visible = False

                        PanelNew.Visible = True
                        LoadDataNew()
                    Case 2
                        PanelOld.Visible = True
                        PanelNew.Visible = True
                        LoadDataNew()
                        loaddataold()
                    Case 3
                        PanelOld.Visible = False
                        PanelNew.Visible = True
                        LoadDataNew()
                End Select

            End Using
        End If



        'If Not ObjDataUpdatingParameterAlert_Approval Is Nothing Then

        '    Using objUser As User = DataRepository.UserProvider.GetBypkUserID(ObjDataUpdatingParameterAlert_Approval.FK_MsUserID.GetValueOrDefault(0))
        '        If Not objUser Is Nothing Then
        '            LblRequestBy.Text = objUser.UserName
        '        End If
        '    End Using


        '    LblRequestDate.Text = ObjDataUpdatingParameterAlert_Approval.CreatedDate.GetValueOrDefault(Sahassa.AML.Commonly.GetDefaultDate).ToString("dd-MMM-yyyy").Replace("01-Jan-0001", "")
        '    If ObjDataUpdatingParameterAlert_Approval.FK_ModeID.GetValueOrDefault(0) = 1 Then
        '        LblMode.Text = "Add"
        '    ElseIf ObjDataUpdatingParameterAlert_Approval.FK_ModeID.GetValueOrDefault(0) = 2 Then
        '        LblMode.Text = "Edit"
        '    ElseIf ObjDataUpdatingParameterAlert_Approval.FK_ModeID.GetValueOrDefault(0) = 3 Then
        '        LblMode.Text = "Delete"
        '    End If
        '    If ObjTDataUpdatingParameterAlert_ApprovalDetail.Count > 0 Then
        '        Using objHigh As DataUpdatingParameterAlert_ApprovalDetail = ObjTDataUpdatingParameterAlert_ApprovalDetail.Find(DataUpdatingParameterAlert_ApprovalDetailColumn.PK_DataUpdatingParameterAlert_ID, 1)
        '            If Not objHigh Is Nothing Then
        '                ChkHighRiskCustomer.Checked = objHigh.Activation_old.GetValueOrDefault(False)
        '                LblYearHighRiskcustomer.Text = objHigh.YearOnOpeningCIF_old.GetValueOrDefault(0)

        '                ChkHighRiskCustomerNew.Checked = objHigh.Activation.GetValueOrDefault(False)
        '                LblYearHighRiskcustomerNew.Text = objHigh.YearOnOpeningCIF.GetValueOrDefault(0)
        '            End If
        '        End Using

        '        Using objNonHigh As DataUpdatingParameterAlert_ApprovalDetail = ObjTDataUpdatingParameterAlert_ApprovalDetail.Find(DataUpdatingParameterAlert_ApprovalDetailColumn.PK_DataUpdatingParameterAlert_ID, 2)
        '            If Not objNonHigh Is Nothing Then
        '                ChkNonHighRiskCustomer.Checked = objNonHigh.Activation_old.GetValueOrDefault(False)
        '                LblYearNonHighRiskCustomer.Text = objNonHigh.YearOnOpeningCIF_old.GetValueOrDefault(0)

        '                ChkNonHighRiskCustomerNew.Checked = objNonHigh.Activation.GetValueOrDefault(False)
        '                LblYearNonHighRiskCustomerNew.Text = objNonHigh.YearOnOpeningCIF.GetValueOrDefault(0)
        '            End If
        '        End Using

        '    End If
        'End If
    End Sub

    Protected Sub Page_Load1(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                ClearSession()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using

                LoadDataApproval()
            End If
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBtnAccept_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAccept.Click
        Try

            Select Case ObjWorkflowHistoryAccess_Approval.FK_ModeID.GetValueOrDefault(0)
                Case 1
                    If WorkFlowHistoryAccessBLL.acceptAdd(Me.objPkPendingID) Then
                        Response.Redirect("WorkFlowHistoryAccessApproval.aspx", False)
                    End If
                Case 2
                    If WorkFlowHistoryAccessBLL.acceptEdit(Me.objPkPendingID) Then
                        Response.Redirect("WorkFlowHistoryAccessApproval.aspx", False)
                    End If
                Case 3
                    If WorkFlowHistoryAccessBLL.acceptDelete(Me.objPkPendingID) Then
                        Response.Redirect("WorkFlowHistoryAccessApproval.aspx", False)
                    End If

            End Select



        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImgBtnReject_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnReject.Click
        Try
            Select Case ObjWorkflowHistoryAccess_Approval.FK_ModeID.GetValueOrDefault(0)
                Case 1
                    If WorkFlowHistoryAccessBLL.RejectAdd(Me.objPkPendingID) Then
                        Response.Redirect("WorkFlowHistoryAccessApproval.aspx", False)
                    End If
                Case 2
                    If WorkFlowHistoryAccessBLL.RejectEdit(Me.objPkPendingID) Then
                        Response.Redirect("WorkFlowHistoryAccessApproval.aspx", False)
                    End If
                Case 3
                    If WorkFlowHistoryAccessBLL.RejectDelete(Me.objPkPendingID) Then
                        Response.Redirect("WorkFlowHistoryAccessApproval.aspx", False)
                    End If

            End Select


        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImgBackAdd_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImgBackAdd.Click
        Try

            Response.Redirect("WorkFlowHistoryAccessApproval.aspx", False)

        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub
End Class
