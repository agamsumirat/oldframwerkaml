Option Explicit On
Option Strict On

Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports AMLBLL

Partial Class TransactionFrequencyApprovalDetail
    Inherits Parent

    Public ReadOnly Property PKTransactionFrequencyApprovalID() As Integer
        Get
            If Session("TransactionFrequencyApprovalDetail.PK") Is Nothing Then
                Dim StrTmppkTransactionFrequencyApprovalId As String

                StrTmppkTransactionFrequencyApprovalId = Request.Params("PKTransactionFrequencyApprovalID")
                If Integer.TryParse(StrTmppkTransactionFrequencyApprovalId, 0) Then
                    Session("TransactionFrequencyApprovalDetail.PK") = StrTmppkTransactionFrequencyApprovalId
                Else
                    Throw New SahassaException("Parameter PKTransactionFrequencyApprovalID is not valid.")
                End If

                Return CInt(Session("TransactionFrequencyApprovalDetail.PK"))
            Else
                Return CInt(Session("TransactionFrequencyApprovalDetail.PK"))
            End If
        End Get
    End Property

    Public ReadOnly Property ObjTransactionFrequencyApproval() As TList(Of TransactionFrequency_Approval)
        Get
            Dim PkApprovalid As Integer
            Try
                If Session("TransactionFrequencyApprovalDetail.ObjTransactionFrequencyApproval") Is Nothing Then
                    PkApprovalid = PKTransactionFrequencyApprovalID
                    Session("TransactionFrequencyApprovalDetail.ObjTransactionFrequencyApproval") = DataRepository.TransactionFrequency_ApprovalProvider.GetPaged("PK_TransactionFrequency_ApprovalID = " & PkApprovalid, "", 0, Integer.MaxValue, 0)
                End If
                Return CType(Session("TransactionFrequencyApprovalDetail.ObjTransactionFrequencyApproval"), TList(Of TransactionFrequency_Approval))
            Finally
            End Try
        End Get
    End Property

    Private Sub LoadDataApproval()
        If ObjTransactionFrequencyApproval.Count > 0 Then
            If ObjTransactionFrequencyApproval(0).FK_ModeID.GetValueOrDefault(0) = 2 Then
                Me.LblAction.Text = "Edit"
            End If

            Dim strRequestBy As String = ""
            Using objRequestBy As User = SahassaNettier.Data.DataRepository.UserProvider.GetByPkUserID(ObjTransactionFrequencyApproval(0).FK_MsUserID.GetValueOrDefault(0))
                If Not objRequestBy Is Nothing Then
                    strRequestBy = objRequestBy.UserID & " ( " & objRequestBy.UserName & " ) "
                End If
            End Using

            LblRequestBy.Text = strRequestBy
            LblRequestDate.Text = ObjTransactionFrequencyApproval(0).CreatedDate.GetValueOrDefault().ToString("dd-MM-yyyy")

            If ObjTransactionFrequencyApproval(0).FK_ModeID.GetValueOrDefault(0) = 2 Then
                'Old
                Me.GrdVwTransactionFrequencyOld.DataSource = DataRepository.TransactionFrequencyProvider.GetPaged("", "Tier", 0, Integer.MaxValue, 0)
                Me.GrdVwTransactionFrequencyOld.DataBind()

                'New
                Me.GrdVwTransactionFrequencyNew.DataSource = DataRepository.TransactionFrequency_ApprovalDetailProvider.GetPaged("", "Tier", 0, Integer.MaxValue, 0)
                Me.GrdVwTransactionFrequencyNew.DataBind()
            End If
        Else
            'kalau ternyata sudah di approve /reject dan memakai tombol back, maka redirect ke viewapproval
            Response.Redirect("TransactionFrequencyApproval.aspx", False)
        End If
    End Sub

    Private Sub ClearSession()
        Session("TransactionFrequencyApprovalDetail.PK") = Nothing
        Session("TransactionFrequencyApprovalDetail.ObjTransactionFrequencyApproval") = Nothing
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                ClearSession()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using

                LoadDataApproval()
            End If
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImgBtnAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAccept.Click
        Try
            If ObjTransactionFrequencyApproval.Count > 0 Then
                Using ObjTransactionFrequencyBll As New TransactionFrequencyBLL
                    If ObjTransactionFrequencyBll.AcceptEdit Then
                        Response.Redirect("TransactionFrequencyApproval.aspx", False)
                    End If
                End Using
            Else
                Response.Redirect("TransactionFrequencyApproval.aspx", False)
            End If

        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBtnReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnReject.Click
        Try
            If ObjTransactionFrequencyApproval.Count > 0 Then
                Using ObjTransactionFrequencyBll As New TransactionFrequencyBLL
                    If ObjTransactionFrequencyBll.RejectEdit Then
                        Response.Redirect("TransactionFrequencyApproval.aspx", False)
                    End If
                End Using
            Else
                Response.Redirect("TransactionFrequencyApproval.aspx", False)
            End If
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBackAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBackAdd.Click
        Try
            Response.Redirect("TransactionFrequencyApproval.aspx", False)
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    'Protected Sub CvalHandleErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles CvalHandleErr.PreRender
    '    If CvalHandleErr.IsValid = False Then
    '        Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
    '    End If
    'End Sub

    'Protected Sub CvalPageErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles CvalPageErr.PreRender
    '    If CvalPageErr.IsValid = False Then
    '        Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
    '    End If
    'End Sub
End Class