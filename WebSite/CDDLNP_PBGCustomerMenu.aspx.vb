﻿Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports AMLBLL
Imports AMLBLL.CDDLNPBLL
Imports System.Data

Partial Class CDDLNP_PBGCustomerMenu
    Inherits Parent

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not Page.IsPostBack Then
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using

                ImageAddNew.AlternateText = "Create EDD New Customer"
                ImageAddNew.CommandName = "CDDLNPAdd.aspx"
                ImageAddNew.CommandArgument = "CDDLNPAdd.aspx?TID=1&NTID=1"

                ImageAddExisting.AlternateText = "Create EDD Existing Customer"
                ImageAddExisting.CommandName = "CDDLNPAdd.aspx"
                ImageAddExisting.CommandArgument = "CDDLNPAdd.aspx?TID=2&NTID=1"

                ImageViewNew.AlternateText = "Inquiry EDD New Customer"
                ImageViewNew.CommandName = "CDDLNP_PBGNewCustomerView.aspx"
                ImageViewNew.CommandArgument = "CDDLNP_PBGNewCustomerView.aspx"

                ImageViewExisting.AlternateText = "Inquiry EDD Existing Customer"
                ImageViewExisting.CommandName = "CDDLNP_PBGExistingCustomerView.aspx"
                ImageViewExisting.CommandArgument = "CDDLNP_PBGExistingCustomerView.aspx"
            End If
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub

    Protected Sub ImageButton_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAddNew.Click, ImageAddExisting.Click, ImageViewNew.Click, ImageViewExisting.Click
        Try
            Dim ImgButton As ImageButton = CType(sender, ImageButton)
            Dim intGroupId As Int32 = DataRepository.UserProvider.GetBypkUserID(Sahassa.AML.Commonly.SessionPkUserId).UserGroupId

            If DataRepository.FileSecurityProvider.GetTotalItems("FileSecurityGroupId = " & intGroupId & " AND FileSecurityFileName = '" & ImgButton.CommandName & "'", 0) = 0 Then
                'Throw New Exception("Anda tidak memiliki akses untuk page " & ImageButton.CommandName)
                Throw New Exception("Your user id does not have access to the menu '" & ImgButton.AlternateText & "'")
            Else
                Dim isExist As Boolean = False
                For Each objWorkflow As CDDLNPNettier.Entities.CDDLNP_WorkflowUpload In CDDLNPNettier.Data.DataRepository.CDDLNP_WorkflowUploadProvider.GetPaged("List_RM LIKE '%" & Sahassa.AML.Commonly.SessionUserId & "%'", String.Empty, 0, Int32.MaxValue, 0)
                    For Each objUserId As String In objWorkflow.List_RM.Split(";")
                        If String.Compare(Sahassa.AML.Commonly.SessionUserId, objUserId, StringComparison.OrdinalIgnoreCase) = 0 Then
                            isExist = True
                        End If
                    Next
                Next

                If Not isExist AndAlso ImgButton.CommandName = "CDDLNPAdd.aspx" Then
                    Throw New Exception("Your user ID has not been assigned to create EDD.")
                Else
                    SessionReferenceID = 0
                    SessionCDDLNPAuditTrailId = 0
                    Me.Response.Redirect(ImgButton.CommandArgument, False)
                End If
            End If

        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub
End Class