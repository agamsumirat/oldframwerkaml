#Region "Imports..."
Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports SahassaNettier.Services
Imports AMLBLL.ValidateBLL
Imports AMLBLL.DataType

#End Region

Partial Class CustomerCTREdit
    Inherits Parent

#Region "properties..."

    ReadOnly Property GetPK_CustomerCTR_ID() As Long
        Get
            If Not Request.Params("PK_CustomerCTR_ID") Is Nothing Then
                Return CLng(Request.Params("PK_CustomerCTR_ID"))
            Else
                Return -1
            End If
        End Get
    End Property

    ReadOnly Property GetTanggalTransaksi() As String
        Get
            If Not Request.Params("TanggalTransaksi") Is Nothing Then
                Return CStr(Request.Params("TanggalTransaksi")).Replace("'", "''")
            Else
                Return ""
            End If

        End Get
    End Property

    ReadOnly Property GetTipeTransaksi() As String
        Get
            If Not Request.Params("TipeTransaksi") Is Nothing Then
                Return CStr(Request.Params("TipeTransaksi")).Replace("'", "''")
            Else
                Return ""
            End If

        End Get
    End Property

    ReadOnly Property GetNomorTiketTransaksi() As String
        Get
            If Not Request.Params("NomorTiketTransaksi") Is Nothing Then
                Return CStr(Request.Params("NomorTiketTransaksi")).Replace("'", "''")
            Else
                Return ""
            End If
        End Get
    End Property

    Private Property SetnGetRowEdit() As Integer
        Get
            If Session("CustomerCTREdit.RowEdit") Is Nothing Then
                Session("CustomerCTREdit.RowEdit") = -1
            End If
            Return Session("CustomerCTREdit.RowEdit")
        End Get
        Set(ByVal value As Integer)
            Session("CustomerCTREdit.RowEdit") = value
        End Set
    End Property

    Private Property SetnGetActionType() As String
        Get
            If Session("CustomerCTREdit.ActionType") Is Nothing Then
                Session("CustomerCTREdit.ActionType") = ""
            End If
            Return Session("CustomerCTREdit.ActionType")
        End Get
        Set(ByVal value As String)
            Session("CustomerCTREdit.ActionType") = value
        End Set
    End Property

    Private Property SetnGetTransaction() As TList(Of CustomerCTRTransaction)
        Get
            If Session("CustomerCTREdit.Transaction") Is Nothing Then
                Session("CustomerCTREdit.Transaction") = New TList(Of CustomerCTRTransaction)
            End If
            Return Session("CustomerCTREdit.Transaction")
        End Get
        Set(ByVal value As TList(Of CustomerCTRTransaction))
            Session("CustomerCTREdit.Transaction") = value
        End Set
    End Property

    'Private Property SetnGetTransactionOri() As TList(Of CustomerCTRTransaction)
    '    Get
    '        If Session("CustomerCTREdit.TransactionOri") Is Nothing Then
    '            Session("CustomerCTREdit.TransactionOri") = New TList(Of CustomerCTRTransaction)
    '        End If
    '        Return Session("CustomerCTREdit.TransactionOri")
    '    End Get
    '    Set(ByVal value As TList(Of CustomerCTRTransaction))
    '        Session("CustomerCTREdit.TransactionOri") = value
    '    End Set
    'End Property

    Private Property SetnGetTransactionDeleted() As TList(Of CustomerCTRTransaction)
        Get
            If Session("CustomerCTREdit.TransactionDeleted") Is Nothing Then
                Session("CustomerCTREdit.TransactionDeleted") = New TList(Of CustomerCTRTransaction)
            End If
            Return Session("CustomerCTREdit.TransactionDeleted")
        End Get
        Set(ByVal value As TList(Of CustomerCTRTransaction))
            Session("CustomerCTREdit.TransactionDeleted") = value
        End Set
    End Property

    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("CustomerCTREdit.Sort") Is Nothing, "PK_CustomerCTRTransaction_ID  asc", Session("CustomerCTREdit.Sort"))
        End Get
        Set(ByVal Value As String)
            Session("CustomerCTREdit.Sort") = Value
        End Set
    End Property

#End Region

#Region "events..."

    Protected Sub chkCopyAlamatDOM_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkCopyAlamatDOM.CheckedChanged
        Try

            If chkCopyAlamatDOM.Checked = True Then
                txtIDNamaJalan.Text = txtDOMNamaJalan.Text
                txtIDRT.Text = txtDOMRT.Text
                txtIDRW.Text = txtDOMRW.Text
                txtIDKelurahan.Text = txtDOMKelurahan.Text
                txtIDKecamatan.Text = txtDOMKecamatan.Text
                txtIDKotaKab.Text = txtDOMKotaKab.Text
                txtIDKodePos.Text = txtDOMKodePos.Text
                txtIDProvinsi.Text = txtDOMProvinsi.Text
                txtIDNegara.Text = txtDOMNegara.Text

                hfIDKelurahan.Value = hfDOMKelurahan.Value
                hfIDKecamatan.Value = hfDOMKecamatan.Value
                hfIDKotaKab.Value = hfDOMKotaKab.Value
                hfIDProvinsi.Value = hfDOMProvinsi.Value
                hfIDNegara.Value = hfDomNegara.Value

                txtIDNamaJalan.Enabled = False
                txtIDRT.Enabled = False
                txtIDRW.Enabled = False
                txtIDKodePos.Enabled = False
            Else
                txtIDNamaJalan.Text = ""
                txtIDRT.Text = ""
                txtIDRW.Text = ""
                txtIDKelurahan.Text = ""
                txtIDKecamatan.Text = ""
                txtIDKotaKab.Text = ""
                txtIDKodePos.Text = ""
                txtIDProvinsi.Text = ""
                txtIDNegara.Text = ""

                hfIDKelurahan.Value = ""
                hfIDKecamatan.Value = ""
                hfIDKotaKab.Value = ""
                hfIDProvinsi.Value = ""
                hfIDNegara.Value = ""

                txtIDNamaJalan.Enabled = True
                txtIDRT.Enabled = True
                txtIDRW.Enabled = True
                txtIDKodePos.Enabled = True
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub imgOKMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgOKMsg.Click
        Try
            If Me.GetTanggalTransaksi = "" Then
                Response.Redirect("CustomerCTRView.aspx", False)
            Else
                Response.Redirect("CustomerCTRView.aspx?TanggalTransaksi=" & Me.GetTanggalTransaksi & " &TipeTransaksi=" & Me.GetTipeTransaksi & " & NomorTiketTransaksi=" & Me.GetNomorTiketTransaksi, False)
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub imgDOMKelurahan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgDOMKelurahan.Click
        Try
            If Session("PickerKelurahan.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerKelurahan.Data")).Split(";")
                txtDOMKelurahan.Text = strData(1)
                hfDOMKelurahan.Value = strData(0)
                FillNextLevelAlamatIndividuDomisili(AddressLevel.Kelurahan)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgDOMKecamatan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgDOMKecamatan.Click
        Try
            If Session("PickerKecamatan.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerKecamatan.Data")).Split(";")
                txtDOMKecamatan.Text = strData(1)
                hfDOMKecamatan.Value = strData(0)
                FillNextLevelAlamatIndividuDomisili(AddressLevel.Kecamatan)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgIDProvinsi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgIDProvinsi.Click
        Try
            If Session("PickerProvinsi.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                txtIDProvinsi.Text = strData(1)
                hfIDProvinsi.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgIDKelurahan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgIDKelurahan.Click
        Try
            If Session("PickerKelurahan.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerKelurahan.Data")).Split(";")
                txtIDKelurahan.Text = strData(1)
                hfIDKelurahan.Value = strData(0)
                FillNextLevelAlamatIndividuIdentitas(AddressLevel.Kelurahan)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgCORPDLKelurahan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCORPDLKelurahan.Click
        Try
            If Session("PickerKelurahan.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerKelurahan.Data")).Split(";")
                txtCORPDLKelurahan.Text = strData(1)
                hfCORPDLKelurahan.Value = strData(0)
                FillNextLevelAlamatKorporasiDalamNegeri(AddressLevel.Kelurahan)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgIDKecamatan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgIDKecamatan.Click
        Try
            If Session("PickerKecamatan.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerKecamatan.Data")).Split(";")
                txtIDKecamatan.Text = strData(1)
                hfIDKecamatan.Value = strData(0)
                FillNextLevelAlamatIndividuIdentitas(AddressLevel.Kecamatan)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgCORPDLKecamatan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCORPDLKecamatan.Click
        Try
            If Session("PickerKecamatan.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerKecamatan.Data")).Split(";")
                txtCORPDLKecamatan.Text = strData(1)
                hfCORPDLKecamatan.Value = strData(0)
                FillNextLevelAlamatKorporasiDalamNegeri(AddressLevel.Kecamatan)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgDOMKotaKab_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgDOMKotaKab.Click
        Try
            If Session("PickerKotaKab.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
                txtDOMKotaKab.Text = strData(1)
                hfDOMKotaKab.Value = strData(0)
                FillNextLevelAlamatIndividuDomisili(AddressLevel.KabupatenKota)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgIDKotaKab_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgIDKotaKab.Click
        Try
            If Session("PickerKotaKab.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
                txtIDKotaKab.Text = strData(1)
                hfIDKotaKab.Value = strData(0)
                FillNextLevelAlamatIndividuIdentitas(AddressLevel.KabupatenKota)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgIDKota_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgIDKota.Click
        Try
            If Session("PickerKotaKab.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
                txtNAKota.Text = strData(1)
                hfIDKota.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgCORPDLKotaKab_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCORPDLKotaKab.Click
        Try
            If Session("PickerKotaKab.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
                txtCORPDLKotaKab.Text = strData(1)
                hfCORPDLKotaKab.Value = strData(0)
                FillNextLevelAlamatKorporasiDalamNegeri(AddressLevel.KabupatenKota)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgCORPLNKota_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCORPLNKota.Click
        Try
            If Session("PickerKotaKab.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
                txtCORPLNKota.Text = strData(1)
                hfCORPLNKota.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgDOMProvinsi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgDOMProvinsi.Click
        Try
            If Session("PickerProvinsi.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                txtDOMProvinsi.Text = strData(1)
                hfDOMProvinsi.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgNAProvinsi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgNAProvinsi.Click
        Try
            If Session("PickerProvinsi.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                txtNAProvinsi.Text = strData(1)
                hfNAProvinsi.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgCORPDLProvinsi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCORPDLProvinsi.Click
        Try
            If Session("PickerProvinsi.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                txtCORPDLProvinsi.Text = strData(1)
                hfCORPDLProvinsi.Value = strData(0)
                FillNextLevelAlamatKorporasiDalamNegeri(AddressLevel.Propinsi)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgCORPLNProvinsi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCORPLNProvinsi.Click
        Try
            If Session("PickerProvinsi.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                txtCORPLNProvinsi.Text = strData(1)
                hfCORPLNProvinsi.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgIDNegara_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgIDNANegara.Click
        Try
            If Session("PickerNegara.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
                txtNANegara.Text = strData(1)
                hfNANegara.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgCORPDLNegara_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCORPDLNegara.Click
        Try
            If Session("PickerNegara.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
                txtCORPDLNegara.Text = strData(1)
                hfCORPDLNegara.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgCORPLNNegara_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCORPLNNegara.Click
        Try
            If Session("PickerNegara.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
                txtCORPLNNegara.Text = strData(1)
                hfCORPLNNegara.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgPekerjaan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgPekerjaan.Click
        Try
            If Session("PickerPekerjaan.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerPekerjaan.Data")).Split(";")
                txtPekerjaan.Text = strData(1)
                hfPekerjaan.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgCORPBidangUsaha_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCORPBidangUsaha.Click
        Try
            If Session("PickerBidangUsaha.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerBidangUsaha.Data")).Split(";")
                txtCORPBidangUsaha.Text = strData(1)
                hfCORPBidangUsaha.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Try
            If Me.GetTanggalTransaksi = "" Then
                Response.Redirect("CustomerCTRView.aspx", False)
            Else

                Response.Redirect("CustomerCTRView.aspx?TanggalTransaksi=" & Me.GetTanggalTransaksi & " &TipeTransaksi=" & Me.GetTipeTransaksi & " & NomorTiketTransaksi=" & Me.GetNomorTiketTransaksi, False)
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub rblKewarganegaraan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblKewarganegaraan.SelectedIndexChanged
        Try
            If rblKewarganegaraan.SelectedIndex = 0 Then
                For Each item As ListItem In cboNegara.Items
                    If item.Text.ToLower.Contains("indonesia") Then
                        cboNegara.SelectedValue = item.Value
                    End If
                Next
                tblnegaraasal.Visible = False
            Else
                tblnegaraasal.Visible = True

            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                clearSession()
                SetControlLoad()
                Me.txtPenghasilanRataRata.Attributes.Add("onblur", "javascript:CommaFormatted(this,this.value)")
                If Me.GetTanggalTransaksi <> "" Or Me.GetTipeTransaksi <> "" Or Me.GetNomorTiketTransaksi <> "" Then
                    Me.NewTrx()
                    'TxtTanggalTransaksi.Text = Me.GetTanggalTransaksi
                    TxtTanggalTransaksi.Text = CDate(Me.GetTanggalTransaksi).ToString("dd-MM-yyyy")
                    Select Case Me.GetTipeTransaksi
                        Case "C"
                            RblTipeTransaksi.SelectedValue = "C"
                        Case "D"
                            RblTipeTransaksi.SelectedValue = "D"
                        Case Else
                            RblTipeTransaksi.SelectedIndex = -1
                    End Select
                    TxtNomorTiketTransaksi.Text = Me.GetNomorTiketTransaksi
                Else
                    Me.ShowEditTransaksi(False)
                End If
                Me.popTglLahir.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtTglLahir.ClientID & "'), 'dd-mm-yyyy')")
                Me.popTglLahir.Style.Add("display", "")
                Me.popUpTanggalTransaksi.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TxtTanggalTransaksi.ClientID & "'), 'dd-mm-yyyy')")
                Me.popUpTanggalTransaksi.Style.Add("display", "")


                Me.LoadCustomerCTRToField()
                Me.LoadCustomerCTRTransaction()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using

                'If MultiView1.ActiveViewIndex = 0 Then
                '    ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('Id','Img1','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
                '    ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('Transaksi','Img2','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
                'End If
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub rblTipePelapor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblTipePelapor.SelectedIndexChanged
        Try
            If rblTipePelapor.SelectedValue = 1 Then
                divPerorangan.Visible = True
                divKorporasi.Visible = False
            Else
                divPerorangan.Visible = False
                divKorporasi.Visible = True
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try

        Catch ex As Exception

        End Try
    End Sub

#End Region

#Region "Function..."

    Private Enum AddressLevel
        Kelurahan = 1
        Kecamatan
        KabupatenKota
        Propinsi
        Negara
    End Enum

    Private Sub FillNextLevelAlamatIndividuDomisili(enumAddress As AddressLevel)
        Select Case enumAddress
            Case AddressLevel.Kelurahan
                Using ObjMsKelurahan As MsKelurahan = DataRepository.MsKelurahanProvider.GetByIDKelurahan(hfDOMKelurahan.Value)
                    If Not ObjMsKelurahan Is Nothing Then
                        hfDOMKecamatan.Value = ObjMsKelurahan.IDKecamatan.GetValueOrDefault()
                        Using ObjMsKecamatan As MsKecamatan = DataRepository.MsKecamatanProvider.GetByIDKecamatan(hfDOMKecamatan.Value)
                            If Not ObjMsKecamatan Is Nothing Then
                                txtDOMKecamatan.Text = ObjMsKecamatan.NamaKecamatan
                                hfDOMKotaKab.Value = ObjMsKecamatan.IDKotaKabupaten.GetValueOrDefault()
                                Using ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(hfDOMKotaKab.Value)
                                    If Not ObjMsKotaKab Is Nothing Then
                                        txtDOMKotaKab.Text = ObjMsKotaKab.NamaKotaKab
                                        hfDOMProvinsi.Value = ObjMsKotaKab.IDPropinsi.GetValueOrDefault()
                                        Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfDOMProvinsi.Value)
                                            If Not ObjMsProvince Is Nothing Then
                                                txtDOMProvinsi.Text = ObjMsProvince.Nama
                                                hfDomNegara.Value = ObjMsProvince.Code
                                                Using ObjMsNegara As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(hfDomNegara.Value)
                                                    If Not ObjMsNegara Is Nothing Then
                                                        txtDOMNegara.Text = ObjMsNegara.NamaNegara
                                                    End If
                                                End Using
                                            End If
                                        End Using
                                    End If
                                End Using
                            End If
                        End Using

                    End If
                End Using
            Case AddressLevel.Kecamatan
                Using ObjMsKecamatan As MsKecamatan = DataRepository.MsKecamatanProvider.GetByIDKecamatan(hfDOMKecamatan.Value)
                    If Not ObjMsKecamatan Is Nothing Then
                        txtDOMKecamatan.Text = ObjMsKecamatan.NamaKecamatan
                        hfDOMKotaKab.Value = ObjMsKecamatan.IDKotaKabupaten.GetValueOrDefault()
                        Using ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(hfDOMKotaKab.Value)
                            If Not ObjMsKotaKab Is Nothing Then
                                txtDOMKotaKab.Text = ObjMsKotaKab.NamaKotaKab
                                hfDOMProvinsi.Value = ObjMsKotaKab.IDPropinsi.GetValueOrDefault()
                                Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfDOMProvinsi.Value)
                                    If Not ObjMsProvince Is Nothing Then
                                        txtDOMProvinsi.Text = ObjMsProvince.Nama
                                        hfDomNegara.Value = ObjMsProvince.Code
                                        Using ObjMsNegara As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(hfDomNegara.Value)
                                            If Not ObjMsNegara Is Nothing Then
                                                txtDOMNegara.Text = ObjMsNegara.NamaNegara
                                            End If
                                        End Using
                                    End If
                                End Using
                            End If
                        End Using
                    End If
                End Using
            Case AddressLevel.KabupatenKota
                Using ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(hfDOMKotaKab.Value)
                    If Not ObjMsKotaKab Is Nothing Then
                        txtDOMKotaKab.Text = ObjMsKotaKab.NamaKotaKab
                        hfDOMProvinsi.Value = ObjMsKotaKab.IDPropinsi.GetValueOrDefault()
                        Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfDOMProvinsi.Value)
                            If Not ObjMsProvince Is Nothing Then
                                txtDOMProvinsi.Text = ObjMsProvince.Nama
                                hfDomNegara.Value = ObjMsProvince.Code
                                Using ObjMsNegara As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(hfDomNegara.Value)
                                    If Not ObjMsNegara Is Nothing Then
                                        txtDOMNegara.Text = ObjMsNegara.NamaNegara
                                    End If
                                End Using
                            End If
                        End Using
                    End If
                End Using
            Case AddressLevel.Propinsi
                Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfDOMProvinsi.Value)
                    If Not ObjMsProvince Is Nothing Then
                        txtDOMProvinsi.Text = ObjMsProvince.Nama
                        hfDomNegara.Value = ObjMsProvince.Code
                        Using ObjMsNegara As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(hfDomNegara.Value)
                            If Not ObjMsNegara Is Nothing Then
                                txtDOMNegara.Text = ObjMsNegara.NamaNegara
                            End If
                        End Using
                    End If
                End Using
        End Select
    End Sub

    Private Sub FillNextLevelAlamatIndividuIdentitas(enumAddress As AddressLevel)
        Select Case enumAddress
            Case AddressLevel.Kelurahan
                Using ObjMsKelurahan As MsKelurahan = DataRepository.MsKelurahanProvider.GetByIDKelurahan(hfIDKelurahan.Value)
                    If Not ObjMsKelurahan Is Nothing Then
                        hfIDKecamatan.Value = ObjMsKelurahan.IDKecamatan.GetValueOrDefault()
                        Using ObjMsKecamatan As MsKecamatan = DataRepository.MsKecamatanProvider.GetByIDKecamatan(hfIDKecamatan.Value)
                            If Not ObjMsKecamatan Is Nothing Then
                                txtIDKecamatan.Text = ObjMsKecamatan.NamaKecamatan
                                hfIDKotaKab.Value = ObjMsKecamatan.IDKotaKabupaten.GetValueOrDefault()
                                Using ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(hfIDKotaKab.Value)
                                    If Not ObjMsKotaKab Is Nothing Then
                                        txtIDKotaKab.Text = ObjMsKotaKab.NamaKotaKab
                                        hfIDProvinsi.Value = ObjMsKotaKab.IDPropinsi.GetValueOrDefault()
                                        Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfIDProvinsi.Value)
                                            If Not ObjMsProvince Is Nothing Then
                                                txtIDProvinsi.Text = ObjMsProvince.Nama
                                                hfIDNegara.Value = ObjMsProvince.Code
                                                Using ObjMsNegara As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(hfIDNegara.Value)
                                                    If Not ObjMsNegara Is Nothing Then
                                                        txtIDNegara.Text = ObjMsNegara.NamaNegara
                                                    End If
                                                End Using
                                            End If
                                        End Using
                                    End If
                                End Using
                            End If
                        End Using

                    End If
                End Using
            Case AddressLevel.Kecamatan
                Using ObjMsKecamatan As MsKecamatan = DataRepository.MsKecamatanProvider.GetByIDKecamatan(hfIDKecamatan.Value)
                    If Not ObjMsKecamatan Is Nothing Then
                        txtIDKecamatan.Text = ObjMsKecamatan.NamaKecamatan
                        hfIDKotaKab.Value = ObjMsKecamatan.IDKotaKabupaten.GetValueOrDefault()
                        Using ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(hfIDKotaKab.Value)
                            If Not ObjMsKotaKab Is Nothing Then
                                txtIDKotaKab.Text = ObjMsKotaKab.NamaKotaKab
                                hfIDProvinsi.Value = ObjMsKotaKab.IDPropinsi.GetValueOrDefault()
                                Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfIDProvinsi.Value)
                                    If Not ObjMsProvince Is Nothing Then
                                        txtIDProvinsi.Text = ObjMsProvince.Nama
                                        hfIDNegara.Value = ObjMsProvince.Code
                                        Using ObjMsNegara As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(hfIDNegara.Value)
                                            If Not ObjMsNegara Is Nothing Then
                                                txtIDNegara.Text = ObjMsNegara.NamaNegara
                                            End If
                                        End Using
                                    End If
                                End Using
                            End If
                        End Using
                    End If
                End Using
            Case AddressLevel.KabupatenKota
                Using ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(hfIDKotaKab.Value)
                    If Not ObjMsKotaKab Is Nothing Then
                        txtIDKotaKab.Text = ObjMsKotaKab.NamaKotaKab
                        hfIDProvinsi.Value = ObjMsKotaKab.IDPropinsi.GetValueOrDefault()
                        Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfIDProvinsi.Value)
                            If Not ObjMsProvince Is Nothing Then
                                txtIDProvinsi.Text = ObjMsProvince.Nama
                                hfIDNegara.Value = ObjMsProvince.Code
                                Using ObjMsNegara As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(hfIDNegara.Value)
                                    If Not ObjMsNegara Is Nothing Then
                                        txtIDNegara.Text = ObjMsNegara.NamaNegara
                                    End If
                                End Using
                            End If
                        End Using
                    End If
                End Using
            Case AddressLevel.Propinsi
                Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfIDProvinsi.Value)
                    If Not ObjMsProvince Is Nothing Then
                        txtIDProvinsi.Text = ObjMsProvince.Nama
                        hfIDNegara.Value = ObjMsProvince.Code
                        Using ObjMsNegara As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(hfIDNegara.Value)
                            If Not ObjMsNegara Is Nothing Then
                                txtIDNegara.Text = ObjMsNegara.NamaNegara
                            End If
                        End Using
                    End If
                End Using
        End Select
    End Sub

    Private Sub FillNextLevelAlamatKorporasiDalamNegeri(enumAddress As AddressLevel)
        Select Case enumAddress
            Case AddressLevel.Kelurahan
                Using ObjMsKelurahan As MsKelurahan = DataRepository.MsKelurahanProvider.GetByIDKelurahan(hfCORPDLKelurahan.Value)
                    If Not ObjMsKelurahan Is Nothing Then
                        hfCORPDLKecamatan.Value = ObjMsKelurahan.IDKecamatan.GetValueOrDefault()
                        Using ObjMsKecamatan As MsKecamatan = DataRepository.MsKecamatanProvider.GetByIDKecamatan(hfCORPDLKecamatan.Value)
                            If Not ObjMsKecamatan Is Nothing Then
                                txtCORPDLKecamatan.Text = ObjMsKecamatan.NamaKecamatan
                                hfCORPDLKotaKab.Value = ObjMsKecamatan.IDKotaKabupaten.GetValueOrDefault()
                                Using ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(hfCORPDLKotaKab.Value)
                                    If Not ObjMsKotaKab Is Nothing Then
                                        txtCORPDLKotaKab.Text = ObjMsKotaKab.NamaKotaKab
                                        hfCORPDLProvinsi.Value = ObjMsKotaKab.IDPropinsi.GetValueOrDefault()
                                        Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfCORPDLProvinsi.Value)
                                            If Not ObjMsProvince Is Nothing Then
                                                txtCORPDLProvinsi.Text = ObjMsProvince.Nama
                                                If ObjMsProvince.Code <> "" Then
                                                    hfCORPDLNegara.Value = ObjMsProvince.Code
                                                    Using ObjMsNegara As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(hfCORPDLNegara.Value)
                                                        If Not ObjMsNegara Is Nothing Then
                                                            txtCORPDLNegara.Text = ObjMsNegara.NamaNegara
                                                        End If
                                                    End Using
                                                End If
                                            End If
                                        End Using
                                    End If
                                End Using
                            End If
                        End Using

                    End If
                End Using
            Case AddressLevel.Kecamatan
                Using ObjMsKecamatan As MsKecamatan = DataRepository.MsKecamatanProvider.GetByIDKecamatan(hfCORPDLKecamatan.Value)
                    If Not ObjMsKecamatan Is Nothing Then
                        txtCORPDLKecamatan.Text = ObjMsKecamatan.NamaKecamatan
                        hfCORPDLKotaKab.Value = ObjMsKecamatan.IDKotaKabupaten.GetValueOrDefault()
                        Using ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(hfCORPDLKotaKab.Value)
                            If Not ObjMsKotaKab Is Nothing Then
                                txtCORPDLKotaKab.Text = ObjMsKotaKab.NamaKotaKab
                                hfCORPDLProvinsi.Value = ObjMsKotaKab.IDPropinsi.GetValueOrDefault()
                                Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfCORPDLProvinsi.Value)
                                    If Not ObjMsProvince Is Nothing Then
                                        txtCORPDLProvinsi.Text = ObjMsProvince.Nama
                                        If ObjMsProvince.Code <> "" Then
                                            hfCORPDLNegara.Value = ObjMsProvince.Code
                                            Using ObjMsNegara As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(hfCORPDLNegara.Value)
                                                If Not ObjMsNegara Is Nothing Then
                                                    txtCORPDLNegara.Text = ObjMsNegara.NamaNegara
                                                End If
                                            End Using
                                        End If
                                    End If
                                End Using
                            End If
                        End Using
                    End If
                End Using
            Case AddressLevel.KabupatenKota
                Using ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(hfCORPDLKotaKab.Value)
                    If Not ObjMsKotaKab Is Nothing Then
                        txtCORPDLKotaKab.Text = ObjMsKotaKab.NamaKotaKab
                        hfCORPDLProvinsi.Value = ObjMsKotaKab.IDPropinsi.GetValueOrDefault()
                        Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfCORPDLProvinsi.Value)
                            If Not ObjMsProvince Is Nothing Then
                                txtCORPDLProvinsi.Text = ObjMsProvince.Nama
                                If ObjMsProvince.Code <> "" Then
                                    hfCORPDLNegara.Value = ObjMsProvince.Code
                                    Using ObjMsNegara As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(hfCORPDLNegara.Value)
                                        If Not ObjMsNegara Is Nothing Then
                                            txtCORPDLNegara.Text = ObjMsNegara.NamaNegara
                                        End If
                                    End Using
                                End If
                            End If
                        End Using
                    End If
                End Using
            Case AddressLevel.Propinsi
                Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfCORPDLProvinsi.Value)
                    If Not ObjMsProvince Is Nothing Then
                        txtCORPDLProvinsi.Text = ObjMsProvince.Nama
                        If ObjMsProvince.Code <> "" Then
                            hfCORPDLNegara.Value = ObjMsProvince.Code
                            Using ObjMsNegara As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(hfCORPDLNegara.Value)
                                If Not ObjMsNegara Is Nothing Then
                                    txtCORPDLNegara.Text = ObjMsNegara.NamaNegara
                                End If
                            End Using
                        End If
                    End If
                End Using
        End Select
    End Sub

    Sub hideSave()
        ImageSave.Visible = False
        ImageCancel.Visible = False
    End Sub

    Function isvalidData() As Boolean
        Try


            'Indv
            If rblTipePelapor.SelectedValue = 1 Then
                If txtNamaLengkap.Text = vbNullString OrElse txtNamaLengkap.Text = "" Then
                    Throw New Exception("Nama Lengkap harus di isi")

                End If

                If txtTempatLahir.Text = vbNullString OrElse txtTempatLahir.Text = "" Then
                    Throw New Exception("Tempat Lahir harus di isi")

                End If
                If txtTglLahir.Text = vbNullString OrElse txtTglLahir.Text = "" Then
                    Throw New Exception("Tanggal Lahir harus di isi")

                End If
                If Sahassa.AML.Commonly.IsDateValid("dd-MM-yyyy", txtTglLahir.Text) = False Then
                    Throw New Exception("Format Tanggal Lahir tidak valid")

                End If
                If rblKewarganegaraan.SelectedIndex = -1 Then
                    Throw New Exception("Kewarganegaraan   harus di pilih")

                End If
                If rblKewarganegaraan.SelectedIndex = 1 Then
                    If cboNegara.SelectedIndex = 0 Then
                        Throw New Exception("Negara harus di pilih")

                    End If
                    If txtNAKodePos.Text = vbNullString OrElse txtNAKodePos.Text = "" Then
                        Throw New Exception("Kode Pos harus di isi (Alamat Sesuai Negara Asal)")

                    End If

                    If txtNANegara.Text = vbNullString OrElse txtNANegara.Text = "" Then
                        Throw New Exception("Negara harus di isi (Alamat Sesuai Negara Asal)")

                    End If
                End If
                If txtDOMKotaKab.Text = vbNullString OrElse txtDOMKotaKab.Text = "" Then
                    Throw New Exception("Kota / Kabupaten harus di isi (Alamat Domisili)")

                End If
                If txtDOMProvinsi.Text = vbNullString OrElse txtDOMProvinsi.Text = "" Then
                    Throw New Exception("Provinsi harus di isi (Alamat Domisili)")
                End If
                If txtDOMNegara.Text = vbNullString OrElse txtDOMNegara.Text = "" Then
                    Throw New Exception("Negara harus di isi (Alamat Domisili)")
                End If

                If txtIDKotaKab.Text = vbNullString OrElse txtIDKotaKab.Text = "" Then
                    Throw New Exception("Kota Kabupaten harus di isi (Alamat Sesuai Bukti Identitas)")

                End If
                If txtIDProvinsi.Text = vbNullString OrElse txtIDProvinsi.Text = "" Then
                    Throw New Exception("Kota Provinsi harus di isi (Alamat Sesuai Bukti Identitas)")
                End If
                If txtIDNegara.Text = vbNullString OrElse txtIDNegara.Text = "" Then
                    Throw New Exception("Negara harus di isi (Alamat Sesuai Bukti Identitas)")
                End If

                If cboJenisDocID.SelectedIndex = 0 Then
                    Throw New Exception("Jenis Dokumen Identitas harus di pilih")
                End If

                If txtNomorID.Text = vbNullString OrElse txtNomorID.Text = "" Then
                    Throw New Exception("Nomor Identitas harus di isi")
                End If

                If txtNomorID.Text <> "" Then
                    If Not Regex.Match(txtNomorID.Text.Trim(), "^[a-zA-Z0-9]*$").Success Then
                        Throw New Exception("Nomor Identitas tidak boleh mengandung special karakter")
                    End If
                End If

                If txtNPWP.Text <> "" Then
                    If AMLBLL.ValidateBLL.isContainSpecialCharacter(txtNPWP.Text, " .-/,") Then
                        Throw New Exception("Nomor Pokok Wajib Pajak (NPWP) tidak boleh mengandung special karakter")
                    End If
                End If

                If txtPekerjaan.Text = vbNullString OrElse txtPekerjaan.Text = "" Then
                    Throw New Exception("Pekerjaan harus di isi")

                End If

                If txtPenghasilanRataRata.Text <> "" Then
                    If IsNumeric(txtPenghasilanRataRata.Text.Replace(".", "")) = False Then
                        Throw New Exception("Penghasilan rata-rata/th harus di isi dengan angka")
                    End If
                End If

                Dim StrWhere As String
                StrWhere = CustomerCTRColumn.CustomerName.ToString & "='" & Me.txtNamaLengkap.Text.Replace("'", "''") & "'"
                StrWhere = StrWhere & " AND " & CustomerCTRColumn.INDV_TanggalLahir.ToString & "='" & Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", Me.txtTglLahir.Text).ToString("yyyy-MM-dd") & "'"
                StrWhere = StrWhere & " AND " & CustomerCTRColumn.PK_CustomerCTR_ID.ToString & "<>'" & Me.GetPK_CustomerCTR_ID & "'"
                StrWhere = StrWhere & " AND " & CustomerCTRColumn.IsExistingCustomer.ToString & "='0'"

                Using ObjListCustomerCTR As TList(Of CustomerCTR) = DataRepository.CustomerCTRProvider.GetPaged(StrWhere, "", 0, Integer.MaxValue, Nothing)
                    If Not ObjListCustomerCTR Is Nothing Then
                        If ObjListCustomerCTR.Count > 0 Then
                            Throw New Exception("Data sudah ada di dalam database (Nama dan Tanggal Lahir), silahkan edit data yang sudah ada atau masukan data lain")
                        End If
                    End If
                End Using

            End If

            'Corp
            If rblTipePelapor.SelectedValue = 2 Then
                If cboCORPBentukBadanUsaha.SelectedIndex = 0 Then
                    Throw New Exception("Bentuk Badan Usaha harus di isi")

                End If
                If txtCORPNama.Text = vbNullString OrElse txtCORPNama.Text = "" Then
                    Throw New Exception("Nama Korporasi harus di isi")

                End If
                If cboCORPBentukBadanUsaha.SelectedItem.Text.ToLower.Contains("yayasan") = False Then
                    If txtCORPBidangUsaha.Text = vbNullString OrElse txtCORPBidangUsaha.Text = "" Then
                        Throw New Exception("Bidang Usaha Korporasi harus di isi")

                    End If
                End If

                If rblCORPTipeAlamat.SelectedIndex = -1 Then
                    Throw New Exception("Alamat Korporasi harus di pilih")

                End If
                If rblCORPTipeAlamat.SelectedIndex = 0 Then
                    If txtCORPDLKotaKab.Text = vbNullString OrElse txtCORPDLKotaKab.Text = "" Then
                        Throw New Exception("Kota / Kabupaten (Alamat Lengkap Korporasi) harus di isi")

                    End If
                    If txtCORPDLProvinsi.Text = vbNullString OrElse txtCORPDLProvinsi.Text = "" Then
                        Throw New Exception("Provinsi (Alamat Lengkap Korporasi) harus di isi")

                    End If
                    If txtCORPDLNegara.Text = vbNullString OrElse txtCORPDLNegara.Text = "" Then
                        Throw New Exception("Negara (Alamat Lengkap Korporasi) harus di isi")

                    End If


                End If
                If rblCORPTipeAlamat.SelectedIndex = 1 Then
                    If txtCORPLNKodePos.Text = vbNullString OrElse txtCORPLNKodePos.Text = "" Then
                        Throw New Exception("Kode Pos (Alamat Korporasi Luar Negeri) harus di isi")

                    End If
                    If txtCORPLNNegara.Text = vbNullString OrElse txtCORPLNNegara.Text = "" Then
                        Throw New Exception("Negara (Alamat Korporasi Luar Negeri) harus di isi")

                    End If
                End If

                If txtCORPNPWP.Text <> "" Then
                    If AMLBLL.ValidateBLL.isContainSpecialCharacter(txtCORPNPWP.Text, " .-/,") Then
                        Throw New Exception("Nomor Pokok Wajib Pajak (NPWP) tidak boleh mengandung special karakter")
                    End If
                End If

                Dim StrWhere As String
                StrWhere = CustomerCTRColumn.CustomerName.ToString & "='" & Me.txtNamaLengkap.Text.Replace("'", "''") & "'"
                StrWhere = StrWhere & " AND " & CustomerCTRColumn.NPWP.ToString & "='" & Me.txtCORPNPWP.Text & "'"
                StrWhere = StrWhere & " AND " & CustomerCTRColumn.PK_CustomerCTR_ID.ToString & "<>'" & Me.GetPK_CustomerCTR_ID & "'"
                StrWhere = StrWhere & " AND " & CustomerCTRColumn.IsExistingCustomer.ToString & "='0'"

                Using ObjListCustomerCTR As TList(Of CustomerCTR) = DataRepository.CustomerCTRProvider.GetPaged(StrWhere, "", 0, Integer.MaxValue, Nothing)
                    If Not ObjListCustomerCTR Is Nothing Then
                        If ObjListCustomerCTR.Count > 0 Then
                            Throw New Exception("Data sudah ada di dalam database (Nama dan NPWP), silahkan edit data yang sudah ada atau masukan data lain")
                        End If
                    End If
                End Using
            End If
            Return True

        Catch ex As Exception
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message.ToString
            Return False
        End Try

    End Function

    Private Sub SetControlLoad()

        'bind MsNegara
        Using objNegara As TList(Of MsNegara) = DataRepository.MsNegaraProvider.GetAll
            If objNegara.Count > 0 Then
                cboNegara.Items.Clear()
                cboNegara.Items.Add("-Select-")


                For i As Integer = 0 To objNegara.Count - 1
                    cboNegara.Items.Add(New ListItem(objNegara(i).NamaNegara, objNegara(i).IDNegara.ToString))
                Next
            End If
        End Using

        'Bind MsBentukBadanUsaha
        Using objBentukBadanUsaha As TList(Of MsBentukBidangUsaha) = DataRepository.MsBentukBidangUsahaProvider.GetAll
            If objBentukBadanUsaha.Count > 0 Then
                cboCORPBentukBadanUsaha.Items.Clear()
                cboCORPBentukBadanUsaha.Items.Add("-Select-")

                For i As Integer = 0 To objBentukBadanUsaha.Count - 1
                    cboCORPBentukBadanUsaha.Items.Add(New ListItem(objBentukBadanUsaha(i).BentukBidangUsaha, objBentukBadanUsaha(i).IdBentukBidangUsaha.ToString))
                Next
            End If
        End Using

        'bind ID type
        Using objJenisID As TList(Of MsIDType) = DataRepository.MsIDTypeProvider.GetAll

            Using objHapusJenisID As MsIDType = objJenisID.Find(MsIDTypeColumn.MsIDType_Name, "kartu pelajar")
                If Not IsNothing(objHapusJenisID) Then
                    objJenisID.Remove(objHapusJenisID)
                End If
            End Using

            cboJenisDocID.Items.Clear()
            cboJenisDocID.Items.Add("-Select-")

            For i As Integer = 0 To objJenisID.Count - 1
                cboJenisDocID.Items.Add(New ListItem(objJenisID(i).MsIDType_Name, objJenisID(i).Pk_MsIDType_Id.ToString))
            Next
        End Using

        'bind calendar
        Me.popTglLahir.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtTglLahir.ClientID & "'), 'dd-mm-yyyy')")
        Me.popTglLahir.Style.Add("display", "")


        Me.DdlTujuanTransaksi.Items.Clear()
        Me.DdlTujuanTransaksi.DataSource = AMLBLL.CustomerCTRBLL.GetAllMsTujuanTransaksi()
        Me.DdlTujuanTransaksi.DataValueField = "IdTujuanTransaksi"
        Me.DdlTujuanTransaksi.DataTextField = "NamaTujuanTransaksi"
        Me.DdlTujuanTransaksi.DataBind()
        Me.DdlTujuanTransaksi.Items.Insert(0, New ListItem("[Please Select Data]", "0"))
        Me.DdlTujuanTransaksi.SelectedIndex = 0

        Me.DdlRelasiDenganPemilikRekening.Items.Clear()
        Me.DdlRelasiDenganPemilikRekening.DataSource = AMLBLL.CustomerCTRBLL.GetAllMsRelasiDenganPemilikRekening()
        Me.DdlRelasiDenganPemilikRekening.DataValueField = "IdRelasiDenganPemilikRekening"
        Me.DdlRelasiDenganPemilikRekening.DataTextField = "NamaRelasiDenganPemilikRekening"
        Me.DdlRelasiDenganPemilikRekening.DataBind()
        Me.DdlRelasiDenganPemilikRekening.Items.Insert(0, New ListItem("[Please Select Data]", "0"))
        Me.DdlRelasiDenganPemilikRekening.SelectedIndex = 0
    End Sub

    Sub clearSession()

        Session("CustomerCTREdit.RowEdit") = Nothing
        Session("CustomerCTREdit.ActionType") = Nothing
        Session("CustomerCTREdit.Transaction") = Nothing
        Session("CustomerCTREdit.Sort") = Nothing
    End Sub

#End Region

#Region "Fill Object From Control..."

    Private Sub FillCustomerCTRObject(ByRef objCustomerCTR As CustomerCTR)
        FillOrNothing(objCustomerCTR.CustomerType, rblTipePelapor.SelectedValue, True, oByte)
        objCustomerCTR.IsExistingCustomer = "0"

        If rblTipePelapor.SelectedValue = "1" Then
            'individu
            FillOrNothing(objCustomerCTR.INDV_Gelar, txtGelar.Text)
            FillOrNothing(objCustomerCTR.CustomerName, txtNamaLengkap.Text)
            FillOrNothing(objCustomerCTR.CustomerAlias, txtNamaAlias.Text)
            FillOrNothing(objCustomerCTR.INDV_TempatLahir, txtTempatLahir.Text)
            FillOrNothing(objCustomerCTR.INDV_TanggalLahir, Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", txtTglLahir.Text).ToString("yyyy-MM-dd"), True, oDate)
            FillOrNothing(objCustomerCTR.INDV_Kewarganegaraan, rblKewarganegaraan.SelectedValue.ToString)
            FillOrNothing(objCustomerCTR.INDV_FK_MsNegara_Id, cboNegara.SelectedValue, True, oInt)
            FillOrNothing(objCustomerCTR.INDV_FK_MsIDType_Id, cboJenisDocID.SelectedValue, True, oInt)
            FillOrNothing(objCustomerCTR.INDV_NomorId, txtNomorID.Text)
            FillOrNothing(objCustomerCTR.NPWP, txtNPWP.Text)
            FillOrNothing(objCustomerCTR.INDV_FK_MsPekerjaan_Id, hfPekerjaan.Value, True, oInt)
            FillOrNothing(objCustomerCTR.INDV_Jabatan, txtJabatan.Text)
            FillOrNothing(objCustomerCTR.INDV_PenghasilanRataRata, txtPenghasilanRataRata.Text)
            FillOrNothing(objCustomerCTR.INDV_TempatBekerja, txtTempatKerja.Text)
            'domisili
            FillOrNothing(objCustomerCTR.INDV_DOM_NamaJalan, txtDOMNamaJalan.Text)
            FillOrNothing(objCustomerCTR.INDV_DOM_RT, txtDOMRT.Text)
            FillOrNothing(objCustomerCTR.INDV_DOM_RW, txtDOMRW.Text)
            FillOrNothing(objCustomerCTR.INDV_DOM_FK_MsKelurahan_Id, hfDOMKelurahan.Value, True, OLong)
            FillOrNothing(objCustomerCTR.INDV_DOM_FK_MsKecamatan_Id, hfDOMKecamatan.Value, True, oInt)
            FillOrNothing(objCustomerCTR.INDV_DOM_FK_MsKotaKab_Id, hfDOMKotaKab.Value, True, oInt)
            FillOrNothing(objCustomerCTR.INDV_DOM_KodePos, txtDOMKodePos.Text)
            FillOrNothing(objCustomerCTR.INDV_DOM_FK_MsProvince_Id, hfDOMProvinsi.Value, True, oInt)
            FillOrNothing(objCustomerCTR.INDV_DOM_FK_MsNegara_Id, hfDomNegara.Value, True, oInt)

            'identitas
            FillOrNothing(objCustomerCTR.INDV_ID_NamaJalan, txtIDNamaJalan.Text)
            FillOrNothing(objCustomerCTR.INDV_ID_RT, txtIDRT.Text)
            FillOrNothing(objCustomerCTR.INDV_ID_RW, txtIDRW.Text)
            FillOrNothing(objCustomerCTR.INDV_ID_FK_MsKelurahan_Id, hfIDKelurahan.Value, True, OLong)
            FillOrNothing(objCustomerCTR.INDV_ID_FK_MsKecamatan_Id, hfIDKecamatan.Value, True, oInt)
            FillOrNothing(objCustomerCTR.INDV_ID_FK_MsKotaKab_Id, hfIDKotaKab.Value, True, oInt)
            FillOrNothing(objCustomerCTR.INDV_ID_KodePos, txtIDKodePos.Text)
            FillOrNothing(objCustomerCTR.INDV_ID_FK_MsProvince_Id, hfIDProvinsi.Value, True, oInt)
            FillOrNothing(objCustomerCTR.INDV_ID_FK_MsNegara_Id, hfIDNegara.Value, True, oInt)
            'negara asing
            FillOrNothing(objCustomerCTR.INDV_NA_NamaJalan, txtNANamaJalan.Text)
            FillOrNothing(objCustomerCTR.INDV_NA_FK_MsNegara_Id, hfNANegara.Value, True, oInt)
            FillOrNothing(objCustomerCTR.INDV_NA_FK_MsProvince_Id, hfNAProvinsi.Value, True, oInt)
            FillOrNothing(objCustomerCTR.INDV_NA_FK_MsKotaKab_Id, hfIDKota.Value, True, oInt)
            FillOrNothing(objCustomerCTR.INDV_NA_KodePos, txtNAKodePos.Text)
        Else
            'corp
            If cboCORPBentukBadanUsaha.SelectedIndex <> 0 Then
                FillOrNothing(objCustomerCTR.CORP_FK_MsBentukBadanUsaha_Id, cboCORPBentukBadanUsaha.SelectedValue, True, oInt)
            End If
            FillOrNothing(objCustomerCTR.CustomerName, txtCORPNama.Text)
            FillOrNothing(objCustomerCTR.CORP_FK_MsBidangUsaha_Id, hfCORPBidangUsaha.Value, True, oInt)

            If rblCORPTipeAlamat.SelectedIndex <> 0 Then
                FillOrNothing(objCustomerCTR.CORP_TipeAlamat, rblCORPTipeAlamat.SelectedValue, True, oInt)
            End If
            FillOrNothing(objCustomerCTR.CORP_DOM_NamaJalan, txtCORPDLNamaJalan.Text)
            FillOrNothing(objCustomerCTR.CORP_DOM_RT, txtCORPDLRT.Text)
            FillOrNothing(objCustomerCTR.CORP_DOM_RW, txtCORPDLRW.Text)
            FillOrNothing(objCustomerCTR.CORP_DOM_FK_MsKelurahan_Id, hfCORPDLKelurahan.Value, True, OLong)
            FillOrNothing(objCustomerCTR.CORP_DOM_FK_MsKecamatan_Id, hfCORPDLKecamatan.Value, True, oInt)
            FillOrNothing(objCustomerCTR.CORP_DOM_FK_MsKotaKab_Id, hfCORPDLKotaKab.Value, True, oInt)
            FillOrNothing(objCustomerCTR.CORP_DOM_KodePos, txtCORPDLKodePos.Text)
            FillOrNothing(objCustomerCTR.CORP_DOM_FK_MsProvince_Id, hfCORPDLProvinsi.Value, True, oInt)
            FillOrNothing(objCustomerCTR.CORP_DOM_FK_MsNegara_Id, hfCORPDLNegara.Value, True, oInt)



            FillOrNothing(objCustomerCTR.CORP_LN_NamaJalan, txtCORPLNNamaJalan.Text)
            FillOrNothing(objCustomerCTR.CORP_LN_FK_MsNegara_Id, hfCORPLNNegara.Value, True, oInt)
            FillOrNothing(objCustomerCTR.CORP_LN_FK_MsProvince_Id, hfCORPLNProvinsi.Value, True, oInt)
            FillOrNothing(objCustomerCTR.CORP_LN_MsKotaKab_Id, hfCORPLNKota.Value, True, oInt)
            FillOrNothing(objCustomerCTR.CORP_LN_KodePos, txtCORPLNKodePos.Text)
            FillOrNothing(objCustomerCTR.NPWP, txtCORPNPWP.Text)
        End If


        FillOrNothing(objCustomerCTR.LastUpdateBy, Sahassa.AML.Commonly.SessionUserId)
        FillOrNothing(objCustomerCTR.LastUpdateDate, Now(), True, oDate)
    End Sub

#End Region

#Region "Essential Function ..."

    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Try
            If isvalidData() Then

                Using OTrans As TransactionManager = New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)

                    Try

                        OTrans.BeginTransaction(Data.IsolationLevel.ReadUncommitted)

                        Using ObjCustomerCtr As CustomerCTR = DataRepository.CustomerCTRProvider.GetByPK_CustomerCTR_ID(Me.GetPK_CustomerCTR_ID)
                            FillCustomerCTRObject(ObjCustomerCtr)
                            DataRepository.CustomerCTRProvider.Save(OTrans, ObjCustomerCtr)

                            ' Set PK_CustomerCTR_ID for each Transaction
                            For Each ObjTrx As CustomerCTRTransaction In SetnGetTransaction
                                ObjTrx.FK_CustomerCTR_ID = ObjCustomerCtr.PK_CustomerCTR_ID
                            Next
                            DataRepository.CustomerCTRTransactionProvider.Save(OTrans, SetnGetTransaction)

                            For Each ObjTrx As CustomerCTRTransaction In SetnGetTransactionDeleted
                                'Using objTtransactiongrips As TList(Of TransactionDetailCTRGrips) = DataRepository.TransactionDetailCTRGripsProvider.GetPaged(OTrans, TransactionDetailCTRGripsColumn.TransactionDate.ToString & "='" & ObjTrx.TransactionDate.ToString("yyyy-MM-dd") & "' and " & TransactionDetailCTRGripsColumn.TransactionTicketNo.ToString & "='" & ObjTrx.TransactionTicketNumber & "' and " & TransactionDetailCTRGripsColumn.DebetCredit.ToString & "<>'" & ObjTrx.TransactionType.ToString & "' ", "", 0, Integer.MaxValue, 0)
                                Using objTtransactiongrips As TList(Of TransactionDetailCTRGrips) = DataRepository.TransactionDetailCTRGripsProvider.GetPaged(OTrans, TransactionDetailCTRGripsColumn.TransactionDate.ToString & "='" & ObjTrx.TransactionDate.ToString("yyyy-MM-dd") & "' and " & TransactionDetailCTRGripsColumn.TransactionTicketNo.ToString & "='" & ObjTrx.TransactionTicketNumber & "' ", "", 0, Integer.MaxValue, 0)
                                    If objTtransactiongrips.Count > 0 Then

                                        For Each item As TransactionDetailCTRGrips In objTtransactiongrips
                                            item.FK_CustomerCTR_ID = Nothing
                                            item.FK_CustomerCTRTransaction_ID = Nothing
                                        Next
                                        DataRepository.TransactionDetailCTRGripsProvider.Save(OTrans, objTtransactiongrips)

                                    End If
                                End Using
                            Next

                            ''Ini di-update di batch
                            'For Each ObjTrx As CustomerCTRTransaction In SetnGetTransaction
                            '    'Using objTtransactiongrips As TList(Of TransactionDetailCTRGrips) = DataRepository.TransactionDetailCTRGripsProvider.GetPaged(OTrans, TransactionDetailCTRGripsColumn.TransactionDate.ToString & "='" & ObjTrx.TransactionDate.ToString("yyyy-MM-dd") & "' and " & TransactionDetailCTRGripsColumn.TransactionTicketNo.ToString & "='" & ObjTrx.TransactionTicketNumber & "' and " & TransactionDetailCTRGripsColumn.DebetCredit.ToString & "<>'" & ObjTrx.TransactionType.ToString & "' ", "", 0, Integer.MaxValue, 0)
                            '    Using objTtransactiongrips As TList(Of TransactionDetailCTRGrips) = DataRepository.TransactionDetailCTRGripsProvider.GetPaged(OTrans, TransactionDetailCTRGripsColumn.TransactionDate.ToString & "='" & ObjTrx.TransactionDate.ToString("yyyy-MM-dd") & "' and " & TransactionDetailCTRGripsColumn.TransactionTicketNo.ToString & "='" & ObjTrx.TransactionTicketNumber & "' ", "", 0, Integer.MaxValue, 0)
                            '        If objTtransactiongrips.Count > 0 Then

                            '            For Each item As TransactionDetailCTRGrips In objTtransactiongrips
                            '                item.FK_CustomerCTR_ID = ObjCustomerCtr.PK_CustomerCTR_ID
                            '                item.FK_CustomerCTRTransaction_ID = ObjCustomerCtr.PK_CustomerCTR_ID
                            '            Next

                            '            DataRepository.TransactionDetailCTRGripsProvider.Save(OTrans, objTtransactiongrips)
                            '        End If
                            '    End Using
                            'Next

                            OTrans.Commit()

                            mtvPage.ActiveViewIndex = 1
                            lblMsg.Text = "CustomerCTR data has been save successfully"
                            hideSave()

                            Dim strSQLMapping As String = String.Empty
                            For Each ObjTrx As CustomerCTRTransaction In SetnGetTransaction
                                strSQLMapping += " DELETE FROM TaskListCTRPelaku WITH (ROWLOCK)"
                                strSQLMapping += " WHERE TransactionDate = '" & ObjTrx.TransactionDate.ToString("yyyy-MM-dd") & "'"
                                strSQLMapping += " AND TransactionTicketNo = '" & ObjTrx.TransactionTicketNumber & "'"
                                strSQLMapping += vbCrLf
                            Next
                            If strSQLMapping <> String.Empty Then
                                DataRepository.Provider.ExecuteNonQuery(Data.CommandType.Text, strSQLMapping)
                            End If
                        End Using

                    Catch ex As Exception
                        If OTrans.IsOpen Then
                            OTrans.Rollback()
                        End If
                        Throw
                    End Try
                End Using
            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message.ToString
        End Try
    End Sub

#End Region

    Sub LoadCustomerCTRToField()
        Using objCustomerCTR As CustomerCTR = DataRepository.CustomerCTRProvider.GetByPK_CustomerCTR_ID(Me.GetPK_CustomerCTR_ID)
            SafeDefaultValue = ""
            If Not IsNothing(objCustomerCTR) Then

                'Ambil data buat cari PK Negara, Provinsi, KotaKab, dkk (ini variable yang bakal dpke berkali2)
                Dim i As Integer = 0 'buat iterasi
                Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetAll
                Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
                Dim objKelurahan As TList(Of MsKelurahan) = DataRepository.MsKelurahanProvider.GetAll
                Dim objSKelurahan As MsKelurahan 'Penampung hasil search kelurahan
                Dim objKecamatan As TList(Of MsKecamatan) = DataRepository.MsKecamatanProvider.GetAll
                Dim objSkecamatan As MsKecamatan 'Penampung hasil search kecamatan
                Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetAll
                Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi
                Dim objNegara As TList(Of MsNegara) = DataRepository.MsNegaraProvider.GetAll
                Dim objSnegara As MsNegara 'Penampung hasil search negara yang pke picker
                Dim objPekerjaan As TList(Of MsPekerjaan) = DataRepository.MsPekerjaanProvider.GetAll
                Dim objSPekerjaan As MsPekerjaan 'Penampung hasil search pekerjaan
                Dim objBidangUsaha As TList(Of MsBidangUsaha) = DataRepository.MsBidangUsahaProvider.GetAll
                Dim objSBidangUsaha As MsBidangUsaha 'Penampung hasil search bidang usaha

                'LblCIFNumber.Text = objCustomerCTR.CIFNumber
                'LblAccountNumber.Text = objCustomerCTR.AccountNumber

                SafeSelectedValue(rblTipePelapor, objCustomerCTR.CustomerType)

                If rblTipePelapor.SelectedValue = 1 Then
                    divPerorangan.Visible = True
                    divKorporasi.Visible = False
                Else
                    divPerorangan.Visible = False
                    divKorporasi.Visible = True
                End If


                txtGelar.Text = Safe(objCustomerCTR.INDV_Gelar)
                txtNamaLengkap.Text = Safe(objCustomerCTR.CustomerName)
                txtNamaAlias.Text = Safe(objCustomerCTR.CustomerAlias)
                txtTempatLahir.Text = Safe(objCustomerCTR.INDV_TempatLahir)
                txtTglLahir.Text = objCustomerCTR.INDV_TanggalLahir.GetValueOrDefault().ToString("dd-MM-yyyy")
                SafeSelectedValue(rblKewarganegaraan, objCustomerCTR.INDV_Kewarganegaraan)

                If rblKewarganegaraan.SelectedIndex = 0 Then
                    tblnegaraasal.Visible = False
                Else
                    tblnegaraasal.Visible = True
                End If

                i = 0
                For Each listNegara As ListItem In cboNegara.Items
                    If objCustomerCTR.INDV_FK_MsNegara_Id.ToString = listNegara.Value Then
                        cboNegara.SelectedIndex = i
                        Exit For
                    End If
                    i = i + 1
                Next
                txtDOMNamaJalan.Text = Safe(objCustomerCTR.INDV_DOM_NamaJalan)
                txtDOMRT.Text = Safe(objCustomerCTR.INDV_DOM_RT)
                txtDOMRW.Text = Safe(objCustomerCTR.INDV_DOM_RW)
                objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objCustomerCTR.INDV_DOM_FK_MsKelurahan_Id)
                If Not IsNothing(objSKelurahan) Then
                    txtDOMKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
                    hfDOMKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
                End If
                objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objCustomerCTR.INDV_DOM_FK_MsKecamatan_Id)
                If Not IsNothing(objSkecamatan) Then
                    txtDOMKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
                    hfDOMKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objCustomerCTR.INDV_DOM_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtDOMKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfDOMKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtDOMKodePos.Text = Safe(objCustomerCTR.INDV_DOM_KodePos)
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objCustomerCTR.INDV_DOM_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtDOMProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfDOMProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, objCustomerCTR.INDV_DOM_FK_MsNegara_Id)
                If Not IsNothing(objSnegara) Then
                    txtDOMNegara.Text = Safe(objSnegara.NamaNegara)
                    hfDomNegara.Value = Safe(objSnegara.IDNegara)
                End If

                txtIDNamaJalan.Text = Safe(objCustomerCTR.INDV_ID_NamaJalan)
                txtIDRT.Text = Safe(objCustomerCTR.INDV_ID_RT)
                txtIDRW.Text = Safe(objCustomerCTR.INDV_ID_RW)
                objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objCustomerCTR.INDV_ID_FK_MsKelurahan_Id)
                If Not IsNothing(objSKelurahan) Then
                    txtIDKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
                    hfIDKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
                End If
                objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objCustomerCTR.INDV_ID_FK_MsKecamatan_Id)
                If Not IsNothing(objSkecamatan) Then
                    txtIDKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
                    hfIDKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objCustomerCTR.INDV_ID_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtIDKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfIDKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtIDKodePos.Text = Safe(objCustomerCTR.INDV_ID_KodePos)
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objCustomerCTR.INDV_ID_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtIDProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfIDProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, objCustomerCTR.INDV_ID_FK_MsNegara_Id)
                If Not IsNothing(objSnegara) Then
                    txtIDNegara.Text = Safe(objSnegara.NamaNegara)
                    hfIDNegara.Value = Safe(objSnegara.IDNegara)
                End If

                txtNANamaJalan.Text = Safe(objCustomerCTR.INDV_NA_NamaJalan)
                objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, objCustomerCTR.INDV_NA_FK_MsNegara_Id)
                If Not IsNothing(objSnegara) Then
                    txtNANegara.Text = Safe(objSnegara.NamaNegara)
                    hfNANegara.Value = Safe(objSnegara.IDNegara)
                End If
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objCustomerCTR.INDV_NA_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtNAProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfNAProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objCustomerCTR.INDV_NA_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtNAKota.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfIDKota.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtNAKodePos.Text = Safe(objCustomerCTR.INDV_NA_KodePos)
                i = 0
                For Each jenisDoc As ListItem In cboJenisDocID.Items
                    If objCustomerCTR.INDV_FK_MsIDType_Id.ToString = jenisDoc.Value Then
                        cboJenisDocID.SelectedIndex = i
                        Exit For
                    End If
                    i = i + 1
                Next
                txtNomorID.Text = Safe(objCustomerCTR.INDV_NomorId)
                txtNPWP.Text = Safe(objCustomerCTR.NPWP)
                objSPekerjaan = objPekerjaan.Find(MsPekerjaanColumn.IDPekerjaan, objCustomerCTR.INDV_FK_MsPekerjaan_Id)
                If Not IsNothing(objSPekerjaan) Then
                    txtPekerjaan.Text = Safe(objSPekerjaan.NamaPekerjaan)
                    hfPekerjaan.Value = Safe(objSPekerjaan.IDPekerjaan)
                End If
                txtJabatan.Text = Safe(objCustomerCTR.INDV_Jabatan)
                Try
                    txtPenghasilanRataRata.Text = CDbl(objCustomerCTR.INDV_PenghasilanRataRata).ToString("#,###,###,###,###.##")

                Catch ex As Exception
                    txtPenghasilanRataRata.Text = Safe(objCustomerCTR.INDV_PenghasilanRataRata)
                End Try


                txtTempatKerja.Text = Safe(objCustomerCTR.INDV_TempatBekerja)


                i = 0
                For Each listBentukBadanUsaha As ListItem In cboCORPBentukBadanUsaha.Items
                    If objCustomerCTR.CORP_FK_MsBentukBadanUsaha_Id.ToString = listBentukBadanUsaha.Value Then
                        cboCORPBentukBadanUsaha.SelectedIndex = i
                        Exit For
                    End If
                    i = i + 1
                Next
                txtCORPNama.Text = Safe(objCustomerCTR.CustomerName)
                objSBidangUsaha = objBidangUsaha.Find(MsBidangUsahaColumn.IdBidangUsaha, objCustomerCTR.CORP_FK_MsBidangUsaha_Id)
                If Not IsNothing(objSBidangUsaha) Then
                    txtCORPBidangUsaha.Text = Safe(objSBidangUsaha.NamaBidangUsaha)
                    hfCORPBidangUsaha.Value = Safe(objSBidangUsaha.IdBidangUsaha)
                End If

                SafeNumIndex(rblCORPTipeAlamat, objCustomerCTR.CORP_TipeAlamat)

                txtCORPDLNamaJalan.Text = Safe(objCustomerCTR.CORP_DOM_NamaJalan)
                txtCORPDLRT.Text = Safe(objCustomerCTR.CORP_DOM_RT)
                txtCORPDLRW.Text = Safe(objCustomerCTR.CORP_DOM_RW)

                objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objCustomerCTR.CORP_DOM_FK_MsKelurahan_Id)
                If Not IsNothing(objSKelurahan) Then
                    txtCORPDLKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
                    hfCORPDLKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
                End If
                objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objCustomerCTR.CORP_DOM_FK_MsKecamatan_Id)
                If Not IsNothing(objSkecamatan) Then
                    txtCORPDLKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
                    hfCORPDLKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objCustomerCTR.CORP_DOM_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtCORPDLKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfCORPDLKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtCORPDLKodePos.Text = Safe(objCustomerCTR.CORP_DOM_KodePos)
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objCustomerCTR.CORP_DOM_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtCORPDLProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfCORPDLProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, objCustomerCTR.CORP_DOM_FK_MsNegara_Id)
                If Not IsNothing(objSnegara) Then
                    txtCORPDLNegara.Text = Safe(objSnegara.NamaNegara)
                    hfCORPDLNegara.Value = Safe(objSnegara.IDNegara)
                End If
                txtCORPLNNamaJalan.Text = Safe(objCustomerCTR.CORP_LN_NamaJalan)
                objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, objCustomerCTR.CORP_LN_FK_MsNegara_Id)
                If Not IsNothing(objSnegara) Then
                    txtCORPLNNegara.Text = Safe(objSnegara.NamaNegara)
                    hfCORPLNNegara.Value = Safe(objSnegara.IDNegara)
                End If
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objCustomerCTR.CORP_LN_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtCORPLNProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfCORPLNProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objCustomerCTR.CORP_LN_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtCORPLNKota.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfCORPLNKota.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtCORPLNKodePos.Text = Safe(objCustomerCTR.CORP_LN_KodePos)
                txtCORPNPWP.Text = Safe(objCustomerCTR.NPWP)

            End If


        End Using
    End Sub

    Protected Sub imgDOMNegara_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles imgDOMNegara.Click
        Try
            If Session("PickerNegara.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
                txtDOMNegara.Text = strData(1)
                hfDomNegara.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub imgIDNegara_Click1(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles imgIDNegara.Click
        Try
            If Session("PickerNegara.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
                txtIDNegara.Text = strData(1)
                hfIDNegara.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

#Region "Transaksi"
    Sub LoadCustomerCTRTransaction()
        Me.SetnGetTransaction = DataRepository.CustomerCTRTransactionProvider.GetPaged(CustomerCTRTransactionColumn.FK_CustomerCTR_ID.ToString & "=" & Me.GetPK_CustomerCTR_ID & " AND " & CustomerCTRTransactionColumn.IsProcessed.ToString & "=0", CustomerCTRTransactionColumn.PK_CustomerCTRTransaction_ID.ToString, 0, Integer.MaxValue, Nothing)
        'Me.SetnGetTransactionOri = Me.SetnGetTransaction
        Me.SetnGetTransactionDeleted = New TList(Of CustomerCTRTransaction)
        Me.GridViewTransaction.DataSource = Me.SetnGetTransaction
        Me.GridViewTransaction.DataBind()
    End Sub

    Private Sub ClearTransactionUI()
        TxtTanggalTransaksi.Text = ""
        RblTipeTransaksi.SelectedIndex = -1
        TxtNomorTiketTransaksi.Text = ""
        DdlTujuanTransaksi.SelectedIndex = 0
        DdlRelasiDenganPemilikRekening.SelectedIndex = 0
        Me.SetnGetRowEdit = -1
    End Sub

    Private Sub ShowEditTransaksi(blnShow As Boolean)
        Me.TblEditTransaksi.Visible = blnShow
        Me.TrNewTrx.Visible = Not blnShow
    End Sub

    Private Function IsTransactionDataValid() As Boolean
        Dim SbErrorMessage As New StringBuilder
        Dim DatTanggalTransaksi As DateTime

        Try
            If TxtTanggalTransaksi.Text = "" Then
                SbErrorMessage.AppendLine("Tanggal Transaksi harus diisi<br>")

            ElseIf Sahassa.AML.Commonly.IsDateValid("dd-MM-yyyy", TxtTanggalTransaksi.Text) = False Then
                SbErrorMessage.AppendLine("Format Tanggal Transaksi tidak valid, gunakan format dd-MM-yyyy<br>")
            End If

            'ElseIf DateTime.TryParse(TxtTanggalTransaksi.Text, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.AdjustToUniversal, DatTanggalTransaksi) = False Then
            'SbErrorMessage.AppendLine("Format Tanggal Transaksi tidak valid, gunakan format yyyy-mm-dd<br>")
            'End If

            If RblTipeTransaksi.SelectedIndex = -1 Then
                SbErrorMessage.AppendLine("Tipe Transaksi harus dipilih<br>")
            End If

            If TxtNomorTiketTransaksi.Text = "" Then
                SbErrorMessage.AppendLine("Nomor Tiket Transaksi harus diisi<br>")
            Else

                If Me.SetnGetActionType = "add" Then

                    If AMLBLL.CustomerCTRBLL.IsAlreadyInCustomerCtrTransaction(TxtNomorTiketTransaksi.Text, 0) Then
                        SbErrorMessage.AppendLine("Nomor Tiket Transaksi sudah pernah di input<br>")
                    End If

                    If SetnGetTransaction.FindAll(CustomerCTRTransactionColumn.TransactionTicketNumber, TxtNomorTiketTransaksi.Text).Count > 0 Then
                        SbErrorMessage.AppendLine("Nomor Tiket Transaksi sudah pernah di input<br>")
                    End If
                Else
                    If SetnGetTransaction.FindAll(CustomerCTRTransactionColumn.TransactionTicketNumber, TxtNomorTiketTransaksi.Text).Count > 0 Then
                        If SetnGetTransaction.Item(Me.SetnGetRowEdit).TransactionTicketNumber <> SetnGetTransaction.FindAll(CustomerCTRTransactionColumn.TransactionTicketNumber, TxtNomorTiketTransaksi.Text)(0).TransactionTicketNumber Then
                            SbErrorMessage.AppendLine("Nomor Tiket Transaksi sudah pernah di input<br>")
                        End If


                    End If

                End If
            End If

            If DdlTujuanTransaksi.SelectedIndex < 1 Then
                SbErrorMessage.AppendLine("Tujuan Transaksi harus dipilih<br>")
            ElseIf DdlTujuanTransaksi.SelectedValue = 1 And TxtOtherTujuanTransaksi.Text.Trim = "" Then
                SbErrorMessage.AppendLine("Tujuan Transaksi harus di input<br>")
            End If

            If DdlRelasiDenganPemilikRekening.SelectedIndex < 1 Then
                SbErrorMessage.AppendLine("Relasi dengan Pemilik Rekening harus dipilih<br>")

            ElseIf DdlRelasiDenganPemilikRekening.SelectedValue = 1 And TxtOtherRelasiDenganPemilikRekening.Text.Trim = "" Then
                SbErrorMessage.AppendLine("Relasi dengan Pemilik Rekening harus diinput<br>")
            End If

            If Sahassa.AML.Commonly.IsDateValid("dd-MM-yyyy", TxtTanggalTransaksi.Text) AndAlso TxtNomorTiketTransaksi.Text <> "" Then
                Dim strTicketDate1 As String = Right(TxtTanggalTransaksi.Text, 2) & Mid(TxtTanggalTransaksi.Text, 4, 2) & Left(TxtTanggalTransaksi.Text, 2)
                Dim strTicketDate2 As String = Left(TxtNomorTiketTransaksi.Text, 6)
                If strTicketDate1 <> strTicketDate2 Then
                    SbErrorMessage.AppendLine("Nomor tiket transaksi tidak sesuai dengan tanggal transaksi")
                End If
            End If

            If SbErrorMessage.Length > 0 Then
                Throw New Exception(SbErrorMessage.ToString())
            End If

            Return True
        Catch ex As Exception
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message.ToString
            Return False
        End Try


    End Function

    Private Sub NewTrx()
        Me.ClearTransactionUI()
        ShowEditTransaksi(True)
        Me.SetnGetActionType = "add"
    End Sub

    Protected Sub BtnNewTrx_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles BtnNewTrx.Click
        Try
            Me.NewTrx()
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub BtnSave_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles BtnSave.Click
        Try
            If Me.IsTransactionDataValid() Then
                If Me.SetnGetActionType = "add" Then
                    Using ObjCustomerTransaction As CustomerCTRTransaction = New CustomerCTRTransaction()
                        ObjCustomerTransaction.TransactionDate = SafeDate(Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", Me.TxtTanggalTransaksi.Text).ToString("yyyy-MM-dd")) 'SafeDate(Me.TxtTanggalTransaksi.Text)
                        ObjCustomerTransaction.TransactionType = RblTipeTransaksi.SelectedValue
                        ObjCustomerTransaction.TransactionTicketNumber = Me.TxtNomorTiketTransaksi.Text
                        ObjCustomerTransaction.FK_TujuanTransaksi_ID = Me.DdlTujuanTransaksi.SelectedValue
                        ObjCustomerTransaction.OtherTujuanTransaksi = Me.TxtOtherTujuanTransaksi.Text
                        ObjCustomerTransaction.FK_RelasiDenganPemilikRekening_ID = Me.DdlRelasiDenganPemilikRekening.SelectedValue
                        ObjCustomerTransaction.OtherRelasiDenganPemilikRekening = Me.TxtOtherRelasiDenganPemilikRekening.Text
                        ObjCustomerTransaction.FK_CustomerCTR_ID = -1
                        ObjCustomerTransaction.IsProcessed = 0
                        ObjCustomerTransaction.CreatedBy = Sahassa.AML.Commonly.SessionUserId
                        ObjCustomerTransaction.CreatedDate = DateTime.Now
                        ObjCustomerTransaction.LastUpdateBy = Sahassa.AML.Commonly.SessionUserId
                        ObjCustomerTransaction.LastUpdateDate = DateTime.Now
                        SetnGetTransaction.Add(ObjCustomerTransaction)
                    End Using
                ElseIf Me.SetnGetActionType = "edit" Then

                    Using ObjCustomerTransaction As CustomerCTRTransaction = SetnGetTransaction.Item(Me.SetnGetRowEdit)
                        ObjCustomerTransaction.TransactionDate = SafeDate(Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", Me.TxtTanggalTransaksi.Text).ToString("yyyy-MM-dd")) 'SafeDate(Me.TxtTanggalTransaksi.Text)
                        ObjCustomerTransaction.TransactionType = RblTipeTransaksi.SelectedValue
                        ObjCustomerTransaction.TransactionTicketNumber = Me.TxtNomorTiketTransaksi.Text
                        ObjCustomerTransaction.FK_TujuanTransaksi_ID = Me.DdlTujuanTransaksi.SelectedValue
                        ObjCustomerTransaction.OtherTujuanTransaksi = Me.TxtOtherTujuanTransaksi.Text
                        ObjCustomerTransaction.FK_RelasiDenganPemilikRekening_ID = Me.DdlRelasiDenganPemilikRekening.SelectedValue
                        ObjCustomerTransaction.OtherRelasiDenganPemilikRekening = Me.TxtOtherRelasiDenganPemilikRekening.Text
                        ObjCustomerTransaction.LastUpdateBy = Sahassa.AML.Commonly.SessionUserId
                        ObjCustomerTransaction.LastUpdateDate = DateTime.Now
                    End Using

                End If

                GridViewTransaction.DataSource = SetnGetTransaction
                GridViewTransaction.DataBind()
                ShowEditTransaksi(False)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub BtnCancel_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles BtnCancel.Click
        Try
            Me.ClearTransactionUI()
            ShowEditTransaksi(False)
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub GridViewTransaction_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridViewTransaction.ItemDataBound
        If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then

            e.Item.Cells(3).Text = CStr((e.Item.ItemIndex + 1))

            Dim objlbltransactiontype As Label = CType(e.Item.FindControl("LblTransactiontype"), Label)
            Dim objctransaction As CustomerCTRTransaction = CType(e.Item.DataItem, CustomerCTRTransaction)
            If objctransaction.TransactionType = "C" Then
                objlbltransactiontype.Text = "Setor"
            ElseIf objctransaction.TransactionType = "D" Then
                objlbltransactiontype.Text = "Tarik"
            End If
            Dim objCustomerCTRTransaction As CustomerCTRTransaction = e.Item.DataItem
            If e.Item.Cells(7).Text <> "&nbsp;" Then
                Using objLblTujuanTransaksi As Label = CType(e.Item.FindControl("LblTujuanTransaksi"), Label)
                    Using ObjMsTujuanTransaksi As MsTujuanTransaksi = AMLBLL.CustomerCTRBLL.GetMsTujuanTransaksiByPK(CInt(e.Item.Cells(1).Text))
                        If Not IsNothing(ObjMsTujuanTransaksi) Then
                            If objCustomerCTRTransaction.FK_TujuanTransaksi_ID = 1 Then
                                objLblTujuanTransaksi.Text = ObjMsTujuanTransaksi.NamaTujuanTransaksi + " : " & objCustomerCTRTransaction.OtherTujuanTransaksi
                            Else
                                objLblTujuanTransaksi.Text = ObjMsTujuanTransaksi.NamaTujuanTransaksi
                            End If



                        End If
                    End Using
                End Using
            End If

            If e.Item.Cells(8).Text <> "&nbsp;" Then
                Using objLblRelasiDenganPemilikRekening As Label = CType(e.Item.FindControl("LblRelasiDenganPemilikRekening"), Label)
                    Using ObjMsRelasiDenganPemilikRekening As MsRelasiDenganPemilikRekening = AMLBLL.CustomerCTRBLL.GetMsRelasiDenganPemilikRekeningByPK(CInt(e.Item.Cells(2).Text))
                        If Not IsNothing(ObjMsRelasiDenganPemilikRekening) Then
                            objLblRelasiDenganPemilikRekening.Text = ObjMsRelasiDenganPemilikRekening.NamaRelasiDenganPemilikRekening

                            If objCustomerCTRTransaction.FK_RelasiDenganPemilikRekening_ID = 1 Then
                                objLblRelasiDenganPemilikRekening.Text = ObjMsRelasiDenganPemilikRekening.NamaRelasiDenganPemilikRekening + " : " & objCustomerCTRTransaction.OtherRelasiDenganPemilikRekening
                            Else
                                objLblRelasiDenganPemilikRekening.Text = ObjMsRelasiDenganPemilikRekening.NamaRelasiDenganPemilikRekening
                            End If

                        End If
                    End Using
                End Using
            End If

            If (e.Item.Cells(12).HasControls()) Then
                Dim lnkbtnDelete As LinkButton = (e.Item.FindControl("delbutton"))
                lnkbtnDelete.Attributes.Add("onclick", "return confirm('Do you want to Delete?');")
            End If
        End If
    End Sub

    Protected Sub GridViewTransaction_ItemCommand(source As Object, e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridViewTransaction.ItemCommand

        If e.CommandName.ToLower = "edit" Then
            Me.ClearTransactionUI()
            ShowEditTransaksi(True)
            Me.SetnGetActionType = "edit"
            'Me.SetnGetRowEdit = e.Item.ItemIndex
            Me.SetnGetRowEdit = (Me.GridViewTransaction.CurrentPageIndex * 10) + e.Item.ItemIndex

            Using ObjCustomerTransaction As CustomerCTRTransaction = Me.SetnGetTransaction.Item(Me.SetnGetRowEdit)
                Me.TxtTanggalTransaksi.Text = ObjCustomerTransaction.TransactionDate.ToString("dd-MM-yyyy")
                RblTipeTransaksi.SelectedValue = ObjCustomerTransaction.TransactionType
                Me.TxtNomorTiketTransaksi.Text = ObjCustomerTransaction.TransactionTicketNumber
                Me.DdlTujuanTransaksi.SelectedValue = ObjCustomerTransaction.FK_TujuanTransaksi_ID
                Me.TxtOtherTujuanTransaksi.Text = ObjCustomerTransaction.OtherTujuanTransaksi
                Me.DdlRelasiDenganPemilikRekening.SelectedValue = ObjCustomerTransaction.FK_RelasiDenganPemilikRekening_ID
                Me.TxtOtherRelasiDenganPemilikRekening.Text = ObjCustomerTransaction.OtherRelasiDenganPemilikRekening
            End Using
            ShowTujuanTransaksiOther()
            ShowRelasiDenganPemilikRekeningOther()
        ElseIf e.CommandName.ToLower = "delete" Then
            Me.SetnGetActionType = "delete"
            'Me.SetnGetRowEdit = e.Item.ItemIndex
            Me.SetnGetRowEdit = (Me.GridViewTransaction.CurrentPageIndex * 10) + e.Item.ItemIndex
            Me.SetnGetTransaction.RemoveAt(Me.SetnGetRowEdit)
            GridViewTransaction.DataSource = SetnGetTransaction
            GridViewTransaction.DataBind()
            ShowEditTransaksi(False)
        End If
    End Sub

#End Region

    Private Sub ShowTujuanTransaksiOther()
        If DdlTujuanTransaksi.SelectedValue = "1" Then
            Me.TxtOtherTujuanTransaksi.Visible = True
        Else
            Me.TxtOtherTujuanTransaksi.Visible = False
        End If
    End Sub

    Private Sub ShowRelasiDenganPemilikRekeningOther()
        If DdlRelasiDenganPemilikRekening.SelectedValue = "1" Then
            Me.TxtOtherRelasiDenganPemilikRekening.Visible = True
        Else
            Me.TxtOtherRelasiDenganPemilikRekening.Visible = False
        End If
    End Sub

    Protected Sub DdlRelasiDenganPemilikRekening_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles DdlRelasiDenganPemilikRekening.SelectedIndexChanged

        Me.TxtOtherRelasiDenganPemilikRekening.Text = ""
        Me.ShowRelasiDenganPemilikRekeningOther()
    End Sub

    Protected Sub DdlTujuanTransaksi_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles DdlTujuanTransaksi.SelectedIndexChanged
        Me.TxtOtherTujuanTransaksi.Text = ""
        Me.ShowTujuanTransaksiOther()
    End Sub

    Protected Sub delbutton_Click(sender As Object, e As EventArgs)
        Try
            Dim objgriditem As DataGridItem = CType(CType(sender, LinkButton).NamingContainer, DataGridItem)

            Me.SetnGetActionType = "delete"
            'Me.SetnGetRowEdit = objgriditem.ItemIndex
            Me.SetnGetRowEdit = (Me.GridViewTransaction.CurrentPageIndex * 10) + objgriditem.ItemIndex
            Me.SetnGetTransactionDeleted.Add(SetnGetTransaction(SetnGetRowEdit))
            Me.SetnGetTransaction.RemoveAt(Me.SetnGetRowEdit)
            If GridViewTransaction.PageCount > 1 Then
                If Me.SetnGetTransaction.Count Mod 10 = 0 Then
                    GridViewTransaction.CurrentPageIndex = GridViewTransaction.PageCount - 2
                End If
            End If
            GridViewTransaction.DataSource = SetnGetTransaction
            GridViewTransaction.DataBind()
            ShowEditTransaksi(False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridViewTransaction_PageIndexChanged(source As Object, e As DataGridPageChangedEventArgs) Handles GridViewTransaction.PageIndexChanged
        Try
            GridViewTransaction.CurrentPageIndex = e.NewPageIndex
            GridViewTransaction.DataSource = SetnGetTransaction
            GridViewTransaction.DataBind()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridViewTransaction_SortCommand(source As Object, e As DataGridSortCommandEventArgs) Handles GridViewTransaction.SortCommand
        Dim GridUser As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
            SetnGetTransaction.Sort(Me.SetnGetSort)
            GridViewTransaction.DataSource = SetnGetTransaction
            GridViewTransaction.DataBind()

            ShowEditTransaksi(False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class

