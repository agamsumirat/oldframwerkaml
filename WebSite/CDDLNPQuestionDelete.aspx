<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="CDDLNPQuestionDelete.aspx.vb" Inherits="CDDLNPQuestionDelete" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
        height="72" style="border-top-style: none; border-right-style: none; border-left-style: none;
        border-bottom-style: none" width="100%">
        <tr>
            <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>
                    <img height="17" src="Images/dot_title.gif" width="17" />
                    <asp:Label ID="lblHeader" runat="server" Text="EDD Question - Delete"></asp:Label>&nbsp;
                    <hr />
                </strong>
            </td>
        </tr>
    </table>
    <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
        border="2" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none">
        <tr class="formText">
            <td bgcolor="#ffffff" width="5px">
                &nbsp;</td>
            <td bgcolor="#ffffff" width="150px">
                <strong>Question</strong></td>
            <td bgcolor="#ffffff" colspan="2">
                <asp:Label ID="txtQuestion" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" width="5px">
                &nbsp;</td>
            <td bgcolor="#ffffff" width="150px">
                <strong>
                Question Type</strong></td>
            <td bgcolor="#ffffff" colspan="2">
                <asp:Label ID="txtQuestionType" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" width="5px">
                &nbsp;</td>
            <td bgcolor="#ffffff" width="150px">
                <strong>
                Is Required</strong></td>
            <td bgcolor="#ffffff" colspan="2">
                <asp:Label ID="txtIsRequired" runat="server" /></td>
        </tr>
        <tr class="formText" id="RowQuestionDetail" runat="server" visible="false">
            <td bgcolor="#ffffff" width="5px">
                &nbsp;</td>
            <td bgcolor="#ffffff" colspan="3">
                <table width="100%">
                    <tr>
                        <td colspan="2" style="height: 15px">
                            <strong>Question Detail</strong>
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:DataGrid ID="GridViewCDDQuestionDetail" runat="server" AutoGenerateColumns="False"
                                Font-Size="XX-Small" BackColor="White" CellPadding="4" Width="100%" GridLines="Vertical"
                                BorderColor="#DEDFDE" ForeColor="Black">
                                <FooterStyle BackColor="#CCCC99"></FooterStyle>
                                <AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
                                <ItemStyle BackColor="#F7F7DE"></ItemStyle>
                                <HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#6B696B"></HeaderStyle>
                                <Columns>
                                    <asp:BoundColumn HeaderText="Question Detail" DataField="QuestionDetail" SortExpression="QuestionDetail  asc">
                                        <ItemStyle Width="35%" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="Is Need Validation" DataField="IsNeedValidation" SortExpression="IsNeedValidation  asc" Visible="false">
                                        <ItemStyle Width="15%" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="FK_CDD_VAlidation_ID" Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="Validation">
                                        <ItemStyle Width="40%" />
                                    </asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr runat="server" class="formText">
            <td bgcolor="#ffffff" width="5px">
                &nbsp;</td>
            <td bgcolor="#ffffff" width="150px">
                <strong>
                Parent Question</strong></td>
            <td bgcolor="#ffffff" colspan="2">
                <asp:Label ID="txtParentQuestion" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText" bgcolor="#dddddd" height="30">
            <td style="width: 5px">
                <img height="15" src="images/arrow.gif" width="15"></td>
            <td colspan="5" style="height: 9px">
                <table cellspacing="0" cellpadding="3" border="0">
                    <tr>
                        <td>
                            <ajax:AjaxPanel ID="AjaxPanel7" runat="server">
                                <asp:ImageButton ID="ImageDelete" runat="server" CausesValidation="True" ImageUrl="~/Images/button/delete.gif">
                                </asp:ImageButton></ajax:AjaxPanel></td>
                        <td>
                            <ajax:AjaxPanel ID="AjaxPanel8" runat="server">
                                <asp:ImageButton ID="ImageCancel" runat="server" ImageUrl="~/Images/Button/Cancel.gif">
                                </asp:ImageButton></ajax:AjaxPanel></td>
                    </tr>
                </table>
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
        </tr>
    </table>
</asp:Content>
