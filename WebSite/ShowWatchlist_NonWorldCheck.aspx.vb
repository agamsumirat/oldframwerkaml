﻿Partial Class ShowWatchlist_NonWorldCheck
    Inherits System.Web.UI.Page

    Public Property UID As String
        Get
            Return Session("ShowWatchlist_NonWorldCheck.UID")
        End Get
        Set(ByVal value As String)
            Session("ShowWatchlist_NonWorldCheck.UID") = value
        End Set
    End Property
    Private Sub ShowWatchlist_NonWorldCheck_Load(sender As Object, e As EventArgs) Handles Me.Load

        Try
            If Not IsPostBack Then

                UID = Request.Params("uid")

                If UID IsNot Nothing Then
                    LoadDataVerificationListMaster(UID)
                End If

            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub LoadDataVerificationListMaster(IDParam As String)

        Dim currentRow As Data.DataRow
        Dim parsedDOB As Date
        Dim isValidDate As Boolean

        Using objdt As Data.DataTable = EKABLL.ScreeningBLL.GetVerificationListMasterRelationData(IDParam)

            If objdt.Rows.Count = 0 Then
                Return
            End If

            currentRow = objdt.Rows(0)

            lblName.Text = currentRow.Item(0).ToString()

            isValidDate = Date.TryParse(currentRow.Item(1).ToString(), parsedDOB)

            If isValidDate Then
                lblDOB.Text = parsedDOB.ToString("dd-MMM-yyyy")
            Else
                lblDOB.Text = currentRow.Item(1).ToString()
            End If

            lblYOB.Text = currentRow.Item(2).ToString()
            lblNationality.Text = currentRow.Item(3).ToString()
            lblBirthPlace.Text = currentRow.Item(4).ToString()
            lblListType.Text = currentRow.Item(5)
            lblCategoryType.Text = currentRow.Item(6)
            lblCustomRemark_1.Text = currentRow.Item(7)
            lblCustomRemark_2.Text = currentRow.Item(8)
            lblCustomRemark_3.Text = currentRow.Item(9)
            lblCustomRemark_4.Text = currentRow.Item(10)
            lblCustomRemark_5.Text = currentRow.Item(11)
        End Using

    End Sub

End Class
