<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="AuxTransactionCodeParameter.aspx.vb" Inherits="AuxTransactionCodeParameter" title="Auxiliary Transaction Code Parameter" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
<table>           
	<tr>
		<td vAlign="bottom" align="left"><IMG height="15" src="images/dot_title.gif" width="15"></td>
		<td class="maintitle" vAlign="bottom" width="99%" bgColor="#ffffff"><asp:label id="lblTitle" runat="server">Excluded Transaction Code Parameter</asp:label></td>
	</tr>
	<tr>
		<td background="images/validationbground.gif" colSpan="2"><ajax:ajaxpanel id="AjaxpanelValidSumm" runat="server" Width="688px">
            &nbsp;<asp:customvalidator id="cvalPageErr" runat="server" Display="Dynamic" ErrorMessage="CustomValidator">
					<b></b>
				</asp:customvalidator>
				<asp:label id="lblSuccess" runat="server" CssClass="validationok" Visible="False" Width="488px"></asp:label>
			</ajax:ajaxpanel></td>
	</tr>

	<tr>
		<td bgColor="#ffffff" colSpan="2">          
			<table id="Table1" borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
				border="2">                 
				<tr class="formText">                  
					<td width="5" bgColor="#ffffff" height="146">&nbsp;</td>           
					<TD width="220" bgColor="#ffffff" height="146">
						<P>
                            List of available Transaction Code</P>
						<P><%--<ajax:ajaxpanel id="AjaxPanelListAvailableFile" runat="server">--%>                  
								    <asp:listbox id="ListAvailableFile" tabIndex="2" runat="server" Width="360px" CssClass="searcheditcbo"
									    SelectionMode="Multiple" Height="200px" AutoPostBack="false"></asp:listbox>
                  		<%--</ajax:ajaxpanel>--%>
                            
                            </P>
					</TD>
					<td width="12" bgColor="#ffffff" height="146">
						<P><%--<ajax:ajaxpanel id="AjaxPanelImageAdd" runat="server">--%>
								<asp:imagebutton id="ImageAdd" runat="server" ImageUrl="Images/arrow.gif" 
                                    ></asp:imagebutton>
							<%--</ajax:ajaxpanel>--%></P>
						<P><%--<ajax:ajaxpanel id="AjaxPanelImageRemove" runat="server">--%>
								<asp:imagebutton id="ImageRemove" runat="server" ImageUrl="Images/left.gif"></asp:imagebutton>
							<%--</ajax:ajaxpanel>--%></P>
					</td>
					<TD width="99%" bgColor="#ffffff" height="146"><P>
                        List of Ignored Transaction Code</P>
						<P><%--<ajax:ajaxpanel id="AjaxPanelListSelectedFile" runat="server">--%>
								<asp:listbox id="ListSelectedFile" tabIndex="3" runat="server" Width="360px" CssClass="searcheditcbo"
									SelectionMode="Multiple" Height="200px" AutoPostBack="false"></asp:listbox>
							<%--</ajax:ajaxpanel>--%></P>
					</TD>            
				</tr>               
				<tr class="formText" bgColor="#dddddd" height="20">
					<td width="15"><IMG height="15" src="images/arrow.gif" width="15"></td>
					<td>
						<table cellSpacing="0" cellPadding="3" border="0">
							<tr>
								<td><asp:imagebutton id="ImageSave" runat="server" ImageUrl="images/button/save.gif" CausesValidation="True"></asp:imagebutton></td>
								<td><asp:imagebutton id="ImageCancel" runat="server" ImageUrl="images/button/cancel.gif" CausesValidation="False"></asp:imagebutton></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	</table>
</asp:Content>