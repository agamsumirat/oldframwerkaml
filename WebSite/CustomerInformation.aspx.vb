Imports SahassaNettier.Entities
Imports SahassaNettier.Data
Imports System.Data
Imports System.Data.SqlClient

Partial Class CustomerInformation
    Inherits Parent

#Region " Property "
    Private StrCIFNo As String = Nothing
    ''' <summary>
    ''' selected item store
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSelectedItemFilter() As ArrayList
        Get
            Return IIf(Session("CustomerInformationViewSelected") Is Nothing, New ArrayList, Session("CustomerInformationViewSelected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("CustomerInformationViewSelected") = value
        End Set
    End Property
    ''' <summary>
    ''' setnget search field
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetFieldSearch() As String
        Get
            Return IIf(Session("CustomerInformationViewFieldSearch") Is Nothing, "", Session("CustomerInformationViewFieldSearch"))
        End Get
        Set(ByVal Value As String)
            Session("CustomerInformationViewFieldSearch") = Value
        End Set
    End Property
    ''' <summary>
    ''' search value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetValueSearch() As String
        Get
            Return IIf(Session("CustomerInformationViewValueSearch") Is Nothing, "", Session("CustomerInformationViewValueSearch"))
        End Get
        Set(ByVal Value As String)
            Session("CustomerInformationViewValueSearch") = Value
        End Set
    End Property
    ''' <summary>
    ''' sort expresion
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("CustomerInformationViewSort") Is Nothing, "CIFNo  asc", Session("CustomerInformationViewSort"))
        End Get
        Set(ByVal Value As String)
            Session("CustomerInformationViewSort") = Value
        End Set
    End Property
    ''' <summary>
    ''' current page index
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("CustomerInformationViewCurrentPage") Is Nothing, 0, Session("CustomerInformationViewCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("CustomerInformationViewCurrentPage") = Value
        End Set
    End Property
    ''' <summary>
    ''' total pages
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    ''' <summary>
    ''' row total
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("CustomerInformationViewRowTotal") Is Nothing, 0, Session("CustomerInformationViewRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("CustomerInformationViewRowTotal") = Value
        End Set
    End Property
    ''' <summary>
    ''' save bind table
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetBindTable() As Data.DataTable
        Get
            Dim StrFilter As String = Me.SetnGetFieldSearch.Replace("-=Search=-", Me.SetnGetValueSearch.Replace("'", "''"))
            If StrFilter <> "" Then
                StrFilter = StrFilter & " AND (CFMAST.CFINSC IN (SELECT InsiderCodeId FROM GroupInsiderCodeMapping WHERE GroupId= (SELECT UserGroupId FROM [User] WHERE UserId = '" & Sahassa.AML.Commonly.SessionUserId & "')) OR CFMAST.CFINSC='' OR CFMAST.CFINSC IS NULL)"
            Else
                StrFilter = "(CFMAST.CFINSC IN (SELECT InsiderCodeId FROM GroupInsiderCodeMapping WHERE GroupId= (SELECT UserGroupId FROM [User] WHERE UserId = '" & Sahassa.AML.Commonly.SessionUserId & "')) OR CFMAST.CFINSC='' OR CFMAST.CFINSC IS NULL)"
            End If
            Return IIf(Session("CustomerInformationViewData") Is Nothing, Sahassa.AML.Commonly.GetDatasetPaging(Me.Tables, Me.PK, Me.SetnGetSort, Me.SetnGetCurrentPage + 1, Me.PageSize, Me.Fields, StrFilter, ""), Session("CustomerInformationViewData"))
        End Get
        Set(ByVal value As Data.DataTable)
            Session("CustomerInformationViewData") = value
        End Set
    End Property

    'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    ''' <summary>
    ''' Nama Table
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property Tables() As String
        Get
            'Return "CDD_CIF_Master INNER JOIN AccountOwnerWorkingUnitMapping ON CDD_CIF_Master.AccountOwnerId = AccountOwnerWorkingUnitMapping.AccountOwnerID INNER JOIN  WorkingUnit ON AccountOwnerWorkingUnitMapping.WorkingUnitID = WorkingUnit.WorkingUnitID LEFT JOIN CustomerInList ON CDD_CIF_Master.CIFNo=CustomerInList.CIFNo INNER JOIN dbo.CDMAST on CDD_CIF_Master.CIFNo = dbo.CDMAST.CIFNo"
            'Return "CustomerInformation_WebTempTable Inner Join dbo.CDMAST ON dbo.CDMAST.CIFNO = CustomerInformation_WebTempTable.CIFNo"
            Return "CustomerInformation_WebTempTable INNER JOIN CFMAST ON CustomerInformation_WebTempTable.CIFNo=CFMAST.CFCIF# left join RiskFactFinalResult a on CustomerInformation_WebTempTable.CIFNo=a.cifno"
        End Get
    End Property

    ''' <summary>
    ''' PK
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property PK() As String
        Get
            'Return "CDD_CIF_Master.CIFNo"
            Return "CustomerInformation_WebTempTable.CIFNo"
        End Get
    End Property

    ''' <summary>
    ''' Fields
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property Fields() As String
        Get
            'Return "CDD_CIF_Master.CIFNo, CDD_CIF_Master.Name, CDD_CIF_Master.DateOfBirth, CDD_CIF_Master.AccountOwnerId, CDD_CIF_Master.OpeningDate, WorkingUnit.WorkingUnitName, CASE WHEN NOT CustomerInListId IS NULL THEN 'TRUE' ELSE 'FALSE' END AS IsCustomerInList"
            Return "CustomerInformation_WebTempTable.CIFNo, [Name], DateOfBirth, AccountOwnerId, OpeningDate, WorkingUnitName, isnull(a.amlrisk,'Low') as  IsCustomerInList"
        End Get
    End Property

    ''' <summary>
    ''' Page Size
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property PageSize() As Int16
        Get
            Return IIf(Session("PageSize") IsNot Nothing, Session("PageSize"), 10)
        End Get
        Set(ByVal value As Int16)
            Session("PageSize") = value
        End Set
    End Property

    'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
#End Region

    Private Property SearchType() As Boolean
        Get
            If Session("CustomerInformation.SearchType") = Nothing Then
                Session("CustomerInformation.SearchType") = False
            End If
            Return Session("CustomerInformation.SearchType")
        End Get
        Set(ByVal value As Boolean)
            Session("CustomerInformation.SearchType") = value
        End Set
    End Property

    Private Property StrAdvanceSearchFilter() As String
        Get
            If Session("CustomerInformation.StrAdvanceSearchFilter") = Nothing Then
                Session("CustomerInformation.StrAdvanceSearchFilter") = ""
            End If
            Return Session("CustomerInformation.StrAdvanceSearchFilter")
        End Get
        Set(ByVal value As String)
            Session("CustomerInformation.StrAdvanceSearchFilter") = value
        End Set
    End Property

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' fill search
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub FillSearch()
        Try
            Me.ComboSearch.Items.Add(New ListItem("...", ""))
            Me.ComboSearch.Items.Add(New ListItem("Branch Owner", "WorkingUnitName Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("CIF No", "CustomerInformation_WebTempTable.CIFNo Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Name", "CustomerInformation_WebTempTable.Name Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Date Of Birth", "DateOfBirth BETWEEN '-=Search=- 00:00' AND '-=Search=- 23:59'"))
            Me.ComboSearch.Items.Add(New ListItem("Opening Date", "OpeningDate BETWEEN '-=Search=- 00:00' AND '-=Search=- 23:59'"))
            'Me.ComboSearch.Items.Add(New ListItem("IsCustomerInList", "CASE WHEN NOT CustomerInListId IS NULL THEN 'TRUE' ELSE 'FALSE' END LIKE '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("AML/CFT Risk", "isnull(a.amlrisk,'Low')  LIKE '%-=Search=-%'"))
        Catch
            Throw
        End Try
    End Sub

    Private Sub ClearThisPageSessions()
        Session("CustomerInformationViewSelected") = Nothing
        Session("CustomerInformationViewFieldSearch") = Nothing
        Session("CustomerInformationViewValueSearch") = Nothing
        Session("CustomerInformationViewSort") = Nothing
        Session("CustomerInformationViewCurrentPage") = Nothing
        Session("CustomerInformationViewRowTotal") = Nothing
        Session("CustomerInformationViewData") = Nothing

        Session("CustomerInformation.SearchType") = Nothing
        Session("QueryBuilderDinamic.SelectedDataRow") = Nothing
        Session("QueryBuilderDinamic.ObjDataFilter") = Nothing
        Session("CustomerInformation.StrAdvanceSearchFilter") = Nothing
    End Sub

    ''' <summary>
    ''' Load Data
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                Me.ClearThisPageSessions()
                Me.ComboSearch.Attributes.Add("onchange", "javascript:Check2Calendar(this,'" & Me.TextSearch.ClientID & "', '" & Me.popUp.ClientID & "',4,5);")
                Me.popUp.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TextSearch.ClientID & "'), 'dd-mmm-yyyy')")
                Me.popUp.Style.Add("display", "none")
                Me.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow
                Me.FillSearch()
                Me.GridMSUserView.PageSize = Me.PageSize

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using

                Me.popUp1.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TxtFieldAwal.ClientID & "'), 'dd-mm-yyyy')")
                Me.popUp2.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TxtFieldAkhir.ClientID & "'), 'dd-mm-yyyy')")
                ChangeSearchType()
                LoadField()
                Filterdata()
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' page navigate button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknown Commandname " & e.CommandName)
            End Select
            Me.CollectSelected()
            Filterdata()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' bind grid
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Sub BindGrid()
        Try
            'Me.SetnGetBindTable = Sahassa.AML.Commonly.GetDatasetPaging(Me.Tables, Me.PK, Me.SetnGetSort, Me.SetnGetCurrentPage + 1, Me.PageSize, Me.Fields, Me.SetnGetFieldSearch.Replace("-=Search=-", Me.SetnGetValueSearch.Replace("'", "''")), "")
            Dim StrFilter As String = Me.SetnGetFieldSearch.Replace("-=Search=-", Me.SetnGetValueSearch.Replace("'", "''"))
            If StrFilter <> "" Then
                StrFilter = StrFilter & " AND (CFMAST.CFINSC IN (SELECT InsiderCodeId FROM GroupInsiderCodeMapping WHERE GroupId= (SELECT UserGroupId FROM [User] WHERE UserId = '" & Sahassa.AML.Commonly.SessionUserId & "')) OR CFMAST.CFINSC='' OR CFMAST.CFINSC IS NULL)"
            Else
                StrFilter = "(CFMAST.CFINSC IN (SELECT InsiderCodeId FROM GroupInsiderCodeMapping WHERE GroupId= (SELECT UserGroupId FROM [User] WHERE UserId = '" & Sahassa.AML.Commonly.SessionUserId & "')) OR CFMAST.CFINSC='' OR CFMAST.CFINSC IS NULL)"
            End If
            Me.SetnGetBindTable = Sahassa.AML.Commonly.GetDatasetPaging(Me.Tables, Me.PK, Me.SetnGetSort, Me.SetnGetCurrentPage + 1, Me.PageSize, Me.Fields, StrFilter, "")
            Me.GridMSUserView.DataSource = Me.SetnGetBindTable
            Using AccessCustomerInformation As New AMLDAL.CustomerInformationTableAdapters.GetCountCustomerInformationFilterTableAdapter
                Dim TotalRow As Int64
                TotalRow = Sahassa.AML.Commonly.GetTableCount(Me.Tables, StrFilter)
                'TotalRow = AccessCustomerInformation.Fill("WHERE " & StrFilter)
                Me.SetnGetRowTotal = TotalRow
            End Using
            Me.GridMSUserView.VirtualItemCount = Me.SetnGetRowTotal
            Me.GridMSUserView.DataBind()
        Catch
            Throw
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' set navigate info
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.ComboSearch.SelectedValue = Me.SetnGetFieldSearch
            Me.TextSearch.Text = Me.SetnGetValueSearch
        Catch
            Throw
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' change sort expression
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridMSUserView.SortCommand
        Dim GridUser As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
            Filterdata()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' image button search
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Try
            CollectSelected()
            Me.SetnGetFieldSearch = Me.ComboSearch.SelectedValue
            If Me.ComboSearch.SelectedIndex = 0 Then
                Me.TextSearch.Text = ""
                Me.SetnGetValueSearch = Me.TextSearch.Text
            ElseIf Me.ComboSearch.SelectedIndex = 4 Or Me.ComboSearch.SelectedIndex = 5 Then
                Dim DateSearch As DateTime
                Try
                    DateSearch = DateTime.Parse(Me.TextSearch.Text, New System.Globalization.CultureInfo("id-ID"))
                Catch ex As ArgumentOutOfRangeException
                    Throw New Exception("Input character cannot be convert to datetime.")
                Catch ex As FormatException
                    Throw New Exception("Unknown format date")
                End Try
                Me.SetnGetValueSearch = DateSearch.ToString("dd-MMM-yyyy")
            Else
                Me.SetnGetValueSearch = Me.TextSearch.Text
            End If
            Me.SetnGetCurrentPage = 0
            Filterdata()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' grid edit command handler
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridMSUserView.EditCommand
        Dim strCIFNo As String = e.Item.Cells(2).Text
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "CustomerInformationDetail.aspx?CIFNo=" & strCIFNo

            MagicAjax.AjaxCallHelper.Redirect("CustomerInformationDetail.aspx?CIFNo=" & strCIFNo)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub


    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' go button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	14/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be a positive number greater than zero.")
                End If
            Else
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If
            Filterdata()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    ''' <summary>
    ''' collect sub
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = gridRow.FindControl("CheckBoxExporttoExcel")
                Dim CIFNo As String = gridRow.Cells(2).Text
                Dim ArrTarget As ArrayList = Me.SetnGetSelectedItemFilter
                If ArrTarget.Contains(CIFNo) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(CIFNo)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(CIFNo)
                End If
                Me.SetnGetSelectedItemFilter = ArrTarget
            End If
        Next
    End Sub

    Sub Filterdata()
        If Me.SearchType Then
            Me.BindGridAdvanceSearch()
        Else
            Me.BindGrid()
        End If
        Me.SetInfoNavigate()
        Me.ChangeSearchType()
    End Sub

    ''' <summary>
    ''' prerender event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
           
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    ''' <summary>
    ''' clear all control except control
    ''' </summary>
    ''' <param name="control">excluded control</param>
    ''' <remarks></remarks>
    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub 'ClearControls

    ''' <summary>
    ''' bind selected item
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub BindSelected()
        Try
            'Dim Rows As New ArrayList
            'Dim oTableCustomerInformation As New Data.DataTable
            'Dim oRowCustomerInformation As Data.DataRow
            'oTableCustomerInformation.Columns.Add("WorkingUnitName", GetType(String))
            'oTableCustomerInformation.Columns.Add("CIFNo", GetType(String))
            'oTableCustomerInformation.Columns.Add("Name", GetType(String))
            'oTableCustomerInformation.Columns.Add("DateOfBirth", GetType(DateTime))
            'oTableCustomerInformation.Columns.Add("OpeningDate", GetType(DateTime))
            'oTableCustomerInformation.Columns.Add("IsCustomerInList", GetType(String))
            'oTableCustomerInformation.Columns.Item("IsCustomerInList").Caption = "VerificationList?"
            'For Each IdPk As String In Me.SetnGetSelectedItemFilter
            '    Dim rowData() As Data.DataRow = Me.SetnGetBindTable.Select("CIFNo = '" & IdPk & "'")
            '    If rowData.Length > 0 Then
            '        oRowCustomerInformation = oTableCustomerInformation.NewRow
            '        oRowCustomerInformation(0) = rowData(0)(5)
            '        oRowCustomerInformation(1) = rowData(0)(0)
            '        oRowCustomerInformation(2) = rowData(0)(1)
            '        oRowCustomerInformation(3) = rowData(0)(2)
            '        oRowCustomerInformation(4) = rowData(0)(4)
            '        oRowCustomerInformation(5) = rowData(0)(6)
            '        oTableCustomerInformation.Rows.Add(oRowCustomerInformation)
            '    End If
            'Next
            'Me.GridMSUserView.DataSource = oTableCustomerInformation
            'Me.GridMSUserView.AllowPaging = False
            'Me.GridMSUserView.DataBind()

            ''Sembunyikan kolom ke 0,1,4 & 5 agar tidak ikut diekspor ke excel
            'Me.GridMSUserView.Columns(0).Visible = False
            'Me.GridMSUserView.Columns(7).Visible = False

            Dim listPK As New System.Collections.Generic.List(Of String)
            listPK.Add("'0'")
            For Each IdPk As String In Me.SetnGetSelectedItemFilter
                listPK.Add("'" & IdPk.ToString & "'")
            Next

            Me.GridMSUserView.DataSource = Sahassa.AML.Commonly.GetDatasetPaging(Me.Tables, Me.PK, Me.SetnGetSort, 0, Int32.MaxValue, Me.Fields, "CustomerInformation_WebTempTable.CIFNo IN (" & String.Join(",", listPK.ToArray) & ")", "")
            Me.GridMSUserView.AllowPaging = False
            Me.GridMSUserView.DataBind()

            'Sembunyikan kolom ke 0,1,4 & 5 agar tidak ikut diekspor ke excel
            Me.GridMSUserView.Columns(0).Visible = False
            Me.GridMSUserView.Columns(7).Visible = False
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    ''' <summary>
    ''' export button 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Me.CollectSelected()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelected()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=CustomerInformationView.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridMSUserView)
            GridMSUserView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get item bound
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridMSUserView.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Dim chkBox As CheckBox = e.Item.FindControl("CheckBoxExporttoExcel")
                chkBox.Checked = Me.SetnGetSelectedItemFilter.Contains(e.Item.Cells(2).Text)
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' select all
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItemFilter
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(2).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItemFilter.Contains(StrKey) = False Then
                        ArrTarget.Add(StrKey)
                    End If
                Else
                    If Me.SetnGetSelectedItemFilter.Contains(StrKey) Then
                        ArrTarget.Remove(StrKey)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItemFilter = ArrTarget
        Filterdata()
    End Sub

    Private Sub BindExportAll()
        Try
            Dim oTableCustomerInformation As New Data.DataTable
            Dim oRowCustomerInformation As Data.DataRow
            Dim TblExportAccountInformation As New Data.DataTable

            oTableCustomerInformation.Columns.Add("WorkingUnitName", GetType(String))
            oTableCustomerInformation.Columns.Add("CIFNo", GetType(String))
            oTableCustomerInformation.Columns.Add("Name", GetType(String))
            oTableCustomerInformation.Columns.Add("DateOfBirth", GetType(DateTime))
            oTableCustomerInformation.Columns.Add("OpeningDate", GetType(DateTime))
            oTableCustomerInformation.Columns.Add("IsCustomerInList", GetType(String))
            oTableCustomerInformation.Columns.Item("IsCustomerInList").Caption = "VerificationList?"

            Dim StrFilter As String = ""

            If Me.SearchType Then
                StrFilter = Me.StrAdvanceSearchFilter
            Else
                StrFilter = Me.SetnGetFieldSearch.Replace("-=Search=-", Me.SetnGetValueSearch.Replace("'", "''"))
            End If

            If StrFilter <> "" Then
                StrFilter = StrFilter & " AND (CFMAST.CFINSC IN (SELECT InsiderCodeId FROM GroupInsiderCodeMapping WHERE GroupId= (SELECT UserGroupId FROM [User] WHERE UserId = '" & Sahassa.AML.Commonly.SessionUserId & "')) OR CFMAST.CFINSC='' OR CFMAST.CFINSC IS NULL)"
            Else
                StrFilter = "(CFMAST.CFINSC IN (SELECT InsiderCodeId FROM GroupInsiderCodeMapping WHERE GroupId= (SELECT UserGroupId FROM [User] WHERE UserId = '" & Sahassa.AML.Commonly.SessionUserId & "')) OR CFMAST.CFINSC='' OR CFMAST.CFINSC IS NULL)"
            End If

            TblExportAccountInformation = Sahassa.AML.Commonly.GetDatasetPaging(Me.Tables, Me.PK, Me.SetnGetSort, Me.SetnGetCurrentPage + 1, 65535, Me.Fields, StrFilter, "")

            For Each rowData As Data.DataRow In TblExportAccountInformation.Rows
                oRowCustomerInformation = oTableCustomerInformation.NewRow
                oRowCustomerInformation("WorkingUnitName") = rowData("WorkingUnitName")
                oRowCustomerInformation("CIFNo") = rowData("CIFNo")
                oRowCustomerInformation("Name") = rowData("Name")
                oRowCustomerInformation("DateOfBirth") = rowData("DateOfBirth")
                oRowCustomerInformation("OpeningDate") = rowData("OpeningDate")
                oRowCustomerInformation("IsCustomerInList") = rowData("IsCustomerInList")
                oTableCustomerInformation.Rows.Add(oRowCustomerInformation)
            Next
            Me.GridMSUserView.DataSource = oTableCustomerInformation
            Me.GridMSUserView.AllowPaging = False
            Me.GridMSUserView.DataBind()

            'Sembunyikan kolom ke 0,1,4 & 5 agar tidak ikut diekspor ke excel
            Me.GridMSUserView.Columns(0).Visible = False
            Me.GridMSUserView.Columns(7).Visible = False
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Protected Sub LnkBtnExportAllToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LnkBtnExportAllToExcel.Click
        Try
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindExportAll()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=CustomerInformationViewAll.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridMSUserView)
            GridMSUserView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

#Region "Advance Search"
    Const StrTableName As String = "CustomerInformation_WebTempTable"

    Protected Sub LnkSearchType_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LnkSearchType.Click
        Try
            Me.SearchType = Not Me.SearchType
            Me.ChangeSearchType()

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub ChangeSearchType()
        Me.TblSimpleSearch.Visible = Not Me.SearchType
        Me.TblAdvanceSearch.Visible = Me.SearchType
        Me.TblSearchType.Visible = True

        If Me.SearchType Then
            Me.LnkSearchType.Text = "Simple Search"
        Else
            Me.LnkSearchType.Text = "Advance Search"
        End If
    End Sub

    Sub LoadField()
        Dim ObjDs As Data.DataSet
        Try
            CboFilter.Items.Clear()
            Dim objParam As Object() = {StrTableName}
            ObjDs = DataRepository.Provider.ExecuteDataSet("usp_GetQueryFieldByTableName", objParam)
            'FieldList.DataSource = ObjDs.Tables(0).DefaultView
            'FieldList.DataBind()
            CboFilter.Visible = True
            CboFilter.DataSource = ObjDs.Tables(0).DefaultView
            CboFilter.DataTextField = "Column_name"
            CboFilter.DataTextField = "Column_name"
            CboFilter.AppendDataBoundItems = True
            CboFilter.Items.Add(New System.Web.UI.WebControls.ListItem("[FIELD LIST]", ""))
            CboFilter.DataBind()
        Catch
            Throw
        Finally
            If Not ObjDs Is Nothing Then
                ObjDs.Dispose()
                ObjDs = Nothing
            End If
        End Try

    End Sub

    Protected Sub CboFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CboFilter.SelectedIndexChanged
        Dim objds As Data.DataSet
        Try
            Dim objParam As Object() = {StrTableName}
            objds = DataRepository.Provider.ExecuteDataSet("usp_GetQueryFieldByTableName", objParam)
            Dim ObjView As Data.DataView = objds.Tables(0).DefaultView
            ObjView.RowFilter = "Column_Name='" & CboFilter.SelectedValue & "'"

            If ObjView.Count > 0 Then
                Session("QueryBuilderDinamic.SelectedDataRow") = ObjView.Item(0)
                cboOperator.Visible = True
                Dim strDataType As String = ObjView.Item(0).Item(7)

                Select Case strDataType.ToLower
                    Case "bigint", "int", "smallint", "tinyint", "decimal", "numeric", "money", "float"
                        cboOperator.Items.Clear()
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("[OPERATOR]", "[OPERATOR]"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Equals", "Equals"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Does Not Equal", "Does Not Equal"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is Greater Than", "Is Greater Than"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is Less Than", "Is Less Than"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is Equals or Greater", "Is Equals or Greater"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is Equals or Less", "Is Equals or Less"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is Between", "Is Between"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is Not Between", "Is Not Between"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is Null", "Is Null"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is Not Null", "Is Not Null"))


                    Case "bit"
                        cboOperator.Items.Clear()
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("[OPERATOR]", "[OPERATOR]"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is True", "Is True"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is False", "Is False"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is Null", "Is Null"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is Not Null", "Is Not Null"))

                    Case "datetime"
                        cboOperator.Items.Clear()
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("[OPERATOR]", "[OPERATOR]"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Equals", "Equals"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Does Not Equal", "Does Not Equal"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is Between", "Is Between"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is Not Between", "Is Not Between"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is Equals or Greater", "Is Equals or Greater"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is Equals or Less", "Is Equals or Less"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is Greater Than", "Is Greater Than"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is Less Than", "Is Less Than"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is Null", "Is Null"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is Not Null", "Is Not Null"))

                    Case "varchar", "nvarchar", "char"
                        cboOperator.Items.Clear()
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("[OPERATOR]", "[OPERATOR]"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Equals", "Equals"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Does Not Equal", "Does Not Equal"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Contains", "Contains"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Start With", "Start With"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Ends With", "Ends With"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is Null", "Is Null"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is Not Null", "Is Not Null"))

                    Case Else
                        cboOperator.Items.Clear()
                        cboOperator.Items.Add(New ListItem("[OPERATOR]", "[OPERATOR]"))
                        cboOperator.Items.Add(New ListItem("Equals", "Equals"))
                        cboOperator.Items.Add(New ListItem("Does Not Equal", "Does Not Equal"))
                        cboOperator.Items.Add(New ListItem("Contains", "Contains"))
                        cboOperator.Items.Add(New ListItem("Start With", "Start With"))
                        cboOperator.Items.Add(New ListItem("Ends With", "Ends With"))
                        cboOperator.Items.Add(New ListItem("Is Null", "Is Null"))
                        cboOperator.Items.Add(New ListItem("Is Not Null", "Is Not Null"))

                End Select

            End If
            cboOperator_SelectedIndexChanged(Nothing, Nothing)

        Catch ex As Exception

            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not objds Is Nothing Then
                objds.Dispose()
                objds = Nothing
            End If

        End Try
    End Sub

    Protected Sub cboOperator_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboOperator.SelectedIndexChanged
        Try
            If Not SelectedDataRow Is Nothing Then

                Dim strDataType As String = SelectedDataRow.Item(7)
                If Not strDataType.ToLower.Contains("datetime") Then
                    popUp1.Visible = False
                    popUp2.Visible = False
                Else
                    popUp1.Visible = True
                    popUp2.Visible = True
                End If

                TxtFieldAwal.Text = ""
                TxtFieldAkhir.Text = ""
                Select Case cboOperator.SelectedValue.ToLower
                    Case "[OPERATOR]".ToLower
                        PanelAwal.Visible = False
                        PanelAnd.Visible = False
                        PanelAkhir.Visible = False
                        ImageAddFilter.Visible = False
                    Case "Equals".ToLower, "does not equal".ToLower, "Is Greater Than".ToLower, "Is Less Than".ToLower, "Is Equals or Greater".ToLower, "Is Equals or Less".ToLower, "Contains".ToLower, "Start With".ToLower, "Ends With".ToLower
                        PanelAwal.Visible = True
                        PanelAnd.Visible = False
                        PanelAkhir.Visible = False
                        If strDataType.ToLower.Contains("datetime") Then
                            popUp1.Visible = True
                            popUp2.Visible = False
                        End If
                        ImageAddFilter.Visible = True
                    Case "Is Between".ToLower, "Is Not Between".ToLower
                        PanelAwal.Visible = True
                        PanelAnd.Visible = True
                        PanelAkhir.Visible = True
                        If strDataType.ToLower.Contains("datetime") Then
                            popUp1.Visible = True
                            popUp2.Visible = True
                        End If
                        ImageAddFilter.Visible = True
                    Case "Is Null".ToLower, "Is Not Null".ToLower, "Is True".ToLower
                        PanelAwal.Visible = False
                        PanelAnd.Visible = False
                        PanelAkhir.Visible = False
                        ImageAddFilter.Visible = True
                End Select
            End If
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        Finally

        End Try
    End Sub

    Public ReadOnly Property ObjDataFilter() As DataTable
        Get
            If Session("QueryBuilderDinamic.ObjDataFilter") Is Nothing Then
                Dim objData As New DataTable
                Dim ObjCol As New DataColumn("PK", GetType(Integer))
                ObjCol.AutoIncrement = True
                ObjCol.AutoIncrementSeed = 1
                ObjCol.AutoIncrementStep = 1
                objData.Columns.Add(ObjCol)
                objData.PrimaryKey = New DataColumn() {ObjCol}
                objData.Columns.Add("AndOr", GetType(String))
                objData.Columns.Add("ColumnName", GetType(String))
                objData.Columns.Add("Operator", GetType(String))
                objData.Columns.Add("nilai", GetType(String))
                Session("QueryBuilderDinamic.ObjDataFilter") = objData
                Return CType(Session("QueryBuilderDinamic.ObjDataFilter"), DataTable)
            Else
                Return CType(Session("QueryBuilderDinamic.ObjDataFilter"), DataTable)
            End If
        End Get
    End Property

    Public ReadOnly Property SelectedDataRow() As DataRowView
        Get
            Return CType(Session("QueryBuilderDinamic.SelectedDataRow"), DataRowView)
        End Get
    End Property

    Protected Sub ImageAddFilter_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAddFilter.Click
        Try
            If CboFilter.SelectedValue = "" Then
                Exit Sub
            End If
            Dim ObjRow As DataRow = ObjDataFilter.NewRow
            If ObjDataFilter.Rows.Count > 0 Then
                ObjRow("AndOr") = CboAndOR.SelectedValue
            Else
                ObjRow("AndOr") = ""
            End If
            ObjRow("ColumnName") = CboFilter.SelectedValue
            ObjRow("Operator") = cboOperator.SelectedValue
            Select Case cboOperator.SelectedValue.ToLower
                Case "Equals".ToLower, "does not equal".ToLower, "Is Greater Than".ToLower, "Is Less Than".ToLower, "Is Equals or Greater".ToLower, "Is Equals or Less".ToLower, "Contains".ToLower, "Start With".ToLower, "Ends With".ToLower
                    If SelectedDataRow.Item(7).ToString.ToLower.Contains("datetime") Then
                        If Sahassa.AML.Commonly.IsDateValid("dd-MM-yyyy", TxtFieldAwal.Text.Trim) Then
                            ObjRow("nilai") = TxtFieldAwal.Text.Trim
                        Else
                            cvalPageError.IsValid = False
                            cvalPageError.ErrorMessage = "Date is not in valid format."
                            Exit Sub
                        End If
                    Else
                        ObjRow("nilai") = TxtFieldAwal.Text.Trim
                    End If
                Case "Is Between".ToLower, "Is Not Between".ToLower
                    If SelectedDataRow.Item(7).ToString.ToLower.Contains("datetime") Then
                        If Not Sahassa.AML.Commonly.IsDateValid("dd-MM-yyyy", TxtFieldAwal.Text.Trim) Then
                            cvalPageError.IsValid = False
                            cvalPageError.ErrorMessage = "Begin Date is not in valid format."
                            Exit Sub
                        End If

                        If Not Sahassa.AML.Commonly.IsDateValid("dd-MM-yyyy", TxtFieldAkhir.Text.Trim) Then
                            cvalPageError.IsValid = False
                            cvalPageError.ErrorMessage = "End Date is not in valid format."
                            Exit Sub
                        End If
                        ObjRow("nilai") = TxtFieldAwal.Text.Trim & " and " & TxtFieldAkhir.Text.Trim
                    Else
                        ObjRow("nilai") = TxtFieldAwal.Text.Trim & " and " & TxtFieldAkhir.Text.Trim
                    End If
                Case "Is Null".ToLower, "Is Not Null".ToLower, "Is True".ToLower
                    ObjRow("nilai") = ""
            End Select

            ObjDataFilter.Rows.Add(ObjRow)
            If ObjDataFilter.Rows.Count > 0 Then
                PanelAndOr.Visible = True
            Else
                PanelAndOr.Visible = False
            End If
            GridViewList.DataSource = ObjDataFilter
            GridViewList.DataBind()
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridViewList_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GridViewList.RowDeleting
        Try
            Dim Pk As Integer = GridViewList.DataKeys(e.RowIndex).Value.ToString

            Dim ObjRow As DataRow = ObjDataFilter.Rows.Find(Pk)
            ObjDataFilter.Rows.Remove(ObjRow)
            If ObjDataFilter.Rows.Count = 0 Then
                PanelAndOr.Visible = False
            ElseIf ObjDataFilter.Rows.Count > 0 Then
                ObjRow = ObjDataFilter.Rows(0)
                ObjRow("AndOr") = ""
            End If
            GridViewList.DataSource = ObjDataFilter
            GridViewList.DataBind()

        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImgAdvanceSearch_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgAdvanceSearch.Click
        Try
            Dim strWhere As String = ""
            Dim ObjDs As Data.DataSet
            Dim objParam As Object() = {StrTableName}
            ObjDs = DataRepository.Provider.ExecuteDataSet("usp_GetQueryFieldByTableName", objParam)
            Dim objView As DataView = ObjDs.Tables(0).DefaultView

            Dim strType As String = ""
            For Each objrow As DataRow In ObjDataFilter.Rows
                strWhere &= " " & objrow("andor") & " (" & StrTableName & "." & objrow("ColumnName")
                objView.RowFilter = " column_name='" & objrow("ColumnName") & "'"
                strType = objView.Item(0).Item(7)
                Select Case objrow("Operator").ToString.ToLower
                    Case "Equals".ToLower
                        Select Case strType.ToLower
                            Case "bigint", "int", "smallint", "tinyint", "decimal", "numeric", "money", "float"
                                strWhere &= " = " & objrow("nilai") & " )"
                            Case "datetime"
                                strWhere &= " between '" & Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", objrow("nilai")).ToString("yyyy-MM-dd") & " 00:00:00' and '" & Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", objrow("nilai")).ToString("yyyy-MM-dd") & " 23:59:59' )"
                            Case "varchar", "nvarchar", "char"
                                strWhere &= " ='" & formatstring(objrow("nilai").ToString) & "' )"
                            Case Else
                                strWhere &= " = " & objrow("nilai") & " )"
                        End Select
                    Case "Does Not Equal".ToLower
                        Select Case strType.ToLower
                            Case "bigint", "int", "smallint", "tinyint", "decimal", "numeric", "money", "float"
                                strWhere &= " <> " & objrow("nilai") & " )"
                            Case "datetime"
                                strWhere &= " not between  '" & Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", objrow("nilai")).ToString("yyyy-MM-dd") & " 00:00:00' and '" & Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", objrow("nilai")).ToString("yyyy-MM-dd") & " 23:59:59' )"
                            Case "varchar", "nvarchar", "char"
                                strWhere &= "<>'" & formatstring(objrow("nilai").ToString) & "' )"
                            Case Else
                                strWhere &= " <> " & objrow("nilai") & " )"
                        End Select
                    Case "Is Greater Than".ToLower
                        Select Case strType.ToLower
                            Case "bigint", "int", "smallint", "tinyint", "decimal", "numeric", "money", "float"
                                strWhere &= " > " & objrow("nilai") & " )"
                            Case "datetime"
                                strWhere &= " >'" & Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", objrow("nilai")).ToString("yyyy-MM-dd") & " 00:00:00'" & " )"
                            Case Else
                                strWhere &= " > " & objrow("nilai") & " )"
                        End Select
                    Case "Is Less Than".ToLower
                        Select Case strType.ToLower
                            Case "bigint", "int", "smallint", "tinyint", "decimal", "numeric", "money", "float"
                                strWhere &= " < " & objrow("nilai") & " )"
                            Case "datetime"
                                strWhere &= " <'" & Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", objrow("nilai")).ToString("yyyy-MM-dd") & " 00:00:00'" & " )"
                            Case Else
                                strWhere &= " < " & objrow("nilai") & " )"
                        End Select

                    Case "Is Equals or Greater".ToLower
                        Select Case strType.ToLower
                            Case "bigint", "int", "smallint", "tinyint", "decimal", "numeric", "money", "float"
                                strWhere &= " >= " & objrow("nilai") & " )"
                            Case "datetime"
                                strWhere &= " >= '" & Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", objrow("nilai")).ToString("yyyy-MM-dd") & " 00:00:00'" & " )"

                            Case Else
                                strWhere &= " >= " & objrow("nilai") & " )"
                        End Select
                    Case "Is Equals or Less".ToLower
                        Select Case strType.ToLower
                            Case "bigint", "int", "smallint", "tinyint", "decimal", "numeric", "money", "float"
                                strWhere &= " <= " & objrow("nilai") & " )"

                            Case "datetime"
                                strWhere &= " <= '" & Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", objrow("nilai")).ToString("yyyy-MM-dd") & " 23:59:59'" & " )"
                            Case Else
                                strWhere &= " <= " & objrow("nilai") & " )"
                        End Select
                    Case "Is Between".ToLower
                        Select Case strType.ToLower
                            Case "bigint", "int", "smallint", "tinyint", "decimal", "numeric", "money", "float"
                                strWhere &= " between " & objrow("nilai") & " )"

                            Case "datetime"

                                'Dim arrtemp() As String = objrow("nilai").ToString.ToLower.Split("and")
                                Dim indexand As Integer = objrow("nilai").ToString.ToLower.IndexOf("and")

                                Dim date1 As Date = Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", objrow("nilai").ToString.ToLower.Substring(0, indexand - 1).Trim)
                                Dim date2 As Date = Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", objrow("nilai").ToString.ToLower.Substring(indexand + 3).Trim)

                                strWhere &= " between '" & date1.ToString("yyyy-MM-dd") & " 00:00:00' and '" & date2.ToString("yyyy-MM-dd") & " 23:59:59'" & " )"
                            Case Else
                                strWhere &= " between " & objrow("nilai") & " )"
                        End Select
                    Case "Is Not Between".ToLower
                        Select Case strType.ToLower
                            Case "bigint", "int", "smallint", "tinyint", "decimal", "numeric", "money", "float"
                                strWhere &= " not between " & objrow("nilai") & " )"

                            Case "datetime"
                                Dim indexand As Integer = objrow("nilai").ToString.ToLower.IndexOf("and")

                                Dim date1 As Date = Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", objrow("nilai").ToString.ToLower.Substring(0, indexand - 1).Trim)
                                Dim date2 As Date = Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", objrow("nilai").ToString.ToLower.Substring(indexand + 3).Trim)

                                strWhere &= " not between '" & date1.ToString("yyyy-MM-dd") & " 00:00:00' and '" & date2.ToString("yyyy-MM-dd") & " 23:59:59'" & " )"
                            Case Else
                                strWhere &= " not between " & objrow("nilai") & " )"
                        End Select
                    Case "Is Null".ToLower
                        Select Case strType.ToLower
                            Case "bigint", "int", "smallint", "tinyint", "decimal", "numeric", "money", "float"
                                strWhere &= " is null " & " )"
                            Case "bit"
                                strWhere &= " is null " & " )"
                            Case "varchar", "nvarchar", "char"
                                strWhere &= " is null " & " )"
                            Case "datetime"
                                strWhere &= " is null" & " )"
                            Case Else
                                strWhere &= " is null " & " )"
                        End Select
                    Case "Is Not Null".ToLower
                        Select Case strType.ToLower
                            Case "bigint", "int", "smallint", "tinyint", "decimal", "numeric", "money", "float"
                                strWhere &= " is not null " & " )"
                            Case "bit"
                                strWhere &= " is not null " & " )"
                            Case "varchar", "nvarchar", "char"
                                strWhere &= " is not null " & " )"
                            Case "datetime"
                                strWhere &= " is not null " & " )"
                            Case Else
                                strWhere &= " is not null " & " )"
                        End Select
                    Case "Is True".ToLower
                        Select Case strType.ToLower
                            Case "bit"
                                strWhere &= " =1" & " )"
                            Case Else
                                strWhere &= " =1" & " )"
                        End Select
                    Case "Is False".ToLower
                        Select Case strType.ToLower
                            Case "bit"
                                strWhere &= " =0" & " )"
                            Case Else
                                strWhere &= " =0" & " )"
                        End Select
                    Case "Contains".ToLower
                        Select Case strType.ToLower
                            Case "varchar", "nvarchar", "char"
                                strWhere &= " like '%" & objrow("nilai") & "%'" & " )"
                            Case Else
                                strWhere &= " like '%" & objrow("nilai") & "%'" & " )"
                        End Select
                    Case "Start With".ToLower
                        Select Case strType.ToLower
                            Case "varchar", "nvarchar", "char"
                                strWhere &= " like '" & objrow("nilai") & "%'" & " )"
                            Case Else
                                strWhere &= " like '" & objrow("nilai") & "%'" & " )"
                        End Select
                    Case "Ends With".ToLower
                        Select Case strType.ToLower
                            Case "varchar", "nvarchar", "char"
                                strWhere &= " like '%" & objrow("nilai") & "'" & " )"
                            Case Else
                                strWhere &= " like '%" & objrow("nilai") & "'" & " )"
                        End Select
                End Select
            Next

            Me.StrAdvanceSearchFilter = strWhere
            Me.SetnGetCurrentPage = 0
            Filterdata()
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub BindGridAdvanceSearch()
        Dim StrWhere As String = Me.StrAdvanceSearchFilter

        If StrWhere <> "" Then
            StrWhere = StrWhere & " AND (CFMAST.CFINSC IN (SELECT InsiderCodeId FROM GroupInsiderCodeMapping WHERE GroupId= (SELECT UserGroupId FROM [User] WHERE UserId = '" & Sahassa.AML.Commonly.SessionUserId & "')) OR CFMAST.CFINSC='' OR CFMAST.CFINSC IS NULL)"
        Else
            StrWhere = "(CFMAST.CFINSC IN (SELECT InsiderCodeId FROM GroupInsiderCodeMapping WHERE GroupId= (SELECT UserGroupId FROM [User] WHERE UserId = '" & Sahassa.AML.Commonly.SessionUserId & "')) OR CFMAST.CFINSC='' OR CFMAST.CFINSC IS NULL)"
        End If

        Me.SetnGetBindTable = Sahassa.AML.Commonly.GetDatasetPaging(Me.Tables, Me.PK, Me.SetnGetSort, Me.SetnGetCurrentPage + 1, Me.PageSize, Me.Fields, StrWhere, "")
        Me.GridMSUserView.DataSource = Me.SetnGetBindTable

        Using AccessCustomerInformation As New AMLDAL.CustomerInformationTableAdapters.GetCountCustomerInformationFilterTableAdapter
            Dim TotalRow As Int64
            TotalRow = Sahassa.AML.Commonly.GetTableCount(Me.Tables, StrWhere)
            'TotalRow = AccessCustomerInformation.Fill("WHERE " & StrFilter)
            Me.SetnGetRowTotal = TotalRow
        End Using
        Me.GridMSUserView.VirtualItemCount = Me.SetnGetRowTotal
        Me.GridMSUserView.DataBind()
    End Sub

    Function formatstring(ByVal strData As String) As String
        If strData.Length > 0 Then
            If Left(strData, 1) = "'" Then
                strData = Right(strData, strData.Length - 1)
            End If
            If Right(strData, 1) = "'" Then
                strData = Left(strData, strData.Length - 1)
            End If
            strData = strData.Replace("'", "''")
        End If
        Return strData
    End Function

#End Region

End Class
