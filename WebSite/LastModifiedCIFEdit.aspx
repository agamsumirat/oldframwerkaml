<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="LastModifiedCIFEdit.aspx.vb" Inherits="LastModifiedCIFEdit"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
    <table id="title"  border="2" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr>
            <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>CDD Updated&nbsp;
                    <hr />
                </strong>
                <asp:Label ID="LblSuccess" runat="server" CssClass="validationok" Visible="False"
                    Width="100%"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" colspan="4" style="border-top-style: none; border-right-style: none;
                border-left-style: none; height: 24px; border-bottom-style: none"><span style="color: #ff0000">* Required</span></td>
        </tr>
    </table>	
	<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2" height="72" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
		<tr class="formText">
			<td bgColor="#ffffff" style="width: 24px; height: 24px"><br />
                </td>
			<td width="20%" bgColor="#ffffff" style="height: 24px">
                CIF No</td>
			<td width="5" bgColor="#ffffff" style="height: 24px">:</td>
			<td width="80%" bgColor="#ffffff" style="height: 24px">
                &nbsp;<asp:LinkButton 
                    ID="LblCif" runat="server"></asp:LinkButton>
            </td>
		</tr>
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
            </td>
            <td bgcolor="#ffffff" style="height: 24px" width="20%">
                Name</td>
            <td bgcolor="#ffffff" style="height: 24px" width="5">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px" width="80%">
                <asp:Label ID="LblName" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
                &nbsp;</td>
            <td bgcolor="#ffffff" style="height: 24px" width="20%">
                Data Updating Reason</td>
            <td bgcolor="#ffffff" style="height: 24px" width="5">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px" width="80%">
                <asp:Label ID="LblDataupdatingReason" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
                &nbsp;</td>
            <td bgcolor="#ffffff" style="height: 24px" width="20%">
                Date Open CIF</td>
            <td bgcolor="#ffffff" style="height: 24px" width="5">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px" width="80%">
                <asp:Label ID="LblDateOpenCIF" runat="server"></asp:Label></td>
        </tr>
		<tr class="formText">
			<td bgColor="#ffffff" style="width: 24px; height: 24px;">
               
                &nbsp;</td>
			<td bgColor="#ffffff" style="height: 24px">
                Date Of CDD Updated&nbsp;</td>
            <td bgcolor="#ffffff" style="height: 24px">
                :
            </td>
            <td bgcolor="#ffffff" style="height: 24px">
                <asp:TextBox ID="TxtlastModified" runat="server" CssClass="textbox"></asp:TextBox>
                <input id="popUp" runat="server" name="popUpCalc" style="border-right: #ffffff 0px solid;
                    border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
                    border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
                    height: 17px" title="Click to show calendar" type="button" /></td>
		</tr>
		<tr class="formText">
			<td bgColor="#ffffff" style="width: 24px; height: 24px;">
               
                &nbsp;</td>
			<td bgColor="#ffffff" style="height: 24px">
                &nbsp;</td>
            <td bgcolor="#ffffff" style="height: 24px">
                &nbsp;</td>
            <td bgcolor="#ffffff" style="height: 24px">
                &nbsp;</td>
		</tr>
		<tr class="formText">
			<td bgColor="#ffffff" style="width: 24px; height: 24px;">
               
                &nbsp;</td>
			<td bgColor="#ffffff" style="height: 24px" colspan="3">
                <strong>Data Mandatory /Incomplate Blank</strong></td>
		</tr>
		<tr class="formText">
			<td bgColor="#ffffff" style="width: 24px; height: 24px;">
               
                &nbsp;</td>
			<td bgColor="#ffffff" style="height: 24px" colspan="3">
                <asp:DataList ID="DataListMandatory" runat="server" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Vertical">
                    <AlternatingItemStyle BackColor="White" />
                    <FooterStyle BackColor="#CCCC99" />
                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                     
                    <HeaderTemplate>
                        Mandatory Field
                    </HeaderTemplate>
                     
                    <ItemStyle BackColor="#F7F7DE" />
                    <ItemTemplate>
                        <asp:Label ID="Label1" Text='<%# Eval("ValidationMessage") %>' runat="server"></asp:Label>
                    </ItemTemplate>
                    <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                </asp:DataList>
            </td>
		</tr>
		<tr class="formText">
			<td bgColor="#ffffff" style="width: 24px; height: 24px;">
               
                &nbsp;</td>
			<td bgColor="#ffffff" style="height: 24px">
                &nbsp;</td>
            <td bgcolor="#ffffff" style="height: 24px">
                &nbsp;</td>
            <td bgcolor="#ffffff" style="height: 24px">
                &nbsp;</td>
		</tr>
		<tr class="formText">
			<td bgColor="#ffffff" style="width: 24px; height: 24px;">
               
                &nbsp;</td>
			<td bgColor="#ffffff" style="height: 24px" colspan="3">
                <strong>Data Incorrect</strong></td>
		</tr>
		<tr class="formText">
			<td bgColor="#ffffff" style="width: 24px; height: 24px;">
               
                &nbsp;</td>
			<td bgColor="#ffffff" style="height: 24px" colspan="3">
                <asp:GridView ID="GrdInvalid" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" EnableModelValidation="True" ForeColor="Black" GridLines="Vertical">
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:BoundField DataField="ValidationMessage" HeaderText="Validation Rule" />
                        <asp:BoundField DataField="FieldValue" HeaderText="Data" />
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                    <RowStyle BackColor="#F7F7DE" />
                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                </asp:GridView>
            </td>
		</tr>
		<tr class="formText" bgColor="#dddddd" height="30">
			<td style="width: 24px"><IMG height="15" src="images/arrow.gif" width="15"></td>
			<td colSpan="3" style="height: 9px">
				<table cellSpacing="0" cellPadding="3" border="0">
					<tr>
						<td><asp:imagebutton id="ImageSave" runat="server" CausesValidation="True" SkinID="AddButton"></asp:imagebutton></td>
						<td><asp:imagebutton id="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton"></asp:imagebutton></td>
					</tr>
				</table>               
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
		</tr>
	</table>
	
</asp:Content>


