﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="CDDLNPMappingQuestionEdit.aspx.vb" Inherits="CDDLNPMappingQuestionEdit" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">

    <script type="text/javascript" language="javascript">
    function OpenDialogCDDQuestion() 
        {
            window.showModalDialog("PopupCDDQuestion.aspx","#1","dialogHeight: 650px; dialogWidth: 650px; dialogTop: 190px; dialogLeft: 220px; edge: Raised; center: Yes; help: No; resizable: No; status: No; scrollbar: Yes;")
        }
    </script>

    <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
        height="72" style="border-top-style: none; border-right-style: none; border-left-style: none;
        border-bottom-style: none" width="100%">
        <tr>
            <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>
                    <img height="17" src="Images/dot_title.gif" width="17" />
                    <asp:Label ID="lblHeader" runat="server" Text="EDD Mapping Question - Edit"></asp:Label>&nbsp;
                    <hr />
                </strong>
            </td>
        </tr>
    </table>
    <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
        border="2" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none">
        <tr class="formText" height="20px">
            <td bgcolor="#ffffff" width="5px">
                &nbsp;</td>
            <td bgcolor="#ffffff" width="20%">
                CDD Type</td>
            <td bgcolor="#ffffff" width="5px">
                :</td>
            <td bgcolor="#ffffff" colspan="2" width="80%">
                <asp:Label ID="lblCDDType" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText" height="20px">
            <td bgcolor="#ffffff">
                &nbsp;</td>
            <td bgcolor="#ffffff">
                CDD Nasabah Type</td>
            <td bgcolor="#ffffff">
                :</td>
            <td bgcolor="#ffffff" colspan="2">
                <asp:Label ID="lblCDDNasabahType" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText" height="20px">
            <td bgcolor="#ffffff" colspan="5">
                &nbsp;</td>
        </tr>
        <tr class="formText" height="20px">
            <td bgcolor="#ffffff">
                &nbsp;</td>
            <td bgcolor="#ffffff">
                CDD Bagian</td>
            <td bgcolor="#ffffff">
                :</td>
            <td bgcolor="#ffffff" colspan="2">
                <asp:DropDownList ID="ddlCDDBagian" runat="server" CssClass="combobox">
                </asp:DropDownList></td>
        </tr>
        <tr class="formText" height="20px">
            <td bgcolor="#ffffff">
                &nbsp;</td>
            <td bgcolor="#ffffff">
                CDD Question</td>
            <td bgcolor="#ffffff">
                :</td>
            <td bgcolor="#ffffff" colspan="2">
                <asp:TextBox ID="txtParentQuestion" runat="server" CssClass="textbox" Width="200px"></asp:TextBox>
                <asp:ImageButton ID="ImageBrowse" runat="server" CausesValidation="True" ImageUrl="~/Images/Button/Browse.gif"
                    OnClientClick="javascript:OpenDialogCDDQuestion();" />
                <asp:HiddenField ID="hfQuestionID" runat="server" />
            </td>
        </tr>
        <tr class="formText" height="20px">
            <td bgcolor="#ffffff">
                &nbsp;</td>
            <td bgcolor="#ffffff">
                &nbsp;</td>
            <td bgcolor="#ffffff">
                &nbsp;</td>
            <td bgcolor="#ffffff" colspan="2">
                <asp:ImageButton ID="ImageAdd" runat="server" CausesValidation="True" ImageUrl="~/Images/Button/Add.gif" /></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" width="5px">
                &nbsp;</td>
            <td bgcolor="#ffffff" colspan="4">
                <table width="100%">
                    <tr>
                        <td colspan="2">
                            <ajax:AjaxPanel ID="AjaxPanel1" runat="server">
                                <asp:DataGrid ID="GridViewCDDQuestionDetail" runat="server" AutoGenerateColumns="False"
                                    Font-Size="XX-Small" BackColor="White" CellPadding="4" Width="100%" GridLines="Vertical"
                                    BorderColor="#DEDFDE" ForeColor="Black" AllowPaging="True">
                                    <FooterStyle BackColor="#CCCC99" />
                                    <AlternatingItemStyle BackColor="White" />
                                    <ItemStyle BackColor="#F7F7DE" />
                                    <HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#6B696B" />
                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Visible="false" />
                                    <Columns>
                                        <asp:BoundColumn DataField="FK_MappingSettingCDDBagian_ID" Visible="False" />
                                        <asp:BoundColumn HeaderText="CDD Bagian Name">
                                            <ItemStyle Width="25%" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="FK_CDD_Question_ID" Visible="False" />
                                        <asp:BoundColumn HeaderText="CDD Question">
                                            <ItemStyle Width="70%" />
                                        </asp:BoundColumn>
                                        <asp:TemplateColumn>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="LnkDelete" runat="server" CausesValidation="false" CommandName="Delete"
                                                    Text="Delete"></asp:LinkButton>
                                            </ItemTemplate>
                                            <HeaderStyle Width="5%" />
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </ajax:AjaxPanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" width="5">
                &nbsp;</td>
            <td bgcolor="#ffffff" colspan="4">
                <ajax:AjaxPanel ID="AjaxPanel2" runat="server">
                    <table id="Table4" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                        bgcolor="#ffffff" border="2">
                        <tr>
                            <td class="regtext" valign="middle" align="left" bgcolor="#ffffff" style="width: 150px;
                                height: 15px">
                                Page&nbsp;<asp:Label ID="PageCurrentPage" runat="server" CssClass="regtext">0</asp:Label>&nbsp;of
                                <asp:Label ID="PageTotalPages" runat="server" CssClass="regtext">0</asp:Label>
                                (<asp:Label ID="PageTotalRows" runat="server">0</asp:Label>
                                Records)</td>
                            <td class="regtext" valign="middle" align="left" width="5" bgcolor="#ffffff" style="height: 15px">
                                <font face="Verdana, Arial, Helvetica, sans-serif" size="1"></font>
                            </td>
                            <td class="regtext" valign="middle" align="left" bgcolor="#ffffff" style="height: 15px">
                            </td>
                            <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff" style="height: 15px">
                                <img height="5" src="images/first.gif" width="6">
                            </td>
                            <td class="regtext" valign="middle" align="right" width="50" bgcolor="#ffffff" style="height: 15px">
                                <asp:LinkButton ID="LinkButtonFirst" runat="server" CssClass="regtext" CommandName="First"
                                    OnCommand="PageNavigate">First</asp:LinkButton>
                            </td>
                            <td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff" style="height: 15px">
                                <img height="5" src="images/prev.gif" width="6"></td>
                            <td class="regtext" valign="middle" align="right" width="50" bgcolor="#ffffff" style="height: 15px">
                                <asp:LinkButton ID="LinkButtonPrevious" runat="server" CssClass="regtext" CommandName="Prev"
                                    OnCommand="PageNavigate">Previous</asp:LinkButton>
                            </td>
                            <td class="regtext" valign="middle" align="right" width="50" bgcolor="#ffffff" style="height: 15px">
                                <asp:LinkButton ID="LinkButtonNext" runat="server" CssClass="regtext" CommandName="Next"
                                    OnCommand="PageNavigate">Next</asp:LinkButton></td>
                            <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff" style="height: 15px">
                                <img height="5" src="images/next.gif" width="6"></td>
                            <td class="regtext" valign="middle" align="left" width="50" bgcolor="#ffffff" style="height: 15px">
                                <asp:LinkButton ID="LinkButtonLast" runat="server" CssClass="regtext" CommandName="Last"
                                    OnCommand="PageNavigate">Last</asp:LinkButton>
                            </td>
                            <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff" style="height: 15px">
                                <img height="5" src="images/last.gif" width="6"></td>
                        </tr>
                    </table>
                </ajax:AjaxPanel>
            </td>
        </tr>
        <tr class="formText" bgcolor="#dddddd" height="30">
            <td style="width: 5px">
                <img height="15" src="images/arrow.gif" width="15"></td>
            <td colspan="6" style="height: 9px">
                <table cellspacing="0" cellpadding="3" border="0">
                    <tr>
                        <td>
                            <ajax:AjaxPanel ID="AjaxPanel7" runat="server">
                                <asp:ImageButton ID="ImageSave" runat="server" CausesValidation="True" ImageUrl="~/Images/Button/Save.gif">
                                </asp:ImageButton></ajax:AjaxPanel></td>
                        <td>
                            <ajax:AjaxPanel ID="AjaxPanel8" runat="server">
                                <asp:ImageButton ID="ImageCancel" runat="server" ImageUrl="~/Images/Button/Cancel.gif">
                                </asp:ImageButton></ajax:AjaxPanel></td>
                    </tr>
                </table>
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
        </tr>
    </table>
</asp:Content>
