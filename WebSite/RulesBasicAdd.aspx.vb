Imports System.Data.SqlClient
Imports Sahassa.AML.TableAdapterHelper
Partial Class RulesBasicAdd
    Inherits Parent

    Private StrCaseGroupingBy As String
    Private StrRulesBasicExpression As String

    Protected Sub TransactionFrequencyCheckBox_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TransactionFrequencyCheckBox.CheckedChanged
        Me.TransactionFrequencyTextBox.Enabled = TransactionFrequencyCheckBox.Checked
    End Sub

    Protected Sub Menu1_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs) Handles Menu1.MenuItemClick
        Me.MultiView1.ActiveViewIndex = Integer.Parse(e.Item.Value)
    End Sub

    Protected Sub TransactionPeriodCheckBox_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TransactionPeriodCheckBox.CheckedChanged
        Me.TransactionPeriodTextBox.Enabled = TransactionPeriodCheckBox.Checked
        Me.TransactionPeriodTypeDropDownList.Enabled = TransactionPeriodCheckBox.Checked
    End Sub

    Protected Sub TransactionAmountFromDropDownList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TransactionAmountFromDropDownList.SelectedIndexChanged
        Me.TransactionAmountFromTextBox.Visible = (TransactionAmountFromDropDownList.SelectedIndex = 1)
    End Sub

    Protected Sub TransactionAmountUntilDropDownList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TransactionAmountUntilDropDownList.SelectedIndexChanged
        Me.TransactionAmountUntilTextBox.Visible = (TransactionAmountUntilDropDownList.SelectedIndex = 1)
    End Sub

    Private Function LoadNegativeList() As Boolean
        Using VerificationListCategoryAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.VerificationListCategoryTableAdapter
            Using VerificationListCategoryTable As New AMLDAL.BasicRulesDataSet.VerificationListCategoryDataTable
                VerificationListCategoryAdapter.Fill(VerificationListCategoryTable)
                Me.SourceNegativeListCheckBoxList.DataSource = VerificationListCategoryTable
                Me.SourceNegativeListCheckBoxList.DataTextField = "CategoryName"
                Me.SourceNegativeListCheckBoxList.DataValueField = "CategoryID"
                Me.SourceNegativeListCheckBoxList.DataBind()
                'Me.DestinationNegativeListCheckBoxList.DataSource = VerificationListCategoryTable
                'Me.DestinationNegativeListCheckBoxList.DataTextField = "CategoryName"
                'Me.DestinationNegativeListCheckBoxList.DataValueField = "CategoryID"
                'Me.DestinationNegativeListCheckBoxList.DataBind()
            End Using

        End Using
    End Function

    Private Function LoadCustomerSubType() As Boolean
        Using CustomerSubTypeAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.CustomerSubTypeTableAdapter
            Using CustomerSubTypeTable As New AMLDAL.BasicRulesDataSet.CustomerSubTypeDataTable
                CustomerSubTypeAdapter.Fill(CustomerSubTypeTable)
                Me.SourceCustomerSubTypeDropDownList.DataSource = CustomerSubTypeTable
                Me.SourceCustomerSubTypeDropDownList.DataTextField = "CustomerSubTypeDescription"
                Me.SourceCustomerSubTypeDropDownList.DataValueField = "CustomerSubTypeCode"
                Me.SourceCustomerSubTypeDropDownList.DataBind()
                'Me.DestinationCustomerSubTypeDropDownList.DataSource = CustomerSubTypeTable
                'Me.DestinationCustomerSubTypeDropDownList.DataTextField = "CustomerSubTypeDescription"
                'Me.DestinationCustomerSubTypeDropDownList.DataValueField = "CustomerSubTypeCode"
                'Me.DestinationCustomerSubTypeDropDownList.DataBind()
            End Using
        End Using
    End Function

    Private Function LoadInternalIndustryCode() As Boolean
        Using InternalIndustryCodeAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.InternalIndustryCodeTableAdapter
            Using InternalIndustryCodeTable As New AMLDAL.BasicRulesDataSet.InternalIndustryCodeDataTable
                InternalIndustryCodeAdapter.Fill(InternalIndustryCodeTable)
                Me.SourceInternalIndustryCodeDropDownList.DataSource = InternalIndustryCodeTable
                Me.SourceInternalIndustryCodeDropDownList.DataTextField = "InternalIndustryDescription"
                Me.SourceInternalIndustryCodeDropDownList.DataValueField = "InternalIndustryCode"
                Me.SourceInternalIndustryCodeDropDownList.DataBind()
                'Me.DestinationInternalIndustryCodeDropDownList.DataSource = InternalIndustryCodeTable
                'Me.DestinationInternalIndustryCodeDropDownList.DataTextField = "InternalIndustryDescription"
                'Me.DestinationInternalIndustryCodeDropDownList.DataValueField = "InternalIndustryCode"
                'Me.DestinationInternalIndustryCodeDropDownList.DataBind()
            End Using
        End Using
    End Function

    Private Function LoadBusinessType() As Boolean
        Using BusinessTypeAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.BusinessTypeTableAdapter
            Using BusinessTypeTable As New AMLDAL.BasicRulesDataSet.BusinessTypeDataTable
                BusinessTypeAdapter.Fill(BusinessTypeTable)
                Me.SourceBusinessTypeDropDownList.DataSource = BusinessTypeTable
                Me.SourceBusinessTypeDropDownList.DataTextField = "BusinessTypeDescription"
                Me.SourceBusinessTypeDropDownList.DataValueField = "BusinessTypeCode"
                Me.SourceBusinessTypeDropDownList.DataBind()
                'Me.DestinationBusinessTypeDropDownList.DataSource = BusinessTypeTable
                'Me.DestinationBusinessTypeDropDownList.DataTextField = "BusinessTypeDescription"
                'Me.DestinationBusinessTypeDropDownList.DataValueField = "BusinessTypeCode"
                'Me.DestinationBusinessTypeDropDownList.DataBind()
            End Using
        End Using
    End Function

    Private Function LoadAccountType() As Boolean
        Using AccountTypeAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.AccountTypeTableAdapter
            Using AccountTypeTable As New AMLDAL.BasicRulesDataSet.AccountTypeDataTable
                AccountTypeAdapter.Fill(AccountTypeTable)
                Me.SourceAccountTypeCheckBoxList.DataSource = AccountTypeTable
                Me.SourceAccountTypeCheckBoxList.DataTextField = "AccountTypeDescription"
                Me.SourceAccountTypeCheckBoxList.DataValueField = "AccountTypeCode"
                Me.SourceAccountTypeCheckBoxList.DataBind()
                'Me.DestinationAccountTypeCheckBoxList.DataSource = AccountTypeTable
                'Me.DestinationAccountTypeCheckBoxList.DataTextField = "AccountTypeDescription"
                'Me.DestinationAccountTypeCheckBoxList.DataValueField = "AccountTypeCode"
                'Me.DestinationAccountTypeCheckBoxList.DataBind()
            End Using
        End Using
    End Function

    Private Function LoadAccountStatus() As Boolean
        Using AccountStatusAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.AccountStatusTableAdapter
            Using AccountStatusTable As New AMLDAL.BasicRulesDataSet.AccountStatusDataTable
                AccountStatusAdapter.Fill(AccountStatusTable)
                Me.SourceAccountStatusDropDownList.DataSource = AccountStatusTable
                Me.SourceAccountStatusDropDownList.DataTextField = "AccountStatusDescription"
                Me.SourceAccountStatusDropDownList.DataValueField = "AccountStatusCode"
                Me.SourceAccountStatusDropDownList.DataBind()
                'Me.DestinationAccountStatusDropDownList.DataSource = AccountStatusTable
                'Me.DestinationAccountStatusDropDownList.DataTextField = "AccountStatusDescription"
                'Me.DestinationAccountStatusDropDownList.DataValueField = "AccountStatusCode"
                'Me.DestinationAccountStatusDropDownList.DataBind()
            End Using
        End Using
    End Function

    Private Function LoadTransactionType() As Boolean
        Using TransactionChannelTypeAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.TransactionChannelTypeTableAdapter
            Using TransactionChannelTypeTable As New AMLDAL.BasicRulesDataSet.TransactionChannelTypeDataTable
                TransactionChannelTypeAdapter.Fill(TransactionChannelTypeTable)
                Me.TransactionTypeDropDownList.DataSource = TransactionChannelTypeTable
                Me.TransactionTypeDropDownList.DataTextField = "TransactionChannelTypeName"
                Me.TransactionTypeDropDownList.DataValueField = "PK_TransactionChannelType"
                Me.TransactionTypeDropDownList.DataBind()
            End Using
        End Using
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Me.LoadNegativeList()
            Me.LoadCustomerSubType()
            Me.LoadInternalIndustryCode()
            Me.LoadBusinessType()
            Me.LoadAccountType()
            Me.LoadAccountStatus()
            Me.LoadTransactionType()
        End If
    End Sub

    'Private Function BuildExpression() As String
    '    Dim SQLSelectExpression As String
    '    Dim SQLFromExpression As String
    '    Dim SQLWhereExpression As String = ""
    '    Dim SQLGroupByExpression As String = ""
    '    Dim SQLHavingExpression As String = ""
    '    StrCaseGroupingBy = "TransactionDetail.AccountNo"
    '    ' 1 CIF & Related Source Fund
    '    If Me.TransactionRelatedCIFCheckBox.Checked Then
    '        StrCaseGroupingBy = "CustomerLinkForRules_TempTable.CIFNo"
    '    End If
    '    SQLGroupByExpression = StrCaseGroupingBy & ", TransactionDetail.AccountOwnerId, TransactionDetail.TransactionDetailId  "
    '    SQLSelectExpression = "SELECT " & StrCaseGroupingBy & ", TransactionDetail.AccountOwnerId, TransactionDetail.TransactionDetailId "
    '    SQLFromExpression = " FROM TransactionDetail"
    '    'TODO: Rubah join kalo filternya menggunakan saja
    '    If SQLFromExpression.IndexOf("CFMAST") > -1 Then
    '        SQLFromExpression = SQLFromExpression & " INNER JOIN CFMAST ON TransactionDetail.CIFNo=CFMAST.CFCIF#"
    '    End If
    '    If SQLFromExpression.IndexOf("AllAccount_AllInfo") > -1 Then
    '        SQLFromExpression = SQLFromExpression & " INNER JOIN AllAccount_AllInfo ON TransactionDetail.AccountNo=AllAccount_AllInfo.AccountNo"
    '    End If
    '    If SQLFromExpression.IndexOf("vw_NegativeCIFVerificationListCategory") > -1 Then
    '        SQLFromExpression = SQLFromExpression & " INNER JOIN vw_NegativeCIFVerificationListCategory ON TransactionDetail.CIFNo=vw_NegativeCIFVerificationListCategory.CIFNo"
    '    End If

    '    ' Customer Of
    '    Select Case SourceCustomerOfDropDownList.SelectedIndex
    '        'Case 0 ' All
    '        'No Filter, commented
    '        Case 1 ' Nasabah Bank Niaga
    '            SQLWhereExpression = SQLWhereExpression & " AND TransactionDetail.CIFNo<>''"
    '        Case 2 ' Non Nasabah Bank Niaga
    '            SQLWhereExpression = SQLWhereExpression & " AND TransactionDetail.CIFNo=''"
    '    End Select
    '    ' Customer Type
    '    Select Case SourceCustomerTypeDropDownList.SelectedIndex
    '        'Case 0 ' All
    '        'No Filter, Commentted
    '        Case 1 ' Nasabah Personal

    '            SQLWhereExpression = SQLWhereExpression & " AND CFMAST.CFCLAS='A'"
    '        Case 2 ' Nasabah Company
    '            SQLWhereExpression = SQLWhereExpression & " AND CFMAST.CFCLAS='B'"
    '    End Select
    '    ' Negative List
    '    Dim NegativeListItem As New ListItem
    '    Dim StrNegativeListCategoryID As String = ""
    '    For Each NegativeListItem In SourceNegativeListCheckBoxList.Items
    '        If NegativeListItem.Selected Then
    '            StrNegativeListCategoryID = StrNegativeListCategoryID & NegativeListItem.Value & ", "
    '        End If
    '    Next
    '    If StrNegativeListCategoryID.Length > 0 Then
    '        StrNegativeListCategoryID = StrNegativeListCategoryID.Substring(0, StrNegativeListCategoryID.Length - 2)
    '        SQLWhereExpression = SQLWhereExpression & " AND vw_NegativeCIFVerificationListCategory.VerificationListCategoryId IN (" & StrNegativeListCategoryID & ")"
    '    End If
    '    ' High Risk Business
    '    If Me.SourceHighRiskBusinessCheckBox.Checked Then
    '        SQLWhereExpression = SQLWhereExpression & " AND CFMAST.CFYIDC IN (SELECT HBCDBU FROM CFHRBU)"
    '    End If
    '    ' High Risk Country
    '    If Me.SourceHighRiskCountryCheckBox.Checked Then
    '        SQLWhereExpression = SQLWhereExpression & " AND CFMAST.CFCOUN IN (SELECT CTCODE FROM CFHCTM)"
    '    End If
    '    ' Customer Sub Type
    '    If Me.SourceCustomerSubTypeDropDownList.SelectedValue <> "All" Then
    '        SQLWhereExpression = SQLWhereExpression & " AND CFMAST.CFSCLA ='" & Me.SourceCustomerSubTypeDropDownList.SelectedValue & "'"
    '    End If
    '    ' Business Type
    '    If Me.SourceBusinessTypeDropDownList.SelectedValue <> "All" Then
    '        SQLWhereExpression = SQLWhereExpression & " AND CFMAST.CFBUST ='" & Me.SourceBusinessTypeDropDownList.SelectedValue & "'"
    '    End If
    '    ' Internal Industry Code
    '    If Me.SourceInternalIndustryCodeDropDownList.SelectedValue <> "All" Then
    '        SQLWhereExpression = SQLWhereExpression & " AND CFMAST.CFYIDC ='" & Me.SourceInternalIndustryCodeDropDownList.SelectedValue & "'"
    '    End If
    '    '--------
    '    ' Account
    '    '--------
    '    ' Account Opened
    '    If Me.SourceOpenedWithinTextBox.Text <> "" Then
    '        Select Case Me.SourceOpenedWithinTypeDropDownList.SelectedIndex
    '            Case 0
    '                SQLWhereExpression = SQLWhereExpression & " AND DATEDIFF(DAY,AllAccount_AllInfo.OpeningDate,@ProcessDate)>=" & Me.SourceOpenedWithinTextBox.Text
    '            Case 1
    '                SQLWhereExpression = SQLWhereExpression & " AND DATEDIFF(MONTH,AllAccount_AllInfo.OpeningDate,@ProcessDate)>=" & Me.SourceOpenedWithinTextBox.Text
    '            Case 2
    '                SQLWhereExpression = SQLWhereExpression & " AND DATEDIFF(YEAR,AllAccount_AllInfo.OpeningDate,@ProcessDate)>=" & Me.SourceOpenedWithinTextBox.Text
    '        End Select
    '    End If
    '    ' Account Type
    '    Dim AccountTypeItem As New ListItem
    '    Dim StrAccountTypeList As String = ""
    '    For Each AccountTypeItem In SourceAccountTypeCheckBoxList.Items
    '        If AccountTypeItem.Selected Then
    '            StrAccountTypeList = StrAccountTypeList & "'" & AccountTypeItem.Value & "', "
    '        End If
    '    Next
    '    If StrAccountTypeList.Length > 0 Then
    '        StrAccountTypeList = StrAccountTypeList.Substring(0, StrAccountTypeList.Length - 2)
    '        SQLWhereExpression = SQLWhereExpression & " AND AllAccount_AllInfo.AccountType IN (" & StrAccountTypeList & ")"
    '    End If
    '    ' Segmentation
    '    If Me.SourceSegmentationDropDownList.SelectedValue <> "All" Then
    '        Select Case Me.SourceSegmentationDropDownList.SelectedValue
    '            Case "Individual (Except BNPC & PBG)"
    '                SQLWhereExpression = SQLWhereExpression & " AND CFMAST.CFCLAS='A'"
    '                SQLWhereExpression = SQLWhereExpression & " AND NOT AllAccount_AllInfo.BranchId IN (SELECT BranchId FROM BranchTypeMapping WHERE BranchTypeId=1 OR BranchTypeId=2)"
    '            Case "Affluent"
    '                SQLWhereExpression = SQLWhereExpression & " AND CFMAST.CFCLAS='A'"
    '                SQLWhereExpression = SQLWhereExpression & " AND AllAccount_AllInfo.BranchId IN (SELECT BranchId FROM BranchTypeMapping WHERE BranchTypeId=1 OR BranchTypeId=2)"
    '            Case "Business"
    '                SQLWhereExpression = SQLWhereExpression & " AND AllAccount_AllInfo.Segment='02'"
    '            Case "Corporate"
    '                SQLWhereExpression = SQLWhereExpression & " AND AllAccount_AllInfo.Segment='01'"
    '            Case "UKM" ' Berdasarkan Kode Cabang
    '                SQLWhereExpression = SQLWhereExpression & " AND AllAccount_AllInfo.BranchId IN (SELECT BranchId FROM BranchTypeMapping WHERE BranchTypeId=3)"
    '            Case "Financial Institution"
    '                SQLWhereExpression = SQLWhereExpression & " AND AllAccount_AllInfo.BranchId IN (SELECT BranchId FROM BranchTypeMapping WHERE BranchTypeId=4)"
    '        End Select
    '    End If
    '    ' Account Status
    '    If Me.SourceAccountStatusDropDownList.SelectedValue <> "A" Then
    '        SQLWhereExpression = SQLWhereExpression & " AND AllAccount_AllInfo.AccountStatus = '" & Me.SourceAccountStatusDropDownList.SelectedValue & "'"
    '    End If
    '    ' Transaction Type
    '    If Me.TransactionTypeDropDownList.SelectedValue <> "0" Then
    '        SQLWhereExpression = SQLWhereExpression & " AND TransactionDetail.TransactionChannelType= " & Me.TransactionTypeDropDownList.SelectedValue
    '    End If
    '    ' Auxiliary Transaction Code
    '    If Me.AuxiliaryTransactionCodeTextBox.Text <> "" Then
    '        'aa,bb,cc, --> aa','bb','cc
    '        ' harus di pastikan inputnya benar atau ngak pake interface laen seperti checklistbox
    '        SQLWhereExpression = SQLWhereExpression & " AND TransactionDetail.AuxiliaryTransactionCode " & Me.AuxiliaryTransactionCodeDropDownList.SelectedValue & " ('" & Me.AuxiliaryTransactionCodeTextBox.Text.Replace(",", "','") & "')"
    '    End If
    '    ' Transaction Amount
    '    If Me.TransactionAmountFromDropDownList.SelectedValue = "1" Then
    '        If Me.TransactionAmountUntilDropDownList.SelectedValue = "1" Then
    '            SQLHavingExpression = SQLHavingExpression & " AND SUM(TransactionDetail.TransactionLocalEquivalent) >= " & Me.TransactionAmountFromTextBox.Text
    '            SQLHavingExpression = SQLHavingExpression & " AND SUM(TransactionDetail.TransactionLocalEquivalent) <= " & Me.TransactionAmountUntilTextBox.Text
    '        Else
    '            SQLHavingExpression = SQLHavingExpression & " AND SUM(TransactionDetail.TransactionLocalEquivalent) >= " & Me.TransactionAmountFromTextBox.Text
    '        End If
    '    Else ' Unspecified
    '        If Me.TransactionAmountUntilDropDownList.SelectedValue = "1" Then
    '            SQLHavingExpression = SQLHavingExpression & " AND SUM(TransactionDetail.TransactionLocalEquivalent) <= " & Me.TransactionAmountUntilTextBox.Text
    '        Else
    '            ' no filter do nothing
    '        End If
    '    End If
    '    ' Amount >= Nominal Transaction Normal
    '    If Me.TransactionAmountGEKYCTransactionNormalCheckBox.Checked Then
    '        SQLWhereExpression = SQLWhereExpression & " AND TransactionDetail.TransactionLocalEquivalent >= (SELECT CHTNHL FROM CFHATN WHERE CHTNCD=AllAccount_AllInfo.NominalTransaksiNormal)"
    '    End If
    '    ' Credit Or Debit
    '    If Me.TransactionCreditORDebitDropDownList.SelectedValue <> "All" Then
    '        SQLWhereExpression = SQLWhereExpression & " AND TransactionDetail.CreditORDebit='" & Me.TransactionCreditORDebitDropDownList.SelectedValue & "'"
    '    End If
    '    ' Transaction Frequency
    '    If Me.TransactionFrequencyCheckBox.Checked Then
    '        SQLHavingExpression = SQLHavingExpression & " AND COUNT(*) >= " & Me.TransactionFrequencyTextBox.Text
    '    End If
    '    ' Transaction Period
    '    If Me.TransactionPeriodCheckBox.Checked Then
    '        Select Case Me.SourceOpenedWithinTypeDropDownList.SelectedIndex
    '            Case 0
    '                SQLWhereExpression = SQLWhereExpression & " AND DATEDIFF(DAY,TransactionDetail.TransactionDate,@ProcessDate)>=" & Me.SourceOpenedWithinTextBox.Text
    '            Case 1
    '                SQLWhereExpression = SQLWhereExpression & " AND DATEDIFF(MONTH,TransactionDetail.TransactionDate,@ProcessDate)>=" & Me.SourceOpenedWithinTextBox.Text
    '            Case 2
    '                SQLWhereExpression = SQLWhereExpression & " AND DATEDIFF(YEAR,TransactionDetail.TransactionDate,@ProcessDate)>=" & Me.SourceOpenedWithinTextBox.Text
    '        End Select
    '    End If
    '    ' Country Code
    '    If Me.TransactionCountryCodeTextBox.Text <> "" Then
    '        'aa,bb,cc, --> aa','bb','cc
    '        ' harus di pastikan inputnya benar atau ngak pake interface laen seperti checklistbox
    '        SQLWhereExpression = SQLWhereExpression & " AND TransactionDetail.CountryCode " & Me.TransactionCountryCodeDropDownList.SelectedValue & " ('" & Me.TransactionCountryCodeTextBox.Text.Replace(",", "','") & "')"
    '    End If
    '    ' 1 CIF & Related Source Fund
    '    If Me.TransactionRelatedCIFCheckBox.Checked Then
    '        'SQLWhereExpression = SQLWhereExpression & " AND TransactionDetail.CountryCode " & Me.TransactionCountryCodeDropDownList.SelectedValue & " ('" & Me.TransactionCountryCodeTextBox.Text.Replace(",", "','") & "')"
    '    End If
    '    If SQLWhereExpression <> "" Then
    '        SQLWhereExpression = " WHERE " & SQLWhereExpression.Substring(4)
    '    End If
    '    If SQLGroupByExpression <> "" Then
    '        SQLGroupByExpression = " GROUP BY " & SQLGroupByExpression
    '    End If
    '    If SQLHavingExpression <> "" Then
    '        SQLHavingExpression = " HAVING " & SQLHavingExpression.Substring(4)
    '    End If
    '    Return SQLSelectExpression & SQLFromExpression & SQLWhereExpression & SQLGroupByExpression & SQLHavingExpression

    'End Function

    Private Function BuildExpressionForCIF() As String
        Dim StrExpression As String
        Dim SQLSelectExpression As String
        Dim SQLFromExpression As String
        Dim SQLWhereExpression As String = ""
        Dim SQLGroupByExpression As String = ""
        Dim SQLHavingExpression As String = ""
        StrCaseGroupingBy = "CIFNo"
        SQLGroupByExpression = "CustomerLinkForRules_TempTable.CIFNo"
        SQLSelectExpression = "SELECT CustomerLinkForRules_TempTable.CIFNo AS " & StrCaseGroupingBy
        SQLFromExpression = " FROM TransactionDetail"
        SQLFromExpression = SQLFromExpression & " INNER JOIN CustomerLinkForRules_TempTable ON TransactionDetail.CIFNo=CustomerLinkForRules_TempTable.CIFRelationNo"
        'TODO: Rubah join kalo filternya menggunakan saja

        ' Customer Of
        Select Case SourceCustomerOfDropDownList.SelectedIndex
            'Case 0 ' All
            'No Filter, commented
            Case 1 ' Nasabah Bank Niaga
                SQLWhereExpression = SQLWhereExpression & " AND TransactionDetail.CIFNo<>''"
            Case 2 ' Non Nasabah Bank Niaga
                SQLWhereExpression = SQLWhereExpression & " AND TransactionDetail.CIFNo=''"
        End Select
        ' Customer Type
        Select Case SourceCustomerTypeDropDownList.SelectedIndex
            'Case 0 ' All
            'No Filter, Commentted
            Case 1 ' Nasabah Personal
                If SQLFromExpression.IndexOf("CFMAST") < 0 Then
                    SQLFromExpression = SQLFromExpression & " INNER JOIN CFMAST ON TransactionDetail.CIFNo=CFMAST.CFCIF#"
                End If
                SQLWhereExpression = SQLWhereExpression & " AND CFMAST.CFCLAS='A'"
            Case 2 ' Nasabah Company
                If SQLFromExpression.IndexOf("CFMAST") < 0 Then
                    SQLFromExpression = SQLFromExpression & " INNER JOIN CFMAST ON TransactionDetail.CIFNo=CFMAST.CFCIF#"
                End If
                SQLWhereExpression = SQLWhereExpression & " AND CFMAST.CFCLAS='B'"
        End Select
        ' Negative List
        Dim NegativeListItem As New ListItem
        Dim StrNegativeListCategoryID As String = ""
        For Each NegativeListItem In SourceNegativeListCheckBoxList.Items
            If NegativeListItem.Selected Then
                StrNegativeListCategoryID = StrNegativeListCategoryID & NegativeListItem.Value & ", "
            End If
        Next
        If StrNegativeListCategoryID.Length > 0 Then
            StrNegativeListCategoryID = StrNegativeListCategoryID.Substring(0, StrNegativeListCategoryID.Length - 2)
            If SQLFromExpression.IndexOf("vw_NegativeCIFVerificationListCategory") < 0 Then
                SQLFromExpression = SQLFromExpression & " INNER JOIN vw_NegativeCIFVerificationListCategory ON TransactionDetail.CIFNo=vw_NegativeCIFVerificationListCategory.CIFNo"
            End If

            SQLWhereExpression = SQLWhereExpression & " AND vw_NegativeCIFVerificationListCategory.VerificationListCategoryId IN (" & StrNegativeListCategoryID & ")"
        End If
        ' High Risk Business
        If Me.SourceHighRiskBusinessCheckBox.Checked Then
            If SQLFromExpression.IndexOf("CFMAST") < 0 Then
                SQLFromExpression = SQLFromExpression & " INNER JOIN CFMAST ON TransactionDetail.CIFNo=CFMAST.CFCIF#"
            End If
            SQLWhereExpression = SQLWhereExpression & " AND CFMAST.CFYIDC IN (SELECT HBCDBU FROM CFHRBU)"
        End If
        ' High Risk Country
        If Me.SourceHighRiskCountryCheckBox.Checked Then
            If SQLFromExpression.IndexOf("CFMAST") < 0 Then
                SQLFromExpression = SQLFromExpression & " INNER JOIN CFMAST ON TransactionDetail.CIFNo=CFMAST.CFCIF#"
            End If
            SQLWhereExpression = SQLWhereExpression & " AND CFMAST.CFCOUN IN (SELECT CTCODE FROM CFHCTM)"
        End If
        ' Customer Sub Type
        If Me.SourceCustomerSubTypeDropDownList.SelectedValue <> "All" Then
            If SQLFromExpression.IndexOf("CFMAST") < 0 Then
                SQLFromExpression = SQLFromExpression & " INNER JOIN CFMAST ON TransactionDetail.CIFNo=CFMAST.CFCIF#"
            End If
            SQLWhereExpression = SQLWhereExpression & " AND CFMAST.CFSCLA ='" & Me.SourceCustomerSubTypeDropDownList.SelectedValue & "'"
        End If
        ' Business Type
        If Me.SourceBusinessTypeDropDownList.SelectedValue <> "All" Then
            If SQLFromExpression.IndexOf("CFMAST") < 0 Then
                SQLFromExpression = SQLFromExpression & " INNER JOIN CFMAST ON TransactionDetail.CIFNo=CFMAST.CFCIF#"
            End If
            SQLWhereExpression = SQLWhereExpression & " AND CFMAST.CFBUST ='" & Me.SourceBusinessTypeDropDownList.SelectedValue & "'"
        End If
        ' Internal Industry Code
        If Me.SourceInternalIndustryCodeDropDownList.SelectedValue <> "All" Then
            If SQLFromExpression.IndexOf("CFMAST") < 0 Then
                SQLFromExpression = SQLFromExpression & " INNER JOIN CFMAST ON TransactionDetail.CIFNo=CFMAST.CFCIF#"
            End If
            SQLWhereExpression = SQLWhereExpression & " AND CFMAST.CFYIDC ='" & Me.SourceInternalIndustryCodeDropDownList.SelectedValue & "'"
        End If
        '--------
        ' Account
        '--------
        ' Account Opened
        If Me.SourceOpenedWithinTextBox.Text <> "" Then
            If CType(Me.SourceOpenedWithinTextBox.Text, Integer) <> 0 Then
                Select Case Me.SourceOpenedWithinTypeDropDownList.SelectedIndex
                    Case 0
                        If SQLFromExpression.IndexOf("AllAccount_AllInfo") < 0 Then
                            SQLFromExpression = SQLFromExpression & " INNER JOIN AllAccount_AllInfo ON TransactionDetail.AccountNo=AllAccount_AllInfo.AccountNo"
                        End If
                        SQLWhereExpression = SQLWhereExpression & " AND DATEDIFF(DAY,AllAccount_AllInfo.OpeningDate,@ProcessDate)>=0"
                        SQLWhereExpression = SQLWhereExpression & " AND DATEDIFF(DAY,AllAccount_AllInfo.OpeningDate,@ProcessDate)<=" & CType(Me.SourceOpenedWithinTextBox.Text, Integer) - 1
                    Case 1
                        If SQLFromExpression.IndexOf("AllAccount_AllInfo") < 0 Then
                            SQLFromExpression = SQLFromExpression & " INNER JOIN AllAccount_AllInfo ON TransactionDetail.AccountNo=AllAccount_AllInfo.AccountNo"
                        End If
                        SQLWhereExpression = SQLWhereExpression & " AND DATEDIFF(MONTH,AllAccount_AllInfo.OpeningDate,@ProcessDate)>=0"
                        SQLWhereExpression = SQLWhereExpression & " AND DATEDIFF(MONTH,AllAccount_AllInfo.OpeningDate,@ProcessDate)<=" & CType(Me.SourceOpenedWithinTextBox.Text, Integer) - 1
                    Case 2
                        If SQLFromExpression.IndexOf("AllAccount_AllInfo") < 0 Then
                            SQLFromExpression = SQLFromExpression & " INNER JOIN AllAccount_AllInfo ON TransactionDetail.AccountNo=AllAccount_AllInfo.AccountNo"
                        End If
                        SQLWhereExpression = SQLWhereExpression & " AND DATEDIFF(YEAR,AllAccount_AllInfo.OpeningDate,@ProcessDate)>=0"
                        SQLWhereExpression = SQLWhereExpression & " AND DATEDIFF(YEAR,AllAccount_AllInfo.OpeningDate,@ProcessDate)<=" & CType(Me.SourceOpenedWithinTextBox.Text, Integer) - 1
                End Select
            End If
        End If
        ' Account Type
        Dim AccountTypeItem As New ListItem
        Dim StrAccountTypeList As String = ""
        For Each AccountTypeItem In SourceAccountTypeCheckBoxList.Items
            If AccountTypeItem.Selected Then
                StrAccountTypeList = StrAccountTypeList & "'" & AccountTypeItem.Value & "', "
            End If
        Next
        If StrAccountTypeList.Length > 0 Then
            StrAccountTypeList = StrAccountTypeList.Substring(0, StrAccountTypeList.Length - 2)
            If SQLFromExpression.IndexOf("AllAccount_AllInfo") < 0 Then
                SQLFromExpression = SQLFromExpression & " INNER JOIN AllAccount_AllInfo ON TransactionDetail.AccountNo=AllAccount_AllInfo.AccountNo"
            End If
            SQLWhereExpression = SQLWhereExpression & " AND AllAccount_AllInfo.AccountType IN (" & StrAccountTypeList & ")"
        End If
        ' Segmentation
        If Me.SourceSegmentationDropDownList.SelectedValue <> "All" Then
            Select Case Me.SourceSegmentationDropDownList.SelectedValue
                Case "Individual (Except BNPC & PBG)"
                    If SQLFromExpression.IndexOf("CFMAST") < 0 Then
                        SQLFromExpression = SQLFromExpression & " INNER JOIN CFMAST ON TransactionDetail.CIFNo=CFMAST.CFCIF#"
                    End If
                    SQLWhereExpression = SQLWhereExpression & " AND CFMAST.CFCLAS='A'"
                    If SQLFromExpression.IndexOf("AllAccount_AllInfo") < 0 Then
                        SQLFromExpression = SQLFromExpression & " INNER JOIN AllAccount_AllInfo ON TransactionDetail.AccountNo=AllAccount_AllInfo.AccountNo"
                    End If
                    SQLWhereExpression = SQLWhereExpression & " AND NOT AllAccount_AllInfo.BranchId IN (SELECT BranchId FROM BranchTypeMapping WHERE BranchTypeId=1 OR BranchTypeId=2)"
                Case "Affluent"
                    If SQLFromExpression.IndexOf("CFMAST") < 0 Then
                        SQLFromExpression = SQLFromExpression & " INNER JOIN CFMAST ON TransactionDetail.CIFNo=CFMAST.CFCIF#"
                    End If
                    SQLWhereExpression = SQLWhereExpression & " AND CFMAST.CFCLAS='A'"
                    If SQLFromExpression.IndexOf("AllAccount_AllInfo") < 0 Then
                        SQLFromExpression = SQLFromExpression & " INNER JOIN AllAccount_AllInfo ON TransactionDetail.AccountNo=AllAccount_AllInfo.AccountNo"
                    End If
                    SQLWhereExpression = SQLWhereExpression & " AND AllAccount_AllInfo.BranchId IN (SELECT BranchId FROM BranchTypeMapping WHERE BranchTypeId=1 OR BranchTypeId=2)"
                Case "Business"
                    If SQLFromExpression.IndexOf("AllAccount_AllInfo") < 0 Then
                        SQLFromExpression = SQLFromExpression & " INNER JOIN AllAccount_AllInfo ON TransactionDetail.AccountNo=AllAccount_AllInfo.AccountNo"
                    End If
                    SQLWhereExpression = SQLWhereExpression & " AND AllAccount_AllInfo.Segment='02'"
                Case "Corporate"
                    If SQLFromExpression.IndexOf("AllAccount_AllInfo") < 0 Then
                        SQLFromExpression = SQLFromExpression & " INNER JOIN AllAccount_AllInfo ON TransactionDetail.AccountNo=AllAccount_AllInfo.AccountNo"
                    End If
                    SQLWhereExpression = SQLWhereExpression & " AND AllAccount_AllInfo.Segment='01'"
                Case "UKM" ' Berdasarkan Kode Cabang
                    If SQLFromExpression.IndexOf("AllAccount_AllInfo") < 0 Then
                        SQLFromExpression = SQLFromExpression & " INNER JOIN AllAccount_AllInfo ON TransactionDetail.AccountNo=AllAccount_AllInfo.AccountNo"
                    End If
                    SQLWhereExpression = SQLWhereExpression & " AND AllAccount_AllInfo.BranchId IN (SELECT BranchId FROM BranchTypeMapping WHERE BranchTypeId=3)"
                Case "Financial Institution"
                    If SQLFromExpression.IndexOf("AllAccount_AllInfo") < 0 Then
                        SQLFromExpression = SQLFromExpression & " INNER JOIN AllAccount_AllInfo ON TransactionDetail.AccountNo=AllAccount_AllInfo.AccountNo"
                    End If
                    SQLWhereExpression = SQLWhereExpression & " AND AllAccount_AllInfo.BranchId IN (SELECT BranchId FROM BranchTypeMapping WHERE BranchTypeId=4)"
            End Select
        End If
        ' Account Status
        If Me.SourceAccountStatusDropDownList.SelectedValue <> "A" Then
            If SQLFromExpression.IndexOf("AllAccount_AllInfo") < 0 Then
                SQLFromExpression = SQLFromExpression & " INNER JOIN AllAccount_AllInfo ON TransactionDetail.AccountNo=AllAccount_AllInfo.AccountNo"
            End If
            SQLWhereExpression = SQLWhereExpression & " AND AllAccount_AllInfo.AccountStatus = '" & Me.SourceAccountStatusDropDownList.SelectedValue & "'"
        End If
        ' Transaction Type
        If Me.TransactionTypeDropDownList.SelectedValue <> "0" Then
            SQLWhereExpression = SQLWhereExpression & " AND TransactionDetail.TransactionChannelType= " & Me.TransactionTypeDropDownList.SelectedValue
        End If
        ' Auxiliary Transaction Code
        If Me.AuxiliaryTransactionCodeTextBox.Text <> "" Then
            'aa,bb,cc, --> aa','bb','cc
            ' harus di pastikan inputnya benar atau ngak pake interface laen seperti checklistbox
            SQLWhereExpression = SQLWhereExpression & " AND TransactionDetail.AuxiliaryTransactionCode " & Me.AuxiliaryTransactionCodeDropDownList.SelectedValue & " ('" & Me.AuxiliaryTransactionCodeTextBox.Text.Replace(",", "','") & "')"
        End If
        ' Transaction Amount
        If Me.TransactionAmountFromDropDownList.SelectedValue = "1" Then
            If Me.TransactionAmountUntilDropDownList.SelectedValue = "1" Then
                SQLHavingExpression = SQLHavingExpression & " AND SUM(TransactionDetail.TransactionLocalEquivalent) >= " & Me.TransactionAmountFromTextBox.Text
                SQLHavingExpression = SQLHavingExpression & " AND SUM(TransactionDetail.TransactionLocalEquivalent) <= " & Me.TransactionAmountUntilTextBox.Text
            Else
                SQLHavingExpression = SQLHavingExpression & " AND SUM(TransactionDetail.TransactionLocalEquivalent) >= " & Me.TransactionAmountFromTextBox.Text
            End If
        Else ' Unspecified
            If Me.TransactionAmountUntilDropDownList.SelectedValue = "1" Then
                SQLHavingExpression = SQLHavingExpression & " AND SUM(TransactionDetail.TransactionLocalEquivalent) <= " & Me.TransactionAmountUntilTextBox.Text
            Else
                ' no filter do nothing
            End If
        End If
        ' Amount >= Nominal Transaction Normal
        If Me.TransactionAmountGEKYCTransactionNormalCheckBox.Checked Then
            If SQLFromExpression.IndexOf("AllAccount_AllInfo") < 0 Then
                SQLFromExpression = SQLFromExpression & " INNER JOIN AllAccount_AllInfo ON TransactionDetail.AccountNo=AllAccount_AllInfo.AccountNo"
            End If
            SQLWhereExpression = SQLWhereExpression & " AND TransactionDetail.TransactionLocalEquivalent >= (SELECT CHTNHL FROM CFHATN WHERE CHTNCD=AllAccount_AllInfo.NominalTransaksiNormal)"
        End If
        ' Debit Or Credit 
        If Me.TransactionDebitORCreditDropDownList.SelectedValue <> "All" Then
            SQLWhereExpression = SQLWhereExpression & " AND TransactionDetail.DebitORCredit='" & Me.TransactionDebitORCreditDropDownList.SelectedValue & "'"
        End If
        ' Transaction Frequency
        If Me.TransactionFrequencyCheckBox.Checked Then
            SQLHavingExpression = SQLHavingExpression & " AND COUNT(*) >= " & Me.TransactionFrequencyTextBox.Text
        End If
        ' Transaction Period
        If Me.TransactionPeriodCheckBox.Checked Then
            Select Case Me.TransactionPeriodTypeDropDownList.SelectedIndex
                Case 0
                    SQLWhereExpression = SQLWhereExpression & " AND DATEDIFF(DAY,TransactionDetail.TransactionDate,@ProcessDate)>=0"
                    SQLWhereExpression = SQLWhereExpression & " AND DATEDIFF(DAY,TransactionDetail.TransactionDate,@ProcessDate)<=" & CType(Me.TransactionPeriodTextBox.Text, Integer) - 1
                Case 1
                    SQLWhereExpression = SQLWhereExpression & " AND DATEDIFF(MONTH,TransactionDetail.TransactionDate,@ProcessDate)>=0"
                    SQLWhereExpression = SQLWhereExpression & " AND DATEDIFF(MONTH,TransactionDetail.TransactionDate,@ProcessDate)<=" & CType(Me.TransactionPeriodTextBox.Text, Integer) - 1
                Case 2
                    SQLWhereExpression = SQLWhereExpression & " AND DATEDIFF(YEAR,TransactionDetail.TransactionDate,@ProcessDate)>=0"
                    SQLWhereExpression = SQLWhereExpression & " AND DATEDIFF(YEAR,TransactionDetail.TransactionDate,@ProcessDate)<=" & CType(Me.TransactionPeriodTextBox.Text, Integer) - 1
            End Select
        End If
        ' Country Code
        If Me.TransactionCountryCodeTextBox.Text <> "" Then
            'aa,bb,cc, --> aa','bb','cc
            ' harus di pastikan inputnya benar atau ngak pake interface laen seperti checklistbox
            SQLWhereExpression = SQLWhereExpression & " AND TransactionDetail.CountryCode " & Me.TransactionCountryCodeDropDownList.SelectedValue & " ('" & Me.TransactionCountryCodeTextBox.Text.Replace(",", "','") & "')"
        End If
        ' 1 CIF & Related Source Fund
        If Me.TransactionRelatedCIFCheckBox.Checked Then
            'SQLWhereExpression = SQLWhereExpression & " AND TransactionDetail.CountryCode " & Me.TransactionCountryCodeDropDownList.SelectedValue & " ('" & Me.TransactionCountryCodeTextBox.Text.Replace(",", "','") & "')"
        End If
        If SQLWhereExpression <> "" Then
            SQLWhereExpression = " WHERE " & SQLWhereExpression.Substring(4)
        End If
        If SQLGroupByExpression <> "" Then
            SQLGroupByExpression = " GROUP BY " & SQLGroupByExpression
        End If
        If SQLHavingExpression <> "" Then
            SQLHavingExpression = " HAVING " & SQLHavingExpression.Substring(4)
        End If
        StrExpression = SQLSelectExpression & SQLFromExpression & SQLWhereExpression & SQLGroupByExpression & SQLHavingExpression

        Return StrExpression
    End Function

    Private Function BuildExpressionForAccount() As String
        Dim StrExpression As String
        Dim SQLSelectExpression As String
        Dim SQLFromExpression As String
        Dim SQLWhereExpression As String = ""
        Dim SQLGroupByExpression As String = ""
        Dim SQLHavingExpression As String = ""

        StrCaseGroupingBy = "AccountNo"
        SQLGroupByExpression = " TransactionDetail.AccountNo"
        SQLSelectExpression = "SELECT TransactionDetail.AccountNo AS " & StrCaseGroupingBy
        SQLFromExpression = " FROM TransactionDetail"
        'TODO: Rubah join kalo filternya menggunakan saja

        ' Customer Of
        Select Case SourceCustomerOfDropDownList.SelectedIndex
            'Case 0 ' All
            'No Filter, commented
            Case 1 ' Nasabah Bank Niaga
                SQLWhereExpression = SQLWhereExpression & " AND TransactionDetail.CIFNo<>''"
            Case 2 ' Non Nasabah Bank Niaga
                SQLWhereExpression = SQLWhereExpression & " AND TransactionDetail.CIFNo=''"
        End Select
        ' Customer Type
        Select Case SourceCustomerTypeDropDownList.SelectedIndex
            'Case 0 ' All
            'No Filter, Commentted
            Case 1 ' Nasabah Personal
                If SQLFromExpression.IndexOf("CFMAST") < 0 Then
                    SQLFromExpression = SQLFromExpression & " INNER JOIN CFMAST ON TransactionDetail.CIFNo=CFMAST.CFCIF#"
                End If
                SQLWhereExpression = SQLWhereExpression & " AND CFMAST.CFCLAS='A'"
            Case 2 ' Nasabah Company
                If SQLFromExpression.IndexOf("CFMAST") < 0 Then
                    SQLFromExpression = SQLFromExpression & " INNER JOIN CFMAST ON TransactionDetail.CIFNo=CFMAST.CFCIF#"
                End If
                SQLWhereExpression = SQLWhereExpression & " AND CFMAST.CFCLAS='B'"
        End Select
        ' Negative List
        Dim NegativeListItem As New ListItem
        Dim StrNegativeListCategoryID As String = ""
        For Each NegativeListItem In SourceNegativeListCheckBoxList.Items
            If NegativeListItem.Selected Then
                StrNegativeListCategoryID = StrNegativeListCategoryID & NegativeListItem.Value & ", "
            End If
        Next
        If StrNegativeListCategoryID.Length > 0 Then
            StrNegativeListCategoryID = StrNegativeListCategoryID.Substring(0, StrNegativeListCategoryID.Length - 2)
            If SQLFromExpression.IndexOf("vw_NegativeCIFVerificationListCategory") < 0 Then
                SQLFromExpression = SQLFromExpression & " INNER JOIN vw_NegativeCIFVerificationListCategory ON TransactionDetail.CIFNo=vw_NegativeCIFVerificationListCategory.CIFNo"
            End If
            SQLWhereExpression = SQLWhereExpression & " AND vw_NegativeCIFVerificationListCategory.VerificationListCategoryId IN (" & StrNegativeListCategoryID & ")"
        End If
        ' High Risk Business
        If Me.SourceHighRiskBusinessCheckBox.Checked Then
            If SQLFromExpression.IndexOf("CFMAST") < 0 Then
                SQLFromExpression = SQLFromExpression & " INNER JOIN CFMAST ON TransactionDetail.CIFNo=CFMAST.CFCIF#"
            End If
            SQLWhereExpression = SQLWhereExpression & " AND CFMAST.CFYIDC IN (SELECT HBCDBU FROM CFHRBU)"
        End If
        ' High Risk Country
        If Me.SourceHighRiskCountryCheckBox.Checked Then
            If SQLFromExpression.IndexOf("CFMAST") < 0 Then
                SQLFromExpression = SQLFromExpression & " INNER JOIN CFMAST ON TransactionDetail.CIFNo=CFMAST.CFCIF#"
            End If
            SQLWhereExpression = SQLWhereExpression & " AND CFMAST.CFCOUN IN (SELECT CTCODE FROM CFHCTM)"
        End If
        ' Customer Sub Type
        If Me.SourceCustomerSubTypeDropDownList.SelectedValue <> "All" Then
            If SQLFromExpression.IndexOf("CFMAST") < 0 Then
                SQLFromExpression = SQLFromExpression & " INNER JOIN CFMAST ON TransactionDetail.CIFNo=CFMAST.CFCIF#"
            End If
            SQLWhereExpression = SQLWhereExpression & " AND CFMAST.CFSCLA ='" & Me.SourceCustomerSubTypeDropDownList.SelectedValue & "'"
        End If
        ' Business Type
        If Me.SourceBusinessTypeDropDownList.SelectedValue <> "All" Then
            If SQLFromExpression.IndexOf("CFMAST") < 0 Then
                SQLFromExpression = SQLFromExpression & " INNER JOIN CFMAST ON TransactionDetail.CIFNo=CFMAST.CFCIF#"
            End If
            SQLWhereExpression = SQLWhereExpression & " AND CFMAST.CFBUST ='" & Me.SourceBusinessTypeDropDownList.SelectedValue & "'"
        End If
        ' Internal Industry Code
        If Me.SourceInternalIndustryCodeDropDownList.SelectedValue <> "All" Then
            If SQLFromExpression.IndexOf("CFMAST") < 0 Then
                SQLFromExpression = SQLFromExpression & " INNER JOIN CFMAST ON TransactionDetail.CIFNo=CFMAST.CFCIF#"
            End If
            SQLWhereExpression = SQLWhereExpression & " AND CFMAST.CFYIDC ='" & Me.SourceInternalIndustryCodeDropDownList.SelectedValue & "'"
        End If
        '--------
        ' Account
        '--------
        ' Account Opened
        If Me.SourceOpenedWithinTextBox.Text <> "" Then
            If CType(Me.SourceOpenedWithinTextBox.Text, Integer) <> 0 Then
                Select Case Me.SourceOpenedWithinTypeDropDownList.SelectedIndex
                    Case 0 ' Days
                        If SQLFromExpression.IndexOf("AllAccount_AllInfo") < 0 Then
                            SQLFromExpression = SQLFromExpression & " INNER JOIN AllAccount_AllInfo ON TransactionDetail.AccountNo=AllAccount_AllInfo.AccountNo"
                        End If
                        SQLWhereExpression = SQLWhereExpression & " AND DATEDIFF(DAY,AllAccount_AllInfo.OpeningDate,@ProcessDate)>=0"
                        SQLWhereExpression = SQLWhereExpression & " AND DATEDIFF(DAY,AllAccount_AllInfo.OpeningDate,@ProcessDate)<=" & CType(Me.SourceOpenedWithinTextBox.Text, Integer) - 1
                    Case 1 ' Months
                        If SQLFromExpression.IndexOf("AllAccount_AllInfo") < 0 Then
                            SQLFromExpression = SQLFromExpression & " INNER JOIN AllAccount_AllInfo ON TransactionDetail.AccountNo=AllAccount_AllInfo.AccountNo"
                        End If
                        SQLWhereExpression = SQLWhereExpression & " AND DATEDIFF(MONTH,AllAccount_AllInfo.OpeningDate,@ProcessDate)>=0"
                        SQLWhereExpression = SQLWhereExpression & " AND DATEDIFF(MONTH,AllAccount_AllInfo.OpeningDate,@ProcessDate)<=" & CType(Me.SourceOpenedWithinTextBox.Text, Integer) - 1
                    Case 2 ' Years
                        If SQLFromExpression.IndexOf("AllAccount_AllInfo") < 0 Then
                            SQLFromExpression = SQLFromExpression & " INNER JOIN AllAccount_AllInfo ON TransactionDetail.AccountNo=AllAccount_AllInfo.AccountNo"
                        End If
                        SQLWhereExpression = SQLWhereExpression & " AND DATEDIFF(YEAR,AllAccount_AllInfo.OpeningDate,@ProcessDate)>=0"
                        SQLWhereExpression = SQLWhereExpression & " AND DATEDIFF(YEAR,AllAccount_AllInfo.OpeningDate,@ProcessDate)<=" & CType(Me.SourceOpenedWithinTextBox.Text, Integer) - 1
                End Select
            End If
        End If
        ' Account Type
        Dim AccountTypeItem As New ListItem
        Dim StrAccountTypeList As String = ""
        For Each AccountTypeItem In SourceAccountTypeCheckBoxList.Items
            If AccountTypeItem.Selected Then
                StrAccountTypeList = StrAccountTypeList & "'" & AccountTypeItem.Value & "', "
            End If
        Next
        If StrAccountTypeList.Length > 0 Then
            StrAccountTypeList = StrAccountTypeList.Substring(0, StrAccountTypeList.Length - 2)
            If SQLFromExpression.IndexOf("AllAccount_AllInfo") < 0 Then
                SQLFromExpression = SQLFromExpression & " INNER JOIN AllAccount_AllInfo ON TransactionDetail.AccountNo=AllAccount_AllInfo.AccountNo"
            End If
            SQLWhereExpression = SQLWhereExpression & " AND AllAccount_AllInfo.AccountType IN (" & StrAccountTypeList & ")"
        End If
        ' Segmentation
        If Me.SourceSegmentationDropDownList.SelectedValue <> "All" Then
            Select Case Me.SourceSegmentationDropDownList.SelectedValue
                Case "Individual (Except BNPC & PBG)"
                    If SQLFromExpression.IndexOf("CFMAST") < 0 Then
                        SQLFromExpression = SQLFromExpression & " INNER JOIN CFMAST ON TransactionDetail.CIFNo=CFMAST.CFCIF#"
                    End If
                    SQLWhereExpression = SQLWhereExpression & " AND CFMAST.CFCLAS='A'"
                    If SQLFromExpression.IndexOf("AllAccount_AllInfo") < 0 Then
                        SQLFromExpression = SQLFromExpression & " INNER JOIN AllAccount_AllInfo ON TransactionDetail.AccountNo=AllAccount_AllInfo.AccountNo"
                    End If
                    SQLWhereExpression = SQLWhereExpression & " AND NOT AllAccount_AllInfo.BranchId IN (SELECT BranchId FROM BranchTypeMapping WHERE BranchTypeId=1 OR BranchTypeId=2)"
                Case "Affluent"
                    If SQLFromExpression.IndexOf("CFMAST") < 0 Then
                        SQLFromExpression = SQLFromExpression & " INNER JOIN CFMAST ON TransactionDetail.CIFNo=CFMAST.CFCIF#"
                    End If
                    SQLWhereExpression = SQLWhereExpression & " AND CFMAST.CFCLAS='A'"
                    If SQLFromExpression.IndexOf("AllAccount_AllInfo") < 0 Then
                        SQLFromExpression = SQLFromExpression & " INNER JOIN AllAccount_AllInfo ON TransactionDetail.AccountNo=AllAccount_AllInfo.AccountNo"
                    End If
                    SQLWhereExpression = SQLWhereExpression & " AND AllAccount_AllInfo.BranchId IN (SELECT BranchId FROM BranchTypeMapping WHERE BranchTypeId=1 OR BranchTypeId=2)"
                Case "Business"
                    If SQLFromExpression.IndexOf("AllAccount_AllInfo") < 0 Then
                        SQLFromExpression = SQLFromExpression & " INNER JOIN AllAccount_AllInfo ON TransactionDetail.AccountNo=AllAccount_AllInfo.AccountNo"
                    End If
                    SQLWhereExpression = SQLWhereExpression & " AND AllAccount_AllInfo.Segment='02'"
                Case "Corporate"
                    If SQLFromExpression.IndexOf("AllAccount_AllInfo") < 0 Then
                        SQLFromExpression = SQLFromExpression & " INNER JOIN AllAccount_AllInfo ON TransactionDetail.AccountNo=AllAccount_AllInfo.AccountNo"
                    End If
                    SQLWhereExpression = SQLWhereExpression & " AND AllAccount_AllInfo.Segment='01'"
                Case "UKM" ' Berdasarkan Kode Cabang
                    If SQLFromExpression.IndexOf("AllAccount_AllInfo") < 0 Then
                        SQLFromExpression = SQLFromExpression & " INNER JOIN AllAccount_AllInfo ON TransactionDetail.AccountNo=AllAccount_AllInfo.AccountNo"
                    End If
                    SQLWhereExpression = SQLWhereExpression & " AND AllAccount_AllInfo.BranchId IN (SELECT BranchId FROM BranchTypeMapping WHERE BranchTypeId=3)"
                Case "Financial Institution"
                    If SQLFromExpression.IndexOf("AllAccount_AllInfo") < 0 Then
                        SQLFromExpression = SQLFromExpression & " INNER JOIN AllAccount_AllInfo ON TransactionDetail.AccountNo=AllAccount_AllInfo.AccountNo"
                    End If
                    SQLWhereExpression = SQLWhereExpression & " AND AllAccount_AllInfo.BranchId IN (SELECT BranchId FROM BranchTypeMapping WHERE BranchTypeId=4)"
            End Select
        End If
        ' Account Status
        If Me.SourceAccountStatusDropDownList.SelectedValue <> "A" Then
            If SQLFromExpression.IndexOf("AllAccount_AllInfo") < 0 Then
                SQLFromExpression = SQLFromExpression & " INNER JOIN AllAccount_AllInfo ON TransactionDetail.AccountNo=AllAccount_AllInfo.AccountNo"
            End If
            SQLWhereExpression = SQLWhereExpression & " AND AllAccount_AllInfo.AccountStatus = '" & Me.SourceAccountStatusDropDownList.SelectedValue & "'"
        End If
        ' Transaction Type
        If Me.TransactionTypeDropDownList.SelectedValue <> "0" Then
            SQLWhereExpression = SQLWhereExpression & " AND TransactionDetail.TransactionChannelType= " & Me.TransactionTypeDropDownList.SelectedValue
        End If
        ' Auxiliary Transaction Code
        If Me.AuxiliaryTransactionCodeTextBox.Text <> "" Then
            'aa,bb,cc, --> aa','bb','cc
            ' harus di pastikan inputnya benar atau ngak pake interface laen seperti checklistbox
            SQLWhereExpression = SQLWhereExpression & " AND TransactionDetail.AuxiliaryTransactionCode " & Me.AuxiliaryTransactionCodeDropDownList.SelectedValue & " ('" & Me.AuxiliaryTransactionCodeTextBox.Text.Replace(",", "','") & "')"
        End If
        ' Transaction Amount
        If Me.TransactionAmountFromDropDownList.SelectedValue = "1" Then
            If Me.TransactionAmountUntilDropDownList.SelectedValue = "1" Then
                SQLHavingExpression = SQLHavingExpression & " AND SUM(TransactionDetail.TransactionLocalEquivalent) >= " & Me.TransactionAmountFromTextBox.Text
                SQLHavingExpression = SQLHavingExpression & " AND SUM(TransactionDetail.TransactionLocalEquivalent) <= " & Me.TransactionAmountUntilTextBox.Text
            Else
                SQLHavingExpression = SQLHavingExpression & " AND SUM(TransactionDetail.TransactionLocalEquivalent) >= " & Me.TransactionAmountFromTextBox.Text
            End If
        Else ' Unspecified
            If Me.TransactionAmountUntilDropDownList.SelectedValue = "1" Then
                SQLHavingExpression = SQLHavingExpression & " AND SUM(TransactionDetail.TransactionLocalEquivalent) <= " & Me.TransactionAmountUntilTextBox.Text
            Else
                ' no filter do nothing
            End If
        End If
        ' Amount >= Nominal Transaction Normal
        If Me.TransactionAmountGEKYCTransactionNormalCheckBox.Checked Then
            If SQLFromExpression.IndexOf("AllAccount_AllInfo") < 0 Then
                SQLFromExpression = SQLFromExpression & " INNER JOIN AllAccount_AllInfo ON TransactionDetail.AccountNo=AllAccount_AllInfo.AccountNo"
            End If
            SQLWhereExpression = SQLWhereExpression & " AND TransactionDetail.TransactionLocalEquivalent >= (SELECT CHTNHL FROM CFHATN WHERE CHTNCD=AllAccount_AllInfo.NominalTransaksiNormal)"
        End If
        ' Debit Or Credit 
        If Me.TransactionDebitORCreditDropDownList.SelectedValue <> "All" Then
            SQLWhereExpression = SQLWhereExpression & " AND TransactionDetail.DebitORCredit='" & Me.TransactionDebitORCreditDropDownList.SelectedValue & "'"
        End If
        ' Transaction Frequency
        If Me.TransactionFrequencyCheckBox.Checked Then
            SQLHavingExpression = SQLHavingExpression & " AND COUNT(*) >= " & Me.TransactionFrequencyTextBox.Text
        End If
        ' Transaction Period
        If Me.TransactionPeriodCheckBox.Checked Then
            Select Case Me.TransactionPeriodTypeDropDownList.SelectedIndex
                Case 0
                    SQLWhereExpression = SQLWhereExpression & " AND DATEDIFF(DAY,TransactionDetail.TransactionDate,@ProcessDate)>=0"
                    SQLWhereExpression = SQLWhereExpression & " AND DATEDIFF(DAY,TransactionDetail.TransactionDate,@ProcessDate)<=" & CType(Me.TransactionPeriodTextBox.Text, Integer) - 1
                Case 1
                    SQLWhereExpression = SQLWhereExpression & " AND DATEDIFF(MONTH,TransactionDetail.TransactionDate,@ProcessDate)>=0"
                    SQLWhereExpression = SQLWhereExpression & " AND DATEDIFF(MONTH,TransactionDetail.TransactionDate,@ProcessDate)<=" & CType(Me.TransactionPeriodTextBox.Text, Integer) - 1
                Case 2
                    SQLWhereExpression = SQLWhereExpression & " AND DATEDIFF(YEAR,TransactionDetail.TransactionDate,@ProcessDate)>=0"
                    SQLWhereExpression = SQLWhereExpression & " AND DATEDIFF(YEAR,TransactionDetail.TransactionDate,@ProcessDate)<=" & CType(Me.TransactionPeriodTextBox.Text, Integer) - 1
            End Select
        End If
        ' Country Code
        If Me.TransactionCountryCodeTextBox.Text <> "" Then
            'aa,bb,cc, --> aa','bb','cc
            ' harus di pastikan inputnya benar atau ngak pake interface laen seperti checklistbox
            SQLWhereExpression = SQLWhereExpression & " AND TransactionDetail.CountryCode " & Me.TransactionCountryCodeDropDownList.SelectedValue & " ('" & Me.TransactionCountryCodeTextBox.Text.Replace(",", "','") & "')"
        End If
        ' 1 CIF & Related Source Fund
        If Me.TransactionRelatedCIFCheckBox.Checked Then
            'SQLWhereExpression = SQLWhereExpression & " AND TransactionDetail.CountryCode " & Me.TransactionCountryCodeDropDownList.SelectedValue & " ('" & Me.TransactionCountryCodeTextBox.Text.Replace(",", "','") & "')"
        End If
        If SQLWhereExpression <> "" Then
            SQLWhereExpression = " WHERE " & SQLWhereExpression.Substring(4)
        End If
        If SQLGroupByExpression <> "" Then
            SQLGroupByExpression = " GROUP BY " & SQLGroupByExpression
        End If
        If SQLHavingExpression <> "" Then
            SQLHavingExpression = " HAVING " & SQLHavingExpression.Substring(4)
        End If
        StrExpression = SQLSelectExpression & SQLFromExpression & SQLWhereExpression & SQLGroupByExpression & SQLHavingExpression

        Return StrExpression
    End Function

    Private Function BuildExpression() As String
        Dim StrExpression As String
        ' 1 CIF & Related Source Fund
        If Me.TransactionRelatedCIFCheckBox.Checked Then
            StrExpression = Me.BuildExpressionForCIF
        Else
            StrExpression = Me.BuildExpressionForAccount
        End If
        Return StrExpression
    End Function

    Private Function CheckInputValid() As Boolean
        Dim cek As Integer = 0
        Dim ErrorMessage As String = ""

        If Me.SourceOpenedWithinTextBox.Text <> "" Then
            If Not IsNumeric(Me.SourceOpenedWithinTextBox.Text) Then
                cek = 1
                If ErrorMessage <> "" Then ErrorMessage = ErrorMessage & ", "
                ErrorMessage = ErrorMessage & "'Account - Opened Within' data must be numeric"
                'Throw New Exception("'Account - Opened Within' data must be numeric")
            End If
        End If
        If Me.TransactionAmountFromDropDownList.SelectedIndex > 0 And Not IsNumeric(TransactionAmountFromTextBox.Text) Then
            cek = 1
            If ErrorMessage <> "" Then ErrorMessage = ErrorMessage & ", "
            ErrorMessage = ErrorMessage & "'Transaction Amount From' must be numeric"
            'Throw New Exception("'Transaction Amount From' must be numeric")
        End If
        If Me.TransactionAmountUntilDropDownList.SelectedIndex > 0 And Not IsNumeric(TransactionAmountUntilTextBox.Text) Then
            cek = 1
            If ErrorMessage <> "" Then ErrorMessage = ErrorMessage & ", "
            ErrorMessage = ErrorMessage & "'Transaction Amount Until' must be numeric"
            'Throw New Exception("'Transaction Amount Until' must be numeric")
        End If
        If Me.TransactionFrequencyCheckBox.Checked = True And Not IsNumeric(TransactionFrequencyTextBox.Text) Then
            cek = 1
            If ErrorMessage <> "" Then ErrorMessage = ErrorMessage & ", "
            ErrorMessage = ErrorMessage & "'Within Transaction' must be numeric"
            'Throw New Exception("'Within Transaction' must be numeric")
        End If
        If Me.TransactionPeriodCheckBox.Checked = True And Not IsNumeric(TransactionPeriodTextBox.Text) Then
            cek = 1
            If ErrorMessage <> "" Then ErrorMessage = ErrorMessage & ", "
            ErrorMessage = ErrorMessage & "'Within Period' must be numeric"
            'Throw New Exception("'Within Period' must be numeric")
        End If

        If cek = 0 Then
            Return True
        Else
            Throw New Exception(ErrorMessage)
            Return False
        End If
    End Function

    Private Function SaveBySU() As Boolean
        Try
            Dim StrSelectedNegativeListCategoryId As String
            Dim StrSelectedAccountType As String
            Dim Counter As Integer

            StrSelectedNegativeListCategoryId = ""
            StrSelectedAccountType = ""
            For Counter = 0 To Me.SourceNegativeListCheckBoxList.Items.Count - 1
                If Me.SourceNegativeListCheckBoxList.Items(Counter).Selected Then
                    StrSelectedNegativeListCategoryId = StrSelectedNegativeListCategoryId & Me.SourceNegativeListCheckBoxList.Items(Counter).Value & ", "
                End If
            Next
            If StrSelectedNegativeListCategoryId.Length > 0 Then
                StrSelectedNegativeListCategoryId = StrSelectedNegativeListCategoryId.Remove(StrSelectedNegativeListCategoryId.Length - 2)
            End If
            For Counter = 0 To Me.SourceAccountTypeCheckBoxList.Items.Count - 1
                If Me.SourceAccountTypeCheckBoxList.Items(Counter).Selected Then
                    StrSelectedAccountType = StrSelectedAccountType & Me.SourceAccountTypeCheckBoxList.Items(Counter).Value & ", "
                End If
            Next
            If StrSelectedAccountType.Length > 0 Then
                StrSelectedAccountType = StrSelectedAccountType.Remove(StrSelectedAccountType.Length - 2)
            End If
            Dim BasicRulesDescription As String
            If Me.BasicRulesDescriptionTextBox.Text.Length > 255 Then
                BasicRulesDescription = Me.BasicRulesDescriptionTextBox.Text.Substring(0, 255)
            Else
                BasicRulesDescription = Me.BasicRulesDescriptionTextBox.Text
            End If
            Dim AccountOpenedWithin As Short
            Dim AuxiliaryTransactionCodeIsIn As Boolean
            Dim TransactionAmountFrom As Decimal
            Dim TransactionAmountUntil As Decimal
            Dim TransactionFrequency As Short
            Dim TransactionPeriodNumber As Short
            Dim TransactionCountryCodeIsIn As Boolean
            If Me.SourceOpenedWithinTextBox.Text = "" Then
                AccountOpenedWithin = 0
            Else
                AccountOpenedWithin = Short.Parse(Me.SourceOpenedWithinTextBox.Text)
            End If
            AuxiliaryTransactionCodeIsIn = (Me.AuxiliaryTransactionCodeDropDownList.SelectedValue = "IN")
            If Me.TransactionAmountFromTextBox.Text = "" Then
                TransactionAmountFrom = 0
            Else
                TransactionAmountFrom = Decimal.Parse(Me.TransactionAmountFromTextBox.Text)
            End If
            If Me.TransactionAmountUntilTextBox.Text = "" Then
                TransactionAmountUntil = 0
            Else
                TransactionAmountUntil = Decimal.Parse(Me.TransactionAmountUntilTextBox.Text)
            End If
            If Me.TransactionFrequencyTextBox.Text = "" Then
                TransactionFrequency = 0
            Else
                TransactionFrequency = Short.Parse(Me.TransactionFrequencyTextBox.Text)
            End If
            If Me.TransactionPeriodTextBox.Text = "" Then
                TransactionPeriodNumber = 0
            Else
                TransactionPeriodNumber = Short.Parse(Me.TransactionPeriodTextBox.Text)
            End If
            TransactionCountryCodeIsIn = (Me.TransactionCountryCodeDropDownList.SelectedValue = "IN")
            Dim oSQLTrans As SqlTransaction = Nothing

            Try
                Using RulesBasicAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.RulesBasicTableAdapter
                    oSQLTrans = BeginTransaction(RulesBasicAdapter)

                    RulesBasicAdapter.Insert(Me.BasicRulesNameTextBox.Text, BasicRulesDescription, _
                        Me.AlertToDropDownList.SelectedValue, Me.RuleEnabledCheckBox.Checked, Me.SourceCustomerOfDropDownList.SelectedValue, _
                        Me.SourceCustomerTypeDropDownList.SelectedValue, StrSelectedNegativeListCategoryId, Me.SourceHighRiskBusinessCheckBox.Checked, _
                        Me.SourceHighRiskCountryCheckBox.Checked, Me.SourceCustomerSubTypeDropDownList.SelectedValue, _
                        Me.SourceBusinessTypeDropDownList.SelectedValue, Me.SourceInternalIndustryCodeDropDownList.SelectedValue, _
                        AccountOpenedWithin, Me.SourceOpenedWithinTypeDropDownList.SelectedValue, StrSelectedAccountType, _
                        Me.SourceAccountStatusDropDownList.SelectedValue, Me.SourceSegmentationDropDownList.SelectedValue, Me.TransactionTypeDropDownList.SelectedValue, _
                        AuxiliaryTransactionCodeIsIn, Me.AuxiliaryTransactionCodeTextBox.Text, Me.TransactionAmountFromDropDownList.SelectedValue, _
                        TransactionAmountFrom, Me.TransactionAmountUntilDropDownList.SelectedValue, TransactionAmountUntil, _
                        Me.TransactionAmountGEKYCTransactionNormalCheckBox.Checked, Me.TransactionDebitORCreditDropDownList.SelectedValue, _
                        Me.TransactionFrequencyCheckBox.Checked, TransactionFrequency, Me.TransactionPeriodCheckBox.Checked, TransactionPeriodNumber, _
                        Me.TransactionPeriodTypeDropDownList.SelectedValue, TransactionCountryCodeIsIn, Me.TransactionCountryCodeTextBox.Text, _
                        Me.TransactionRelatedCIFCheckBox.Checked, Me.StrCaseGroupingBy, StrRulesBasicExpression, Sahassa.AML.Commonly.SessionUserId, Now)

                    Me.InsertAuditTrail(oSQLTrans)
                    oSQLTrans.Commit()
                End Using
            Catch ex As Exception
                If Not oSQLTrans Is Nothing Then
                    oSQLTrans.Rollback()
                End If
                LogError(ex)
                Throw
            Finally
                If Not oSQLTrans Is Nothing Then
                    oSQLTrans.Dispose()
                    oSQLTrans = Nothing
                End If
            End Try


        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Function

    ''' <summary>
    ''' cancel handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Me.Response.Redirect("RulesBasicView.aspx", False)
    End Sub

    Private Sub InsertAuditTrail(ByRef oSQLTrans As SqlTransaction)
        Try
            Dim BasicRulesDescription As String
            If Me.BasicRulesDescriptionTextBox.Text.Length > 255 Then
                BasicRulesDescription = Me.BasicRulesDescriptionTextBox.Text.Substring(0, 255)
            Else
                BasicRulesDescription = Me.BasicRulesDescriptionTextBox.Text
            End If
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(6)
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                SetTransaction(AccessAudit, oSQLTrans)
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "BasicRule", "LastUpdatedDate", "Add", "", Now.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "BasicRule", "BasicRuleName", "Add", "", Me.BasicRulesNameTextBox.Text, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "BasicRule", "Description", "Add", "", BasicRulesDescription, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "BasicRule", "AlertTo", "Add", "", Me.AlertToDropDownList.SelectedItem.ToString(), "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "BasicRule", "Enabled", "Add", "", Me.RuleEnabledCheckBox.Checked.ToString(), "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "BasicRule", "Expression", "Add", "", Me.StrRulesBasicExpression, "Accepted")
            End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Function SaveToPendingApproval() As Boolean
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            Dim BasicRulesName As String = Trim(Me.BasicRulesNameTextBox.Text)

            Using RulesBasic_PendingApprovalAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.RulesBasic_PendingApprovalTableAdapter
                oSQLTrans = BeginTransaction(RulesBasic_PendingApprovalAdapter)

                Dim BasicRulesDescription As String
                If Me.BasicRulesDescriptionTextBox.Text.Length > 255 Then
                    BasicRulesDescription = Me.BasicRulesDescriptionTextBox.Text.Substring(0, 255)
                Else
                    BasicRulesDescription = Me.BasicRulesDescriptionTextBox.Text
                End If

                Dim BasicRulesPendingApprovalID As Integer


                'Tambahkan ke dalam tabel RulesBasic_PendingApproval dengan ModeID = 1 (Add) 
                BasicRulesPendingApprovalID = RulesBasic_PendingApprovalAdapter.RulesBasic_PendingApprovalInsert(BasicRulesName, Now, "Basic Rule Add", 1, Sahassa.AML.Commonly.SessionUserId)
                Dim StrSelectedNegativeListCategoryId As String
                Dim StrSelectedAccountType As String
                Dim Counter As Integer
                StrSelectedNegativeListCategoryId = ""
                StrSelectedAccountType = ""
                For Counter = 0 To Me.SourceNegativeListCheckBoxList.Items.Count - 1
                    If Me.SourceNegativeListCheckBoxList.Items(Counter).Selected Then
                        StrSelectedNegativeListCategoryId = StrSelectedNegativeListCategoryId & Me.SourceNegativeListCheckBoxList.Items(Counter).Value & ", "
                    End If
                Next
                If StrSelectedNegativeListCategoryId.Length > 0 Then
                    StrSelectedNegativeListCategoryId = StrSelectedNegativeListCategoryId.Remove(StrSelectedNegativeListCategoryId.Length - 2)
                End If
                For Counter = 0 To Me.SourceAccountTypeCheckBoxList.Items.Count - 1
                    If Me.SourceAccountTypeCheckBoxList.Items(Counter).Selected Then
                        StrSelectedAccountType = StrSelectedAccountType & Me.SourceAccountTypeCheckBoxList.Items(Counter).Value & ", "
                    End If
                Next
                If StrSelectedAccountType.Length > 0 Then
                    StrSelectedAccountType = StrSelectedAccountType.Remove(StrSelectedAccountType.Length - 2)
                End If

                Dim AccountOpenedWithin As Short
                Dim AuxiliaryTransactionCodeIsIn As Boolean
                Dim TransactionAmountFrom As Decimal
                Dim TransactionAmountUntil As Decimal
                Dim TransactionFrequency As Short
                Dim TransactionPeriodNumber As Short
                Dim TransactionCountryCodeIsIn As Boolean
                If Me.SourceOpenedWithinTextBox.Text = "" Then
                    AccountOpenedWithin = 0
                Else
                    AccountOpenedWithin = Short.Parse(Me.SourceOpenedWithinTextBox.Text)
                End If
                AuxiliaryTransactionCodeIsIn = (Me.AuxiliaryTransactionCodeDropDownList.SelectedValue = "IN")
                If Me.TransactionAmountFromTextBox.Text = "" Then
                    TransactionAmountFrom = 0
                Else
                    TransactionAmountFrom = Decimal.Parse(Me.TransactionAmountFromTextBox.Text)
                End If
                If Me.TransactionAmountUntilTextBox.Text = "" Then
                    TransactionAmountUntil = 0
                Else
                    TransactionAmountUntil = Decimal.Parse(Me.TransactionAmountUntilTextBox.Text)
                End If
                If Me.TransactionFrequencyTextBox.Text = "" Then
                    TransactionFrequency = 0
                Else
                    TransactionFrequency = Short.Parse(Me.TransactionFrequencyTextBox.Text)
                End If
                If Me.TransactionPeriodTextBox.Text = "" Then
                    TransactionPeriodNumber = 0
                Else
                    TransactionPeriodNumber = Short.Parse(Me.TransactionPeriodTextBox.Text)
                End If
                TransactionCountryCodeIsIn = (Me.TransactionCountryCodeDropDownList.SelectedValue = "IN")
                'Tambahkan ke dalam tabel RulesBasic_Approval dengan ModeID = 1 (Add) 
                Using RulesBasic_ApprovalAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.RulesBasicApprovalTableAdapter
                    SetTransaction(RulesBasic_ApprovalAdapter, oSQLTrans)
                    RulesBasic_ApprovalAdapter.Insert(BasicRulesPendingApprovalID, 0, BasicRulesName, BasicRulesDescription, _
                    Me.AlertToDropDownList.SelectedValue, Me.RuleEnabledCheckBox.Checked, Me.SourceCustomerOfDropDownList.SelectedValue, _
                    Me.SourceCustomerTypeDropDownList.SelectedValue, StrSelectedNegativeListCategoryId, Me.SourceHighRiskBusinessCheckBox.Checked, _
                    Me.SourceHighRiskCountryCheckBox.Checked, Me.SourceCustomerSubTypeDropDownList.SelectedValue, _
                    Me.SourceBusinessTypeDropDownList.SelectedValue, Me.SourceInternalIndustryCodeDropDownList.SelectedValue, _
                    AccountOpenedWithin, Me.SourceOpenedWithinTypeDropDownList.SelectedValue, StrSelectedAccountType, _
                    Me.SourceAccountStatusDropDownList.SelectedValue, Me.SourceSegmentationDropDownList.SelectedValue, Me.TransactionTypeDropDownList.SelectedValue, _
                    AuxiliaryTransactionCodeIsIn, Me.AuxiliaryTransactionCodeTextBox.Text, Me.TransactionAmountFromDropDownList.SelectedValue, _
                    TransactionAmountFrom, Me.TransactionAmountUntilDropDownList.SelectedValue, TransactionAmountUntil, _
                    Me.TransactionAmountGEKYCTransactionNormalCheckBox.Checked, Me.TransactionDebitORCreditDropDownList.SelectedValue, _
                    Me.TransactionFrequencyCheckBox.Checked, TransactionFrequency, Me.TransactionPeriodCheckBox.Checked, TransactionPeriodNumber, _
                    Me.TransactionPeriodTypeDropDownList.SelectedValue, TransactionCountryCodeIsIn, Me.TransactionCountryCodeTextBox.Text, _
                    Me.TransactionRelatedCIFCheckBox.Checked, Me.StrCaseGroupingBy, StrRulesBasicExpression, Sahassa.AML.Commonly.SessionUserId, Now, _
                    0, "", Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, _
                    Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, _
                    Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, "", "", Nothing, Nothing)
                End Using
                oSQLTrans.Commit()

                Dim MessagePendingID As Integer = 88801 'MessagePendingID 88801 = Basic Rule Add 
                Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & BasicRulesName, False)

            End Using

        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Rollback()
            End If
            LogError(ex)
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try

    End Function

    ''' <summary>
    ''' update handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Page.Validate()
        If Page.IsValid Then
            Try
                If Me.CheckInputValid() Then
                    StrRulesBasicExpression = Me.BuildExpression()
                    Dim BasicRulesName As String = Trim(Me.BasicRulesNameTextBox.Text)

                    'Periksa apakah RulesBasic Name tersebut sudah ada dalam tabel RulesBasic atau belum
                    Dim DataCount As Integer = 0
                    Using RulesBasicQueryAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.QueriesTableAdapter
                        DataCount = RulesBasicQueryAdapter.RulesBasicGetCountByName(BasicRulesName)
                    End Using
                    'Counter = 0 berarti RulesBasic tersebut belum pernah ada dalam tabel RulesBasic
                    If DataCount = 0 Then
                        'Periksa apakah RulesBasic Name tersebut sudah ada dalam tabel RulesBasic_PendingApproval atau belum


                        Using RulesBasic_PendingApprovalAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.RulesBasic_PendingApprovalTableAdapter
                            DataCount = RulesBasic_PendingApprovalAdapter.CountRulesBasicPendingApproval(BasicRulesName)

                            'Counter = 0 berarti RulesBasic tersebut statusnya tidak dalam pending approval dan boleh ditambahkan dlm tabel RiskRating_Approval
                            If DataCount = 0 Then

                                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                                    Me.SaveBySU()
                                    Me.LblSucces.Visible = True
                                    Me.LblSucces.Text = "Success to Insert Basic Rule."
                                Else
                                    Me.SaveToPendingApproval()
                                End If
                            Else
                                Throw New Exception("Cannot add the following Basic Rule : '" & BasicRulesName & "' because it is currently waiting for approval.")
                            End If
                        End Using
                    Else
                        Throw New Exception("Cannot add the following Basic Rule : '" & BasicRulesName & "' because that Basic Rule Name already exists in the database.")
                    End If
                End If
            Catch ex As Exception
                Me.cvalPageError.IsValid = False
                Me.cvalPageError.ErrorMessage = ex.Message
            End Try
        End If
    End Sub


End Class
