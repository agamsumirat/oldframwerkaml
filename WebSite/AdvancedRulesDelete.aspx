<%@ Page Language="VB" MasterPageFile="~/masterpage.master" AutoEventWireup="false" CodeFile="AdvancedRulesDelete.aspx.vb" Inherits="AdvancedRulesDelete" title="Advanced Rules - Delete" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2" height="231">
        <tr class="formText">
            <td bgcolor="#ffffff" height="24" style="border-top-style: none; border-right-style: none;
                border-left-style: none; border-bottom-style: none" colspan="4">
                <strong><span style="font-size: 18px">Advanced Rules - Delete
                    <hr />
                </span></strong>
                <asp:Label ID="LblSuccess" runat="server" CssClass="validationok" Visible="False"
                    Width="100%"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" height="24" style="border-top-style: none; border-right-style: none;
                border-left-style: none; border-bottom-style: none" width="5">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/validationsign_animate.gif" /></td>
            <td bgcolor="#ffffff" colspan="3" style="border-top-style: none; border-right-style: none;
                border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>The following Advanced Rules will be deleted :</strong></td>
        </tr>
		<tr class="formText">
			<td width="5" bgColor="#ffffff" style="height: 30px">
                </td>
			<td width="20%" bgColor="#ffffff" style="height: 30px">
                Advanced Rules Name</td>
			<td width="5" bgColor="#ffffff" style="height: 30px">:</td>
			<td width="80%" bgColor="#ffffff" style="height: 30px">
                <asp:Label ID="LabelAdvancedRulesName" runat="server"></asp:Label></td>
		</tr>
		<tr class="formText">
			<td bgColor="#ffffff" style="height: 32px">
                </td>
			<td bgColor="#ffffff" style="height: 32px">
                Advanced Rules Description</td>
			<td bgColor="#ffffff" style="height: 32px">:</td>
			<td bgColor="#ffffff" style="height: 32px">
                <asp:Label ID="LabelAdvancedRulesDescription" runat="server"></asp:Label></td>
		</tr>
		<tr class="formText">
			<td bgColor="#ffffff" height="24">
                </td>
			<td bgColor="#ffffff">
                Case Grouping by</td>
			<td bgColor="#ffffff">:</td>
			<td bgColor="#ffffff">
                <asp:Label ID="LabelAdvancedRulesCaseGroupingBy" runat="server"></asp:Label></td>
		</tr>
		<tr class="formText">
			<td bgColor="#ffffff" height="24">&nbsp;</td>
			<td bgColor="#ffffff">
                Enabled</td>
			<td bgColor="#ffffff">:</td>
			<td bgColor="#ffffff">
                <asp:Label ID="LabelEnabled" runat="server"></asp:Label></td>
		</tr>
		<tr class="formText">
			<td bgColor="#ffffff" height="24">&nbsp;</td>
			<td bgColor="#ffffff">
                Expression</td>
			<td bgColor="#ffffff">:</td>
			<td bgColor="#ffffff">
                <asp:Label ID="LabelExpression" runat="server"></asp:Label></td>
		</tr>
		<tr class="formText" bgColor="#dddddd" height="30">
			<td width="15"><IMG height="15" src="images/arrow.gif" width="15"></td>
			<td colSpan="3">
				<table cellSpacing="0" cellPadding="3" border="0">
					<tr>
						<td><asp:imagebutton id="ImageSave" runat="server" CausesValidation="True" SkinID="deleteButton"></asp:imagebutton></td>
						<td><asp:imagebutton id="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton"></asp:imagebutton></td>
					</tr>
				</table>               
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
		</tr>
	</table>

</asp:Content>

