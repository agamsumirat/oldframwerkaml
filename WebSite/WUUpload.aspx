﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="WUUpload.aspx.vb" Inherits="WUUpload" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    <div id="divcontent" class="divcontent">
        <%--<ajax:AjaxPanel ID="a" runat="server" meta:resourcekey="aResource1" 
                                        BackColor="White" BorderColor="White" Width="100%">
                                <asp:ValidationSummary ID="ValidationSummaryunhandle" runat="Server" CssClass="validation" meta:resourcekey="ValidationSummaryunhandleResource1" />
								<asp:ValidationSummary ID="ValidationSummaryhandle" runat="server"  
                                        CssClass="validationok" ForeColor="Black" ValidationGroup="handle" 
                                        meta:resourcekey="ValidationSummaryhandleResource1" BackColor="White" 
                                        Width="18001px"/>
						        </ajax:AjaxPanel>  --%>
        <ajax:AjaxPanel ID="AjaxPanel5" runat="server" Width="100%" meta:resourcekey="AjaxPanel5Resource1">
            <asp:MultiView ID="MultiViewKPIOvertimeUpload" runat="server" ActiveViewIndex="0">
                <asp:View ID="VwUpload" runat="server">
                    <table width="100%" bgcolor="White">
                        <tr>
                            <td height="20px" valign="middle" style="width: 852px" bgcolor="White">
                                <table>
                                    <tr>
                                        <td valign="bottom" align="left" bgcolor="#ffffff" width="15">
                                            <img height="15" src="images/dot_title.gif" width="15">
                                        </td>
                                        <td class="maintitle" valign="bottom" bgcolor="#ffffff">
                                            <asp:Label ID="LabelTitle" runat="server">Western Union UPLOAD</asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="White">
                                <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                    bgcolor="#dddddd" border="2">
                                    <tr id="ModifyRETBDI">
                                        <td valign="top" width="100%" bgcolor="#ffffff" style="height: 132px" id="UploadExcel">
                                            <table cellspacing="4" cellpadding="0" width="100%" border="0">
                                                <tr>
                                                    <td valign="middle" width="100%" nowrap="">
                                                        <table style="width: 100%; height: 100%;">
                                                            <tr>
                                                                <td style="width: 82px; height: 18px">
                                                                </td>
                                                                <td style="width: 61px; height: 18px">
                                                                    &nbsp;
                                                                </td>
                                                                <td align="center" style="height: 18px">
                                                                    &nbsp;
                                                                </td>
                                                                <td style="width: 100%; height: 18px">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 82px; height: 18px">
                                                                </td>
                                                                <td style="width: 61px; height: 18px" align="left">
                                                                    <asp:Label ID="Label2" runat="server">File</asp:Label>
                                                                </td>
                                                                <td align="center" style="height: 18px">
                                                                    :
                                                                </td>
                                                                <td style="width: 100%; height: 18px">
                                                                    &nbsp;<asp:FileUpload ID="FileUploadAttachment" OnClientClick="aspnetForm.encoding = 'multipart/form-data';"
                                                                        ajaxcall="none" runat="server" CssClass="textbox" Width="400px" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 82px; height: 10px">
                                                                </td>
                                                                <td style="width: 61px; height: 10px">
                                                                    <asp:Label ID="Label3" runat="server"> </asp:Label>
                                                                </td>
                                                                <td align="center" style="height: 10px">
                                                                </td>
                                                                <td style="width: 100%; height: 10px">
                                                                    &nbsp;<asp:Label ID="Label36" runat="server" Text=""> </asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 82px; height: 21px">
                                                                </td>
                                                                <td style="width: 61px; height: 21px">
                                                                </td>
                                                                <td align="center" style="height: 21px">
                                                                </td>
                                                                <td style="width: 100%; height: 21px">
                                                                    <%--<ajax:AjaxPanel ID="AjaxPanel11" runat="server" Width="100%" meta:resourcekey="AjaxPanel11Resource1">--%>
                                                                    <asp:ImageButton ID="imgUpload" runat="server" OnClientClick="aspnetForm.encoding = 'multipart/form-data';"
                                                                        ajaxcall="none" ImageUrl="~/Images/button/upload.gif" CausesValidation="false"
                                                                        Style="height: 20px" />
                                                                    <%--</ajax:AjaxPanel> 	--%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 82px; height: 21px">
                                                                    &nbsp;</td>
                                                                <td style="width: 61px; height: 21px">
                                                                    &nbsp;</td>
                                                                <td align="center" style="height: 21px">
                                                                    &nbsp;</td>
                                                                <td style="width: 100%; height: 21px">
                                                                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/FolderTemplate/WUUploadTemplate.xls">Download Template</asp:HyperLink>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 852px;">
                                            <asp:CustomValidator ID="cvalPageErr" runat="server" Display="None"> </asp:CustomValidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:View>
                <asp:View ID="VwSuccessData" runat="server">
                    <table align="center" bgcolor="#dddddd" border="2" bordercolor="#ffffff" cellpadding="2"
                        cellspacing="1" width="100%">
                        <tr id="Tr1">
                            <td id="Td1" bgcolor="#ffffff" style="height: 80px" valign="top" width="100%">
                                <table border="0" cellpadding="0" cellspacing="4" width="100%">
                                    <tr>
                                        <td style="width: 82px; height: 18px">
                                        </td>
                                        <td style="width: 1%; height: 18px; font-weight: bold">
                                            Total Success Data
                                        </td>
                                        <td align="center" style="width: 18px; height: 18px" width="100%">
                                            :
                                        </td>
                                        <td style="width: 100%; height: 18px">
                                            <asp:Label ID="lblValueSuccessData" runat="server">0</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 82px; height: 18px">
                                        </td>
                                        <td align="left" style="width: 1%; height: 18px; font-weight: bold">
                                            Total Failed Data
                                        </td>
                                        <td align="center" style="width: 18px; height: 18px" width="100%">
                                            :
                                        </td>
                                        <td style="width: 100%; height: 18px">
                                            <asp:Label ID="lblValueFailedData" runat="server">0</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 82px; height: 10px">
                                        </td>
                                        <td style="width: 1%; height: 10px; font-weight: bold">
                                            Total Uploaded data
                                        </td>
                                        <td align="center" style="width: 18px; height: 10px" width="100%">
                                            :
                                        </td>
                                        <td style="width: 100%; height: 10px">
                                            <asp:Label ID="lblValueuploadeddata" runat="server">0</asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <table id="tablesukses" width="99%" bgcolor="White">
                        <tr>
                            <td background="images/search-bar-background.gif" valign="middle" style="width: 99%;
                                height: 20px; border-right: gray thin solid; border-top: gray thin solid; border-left: gray thin solid;
                                border-bottom: gray thin solid;">
                                <table cellspacing="0" cellpadding="3" border="0" width="99%">
                                    <tr>
                                        <td valign="bottom" align="left" width="15">
                                            <img height="15" src="images/done.png" width="15">
                                        </td>
                                        <td class="maintitle" valign="bottom">
                                            <asp:Label ID="Label6" runat="server" Text="Success Data">
                                            </asp:Label>
                                            &nbsp;:&nbsp;<asp:Label ID="lbljumlahsukses" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100%;">
                                <table cellspacing="0" cellpadding="3" border="0" width="99%">
                                    <tr>
                                        <td bgcolor="#ffffff" style="width: 99%;">
                                            <asp:DataGrid ID="GridSuccessList" runat="server" CellPadding="4" AllowPaging="True"
                                                Width="100%" GridLines="None" AllowSorting="True" ForeColor="White" BorderColor="#003300"
                                                BorderStyle="Solid" AutoGenerateColumns="False" Font-Bold="False" Font-Italic="False"
                                                Font-Overline="False" Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center">
                                                <PagerStyle HorizontalAlign="Center" ForeColor="White" BackColor="#666666"></PagerStyle>
                                                <HeaderStyle Font-Bold="True" ForeColor="White" Wrap="False" BackColor="#1C5E55"
                                                    Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False"
                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <Columns>
                                                    <asp:BoundColumn HeaderText="No">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="PK_WUUpload_id" HeaderText="PK_WUUpload_id" SortExpression="PK_WUUpload_id asc"
                                                        Visible="False"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="SortedBYvalue" HeaderText="SortedBYvalue" SortExpression="SortedBYvalue asc">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="SortedBYID" HeaderText="SortedBYID">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn HeaderText="SendpayIndicator" DataField="SendpayIndicator">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Account" HeaderText="Account">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="AgentCountryCode" HeaderText="AgentCountryCode">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="AgentCountryName" HeaderText="AgentCountryName">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="MTCN" HeaderText="MTCN">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="TransactionDate" HeaderText="TransactionDate">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="TransactionTime" HeaderText="TransactionTime">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Principal" HeaderText="Principal">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Currency" HeaderText="Currency">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="CurrencyName" HeaderText="CurrencyName">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="customer" HeaderText="customer">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="customerotherside" HeaderText="customerotherside">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="address" HeaderText="address">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="City" HeaderText="City">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Postalcode" HeaderText="Postalcode">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="phone" HeaderText="phone">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="IDType" HeaderText="IDType">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="IDNumber" HeaderText="IDNumber">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="AgentLocation" HeaderText="AgentLocation">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="OtherSideCountrycode" HeaderText="OtherSideCountrycode">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="OtherSideCountryname" HeaderText="OtherSideCountryname">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Source" HeaderText="Source">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="AgentCity" HeaderText="AgentCity">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="WUCardNumber" HeaderText="WUCardNumber">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="ReceiverIsCompany" HeaderText="ReceiverIsCompany">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="SenderIsCompany" HeaderText="SenderIsCompany">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="S_SEN_FIRSTNAME" HeaderText="S_SEN_FIRSTNAME">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="S_SEN_LASTNAME" HeaderText="S_SEN_LASTNAME">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="S_SEN_CITY" HeaderText="S_SEN_CITY">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="S_SEN_ZIP" HeaderText="S_SEN_ZIP">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="S_SEN_ADDRESS" HeaderText="S_SEN_ADDRESS">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="S_REC_FIRSTNAME" HeaderText="S_REC_FIRSTNAME">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="S_REC_LASTNAME" HeaderText="S_REC_LASTNAME">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="S_SEN_PHONE" HeaderText="S_SEN_PHONE">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="S_SEN_IDTYPE" HeaderText="S_SEN_IDTYPE">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="S_SEN_IDISSUER" HeaderText="S_SEN_IDISSUER">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="S_SEN_IDNUMBER" HeaderText="S_SEN_IDNUMBER">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="S_SEN_ISSUEDDATE" HeaderText="S_SEN_ISSUEDDATE">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="S_SEN_EXPDATE" HeaderText="S_SEN_EXPDATE">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="S_SEN_BIRTHDATE" HeaderText="S_SEN_BIRTHDATE">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="S_SEN_OCCUPATION" HeaderText="S_SEN_OCCUPATION">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="S_SEN_COMMENTS" HeaderText="S_SEN_COMMENTS">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="P_REC_FIRSTNAME" HeaderText="P_REC_FIRSTNAME">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="P_REC_LASTNAME" HeaderText="P_REC_LASTNAME">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="P_REC_ADDRESS" HeaderText="P_REC_ADDRESS">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="P_REC_ADDRESS_2" HeaderText="P_REC_ADDRESS_2">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="P_REC_CITY" HeaderText="P_REC_CITY">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="P_REC_ZIP" HeaderText="P_REC_ZIP">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="P_REC_PHONE" HeaderText="P_REC_PHONE">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="P_SEN_FIRSTNAME" HeaderText="P_SEN_FIRSTNAME">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="P_SEN_LASTNAME" HeaderText="P_SEN_LASTNAME">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="P_REC_IDTYPE" HeaderText="P_REC_IDTYPE">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="P_REC_IDISSUER" HeaderText="P_REC_IDISSUER">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="P_REC_IDNUMBER" HeaderText="P_REC_IDNUMBER">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="P_REC_ISSUEDDATE" HeaderText="P_REC_ISSUEDDATE">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="P_REC_EXPDATE" HeaderText="P_REC_EXPDATE">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="P_REC_BIRTHDATE" HeaderText="P_REC_BIRTHDATE">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="P_REC_OCCUPATION" HeaderText="P_REC_OCCUPATION">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="P_REC_COMMENTS" HeaderText="P_REC_COMMENTS">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="S_SEN_IDHASEXP" HeaderText="S_SEN_IDHASEXP">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="S_SEN_HASID" HeaderText="S_SEN_HASID">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="S_SEN_BIRTHCOUNTRY" HeaderText="S_SEN_BIRTHCOUNTRY">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="P_REC_PROVINCE" HeaderText="P_REC_PROVINCE">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="P_REC_COUNTRY" HeaderText="P_REC_COUNTRY">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="P_REC_HASPHONE" HeaderText="P_REC_HASPHONE">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="P_REC_IDHASEXP" HeaderText="P_REC_IDHASEXP">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="P_REC_BIRTHCOUNTRY" HeaderText="P_REC_BIRTHCOUNTRY">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="P_SENDERS_BIRTHCOUNTRY" HeaderText="P_SENDERS_BIRTHCOUNTRY">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="P_SENDERS_BIRTHDATE" HeaderText="P_SENDERS_BIRTHDATE">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="P_SENDERS_ADDRESS" HeaderText="P_SENDERS_ADDRESS">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="P_SENDERS_CITY" HeaderText="P_SENDERS_CITY">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="P_SENDERS_ZIP" HeaderText="P_SENDERS_ZIP">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="P_SENDERS_COUNTRY" HeaderText="P_SENDERS_COUNTRY">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="S_SEN_VERIFIED_CUST_DATA" HeaderText="S_SEN_VERIFIED_CUST_DATA">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="P_REC_VERIFIED_CUST_DATA" HeaderText="P_REC_VERIFIED_CUST_DATA">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="S_SEN_NATIONALITY" HeaderText="S_SEN_NATIONALITY">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="S_SEN_GENDER" HeaderText="S_SEN_GENDER">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="S_SEN_EMPLOYER_NAME" HeaderText="S_SEN_EMPLOYER_NAME">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="S_SEN_WORK_NATURE" HeaderText="S_SEN_WORK_NATURE">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="S_SEN_REASONOFTRANS" HeaderText="S_SEN_REASONOFTRANS">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="P_REC_NATIONALITY" HeaderText="P_REC_NATIONALITY">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="P_REC_GENDER" HeaderText="P_REC_GENDER">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="P_REC_EMPLOYER_NAME" HeaderText="P_REC_EMPLOYER_NAME">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="P_REC_WORK_NATURE" HeaderText="P_REC_WORK_NATURE">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="P_REC_REASONOFTRANS" HeaderText="P_REC_REASONOFTRANS">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="ROWCOLOR" HeaderText="ROWCOLOR">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="COLCOLOR" HeaderText="COLCOLOR">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="GENDT" HeaderText="GENDT">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundColumn>
                                                </Columns>
                                                <EditItemStyle BackColor="#7C6F57" />
                                                <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                                                <SelectedItemStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                                                <AlternatingItemStyle BackColor="White" />
                                                <ItemStyle BackColor="#E3EAEB" />
                                            </asp:DataGrid>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <table id="tableanomaly" bgcolor="White" style="width: 100%">
                        <tr>
                            <td background="images/search-bar-background.gif" valign="middle" style="width: 99%;
                                height: 20px; border-right: gray thin solid; border-top: gray thin solid; border-left: gray thin solid;
                                border-bottom: gray thin solid;" bgcolor="White" width="100%">
                                <table cellspacing="0" cellpadding="3" border="0" width="100%">
                                    <tr>
                                        <td valign="bottom" align="left" width="15">
                                            <img height="15" src="images/warning.png" width="15">
                                        </td>
                                        <td class="maintitle" valign="bottom">
                                            <asp:Label ID="Label4" runat="server" Text="Anomaly Data"></asp:Label>
                                            &nbsp;:&nbsp;<asp:Label ID="lbljumlahanomalydata" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100%;" bgcolor="White" width="100%">
                                <table cellspacing="0" cellpadding="3" border="0" width="100%">
                                    <tr>
                                        <td bgcolor="#ffffff" style="width: 99%;">
                                            <asp:DataGrid ID="GridAnomalyList" runat="server" CellPadding="4" Width="100%" GridLines="None"
                                                AllowSorting="True" ForeColor="#333333" AutoGenerateColumns="False" BorderColor="Maroon"
                                                BorderStyle="Solid" BackColor="White" AllowPaging="True">
                                                <PagerStyle HorizontalAlign="Center" ForeColor="#333333" BackColor="#FFCC66"></PagerStyle>
                                                <HeaderStyle Font-Bold="True" ForeColor="White" Wrap="False" BackColor="#990000" />
                                                <Columns>
                                                    <asp:BoundColumn DataField="Error_description" HeaderText="Description">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="RecordNumber" HeaderText="Error Line Number">
                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" ForeColor="White" />
                                                        <ItemStyle Width="20%" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn Visible="false" DataField="SortedBYvalue" HeaderText="SortedBYvalue" />
                                                    <asp:BoundColumn Visible="false" DataField="SortedBYID" HeaderText="SortedBYID" />
                                                    <asp:BoundColumn Visible="false" DataField="SendpayIndicator" HeaderText="SendpayIndicator"  />
                                                    <asp:BoundColumn Visible="false" DataField="Account" HeaderText="Account" />
                                                    <asp:BoundColumn Visible="false" DataField="AgentCountryCode" HeaderText="AgentCountryCode" />
                                                    <asp:BoundColumn Visible="false" DataField="AgentCountryName" HeaderText="AgentCountryName" />
                                                    <asp:BoundColumn Visible="false" DataField="MTCN" HeaderText="MTCN" />
                                                    <asp:BoundColumn Visible="false" DataField="TransactionDate" HeaderText="TransactionDate" />
                                                    <asp:BoundColumn Visible="false" DataField="TransactionTime" HeaderText="TransactionTime" />
                                                    <asp:BoundColumn Visible="false" DataField="Principal" HeaderText="Principal" />
                                                    <asp:BoundColumn Visible="false" DataField="Currency" HeaderText="Currency" />
                                                    <asp:BoundColumn Visible="false" DataField="CurrencyName" HeaderText="CurrencyName" />
                                                    <asp:BoundColumn Visible="false" DataField="customer" HeaderText="customer" />
                                                    <asp:BoundColumn Visible="false" DataField="customerotherside" HeaderText="customerotherside" />
                                                    <asp:BoundColumn Visible="false" DataField="address" HeaderText="address" />
                                                    <asp:BoundColumn Visible="false" DataField="City" HeaderText="City" />
                                                    <asp:BoundColumn Visible="false" DataField="Postalcode" HeaderText="Postalcode" />
                                                    <asp:BoundColumn Visible="false" DataField="phone" HeaderText="phone" />
                                                    <asp:BoundColumn Visible="false" DataField="IDType" HeaderText="IDType" />
                                                    <asp:BoundColumn Visible="false" DataField="IDNumber" HeaderText="IDNumber" />
                                                    <asp:BoundColumn Visible="false" DataField="AgentLocation" HeaderText="AgentLocation" />
                                                    <asp:BoundColumn Visible="false" DataField="OtherSideCountrycode" HeaderText="OtherSideCountrycode" />
                                                    <asp:BoundColumn Visible="false" DataField="OtherSideCountryname" HeaderText="OtherSideCountryname" />
                                                    <asp:BoundColumn Visible="false" DataField="Source" HeaderText="Source" />
                                                    <asp:BoundColumn Visible="false" DataField="AgentCity" HeaderText="AgentCity" />
                                                    <asp:BoundColumn Visible="false" DataField="WUCardNumber" HeaderText="WUCardNumber" />
                                                    <asp:BoundColumn Visible="false" DataField="ReceiverIsCompany" HeaderText="ReceiverIsCompany" />
                                                    <asp:BoundColumn Visible="false" DataField="SenderIsCompany" HeaderText="SenderIsCompany" />
                                                    <asp:BoundColumn Visible="false" DataField="S_SEN_FIRSTNAME" HeaderText="S_SEN_FIRSTNAME" />
                                                    <asp:BoundColumn Visible="false" DataField="S_SEN_LASTNAME" HeaderText="S_SEN_LASTNAME" />
                                                    <asp:BoundColumn Visible="false" DataField="S_SEN_CITY" HeaderText="S_SEN_CITY" />
                                                    <asp:BoundColumn Visible="false" DataField="S_SEN_ZIP" HeaderText="S_SEN_ZIP" />
                                                    <asp:BoundColumn Visible="false" DataField="S_SEN_ADDRESS" HeaderText="S_SEN_ADDRESS" />
                                                    <asp:BoundColumn Visible="false" DataField="S_REC_FIRSTNAME" HeaderText="S_REC_FIRSTNAME" />
                                                    <asp:BoundColumn Visible="false" DataField="S_REC_LASTNAME" HeaderText="S_REC_LASTNAME" />
                                                    <asp:BoundColumn Visible="false" DataField="S_SEN_PHONE" HeaderText="S_SEN_PHONE" />
                                                    <asp:BoundColumn Visible="false" DataField="S_SEN_IDTYPE" HeaderText="S_SEN_IDTYPE" />
                                                    <asp:BoundColumn Visible="false" DataField="S_SEN_IDISSUER" HeaderText="S_SEN_IDISSUER" />
                                                    <asp:BoundColumn Visible="false" DataField="S_SEN_IDNUMBER" HeaderText="S_SEN_IDNUMBER" />
                                                    <asp:BoundColumn Visible="false" DataField="S_SEN_ISSUEDDATE" HeaderText="S_SEN_ISSUEDDATE" />
                                                    <asp:BoundColumn Visible="false" DataField="S_SEN_EXPDATE" HeaderText="S_SEN_EXPDATE" />
                                                    <asp:BoundColumn Visible="false" DataField="S_SEN_BIRTHDATE" HeaderText="S_SEN_BIRTHDATE" />
                                                    <asp:BoundColumn Visible="false" DataField="S_SEN_OCCUPATION" HeaderText="S_SEN_OCCUPATION" />
                                                    <asp:BoundColumn Visible="false" DataField="S_SEN_COMMENTS" HeaderText="S_SEN_COMMENTS" />
                                                    <asp:BoundColumn Visible="false" DataField="P_REC_FIRSTNAME" HeaderText="P_REC_FIRSTNAME" />
                                                    <asp:BoundColumn Visible="false" DataField="P_REC_LASTNAME" HeaderText="P_REC_LASTNAME" />
                                                    <asp:BoundColumn Visible="false" DataField="P_REC_ADDRESS" HeaderText="P_REC_ADDRESS" />
                                                    <asp:BoundColumn Visible="false" DataField="P_REC_ADDRESS_2" HeaderText="P_REC_ADDRESS_2" />
                                                    <asp:BoundColumn Visible="false" DataField="P_REC_CITY" HeaderText="P_REC_CITY" />
                                                    <asp:BoundColumn Visible="false" DataField="P_REC_ZIP" HeaderText="P_REC_ZIP" />
                                                    <asp:BoundColumn Visible="false" DataField="P_REC_PHONE" HeaderText="P_REC_PHONE" />
                                                    <asp:BoundColumn Visible="false" DataField="P_SEN_FIRSTNAME" HeaderText="P_SEN_FIRSTNAME" />
                                                    <asp:BoundColumn Visible="false" DataField="P_SEN_LASTNAME" HeaderText="P_SEN_LASTNAME" />
                                                    <asp:BoundColumn Visible="false" DataField="P_REC_IDTYPE" HeaderText="P_REC_IDTYPE" />
                                                    <asp:BoundColumn Visible="false" DataField="P_REC_IDISSUER" HeaderText="P_REC_IDISSUER" />
                                                    <asp:BoundColumn Visible="false" DataField="P_REC_IDNUMBER" HeaderText="P_REC_IDNUMBER" />
                                                    <asp:BoundColumn Visible="false" DataField="P_REC_ISSUEDDATE" HeaderText="P_REC_ISSUEDDATE" />
                                                    <asp:BoundColumn Visible="false" DataField="P_REC_EXPDATE" HeaderText="P_REC_EXPDATE" />
                                                    <asp:BoundColumn Visible="false" DataField="P_REC_BIRTHDATE" HeaderText="P_REC_BIRTHDATE" />
                                                    <asp:BoundColumn Visible="false" DataField="P_REC_OCCUPATION" HeaderText="P_REC_OCCUPATION" />
                                                    <asp:BoundColumn Visible="false" DataField="P_REC_COMMENTS" HeaderText="P_REC_COMMENTS" />
                                                    <asp:BoundColumn Visible="false" DataField="S_SEN_IDHASEXP" HeaderText="S_SEN_IDHASEXP" />
                                                    <asp:BoundColumn Visible="false" DataField="S_SEN_HASID" HeaderText="S_SEN_HASID" />
                                                    <asp:BoundColumn Visible="false" DataField="S_SEN_BIRTHCOUNTRY" HeaderText="S_SEN_BIRTHCOUNTRY" />
                                                    <asp:BoundColumn Visible="false" DataField="P_REC_PROVINCE" HeaderText="P_REC_PROVINCE" />
                                                    <asp:BoundColumn Visible="false" DataField="P_REC_COUNTRY" HeaderText="P_REC_COUNTRY" />
                                                    <asp:BoundColumn Visible="false" DataField="P_REC_HASPHONE" HeaderText="P_REC_HASPHONE" />
                                                    <asp:BoundColumn Visible="false" DataField="P_REC_IDHASEXP" HeaderText="P_REC_IDHASEXP" />
                                                    <asp:BoundColumn Visible="false" DataField="P_REC_BIRTHCOUNTRY" HeaderText="P_REC_BIRTHCOUNTRY" />
                                                    <asp:BoundColumn Visible="false" DataField="P_SENDERS_BIRTHCOUNTRY" HeaderText="P_SENDERS_BIRTHCOUNTRY" />
                                                    <asp:BoundColumn Visible="false" DataField="P_SENDERS_BIRTHDATE" HeaderText="P_SENDERS_BIRTHDATE" />
                                                    <asp:BoundColumn Visible="false" DataField="P_SENDERS_ADDRESS" HeaderText="P_SENDERS_ADDRESS" />
                                                    <asp:BoundColumn Visible="false" DataField="P_SENDERS_CITY" HeaderText="P_SENDERS_CITY" />
                                                    <asp:BoundColumn Visible="false" DataField="P_SENDERS_ZIP" HeaderText="P_SENDERS_ZIP" />
                                                    <asp:BoundColumn Visible="false" DataField="P_SENDERS_COUNTRY" HeaderText="P_SENDERS_COUNTRY" />
                                                    <asp:BoundColumn Visible="false" DataField="S_SEN_VERIFIED_CUST_DATA" HeaderText="S_SEN_VERIFIED_CUST_DATA" />
                                                    <asp:BoundColumn Visible="false" DataField="P_REC_VERIFIED_CUST_DATA" HeaderText="P_REC_VERIFIED_CUST_DATA" />
                                                    <asp:BoundColumn Visible="false" DataField="S_SEN_NATIONALITY" HeaderText="S_SEN_NATIONALITY" />
                                                    <asp:BoundColumn Visible="false" DataField="S_SEN_GENDER" HeaderText="S_SEN_GENDER" />
                                                    <asp:BoundColumn Visible="false" DataField="S_SEN_EMPLOYER_NAME" HeaderText="S_SEN_EMPLOYER_NAME" />
                                                    <asp:BoundColumn Visible="false" DataField="S_SEN_WORK_NATURE" HeaderText="S_SEN_WORK_NATURE" />
                                                    <asp:BoundColumn Visible="false" DataField="S_SEN_REASONOFTRANS" HeaderText="S_SEN_REASONOFTRANS" />
                                                    <asp:BoundColumn Visible="false" DataField="P_REC_NATIONALITY" HeaderText="P_REC_NATIONALITY" />
                                                    <asp:BoundColumn Visible="false" DataField="P_REC_GENDER" HeaderText="P_REC_GENDER" />
                                                    <asp:BoundColumn Visible="false" DataField="P_REC_EMPLOYER_NAME" HeaderText="P_REC_EMPLOYER_NAME" />
                                                    <asp:BoundColumn Visible="false" DataField="P_REC_WORK_NATURE" HeaderText="P_REC_WORK_NATURE" />
                                                    <asp:BoundColumn Visible="false" DataField="P_REC_REASONOFTRANS" HeaderText="P_REC_REASONOFTRANS" />
                                                    <asp:BoundColumn Visible="false" DataField="ROWCOLOR" HeaderText="ROWCOLOR" />
                                                    <asp:BoundColumn Visible="false" DataField="COLCOLOR" HeaderText="COLCOLOR" />
                                                    <asp:BoundColumn Visible="false" DataField="GENDT" HeaderText="GENDT" />
                                                </Columns>
                                                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                <SelectedItemStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                <AlternatingItemStyle BackColor="White" />
                                                <ItemStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                            </asp:DataGrid>
                                            <asp:Button ID="BtnExportExcel" runat="server" Font-Names="Calibri" Height="20px"
                                                Text="Export Annomaly Data To Excel" Width="209px" ajaxcall="none" />
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <asp:Button ID="BtnSave" runat="server" Font-Names="Calibri" Height="20px" Text="Save"
                                                Width="51px" />
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <asp:Button ID="BtnCancel" runat="server" Font-Names="Calibri" Height="20px" Text="Cancel"
                                                Width="51px" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="White" width="100%">
                            </td>
                        </tr>
                    </table>
                </asp:View>
                <asp:View ID="VwConfirmation" runat="server">
                    <table style="width: 100%" bordercolor="#ffffff" cellspacing="1" cellpadding="2"
                        width="100%" bgcolor="#dddddd" border="0">
                        <tr bgcolor="#ffffff">
                            <td colspan="2" align="center" style="height: 17px">
                                <asp:Label ID="LblConfirmation" runat="server" meta:resourcekey="LblConfirmationResource1"></asp:Label>
                            </td>
                        </tr>
                        <tr bgcolor="#ffffff">
                            <td align="center" colspan="2">
                                <asp:ImageButton ID="ImgBtnAdd" runat="server" ImageUrl="~/images/button/Ok.gif"
                                    CausesValidation="False" meta:resourcekey="ImgBtnAddResource1" />
                            </td>
                        </tr>
                    </table>
                </asp:View>
            </asp:MultiView>
        </ajax:AjaxPanel>
        <ajax:AjaxPanel ID="AjaxPanel14" runat="server" meta:resourcekey="AjaxPanel14Resource1">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" background="Images/button-bground.gif" valign="middle">
                        <img height="1" src="Images/blank.gif" width="5" />
                    </td>
                    <td align="left" background="Images/button-bground.gif" valign="middle">
                        <img height="15" src="images/arrow.gif" width="15" />&nbsp;
                    </td>
                    <td background="Images/button-bground.gif" style="width: 5px" align="center" valign="middle">
                        <asp:ImageButton ID="Imgbtnsave" runat="server" ImageUrl="~/images/button/save.gif"
                            CausesValidation="False" meta:resourcekey="ImgBackAddResource1" Visible="False" />
                    </td>
                    <td background="Images/button-bground.gif">
                        <asp:ImageButton ID="Imgbtncancel" runat="server" ImageUrl="~/images/button/cancel.gif"
                            CausesValidation="False" meta:resourcekey="ImgBackAddResource1" Visible="False"
                            Style="margin-left: 4px" />
                    </td>
                    <td background="Images/button-bground.gif" width="99%">
                        <img height="1" src="Images/blank.gif" width="1" />
                    </td>
                    <td style="width: 25px">
                    </td>
                </tr>
            </table>
            <asp:CustomValidator ID="CvalHandleErr" runat="server" Display="None" ValidationGroup="handle"
                meta:resourcekey="CvalHandleErrResource1"></asp:CustomValidator>
            <asp:CustomValidator ID="CustomValidator1" runat="server" Display="None" meta:resourcekey="CvalPageErrResource1"> </asp:CustomValidator>
        </ajax:AjaxPanel>
    </div>
</asp:Content>
