<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="SignInProblemHelp.aspx.vb" Inherits="SignInProblemHelp" title="Sign In Problem Help" %>
<%--<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/logo_niaga.gif" />
</asp:Content>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
    <table style="width: 916px; height: 184px">
        <tr>
            <td align="left" colspan="1" style="width: 10px">
            </td>
            <td colspan="3" align="left" style="width: 654px; text-align: justify;">
                <br />
                If you're having problem with signing-in to the Anti Money Laundering (AML) &nbsp;System
                then perhaps these following tips can be helpful:<br />
                <br />
                * Make sure you have entered your <em>username</em> and <em>password</em> correctly.
                Both <em>username</em> and <em>password</em> are case-sensitive meaning that uppercase
                character is considered different from lowercase character.
                <br />
                * Contact your System Administrator if you're certain that you've entered
                your username and password correctly but still cannot sign-in to the AML 
                System.<br />
                <br />
                </td>
        </tr>
        <tr>
            <td colspan="1" style="width: 8px">
            </td>
            <td colspan="3" style="width: 654px">
                <asp:HyperLink ID="HyperLinkBack" runat="server" NavigateUrl="~/Login.aspx" Target="_self">[Back to Login Page]</asp:HyperLink><br />
                <br />
                <br />
                <br />
            </td>
        </tr>
    </table>
</asp:Content>
<%--<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder4" Runat="Server">
    <table id="FooterTable" style="width: 100%">
        <tr>
            <td colspan="3" rowspan="3" style="height: 74px; text-align: right">
                <hr color="#f40101" noshade="noshade" size="1" />
                    Copyright � Bank Niaga 2006. All rights reserved.<br />
                    Version 1.0.0 (Build 1235)<br />
                    Developed by
                <asp:HyperLink ID="HyperLinkSahassaWebsite" runat="server" Font-Italic="True" Font-Underline="True"
                    NavigateUrl="http://www.sahassa.com" TabIndex="5" Target="_blank">Sahassa</asp:HyperLink></td>
        </tr>
        <tr>
        </tr>
        <tr>
        </tr>
    </table>
</asp:Content>--%>