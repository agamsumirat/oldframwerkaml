<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Settings.aspx.vb" Inherits="Settings" title="Settings" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
	<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2" height="231">
        <tr class="formText">
            <td bgcolor="#ffffff" colspan="4">
                <strong><span style="font-size: 18px">Settings
                    <hr />
                </span></strong>
                <asp:Label ID="LblSuccess" runat="server" CssClass="validationok" Visible="False"
                    Width="100%"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" colspan="4"><span style="color: #ff0000">* Required</span></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" height="24" width="5" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
            </td>
            <td bgcolor="#ffffff" colspan="3">
                <asp:Panel ID="PanelGeneralSettings" runat="server" GroupingText="General Settings" Width="582px">
                    <ajax:AjaxPanel ID="AjaxPanel1" runat="server" Width="510px" Height="60px">
                <table style="width: 400px; height: 30px" border="0" cellpadding="0" cellspacing="0" runat="server">
                    <tr id="pilih1">
                        <td style="width: 45%; height: 30px">
                    Display record per page</td>
                          <td style="width: 15; height: 30px" align="center">
                            :
                        </td>
                        <td style="width: 55%; height: 30px">
                    &nbsp;<asp:DropDownList ID="DropDownListPagingLimit" runat="server" AutoPostBack="True" CssClass="searcheditcbo"
                        TabIndex="1" Width="177px">
                        <asp:ListItem Selected="True">10</asp:ListItem>
                        <asp:ListItem>15</asp:ListItem>
                        <asp:ListItem>20</asp:ListItem>
                        <asp:ListItem>25</asp:ListItem>
                        <asp:ListItem>50</asp:ListItem>
                        <asp:ListItem>100</asp:ListItem>
                        <asp:ListItem Value="-1">Enter my setting</asp:ListItem>
                    </asp:DropDownList></td>
                    </tr>
                    <tr id="pilih2" runat="server">
                        <td style="width: 40%; height: 30px">
                            Enter display record per page</td>
                        <td align="center" style="width: 15px; height: 30px">
                            :</td>
                        <td style="width: 60%; height: 30px">
                            &nbsp;<asp:TextBox ID="TextBoxPagingLimit" runat="server" CssClass="textBox"></asp:TextBox>
                            </td>
                    </tr>
                </table><strong><span style="color: #ff0000">             
                </span></strong>              
            </ajax:AjaxPanel>
                </asp:Panel>
            </td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" height="24" width="5" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
            </td>
            <td bgcolor="#ffffff" colspan="3" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
            </td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" height="24" width="5">
                <br />
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorFirstName" runat="server" ControlToValidate="TextUserName"
                    Display="None" ErrorMessage="User Name cannot be blank">*</asp:RequiredFieldValidator><br />
                <asp:RegularExpressionValidator ID="RegularExpressionValidatorFirstName" runat="server"
                    ControlToValidate="TextUserName" ErrorMessage="User Name must starts with a letter then it can be followed by other characters."
                    ValidationExpression="[a-zA-Z](\w*\s*)*" Display="None">*</asp:RegularExpressionValidator><br />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TextboxEmailAddr"
                    Display="None" ErrorMessage="Email Address cannot be blank ">*</asp:RequiredFieldValidator><br />
                <asp:RegularExpressionValidator
                        ID="RegularExpressionValidatorEmailAddress" runat="server" ControlToValidate="TextboxEmailAddr"
                        ErrorMessage="Email format is incorrect" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="None">*</asp:RegularExpressionValidator><br />
                <asp:RegularExpressionValidator ID="RegularExpressionValidatorMobilePhone" runat="server"
                    ControlToValidate="TextboxMobilePhone" ErrorMessage="Mobile Phone must be digits only and at least 8 digits long"
                    ValidationExpression="\d{8}\d*" Display="None">*</asp:RegularExpressionValidator><br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" colspan="3">
                <asp:Panel ID="PanelUserInformation" runat="server" GroupingText="User Information" Width="582px">
                     <table style="width: 582px; height: 81px" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 20%; height: 30px">
                    User ID</td>
                          <td style="width: 15; height: 30px" align="center">
                            :
                        </td>
                        <td style="width: 80%; height: 30px">
                    &nbsp;<asp:Label ID="LabelUserId" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 20%; height: 30px">
                User Name</td>
                        <td style="width: 15; height: 30px" align="center">
                            :
                        </td>
                        <td style="width: 80%; height: 30px">
                        &nbsp;<asp:TextBox ID="TextUserName" runat="server" CssClass="textBox" MaxLength="50"
                                Width="200px"></asp:TextBox>
                            <strong><span style="color: #ff0000">*</span></strong></td>
                    </tr>
                    <tr>
                        <td style="width: 20%; height: 30px">
                Email Address</td>
                        <td style="width: 15; height: 30px" align="center">
                            :
                        </td>
                        <td style="width: 80%; height: 30px">
                            &nbsp;<asp:TextBox ID="TextboxEmailAddr" runat="server" CssClass="textBox" MaxLength="50"
                                Width="200px"></asp:TextBox>
                <strong><span style="color: #ff0000">*</span></strong></td>
                    </tr>
                    <tr>
                        <td style="width: 20%; height: 30px">
                Mobile Phone</td>
                        <td style="width: 15; height: 30px" align="center">
                            :
                        </td>
                        <td style="width: 80%; height: 30px">
                            &nbsp;<asp:TextBox ID="TextboxMobilePhone" runat="server" CssClass="textBox" MaxLength="15"
                                Width="200px"></asp:TextBox>
                </td>
                    </tr>
                         <tr>
                             <td style="width: 20%; height: 30px">
                <asp:LinkButton ID="LinkButtonChangePassword" runat="server" CausesValidation="False">Change password</asp:LinkButton></td>
                             <td align="center" style="width: 15px; height: 30px">
                             </td>
                             <td style="width: 80%; height: 30px">
                             </td>
                         </tr>
                </table>
                </asp:Panel>
            </td>
        </tr>
		<tr class="formText" bgColor="#dddddd" height="30">
			<td width="15"><IMG height="15" src="images/arrow.gif" width="15"></td>
			<td colSpan="3">
				<table cellSpacing="0" cellPadding="3" border="0">
					<tr>
						<td><asp:imagebutton id="ImageSave" runat="server" CausesValidation="True" SkinID="saveButton"></asp:imagebutton></td>
						<td><asp:imagebutton id="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton"></asp:imagebutton></td>
					</tr>
				</table>               
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
		</tr>
	</table>
	<script>
	    document.getElementById('<%=GetFocusID %>').focus();
	</script>
</asp:Content>