﻿<%@ Page Title="" Language="VB" MasterPageFile="~/CDDLNPPrintMasterPage.master" AutoEventWireup="false"
    CodeFile="CDDLNPPrint.aspx.vb" Inherits="CDDLNPPrint" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td align="left" valign="middle">
                &nbsp;&nbsp;&nbsp;&nbsp;<img src="images/logo.gif" />
            </td>
        </tr>
        <tr id="trprint">
            <td align="right" valign="middle">
                <asp:ImageButton ID="ImagePrint" runat="server" CausesValidation="False" ImageUrl="~/Images/Button/Print.gif"
                    OnClientClick="javascript:printdetail()"></asp:ImageButton>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </td>
        </tr>
        <tr>
            <td align="center" valign="middle">
                <asp:Label ID="lblHeader" runat="server" Font-Size="Large" Font-Bold="true" Font-Underline="true"></asp:Label></td>
        </tr>
        <tr>
            <td id="tdcontent" height="99%" valign="top" bgcolor="#FFFFFF">
                <div>
                    <table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td class="divcontentinside" bgcolor="#FFFFFF">
                                <ajax:AjaxPanel ID="AjaxPanel1" runat="server">
                                    <asp:Table ID="TableCategory" runat="server" Width="100%" CellSpacing="0" CellPadding="4">
                                    </asp:Table>
                                </ajax:AjaxPanel>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr style="page-break-before: always">
            <td>
                <table cellpadding="4" cellspacing="0" border="1" width="100%" bordercolor="black">
                    <tr>
                        <td align="center" valign="middle" colspan="4">
                            <asp:Label ID="Label1" runat="server" Font-Size="Large" Font-Bold="true">PERSETUJUAN</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td id="tdApproval1" runat="server" align="center" valign="middle" width="25%">
                            <asp:Label ID="Label2" runat="server" Font-Size="small" Font-Bold="true">EDD dilakukan oleh</asp:Label>
                        </td>
                        <td id="tdApproval2" runat="server" align="center" valign="middle" width="25%">
                            <asp:Label ID="Label3" runat="server" Font-Size="small" Font-Bold="true">EDD direview oleh</asp:Label>
                        </td>
                        <td id="tdApproval3" runat="server" align="center" valign="middle" width="25%">
                            <asp:Label ID="Label4" runat="server" Font-Size="small" Font-Bold="true">EDD direview oleh</asp:Label>
                        </td>
                        <td id="tdApproval4" runat="server" align="center" valign="middle" width="25%">
                            <asp:Label ID="Label5" runat="server" Font-Size="small" Font-Bold="true">EDD direview oleh</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td id="tdStatement1" runat="server" align="left" valign="top" width="25%">
                            <asp:Label ID="lblStatement1" runat="server"></asp:Label>&nbsp;
                        </td>
                        <td id="tdStatement2" runat="server" align="left" valign="top" width="25%">
                            <asp:Label ID="lblStatement2" runat="server"></asp:Label>&nbsp;
                        </td>
                        <td id="tdStatement3" runat="server" align="left" valign="top" width="25%">
                            <asp:Label ID="lblStatement3" runat="server"></asp:Label>&nbsp;
                        </td>
                        <td id="tdStatement4" runat="server" align="left" valign="top" width="25%">
                            <asp:Label ID="lblStatement4" runat="server"></asp:Label>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td id="tdUser1" runat="server" align="left" valign="top" width="25%">
                            <asp:Label ID="lblLevel1" runat="server"></asp:Label>
                            :
                            <br />
                            <asp:Label ID="lblUser1" runat="server"></asp:Label>
                        </td>
                        <td id="tdUser2" runat="server" align="left" valign="top" width="25%">
                            <asp:Label ID="lblLevel2" runat="server"></asp:Label>
                            :
                            <br />
                            <asp:Label ID="lblUser2" runat="server"></asp:Label>
                        </td>
                        <td id="tdUser3" runat="server" align="left" valign="top" width="25%">
                            <asp:Label ID="lblLevel3" runat="server"></asp:Label>
                            :
                            <br />
                            <asp:Label ID="lblUser3" runat="server"></asp:Label>
                        </td>
                        <td id="tdUser4" runat="server" align="left" valign="top" width="25%">
                            <asp:Label ID="lblLevel4" runat="server"></asp:Label>
                            :
                            <br />
                            <asp:Label ID="lblUser4" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td id="tdDate1" runat="server" align="left" valign="top" width="25%">
                            Tanggal :
                            <asp:Label ID="lblDate1" runat="server"></asp:Label>
                        </td>
                        <td id="tdDate2" runat="server" align="left" valign="top" width="25%">
                            Tanggal :
                            <asp:Label ID="lblDate2" runat="server"></asp:Label>
                        </td>
                        <td id="tdDate3" runat="server" align="left" valign="top" width="25%">
                            Tanggal :
                            <asp:Label ID="lblDate3" runat="server"></asp:Label>
                        </td>
                        <td id="tdDate4" runat="server" align="left" valign="top" width="25%">
                            Tanggal :
                            <asp:Label ID="lblDate4" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <script language="javascript">
				function printdetail()			
					{
						window.print();
						window.opener='';
						window.close(); 		
					}
				<%--setTimeout('printdetail()',1000);--%>
    </script>

</asp:Content>
