#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports System.Collections.Generic
Imports Sahassa.AML
#End Region

Partial Class MsKotaKab_Detail
	Inherits Parent

#Region "Function"

	Sub ChangeMultiView(index As Integer)
		MtvMsUser.ActiveViewIndex = index
	End Sub



	Private Sub LoadData()
		Dim ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetPaged(MsKotaKabColumn.IDKotaKab.ToString & "=" & parID, "", 0, 1, Nothing)(0)
		If  ObjMsKotaKab Is Nothing Then Throw New Exception("Data Not Found")
		With ObjMsKotaKab
			SafeDefaultValue = "-"
		  HFProvince.value = Safe(.IDPropinsi)
dim OProvince as  MsProvince = Datarepository.MsProvinceProvider.GetByIdProvince(.IDPropinsi.GetValueOrDefault)
if OProvince isnot nothing then LBSearchNama.text = safe(OProvince.Nama)

 lblIDKotaKab.Text = .IDKotaKab
lblNamaKotaKab.Text = Safe(.NamaKotaKab)

		  'other info
            Dim Omsuser As TList(Of User)
            Omsuser = DataRepository.UserProvider.GetByUserID(.CreatedBy)
            If Omsuser.Count > 0 Then
                lblCreatedBy.Text = Omsuser(0).UserName
            End If
            lblCreatedDate.Text = FormatDate(.CreatedDate)
            Omsuser = DataRepository.UserProvider.GetByUserID(.LastUpdatedBy)
            If Omsuser.Count > 0 Then
                lblUpdatedby.Text = Omsuser(0).UserName
            End If
			lblUpdatedDate.Text = FormatDate(.LastUpdatedDate)
			lblActivation.Text = SafeActiveInactive(.Activation)
			Dim L_objMappingMsKotaKabNCBSPPATK As TList(Of MappingMsKotaKabNCBSPPATK)
			L_objMappingMsKotaKabNCBSPPATK = DataRepository.MappingMsKotaKabNCBSPPATKProvider.GetPaged(MappingMsKotaKabNCBSPPATKColumn.IDKotaKab.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)

			Listmaping.AddRange(L_objMappingMsKotaKabNCBSPPATK)

		End With
	End Sub


#End Region

#Region "events..."

	Protected Sub ImageCancel_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
		Response.Redirect("MsKotaKab_View.aspx")
	End Sub

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Not Page.IsPostBack Then
			Try
                'AuditTrailBLL.InsertAuditTrailUserAccess(SessionCurrentPage)
				Listmaping = New TList(Of MappingMsKotaKabNCBSPPATK)
				LoadData()
			Catch ex As Exception
				LogError(ex)
				CvalPageErr.IsValid = False
				CvalPageErr.ErrorMessage = ex.Message
			End Try
		End If
	End Sub

	Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
		Try
			BindListMapping()
		Catch ex As Exception
			LogError(ex)
			CvalPageErr.IsValid = False
			CvalPageErr.ErrorMessage = ex.Message
		End Try
	End Sub

	
	Private Sub BindListMapping()
		LBMapping.DataSource = ListMappingDisplay
		LBMapping.DataBind()
	End Sub
#End Region

#Region "Property..."

	Public ReadOnly Property parID As String
		Get
			Return Request.Item("ID")
		End Get
	End Property
	''' <summary>
	''' <summary>
	''' Menyimpan Item untuk mapping sementara
	''' </summary>
	''' <value></value>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Property Listmaping As TList(Of MappingMsKotaKabNCBSPPATK)
		Get
			Return Session("Listmaping.data")
		End Get
		Set(value As TList(Of MappingMsKotaKabNCBSPPATK))
			Session("Listmaping.data") = value
		End Set
	End Property
	''' <summary>
	''' List yang tampil di ListBox
	''' </summary>
	''' <value></value>
	''' <returns></returns>
	''' <remarks></remarks>
	Private ReadOnly Property ListMappingDisplay() As List(Of String)
		Get
			Dim Temp As New List(Of String)
			For Each i As MappingMsKotaKabNCBSPPATK In Listmaping.FindAllDistinct("PK_MappingMsKotaKabNCBSPPATK_Id")
                Temp.Add(i.idKotakabNCBS.ToString & "-" & i.Nama)
			Next
			Return Temp
		End Get

	End Property
#End Region

End Class



