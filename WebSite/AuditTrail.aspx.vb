Imports System.Globalization.CultureInfo
Partial Class AuditTrail
    Inherits Parent

#Region " Property "
    ''' <summary>
    ''' Nama Table
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property Tables() As String
        Get
            Return " AuditTrail"
        End Get
    End Property
    ''' <summary>
    ''' PK
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property PK() As String
        Get
            Return "AuditTrail.AuditTrailID"
        End Get
    End Property
    ''' <summary>
    ''' Fields
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property Fields() As String
        Get
            Return "AuditTrail.AuditTrailID, AuditTrail.AuditTrailDate, AuditTrail.AuditTrailCreatedBy, AuditTrail.AuditTrailApprovedBy, AuditTrail.AuditTrailModuleName, AuditTrail.AuditTrailFieldName, AuditTrail.AuditTrailOperation, AuditTrail.AuditTrailOldValue, AuditTrail.AuditTrailNewValue, AuditTrail.AuditTrailDescription"
        End Get
    End Property
    ''' <summary>
    ''' Page Size
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property PageSize() As Int16
        Get
            Return IIf(Session("PageSize") IsNot Nothing, Session("PageSize"), 10)
        End Get
        Set(ByVal value As Int16)
            Session("PageSize") = value
        End Set
    End Property

    ''' <summary>
    ''' selected item store
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("MsAuditTrailViewSelected") Is Nothing, New ArrayList, Session("MsAuditTrailViewSelected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("MsAuditTrailViewSelected") = value
        End Set
    End Property
    ''' <summary>
    ''' setnget search field
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetFieldSearch() As String
        Get
            Return IIf(Session("MsAuditTrailViewFieldSearch") Is Nothing, "", Session("MsAuditTrailViewFieldSearch"))
        End Get
        Set(ByVal Value As String)
            Session("MsAuditTrailViewFieldSearch") = Value
        End Set
    End Property
    ''' <summary>
    ''' search value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetValueSearch() As String
        Get
            Return IIf(Session("MsAuditTrailViewValueSearch") Is Nothing, "", Session("MsAuditTrailViewValueSearch"))
        End Get
        Set(ByVal Value As String)
            Session("MsAuditTrailViewValueSearch") = Value
        End Set
    End Property
    ''' <summary>
    ''' sort expresion
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("MsAuditTrailViewSort") Is Nothing, "AuditTrailId  asc", Session("MsAuditTrailViewSort"))
        End Get
        Set(ByVal Value As String)
            Session("MsAuditTrailViewSort") = Value
        End Set
    End Property
    ''' <summary>
    ''' current page index
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("MsAuditTrailViewCurrentPage") Is Nothing, 0, Session("MsAuditTrailViewCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("MsAuditTrailViewCurrentPage") = Value
        End Set
    End Property
    ''' <summary>
    ''' total pages
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    ''' <summary>
    ''' row total
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("MsAuditTrailViewRowTotal") Is Nothing, 0, Session("MsAuditTrailViewRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("MsAuditTrailViewRowTotal") = Value
        End Set
    End Property
    ''' <summary>
    ''' save bind table
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetBindTable() As Data.DataTable
        Get
            Return IIf(Session("MsAuditTrailViewData") Is Nothing, New AMLDAL.AMLDataSet.AuditTrailDataTable, Session("MsAuditTrailViewData"))
        End Get
        Set(ByVal value As Data.DataTable)
            Session("MsAuditTrailViewData") = value
        End Set
    End Property
#End Region

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' fill search
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub FillSearch()
        Try
            Me.ComboSearch.Items.Add(New ListItem("...", ""))
            Me.ComboSearch.Items.Add(New ListItem("Created by", "AuditTrailCreatedBy Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Entry Date", "AuditTrailDate >= '-=Search=- 00:00' AND AuditTrailDate <= '-=Search=- 23:59'"))
            Me.ComboSearch.Items.Add(New ListItem("Approve/Reject ID", "AuditTrailApprovedBy Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Category", "AuditTrailModuleName Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Field", "AuditTrailFieldName Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Activity", "AuditTrailOperation Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Old Value", "AuditTrailOldValue Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("New Value", "AuditTrailNewValue Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Description", "AuditTrailDescription Like '%-=Search=-%'"))
        Catch
            Throw
        End Try
    End Sub

    Private Sub ClearThisPageSessions()
        Session("DateFormat") = Nothing

        Session("MsAuditTrailViewSelected") = Nothing
        Session("MsAuditTrailViewFieldSearch") = Nothing
        Session("MsAuditTrailViewValueSearch") = Nothing
        Session("MsAuditTrailViewSort") = Nothing
        Session("MsAuditTrailViewCurrentPage") = Nothing
        Session("MsAuditTrailViewRowTotal") = Nothing
        Session("MsAuditTrailViewData") = Nothing
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    '''     [Hendry]	24/06/2007	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not Me.IsPostBack Then
                Me.ClearThisPageSessions()
                Me.ComboSearch.Attributes.Add("onchange", "javascript:Check(this, 2, '" & Me.TextSearch.ClientID & "', '" & Me.popUp.ClientID & "');")
                Me.popUp.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TextSearch.ClientID & "'), 'dd-mmm-yyyy')")
                Me.popUp.Style.Add("display", "none")
                If Sahassa.AML.Commonly.SessionPagingLimit <> "" Then
                    Me.PageSize = Sahassa.AML.Commonly.SessionPagingLimit
                Else
                    Me.PageSize = 10
                End If
                Me.FillSearch()
                Me.GridMSUserView.PageSize = Me.PageSize

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' page navigate button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
            Me.CollectSelected()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' bind grid
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' 	[Hendry]	24/06/2007	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Sub BindGrid()
        Try
            Me.SetnGetBindTable = Sahassa.AML.Commonly.GetDatasetPaging(Me.Tables, Me.PK, Me.SetnGetSort, Me.SetnGetCurrentPage + 1, Me.PageSize, Me.Fields, Me.SetnGetFieldSearch.Replace("-=Search=-", Me.SetnGetValueSearch.Replace("'", "''")), "")
            Me.GridMSUserView.DataSource = Me.SetnGetBindTable
            Using AccessAuditTrail As New AMLDAL.AMLDataSetTableAdapters.GetCountAuditTrailFilterTableAdapter
                Dim Filter As String = Me.SetnGetFieldSearch.Replace("-=Search=-", Me.SetnGetValueSearch.Replace("'", "''"))
                If Filter <> "" Then
                    Me.SetnGetRowTotal = AccessAuditTrail.Fill("Where " & Filter)
                Else
                    Me.SetnGetRowTotal = AccessAuditTrail.Fill(Filter)
                End If
            End Using
            Me.GridMSUserView.VirtualItemCount = Me.SetnGetRowTotal
            Me.GridMSUserView.DataBind()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' set navigate info
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.ComboSearch.SelectedValue = Me.SetnGetFieldSearch                       
            Me.TextSearch.Text = Me.SetnGetValueSearch
        Catch
            Throw
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' change sort expression
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridMSUserView.SortCommand
        Dim GridUser As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' image button search
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Try
            Me.CollectSelected()
            Me.SetnGetFieldSearch = Me.ComboSearch.SelectedValue
            If Me.ComboSearch.SelectedIndex = 0 Then
                Me.TextSearch.Text = ""
                Me.SetnGetValueSearch = Me.TextSearch.Text
            ElseIf Me.ComboSearch.SelectedIndex = 2 Then
                Dim DateSearch As DateTime
                Try
                    DateSearch = DateTime.Parse(Me.TextSearch.Text, New System.Globalization.CultureInfo("id-ID"))
                Catch ex As ArgumentOutOfRangeException
                    Throw New Exception("Input character cannot be convert to datetime.")
                Catch ex As FormatException
                    Throw New Exception("Unknown format date")
                End Try
                Me.SetnGetValueSearch = DateSearch.ToString("dd-MMM-yyyy")
            Else
                Me.SetnGetValueSearch = Me.TextSearch.Text
            End If
            Me.SetnGetCurrentPage = 0
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' go button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	14/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            Me.CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be a positive number greater than zero.")
                End If
            Else
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get item bound
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridMSUserView.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Dim chkBox As CheckBox = e.Item.FindControl("CheckBoxExporttoExcel")
                chkBox.Checked = Me.SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    ''' <summary>
    ''' collect sub
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = gridRow.FindControl("CheckBoxExporttoExcel")
                Dim AuditTrailID As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
                If ArrTarget.Contains(AuditTrailID) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(AuditTrailID)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(AuditTrailID)
                End If
                Me.SetnGetSelectedItem = ArrTarget
            End If
        Next
    End Sub

    ''' <summary>
    ''' prerender event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    ''' <summary>
    ''' clear all control except control
    ''' </summary>
    ''' <param name="control">excluded control</param>
    ''' <remarks></remarks>
    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub 'ClearControls

    ''' <summary>
    ''' bind selected item
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub BindSelected()

        'Dim Rows As New ArrayList

        'Dim oTableUserAccess As New Data.DataTable
        'Dim oRowUserAccess As Data.DataRow
        'oTableUserAccess.Columns.Add("AuditTrailID", GetType(Int64))
        'oTableUserAccess.Columns.Add("AuditTrailCreatedBy", GetType(String))
        'oTableUserAccess.Columns.Add("AuditTrailDate", GetType(DateTime))
        'oTableUserAccess.Columns.Add("AuditTrailApprovedBy", GetType(String))
        'oTableUserAccess.Columns.Add("AuditTrailModuleName", GetType(String))
        'oTableUserAccess.Columns.Add("AuditTrailFieldName", GetType(String))
        'oTableUserAccess.Columns.Add("AuditTrailOperation", GetType(String))
        'oTableUserAccess.Columns.Add("AuditTrailOldValue", GetType(String))
        'oTableUserAccess.Columns.Add("AuditTrailNewValue", GetType(String))
        'oTableUserAccess.Columns.Add("AuditTrailDescription", GetType(String))

        'For Each IdPk As String In Me.SetnGetSelectedItem
        '    Dim rowData() As Data.DataRow = Me.SetnGetBindTable.Select("AuditTrailID = '" & IdPk & "'")
        '    If rowData.Length > 0 Then
        '        'Rows.Add(rowData(0))
        '        oRowUserAccess = oTableUserAccess.NewRow
        '        oRowUserAccess(0) = rowData(0)(0)
        '        oRowUserAccess(1) = rowData(0)(2)
        '        oRowUserAccess(2) = rowData(0)(1)
        '        oRowUserAccess(3) = rowData(0)(3)
        '        oRowUserAccess(4) = rowData(0)(4)
        '        oRowUserAccess(5) = rowData(0)(5)
        '        oRowUserAccess(6) = rowData(0)(6)
        '        oRowUserAccess(7) = rowData(0)(7)
        '        oRowUserAccess(8) = rowData(0)(8)
        '        oRowUserAccess(9) = rowData(0)(9)
        '        oTableUserAccess.Rows.Add(oRowUserAccess)
        '    End If
        'Next

        'Me.GridMSUserView.DataSource = oTableUserAccess
        'Me.GridMSUserView.AllowPaging = False
        'Me.GridMSUserView.DataBind()

        Dim SbPk As New StringBuilder
        SbPk.Append("0, ")
        For Each IdPk As Int64 In Me.SetnGetSelectedItem
            SbPk.Append(IdPk.ToString & ", ")
        Next

        Me.GridMSUserView.DataSource = SahassaNettier.Data.DataRepository.AuditTrailProvider.GetPaged("AuditTrailID in (" & SbPk.ToString.Substring(0, SbPk.Length - 2) & ") ", Me.SetnGetSort, 0, Integer.MaxValue, 0)
        Me.GridMSUserView.AllowPaging = False
        Me.GridMSUserView.DataBind()

        'Sembunyikan kolom ke 0 & 1 agar tidak ikut diekspor ke excel
        Me.GridMSUserView.Columns(0).Visible = False
        Me.GridMSUserView.Columns(1).Visible = False

    End Sub

    ''' <summary>
    ''' export button 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Me.CollectSelected()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelected()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=AuditTrail.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridMSUserView)
            GridMSUserView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    ''' <summary>
    ''' select all
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(1).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItem.Contains(StrKey) = False Then
                        ArrTarget.Add(StrKey)
                    End If
                Else
                    If Me.SetnGetSelectedItem.Contains(StrKey) Then
                        ArrTarget.Remove(StrKey)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItem = ArrTarget
    End Sub
End Class