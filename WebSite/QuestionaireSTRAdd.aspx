<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="QuestionaireSTRAdd.aspx.vb" Inherits="QuestionaireSTRAdd" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    <ajax:AjaxPanel runat="server" ID="Ajax1">
        <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
            height="72" style="border-top-style: none; border-right-style: none; border-left-style: none;
            border-bottom-style: none" width="100%">
            <tr>
                <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                    border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                    <strong>Questionaire STR - ADD
                        <hr />
                    </strong>
                    <asp:Label ID="LblSuccess" runat="server" CssClass="validationok" Visible="False"
                        Width="100%"></asp:Label></td>
            </tr>
            <tr class="formText">
                <td bgcolor="#ffffff" colspan="4" style="border-top-style: none; border-right-style: none;
                    border-left-style: none; height: 24px; border-bottom-style: none">
                    <span style="color: #ff0000">* Required</span></td>
            </tr>
        </table>
        <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
            border="2" height="72" style="border-top-style: none; border-right-style: none;
            border-left-style: none; border-bottom-style: none">
            <tr class="formText">
                <td bgcolor="#ffffff" style="width: 24px; height: 24px">
                </td>
                <td width="20%" bgcolor="#ffffff" style="height: 24px">
                    Description Alert STR</td>
                <td width="5" bgcolor="#ffffff" style="height: 24px">
                    :</td>
                <td bgcolor="#ffffff" style="height: 24px; width: 80%;">
                    &nbsp;<asp:DropDownList ID="CboAlertSTR" runat="server" CssClass="combobox" Width="182px">
                    </asp:DropDownList><strong><span style="color: #ff0000"></span></strong></td>
            </tr>
            <tr class="formText">
                <td bgcolor="#ffffff" style="width: 24px; height: 24px">
                </td>
                <td width="20%" bgcolor="#ffffff" style="height: 24px">
                    Question Type</td>
                <td width="5" bgcolor="#ffffff" style="height: 24px">
                    :</td>
                <td bgcolor="#ffffff" style="height: 24px; width: 80%;">
                    &nbsp;<asp:DropDownList ID="CboQuestionType" runat="server" CssClass="combobox" Width="182px" AutoPostBack="True">
                    </asp:DropDownList>
                    <strong><span
                        style="color: #ff0000"></span></strong></td>
            </tr>
            <tr class="formText">
                <td bgcolor="#ffffff" style="width: 24px; height: 24px">
                </td>
                <td width="20%" bgcolor="#ffffff" style="height: 24px">
                    Question No</td>
                <td width="5" bgcolor="#ffffff" style="height: 24px">
                    :</td>
                <td bgcolor="#ffffff" style="height: 24px; width: 80%;">
                    &nbsp;<asp:TextBox ID="TxtQuestionNo" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="Number" runat="server" ControlToValidate="TxtQuestionNo"
                        EnableViewState="False" ErrorMessage="Question No Must number " ValidationExpression="(\d)*">*</asp:RegularExpressionValidator><strong><span
                            style="color: #ff0000"></span></strong></td>
            </tr>
            <tr class="formText">
                <td bgcolor="#ffffff" style="width: 24px; height: 24px">
                </td>
                <td width="20%" bgcolor="#ffffff" style="height: 24px">
                    Question</td>
                <td width="5" bgcolor="#ffffff" style="height: 24px">
                    :</td>
                <td bgcolor="#ffffff" style="height: 24px; width: 80%;">
                    &nbsp;
                    <asp:TextBox ID="TxtQuestion" runat="server" TextMode="MultiLine" Height="112px"
                        Width="481px"></asp:TextBox><strong><span style="color: #ff0000"></span></strong></td>
            </tr>
            <tr class="formText" id="abc" runat="server">
                <td bgcolor="#ffffff" style="width: 24px; height: 24px">
                </td>
                <td bgcolor="#ffffff" style="height: 24px" width="20%">
                    <asp:Label ID="Label1" runat="server" Text="Multiple Choice"></asp:Label></td>
                <td bgcolor="#ffffff" style="height: 24px" width="5">
                    :</td>
                <td bgcolor="#ffffff" style="height: 24px; width: 80%;">
                    &nbsp;<asp:TextBox ID="TxtMultipleChoice" runat="server" TextMode="MultiLine" Height="54px"
                        Width="481px"></asp:TextBox>&nbsp;</td>
            </tr>
            <tr class="formText" id="bcd" runat="server">
                <td bgcolor="#ffffff" style="width: 24px; height: 24px">
                </td>
                <td bgcolor="#ffffff" style="height: 24px" width="20%">
                </td>
                <td bgcolor="#ffffff" style="height: 24px" width="5">
                </td>
                <td bgcolor="#ffffff" style="height: 24px; width: 80%;">
                    <asp:ImageButton ID="ImageAdd" runat="server" CausesValidation="True" SkinID="AddButton" /></td>
            </tr>
            <tr class="formText" id="cde" runat="server">
                <td bgcolor="#ffffff" style="width: 24px; height: 24px">
                </td>
                <td bgcolor="#ffffff" style="height: 24px" width="20%">
                </td>
                <td bgcolor="#ffffff" style="height: 24px" width="5">
                </td>
                <td bgcolor="#ffffff" style="height: 24px; width: 80%;">
                    <asp:DataGrid ID="gridView" runat="server" AllowPaging="True" AllowSorting="True"
                        AutoGenerateColumns="False" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None"
                        BorderWidth="1px" CellPadding="4" Font-Size="XX-Small" ForeColor="Black" GridLines="Vertical"
                        Width="100%">
                        <FooterStyle BackColor="#CCCC99" />
                        <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                        <AlternatingItemStyle BackColor="White" />
                        <ItemStyle BackColor="#F7F7DE" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="No." Visible="False" ></asp:TemplateColumn>
                            <asp:BoundColumn DataField="PK_MsQuestionaireSTRMultipleChoice_ID" HeaderText="PK_MsQuestionaireSTRMultipleChoice_ID" Visible="false">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="MultipleChoice" HeaderText="Choice" SortExpression="MultipleChoice Desc">
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Edit" SortExpression="Edit">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkGridEdit" runat="server" CausesValidation="False" CommandName="EDIT"
                                        Text="Edit"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Delete" SortExpression="Delete">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkGridDelete" runat="server" CausesValidation="False" CommandName="DELETE"
                                        Text="Delete"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <HeaderStyle BackColor="#6B696B" Font-Bold="True" Font-Size="XX-Small" ForeColor="White" />
                        <PagerStyle Mode="NumericPages" />
                    </asp:DataGrid></td>
            </tr>
            <tr class="formText" bgcolor="#dddddd" height="30">
                <td style="width: 24px">
                    <img height="15" src="images/arrow.gif" width="15"></td>
                <td colspan="3" style="height: 9px">
                    <table cellspacing="0" cellpadding="3" border="0">
                        <tr>
                            <td style="height: 36px">
                                <asp:ImageButton ID="ImageSave" runat="server" CausesValidation="True" SkinID="SaveButton">
                                </asp:ImageButton></td>
                            <td style="height: 36px; width: 35px;">
                                <asp:ImageButton ID="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton">
                                </asp:ImageButton></td>
                        </tr>
                    </table>
                    <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
            </tr>
        </table>
    </ajax:AjaxPanel>
</asp:Content>
