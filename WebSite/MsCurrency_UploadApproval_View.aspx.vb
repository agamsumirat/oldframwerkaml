#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
#End Region
Partial Class MsCurrency_UploadApproval_view
    Inherits Parent

#Region "Member"
    Private ReadOnly BindGridFromExcel As Boolean
#End Region

#Region "Property"

    Public ReadOnly Property SetnGetUserID() As String
        Get
            Return SessionPkUserId
        End Get
    End Property

    Public Property SetMsUser_StaffName() As String
        Get
            If Not Session("MsCurrency_APPROVAL.SetMsUser_StaffName") Is Nothing Then
                Return CStr(Session("MsCurrency_APPROVAL.SetMsUser_StaffName"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("MsCurrency_APPROVAL.SetMsUser_StaffName") = value
        End Set
    End Property
    Public Property SetRequestedDate() As String
        Get
            If Not Session("MsCurrency_APPROVAL.SetRequestedDate") Is Nothing Then
                Return CStr(Session("MsCurrency_APPROVAL.SetRequestedDate"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("MsCurrency_APPROVAL.SetRequestedDate") = value
        End Set
    End Property

    Public Property SetRequestedDate2() As String
        Get
            If Not Session("MsCurrency_APPROVAL.SetRequestedDate2") Is Nothing Then
                Return CStr(Session("MsCurrency_APPROVAL.SetRequestedDate2"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("MsCurrency_APPROVAL.SetRequestedDate2") = value
        End Set
    End Property



    Private Property SetnGetSort() As String
        Get
            Return CType(IIf(Session("MsCurrency_APPROVAL.Sort") Is Nothing, "PK_MsCurrencyPPATK_Approval_Id  asc", Session("MsCurrency_APPROVAL.Sort")), String)
        End Get
        Set(ByVal Value As String)
            Session("MsCurrency_APPROVAL.Sort") = Value
        End Set
    End Property
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return CType(IIf(Session("MsCurrency_APPROVAL.SelectedItem") Is Nothing, New ArrayList, Session("MsCurrency_APPROVAL.SelectedItem")), ArrayList)
        End Get
        Set(ByVal value As ArrayList)
            Session("MsCurrency_APPROVAL.SelectedItem") = value
        End Set
    End Property
    Private Property SetnGetRowTotal() As Int32
        Get
            Return CType(IIf(Session("MsCurrency_APPROVAL.RowTotal") Is Nothing, 0, Session("MsCurrency_APPROVAL.RowTotal")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("MsCurrency_APPROVAL.RowTotal") = Value
        End Set
    End Property
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return iTotalPages(SetnGetRowTotal)
        End Get
    End Property
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return CType(IIf(Session("MsCurrency_APPROVAL.CurrentPage") Is Nothing, 0, Session("MsCurrency_APPROVAL.CurrentPage")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("MsCurrency_APPROVAL.CurrentPage") = Value
        End Set
    End Property
    Public ReadOnly Property SetnGetBindTable() As VList(Of Vw_MsCurrency_Approval)
        Get
            Dim strWhereClause(-1) As String
            Dim strAllWhereClause As String
            If SetnGetCurrentPage > GetPageTotal - 1 And GetPageTotal - 1 > 0 Then
                SetnGetCurrentPage = GetPageTotal - 1
            End If

            Dim StrFilterWorkingUnit As String = "RequestedBy <> '" & Sahassa.AML.Commonly.SessionPkUserId & "' and RequestedBy IN (SELECT x.pkUserID FROM UserWorkingUnitAssignment uwua INNER JOIN  [USER] x ON x.UserID=uwua.UserID WHERE uwua.WorkingUnitID IN (SELECT uwua2.WorkingUnitID FROM UserWorkingUnitAssignment uwua2 WHERE uwua2.UserID= '" & Sahassa.AML.Commonly.SessionUserId & "'))"
            ReDim Preserve strWhereClause(strWhereClause.Length)
            strWhereClause(strWhereClause.Length - 1) = StrFilterWorkingUnit

            ReDim Preserve strWhereClause(strWhereClause.Length)
            strWhereClause(strWhereClause.Length - 1) = "isUpload=1"

            If SetMsUser_StaffName.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = Vw_MsCurrency_ApprovalColumn.UserName.ToString & " like '%" & SetMsUser_StaffName.Trim.Replace("'", "''") & "%'"
            End If

            If SetRequestedDate.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = Vw_MsCurrency_ApprovalColumn.RequestedDate.ToString & " between '" & CToDate2(SetRequestedDate).ToString("dd-MMM-yyyy") & " 00:00' and '" & CToDate2(SetRequestedDate2).ToString("dd-MMM-yyyy") & " 23:59' "
            End If


            strAllWhereClause = String.Join(" and ", strWhereClause)

            Return DataRepository.Vw_MsCurrency_ApprovalProvider.GetPaged(strAllWhereClause, SetnGetSort, SetnGetCurrentPage, GetDisplayedTotalRow, SetnGetRowTotal)

        End Get

    End Property


#End Region

#Region "Function"
    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                    'Skip
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub


    Private Sub BindSelectedAll()
        Try
            GV.DataSource = DataRepository.Vw_MsCurrency_ApprovalProvider.GetAll
            GV.AllowPaging = False
            GV.DataBind()

            For i As Integer = 0 To GV.Items.Count - 1
                For y As Integer = 0 To GV.Columns.Count - 1
                    GV.Items(i).Cells(y).Attributes.Add("class", "text")
                Next
            Next

        Catch
            Throw
        End Try


    End Sub
    Private Sub SetCheckedAll()
        Dim i As Int16 = 0
        Dim TotalRow As Int16 = 0
        For Each gridRow As DataGridItem In GV.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = CType(gridRow.FindControl("CheckBoxExporttoExcel"), CheckBox)
                If chkBox.Checked Then
                    i = CType(i + 1, Int16)
                End If
                TotalRow = CType(TotalRow + 1, Int16)
            End If
        Next
        If TotalRow = 0 Then
            CheckBoxSelectAll.Checked = False
        Else
            If i = TotalRow Then
                CheckBoxSelectAll.Checked = True
            Else
                CheckBoxSelectAll.Checked = False
            End If
        End If
    End Sub
    Private Sub SetInfoNavigate()
        PageCurrentPage.Text = (SetnGetCurrentPage + 1).ToString
        PageTotalPages.Text = GetPageTotal.ToString
        PageTotalRows.Text = SetnGetRowTotal.ToString
        LinkButtonNext.Enabled = (Not SetnGetCurrentPage + 1 = GetPageTotal) AndAlso GetPageTotal <> 0
        LinkButtonLast.Enabled = (Not SetnGetCurrentPage + 1 = GetPageTotal) AndAlso GetPageTotal <> 0
        First.Enabled = Not SetnGetCurrentPage = 0
        LinkButtonPrevious.Enabled = Not SetnGetCurrentPage = 0
    End Sub

    Private Sub BindSelected()
        Dim Rows As New ArrayList

        For Each IdDIN As Int64 In SetnGetSelectedItem
            Using rowData As VList(Of Vw_MsCurrency_Approval) = DataRepository.Vw_MsCurrency_ApprovalProvider.GetPaged("PK_MsCurrencyPPATK_Approval_Id = " & IdDIN & "", "", 0, Integer.MaxValue, 0)
                If rowData.Count > 0 Then
                    Rows.Add(rowData(0))
                End If
            End Using
        Next

        GV.DataSource = Rows
        GV.AllowPaging = False
        GV.DataBind()
        For i As Integer = 0 To GV.Items.Count - 1
            For y As Integer = 0 To GV.Columns.Count - 1
                GV.Items(i).Cells(y).Attributes.Add("class", "text")
            Next
        Next
    End Sub
    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In GV.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = CType(gridRow.FindControl("CheckBoxExporttoExcel"), CheckBox)
                Dim PKMsUserId As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = SetnGetSelectedItem
                If ArrTarget.Contains(PKMsUserId) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(PKMsUserId)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(PKMsUserId)
                End If
                SetnGetSelectedItem = ArrTarget
            End If
        Next

    End Sub
    Private Sub SettingControlSearching()
        txtMsUser_StaffName.Text = SetMsUser_StaffName
        txtRequestedDate1.Text = SetRequestedDate
        txtRequestedDate2.Text = SetRequestedDate2


    End Sub
    Private Sub SettingPropertySearching()
        SetMsUser_StaffName = txtMsUser_StaffName.Text.Trim
        SetRequestedDate = txtRequestedDate1.Text.Trim
        SetRequestedDate2 = txtRequestedDate2.Text.Trim

    End Sub

    Private Sub ClearThisPageSessions()
        Clearproperty()
        LblMessage.Visible = False
        LblMessage.Text = ""
        SetnGetRowTotal = Nothing
        SetnGetSort = Nothing
        SetnGetCurrentPage = 0
    End Sub
    Private Sub Clearproperty()
        SetMsUser_StaffName = Nothing
        SetRequestedDate = Nothing
        SetRequestedDate2 = Nothing


    End Sub
    Private Sub popDateControl()
        '================================================= poppup for  Requested Date =========================================================================
        PopRequestedDate1.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtRequestedDate1.ClientID & "'), 'dd-mm-yyyy')")
        PopRequestedDate2.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtRequestedDate2.ClientID & "'), 'dd-mm-yyyy')")

    End Sub


    Private Sub bindgrid()
        SettingControlSearching()
        GV.DataSource = SetnGetBindTable
        GV.CurrentPageIndex = SetnGetCurrentPage
        GV.VirtualItemCount = SetnGetRowTotal
        GV.DataBind()
        If SetnGetRowTotal > 0 Then
            LabelNoRecordFound.Visible = False
        Else
            LabelNoRecordFound.Visible = True
        End If
    End Sub

#End Region

#Region "events..."

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            LblMessage.Text = ""
            LblMessage.Visible = False
            If Not Page.IsPostBack Then
                ClearThisPageSessions()
                popDateControl()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
            End If

        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : SetnGetCurrentPage = 0
                Case "Prev" : SetnGetCurrentPage -= 1
                Case "Next" : SetnGetCurrentPage += 1
                Case "Last" : SetnGetCurrentPage = GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
            CollectSelected()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    Protected Sub ImgBtnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnSearch.Click
        Try
            SettingPropertySearching()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            bindgrid()
            SetInfoNavigate()
            SetCheckedAll()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImgBtnClearSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnClearSearch.Click
        Try
            Clearproperty()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        Try
            'For Each gridRow As DataGridItem In GV.Items
            '    If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
            '        Dim pkid As String = gridRow.Cells(1).Text
            '        Dim ArrTarget As ArrayList = SetnGetSelectedItem
            '        If SetnGetSelectedItem.Contains(pkid) Then
            '            If CheckBoxSelectAll.Checked Then
            '                If Not ArrTarget.Contains(pkid) Then
            '                    ArrTarget.Add(pkid)
            '                End If
            '            Else
            '                ArrTarget.Remove(pkid)
            '            End If
            '        Else
            '            If CheckBoxSelectAll.Checked Then
            '                If Not ArrTarget.Contains(pkid) Then
            '                    ArrTarget.Add(pkid)
            '                End If
            '            End If
            '        End If
            '        SetnGetSelectedItem = ArrTarget
            '    End If
            'Next
            Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
            For Each gridRow As DataGridItem In Me.GV.Items
                If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                    Dim StrKey As String = gridRow.Cells(1).Text
                    If Me.CheckBoxSelectAll.Checked Then
                        If Me.SetnGetSelectedItem.Contains(StrKey) = False Then
                            ArrTarget.Add(StrKey)
                        End If
                    Else
                        If Me.SetnGetSelectedItem.Contains(StrKey) Then
                            ArrTarget.Remove(StrKey)
                        End If
                    End If
                End If
            Next
            Me.SetnGetSelectedItem = ArrTarget
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub GridMsUserView_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GV.EditCommand
        Dim PKMsUserApprovalid As Integer
        Try
            PKMsUserApprovalid = CInt(e.Item.Cells(1).Text)
            Response.Redirect("UserApprovalDetail.aspx?PKMsUserApprovalID=" & PKMsUserApprovalid, False)
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub GridMsUserView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GV.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Dim chkBox As CheckBox = CType(e.Item.FindControl("CheckBoxExporttoExcel"), CheckBox)
                chkBox.Checked = SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)
                If BindGridFromExcel = True Then
                    e.Item.Cells(2).Text = CStr((e.Item.ItemIndex + 1))
                Else
                    e.Item.Cells(2).Text = CStr((e.Item.ItemIndex + 1) + (SetnGetCurrentPage * GetDisplayedTotalRow))
                End If
            End If
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub
    Protected Sub GridMsUserView_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GV.SortCommand
        Dim GridUser As DataGrid = CType(source, DataGrid)
        Try
            SetnGetSort = ChangeSortCommand(e.SortExpression)
            GridUser.Columns(IndexSort(GridUser, e.SortExpression)).SortExpression = SetnGetSort
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click

        Try
            CollectSelected()
            Const strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            BindSelected()
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=MsCurrencyUploadApproval.xls")
            Response.Charset = ""
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            EnableViewState = False
            Using stringWrite As System.IO.StringWriter = New System.IO.StringWriter()
                Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
                ClearControls(GV)
                GV.RenderControl(htmlWrite)
                Response.Write(strStyle)
                Response.Write(stringWrite.ToString())
            End Using
            Response.End()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            Me.CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) AndAlso (CInt(Me.TextGoToPage.Text) > 0) Then
                If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                    Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                Else
                    'Throw New Exception("SahassaCommonly.CommonlyEnum.EXECPTION_PAGENUMBER_GREATER_TOTALPAGENUMBER")
                    Throw New Exception("Page number must be less than or equal to the total page count.")
                End If
            Else
                'Throw New Exception("SahassaCommonly.CommonlyEnum.EXECPTION_PAGENUMBER_NUMERICPOSITIF")
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If
            CollectSelected()
        Catch ex As Exception
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub lnkExportAllData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExportAllData.Click

        Try

            Const strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            BindSelectedAll()
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=MsCurrencyUploadApprovalAll.xls")
            Response.Charset = ""
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            EnableViewState = False
            Using stringWrite As System.IO.StringWriter = New System.IO.StringWriter()
                Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
                ClearControls(GV)
                GV.RenderControl(htmlWrite)
                Response.Write(strStyle)
                Response.Write(stringWrite.ToString())
            End Using
            Response.End()

        Catch ex As Exception
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub


#End Region

End Class


