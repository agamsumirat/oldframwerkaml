#Region "Imports..."
Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports SahassaNettier.Services
Imports AMLBLL.ValidateBLL
Imports AMLBLL.DataType

#End Region

Partial Class CustomerCTRDelete
    Inherits Parent

#Region "properties..."

    ReadOnly Property GetPK_CustomerCTR_ID() As Long
        Get
            If Not Request.Params("PK_CustomerCTR_ID") Is Nothing Then
                Return CLng(Request.Params("PK_CustomerCTR_ID"))
            Else
                Return -1
            End If
        End Get
    End Property

    Private Property SetnGetRowEdit() As Integer
        Get
            If Session("CustomerCTRDelete.RowEdit") Is Nothing Then
                Session("CustomerCTRDelete.RowEdit") = -1
            End If
            Return Session("CustomerCTRDelete.RowEdit")
        End Get
        Set(ByVal value As Integer)
            Session("CustomerCTRDelete.RowEdit") = value
        End Set
    End Property

    Private Property SetnGetActionType() As String
        Get
            If Session("CustomerCTRDelete.ActionType") Is Nothing Then
                Session("CustomerCTRDelete.ActionType") = ""
            End If
            Return Session("CustomerCTRDelete.ActionType")
        End Get
        Set(ByVal value As String)
            Session("CustomerCTRDelete.ActionType") = value
        End Set
    End Property

    Private Property SetnGetTransaction() As TList(Of CustomerCTRTransaction)
        Get
            If Session("CustomerCTRDelete.Transaction") Is Nothing Then
                Session("CustomerCTRDelete.Transaction") = New TList(Of CustomerCTRTransaction)
            End If
            Return Session("CustomerCTRDelete.Transaction")
        End Get
        Set(ByVal value As TList(Of CustomerCTRTransaction))
            Session("CustomerCTRDelete.Transaction") = value
        End Set
    End Property

    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("CustomerCTRDelete.Sort") Is Nothing, "PK_CustomerCTRTransaction_ID  asc", Session("CustomerCTRDelete.Sort"))
        End Get
        Set(ByVal Value As String)
            Session("CustomerCTRDelete.Sort") = Value
        End Set
    End Property

#End Region

#Region "events..."

    Protected Sub imgOKMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgOKMsg.Click
        Try
            Response.Redirect("CustomerCTRView.aspx")
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Try
            Response.Redirect("CustomerCTRView.aspx")
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub rblKewarganegaraan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblKewarganegaraan.SelectedIndexChanged
        Try
            If rblKewarganegaraan.SelectedIndex = 0 Then
                For Each item As ListItem In cboNegara.Items
                    If item.Text.ToLower.Contains("indonesia") Then
                        cboNegara.SelectedValue = item.Value
                    End If
                Next
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                clearSession()
                SetControlLoad()



                Me.LoadCustomerCTRToField()
                Me.LoadCustomerCTRTransaction()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using

                'If MultiView1.ActiveViewIndex = 0 Then
                '    ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('Id','Img1','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
                '    ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('Transaksi','Img2','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
                'End If
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub rblTipePelapor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblTipePelapor.SelectedIndexChanged
        Try
            If rblTipePelapor.SelectedValue = 1 Then
                divPerorangan.Visible = True
                divKorporasi.Visible = False
            Else
                divPerorangan.Visible = False
                divKorporasi.Visible = True
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try

        Catch ex As Exception

        End Try
    End Sub

#End Region

#Region "Function..."

    Private Enum AddressLevel
        Kelurahan = 1
        Kecamatan
        KabupatenKota
        Propinsi
        Negara
    End Enum

    Sub hideSave()
        ImageDelete.Visible = False
        ImageCancel.Visible = False
    End Sub


    Private Sub SetControlLoad()

        'bind MsNegara
        Using objNegara As TList(Of MsNegara) = DataRepository.MsNegaraProvider.GetAll
            If objNegara.Count > 0 Then
                cboNegara.Items.Clear()
                cboNegara.Items.Add("-Select-")


                For i As Integer = 0 To objNegara.Count - 1
                    cboNegara.Items.Add(New ListItem(objNegara(i).NamaNegara, objNegara(i).IDNegara.ToString))
                Next
            End If
        End Using

        'Bind MsBentukBadanUsaha
        Using objBentukBadanUsaha As TList(Of MsBentukBidangUsaha) = DataRepository.MsBentukBidangUsahaProvider.GetAll
            If objBentukBadanUsaha.Count > 0 Then
                cboCORPBentukBadanUsaha.Items.Clear()
                cboCORPBentukBadanUsaha.Items.Add("-Select-")

                For i As Integer = 0 To objBentukBadanUsaha.Count - 1
                    cboCORPBentukBadanUsaha.Items.Add(New ListItem(objBentukBadanUsaha(i).BentukBidangUsaha, objBentukBadanUsaha(i).IdBentukBidangUsaha.ToString))
                Next
            End If
        End Using

        'bind ID type
        Using objJenisID As TList(Of MsIDType) = DataRepository.MsIDTypeProvider.GetAll

            Using objHapusJenisID As MsIDType = objJenisID.Find(MsIDTypeColumn.MsIDType_Name, "kartu pelajar")
                If Not IsNothing(objHapusJenisID) Then
                    objJenisID.Remove(objHapusJenisID)
                End If
            End Using

            cboJenisDocID.Items.Clear()
            cboJenisDocID.Items.Add("-Select-")

            For i As Integer = 0 To objJenisID.Count - 1
                cboJenisDocID.Items.Add(New ListItem(objJenisID(i).MsIDType_Name, objJenisID(i).Pk_MsIDType_Id.ToString))
            Next
        End Using

        'bind calendar
   
    End Sub

    Sub clearSession()

        Session("CustomerCTRDelete.RowEdit") = Nothing
        Session("CustomerCTRDelete.ActionType") = Nothing
        Session("CustomerCTRDelete.Transaction") = Nothing
        Session("CustomerCTRDelete.Sort") = Nothing
    End Sub

#End Region

#Region "Essential Function ..."

    Protected Sub ImageDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageDelete.Click
        Try
            Using OTrans As TransactionManager = New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)

                Try

                    OTrans.BeginTransaction()
                    Using ObjCustomerCtr As CustomerCTR = DataRepository.CustomerCTRProvider.GetByPK_CustomerCTR_ID(Me.GetPK_CustomerCTR_ID)

                        DataRepository.CustomerCTRTransactionProvider.Delete(OTrans, SetnGetTransaction)
                        DataRepository.CustomerCTRProvider.Delete(OTrans, ObjCustomerCtr)
                    End Using



                    mtvPage.ActiveViewIndex = 1
                    lblMsg.Text = "CustomerCTR data has been delete successfully"
                    hideSave()
                    OTrans.Commit()

                Catch ex As Exception

                    OTrans.Rollback()

                    Throw

                End Try

            End Using





        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message.ToString
        End Try
    End Sub

#End Region

    Sub LoadCustomerCTRToField()
        Using objCustomerCTR As CustomerCTR = DataRepository.CustomerCTRProvider.GetByPK_CustomerCTR_ID(Me.GetPK_CustomerCTR_ID)
            SafeDefaultValue = ""
            If Not IsNothing(objCustomerCTR) Then

                'Ambil data buat cari PK Negara, Provinsi, KotaKab, dkk (ini variable yang bakal dpke berkali2)
                Dim i As Integer = 0 'buat iterasi
                Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetAll
                Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
                Dim objKelurahan As TList(Of MsKelurahan) = DataRepository.MsKelurahanProvider.GetAll
                Dim objSKelurahan As MsKelurahan 'Penampung hasil search kelurahan
                Dim objKecamatan As TList(Of MsKecamatan) = DataRepository.MsKecamatanProvider.GetAll
                Dim objSkecamatan As MsKecamatan 'Penampung hasil search kecamatan
                Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetAll
                Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi
                Dim objNegara As TList(Of MsNegara) = DataRepository.MsNegaraProvider.GetAll
                Dim objSnegara As MsNegara 'Penampung hasil search negara yang pke picker
                Dim objPekerjaan As TList(Of MsPekerjaan) = DataRepository.MsPekerjaanProvider.GetAll
                Dim objSPekerjaan As MsPekerjaan 'Penampung hasil search pekerjaan
                Dim objBidangUsaha As TList(Of MsBidangUsaha) = DataRepository.MsBidangUsahaProvider.GetAll
                Dim objSBidangUsaha As MsBidangUsaha 'Penampung hasil search bidang usaha

                'LblCIFNumber.Text = objCustomerCTR.CIFNumber
                'LblAccountNumber.Text = objCustomerCTR.AccountNumber

                SafeSelectedValue(rblTipePelapor, objCustomerCTR.CustomerType)

                If rblTipePelapor.SelectedValue = 1 Then
                    divPerorangan.Visible = True
                    divKorporasi.Visible = False
                Else
                    divPerorangan.Visible = False
                    divKorporasi.Visible = True
                End If


                txtGelar.Text = Safe(objCustomerCTR.INDV_Gelar)
                txtNamaLengkap.Text = Safe(objCustomerCTR.CustomerName)
                txtNamaAlias.Text = Safe(objCustomerCTR.CustomerAlias)
                txtTempatLahir.Text = Safe(objCustomerCTR.INDV_TempatLahir)
                txtTglLahir.Text = objCustomerCTR.INDV_TanggalLahir.GetValueOrDefault().ToString("dd-MM-yyyy")
                SafeSelectedValue(rblKewarganegaraan, objCustomerCTR.INDV_Kewarganegaraan)
                i = 0
                For Each listNegara As ListItem In cboNegara.Items
                    If objCustomerCTR.INDV_FK_MsNegara_Id.ToString = listNegara.Value Then
                        cboNegara.SelectedIndex = i
                        Exit For
                    End If
                    i = i + 1
                Next
                txtDOMNamaJalan.Text = Safe(objCustomerCTR.INDV_DOM_NamaJalan)
                txtDOMRT.Text = Safe(objCustomerCTR.INDV_DOM_RT)
                txtDOMRW.Text = Safe(objCustomerCTR.INDV_DOM_RW)
                objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objCustomerCTR.INDV_DOM_FK_MsKelurahan_Id)
                If Not IsNothing(objSKelurahan) Then
                    txtDOMKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
                    'hfDOMKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
                End If
                objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objCustomerCTR.INDV_DOM_FK_MsKecamatan_Id)
                If Not IsNothing(objSkecamatan) Then
                    txtDOMKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
                    'hfDOMKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objCustomerCTR.INDV_DOM_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtDOMKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    'hfDOMKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtDOMKodePos.Text = Safe(objCustomerCTR.INDV_DOM_KodePos)
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objCustomerCTR.INDV_DOM_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtDOMProvinsi.Text = Safe(objSProvinsi.Nama)
                    'hfDOMProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, objCustomerCTR.INDV_DOM_FK_MsNegara_Id)
                If Not IsNothing(objSnegara) Then
                    txtDOMNegara.Text = Safe(objSnegara.NamaNegara)
                    'hfDomNegara.Value = Safe(objSnegara.IDNegara)
                End If

                txtIDNamaJalan.Text = Safe(objCustomerCTR.INDV_ID_NamaJalan)
                txtIDRT.Text = Safe(objCustomerCTR.INDV_ID_RT)
                txtIDRW.Text = Safe(objCustomerCTR.INDV_ID_RW)
                objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objCustomerCTR.INDV_ID_FK_MsKelurahan_Id)
                If Not IsNothing(objSKelurahan) Then
                    txtIDKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
                    'hfIDKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
                End If
                objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objCustomerCTR.INDV_ID_FK_MsKecamatan_Id)
                If Not IsNothing(objSkecamatan) Then
                    txtIDKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
                    ' hfIDKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objCustomerCTR.INDV_ID_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtIDKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    'hfIDKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtIDKodePos.Text = Safe(objCustomerCTR.INDV_ID_KodePos)
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objCustomerCTR.INDV_ID_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtIDProvinsi.Text = Safe(objSProvinsi.Nama)
                    'hfIDProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, objCustomerCTR.INDV_ID_FK_MsNegara_Id)
                If Not IsNothing(objSnegara) Then
                    txtIDNegara.Text = Safe(objSnegara.NamaNegara)
                    'hfIDNegara.Value = Safe(objSnegara.IDNegara)
                End If

                txtNANamaJalan.Text = Safe(objCustomerCTR.INDV_NA_NamaJalan)
                objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, objCustomerCTR.INDV_NA_FK_MsNegara_Id)
                If Not IsNothing(objSnegara) Then
                    txtNANegara.Text = Safe(objSnegara.NamaNegara)
                    'hfNANegara.Value = Safe(objSnegara.IDNegara)
                End If
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objCustomerCTR.INDV_NA_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtNAProvinsi.Text = Safe(objSProvinsi.Nama)
                    'hfNAProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objCustomerCTR.INDV_NA_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtNAKota.Text = Safe(objSkotaKab.NamaKotaKab)
                    'hfIDKota.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtNAKodePos.Text = Safe(objCustomerCTR.INDV_NA_KodePos)
                i = 0
                For Each jenisDoc As ListItem In cboJenisDocID.Items
                    If objCustomerCTR.INDV_FK_MsIDType_Id.ToString = jenisDoc.Value Then
                        cboJenisDocID.SelectedIndex = i
                        Exit For
                    End If
                    i = i + 1
                Next
                txtNomorID.Text = Safe(objCustomerCTR.INDV_NomorId)
                txtNPWP.Text = Safe(objCustomerCTR.NPWP)
                objSPekerjaan = objPekerjaan.Find(MsPekerjaanColumn.IDPekerjaan, objCustomerCTR.INDV_FK_MsPekerjaan_Id)
                If Not IsNothing(objSPekerjaan) Then
                    txtPekerjaan.Text = Safe(objSPekerjaan.NamaPekerjaan)
                    'hfPekerjaan.Value = Safe(objSPekerjaan.IDPekerjaan)
                End If
                txtJabatan.Text = Safe(objCustomerCTR.INDV_Jabatan)
                txtPenghasilanRataRata.Text = Safe(objCustomerCTR.INDV_PenghasilanRataRata)
                txtTempatKerja.Text = Safe(objCustomerCTR.INDV_TempatBekerja)


                i = 0
                For Each listBentukBadanUsaha As ListItem In cboCORPBentukBadanUsaha.Items
                    If objCustomerCTR.CORP_FK_MsBentukBadanUsaha_Id.ToString = listBentukBadanUsaha.Value Then
                        cboCORPBentukBadanUsaha.SelectedIndex = i
                        Exit For
                    End If
                    i = i + 1
                Next
                txtCORPNama.Text = Safe(objCustomerCTR.CustomerName)
                objSBidangUsaha = objBidangUsaha.Find(MsBidangUsahaColumn.IdBidangUsaha, objCustomerCTR.CORP_FK_MsBidangUsaha_Id)
                If Not IsNothing(objSBidangUsaha) Then
                    txtCORPBidangUsaha.Text = Safe(objSBidangUsaha.NamaBidangUsaha)
                    'hfCORPBidangUsaha.Value = Safe(objSBidangUsaha.IdBidangUsaha)
                End If

                SafeNumIndex(rblCORPTipeAlamat, objCustomerCTR.CORP_TipeAlamat)

                txtCORPDLNamaJalan.Text = Safe(objCustomerCTR.CORP_DOM_NamaJalan)
                txtCORPDLRT.Text = Safe(objCustomerCTR.CORP_DOM_RT)
                txtCORPDLRW.Text = Safe(objCustomerCTR.CORP_DOM_RW)

                objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objCustomerCTR.CORP_DOM_FK_MsKelurahan_Id)
                If Not IsNothing(objSKelurahan) Then
                    txtCORPDLKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
                    'hfCORPDLKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
                End If
                objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objCustomerCTR.CORP_DOM_FK_MsKecamatan_Id)
                If Not IsNothing(objSkecamatan) Then
                    txtCORPDLKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
                    'hfCORPDLKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objCustomerCTR.CORP_DOM_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtCORPDLKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    'hfCORPDLKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtCORPDLKodePos.Text = Safe(objCustomerCTR.CORP_DOM_KodePos)
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objCustomerCTR.CORP_DOM_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtCORPDLProvinsi.Text = Safe(objSProvinsi.Nama)
                    'hfCORPDLProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, objCustomerCTR.CORP_DOM_FK_MsNegara_Id)
                If Not IsNothing(objSnegara) Then
                    txtCORPDLNegara.Text = Safe(objSnegara.NamaNegara)
                    'hfCORPDLNegara.Value = Safe(objSnegara.IDNegara)
                End If
                txtCORPLNNamaJalan.Text = Safe(objCustomerCTR.CORP_LN_NamaJalan)
                objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, objCustomerCTR.CORP_LN_FK_MsNegara_Id)
                If Not IsNothing(objSnegara) Then
                    txtCORPLNNegara.Text = Safe(objSnegara.NamaNegara)
                    'hfCORPLNNegara.Value = Safe(objSnegara.IDNegara)
                End If
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objCustomerCTR.CORP_LN_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtCORPLNProvinsi.Text = Safe(objSProvinsi.Nama)
                    'hfCORPLNProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objCustomerCTR.CORP_LN_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtCORPLNKota.Text = Safe(objSkotaKab.NamaKotaKab)
                    'hfCORPLNKota.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtCORPLNKodePos.Text = Safe(objCustomerCTR.CORP_LN_KodePos)
                txtCORPNPWP.Text = Safe(objCustomerCTR.NPWP)

            End If


        End Using
    End Sub

    Sub LoadCustomerCTRTransaction()
        Me.SetnGetTransaction = DataRepository.CustomerCTRTransactionProvider.GetPaged(CustomerCTRTransactionColumn.FK_CustomerCTR_ID.ToString & "=" & Me.GetPK_CustomerCTR_ID, CustomerCTRTransactionColumn.PK_CustomerCTRTransaction_ID.ToString, 0, Integer.MaxValue, Nothing)
        Me.GridViewTransaction.DataSource = Me.SetnGetTransaction
        Me.GridViewTransaction.DataBind()
    End Sub

  
    Protected Sub GridViewTransaction_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridViewTransaction.ItemDataBound
        If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then

            e.Item.Cells(3).Text = CStr((e.Item.ItemIndex + 1))


            If e.Item.Cells(7).Text <> "&nbsp;" Then
                Using objLblTujuanTransaksi As Label = CType(e.Item.FindControl("LblTujuanTransaksi"), Label)
                    Using ObjMsTujuanTransaksi As MsTujuanTransaksi = AMLBLL.CustomerCTRBLL.GetMsTujuanTransaksiByPK(CInt(e.Item.Cells(1).Text))
                        If Not IsNothing(ObjMsTujuanTransaksi) Then
                            objLblTujuanTransaksi.Text = ObjMsTujuanTransaksi.NamaTujuanTransaksi
                        End If
                    End Using
                End Using
            End If

            If e.Item.Cells(8).Text <> "&nbsp;" Then
                Using objLblRelasiDenganPemilikRekening As Label = CType(e.Item.FindControl("LblRelasiDenganPemilikRekening"), Label)
                    Using ObjMsRelasiDenganPemilikRekening As MsRelasiDenganPemilikRekening = AMLBLL.CustomerCTRBLL.GetMsRelasiDenganPemilikRekeningByPK(CInt(e.Item.Cells(2).Text))
                        If Not IsNothing(ObjMsRelasiDenganPemilikRekening) Then
                            objLblRelasiDenganPemilikRekening.Text = ObjMsRelasiDenganPemilikRekening.NamaRelasiDenganPemilikRekening
                        End If
                    End Using
                End Using
            End If

        End If
    End Sub



End Class

