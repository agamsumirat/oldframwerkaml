Option Strict On
Option Explicit On
Imports amlbll
Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports SahassaNettier.Services
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper


Imports System.IO
Imports System.Collections.Generic

Partial Class MappingGroupMenuSTRPPATKView
    Inherits Parent

    Public Property PK_MappingGroupMenuSTRPPATK_ID() As String
        Get
            Return CType(Session("MappingGroupMenuSTRPPATKView.PK_MappingGroupMenuSTRPPATK_ID"), String)
        End Get
        Set(ByVal value As String)
            Session("MappingGroupMenuSTRPPATKView.PK_MappingGroupMenuSTRPPATK_ID") = value
        End Set
    End Property

    Public Property SetnGetGroupName() As String
        Get
            If Not Session("MappingGroupMenuSTRPPATKView.SetnGetGroupName") Is Nothing Then
                Return CStr(Session("MappingGroupMenuSTRPPATKView.SetnGetGroupName"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("MappingGroupMenuSTRPPATKView.SetnGetGroupName") = value
        End Set
    End Property

    Private Property SetnGetSort() As String
        Get
            Return CType(IIf(Session("MappingGroupMenuSTRPPATKView.Sort") Is Nothing, "GroupName  asc", Session("MappingGroupMenuSTRPPATKView.Sort")), String)
        End Get
        Set(ByVal Value As String)
            Session("MappingGroupMenuSTRPPATKView.Sort") = Value
        End Set
    End Property

    Private Property SetnGetRowTotal() As Int32
        Get
            Return CType(IIf(Session("MappingGroupMenuSTRPPATKView.RowTotal") Is Nothing, 0, Session("MappingGroupMenuSTRPPATKView.RowTotal")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("MappingGroupMenuSTRPPATKView.RowTotal") = Value
        End Set
    End Property
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property

    Private Property SetnGetCurrentPage() As Int32
        Get
            Return CType(IIf(Session("MappingGroupMenuSTRPPATKView.CurrentPage") Is Nothing, 0, Session("MappingGroupMenuSTRPPATKView.CurrentPage")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("MappingGroupMenuSTRPPATKView.CurrentPage") = Value
        End Set
    End Property

    Function Getvw_MappingGroupMenuSTRPPATK(ByVal strWhereClause As String, ByVal strOrderBy As String, ByVal intStart As Integer, ByVal intPageLength As Integer, ByRef intCount As Integer) As VList(Of vw_MappingGroupMenuSTRPPATK)
        Dim Objvw_MappingGroupMenuSTRPPATK As VList(Of vw_MappingGroupMenuSTRPPATK) = Nothing
        Objvw_MappingGroupMenuSTRPPATK = DataRepository.vw_MappingGroupMenuSTRPPATKProvider.GetPaged(strWhereClause, strOrderBy, intStart, intPageLength, intCount)
        Return Objvw_MappingGroupMenuSTRPPATK
    End Function

    Public Property SetnGetBindTable() As VList(Of vw_MappingGroupMenuSTRPPATK)
        Get
            Dim strWhereClause(-1) As String
            Dim strAllWhereClause As String = ""
            If Me.SetnGetCurrentPage > Me.GetPageTotal - 1 And Me.GetPageTotal - 1 > 0 Then
                Me.SetnGetCurrentPage = Me.GetPageTotal - 1
            End If
            If SetnGetGroupName.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "GroupName like '%" & SetnGetGroupName.Trim.Replace("'", "''") & "%'"
            End If

            strAllWhereClause = String.Join(" and ", strWhereClause)
            'If strAllWhereClause.Trim.Length > 0 Then
            '    strAllWhereClause += " and Fk_MsGroupApproval_Id=" & Sahassa.AML.Commonly.SessionGroupApprovalId & " and " & vw_MsHelp_ApprovalColumn.FK_MsUserID.ToString & " <> " & Sahassa.AML.Commonly.SessionPkUserId
            'Else
            '    strAllWhereClause += "  Fk_MsGroupApproval_Id =" & Sahassa.AML.Commonly.SessionGroupApprovalId & " and " & vw_MsHelp_ApprovalColumn.FK_MsUserID.ToString & " <> " & Sahassa.AML.Commonly.SessionPkUserId

            'End If
            Session("MappingGroupMenuSTRPPATKView.Table") = Getvw_MappingGroupMenuSTRPPATK(strAllWhereClause, SetnGetSort, SetnGetCurrentPage, Sahassa.AML.Commonly.GetDisplayedTotalRow, Me.SetnGetRowTotal)


            Return CType(Session("MappingGroupMenuSTRPPATKView.Table"), VList(Of vw_MappingGroupMenuSTRPPATK))


        End Get
        Set(ByVal value As VList(Of vw_MappingGroupMenuSTRPPATK))
            Session("MappingGroupMenuSTRPPATKView.Table") = value
        End Set
    End Property



    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select            
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub ClearThisPageSessions()
        'Session("MappingGroupMenuSTRPPATKView.PK_MappingGroupMenuSTRPPATK_ApprovalDetail_ID") = Nothing
        Session("MappingGroupMenuSTRPPATKView.SetnGetGroupName") = Nothing

        SetnGetCurrentPage = Nothing
        Me.SetnGetRowTotal = Nothing
        Me.SetnGetSort = Nothing

    End Sub


    Private Sub bindgrid()
        SettingControlSearching()
        Me.GridGMSTRPPATK.DataSource = Me.SetnGetBindTable
        Me.GridGMSTRPPATK.CurrentPageIndex = Me.SetnGetCurrentPage
        Me.GridGMSTRPPATK.VirtualItemCount = Me.SetnGetRowTotal
        Me.GridGMSTRPPATK.DataBind()

        'If SetnGetRowTotal > 0 Then
        '    LabelNoRecordFound.Visible = False
        'Else
        '    LabelNoRecordFound.Visible = True
        'End If
    End Sub

    Private Sub SettingPropertySearching()

        SetnGetGroupName = TxtGroupName.Text.Trim

    End Sub

    Private Sub SettingControlSearching()

        TxtGroupName.Text = SetnGetGroupName

    End Sub



    Private Sub SetInfoNavigate()

        Me.PageCurrentPage.Text = (Me.SetnGetCurrentPage + 1).ToString
        Me.PageTotalPages.Text = Me.GetPageTotal.ToString
        Me.PageTotalRows.Text = Me.SetnGetRowTotal.ToString
        Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
        Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
        Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
        Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0

    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

        Try
            Me.bindgrid()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try

    End Sub

    Protected Sub ImageButtonSearchCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearchCancel.Click
        Try

            SetnGetGroupName = Nothing

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Try
            SettingPropertySearching()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then

                ClearThisPageSessions()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using

                bindgrid()

            End If
            GridGMSTRPPATK.PageSize = CInt(Sahassa.AML.Commonly.GetDisplayedTotalRow)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub LinkButtonAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonAddNew.Click

        Try
            Response.Redirect("MappingGroupMenuSTRPPATKAdd.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try

    End Sub

    Protected Sub GridGMSTRPPATK_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridGMSTRPPATK.EditCommand
        Dim PK_MappingGroupMenuSTRPPATK_ID As Integer 'primary key
        'Dim StrUserID As String  'unik
        Try
            PK_MappingGroupMenuSTRPPATK_ID = CType(e.Item.Cells(1).Text, Integer)
            'StrUserID = e.Item.Cells(2).Text

            Response.Redirect("MappingGroupMenuSTRPPATKEdit.aspx?PK_MappingGroupMenuSTRPPATK_ID=" & PK_MappingGroupMenuSTRPPATK_ID, False)
            'Using ObjMsBranchBll As New SahassaBLL.MsHelpBLL
            '    If ObjMsBranchBll.IsNotExistInApproval(StrUserID) Then
            '        Response.Redirect("MsHelpEdit.aspx?PkMsHelpID=" & PkMsHelpID, False)
            '    End If
            'End Using

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridGMSTRPPATK_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridGMSTRPPATK.DeleteCommand
        Dim PK_MappingGroupMenuSTRPPATK_ID As Integer 'primary key
        'Dim StrUnikID As String  'unik
        Try
            PK_MappingGroupMenuSTRPPATK_ID = CType(e.Item.Cells(1).Text, Integer)
            'StrUnikID = e.Item.Cells(2).Text

            Response.Redirect("MappingGroupMenuSTRPPATKDelete.aspx?PK_MappingGroupMenuSTRPPATK_ID=" & PK_MappingGroupMenuSTRPPATK_ID, False)
            'Using ObjMsBranchBll As New SahassaBLL.MsHelpBLL
            '    If ObjMsBranchBll.IsNotExistInApproval(StrUnikID) Then
            '        Response.Redirect("MsHelpDelete.aspx?PkMsHelpID=" & PkMsHelpID, False)
            '    End If
            'End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub



    Protected Sub GridGMSTRPPATK_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridGMSTRPPATK.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then

                Dim EditButton As LinkButton = CType(e.Item.Cells(3).FindControl("LnkEdit"), LinkButton)
                Dim DelButton As LinkButton = CType(e.Item.Cells(4).FindControl("LnkDelete"), LinkButton)

                e.Item.Cells(0).Text = CStr((e.Item.ItemIndex + 1))
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            If IsNumeric(Me.TextGoToPage.Text) AndAlso (CInt(Me.TextGoToPage.Text) > 0) Then
                If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                    Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                Else
                    Throw New Exception(Sahassa.AML.CommonlyEnum.EXECPTION_PAGENUMBER_GREATER_TOTALPAGENUMBER)
                End If
            Else
                Throw New Exception(Sahassa.AML.CommonlyEnum.EXECPTION_PAGENUMBER_NUMERICPOSITIF)
            End If

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridGMSTRPPATK_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridGMSTRPPATK.SortCommand
        Dim GridUser As DataGrid = CType(source, DataGrid)
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class
