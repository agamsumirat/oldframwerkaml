﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="WUUpload_ApprovalDetail.aspx.vb" Inherits="WUUpload_ApprovalDetail" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
<script src="script/popcalendar.js"></script>
    <div id="div1" class="divcontent">
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<tr>
				<td id="tdcontent" height="99%" valign="top" bgcolor="#FFFFFF" width="100%">
					<div id="divcontent" class="divcontent">
						<table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>								
								<td class="divcontentinside" bgcolor="#FFFFFF">                                
                                    <ajax:AjaxPanel ID="a" runat="server" meta:resourcekey="aResource1">
                                                            <asp:ValidationSummary ID="ValidationSummaryunhandle" runat="server" 
                                                                CssClass="validation" 
                                                                meta:resourcekey="ValidationSummaryunhandleResource1" Width="590px" />
                                                            <asp:ValidationSummary ID="ValidationSummaryhandle" runat="server" 
                                                                CssClass="validationok" ForeColor="Black" 
                                                                meta:resourcekey="ValidationSummaryhandleResource1" ValidationGroup="handle" />
                                    </ajax:AjaxPanel>
                                    <ajax:AjaxPanel ID="AjaxPanel5" runat="server" Width="100%" meta:resourcekey="AjaxPanel5Resource1">
                                <asp:MultiView ID="MultiViewUpload" runat="server" ActiveViewIndex="0">
                                    <asp:View ID="VwUpload" runat="server">
                                            <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
											                height="72" style="border-top-style: none; border-right-style: none; border-left-style: none;
											                border-bottom-style: none" bgcolor="#dddddd" width="100%">
											            <tr bgcolor="#ffffff">
												<td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
													border-right-style: none; border-left-style: none; border-bottom-style: none; width: 100%;">
													<strong>
														<img src="Images/dot_title.gif" width="17" height="17">
														<asp:Label ID="Label7" runat="server" Text="WESTERN UNION UPLOAD APPROVAL DETAIL" 
                                                        Font-Size="Small"></asp:Label>
														<hr />
													</strong>&nbsp;&nbsp;&nbsp;&nbsp;<ajax:AjaxPanel ID="AjxMessage" runat="server" meta:resourcekey="AjxMessageResource1">
														<asp:Label ID="LblMessage" background="images/validationbground.gif" runat="server"
															CssClass="validationok" Width="100%" meta:resourcekey="LblMessageResource1"></asp:Label>
													</ajax:AjaxPanel>
												</td>
											            </tr>
										            </table>										            
											<table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
												border="2">
												<tr id="searchbox">
													<td colspan="2" valign="top" width="100%" bgcolor="#ffffff">
														<table cellpadding="0" width="100%" border="0">
															<tr>
																<td valign="middle" nowrap>
																	<ajax:AjaxPanel ID="AjaxPanel1" runat="server" Width="100%" 
                                                                        meta:resourcekey="AjaxPanel1Resource1">
																		<table style="height: 100%">
																			<tr>
																				<td colspan="3" style="height: 7px">
																				</td>
																			</tr>
																			<tr>
																				<td nowrap style="height: 26px; width: 119px;">
																					<asp:Label ID="LabelMsUser_StaffName" runat="server" Text="Name"></asp:Label>
																				</td>
																				<td>
																					:
																				</td>
																				<td style="height: 26px; width: 765px;">
																					&nbsp;&nbsp;<asp:TextBox ID="txtMsUser_StaffName" runat="server" CssClass="searcheditbox" 
                                                                                        TabIndex="2" Width="296px"></asp:TextBox>
																				</td>
																			</tr>
																			<tr>
																				<td nowrap style="height: 26px; width: 119px;">
																					<asp:Label ID="LabelRequestedDate" runat="server" Text="Requested Date"></asp:Label>
																				</td>
																				<td>
																					:
																				</td>
																				<td style="height: 26px; width: 765px;">
                                                                                    &nbsp;
																					<asp:TextBox ID="txtRequestedDate" runat="server" CssClass="textBox" 
                                                                                        MaxLength="1000" TabIndex="2" ToolTip="RequestedDate" Width="296px"></asp:TextBox>
                                                                                    &nbsp;&nbsp;&nbsp;
																				</td>
																			</tr>
																		</table>
																	</ajax:AjaxPanel>
																</td>
															</tr>
														</table>
														<ajax:AjaxPanel ID="AjaxPanel3" runat="server" 
                                                            meta:resourcekey="AjaxPanel3Resource1" Width="100%">
															<asp:CustomValidator ID="CvalPageErr" runat="server" Display="None" meta:resourcekey="CvalPageErrResource1"></asp:CustomValidator></ajax:AjaxPanel>
														<ajax:AjaxPanel ID="Ajaxpanel2" runat="server" 
                                                            meta:resourcekey="Ajaxpanel2Resource1" Width="100%">
															<asp:CustomValidator ID="CvalHandleErr" runat="server" ValidationGroup="handle" Display="None"
																meta:resourcekey="CvalHandleErrResource1"></asp:CustomValidator></ajax:AjaxPanel>
													</td>
												</tr>
											</table>
										            <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
											               border="2">										
                                                        <tr>                                                    
                                                        <td bgcolor="#ffffff">
                                                            <ajax:AjaxPanel ID="AjaxPanel4" runat="server" 
                                                                meta:resourcekey="AjaxPanel4Resource1" Width="100%">                                                                
                                                                <asp:DataGrid ID="DataGrid1" runat="server" 
                                                                    AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" 
                                                                    CellPadding="4" Font-Bold="False" Font-Italic="False" 
                                                                    Font-Overline="False" Font-Strikeout="False" Font-Underline="False" 
                                                                    ForeColor="White" GridLines="None" HorizontalAlign="Center" Width="100%" 
                                                                    BorderColor="#003300" BorderStyle="Solid">
                                                                    <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                                                                    <HeaderStyle BackColor="#1C5E55" Font-Bold="True" Font-Italic="False" 
                                                                        Font-Overline="False" Font-Strikeout="False" Font-Underline="False" 
                                                                        ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" 
                                                                        Wrap="False" />
                                                                    <Columns>
                                                                        <asp:BoundColumn HeaderText="No">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                                                                        </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="PK_WUUpload_id" HeaderText="PK_WUUpload_id" 
                                                                                SortExpression="PK_WUUpload_id asc" Visible="False"></asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="SortedBYvalue" HeaderText="SortedBYvalue" SortExpression="SortedBYvalue asc">
                                                                            </asp:BoundColumn>
																		    <asp:BoundColumn DataField="SortedBYID" HeaderText="SortedBYID">
                                                                                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
																		    <asp:BoundColumn HeaderText="SendpayIndicator" DataField="SendpayIndicator">
                                                                                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
																		    <asp:BoundColumn DataField="Account" HeaderText="Account">
                                                                                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="AgentCountryCode" HeaderText="AgentCountryCode">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="AgentCountryName" HeaderText="AgentCountryName">
                                                                           <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="MTCN" HeaderText="MTCN">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="TransactionDate" HeaderText="TransactionDate">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="TransactionTime" HeaderText="TransactionTime">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="Principal" HeaderText="Principal">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="Currency" HeaderText="Currency">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="CurrencyName" HeaderText="CurrencyName">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="customer" HeaderText="customer">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="customerotherside" HeaderText="customerotherside">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="address" HeaderText="address">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="City" HeaderText="City">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="Postalcode" HeaderText="Postalcode">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="phone" HeaderText="phone">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="IDType" HeaderText="IDType">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="IDNumber" HeaderText="IDNumber">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="AgentLocation" HeaderText="AgentLocation">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="OtherSideCountrycode" HeaderText="OtherSideCountrycode">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="OtherSideCountryname" HeaderText="OtherSideCountryname">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="Source" HeaderText="Source">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="AgentCity" HeaderText="AgentCity">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="WUCardNumber" HeaderText="WUCardNumber">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="ReceiverIsCompany" HeaderText="ReceiverIsCompany">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="SenderIsCompany" HeaderText="SenderIsCompany">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="S_SEN_FIRSTNAME" HeaderText="S_SEN_FIRSTNAME">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="S_SEN_LASTNAME" HeaderText="S_SEN_LASTNAME">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="S_SEN_CITY" HeaderText="S_SEN_CITY">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="S_SEN_ZIP" HeaderText="S_SEN_ZIP">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="S_SEN_ADDRESS" HeaderText="S_SEN_ADDRESS">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="S_REC_FIRSTNAME" HeaderText="S_REC_FIRSTNAME">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="S_REC_LASTNAME" HeaderText="S_REC_LASTNAME">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="S_SEN_PHONE" HeaderText="S_SEN_PHONE">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="S_SEN_IDTYPE" HeaderText="S_SEN_IDTYPE">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="S_SEN_IDISSUER" HeaderText="S_SEN_IDISSUER">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="S_SEN_IDNUMBER" HeaderText="S_SEN_IDNUMBER">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="S_SEN_ISSUEDDATE" HeaderText="S_SEN_ISSUEDDATE">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="S_SEN_EXPDATE" HeaderText="S_SEN_EXPDATE">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="S_SEN_BIRTHDATE" HeaderText="S_SEN_BIRTHDATE">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="S_SEN_OCCUPATION" HeaderText="S_SEN_OCCUPATION">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="S_SEN_COMMENTS" HeaderText="S_SEN_COMMENTS">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="P_REC_FIRSTNAME" HeaderText="P_REC_FIRSTNAME">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="P_REC_LASTNAME" HeaderText="P_REC_LASTNAME">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="P_REC_ADDRESS" HeaderText="P_REC_ADDRESS">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="P_REC_ADDRESS_2" HeaderText="P_REC_ADDRESS_2">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="P_REC_CITY" HeaderText="P_REC_CITY">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="P_REC_ZIP" HeaderText="P_REC_ZIP">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="P_REC_PHONE" HeaderText="P_REC_PHONE">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="P_SEN_FIRSTNAME" HeaderText="P_SEN_FIRSTNAME">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="P_SEN_LASTNAME" HeaderText="P_SEN_LASTNAME">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="P_REC_IDTYPE" HeaderText="P_REC_IDTYPE">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="P_REC_IDISSUER" HeaderText="P_REC_IDISSUER">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="P_REC_IDNUMBER" HeaderText="P_REC_IDNUMBER">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="P_REC_ISSUEDDATE" HeaderText="P_REC_ISSUEDDATE">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="P_REC_EXPDATE" HeaderText="P_REC_EXPDATE">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="P_REC_BIRTHDATE" HeaderText="P_REC_BIRTHDATE">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="P_REC_OCCUPATION" HeaderText="P_REC_OCCUPATION">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="P_REC_COMMENTS" HeaderText="P_REC_COMMENTS">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="S_SEN_IDHASEXP" HeaderText="S_SEN_IDHASEXP">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="S_SEN_HASID" HeaderText="S_SEN_HASID">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="S_SEN_BIRTHCOUNTRY" HeaderText="S_SEN_BIRTHCOUNTRY">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="P_REC_PROVINCE" HeaderText="P_REC_PROVINCE">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="P_REC_COUNTRY" HeaderText="P_REC_COUNTRY">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="P_REC_HASPHONE" HeaderText="P_REC_HASPHONE">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="P_REC_IDHASEXP" HeaderText="P_REC_IDHASEXP">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="P_REC_BIRTHCOUNTRY" HeaderText="P_REC_BIRTHCOUNTRY">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="P_SENDERS_BIRTHCOUNTRY" HeaderText="P_SENDERS_BIRTHCOUNTRY">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="P_SENDERS_BIRTHDATE" HeaderText="P_SENDERS_BIRTHDATE">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="P_SENDERS_ADDRESS" HeaderText="P_SENDERS_ADDRESS">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="P_SENDERS_CITY" HeaderText="P_SENDERS_CITY">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="P_SENDERS_ZIP" HeaderText="P_SENDERS_ZIP">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="P_SENDERS_COUNTRY" HeaderText="P_SENDERS_COUNTRY">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="S_SEN_VERIFIED_CUST_DATA" HeaderText="S_SEN_VERIFIED_CUST_DATA">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="P_REC_VERIFIED_CUST_DATA" HeaderText="P_REC_VERIFIED_CUST_DATA">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="S_SEN_NATIONALITY" HeaderText="S_SEN_NATIONALITY">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="S_SEN_GENDER" HeaderText="S_SEN_GENDER">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="S_SEN_EMPLOYER_NAME" HeaderText="S_SEN_EMPLOYER_NAME">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="S_SEN_WORK_NATURE" HeaderText="S_SEN_WORK_NATURE">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="S_SEN_REASONOFTRANS" HeaderText="S_SEN_REASONOFTRANS">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="P_REC_NATIONALITY" HeaderText="P_REC_NATIONALITY">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="P_REC_GENDER" HeaderText="P_REC_GENDER">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="P_REC_EMPLOYER_NAME" HeaderText="P_REC_EMPLOYER_NAME">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="P_REC_WORK_NATURE" HeaderText="P_REC_WORK_NATURE">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="P_REC_REASONOFTRANS" HeaderText="P_REC_REASONOFTRANS">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="ROWCOLOR" HeaderText="ROWCOLOR">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="COLCOLOR" HeaderText="COLCOLOR">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="GENDT" HeaderText="GENDT">
                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                    HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                    VerticalAlign="Middle" />
                                                                            </asp:BoundColumn>
                                                                    </Columns>
                                                                    <EditItemStyle BackColor="#7C6F57" />
                                                                    <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                                                                    <SelectedItemStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                                                                    <AlternatingItemStyle BackColor="White" />
                                                                    <ItemStyle BackColor="#E3EAEB" />
                                                                </asp:DataGrid>
                                                            </ajax:AjaxPanel>
                                                        </td>
                                                    </tr>                                                       
                                                        <tr>
                                                            <td>
                                                                <br />
                                                                <ajax:AjaxPanel ID="ajax10" runat="server" Width="464px" >
                                                                    <asp:Button ID="BtnAccept" runat="server" Text="Accept" />
                                                                    &nbsp;
                                                                    <asp:Button ID="BtnReject" runat="server" Text="Reject" />
                                                                    &nbsp;
                                                                    <asp:Button ID="BtnCancel" runat="server" Text="&lt;&lt; Back" />
                                                                </ajax:AjaxPanel>
                                                            </td>
                                                        </tr>
                                                        </table>
                                                    
                                    </asp:View>
                                    <asp:View ID="VwConfirmation" runat="server">
											<table style="width: 100%" bordercolor="#ffffff" cellspacing="1" cellpadding="2"
												width="100%" bgcolor="#dddddd" border="0">
												<tr bgcolor="#ffffff">
													<td colspan="2" align="center" style="height: 17px">
														<asp:Label ID="LblConfirmation" runat="server" meta:resourcekey="LblConfirmationResource1"></asp:Label>
													</td>
												</tr>
												<tr bgcolor="#ffffff">
													<td align="center" colspan="2">
														<asp:ImageButton ID="ImgBtnAdd" runat="server" ImageUrl="~/images/button/Ok.gif"
															CausesValidation="False" meta:resourcekey="ImgBtnAddResource1" />
													</td>
												</tr>
											</table>
										</asp:View>
                                </asp:MultiView> 
                                </ajax:AjaxPanel>
                               </td>
                            </tr>
						</table>
                    </div> 								
				</td>
			</tr>
			<tr>							
					<ajax:AjaxPanel ID="AjaxPanel14" runat="server" meta:resourcekey="AjaxPanel14Resource1">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td align="left" background="Images/button-bground.gif" valign="middle">
									<img height="1" src="Images/blank.gif" width="5" />
								</td>
								<td align="left" background="Images/button-bground.gif" valign="middle">
									<img height="15" src="images/arrow.gif" width="15" />&nbsp;
								</td>
								<td background="Images/button-bground.gif">
									&nbsp;
								</td>
								<td background="Images/button-bground.gif">
									&nbsp;
								</td>
								<td background="Images/button-bground.gif">
									&nbsp;
								</td>
								<td background="Images/button-bground.gif" width="99%">
									<img height="1" src="Images/blank.gif" width="1" />
								</td>
								<td>
									
								</td>
							</tr>
						</table>
						<asp:CustomValidator ID="CustomValidator1" runat="server" Display="None" ValidationGroup="handle"
							    meta:resourcekey="CustomValidator1Resource1"></asp:CustomValidator>
                        <asp:CustomValidator ID="CustomValidator2" runat="server" Display="None" 
                                meta:resourcekey="CustomValidator2Resource1"></asp:CustomValidator>
                        </ajax:AjaxPanel>
            </tr>	
	        </table>
    </div>
</asp:Content>

