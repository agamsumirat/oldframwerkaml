﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="UploadHiportAccount.aspx.vb" Inherits="UploadHiportAccount" %>


<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">

<ajax:ajaxpanel id="AjaxPanel1" runat="server">
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
        <asp:View ID="View1" runat="server">

<table id="title"  border="2" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr>
            <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Upload Hiport Account
                    <hr />
                </strong>
                <asp:Label ID="LblSuccess" runat="server" CssClass="validationok" Visible="False"
                    Width="100%"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" colspan="4" style="border-top-style: none; border-right-style: none;
                border-left-style: none; height: 24px; border-bottom-style: none"><span style="color: #ff0000"><asp:CustomValidator ID="cvalPageError" runat="server"
                    Display="None"></asp:CustomValidator></span></td>
        </tr>
    </table>	
    <table style="width: 100%">
      <tr>
            <td style="width: 200px" valign="top">
                Upload Hiport Account</td>
            <td style="width: 1px" valign="top">
                :</td>
            <td valign="top" style="width: 250px">
                <asp:FileUpload ID="FileUpload1" runat="server" />&nbsp;&nbsp;&nbsp;
            </td>
            <td valign="top">
                <asp:ImageButton ajaxcall="none" ID="ImgSearch" runat="server" 
                    ImageUrl="~/Images/button/view.gif" style="height: 17px" /></td>
        </tr>
       
        <tr>
            <td style="width: 200px" valign="top">
            </td>
            <td style="width: 1px" valign="top">
            </td>
            <td colspan="2" valign="top">
            </td>
        </tr>
       
        <tr>
            <td colspan="4" valign="top">
                <asp:Panel ID="PanelValidasi" runat="server" Visible="false" GroupingText="Validasi">                    
                Invalid Data
                    <asp:TextBox ID="TxtValidasi" runat="server" Rows="15" TextMode="MultiLine" 
                        Width="100%"></asp:TextBox>
                </asp:Panel>
            </td>
        </tr>
       
        <tr>
            <td colspan="4" valign="top">

                  <asp:Panel ID="PanelValidData" runat="server" Visible="false" GroupingText="Valid Data">                    
                Valid Data
                      <asp:GridView ID="GridValidData" runat="server" BackColor="White" 
                          BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" 
                          EnableModelValidation="True" ForeColor="Black" GridLines="Vertical" 
                          AllowPaging="True">
                          <AlternatingRowStyle BackColor="White" />
                          <FooterStyle BackColor="#CCCC99" />
                          <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                          <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                          <RowStyle BackColor="#F7F7DE" />
                          <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                      </asp:GridView>
                      <br />
                      <asp:ImageButton ID="ImageSave" runat="server" 
                          ImageUrl="~/Images/button/save.gif" Visible="false" />
                </asp:Panel>
                </td>
        </tr>
       
    </table>
    

        </asp:View>
        <asp:View ID="View2" runat="server">
            &nbsp;<table style="width: 100%">
                <tr>
                    <td align="center">
                        <asp:Label ID="LblMessage" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:ImageButton ID="ImgBack" runat="server" ImageUrl="~/Images/button/ok.gif" /></td>
                </tr>
            </table>
        </asp:View>
    </asp:MultiView>
    </ajax:ajaxpanel>
</asp:Content>
