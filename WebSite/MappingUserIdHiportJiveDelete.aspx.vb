Option Strict On
Option Explicit On
Imports amlbll
Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports SahassaNettier.Services
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper


Imports System.IO
Imports System.Collections.Generic

Partial Class MappingUserIdHiportJiveDelete
    Inherits Parent

    Public ReadOnly Property PK_MappingUserIdHIPORTJIVE_ID() As Integer
        Get
            Dim strTemp As String = Request.Params("PK_MappingUserIdHIPORTJIVE_ID")
            Dim intResult As Integer
            If Integer.TryParse(strTemp, intResult) Then
                Return intResult
            Else
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = "Parameter PK_MappingUserIdHIPORTJIVE_ID is invalid."

            End If
        End Get
    End Property

    Public ReadOnly Property Objvw_MappingUserIdHIPORTJIVE() As VList(Of vw_MappingUserIdHiportJive)
        Get
            If Session("Objvw_MappingUserIdHIPORTJIVE.MappingUserIdHIPORTJIVEDelete") Is Nothing Then

                Session("Objvw_MappingUserIdHIPORTJIVE.MappingUserIdHIPORTJIVEDelete") = DataRepository.vw_MappingUserIdHiportJiveProvider.GetPaged("PK_MappingUserIdHIPORTJIVE_ID = '" & PK_MappingUserIdHIPORTJIVE_ID & "'", "", 0, Integer.MaxValue, 0)

            End If
            Return CType(Session("Objvw_MappingUserIdHIPORTJIVE.MappingUserIdHIPORTJIVEDelete"), VList(Of vw_MappingUserIdHiportJive))
        End Get
    End Property


    Private Sub loaddata()

        If Objvw_MappingUserIdHIPORTJIVE.Count > 0 Then
            TxtDelete.Text = Objvw_MappingUserIdHIPORTJIVE(0).UserName

        Else
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = MappingUserIdHiportJiveBLL.DATA_NOTVALID
        End If
    End Sub

    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim oTrans As SqlTransaction = Nothing
        Try

            Using ObjList As MappingUserIdHiportJive = DataRepository.MappingUserIdHiportJiveProvider.GetByPK_MappingUserIdHiportJive_ID(PK_MappingUserIdHIPORTJIVE_ID)
                If ObjList Is Nothing Then
                    Throw New AMLBLL.SahassaException(MappingUserIdHiportJiveEnum.DATA_NOTVALID)
                End If
            End Using

            Dim UserID As Integer = 0
            Dim Username As String = ""

            Using Objlist As TList(Of MappingUserIdHiportJive) = DataRepository.MappingUserIdHiportJiveProvider.GetPaged("PK_MappingUserIdHIPORTJIVE_ID = '" & PK_MappingUserIdHIPORTJIVE_ID & "'", "", 0, Integer.MaxValue, 0)
                If Objlist.Count > 0 Then

                    AMLBLL.MappingGroupMenuHiportJiveBLL.IsDataValidDeleteApproval(CStr(PK_MappingUserIdHIPORTJIVE_ID))

                    If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                        Using Objvw_MappingUserIdHIPORTJIVE As VList(Of vw_MappingUserIdHiportJive) = DataRepository.vw_MappingUserIdHiportJiveProvider.GetPaged("PK_MappingUserIdHIPORTJIVE_ID = '" & PK_MappingUserIdHIPORTJIVE_ID & "'", "", 0, Integer.MaxValue, 0)
                            Me.LblSuccess.Text = "Terimakasih data " & Objvw_MappingUserIdHIPORTJIVE(0).UserName & " sudah dihapus ."
                            Me.LblSuccess.Visible = True
                            Using ObjMappingUserIdHIPORTJIVE As TList(Of MappingUserIdHiportJive) = DataRepository.MappingUserIdHiportJiveProvider.GetPaged("PK_MappingUserIdHIPORTJIVE_ID = '" & PK_MappingUserIdHIPORTJIVE_ID & "'", "", 0, Integer.MaxValue, 0)
                                UserID = CInt(ObjMappingUserIdHIPORTJIVE(0).FK_User_ID)
                                AMLBLL.MappingUserIdHiportJiveBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "MappingUserIdHIPORTJIVE", "UserName", "Delete", "", Objvw_MappingUserIdHIPORTJIVE(0).UserName, "Acc")
                                AMLBLL.MappingUserIdHiportJiveBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "MappingUserIdHIPORTJIVE", "Kode_AO", "Delete", "", txtKodeAO.Text, "Acc")
                                DataRepository.MappingUserIdHiportJiveProvider.Delete(ObjMappingUserIdHIPORTJIVE)
                            End Using
                        End Using
                    Else

                        Using ObjMappingUserIdHIPORTJIVE_Approval As New MappingUserIdHiportJive_Approval
                            Using ObjMappingUserIdHIPORTJIVEList As VList(Of vw_MappingUserIdHiportJive) = DataRepository.vw_MappingUserIdHiportJiveProvider.GetPaged("PK_MappingUserIdHIPORTJIVE_ID = '" & PK_MappingUserIdHIPORTJIVE_ID & "'", "", 0, Integer.MaxValue, 0)
                                If ObjMappingUserIdHIPORTJIVEList.Count > 0 Then

                                    Username = ObjMappingUserIdHIPORTJIVEList(0).UserName
                                    UserID = CInt(ObjMappingUserIdHIPORTJIVEList(0).FK_User_ID)
                                    ObjMappingUserIdHIPORTJIVE_Approval.FK_MsUserID = Sahassa.AML.Commonly.SessionPkUserId
                                    ObjMappingUserIdHIPORTJIVE_Approval.UserName = Username
                                    ObjMappingUserIdHIPORTJIVE_Approval.FK_ModeID = CInt(Sahassa.AML.Commonly.TypeMode.Delete)
                                    ObjMappingUserIdHIPORTJIVE_Approval.CreatedDate = Now
                                    AMLBLL.MappingUserIdHiportJiveBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "MappingUserIdHIPORTJIVE", "Username", "Delete", "", Username, "PendingApproval")
                                    AMLBLL.MappingUserIdHiportJiveBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "MappingUserIdHIPORTJIVE", "Kode_AO", "Delete", "", ObjMappingUserIdHIPORTJIVEList(0).Kode_AO, "PendingApproval")
                                    DataRepository.MappingUserIdHiportJive_ApprovalProvider.Save(ObjMappingUserIdHIPORTJIVE_Approval)

                                    Using ObjMappingUserIdHIPORTJIVE_approvalDetail As New MappingUserIdHiportJive_ApprovalDetail
                                        ObjMappingUserIdHIPORTJIVE_approvalDetail.FK_MappingUserIdHiportJive_Approval_ID = ObjMappingUserIdHIPORTJIVE_Approval.PK_MappingUserIdHiportJive_Approval_ID
                                        ObjMappingUserIdHIPORTJIVE_approvalDetail.FK_User_ID = UserID
                                        ObjMappingUserIdHIPORTJIVE_approvalDetail.PK_MappingUserIdHiportJive_ID = PK_MappingUserIdHIPORTJIVE_ID
                                        DataRepository.MappingUserIdHiportJive_ApprovalDetailProvider.Save(ObjMappingUserIdHIPORTJIVE_approvalDetail)
                                    End Using

                                    'Sahassa.AML.Commonly.SessionIntendedPage = "MappingUserIdHIPORTJIVEView.aspx"
                                    'Me.Response.Redirect("MappingUserIdHIPORTJIVEView.aspx", False)
                                    Using ObjUser As TList(Of User) = DataRepository.UserProvider.GetPaged("PKUserID = '" & UserID & "'", "", 0, Integer.MaxValue, 0)
                                        Me.LblSuccess.Text = "Data saved successfully and waiting for Approval."
                                        Me.LblSuccess.Visible = True
                                    End Using
                                End If
                            End Using
                        End Using
                    End If
                    MultiView1.ActiveViewIndex = 1
                Else
                    Me.LblSuccess.Text = "Data already deleted"
                    Me.LblSuccess.Visible = True
                End If
            End Using
        Catch ex As Exception
            If Not oTrans Is Nothing Then oTrans.Rollback()
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not oTrans Is Nothing Then
                oTrans.Dispose()
                oTrans = Nothing
            End If
        End Try
    End Sub



    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "MappingUserIdHIPORTJIVEView.aspx"
            Me.Response.Redirect("MappingUserIdHIPORTJIVEView.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'Using Transcope As New Transactions.TransactionScope
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                '    Transcope.Complete()
            End Using
            Session("Objvw_MappingUserIdHIPORTJIVE.MappingUserIdHIPORTJIVEDelete") = Nothing
            If Not Page.IsPostBack Then
                loaddata()
                MultiView1.ActiveViewIndex = 0
            End If
            'End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub btnOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnOK.Click
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "MappingUserIdHIPORTJIVEView.aspx"
            Me.Response.Redirect("MappingUserIdHIPORTJIVEView.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class


