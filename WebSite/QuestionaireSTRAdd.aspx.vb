Option Strict On
Option Explicit On
Imports amlbll
Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports SahassaNettier.Services
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Imports System.IO
Imports System.Collections.Generic

Partial Class QuestionaireSTRAdd
    Inherits Parent

#Region "Sesion"


    Public Property ObjEditMsQuestionaireSTRMultipleChoice() As MsQuestionaireSTRMultipleChoice
        Get

            Return CType(Session("QuestionaireSTRAdd.ObjEditMsQuestionaireSTRMultipleChoice"), MsQuestionaireSTRMultipleChoice)
        End Get
        Set(value As MsQuestionaireSTRMultipleChoice)
            Session("QuestionaireSTRAdd.ObjEditMsQuestionaireSTRMultipleChoice") = value
        End Set
    End Property


    Public ReadOnly Property ObjMsQuestionaireSTR() As TList(Of MsQuestionaireSTR)
        Get
            If Session("QuestionaireSTRAdd.ObjMsQuestionaireSTR") Is Nothing Then
                Session("QuestionaireSTRAdd.ObjMsQuestionaireSTR") = New TList(Of MsQuestionaireSTR)
            End If
            Return CType(Session("QuestionaireSTRAdd.ObjMsQuestionaireSTR"), TList(Of MsQuestionaireSTR))
        End Get
    End Property

    Public ReadOnly Property ObjMsQuestionaireSTRMultipleChoice() As TList(Of MsQuestionaireSTRMultipleChoice)
        Get
            If Session("QuestionaireSTRAdd.ObjMsQuestionaireSTRMultipleChoice") Is Nothing Then
                Session("QuestionaireSTRAdd.ObjMsQuestionaireSTRMultipleChoice") = New TList(Of MsQuestionaireSTRMultipleChoice)
            End If
            Return CType(Session("QuestionaireSTRAdd.ObjMsQuestionaireSTRMultipleChoice"), TList(Of MsQuestionaireSTRMultipleChoice))
        End Get
    End Property

    Public ReadOnly Property ObjQuestionType() As TList(Of QuestionType)
        Get
            If Session("QuestionaireSTRAdd.ObjQuestionType") Is Nothing Then
                Session("QuestionaireSTRAdd.ObjQuestionType") = New TList(Of QuestionType)
            End If
            Return CType(Session("QuestionaireSTRAdd.ObjQuestionType"), TList(Of QuestionType))
        End Get
    End Property
    Private Property SetnGetCurrentPage() As Integer
        Get
            Return CInt(IIf(Session("SetnGetCurrentPage") Is Nothing, "", Session("SetnGetCurrentPage")))
        End Get
        Set(ByVal value As Integer)
            Session("SetnGetCurrentPage") = value
        End Set
    End Property
#End Region

#Region "ClearSession"
    Sub ClearSession()
        ObjEditMsQuestionaireSTRMultipleChoice = Nothing
        Session("QuestionaireSTRAdd.ObjMsQuestionaireSTR") = Nothing
        Session("QuestionaireSTRAdd.ObjMsQuestionaireSTRMultipleChoice") = Nothing
        Session("QuestionaireSTRAdd.ObjQuestionType") = Nothing
        Session("SetnGetCurrentPage") = Nothing
    End Sub
#End Region

#Region "load mode"
    Private Sub LoadCboAlertSTR()
        Session("QuestionaireSTRAdd.Mode") = Nothing
        CboAlertSTR.Items.Clear()
        Using ObjQuestionaireSTRBLL As New AMLBLL.QuestionaireSTRBLL
            CboAlertSTR.AppendDataBoundItems = True
            CboAlertSTR.DataSource = ObjQuestionaireSTRBLL.GetLoadCboAlertSTR
            CboAlertSTR.DataTextField = RulesAdvancedColumn.RulesAdvancedName.ToString
            CboAlertSTR.DataValueField = RulesAdvancedColumn.RulesAdvancedName.ToString
            CboAlertSTR.DataBind()
            CboAlertSTR.Items.Insert(0, New ListItem("Please Select Description Alert", ""))
            CboAlertSTR.Items.Insert(1, New ListItem("Financial Profile Abnormal", "Financial Profile Abnormal"))
            CboAlertSTR.Items.Insert(2, New ListItem("Identity Profile", "Identity Profile"))
            CboAlertSTR.Items.Insert(3, New ListItem("Artificial STR", "Artificial STR"))

            Using ObjRulesBasic As TList(Of RulesBasic) = DataRepository.RulesBasicProvider.GetPaged("IsEnabled=1", "", 0, Integer.MaxValue, 0)
                Dim n As Integer = 4
                For Each ObjRulesBasicBindTable As RulesBasic In ObjRulesBasic
                    CboAlertSTR.Items.Insert(n, New ListItem(ObjRulesBasicBindTable.RulesBasicDescription, ObjRulesBasicBindTable.RulesBasicDescription))
                    n = n + 1
                Next
            End Using
        End Using
    End Sub

    Private Sub LoadQuestionType()
        Session("QuestionaireSTRAdd.Mode") = Nothing
        CboQuestionType.Items.Clear()
        Using ObjQuestionaireSTRBLL As New AMLBLL.QuestionaireSTRBLL
            CboQuestionType.AppendDataBoundItems = True
            CboQuestionType.DataSource = ObjQuestionaireSTRBLL.GetLoadCboQuestionType
            CboQuestionType.DataTextField = QuestionTypeColumn.QuestionType.ToString
            CboQuestionType.DataValueField = QuestionTypeColumn.PK_QuestionType_ID.ToString
            CboQuestionType.DataBind()
            CboQuestionType.Items.Insert(0, New ListItem("Please Select Question Type", ""))
        End Using
    End Sub

    Protected Sub CboQuestionType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CboQuestionType.SelectedIndexChanged
        If CboQuestionType.Text = "" Then
            Me.LblSuccess.Text = "Please Select Question Type ."
            Me.LblSuccess.Visible = True
        ElseIf CboQuestionType.Text = CStr(1) Then
            Session("QuestionaireSTRAdd.ObjMsQuestionaireSTRMultipleChoice") = Nothing
            gridView.DataSource = ObjMsQuestionaireSTRMultipleChoice
            gridView.DataBind()
            abc.Visible = False
            bcd.Visible = False
            cde.Visible = False
        Else
            abc.Visible = True
            bcd.Visible = True
            cde.Visible = True
        End If
    End Sub
#End Region

#Region "IMGADD"
    Protected Sub ImageAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAdd.Click

        Try
            If TxtMultipleChoice.Text = "" Then
                Me.LblSuccess.Text = "Please Insert Multiple Choice  ."
                Me.LblSuccess.Visible = True
            Else
                If Me.ObjEditMsQuestionaireSTRMultipleChoice Is Nothing Then

                    Using ObjNewMsQuestionaireSTRMultipleChoice As New MsQuestionaireSTRMultipleChoice
                        Dim generator As New Random
                        Dim randomValue As Integer
                        randomValue = generator.Next(Integer.MinValue, -1)

                        ObjNewMsQuestionaireSTRMultipleChoice.PK_MsQuestionaireSTRMultipleChoice_ID = randomValue
                        ObjNewMsQuestionaireSTRMultipleChoice.MultipleChoice = TxtMultipleChoice.Text
                        ObjMsQuestionaireSTRMultipleChoice.Add(ObjNewMsQuestionaireSTRMultipleChoice)
                        gridView.DataSource = ObjMsQuestionaireSTRMultipleChoice
                        gridView.DataBind()
                        TxtMultipleChoice.Text = ""
                    End Using
                Else
                    ObjEditMsQuestionaireSTRMultipleChoice.MultipleChoice = TxtMultipleChoice.Text
                    gridView.DataSource = ObjMsQuestionaireSTRMultipleChoice
                    gridView.DataBind()
                    TxtMultipleChoice.Text = ""
                End If

            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
#End Region

#Region "IMGSAVE"
    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim oTrans As SqlTransaction = Nothing
        Try
            LblSuccess.Visible = False
            Dim ObjCboAlertSTRList As String = CboAlertSTR.Text
            Dim ObjCboQuestionTypeList As String = CboQuestionType.Text
            Dim ObjQuestionNumber As String = TxtQuestionNo.Text
            Dim ObjQuestion As String = TxtQuestion.Text



            If ObjCboAlertSTRList = "" Then
                Me.LblSuccess.Text = "Please Select Description Alert  ."
                Me.LblSuccess.Visible = True
            Else
                If ObjCboQuestionTypeList = "" Then
                    Me.LblSuccess.Text = "Please Select Question Type ."
                    Me.LblSuccess.Visible = True
                Else
                    If ObjQuestionNumber = "" Then
                        Me.LblSuccess.Text = "Please Insert Question No ."
                        Me.LblSuccess.Visible = True
                    Else

                        If ObjQuestion = "" Then
                            Me.LblSuccess.Text = "Please Insert Question ."
                            Me.LblSuccess.Visible = True
                        Else
                            Using ObjQuestionaireSTRBLL As New QuestionaireSTRBLL
                                If ObjQuestionaireSTRBLL.IsDataValidAddApproval(ObjCboAlertSTRList, ObjQuestionNumber) Then
                                End If
                            End Using
                            'SuperUser
                            If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                                Using ObjMsQuestionaireSTR As New MsQuestionaireSTR
                                    With ObjMsQuestionaireSTR
                                        .DescriptionAlertSTR = ObjCboAlertSTRList
                                        .FK_QuestionType_ID = CInt(ObjCboQuestionTypeList)
                                        .QuestionNo = CInt(ObjQuestionNumber)
                                        .Question = ObjQuestion
                                        .Activation = True
                                        .CreatedBy = Sahassa.AML.Commonly.SessionUserId
                                        .CreatedDate = Date.Now
                                        .LastUpdateBy = Sahassa.AML.Commonly.SessionUserId
                                        .LastUpdateDate = Date.Now
                                    End With
                                    DataRepository.MsQuestionaireSTRProvider.Save(ObjMsQuestionaireSTR)
                                    AMLBLL.QuestionaireSTRBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "QuestionaireSTR", "DescriptionAlertSTR", "Add", "", ObjCboAlertSTRList, "Acc")
                                    AMLBLL.QuestionaireSTRBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "QuestionaireSTR", "QuestionType", "Add", "", ObjCboQuestionTypeList, "Acc")
                                    AMLBLL.QuestionaireSTRBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "QuestionaireSTR", "QuestionNo", "Add", "", ObjQuestionNumber, "Acc")
                                    AMLBLL.QuestionaireSTRBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "QuestionaireSTR", "Question", "Add", "", ObjQuestion, "Acc")
                                    For Each ObjSaveMsQuestionaireSTRMultipleChoice As MsQuestionaireSTRMultipleChoice In ObjMsQuestionaireSTRMultipleChoice
                                        Using ObjNewMsQuestionaireSTRMultipleChoice As New MsQuestionaireSTRMultipleChoice
                                            With ObjNewMsQuestionaireSTRMultipleChoice
                                                .FK_MsQuestionaireSTR_ID = ObjMsQuestionaireSTR.PK_MsQuestionaireSTR_ID
                                                .MultipleChoice = ObjSaveMsQuestionaireSTRMultipleChoice.MultipleChoice
                                            End With
                                            DataRepository.MsQuestionaireSTRMultipleChoiceProvider.Save(ObjNewMsQuestionaireSTRMultipleChoice)
                                            AMLBLL.QuestionaireSTRBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "QuestionaireSTR", "MultipleChoice", "Add", "", ObjSaveMsQuestionaireSTRMultipleChoice.MultipleChoice, "Acc")
                                        End Using
                                    Next

                                    Me.LblSuccess.Text = "Description Alert STR " & ObjCboAlertSTRList & " AND Question number " & ObjQuestionNumber & " has join to Questionaire STR"
                                    Me.LblSuccess.Visible = True

                                End Using

                            Else 'bukan SuperUser
                                Using ObjMsQuestionaireSTR_approval As New MsQuestionaireSTR_Approval
                                    With ObjMsQuestionaireSTR_approval
                                        .FK_MsUserID = Sahassa.AML.Commonly.SessionPkUserId
                                        .DescriptionAlertSTR = ObjCboAlertSTRList
                                        .QuestionNo = CInt(ObjQuestionNumber)
                                        .FK_ModeID = CInt(Sahassa.AML.Commonly.TypeMode.Add)
                                        .CreatedDate = Date.Now
                                    End With
                                    DataRepository.MsQuestionaireSTR_ApprovalProvider.Save(ObjMsQuestionaireSTR_approval)
                                    Using ObjMsQuestionaireSTR_approvalDetail As New MsQuestionaireSTR_ApprovalDetail
                                        With ObjMsQuestionaireSTR_approvalDetail
                                            .FK_MsQuestionaireSTR_ApprovalID = ObjMsQuestionaireSTR_approval.PK_MsQuestionaireSTR_ApprovalID
                                            .DescriptionAlertSTR = ObjCboAlertSTRList
                                            .FK_QuestionType_ID = CInt(ObjCboQuestionTypeList)
                                            .QuestionNo = CInt(ObjQuestionNumber)
                                            .Question = ObjQuestion
                                            .Activation = True
                                            .CreatedBy = Sahassa.AML.Commonly.SessionUserId
                                            .CreatedDate = Date.Now
                                            .LastUpdateBy = Sahassa.AML.Commonly.SessionUserId
                                            .LastUpdateDate = Date.Now
                                        End With
                                        DataRepository.MsQuestionaireSTR_ApprovalDetailProvider.Save(ObjMsQuestionaireSTR_approvalDetail)
                                        AMLBLL.QuestionaireSTRBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "QuestionaireSTR", "DescriptionAlertSTR", "Add", "", ObjCboAlertSTRList, "PendingApproval")
                                        AMLBLL.QuestionaireSTRBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "QuestionaireSTR", "QuestionType", "Add", "", ObjCboQuestionTypeList, "PendingApproval")
                                        AMLBLL.QuestionaireSTRBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "QuestionaireSTR", "QuestionNo", "Add", "", ObjQuestionNumber, "PendingApproval")
                                        AMLBLL.QuestionaireSTRBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "QuestionaireSTR", "Question", "Add", "", ObjQuestion, "PendingApproval")
                                        For Each ObjSaveMsQuestionaireSTRMultipleChoice As MsQuestionaireSTRMultipleChoice In ObjMsQuestionaireSTRMultipleChoice
                                            Using ObjMsQuestionaireSTRMultipleChoice_approvaldetail As New MsQuestionaireSTRMultipleChoice_ApprovalDetail
                                                With ObjMsQuestionaireSTRMultipleChoice_approvaldetail
                                                    .FK_MsQuestionaireSTR_ApprovalID = CInt(ObjMsQuestionaireSTR_approval.PK_MsQuestionaireSTR_ApprovalID)
                                                    .MultipleChoice = ObjSaveMsQuestionaireSTRMultipleChoice.MultipleChoice
                                                End With
                                                DataRepository.MsQuestionaireSTRMultipleChoice_ApprovalDetailProvider.Save(ObjMsQuestionaireSTRMultipleChoice_approvaldetail)
                                                AMLBLL.QuestionaireSTRBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "QuestionaireSTR", "MultipleChoice", "Add", "", ObjSaveMsQuestionaireSTRMultipleChoice.MultipleChoice, "PendingApproval")
                                            End Using
                                        Next
                                        Me.LblSuccess.Text = "Description Alert STR " & ObjCboAlertSTRList & " AND Question number " & ObjQuestionNumber & " has add to Pending Approval"
                                        Me.LblSuccess.Visible = True
                                    End Using
                                End Using
                            End If
                            clearControl()
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            If Not oTrans Is Nothing Then oTrans.Rollback()
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not oTrans Is Nothing Then
                oTrans.Dispose()
                oTrans = Nothing
            End If
        End Try
    End Sub
#End Region

    Sub clearControl()
        CboQuestionType.SelectedIndex = 0
        CboAlertSTR.SelectedIndex = 0
        TxtMultipleChoice.Text = ""
        TxtQuestion.Text = ""
        TxtQuestionNo.Text = ""
        gridView.DataSource = Nothing
        gridView.DataBind()
    End Sub

#Region "IMGCANCEL"
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "QuestionaireSTRView.aspx"
            Me.Response.Redirect("QuestionaireSTRView.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'Using Transcope As New Transactions.TransactionScope
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                '    Transcope.Complete()
            End Using

            If Not Page.IsPostBack Then
                ClearSession()
                LoadCboAlertSTR()
                LoadQuestionType()
                abc.Visible = False
                bcd.Visible = False
                cde.Visible = False
            End If
            'End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

#Region "Grid"
    Protected Sub gridview_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles gridView.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then

                Dim attachment As String = e.Item.Cells(2).Text
                Dim editBtn As LinkButton = CType(e.Item.Cells(3).FindControl("lnkGridEdit"), LinkButton)
                Dim delBtn As LinkButton = CType(e.Item.Cells(4).FindControl("lnkGridDelete"), LinkButton)

                editBtn.OnClientClick = Edit_ATTACHMENT_CONFIRMATION(attachment)
                delBtn.OnClientClick = DELETE_ATTACHMENT_CONFIRMATION(attachment)
                e.Item.Cells(0).Text = CStr((e.Item.ItemIndex + 1))
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub gridView_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles gridView.ItemCommand
        Dim PKMsQuestionaireSTRMultipleChoiceID As Integer
        Dim BDeleteNeedApproval As Boolean = False
        If e.Item.ItemType <> ListItemType.Header And e.Item.ItemType <> ListItemType.Footer Then
            Select Case e.CommandName
                Case "DELETE"
                    PKMsQuestionaireSTRMultipleChoiceID = CType(e.Item.Cells(0).Text, Integer)
                    ObjMsQuestionaireSTRMultipleChoice.RemoveAt(PKMsQuestionaireSTRMultipleChoiceID - 1)

                    gridView.DataSource = ObjMsQuestionaireSTRMultipleChoice
                    gridView.DataBind()
                Case "EDIT"
                    ObjEditMsQuestionaireSTRMultipleChoice = Nothing


                    PKMsQuestionaireSTRMultipleChoiceID = CType(e.Item.Cells(1).Text, Integer)
                    Using objedit As MsQuestionaireSTRMultipleChoice = ObjMsQuestionaireSTRMultipleChoice.Find(MsQuestionaireSTRMultipleChoiceColumn.PK_MsQuestionaireSTRMultipleChoice_ID, CInt(PKMsQuestionaireSTRMultipleChoiceID))
                        If Not objedit Is Nothing Then
                            ObjEditMsQuestionaireSTRMultipleChoice = objedit
                            TxtMultipleChoice.Text = objedit.MultipleChoice
                        End If
                    End Using



               

            End Select
        End If
    End Sub

    Protected Sub gridView_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles gridView.PageIndexChanged
        gridView.DataSource = ObjMsQuestionaireSTRMultipleChoice
        SetnGetCurrentPage = e.NewPageIndex
        gridView.CurrentPageIndex = SetnGetCurrentPage
        gridView.DataBind()
    End Sub

    Public Shared Function Edit_ATTACHMENT_CONFIRMATION(ByVal str As String) As String
        Dim result As String = String.Empty

        If Not Sahassa.AML.Commonly.SessionLanguage Is Nothing Then
            Select Case Sahassa.AML.Commonly.SessionLanguage.ToLower()
                Case "en-us"
                    result = "javascript:return window.confirm('Are you sure want to Edit " & str & "?');"
                Case "id-id"
                    result = "javascript:return window.confirm('Anda yakin ingin Edit " & str & "?');"
                Case Else
                    result = "javascript:return window.confirm('Are you sure want to Edit " & str & "?');"
            End Select
        Else
            result = "javascript:return window.confirm('Are you sure want to Edit " & str & "?');"
        End If

        Return result
    End Function

    Public Shared Function DELETE_ATTACHMENT_CONFIRMATION(ByVal str As String) As String
        Dim result As String = String.Empty

        If Not Sahassa.AML.Commonly.SessionLanguage Is Nothing Then
            Select Case Sahassa.AML.Commonly.SessionLanguage.ToLower()
                Case "en-us"
                    result = "javascript:return window.confirm('Are you sure want to delete " & str & "?');"
                Case "id-id"
                    result = "javascript:return window.confirm('Anda yakin ingin menghapus " & str & "?');"
                Case Else
                    result = "javascript:return window.confirm('Are you sure want to delete " & str & "?');"
            End Select
        Else
            result = "javascript:return window.confirm('Are you sure want to delete " & str & "?');"
        End If

        Return result
    End Function
#End Region

End Class
