
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Partial Class DataUpdatingEmailTemplateApprovalDetail
    Inherits Parent

#Region " Property "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get confirm type
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property ParamType() As Sahassa.AML.Commonly.TypeConfirm
        Get
            Return Me.Request.Params("TypeConfirm")
        End Get
    End Property

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get param DataUpdatingEmailTemplateApprovalID 
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    15/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property DataUpdatingEmailTemplateApprovalID() As Int64
        Get
            Return Me.Request.Params("ApprovalID")
        End Get
    End Property

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get param Parameters_PendingApprovalID
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    15/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property ParametersPendingApprovalID() As Int64
        Get
            Return Me.Request.Params("PendingApprovalID")
        End Get
    End Property

    ''' <summary>
    ''' get user id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetUserID() As String
        Get
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.Parameters_PendingApprovalTableAdapter
                Return AccessPending.SelectParameters_PendingApprovalUserID(Me.ParametersPendingApprovalID)
            End Using
        End Get
    End Property
#End Region

#Region " Load "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' hide all
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    21/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub HideAll()
        Dim StrId As String = ""
        Select Case Me.ParamType
            Case Sahassa.AML.Commonly.TypeConfirm.DataUpdatingEmailTemplateAdd
                StrId = "UserAdd"
            Case Sahassa.AML.Commonly.TypeConfirm.DataUpdatingEmailTemplateEdit
                StrId = "UserEdi"
            Case Else
                Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
        End Select
        For Each ObjRow As HtmlControls.HtmlTableRow In Me.TableDetail.Rows
            ObjRow.Visible = ObjRow.ID = "Button11" Or ObjRow.ID.Substring(0, 7) = StrId
        Next
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load DataUpdatingEmailTemplate add
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    21/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub LoadDataUpdatingEmailTemplateAdd()
        Me.LabelTitle.Text = "Activity: Add Data Updating - Email Template"
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.DataUpdatingEmailTemplate_ApprovalTableAdapter
            Using ObjTable As Data.DataTable = AccessPending.GetDataUpdatingEmailTemplateApprovalData(Me.DataUpdatingEmailTemplateApprovalID)
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.DataUpdatingEmailTemplate_ApprovalRow = ObjTable.Rows(0)

                    Using objLevelTypeName As New AMLDAL.AMLDataSetTableAdapters.LevelTypeTableAdapter
                        Me.LabelToAdd.Text = objLevelTypeName.GetLevelTypeNameByLevelTypeID(rowData.ToLevelTypeID)
                        Me.LabelCCAdd.Text = objLevelTypeName.GetLevelTypeNameByLevelTypeID(rowData.CCLevelTypeID)
                        Me.LabelBCCAdd.Text = objLevelTypeName.GetLevelTypeNameByLevelTypeID(rowData.BCCLevelTypeID)
                    End Using

                    Me.LabelSubjectAdd.Text = rowData.Subject
                    Me.TextBodyAdd.Text = rowData.Body

                    If rowData.IncludeAttachment = True Then
                        Me.LabelIncludeAttachmentAdd.Text = "Yes"
                    Else
                        Me.LabelIncludeAttachmentAdd.Text = "No"
                    End If
                End If
            End Using
        End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load DataUpdatingEmailTemplate edit
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    21/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub LoadDataUpdatingEmailTemplateEdit()
        Me.LabelTitle.Text = "Activity: Edit Data Updating - Email Template"
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.DataUpdatingEmailTemplate_ApprovalTableAdapter
            Using ObjTable As Data.DataTable = AccessPending.GetDataUpdatingEmailTemplateApprovalData(Me.DataUpdatingEmailTemplateApprovalID)
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.DataUpdatingEmailTemplate_ApprovalRow = ObjTable.Rows(0)
                    'Tarik informasi nilai-nilai baru
                    Using objNewLevelTypeName As New AMLDAL.AMLDataSetTableAdapters.LevelTypeTableAdapter
                        Me.LabelNewTo.Text = objNewLevelTypeName.GetLevelTypeNameByLevelTypeID(rowData.ToLevelTypeID)
                        Me.LabelNewCC.Text = objNewLevelTypeName.GetLevelTypeNameByLevelTypeID(rowData.CCLevelTypeID)
                        Me.LabelNewBCC.Text = objNewLevelTypeName.GetLevelTypeNameByLevelTypeID(rowData.BCCLevelTypeID)
                    End Using

                    Me.LabelNewSubject.Text = rowData.Subject
                    Me.TextNewBody.Text = rowData.Body

                    If rowData.IncludeAttachment = True Then
                        Me.LabelNewIncludeAttachment.Text = "Yes"
                    Else
                        Me.LabelNewIncludeAttachment.Text = "No"
                    End If

                    'Tarik informasi nilai-nilai lama
                    Using objOldLevelTypeName As New AMLDAL.AMLDataSetTableAdapters.LevelTypeTableAdapter
                        Me.LabelOldTo.Text = objOldLevelTypeName.GetLevelTypeNameByLevelTypeID(rowData.ToLevelTypeID_Old)
                        Me.LabelOldCC.Text = objOldLevelTypeName.GetLevelTypeNameByLevelTypeID(rowData.CCLevelTypeID_Old)
                        Me.LabelOldBCC.Text = objOldLevelTypeName.GetLevelTypeNameByLevelTypeID(rowData.BCCLevelTypeID_Old)
                    End Using

                    Me.LabelOldSubject.Text = rowData.Subject_Old
                    Me.TextOldBody.Text = rowData.Body_Old

                    If rowData.IncludeAttachment_Old = True Then
                        Me.LabelOldIncludeAttachment.Text = "Yes"
                    Else
                        Me.LabelOldIncludeAttachment.Text = "No"
                    End If
                End If
            End Using
        End Using
    End Sub
#End Region

#Region "Delete All Approval"
    ''' <summary>
    ''' Delete All Approval
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function DeleteAllApproval(ByRef oSQLTrans As SqlTransaction) As Boolean
        Try
            'Using TransScope As New Transactions.TransactionScope
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.DataUpdatingEmailTemplate_ApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, oSQLTrans)

                Using AccessParametersApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_ApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessParametersApproval, oSQLTrans)

                    Using AccessParametersPendingApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_PendingApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessParametersPendingApproval, oSQLTrans)

                        'hapus item tersebut dalam tabel DataUpdatingEmailTemplate_Approval
                        AccessPending.DeleteDataUpdatingEmailTemplateApproval(Me.DataUpdatingEmailTemplateApprovalID)

                        'hapus item tersebut dalam tabel Parameters_Approval
                        AccessParametersApproval.DeleteParametersApproval(Me.ParametersPendingApprovalID)

                        'hapus item tersebut dalam tabel Parameters_PendingApproval
                        AccessParametersPendingApproval.DeleteParametersPendingApproval(Me.ParametersPendingApprovalID)
                    End Using
                End Using
            End Using
            'End Using
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

#End Region

#Region "Insert dan Cek AuditTrail"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function InsertAuditTrail(ByVal mode As String, ByVal Action As String, ByRef oSQLTrans As SqlTransaction) As Boolean
        Try
            'Using TransScope As New Transactions.TransactionScope
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.DataUpdatingEmailTemplate_ApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, oSQLTrans)
                Using AccessParametersApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_ApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessParametersApproval, oSQLTrans)
                    Using AccessParametersPendingApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_PendingApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessParametersPendingApproval, oSQLTrans)
                        Dim ObjTable As Data.DataTable = AccessPending.GetDataUpdatingEmailTemplateApprovalData(Me.DataUpdatingEmailTemplateApprovalID)
                        If ObjTable.Rows.Count > 0 Then
                            Dim rowData As AMLDAL.AMLDataSet.DataUpdatingEmailTemplate_ApprovalRow = ObjTable.Rows(0)
                            'catat aktifitas dalam tabel Audit Trail
                            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(8)
                            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                                If mode.ToLower = "add" Then
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "ToLevelTypeID", mode, "", rowData.ToLevelTypeID, Action)
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "CCLevelTypeID", mode, "", rowData.CCLevelTypeID, Action)
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "BCCLevelTypeID", mode, "", rowData.BCCLevelTypeID, Action)
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "Subject", mode, "", rowData.Subject, Action)
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "Body", mode, "", rowData.Body, Action)
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "IncludeAttachment", mode, "", rowData.IncludeAttachment, Action)
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "CreatedDate", mode, "", rowData.CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), Action)
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "LastUpdateDate", mode, "", rowData.LastUpdateDate.ToString("dd-MMMM-yyyy HH:mm"), Action)
                                Else
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "ToLevelTypeID", mode, rowData.ToLevelTypeID_Old, rowData.ToLevelTypeID, Action)
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "CCLevelTypeID", mode, rowData.CCLevelTypeID_Old, rowData.CCLevelTypeID, Action)
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "BCCLevelTypeID", mode, rowData.BCCLevelTypeID_Old, rowData.BCCLevelTypeID, Action)
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "Subject", mode, rowData.Subject_Old, rowData.Subject, Action)
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "Body", mode, rowData.Body_Old, rowData.Body, Action)
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "IncludeAttachment", mode, rowData.IncludeAttachment_Old, rowData.IncludeAttachment, Action)
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "CreatedDate", mode, rowData.CreatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), rowData.CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), Action)
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "LastUpdateDate", mode, rowData.LastUpdateDate_Old.ToString("dd-MMMM-yyyy HH:mm"), rowData.LastUpdateDate.ToString("dd-MMMM-yyyy HH:mm"), Action)
                                End If
                            End Using
                        End If
                    End Using
                End Using
            End Using
            'End Using
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Function InsertAuditTrail_Begin(ByVal mode As String, ByVal Action As String, ByRef oSQLTrans As SqlTransaction) As Boolean
        Try
            'Using TransScope As New Transactions.TransactionScope
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.DataUpdatingEmailTemplate_ApprovalTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessPending, IsolationLevel.ReadUncommitted)
                Using AccessParametersApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_ApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessParametersApproval, oSQLTrans)
                    Using AccessParametersPendingApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_PendingApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessParametersPendingApproval, oSQLTrans)
                        Dim ObjTable As Data.DataTable = AccessPending.GetDataUpdatingEmailTemplateApprovalData(Me.DataUpdatingEmailTemplateApprovalID)
                        If ObjTable.Rows.Count > 0 Then
                            Dim rowData As AMLDAL.AMLDataSet.DataUpdatingEmailTemplate_ApprovalRow = ObjTable.Rows(0)
                            'catat aktifitas dalam tabel Audit Trail
                            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(8)
                            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                                If mode.ToLower = "add" Then
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "ToLevelTypeID", mode, "", rowData.ToLevelTypeID, Action)
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "CCLevelTypeID", mode, "", rowData.CCLevelTypeID, Action)
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "BCCLevelTypeID", mode, "", rowData.BCCLevelTypeID, Action)
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "Subject", mode, "", rowData.Subject, Action)
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "Body", mode, "", rowData.Body, Action)
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "IncludeAttachment", mode, "", rowData.IncludeAttachment, Action)
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "CreatedDate", mode, "", rowData.CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), Action)
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "LastUpdateDate", mode, "", rowData.LastUpdateDate.ToString("dd-MMMM-yyyy HH:mm"), Action)
                                Else
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "ToLevelTypeID", mode, rowData.ToLevelTypeID_Old, rowData.ToLevelTypeID, Action)
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "CCLevelTypeID", mode, rowData.CCLevelTypeID_Old, rowData.CCLevelTypeID, Action)
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "BCCLevelTypeID", mode, rowData.BCCLevelTypeID_Old, rowData.BCCLevelTypeID, Action)
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "Subject", mode, rowData.Subject_Old, rowData.Subject, Action)
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "Body", mode, rowData.Body_Old, rowData.Body, Action)
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "IncludeAttachment", mode, rowData.IncludeAttachment_Old, rowData.IncludeAttachment, Action)
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "CreatedDate", mode, rowData.CreatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), rowData.CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), Action)
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingEmailTemplate", "LastUpdateDate", mode, rowData.LastUpdateDate_Old.ToString("dd-MMMM-yyyy HH:mm"), rowData.LastUpdateDate.ToString("dd-MMMM-yyyy HH:mm"), Action)
                                End If
                            End Using
                        End If
                    End Using
                End Using
            End Using
            'End Using
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
#End Region

#Region " Accept "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' DataUpdatingEmailTemplate add accept
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    21/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptDataUpdatingEmailTemplateAdd()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            Using AccessDataUpdatingEmailTemplate As New AMLDAL.AMLDataSetTableAdapters.DataUpdatingEmailTemplateTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessDataUpdatingEmailTemplate, IsolationLevel.ReadUncommitted)
                Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.DataUpdatingEmailTemplate_ApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, oSQLTrans)
                    Dim ObjTable As Data.DataTable = AccessPending.GetDataUpdatingEmailTemplateApprovalData(Me.DataUpdatingEmailTemplateApprovalID)
                    If ObjTable.Rows.Count > 0 Then
                        Dim rowData As AMLDAL.AMLDataSet.DataUpdatingEmailTemplate_ApprovalRow = ObjTable.Rows(0)
                        'tambahkan item tersebut dalam tabel DataUpdatingEmailTemplate
                        AccessDataUpdatingEmailTemplate.Insert(rowData.ToLevelTypeID, rowData.CCLevelTypeID, rowData.BCCLevelTypeID, rowData.Subject, rowData.Body, rowData.IncludeAttachment, rowData.CreatedDate, rowData.LastUpdateDate)

                        If Me.InsertAuditTrail("Add", "Accept", oSQLTrans) Then
                            Me.DeleteAllApproval(oSQLTrans)
                            Me.Response.Redirect("DataUpdatingEmailTemplateManagementApproval.aspx", False)
                        Else
                            Throw New Exception("Failed to insert Audit Trail.")
                        End If
                    End If
                End Using
                oSQLTrans.Commit()
            End Using
        Catch
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' DataUpdatingEmailTemplate edit accept
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    21/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptDataUpdatingEmailTemplateEdit()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            Using AccessDataUpdatingEmailTemplate As New AMLDAL.AMLDataSetTableAdapters.DataUpdatingEmailTemplateTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessDataUpdatingEmailTemplate, IsolationLevel.ReadUncommitted)
                Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.DataUpdatingEmailTemplate_ApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, oSQLTrans)
                    Dim ObjTable As Data.DataTable = AccessPending.GetDataUpdatingEmailTemplateApprovalData(Me.DataUpdatingEmailTemplateApprovalID)
                    If ObjTable.Rows.Count > 0 Then
                        Dim rowData As AMLDAL.AMLDataSet.DataUpdatingEmailTemplate_ApprovalRow = ObjTable.Rows(0)
                        'update item tersebut dalam tabel DataUpdatingEmailTemplate
                        AccessDataUpdatingEmailTemplate.UpdateDataUpdatingEmailTemplate(rowData.ToLevelTypeID, rowData.CCLevelTypeID, rowData.BCCLevelTypeID, rowData.Subject, rowData.Body, rowData.IncludeAttachment, rowData.LastUpdateDate)
                        If Me.InsertAuditTrail("Edit", "Accept", oSQLTrans) Then
                            Me.DeleteAllApproval(oSQLTrans)
                            Me.Response.Redirect("DataUpdatingEmailTemplateManagementApproval.aspx", False)
                        Else
                            Throw New Exception("Failed to insert Audit Trail.")
                        End If
                    End If
                End Using
                oSQLTrans.Commit()
            End Using

        Catch
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub

#End Region

#Region " Reject "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' reject DataUpdatingEmailTemplate add
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    21/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectDataUpdatingEmailTemplateAdd()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            If Me.InsertAuditTrail_Begin("Add", "Reject", oSQLTrans) Then
                Me.DeleteAllApproval(oSQLTrans)
                Me.Response.Redirect("DataUpdatingEmailTemplateManagementApproval.aspx", False)
            Else
                Throw New Exception("Failed to insert Audit Trail.")
            End If
        Catch
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' reject DataUpdatingEmailTemplate edit
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    21/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectDataUpdatingEmailTemplateEdit()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            If Me.InsertAuditTrail_Begin("Edit", "Reject", oSQLTrans) Then
                Me.DeleteAllApproval(oSQLTrans)
                Me.Response.Redirect("DataUpdatingEmailTemplateManagementApproval.aspx", False)
            Else
                Throw New Exception("Failed to insert Audit Trail.")
            End If
        Catch
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub

#End Region

    ''' <summary>
    ''' page load event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.ImageAccept.Attributes.Add("onclick", "javascript: if(confirm('Do you want to accept this activity ?')== false) return false;")
                Me.ImageReject.Attributes.Add("onclick", "javascript: if(confirm('Do you want to reject this activity ?')== false) return false;")
                Me.HideAll()
                Select Case Me.ParamType
                    Case Sahassa.AML.Commonly.TypeConfirm.DataUpdatingEmailTemplateAdd
                        Me.LoadDataUpdatingEmailTemplateAdd()
                    Case Sahassa.AML.Commonly.TypeConfirm.DataUpdatingEmailTemplateEdit
                        Me.LoadDataUpdatingEmailTemplateEdit()
                    Case Else
                        Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
                End Select

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                    'End Using
                End Using
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' accept button handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click
        Try
            Select Case Me.ParamType
                Case Sahassa.AML.Commonly.TypeConfirm.DataUpdatingEmailTemplateAdd
                    Me.AcceptDataUpdatingEmailTemplateAdd()
                Case Sahassa.AML.Commonly.TypeConfirm.DataUpdatingEmailTemplateEdit
                    Me.AcceptDataUpdatingEmailTemplateEdit()
                Case Else
                    Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
            End Select

            Sahassa.AML.Commonly.SessionIntendedPage = "DataUpdatingEmailTemplateManagementApproval.aspx"

            Me.Response.Redirect("DataUpdatingEmailTemplateManagementApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' reject button handlert
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click
        Try
            Select Case Me.ParamType
                Case Sahassa.AML.Commonly.TypeConfirm.DataUpdatingEmailTemplateAdd
                    Me.RejectDataUpdatingEmailTemplateAdd()
                Case Sahassa.AML.Commonly.TypeConfirm.DataUpdatingEmailTemplateEdit
                    Me.RejectDataUpdatingEmailTemplateEdit()
                Case Else
                    Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
            End Select

            Sahassa.AML.Commonly.SessionIntendedPage = "DataUpdatingEmailTemplateManagementApproval.aspx"

            Me.Response.Redirect("DataUpdatingEmailTemplateManagementApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' back button handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "DataUpdatingEmailTemplateManagementApproval.aspx"

        Me.Response.Redirect("DataUpdatingEmailTemplateManagementApproval.aspx", False)
    End Sub
End Class