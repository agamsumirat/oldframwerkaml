Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Partial Class VerificationListCategoryAdd
    Inherits Parent

    ''' <summary>
    ''' cancel handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "VerificationListCategoryView.aspx"

        Me.Response.Redirect("VerificationListCategoryView.aspx", False)
    End Sub

    Private Sub InsertAuditTrail()
        Try
            Dim CategoryDescription As String
            If Me.TextCategoryDescription.Text.Length <= 255 Then
                CategoryDescription = Me.TextCategoryDescription.Text
            Else
                CategoryDescription = Me.TextCategoryDescription.Text.Substring(0, 255)
            End If

            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(3)
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "VerificationListCategory", "CategoryName", "Add", "", Me.TextCategoryName.Text, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "VerificationListCategory", "CategoryDescription", "Add", "", CategoryDescription, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "VerificationListCategory", "CategoryCreatedDate", "Add", "", Now.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
            End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub InsertVerificationListCategory()
        Try
            Dim CategoryDescription As String
            If Me.TextCategoryDescription.Text.Length <= 255 Then
                CategoryDescription = Me.TextCategoryDescription.Text
            Else
                CategoryDescription = Me.TextCategoryDescription.Text.Substring(0, 255)
            End If

            Using AccessInsertVerificationListCategory As New AMLDAL.AMLDataSetTableAdapters.VerificationListCategoryTableAdapter
                AccessInsertVerificationListCategory.Insert(Me.TextCategoryName.Text, CategoryDescription, Now)
            End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    ''' <summary>
    ''' update handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim oSQLTrans As sqltransaction = Nothing
        Page.Validate()

        If Page.IsValid Then
            Try
                Dim CategoryName As String = Trim(Me.TextCategoryName.Text)

                'Periksa apakah CategoryName tersebut sudah ada dalam tabel VerificationListCategory atau belum
                Using AccessCategory As New AMLDAL.AMLDataSetTableAdapters.VerificationListCategoryTableAdapter
                    Dim counter As Int32 = AccessCategory.CountMatchingCategory(CategoryName)

                    'Counter = 0 berarti CategoryName tersebut belum pernah ada dalam tabel VerificationListCategory
                    If counter = 0 Then
                        'Periksa apakah CategoryName tersebut sudah ada dalam tabel VerificationListCategory_Approval atau belum
                        Using AccessCategoryApproval As New AMLDAL.AMLDataSetTableAdapters.VerificationListCategory_ApprovalTableAdapter
                            oSQLTrans = BeginTransaction(AccessCategoryApproval, IsolationLevel.ReadUncommitted)
                            counter = AccessCategoryApproval.CountVerificationListCategoryApprovalByCategoryName(CategoryName)

                            'Counter = 0 berarti CategoryName tersebut statusnya tidak dalam pending approval dan boleh ditambahkan dlm tabel VerificationListCategory_Approval
                            If counter = 0 Then
                                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                                    Me.InsertAuditTrail()
                                    Me.InsertVerificationListCategory()
                                    Me.LblSuccess.Visible = True
                                    Me.LblSuccess.Text = "Success to Insert Verification List Category."
                                Else
                                    Dim CategoryDescription As String
                                    If Me.TextCategoryDescription.Text.Length <= 255 Then
                                        CategoryDescription = Me.TextCategoryDescription.Text
                                    Else
                                        CategoryDescription = Me.TextCategoryDescription.Text.Substring(0, 255)
                                    End If

                                    Dim CategoryPendingApprovalID As Int64

                                    'Using TransScope As New Transactions.TransactionScope
                                    Using AccessCategoryPendingApproval As New AMLDAL.AMLDataSetTableAdapters.VerificationListCategory_PendingApprovalTableAdapter
                                        Settransaction(AccessCategoryPendingApproval, oSQLTrans)
                                        'Tambahkan ke dalam tabel VerificationListCategory_PendingApproval dengan ModeID = 1 (Add) 
                                        CategoryPendingApprovalID = AccessCategoryPendingApproval.InsertVerificationListCategory_PendingApproval(Sahassa.AML.Commonly.SessionUserId, Now, "Verification List Category Add", 1)

                                        'Tambahkan ke dalam tabel VerificationListCategory_Approval dengan ModeID = 1 (Add) 
                                        AccessCategoryApproval.Insert(CategoryPendingApprovalID, 1, Nothing, CategoryName, CategoryDescription, Now, Nothing, Nothing, Nothing, Nothing)

                                        oSQLTrans.commit()
                                        'TransScope.Complete()
                                    End Using
                                    'End Using

                                    Dim MessagePendingID As Integer = 8901 'MessagePendingID 8901 = VerificationListCategory Add 

                                    Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & CategoryName

                                    Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & CategoryName, False)
                                End If
                            Else
                                Throw New Exception("Cannot add the following Category Name: '" & CategoryName & "' because it is currently waiting for approval.")
                            End If
                        End Using
                    Else
                        Throw New Exception("Cannot add the following Category Name: '" & CategoryName & "' because that Category Name already exists in the database.")
                    End If
                End Using
            Catch ex As Exception
                If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
                Me.cvalPageError.IsValid = False
                Me.cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            Finally
                If Not oSQLTrans Is Nothing Then
                    oSQLTrans.dispose()
                    oSQLTrans = Nothing
                End If
            End Try
        End If
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

            'Using Transcope As New Transactions.TransactionScope
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                '        Transcope.Complete()
            End Using
            'End Using
        End If
    End Sub
End Class