<%@ Page Language="VB" MasterPageFile="~/masterpage.master" AutoEventWireup="false" CodeFile="SuspiciusPersonEdit.aspx.vb" Inherits="SuspiciusPersonEdit"  %>
<%@ Register Src="webcontrol/PopUpWIC.ascx" TagName="PopUpWIC" TagPrefix="uc2" %>

<%@ Register Src="webcontrol/PopUpCIF.ascx" TagName="PopUpCIF" TagPrefix="uc1" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
<ajax:AjaxPanel ID="AjaxPanel2" runat="server" >
    <uc1:PopUpCIF ID="PopUpCIF1" runat="server" />
    <uc2:PopUpWIC ID="PopUpWIC1" runat="server" />
  

</ajax:AjaxPanel> 

<table id="title"  border="2" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr>
            <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Custom Alert - &nbsp;Edit
                    <hr />
                </strong>
                <asp:Label ID="LblSuccess" runat="server" CssClass="validationok" Visible="False"
                    Width="100%"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" colspan="4" style="border-top-style: none; border-right-style: none;
                border-left-style: none; height: 24px; border-bottom-style: none"><span style="color: #ff0000">* Required</span></td>
        </tr>
    </table>	
	<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2" height="72" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
            </td>
            <td bgcolor="#ffffff" style="height: 24px" width="20%">
                Source Data</td>
            <td bgcolor="#ffffff" style="height: 24px" width="5">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px" width="80%">
            <ajax:ajaxpanel id="AjaxPanel1" runat="server">
                <asp:DropDownList ID="CboSourceData" runat="server" AutoPostBack="True" CssClass="combobox">
                    <asp:ListItem>None</asp:ListItem>
                    <asp:ListItem Value="CIF"></asp:ListItem>
                    <asp:ListItem Value="WIC"></asp:ListItem>
                </asp:DropDownList>
                <asp:ImageButton ID="Browse" runat="server" ImageUrl="~/Images/button/browse.gif"
                    Visible="False" /></ajax:ajaxpanel>
                </td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
            </td>
            <td bgcolor="#ffffff" style="height: 24px" width="20%">
                CIFNo</td>
            <td bgcolor="#ffffff" style="height: 24px" width="5">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px" width="80%">
                <ajax:AjaxPanel ID="Ajaxpanel14" runat="server">
                    <asp:TextBox ID="txtCIFNo" runat="server" CssClass="textBox" MaxLength="250" Width="200px"></asp:TextBox>
                </ajax:AjaxPanel>
            </td>
        </tr>
		<tr class="formText">
			<td bgColor="#ffffff" style="width: 24px; height: 24px"><br />
                </td>
			<td width="20%" bgColor="#ffffff" style="height: 24px">
                Name</td>
			<td width="5" bgColor="#ffffff" style="height: 24px">:</td>
			<td width="80%" bgColor="#ffffff" style="height: 24px">
			 <ajax:ajaxpanel id="AjaxPanel3" runat="server">
			<asp:textbox id="TxtName" runat="server" CssClass="textBox" MaxLength="250" Width="200px" ></asp:textbox>
                <strong><span style="color: #ff0000">*</span></strong>
                </ajax:ajaxpanel> 
                </td>
		</tr>
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
            </td>
            <td bgcolor="#ffffff" style="height: 24px" width="20%">
                Birth Date</td>
            <td bgcolor="#ffffff" style="height: 24px" width="5">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px" width="80%">
             <ajax:ajaxpanel id="AjaxPanel4" runat="server">
                <asp:TextBox ID="TxtTglLahir" runat="server" CssClass="textBox" Width="120px"></asp:TextBox>
                <input id="PopUpTglLahir" runat="server" name="popUpCalc1" onclick="popUpCalendar(this, form1.TextStartDate, 'dd-mmm-yyyy')"
                    style="border-right: #ffffff 0px solid; border-top: #ffffff 0px solid; font-size: 11px;
                    background-image: url(Script/Calendar/cal.gif); border-left: #ffffff 0px solid;
                    width: 16px; border-bottom: #ffffff 0px solid; height: 17px" title="Click to show calendar"
                    type="button" />
                <strong><span style="color: #ff0000">*</span></strong>
                </ajax:ajaxpanel> 
                </td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
            </td>
            <td bgcolor="#ffffff" style="height: 24px" width="20%">
                Birth Place</td>
            <td bgcolor="#ffffff" style="height: 24px" width="5">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px" width="80%">
             <ajax:ajaxpanel id="AjaxPanel5" runat="server">
                <asp:TextBox ID="TxtTempatLahir" runat="server" CssClass="textBox" MaxLength="200"
                    Width="237px"></asp:TextBox><strong><span style="color: #ff0000"></span></strong>
                    </ajax:ajaxpanel> 
                    </td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
            </td>
            <td bgcolor="#ffffff" style="height: 24px" width="20%">
                Identity No</td>
            <td bgcolor="#ffffff" style="height: 24px" width="5">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px" width="80%">
             <ajax:ajaxpanel id="AjaxPanel6" runat="server">
                <asp:TextBox ID="TxtNoIdentitas" runat="server" CssClass="textBox" MaxLength="50"
                    Width="235px"></asp:TextBox>
                <strong><span style="color: #ff0000">*</span></strong>
                </ajax:ajaxpanel> 
                </td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
            </td>
            <td bgcolor="#ffffff" style="height: 24px" valign="top" width="20%">
                Address</td>
            <td bgcolor="#ffffff" style="height: 24px" valign="top" width="5">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px" valign="top" width="80%">
             <ajax:ajaxpanel id="AjaxPanel7" runat="server">
                <asp:TextBox ID="TxtAlamat" runat="server" CssClass="textBox" MaxLength="350" Rows="5"
                    TextMode="MultiLine" Width="500px"></asp:TextBox><strong><span style="color: #ff0000">*</span></strong>
                    </ajax:ajaxpanel> 
                    </td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
            </td>
            <td bgcolor="#ffffff" style="height: 24px" width="20%">
                Kecamatan/Kelurahan</td>
            <td bgcolor="#ffffff" style="height: 24px" width="5">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px" width="80%">
             <ajax:ajaxpanel id="AjaxPanel8" runat="server">
                <asp:TextBox ID="TxtKecamatanKelurahan" runat="server" CssClass="textBox" MaxLength="50"
                    Width="235px"></asp:TextBox>
                    </ajax:ajaxpanel> 
                    </td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
            </td>
            <td bgcolor="#ffffff" style="height: 24px" width="20%">
                Phone No</td>
            <td bgcolor="#ffffff" style="height: 24px" width="5">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px" width="80%">
             <ajax:ajaxpanel id="AjaxPanel9" runat="server">
                <asp:TextBox ID="TxtNoTelp" runat="server" CssClass="textBox" MaxLength="250" Width="200px"></asp:TextBox>
                </ajax:ajaxpanel> 
                </td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
            </td>
            <td bgcolor="#ffffff" style="height: 24px" width="20%">
                Position</td>
            <td bgcolor="#ffffff" style="height: 24px" width="5">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px" width="80%">
             <ajax:ajaxpanel id="AjaxPanel10" runat="server">
                <asp:TextBox ID="TxtPekerjaan" runat="server" CssClass="textBox" MaxLength="250"
                    Width="200px"></asp:TextBox>
                    </ajax:ajaxpanel> 
                    </td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
            </td>
            <td bgcolor="#ffffff" style="height: 24px" width="20%">
                Company Name</td>
            <td bgcolor="#ffffff" style="height: 24px" width="5">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px" width="80%">
             <ajax:ajaxpanel id="AjaxPanel11" runat="server">
                <asp:TextBox ID="TxtAlamatTempatKerja" runat="server" CssClass="textBox" MaxLength="250"
                    Width="200px"></asp:TextBox>
                    </ajax:ajaxpanel>
                    </td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
            </td>
            <td bgcolor="#ffffff" style="height: 24px" width="20%">
                NPWP</td>
            <td bgcolor="#ffffff" style="height: 24px" width="5">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px" width="80%">
             <ajax:ajaxpanel id="AjaxPanel12" runat="server">
                <asp:TextBox ID="TxtNPWP" runat="server" CssClass="textBox" MaxLength="50" Width="200px"></asp:TextBox>
                </ajax:ajaxpanel> 
                </td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
            </td>
            <td bgcolor="#ffffff" style="height: 24px" width="20%">
                Description</td>
            <td bgcolor="#ffffff" style="height: 24px" width="5">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px" width="80%">
             <ajax:ajaxpanel id="AjaxPanel13" runat="server">
                <asp:TextBox ID="TxtDescription" runat="server" CssClass="textBox" Rows="5" TextMode="MultiLine"
                    Width="500px"></asp:TextBox>
					                    </ajax:ajaxpanel> 
					</td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
            </td>
            <td bgcolor="#ffffff" style="height: 24px" width="20%">
                Created By
            </td>
            <td bgcolor="#ffffff" style="height: 24px" width="5">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px" width="80%">
                <asp:Label ID="lblCreatedBy" runat="server"></asp:Label></td>
        </tr>

	
		<tr class="formText" bgColor="#dddddd" height="30">
			<td style="width: 24px"><IMG height="15" src="images/arrow.gif" width="15"></td>
			<td colSpan="3" style="height: 9px">
				<table cellSpacing="0" cellPadding="3" border="0">
					<tr>
						<td><asp:imagebutton id="ImageSave" runat="server" CausesValidation="True" SkinID="AddButton"></asp:imagebutton></td>
						<td><asp:imagebutton id="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton"></asp:imagebutton></td>
					</tr>
				</table>               
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
		</tr>
        <tr bgcolor="#dddddd" class="formText" height="30">
            <td style="width: 24px">
            </td>
            <td colspan="3" style="height: 9px">
            </td>
        </tr>
	</table>
	<script>
	    document.getElementById('<%=GetFocusID %>').focus();
	</script>
</asp:Content>

