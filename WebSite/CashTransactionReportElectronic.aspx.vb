
Partial Class CashTransactionReportElectronic
    Inherits Parent


    Protected Sub GenerateFileButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles GenerateFileButton.Click
        Dim DateSearch As DateTime
        Try
            If Me.TextSearch.Text = "" Then
                Throw New Exception("Please select date first.")
            End If
            DateSearch = DateTime.Parse(Me.TextSearch.Text, New System.Globalization.CultureInfo("id-ID"))
            Using CTRAdapter As New AMLDAL.CTRDataSetTableAdapters.CTRSelectTableAdapter
                Using CTRTable As New AMLDAL.CTRDataSet.CTRSelectDataTable
                    CTRAdapter.FillByCreatedDate(CTRTable, DateSearch)
                    If Not CTRTable Is Nothing Then
                        If CTRTable.Rows.Count > 0 Then
                            Page.Response.Redirect("CashTransactionReportElectronicDownload.aspx?CreatedDate=" & DateSearch.ToString("yyyy-MM-dd"), False)
                        Else
                            Throw New Exception("There no CTR data at selected date '" & DateSearch.ToString("yyyy-MM-dd") & "'")
                        End If
                    Else
                        Throw New Exception("There no CTR data at selected date '" & DateSearch.ToString("yyyy-MM-dd") & "'")
                    End If
                End Using
            End Using

        Catch ex As ArgumentOutOfRangeException
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = "Input character cannot be convert to datetime."
        Catch ex As FormatException
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = "Unknown format date."
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub UpdateButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles UpdateButton.Click
        Dim DateSearch As DateTime
        Try
            If Me.TextSearch.Text = "" Then
                Throw New Exception("Please select date first.")
            End If
            DateSearch = DateTime.Parse(Me.TextSearch.Text, New System.Globalization.CultureInfo("id-ID"))
            Using CTRAdapter As New AMLDAL.CTRDataSetTableAdapters.CTRSelectTableAdapter
                Using CTRTable As New AMLDAL.CTRDataSet.CTRSelectDataTable
                    CTRAdapter.FillByCreatedDate(CTRTable, DateSearch)
                    If Not CTRTable Is Nothing Then
                        If CTRTable.Rows.Count > 0 Then
                            Me.PPATKConfirmationNumberPanel.Visible = True
                        Else
                            Throw New Exception("There no CTR data at selected date '" & DateSearch.ToString("yyyy-MM-dd") & "'")
                        End If
                    Else
                        Throw New Exception("There no CTR data at selected date '" & DateSearch.ToString("yyyy-MM-dd") & "'")
                    End If
                End Using
            End Using


        Catch ex As ArgumentOutOfRangeException
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = "Input character cannot be convert to datetime."
        Catch ex As FormatException
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = "Unknown format date."
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Private Function GetCTRUploadedConfirmationNumber(ByVal file As HttpPostedFile) As ArrayList

        Dim myStream As System.IO.Stream
        ' Initialize the stream to read the uploaded file.
        myStream = FileUpload1.FileContent
        Dim StrConfirmationNumber As String = ""
        Using sr As IO.StreamReader = New IO.StreamReader(myStream)
            StrConfirmationNumber = sr.ReadToEnd
            sr.Close()
        End Using
        Dim ArrConfirmationNumber As String() = Nothing
        Dim ArrReturn As New ArrayList
        Dim Counter As Integer
        ArrConfirmationNumber = StrConfirmationNumber.Split(vbCrLf)
        If Not ArrConfirmationNumber Is Nothing Then
            If ArrConfirmationNumber.Length > 0 Then
                ''                ' Contoh
                ''                '20040810-588243
                ''                '20040810-457584
                ''                '20040810-259348
                'For Counter = 0 To ArrConfirmationNumber.Length - 1
                '    If ArrConfirmationNumber(Counter).Trim().Length = 21 Then
                '        ArrReturn.Add(ArrConfirmationNumber(Counter).Trim())
                '    Else
                '        Throw New Exception("Invalid Confirmation Number '" & ArrConfirmationNumber(Counter).Trim() & "' at line " & Counter + 1 & ". Confirmation Number must be 21 characters.")
                '    End If
                'Next
                ' Contoh new
                '1. 20080115-721376247808
                '2. 20080115-887915347968
                '3. 20080115-857139511296
                '4. 20080115-028095602688
                '5. 20080115-699248345088
                '6. 20080115-685790068736
                '7. 20080115-758788259840
                ' Update 19 November 2009 by Johan cause changes in PPATK system
                'Nama File	Order	No Referensi Internal PJK	No Referensi PPATK
                'C080910001	00023	 	SR5082-C080910001-00023-20080919
                'C080910001	00024	 	SR5082-C080910001-00024-20080919
                'C080910001	00025	 	SR5082-C080910001-00025-20080919
                'C080910001	00026	 	SR5082-C080910001-00026-20080919
                'C080910001	00027	 	SR5082-C080910001-00027-20080919
                'C080910001	00028	 	SR5082-C080910001-00028-20080919
                'C080910001	00029	 	SR5082-C080910001-00029-20080919
                'C080910001	00030	 	SR5082-C080910001-00030-20080919
                'C080910001	00031	 	SR5082-C080910001-00031-20080919
                'C080910001	00032	 	SR5082-C080910001-00032-20080919
                Dim StrConfirmationNumberDetail As String
                Dim IntStartCounter As Integer
                IntStartCounter = 0
                If Not System.Configuration.ConfigurationManager.AppSettings("CTRConfirmSkipFirstRow") Is Nothing Then
                    If System.Configuration.ConfigurationManager.AppSettings("CTRConfirmSkipFirstRow") Then
                        IntStartCounter = 1
                    End If
                End If

                For Counter = IntStartCounter To ArrConfirmationNumber.Length - 1
                    'StrConfirmationNumberDetail = ArrConfirmationNumber(Counter).Trim()
                    'If StrConfirmationNumberDetail <> "" Then
                    '    StrConfirmationNumberDetail = StrConfirmationNumberDetail.Substring(StrConfirmationNumberDetail.IndexOf(".") + 1).Trim()
                    '    If Text.RegularExpressions.Regex.IsMatch(StrConfirmationNumberDetail, "^\d{8,8}\-\d{12,12}$") Then
                    '        ArrReturn.Add(StrConfirmationNumberDetail)
                    '    Else
                    '        Throw New Exception("Invalid Confirmation Number '" & StrConfirmationNumberDetail & "' at line " & Counter + 1 & ". Confirmation Number must be using following format 'XXXXXXXX-XXXXXXXXXXXX' (21 Characters)")
                    '    End If
                    'End If
                    ' Update 19 November 2009 by Johan cause changes in PPATK system
                    StrConfirmationNumberDetail = ArrConfirmationNumber(Counter).Trim()
                    StrConfirmationNumberDetail = Right(StrConfirmationNumberDetail, 32)
                    If StrConfirmationNumberDetail <> "" Then
                        If Text.RegularExpressions.Regex.IsMatch(StrConfirmationNumberDetail, "^\w{6,6}\-\w{10,10}\-\d{5,5}\-\d{8,8}$") Then
                            ArrReturn.Add(StrConfirmationNumberDetail)
                        Else
                            Throw New Exception("Invalid Confirmation Number '" & StrConfirmationNumberDetail & "' at line " & Counter + 1 & ". Confirmation Number must be using following format 'XXXXXX-XXXXXXXXXX-XXXXX-XXXXXXXX' (32 Characters)")
                        End If
                    End If
                Next
            End If
        End If
        Return ArrReturn
    End Function


    Protected Sub SaveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles SaveButton.Click
        Dim DateSearch As DateTime
        Try
            If FileUpload1.HasFile Then
                Dim ArrConfirmationNumber As New ArrayList
                ArrConfirmationNumber = GetCTRUploadedConfirmationNumber(FileUpload1.PostedFile)
                If Not ArrConfirmationNumber Is Nothing Then
                    If ArrConfirmationNumber.Count > 0 Then
                        DateSearch = DateTime.Parse(Me.TextSearch.Text, New System.Globalization.CultureInfo("id-ID"))
                        Using CTRAdapter As New AMLDAL.CTRDataSetTableAdapters.CTRSelectTableAdapter
                            Using CTRTable As New AMLDAL.CTRDataSet.CTRSelectDataTable
                                CTRAdapter.FillByCreatedDate(CTRTable, DateSearch)
                                If Not CTRTable Is Nothing Then
                                    If CTRTable.Rows.Count > 0 Then
                                        If ArrConfirmationNumber.Count = CTRTable.Rows.Count Then
                                            ' Do Update PPATK Confirmation
                                            'CTRAdapter.CTRUpdateByPPATKConfirmationNoCreatedDate(Me.PPATKConfirmationNumberTextBox.Text, Sahassa.AML.Commonly.SessionUserId, DateSearch)
                                            Dim Counter As Integer = 0
                                            Dim CTRTableRow As AMLDAL.CTRDataSet.CTRSelectRow
                                            For Each CTRTableRow In CTRTable.Rows
                                                CTRAdapter.CTRUpdatePPATKConfirmationNoByPK_CTRID(ArrConfirmationNumber(Counter), Sahassa.AML.Commonly.SessionUserId, CTRTableRow.PK_CTRID)
                                                Counter = Counter + 1
                                            Next
                                            Me.PPATKConfirmationNumberPanel.Visible = False
                                            Me.UpdatePPATKConfirmationNumberStatusLabel.Text = "Update PPATK Confirmation Number has done successfully."
                                            Me.UpdatePPATKConfirmationNumberStatusLabel.Visible = True
                                        Else
                                            Throw New Exception("Count Of Confirmation Number not same with count of CTR at selected date '" & DateSearch.ToString("yyyy-MM-dd") & "'")
                                        End If
                                    Else
                                        Throw New Exception("There no CTR data at selected date '" & DateSearch.ToString("yyyy-MM-dd") & "'")
                                    End If
                                Else
                                    Throw New Exception("There no CTR data at selected date '" & DateSearch.ToString("yyyy-MM-dd") & "'")
                                End If
                            End Using
                        End Using
                    End If
                End If
            End If
            
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Me.popUp.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TextSearch.ClientID & "'), 'dd-mmm-yyyy')")
            Me.PPATKConfirmationNumberPanel.Visible = False
            Me.UpdatePPATKConfirmationNumberStatusLabel.Visible = False
        End If
    End Sub

    Protected Sub CancelUpdateButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CancelUpdateButton.Click
        Me.UpdatePPATKConfirmationNumberStatusLabel.Visible = False
        Me.PPATKConfirmationNumberPanel.Visible = False
    End Sub
End Class
