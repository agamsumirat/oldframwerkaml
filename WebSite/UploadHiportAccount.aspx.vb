﻿Imports System.Data

Partial Class UploadHiportAccount
    Inherits Parent
    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try

            If Not Page.IsPostBack Then
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)


                End Using
            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImgSearch_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImgSearch.Click


        Try
            If FileUpload1.HasFile Then
                If System.IO.Path.GetExtension(FileUpload1.FileName).ToLower = ".txt" Then

                    Dim currentrow(4) As String
                    Dim NAV_TXN_DATE As String
                    Dim FUND_ID As String
                    Dim description As String
                    Dim CIF As String
                    Dim NAV As String
                    Dim accountno As String
                    Dim objDt As New System.Data.DataTable


                    Using cmd As System.Data.SqlClient.SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("usp_DeleteValidateAccountHiport")
                        Dim customerIDParam As New System.Data.SqlClient.SqlParameter("@UserID", Sahassa.AML.Commonly.SessionUserId)
                        cmd.Parameters.Add(customerIDParam)
                        SahassaNettier.Data.DataRepository.Provider.ExecuteNonQuery(cmd)
                    End Using


                    Using NAVDate As New DataColumn()
                        NAVDate.ColumnName = "NAVDate"
                        NAVDate.DataType = GetType(String)
                        objDt.Columns.Add(NAVDate)
                    End Using

                    Using objFundid As New DataColumn()
                        objFundid.ColumnName = "FundID"
                        objFundid.DataType = GetType(String)
                        objDt.Columns.Add(objFundid)
                    End Using

                    Using objFundid As New DataColumn()
                        objFundid.ColumnName = "Description"
                        objFundid.DataType = GetType(String)
                        objDt.Columns.Add(objFundid)
                    End Using

                    Using objFundid As New DataColumn()
                        objFundid.ColumnName = "CIF"
                        objFundid.DataType = GetType(String)
                        objDt.Columns.Add(objFundid)
                    End Using
                    Using objFundid As New DataColumn()
                        objFundid.ColumnName = "NAV"
                        objFundid.DataType = GetType(String)
                        objDt.Columns.Add(objFundid)
                    End Using

                    Using objFundid As New DataColumn()
                        objFundid.ColumnName = "AccountNo"
                        objFundid.DataType = GetType(String)
                        objDt.Columns.Add(objFundid)
                    End Using
                    

                    TxtValidasi.Text = ""

                    Using MyReader As New Microsoft.VisualBasic.FileIO.TextFieldParser(FileUpload1.FileContent)
                        MyReader.TextFieldType = FileIO.FieldType.Delimited
                        MyReader.SetDelimiters("|")
                        Dim intBaris As Integer = 0
                        While Not MyReader.EndOfData
                            intBaris += 1

                            If intBaris > 2 Then
                                currentrow = MyReader.ReadFields()


                                Dim objnewrow As DataRow = objDt.NewRow
                                objnewrow("NAVDate") = currentrow(0)
                                objnewrow("FundID") = currentrow(1)
                                objnewrow("Description") = currentrow(2)
                                objnewrow("CIF") = currentrow(3)
                                objnewrow("NAV") = currentrow(4)
                                objnewrow("AccountNo") = currentrow(5)
                                objDt.Rows.Add(objnewrow)



                            Else
                                MyReader.ReadFields()
                            End If
                           
                        End While

                        AMLBLL.HiportAccountBLL.ProcessTextHiport(objDt)
                        BindGrid()
                        TxtValidasi.Text = AMLBLL.HiportAccountBLL.GetInValidAccount
                        If TxtValidasi.Text.Trim.Length > 0 Then
                            PanelValidasi.Visible = True
                            ImageSave.Visible = False
                        Else
                            PanelValidasi.Visible = False
                            ImageSave.Visible = True
                        End If
                    End Using


                Else
                    cvalPageError.IsValid = False
                    cvalPageError.ErrorMessage = "File Hiport Must Txt."
                End If

            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
    Sub BindGrid()

        GridValidData.DataSource = AMLBLL.HiportAccountBLL.GetValidAccount
        GridValidData.PageSize = Sahassa.AML.Commonly.SessionPagingLimit
        GridValidData.DataBind()
        If GridValidData.Rows.Count > 0 Then
            PanelValidData.Visible = True
        Else
            PanelValidData.Visible = False
        End If
    End Sub


    Protected Sub ImgBack_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImgBack.Click
        Try
            Response.Redirect("UploadHiportAccount.aspx", False)

        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub GridValidData_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridValidData.PageIndexChanging
        Try

            GridValidData.PageIndex = e.NewPageIndex
            BindGrid()

        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImageSave_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Try
            If Sahassa.AML.Commonly.SessionPkUserId = "1" Then
                AMLBLL.HiportAccountBLL.Save()
                LblMessage.Text = "Hiport Data Saved"
            Else
                AMLBLL.HiportAccountBLL.SaveApproval()
                LblMessage.Text = "Hiport Data Saved into Pending Approval"
            End If
            MultiView1.ActiveViewIndex = 1


        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
End Class
