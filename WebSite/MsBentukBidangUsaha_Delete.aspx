<%@ Page Language="VB" AutoEventWireup="false" CodeFile="~/MsBentukBidangUsaha_Delete.aspx.vb"
    Inherits="MsBentukBidangUsaha_Delete" MasterPageFile="~/MasterPage.master" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <img src="Images/blank.gif" width="5" height="1" />
            </td>
            <td>
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td width="99%" bgcolor="#FFFFFF">
                            <img src="Images/blank.gif" width="1" height="1" />
                        </td>
                        <td bgcolor="#FFFFFF">
                            <img src="Images/blank.gif" width="1" height="1" />
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="Images/blank.gif" width="5" height="1" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td id="tdcontent" valign="top" bgcolor="#FFFFFF">
                <table id="tblpenampung" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="width: 14px">
                            <img src="Images/blank.gif" width="20" height="100%" />
                        </td>
                        <td class="divcontentinside" bgcolor="#FFFFFF" style="width: 100%">
                            &nbsp;
                            <ajax:AjaxPanel ID="AjaxPanel5" runat="server" Width="100%" meta:resourcekey="AjaxPanel5Resource1">
                                <asp:MultiView ID="MtvMsUser" runat="server" ActiveViewIndex="0">
                                    <asp:View ID="VwAdd" runat="server">
                                        <table style="width: 100%" bordercolor="#ffffff" cellspacing="1" cellpadding="2"
                                            width="100%" bgcolor="#dddddd" border="0">
                                            <tr>
                                                <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                                                    border-right-style: none; border-left-style: none; border-bottom-style: none"
                                                    valign="top">
                                                    <img src="Images/dot_title.gif" width="17" height="17">
                                                    <asp:Label ID="LblValue" runat="server" Font-Bold="True" Font-Size="Medium" Text="Master Bentuk Badan Usaha Delete"></asp:Label>
                                                    <hr />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                                                    border-right-style: none; border-left-style: none; border-bottom-style: none;
                                                    height: 26px;" valign="top">
                                                </td>
                                            </tr>
                                            <tr bgcolor="#ffffff">
                                                <td style="width: 22px; height: 15px">
                                                </td>
                                                <td bgcolor="#fff7e6" style="width: 156px; height: 15px" valign="top">
                                                    <asp:Label ID="LabelIdBentukBidangUsaha" runat="server" Text="Id"></asp:Label>
                                                </td>
                                                <td style="width: 6px; height: 15px" valign="top">
                                                    :
                                                </td>
                                                <td colspan="7" style="height: 15px" valign="top">
                                                    <asp:Label ID="lblIdBentukBidangUsaha" runat="server" ForeColor="#404040"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr bgcolor="#ffffff">
                                                <td style="width: 22px; height: 15px">
                                                </td>
                                                <td bgcolor="#fff7e6" style="width: 156px; height: 15px" valign="top">
                                                    <asp:Label ID="LabelBentukBidangUsaha" runat="server" Text="Nama"></asp:Label>
                                                </td>
                                                <td style="width: 6px; height: 15px" valign="top">
                                                    :
                                                </td>
                                                <td colspan="7" style="height: 15px" valign="top">
                                                    <asp:Label ID="lblBentukBidangUsaha" runat="server" ForeColor="#404040"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr bgcolor="#ffffff">
                                                <td style="width: 22px; height: 15px">
                                                    &nbsp;</td>
                                                <td bgcolor="#fff7e6" style="width: 156px; height: 15px" valign="top">
                                                    <asp:Label ID="LabelNamaBidangUsaha0" runat="server" Text="Activation"></asp:Label>
                                                </td>
                                                <td style="width: 6px; height: 15px" valign="top">
                                                    :</td>
                                                <td style="height: 15px" valign="top">
                                                    <asp:Label ID="lblActivation" runat="server" ForeColor="#404040"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr bgcolor="#ffffff">
                                                <td style="width: 22px; height: 24px">
                                                    &nbsp;</td>
                                                <td bgcolor="#fff7e6" style="width: 153px; height: 24px" valign="top">
                                                    Mapping Item
                                                </td>
                                                <td style="width: 6px; height: 24px" valign="top">
                                                    :</td>
                                                <td style="height: 24px" valign="top">
                                                    <asp:ListBox ID="LBMapping" runat="server" Height="204px" Width="170px" CssClass="textbox">
                                                    </asp:ListBox>
                                                </td>
                                            </tr>
                                            <tr bgcolor="#ffffff">
                                                <td style="height: 24px">
                                                    &nbsp;</td>
                                                <td bgcolor="#fff7e6" style="height: 24px" valign="top">
                                                    Created Date</td>
                                                <td style="width: 6px; height: 24px" valign="top">
                                                    :</td>
                                                <td style="height: 24px" valign="top">
                                                    <asp:Label ID="lblCreatedDate" runat="server" ForeColor="#404040"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr bgcolor="#ffffff">
                                                <td style="height: 24px">
                                                    &nbsp;</td>
                                                <td bgcolor="#fff7e6" style="height: 24px" valign="top">
                                                    Created By</td>
                                                <td style="width: 6px; height: 24px" valign="top">
                                                    :</td>
                                                <td style="height: 24px" valign="top">
                                                    <asp:Label ID="lblCreatedBy" runat="server" ForeColor="#404040"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr bgcolor="#ffffff">
                                                <td style="height: 24px">
                                                    &nbsp;</td>
                                                <td bgcolor="#fff7e6" style="height: 24px" valign="top">
                                                    Updated Date</td>
                                                <td style="width: 6px; height: 24px" valign="top">
                                                    :</td>
                                                <td style="height: 24px" valign="top">
                                                    <asp:Label ID="lblUpdatedDate" runat="server" ForeColor="#404040"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr bgcolor="#ffffff">
                                                <td style="height: 24px">
                                                    &nbsp;</td>
                                                <td bgcolor="#fff7e6" style="height: 24px" valign="top">
                                                    Updated By</td>
                                                <td style="width: 6px; height: 24px" valign="top">
                                                    :</td>
                                                <td style="height: 24px" valign="top">
                                                    <asp:Label ID="lblUpdatedby" runat="server" ForeColor="#404040"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:View>
                                    <asp:View ID="VwConfirmation" runat="server">
                                        <table style="width: 100%" bordercolor="#ffffff" cellspacing="1" cellpadding="2"
                                            width="100%" bgcolor="#dddddd" border="0">
                                            <tr bgcolor="#ffffff">
                                                <td colspan="2" align="center" style="height: 17px">
                                                    <asp:Label ID="LblConfirmation" runat="server" meta:resourcekey="LblConfirmationResource1"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr bgcolor="#ffffff">
                                                <td align="center" colspan="2">
                                                    <asp:ImageButton ID="ImgBtnAdd" runat="server" ImageUrl="~/images/button/Ok.gif"
                                                        CausesValidation="False" meta:resourcekey="ImgBtnAddResource1" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:View>
                                </asp:MultiView>
                            </ajax:AjaxPanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <ajax:AjaxPanel ID="AjaxPanel1" runat="server">
                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td background="Images/button-bground.gif" align="left" valign="middle">
                                <img src="Images/blank.gif" width="5" height="1" /></td>
                            <td background="Images/button-bground.gif" align="left" valign="middle">
                                &nbsp;</td>
                            <td background="Images/button-bground.gif" style="width: 5px">
                                &nbsp;</td>
                            <td background="Images/button-bground.gif">
                                &nbsp;</td>
                            <td background="Images/button-bground.gif">
                                <asp:ImageButton ID="imgOk" runat="server" CausesValidation="False" ImageUrl="~/Images/button/delete.gif">
                                </asp:ImageButton>
                            </td>
                            <td background="Images/button-bground.gif">
                                &nbsp;<asp:ImageButton ID="ImageCancel" runat="server" CausesValidation="False" ImageUrl="~/Images/button/back.gif">
                                </asp:ImageButton></td>
                            <td width="99%" background="Images/button-bground.gif">
                                <img src="Images/blank.gif" width="1" height="1" />
                                <asp:CustomValidator ID="CvalHandleErr" runat="server" Display="None" meta:resourcekey="CvalHandleErrResource1"
                                    ValidationGroup="handle"></asp:CustomValidator>
                                <asp:CustomValidator ID="CvalPageErr" runat="server" Display="None" meta:resourcekey="CvalPageErrResource1"></asp:CustomValidator>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </ajax:AjaxPanel>
            </td>
        </tr>
    </table>
</asp:Content>
