﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="CaseManagementViewByCIFDetail.aspx.vb" Inherits="CaseManagementViewByCIFDetail"
    Culture="id-ID" UICulture="id-ID" ValidateRequest="false" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
        height="72" style="border-top-style: none; border-right-style: none; border-left-style: none;
        border-bottom-style: none" width="100%">
        <tr>
            <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Alert Case Management Detail</strong>
            </td>
        </tr>
    </table>
    <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
        border="2">
        <ajax:AjaxPanel ID="AjaxPanel15" runat="server">
            <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></ajax:AjaxPanel><tr>
                <td>
                    <table>
                        <tr>
                            <td style="width: 69%">
                                <asp:GridView runat="server" ID="GrdSum" AutoGenerateColumns="False" EnableModelValidation="True"
                                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px"
                                    CellPadding="4" ForeColor="Black">
                                    <AlternatingRowStyle BackColor="White" />
                                    <Columns>
                                        <asp:BoundField DataField="CaseStatusDescription" HeaderText="Case Status" />
                                        <asp:BoundField DataField="Count" HeaderText="Count" />
                                        <asp:BoundField DataField="AlertType" HeaderText="Alert Type" />
                                        <asp:BoundField DataField="Aging" HeaderText="Aging" />
                                    </Columns>
                                    <FooterStyle BackColor="#CCCC99" />
                                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                    <RowStyle BackColor="#F7F7DE" />
                                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        <tr>
            <td bgcolor="#ffffff">
                <ajax:AjaxPanel ID="AjaxPanel4" runat="server">
                    <asp:DataGrid ID="GridMSUserView" runat="server" AutoGenerateColumns="False" Font-Size="XX-Small"
                        BackColor="White" CellPadding="4" BorderWidth="1px" BorderStyle="None" AllowPaging="True"
                        Width="100%" GridLines="Vertical" AllowSorting="True" BorderColor="#DEDFDE" ForeColor="Black">
                        <FooterStyle BackColor="#CCCC99"></FooterStyle>
                        <SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#CE5D5A"></SelectedItemStyle>
                        <AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
                        <ItemStyle BackColor="#F7F7DE" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                            Font-Strikeout="False" Font-Underline="False"></ItemStyle>
                        <HeaderStyle Font-Size="XX-Small" Font-Bold="True" ForeColor="White" BackColor="#6B696B">
                        </HeaderStyle>
                        <Columns>
                            <asp:TemplateColumn>
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBoxExporttoExcel" runat="server" ajaxcall="none" AutoPostBack="True"
                                        OnCheckedChanged="CheckBoxExporttoExcel_CheckedChanged" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="PK_CaseManagementID" SortExpression="CaseManagement.PK_CaseManagementID  desc"
                                HeaderText="Alert Case ID"></asp:BoundColumn>
                            <asp:BoundColumn DataField="cifno" HeaderText="CIF No" SortExpression="cifno  desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CustomerName" HeaderText="Customer Name" SortExpression="CustomerName desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="RMCode" HeaderText="RM Code" SortExpression="RMCode desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CaseDescription" HeaderText="Alert Type" SortExpression="CaseManagement.CaseDescription  desc">
                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" Wrap="False" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CaseStatusDescription" HeaderText="Case Status" SortExpression="CaseStatus.CaseStatusDescription  desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="WorkflowStep" SortExpression="CaseManagement.WorkflowStep  desc"
                                HeaderText="Workflow Step"></asp:BoundColumn>
                            <asp:BoundColumn DataField="CreatedDate" HeaderText="Created Date" SortExpression="CaseManagement.CreatedDate  desc"
                                DataFormatString="{0:dd-MM-yyyy}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="LastUpdated" HeaderText="Last Updated Date" SortExpression="CaseManagement.LastUpdated  desc"
                                DataFormatString="{0:dd-MM-yyyy HH:mm}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="AccountOwnerIDName" HeaderText="Account Owner" SortExpression="CAST(CaseManagement.FK_AccountOwnerID AS VARCHAR) + ' - ' + AccountOwner.AccountOwnerName desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn HeaderText="PIC">
                                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" ForeColor="White" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="HighRiskCustomer" HeaderText="AML/CFT Risk" SortExpression="dbo.ufn_GetHighRiskCustomerByPkCaseManagementID(casemanagement.PK_CaseManagementID) desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Aging" HeaderText="Aging" SortExpression="casemanagement.Aging  desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="ProposedAction" HeaderText="Last Proposed Action" SortExpression="CaseManagementProposedAction.ProposedAction desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="SameCaseCount" HeaderText="Count Same Case" SortExpression="(SELECT COUNT(1) FROM CaseManagementSameCaseHistory cmsch WHERE cmsch.FK_CaseManagement_ID=CaseManagement.PK_CaseManagementID)  desc">
                            </asp:BoundColumn>
                            <asp:EditCommandColumn EditText="Detail"></asp:EditCommandColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Right" ForeColor="Black" BackColor="#F7F7DE"
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                </ajax:AjaxPanel>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="background-color: #ffffff">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td nowrap>
                            &nbsp;<asp:CheckBox ID="CheckBoxSelectAll" runat="server" Text="Select All Current Page"
                                AutoPostBack="True" />
                            &nbsp;
                        </td>
                        <td width="99%">
                            &nbsp;&nbsp;<asp:LinkButton ID="LinkButtonExportExcel" runat="server">Export 
		        to Excel</asp:LinkButton>
                            <asp:LinkButton ID="LnkBtnExportAllToExcel" runat="server">Export All to Excel</asp:LinkButton>
                        </td>
                        <td align="right" nowrap>
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor="#ffffff">
                <table id="Table3" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                    bgcolor="#ffffff" border="2">
                    <tr class="regtext" align="center" bgcolor="#dddddd">
                        <td valign="top" align="left" bgcolor="#ffffff" style="width: 50%">
                            Page&nbsp;<ajax:AjaxPanel ID="AjaxPanel6" runat="server">
                                <asp:Label ID="PageCurrentPage" runat="server" CssClass="regtext">0</asp:Label>
                                &nbsp;of&nbsp;
                                <asp:Label ID="PageTotalPages" runat="server" CssClass="regtext">0</asp:Label>
                            </ajax:AjaxPanel>
                        </td>
                        <td valign="top" align="right" width="50%" bgcolor="#ffffff">
                            Total Records&nbsp;
                            <ajax:AjaxPanel ID="AjaxPanel7" runat="server">
                                <asp:Label ID="PageTotalRows" runat="server">0</asp:Label>
                            </ajax:AjaxPanel>
                        </td>
                    </tr>
                </table>
                <table id="Table4" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                    bgcolor="#ffffff" border="2">
                    <tr bgcolor="#ffffff">
                        <td class="regtext" valign="middle" align="left" colspan="11" height="7">
                            <hr color="#f40101" noshade size="1">
                        </td>
                    </tr>
                    <tr>
                        <td class="regtext" valign="middle" align="left" width="63" bgcolor="#ffffff">
                            Go to page
                        </td>
                        <td class="regtext" valign="middle" align="left" width="5" bgcolor="#ffffff">
                            <font face="Verdana, Arial, Helvetica, sans-serif" size="1">
                                <ajax:AjaxPanel ID="AjaxPanel8" runat="server">
                                    <asp:TextBox ID="TextGoToPage" runat="server" Width="38px" CssClass="searcheditbox"></asp:TextBox>
                                </ajax:AjaxPanel>
                            </font>
                        </td>
                        <td class="regtext" valign="middle" align="left" bgcolor="#ffffff">
                            <ajax:AjaxPanel ID="AjaxPanel9" runat="server">
                                <asp:ImageButton ID="ImageButtonGo" runat="server" SkinID="GoButton"></asp:ImageButton>
                            </ajax:AjaxPanel>
                        </td>
                        <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                            <img height="5" src="images/first.gif" width="6">
                        </td>
                        <td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff">
                            <ajax:AjaxPanel ID="AjaxPanel10" runat="server">
                                <asp:LinkButton ID="LinkButtonFirst" runat="server" CssClass="regtext" CommandName="First"
                                    OnCommand="PageNavigate">First</asp:LinkButton>
                            </ajax:AjaxPanel>
                        </td>
                        <td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff">
                            <img height="5" src="images/prev.gif" width="6">
                        </td>
                        <td class="regtext" valign="middle" align="right" width="14" bgcolor="#ffffff">
                            <ajax:AjaxPanel ID="AjaxPanel11" runat="server">
                                <asp:LinkButton ID="LinkButtonPrevious" runat="server" CssClass="regtext" CommandName="Prev"
                                    OnCommand="PageNavigate">Previous</asp:LinkButton>
                            </ajax:AjaxPanel>
                        </td>
                        <td class="regtext" valign="middle" align="right" width="60" bgcolor="#ffffff">
                            <ajax:AjaxPanel ID="AjaxPanel12" runat="server">
                                <a class="pageNav" href="#">
                                    <asp:LinkButton ID="LinkButtonNext" runat="server" CssClass="regtext" CommandName="Next"
                                        OnCommand="PageNavigate">Next</asp:LinkButton>
                                </a>
                            </ajax:AjaxPanel>
                        </td>
                        <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                            <img height="5" src="images/next.gif" width="6">
                        </td>
                        <td class="regtext" valign="middle" align="left" width="25" bgcolor="#ffffff">
                            <ajax:AjaxPanel ID="AjaxPanel13" runat="server">
                                <asp:LinkButton ID="LinkButtonLast" runat="server" CssClass="regtext" CommandName="Last"
                                    OnCommand="PageNavigate">Last</asp:LinkButton>
                            </ajax:AjaxPanel>
                        </td>
                        <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                            <img height="5" src="images/last.gif" width="6">
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="Panel1" runat="server" Height="100%" Width="100% ">
                    <table style="width: 100%" runat="server" id="TblPropose">
                        <tr>
                            <td colspan="3">
                                <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="validation"
                                    ValidationGroup="FillInvestigation" Width="100%" />
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 15px" nowrap="noWrap" valign="top" width="1%">
                                Investigation Notes<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                    Display="Dynamic" ErrorMessage="Please Fill Investigation Notes" ValidationGroup="FillInvestigation"
                                    ControlToValidate="TextInvestigationNotes">*</asp:RequiredFieldValidator>
                            </td>
                            <td style="height: 15px" width="1">
                                :
                            </td>
                            <td style="height: 15px" width="99%">
                                <asp:TextBox ID="TextInvestigationNotes" runat="server" Columns="5" Rows="5" TextMode="MultiLine"
                                    Width="400px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="noWrap" width="1%">
                                Proposed Action
                            </td>
                            <td width="1">
                                :
                            </td>
                            <td width="99%">
                                <%--<ajax:AjaxPanel ID="test" runat="server">--%>
                                <asp:DropDownList ID="CboProposedAction" runat="server" AppendDataBoundItems="True"
                                    AutoPostBack="True">
                                </asp:DropDownList>
                                <%--</ajax:AjaxPanel>--%>
                            </td>
                        </tr>
                        <tr id="trAccountOwner" runat="server">
                            <td nowrap="nowrap" width="1%">
                                New Account Owner
                            </td>
                            <td width="1">
                                :
                            </td>
                            <td width="99%">
                                <%--<ajax:AjaxPanel ID="AjaxPanel1" runat="server">--%>
                                <asp:DropDownList ID="CboAccountOwner" runat="server">
                                </asp:DropDownList>
                                <%--</ajax:AjaxPanel>--%>
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="noWrap" width="1%">
                                Attachment<asp:CustomValidator ID="CustomValidator1" runat="server" Display="Dynamic"
                                    ErrorMessage="CustomValidator" ValidationGroup="FillInvestigation">*</asp:CustomValidator>
                            </td>
                            <td width="1">
                                :
                            </td>
                            <td width="99%">
                                <asp:FileUpload ID="FileUpload1" runat="server" />
                                <asp:Button ID="ButtonAdd" runat="server" Text="Add" ValidationGroup="FillInvestigation" />
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" width="1%">
                            </td>
                            <td width="1">
                            </td>
                            <td valign="top" width="99%">
                                <asp:ListBox ID="ListAttachment" runat="server" Width="400px"></asp:ListBox>
                                <asp:Button ID="ButtonDelete" runat="server" Text="Delete" />
                            </td>
                        </tr>
                    </table>
                    <asp:Button ID="BtnSaveAsDraftWorklfow" runat="server" Text="Save As Draft " Visible="False" />
                        <asp:Button ID="BtnDoneProposed" runat="server" Text="Done & Proposed " ValidationGroup="FillInvestigation" OnClientClick="javascript:this.disabled=true;this.value='Processing.. Please wait..';" UseSubmitBehavior="false" />
                </asp:Panel>
            </td>
        </tr>
    </table>
    <asp:ImageButton ID="ImageButton1" runat="server" SkinID="BackButton" OnClientClick="javascript:history.back()" />
</asp:Content>
