
Partial Class ProposalSTRDetail
    Inherits Parent

    Public Enum EnProposalStatus
        bNew = 1
        InProgress
        Complated
    End Enum
    Public Enum EnProposalEventType
        WorkFlowStated = 1
        TaskStarted
        TaskCompleted
        TaskCanceledBySystem
        WorkFlowFinish
    End Enum
    Public Enum EnProposalResponse
        DilaporkankePPATKsebagaiSTR = 1
        DilaporkankePPATKsebagaiInformasi
        TidakDilaporkankePPATKsebagaiSTR
    End Enum
    Private _ProposalSTR As AMLDAL.ProposalSTR.ProposalSTRRow
    Private ReadOnly Property ProposalSTR() As AMLDAL.ProposalSTR.ProposalSTRRow
        Get
            Try
                If Not _ProposalSTR Is Nothing Then
                    Return _ProposalSTR
                Else
                    Using adapter As New AMLDAL.ProposalSTRTableAdapters.ProposalSTRTableAdapter
                        Using otable As AMLDAL.ProposalSTR.ProposalSTRDataTable = adapter.GetProposalSTRByPK(Me.PRoposalSTRID)
                            If otable.Rows.Count > 0 Then
                                _ProposalSTR = otable.Rows(0)
                                Return _ProposalSTR
                            Else
                                _ProposalSTR = Nothing
                                Return _ProposalSTR
                            End If
                        End Using
                    End Using
                End If
            Catch ex As Exception
                LogError(ex)
                Throw
            End Try
        End Get
    End Property

    Private ReadOnly Property PRoposalSTRID() As Integer
        Get
            Try
                Dim temp As String


                temp = Request.Params("PKProposalSTRID")
                If temp = "" Or Not IsNumeric(temp) Then
                    Throw New Exception("ProposalSTRID is invalid")
                Else
                    Return temp
                End If
                'cek status proposalstrworkflowhistory kalau = bcomplate=false dan baktifworkflow=true untuk eventype=2(task started) sudah 0 maka redirect

            Catch ex As Exception
                LogError(ex)
                Throw
            End Try
        End Get
    End Property
    Private _ProposalSTRWorkFlowHistory As AMLDAL.ProposalSTR.ProposalSTRWorkflowHistoryRow
    ''' <summary>
    ''' mendapatkan current workflow history yang harus di complate(cari taskstarted dan bcomplate=false) 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ProposalSTRWorkFlowHistory() As AMLDAL.ProposalSTR.ProposalSTRWorkflowHistoryRow
        Get
            Try
                If Not _ProposalSTRWorkFlowHistory Is Nothing Then
                    Return _ProposalSTRWorkFlowHistory
                Else
                    Using adapter As New AMLDAL.ProposalSTRTableAdapters.ProposalSTRWorkflowHistoryTableAdapter
                        Using otable As AMLDAL.ProposalSTR.ProposalSTRWorkflowHistoryDataTable = adapter.GetProposalSTRWorkflowHistory(EnProposalEventType.TaskStarted, Me.PRoposalSTRID, False, "%" & Sahassa.AML.Commonly.SessionUserId & "%")
                            If otable.Rows.Count > 0 Then
                                _ProposalSTRWorkFlowHistory = otable.Rows(0)
                                Return _ProposalSTRWorkFlowHistory
                            Else
                                _ProposalSTRWorkFlowHistory = Nothing
                                Return _ProposalSTRWorkFlowHistory
                            End If
                        End Using
                    End Using
                End If

            Catch ex As Exception
                LogError(ex)
                Throw
            End Try
        End Get
    End Property

  
 
 
  
    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImagePrint.Click
        'Response.Redirect("TestPrint.aspx", False)
        Response.Redirect("PrintProposalSTR.aspx?PKProposalSTRID=" & Me.PRoposalSTRID, False)
    End Sub

    ''' <summary>
    ''' load data  proposalstr
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub LoadData()
        Try
            If Not Me.ProposalSTR Is Nothing Then
                If Not ProposalSTR.IsKasusPolisiNull Then
                    Me.TextKasusPosisi.Text = ProposalSTR.KasusPolisi
                End If
                If Not ProposalSTR.IsIndicatorMencurigakanNull Then
                    Me.TextIndikatorMencurigakan.Text = ProposalSTR.IndicatorMencurigakan
                End If
                If Not ProposalSTR.IsUnsurTKMNull Then
                    Me.TextUnsurTKM.Text = ProposalSTR.UnsurTKM
                End If
                If Not ProposalSTR.IsLainLainNull Then
                    Me.TextLainLain.Text = Me.ProposalSTR.LainLain
                End If
                If Not ProposalSTR.IsKesimpulanNull Then
                    Me.TextKesimpulan.Text = Me.ProposalSTR.Kesimpulan
                End If
            End If
            Using adapter As New AMLDAL.ProposalSTRTableAdapters.ProposalSTRWorkFlowHistTableAdapter
                GridView1.DataSource = adapter.GetProposalSTRWorkflowHistoryByFKProposalSTRID(Me.PRoposalSTRID)
                GridView1.DataBind()
            End Using
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                LoadData()
            End If

            Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

            End Using
        Catch ex As Exception
            LogError(ex)
            CValPageError.IsValid = False
            CValPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImageButtonBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonBack.Click
        Response.Redirect("ProposalSTRView.aspx", False)
    End Sub
End Class
