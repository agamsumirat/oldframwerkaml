﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="CDDLNPQuestionAdd.aspx.vb" Inherits="CDDLNPQuestionAdd" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">

    <script type="text/javascript" language="javascript">
    function OpenDialogCDDQuestion() 
        {
            window.showModalDialog("PopupCDDQuestion.aspx","#1","dialogHeight: 650px; dialogWidth: 650px; dialogTop: 190px; dialogLeft: 220px; edge: Raised; center: Yes; help: No; resizable: No; status: No; scrollbar: Yes;")
        }
    </script>

    <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
        height="72" style="border-top-style: none; border-right-style: none; border-left-style: none;
        border-bottom-style: none" width="100%">
        <tr>
            <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>
                    <img height="17" src="Images/dot_title.gif" width="17" />
                    <asp:Label ID="lblHeader" runat="server" Text="EDD Question - Add"></asp:Label>&nbsp;
                    <hr />
                </strong>
            </td>
        </tr>
    </table>
    <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
        border="2" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none">
        <tr class="formText">
            <td bgcolor="#ffffff" width="5px">
                &nbsp;</td>
            <td bgcolor="#ffffff" width="150px">
                <strong>Question</strong></td>
            <td bgcolor="#ffffff" colspan="2">
                <CKEditor:CKEditorControl ID="txtQuestion" runat="server" MaxLength="255" Toolbar="Basic"></CKEditor:CKEditorControl></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" width="5px">
                &nbsp;</td>
            <td bgcolor="#ffffff" width="150px">
                <strong>
                Question Type</strong></td>
            <td bgcolor="#ffffff" colspan="2">
                <asp:DropDownList ID="ddlQuestionType" runat="server" CssClass="combobox" AutoPostBack="True">
                </asp:DropDownList></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" width="5px">
                &nbsp;</td>
            <td bgcolor="#ffffff" width="150px">
                <strong>
                Is Required</strong></td>
            <td bgcolor="#ffffff" colspan="2">
                <asp:CheckBox ID="chkIsRequired" runat="server" /></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" width="5">
            </td>
            <td bgcolor="#ffffff" width="150">
                <strong>Question Sequence</strong></td>
            <td bgcolor="#ffffff" colspan="2">
                <asp:DropDownList ID="ddlSequence" runat="server" CssClass="combobox">
                </asp:DropDownList></td>
        </tr>
        <tr class="formText" id="RowQuestionDetail" runat="server" visible="false">
            <td bgcolor="#ffffff" width="5px">
                &nbsp;</td>
            <td bgcolor="#ffffff" colspan="3">
                <table width="100%">
                    <tr>
                        <td colspan="2" style="height: 15px">
                            <strong>Question Detail</strong>
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td width="20%">
                            Question Detail</td>
                        <td>
                            <asp:TextBox ID="txtQuestionDetail" runat="server" CssClass="textbox" Width="200px"></asp:TextBox></td>
                    </tr>
                    <tr runat="Server">
                        <td>
                            Validation</td>
                        <td>
                            <asp:DropDownList ID="ddlValidation" runat="server" CssClass="combobox">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:ImageButton ID="ImageAdd" runat="server" CausesValidation="True" ImageUrl="~/Images/Button/Add.gif" /><asp:ImageButton
                                ID="ImageEdit" runat="server" ImageUrl="~/Images/Button/Edit.gif" Visible="False" />&nbsp;<asp:ImageButton
                                    ID="ImageCancelEdit" runat="server" ImageUrl="~/Images/Button/Cancel.gif" Visible="False" /></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:DataGrid ID="GridViewCDDQuestionDetail" runat="server" AutoGenerateColumns="False"
                                Font-Size="XX-Small" BackColor="White" CellPadding="4" Width="100%" GridLines="Vertical"
                                BorderColor="#DEDFDE" ForeColor="Black">
                                <FooterStyle BackColor="#CCCC99"></FooterStyle>
                                <AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
                                <ItemStyle BackColor="#F7F7DE"></ItemStyle>
                                <HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#6B696B"></HeaderStyle>
                                <Columns>
                                    <asp:BoundColumn HeaderText="Question Detail" DataField="QuestionDetail" SortExpression="QuestionDetail  asc">
                                        <ItemStyle Width="35%" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="Is Need Validation" DataField="IsNeedValidation" SortExpression="IsNeedValidation  asc" Visible="false">
                                        <ItemStyle Width="15%" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="FK_CDD_VAlidation_ID" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="Validation">
                                        <ItemStyle Width="40%" />
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LnkEdit" runat="server" CausesValidation="false" CommandName="Edit"
                                                Text="Edit"></asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderStyle Width="5%" />
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LnkDelete" runat="server" CausesValidation="false" CommandName="Delete"
                                                Text="Delete"></asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderStyle Width="5%" />
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr runat="server" class="formText">
            <td bgcolor="#ffffff" width="5px">
                &nbsp;</td>
            <td bgcolor="#ffffff" width="150px">
                <strong>
                Parent Question</strong></td>
            <td bgcolor="#ffffff" colspan="2">
                <asp:Label ID="lblParentQuestion" runat="server"></asp:Label>
                <asp:HiddenField ID="hfParentQuestionID" runat="server" />
                <br />
                <asp:ImageButton ID="ImageBrowse" runat="server" CausesValidation="True" ImageUrl="~/Images/Button/Browse.gif"
                    OnClientClick="javascript:OpenDialogCDDQuestion();" />
                <asp:ImageButton ID="ImageRemove" runat="server" CausesValidation="True" ImageUrl="~/Images/Button/Remove.gif" /></td>
            
        </tr>
        <tr class="formText" bgcolor="#dddddd" height="30">
            <td style="width: 5px">
                <img height="15" src="images/arrow.gif" width="15"></td>
            <td colspan="5" style="height: 9px">
                <table cellspacing="0" cellpadding="3" border="0">
                    <tr>
                        <td>
                            <ajax:AjaxPanel ID="AjaxPanel7" runat="server">
                                <asp:ImageButton ID="ImageSave" runat="server" CausesValidation="True" ImageUrl="~/Images/Button/Save.gif">
                                </asp:ImageButton></ajax:AjaxPanel></td>
                        <td>
                            <ajax:AjaxPanel ID="AjaxPanel8" runat="server">
                                <asp:ImageButton ID="ImageCancel" runat="server" ImageUrl="~/Images/Button/Cancel.gif">
                                </asp:ImageButton></ajax:AjaxPanel></td>
                    </tr>
                </table>
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
        </tr>
    </table>
</asp:Content>
