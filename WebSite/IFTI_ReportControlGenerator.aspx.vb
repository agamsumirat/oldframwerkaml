Imports amlbll
Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports SahassaNettier.Services
Imports Sahassa.AML.Commonly

Partial Class IFTI_ReportControlGenerator
    Inherits Parent




    Sub LoadTransactionType()
        Using objTransactiontype As TList(Of IFTI_Type) = IFTIReportGeneratorBLL.GetTlistIFTI_Type("", "", 0, Integer.MaxValue, 0)
            rblTransactionType.AppendDataBoundItems = True
            rblTransactionType.DataSource = objTransactiontype
            rblTransactionType.DataTextField = IFTI_TypeColumn.IFTIType.ToString
            rblTransactionType.DataValueField = IFTI_TypeColumn.PK_IFTI_Type_ID.ToString
            rblTransactionType.DataBind()

            rblTransactionType.Items.Add(New ListItem("All", 5))
        End Using
    End Sub


    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try



            'Using Transcope As New Transactions.TransactionScope
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                '    Transcope.Complete()
            End Using


            If Not IsPostBack Then
                Me.popUpStartDate.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.textDateFrom.ClientID & "'), 'dd-mmm-yyyy')")
                Me.popUpStartDate.Style.Add("display", "")
                Me.popUpEndDate.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.textDateTo.ClientID & "'), 'dd-mmm-yyyy')")
                Me.popUpEndDate.Style.Add("display", "")
                LoadTransactionType()

            End If

        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    Public ReadOnly Property GetFocusID() As String
        Get
            Return Me.btnGenerate.ClientID
        End Get
    End Property


    Function IsDataValid() As Boolean
        Dim strErrorMessage As New StringBuilder

        If Not IsDateValid("dd-MMM-yyyy", textDateFrom.Text) Then
            strErrorMessage.Append("Date From Must in format dd-MMM-yyyy </br>")
        End If

        If Not IsDateValid("dd-MMM-yyyy", textDateTo.Text) Then
            strErrorMessage.Append("Date To Must in format dd-MMM-yyyy </br>")
        End If

        If IsDateValid("dd-MMM-yyyy", textDateFrom.Text) AndAlso IsDateValid("dd-MMM-yyyy", textDateTo.Text) Then
            If DateDiff(DateInterval.Day, ConvertToDate("dd-MMM-yyyy", textDateFrom.Text), ConvertToDate("dd-MMM-yyyy", textDateTo.Text)) < 0 Then
                strErrorMessage.Append("Date From Must Greater than Date To </br>")
            End If
        End If

        'todohendra  Add validasi Transaction Type harus di isi
        If rblTransactionType.SelectedValue = "" Then
            strErrorMessage.Append("Please Select at least one Transaction Type  </br>")
        End If


        'todohendra  Add Report Format harus di isi
        If rblReportFormat.SelectedValue = "" Then
            strErrorMessage.Append("Please Select at least one Report format  </br>")
        End If

        

        If strErrorMessage.ToString.Trim.Length > 0 Then
            Throw New Exception(strErrorMessage.ToString)
        Else
            Return True
        End If

    End Function

    Protected Sub btnGenerate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGenerate.Click

        Dim dirPath As String = Server.MapPath(Request.ServerVariables("PATH_INFO")).Substring(0, Server.MapPath(Request.ServerVariables("PATH_INFO")).LastIndexOf("\") + 1) & "FolderExport\"
        Dim dirPathTemplate As String = Server.MapPath(Request.ServerVariables("PATH_INFO")).Substring(0, Server.MapPath(Request.ServerVariables("PATH_INFO")).LastIndexOf("\") + 1) & "FolderTemplate\"
        Dim FileReturn As String = ""
        Try
            If IsDataValid() Then
                dirPath = dirPath & Sahassa.AML.Commonly.SessionUserId & "\"
                If Not IO.Directory.Exists(dirPath) Then
                    IO.Directory.CreateDirectory(dirPath)
                End If
                FileReturn = IFTIReportGeneratorBLL.GenerateIFTIReport(ConvertToDate("dd-MMM-yyyy", textDateFrom.Text), ConvertToDate("dd-MMM-yyyy", textDateTo.Text), rblTransactionType.SelectedValue, rblReportFormat.SelectedValue, dirPath, dirPathTemplate)



                Response.Clear()
                Response.ClearHeaders()
                Response.AddHeader("content-disposition", "attachment;filename=" & IO.Path.GetFileName(FileReturn))
                Response.Charset = ""
                Response.AddHeader("cache-control", "max-age=0")
                Me.EnableViewState = False
                Response.ContentType = "application/octet-stream"
                Response.BinaryWrite(IO.File.ReadAllBytes(FileReturn))
                Response.End()

            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
End Class
