#Region "Imports..."
Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports System.Collections.Generic
Imports amlbll
Imports amlbll.ValidateBLL
Imports amlbll.DataType
Imports Sahassa.AML.Commonly
#End Region
Partial Class IFTI_Edit_Swift_Incoming
    Inherits Parent
    Private BindGridFromExcel As Boolean = False
#Region "properties..."
    ReadOnly Property getIFTIPK() As Integer
        Get
            If Session("IFTIEdit.IFTIPK") = Nothing Then
                Session("IFTIEdit.IFTIPK") = CInt(Request.Params("ID"))
            End If
            Return Session("IFTIEdit.IFTIPK")
        End Get
    End Property
    'Private Property SetnGetSenderAccount() As String
    '    Get
    '        Return IIf(Session("IFTIEdit.SenderAccount") Is Nothing, "", Session("IFTIEdit.SenderAccount"))
    '    End Get
    '    Set(ByVal Value As String)
    '        Session("IFTIEdit.SenderAccount") = Value
    '    End Set
    'End Property
    Public Property getIFTIBeneficiaryTempPK() As Integer
        Get
            Return IIf(Session("IFTIEdit.IFTIBeneficiaryTempPK") Is Nothing, Nothing, Session("IFTIEdit.IFTIBeneficiaryTempPK"))
        End Get
        Set(ByVal value As Integer)
            Session("IFTIEdit.IFTIBeneficiaryTempPK") = value
        End Set

    End Property
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("IFTIViewSelected") Is Nothing, New ArrayList, Session("IFTIViewSelected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("IFTIViewSelected") = value
        End Set
    End Property
    Private Property SetnGetSenderAccount() As String
        Get
            Return IIf(Session("IFTIEdit.SenderAccount") Is Nothing, "", Session("IFTIEdit.SenderAccount"))
        End Get
        Set(ByVal Value As String)
            Session("IFTIEdit.SenderAccount") = Value
        End Set
    End Property
    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("IFTIEditSort") Is Nothing, "Pk_IFTI_Id  asc", Session("IFTIEditSort"))
        End Get
        Set(ByVal Value As String)
            Session("IFTIEditSort") = Value
        End Set
    End Property

    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("IFTIEditCurrentPage") Is Nothing, 0, Session("IFTIEditCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("IFTIEditCurrentPage") = Value
        End Set
    End Property

    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property

    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("IFTIEditRowTotal") Is Nothing, 0, Session("IFTIEditRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("IFTIEditRowTotal") = Value
        End Set
    End Property
    'ReadOnly Property getSenderAccount() As Integer
    '    Get
    '        If Session("IFTIEdit.SenderAccount") = Nothing Then
    '            Session("IFTIEdit.SenderAccount") = CInt(Request.Params("ID"))
    '        End If
    '        Return Session("IFTIEdit.SenderAccount")
    '    End Get
    'End Property
    Private Property SetnGetCurrentPageReceiver() As Integer
        Get
            Dim result As Integer = 0
            If Not (Session("IFTIEdit.CurrentPageReceiver") Is Nothing) Then
                result = CType(Session("IFTIEdit.CurrentPageReceiver"), Integer)
            End If

            Return result
        End Get
        Set(ByVal Value As Integer)
            Session("IFTIEdit.CurrentPageReceiver") = Value
        End Set
    End Property
    Private Property ReceiverDataTable() As Data.DataTable
        Get
            Dim dt As Data.DataTable = DataRepository.IFTI_BeneficiaryProvider.GetPaged(IFTI_BeneficiaryColumn.FK_IFTI_ID.ToString & " = " & getIFTIPK, "", 0, Integer.MaxValue, 0).ToDataSet(False).Tables(0)
            dt = CType(IIf(Session("IFTIEdit.Receiver") Is Nothing, dt, Session("IFTIEdit.Receiver")), Data.DataTable)
            Session("IFTIEdit.Receiver") = dt
            Return dt
        End Get
        Set(ByVal value As Data.DataTable)
            Session("IFTIEdit.Receiver") = value
        End Set
    End Property
    'ReadOnly Property getSenderAccount() As Integer
    '    Get
    '        If Session("IFTIEdit.SenderAccount") = Nothing Then
    '            Session("IFTIEdit.SenderAccount") = CInt(Request.Params("ID"))
    '        End If
    '        Return Session("IFTIEdit.SenderAccount")
    '    End Get
    'End Property
#End Region
#Region "Validation"
#Region "Validation Sender"
    Sub ValidasiControl()
        If ObjectAntiNull(SwiftInUmum_TanggalLaporan.Text) = False Then Throw New Exception("Tanggal Laporan harus diisi  ")
        If Me.RbSwiftInUmum_JenisLaporan.SelectedValue <> "1" And ObjectAntiNull(SwiftInUmum_LtdnKoreksi.Text) = False Then Throw New Exception("No. LTDLN Koreksi harus diisi  ")
        If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", SwiftInUmum_TanggalLaporan.Text) = False Then
            Throw New Exception("Tanggal Laporan tidak valid")
        End If
        If ObjectAntiNull(SwiftInUmum_NamaPJKBank.Text) = False Then Throw New Exception("Nama PJK Bank Pelapor harus diisi  ")
        If ObjectAntiNull(SwiftInUmum_NamaPJKBank.Text) = True And SwiftInUmum_NamaPJKBank.Text.Length < 3 Then Throw New Exception("Nama PJK Bank Pelapor harus diisi lebih dari 2 karakter! ")
        If ObjectAntiNull(SwiftInUmum_NamaPejabatPJKBank.Text) = False Then Throw New Exception("Nama Pejabat PJK Bank Pelapor harus diisi  ")
        If ObjectAntiNull(SwiftInUmum_NamaPejabatPJKBank.Text) = True And SwiftInUmum_NamaPejabatPJKBank.Text.Length < 3 Then Throw New Exception("Nama Pejabat PJK Bank Pelapor harus diisi lebih dari 2 karakter!")
        If RbSwiftInUmum_JenisLaporan.SelectedIndex = -1 Then Throw New Exception("Jenis Laporan harus dipilih  ")
    End Sub
    Sub validasiSenderIndividu()
        'senderIndividu
        'parameter value

        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
        Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
        Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)


        If rbrbSwiftIn_TipePengirim.SelectedIndex = -1 Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Tipe Pengirim harus diisi  ")
        If rbSwiftIn_TipeNasabah.SelectedIndex = -1 Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Tipe Nasabah harus diisi  ")
        If ObjectAntiNull(txtSwiftInPengirim_rekening.Text) = False And ObjectAntiNull(txtSwiftInPengirimNasabah_IND_alamatIden.Text) = False Then Throw New Exception(" Identitas Pengirim Nasabah Perorangan: No Rekening dan alamat setidaknya wajib diisi salah satu!  ")
        If ObjectAntiNull(txtSwiftInPengirimNasabah_IND_nama.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Nama Lengkap harus diisi  ")
        If ObjectAntiNull(txtSwiftInPengirimNasabah_IND_nama.Text) = True And txtSwiftInPengirimNasabah_IND_nama.Text.Length < 3 Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Nama Lengkap harus diisi lebih dari 2 karakter! ")
        If ObjectAntiNull(txtSwiftInPengirimNasabah_IND_TanggalLahir.Text) = True Then
            If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", txtSwiftInPengirimNasabah_IND_TanggalLahir.Text) = False Then
                Throw New Exception("Identitas Pengirim Nasabah Perorangan: Tanggal Lahir tidak valid")
            End If
        End If
        If RbSwiftInPengirimNasabah_IND_Warganegara.SelectedValue = "2" Then
            If ObjectAntiNull(txtSwiftInPengirimNasabah_IND_negara.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Negara Kewarganegaraan harus diisi  ")
            If Me.hfSwiftInPengirimNasabah_IND_negara.Value = ParameterNegara.MsSystemParameter_Value Then
                If ObjectAntiNull(txtSwiftInPengirimNasabah_IND_negaraLain.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Negara Lain Kewarganegaraan harus diisi  ")
            End If

            ''If ObjectAntiNull(txtSwiftInPengirimNasabah_IND_negara.Text) = True And ObjectAntiNull(txtSwiftInPengirimNasabah_IND_negaraLain.Text) = True Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Negara Kewarganegaraan harus diisi salah satu saja  ")
        End If
        'If ObjectAntiNull(txtSwiftInPengirimNasabah_IND_nama.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Nama Lengkap harus diisi  ")
        If ObjectAntiNull(txtSwiftInPengirimNasabah_IND_negaraIden.Text) = False And ObjectAntiNull(txtSwiftInPengirimNasabah_IND_negaraLainIden.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Negara harus diisi  ")
        If ObjectAntiNull(txtSwiftInPengirimNasabah_IND_negaraLainIden.Text) = True And txtSwiftInPengirimNasabah_IND_negaraLainIden.Text.Length < 3 Then
            Throw New Exception("Identitas Pengirim Nasabah Perorangan: Negara Lain harus diisi minimal 3 karakter!  ")
        End If

        If ObjectAntiNull(txtSwiftInPengirimNasabah_IND_NoTelp.Text) = True And IsPhoneNumber(txtSwiftInPengirimNasabah_IND_NoTelp.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: No telp tidak valid!  ")

        If txtSwiftInPengirimNasabah_IND_NomorIdentitas.Text <> "" Then
            If Me.cbo_SwiftInpengirimNas_Ind_jenisidentitas.SelectedIndex > 0 Then
                If Me.cbo_SwiftInpengirimNas_Ind_jenisidentitas.SelectedValue = 1 Or Me.cbo_SwiftInpengirimNas_Ind_jenisidentitas.SelectedValue = 2 Then
                    If Not Regex.Match(txtSwiftInPengirimNasabah_IND_NomorIdentitas.Text.Trim(), "^[0-9]*$").Success Then
                        Throw New Exception("Nomor Identitas hanya boleh menggunakan angka!")
                    End If
                Else
                    If Not Regex.Match(txtSwiftInPengirimNasabah_IND_NomorIdentitas.Text.Trim(), "^[a-zA-Z0-9]*$").Success Then
                        Throw New Exception("Nomor Identitas hanya boleh menggunakan huruf dan angka!")
                    End If
                End If

            Else
                Throw New Exception("Jenis Identitas hanya Harus diisi!")
            End If
        End If
    End Sub
    Sub validasiSenderKorporasi()
        'senderKorp
        'parameter value

        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
        Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
        Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)


        If rbrbSwiftIn_TipePengirim.SelectedIndex = -1 Then Throw New Exception("Identitas Pengirim Nasabah Korporasi: Tipe Pengirim harus diisi  ")
        If rbSwiftIn_TipeNasabah.SelectedIndex = -1 Then Throw New Exception("Identitas Pengirim Nasabah Korporasi: Tipe Nasabah harus diisi  ")

        If ObjectAntiNull(txtSwiftInPengirim_rekening.Text) = False And ObjectAntiNull(Me.txtSwiftInPengirimNasabah_Korp_AlamatKorp.Text) = False Then Throw New Exception(" Identitas Pengirim Nasabah Korporasi: No Rekening harus diisi apabila alamat tidak diisi!  ")
        If ObjectAntiNull(txtSwiftInPengirimNasabah_Korp_namaKorp.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah Korporasi: Nama Korporasi harus diisi  ")
        If ObjectAntiNull(txtSwiftInPengirimNasabah_Korp_namaKorp.Text) = True And txtSwiftInPengirimNasabah_Korp_namaKorp.Text.Length < 3 Then Throw New Exception("Identitas Pengirim Nasabah Korporasi: Nama Korporasi harus diisi lebih dari 3 karakter! ")
        If ObjectAntiNull(txtSwiftInPengirimNasabah_Korp_NegaraKorp.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah Korporasi: Negara harus diisi  ")
        If Me.hfSwiftInPengirimNasabah_Korp_NegaraKorp.Value = ParameterNegara.MsSystemParameter_Value Then
            If ObjectAntiNull(txtSwiftInPengirimNasabah_IND_negaraLain.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah Korporasi: Negara Lain harus diisi  ")
        End If

        'If ObjectAntiNull(txtSwiftInPengirimNasabah_Korp_NegaraKorp.Text) = True And ObjectAntiNull(txtSwiftInPengirimNasabah_IND_negaraLain.Text) = True Then Throw New Exception("Identitas Pengirim Nasabah Korporasi: Negara harus diisi salah satu saja  ")
        If ObjectAntiNull(txtSwiftInPengirimNasabah_Korp_NoTelp.Text) = True And IsPhoneNumber(txtSwiftInPengirimNasabah_Korp_NoTelp.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah Korporasi: No telp tidak valid!  ")
    End Sub
    Sub validasiSenderNonNasabah()
        'senderNOnNasabah
        'parameter value

        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
        Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
        Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)

        'If ObjectAntiNull(txtSwiftInPengirim_rekening.Text) = False Or txtSwiftInPengirim_rekening.Text.Length Then Throw New Exception(" Identitas Pengirim Non Nasabah: No Rekening harus diisi  ")
        If rbrbSwiftIn_TipePengirim.SelectedIndex = -1 Then Throw New Exception("Identitas Pengirim Non Nasabah: Tipe Nasabah harus diisi  ")

        If ObjectAntiNull(txtSwiftInPengirimNonNasabah_Nama.Text) = False Then Throw New Exception("Identitas Pengirim Non Nasabah:Nama Lengkap harus diisi  ")
        If ObjectAntiNull(txtSwiftInPengirimNonNasabah_Nama.Text) = True And txtSwiftInPengirimNonNasabah_Nama.Text.Length < 3 Then Throw New Exception("Identitas Pengirim Non Nasabah:Nama Lengkap harus diisi lebih dari 2 karakter!  ")

        If ObjectAntiNull(Me.txtSwiftInPengirimNonNasabah_Alamat.Text) = True And Me.txtSwiftInPengirimNonNasabah_Alamat.Text.Length < 3 Then Throw New Exception("Identitas Pengirim Non Nasabah: Apabila diisi, alamat harus diisi  lebih dari 2 karakter!")
        If ObjectAntiNull(txtSwiftInPengirimNonNasabah_Negara.Text) = False Then Throw New Exception("Identitas Pengirim Non Nasabah: Negara harus diisi  ")
        If Me.hfSwiftInPengirimNonNasabah_Negara.Value = ParameterNegara.MsSystemParameter_Value Then
            If ObjectAntiNull(txtSwiftInPengirimNonNasabah_negaraLain.Text) = False Then Throw New Exception("Identitas Pengirim Non Nasabah: Negara Lain harus diisi  ")
        End If

        'If ObjectAntiNull(txtSwiftInPengirimNonNasabah_Negara.Text) = True And ObjectAntiNull(txtSwiftInPengirimNonNasabah_negaraLain.Text) = True Then Throw New Exception("Identitas Pengirim Non Nasabah: Negara harus diisi salah satu saja  ")
        If ObjectAntiNull(txtSwiftInPengirimNonNasabah_NoTelp.Text) = True And IsPhoneNumber(txtSwiftInPengirimNonNasabah_NoTelp.Text) = False Then Throw New Exception("Identitas Pengirim NonNasabah: No telp tidak valid!  ")
    End Sub
#End Region
#Region "Validation Receiver"
    Function IsValidDecimal(ByVal intPrecision As Integer, ByVal intScale As Integer, ByVal strDatatoValidate As String) As Boolean

        Return Regex.IsMatch(strDatatoValidate, "^[\d]{1," & intPrecision & "}(\.[\d]{1," & intScale & "})?$")

    End Function
    Private Function ValidateReceiver() As Boolean
        Dim result As Boolean = True
        If cboSwiftInPenerima_TipePJKBank.SelectedIndex = -1 Then Throw New Exception(" Identitas Penerima Nasabah Perorangan: TIpe PJK Bank harus diisi  ")
        If RbSwiftInPenerima_TipeNasabah.SelectedIndex = -1 Then Throw New Exception("Identitas Penerima Nasabah Perorangan: Tipe Nasabah harus diisi  ")
        'parameter value

        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
        Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
        Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)
        Dim ParameterBidangUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BidangUsaha)
        If cboSwiftInPenerima_TipePJKBank.SelectedValue = 1 Then
            If RbSwiftInPenerima_TipePenerima.SelectedIndex = -1 Then Throw New Exception("Identitas Penerima Nasabah : Tipe Pengirim harus diisi  ")
            If RbSwiftInPenerima_TipePenerima.SelectedValue = 1 Then
                If RbSwiftInPenerima_TipeNasabah.SelectedValue = 1 Then
                    'penerimaIndividu
                    If ObjectAntiNull(txtSwiftInPenerimaNasabah_Rekening.Text) = False Or txtSwiftInPenerimaNasabah_Rekening.Text.Length < 3 Then
                        result = False
                        Throw New Exception(" Identitas Penerima Nasabah Perorangan: No Rekening harus diisi dengan minimal 3 karakter! ")
                    End If
                    If ObjectAntiNull(txtSwiftInPenerimaNasabah_IND_nama.Text) = False Or txtSwiftInPenerimaNasabah_IND_nama.Text.Length < 3 Then
                        result = False
                        Throw New Exception(" Identitas Penerima Nasabah Perorangan: Nama Lengkap harus diisi dengan minimal 3 karakter! ")
                    End If
                    If ObjectAntiNull(txtSwiftInPenerimaNasabah_IND_TanggalLahir.Text) = False Then Throw New Exception("Identitas Penerima Nasabah Perorangan: Tanggal Lahir harus diisi!")
                    If ObjectAntiNull(txtSwiftInPenerimaNasabah_IND_TanggalLahir.Text) = True Then
                        If IsDateValid("dd-MMM-yyyy", txtSwiftInPenerimaNasabah_IND_TanggalLahir.Text) = False Then

                            Throw New Exception("Identitas Penerima Nasabah Perorangan: Tanggal Lahir tidak valid")
                        End If
                    End If
                    If RbSwiftInPenerimaNasabah_IND_Kewarganegaraan.SelectedValue = "2" Then
                        If ObjectAntiNull(txtSwiftInPenerimaNasabah_IND_negara.Text) = False Then
                            result = False
                            Throw New Exception("Identitas Penerima Nasabah Perorangan: Negara Kewarganegaraan harus diisi  ")
                        End If
                        If Me.hfSwiftInPenerimaNasabah_IND_negara.Value = ParameterNegara.MsSystemParameter_Value Then
                            If ObjectAntiNull(txtSwiftInPenerimaNasabah_IND_negaralain.Text) = False Then
                                result = False
                                Throw New Exception("Identitas Penerima Nasabah Perorangan: Negara Lain Kewarganegaraan harus diisi  ")
                            End If
                        End If
                        'If ObjectAntiNull(txtSwiftInPenerimaNasabah_IND_negara.Text) = True And ObjectAntiNull(txtSwiftInPenerimaNasabah_IND_negaralain.Text) = True Then
                        '    result = False
                        '    Throw New Exception("Identitas Penerima Nasabah Perorangan: Negara Kewarganegaraan harus diisi salah satu saja  ")
                        'End If
                    End If
                    If ObjectAntiNull(txtSwiftInPenerimaNasabah_IND_pekerjaan.Text) = False Then
                        result = False
                        Throw New Exception(" Identitas Penerima Nasabah Perorangan: Pekerjaan harus diisi  ")
                    End If
                    If Me.hfSwiftInIdenPenerimaNas_Ind_pekerjaan.Value = ParameterPekerjaan.MsSystemParameter_Value Then
                        If ObjectAntiNull(TxtSwiftInIdenPenerimaNas_Ind_Pekerjaanlain.Text) = False Then
                            result = False
                            Throw New Exception(" Identitas Penerima Nasabah Perorangan: Pekerjaan Lain harus diisi  ")
                        End If
                    End If
                    'If ObjectAntiNull(txtSwiftInPenerimaNasabah_IND_pekerjaan.Text) = True And ObjectAntiNull(TxtSwiftInIdenPenerimaNas_Ind_Pekerjaanlain.Text) = True Then
                    '    result = False
                    '    Throw New Exception("Identitas Penerima Nasabah Perorangan: Pekerjaan harus diisi salah satu saja  ")
                    'End If

                    'If ObjectAntiNull(TxtSwiftInIdenPenerimaNas_Ind_Kota.Text) = True And ObjectAntiNull(TxtSwiftInIdenPenerimaNas_Ind_kotalain.Text) = True Then
                    '    result = False
                    '    Throw New Exception("Identitas Penerima Nasabah Perorangan: Kota/Kab harus diisi salah satu saja  ")
                    'End If
                    'If ObjectAntiNull(TxtSwiftInIdenPenerimaNas_Ind_provinsi.Text) = True And ObjectAntiNull(TxtSwiftInIdenPenerimaNas_Ind_provinsiLain.Text) = True Then
                    '    result = False
                    '    Throw New Exception("Identitas Penerima Nasabah Perorangan: Provinsi harus diisi salah satu saja  ")
                    'End If
                    If ObjectAntiNull(Me.TxtSwiftInIdenPenerimaNas_Ind_Alamat.Text) = True And TxtSwiftInIdenPenerimaNas_Ind_Alamat.Text.Length < 3 Then
                        result = False
                        Throw New Exception(" Identitas Penerima Nasabah Perorangan: Alamat Domisili harus diisi minimal 3 karakter! ")
                    End If
                    If ObjectAntiNull(TxtSwiftInIdenPenerimaNas_Ind_alamatIdentitas.Text) = False Or TxtSwiftInIdenPenerimaNas_Ind_alamatIdentitas.Text.Length < 3 Then
                        result = False
                        Throw New Exception(" Identitas Penerima Nasabah Perorangan: Alamat Identitas harus diisi minimal 3 karakter! ")
                    End If
                    If ObjectAntiNull(TxtSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Text) = False Then
                        result = False
                        Throw New Exception("Identitas Penerima Nasabah Perorangan:  Kota/Kab Identitas harus diisi  ")
                    End If
                    If Me.hfSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Value = ParameterKota.MsSystemParameter_Value Then
                        If ObjectAntiNull(TxtSwiftInIdenPenerimaNas_Ind_KotaLainIden.Text) = False Then
                            result = False
                            Throw New Exception("Identitas Penerima Nasabah Perorangan:  Kota/Kab Lain Identitas harus diisi  ")
                        End If
                    End If
                    'If ObjectAntiNull(TxtSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Text) = True And ObjectAntiNull(TxtSwiftInIdenPenerimaNas_Ind_KotaLainIden.Text) = True Then
                    '    result = False
                    '    Throw New Exception("Identitas Penerima Nasabah Perorangan: Kota/Kab harus diisi salah satu saja  ")
                    'End If
                    If ObjectAntiNull(TxtSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Text) = False Then
                        result = False
                        Throw New Exception(" Identitas Penerima Nasabah Perorangan: Provinsi Identitas harus diisi  ")
                    End If
                    If Me.hfSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Value = ParameterProvinsi.MsSystemParameter_Value Then
                        If ObjectAntiNull(TxtSwiftInIdenPenerimaNas_Ind_ProvinsilainIden.Text) = False Then
                            result = False
                            Throw New Exception(" Identitas Penerima Nasabah Perorangan: Provinsi Lain Identitas harus diisi  ")
                        End If
                    End If
                    'If ObjectAntiNull(TxtSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Text) = True And ObjectAntiNull(TxtSwiftInIdenPenerimaNas_Ind_ProvinsilainIden.Text) = True Then
                    '    result = False
                    '    Throw New Exception("Identitas Penerima Nasabah Perorangan: Provinsi harus diisi salah satu saja  ")
                    'End If
                    If cbo_SwiftInpenerimaNas_Ind_jenisidentitas.SelectedIndex = -1 Then
                        result = False
                        Throw New Exception(" Identitas Penerima Nasabah Perorangan: Jenis Dokumen identitas harus diisi  ")
                    End If
                    If ObjectAntiNull(TxtISwiftIndenPenerimaNas_Ind_noIdentitas.Text) = False Or TxtISwiftIndenPenerimaNas_Ind_noIdentitas.Text.Length < 3 Then
                        result = False
                        Throw New Exception(" Identitas Penerima Nasabah Perorangan: Nomor Identitas harus diisi angka lebih dari 2 digit  ")
                    End If
                    If TxtISwiftIndenPenerimaNas_Ind_noIdentitas.Text <> "" Then
                        If Me.cbo_SwiftInpenerimaNas_Ind_jenisidentitas.SelectedValue = 1 Or Me.cbo_SwiftInpenerimaNas_Ind_jenisidentitas.SelectedValue = 2 Then
                            If Not Regex.Match(TxtISwiftIndenPenerimaNas_Ind_noIdentitas.Text.Trim(), "^[0-9]*$").Success Then
                                Throw New Exception("Nomor Identitas hanya boleh menggunakan angka!")
                            End If
                        Else
                            If Not Regex.Match(TxtISwiftIndenPenerimaNas_Ind_noIdentitas.Text.Trim(), "^[a-zA-Z0-9]*$").Success Then
                                Throw New Exception("Nomor Identitas hanya boleh menggunakan huruf dan angka!")
                            End If
                        End If

                    End If
                    'If ObjectAntiNull(txtSwiftInIdenPenerimaNas_IND_NilaiTransaksiKeuangan.Text) = False Then
                    '    result = False
                    '    Throw New Exception(" Identitas Penerima Nasabah Perorangan: Nilai Transaksi Keuangan harus diisi  ")
                    'End If
                    If ReceiverDataTable.Rows.Count > 1 Then
                        If ObjectAntiNull(txtSwiftInIdenPenerimaNas_IND_NilaiTransaksiKeuangan.Text) = False Then
                            result = False
                            Throw New Exception("Identitas Penerima Nasabah Perorangan: Nilai Transaksi Keuangan harus diisi apabila penerima lebih dari 1  ")
                        End If
                        If ObjectAntiNull(txtSwiftInIdenPenerimaNas_IND_NilaiTransaksiKeuangan.Text) = True Then
                            If Not IsValidDecimal(15, 2, txtSwiftInIdenPenerimaNas_IND_NilaiTransaksiKeuangan.Text) Then
                                result = False
                                Throw New Exception("Identitas Penerima Nasabah Perorangan: Nilai Transaksi Keuangan tidak valid!  ")
                            End If
                        End If
                    End If

                Else

                    'pennerimaKorporasi
                    'If cboSwiftInPenerimaNonNasabah_TipePJKBank.SelectedIndex = -1 Then Throw New Exception("Identitas Penerima Nasabah Korporasi: Tipe PJK Bank harus diisi  ")
                    'If RbSwiftInPenerimaNonNasabah_TipePenerima.SelectedIndex = -1 Then Throw New Exception("Identitas Penerima Nasabah Korporasi:  Tipe Pengirim harus diisi  ")
                    'If RbSwiftInPenerimaNonNasabah_TipeNasabah.SelectedIndex = -1 Then Throw New Exception("Identitas Penerima Nasabah Korporasi: Tipe Nasabah harus diisi  ")
                    If ObjectAntiNull(txtSwiftInPenerimaNasabah_Rekening.Text) = False Then
                        result = False
                        Throw New Exception("Identitas Penerima Nasabah Korporasi:  No Rekening harus diisi  ")
                    End If

                    If ObjectAntiNull(txtSwiftInPenerimaNasabah_Korp_namaKorp.Text) = False Or txtSwiftInPenerimaNasabah_Korp_namaKorp.Text.Length < 3 Then
                        result = False
                        Throw New Exception(" Identitas Penerima Nasabah Korporasi: Nama Korporasi harus diisi lebih dari 2 karakter! ")
                    End If
                    If ObjectAntiNull(txtSwiftInPenerimaNasabah_Korp_BidangUsahaKorp.Text) = False Then
                        result = False
                        Throw New Exception(" Identitas Penerima Nasabah Korporasi:  Bidang Usaha Korporasi harus diisi  ")
                    End If
                    If Me.hfSwiftInPenerimaNasabah_Korp_BidangUsahaKorp.Value = ParameterBidangUsaha.MsSystemParameter_Value Then
                        If ObjectAntiNull(txtSwiftInPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Text) = False Then
                            result = False
                            Throw New Exception(" Identitas Penerima Nasabah Korporasi:  Bidang Usaha Lain Korporasi harus diisi  ")
                        End If
                    End If
                    'If ObjectAntiNull(txtSwiftInPenerimaNasabah_Korp_BidangUsahaKorp.Text) = True And ObjectAntiNull(txtSwiftInPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Text) = True Then
                    '    result = False
                    '    Throw New Exception("Identitas Penerima Nasabah Korporasi: Bidang Usaha Korporasi harus diisi salah satu saja  ")
                    'End If
                    If ObjectAntiNull(Me.txtSwiftInPenerimaNasabah_Korp_AlamatKorp.Text) = False Or txtSwiftInPenerimaNasabah_Korp_AlamatKorp.Text.Length < 3 Then
                        result = False
                        Throw New Exception("Identitas Penerima Nasabah Korporasi: Alamat Identitas harus diisi lebih dari 2 karakter! ")
                    End If
                    If ObjectAntiNull(txtSwiftInPenerimaNasabah_Korp_Kotakab.Text) = False Then
                        result = False
                        Throw New Exception("Identitas Penerima Nasabah Korporasi: Kota/Kabupaten harus diisi  ")
                    End If
                    If Me.hfSwiftInPenerimaNasabah_Korp_kotakab.Value = ParameterKota.MsSystemParameter_Value Then
                        If ObjectAntiNull(txtSwiftInPenerimaNasabah_Korp_kotaLain.Text) = False Then
                            result = False
                            Throw New Exception("Identitas Penerima Nasabah Korporasi: Kota/Kabupaten Lain harus diisi  ")
                        End If
                    End If
                    'If ObjectAntiNull(txtSwiftInPenerimaNasabah_Korp_Kotakab.Text) = True And ObjectAntiNull(txtSwiftInPenerimaNasabah_Korp_kotaLain.Text) = True Then
                    '    result = False
                    '    Throw New Exception("Identitas Penerima Nasabah Korporasi: Kota/Kabupaten harus diisi salah satu saja  ")
                    'End If
                    If ObjectAntiNull(txtSwiftInPenerimaNasabah_Korp_propinsi.Text) = False Then
                        result = False
                        Throw New Exception("Identitas Penerima Nasabah Korporasi: Propinsi harus diisi  ")
                    End If
                    If Me.hfSwiftInPenerimaNasabah_Korp_propinsi.Value = ParameterProvinsi.MsSystemParameter_Value Then
                        If ObjectAntiNull(txtSwiftInPenerimaNasabah_Korp_propinsilain.Text) = False Then
                            result = False
                            Throw New Exception("Identitas Penerima Nasabah Korporasi: Propinsi Lain harus diisi  ")
                        End If
                    End If
                    'If ObjectAntiNull(txtSwiftInPenerimaNasabah_Korp_propinsi.Text) = True And ObjectAntiNull(txtSwiftInPenerimaNasabah_Korp_propinsilain.Text) = True Then
                    '    result = False
                    '    Throw New Exception("Identitas Penerima Nasabah Korporasi: Propinsi harus diisi salah satu saja  ")
                    'End If
                    'If ObjectAntiNull(txtSwiftInPenerimaNasabah_Korp_NilaiTransaksiKeuangan.Text) = False Then
                    '    result = False
                    '    Throw New Exception(" Identitas Penerima Nasabah Korporasi: Nilai Transaksi Keuangan harus diisi  ")
                    'End If
                    If ReceiverDataTable.Rows.Count > 1 Then
                        If ObjectAntiNull(txtSwiftInPenerimaNasabah_Korp_NilaiTransaksiKeuangan.Text) = False Then
                            result = False
                            Throw New Exception("Identitas Penerima Nasabah Korporasi: Nilai Transaksi Keuangan harus diisi apabila penerima lebih dari 1  ")
                        End If
                        If ObjectAntiNull(txtSwiftInPenerimaNasabah_Korp_NilaiTransaksiKeuangan.Text) = True Then
                            If Not IsValidDecimal(15, 2, txtSwiftInPenerimaNasabah_Korp_NilaiTransaksiKeuangan.Text) Then
                                result = False
                                Throw New Exception("Identitas Penerima Nasabah Korporasi: Nilai Transaksi Keuangan tidak valid!  ")
                            End If
                        End If
                    End If
                End If
            Else
                'penerimaNOnNasabah
                'If ObjectAntiNull(txtSwiftInPenerimaNasabah_Rekening.Text) = False Then
                '    result = False
                '    Throw New Exception("Identitas Penerima Non Nasabah : No Rekening harus diisi  ")
                'End If

                If ObjectAntiNull(TxtSwiftInPenerimaNonNasabah_nama.Text) = False Or TxtSwiftInPenerimaNonNasabah_nama.Text.Length < 3 Then
                    result = False
                    Throw New Exception("Identitas Penerima Non Nasabah : Nama Lengkap harus diisi lebih dari 2 karakter! ")
                End If
                If ObjectAntiNull(TxtSwiftInPenerimaNonNasabah_TanggalLahir.Text) = True Then

                    If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", TxtSwiftInPenerimaNonNasabah_TanggalLahir.Text) = False Then
                        Throw New Exception("Identitas Penerima Non Nasabah : Tanggal Lahir tidak valid")
                    End If
                End If
                If TxtSwiftInPenerimaNonNasabah_NomorIden.Text <> "" Then
                    If Me.CboSwiftInPenerimaNonNasabah_JenisDokumen.SelectedValue = 1 Or Me.CboSwiftInPenerimaNonNasabah_JenisDokumen.SelectedValue = 2 Then
                        If Not Regex.Match(TxtSwiftInPenerimaNonNasabah_NomorIden.Text.Trim(), "^[0-9]*$").Success Then
                            Throw New Exception("Nomor Identitas hanya boleh menggunakan angka!")
                        End If
                    Else
                        If Not Regex.Match(TxtSwiftInPenerimaNonNasabah_NomorIden.Text.Trim(), "^[a-zA-Z0-9]*$").Success Then
                            Throw New Exception("Nomor Identitas hanya boleh menggunakan huruf dan angka!")
                        End If
                    End If

                End If
                If ReceiverDataTable.Rows.Count > 1 Then
                    If ObjectAntiNull(TxtSwiftInPenerimaNonNasabah_NilaiTransaksiKeuangan.Text) = False Then
                        result = False
                        Throw New Exception("Identitas Penerima Non Nasabah : Nilai Transaksi Keuangan harus diisi apabila penerima lebih dari 1  ")
                    End If
                    If ObjectAntiNull(TxtSwiftInPenerimaNonNasabah_NilaiTransaksiKeuangan.Text) = True Then
                        If Not IsValidDecimal(15, 2, TxtSwiftInPenerimaNonNasabah_NilaiTransaksiKeuangan.Text) Then
                            result = False
                            Throw New Exception("Identitas Penerima Non Nasabah : Nilai Transaksi Keuangan tidak valid!  ")
                        End If
                    End If
                End If

            End If
        Else
            If RbSwiftInPenerima_TipeNasabah.SelectedValue = 1 Then
                If ObjectAntiNull(TxtSwiftInPenerimaPenerus_Rekening.Text) = False Or TxtSwiftInPenerimaPenerus_Rekening.Text.Length < 3 Then
                    result = False
                    Throw New Exception("Identitas Penerima Penerus Nasabah Perorangan: No Rekening harus diisi lebih dari 2 karakter! ")
                End If

                If ObjectAntiNull(TxtSwiftInPenerimaPenerus_Ind_namaBank.Text) = False Then
                    result = False
                    Throw New Exception("Identitas Penerima Penerus Nasabah Perorangan: Nama Bank harus diisi  ")
                End If
                If ObjectAntiNull(TxtSwiftInPenerimaPenerus_Ind_nama.Text) = False Then
                    result = False
                    Throw New Exception("Identitas Penerima Penerus Nasabah Perorangan: Nama Lengkap harus diisi  ")
                End If

                If RBSwiftInPenerimaPenerus_Ind_Kewarganegaraan.SelectedValue = "2" Then
                    If ObjectAntiNull(TxtSwiftInPenerimaPenerus_IND_negara.Text) = False Then
                        result = False
                        Throw New Exception("Identitas Penerima Penerus Nasabah Perorangan: Negara Kewarganegaraan harus diisi  ")
                    End If
                    If Me.hfSwiftInPenerimaPenerus_IND_negara.Value = ParameterNegara.MsSystemParameter_Value Then
                        If ObjectAntiNull(TxtSwiftInPenerimaPenerus_IND_negaralain.Text) = False Then
                            result = False
                            Throw New Exception("Identitas Penerima Penerus Nasabah Perorangan: Negara Kewarganegaraan harus diisi  ")
                        End If
                    End If
                    'If ObjectAntiNull(TxtSwiftInPenerimaPenerus_IND_negara.Text) = True And ObjectAntiNull(TxtSwiftInPenerimaPenerus_IND_negaralain.Text) = True Then
                    '    result = False
                    '    Throw New Exception("Identitas Penerima Penerus Nasabah Perorangan: Negara Kewarganegaraan harus diisi salah satu saja  ")
                    'End If
                End If
                'dom
                'If ObjectAntiNull(TxtSwiftInPenerimaPenerus_IND_kotaDom.Text) = True And ObjectAntiNull(TxtSwiftInPenerimaPenerus_IND_kotaLainDom.Text) = True Then
                '    result = False
                '    Throw New Exception("Identitas Penerima Penerus Nasabah Perorangan: Kota/Kab harus diisi salah satu saja  ")
                'End If
                'If ObjectAntiNull(TxtSwiftInPenerimaPenerus_IND_ProvDom.Text) = True And ObjectAntiNull(TxtSwiftInPenerimaPenerus_IND_ProvLainDom.Text) = True Then
                '    result = False
                '    Throw New Exception("Identitas Penerima Penerus Nasabah Perorangan: Provinsi harus diisi salah satu saja  ")
                'End If

                'If ObjectAntiNull(TxtSwiftInPenerimaPenerus_IND_pekerjaan.Text) = True And ObjectAntiNull(TxtSwiftInPenerimaPenerus_IND_pekerjaanLain.Text) = True Then
                '    result = False
                '    Throw New Exception("Identitas Penerima Penerus Nasabah Perorangan: Pekerjaan harus diisi salah satu saja  ")
                'End If
                'iden
                'If ObjectAntiNull(TxtSwiftInPenerimaPenerus_IND_kotaIden.Text) = True And ObjectAntiNull(TxtSwiftInPenerimaPenerus_IND_KotaLainIden.Text) = True Then
                '    result = False
                '    Throw New Exception("Identitas Penerima Penerus Nasabah Perorangan: Kota/Kab harus diisi salah satu saja  ")
                'End If
                'If ObjectAntiNull(TxtSwiftInPenerimaPenerus_IND_ProvIden.Text) = True And ObjectAntiNull(TxtSwiftInPenerimaPenerus_IND_ProvLainIden.Text) = True Then
                '    result = False
                '    Throw New Exception("Identitas Penerima Penerus Nasabah Perorangan: Provinsi harus diisi salah satu saja  ")
                'End If
                If TxtSwiftInPenerimaPenerus_IND_nomoridentitas.Text <> "" Then
                    If Me.CBoSwiftInPenerimaPenerus_IND_jenisIdentitas.SelectedValue = 1 Or Me.CBoSwiftInPenerimaPenerus_IND_jenisIdentitas.SelectedValue = 2 Then
                        If Not Regex.Match(TxtSwiftInPenerimaPenerus_IND_nomoridentitas.Text.Trim(), "^[0-9]*$").Success Then
                            Throw New Exception("Nomor Identitas hanya boleh menggunakan angka!")
                        End If
                    Else
                        If Not Regex.Match(TxtSwiftInPenerimaPenerus_IND_nomoridentitas.Text.Trim(), "^[a-zA-Z0-9]*$").Success Then
                            Throw New Exception("Nomor Identitas hanya boleh menggunakan huruf dan angka!")
                        End If
                    End If
                End If
                'If Not IsNumeric(TxtSwiftInPenerimaPenerus_IND_NIlaiTransaksi.Text) Then
                '    Throw New Exception("Identitas Penerima Penerus Nasabah Perorangan: Nilai Transaksi Keuangan tidak valid")
                'End If
                If ReceiverDataTable.Rows.Count > 1 Then
                    If ObjectAntiNull(TxtSwiftInPenerimaPenerus_IND_NIlaiTransaksi.Text) = False Then
                        result = False
                        Throw New Exception("Identitas Penerima Nasabah Perorangan : Nilai Transaksi Keuangan harus diisi apabila penerima lebih dari 1  ")
                    End If
                    If ObjectAntiNull(TxtSwiftInPenerimaPenerus_IND_NIlaiTransaksi.Text) = True Then
                        If Not IsValidDecimal(15, 2, TxtSwiftInPenerimaPenerus_IND_NIlaiTransaksi.Text) Then
                            result = False
                            Throw New Exception("Identitas Penerima Nasabah Perorangan : Nilai Transaksi Keuangan tidak valid!  ")
                        End If
                    End If
                End If
            Else
                If ObjectAntiNull(TxtSwiftInPenerimaPenerus_Rekening.Text) = False Then
                    result = False
                    Throw New Exception("Identitas Penerima Nasabah Korporasi : No Rekening harus diisi  ")
                End If

                If ObjectAntiNull(txtSwiftInPenerimaPenerus_Korp_namabank.Text) = False And ObjectAntiNull(TxtSwiftInPenerimaPenerus_Rekening.Text) = True Then
                    result = False
                    Throw New Exception("Identitas Penerima Nasabah Korporasi : Nama Bank harus diisi  ")
                End If
                If ObjectAntiNull(txtSwiftInPenerimaPenerus_Korp_NamaKorp.Text) = False Or txtSwiftInPenerimaPenerus_Korp_NamaKorp.Text.Length < 3 Then
                    result = False
                    Throw New Exception("Identitas Penerima Nasabah Korporasi : Nama Korporasi harus diisi lebih dari 2 karakter! ")
                End If

                'If ObjectAntiNull(txtSwiftInPenerimaPenerus_Korp_BidangUsahaKorp.Text) = True And ObjectAntiNull(txtSwiftInPenerimaPenerus_Korp_BidangUsahaKorpLainnya.Text) = True Then
                '    result = False
                '    Throw New Exception("Identitas Penerima Penerus Nasabah Korporasi: Bidang Usaha Korporasi harus diisi salah satu saja  ")
                'End If
                'If ObjectAntiNull(txtSwiftInPenerimaPenerus_Korp_kotakorp.Text) = True And ObjectAntiNull(txtSwiftInPenerimaPenerus_Korp_kotalainnyakorp.Text) = True Then
                '    result = False
                '    Throw New Exception("Identitas Penerima Penerus Nasabah Korporasi: Kota/Kabupaten harus diisi salah satu saja  ")
                'End If
                'If ObjectAntiNull(txtSwiftInPenerimaPenerus_Korp_ProvinsiKorp.Text) = True And ObjectAntiNull(txtSwiftInPenerimaPenerus_Korp_provinsiLainnyaKorp.Text) = True Then
                '    result = False
                '    Throw New Exception("Identitas Penerima Penerus Nasabah Korporasi: Propinsi harus diisi salah satu saja  ")
                'End If

                If ReceiverDataTable.Rows.Count > 1 Then
                    If ObjectAntiNull(txtSwiftInPenerimaPenerus_Korp_NilaiTransaksi.Text) = False Then
                        result = False
                        Throw New Exception("Identitas Penerima Nasabah Korporasi : Nilai Transaksi Keuangan harus diisi apabila penerima lebih dari 1  ")
                    End If
                    If ObjectAntiNull(txtSwiftInPenerimaPenerus_Korp_NilaiTransaksi.Text) = True Then
                        If Not IsValidDecimal(15, 2, txtSwiftInPenerimaPenerus_Korp_NilaiTransaksi.Text) Then
                            result = False
                            Throw New Exception("Identitas Penerima Nasabah Korporasi : Nilai Transaksi Keuangan tidak valid!  ")
                        End If
                    End If
                End If
            End If
        End If

        'If ObjectAntiNull(Transaksi_SwiftInSender0.Text) = True And Transaksi_SwiftInSender0.Text.Length < 3 Then
        '    result = False
        '    Throw New Exception("Identitas Penerima Penerus Nasabah Korporasi: Referensi pengirim harus diisi lebih dari 2 karakter!  ")
        'End If
        Return result
    End Function
    Private Function ValidateReceiverSaveAll() As Boolean
        Dim result As ArrayList
        Dim i As Integer
        Dim dt As Data.DataTable = ReceiverDataTable
        Dim rowCount As Integer = dt.Rows.Count
        Dim tipepjkbank As Integer
        Dim tipenasabah As Integer
        If (rowCount > 0) Then
            result = New ArrayList
        End If

        For i = 0 To rowCount - 1
            Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
            Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
            Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
            Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)
            Dim ParameterBidangUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BidangUsaha)
            If ObjectAntiNull(dt.Rows(i)("PJKBank_type")) = True Then
                tipepjkbank = dt.Rows(i)("PJKBank_type")

                If dt.Rows(i)("PJKBank_type") = 1 Then
                    If ObjectAntiNull(dt.Rows(i)("FK_IFTI_NasabahType_ID")) = True Then
                        tipenasabah = dt.Rows(i)("FK_IFTI_NasabahType_ID")
                        Select Case (tipenasabah)
                            Case 1
                                'penerimaIndividu
                                If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_INDV_NoRekening")) = False Or dt.Rows(i)("Beneficiary_Nasabah_INDV_NoRekening").Length < 3 Then
                                    'result = False
                                    Throw New Exception(" Identitas Penerima " & i + 1 & " Nasabah Perorangan: No Rekening harus diisi dengan minimal 3 karakter! ")
                                End If
                                If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_INDV_NamaLengkap")) = False Or dt.Rows(i)("Beneficiary_Nasabah_INDV_NamaLengkap").Length < 3 Then
                                    'result = False
                                    Throw New Exception(" Identitas Penerima " & i + 1 & " Nasabah Perorangan: Nama Lengkap harus diisi dengan minimal 3 karakter! ")
                                End If
                                If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_INDV_TanggalLahir")) = False Then
                                    Throw New Exception(" Identitas Penerima " & i + 1 & " Nasabah Perorangan: Tanggal lahir harus diisi! ")
                                End If
                                'If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_INDV_TanggalLahir")) = True Then
                                '    If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", dt.Rows(i)("Beneficiary_Nasabah_INDV_TanggalLahir")) = False Then
                                '        Throw New Exception("Identitas Penerima " & i + 1 & " Nasabah Perorangan: Tanggal Lahir tidak valid")
                                '    End If
                                'End If
                                If dt.Rows(i)("Beneficiary_Nasabah_INDV_KewargaNegaraan") = 2 Then
                                    If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_INDV_Negara")) = False Then
                                        'result = False
                                        Throw New Exception("Identitas Penerima " & i + 1 & " Nasabah Perorangan: Negara Kewarganegaraan harus diisi  ")
                                    End If
                                    If dt.Rows(i)("Beneficiary_Nasabah_INDV_Negara") = ParameterNegara.MsSystemParameter_Value Then
                                        If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_INDV_NegaraLainnya")) = False Then
                                            'result = False
                                            Throw New Exception("Identitas Penerima " & i + 1 & " Nasabah Perorangan: Negara Kewarganegaraan harus diisi  ")
                                        End If
                                    End If

                                End If
                                If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_INDV_Pekerjaan")) = False Then
                                    'result = False
                                    Throw New Exception(" Identitas Penerima " & i + 1 & " Nasabah Perorangan: Pekerjaan harus diisi  ")
                                End If
                                If dt.Rows(i)("Beneficiary_Nasabah_INDV_Pekerjaan") = ParameterPekerjaan.MsSystemParameter_Value Then
                                    If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_INDV_PekerjaanLainnya")) = False Then
                                        'result = False
                                        Throw New Exception(" Identitas Penerima " & i + 1 & " Nasabah Perorangan: Pekerjaan Lain harus diisi  ")
                                    End If
                                End If
                                If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_INDV_ID_Alamat_Iden")) = False Or dt.Rows(i)("Beneficiary_Nasabah_INDV_ID_Alamat_Iden").Length < 3 Then
                                    'result = False
                                    Throw New Exception(" Identitas Penerima " & i + 1 & " Nasabah Perorangan: Alamat Identitas harus diisi minimal 3 karakter! ")
                                End If
                                If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_INDV_ID_KotaKab_Iden")) = False Then
                                    'result = False
                                    Throw New Exception("Identitas Penerima " & i + 1 & " Nasabah Perorangan:  Kota/Kab harus diisi  ")
                                End If
                                If dt.Rows(i)("Beneficiary_Nasabah_INDV_ID_KotaKab_Iden") = ParameterKota.MsSystemParameter_Value Then
                                    If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden")) = False Then
                                        'result = False
                                        Throw New Exception("Identitas Penerima " & i + 1 & " Nasabah Perorangan:  Kota/Kab Lain harus diisi  ")
                                    End If
                                End If
                                If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_INDV_ID_Propinsi_Iden")) = False Then
                                    'result = False
                                    Throw New Exception(" Identitas Penerima " & i + 1 & " Nasabah Perorangan: Provinsi harus diisi  ")
                                End If
                                If dt.Rows(i)("Beneficiary_Nasabah_INDV_ID_Propinsi_Iden") = ParameterProvinsi.MsSystemParameter_Value Then
                                    If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden")) = False Then
                                        'result = False
                                        Throw New Exception(" Identitas Penerima " & i + 1 & " Nasabah Perorangan: Provinsi Lain harus diisi  ")
                                    End If
                                End If
                                If dt.Rows(i)("Beneficiary_Nasabah_INDV_FK_IFTI_IDType") = -1 Then
                                    'result = False
                                    Throw New Exception(" Identitas Penerima " & i + 1 & " Nasabah Perorangan: Jenis Dokumen identitas harus diisi  ")
                                End If
                                If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_INDV_NomorID")) = False Or dt.Rows(i)("Beneficiary_Nasabah_INDV_NomorID").Length < 3 Then
                                    'result = False
                                    Throw New Exception(" Identitas Penerima " & i + 1 & " Nasabah Perorangan: Nomor Identitas harus diisi angka lebih dari 2 digit  ")
                                End If
                                If dt.Rows(i)("Beneficiary_Nasabah_INDV_NomorID") <> "" Then
                                    If dt.Rows(i)("Beneficiary_Nasabah_INDV_FK_IFTI_IDType") = 1 Or dt.Rows(i)("Beneficiary_Nasabah_INDV_FK_IFTI_IDType") = 2 Then
                                        If Not Regex.Match(dt.Rows(i)("Beneficiary_Nasabah_INDV_NomorID").Trim(), "^[0-9]*$").Success Then
                                            Throw New Exception("Identitas Penerima " & i + 1 & " Nasabah Perorangan: Nomor Identitas hanya boleh menggunakan angka!")
                                        End If
                                    Else
                                        If Not Regex.Match(dt.Rows(i)("Beneficiary_Nasabah_INDV_NomorID").Trim(), "^[a-zA-Z0-9]*$").Success Then
                                            Throw New Exception("Identitas Penerima " & i + 1 & " Nasabah Perorangan: Nomor Identitas hanya boleh menggunakan huruf dan angka!")
                                        End If
                                    End If

                                End If

                                If ReceiverDataTable.Rows.Count > 1 Then
                                    If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan")) = False Then
                                        'result = False
                                        Throw New Exception("Identitas Penerima " & i + 1 & " Nasabah Perorangan: Nilai Transaksi Keuangan harus diisi apabila penerima lebih dari 1  ")
                                    End If
                                    If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan")) = True Then
                                        If Not IsValidDecimal(15, 2, dt.Rows(i)("Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan")) Then
                                            'result = False
                                            Throw New Exception("Identitas Penerima " & i + 1 & " Nasabah Perorangan: Nilai Transaksi Keuangan tidak valid!  ")
                                        End If
                                    End If
                                End If

                            Case 2
                                'penerimaKorporasi
                                If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_CORP_NoRekening")) = False Then
                                    'result = False
                                    Throw New Exception("Identitas Penerima " & i + 1 & " Nasabah Korporasi:  No Rekening harus diisi  ")
                                End If

                                If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_CORP_NamaKorporasi")) = False Or dt.Rows(i)("Beneficiary_Nasabah_CORP_NamaKorporasi").Length < 3 Then
                                    'result = False
                                    Throw New Exception(" Identitas Penerima " & i + 1 & " Nasabah Korporasi: Nama Korporasi harus diisi lebih dari 2 karakter! ")
                                End If
                                If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id")) = False Then
                                    'result = False
                                    Throw New Exception(" Identitas Penerima " & i + 1 & " Nasabah Korporasi:  Bidang Usaha Korporasi harus diisi  ")
                                End If
                                If dt.Rows(i)("Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id") = ParameterBidangUsaha.MsSystemParameter_Value Then
                                    If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_CORP_BidangUsahaLainnya")) = False Then
                                        'result = False
                                        Throw New Exception(" Identitas Penerima " & i + 1 & " Nasabah Korporasi:  Bidang Usaha Lain Korporasi harus diisi  ")
                                    End If
                                End If

                                If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_CORP_AlamatLengkap")) = False Or dt.Rows(i)("Beneficiary_Nasabah_CORP_AlamatLengkap").Length < 3 Then
                                    'result = False
                                    Throw New Exception("Identitas Penerima " & i + 1 & " Nasabah Korporasi: Alamat Identitas harus diisi lebih dari 2 karakter! ")
                                End If
                                If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_CORP_ID_KotaKab")) = False Then
                                    'result = False
                                    Throw New Exception("Identitas Penerima " & i + 1 & " Nasabah Korporasi: Kota/Kabupaten harus diisi  ")
                                End If
                                If dt.Rows(i)("Beneficiary_Nasabah_CORP_ID_KotaKab") = ParameterKota.MsSystemParameter_Value Then
                                    If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_CORP_ID_KotaKabLainnya")) = False Then
                                        'result = False
                                        Throw New Exception("Identitas Penerima " & i + 1 & " Nasabah Korporasi: Kota/Kabupaten Lain harus diisi  ")
                                    End If
                                End If
                                If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_CORP_ID_Propinsi")) = False Then
                                    'result = False
                                    Throw New Exception("Identitas Penerima " & i + 1 & " Nasabah Korporasi: Propinsi harus diisi  ")
                                End If
                                If dt.Rows(i)("Beneficiary_Nasabah_CORP_ID_Propinsi") = ParameterProvinsi.MsSystemParameter_Value Then
                                    If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_CORP_ID_PropinsiLainnya")) = False Then
                                        'result = False
                                        Throw New Exception("Identitas Penerima " & i + 1 & " Nasabah Korporasi: Propinsi Lain harus diisi  ")
                                    End If
                                End If
                                'If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan")) = False Then
                                '    'result = False
                                '    Throw New Exception(" Identitas Penerima " & i + 1 & " Nasabah Korporasi: Nilai Transaksi Keuangan harus diisi  ")
                                'End If
                                If ReceiverDataTable.Rows.Count > 1 Then
                                    If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan")) = False Then
                                        'result = False
                                        Throw New Exception("Identitas Penerima " & i + 1 & " Nasabah Korporasi: Nilai Transaksi Keuangan harus diisi apabila penerima lebih dari 1  ")
                                    End If
                                    If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan")) = True Then
                                        If Not IsValidDecimal(15, 2, dt.Rows(i)("Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan")) Then
                                            'result = False
                                            Throw New Exception("Identitas Penerima " & i + 1 & " Nasabah Korporasi: Nilai Transaksi Keuangan tidak valid!  ")
                                        End If
                                    End If
                                End If
                            Case 3
                                'penerimaNOnNasabah

                                If ObjectAntiNull(dt.Rows(i)("Beneficiary_NonNasabah_NamaLengkap")) = False Or dt.Rows(i)("Beneficiary_NonNasabah_NamaLengkap").Length < 3 Then
                                    'result = False
                                    Throw New Exception("Identitas Penerima " & i + 1 & " Non Nasabah : Nama Lengkap harus diisi lebih dari 2 karakter! ")
                                End If
                                If ObjectAntiNull(dt.Rows(i)("Beneficiary_NonNasabah_TanggalLahir")) = True Then
                                    If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", dt.Rows(i)("Beneficiary_NonNasabah_TanggalLahir")) = False Then
                                        Throw New Exception("Identitas Penerima " & i + 1 & " Non Nasabah : Tanggal Lahir tidak valid")
                                    End If
                                End If
                                If dt.Rows(i)("Beneficiary_NonNasabah_NomorID") <> "" Then
                                    If Not Regex.Match(dt.Rows(i)("Beneficiary_NonNasabah_NomorID").Trim(), "^[a-zA-Z0-9]*$").Success Then
                                        Throw New Exception("Identitas Penerima " & i + 1 & " Non Nasabah : Nomor Identitas hanya boleh menggunakan huruf dan angka!")
                                    End If
                                End If
                                If ReceiverDataTable.Rows.Count > 1 Then
                                    If ObjectAntiNull(dt.Rows(i)("Beneficiary_NonNasabah_NilaiTransaksikeuangan")) = False Then
                                        'result = False
                                        Throw New Exception("Identitas Penerima " & i + 1 & " Non Nasabah : Nilai Transaksi Keuangan harus diisi apabila penerima lebih dari 1  ")
                                    End If
                                    If ObjectAntiNull(dt.Rows(i)("Beneficiary_NonNasabah_NilaiTransaksikeuangan")) = True Then
                                        If Not IsValidDecimal(15, 2, dt.Rows(i)("Beneficiary_NonNasabah_NilaiTransaksikeuangan")) Then
                                            'result = False
                                            Throw New Exception("Identitas Penerima " & i + 1 & " Non Nasabah : Nilai Transaksi Keuangan tidak valid!  ")
                                        End If
                                    End If
                                End If
                            Case 4
                                If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_INDV_NoRekening")) = False Or dt.Rows(i)("Beneficiary_Nasabah_INDV_NoRekening").Length < 3 Then
                                    'result = False
                                    Throw New Exception("Identitas Penerima " & i + 1 & " Penerus Nasabah Perorangan: No Rekening harus diisi lebih dari 2 karakter! ")
                                End If

                                If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_INDV_NamaBank")) = False Then
                                    'result = False
                                    Throw New Exception("Identitas Penerima " & i + 1 & " Penerus Nasabah Perorangan: Nama Bank harus diisi  ")
                                End If
                                If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_INDV_NamaLengkap")) = False Then
                                    'result = False
                                    Throw New Exception("Identitas Penerima " & i + 1 & " Penerus Nasabah Perorangan: Nama Lengkap harus diisi  ")
                                End If

                                If dt.Rows(i)("Beneficiary_Nasabah_INDV_KewargaNegaraan") = 2 Then
                                    If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_INDV_Negara")) = False Then
                                        'result = False
                                        Throw New Exception("Identitas Penerima " & i + 1 & " Penerus Nasabah Perorangan: Negara Kewarganegaraan harus diisi  ")
                                    End If
                                    If dt.Rows(i)("Beneficiary_Nasabah_INDV_Negara") = ParameterNegara.MsSystemParameter_Value Then
                                        If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_INDV_NegaraLainnya")) = False Then
                                            'result = False
                                            Throw New Exception("Identitas Penerima " & i + 1 & " Penerus Nasabah Perorangan: Negara Kewarganegaraan harus diisi  ")
                                        End If
                                    End If
                                End If
                                If dt.Rows(i)("Beneficiary_Nasabah_INDV_NomorID") <> "" Then
                                    If dt.Rows(i)("Beneficiary_Nasabah_INDV_FK_IFTI_IDType") = 1 Or dt.Rows(i)("Beneficiary_Nasabah_INDV_FK_IFTI_IDType") = 2 Then
                                        If Not Regex.Match(dt.Rows(i)("Beneficiary_Nasabah_INDV_NomorID").Trim(), "^[0-9]*$").Success Then
                                            Throw New Exception("Identitas Penerima " & i + 1 & " Penerus Nasabah Perorangan:Nomor Identitas hanya boleh menggunakan angka!")
                                        End If
                                    Else
                                        If Not Regex.Match(dt.Rows(i)("Beneficiary_Nasabah_INDV_NomorID").Trim(), "^[a-zA-Z0-9]*$").Success Then
                                            Throw New Exception("Identitas Penerima " & i + 1 & " Penerus Nasabah Perorangan:Nomor Identitas hanya boleh menggunakan huruf dan angka!")
                                        End If
                                    End If

                                End If
                                If ReceiverDataTable.Rows.Count > 1 Then
                                    If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan")) = False Then
                                        'result = False
                                        Throw New Exception("Identitas Penerima " & i + 1 & " Nasabah Perorangan : Nilai Transaksi Keuangan harus diisi apabila penerima lebih dari 1  ")
                                    End If
                                    If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan")) = True Then
                                        If Not IsValidDecimal(15, 2, dt.Rows(i)("Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan")) Then
                                            'result = False
                                            Throw New Exception("Identitas Penerima " & i + 1 & " Nasabah Perorangan : Nilai Transaksi Keuangan tidak valid!  ")
                                        End If
                                    End If
                                End If
                            Case 5
                                If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_CORP_NoRekening")) = False Then
                                    'result = False
                                    Throw New Exception("Identitas Penerima " & i + 1 & " Nasabah Korporasi : No Rekening harus diisi  ")
                                End If

                                If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_CORP_NamaBank")) = False And ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_CORP_NoRekening")) = False Then
                                    'result = False
                                    Throw New Exception("Identitas Penerima " & i + 1 & " Nasabah Korporasi : Nama Bank harus diisi  ")
                                End If
                                If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_CORP_NamaKorporasi")) = False Or dt.Rows(i)("Beneficiary_Nasabah_CORP_NamaKorporasi").Length < 3 Then
                                    'result = False
                                    Throw New Exception("Identitas Penerima " & i + 1 & " Nasabah Korporasi : Nama Korporasi harus diisi lebih dari 2 karakter! ")
                                End If

                                If ReceiverDataTable.Rows.Count > 1 Then
                                    If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan")) = False Then
                                        'result = False
                                        Throw New Exception("Identitas Penerima " & i + 1 & " Nasabah Korporasi : Nilai Transaksi Keuangan harus diisi apabila penerima lebih dari 1  ")
                                    End If
                                    If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan")) = True Then
                                        If Not IsValidDecimal(15, 2, dt.Rows(i)("Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan")) Then
                                            'result = False
                                            Throw New Exception("Identitas Penerima " & i + 1 & " Nasabah Korporasi : Nilai Transaksi Keuangan tidak valid!  ")
                                        End If
                                    End If
                                End If

                        End Select

                    End If

                End If
            End If

        Next

    End Function
    'Sub validasiReceiver1()
    '    'penerimaIndividu
    '    'parameter value

    '    Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
    '    Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
    '    Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
    '    Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)
    '    Dim ParameterBidangUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BidangUsaha)

    '    If cboSwiftInPenerima_TipePJKBank.SelectedIndex = -1 Then Throw New Exception(" Identitas Penerima Nasabah Perorangan: Tipe PJK Bank harus diisi  ")
    '    If RbSwiftInPenerima_TipePenerima.SelectedIndex = -1 Then Throw New Exception("Identitas Penerima Nasabah Perorangan: Tipe Pengirim harus diisi  ")
    '    If RbSwiftInPenerima_TipeNasabah.SelectedIndex = -1 Then Throw New Exception("Identitas Penerima Nasabah Perorangan: Tipe Nasabah harus diisi  ")

    '    If ObjectAntiNull(txtSwiftInPenerimaNasabah_Rekening.Text) = False Then Throw New Exception(" Identitas Penerima Nasabah Perorangan: No Rekening harus diisi  ")
    '    If ObjectAntiNull(txtSwiftInPenerimaNasabah_IND_nama.Text) = False Then Throw New Exception(" Identitas Penerima Nasabah Perorangan: Nama Lengkap harus diisi  ")
    '    If ObjectAntiNull(txtSwiftInPenerimaNasabah_IND_TanggalLahir.Text) = True Then
    '        If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", txtSwiftInPenerimaNasabah_IND_TanggalLahir.Text) = False Then
    '            Throw New Exception("Identitas Penerima Nasabah Perorangan: Tanggal Lahir tidak valid")
    '        End If
    '    End If
    '    If RbSwiftInPenerimaNasabah_IND_Kewarganegaraan.SelectedValue = "2" Then
    '        If ObjectAntiNull(txtSwiftInPenerimaNasabah_IND_negara.Text) = False And ObjectAntiNull(txtSwiftInPenerimaNasabah_IND_negaralain.Text) = False Then Throw New Exception("Identitas Penerima Nasabah Perorangan: Negara Kewarganegaraan harus diisi  ")
    '        If ObjectAntiNull(txtSwiftInPenerimaNasabah_IND_negaralain.Text) = True And txtSwiftInPenerimaNasabah_IND_negaralain.Text.Length < 3 Then Throw New Exception("Identitas Penerima Nasabah Perorangan: Negara Kewarganegaraan harus diisi lebih dari 3 karakter!  ")
    '    End If
    '    If ObjectAntiNull(txtSwiftInPenerimaNasabah_IND_pekerjaan.Text) = False Then Throw New Exception(" Identitas Penerima Nasabah Perorangan: Pekerjaan harus diisi  ")
    '    If Me.hfSwiftInIdenPenerimaNas_Ind_pekerjaan.Value = ParameterPekerjaan.MsSystemParameter_Value Then
    '        If ObjectAntiNull(TxtSwiftInIdenPenerimaNas_Ind_Pekerjaanlain.Text) = False Then Throw New Exception(" Identitas Penerima Nasabah Perorangan: Pekerjaan harus diisi  ")
    '    End If
    '    'If ObjectAntiNull(txtSwiftInPenerimaNasabah_IND_pekerjaan.Text) = True And ObjectAntiNull(TxtSwiftInIdenPenerimaNas_Ind_Pekerjaanlain.Text) = True Then Throw New Exception("Identitas Penerima Nasabah Perorangan: Pekerjaan harus diisi salah satu saja  ")

    '    'If ObjectAntiNull(TxtSwiftInIdenPenerimaNas_Ind_Kota.Text) = True And ObjectAntiNull(TxtSwiftInIdenPenerimaNas_Ind_kotalain.Text) = True Then Throw New Exception("Identitas Penerima Nasabah Perorangan: Kota/Kab harus diisi salah satu saja  ")
    '    'If ObjectAntiNull(TxtSwiftInIdenPenerimaNas_Ind_provinsi.Text) = True And ObjectAntiNull(TxtSwiftInIdenPenerimaNas_Ind_provinsiLain.Text) = True Then Throw New Exception("Identitas Penerima Nasabah Perorangan: Provinsi harus diisi salah satu saja  ")

    '    If ObjectAntiNull(TxtSwiftInIdenPenerimaNas_Ind_alamatIdentitas.Text) = False Then Throw New Exception(" Alamat Identitas harus diisi  ")
    '    If ObjectAntiNull(TxtSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Text) = False Then Throw New Exception("Identitas Penerima Nasabah Perorangan:  Kota/Kab harus diisi  ")
    '    If Me.hfSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Value = ParameterKota.MsSystemParameter_Value Then
    '        If ObjectAntiNull(TxtSwiftInIdenPenerimaNas_Ind_KotaLainIden.Text) = False Then Throw New Exception("Identitas Penerima Nasabah Perorangan:  Kota/Kab harus diisi  ")
    '    End If
    '    'If ObjectAntiNull(TxtSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Text) = True And ObjectAntiNull(TxtSwiftInIdenPenerimaNas_Ind_KotaLainIden.Text) = True Then Throw New Exception("Identitas Penerima Nasabah Perorangan: Kota/Kab harus diisi salah satu saja  ")
    '    If ObjectAntiNull(TxtSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Text) = False Then Throw New Exception(" Identitas Penerima Nasabah Perorangan: Provinsi harus diisi  ")
    '    If Me.hfSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Value = ParameterProvinsi.MsSystemParameter_Value Then
    '        If ObjectAntiNull(TxtSwiftInIdenPenerimaNas_Ind_ProvinsilainIden.Text) = False Then Throw New Exception(" Identitas Penerima Nasabah Perorangan: Provinsi harus diisi  ")
    '    End If
    '    'If ObjectAntiNull(TxtSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Text) = True And ObjectAntiNull(TxtSwiftInIdenPenerimaNas_Ind_ProvinsilainIden.Text) = True Then Throw New Exception("Identitas Penerima Nasabah Perorangan: Provinsi harus diisi salah satu saja  ")
    '    If ObjectAntiNull(TxtISwiftIndenPenerimaNas_Ind_noTelp.Text) = True And IsPhoneNumber(TxtISwiftIndenPenerimaNas_Ind_noTelp.Text) = False Then Throw New Exception("Identitas Pengirim Perorangan: No telp tidak valid!  ")
    '    If cbo_SwiftInpenerimaNas_Ind_jenisidentitas.SelectedIndex = -1 Then Throw New Exception(" Identitas Penerima Nasabah Perorangan: Jenis Dokumen identitas harus diisi  ")
    '    If ObjectAntiNull(TxtISwiftIndenPenerimaNas_Ind_noIdentitas.Text) = False Then Throw New Exception(" Identitas Penerima Nasabah Perorangan: Nomor Identitas harus diisi  ")
    '    If ObjectAntiNull(txtSwiftInIdenPenerimaNas_IND_NilaiTransaksiKeuangan.Text) = False Then Throw New Exception(" Identitas Penerima Nasabah Perorangan: Nilai Transaksi Keuangan harus diisi  ")
    '    If Not IsNumeric(txtSwiftInIdenPenerimaNas_IND_NilaiTransaksiKeuangan.Text) Then
    '        Throw New Exception("Identitas Penerima Nasabah Perorangan: Nilai Transaksi Keuangan tidak valid")
    '    End If
    'End Sub
    'Sub validasiReceiver2()
    '    'pennerimaKorporasi
    '    'parameter value

    '    Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
    '    Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
    '    Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
    '    Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)
    '    Dim ParameterBidangUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BidangUsaha)

    '    If cboSwiftInPenerima_TipePJKBank.SelectedIndex = -1 Then Throw New Exception("Identitas Penerima Nasabah Korporasi: Tipe PJK Bank harus diisi  ")
    '    If RbSwiftInPenerima_TipePenerima.SelectedIndex = -1 Then Throw New Exception("Identitas Penerima Nasabah Korporasi:  Tipe Pengirim harus diisi  ")
    '    If RbSwiftInPenerima_TipeNasabah.SelectedIndex = -1 Then Throw New Exception("Identitas Penerima Nasabah Korporasi: Tipe Nasabah harus diisi  ")
    '    If ObjectAntiNull(txtSwiftInPenerimaNasabah_Rekening.Text) = False Then Throw New Exception("Identitas Penerima Nasabah Korporasi:  No Rekening harus diisi  ")

    '    If ObjectAntiNull(txtSwiftInPenerimaNasabah_Korp_namaKorp.Text) = False Then Throw New Exception(" Identitas Penerima Nasabah Korporasi: Nama Korporasi harus diisi  ")
    '    If ObjectAntiNull(txtSwiftInPenerimaNasabah_Korp_BidangUsahaKorp.Text) = False Then Throw New Exception(" Identitas Penerima Nasabah Korporasi:  Bidang Usaha Korporasi harus diisi  ")
    '    If Me.hfSwiftInPenerimaNasabah_Korp_BidangUsahaKorp.Value = ParameterBidangUsaha.MsSystemParameter_Value Then
    '        If ObjectAntiNull(txtSwiftInPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Text) = False Then Throw New Exception(" Identitas Penerima Nasabah Korporasi:  Bidang Usaha Korporasi harus diisi  ")
    '    End If
    '    'If ObjectAntiNull(txtSwiftInPenerimaNasabah_Korp_BidangUsahaKorp.Text) = True And ObjectAntiNull(txtSwiftInPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Text) = True Then Throw New Exception("Identitas Penerima Nasabah Korporasi: Bidang Usaha Korporasi harus diisi salah satu saja  ")
    '    If ObjectAntiNull(Me.txtSwiftInPenerimaNasabah_Korp_AlamatKorp.Text) = False Then Throw New Exception("Identitas Penerima Nasabah Korporasi: Alamat Identitas harus diisi  ")
    '    If ObjectAntiNull(txtSwiftInPenerimaNasabah_Korp_Kotakab.Text) = False Then Throw New Exception("Identitas Penerima Nasabah Korporasi: Kota/Kabupaten harus diisi  ")
    '    If Me.hfSwiftInPenerimaNasabah_Korp_kotakab.Value = ParameterKota.MsSystemParameter_Value Then
    '        If ObjectAntiNull(txtSwiftInPenerimaNasabah_Korp_kotaLain.Text) = False Then Throw New Exception("Identitas Penerima Nasabah Korporasi: Kota/Kabupaten harus diisi  ")
    '    End If
    '    'If ObjectAntiNull(txtSwiftInPenerimaNasabah_Korp_Kotakab.Text) = True And ObjectAntiNull(txtSwiftInPenerimaNasabah_Korp_kotaLain.Text) = True Then Throw New Exception("Identitas Penerima Nasabah Korporasi: Kota/Kabupaten harus diisi salah satu saja  ")
    '    If ObjectAntiNull(txtSwiftInPenerimaNasabah_Korp_propinsi.Text) = False Then Throw New Exception("Identitas Penerima Nasabah Korporasi: Propinsi harus diisi  ")
    '    If Me.hfSwiftInPenerimaNasabah_Korp_propinsi.Value = ParameterProvinsi.MsSystemParameter_Value Then
    '        If ObjectAntiNull(txtSwiftInPenerimaNasabah_Korp_propinsilain.Text) = False Then Throw New Exception("Identitas Penerima Nasabah Korporasi: Propinsi harus diisi  ")
    '    End If
    '    'If ObjectAntiNull(txtSwiftInPenerimaNasabah_Korp_propinsi.Text) = True And ObjectAntiNull(txtSwiftInPenerimaNasabah_Korp_propinsilain.Text) = True Then Throw New Exception("Identitas Penerima Nasabah Korporasi: Propinsi harus diisi salah satu saja  ")
    '    If ObjectAntiNull(txtSwiftInPenerimaNasabah_Korp_NoTelp.Text) = True And IsPhoneNumber(txtSwiftInPenerimaNasabah_Korp_NoTelp.Text) = False Then Throw New Exception("Identitas Pengirim Korporasi: No telp tidak valid!  ")
    '    If ObjectAntiNull(txtSwiftInPenerimaNasabah_Korp_NilaiTransaksiKeuangan.Text) = False Then Throw New Exception(" Identitas Penerima Nasabah Korporasi: Nilai Transaksi Keuangan harus diisi  ")
    '    If Not IsNumeric(txtSwiftInPenerimaNasabah_Korp_NilaiTransaksiKeuangan.Text) Then
    '        Throw New Exception("Identitas Penerima Nasabah Korporasi: Nilai Transaksi Keuangan tidak valid")
    '    End If
    'End Sub
    'Sub validasiReceiver3()
    '    'penerimaNOnNasabah
    '    'parameter value

    '    Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
    '    Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
    '    Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
    '    Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)
    '    Dim ParameterBidangUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BidangUsaha)

    '    If cboSwiftInPenerima_TipePJKBank.SelectedIndex = -1 Then Throw New Exception("Identitas Penerima Non Nasabah : Tipe PJK Bank harus diisi  ")
    '    If RbSwiftInPenerima_TipePenerima.SelectedIndex = -1 Then Throw New Exception("Identitas Penerima Non Nasabah : Tipe Pengirim harus diisi  ")
    '    If RbSwiftInPenerima_TipeNasabah.SelectedIndex = -1 Then Throw New Exception("Identitas Penerima Non Nasabah : Tipe Nasabah harus diisi  ")
    '    If ObjectAntiNull(txtSwiftInPenerimaNasabah_Rekening.Text) = False Then Throw New Exception("Identitas Penerima Non Nasabah : No Rekening harus diisi  ")

    '    If ObjectAntiNull(TxtSwiftInPenerimaNonNasabah_nama.Text) = False Then Throw New Exception("Identitas Penerima Non Nasabah : Nama Lengkap harus diisi  ")
    '    If ObjectAntiNull(TxtSwiftInPenerimaNonNasabah_TanggalLahir.Text) = True Then
    '        If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", TxtSwiftInPenerimaNonNasabah_TanggalLahir.Text) = False Then
    '            Throw New Exception("Identitas Penerima Non Nasabah : Tanggal Lahir tidak valid")
    '        End If
    '    End If
    '    If ObjectAntiNull(TxtSwiftInPenerimaNonNasabah_NoTelepon.Text) = True And IsPhoneNumber(TxtSwiftInPenerimaNonNasabah_NoTelepon.Text) = False Then Throw New Exception("Identitas Pengirim Non Nasabah: No telp tidak valid!  ")
    '    If Not IsNumeric(TxtSwiftInPenerimaNonNasabah_NilaiTransaksiKeuangan.Text) Then
    '        Throw New Exception("Identitas Penerima Non Nasabah : Nilai Transaksi Keuangan tidak valid")
    '    End If
    'End Sub
    'Sub validasiReceiver4()
    '    'parameter value

    '    Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
    '    Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
    '    Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
    '    Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)
    '    Dim ParameterBidangUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BidangUsaha)

    '    If cboSwiftInPenerima_TipePJKBank.SelectedIndex = -1 Then Throw New Exception("Identitas Penerima Penerus Nasabah Perorangan: Tipe PJK Bank harus diisi  ")
    '    If RbSwiftInPenerima_TipePenerima.SelectedIndex = -1 Then Throw New Exception("Identitas Penerima Penerus Nasabah Perorangan: Tipe Pengirim harus diisi  ")
    '    If RbSwiftInPenerima_TipeNasabah.SelectedIndex = -1 Then Throw New Exception("Identitas Penerima Penerus Nasabah Perorangan: Tipe Nasabah harus diisi  ")
    '    If ObjectAntiNull(TxtSwiftInPenerimaPenerus_Rekening.Text) = False Then Throw New Exception("Identitas Penerima Penerus Nasabah Perorangan: No Rekening harus diisi  ")

    '    If ObjectAntiNull(TxtSwiftInPenerimaPenerus_Ind_namaBank.Text) = False Then Throw New Exception("Identitas Penerima Penerus Nasabah Perorangan: Nama Bank harus diisi  ")
    '    If ObjectAntiNull(TxtSwiftInPenerimaPenerus_Ind_nama.Text) = False Then Throw New Exception("Identitas Penerima Penerus Nasabah Perorangan: Nama Lengkap harus diisi  ")

    '    If RBSwiftInPenerimaPenerus_Ind_Kewarganegaraan.SelectedValue = "2" Then
    '        If ObjectAntiNull(TxtSwiftInPenerimaPenerus_IND_negara.Text) = False And ObjectAntiNull(TxtSwiftInPenerimaPenerus_IND_negaralain.Text) = False Then Throw New Exception("Identitas Penerima Penerus Nasabah Perorangan: Negara Kewarganegaraan harus diisi  ")
    '        If ObjectAntiNull(TxtSwiftInPenerimaPenerus_IND_negaralain.Text) = True And TxtSwiftInPenerimaPenerus_IND_negaralain.Text.Length < 3 Then Throw New Exception("Identitas Penerima Penerus Nasabah Perorangan: Negara Kewarganegaraan harus diisi lebih dari 2 karakter!  ")
    '    End If
    '    'dom
    '    'If ObjectAntiNull(TxtSwiftInPenerimaPenerus_IND_kotaDom.Text) = True And ObjectAntiNull(TxtSwiftInPenerimaPenerus_IND_kotaLainDom.Text) = True Then Throw New Exception("Identitas Penerima Penerus Nasabah Perorangan: Kota/Kab harus diisi salah satu saja  ")
    '    'If ObjectAntiNull(TxtSwiftInPenerimaPenerus_IND_ProvDom.Text) = True And ObjectAntiNull(TxtSwiftInPenerimaPenerus_IND_ProvLainDom.Text) = True Then Throw New Exception("Identitas Penerima Penerus Nasabah Perorangan: Provinsi harus diisi salah satu saja  ")

    '    'If ObjectAntiNull(TxtSwiftInPenerimaPenerus_IND_pekerjaan.Text) = True And ObjectAntiNull(TxtSwiftInPenerimaPenerus_IND_pekerjaanLain.Text) = True Then Throw New Exception("Identitas Penerima Penerus Nasabah Perorangan: Pekerjaan harus diisi salah satu saja  ")
    '    'iden
    '    'If ObjectAntiNull(TxtSwiftInPenerimaPenerus_IND_kotaIden.Text) = True And ObjectAntiNull(TxtSwiftInPenerimaPenerus_IND_KotaLainIden.Text) = True Then Throw New Exception("Identitas Penerima Penerus Nasabah Perorangan: Kota/Kab harus diisi salah satu saja  ")
    '    'If ObjectAntiNull(TxtSwiftInPenerimaPenerus_IND_ProvIden.Text) = True And ObjectAntiNull(TxtSwiftInPenerimaPenerus_IND_ProvLainIden.Text) = True Then Throw New Exception("Identitas Penerima Penerus Nasabah Perorangan: Provinsi harus diisi salah satu saja  ")
    '    If ObjectAntiNull(TxtSwiftInPenerimaPenerus_IND_NoTelp.Text) = True And IsPhoneNumber(TxtSwiftInPenerimaPenerus_IND_NoTelp.Text) = False Then Throw New Exception("Identitas Pengirim Perorangan: No telp tidak valid!  ")
    '    If Not IsNumeric(TxtSwiftInPenerimaPenerus_IND_NIlaiTransaksi.Text) Then
    '        Throw New Exception("Identitas Penerima Penerus Nasabah Perorangan: Nilai Transaksi Keuangan tidak valid")
    '    End If
    'End Sub
    'Sub validasiReceiver5()
    '    'parameter value

    '    Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
    '    Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
    '    Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
    '    Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)
    '    Dim ParameterBidangUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BidangUsaha)

    '    If cboSwiftInPenerima_TipePJKBank.SelectedIndex = -1 Then Throw New Exception(" TIpe PJK Bank harus diisi  ")
    '    If RbSwiftInPenerima_TipePenerima.SelectedIndex = -1 Then Throw New Exception("Tipe Pengirim harus diisi  ")
    '    If RbSwiftInPenerima_TipeNasabah.SelectedIndex = -1 Then Throw New Exception("Tipe Nasabah harus diisi  ")
    '    If ObjectAntiNull(TxtSwiftInPenerimaPenerus_Rekening.Text) = False Then Throw New Exception(" No Rekening harus diisi  ")

    '    If ObjectAntiNull(txtSwiftInPenerimaPenerus_Korp_namabank.Text) = False Then Throw New Exception(" Nama Bank harus diisi  ")
    '    If ObjectAntiNull(txtSwiftInPenerimaPenerus_Korp_NamaKorp.Text) = False Then Throw New Exception(" Nama Korporasi harus diisi  ")

    '    'If ObjectAntiNull(txtSwiftInPenerimaPenerus_Korp_BidangUsahaKorp.Text) = True And ObjectAntiNull(txtSwiftInPenerimaPenerus_Korp_BidangUsahaKorpLainnya.Text) = True Then Throw New Exception("Identitas Penerima Penerus Nasabah Korporasi: Bidang Usaha Korporasi harus diisi salah satu saja  ")
    '    'If ObjectAntiNull(txtSwiftInPenerimaPenerus_Korp_kotakorp.Text) = True And ObjectAntiNull(txtSwiftInPenerimaPenerus_Korp_kotalainnyakorp.Text) = True Then Throw New Exception("Identitas Penerima Penerus Nasabah Korporasi: Kota/Kabupaten harus diisi salah satu saja  ")
    '    'If ObjectAntiNull(txtSwiftInPenerimaPenerus_Korp_ProvinsiKorp.Text) = True And ObjectAntiNull(txtSwiftInPenerimaPenerus_Korp_provinsiLainnyaKorp.Text) = True Then Throw New Exception("Identitas Penerima Penerus Nasabah Korporasi: Propinsi harus diisi salah satu saja  ")
    '    If ObjectAntiNull(txtSwiftInPenerimaPenerus_Korp_NoTelp.Text) = True And IsPhoneNumber(txtSwiftInPenerimaPenerus_Korp_NoTelp.Text) = False Then Throw New Exception("Identitas Pengirim Korporasi: No telp tidak valid!  ")

    '    If Not IsNumeric(txtSwiftInPenerimaPenerus_Korp_NilaiTransaksi.Text) Then
    '        Throw New Exception("Nilai Transaksi Keuangan tidak valid")
    '    End If
    'End Sub
#End Region
    Sub validasiTransaksi()
        Dim ParameterMataUang As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.MataUang)
        If ObjectAntiNull(Transaksi_SwiftIntanggal0.Text) = False Then Throw New Exception("Tanggal Transaksi harus diisi  ")
        If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", Transaksi_SwiftIntanggal0.Text) = False Then
            Throw New Exception("Tanggal Transaksi tidak valid")
        End If
        If ObjectAntiNull(Transaksi_SwiftInSender0.Text) = False Then Throw New Exception("Referensi Pengirim harus diisi  ")
        If ObjectAntiNull(Transaksi_SwiftInSender0.Text) = True And Transaksi_SwiftInSender0.Text.Length < 3 Then
            Throw New Exception("Identitas Penerima Penerus Nasabah Korporasi: Referensi pengirim harus diisi lebih dari 2 karakter!  ")
        End If
        If ObjectAntiNull(Transaksi_SwiftInSender0.Text) = False Then Throw New Exception("Referensi Pengirim harus diisi  ")
        If ObjectAntiNull(Me.Transaksi_SwiftInSender0.Text) = False Or Transaksi_SwiftInSender0.Text.Length < 2 Then Throw New Exception("Referensi Pengirim harus diisi  ")
        If ObjectAntiNull(Transaksi_SwiftInBankOperationCode0.Text) = False Then Throw New Exception("Kode Operasi Bank harus diisi  ")
        If ReceiverDataTable.Rows.Count > 0 Then
            Dim i As Integer
            Dim rows As Integer = ReceiverDataTable.Rows.Count
            Dim Penerima As Integer
            Dim counter As Integer
            For i = 0 To rows - 1
                Penerima = ReceiverDataTable.Rows(i)("PJKBank_Type")
                If Penerima = 2 Then
                    counter += 1
                End If
            Next
            If counter > 0 Then
                If ObjectAntiNull(Me.Transaksi_SwiftInkantorCabangPengirim0.Text) = False Then Throw New Exception("Kantor Cabang Penyelenggara harus diisi!  ")
            End If
        End If


        If ObjectAntiNull(Transaksi_SwiftInValueTanggalTransaksi.Text) = False Then Throw New Exception("Tanggal Transaksi(value date) harus diisi  ")
        If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", Transaksi_SwiftInValueTanggalTransaksi.Text) = False Then
            Throw New Exception("Tanggal Transaksi tidak valid")
        End If
        If ObjectAntiNull(Transaksi_SwiftInnilaitransaksi.Text) = False Then Throw New Exception("Nilai Transaksi harus diisi  ")

        If ObjectAntiNull(Transaksi_SwiftInnilaitransaksi.Text) = False Then
            If Not IsValidDecimal(15, 2, Transaksi_SwiftInnilaitransaksi.Text) Then Throw New Exception("Nilai Transaksi harus diisi angka ")
        End If
        If ObjectAntiNull(Transaksi_SwiftInMataUangTransaksi0.Text) = False Then Throw New Exception("Mata Uang Transaksi harus diisi! ")
        If Me.hfTransaksi_SwiftInMataUangTransaksi0.Value = ParameterMataUang.MsSystemParameter_Value Then
            If ObjectAntiNull(Me.Transaksi_SwiftInMataUangTransaksi0Lainnya.Text) = False Then Throw New Exception("Mata Uang Lainnya Transaksi harus diisi! ")
        End If
        'If ObjectAntiNull(Transaksi_SwiftInMataUangTransaksi0.Text) = True And ObjectAntiNull(Transaksi_SwiftInMataUangTransaksi0Lainnya.Text) = True Then Throw New Exception("Mata Uang Transaksi harus diisi salah satu saja! ")
        If ObjectAntiNull(Transaksi_SwiftInAmountdalamRupiah0.Text) = False Then Throw New Exception("Nilai Transaksi dalam rupiah harus diisi  ")
        If Not ObjectAntiNull(Transaksi_SwiftInAmountdalamRupiah0.Text) = False Then
            If Not IsValidDecimal(15, 2, Transaksi_SwiftInAmountdalamRupiah0.Text) Then Throw New Exception("Nilai Transaksi dalam rupiah harus diisi angka ")
        End If
        If ObjectAntiNull(Transaksi_SwiftIninstructedAmount0.Text) = True Then
            If Not IsNumeric(Transaksi_SwiftIninstructedAmount0.Text) Then Throw New Exception(" Instructed Amount harus diisi angka ")
        End If
        If ObjectAntiNull(Transaksi_SwiftInnilaiTukar0.Text) = True Then
            If Not IsNumeric(Transaksi_SwiftInnilaiTukar0.Text) Then Throw New Exception(" Nilai Tukar harus diisi angka ")
        End If
        'If ObjectAntiNull(Transaksi_SwiftIncurrency0.Text) = True And ObjectAntiNull(Transaksi_SwiftIncurrencyLainnya.Text) = True Then Throw New Exception("Mata Uang Transaksi harus diisi salah satu! ")
    End Sub
#End Region
#Region "save"
    Sub SaveSwiftInco()
        Try
            Using OTrans As TransactionManager = New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)

                Try
                    OTrans.BeginTransaction()
                    If rbrbSwiftIn_TipePengirim.SelectedIndex = -1 Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Tipe Pengirim harus diisi  ")


                    Dim tipepengirim As Integer = rbrbSwiftIn_TipePengirim.SelectedValue
                    Dim tipenasabah As Integer
                    Dim senderType As Integer
                    'cekvalidasi
                    ValidasiControl()
                    If tipepengirim = 1 Then
                        If rbSwiftIn_TipeNasabah.SelectedIndex = -1 Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Tipe Nasabah harus diisi  ")
                        tipenasabah = rbSwiftIn_TipeNasabah.SelectedValue
                        If tipenasabah = 1 Then
                            senderType = 1
                        Else
                            senderType = 2
                        End If
                    Else
                        senderType = 3
                    End If

                    Select Case (senderType)
                        Case 1
                            'pengirimNasabahIndividu
                            'cekvalidasi
                            validasiSenderIndividu()
                        Case 2
                            'pengirimNasabahKorporasi
                            'cekvalidasi
                            validasiSenderKorporasi()
                        Case 3
                            'PengirimNonNasabah
                            'cekvalidasi
                            validasiSenderNonNasabah()
                    End Select

                    'cek penerima
                    Me.ValidateReceiverSaveAll()
                    validasiTransaksi()
                    'parameter value

                    Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
                    Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
                    Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
                    Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)
                    Dim ParameterBidangUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BidangUsaha)

                    Page.Validate("handle")
                    If Page.IsValid Then
                        ' =========== Insert Header Approval
                        Dim KeyHeaderApproval As Integer
                        Using objIftiApproval As New IFTI_Approval
                            With objIftiApproval
                                FillOrNothing(.FK_MsMode_Id, 2, True, oInt)
                                FillOrNothing(.RequestedBy, SessionPkUserId)
                                FillOrNothing(.RequestedDate, Date.Now)
                                .Fk_IFTI_Type_ID = 2
                                'FillOrNothing(.IsUpload, False)
                            End With
                            DataRepository.IFTI_ApprovalProvider.Save(OTrans, objIftiApproval)
                            KeyHeaderApproval = objIftiApproval.PK_IFTI_Approval_Id
                        End Using

                        '============ Insert Detail Approval
                        Using objIfti_ApprovalDetail As New IFTI_Approval_Detail()
                            Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)

                                With objIfti_ApprovalDetail
                                    'FK
                                    .PK_IFTI_ID_Old = objIfti.PK_IFTI_ID
                                    .PK_IFTI_ID = objIfti.PK_IFTI_ID
                                    .FK_IFTI_Approval_Id = KeyHeaderApproval
                                    .FK_IFTI_Type_ID_Old = objIfti.FK_IFTI_Type_ID
                                    .FK_IFTI_Type_ID = 2
                                    'umum
                                    '--old--
                                    FillOrNothing(.LTDLNNo_Old, objIfti.LTDLNNo, False, oInt)
                                    FillOrNothing(.LTDLNNoKoreksi_Old, objIfti.LTDLNNoKoreksi, False, oInt)
                                    FillOrNothing(.TanggalLaporan_Old, objIfti.TanggalLaporan, False, oDate)
                                    FillOrNothing(.NamaPJKBankPelapor_Old, objIfti.NamaPJKBankPelapor, False, Ovarchar)
                                    FillOrNothing(.NamaPejabatPJKBankPelapor_Old, objIfti.NamaPejabatPJKBankPelapor, False, Ovarchar)
                                    FillOrNothing(.JenisLaporan_Old, objIfti.JenisLaporan, False, oInt)
                                    '--new--
                                    FillOrNothing(.LTDLNNo, Me.SwiftInUmum_LTDN.Text, False, oInt)
                                    FillOrNothing(.LTDLNNoKoreksi, Me.SwiftInUmum_LtdnKoreksi.Text, False, oInt)
                                    FillOrNothing(.TanggalLaporan, Me.SwiftInUmum_TanggalLaporan.Text, False, oDate)
                                    FillOrNothing(.NamaPJKBankPelapor, Me.SwiftInUmum_NamaPJKBank.Text, False, Ovarchar)
                                    FillOrNothing(.NamaPejabatPJKBankPelapor, Me.SwiftInUmum_NamaPejabatPJKBank.Text, False, Ovarchar)
                                    FillOrNothing(.JenisLaporan, Me.RbSwiftInUmum_JenisLaporan.SelectedValue, False, oInt)

                                    'cekPengirimOld
                                    Dim senderOld As Integer = objIfti.Sender_FK_IFTI_NasabahType_ID.GetValueOrDefault(0)

                                    Select Case (senderOld)
                                        Case 1
                                            'pengirimNasabahIndividu

                                            FillOrNothing(.Sender_FK_IFTI_NasabahType_ID_Old, objIfti.Sender_FK_IFTI_NasabahType_ID)
                                            FillOrNothing(.Sender_Nasabah_INDV_NoRekening_Old, objIfti.Sender_Nasabah_INDV_NoRekening)
                                            FillOrNothing(.Sender_Nasabah_INDV_NamaLengkap_Old, objIfti.Sender_Nasabah_INDV_NamaLengkap)
                                            FillOrNothing(.Sender_Nasabah_INDV_TanggalLahir_Old, objIfti.Sender_Nasabah_INDV_TanggalLahir)
                                            FillOrNothing(.Sender_Nasabah_INDV_KewargaNegaraan_Old, objIfti.Sender_Nasabah_INDV_KewargaNegaraan)
                                            If .Sender_Nasabah_INDV_KewargaNegaraan_Old.GetValueOrDefault(1) = "2" Then
                                                FillOrNothing(.Sender_Nasabah_INDV_Negara_Old, objIfti.Sender_Nasabah_INDV_Negara)
                                                FillOrNothing(.Sender_Nasabah_INDV_NegaraLainnya_Old, objIfti.Sender_Nasabah_INDV_NegaraLainnya)
                                            Else
                                                FillOrNothing(.Sender_Nasabah_INDV_Negara_Old, Nothing, False, oInt)
                                                FillOrNothing(.Sender_Nasabah_INDV_NegaraLainnya_Old, Nothing, False, Ovarchar)
                                            End If
                                            FillOrNothing(.Sender_Nasabah_INDV_ID_Alamat_Old, objIfti.Sender_Nasabah_INDV_ID_Alamat)
                                            FillOrNothing(.Sender_Nasabah_INDV_ID_NegaraKota_Old, objIfti.Sender_Nasabah_INDV_ID_NegaraKota)
                                            FillOrNothing(.Sender_Nasabah_INDV_ID_Negara_Old, objIfti.Sender_Nasabah_INDV_ID_Negara)
                                            FillOrNothing(.Sender_Nasabah_INDV_NegaraLainnya_Old, objIfti.Sender_Nasabah_INDV_NegaraLainnya)
                                            FillOrNothing(.Sender_Nasabah_INDV_NoTelp_Old, objIfti.Sender_Nasabah_INDV_NoTelp)
                                            FillOrNothing(.Sender_Nasabah_INDV_FK_IFTI_IDType_Old, objIfti.Sender_Nasabah_INDV_FK_IFTI_IDType)
                                            FillOrNothing(.Sender_Nasabah_INDV_NomorID_Old, objIfti.Sender_Nasabah_INDV_NomorID)
                                        Case 2
                                            'pengirimNasabahKorporasi

                                            FillOrNothing(.Sender_FK_IFTI_NasabahType_ID_Old, objIfti.Sender_FK_IFTI_NasabahType_ID)
                                            FillOrNothing(.Sender_Nasabah_CORP_NoRekening_Old, objIfti.Sender_Nasabah_CORP_NoRekening)
                                            FillOrNothing(.Sender_Nasabah_CORP_NamaKorporasi_Old, objIfti.Sender_Nasabah_CORP_NamaKorporasi)
                                            FillOrNothing(.Sender_Nasabah_CORP_AlamatLengkap_Old, objIfti.Sender_Nasabah_CORP_AlamatLengkap)
                                            FillOrNothing(.Sender_Nasabah_CORP_NegaraKota_Old, objIfti.Sender_Nasabah_CORP_NegaraKota)
                                            FillOrNothing(.Sender_Nasabah_CORP_Negara_Old, objIfti.Sender_Nasabah_CORP_Negara)
                                            FillOrNothing(.Sender_Nasabah_CORP_NegaraLainnya_Old, objIfti.Sender_Nasabah_CORP_NegaraLainnya)
                                            FillOrNothing(.Sender_Nasabah_CORP_NoTelp_Old, objIfti.Sender_Nasabah_CORP_NoTelp)

                                        Case 3
                                            'PengirimNonNasabah

                                            FillOrNothing(.Sender_FK_IFTI_NasabahType_ID_Old, objIfti.Sender_FK_IFTI_NasabahType_ID)
                                            FillOrNothing(.Sender_NonNasabah_NoRekening_Old, objIfti.Sender_NonNasabah_NoRekening)
                                            FillOrNothing(.Sender_NonNasabah_NamaLengkap_Old, objIfti.Sender_NonNasabah_NamaLengkap)
                                            FillOrNothing(.Sender_NonNasabah_NamaBank_Old, objIfti.Sender_NonNasabah_NamaBank)
                                            FillOrNothing(.Sender_NonNasabah_TanggalLahir_Old, objIfti.Sender_NonNasabah_TanggalLahir)
                                            FillOrNothing(.Sender_NonNasabah_ID_Alamat_Old, objIfti.Sender_NonNasabah_ID_Alamat)
                                            FillOrNothing(.Sender_NonNasabah_ID_NegaraBagian_Old, objIfti.Sender_NonNasabah_ID_NegaraBagian)
                                            FillOrNothing(.Sender_NonNasabah_ID_Negara_Old, objIfti.Sender_NonNasabah_ID_Negara)
                                            FillOrNothing(.Sender_NonNasabah_ID_NegaraLainnya_Old, objIfti.Sender_NonNasabah_ID_NegaraLainnya)
                                            FillOrNothing(.Sender_NonNasabah_NoTelp_Old, objIfti.Sender_NonNasabah_NoTelp)

                                    End Select

                                    'cekPengirimNew
                                    Select Case (senderType)
                                        Case 1
                                            'pengirimNasabahIndividu
                                            FillOrNothing(.Sender_FK_IFTI_NasabahType_ID, senderType, False, oInt)
                                            FillOrNothing(.Sender_Nasabah_INDV_NoRekening, Me.txtSwiftInPengirim_rekening.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_Nasabah_INDV_NamaLengkap, Me.txtSwiftInPengirimNasabah_IND_nama.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_Nasabah_INDV_TanggalLahir, Me.txtSwiftInPengirimNasabah_IND_TanggalLahir.Text, False, oInt)
                                            If Me.RbSwiftInPengirimNasabah_IND_Warganegara.SelectedValue <> "" Then
                                                FillOrNothing(.Sender_Nasabah_INDV_KewargaNegaraan, Me.RbSwiftInPengirimNasabah_IND_Warganegara.SelectedValue, False, oInt)
                                            End If
                                            If Me.RbSwiftInPengirimNasabah_IND_Warganegara.SelectedValue = "2" Then
                                                FillOrNothing(.Sender_Nasabah_INDV_Negara, Me.hfSwiftInPengirimNasabah_IND_negara.Value, False, oInt)
                                                If Me.hfSwiftInPengirimNasabah_IND_negara.Value = ParameterNegara.MsSystemParameter_Value Then
                                                    FillOrNothing(.Sender_Nasabah_INDV_NegaraLainnya, Me.txtSwiftInPengirimNasabah_IND_negaraLain.Text, False, Ovarchar)
                                                Else
                                                    .Sender_Nasabah_INDV_NegaraLainnya = ""
                                                End If
                                            Else
                                                FillOrNothing(.Sender_Nasabah_INDV_Negara, Nothing, False, oInt)
                                                FillOrNothing(.Sender_Nasabah_INDV_NegaraLainnya, Nothing, False, Ovarchar)
                                            End If
                                            FillOrNothing(.Sender_Nasabah_INDV_ID_Alamat, Me.txtSwiftInPengirimNasabah_IND_alamatIden.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_Nasabah_INDV_ID_NegaraKota, Me.txtSwiftInPengirimNasabah_IND_negaraBagian.Text, False, oInt)
                                            FillOrNothing(.Sender_Nasabah_INDV_ID_Negara, Me.hfSwiftInPengirimNasabah_IND_negaraIden.Value, False, oInt)
                                            If Me.hfSwiftInPengirimNasabah_IND_negaraIden.Value = ParameterNegara.MsSystemParameter_Value Then
                                                FillOrNothing(.Sender_Nasabah_INDV_ID_NegaraLainnya, Me.txtSwiftInPengirimNasabah_IND_negaraLainIden.Text, False, Ovarchar)
                                            Else
                                                .Sender_Nasabah_INDV_ID_NegaraLainnya = ""
                                            End If
                                            FillOrNothing(.Sender_Nasabah_INDV_NoTelp, Me.txtSwiftInPengirimNasabah_IND_NoTelp.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_Nasabah_INDV_FK_IFTI_IDType, Me.cbo_SwiftInpengirimNas_Ind_jenisidentitas.SelectedValue, False, oInt)
                                            FillOrNothing(.Sender_Nasabah_INDV_NomorID, Me.txtSwiftInPengirimNasabah_IND_NomorIdentitas.Text, False, Ovarchar)
                                        Case 2
                                            'pengirimNasabahKorporasi

                                            FillOrNothing(.Sender_FK_IFTI_NasabahType_ID, senderType, False, oInt)
                                            FillOrNothing(.Sender_Nasabah_CORP_NoRekening, Me.txtSwiftInPengirim_rekening.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_Nasabah_CORP_NamaKorporasi, Me.txtSwiftInPengirimNasabah_Korp_namaKorp.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_Nasabah_CORP_AlamatLengkap, Me.txtSwiftInPengirimNasabah_Korp_AlamatKorp.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_Nasabah_CORP_NegaraKota, Me.txtSwiftInPengirimNasabah_Korp_Kota.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_Nasabah_CORP_Negara, Me.hfSwiftInPengirimNasabah_Korp_NegaraKorp.Value, False, oInt)
                                            If Me.hfSwiftInPengirimNasabah_Korp_NegaraKorp.Value = ParameterNegara.MsSystemParameter_Value Then
                                                FillOrNothing(.Sender_Nasabah_CORP_NegaraLainnya, Me.txtSwiftInPengirimNasabah_Korp_NegaraLainnyaKorp.Text, False, Ovarchar)
                                            Else
                                                .Sender_Nasabah_CORP_NegaraLainnya = ""
                                            End If
                                            FillOrNothing(.Sender_Nasabah_CORP_NoTelp, Me.txtSwiftInPengirimNasabah_Korp_NoTelp.Text, False, Ovarchar)

                                        Case 3
                                            'PengirimNonNasabah
                                            FillOrNothing(.Sender_FK_IFTI_NasabahType_ID, senderType, False, oInt)
                                            FillOrNothing(.Sender_NonNasabah_NoRekening, Me.txtSwiftInPengirimNonNasabah_rekening.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_NonNasabah_NamaLengkap, Me.txtSwiftInPengirimNonNasabah_Nama.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_NonNasabah_NamaBank, Me.txtSwiftInPengirimNonNasabah_namabank.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_NonNasabah_TanggalLahir, Me.txtSwiftInPengirimNonNasabah_TanggalLahir.Text, False, oDate)
                                            FillOrNothing(.Sender_NonNasabah_ID_Alamat, Me.txtSwiftInPengirimNonNasabah_Alamat.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_NonNasabah_ID_NegaraBagian, Me.txtSwiftInPengirimNonNasabah_Kota.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_NonNasabah_ID_Negara, Me.hfSwiftInPengirimNonNasabah_Negara.Value, False, oInt)
                                            If Me.hfSwiftInPengirimNonNasabah_Negara.Value = ParameterNegara.MsSystemParameter_Value Then
                                                FillOrNothing(.Sender_NonNasabah_ID_NegaraLainnya, Me.txtSwiftInPengirimNonNasabah_negaraLain.Text, False, Ovarchar)
                                            Else
                                                .Sender_NonNasabah_ID_NegaraLainnya = ""
                                            End If
                                            FillOrNothing(.Sender_NonNasabah_NoTelp, Me.txtSwiftInPengirimNonNasabah_NoTelp.Text, False, Ovarchar)

                                    End Select
                                    'cekPenerima
                                    'Using objReceiver As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("FK_IFTI_ID=" & getIFTIPK, "", 0, Integer.MaxValue, 0)
                                    '    Dim KeyBeneficiary As Integer = objReceiver(0).PK_IFTI_Beneficiary_ID
                                    '    Dim TipePenerima As Integer = objReceiver(0).FK_IFTI_NasabahType_ID
                                    '    Using objReceiverAppDetail As New IFTI_Approval_Beneficiary ' = DataRepository.IFTI_Approval_BeneficiaryProvider.GetPaged("FK_IFTI_ID=" & getIFTIPK, "", 0, Integer.MaxValue, 0)
                                    '        With objReceiverAppDetail

                                    '            Select Case (TipePenerima)
                                    '                Case 1
                                    '                    validasiReceiver1()

                                    '                    .FK_IFTI_NasabahType_ID = 1
                                    '                    .FK_IFTI_ID = objIfti.PK_IFTI_ID
                                    '                    .FK_IFTI_Approval_Id = KeyHeaderApproval
                                    '                    .FK_IFTI_Beneficiary_ID = KeyBeneficiary

                                    '                    FillOrNothing(.Beneficiary_Nasabah_INDV_NoRekening, Me.txtSwiftInPenerimaNasabah_Rekening.Text)

                                    '                    FillOrNothing(.Beneficiary_Nasabah_INDV_NamaLengkap, Me.txtSwiftInPenerimaNasabah_IND_nama.Text, False, Ovarchar)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_INDV_TanggalLahir, Me.txtSwiftInPenerimaNasabah_IND_TanggalLahir.Text, False, oDate)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_INDV_KewargaNegaraan, Me.RbSwiftInPenerimaNasabah_IND_Kewarganegaraan.SelectedValue, False, oInt)
                                    '                    If RbSwiftInPenerimaNasabah_IND_Kewarganegaraan.SelectedValue = "2" Then
                                    '                        If ObjectAntiNull(Me.txtSwiftInPenerimaNasabah_IND_negara.Text) = True Then
                                    '                            FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Negara, Me.hfSwiftInPenerimaNasabah_IND_negara.Value, False, oInt)
                                    '                            FillOrNothing(.Beneficiary_Nasabah_INDV_NegaraLainnya, Me.txtSwiftInPenerimaNasabah_IND_negaralain.Text, False, Ovarchar)
                                    '                        Else
                                    '                            FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Negara, Nothing, False, oInt)
                                    '                            FillOrNothing(.Beneficiary_Nasabah_INDV_NegaraLainnya, Me.txtSwiftInPenerimaNasabah_IND_negaralain.Text, False, Ovarchar)
                                    '                        End If
                                    '                    Else
                                    '                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Negara, Nothing, False, oInt)
                                    '                        FillOrNothing(.Beneficiary_Nasabah_INDV_NegaraLainnya, Nothing, False, Ovarchar)
                                    '                    End If
                                    '                    'FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Negara, Me.hfSwiftInPenerimaNasabah_IND_negara.Value, False, oInt)
                                    '                    'FillOrNothing(.Beneficiary_Nasabah_INDV_NegaraLainnya, Me.txtSwiftInPenerimaNasabah_IND_negaralain.Text, False, Ovarchar)
                                    '                    'FillOrNothing(.Beneficiary_Nasabah_INDV_Pekerjaan, Me.hfSwiftInIdenPenerimaNas_Ind_pekerjaan.Value, False, oInt)
                                    '                    'FillOrNothing(.Beneficiary_Nasabah_INDV_PekerjaanLainnya, TxtSwiftInIdenPenerimaNas_Ind_Pekerjaanlain.Text, False, Ovarchar)
                                    '                    If ObjectAntiNull(Me.txtSwiftInPenerimaNasabah_IND_pekerjaan.Text) = True Then
                                    '                        FillOrNothing(.Beneficiary_Nasabah_INDV_Pekerjaan, Me.hfSwiftInIdenPenerimaNas_Ind_pekerjaan.Value, False, oInt)
                                    '                        FillOrNothing(.Beneficiary_Nasabah_INDV_PekerjaanLainnya, TxtSwiftInIdenPenerimaNas_Ind_Pekerjaanlain.Text, False, Ovarchar)
                                    '                    Else
                                    '                        FillOrNothing(.Beneficiary_Nasabah_INDV_Pekerjaan, Nothing, False, oInt)
                                    '                        FillOrNothing(.Beneficiary_Nasabah_INDV_PekerjaanLainnya, TxtSwiftInIdenPenerimaNas_Ind_Pekerjaanlain.Text, False, Ovarchar)
                                    '                    End If

                                    '                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Alamat_Dom, Me.TxtSwiftInIdenPenerimaNas_Ind_Alamat.Text, False, Ovarchar)
                                    '                    If ObjectAntiNull(Me.TxtSwiftInIdenPenerimaNas_Ind_Kota.Text) = True Then
                                    '                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKab_Dom, Me.hfSwiftInIdenPenerimaNas_Ind_Kota_dom.Value, False, oInt)
                                    '                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom, TxtSwiftInIdenPenerimaNas_Ind_kotalain.Text, False, Ovarchar)
                                    '                    Else
                                    '                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKab_Dom, Nothing, False, oInt)
                                    '                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom, TxtSwiftInIdenPenerimaNas_Ind_kotalain.Text, False, Ovarchar)
                                    '                    End If
                                    '                    If ObjectAntiNull(Me.TxtSwiftInIdenPenerimaNas_Ind_provinsi.Text) = True Then
                                    '                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Propinsi_Dom, Me.hfSwiftInIdenPenerimaNas_Ind_provinsi_dom.Value, False, oInt)
                                    '                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom, Me.TxtSwiftInIdenPenerimaNas_Ind_provinsiLain.Text, False, Ovarchar)
                                    '                    Else
                                    '                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Propinsi_Dom, Nothing, False, oInt)
                                    '                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom, Me.TxtSwiftInIdenPenerimaNas_Ind_provinsiLain.Text, False, Ovarchar)
                                    '                    End If
                                    '                    'FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKab_Dom, Me.hfSwiftInIdenPenerimaNas_Ind_Kota_dom.Value, False, oInt)
                                    '                    'FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom, TxtSwiftInIdenPenerimaNas_Ind_Kota.Text, False, Ovarchar)
                                    '                    'FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Propinsi_Dom, Me.hfSwiftInIdenPenerimaNas_Ind_provinsi_dom.Value, False, oInt)
                                    '                    'FillOrNothing(.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom, Me.TxtSwiftInIdenPenerimaNas_Ind_provinsiLain.Text, False, Ovarchar)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Alamat_Iden, Me.TxtSwiftInIdenPenerimaNas_Ind_alamatIdentitas.Text, False, Ovarchar)
                                    '                    If ObjectAntiNull(Me.TxtSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Text) = True Then
                                    '                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKab_Iden, Me.hfSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Value, False, oInt)
                                    '                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden, TxtSwiftInIdenPenerimaNas_Ind_KotaLainIden.Text, False, Ovarchar)
                                    '                    Else
                                    '                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKab_Iden, Nothing, False, oInt)
                                    '                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden, TxtSwiftInIdenPenerimaNas_Ind_KotaLainIden.Text, False, Ovarchar)
                                    '                    End If
                                    '                    If ObjectAntiNull(Me.TxtSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Text) = True Then
                                    '                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Propinsi_Iden, Me.hfSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Value, False, oInt)
                                    '                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden, Me.TxtSwiftInIdenPenerimaNas_Ind_ProvinsilainIden.Text, False, Ovarchar)
                                    '                    Else
                                    '                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Propinsi_Iden, Nothing, False, oInt)
                                    '                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden, Me.TxtSwiftInIdenPenerimaNas_Ind_ProvinsilainIden.Text, False, Ovarchar)
                                    '                    End If
                                    '                    'FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKab_Iden, Me.hfSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Value, False, oInt)
                                    '                    'FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden, TxtSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Text, False, Ovarchar)
                                    '                    'FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Propinsi_Iden, Me.hfSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Value, False, oInt)
                                    '                    'FillOrNothing(.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden, Me.TxtSwiftInIdenPenerimaNas_Ind_ProvinsilainIden.Text, False, Ovarchar)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_INDV_NoTelp, Me.TxtISwiftIndenPenerimaNas_Ind_noTelp.Text, False, Ovarchar)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_INDV_FK_IFTI_IDType, cbo_SwiftInpenerimaNas_Ind_jenisidentitas.SelectedValue, False, oInt)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_INDV_NomorID, txtSwiftInPengirimNasabah_IND_NomorIdentitas.Text, False, Ovarchar)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan, txtSwiftInIdenPenerimaNas_IND_NilaiTransaksiKeuangan.Text, False, oDecimal)
                                    '                Case 2
                                    '                    validasiReceiver2()
                                    '                    .FK_IFTI_NasabahType_ID = 2
                                    '                    .FK_IFTI_ID = objIfti.PK_IFTI_ID
                                    '                    .FK_IFTI_Approval_Id = KeyHeaderApproval
                                    '                    .FK_IFTI_Beneficiary_ID = KeyBeneficiary
                                    '                    FillOrNothing(.Beneficiary_Nasabah_CORP_NoRekening, Me.txtSwiftInPenerimaNasabah_Rekening.Text)

                                    '                    FillOrNothing(.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id, cbo_SwiftInPenerimaNasabah_Korp_BentukBadanUsaha.SelectedValue, False, oInt)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya, Me.txtSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLainnya.Text, False, Ovarchar)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_CORP_NamaKorporasi, Me.txtSwiftInPenerimaNasabah_Korp_namaKorp.Text, False, Ovarchar)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id, Me.hfSwiftInPenerimaNasabah_Korp_BidangUsahaKorp.Value, False, oInt)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_CORP_BidangUsahaLainnya, Me.txtSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLainnya.Text, False, Ovarchar)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_CORP_AlamatLengkap, Me.txtSwiftInPenerimaNasabah_Korp_AlamatKorp.Text, TipePenerima, Ovarchar)
                                    '                    'FillOrNothing(.Beneficiary_Nasabah_CORP_ID_KotaKab, Me.hfSwiftInPenerimaNasabah_Korp_kotakab.Value, False, oInt)
                                    '                    'FillOrNothing(.Beneficiary_Nasabah_CORP_ID_KotaKabLainnya, Me.txtSwiftInPenerimaNasabah_Korp_kotaLain.Text, False, Ovarchar)
                                    '                    'FillOrNothing(.Beneficiary_Nasabah_CORP_ID_Propinsi, Me.hfSwiftInPenerimaNasabah_Korp_propinsi.Value, False, oInt)
                                    '                    'FillOrNothing(.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya, txtSwiftInPenerimaNasabah_Korp_propinsilain.Text, False, Ovarchar)
                                    '                    If ObjectAntiNull(Me.txtSwiftInPenerimaNasabah_Korp_Kotakab.Text) = True Then
                                    '                        FillOrNothing(.Beneficiary_Nasabah_CORP_ID_KotaKab, Me.hfSwiftInPenerimaNasabah_Korp_kotakab.Value, False, oInt)
                                    '                        FillOrNothing(.Beneficiary_Nasabah_CORP_ID_KotaKabLainnya, Me.txtSwiftInPenerimaNasabah_Korp_kotaLain.Text, False, Ovarchar)
                                    '                    Else
                                    '                        FillOrNothing(.Beneficiary_Nasabah_CORP_ID_KotaKab, Nothing, False, oInt)
                                    '                        FillOrNothing(.Beneficiary_Nasabah_CORP_ID_KotaKabLainnya, Me.txtSwiftInPenerimaNasabah_Korp_kotaLain.Text, False, Ovarchar)
                                    '                    End If
                                    '                    If ObjectAntiNull(Me.txtSwiftInPenerimaNasabah_Korp_propinsi.Text) = True Then
                                    '                        FillOrNothing(.Beneficiary_Nasabah_CORP_ID_Propinsi, Me.hfSwiftInPenerimaNasabah_Korp_propinsi.Value, False, oInt)
                                    '                        FillOrNothing(.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya, txtSwiftInPenerimaNasabah_Korp_propinsilain.Text, False, Ovarchar)
                                    '                    Else
                                    '                        FillOrNothing(.Beneficiary_Nasabah_CORP_ID_Propinsi, Nothing, False, oInt)
                                    '                        FillOrNothing(.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya, txtSwiftInPenerimaNasabah_Korp_propinsilain.Text, False, Ovarchar)
                                    '                    End If
                                    '                    FillOrNothing(.Beneficiary_Nasabah_CORP_NoTelp, txtSwiftInPenerimaNasabah_Korp_NoTelp.Text, False, Ovarchar)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan, txtSwiftInPenerimaNasabah_Korp_NilaiTransaksiKeuangan.Text, False, oDecimal)
                                    '                Case 3
                                    '                    validasiReceiver3()
                                    '                    .FK_IFTI_NasabahType_ID = 3
                                    '                    .FK_IFTI_ID = objIfti.PK_IFTI_ID
                                    '                    .FK_IFTI_Approval_Id = KeyHeaderApproval
                                    '                    .FK_IFTI_Beneficiary_ID = KeyBeneficiary

                                    '                    FillOrNothing(.Beneficiary_NonNasabah_NamaLengkap, Me.TxtSwiftInPenerimaNonNasabah_nama.Text, False, Ovarchar)
                                    '                    FillOrNothing(.Beneficiary_NonNasabah_TanggalLahir, Me.TxtSwiftInPenerimaNonNasabah_TanggalLahir.Text, False, oDate)
                                    '                    FillOrNothing(.Beneficiary_NonNasabah_ID_Alamat, Me.TxtSwiftInPenerimaNonNasabah_alamatiden.Text, False, Ovarchar)
                                    '                    FillOrNothing(.Beneficiary_NonNasabah_NoTelp, Me.TxtSwiftInPenerimaNonNasabah_NoTelepon.Text, False, Ovarchar)
                                    '                    FillOrNothing(.Beneficiary_NonNasabah_FK_IFTI_IDType, Me.CboSwiftInPenerimaNonNasabah_JenisDokumen.SelectedValue, False, oInt)
                                    '                    FillOrNothing(.Beneficiary_NonNasabah_NomorID, Me.TxtSwiftInPenerimaNonNasabah_NomorIden.Text, False, Ovarchar)
                                    '                    FillOrNothing(.Beneficiary_NonNasabah_NilaiTransaksikeuangan, Me.TxtSwiftInPenerimaNonNasabah_NilaiTransaksiKeuangan.Text, False, oDecimal)

                                    '                Case 4
                                    '                    validasiReceiver4()
                                    '                    .FK_IFTI_NasabahType_ID = 4
                                    '                    .FK_IFTI_ID = objIfti.PK_IFTI_ID
                                    '                    .FK_IFTI_Approval_Id = KeyHeaderApproval
                                    '                    .FK_IFTI_Beneficiary_ID = KeyBeneficiary

                                    '                    FillOrNothing(.Beneficiary_Nasabah_INDV_NoRekening, Me.TxtSwiftInPenerimaPenerus_Rekening.Text, False, Ovarchar)

                                    '                    FillOrNothing(.Beneficiary_Nasabah_INDV_NamaBank, Me.TxtSwiftInPenerimaPenerus_Ind_namaBank.Text, False, Ovarchar)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_INDV_NamaLengkap, Me.TxtSwiftInPenerimaPenerus_Ind_nama.Text, False, Ovarchar)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_INDV_TanggalLahir, Me.TxtSwiftInPenerimaPenerus_Ind_TanggalLahir.Text, False, oDate)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_INDV_KewargaNegaraan, Me.RBSwiftInPenerimaPenerus_Ind_Kewarganegaraan.SelectedValue, False, oInt)
                                    '                    'FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Negara, Me.hfSwiftInPenerimaPenerus_IND_negara.Value, False, oInt)
                                    '                    'FillOrNothing(.Beneficiary_Nasabah_INDV_NegaraLainnya, Me.TxtSwiftInPenerimaPenerus_IND_negaralain.Text, False, Ovarchar)
                                    '                    If RBSwiftInPenerimaPenerus_Ind_Kewarganegaraan.SelectedValue = "2" Then
                                    '                        If ObjectAntiNull(Me.TxtSwiftInPenerimaPenerus_IND_negara.Text) = True Then
                                    '                            FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Negara, Me.hfSwiftInPenerimaPenerus_IND_negara.Value, False, oInt)
                                    '                            FillOrNothing(.Beneficiary_Nasabah_INDV_NegaraLainnya, Me.TxtSwiftInPenerimaPenerus_IND_negaralain.Text, False, Ovarchar)
                                    '                        Else
                                    '                            FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Negara, Nothing, False, oInt)
                                    '                            FillOrNothing(.Beneficiary_Nasabah_INDV_NegaraLainnya, Me.TxtSwiftInPenerimaPenerus_IND_negaralain.Text, False, Ovarchar)
                                    '                        End If
                                    '                    Else
                                    '                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Negara, Nothing, False, oInt)
                                    '                        FillOrNothing(.Beneficiary_Nasabah_INDV_NegaraLainnya, Nothing, False, Ovarchar)
                                    '                    End If
                                    '                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Alamat_Dom, Me.TxtSwiftInPenerimaPenerus_IND_alamatDom.Text, False, Ovarchar)
                                    '                    If ObjectAntiNull(Me.TxtSwiftInPenerimaPenerus_IND_kotaDom.Text) = True Then
                                    '                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKab_Dom, Me.hfSwiftInPenerimaPenerus_IND_kotaDom.Value, False, oInt)
                                    '                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom, TxtSwiftInPenerimaPenerus_IND_kotaLainDom.Text, False, Ovarchar)
                                    '                    Else
                                    '                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKab_Dom, Nothing, False, oInt)
                                    '                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom, TxtSwiftInPenerimaPenerus_IND_kotaLainDom.Text, False, Ovarchar)
                                    '                    End If
                                    '                    If ObjectAntiNull(Me.TxtSwiftInPenerimaPenerus_IND_ProvDom.Text) = True Then
                                    '                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Propinsi_Dom, Me.hfSwiftInPenerimaPenerus_IND_ProvDom.Value, False, oInt)
                                    '                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom, Me.TxtSwiftInPenerimaPenerus_IND_ProvLainDom.Text, False, Ovarchar)
                                    '                    Else
                                    '                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Propinsi_Dom, Nothing, False, oInt)
                                    '                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom, Me.TxtSwiftInPenerimaPenerus_IND_ProvLainDom.Text, False, Ovarchar)
                                    '                    End If
                                    '                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Alamat_Iden, Me.TxtSwiftInPenerimaPenerus_IND_alamatIden.Text, False, Ovarchar)
                                    '                    If ObjectAntiNull(Me.TxtSwiftInPenerimaPenerus_IND_kotaIden.Text) = True Then
                                    '                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKab_Iden, Me.hfSwiftInPenerimaPenerus_IND_kotaIden.Value, False, oInt)
                                    '                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden, TxtSwiftInPenerimaPenerus_IND_kotaIden.Text, False, Ovarchar)
                                    '                    Else
                                    '                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKab_Iden, Nothing, False, oInt)
                                    '                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden, TxtSwiftInPenerimaPenerus_IND_kotaIden.Text, False, Ovarchar)
                                    '                    End If

                                    '                    If ObjectAntiNull(Me.TxtSwiftInPenerimaPenerus_IND_ProvIden.Text) = True Then
                                    '                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Propinsi_Iden, Me.hfSwiftInPenerimaPenerus_IND_ProvIden.Value, False, oInt)
                                    '                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden, Me.TxtSwiftInPenerimaPenerus_IND_ProvIden.Text, False, Ovarchar)
                                    '                    Else
                                    '                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Propinsi_Iden, Nothing, False, oInt)
                                    '                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden, Me.TxtSwiftInPenerimaPenerus_IND_ProvIden.Text, False, Ovarchar)
                                    '                    End If
                                    '                    FillOrNothing(.Beneficiary_Nasabah_INDV_NoTelp, Me.TxtSwiftInPenerimaPenerus_IND_NoTelp.Text, False, Ovarchar)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_INDV_FK_IFTI_IDType, Me.cbo_SwiftInpenerimaNas_Ind_jenisidentitas.SelectedValue, False, oInt)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_INDV_NomorID, Me.TxtSwiftInPenerimaPenerus_IND_nomoridentitas.Text, False, Ovarchar)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan, Me.TxtSwiftInPenerimaPenerus_IND_NIlaiTransaksi.Text, False, oDecimal)
                                    '                Case 5
                                    '                    validasiReceiver5()
                                    '                    .FK_IFTI_NasabahType_ID = 5
                                    '                    .FK_IFTI_ID = objIfti.PK_IFTI_ID
                                    '                    .FK_IFTI_Approval_Id = KeyHeaderApproval
                                    '                    .FK_IFTI_Beneficiary_ID = KeyBeneficiary
                                    '                    FillOrNothing(.Beneficiary_Nasabah_CORP_NoRekening, Me.TxtSwiftInPenerimaPenerus_Rekening.Text)

                                    '                    FillOrNothing(.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id, cbo_SwiftInPenerimaPenerus_Korp_BentukBadanUsaha.SelectedValue, False, oInt)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya, Me.txtSwiftInPenerimaPenerus_Korp_BentukBadanUsahaLain.Text, False, Ovarchar)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_CORP_NamaKorporasi, Me.txtSwiftInPenerimaPenerus_Korp_NamaKorp.Text, False, Ovarchar)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id, Me.hfSwiftInPenerimaPenerus_Korp_BidangUsahaKorp.Value, False, oInt)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_CORP_BidangUsahaLainnya, Me.txtSwiftInPenerimaPenerus_Korp_BentukBadanUsahaLain.Text, False, Ovarchar)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_CORP_AlamatLengkap, Me.txtSwiftInPenerimaPenerus_Korp_alamatkorp.Text, False, Ovarchar)
                                    '                    If ObjectAntiNull(Me.txtSwiftInPenerimaPenerus_Korp_kotakorp.Text) = True Then
                                    '                        FillOrNothing(.Beneficiary_Nasabah_CORP_ID_KotaKab, Me.hfSwiftInPenerimaPenerus_Korp_kotakorp.Value, False, oInt)
                                    '                        FillOrNothing(.Beneficiary_Nasabah_CORP_ID_KotaKabLainnya, Me.txtSwiftInPenerimaPenerus_Korp_kotalainnyakorp.Text, False, Ovarchar)
                                    '                    Else
                                    '                        FillOrNothing(.Beneficiary_Nasabah_CORP_ID_KotaKab, Nothing, False, oInt)
                                    '                        FillOrNothing(.Beneficiary_Nasabah_CORP_ID_KotaKabLainnya, Me.txtSwiftInPenerimaPenerus_Korp_kotalainnyakorp.Text, False, Ovarchar)

                                    '                    End If
                                    '                    If ObjectAntiNull(Me.txtSwiftInPenerimaPenerus_Korp_ProvinsiKorp.Text) = True Then
                                    '                        FillOrNothing(.Beneficiary_Nasabah_CORP_ID_Propinsi, Me.hfSwiftInPenerimaPenerus_Korp_ProvinsiKorp.Value, False, oInt)
                                    '                        FillOrNothing(.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya, txtSwiftInPenerimaPenerus_Korp_provinsiLainnyaKorp.Text, False, Ovarchar)
                                    '                    Else
                                    '                        FillOrNothing(.Beneficiary_Nasabah_CORP_ID_Propinsi, Nothing, False, oInt)
                                    '                        FillOrNothing(.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya, txtSwiftInPenerimaPenerus_Korp_provinsiLainnyaKorp.Text, False, Ovarchar)
                                    '                    End If
                                    '                    FillOrNothing(.Beneficiary_Nasabah_CORP_NoTelp, Me.txtSwiftInPenerimaPenerus_Korp_NoTelp.Text, False, Ovarchar)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan, Me.txtSwiftInPenerimaPenerus_Korp_NilaiTransaksi.Text, False, oDecimal)

                                    '            End Select
                                    '        End With
                                    '        'save beneficiary
                                    '        DataRepository.IFTI_Approval_BeneficiaryProvider.Save(OTrans, objReceiverAppDetail)
                                    '    End Using


                                    'End Using
                                    Dim result As ArrayList
                                    Dim i As Integer
                                    Dim dt As Data.DataTable = ReceiverDataTable
                                    Dim rowCount As Integer = dt.Rows.Count

                                    If (rowCount > 0) Then
                                        result = New ArrayList
                                    End If

                                    For i = 0 To rowCount - 1
                                        Dim objReceiverAppDetail As New IFTI_Approval_Beneficiary
                                        Dim KeyBeneficiary As Integer '= dt.Rows(i)("PK_IFTI_Beneficiary_ID")
                                        If ObjectAntiNull(dt.Rows(i)("PK_IFTI_Beneficiary_ID")) Then
                                            KeyBeneficiary = dt.Rows(i)("PK_IFTI_Beneficiary_ID")
                                        End If
                                        Dim TipePenerima As Integer = dt.Rows(i)("FK_IFTI_NasabahType_ID")
                                        With objReceiverAppDetail
                                            '.ChronologyDate = CDate(dt.Rows(i)("ChronologyDate").ToString)
                                            '.Description = dt.Rows(i)("Description").ToString
                                            '.ReportedBy = dt.Rows(i)("ReportedBy").ToString()
                                            '.ReportedWorkUnit = dt.Rows(i)("ReportedWorkUnit").ToString()
                                            '.OwnershipIssue = txtChronologyOwnershipIssue.Text
                                            '.OwnershipIssueUnit = txtChronologyOwnershipIssueUnit.Text

                                            Select Case (TipePenerima)
                                                Case 1
                                                    'validasiReceiverInd()
                                                    .FK_IFTI_NasabahType_ID = 1
                                                    .FK_IFTI_ID = objIfti.PK_IFTI_ID
                                                    .FK_IFTI_Approval_Id = KeyHeaderApproval
                                                    If ObjectAntiNull(dt.Rows(i)("PK_IFTI_Beneficiary_ID")) Then
                                                        .FK_IFTI_Beneficiary_ID = KeyBeneficiary
                                                        .PJKBank_type = Me.cboSwiftInPenerima_TipePJKBank.SelectedValue
                                                    End If

                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_NoRekening, dt.Rows(i)("Beneficiary_Nasabah_INDV_NoRekening").ToString)

                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_NamaLengkap, dt.Rows(i)("Beneficiary_Nasabah_INDV_NamaLengkap").ToString, False, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_TanggalLahir, dt.Rows(i)("Beneficiary_Nasabah_INDV_TanggalLahir").ToString, False, oDate)
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_KewargaNegaraan, dt.Rows(i)("Beneficiary_Nasabah_INDV_KewargaNegaraan"), False, oInt)
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_Negara, dt.Rows(i)("Beneficiary_Nasabah_INDV_Negara"), False, oInt)
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_NegaraLainnya, dt.Rows(i)("Beneficiary_Nasabah_INDV_NegaraLainnya").ToString, False, Ovarchar)

                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_Pekerjaan, dt.Rows(i)("Beneficiary_Nasabah_INDV_Pekerjaan"), False, oInt)
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_PekerjaanLainnya, dt.Rows(i)("Beneficiary_Nasabah_INDV_PekerjaanLainnya").ToString, False, Ovarchar)


                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Alamat_Dom, dt.Rows(i)("Beneficiary_Nasabah_INDV_ID_Alamat_Dom").ToString, False, Ovarchar)
                                                    'If ObjectAntiNull(Me.TxtSwiftInIdenPenerimaNas_Ind_Kota.Text) = True Then
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKab_Dom, dt.Rows(i)("Beneficiary_Nasabah_INDV_ID_KotaKab_Dom"), False, oInt)
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom, dt.Rows(i)("Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom").ToString, False, Ovarchar)
                                                    'Else
                                                    '    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKab_Dom, Nothing, False, oInt)
                                                    '    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom, TxtSwiftInIdenPenerimaNas_Ind_kotalain.Text, False, Ovarchar)
                                                    'End If
                                                    'If ObjectAntiNull(Me.TxtSwiftInIdenPenerimaNas_Ind_provinsi.Text) = True Then
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Propinsi_Dom, dt.Rows(i)("Beneficiary_Nasabah_INDV_ID_Propinsi_Dom"), False, oInt)
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom, dt.Rows(i)("Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom").ToString, False, Ovarchar)
                                                    'Else
                                                    '    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Propinsi_Dom, Nothing, False, oInt)
                                                    '    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom, Me.TxtSwiftInIdenPenerimaNas_Ind_provinsiLain.Text, False, Ovarchar)
                                                    'End If
                                                    'FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKab_Dom, Me.hfSwiftInIdenPenerimaNas_Ind_Kota_dom.Value, False, oInt)
                                                    'FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom, TxtSwiftInIdenPenerimaNas_Ind_Kota.Text, False, Ovarchar)
                                                    'FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Propinsi_Dom, Me.hfSwiftInIdenPenerimaNas_Ind_provinsi_dom.Value, False, oInt)
                                                    'FillOrNothing(.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom, Me.TxtSwiftInIdenPenerimaNas_Ind_provinsiLain.Text, False, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Alamat_Iden, dt.Rows(i)("Beneficiary_Nasabah_INDV_ID_Alamat_Iden").ToString, True, Ovarchar)
                                                    'If ObjectAntiNull(Me.TxtSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Text) = True Then
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKab_Iden, dt.Rows(i)("Beneficiary_Nasabah_INDV_ID_KotaKab_Iden"), False, oInt)
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden, dt.Rows(i)("Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden").ToString, False, Ovarchar)
                                                    'Else
                                                    '    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKab_Iden, Nothing, False, oInt)
                                                    '    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden, TxtSwiftInIdenPenerimaNas_Ind_KotaLainIden.Text, False, Ovarchar)
                                                    'End If
                                                    'If ObjectAntiNull(Me.TxtSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Text) = True Then
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Propinsi_Iden, dt.Rows(i)("Beneficiary_Nasabah_INDV_ID_Propinsi_Iden"), False, oInt)
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden, dt.Rows(i)("Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden").ToString, False, Ovarchar)
                                                    'Else
                                                    'FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Propinsi_Iden, Nothing, False, oInt)
                                                    'FillOrNothing(.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden, Me.TxtSwiftInIdenPenerimaNas_Ind_ProvinsilainIden.Text, False, Ovarchar)
                                                    'End If
                                                    'FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKab_Iden, Me.hfSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Value, False, oInt)
                                                    'FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden, TxtSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Text, False, Ovarchar)
                                                    'FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Propinsi_Iden, Me.hfSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Value, False, oInt)
                                                    'FillOrNothing(.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden, Me.TxtSwiftInIdenPenerimaNas_Ind_ProvinsilainIden.Text, False, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_NoTelp, dt.Rows(i)("Beneficiary_Nasabah_INDV_NoTelp".ToString), False, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_FK_IFTI_IDType, dt.Rows(i)("Beneficiary_Nasabah_INDV_FK_IFTI_IDType"), True, oInt)
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_NomorID, dt.Rows(i)("Beneficiary_Nasabah_INDV_NomorID").ToString, True, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan, dt.Rows(i)("Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan"), True, oDecimal)
                                                Case 2
                                                    'validasiReceiverKorp()
                                                    .FK_IFTI_NasabahType_ID = 2
                                                    .FK_IFTI_ID = objIfti.PK_IFTI_ID
                                                    .FK_IFTI_Approval_Id = KeyHeaderApproval

                                                    If ObjectAntiNull(dt.Rows(i)("PK_IFTI_Beneficiary_ID")) Then
                                                        .FK_IFTI_Beneficiary_ID = KeyBeneficiary
                                                        .PJKBank_type = Me.cboSwiftInPenerima_TipePJKBank.SelectedValue
                                                    End If
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_NoRekening, dt.Rows(i)("Beneficiary_Nasabah_corp_NoRekening").ToString)

                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id, dt.Rows(i)("Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id"), True, oInt)
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya, dt.Rows(i)("Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya").ToString, True, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_NamaKorporasi, dt.Rows(i)("Beneficiary_Nasabah_CORP_NamaKorporasi").ToString, True, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id, dt.Rows(i)("Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id"), True, oInt)
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_BidangUsahaLainnya, dt.Rows(i)("Beneficiary_Nasabah_CORP_BidangUsahaLainnya").ToString, True, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_AlamatLengkap, dt.Rows(i)("Beneficiary_Nasabah_CORP_AlamatLengkap").ToString, TipePenerima, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_ID_NegaraBagian, dt.Rows(i)("Beneficiary_Nasabah_CORP_ID_NegaraBagian"), True, oInt)
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_ID_Negara, dt.Rows(i)("Beneficiary_Nasabah_CORP_ID_Negara"), True, oInt)
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_ID_NegaraLainnya, dt.Rows(i)("Beneficiary_Nasabah_CORP_ID_NegaraLainnya").ToString, True, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan, dt.Rows(i)("Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan"), True, oDecimal)


                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_NoRekening, dt.Rows(i)("Beneficiary_Nasabah_corp_NoRekening").ToString)

                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id, dt.Rows(i)("Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id"), False, oInt)
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya, dt.Rows(i)("Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya").ToString, False, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_NamaKorporasi, dt.Rows(i)("Beneficiary_Nasabah_CORP_NamaKorporasi"), False, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id, dt.Rows(i)("Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id"), False, oInt)
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_BidangUsahaLainnya, dt.Rows(i)("Beneficiary_Nasabah_CORP_BidangUsahaLainnya").ToString, False, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_AlamatLengkap, dt.Rows(i)("Beneficiary_Nasabah_CORP_AlamatLengkap").ToString, TipePenerima, Ovarchar)

                                                    'If ObjectAntiNull(Me.txtSwiftInPenerimaNasabah_Korp_Kotakab.Text) = True Then
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_ID_KotaKab, dt.Rows(i)("Beneficiary_Nasabah_CORP_ID_KotaKab"), False, oInt)
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_ID_KotaKabLainnya, dt.Rows(i)("Beneficiary_Nasabah_CORP_ID_KotaKabLainnya").ToString, False, Ovarchar)
                                                    'Else
                                                    '    FillOrNothing(.Beneficiary_Nasabah_CORP_ID_KotaKab, Nothing, False, oInt)
                                                    '    FillOrNothing(.Beneficiary_Nasabah_CORP_ID_KotaKabLainnya, Me.txtSwiftInPenerimaNasabah_Korp_kotaLain.Text, False, Ovarchar)
                                                    'End If
                                                    'If ObjectAntiNull(Me.txtSwiftInPenerimaNasabah_Korp_propinsi.Text) = True Then
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_ID_Propinsi, dt.Rows(i)("Beneficiary_Nasabah_CORP_ID_Propinsi"), False, oInt)
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya, dt.Rows(i)("Beneficiary_Nasabah_CORP_ID_PropinsiLainnya").ToString, False, Ovarchar)
                                                    'Else
                                                    '    FillOrNothing(.Beneficiary_Nasabah_CORP_ID_Propinsi, Nothing, False, oInt)
                                                    '    FillOrNothing(.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya, txtSwiftInPenerimaNasabah_Korp_propinsilain.Text, False, Ovarchar)
                                                    'End If
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_NoTelp, dt.Rows(i)("Beneficiary_Nasabah_CORP_NoTelp").ToString, False, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan, dt.Rows(i)("Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan"), False, oDecimal)
                                                Case 3
                                                    'validasiReceiverNonNasabah()
                                                    .FK_IFTI_NasabahType_ID = 3
                                                    .FK_IFTI_ID = objIfti.PK_IFTI_ID
                                                    .FK_IFTI_Approval_Id = KeyHeaderApproval

                                                    If ObjectAntiNull(dt.Rows(i)("PK_IFTI_Beneficiary_ID")) Then
                                                        .FK_IFTI_Beneficiary_ID = KeyBeneficiary
                                                        .PJKBank_type = Me.cboSwiftInPenerima_TipePJKBank.SelectedValue
                                                    End If
                                                    FillOrNothing(.Beneficiary_NonNasabah_NamaLengkap, dt.Rows(i)("Beneficiary_NonNasabah_NamaLengkap").ToString, False, Ovarchar)
                                                    FillOrNothing(.Beneficiary_NonNasabah_TanggalLahir, dt.Rows(i)("Beneficiary_NonNasabah_TanggalLahir"), False, oDate)
                                                    FillOrNothing(.Beneficiary_NonNasabah_ID_Alamat, dt.Rows(i)("Beneficiary_NonNasabah_ID_Alamat").ToString, False, Ovarchar)
                                                    FillOrNothing(.Beneficiary_NonNasabah_NoTelp, dt.Rows(i)("Beneficiary_NonNasabah_NoTelp").ToString, False, Ovarchar)
                                                    FillOrNothing(.Beneficiary_NonNasabah_FK_IFTI_IDType, dt.Rows(i)("Beneficiary_NonNasabah_FK_IFTI_IDType"), False, oInt)
                                                    FillOrNothing(.Beneficiary_NonNasabah_NomorID, dt.Rows(i)("Beneficiary_NonNasabah_NomorID").ToString, False, Ovarchar)
                                                    FillOrNothing(.Beneficiary_NonNasabah_NilaiTransaksikeuangan, dt.Rows(i)("Beneficiary_NonNasabah_NilaiTransaksikeuangan"), False, oDecimal)

                                                Case 4
                                                    .FK_IFTI_NasabahType_ID = 4
                                                    .FK_IFTI_ID = objIfti.PK_IFTI_ID
                                                    .FK_IFTI_Approval_Id = KeyHeaderApproval

                                                    If ObjectAntiNull(dt.Rows(i)("PK_IFTI_Beneficiary_ID")) Then
                                                        .FK_IFTI_Beneficiary_ID = KeyBeneficiary
                                                        .PJKBank_type = Me.cboSwiftInPenerima_TipePJKBank.SelectedValue
                                                    End If
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_NoRekening, dt.Rows(i)("Beneficiary_Nasabah_INDV_NoRekening").ToString)


                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_NamaBank, dt.Rows(i)("Beneficiary_Nasabah_INDV_NamaBank").ToString, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_NamaLengkap, dt.Rows(i)("Beneficiary_Nasabah_INDV_NamaLengkap").ToString, False, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_TanggalLahir, dt.Rows(i)("Beneficiary_Nasabah_INDV_TanggalLahir").ToString, False, oDate)
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_KewargaNegaraan, dt.Rows(i)("Beneficiary_Nasabah_INDV_KewargaNegaraan"), False, oInt)
                                                    'FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Negara, Me.hfSwiftInPenerimaPenerus_IND_negara.Value, False, oInt)
                                                    'FillOrNothing(.Beneficiary_Nasabah_INDV_NegaraLainnya, Me.TxtSwiftInPenerimaPenerus_IND_negaralain.Text, False, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Negara, dt.Rows(i)("Beneficiary_Nasabah_INDV_Negara"), False, oInt)
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_NegaraLainnya, dt.Rows(i)("Beneficiary_Nasabah_INDV_NegaraLainnya").ToString, False, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Alamat_Dom, dt.Rows(i)("Beneficiary_Nasabah_INDV_ID_Alamat_Dom").ToString, False, Ovarchar)
                                                    'If ObjectAntiNull(Me.TxtSwiftInIdenPenerimaNas_Ind_Kota.Text) = True Then
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKab_Dom, dt.Rows(i)("Beneficiary_Nasabah_INDV_ID_KotaKab_Dom"), False, oInt)
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom, dt.Rows(i)("Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom").ToString, False, Ovarchar)
                                                    'Else
                                                    '    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKab_Dom, Nothing, False, oInt)
                                                    '    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom, TxtSwiftInIdenPenerimaNas_Ind_kotalain.Text, False, Ovarchar)
                                                    'End If
                                                    'If ObjectAntiNull(Me.TxtSwiftInIdenPenerimaNas_Ind_provinsi.Text) = True Then
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Propinsi_Dom, dt.Rows(i)("Beneficiary_Nasabah_INDV_ID_Propinsi_Dom"), False, oInt)
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom, dt.Rows(i)("Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom").ToString, False, Ovarchar)
                                                    'Else
                                                    '    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Propinsi_Dom, Nothing, False, oInt)
                                                    '    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom, Me.TxtSwiftInPenerimaPenerus_IND_ProvLainDom.Text, False, Ovarchar)
                                                    'End If
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Alamat_Iden, dt.Rows(i)("Beneficiary_Nasabah_INDV_ID_Alamat_Iden").ToString, True, Ovarchar)
                                                    'If ObjectAntiNull(Me.TxtSwiftInPenerimaPenerus_IND_kotaIden.Text) = True Then
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKab_Iden, dt.Rows(i)("Beneficiary_Nasabah_INDV_ID_KotaKab_Iden"), False, oInt)
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden, dt.Rows(i)("Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden").ToString, False, Ovarchar)
                                                    'Else
                                                    '    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKab_Iden, Nothing, False, oInt)
                                                    '    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden, TxtSwiftInPenerimaPenerus_IND_kotaIden.Text, False, Ovarchar)
                                                    'End If

                                                    'If ObjectAntiNull(Me.TxtSwiftInPenerimaPenerus_IND_ProvIden.Text) = True Then
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Propinsi_Iden, dt.Rows(i)("Beneficiary_Nasabah_INDV_ID_Propinsi_Iden"), False, oInt)
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden, dt.Rows(i)("Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden").ToString, False, Ovarchar)
                                                    'Else
                                                    '    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Propinsi_Iden, Nothing, False, oInt)
                                                    '    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden, Me.TxtSwiftInPenerimaPenerus_IND_ProvIden.Text, False, Ovarchar)
                                                    'End If
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_NoTelp, dt.Rows(i)("Beneficiary_Nasabah_INDV_NoTelp").ToString, False, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_FK_IFTI_IDType, dt.Rows(i)("Beneficiary_Nasabah_INDV_FK_IFTI_IDType"), True, oInt)
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_NomorID, dt.Rows(i)("Beneficiary_Nasabah_INDV_NomorID").ToString, True, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan, dt.Rows(i)("Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan"), True, oDecimal)
                                                Case 5
                                                    .FK_IFTI_NasabahType_ID = 2
                                                    .FK_IFTI_ID = objIfti.PK_IFTI_ID
                                                    .FK_IFTI_Approval_Id = KeyHeaderApproval

                                                    If ObjectAntiNull(dt.Rows(i)("PK_IFTI_Beneficiary_ID")) Then
                                                        .FK_IFTI_Beneficiary_ID = KeyBeneficiary
                                                        .PJKBank_type = Me.cboSwiftInPenerima_TipePJKBank.SelectedValue
                                                    End If

                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_NoRekening, dt.Rows(i)("Beneficiary_Nasabah_corp_NoRekening").ToString)

                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id, dt.Rows(i)("Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id"), True, oInt)
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya, dt.Rows(i)("Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya").ToString, True, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_NamaKorporasi, dt.Rows(i)("Beneficiary_Nasabah_CORP_NamaKorporasi"), True, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id, dt.Rows(i)("Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id"), True, oInt)
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_BidangUsahaLainnya, dt.Rows(i)("Beneficiary_Nasabah_CORP_BidangUsahaLainnya").ToString, True, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_AlamatLengkap, dt.Rows(i)("Beneficiary_Nasabah_CORP_AlamatLengkap").ToString, TipePenerima, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_ID_KotaKab, dt.Rows(i)("Beneficiary_Nasabah_CORP_ID_KotaKab"), False, oInt)
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_ID_KotaKabLainnya, dt.Rows(i)("Beneficiary_Nasabah_CORP_ID_KotaKabLainnya").ToString, False, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_ID_Propinsi, dt.Rows(i)("Beneficiary_Nasabah_CORP_ID_Propinsi"), False, oInt)
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya, dt.Rows(i)("Beneficiary_Nasabah_CORP_ID_PropinsiLainnya").ToString, False, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_NoTelp, dt.Rows(i)("Beneficiary_Nasabah_CORP_NoTelp").ToString, False, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan, dt.Rows(i)("Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan"), False, oDecimal)

                                            End Select
                                        End With
                                        'result.Add(objReceiverAppDetail)
                                        'save beneficiary
                                        DataRepository.IFTI_Approval_BeneficiaryProvider.Save(OTrans, objReceiverAppDetail)
                                    Next

                                    'cekTransaksi
                                    '----old----
                                    FillOrNothing(.TanggalTransaksi_Old, objIfti.TanggalTransaksi)
                                    FillOrNothing(.TimeIndication_Old, objIfti.TimeIndication)
                                    FillOrNothing(.SenderReference_Old, objIfti.SenderReference)
                                    FillOrNothing(.BankOperationCode_Old, objIfti.BankOperationCode)
                                    FillOrNothing(.InstructionCode_Old, objIfti.InstructionCode)
                                    FillOrNothing(.KantorCabangPenyelengaraPengirimAsal_Old, objIfti.KantorCabangPenyelengaraPengirimAsal)
                                    FillOrNothing(.TransactionCode_Old, objIfti.TransactionCode)
                                    FillOrNothing(.ValueDate_TanggalTransaksi_Old, objIfti.ValueDate_TanggalTransaksi)
                                    FillOrNothing(.ValueDate_NilaiTransaksi_Old, objIfti.ValueDate_NilaiTransaksi)
                                    FillOrNothing(.ValueDate_FK_Currency_ID_Old, objIfti.ValueDate_FK_Currency_ID)
                                    FillOrNothing(.ValueDate_CurrencyLainnya_Old, objIfti.ValueDate_CurrencyLainnya)
                                    FillOrNothing(.ValueDate_NilaiTransaksiIDR_Old, objIfti.ValueDate_NilaiTransaksiIDR)
                                    FillOrNothing(.Instructed_Currency_Old, objIfti.Instructed_Currency)
                                    FillOrNothing(.Instructed_CurrencyLainnya_Old, objIfti.Instructed_CurrencyLainnya)
                                    FillOrNothing(.Instructed_Amount_Old, objIfti.Instructed_Amount)
                                    FillOrNothing(.ExchangeRate_Old, objIfti.ExchangeRate)
                                    FillOrNothing(.SendingInstitution_Old, objIfti.SendingInstitution)
                                    FillOrNothing(.TujuanTransaksi_Old, objIfti.TujuanTransaksi)
                                    FillOrNothing(.SumberPenggunaanDana_Old, objIfti.SumberPenggunaanDana)

                                    '-----new---
                                    validasiTransaksi()
                                    FillOrNothing(.TanggalTransaksi, Me.Transaksi_SwiftIntanggal0.Text, False, oDate)
                                    FillOrNothing(.TimeIndication, Me.Transaksi_SwiftInwaktutransaksi0.Text, False, oDate)
                                    FillOrNothing(.SenderReference, Me.Transaksi_SwiftInSender0.Text, False, Ovarchar)
                                    FillOrNothing(.BankOperationCode, Me.Transaksi_SwiftInBankOperationCode0.Text, False, Ovarchar)
                                    FillOrNothing(.InstructionCode, Me.Transaksi_SwiftInInstructionCode0.Text, False, Ovarchar)
                                    FillOrNothing(.KantorCabangPenyelengaraPengirimAsal, Me.Transaksi_SwiftInkantorCabangPengirim0.Text, False, Ovarchar)
                                    FillOrNothing(.TransactionCode, Me.Transaksi_SwiftInkodeTipeTransaksi0.Text, False, Ovarchar)
                                    FillOrNothing(.ValueDate_TanggalTransaksi, Me.Transaksi_SwiftInValueTanggalTransaksi.Text, False, oDate)
                                    FillOrNothing(.ValueDate_NilaiTransaksi, Me.Transaksi_SwiftInnilaitransaksi.Text, False, oDecimal)
                                    If Me.hfTransaksi_SwiftInMataUangTransaksi0.Value <> "" Then
                                        Using objCurrency As TList(Of MsCurrency) = DataRepository.MsCurrencyProvider.GetPaged(MsCurrencyColumn.Code.ToString & _
                                                                                                                                                   " like '%" & Me.Transaksi_SwiftInMataUangTransaksi0.Text & "%'", "", 0, Integer.MaxValue, 0)
                                            If objCurrency.Count > 0 Then
                                                FillOrNothing(.ValueDate_FK_Currency_ID, objCurrency(0).IdCurrency, False, oInt)
                                            End If

                                        End Using
                                    End If

                                    'FillOrNothing(.ValueDate_FK_Currency_ID, Me.hfTransaksi_SwiftInMataUangTransaksi0.Value, False, oInt)
                                    FillOrNothing(.ValueDate_CurrencyLainnya_Old, Me.Transaksi_SwiftInMataUangTransaksi0Lainnya.Text, False, Ovarchar)
                                    FillOrNothing(.ValueDate_NilaiTransaksiIDR, Me.Transaksi_SwiftInAmountdalamRupiah0.Text, False, oDecimal)
                                    If Me.hfTransaksi_SwiftIncurrency.Value <> "" Then
                                        Using objCurrency2 As TList(Of MsCurrency) = DataRepository.MsCurrencyProvider.GetPaged(MsCurrencyColumn.Code.ToString & _
                                                                                                                                                      " like '%" & Me.Transaksi_SwiftIncurrency0.Text & "%'", "", 0, Integer.MaxValue, 0)
                                            If objCurrency2.Count > 0 Then
                                                FillOrNothing(.Instructed_Currency, objCurrency2(0).IdCurrency, False, oInt)
                                            End If

                                        End Using
                                    End If

                                    FillOrNothing(.Instructed_CurrencyLainnya_Old, Me.Transaksi_SwiftIncurrencyLainnya.Text, False, Ovarchar)
                                    FillOrNothing(.Instructed_Amount, Me.Transaksi_SwiftIninstructedAmount0.Text, False, oDecimal)
                                    FillOrNothing(.ExchangeRate, Me.Transaksi_SwiftInnilaiTukar0.Text, False, oDecimal)
                                    FillOrNothing(.SendingInstitution, Me.Transaksi_SwiftInsendingInstitution0.Text, False, Ovarchar)
                                    FillOrNothing(.TujuanTransaksi, Me.Transaksi_SwiftInTujuanTransaksi0.Text, False, Ovarchar)
                                    FillOrNothing(.SumberPenggunaanDana, Me.Transaksi_SwiftInSumberPenggunaanDana0.Text, False, Ovarchar)

                                    'cekInformasiLain
                                    '----old---
                                    FillOrNothing(.InformationAbout_SenderCorrespondent_Old, objIfti.InformationAbout_SenderCorrespondent)
                                    FillOrNothing(.InformationAbout_ReceiverCorrespondent_Old, objIfti.InformationAbout_ReceiverCorrespondent)
                                    FillOrNothing(.InformationAbout_Thirdreimbursementinstitution_Old, objIfti.InformationAbout_Thirdreimbursementinstitution)
                                    FillOrNothing(.InformationAbout_IntermediaryInstitution_Old, objIfti.InformationAbout_IntermediaryInstitution)
                                    FillOrNothing(.RemittanceInformation_Old, objIfti.RemittanceInformation)
                                    FillOrNothing(.SendertoReceiverInformation_Old, objIfti.SendertoReceiverInformation)
                                    FillOrNothing(.RegulatoryReporting_Old, objIfti.RegulatoryReporting)
                                    FillOrNothing(.EnvelopeContents_Old, objIfti.EnvelopeContents)

                                    '---new-----
                                    FillOrNothing(.InformationAbout_SenderCorrespondent, Me.InformasiLainnya_SwiftInSender.Text, False, Ovarchar)
                                    FillOrNothing(.InformationAbout_ReceiverCorrespondent, Me.InformasiLainnya_SwiftInreceiver.Text, False, Ovarchar)
                                    FillOrNothing(.InformationAbout_Thirdreimbursementinstitution, Me.InformasiLainnya_SwiftInthirdReimbursement.Text, False, Ovarchar)
                                    FillOrNothing(.InformationAbout_IntermediaryInstitution, Me.InformasiLainnya_SwiftInintermediary.Text, False, Ovarchar)
                                    FillOrNothing(.RemittanceInformation, Me.InformasiLainnya_SwiftInRemittance.Text, False, Ovarchar)
                                    FillOrNothing(.SendertoReceiverInformation, Me.InformasiLainnya_SwiftInSenderToReceiver.Text, False, Ovarchar)
                                    FillOrNothing(.RegulatoryReporting, Me.InformasiLainnya_SwiftInRegulatoryReport.Text, False, Ovarchar)
                                    FillOrNothing(.EnvelopeContents, Me.InformasiLainnya_SwiftInEnvelopeContents.Text, False, Ovarchar)
                                    'saving
                                End With

                                DataRepository.IFTI_Approval_DetailProvider.Save(OTrans, objIfti_ApprovalDetail)
                            End Using
                            ''Send Email

                            ImageButtonSave.Visible = False
                            ImageButtonCancel.Visible = False
                            lblMsg.Text = "Data has been edited and waiting for approval"
                            MultiViewEditSwiftIn.ActiveViewIndex = 1
                        End Using
                    End If
                    OTrans.Commit()
                Catch ex As Exception
                    OTrans.Rollback()
                    Throw
                End Try

            End Using
        Catch ex As Exception
            'LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    'Sub SaveSwiftIncoDirect()

    '    Try
    '        Using OTrans As TransactionManager = New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)
    '            Try
    '                OTrans.BeginTransaction()
    '                Page.Validate("handle")
    '                If Page.IsValid Then
    '                    ValidasiControl()

    '                    '============ Insert Detail Approval

    '                    Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)

    '                        Dim senderType As Integer = objIfti.Sender_FK_IFTI_NasabahType_ID
    '                        With objIfti
    '                            'FK
    '                            .PK_IFTI_ID = objIfti.PK_IFTI_ID
    '                            .FK_IFTI_Type_ID = 2
    '                            'umum
    '                            '--new--
    '                            FillOrNothing(.LTDLNNo, Me.SwiftInUmum_LTDN.Text, False, oInt)
    '                            FillOrNothing(.LTDLNNoKoreksi, Me.SwiftInUmum_LtdnKoreksi.Text, False, oInt)
    '                            FillOrNothing(.TanggalLaporan, Me.SwiftInUmum_TanggalLaporan.Text, False, oDate)
    '                            FillOrNothing(.NamaPJKBankPelapor, Me.SwiftInUmum_NamaPJKBank.Text, False, Ovarchar)
    '                            FillOrNothing(.NamaPejabatPJKBankPelapor, Me.SwiftInUmum_NamaPejabatPJKBank.Text, False, Ovarchar)
    '                            FillOrNothing(.JenisLaporan, Me.RbSwiftInUmum_JenisLaporan.SelectedValue, False, oInt)
    '                            'cekPengirim
    '                            Select Case (senderType)
    '                                Case 1
    '                                    'pengirimNasabahIndividu
    '                                    'cekvalidasi
    '                                    validasiSenderIndividu()

    '                                    FillOrNothing(.Sender_FK_IFTI_NasabahType_ID, senderType, False, oInt)
    '                                    FillOrNothing(.Sender_Nasabah_INDV_NoRekening, Me.txtSwiftInPengirim_rekening.Text, False, Ovarchar)

    '                                    FillOrNothing(.Sender_Nasabah_INDV_NamaLengkap, Me.txtSwiftInPengirimNasabah_IND_nama.Text, False, Ovarchar)
    '                                    FillOrNothing(.Sender_Nasabah_INDV_TanggalLahir, Me.txtSwiftInPengirimNasabah_IND_TanggalLahir.Text, False, oInt)
    '                                    FillOrNothing(.Sender_Nasabah_INDV_KewargaNegaraan, Me.RbSwiftInPengirimNasabah_IND_Warganegara.SelectedValue, False, oInt)
    '                                    If Me.RbSwiftInPengirimNasabah_IND_Warganegara.SelectedValue = "2" Then
    '                                        If ObjectAntiNull(txtSwiftInPengirimNasabah_IND_negara.Text) = True Then
    '                                            FillOrNothing(.Sender_Nasabah_INDV_Negara, Me.hfSwiftInPengirimNasabah_IND_negara.Value, False, oInt)
    '                                            FillOrNothing(.Sender_Nasabah_INDV_NegaraLainnya, Me.txtSwiftInPengirimNasabah_IND_negaraLain.Text, False, Ovarchar)
    '                                        Else
    '                                            .Sender_Nasabah_INDV_Negara = Nothing
    '                                            FillOrNothing(.Sender_Nasabah_INDV_NegaraLainnya, Me.txtSwiftInPengirimNasabah_IND_negaraLain.Text, False, Ovarchar)
    '                                        End If
    '                                    Else
    '                                        FillOrNothing(.Sender_Nasabah_INDV_Negara, Nothing, False, oInt)
    '                                        FillOrNothing(.Sender_Nasabah_INDV_NegaraLainnya, Nothing, False, Ovarchar)
    '                                    End If
    '                                    FillOrNothing(.Sender_Nasabah_INDV_ID_Alamat, Me.txtSwiftInPengirimNasabah_IND_alamatIden.Text, False, Ovarchar)
    '                                    FillOrNothing(.Sender_Nasabah_INDV_ID_NegaraKota, Me.txtSwiftInPengirimNasabah_IND_negaraBagian.Text, False, oInt)
    '                                    If ObjectAntiNull(Me.txtSwiftInPengirimNasabah_IND_negaraIden.Text) = True Then
    '                                        FillOrNothing(.Sender_Nasabah_INDV_ID_Negara, Me.hfSwiftInPengirimNasabah_IND_negaraIden.Value, False, oInt)
    '                                        FillOrNothing(.Sender_Nasabah_INDV_ID_NegaraLainnya, Me.txtSwiftInPengirimNasabah_IND_negaraLainIden.Text, False, Ovarchar)
    '                                    Else
    '                                        FillOrNothing(.Sender_Nasabah_INDV_ID_Negara, Nothing, False, oInt)
    '                                        FillOrNothing(.Sender_Nasabah_INDV_ID_NegaraLainnya, Me.txtSwiftInPengirimNasabah_IND_negaraLainIden.Text, False, Ovarchar)
    '                                    End If
    '                                    FillOrNothing(.Sender_Nasabah_INDV_NoTelp, Me.txtSwiftInPengirimNasabah_IND_NoTelp.Text, False, Ovarchar)
    '                                    FillOrNothing(.Sender_Nasabah_INDV_FK_IFTI_IDType, Me.cbo_SwiftInpengirimNas_Ind_jenisidentitas.SelectedValue, False, oInt)
    '                                    FillOrNothing(.Sender_Nasabah_INDV_NomorID, Me.txtSwiftInPengirimNasabah_IND_NomorIdentitas.Text, False, Ovarchar)
    '                                Case 2
    '                                    'pengirimNasabahKorporasi
    '                                    'cekvalidasi
    '                                    validasiSenderKorporasi()

    '                                    FillOrNothing(.Sender_FK_IFTI_NasabahType_ID, senderType, False, oInt)
    '                                    FillOrNothing(.Sender_Nasabah_CORP_NoRekening, Me.txtSwiftInPengirim_rekening.Text, False, Ovarchar)

    '                                    FillOrNothing(.Sender_Nasabah_CORP_NamaKorporasi, Me.txtSwiftInPengirimNasabah_Korp_namaKorp.Text, False, Ovarchar)
    '                                    FillOrNothing(.Sender_Nasabah_CORP_AlamatLengkap, Me.txtSwiftInPengirimNasabah_Korp_AlamatKorp.Text, False, Ovarchar)
    '                                    FillOrNothing(.Sender_Nasabah_CORP_NegaraKota, Me.txtSwiftInPengirimNasabah_Korp_Kota.Text, False, Ovarchar)
    '                                    If ObjectAntiNull(Me.txtSwiftInPengirimNasabah_Korp_NegaraKorp.Text) = True Then
    '                                        FillOrNothing(.Sender_Nasabah_CORP_Negara, Me.hfSwiftInPengirimNasabah_Korp_NegaraKorp.Value, False, Ovarchar)
    '                                        FillOrNothing(.Sender_Nasabah_CORP_NegaraLainnya, Me.txtSwiftInPengirimNasabah_Korp_NegaraLainnyaKorp.Text, False, Ovarchar)
    '                                    Else
    '                                        FillOrNothing(.Sender_Nasabah_CORP_Negara, "", False, Ovarchar)
    '                                        FillOrNothing(.Sender_Nasabah_CORP_NegaraLainnya, Me.txtSwiftInPengirimNasabah_Korp_NegaraLainnyaKorp.Text, False, Ovarchar)
    '                                    End If

    '                                    FillOrNothing(.Sender_Nasabah_CORP_NoTelp, Me.txtSwiftInPengirimNasabah_Korp_NoTelp.Text, False, Ovarchar)

    '                                Case 3
    '                                    'PengirimNonNasabah
    '                                    'cekvalidasi
    '                                    validasiSenderNonNasabah()

    '                                    FillOrNothing(.Sender_FK_IFTI_NasabahType_ID, senderType, False, oInt)

    '                                    FillOrNothing(.Sender_NonNasabah_NoRekening, Me.txtSwiftInPengirimNonNasabah_rekening.Text, False, Ovarchar)
    '                                    FillOrNothing(.Sender_NonNasabah_NamaLengkap, Me.txtSwiftInPengirimNonNasabah_Nama.Text, False, Ovarchar)
    '                                    FillOrNothing(.Sender_NonNasabah_NamaBank, Me.txtSwiftInPengirimNonNasabah_namabank.Text, False, Ovarchar)
    '                                    FillOrNothing(.Sender_NonNasabah_TanggalLahir, Me.txtSwiftInPengirimNonNasabah_TanggalLahir.Text, False, oDate)
    '                                    FillOrNothing(.Sender_NonNasabah_ID_Alamat, Me.txtSwiftInPengirimNonNasabah_Alamat.Text, False, Ovarchar)
    '                                    FillOrNothing(.Sender_NonNasabah_ID_NegaraBagian, Me.txtSwiftInPengirimNonNasabah_Kota.Text, False, Ovarchar)
    '                                    If ObjectAntiNull(Me.txtSwiftInPengirimNonNasabah_Negara.Text) = True Then
    '                                        FillOrNothing(.Sender_NonNasabah_ID_Negara, Me.hfSwiftInPengirimNonNasabah_Negara.Value, False, Ovarchar)
    '                                        FillOrNothing(.Sender_NonNasabah_ID_NegaraLainnya, Me.txtSwiftInPengirimNonNasabah_negaraLain.Text, False, Ovarchar)
    '                                    Else
    '                                        FillOrNothing(.Sender_NonNasabah_ID_Negara, Nothing, False, Ovarchar)
    '                                        FillOrNothing(.Sender_NonNasabah_ID_NegaraLainnya, Me.txtSwiftInPengirimNonNasabah_negaraLain.Text, False, Ovarchar)
    '                                    End If
    '                                    FillOrNothing(.Sender_NonNasabah_NoTelp, Me.txtSwiftInPengirimNonNasabah_NoTelp.Text, False, Ovarchar)

    '                            End Select


    '                            'cekPenerima
    '                            Using objReceiver As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("FK_IFTI_ID=" & getIFTIPK, "", 0, Integer.MaxValue, 0)
    '                                Dim KeyBeneficiary As Integer = objReceiver(0).PK_IFTI_Beneficiary_ID
    '                                Dim TipePenerima As Integer = objReceiver(0).FK_IFTI_NasabahType_ID
    '                                With objReceiver(0)
    '                                    Select Case (TipePenerima)
    '                                        Case 1
    '                                            validasiReceiver1()

    '                                            .FK_IFTI_NasabahType_ID = 1
    '                                            .FK_IFTI_ID = objIfti.PK_IFTI_ID

    '                                            FillOrNothing(.Beneficiary_Nasabah_INDV_NoRekening, Me.txtSwiftInPenerimaNasabah_Rekening.Text)

    '                                            FillOrNothing(.Beneficiary_Nasabah_INDV_NamaLengkap, Me.txtSwiftInPenerimaNasabah_IND_nama.Text, False, Ovarchar)
    '                                            FillOrNothing(.Beneficiary_Nasabah_INDV_TanggalLahir, Me.txtSwiftInPenerimaNasabah_IND_TanggalLahir.Text, False, oDate)
    '                                            FillOrNothing(.Beneficiary_Nasabah_INDV_KewargaNegaraan, Me.RbSwiftInPenerimaNasabah_IND_Kewarganegaraan.SelectedValue, False, oInt)
    '                                            If RbSwiftInPenerimaNasabah_IND_Kewarganegaraan.SelectedValue = "2" Then
    '                                                If ObjectAntiNull(Me.txtSwiftInPenerimaNasabah_IND_negara.Text) = True Then
    '                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Negara, Me.hfSwiftInPenerimaNasabah_IND_negara.Value, False, oInt)
    '                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_NegaraLainnya, Me.txtSwiftInPenerimaNasabah_IND_negaralain.Text, False, Ovarchar)
    '                                                Else
    '                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Negara, Nothing, False, oInt)
    '                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_NegaraLainnya, Me.txtSwiftInPenerimaNasabah_IND_negaralain.Text, False, Ovarchar)
    '                                                End If
    '                                            Else
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Negara, Nothing, False, oInt)
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_NegaraLainnya, Nothing, False, Ovarchar)
    '                                            End If
    '                                            If ObjectAntiNull(Me.txtSwiftInPenerimaNasabah_IND_pekerjaan.Text) = True Then
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_Pekerjaan, Me.hfSwiftInIdenPenerimaNas_Ind_pekerjaan.Value, False, oInt)
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_PekerjaanLainnya, TxtSwiftInIdenPenerimaNas_Ind_Pekerjaanlain.Text, False, Ovarchar)
    '                                            Else
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_Pekerjaan, Nothing, False, oInt)
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_PekerjaanLainnya, TxtSwiftInIdenPenerimaNas_Ind_Pekerjaanlain.Text, False, Ovarchar)
    '                                            End If
    '                                            FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Alamat_Dom, Me.TxtSwiftInIdenPenerimaNas_Ind_Alamat.Text, False, Ovarchar)
    '                                            If ObjectAntiNull(Me.TxtSwiftInIdenPenerimaNas_Ind_Kota.Text) = True Then
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKab_Dom, Me.hfSwiftInIdenPenerimaNas_Ind_Kota_dom.Value, False, oInt)
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom, TxtSwiftInIdenPenerimaNas_Ind_kotalain.Text, False, Ovarchar)
    '                                            Else
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKab_Dom, Nothing, False, oInt)
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom, TxtSwiftInIdenPenerimaNas_Ind_kotalain.Text, False, Ovarchar)
    '                                            End If
    '                                            If ObjectAntiNull(Me.TxtSwiftInIdenPenerimaNas_Ind_provinsi.Text) = True Then
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Propinsi_Dom, Me.hfSwiftInIdenPenerimaNas_Ind_provinsi_dom.Value, False, oInt)
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom, Me.TxtSwiftInIdenPenerimaNas_Ind_provinsiLain.Text, False, Ovarchar)
    '                                            Else
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Propinsi_Dom, Nothing, False, oInt)
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom, Me.TxtSwiftInIdenPenerimaNas_Ind_provinsiLain.Text, False, Ovarchar)
    '                                            End If
    '                                            FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Alamat_Iden, Me.TxtSwiftInIdenPenerimaNas_Ind_alamatIdentitas.Text, False, Ovarchar)
    '                                            If ObjectAntiNull(Me.TxtSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Text) = True Then
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKab_Iden, Me.hfSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Value, False, oInt)
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden, TxtSwiftInIdenPenerimaNas_Ind_KotaLainIden.Text, False, Ovarchar)
    '                                            Else
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKab_Iden, Nothing, False, oInt)
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden, TxtSwiftInIdenPenerimaNas_Ind_KotaLainIden.Text, False, Ovarchar)
    '                                            End If
    '                                            If ObjectAntiNull(Me.TxtSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Text) = True Then
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Propinsi_Iden, Me.hfSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Value, False, oInt)
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden, Me.TxtSwiftInIdenPenerimaNas_Ind_ProvinsilainIden.Text, False, Ovarchar)
    '                                            Else
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Propinsi_Iden, Nothing, False, oInt)
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden, Me.TxtSwiftInIdenPenerimaNas_Ind_ProvinsilainIden.Text, False, Ovarchar)
    '                                            End If
    '                                            FillOrNothing(.Beneficiary_Nasabah_INDV_NoTelp, Me.TxtISwiftIndenPenerimaNas_Ind_noTelp.Text, False, Ovarchar)
    '                                            FillOrNothing(.Beneficiary_Nasabah_INDV_FK_IFTI_IDType, cbo_SwiftInpenerimaNas_Ind_jenisidentitas.SelectedValue, False, oInt)
    '                                            FillOrNothing(.Beneficiary_Nasabah_INDV_NomorID, txtSwiftInPengirimNasabah_IND_NomorIdentitas.Text, False, Ovarchar)
    '                                            FillOrNothing(.Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan, txtSwiftInIdenPenerimaNas_IND_NilaiTransaksiKeuangan.Text, False, oDecimal)
    '                                        Case 2
    '                                            validasiReceiver2()
    '                                            .FK_IFTI_NasabahType_ID = 2
    '                                            .FK_IFTI_ID = objIfti.PK_IFTI_ID

    '                                            FillOrNothing(.Beneficiary_Nasabah_CORP_NoRekening, Me.txtSwiftInPenerimaNasabah_Rekening.Text)

    '                                            FillOrNothing(.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id, cbo_SwiftInPenerimaNasabah_Korp_BentukBadanUsaha.SelectedValue, False, oInt)
    '                                            FillOrNothing(.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya, Me.txtSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLainnya.Text, False, Ovarchar)
    '                                            FillOrNothing(.Beneficiary_Nasabah_CORP_NamaKorporasi, Me.txtSwiftInPenerimaNasabah_Korp_namaKorp.Text, False, Ovarchar)
    '                                            FillOrNothing(.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id, Me.hfSwiftInPenerimaNasabah_Korp_BidangUsahaKorp.Value, False, oInt)
    '                                            FillOrNothing(.Beneficiary_Nasabah_CORP_BidangUsahaLainnya, Me.txtSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLainnya.Text, False, Ovarchar)
    '                                            FillOrNothing(.Beneficiary_Nasabah_CORP_AlamatLengkap, Me.txtSwiftInPenerimaNasabah_Korp_AlamatKorp.Text, TipePenerima, Ovarchar)
    '                                            If ObjectAntiNull(Me.txtSwiftInPenerimaNasabah_Korp_Kotakab.Text) = True Then
    '                                                FillOrNothing(.Beneficiary_Nasabah_CORP_ID_KotaKab, Me.hfSwiftInPenerimaNasabah_Korp_kotakab.Value, False, oInt)
    '                                                FillOrNothing(.Beneficiary_Nasabah_CORP_ID_KotaKabLainnya, Me.txtSwiftInPenerimaNasabah_Korp_kotaLain.Text, False, Ovarchar)
    '                                            Else
    '                                                FillOrNothing(.Beneficiary_Nasabah_CORP_ID_KotaKab, Nothing, False, oInt)
    '                                                FillOrNothing(.Beneficiary_Nasabah_CORP_ID_KotaKabLainnya, Me.txtSwiftInPenerimaNasabah_Korp_kotaLain.Text, False, Ovarchar)
    '                                            End If
    '                                            If ObjectAntiNull(Me.txtSwiftInPenerimaNasabah_Korp_propinsi.Text) = True Then
    '                                                FillOrNothing(.Beneficiary_Nasabah_CORP_ID_Propinsi, Me.hfSwiftInPenerimaNasabah_Korp_propinsi.Value, False, oInt)
    '                                                FillOrNothing(.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya, txtSwiftInPenerimaNasabah_Korp_propinsilain.Text, False, Ovarchar)
    '                                            Else
    '                                                FillOrNothing(.Beneficiary_Nasabah_CORP_ID_Propinsi, Nothing, False, oInt)
    '                                                FillOrNothing(.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya, txtSwiftInPenerimaNasabah_Korp_propinsilain.Text, False, Ovarchar)
    '                                            End If
    '                                            FillOrNothing(.Beneficiary_Nasabah_CORP_NoTelp, txtSwiftInPenerimaNasabah_Korp_NoTelp.Text, False, Ovarchar)
    '                                            FillOrNothing(.Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan, txtSwiftInPenerimaNasabah_Korp_NilaiTransaksiKeuangan.Text, False, oDecimal)
    '                                        Case 3
    '                                            validasiReceiver3()
    '                                            .FK_IFTI_NasabahType_ID = 3
    '                                            .FK_IFTI_ID = objIfti.PK_IFTI_ID

    '                                            FillOrNothing(.Beneficiary_NonNasabah_NamaLengkap, Me.TxtSwiftInPenerimaNonNasabah_nama.Text, False, Ovarchar)
    '                                            FillOrNothing(.Beneficiary_NonNasabah_TanggalLahir, Me.TxtSwiftInPenerimaNonNasabah_TanggalLahir.Text, False, oDate)
    '                                            FillOrNothing(.Beneficiary_NonNasabah_ID_Alamat, Me.TxtSwiftInPenerimaNonNasabah_alamatiden.Text, False, Ovarchar)
    '                                            FillOrNothing(.Beneficiary_NonNasabah_NoTelp, Me.TxtSwiftInPenerimaNonNasabah_NoTelepon.Text, False, Ovarchar)
    '                                            FillOrNothing(.Beneficiary_NonNasabah_FK_IFTI_IDType, Me.CboSwiftInPenerimaNonNasabah_JenisDokumen.SelectedValue, False, oInt)
    '                                            FillOrNothing(.Beneficiary_NonNasabah_NomorID, Me.TxtSwiftInPenerimaNonNasabah_NomorIden.Text, False, Ovarchar)
    '                                            FillOrNothing(.Beneficiary_NonNasabah_NilaiTransaksikeuangan, Me.TxtSwiftInPenerimaNonNasabah_NilaiTransaksiKeuangan.Text, False, oDecimal)

    '                                        Case 4
    '                                            validasiReceiver4()
    '                                            .FK_IFTI_NasabahType_ID = 4
    '                                            .FK_IFTI_ID = objIfti.PK_IFTI_ID

    '                                            FillOrNothing(.Beneficiary_Nasabah_INDV_NoRekening, Me.TxtSwiftInPenerimaPenerus_Rekening.Text, False, Ovarchar)

    '                                            FillOrNothing(.Beneficiary_Nasabah_INDV_NamaBank, Me.TxtSwiftInPenerimaPenerus_Ind_namaBank.Text, False, Ovarchar)
    '                                            FillOrNothing(.Beneficiary_Nasabah_INDV_NamaLengkap, Me.TxtSwiftInPenerimaPenerus_Ind_nama.Text, False, Ovarchar)
    '                                            FillOrNothing(.Beneficiary_Nasabah_INDV_TanggalLahir, Me.TxtSwiftInPenerimaPenerus_Ind_TanggalLahir.Text, False, oDate)
    '                                            FillOrNothing(.Beneficiary_Nasabah_INDV_KewargaNegaraan, Me.RBSwiftInPenerimaPenerus_Ind_Kewarganegaraan.SelectedValue, False, oInt)
    '                                            If RBSwiftInPenerimaPenerus_Ind_Kewarganegaraan.SelectedValue = "2" Then
    '                                                If ObjectAntiNull(Me.TxtSwiftInPenerimaPenerus_IND_negara.Text) = True Then
    '                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Negara, Me.hfSwiftInPenerimaPenerus_IND_negara.Value, False, oInt)
    '                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_NegaraLainnya, Me.TxtSwiftInPenerimaPenerus_IND_negaralain.Text, False, Ovarchar)
    '                                                Else
    '                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Negara, Nothing, False, oInt)
    '                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_NegaraLainnya, Me.TxtSwiftInPenerimaPenerus_IND_negaralain.Text, False, Ovarchar)
    '                                                End If
    '                                            Else
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Negara, Nothing, False, oInt)
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_NegaraLainnya, Nothing, False, Ovarchar)
    '                                            End If
    '                                            FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Alamat_Dom, Me.TxtSwiftInPenerimaPenerus_IND_alamatDom.Text, False, Ovarchar)
    '                                            If ObjectAntiNull(Me.TxtSwiftInPenerimaPenerus_IND_kotaDom.Text) = True Then
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKab_Dom, Me.hfSwiftInPenerimaPenerus_IND_kotaDom.Value, False, oInt)
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom, TxtSwiftInPenerimaPenerus_IND_kotaLainDom.Text, False, Ovarchar)
    '                                            Else
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKab_Dom, Nothing, False, oInt)
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom, TxtSwiftInPenerimaPenerus_IND_kotaLainDom.Text, False, Ovarchar)
    '                                            End If
    '                                            If ObjectAntiNull(Me.TxtSwiftInPenerimaPenerus_IND_ProvDom.Text) = True Then
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Propinsi_Dom, Me.hfSwiftInPenerimaPenerus_IND_ProvDom.Value, False, oInt)
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom, Me.TxtSwiftInPenerimaPenerus_IND_ProvLainDom.Text, False, Ovarchar)
    '                                            Else
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Propinsi_Dom, Nothing, False, oInt)
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom, Me.TxtSwiftInPenerimaPenerus_IND_ProvLainDom.Text, False, Ovarchar)
    '                                            End If
    '                                            FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Alamat_Iden, Me.TxtSwiftInPenerimaPenerus_IND_alamatIden.Text, False, Ovarchar)
    '                                            If ObjectAntiNull(Me.TxtSwiftInPenerimaPenerus_IND_kotaIden.Text) = True Then
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKab_Iden, Me.hfSwiftInPenerimaPenerus_IND_kotaIden.Value, False, oInt)
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden, TxtSwiftInPenerimaPenerus_IND_kotaIden.Text, False, Ovarchar)
    '                                            Else
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKab_Iden, Nothing, False, oInt)
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden, TxtSwiftInPenerimaPenerus_IND_kotaIden.Text, False, Ovarchar)
    '                                            End If

    '                                            If ObjectAntiNull(Me.TxtSwiftInPenerimaPenerus_IND_ProvIden.Text) = True Then
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Propinsi_Iden, Me.hfSwiftInPenerimaPenerus_IND_ProvIden.Value, False, oInt)
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden, Me.TxtSwiftInPenerimaPenerus_IND_ProvIden.Text, False, Ovarchar)
    '                                            Else
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Propinsi_Iden, Nothing, False, oInt)
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden, Me.TxtSwiftInPenerimaPenerus_IND_ProvIden.Text, False, Ovarchar)
    '                                            End If
    '                                            FillOrNothing(.Beneficiary_Nasabah_INDV_NoTelp, Me.TxtSwiftInPenerimaPenerus_IND_NoTelp.Text, False, Ovarchar)
    '                                            FillOrNothing(.Beneficiary_Nasabah_INDV_FK_IFTI_IDType, Me.cbo_SwiftInpenerimaNas_Ind_jenisidentitas.SelectedValue, False, oInt)
    '                                            FillOrNothing(.Beneficiary_Nasabah_INDV_NomorID, Me.TxtSwiftInPenerimaPenerus_IND_nomoridentitas.Text, False, Ovarchar)
    '                                            FillOrNothing(.Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan, Me.TxtSwiftInPenerimaPenerus_IND_NIlaiTransaksi.Text, False, oDecimal)
    '                                        Case 5
    '                                            validasiReceiver5()
    '                                            .FK_IFTI_NasabahType_ID = 5
    '                                            .FK_IFTI_ID = objIfti.PK_IFTI_ID

    '                                            FillOrNothing(.Beneficiary_Nasabah_CORP_NoRekening, Me.TxtSwiftInPenerimaPenerus_Rekening.Text)

    '                                            FillOrNothing(.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id, cbo_SwiftInPenerimaPenerus_Korp_BentukBadanUsaha.SelectedValue, False, oInt)
    '                                            FillOrNothing(.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya, Me.txtSwiftInPenerimaPenerus_Korp_BentukBadanUsahaLain.Text, False, Ovarchar)
    '                                            FillOrNothing(.Beneficiary_Nasabah_CORP_NamaKorporasi, Me.txtSwiftInPenerimaPenerus_Korp_NamaKorp.Text, False, Ovarchar)
    '                                            FillOrNothing(.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id, Me.hfSwiftInPenerimaPenerus_Korp_BidangUsahaKorp.Value, False, oInt)
    '                                            FillOrNothing(.Beneficiary_Nasabah_CORP_BidangUsahaLainnya, Me.txtSwiftInPenerimaPenerus_Korp_BentukBadanUsahaLain.Text, False, Ovarchar)
    '                                            FillOrNothing(.Beneficiary_Nasabah_CORP_AlamatLengkap, Me.txtSwiftInPenerimaPenerus_Korp_alamatkorp.Text, False, Ovarchar)
    '                                            If ObjectAntiNull(Me.txtSwiftInPenerimaPenerus_Korp_kotakorp.Text) = True Then
    '                                                FillOrNothing(.Beneficiary_Nasabah_CORP_ID_KotaKab, Me.hfSwiftInPenerimaPenerus_Korp_kotakorp.Value, False, oInt)
    '                                                FillOrNothing(.Beneficiary_Nasabah_CORP_ID_KotaKabLainnya, Me.txtSwiftInPenerimaPenerus_Korp_kotalainnyakorp.Text, False, Ovarchar)
    '                                            Else
    '                                                FillOrNothing(.Beneficiary_Nasabah_CORP_ID_KotaKab, Nothing, False, oInt)
    '                                                FillOrNothing(.Beneficiary_Nasabah_CORP_ID_KotaKabLainnya, Me.txtSwiftInPenerimaPenerus_Korp_kotalainnyakorp.Text, False, Ovarchar)

    '                                            End If
    '                                            If ObjectAntiNull(Me.txtSwiftInPenerimaPenerus_Korp_ProvinsiKorp.Text) = True Then
    '                                                FillOrNothing(.Beneficiary_Nasabah_CORP_ID_Propinsi, Me.hfSwiftInPenerimaPenerus_Korp_ProvinsiKorp.Value, False, oInt)
    '                                                FillOrNothing(.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya, txtSwiftInPenerimaPenerus_Korp_provinsiLainnyaKorp.Text, False, Ovarchar)
    '                                            Else
    '                                                FillOrNothing(.Beneficiary_Nasabah_CORP_ID_Propinsi, Nothing, False, oInt)
    '                                                FillOrNothing(.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya, txtSwiftInPenerimaPenerus_Korp_provinsiLainnyaKorp.Text, False, Ovarchar)
    '                                            End If
    '                                            FillOrNothing(.Beneficiary_Nasabah_CORP_NoTelp, Me.txtSwiftInPenerimaPenerus_Korp_NoTelp.Text, False, Ovarchar)
    '                                            FillOrNothing(.Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan, Me.txtSwiftInPenerimaPenerus_Korp_NilaiTransaksi.Text, False, oDecimal)

    '                                    End Select
    '                                End With
    '                                'save beneficiary
    '                                DataRepository.IFTI_BeneficiaryProvider.Save(OTrans, objReceiver(0))
    '                            End Using

    '                            'cekTransaksi
    '                            validasiTransaksi()

    '                            '-----new---
    '                            FillOrNothing(.TanggalTransaksi, Me.Transaksi_SwiftIntanggal0.Text, False, oDate)
    '                            FillOrNothing(.TimeIndication, Me.Transaksi_SwiftInwaktutransaksi0.Text, False, oDate)
    '                            FillOrNothing(.SenderReference, Me.Transaksi_SwiftInSender0.Text, False, Ovarchar)
    '                            FillOrNothing(.BankOperationCode, Me.Transaksi_SwiftInBankOperationCode0.Text, False, Ovarchar)
    '                            FillOrNothing(.InstructionCode, Me.Transaksi_SwiftInInstructionCode0.Text, False, Ovarchar)
    '                            FillOrNothing(.KantorCabangPenyelengaraPengirimAsal, Me.Transaksi_SwiftInkantorCabangPengirim0.Text, False, Ovarchar)
    '                            FillOrNothing(.TransactionCode, Me.Transaksi_SwiftInkodeTipeTransaksi0.Text, False, Ovarchar)
    '                            FillOrNothing(.ValueDate_TanggalTransaksi, Me.Transaksi_SwiftInValueTanggalTransaksi.Text, False, oDate)
    '                            FillOrNothing(.ValueDate_NilaiTransaksi, Me.Transaksi_SwiftInnilaitransaksi.Text, False, oDecimal)
    '                            FillOrNothing(.ValueDate_FK_Currency_ID, Me.hfTransaksi_SwiftInMataUangTransaksi0.Value, False, oInt)
    '                            FillOrNothing(.ValueDate_CurrencyLainnya, Me.Transaksi_SwiftInMataUangTransaksi0Lainnya.Text, False, Ovarchar)
    '                            FillOrNothing(.ValueDate_NilaiTransaksiIDR, Me.Transaksi_SwiftInAmountdalamRupiah0.Text, False, oDecimal)
    '                            FillOrNothing(.Instructed_Currency, Me.hfTransaksi_SwiftIncurrency.Value, False, oInt)
    '                            FillOrNothing(.Instructed_CurrencyLainnya, Me.Transaksi_SwiftIncurrencyLainnya.Text, False, Ovarchar)
    '                            FillOrNothing(.Instructed_Amount, Me.Transaksi_SwiftIninstructedAmount0.Text, False, oDecimal)
    '                            FillOrNothing(.ExchangeRate, Me.Transaksi_SwiftInnilaiTukar0.Text, False, oDecimal)
    '                            FillOrNothing(.SendingInstitution, Me.Transaksi_SwiftInsendingInstitution0.Text, False, Ovarchar)
    '                            FillOrNothing(.TujuanTransaksi, Me.Transaksi_SwiftInTujuanTransaksi0.Text, False, Ovarchar)
    '                            FillOrNothing(.SumberPenggunaanDana, Me.Transaksi_SwiftInSumberPenggunaanDana0.Text, False, Ovarchar)

    '                            'cekInformasiLain

    '                            '---new-----
    '                            FillOrNothing(.InformationAbout_SenderCorrespondent, Me.InformasiLainnya_SwiftInSender.Text, False, Ovarchar)
    '                            FillOrNothing(.InformationAbout_ReceiverCorrespondent, Me.InformasiLainnya_SwiftInreceiver.Text, False, Ovarchar)
    '                            FillOrNothing(.InformationAbout_Thirdreimbursementinstitution, Me.InformasiLainnya_SwiftInthirdReimbursement.Text, False, Ovarchar)
    '                            FillOrNothing(.InformationAbout_IntermediaryInstitution, Me.InformasiLainnya_SwiftInintermediary.Text, False, Ovarchar)
    '                            FillOrNothing(.RemittanceInformation, Me.InformasiLainnya_SwiftInRemittance.Text, False, Ovarchar)
    '                            FillOrNothing(.SendertoReceiverInformation, Me.InformasiLainnya_SwiftInSenderToReceiver.Text, False, Ovarchar)
    '                            FillOrNothing(.RegulatoryReporting, Me.InformasiLainnya_SwiftInRegulatoryReport.Text, False, Ovarchar)
    '                            FillOrNothing(.EnvelopeContents, Me.InformasiLainnya_SwiftInEnvelopeContents.Text, False, Ovarchar)

    '                            'saving
    '                        End With

    '                        DataRepository.IFTIProvider.Save(OTrans, objIfti)
    '                    End Using

    '                    ImageButtonSave.Visible = False
    '                    ImageButtonCancel.Visible = False
    '                    lblMsg.Text = "Data has been edited"
    '                    MultiViewEditSwiftIn.ActiveViewIndex = 1

    '                End If
    '                OTrans.Commit()
    '            Catch ex As Exception
    '                OTrans.Rollback()
    '                Throw
    '            End Try

    '        End Using
    '    Catch ex As Exception
    '        'LogError(ex)
    '        cvalPageErr.IsValid = False
    '        cvalPageErr.ErrorMessage = ex.Message
    '    End Try
    'End Sub
#End Region

#Region "Function"
    Private Sub SetCOntrolLoad()
        'bind JenisID
        Using objJenisId As TList(Of IFTI_IDType) = DataRepository.IFTI_IDTypeProvider.GetAll
            If objJenisId.Count > 0 Then
                Me.cbo_SwiftInpengirimNas_Ind_jenisidentitas.Items.Clear()
                cbo_SwiftInpengirimNas_Ind_jenisidentitas.Items.Add("-Select-")

                Me.cbo_SwiftInpenerimaNas_Ind_jenisidentitas.Items.Clear()
                cbo_SwiftInpenerimaNas_Ind_jenisidentitas.Items.Add("-Select-")

                Me.CboSwiftInPenerimaNonNasabah_JenisDokumen.Items.Clear()
                CboSwiftInPenerimaNonNasabah_JenisDokumen.Items.Add("-Select-")

                Me.CBoSwiftInPenerimaPenerus_IND_jenisIdentitas.Items.Clear()
                CBoSwiftInPenerimaPenerus_IND_jenisIdentitas.Items.Add("-Select-")

                'Me.cboTransaksi_SwiftInnilaitransaksi.Items.Clear()
                'Me.cboTransaksi_SwiftInnilaitransaksi.Items.Add("-Select-")

                'Me.CBOBenfOwnerNasabah_JenisDokumen.Items.Clear()
                'Me.CBOBenfOwnerNasabah_JenisDokumen.Items.Add("-Select-")


                For i As Integer = 0 To objJenisId.Count - 1
                    cbo_SwiftInpengirimNas_Ind_jenisidentitas.Items.Add(New ListItem(objJenisId(i).IDType, objJenisId(i).PK_IFTI_IDType.ToString))
                    cbo_SwiftInpenerimaNas_Ind_jenisidentitas.Items.Add(New ListItem(objJenisId(i).IDType, objJenisId(i).PK_IFTI_IDType.ToString))
                    CboSwiftInPenerimaNonNasabah_JenisDokumen.Items.Add(New ListItem(objJenisId(i).IDType, objJenisId(i).PK_IFTI_IDType.ToString))
                    CBoSwiftInPenerimaPenerus_IND_jenisIdentitas.Items.Add(New ListItem(objJenisId(i).IDType, objJenisId(i).PK_IFTI_IDType.ToString))
                    ' cboTransaksi_SwiftInnilaitransaksi.Items.Add(New ListItem(objJenisId(i).IDType, objJenisId(i).PK_IFTI_IDType.ToString))
                    ' CBOBenfOwnerNonNasabah_JenisDokumen.Items.Add(New ListItem(objJenisId(i).IDType, objJenisId(i).PK_IFTI_IDType.ToString))
                Next
            End If
            'bindgrid popUpCalender
            Me.PopUpSwiftInTanggalLaporan.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & SwiftInUmum_TanggalLaporan.ClientID & "'), 'dd-mmm-yyyy')")
            Me.PopUpSwiftInTanggalLaporan.Style.Add("display", "")
            Me.PopUpSwiftInTanggalLahir.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtSwiftInPengirimNasabah_IND_TanggalLahir.ClientID & "'), 'dd-mmm-yyyy')")
            Me.PopUpSwiftInTanggalLahir.Style.Add("display", "")
            Me.PopUpSwiftInNonNasabahTanggalLahir.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtSwiftInPengirimNonNasabah_TanggalLahir.ClientID & "'), 'dd-mmm-yyyy')")
            Me.PopUpSwiftInNonNasabahTanggalLahir.Style.Add("display", "")
            Me.SwiftInPenerimaNasabah_Ind_TanggalLahir.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtSwiftInPenerimaNasabah_IND_TanggalLahir.ClientID & "'), 'dd-mmm-yyyy')")
            Me.SwiftInPenerimaNasabah_Ind_TanggalLahir.Style.Add("display", "")
            Me.popUpSwiftInTanggalLahirPenerimaNonNasabah.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TxtSwiftInPenerimaNonNasabah_TanggalLahir.ClientID & "'), 'dd-mmm-yyyy')")
            Me.popUpSwiftInTanggalLahirPenerimaNonNasabah.Style.Add("display", "")
            Me.popUpSwiftInTangglaLahirPengirimPenerus_Ind.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TxtSwiftInPenerimaPenerus_Ind_TanggalLahir.ClientID & "'), 'dd-mmm-yyyy')")
            Me.popUpSwiftInTangglaLahirPengirimPenerus_Ind.Style.Add("display", "")
            Me.popUpTanggalTransaksi_SwiftInTransaksi.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.Transaksi_SwiftIntanggal0.ClientID & "'), 'dd-mmm-yyyy')")
            Me.popUpTanggalTransaksi_SwiftInTransaksi.Style.Add("display", "")
            Me.popUpTanggalTransaksi_SwiftInValueDate0.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.Transaksi_SwiftInValueTanggalTransaksi.ClientID & "'), 'dd-mmm-yyyy')")
            Me.popUpTanggalTransaksi_SwiftInValueDate0.Style.Add("display", "")
        End Using

        'Bind MsBentukBadanUsaha
        Using objBentukBadanUsaha As TList(Of MsBentukBidangUsaha) = DataRepository.MsBentukBidangUsahaProvider.GetAll
            If objBentukBadanUsaha.Count > 0 Then
                cbo_SwiftInPenerimaNasabah_Korp_BentukBadanUsaha.Items.Clear()
                cbo_SwiftInPenerimaNasabah_Korp_BentukBadanUsaha.Items.Add("-Select-")

                cbo_SwiftInPenerimaPenerus_Korp_BentukBadanUsaha.Items.Clear()
                cbo_SwiftInPenerimaPenerus_Korp_BentukBadanUsaha.Items.Add("-Select-")

                For i As Integer = 0 To objBentukBadanUsaha.Count - 1
                    cbo_SwiftInPenerimaNasabah_Korp_BentukBadanUsaha.Items.Add(New ListItem(objBentukBadanUsaha(i).BentukBidangUsaha, objBentukBadanUsaha(i).IdBentukBidangUsaha.ToString))
                    cbo_SwiftInPenerimaPenerus_Korp_BentukBadanUsaha.Items.Add(New ListItem(objBentukBadanUsaha(i).BentukBidangUsaha, objBentukBadanUsaha(i).IdBentukBidangUsaha.ToString))
                Next
            End If
        End Using
    End Sub
    Sub clearSession()
        Session("IFTIEdit.IFTIPK") = Nothing
        Session("IFTIEdit.IFTIBeneficiaryTempPK") = Nothing
        ReceiverDataTable = Nothing
        Session("IFTIEdit.IFTIPK") = Nothing
        Session("PickerProvinsi.Data") = Nothing
        Session("PickerNegara.Data") = Nothing
        'Session("PickerProvinsi.Data") = Nothing
        'Session("PickerProvinsi.Data") = Nothing
    End Sub
    Private Sub TransactionSwiftInc()
        Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)
            SafeDefaultValue = ""
            If Not IsNothing(objIfti) Then
                'umum
                Me.SwiftInUmum_LTDN.Text = Safe(objIfti.LTDLNNo)
                Me.SwiftInUmum_LtdnKoreksi.Text = Safe(objIfti.LTDLNNoKoreksi)
                Me.SwiftInUmum_TanggalLaporan.Text = FormatDate(Safe(objIfti.TanggalLaporan))
                Me.SwiftInUmum_NamaPJKBank.Text = Safe(objIfti.NamaPJKBankPelapor)
                Me.SwiftInUmum_NamaPejabatPJKBank.Text = Safe(objIfti.NamaPejabatPJKBankPelapor)
                Me.RbSwiftInUmum_JenisLaporan.SelectedValue = Safe(objIfti.JenisLaporan)

                'Identitas Pengirim
                ' cek ReceiverType
                Dim SenderType As Integer = objIfti.Sender_FK_IFTI_NasabahType_ID.GetValueOrDefault(1)

                Select Case (SenderType)
                    Case 1
                        Me.rbrbSwiftIn_TipePengirim.SelectedValue = 1
                        Me.trSwiftInTipeNasabah.Visible = True
                        Me.rbSwiftIn_TipeNasabah.SelectedValue = 1
                        Me.MultiViewSwiftInIdenPengirim.ActiveViewIndex = 0
                        Me.MultiViewSwiftInNasabahType.ActiveViewIndex = 0
                        FieldSwiftInPengirimNasabahPerorangan()
                    Case 2
                        Me.rbrbSwiftIn_TipePengirim.SelectedValue = 1
                        Me.trSwiftInTipeNasabah.Visible = True
                        Me.rbSwiftIn_TipeNasabah.SelectedValue = 2
                        Me.MultiViewSwiftInIdenPengirim.ActiveViewIndex = 0
                        Me.MultiViewSwiftInNasabahType.ActiveViewIndex = 1
                        FieldSwiftInPengirimNasabahKorporasi()
                    Case 3
                        Me.rbrbSwiftIn_TipePengirim.SelectedValue = 2
                        Me.trSwiftInTipeNasabah.Visible = False
                        Me.MultiViewSwiftInIdenPengirim.ActiveViewIndex = 1
                        Me.trSwiftInTipeNasabah.Visible = False
                        FieldSwiftInPengirimNonNasabah()
                End Select

                ''Identitas Penerima
                ''cekTipe Penyelenggara
                'Using objIftiBeneficiary As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("FK_IFTI_ID = " & getIFTIPK, "", 0, Integer.MaxValue, 0)
                '    If objIftiBeneficiary.Count > 0 Then
                '        Dim ReceiverType As Integer = objIftiBeneficiary(0).FK_IFTI_NasabahType_ID
                '        Select Case (ReceiverType)
                '            Case 1
                '                'pengirimNasabahIndividu
                '                Me.txtSwiftInPenerimaNasabah_Rekening.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_NoRekening
                '                Me.cboSwiftInPenerima_TipePJKBank.SelectedValue = 1
                '                RbSwiftInPenerima_TipePenerima.SelectedValue = 1
                '                RbSwiftInPenerima_TipeNasabah.SelectedValue = 1
                '                Me.trSwiftInTipepengirim.Visible = True
                '                Me.trSwiftInPenerimaTipeNasabah.Visible = True
                '                Me.MultiViewSwiftInPenerima.ActiveViewIndex = 0
                '                Me.MultiViewSwiftInPenAkhirJenis.ActiveViewIndex = 0
                '                Me.MultiViewSwiftINPenerimaAkhirNasabah.ActiveViewIndex = 0
                '                FieldSwiftInReceiverCase1()
                '            Case 2
                '                'pengirimNasabahKorp
                '                Me.txtSwiftInPenerimaNasabah_Rekening.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_NoRekening
                '                Me.cboSwiftInPenerima_TipePJKBank.SelectedValue = 1
                '                RbSwiftInPenerima_TipePenerima.SelectedValue = 1
                '                RbSwiftInPenerima_TipeNasabah.SelectedValue = 2
                '                Me.trSwiftInTipepengirim.Visible = True
                '                Me.trSwiftInPenerimaTipeNasabah.Visible = True
                '                Me.MultiViewSwiftInPenerima.ActiveViewIndex = 0
                '                Me.MultiViewSwiftInPenAkhirJenis.ActiveViewIndex = 0
                '                Me.MultiViewSwiftINPenerimaAkhirNasabah.ActiveViewIndex = 1
                '                FieldSwiftInReceiverCase2()
                '            Case 3
                '                'PengirimNonNasabah
                '                Me.cboSwiftInPenerima_TipePJKBank.SelectedValue = 1
                '                RbSwiftInPenerima_TipePenerima.SelectedValue = 2
                '                Me.trSwiftInTipepengirim.Visible = True
                '                Me.trSwiftInPenerimaTipeNasabah.Visible = False
                '                Me.MultiViewSwiftInPenerima.ActiveViewIndex = 0
                '                Me.MultiViewSwiftInPenAkhirJenis.ActiveViewIndex = 1
                '                FieldSwiftInReceiverCase3()
                '            Case 4
                '                'PenyelenggaraIndividu
                '                Me.TxtSwiftInPenerimaPenerus_Rekening.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_NoRekening
                '                Me.cboSwiftInPenerima_TipePJKBank.SelectedValue = 2
                '                RbSwiftInPenerima_TipeNasabah.SelectedValue = 1
                '                Me.trSwiftInTipepengirim.Visible = False
                '                Me.trSwiftInPenerimaTipeNasabah.Visible = True
                '                Me.MultiViewSwiftInPenerima.ActiveViewIndex = 1
                '                Me.MultiViewSwiftInPenerimaPenerus.ActiveViewIndex = 0
                '                FieldSwiftInReceiverCase4()
                '            Case 5
                '                'PenyelenggaraKorp
                '                Me.TxtSwiftInPenerimaPenerus_Rekening.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_NoRekening
                '                Me.cboSwiftInPenerima_TipePJKBank.SelectedValue = 2
                '                RbSwiftInPenerima_TipeNasabah.SelectedValue = 2
                '                Me.MultiViewSwiftInPenerima.ActiveViewIndex = 1
                '                Me.MultiViewSwiftInPenerimaPenerus.ActiveViewIndex = 1
                '                Me.trSwiftInTipepengirim.Visible = False
                '                Me.trSwiftInTipeNasabah.Visible = True
                '                FieldSwiftInReceiverCase5()
                '        End Select

                '    End If

                'End Using


                'Transaksi
                Me.Transaksi_SwiftIntanggal0.Text = FormatDate(objIfti.TanggalTransaksi)
                Me.Transaksi_SwiftInwaktutransaksi0.Text = objIfti.TimeIndication
                Me.Transaksi_SwiftInSender0.Text = objIfti.SenderReference
                Me.Transaksi_SwiftInBankOperationCode0.Text = objIfti.BankOperationCode
                Me.Transaksi_SwiftInInstructionCode0.Text = objIfti.InstructionCode
                Me.Transaksi_SwiftInkantorCabangPengirim0.Text = objIfti.KantorCabangPenyelengaraPengirimAsal
                Me.Transaksi_SwiftInkodeTipeTransaksi0.Text = objIfti.TransactionCode
                Me.Transaksi_SwiftInValueTanggalTransaksi.Text = FormatDate(objIfti.ValueDate_TanggalTransaksi)
                Me.Transaksi_SwiftInnilaitransaksi.Text = objIfti.ValueDate_NilaiTransaksi
                'Using objCurrency As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(objIfti.ValueDate_FK_Currency_ID)
                '    If Not IsNothing(objCurrency) Then
                '        Me.Transaksi_SwiftInMataUangTransaksi0.Text = objCurrency.MsCurrency_Code
                '        hfTransaksi_SwiftInMataUangTransaksi0.Value = objCurrency.IdCurrency
                '    End If
                'End Using
                'Dim objCurrency As TList(Of MsCurrency) = DataRepository.MsCurrencyProvider.GetPaged(MsCurrencyColumn.IdCurrency.ToString & _
                '                                                            " like '%" & objIfti.ValueDate_FK_Currency_ID.ToString & "%'", "", 0, Integer.MaxValue, 0)

                'Me.Transaksi_SwiftInMataUangTransaksi0.Text = objCurrency(0).Code & "-" & objCurrency(0).CurrencyName
                'hfTransaksi_SwiftInMataUangTransaksi0.Value = objCurrency(0).IdCurrency
                Using objCurrency As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(objIfti.ValueDate_FK_Currency_ID.GetValueOrDefault(0))
                    If Not IsNothing(objCurrency) Then
                        Me.Transaksi_SwiftInMataUangTransaksi0.Text = objCurrency.Code
                        hfTransaksi_SwiftInMataUangTransaksi0.Value = objCurrency.IdCurrency
                    End If
                End Using
                Me.Transaksi_SwiftInMataUangTransaksi0Lainnya.Text = objIfti.ValueDate_CurrencyLainnya
                Me.Transaksi_SwiftInAmountdalamRupiah0.Text = objIfti.ValueDate_NilaiTransaksiIDR
                'Using objCurrency As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(objIfti.Instructed_Currency)
                '    If Not IsNothing(objCurrency) Then
                '        Me.Transaksi_SwiftIncurrency0.Text = objCurrency.MsCurrency_Code
                '        hfTransaksi_SwiftIncurrency.Value = objCurrency.IdCurrency
                '    End If
                'End Using
                '                Dim objCurrency2 As TList(Of MsCurrency) = DataRepository.MsCurrencyProvider.GetPaged(MsCurrencyColumn.IdCurrency.ToString & _
                '" like '%" & objIfti.Instructed_Currency.ToString & "%'", "", 0, Integer.MaxValue, 0)

                '                Me.Transaksi_SwiftIncurrency0.Text = objCurrency2(0).Code & "-" & objCurrency(0).CurrencyName
                '                hfTransaksi_SwiftIncurrency.Value = objCurrency2(0).IdCurrency
                Using objCurrency As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(objIfti.Instructed_Currency.GetValueOrDefault(0))
                    If Not IsNothing(objCurrency) Then
                        Me.Transaksi_SwiftIncurrency0.Text = objCurrency.Code
                        hfTransaksi_SwiftIncurrency.Value = objCurrency.IdCurrency
                    End If
                End Using
                Me.Transaksi_SwiftIncurrencyLainnya.Text = objIfti.Instructed_CurrencyLainnya
                If Not IsNothing(objIfti.Instructed_Amount) Then
                    Me.Transaksi_SwiftIninstructedAmount0.Text = objIfti.Instructed_Amount
                End If
                If Not IsNothing(objIfti.ExchangeRate) Then
                    Me.Transaksi_SwiftInnilaiTukar0.Text = objIfti.ExchangeRate
                End If
                Me.Transaksi_SwiftInsendingInstitution0.Text = objIfti.SendingInstitution
                Me.Transaksi_SwiftInTujuanTransaksi0.Text = objIfti.TujuanTransaksi
                Me.Transaksi_SwiftInSumberPenggunaanDana0.Text = objIfti.SumberPenggunaanDana

                'informasi lainnya
                '---new-----
                Me.InformasiLainnya_SwiftInSender.Text = objIfti.InformationAbout_SenderCorrespondent
                Me.InformasiLainnya_SwiftInreceiver.Text = objIfti.InformationAbout_ReceiverCorrespondent
                Me.InformasiLainnya_SwiftInthirdReimbursement.Text = objIfti.InformationAbout_Thirdreimbursementinstitution
                Me.InformasiLainnya_SwiftInintermediary.Text = objIfti.InformationAbout_IntermediaryInstitution
                Me.InformasiLainnya_SwiftInRemittance.Text = objIfti.RemittanceInformation
                Me.InformasiLainnya_SwiftInSenderToReceiver.Text = objIfti.SendertoReceiverInformation
                Me.InformasiLainnya_SwiftInRegulatoryReport.Text = objIfti.RegulatoryReporting
                Me.InformasiLainnya_SwiftInEnvelopeContents.Text = objIfti.EnvelopeContents

            End If

        End Using

    End Sub
    Public Shared Function IsPhoneNumber(ByVal s As String) As Boolean
        ''Dim regex = New System.Text.RegularExpressions.Regex("[(]?62?\s*?[2-9][0-9]{[0-9]}?[)]?\s?[-]?\d{5,12}?")
        'Dim regex = New System.Text.RegularExpressions.Regex("[(]?62?[2-9][0-9]{[0-9]}?[)]?[-]?[0-9]*")
        'Return regex.IsMatch(s)
        Dim aString As String = Trim(Replace(s, "(", ""))
        aString = Replace(aString, ")", "")
        aString = Replace(aString, "-", "")
        aString = Replace(aString, " ", "")
        If Not IsNumeric(aString) Then
            Return False
        Else
            Return True
        End If

    End Function
#Region "Receiver"
    Private Sub LoadReceiver()
        Me.GridDataView.DataSource = ReceiverDataTable
        GridDataView.DataBind()
    End Sub

    Private Sub AddReceiver()
        Try
            If ValidateReceiver() Then
                Dim dr As Data.DataRow = Nothing
                Dim tipePJKBank As Integer = cboSwiftInPenerima_TipePJKBank.SelectedValue
                Dim tipepenerima As Integer
                Dim tipenasabah As Integer
                Dim nasabahtype As Integer

                dr = ReceiverDataTable.NewRow
                dr("FK_IFTI_ID") = getIFTIPK

                If tipePJKBank = 1 Then
                    tipepenerima = RbSwiftInPenerima_TipePenerima.SelectedValue
                    If tipepenerima = 1 Then
                        tipenasabah = RbSwiftInPenerima_TipeNasabah.SelectedValue
                        If tipenasabah = 1 Then
                            nasabahtype = 1
                            dr("FK_IFTI_NasabahType_ID") = nasabahtype
                            dr("PJKBank_type") = tipePJKBank

                            dr("Beneficiary_Nasabah_INDV_NoRekening") = txtSwiftInPenerimaNasabah_Rekening.Text
                            dr("Beneficiary_Nasabah_INDV_NamaLengkap") = txtSwiftInPenerimaNasabah_IND_nama.Text
                            If txtSwiftInPenerimaNasabah_IND_TanggalLahir.Text <> "" Then
                                dr("Beneficiary_Nasabah_INDV_TanggalLahir") = txtSwiftInPenerimaNasabah_IND_TanggalLahir.Text.Trim() 'Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", txtSwiftInPenerimaNasabah_IND_TanggalLahir.Text.Trim())
                            End If
                            If RbSwiftInPenerimaNasabah_IND_Kewarganegaraan.SelectedValue <> "" Then
                                dr("Beneficiary_Nasabah_INDV_KewargaNegaraan") = RbSwiftInPenerimaNasabah_IND_Kewarganegaraan.SelectedValue
                            End If

                            If ObjectAntiNull(hfSwiftInPenerimaNasabah_IND_negara.Value) = True Then
                                dr("Beneficiary_Nasabah_INDV_Negara") = hfSwiftInPenerimaNasabah_IND_negara.Value
                            End If

                            dr("Beneficiary_Nasabah_INDV_NegaraLainnya") = txtSwiftInPenerimaNasabah_IND_negaralain.Text
                            If ObjectAntiNull(hfSwiftInIdenPenerimaNas_Ind_pekerjaan.Value) = True Then
                                dr("Beneficiary_Nasabah_INDV_Pekerjaan") = hfSwiftInIdenPenerimaNas_Ind_pekerjaan.Value
                            End If

                            dr("Beneficiary_Nasabah_INDV_PekerjaanLainnya") = Me.TxtSwiftInIdenPenerimaNas_Ind_Pekerjaanlain.Text
                            If ObjectAntiNull(hfSwiftInIdenPenerimaNas_Ind_provinsi_dom.Value) = True Then
                                dr("Beneficiary_Nasabah_INDV_ID_Propinsi_Dom") = hfSwiftInIdenPenerimaNas_Ind_provinsi_dom.Value
                            End If
                            dr("Beneficiary_Nasabah_INDV_ID_Alamat_Dom") = TxtSwiftInIdenPenerimaNas_Ind_Alamat.Text
                            If ObjectAntiNull(hfSwiftInIdenPenerimaNas_Ind_Kota_dom.Value) = True Then
                                dr("Beneficiary_Nasabah_INDV_ID_KotaKab_Dom") = hfSwiftInIdenPenerimaNas_Ind_Kota_dom.Value
                            End If
                            dr("Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom") = TxtSwiftInIdenPenerimaNas_Ind_kotalain.Text

                            dr("Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom") = TxtSwiftInIdenPenerimaNas_Ind_provinsiLain.Text
                            dr("Beneficiary_Nasabah_INDV_ID_Alamat_Iden") = TxtSwiftInIdenPenerimaNas_Ind_alamatIdentitas.Text
                            If ObjectAntiNull(hfSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Value) = True Then
                                dr("Beneficiary_Nasabah_INDV_ID_KotaKab_Iden") = hfSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Value
                            End If
                            dr("Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden") = TxtSwiftInIdenPenerimaNas_Ind_KotaLainIden.Text
                            If ObjectAntiNull(Me.hfSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Value) = True Then
                                dr("Beneficiary_Nasabah_INDV_ID_Propinsi_Iden") = Me.hfSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Value
                            End If
                            dr("Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden") = TxtSwiftInIdenPenerimaNas_Ind_ProvinsilainIden.Text
                            dr("Beneficiary_Nasabah_INDV_NoTelp") = TxtISwiftIndenPenerimaNas_Ind_noTelp.Text
                            dr("Beneficiary_Nasabah_INDV_FK_IFTI_IDType") = cbo_SwiftInpenerimaNas_Ind_jenisidentitas.SelectedValue
                            dr("Beneficiary_Nasabah_INDV_NomorID") = txtSwiftInPengirimNasabah_IND_NomorIdentitas.Text
                            If txtSwiftInIdenPenerimaNas_IND_NilaiTransaksiKeuangan.Text <> "" Then
                                dr("Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan") = txtSwiftInIdenPenerimaNas_IND_NilaiTransaksiKeuangan.Text
                            End If


                            ReceiverDataTable.Rows.Add(dr)

                            txtSwiftInPenerimaNasabah_Rekening.Text = ""
                            txtSwiftInPenerimaNasabah_IND_nama.Text = ""
                            txtSwiftInPenerimaNasabah_IND_TanggalLahir.Text = ""
                            RbSwiftInPenerimaNasabah_IND_Kewarganegaraan.SelectedIndex = 0
                            hfSwiftInPenerimaNasabah_IND_negara.Value = ""
                            txtSwiftInPenerimaNasabah_IND_negara.Text = ""
                            txtSwiftInPenerimaNasabah_IND_negaralain.Text = ""
                            hfSwiftInIdenPenerimaNas_Ind_pekerjaan.Value = ""
                            txtSwiftInPenerimaNasabah_IND_pekerjaan.Text = ""
                            Me.TxtSwiftInIdenPenerimaNas_Ind_Pekerjaanlain.Text = ""
                            TxtSwiftInIdenPenerimaNas_Ind_Alamat.Text = ""
                            Me.hfSwiftInIdenPenerimaNas_Ind_Kota_dom.Value = ""
                            Me.TxtSwiftInIdenPenerimaNas_Ind_Kota.Text = ""
                            TxtSwiftInIdenPenerimaNas_Ind_kotalain.Text = ""
                            hfSwiftInIdenPenerimaNas_Ind_provinsi_dom.Value = ""
                            TxtSwiftInIdenPenerimaNas_Ind_provinsi.Text = ""
                            TxtSwiftInIdenPenerimaNas_Ind_provinsiLain.Text = ""
                            TxtSwiftInIdenPenerimaNas_Ind_alamatIdentitas.Text = ""
                            hfSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Value = ""
                            Me.TxtSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Text = ""
                            TxtSwiftInIdenPenerimaNas_Ind_KotaLainIden.Text = ""
                            TxtSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Text = ""
                            hfSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Value = ""
                            TxtSwiftInIdenPenerimaNas_Ind_provinsiLain.Text = ""
                            TxtSwiftInIdenPenerimaNas_Ind_ProvinsilainIden.Text = ""
                            TxtISwiftIndenPenerimaNas_Ind_noTelp.Text = ""
                            cbo_SwiftInpenerimaNas_Ind_jenisidentitas.SelectedIndex = 0
                            Me.TxtISwiftIndenPenerimaNas_Ind_noIdentitas.Text = ""
                            txtSwiftInIdenPenerimaNas_IND_NilaiTransaksiKeuangan.Text = ""
                            CheckSwiftInPenerimaNas_Ind_alamat.Checked = False


                        Else
                            nasabahtype = 2
                            dr("FK_IFTI_NasabahType_ID") = nasabahtype
                            dr("PJKBank_type") = tipePJKBank

                            dr("Beneficiary_Nasabah_corp_NoRekening") = txtSwiftInPenerimaNasabah_Rekening.Text

                            dr("Beneficiary_Nasabah_CORP_NamaKorporasi") = txtSwiftInPenerimaNasabah_Korp_namaKorp.Text
                            dr("Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya") = txtSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLainnya.Text
                            If cbo_SwiftInPenerimaNasabah_Korp_BentukBadanUsaha.SelectedIndex > 0 Then
                                dr("Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id") = CInt(cbo_SwiftInPenerimaNasabah_Korp_BentukBadanUsaha.SelectedValue)
                            End If

                            If ObjectAntiNull(hfSwiftInPenerimaNasabah_Korp_BidangUsahaKorp.Value) = True Then
                                dr("Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id") = hfSwiftInPenerimaNasabah_Korp_BidangUsahaKorp.Value
                            End If
                            dr("Beneficiary_Nasabah_CORP_BidangUsahaLainnya") = txtSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLainnya.Text
                            dr("Beneficiary_Nasabah_CORP_AlamatLengkap") = txtSwiftInPenerimaNasabah_Korp_AlamatKorp.Text
                            If ObjectAntiNull(hfSwiftInPenerimaNasabah_Korp_kotakab.Value) = True Then
                                dr("Beneficiary_Nasabah_CORP_ID_KotaKab") = hfSwiftInPenerimaNasabah_Korp_kotakab.Value
                            End If
                            dr("Beneficiary_Nasabah_CORP_ID_KotaKabLainnya") = Me.txtSwiftInPenerimaNasabah_Korp_kotaLain.Text
                            If ObjectAntiNull(hfSwiftInPenerimaNasabah_Korp_propinsi.Value) = True Then
                                dr("Beneficiary_Nasabah_CORP_ID_Propinsi") = hfSwiftInPenerimaNasabah_Korp_propinsi.Value
                            End If
                            dr("Beneficiary_Nasabah_CORP_ID_PropinsiLainnya") = txtSwiftInPenerimaNasabah_Korp_propinsilain.Text
                            dr("Beneficiary_Nasabah_CORP_NoTelp") = txtSwiftInPenerimaNasabah_Korp_NoTelp.Text
                            If txtSwiftInPenerimaNasabah_Korp_NilaiTransaksiKeuangan.Text <> "" Then
                                dr("Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan") = txtSwiftInPenerimaNasabah_Korp_NilaiTransaksiKeuangan.Text
                            End If

                            ReceiverDataTable.Rows.Add(dr)

                            txtSwiftInPenerimaNasabah_Rekening.Text = ""

                            txtSwiftInPenerimaNasabah_Korp_namaKorp.Text = ""
                            txtSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLainnya.Text = ""
                            cbo_SwiftInPenerimaNasabah_Korp_BentukBadanUsaha.SelectedIndex = 0
                            hfSwiftInPenerimaNasabah_Korp_BidangUsahaKorp.Value = ""
                            txtSwiftInPenerimaNasabah_Korp_BidangUsahaKorp.Text = ""
                            txtSwiftInPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Text = ""
                            txtSwiftInPenerimaNasabah_Korp_AlamatKorp.Text = ""
                            hfSwiftInPenerimaNasabah_Korp_kotakab.Value = ""
                            txtSwiftInPenerimaNasabah_Korp_Kotakab.Text = ""
                            Me.txtSwiftInPenerimaNasabah_Korp_kotaLain.Text = ""
                            hfSwiftInPenerimaNasabah_Korp_propinsi.Value = ""
                            txtSwiftInPenerimaNasabah_Korp_propinsi.Text = ""
                            txtSwiftInPenerimaNasabah_Korp_propinsilain.Text = ""
                            txtSwiftInPenerimaNasabah_Korp_NoTelp.Text = ""
                            txtSwiftInPenerimaNasabah_Korp_NilaiTransaksiKeuangan.Text = ""

                        End If
                    ElseIf tipepenerima = 2 Then
                        nasabahtype = 3
                        dr("FK_IFTI_NasabahType_ID") = nasabahtype
                        dr("PJKBank_type") = tipePJKBank

                        'dr("Beneficiary_NonNasabah_NoRekening") = txtPenerima_rekening.Text

                        dr("Beneficiary_NonNasabah_NamaLengkap") = TxtSwiftInPenerimaNonNasabah_nama.Text
                        If TxtSwiftInPenerimaNonNasabah_TanggalLahir.Text <> "" Then
                            dr("Beneficiary_NonNasabah_TanggalLahir") = TxtSwiftInPenerimaNonNasabah_TanggalLahir.Text
                        End If

                        dr("Beneficiary_NonNasabah_ID_Alamat") = TxtSwiftInPenerimaNonNasabah_alamatiden.Text
                        dr("Beneficiary_NonNasabah_NoTelp") = TxtSwiftInPenerimaNonNasabah_NoTelepon.Text
                        dr("Beneficiary_NonNasabah_FK_IFTI_IDType") = CboSwiftInPenerimaNonNasabah_JenisDokumen.SelectedValue
                        dr("Beneficiary_NonNasabah_NomorID") = TxtSwiftInPenerimaNonNasabah_NomorIden.Text
                        If TxtSwiftInPenerimaNonNasabah_NilaiTransaksiKeuangan.Text <> "" Then
                            dr("Beneficiary_NonNasabah_NilaiTransaksikeuangan") = TxtSwiftInPenerimaNonNasabah_NilaiTransaksiKeuangan.Text
                        End If


                        ReceiverDataTable.Rows.Add(dr)
                        'txtPenerima_rekening.Text = ""

                        TxtSwiftInPenerimaNonNasabah_nama.Text = ""
                        TxtSwiftInPenerimaNonNasabah_TanggalLahir.Text = ""
                        TxtSwiftInPenerimaNonNasabah_alamatiden.Text = ""
                        TxtSwiftInPenerimaNonNasabah_NoTelepon.Text = ""
                        CboSwiftInPenerimaNonNasabah_JenisDokumen.SelectedValue = ""
                        TxtSwiftInPenerimaNonNasabah_NomorIden.Text = ""
                        TxtSwiftInPenerimaNonNasabah_NilaiTransaksiKeuangan.Text = ""

                    End If

                Else
                    tipenasabah = RbSwiftInPenerima_TipeNasabah.SelectedValue
                    If tipenasabah = 1 Then
                        nasabahtype = 1
                        dr("FK_IFTI_NasabahType_ID") = nasabahtype
                        dr("PJKBank_type") = tipePJKBank

                        dr("Beneficiary_Nasabah_INDV_NoRekening") = TxtSwiftInPenerimaPenerus_Rekening.Text
                        dr("Beneficiary_Nasabah_INDV_NamaBank") = TxtSwiftInPenerimaPenerus_Ind_namaBank.Text
                        dr("Beneficiary_Nasabah_INDV_NamaLengkap") = TxtSwiftInPenerimaPenerus_Ind_nama.Text
                        If TxtSwiftInPenerimaPenerus_Ind_TanggalLahir.Text <> "" Then
                            dr("Beneficiary_Nasabah_INDV_TanggalLahir") = TxtSwiftInPenerimaPenerus_Ind_TanggalLahir.Text.Trim() 'Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", TxtSwiftInPenerimaPenerus_Ind_TanggalLahir.Text.Trim())
                        End If
                        If RBSwiftInPenerimaPenerus_Ind_Kewarganegaraan.SelectedValue <> "" Then
                            dr("Beneficiary_Nasabah_INDV_KewargaNegaraan") = RBSwiftInPenerimaPenerus_Ind_Kewarganegaraan.SelectedValue
                        End If

                        If ObjectAntiNull(hfSwiftInPenerimaPenerus_IND_negara.Value) = True Then
                            dr("Beneficiary_Nasabah_INDV_Negara") = hfSwiftInPenerimaPenerus_IND_negara.Value
                        End If

                        dr("Beneficiary_Nasabah_INDV_NegaraLainnya") = TxtSwiftInPenerimaPenerus_IND_negaralain.Text
                        If ObjectAntiNull(Me.hfSwiftInPenerimaPenerus_IND_pekerjaan.Value) = True Then
                            dr("Beneficiary_Nasabah_INDV_Pekerjaan") = Me.hfSwiftInPenerimaPenerus_IND_pekerjaan.Value
                        End If
                        dr("Beneficiary_Nasabah_INDV_PekerjaanLainnya") = TxtSwiftInPenerimaPenerus_IND_pekerjaanLain.Text

                        dr("Beneficiary_Nasabah_INDV_ID_Alamat_Dom") = TxtSwiftInPenerimaPenerus_IND_alamatDom.Text
                        If ObjectAntiNull(hfSwiftInPenerimaPenerus_IND_kotaDom.Value) = True Then
                            dr("Beneficiary_Nasabah_INDV_ID_KotaKab_Dom") = hfSwiftInPenerimaPenerus_IND_kotaDom.Value
                        End If
                        dr("Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom") = TxtSwiftInPenerimaPenerus_IND_kotaLainDom.Text
                        If ObjectAntiNull(hfSwiftInPenerimaPenerus_IND_ProvDom.Value) = True Then
                            dr("Beneficiary_Nasabah_INDV_ID_Propinsi_Dom") = hfSwiftInPenerimaPenerus_IND_ProvDom.Value
                        End If

                        dr("Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom") = TxtSwiftInPenerimaPenerus_IND_ProvLainDom.Text
                        dr("Beneficiary_Nasabah_INDV_ID_Alamat_Iden") = TxtSwiftInPenerimaPenerus_IND_alamatIden.Text
                        If ObjectAntiNull(hfSwiftInPenerimaPenerus_IND_kotaIden.Value) = True Then
                            dr("Beneficiary_Nasabah_INDV_ID_KotaKab_Iden") = hfSwiftInPenerimaPenerus_IND_kotaIden.Value
                        End If

                        dr("Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden") = TxtSwiftInPenerimaPenerus_IND_KotaLainIden.Text
                        If ObjectAntiNull(Me.hfSwiftInPenerimaPenerus_IND_ProvIden.Value) = True Then
                            dr("Beneficiary_Nasabah_INDV_ID_Propinsi_Iden") = Me.hfSwiftInPenerimaPenerus_IND_ProvIden.Value
                        End If
                        dr("Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden") = TxtSwiftInPenerimaPenerus_IND_ProvLainIden.Text
                        dr("Beneficiary_Nasabah_INDV_NoTelp") = TxtSwiftInPenerimaPenerus_IND_NoTelp.Text
                        dr("Beneficiary_Nasabah_INDV_FK_IFTI_IDType") = cbo_SwiftInpenerimaNas_Ind_jenisidentitas.SelectedValue
                        dr("Beneficiary_Nasabah_INDV_NomorID") = TxtSwiftInPenerimaPenerus_IND_nomoridentitas.Text
                        If TxtSwiftInPenerimaPenerus_IND_NIlaiTransaksi.Text <> "" Then
                            dr("Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan") = TxtSwiftInPenerimaPenerus_IND_NIlaiTransaksi.Text
                        End If


                        ReceiverDataTable.Rows.Add(dr)

                        TxtSwiftInPenerimaPenerus_Rekening.Text = ""
                        TxtSwiftInPenerimaPenerus_Ind_namaBank.Text = ""
                        TxtSwiftInPenerimaPenerus_Ind_nama.Text = ""
                        TxtSwiftInPenerimaPenerus_Ind_TanggalLahir.Text = ""
                        RBSwiftInPenerimaPenerus_Ind_Kewarganegaraan.SelectedValue = 1
                        hfSwiftInPenerimaPenerus_IND_negara.Value = ""
                        TxtSwiftInPenerimaPenerus_IND_negara.Text = ""
                        TxtSwiftInPenerimaPenerus_IND_negaralain.Text = ""
                        hfSwiftInPenerimaPenerus_IND_pekerjaan.Value = ""
                        TxtSwiftInPenerimaPenerus_IND_pekerjaan.Text = ""
                        TxtSwiftInPenerimaPenerus_IND_pekerjaanLain.Text = ""
                        TxtSwiftInPenerimaPenerus_IND_alamatDom.Text = ""
                        hfSwiftInPenerimaPenerus_IND_kotaDom.Value = ""
                        TxtSwiftInPenerimaPenerus_IND_kotaDom.Text = ""
                        TxtSwiftInPenerimaPenerus_IND_kotaLainDom.Text = ""
                        hfSwiftInPenerimaPenerus_IND_ProvDom.Value = ""
                        TxtSwiftInPenerimaPenerus_IND_ProvDom.Text = ""
                        TxtSwiftInPenerimaPenerus_IND_ProvLainDom.Text = ""
                        TxtSwiftInPenerimaPenerus_IND_alamatIden.Text = ""
                        hfSwiftInPenerimaPenerus_IND_kotaIden.Value = ""
                        TxtSwiftInPenerimaPenerus_IND_kotaIden.Text = ""
                        TxtSwiftInPenerimaPenerus_IND_KotaLainIden.Text = ""
                        hfSwiftInPenerimaPenerus_IND_ProvIden.Value = ""
                        TxtSwiftInPenerimaPenerus_IND_ProvIden.Text = ""
                        TxtSwiftInPenerimaPenerus_IND_ProvLainIden.Text = ""
                        TxtSwiftInPenerimaPenerus_IND_NoTelp.Text = ""
                        cbo_SwiftInpenerimaNas_Ind_jenisidentitas.SelectedIndex = 0
                        TxtSwiftInPenerimaPenerus_IND_nomoridentitas.Text = ""
                        TxtSwiftInPenerimaPenerus_IND_NIlaiTransaksi.Text = ""
                        CheckBoxSwiftInPenerimaPenerus_IND_samaIden.Checked = False
                    Else
                        nasabahtype = 2
                        dr("FK_IFTI_NasabahType_ID") = nasabahtype
                        dr("PJKBank_type") = tipePJKBank

                        dr("Beneficiary_Nasabah_corp_NoRekening") = TxtSwiftInPenerimaPenerus_Rekening.Text

                        dr("Beneficiary_Nasabah_CORP_NamaKorporasi") = txtSwiftInPenerimaPenerus_Korp_NamaKorp.Text
                        dr("Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya") = txtSwiftInPenerimaPenerus_Korp_BentukBadanUsahaLain.Text
                        If (cbo_SwiftInPenerimaNasabah_Korp_BentukBadanUsaha.SelectedIndex <> 0) Then
                            dr("Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id") = CInt(cbo_SwiftInPenerimaPenerus_Korp_BentukBadanUsaha.SelectedValue)
                        End If

                        If ObjectAntiNull(hfSwiftInPenerimaNasabah_Korp_BidangUsahaKorp.Value) = True Then
                            dr("Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id") = hfSwiftInPenerimaPenerus_Korp_BidangUsahaKorp.Value
                        End If
                        dr("Beneficiary_Nasabah_CORP_BidangUsahaLainnya") = txtSwiftInPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Text
                        dr("Beneficiary_Nasabah_CORP_AlamatLengkap") = txtSwiftInPenerimaPenerus_Korp_alamatkorp.Text
                        If ObjectAntiNull(hfSwiftInPenerimaPenerus_Korp_kotakorp.Value) = True Then
                            dr("Beneficiary_Nasabah_CORP_ID_KotaKab") = hfSwiftInPenerimaPenerus_Korp_kotakorp.Value
                        End If

                        dr("Beneficiary_Nasabah_CORP_ID_KotaKabLainnya") = Me.txtSwiftInPenerimaPenerus_Korp_kotalainnyakorp.Text
                        If ObjectAntiNull(hfSwiftInPenerimaPenerus_Korp_ProvinsiKorp.Value) = True Then
                            dr("Beneficiary_Nasabah_CORP_ID_Propinsi") = hfSwiftInPenerimaPenerus_Korp_ProvinsiKorp.Value
                        End If

                        dr("Beneficiary_Nasabah_CORP_ID_PropinsiLainnya") = txtSwiftInPenerimaPenerus_Korp_provinsiLainnyaKorp.Text
                        dr("Beneficiary_Nasabah_CORP_NoTelp") = txtSwiftInPenerimaPenerus_Korp_NoTelp.Text
                        If txtSwiftInPenerimaPenerus_Korp_NilaiTransaksi.Text <> String.Empty Then
                            dr("Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan") = txtSwiftInPenerimaPenerus_Korp_NilaiTransaksi.Text
                        End If


                        ReceiverDataTable.Rows.Add(dr)

                        TxtSwiftInPenerimaPenerus_Rekening.Text = ""

                        txtSwiftInPenerimaPenerus_Korp_NamaKorp.Text = ""
                        txtSwiftInPenerimaPenerus_Korp_namabank.Text = ""
                        txtSwiftInPenerimaPenerus_Korp_BentukBadanUsahaLain.Text = ""
                        cbo_SwiftInPenerimaPenerus_Korp_BentukBadanUsaha.SelectedIndex = 0
                        hfSwiftInPenerimaPenerus_Korp_BidangUsahaKorp.Value = ""
                        txtSwiftInPenerimaPenerus_Korp_BidangUsahaKorp.Text = ""
                        Me.txtSwiftInPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Text = ""
                        txtSwiftInPenerimaPenerus_Korp_alamatkorp.Text = ""
                        hfSwiftInPenerimaPenerus_Korp_kotakorp.Value = ""
                        txtSwiftInPenerimaPenerus_Korp_kotakorp.Text = ""
                        txtSwiftInPenerimaPenerus_Korp_kotalainnyakorp.Text = ""
                        hfSwiftInPenerimaPenerus_Korp_ProvinsiKorp.Value = ""
                        txtSwiftInPenerimaPenerus_Korp_ProvinsiKorp.Text = ""
                        txtSwiftInPenerimaPenerus_Korp_provinsiLainnyaKorp.Text = ""
                        txtSwiftInPenerimaPenerus_Korp_NoTelp.Text = ""
                        txtSwiftInPenerimaPenerus_Korp_NilaiTransaksi.Text = ""
                    End If
                End If
                LoadReceiver()
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    Private Sub EditReceiver(ByVal index As Integer)
        Try
            If ValidateReceiver() Then
                Dim dr As Data.DataRow = Nothing
                Dim tipePJKBank As Integer = cboSwiftInPenerima_TipePJKBank.SelectedValue
                Dim tipepenerima As Integer = RbSwiftInPenerima_TipePenerima.SelectedValue
                Dim tipenasabah As Integer
                Dim nasabahtype As Integer

                dr = ReceiverDataTable.Rows(index)
                dr.BeginEdit()

                dr("FK_IFTI_ID") = getIFTIPK

                If tipePJKBank = 1 Then
                    If tipepenerima = 1 Then
                        tipenasabah = RbSwiftInPenerima_TipeNasabah.SelectedValue
                        If tipenasabah = 1 Then
                            nasabahtype = 1
                            dr("FK_IFTI_NasabahType_ID") = nasabahtype
                            dr("PJKBank_type") = tipePJKBank
                            dr("Beneficiary_Nasabah_INDV_NoRekening") = txtSwiftInPenerimaNasabah_Rekening.Text
                            dr("Beneficiary_Nasabah_INDV_NamaLengkap") = txtSwiftInPenerimaNasabah_IND_nama.Text
                            If txtSwiftInPenerimaNasabah_IND_TanggalLahir.Text <> "" Then
                                dr("Beneficiary_Nasabah_INDV_TanggalLahir") = txtSwiftInPenerimaNasabah_IND_TanggalLahir.Text.Trim() 'Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", txtSwiftInPenerimaNasabah_IND_TanggalLahir.Text.Trim())
                            End If
                            If RbSwiftInPenerimaNasabah_IND_Kewarganegaraan.SelectedValue <> "" Then
                                dr("Beneficiary_Nasabah_INDV_KewargaNegaraan") = RbSwiftInPenerimaNasabah_IND_Kewarganegaraan.SelectedValue
                            End If

                            If RbSwiftInPenerimaNasabah_IND_Kewarganegaraan.SelectedValue = 2 Then
                                dr("Beneficiary_Nasabah_INDV_Negara") = hfSwiftInPenerimaNasabah_IND_negara.Value
                                dr("Beneficiary_Nasabah_INDV_NegaraLainnya") = txtSwiftInPenerimaNasabah_IND_negaralain.Text

                            End If
                            dr("Beneficiary_Nasabah_INDV_Pekerjaan") = hfSwiftInIdenPenerimaNas_Ind_pekerjaan.Value
                            dr("Beneficiary_Nasabah_INDV_PekerjaanLainnya") = Me.TxtSwiftInIdenPenerimaNas_Ind_Pekerjaanlain.Text
                            'If ObjectAntiNull(hfSwiftInIdenPenerimaNas_Ind_provinsi_dom.Value) = True Then
                            '    dr("Beneficiary_Nasabah_INDV_ID_Propinsi_Dom") = hfSwiftInIdenPenerimaNas_Ind_provinsi_dom.Value
                            'End If
                            dr("Beneficiary_Nasabah_INDV_ID_Alamat_Dom") = TxtSwiftInIdenPenerimaNas_Ind_Alamat.Text
                            If ObjectAntiNull(hfSwiftInIdenPenerimaNas_Ind_Kota_dom.Value) = True Then
                                dr("Beneficiary_Nasabah_INDV_ID_KotaKab_Dom") = hfSwiftInIdenPenerimaNas_Ind_Kota_dom.Value
                            End If
                            dr("Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom") = TxtSwiftInIdenPenerimaNas_Ind_kotalain.Text
                            If ObjectAntiNull(Me.hfSwiftInIdenPenerimaNas_Ind_provinsi_dom.Value) = True Then
                                dr("Beneficiary_Nasabah_INDV_ID_Propinsi_Dom") = Me.hfSwiftInIdenPenerimaNas_Ind_provinsi_dom.Value
                            End If
                            dr("Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom") = TxtSwiftInIdenPenerimaNas_Ind_provinsiLain.Text
                            dr("Beneficiary_Nasabah_INDV_ID_Alamat_Iden") = TxtSwiftInIdenPenerimaNas_Ind_alamatIdentitas.Text
                            If ObjectAntiNull(hfSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Value) = True Then
                                dr("Beneficiary_Nasabah_INDV_ID_KotaKab_Iden") = hfSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Value
                            End If
                            dr("Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden") = TxtSwiftInIdenPenerimaNas_Ind_KotaLainIden.Text
                            If ObjectAntiNull(Me.hfSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Value) = True Then
                                dr("Beneficiary_Nasabah_INDV_ID_Propinsi_Iden") = Me.hfSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Value
                            End If
                            dr("Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden") = TxtSwiftInIdenPenerimaNas_Ind_ProvinsilainIden.Text
                            dr("Beneficiary_Nasabah_INDV_NoTelp") = TxtISwiftIndenPenerimaNas_Ind_noTelp.Text
                            dr("Beneficiary_Nasabah_INDV_FK_IFTI_IDType") = cbo_SwiftInpenerimaNas_Ind_jenisidentitas.SelectedValue
                            dr("Beneficiary_Nasabah_INDV_NomorID") = Me.TxtISwiftIndenPenerimaNas_Ind_noIdentitas.Text
                            If txtSwiftInIdenPenerimaNas_IND_NilaiTransaksiKeuangan.Text <> String.Empty Then
                                dr("Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan") = txtSwiftInIdenPenerimaNas_IND_NilaiTransaksiKeuangan.Text
                            End If

                            'korp
                            dr("Beneficiary_Nasabah_corp_NoRekening") = vbNull
                            dr("Beneficiary_Nasabah_CORP_NamaKorporasi") = vbNull
                            dr("Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya") = vbNull
                            dr("Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id") = vbNull
                            dr("Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id") = vbNull
                            dr("Beneficiary_Nasabah_CORP_BidangUsahaLainnya") = vbNull
                            dr("Beneficiary_Nasabah_CORP_AlamatLengkap") = vbNull
                            dr("Beneficiary_Nasabah_CORP_ID_KotaKab") = vbNull
                            dr("Beneficiary_Nasabah_CORP_ID_KotaKabLainnya") = vbNull
                            dr("Beneficiary_Nasabah_CORP_ID_Propinsi") = vbNull
                            dr("Beneficiary_Nasabah_CORP_ID_PropinsiLainnya") = vbNull
                            dr("Beneficiary_Nasabah_CORP_NoTelp") = vbNull
                            dr("Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan") = vbNull
                            'nonNasabah
                            dr("Beneficiary_NonNasabah_NamaLengkap") = vbNull
                            dr("Beneficiary_NonNasabah_TanggalLahir") = CType(Nothing, DateTime)
                            dr("Beneficiary_NonNasabah_ID_Alamat") = vbNull
                            dr("Beneficiary_NonNasabah_NoTelp") = vbNull
                            dr("Beneficiary_NonNasabah_FK_IFTI_IDType") = vbNull
                            dr("Beneficiary_NonNasabah_NomorID") = vbNull
                            dr("Beneficiary_NonNasabah_NilaiTransaksikeuangan") = vbNull


                            ReceiverDataTable.AcceptChanges()
                            txtSwiftInPenerimaNasabah_Rekening.Text = ""
                            txtSwiftInPenerimaNasabah_IND_nama.Text = ""
                            txtSwiftInPenerimaNasabah_IND_TanggalLahir.Text = ""
                            RbSwiftInPenerimaNasabah_IND_Kewarganegaraan.SelectedIndex = 0
                            hfSwiftInPenerimaNasabah_IND_negara.Value = ""
                            txtSwiftInPenerimaNasabah_IND_negara.Text = ""
                            txtSwiftInPenerimaNasabah_IND_negaralain.Text = ""
                            hfSwiftInIdenPenerimaNas_Ind_pekerjaan.Value = ""
                            txtSwiftInPenerimaNasabah_IND_pekerjaan.Text = ""
                            Me.TxtSwiftInIdenPenerimaNas_Ind_Pekerjaanlain.Text = ""
                            TxtSwiftInIdenPenerimaNas_Ind_Alamat.Text = ""
                            Me.hfSwiftInIdenPenerimaNas_Ind_Kota_dom.Value = ""
                            TxtSwiftInIdenPenerimaNas_Ind_Kota.Text = ""
                            TxtSwiftInIdenPenerimaNas_Ind_kotalain.Text = ""
                            hfSwiftInIdenPenerimaNas_Ind_provinsi_dom.Value = ""
                            TxtSwiftInIdenPenerimaNas_Ind_provinsi.Text = ""
                            TxtSwiftInIdenPenerimaNas_Ind_provinsiLain.Text = ""
                            TxtSwiftInIdenPenerimaNas_Ind_alamatIdentitas.Text = ""
                            hfSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Value = ""
                            Me.TxtSwiftInIdenPenerimaNas_Ind_KotaLainIden.Text = ""
                            TxtSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Text = ""
                            hfSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Value = ""
                            TxtSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Text = ""
                            TxtSwiftInIdenPenerimaNas_Ind_ProvinsilainIden.Text = ""
                            TxtISwiftIndenPenerimaNas_Ind_noTelp.Text = ""
                            cbo_SwiftInpenerimaNas_Ind_jenisidentitas.SelectedIndex = 0
                            Me.TxtISwiftIndenPenerimaNas_Ind_noIdentitas.Text = ""
                            txtSwiftInIdenPenerimaNas_IND_NilaiTransaksiKeuangan.Text = ""
                            ImageButton_savePenerimaInd.Visible = False
                            ImageButton_AddPenerimaInd.Visible = True
                            CheckSwiftInPenerimaNas_Ind_alamat.Checked = False

                        Else
                            nasabahtype = 2
                            dr("FK_IFTI_NasabahType_ID") = nasabahtype
                            dr("PJKBank_type") = tipePJKBank
                            dr("Beneficiary_Nasabah_corp_NoRekening") = txtSwiftInPenerimaNasabah_Rekening.Text

                            dr("Beneficiary_Nasabah_CORP_NamaKorporasi") = txtSwiftInPenerimaNasabah_Korp_namaKorp.Text
                            dr("Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya") = txtSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLainnya.Text
                            If cbo_SwiftInPenerimaNasabah_Korp_BentukBadanUsaha.SelectedIndex > 0 Then
                                dr("Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id") = CInt(cbo_SwiftInPenerimaNasabah_Korp_BentukBadanUsaha.SelectedValue)
                            End If

                            If ObjectAntiNull(hfSwiftInPenerimaNasabah_Korp_BidangUsahaKorp.Value) = True Then
                                dr("Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id") = hfSwiftInPenerimaNasabah_Korp_BidangUsahaKorp.Value
                            End If
                            dr("Beneficiary_Nasabah_CORP_BidangUsahaLainnya") = txtSwiftInPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Text
                            dr("Beneficiary_Nasabah_CORP_AlamatLengkap") = txtSwiftInPenerimaNasabah_Korp_AlamatKorp.Text
                            If ObjectAntiNull(hfSwiftInPenerimaNasabah_Korp_kotakab.Value) = True Then
                                dr("Beneficiary_Nasabah_CORP_ID_KotaKab") = hfSwiftInPenerimaNasabah_Korp_kotakab.Value
                            End If
                            dr("Beneficiary_Nasabah_CORP_ID_KotaKabLainnya") = Me.txtSwiftInPenerimaNasabah_Korp_kotaLain.Text
                            If ObjectAntiNull(hfSwiftInPenerimaNasabah_Korp_propinsi.Value) = True Then
                                dr("Beneficiary_Nasabah_CORP_ID_Propinsi") = hfSwiftInPenerimaNasabah_Korp_propinsi.Value
                            End If
                            dr("Beneficiary_Nasabah_CORP_ID_PropinsiLainnya") = txtSwiftInPenerimaNasabah_Korp_propinsilain.Text
                            dr("Beneficiary_Nasabah_CORP_NoTelp") = txtSwiftInPenerimaNasabah_Korp_NoTelp.Text
                            If txtSwiftInPenerimaNasabah_Korp_NilaiTransaksiKeuangan.Text <> "" Then
                                dr("Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan") = txtSwiftInPenerimaNasabah_Korp_NilaiTransaksiKeuangan.Text
                            End If

                            'indv
                            dr("Beneficiary_Nasabah_INDV_NoRekening") = vbNull
                            dr("Beneficiary_Nasabah_INDV_NamaLengkap") = vbNull
                            dr("Beneficiary_Nasabah_INDV_TanggalLahir") = CType(Nothing, DateTime)
                            dr("Beneficiary_Nasabah_INDV_KewargaNegaraan") = vbNull
                            dr("Beneficiary_Nasabah_INDV_Negara") = vbNull
                            dr("Beneficiary_Nasabah_INDV_NegaraLainnya") = vbNull
                            dr("Beneficiary_Nasabah_INDV_Pekerjaan") = vbNull
                            dr("Beneficiary_Nasabah_INDV_PekerjaanLainnya") = vbNull
                            dr("Beneficiary_Nasabah_INDV_ID_Alamat_Dom") = vbNull
                            dr("Beneficiary_Nasabah_INDV_ID_KotaKab_Dom") = vbNull
                            dr("Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom") = vbNull
                            dr("Beneficiary_Nasabah_INDV_ID_Propinsi_Dom") = vbNull
                            dr("Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom") = vbNull
                            dr("Beneficiary_Nasabah_INDV_ID_Alamat_Iden") = vbNull
                            dr("Beneficiary_Nasabah_INDV_ID_KotaKab_Iden") = vbNull
                            dr("Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden") = vbNull
                            dr("Beneficiary_Nasabah_INDV_ID_Propinsi_Iden") = vbNull
                            dr("Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden") = vbNull
                            dr("Beneficiary_Nasabah_INDV_NoTelp") = vbNull
                            dr("Beneficiary_Nasabah_INDV_FK_IFTI_IDType") = vbNull
                            dr("Beneficiary_Nasabah_INDV_NomorID") = vbNull
                            dr("Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan") = vbNull

                            'nonNasabah
                            dr("Beneficiary_NonNasabah_NamaLengkap") = vbNull
                            dr("Beneficiary_NonNasabah_TanggalLahir") = CType(Nothing, DateTime)
                            dr("Beneficiary_NonNasabah_ID_Alamat") = vbNull
                            dr("Beneficiary_NonNasabah_NoTelp") = vbNull
                            dr("Beneficiary_NonNasabah_FK_IFTI_IDType") = vbNull
                            dr("Beneficiary_NonNasabah_NomorID") = vbNull
                            dr("Beneficiary_NonNasabah_NilaiTransaksikeuangan") = vbNull

                            ReceiverDataTable.AcceptChanges()

                            txtSwiftInPenerimaNasabah_Rekening.Text = ""

                            txtSwiftInPenerimaNasabah_Korp_namaKorp.Text = ""
                            txtSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLainnya.Text = ""
                            cbo_SwiftInPenerimaNasabah_Korp_BentukBadanUsaha.SelectedIndex = 0
                            hfSwiftInPenerimaNasabah_Korp_BidangUsahaKorp.Value = ""
                            txtSwiftInPenerimaNasabah_Korp_BidangUsahaKorp.Text = ""
                            txtSwiftInPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Text = ""
                            txtSwiftInPenerimaNasabah_Korp_AlamatKorp.Text = ""
                            hfSwiftInPenerimaNasabah_Korp_kotakab.Value = ""
                            txtSwiftInPenerimaNasabah_Korp_Kotakab.Text = ""
                            Me.txtSwiftInPenerimaNasabah_Korp_kotaLain.Text = ""
                            hfSwiftInPenerimaNasabah_Korp_propinsi.Value = ""
                            txtSwiftInPenerimaNasabah_Korp_propinsi.Text = ""
                            txtSwiftInPenerimaNasabah_Korp_propinsilain.Text = ""
                            txtSwiftInPenerimaNasabah_Korp_NoTelp.Text = ""
                            txtSwiftInPenerimaNasabah_Korp_NilaiTransaksiKeuangan.Text = ""
                            ImageButton_savePenerimaKorp.Visible = False
                            ImageButton_AddPenerimaKorp.Visible = True
                        End If
                    ElseIf tipepenerima = 2 Then
                        nasabahtype = 3
                        dr("FK_IFTI_NasabahType_ID") = nasabahtype
                        dr("PJKBank_type") = tipePJKBank
                        'dr("Beneficiary_NonNasabah_NoRekening") = txtPenerima_rekening.Text

                        dr("Beneficiary_NonNasabah_NamaLengkap") = TxtSwiftInPenerimaNonNasabah_nama.Text
                        If TxtSwiftInPenerimaNonNasabah_TanggalLahir.Text <> "" Then
                            dr("Beneficiary_NonNasabah_TanggalLahir") = TxtSwiftInPenerimaNonNasabah_TanggalLahir.Text
                        End If

                        dr("Beneficiary_NonNasabah_ID_Alamat") = TxtSwiftInPenerimaNonNasabah_alamatiden.Text
                        dr("Beneficiary_NonNasabah_NoTelp") = TxtSwiftInPenerimaNonNasabah_NoTelepon.Text
                        dr("Beneficiary_NonNasabah_FK_IFTI_IDType") = CboSwiftInPenerimaNonNasabah_JenisDokumen.SelectedValue
                        dr("Beneficiary_NonNasabah_NomorID") = TxtSwiftInPenerimaNonNasabah_NomorIden.Text
                        If TxtSwiftInPenerimaNonNasabah_NilaiTransaksiKeuangan.Text <> "" Then
                            dr("Beneficiary_NonNasabah_NilaiTransaksikeuangan") = TxtSwiftInPenerimaNonNasabah_NilaiTransaksiKeuangan.Text
                        End If
                        'indv
                        dr("Beneficiary_Nasabah_INDV_NoRekening") = vbNull
                        dr("Beneficiary_Nasabah_INDV_NamaLengkap") = vbNull
                        dr("Beneficiary_Nasabah_INDV_TanggalLahir") = CType(Nothing, DateTime)
                        dr("Beneficiary_Nasabah_INDV_KewargaNegaraan") = vbNull
                        dr("Beneficiary_Nasabah_INDV_Negara") = vbNull
                        dr("Beneficiary_Nasabah_INDV_NegaraLainnya") = vbNull
                        dr("Beneficiary_Nasabah_INDV_Pekerjaan") = vbNull
                        dr("Beneficiary_Nasabah_INDV_PekerjaanLainnya") = vbNull
                        dr("Beneficiary_Nasabah_INDV_ID_Alamat_Dom") = vbNull
                        dr("Beneficiary_Nasabah_INDV_ID_KotaKab_Dom") = vbNull
                        dr("Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom") = vbNull
                        dr("Beneficiary_Nasabah_INDV_ID_Propinsi_Dom") = vbNull
                        dr("Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom") = vbNull
                        dr("Beneficiary_Nasabah_INDV_ID_Alamat_Iden") = vbNull
                        dr("Beneficiary_Nasabah_INDV_ID_KotaKab_Iden") = vbNull
                        dr("Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden") = vbNull
                        dr("Beneficiary_Nasabah_INDV_ID_Propinsi_Iden") = vbNull
                        dr("Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden") = vbNull
                        dr("Beneficiary_Nasabah_INDV_NoTelp") = vbNull
                        dr("Beneficiary_Nasabah_INDV_FK_IFTI_IDType") = vbNull
                        dr("Beneficiary_Nasabah_INDV_NomorID") = vbNull
                        dr("Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan") = vbNull

                        'korp
                        dr("Beneficiary_Nasabah_corp_NoRekening") = vbNull
                        dr("Beneficiary_Nasabah_CORP_NamaKorporasi") = vbNull
                        dr("Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya") = vbNull
                        dr("Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id") = vbNull
                        dr("Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id") = vbNull
                        dr("Beneficiary_Nasabah_CORP_BidangUsahaLainnya") = vbNull
                        dr("Beneficiary_Nasabah_CORP_AlamatLengkap") = vbNull
                        dr("Beneficiary_Nasabah_CORP_ID_KotaKab") = vbNull
                        dr("Beneficiary_Nasabah_CORP_ID_KotaKabLainnya") = vbNull
                        dr("Beneficiary_Nasabah_CORP_ID_Propinsi") = vbNull
                        dr("Beneficiary_Nasabah_CORP_ID_PropinsiLainnya") = vbNull
                        dr("Beneficiary_Nasabah_CORP_NoTelp") = vbNull
                        dr("Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan") = vbNull

                        ReceiverDataTable.AcceptChanges()
                        'txtPenerima_rekening.Text = ""

                        TxtSwiftInPenerimaNonNasabah_nama.Text = ""
                        TxtSwiftInPenerimaNonNasabah_TanggalLahir.Text = ""
                        TxtSwiftInPenerimaNonNasabah_alamatiden.Text = ""
                        TxtSwiftInPenerimaNonNasabah_NoTelepon.Text = ""
                        CboSwiftInPenerimaNonNasabah_JenisDokumen.SelectedIndex = 0
                        TxtSwiftInPenerimaNonNasabah_NomorIden.Text = ""
                        TxtSwiftInPenerimaNonNasabah_NilaiTransaksiKeuangan.Text = ""
                        ImageButton_SavePenerimaNonNasabah.Visible = False
                        ImageButton_AddPenerimaNonNasabah.Visible = True
                    End If

                Else
                    tipenasabah = RbSwiftInPenerima_TipeNasabah.SelectedValue
                    If tipenasabah = 1 Then
                        nasabahtype = 1
                        dr("FK_IFTI_NasabahType_ID") = nasabahtype
                        dr("PJKBank_type") = tipePJKBank
                        dr("Beneficiary_Nasabah_INDV_NoRekening") = TxtSwiftInPenerimaPenerus_Rekening.Text
                        dr("Beneficiary_Nasabah_INDV_NamaBank") = TxtSwiftInPenerimaPenerus_Ind_namaBank.Text
                        dr("Beneficiary_Nasabah_INDV_NamaLengkap") = TxtSwiftInPenerimaPenerus_Ind_nama.Text
                        dr("Beneficiary_Nasabah_INDV_TanggalLahir") = TxtSwiftInPenerimaPenerus_Ind_TanggalLahir.Text.Trim() 'Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", TxtSwiftInPenerimaPenerus_Ind_TanggalLahir.Text.Trim())
                        dr("Beneficiary_Nasabah_INDV_KewargaNegaraan") = RBSwiftInPenerimaPenerus_Ind_Kewarganegaraan.SelectedValue
                        If ObjectAntiNull(hfSwiftInPenerimaPenerus_IND_negara.Value) = True Then
                            dr("Beneficiary_Nasabah_INDV_Negara") = hfSwiftInPenerimaPenerus_IND_negara.Value
                        End If

                        dr("Beneficiary_Nasabah_INDV_NegaraLainnya") = TxtSwiftInPenerimaPenerus_IND_negaralain.Text
                        If ObjectAntiNull(Me.hfSwiftInPenerimaPenerus_IND_pekerjaan.Value) = True Then
                            dr("Beneficiary_Nasabah_INDV_Pekerjaan") = Me.hfSwiftInPenerimaPenerus_IND_pekerjaan.Value
                        End If
                        dr("Beneficiary_Nasabah_INDV_PekerjaanLainnya") = TxtSwiftInPenerimaPenerus_IND_pekerjaanLain.Text

                        dr("Beneficiary_Nasabah_INDV_ID_Alamat_Dom") = TxtSwiftInPenerimaPenerus_IND_alamatDom.Text
                        If ObjectAntiNull(hfSwiftInPenerimaPenerus_IND_kotaDom.Value) = True Then
                            dr("Beneficiary_Nasabah_INDV_ID_KotaKab_Dom") = hfSwiftInPenerimaPenerus_IND_kotaDom.Value
                        End If
                        dr("Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom") = TxtSwiftInPenerimaPenerus_IND_kotaLainDom.Text
                        If ObjectAntiNull(hfSwiftInPenerimaPenerus_IND_ProvDom.Value) = True Then
                            dr("Beneficiary_Nasabah_INDV_ID_Propinsi_Dom") = hfSwiftInPenerimaPenerus_IND_ProvDom.Value
                        End If

                        dr("Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom") = TxtSwiftInPenerimaPenerus_IND_ProvLainDom.Text
                        dr("Beneficiary_Nasabah_INDV_ID_Alamat_Iden") = TxtSwiftInPenerimaPenerus_IND_alamatIden.Text
                        If ObjectAntiNull(hfSwiftInPenerimaPenerus_IND_kotaIden.Value) = True Then
                            dr("Beneficiary_Nasabah_INDV_ID_KotaKab_Iden") = hfSwiftInPenerimaPenerus_IND_kotaIden.Value
                        End If

                        dr("Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden") = TxtSwiftInPenerimaPenerus_IND_KotaLainIden.Text
                        If ObjectAntiNull(Me.hfSwiftInPenerimaPenerus_IND_ProvIden.Value) = True Then
                            dr("Beneficiary_Nasabah_INDV_ID_Propinsi_Iden") = Me.hfSwiftInPenerimaPenerus_IND_ProvIden.Value
                        End If
                        dr("Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden") = TxtSwiftInPenerimaPenerus_IND_ProvLainIden.Text
                        dr("Beneficiary_Nasabah_INDV_NoTelp") = TxtSwiftInPenerimaPenerus_IND_NoTelp.Text
                        dr("Beneficiary_Nasabah_INDV_FK_IFTI_IDType") = cbo_SwiftInpenerimaNas_Ind_jenisidentitas.SelectedValue
                        dr("Beneficiary_Nasabah_INDV_NomorID") = TxtSwiftInPenerimaPenerus_IND_nomoridentitas.Text
                        If TxtSwiftInPenerimaPenerus_IND_NIlaiTransaksi.Text <> "" Then
                            dr("Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan") = TxtSwiftInPenerimaPenerus_IND_NIlaiTransaksi.Text
                        End If

                        'korp
                        dr("Beneficiary_Nasabah_corp_NoRekening") = Nothing
                        dr("Beneficiary_Nasabah_CORP_NamaKorporasi") = Nothing
                        dr("Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya") = Nothing
                        dr("Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id") = Nothing
                        dr("Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id") = Nothing
                        dr("Beneficiary_Nasabah_CORP_BidangUsahaLainnya") = Nothing
                        dr("Beneficiary_Nasabah_CORP_AlamatLengkap") = Nothing
                        dr("Beneficiary_Nasabah_CORP_ID_KotaKab") = Nothing
                        dr("Beneficiary_Nasabah_CORP_ID_KotaKabLainnya") = Nothing
                        dr("Beneficiary_Nasabah_CORP_ID_Propinsi") = Nothing
                        dr("Beneficiary_Nasabah_CORP_ID_PropinsiLainnya") = Nothing
                        dr("Beneficiary_Nasabah_CORP_NoTelp") = Nothing
                        dr("Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan") = Nothing
                        'nonNasabah
                        dr("Beneficiary_NonNasabah_NamaLengkap") = Nothing
                        dr("Beneficiary_NonNasabah_TanggalLahir") = CType(Nothing, DateTime)
                        dr("Beneficiary_NonNasabah_ID_Alamat") = Nothing
                        dr("Beneficiary_NonNasabah_NoTelp") = Nothing
                        dr("Beneficiary_NonNasabah_FK_IFTI_IDType") = Nothing
                        dr("Beneficiary_NonNasabah_NomorID") = Nothing
                        dr("Beneficiary_NonNasabah_NilaiTransaksikeuangan") = Nothing



                        ReceiverDataTable.AcceptChanges()

                        TxtSwiftInPenerimaPenerus_Rekening.Text = ""
                        TxtSwiftInPenerimaPenerus_Ind_namaBank.Text = ""
                        TxtSwiftInPenerimaPenerus_Ind_nama.Text = ""
                        TxtSwiftInPenerimaPenerus_Ind_TanggalLahir.Text = ""
                        RBSwiftInPenerimaPenerus_Ind_Kewarganegaraan.SelectedValue = 1
                        hfSwiftInPenerimaPenerus_IND_negara.Value = ""
                        TxtSwiftInPenerimaPenerus_IND_negara.Text = ""
                        TxtSwiftInPenerimaPenerus_IND_negaralain.Text = ""
                        hfSwiftInPenerimaPenerus_IND_pekerjaan.Value = ""
                        TxtSwiftInPenerimaPenerus_IND_pekerjaan.Text = ""
                        TxtSwiftInPenerimaPenerus_IND_pekerjaanLain.Text = ""
                        TxtSwiftInPenerimaPenerus_IND_alamatDom.Text = ""
                        hfSwiftInPenerimaPenerus_IND_kotaDom.Value = ""
                        TxtSwiftInPenerimaPenerus_IND_kotaDom.Text = ""
                        TxtSwiftInPenerimaPenerus_IND_kotaLainDom.Text = ""
                        hfSwiftInPenerimaPenerus_IND_ProvDom.Value = ""
                        TxtSwiftInPenerimaPenerus_IND_ProvDom.Text = ""
                        TxtSwiftInPenerimaPenerus_IND_ProvLainDom.Text = ""
                        TxtSwiftInPenerimaPenerus_IND_alamatIden.Text = ""
                        hfSwiftInPenerimaPenerus_IND_kotaIden.Value = ""
                        TxtSwiftInPenerimaPenerus_IND_kotaIden.Text = ""
                        TxtSwiftInPenerimaPenerus_IND_KotaLainIden.Text = ""
                        hfSwiftInPenerimaPenerus_IND_ProvIden.Value = ""
                        TxtSwiftInPenerimaPenerus_IND_ProvIden.Text = ""
                        TxtSwiftInPenerimaPenerus_IND_ProvLainIden.Text = ""
                        TxtSwiftInPenerimaPenerus_IND_NoTelp.Text = ""
                        cbo_SwiftInpenerimaNas_Ind_jenisidentitas.SelectedIndex = 0
                        TxtSwiftInPenerimaPenerus_IND_nomoridentitas.Text = ""
                        TxtSwiftInPenerimaPenerus_IND_NIlaiTransaksi.Text = ""
                        ImageButton_SavePenerimaPenerusInd.Visible = False
                        ImageButton_AddPenerimaPenerusInd.Visible = True
                        CheckBoxSwiftInPenerimaPenerus_IND_samaIden.Checked = False
                    Else
                        nasabahtype = 2
                        dr("FK_IFTI_NasabahType_ID") = nasabahtype
                        dr("PJKBank_type") = tipePJKBank
                        dr("Beneficiary_Nasabah_corp_NoRekening") = TxtSwiftInPenerimaPenerus_Rekening.Text

                        dr("Beneficiary_Nasabah_CORP_NamaKorporasi") = txtSwiftInPenerimaPenerus_Korp_NamaKorp.Text
                        dr("Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya") = txtSwiftInPenerimaPenerus_Korp_BentukBadanUsahaLain.Text
                        If Not IsNothing(cbo_SwiftInPenerimaNasabah_Korp_BentukBadanUsaha.SelectedValue) Then
                            dr("Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id") = CInt(cbo_SwiftInPenerimaPenerus_Korp_BentukBadanUsaha.SelectedValue)
                        End If

                        If ObjectAntiNull(hfSwiftInPenerimaNasabah_Korp_BidangUsahaKorp.Value) = True Then
                            dr("Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id") = hfSwiftInPenerimaPenerus_Korp_BidangUsahaKorp.Value
                        End If
                        dr("Beneficiary_Nasabah_CORP_BidangUsahaLainnya") = txtSwiftInPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Text
                        dr("Beneficiary_Nasabah_CORP_AlamatLengkap") = txtSwiftInPenerimaPenerus_Korp_alamatkorp.Text
                        If ObjectAntiNull(hfSwiftInPenerimaPenerus_Korp_kotakorp.Value) = True Then
                            dr("Beneficiary_Nasabah_CORP_ID_KotaKab") = hfSwiftInPenerimaPenerus_Korp_kotakorp.Value
                        End If

                        dr("Beneficiary_Nasabah_CORP_ID_KotaKabLainnya") = Me.txtSwiftInPenerimaPenerus_Korp_kotalainnyakorp.Text
                        If ObjectAntiNull(hfSwiftInPenerimaPenerus_Korp_ProvinsiKorp.Value) = True Then
                            dr("Beneficiary_Nasabah_CORP_ID_Propinsi") = hfSwiftInPenerimaPenerus_Korp_ProvinsiKorp.Value
                        End If

                        dr("Beneficiary_Nasabah_CORP_ID_PropinsiLainnya") = txtSwiftInPenerimaPenerus_Korp_provinsiLainnyaKorp.Text
                        dr("Beneficiary_Nasabah_CORP_NoTelp") = txtSwiftInPenerimaPenerus_Korp_NoTelp.Text
                        If txtSwiftInPenerimaPenerus_Korp_NilaiTransaksi.Text <> "" Then
                            dr("Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan") = txtSwiftInPenerimaPenerus_Korp_NilaiTransaksi.Text
                        End If

                        'indv
                        dr("Beneficiary_Nasabah_INDV_NoRekening") = Nothing
                        dr("Beneficiary_Nasabah_INDV_NamaLengkap") = Nothing
                        dr("Beneficiary_Nasabah_INDV_TanggalLahir") = CType(Nothing, DateTime)
                        dr("Beneficiary_Nasabah_INDV_KewargaNegaraan") = Nothing
                        dr("Beneficiary_Nasabah_INDV_Negara") = Nothing
                        dr("Beneficiary_Nasabah_INDV_NegaraLainnya") = Nothing
                        dr("Beneficiary_Nasabah_INDV_Pekerjaan") = Nothing
                        dr("Beneficiary_Nasabah_INDV_PekerjaanLainnya") = Nothing
                        dr("Beneficiary_Nasabah_INDV_ID_Alamat_Dom") = Nothing
                        dr("Beneficiary_Nasabah_INDV_ID_KotaKab_Dom") = Nothing
                        dr("Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom") = Nothing
                        dr("Beneficiary_Nasabah_INDV_ID_Propinsi_Dom") = Nothing
                        dr("Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom") = Nothing
                        dr("Beneficiary_Nasabah_INDV_ID_Alamat_Iden") = Nothing
                        dr("Beneficiary_Nasabah_INDV_ID_KotaKab_Iden") = Nothing
                        dr("Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden") = Nothing
                        dr("Beneficiary_Nasabah_INDV_ID_Propinsi_Iden") = Nothing
                        dr("Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden") = Nothing
                        dr("Beneficiary_Nasabah_INDV_NoTelp") = Nothing
                        dr("Beneficiary_Nasabah_INDV_FK_IFTI_IDType") = Nothing
                        dr("Beneficiary_Nasabah_INDV_NomorID") = Nothing
                        dr("Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan") = Nothing

                        'nonNasabah
                        dr("Beneficiary_NonNasabah_NamaLengkap") = Nothing
                        dr("Beneficiary_NonNasabah_TanggalLahir") = CType(Nothing, DateTime)
                        dr("Beneficiary_NonNasabah_ID_Alamat") = Nothing
                        dr("Beneficiary_NonNasabah_NoTelp") = Nothing
                        dr("Beneficiary_NonNasabah_FK_IFTI_IDType") = Nothing
                        dr("Beneficiary_NonNasabah_NomorID") = Nothing
                        dr("Beneficiary_NonNasabah_NilaiTransaksikeuangan") = Nothing


                        ReceiverDataTable.AcceptChanges()

                        TxtSwiftInPenerimaPenerus_Rekening.Text = ""

                        txtSwiftInPenerimaPenerus_Korp_NamaKorp.Text = ""
                        txtSwiftInPenerimaPenerus_Korp_BentukBadanUsahaLain.Text = ""
                        cbo_SwiftInPenerimaPenerus_Korp_BentukBadanUsaha.SelectedIndex = 0
                        hfSwiftInPenerimaPenerus_Korp_BidangUsahaKorp.Value = ""
                        txtSwiftInPenerimaPenerus_Korp_BidangUsahaKorp.Text = ""
                        Me.txtSwiftInPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Text = ""
                        txtSwiftInPenerimaPenerus_Korp_alamatkorp.Text = ""
                        hfSwiftInPenerimaPenerus_Korp_kotakorp.Value = ""
                        txtSwiftInPenerimaPenerus_Korp_kotakorp.Text = ""
                        txtSwiftInPenerimaPenerus_Korp_kotalainnyakorp.Text = ""
                        hfSwiftInPenerimaPenerus_Korp_ProvinsiKorp.Value = ""
                        txtSwiftInPenerimaPenerus_Korp_ProvinsiKorp.Text = ""
                        txtSwiftInPenerimaPenerus_Korp_provinsiLainnyaKorp.Text = ""
                        txtSwiftInPenerimaPenerus_Korp_NoTelp.Text = ""
                        txtSwiftInPenerimaPenerus_Korp_NilaiTransaksi.Text = ""
                        ImageButton_SavePenerimaPenerusKorp.Visible = False
                        ImageButton_AddPenerimaPenerusKorp.Visible = True
                    End If
                End If
                GridDataView.Enabled = True
                getIFTIBeneficiaryTempPK = Nothing
                LoadReceiver()
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
#End Region
#End Region
#Region "SwiftInSender..."
    Sub FieldSwiftInPengirimNasabahPerorangan()
        'PengirimNasabahPerorangan

        Dim objNegara As TList(Of MsNegara) = DataRepository.MsNegaraProvider.GetPaged(MsNegaraColumn.Activation.ToString & "=1", MsNegaraColumn.NamaNegara.ToString, 0, Integer.MaxValue, 0)
        Dim objSnegara As MsNegara 'Penampung hasil search negara yang pke picker
        'parameter value

        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
        Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
        Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)

        Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)
            SetnGetSenderAccount = objIfti.Sender_Nasabah_INDV_NoRekening
            Me.txtSwiftInPengirim_rekening.Text = objIfti.Sender_Nasabah_INDV_NoRekening
            Me.txtSwiftInPengirimNasabah_IND_nama.Text = objIfti.Sender_Nasabah_INDV_NamaLengkap
            Me.txtSwiftInPengirimNasabah_IND_TanggalLahir.Text = FormatDate(objIfti.Sender_Nasabah_INDV_TanggalLahir)
            If Not IsNothing(objIfti.Sender_Nasabah_INDV_KewargaNegaraan) Then
                Me.RbSwiftInPengirimNasabah_IND_Warganegara.SelectedValue = objIfti.Sender_Nasabah_INDV_KewargaNegaraan
                If objIfti.Sender_Nasabah_INDV_KewargaNegaraan = "2" Then
                    Me.negaraPengirimInd.Visible = True
                    objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, objIfti.Sender_Nasabah_INDV_Negara)
                    If Not IsNothing(objSnegara) Then
                        txtSwiftInPengirimNasabah_IND_negara.Text = Safe(objSnegara.NamaNegara)
                        hfSwiftInPengirimNasabah_IND_negara.Value = Safe(objSnegara.IDNegara)
                        If hfSwiftInPengirimNasabah_IND_negara.Value = ParameterNegara.MsSystemParameter_Value Then
                            Me.txtSwiftInPengirimNasabah_IND_negaraLain.Text = objIfti.Sender_Nasabah_INDV_NegaraLainnya
                            Me.negaraLainPengirimInd.Visible = True
                        End If
                    End If
                End If
            End If

            Me.txtSwiftInPengirimNasabah_IND_alamatIden.Text = objIfti.Sender_Nasabah_INDV_ID_Alamat
            Me.txtSwiftInPengirimNasabah_IND_negaraBagian.Text = objIfti.Sender_Nasabah_INDV_ID_NegaraKota
            objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, objIfti.Sender_Nasabah_INDV_ID_Negara)
            If Not IsNothing(objSnegara) Then
                txtSwiftInPengirimNasabah_IND_negaraIden.Text = Safe(objSnegara.NamaNegara)
                hfSwiftInPengirimNasabah_IND_negaraIden.Value = Safe(objSnegara.IDNegara)
                If hfSwiftInPengirimNasabah_IND_negaraIden.Value = ParameterNegara.MsSystemParameter_Value Then
                    Me.txtSwiftInPengirimNasabah_IND_negaraLainIden.Text = objIfti.Sender_Nasabah_INDV_ID_NegaraLainnya
                End If
            End If
            Me.txtSwiftInPengirimNasabah_IND_NoTelp.Text = objIfti.Sender_Nasabah_INDV_NoTelp
            Me.cbo_SwiftInpengirimNas_Ind_jenisidentitas.SelectedValue = objIfti.Sender_Nasabah_INDV_FK_IFTI_IDType.GetValueOrDefault(0)
            Me.txtSwiftInPengirimNasabah_IND_NomorIdentitas.Text = objIfti.Sender_Nasabah_INDV_NomorID
        End Using
    End Sub
    Sub FieldSwiftInPengirimNasabahKorporasi()
        'PengirimNasabahKorporasi
        Dim objNegara As TList(Of MsNegara) = DataRepository.MsNegaraProvider.GetPaged(MsNegaraColumn.Activation.ToString & "=1", MsNegaraColumn.NamaNegara.ToString, 0, Integer.MaxValue, 0)
        Dim objSnegara As MsNegara 'Penampung hasil search negara yang pke picker
        Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)

        Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)
            SetnGetSenderAccount = objIfti.Sender_Nasabah_CORP_NoRekening
            Me.txtSwiftInPengirim_rekening.Text = objIfti.Sender_Nasabah_CORP_NoRekening
            Me.txtSwiftInPengirimNasabah_Korp_namaKorp.Text = objIfti.Sender_Nasabah_CORP_NamaKorporasi
            Me.txtSwiftInPengirimNasabah_Korp_AlamatKorp.Text = objIfti.Sender_Nasabah_CORP_AlamatLengkap
            Me.txtSwiftInPengirimNasabah_Korp_Kota.Text = objIfti.Sender_Nasabah_CORP_NegaraKota
            objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, objIfti.Sender_Nasabah_CORP_Negara)
            If Not IsNothing(objSnegara) Then
                txtSwiftInPengirimNasabah_Korp_NegaraKorp.Text = Safe(objSnegara.NamaNegara)
                hfSwiftInPengirimNasabah_Korp_NegaraKorp.Value = Safe(objSnegara.IDNegara)
                If hfSwiftInPengirimNasabah_Korp_NegaraKorp.Value = ParameterNegara.MsSystemParameter_Value Then
                    Me.trSwiftInPengirimNasabah_Korp_NegaraLainnyaKorp.Visible = True
                    Me.txtSwiftInPengirimNasabah_Korp_NegaraLainnyaKorp.Text = objIfti.Sender_Nasabah_CORP_NegaraLainnya
                End If
            End If
            Me.txtSwiftInPengirimNasabah_Korp_NoTelp.Text = objIfti.Sender_Nasabah_CORP_NoTelp
        End Using
    End Sub
    Sub FieldSwiftInPengirimNonNasabah()
        'PenerimaNonNasabah

        Dim objNegara As TList(Of MsNegara) = DataRepository.MsNegaraProvider.GetPaged(MsNegaraColumn.Activation.ToString & "=1", MsNegaraColumn.NamaNegara.ToString, 0, Integer.MaxValue, 0)
        Dim objSnegara As MsNegara 'Penampung hasil search negara yang pke picker
        Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
        Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)
            SetnGetSenderAccount = objIfti.Sender_NonNasabah_NoRekening
            Me.txtSwiftInPengirimNonNasabah_rekening.Text = objIfti.Sender_NonNasabah_NoRekening
            Me.txtSwiftInPengirimNonNasabah_namabank.Text = objIfti.Sender_NonNasabah_NamaBank
            Me.txtSwiftInPengirimNonNasabah_Nama.Text = objIfti.Sender_NonNasabah_NamaLengkap
            Me.txtSwiftInPengirimNonNasabah_TanggalLahir.Text = FormatDate(objIfti.Sender_NonNasabah_TanggalLahir)
            Me.txtSwiftInPengirimNonNasabah_Alamat.Text = objIfti.Sender_NonNasabah_ID_Alamat
            Me.txtSwiftInPengirimNonNasabah_Kota.Text = objIfti.Sender_NonNasabah_ID_NegaraBagian
            objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, objIfti.Sender_NonNasabah_ID_Negara)
            If Not IsNothing(objSnegara) Then
                txtSwiftInPengirimNonNasabah_Negara.Text = Safe(objSnegara.NamaNegara)
                hfSwiftInPengirimNonNasabah_Negara.Value = Safe(objSnegara.IDNegara)
                If hfSwiftInPengirimNonNasabah_Negara.Value = ParameterNegara.MsSystemParameter_Value Then
                    Me.trSwiftInPengirimNonNasabah_negaraLain.Visible = True
                    Me.txtSwiftInPengirimNonNasabah_negaraLain.Text = objIfti.Sender_NonNasabah_ID_NegaraLainnya
                End If
            End If
            Me.txtSwiftInPengirimNonNasabah_NoTelp.Text = objIfti.Sender_NonNasabah_NoTelp
        End Using
    End Sub
#End Region
#Region "SwiftInReceiverType..."
    Sub FieldSwiftInReceiverCase1()
        'PengirimNasabahIndividu
        Dim objNegara As TList(Of MsNegara) = DataRepository.MsNegaraProvider.GetPaged(MsNegaraColumn.Activation.ToString & "=1", MsNegaraColumn.NamaNegara.ToString, 0, Integer.MaxValue, 0)
        Dim objSnegara As MsNegara 'Penampung hasil search negara yang pke picker
        Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetPaged(MsKotaKabColumn.Activation.ToString & "=1", MsKotaKabColumn.NamaKotaKab.ToString, 0, Integer.MaxValue, 0)
        Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
        Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetPaged(MsProvinceColumn.activation.ToString & "=1", MsProvinceColumn.Nama.ToString, 0, Integer.MaxValue, 0)
        Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi
        Dim objPekerjaan As TList(Of MsPekerjaan) = DataRepository.MsPekerjaanProvider.GetPaged(MsPekerjaanColumn.Activation.ToString & "=1", MsPekerjaanColumn.NamaPekerjaan.ToString, 0, Integer.MaxValue, 0)
        Dim objSPekerjaan As MsPekerjaan 'Penampung hasil search pekerjaan
        'parameter value

        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
        Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
        Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)

        'Using objIftiBeneficiary As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("FK_IFTI_ID=" & getIFTIPK, "", 0, 1, 0)
        '    'SetnGetSenderAccount = objIfti.Sender_Nasabah_INDV_NoRekening
        '    Me.txtSwiftInPenerimaNasabah_Rekening.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_NoRekening
        '    Me.txtSwiftInPenerimaNasabah_IND_nama.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_NamaLengkap
        '    Me.txtSwiftInPenerimaNasabah_IND_TanggalLahir.Text = FormatDate(objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_TanggalLahir)
        '    Me.RbSwiftInPenerimaNasabah_IND_Kewarganegaraan.SelectedValue = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_KewargaNegaraan
        '    If objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_KewargaNegaraan = "2" Then
        '        Me.negaraPenerimaInd.Visible = True
        '        Me.negaraLainPenerimaInd.Visible = True
        '        objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_Negara)
        '        If Not IsNothing(objSnegara) Then
        '            txtSwiftInPenerimaNasabah_IND_negara.Text = Safe(objSnegara.NamaNegara)
        '            hfSwiftInPenerimaNasabah_IND_negara.Value = Safe(objSnegara.IDNegara)
        '        End If
        '        Me.txtSwiftInPenerimaNasabah_IND_negaralain.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_NegaraLainnya
        '    End If
        '    objSPekerjaan = objPekerjaan.Find(MsPekerjaanColumn.IDPekerjaan, objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_Pekerjaan)
        '    If Not IsNothing(objSPekerjaan) Then
        '        txtSwiftInPenerimaNasabah_IND_pekerjaan.Text = Safe(objSPekerjaan.NamaPekerjaan)
        '        hfSwiftInIdenPenerimaNas_Ind_pekerjaan.Value = Safe(objSPekerjaan.IDPekerjaan)
        '    End If
        '    Me.TxtSwiftInIdenPenerimaNas_Ind_Pekerjaanlain.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_PekerjaanLainnya
        '    'Alamat Domisili
        '    Me.TxtSwiftInIdenPenerimaNas_Ind_Alamat.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_ID_Alamat_Dom
        '    objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_ID_KotaKab_Dom)
        '    If Not IsNothing(objSkotaKab) Then
        '        Me.TxtSwiftInIdenPenerimaNas_Ind_Kota.Text = Safe(objSkotaKab.NamaKotaKab)
        '        hfSwiftInIdenPenerimaNas_Ind_Kota_dom.Value = Safe(objSkotaKab.IDKotaKab)
        '    End If
        '    Me.TxtSwiftInIdenPenerimaNas_Ind_kotalain.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom
        '    objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(SafeNumIndex(objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_ID_Propinsi_Dom)) 'GetPaged("IdProvince=" & CInt(objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_ID_Propinsi_Dom), "", 0, Integer.MaxValue, 0)
        '    If Not IsNothing(objSProvinsi) Then
        '        Me.TxtSwiftInIdenPenerimaNas_Ind_provinsi.Text = Safe(objSProvinsi.Nama)
        '        hfSwiftInIdenPenerimaNas_Ind_provinsi_dom.Value = Safe(objSProvinsi.IdProvince)
        '    End If

        '    Me.TxtSwiftInIdenPenerimaNas_Ind_provinsiLain.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom

        '    'Alamat Identitas 
        '    Me.TxtSwiftInIdenPenerimaNas_Ind_alamatIdentitas.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_ID_Alamat_Iden
        '    objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_ID_KotaKab_Iden)
        '    If Not IsNothing(objSkotaKab) Then
        '        Me.TxtSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Text = Safe(objSkotaKab.NamaKotaKab)
        '        hfSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Value = Safe(objSkotaKab.IDKotaKab)
        '    End If
        '    Me.TxtSwiftInIdenPenerimaNas_Ind_KotaLainIden.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden
        '    objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(SafeNumIndex(objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_ID_Propinsi_Iden)) 'GetPaged("IdProvince=" & CInt(objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_ID_Propinsi_Iden), "", 0, Integer.MaxValue, 0)
        '    If Not IsNothing(objSProvinsi) Then
        '        Me.TxtSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Text = Safe(objSProvinsi.Nama)
        '        hfSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Value = Safe(objSProvinsi.IdProvince)
        '    End If

        '    Me.TxtSwiftInIdenPenerimaNas_Ind_ProvinsilainIden.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden
        '    Me.TxtISwiftIndenPenerimaNas_Ind_noTelp.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_NoTelp
        '    Me.cbo_SwiftInpenerimaNas_Ind_jenisidentitas.SelectedValue = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_FK_IFTI_IDType
        '    Me.TxtISwiftIndenPenerimaNas_Ind_noIdentitas.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_NomorID
        '    Me.txtSwiftInIdenPenerimaNas_IND_NilaiTransaksiKeuangan.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan
        'End Using
        Dim dr As Data.DataRow = ReceiverDataTable.Rows(CInt(getIFTIBeneficiaryTempPK - 1))
        'dr("FK_IFTI_NasabahType_ID") = 1
        Me.txtSwiftInPenerimaNasabah_Rekening.Text = dr("Beneficiary_Nasabah_INDV_NoRekening").ToString
        Me.txtSwiftInPenerimaNasabah_IND_nama.Text = dr("Beneficiary_Nasabah_INDV_NamaLengkap").ToString
        Me.txtSwiftInPenerimaNasabah_IND_TanggalLahir.Text = FormatDate(dr("Beneficiary_Nasabah_INDV_TanggalLahir"))

        If Not IsNothing(dr("Beneficiary_Nasabah_INDV_KewargaNegaraan")) Then
            Me.RbSwiftInPenerimaNasabah_IND_Kewarganegaraan.SelectedValue = dr("Beneficiary_Nasabah_INDV_KewargaNegaraan")
            If dr("Beneficiary_Nasabah_INDV_KewargaNegaraan") = "2" Then
                Me.negaraPenerimaInd.Visible = True

                objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, (dr("Beneficiary_Nasabah_INDV_Negara")))
                If Not IsNothing(objSnegara) Then
                    txtSwiftInPenerimaNasabah_IND_negara.Text = Safe(objSnegara.NamaNegara)
                    hfSwiftInPenerimaNasabah_IND_negara.Value = Safe(objSnegara.IDNegara)
                    If hfSwiftInPenerimaNasabah_IND_negara.Value = ParameterNegara.MsSystemParameter_Value Then
                        Me.negaraLainPenerimaInd.Visible = True
                        Me.txtSwiftInPenerimaNasabah_IND_negaralain.Text = dr("Beneficiary_Nasabah_INDV_NegaraLainnya").ToString
                    End If
                End If
            End If
        End If

        objSPekerjaan = objPekerjaan.Find(MsPekerjaanColumn.IDPekerjaan, dr("Beneficiary_Nasabah_INDV_Pekerjaan"))
        If Not IsNothing(objSPekerjaan) Then
            txtSwiftInPenerimaNasabah_IND_pekerjaan.Text = Safe(objSPekerjaan.NamaPekerjaan)
            hfSwiftInIdenPenerimaNas_Ind_pekerjaan.Value = Safe(objSPekerjaan.IDPekerjaan)
            If hfSwiftInIdenPenerimaNas_Ind_pekerjaan.Value = ParameterPekerjaan.MsSystemParameter_Value Then
                Me.trSwiftInIdenPenerimaNas_Ind_Pekerjaanlain.Visible = True
                Me.TxtSwiftInIdenPenerimaNas_Ind_Pekerjaanlain.Text = dr("Beneficiary_Nasabah_INDV_PekerjaanLainnya").ToString
            End If
        End If

        'Alamat Domisili
        Me.TxtSwiftInIdenPenerimaNas_Ind_Alamat.Text = dr("Beneficiary_Nasabah_INDV_ID_Alamat_Dom").ToString
        If ObjectAntiNull(dr("Beneficiary_Nasabah_INDV_ID_KotaKab_Dom")) = True Then
            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, dr("Beneficiary_Nasabah_INDV_ID_KotaKab_Dom"))
            If Not IsNothing(objSkotaKab) Then
                Me.TxtSwiftInIdenPenerimaNas_Ind_Kota.Text = Safe(objSkotaKab.NamaKotaKab)
                hfSwiftInIdenPenerimaNas_Ind_Kota_dom.Value = Safe(objSkotaKab.IDKotaKab)
                If hfSwiftInIdenPenerimaNas_Ind_Kota_dom.Value = ParameterKota.MsSystemParameter_Value Then
                    Me.trSwiftInIdenPenerimaNas_Ind_kotalain.Visible = True
                    Me.TxtSwiftInIdenPenerimaNas_Ind_kotalain.Text = dr("Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom").ToString
                End If
            End If
        End If


        If ObjectAntiNull(dr("Beneficiary_Nasabah_INDV_ID_Propinsi_Dom")) = True Then
            objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(SafeNumIndex(dr("Beneficiary_Nasabah_INDV_ID_Propinsi_Dom"))) 'GetPaged("IdProvince=" & CInt(objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_ID_Propinsi_Dom), "", 0, Integer.MaxValue, 0)
            If Not IsNothing(objSProvinsi) Then
                Me.TxtSwiftInIdenPenerimaNas_Ind_provinsi.Text = Safe(objSProvinsi.Nama)
                hfSwiftInIdenPenerimaNas_Ind_provinsi_dom.Value = Safe(objSProvinsi.IdProvince)
                If hfSwiftInIdenPenerimaNas_Ind_provinsi_dom.Value = ParameterProvinsi.MsSystemParameter_Value Then
                    Me.trSwiftInIdenPenerimaNas_Ind_provinsiLain.Visible = True
                    Me.TxtSwiftInIdenPenerimaNas_Ind_provinsiLain.Text = dr("Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom").ToString
                End If
            End If
        End If



        'Alamat Identitas 
        Me.TxtSwiftInIdenPenerimaNas_Ind_alamatIdentitas.Text = dr("Beneficiary_Nasabah_INDV_ID_Alamat_Iden").ToString
        If ObjectAntiNull(dr("Beneficiary_Nasabah_INDV_ID_KotaKab_Iden")) = True Then
            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, dr("Beneficiary_Nasabah_INDV_ID_KotaKab_Iden"))
            If Not IsNothing(objSkotaKab) Then
                Me.TxtSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Text = Safe(objSkotaKab.NamaKotaKab)
                hfSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Value = Safe(objSkotaKab.IDKotaKab)
                If hfSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Value = ParameterKota.MsSystemParameter_Value Then
                    Me.trSwiftInIdenPenerimaNas_Ind_KotaLainIden.Visible = True
                    Me.TxtSwiftInIdenPenerimaNas_Ind_KotaLainIden.Text = dr("Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden").ToString
                End If
            End If
        End If

        If ObjectAntiNull(dr("Beneficiary_Nasabah_INDV_ID_Propinsi_Iden")) = True Then
            objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(SafeNumIndex(dr("Beneficiary_Nasabah_INDV_ID_Propinsi_Iden"))) 'GetPaged("IdProvince=" & CInt(objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_ID_Propinsi_Iden), "", 0, Integer.MaxValue, 0)
            If Not IsNothing(objSProvinsi) Then
                Me.TxtSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Text = Safe(objSProvinsi.Nama)
                hfSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Value = Safe(objSProvinsi.IdProvince)
                If hfSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Value = ParameterProvinsi.MsSystemParameter_Value Then
                    Me.trSwiftInIdenPenerimaNas_Ind_ProvinsilainIden.Visible = True
                    Me.TxtSwiftInIdenPenerimaNas_Ind_ProvinsilainIden.Text = dr("Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden").ToString
                End If
            End If
        End If
        Me.TxtISwiftIndenPenerimaNas_Ind_noTelp.Text = dr("Beneficiary_Nasabah_INDV_NoTelp").ToString
        Me.cbo_SwiftInpenerimaNas_Ind_jenisidentitas.SelectedValue = CInt(dr("Beneficiary_Nasabah_INDV_FK_IFTI_IDType"))
        Me.TxtISwiftIndenPenerimaNas_Ind_noIdentitas.Text = dr("Beneficiary_Nasabah_INDV_NomorID").ToString
        Me.txtSwiftInIdenPenerimaNas_IND_NilaiTransaksiKeuangan.Text = dr("Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan").ToString
        'End Using

    End Sub
    Sub FieldSwiftInReceiverCase2()
        'PengirimNasabahKorporasi

        Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetPaged(MsKotaKabColumn.Activation.ToString & "=1", MsKotaKabColumn.NamaKotaKab.ToString, 0, Integer.MaxValue, 0)
        Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
        Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetPaged(MsProvinceColumn.activation.ToString & "=1", MsProvinceColumn.Nama.ToString, 0, Integer.MaxValue, 0)
        Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi
        Dim objBidangUsaha As TList(Of MsBidangUsaha) = DataRepository.MsBidangUsahaProvider.GetAll
        Dim objSBidangUsaha As MsBidangUsaha 'Penampung hasil search bidang usaha
        Dim objBentukBadanUsaha As TList(Of MsBentukBidangUsaha) = DataRepository.MsBentukBidangUsahaProvider.GetAll
        Dim objSBentukBadanUsaha As MsBentukBidangUsaha

        'parameter value

        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
        Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
        Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)
        Dim ParameterBidangUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BidangUsaha)
        'Using objIftiBeneficiary As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("FK_IFTI_ID=" & getIFTIPK, "", 0, 1, 0)
        '    Me.txtSwiftInPenerimaNasabah_Rekening.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_NoRekening
        '    Using objBentukBadanUsaha As MsBentukBidangUsaha = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id)
        '        If Not IsNothing(objBentukBadanUsaha) Then
        '            Me.cbo_SwiftInPenerimaNasabah_Korp_BentukBadanUsaha.SelectedValue = objBentukBadanUsaha.BentukBidangUsaha
        '        End If
        '    End Using
        '    Me.txtSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLainnya.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya
        '    Me.txtSwiftInPenerimaNasabah_Korp_namaKorp.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_NamaKorporasi
        '    objSBidangUsaha = objBidangUsaha.Find(MsBidangUsahaColumn.IdBidangUsaha, objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id)
        '    If Not IsNothing(objSBidangUsaha) Then
        '        txtSwiftInPenerimaNasabah_Korp_BidangUsahaKorp.Text = Safe(objSBidangUsaha.NamaBidangUsaha)
        '        hfSwiftInPenerimaNasabah_Korp_BidangUsahaKorp.Value = Safe(objSBidangUsaha.IdBidangUsaha)
        '    End If
        '    txtSwiftInPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_BidangUsahaLainnya
        '    Me.txtSwiftInPenerimaNasabah_Korp_AlamatKorp.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_AlamatLengkap
        '    objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_ID_KotaKab)
        '    If Not IsNothing(objSkotaKab) Then
        '        Me.txtSwiftInPenerimaNasabah_Korp_Kotakab.Text = Safe(objSkotaKab.NamaKotaKab)
        '        hfSwiftInPenerimaNasabah_Korp_kotakab.Value = Safe(objSkotaKab.IDKotaKab)
        '    End If
        '    Me.txtSwiftInPenerimaNasabah_Korp_kotaLain.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_ID_KotaKabLainnya
        '    objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(SafeNumIndex(objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_ID_Propinsi))
        '    If Not IsNothing(objSProvinsi) Then
        '        Me.txtSwiftInPenerimaNasabah_Korp_propinsi.Text = Safe(objSProvinsi.Nama)
        '        hfSwiftInPenerimaNasabah_Korp_propinsi.Value = Safe(objSProvinsi.IdProvince)
        '    End If
        '    Me.txtSwiftInPenerimaNasabah_Korp_propinsilain.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_ID_PropinsiLainnya

        '    Me.txtSwiftInPenerimaNasabah_Korp_NoTelp.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_NoTelp
        '    Me.txtSwiftInPenerimaNasabah_Korp_NilaiTransaksiKeuangan.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan
        'End Using
        Dim dr As Data.DataRow = ReceiverDataTable.Rows(CInt(getIFTIBeneficiaryTempPK - 1))
        'dr("FK_IFTI_NasabahType_ID") = 2
        Me.txtSwiftInPenerimaNasabah_Rekening.Text = dr("Beneficiary_Nasabah_corp_NoRekening")
        objSBentukBadanUsaha = objBentukBadanUsaha.Find(MsBentukBidangUsahaColumn.BentukBidangUsaha, dr("Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id"))
        ' Using objBentukBadanUsaha As MsBentukBidangUsaha = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(dr("Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id"))
        If Not IsNothing(objSBentukBadanUsaha) Then
            Me.cbo_SwiftInPenerimaNasabah_Korp_BentukBadanUsaha.SelectedValue = objSBentukBadanUsaha.IdBentukBidangUsaha
        End If
        'End Using
        Me.txtSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLainnya.Text = dr("Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya").ToString
        Me.txtSwiftInPenerimaNasabah_Korp_namaKorp.Text = dr("Beneficiary_Nasabah_CORP_NamaKorporasi").ToString
        objSBidangUsaha = objBidangUsaha.Find(MsBidangUsahaColumn.IdBidangUsaha, dr("Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id"))
        If Not IsNothing(objSBidangUsaha) Then
            txtSwiftInPenerimaNasabah_Korp_BidangUsahaKorp.Text = Safe(objSBidangUsaha.NamaBidangUsaha)
            hfSwiftInPenerimaNasabah_Korp_BidangUsahaKorp.Value = Safe(objSBidangUsaha.IdBidangUsaha)
            If hfSwiftInPenerimaNasabah_Korp_BidangUsahaKorp.Value = ParameterBidangUsaha.MsSystemParameter_Value Then
                Me.trSwiftInPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Visible = True
                txtSwiftInPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Text = dr("Beneficiary_Nasabah_CORP_BidangUsahaLainnya").ToString
            End If
        End If

        Me.txtSwiftInPenerimaNasabah_Korp_AlamatKorp.Text = dr("Beneficiary_Nasabah_CORP_AlamatLengkap").ToString
        If ObjectAntiNull(dr("Beneficiary_Nasabah_CORP_ID_KotaKab")) = True Then
            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, dr("Beneficiary_Nasabah_CORP_ID_KotaKab"))
            If Not IsNothing(objSkotaKab) Then
                Me.txtSwiftInPenerimaNasabah_Korp_Kotakab.Text = Safe(objSkotaKab.NamaKotaKab)
                hfSwiftInPenerimaNasabah_Korp_kotakab.Value = Safe(objSkotaKab.IDKotaKab)
                If hfSwiftInPenerimaNasabah_Korp_kotakab.Value = ParameterKota.MsSystemParameter_Value Then
                    Me.trSwiftInPenerimaNasabah_Korp_kotaLain.Visible = True
                    Me.txtSwiftInPenerimaNasabah_Korp_kotaLain.Text = dr("Beneficiary_Nasabah_CORP_ID_KotaKabLainnya").ToString
                End If
            End If
        End If
        If ObjectAntiNull(dr("Beneficiary_Nasabah_CORP_ID_Propinsi")) = True Then
            objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(dr("Beneficiary_Nasabah_CORP_ID_Propinsi"))
            If Not IsNothing(objSProvinsi) Then
                Me.txtSwiftInPenerimaNasabah_Korp_propinsi.Text = Safe(objSProvinsi.Nama)
                hfSwiftInPenerimaNasabah_Korp_propinsi.Value = Safe(objSProvinsi.IdProvince)
                If hfSwiftInPenerimaNasabah_Korp_propinsi.Value = ParameterProvinsi.MsSystemParameter_Value Then
                    Me.trSwiftInPenerimaNasabah_Korp_propinsilain.Visible = True
                    Me.txtSwiftInPenerimaNasabah_Korp_propinsilain.Text = dr("Beneficiary_Nasabah_CORP_ID_PropinsiLainnya").ToString
                End If
            End If
        End If
        Me.txtSwiftInPenerimaNasabah_Korp_NoTelp.Text = dr("Beneficiary_Nasabah_CORP_NoTelp").ToString
        Me.txtSwiftInPenerimaNasabah_Korp_NilaiTransaksiKeuangan.Text = dr("Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan").ToString
    End Sub
    Sub FieldSwiftInReceiverCase3()
        'PengirimNonNasabah
        'Using objIftiBeneficiary As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("FK_IFTI_ID=" & getIFTIPK, "", 0, 1, 0)
        '    Me.TxtSwiftInPenerimaNonNasabah_nama.Text = objIftiBeneficiary(0).Beneficiary_NonNasabah_NamaLengkap
        '    Me.TxtSwiftInPenerimaNonNasabah_TanggalLahir.Text = FormatDate(objIftiBeneficiary(0).Beneficiary_NonNasabah_TanggalLahir)
        '    Me.TxtSwiftInPenerimaNonNasabah_alamatiden.Text = objIftiBeneficiary(0).Beneficiary_NonNasabah_ID_Alamat
        '    Me.TxtSwiftInPenerimaNonNasabah_NoTelepon.Text = objIftiBeneficiary(0).Beneficiary_NonNasabah_NoTelp
        '    Me.CboSwiftInPenerimaNonNasabah_JenisDokumen.TabIndex = objIftiBeneficiary(0).Beneficiary_NonNasabah_FK_IFTI_IDType
        '    Me.TxtSwiftInPenerimaNonNasabah_NomorIden.Text = objIftiBeneficiary(0).Beneficiary_NonNasabah_NomorID
        '    Me.TxtSwiftInPenerimaNonNasabah_NilaiTransaksiKeuangan.Text = objIftiBeneficiary(0).Beneficiary_NonNasabah_NilaiTransaksikeuangan
        'End Using
        Dim dr As Data.DataRow = ReceiverDataTable.Rows(CInt(getIFTIBeneficiaryTempPK - 1))
        Me.TxtSwiftInPenerimaNonNasabah_nama.Text = dr("Beneficiary_NonNasabah_NamaLengkap").ToString
        Me.TxtSwiftInPenerimaNonNasabah_TanggalLahir.Text = FormatDate(dr("Beneficiary_NonNasabah_TanggalLahir"))
        Me.TxtSwiftInPenerimaNonNasabah_alamatiden.Text = dr("Beneficiary_NonNasabah_ID_Alamat").ToString
        Me.TxtSwiftInPenerimaNonNasabah_NoTelepon.Text = dr("Beneficiary_NonNasabah_NoTelp").ToString
        Me.CboSwiftInPenerimaNonNasabah_JenisDokumen.SelectedValue = dr("Beneficiary_NonNasabah_FK_IFTI_IDType").ToString
        Me.TxtSwiftInPenerimaNonNasabah_NomorIden.Text = dr("Beneficiary_NonNasabah_NomorID").ToString
        Me.TxtSwiftInPenerimaNonNasabah_NilaiTransaksiKeuangan.Text = dr("Beneficiary_NonNasabah_NilaiTransaksikeuangan").ToString

    End Sub
    Sub FieldSwiftInReceiverCase4()
        'PenyelenggaraPenerusIndividu

        'Ambil data buat cari PK Negara, Provinsi, KotaKab, dkk (ini variable yang bakal dpke berkali2)
        Dim i As Integer = 0 'buat iterasi
        Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetPaged(MsKotaKabColumn.Activation.ToString & "=1", MsKotaKabColumn.NamaKotaKab.ToString, 0, Integer.MaxValue, 0)
        Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
        Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetPaged(MsProvinceColumn.activation.ToString & "=1", MsProvinceColumn.Nama.ToString, 0, Integer.MaxValue, 0)
        Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi
        Dim objMapNegara As TList(Of MappingMsNegaraNCBSPPATK) = DataRepository.MappingMsNegaraNCBSPPATKProvider.GetAll
        Dim objSMapNegara As MappingMsNegaraNCBSPPATK 'Penampung hasil search mappingnegara yang pke picker
        Dim objNegara As TList(Of MsNegara) = DataRepository.MsNegaraProvider.GetPaged(MsNegaraColumn.Activation.ToString & "=1", MsNegaraColumn.NamaNegara.ToString, 0, Integer.MaxValue, 0)
        Dim objSnegara As MsNegara 'Penampung hasil search negara yang pke picker
        Dim objPekerjaan As TList(Of MsPekerjaan) = DataRepository.MsPekerjaanProvider.GetPaged(MsPekerjaanColumn.Activation.ToString & "=1", MsPekerjaanColumn.NamaPekerjaan.ToString, 0, Integer.MaxValue, 0)
        Dim objSPekerjaan As MsPekerjaan 'Penampung hasil search pekerjaan
        'parameter value

        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
        Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
        Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)

        'Using objIftiBeneficiary As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("FK_IFTI_ID=" & getIFTIPK, "", 0, 1, 0)
        '    Me.TxtSwiftInPenerimaPenerus_Rekening.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_NoRekening
        '    'Me.TxtSwiftInPenerimaPenerus_Ind_namaBank.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_NamaBank
        '    Me.TxtSwiftInPenerimaPenerus_Ind_nama.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_NamaLengkap
        '    If Not IsNothing(objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_TanggalLahir) Then
        '        Me.TxtSwiftInPenerimaPenerus_Ind_TanggalLahir.Text = FormatDate(objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_TanggalLahir)
        '    End If
        '    If Not IsNothing(objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_KewargaNegaraan) Then
        '        Me.RBSwiftInPenerimaPenerus_Ind_Kewarganegaraan.SelectedValue = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_KewargaNegaraan
        '        If objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_KewargaNegaraan = "2" Then
        '            objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_Negara)
        '            If Not IsNothing(objSnegara) Then
        '                TxtSwiftInPenerimaPenerus_IND_negara.Text = Safe(objSnegara.NamaNegara)
        '                hfSwiftInPenerimaPenerus_IND_negara.Value = Safe(objSnegara.IDNegara)
        '            End If
        '            Me.TxtSwiftInPenerimaPenerus_IND_negaralain.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_NegaraLainnya
        '        End If
        '    End If
        '    objSPekerjaan = objPekerjaan.Find(MsPekerjaanColumn.IDPekerjaan, objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_Pekerjaan)
        '    If Not IsNothing(objSPekerjaan) Then
        '        TxtSwiftInPenerimaPenerus_IND_pekerjaan.Text = Safe(objSPekerjaan.NamaPekerjaan)
        '        hfSwiftInPenerimaPenerus_IND_pekerjaan.Value = Safe(objSPekerjaan.IDPekerjaan)
        '    End If
        '    Me.TxtSwiftInPenerimaPenerus_IND_pekerjaanLain.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_PekerjaanLainnya

        '    Me.TxtSwiftInPenerimaPenerus_IND_alamatDom.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_ID_Alamat_Dom

        '    objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_ID_KotaKab_Dom)
        '    If Not IsNothing(objSkotaKab) Then
        '        TxtSwiftInPenerimaPenerus_IND_kotaDom.Text = Safe(objSkotaKab.NamaKotaKab)
        '        hfSwiftInPenerimaPenerus_IND_kotaDom.Value = Safe(objSkotaKab.IDKotaKab)
        '    End If
        '    Me.TxtSwiftInPenerimaPenerus_IND_kotaLainDom.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom

        '    objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(SafeNumIndex(objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_ID_Propinsi_Dom))
        '    If Not IsNothing(objSProvinsi) Then
        '        TxtSwiftInPenerimaPenerus_IND_ProvDom.Text = Safe(objSProvinsi.Nama)
        '        hfSwiftInPenerimaPenerus_IND_ProvDom.Value = Safe(objSProvinsi.IdProvince)
        '    End If
        '    Me.TxtSwiftInPenerimaPenerus_IND_ProvLainDom.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom

        '    Me.TxtSwiftInPenerimaPenerus_IND_kotaIden.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_ID_Alamat_Iden
        '    objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_ID_KotaKab_Iden)
        '    If Not IsNothing(objSkotaKab) Then
        '        Me.TxtSwiftInPenerimaPenerus_IND_kotaIden.Text = Safe(objSkotaKab.NamaKotaKab)
        '        hfSwiftInPenerimaPenerus_IND_kotaIden.Value = Safe(objSkotaKab.IDKotaKab)
        '    End If
        '    Me.TxtSwiftInPenerimaPenerus_IND_KotaLainIden.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden

        '    objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(SafeNumIndex(objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_ID_Propinsi_Iden))
        '    If Not IsNothing(objSProvinsi) Then
        '        Me.TxtSwiftInPenerimaPenerus_IND_ProvIden.Text = Safe(objSProvinsi.Nama)
        '        hfSwiftInPenerimaPenerus_IND_ProvIden.Value = Safe(objSProvinsi.IdProvince)
        '    End If
        '    Me.TxtSwiftInPenerimaPenerus_IND_ProvLainIden.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden
        '    Me.TxtSwiftInPenerimaPenerus_IND_NoTelp.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_NoTelp
        '    Me.CBoSwiftInPenerimaPenerus_IND_jenisIdentitas.SelectedValue = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_FK_IFTI_IDType
        '    Me.TxtSwiftInPenerimaPenerus_IND_nomoridentitas.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_NomorID
        '    Me.TxtSwiftInPenerimaPenerus_IND_NIlaiTransaksi.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan
        'End Using
        Dim dr As Data.DataRow = ReceiverDataTable.Rows(CInt(getIFTIBeneficiaryTempPK - 1))
        'dr("FK_IFTI_NasabahType_ID") = 1
        Me.TxtSwiftInPenerimaPenerus_Rekening.Text = dr("Beneficiary_Nasabah_INDV_NoRekening").ToString
        Me.TxtSwiftInPenerimaPenerus_Ind_nama.Text = dr("Beneficiary_Nasabah_INDV_NamaLengkap").ToString
        Me.TxtSwiftInPenerimaPenerus_Ind_namaBank.Text = dr("Beneficiary_Nasabah_INDV_NamaBank").ToString
        Me.TxtSwiftInPenerimaPenerus_Ind_TanggalLahir.Text = FormatDate(dr("Beneficiary_Nasabah_INDV_TanggalLahir"))

        If Not IsNothing(dr("Beneficiary_Nasabah_INDV_KewargaNegaraan")) Then
            Me.RBSwiftInPenerimaPenerus_Ind_Kewarganegaraan.SelectedValue = dr("Beneficiary_Nasabah_INDV_KewargaNegaraan")
            If dr("Beneficiary_Nasabah_INDV_KewargaNegaraan") = "2" Then
                Me.negaraPenerimaPenerusInd.Visible = True

                objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, CInt(dr("Beneficiary_Nasabah_INDV_Negara")))
                If Not IsNothing(objSnegara) Then
                    TxtSwiftInPenerimaPenerus_IND_negara.Text = Safe(objSnegara.NamaNegara)
                    hfSwiftInPenerimaPenerus_IND_negara.Value = Safe(objSnegara.IDNegara)
                    If hfSwiftInPenerimaPenerus_IND_negara.Value = ParameterNegara.MsSystemParameter_Value Then
                        Me.negaraLainPenerimaPenerusInd.Visible = True
                        Me.TxtSwiftInPenerimaPenerus_IND_negaralain.Text = dr("Beneficiary_Nasabah_INDV_NegaraLainnya").ToString
                    End If
                End If

            End If
        End If

        If ObjectAntiNull(dr("Beneficiary_Nasabah_INDV_Pekerjaan")) = True Then
            objSPekerjaan = objPekerjaan.Find(MsPekerjaanColumn.IDPekerjaan, dr("Beneficiary_Nasabah_INDV_Pekerjaan"))
            If Not IsNothing(objSPekerjaan) Then
                TxtSwiftInPenerimaPenerus_IND_pekerjaan.Text = Safe(objSPekerjaan.NamaPekerjaan)
                hfSwiftInPenerimaPenerus_IND_pekerjaan.Value = Safe(objSPekerjaan.IDPekerjaan)
                If hfSwiftInPenerimaPenerus_IND_pekerjaan.Value = ParameterPekerjaan.MsSystemParameter_Value Then
                    Me.trSwiftInPenerimaPenerus_IND_pekerjaanLain.Visible = True
                    Me.TxtSwiftInPenerimaPenerus_IND_pekerjaanLain.Text = dr("Beneficiary_Nasabah_INDV_PekerjaanLainnya").ToString
                End If
            End If
        End If


        'Alamat Domisili
        Me.TxtSwiftInPenerimaPenerus_IND_alamatDom.Text = dr("Beneficiary_Nasabah_INDV_ID_Alamat_Dom").ToString
        If ObjectAntiNull(dr("Beneficiary_Nasabah_INDV_ID_KotaKab_Dom")) = True Then
            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, dr("Beneficiary_Nasabah_INDV_ID_KotaKab_Dom"))
            If Not IsNothing(objSkotaKab) Then
                Me.TxtSwiftInPenerimaPenerus_IND_kotaDom.Text = Safe(objSkotaKab.NamaKotaKab)
                Me.hfSwiftInPenerimaPenerus_IND_kotaDom.Value = Safe(objSkotaKab.IDKotaKab)
                If hfSwiftInPenerimaPenerus_IND_kotaDom.Value = ParameterKota.MsSystemParameter_Value Then
                    Me.trSwiftInPenerimaPenerus_IND_kotaLainDom.Visible = True
                    Me.TxtSwiftInPenerimaPenerus_IND_kotaLainDom.Text = dr("Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom").ToString
                End If
            End If
        End If


        If ObjectAntiNull(dr("Beneficiary_Nasabah_INDV_ID_Propinsi_Dom")) = True Then
            objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(SafeNumIndex(dr("Beneficiary_Nasabah_INDV_ID_Propinsi_Dom"))) 'GetPaged("IdProvince=" & CInt(objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_ID_Propinsi_Dom), "", 0, Integer.MaxValue, 0)
            If Not IsNothing(objSProvinsi) Then
                Me.TxtSwiftInPenerimaPenerus_IND_ProvDom.Text = Safe(objSProvinsi.Nama)
                Me.hfSwiftInPenerimaPenerus_IND_ProvDom.Value = Safe(objSProvinsi.IdProvince)
                If hfSwiftInPenerimaPenerus_IND_ProvDom.Value = ParameterProvinsi.MsSystemParameter_Value Then
                    Me.trSwiftInPenerimaPenerus_IND_ProvLainDom.Visible = True
                    Me.TxtSwiftInPenerimaPenerus_IND_ProvLainDom.Text = dr("Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom").ToString
                End If
            End If
        End If



        'Alamat Identitas 
        Me.TxtSwiftInPenerimaPenerus_IND_alamatIden.Text = dr("Beneficiary_Nasabah_INDV_ID_Alamat_Iden").ToString

        If ObjectAntiNull(dr("Beneficiary_Nasabah_INDV_ID_KotaKab_Iden")) = True Then
            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, dr("Beneficiary_Nasabah_INDV_ID_KotaKab_Iden"))
            If Not IsNothing(objSkotaKab) Then
                Me.TxtSwiftInPenerimaPenerus_IND_kotaIden.Text = Safe(objSkotaKab.NamaKotaKab)
                hfSwiftInPenerimaPenerus_IND_kotaIden.Value = Safe(objSkotaKab.IDKotaKab)
                If hfSwiftInPenerimaPenerus_IND_kotaIden.Value = ParameterKota.MsSystemParameter_Value Then
                    Me.trSwiftInPenerimaPenerus_IND_KotaLainIden.Visible = True
                    Me.TxtSwiftInPenerimaPenerus_IND_KotaLainIden.Text = dr("Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden").ToString
                End If
            End If
        End If


        If ObjectAntiNull(dr("Beneficiary_Nasabah_INDV_ID_Propinsi_Iden")) = True Then
            objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(SafeNumIndex(dr("Beneficiary_Nasabah_INDV_ID_Propinsi_Iden"))) 'GetPaged("IdProvince=" & CInt(objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_ID_Propinsi_Iden), "", 0, Integer.MaxValue, 0)
            If Not IsNothing(objSProvinsi) Then
                Me.TxtSwiftInPenerimaPenerus_IND_ProvIden.Text = Safe(objSProvinsi.Nama)
                hfSwiftInPenerimaPenerus_IND_ProvIden.Value = Safe(objSProvinsi.IdProvince)
                If hfSwiftInPenerimaPenerus_IND_ProvIden.Value = ParameterProvinsi.MsSystemParameter_Value Then
                    Me.trSwiftInPenerimaPenerus_IND_ProvLainIden.Visible = True
                    Me.TxtSwiftInPenerimaPenerus_IND_ProvLainIden.Text = dr("Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden").ToString
                End If
            End If
        End If


        Me.TxtSwiftInPenerimaPenerus_IND_NoTelp.Text = dr("Beneficiary_Nasabah_INDV_NoTelp").ToString
        Me.CBoSwiftInPenerimaPenerus_IND_jenisIdentitas.SelectedValue = CInt(dr("Beneficiary_Nasabah_INDV_FK_IFTI_IDType"))
        Me.TxtSwiftInPenerimaPenerus_IND_nomoridentitas.Text = dr("Beneficiary_Nasabah_INDV_NomorID").ToString
        Me.TxtSwiftInPenerimaPenerus_IND_NIlaiTransaksi.Text = dr("Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan").ToString
        'End Using
    End Sub
    Sub FieldSwiftInReceiverCase5()
        'PenyelenggaraPenerusKorporasi

        Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetPaged(MsKotaKabColumn.Activation.ToString & "=1", MsKotaKabColumn.NamaKotaKab.ToString, 0, Integer.MaxValue, 0)
        Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
        Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetPaged(MsProvinceColumn.activation.ToString & "=1", MsProvinceColumn.Nama.ToString, 0, Integer.MaxValue, 0)
        Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi
        Dim objBidangUsaha As TList(Of MsBidangUsaha) = DataRepository.MsBidangUsahaProvider.GetAll
        Dim objSBidangUsaha As MsBidangUsaha 'Penampung hasil search bidang usaha
        Dim objBentukBadanUsaha As TList(Of MsBentukBidangUsaha) = DataRepository.MsBentukBidangUsahaProvider.GetAll
        Dim objSBentukBadanUsaha As MsBentukBidangUsaha

        'parameter value

        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
        Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
        Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)
        Dim ParameterBidangUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BidangUsaha)

        'Using objIftiBeneficiary As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("FK_IFTI_ID=" & getIFTIPK, "", 0, 1, 0)
        '    Me.TxtSwiftInPenerimaPenerus_Rekening.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_NoRekening
        '    Using objBentukBadanUsaha As MsBentukBidangUsaha = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id)
        '        Me.cbo_SwiftInPenerimaPenerus_Korp_BentukBadanUsaha.SelectedValue = objBentukBadanUsaha.BentukBidangUsaha
        '    End Using
        '    Me.txtSwiftInPenerimaPenerus_Korp_BentukBadanUsahaLain.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya
        '    Me.txtSwiftInPenerimaPenerus_Korp_NamaKorp.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_NamaKorporasi
        '    objSBidangUsaha = objBidangUsaha.Find(MsBidangUsahaColumn.IdBidangUsaha, objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id)
        '    If Not IsNothing(objSBidangUsaha) Then
        '        txtSwiftInPenerimaPenerus_Korp_BidangUsahaKorp.Text = Safe(objSBidangUsaha.NamaBidangUsaha)
        '        hfSwiftInPenerimaPenerus_Korp_BidangUsahaKorp.Value = Safe(objSBidangUsaha.IdBidangUsaha)
        '    End If
        '    txtSwiftInPenerimaPenerus_Korp_BidangUsahaKorpLainnya.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_BidangUsahaLainnya
        '    Me.txtSwiftInPenerimaPenerus_Korp_alamatkorp.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_AlamatLengkap
        '    objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_ID_KotaKab)
        '    If Not IsNothing(objSkotaKab) Then
        '        Me.txtSwiftInPenerimaPenerus_Korp_kotakorp.Text = Safe(objSkotaKab.NamaKotaKab)
        '        hfSwiftInPenerimaPenerus_Korp_kotakorp.Value = Safe(objSkotaKab.IDKotaKab)
        '    End If

        '    Me.txtSwiftInPenerimaPenerus_Korp_kotalainnyakorp.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_ID_KotaKabLainnya
        '    objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(SafeNumIndex(objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_ID_Propinsi))
        '    If Not IsNothing(objSProvinsi) Then
        '        Me.txtSwiftInPenerimaPenerus_Korp_ProvinsiKorp.Text = Safe(objSProvinsi.Nama)
        '        hfSwiftInPenerimaPenerus_Korp_ProvinsiKorp.Value = Safe(objSProvinsi.IdProvince)
        '    End If

        '    Me.txtSwiftInPenerimaPenerus_Korp_provinsiLainnyaKorp.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_ID_PropinsiLainnya
        '    Me.txtSwiftInPenerimaPenerus_Korp_NoTelp.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_NoTelp
        '    Me.txtSwiftInPenerimaPenerus_Korp_NilaiTransaksi.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan
        'End Using
        Dim dr As Data.DataRow = ReceiverDataTable.Rows(CInt(getIFTIBeneficiaryTempPK - 1))
        'dr("FK_IFTI_NasabahType_ID") = 2
        Me.TxtSwiftInPenerimaPenerus_Rekening.Text = dr("Beneficiary_Nasabah_corp_NoRekening").ToString
        objSBentukBadanUsaha = objBentukBadanUsaha.Find(MsBentukBidangUsahaColumn.BentukBidangUsaha, dr("Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id"))

        If Not IsNothing(objSBentukBadanUsaha) Then
            Me.cbo_SwiftInPenerimaPenerus_Korp_BentukBadanUsaha.SelectedValue = objSBentukBadanUsaha.IdBentukBidangUsaha
        End If
        Me.txtSwiftInPenerimaPenerus_Korp_BentukBadanUsahaLain.Text = dr("Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya").ToString
        Me.txtSwiftInPenerimaPenerus_Korp_NamaKorp.Text = dr("Beneficiary_Nasabah_CORP_NamaKorporasi").ToString
        Me.TxtSwiftInPenerimaPenerus_Ind_namaBank.Text = dr("Beneficiary_Nasabah_CORP_NamaBank").ToString
        objSBidangUsaha = objBidangUsaha.Find(MsBidangUsahaColumn.IdBidangUsaha, dr("Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id"))
        If Not IsNothing(objSBidangUsaha) Then
            txtSwiftInPenerimaPenerus_Korp_BidangUsahaKorp.Text = Safe(objSBidangUsaha.NamaBidangUsaha)
            hfSwiftInPenerimaNasabah_Korp_BidangUsahaKorp.Value = Safe(objSBidangUsaha.IdBidangUsaha)
            If hfSwiftInPenerimaNasabah_Korp_BidangUsahaKorp.Value = ParameterBidangUsaha.MsSystemParameter_Value Then
                Me.trSwiftInPenerimaPenerus_Korp_BidangUsahaKorpLainnya.Visible = True
                txtSwiftInPenerimaPenerus_Korp_BidangUsahaKorpLainnya.Text = dr("Beneficiary_Nasabah_CORP_BidangUsahaLainnya").ToString
            End If

        End If

        Me.txtSwiftInPenerimaPenerus_Korp_alamatkorp.Text = dr("Beneficiary_Nasabah_CORP_AlamatLengkap").ToString
        If ObjectAntiNull(dr("Beneficiary_Nasabah_CORP_ID_KotaKab")) = True Then
            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, dr("Beneficiary_Nasabah_CORP_ID_KotaKab"))
            If Not IsNothing(objSkotaKab) Then
                Me.txtSwiftInPenerimaPenerus_Korp_kotakorp.Text = Safe(objSkotaKab.NamaKotaKab)
                hfSwiftInPenerimaPenerus_Korp_kotakorp.Value = Safe(objSkotaKab.IDKotaKab)
                If hfSwiftInPenerimaPenerus_Korp_kotakorp.Value = ParameterKota.MsSystemParameter_Value Then
                    Me.trSwiftInPenerimaPenerus_Korp_kotalainnyakorp.Visible = True
                    Me.txtSwiftInPenerimaPenerus_Korp_kotalainnyakorp.Text = dr("Beneficiary_Nasabah_CORP_ID_KotaKabLainnya").ToString
                End If
            End If
        End If


        If ObjectAntiNull(dr("Beneficiary_Nasabah_CORP_ID_Propinsi")) = True Then
            objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(dr("Beneficiary_Nasabah_CORP_ID_Propinsi"))
            If Not IsNothing(objSProvinsi) Then
                Me.txtSwiftInPenerimaPenerus_Korp_ProvinsiKorp.Text = Safe(objSProvinsi.Nama)
                hfSwiftInPenerimaPenerus_Korp_ProvinsiKorp.Value = Safe(objSProvinsi.IdProvince)
                If hfSwiftInPenerimaPenerus_Korp_ProvinsiKorp.Value = ParameterProvinsi.MsSystemParameter_Value Then
                    Me.trSwiftInPenerimaPenerus_Korp_provinsiLainnyaKorp.Visible = True
                    Me.txtSwiftInPenerimaPenerus_Korp_provinsiLainnyaKorp.Text = dr("Beneficiary_Nasabah_CORP_ID_PropinsiLainnya").ToString
                End If
            End If
        End If
        Me.txtSwiftInPenerimaPenerus_Korp_NoTelp.Text = dr("Beneficiary_Nasabah_CORP_NoTelp").ToString
        Me.txtSwiftInPenerimaPenerus_Korp_NilaiTransaksi.Text = dr("Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan").ToString
    End Sub
#End Region

#Region "Event"
#Region "Event Browse"

    Protected Sub imgButton_SwiftInPengirimNas_IND_negaraWarga_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_SwiftInPengirimNas_IND_negaraWarga.Click
        Try
            If Session("PickerNegara.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
                txtSwiftInPengirimNasabah_IND_negara.Text = strData(1)
                Me.hfSwiftInPengirimNasabah_IND_negara.Value = strData(0)
                Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
                If Me.hfSwiftInPengirimNasabah_IND_negara.Value = ParameterNegara.MsSystemParameter_Value Then
                    Me.negaraLainPengirimInd.Visible = True
                Else
                    negaraLainPengirimInd.Visible = False
                End If
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButton_SwiftInPengirimNas_IND_negaraIden_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_SwiftInPengirimNas_IND_negaraIden.Click
        Try
            If Session("PickerNegara.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
                txtSwiftInPengirimNasabah_IND_negaraIden.Text = strData(1)
                Me.hfSwiftInPengirimNasabah_IND_negaraIden.Value = strData(0)
                Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
                If hfSwiftInPengirimNasabah_IND_negaraIden.Value = ParameterNegara.MsSystemParameter_Value Then
                    Me.trSwiftInPengirimNasabah_IND_negaraLainIden.Visible = True
                Else
                    trSwiftInPengirimNasabah_IND_negaraLainIden.Visible = False
                End If
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButton_SwiftInPengirimNasabah_Korp_NegaraKorp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_SwiftInPengirimNasabah_Korp_NegaraKorp.Click
        Try
            If Session("PickerNegara.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
                txtSwiftInPengirimNasabah_Korp_NegaraKorp.Text = strData(1)
                Me.hfSwiftInPengirimNasabah_Korp_NegaraKorp.Value = strData(0)
                Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
                If hfSwiftInPengirimNasabah_Korp_NegaraKorp.Value = ParameterNegara.MsSystemParameter_Value Then
                    Me.trSwiftInPengirimNasabah_Korp_NegaraLainnyaKorp.Visible = True
                Else
                    Me.trSwiftInPengirimNasabah_Korp_NegaraLainnyaKorp.Visible = False
                End If
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImageButton_SwiftInPengirimNonNasabah_negara_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton_SwiftInPengirimNonNasabah_negara.Click

        Try
            If Session("PickerNegara.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
                txtSwiftInPengirimNonNasabah_Negara.Text = strData(1)
                Me.hfSwiftInPengirimNonNasabah_Negara.Value = strData(0)
                Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
                If hfSwiftInPengirimNonNasabah_Negara.Value = ParameterNegara.MsSystemParameter_Value Then
                    Me.trSwiftInPengirimNonNasabah_negaraLain.Visible = True
                Else
                    Me.trSwiftInPengirimNonNasabah_negaraLain.Visible = False
                End If
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImageButton_SwiftInPenerimaNasabah_IND_negara_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton_SwiftInPenerimaNasabah_IND_negara.Click
        Try
            If Session("PickerNegara.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
                txtSwiftInPenerimaNasabah_IND_negara.Text = strData(1)
                Me.hfSwiftInPenerimaNasabah_IND_negara.Value = strData(0)
                Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
                If hfSwiftInPenerimaNasabah_IND_negara.Value = ParameterNegara.MsSystemParameter_Value Then
                    Me.negaraLainPenerimaInd.Visible = True
                Else
                    Me.negaraLainPenerimaInd.Visible = False
                End If
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButton_SwiftInPenerimaNas_Pekerjaan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_SwiftInPenerimaNas_Pekerjaan.Click
        Try
            If Session("PickerPekerjaan.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerPekerjaan.Data")).Split(";")
                txtSwiftInPenerimaNasabah_IND_pekerjaan.Text = strData(1)
                hfSwiftInIdenPenerimaNas_Ind_pekerjaan.Value = strData(0)
                Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)
                If hfSwiftInIdenPenerimaNas_Ind_pekerjaan.Value = ParameterPekerjaan.MsSystemParameter_Value Then
                    Me.trSwiftInIdenPenerimaNas_Ind_Pekerjaanlain.Visible = True
                Else
                    Me.trSwiftInIdenPenerimaNas_Ind_Pekerjaanlain.Visible = False
                End If
            End If
        Catch ex As Exception
            ' LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButton_SwiftInPenerimaNas_KotaDom_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_SwiftInPenerimaNas_KotaDom.Click
        Try
            If Session("PickerKotaKab.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
                TxtSwiftInIdenPenerimaNas_Ind_Kota.Text = strData(1)
                hfSwiftInIdenPenerimaNas_Ind_Kota_dom.Value = strData(0)
                hfSwiftInIdenPenerimaNas_Ind_provinsi_dom.Value = IFTIBLL.GetProvinceIDByKotaKab(strData(0))
                TxtSwiftInIdenPenerimaNas_Ind_provinsi.Text = IFTIBLL.GetProvincetextByKotaKab(strData(0))
                Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
                If hfSwiftInIdenPenerimaNas_Ind_Kota_dom.Value = ParameterKota.MsSystemParameter_Value Then
                    Me.trSwiftInIdenPenerimaNas_Ind_kotalain.Visible = True
                Else
                    Me.trSwiftInIdenPenerimaNas_Ind_kotalain.Visible = False
                End If
                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.KabupatenKota)
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButton_SwiftInPenerimaNas_ProvinsiDom_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_SwiftInPenerimaNas_ProvinsiDom.Click
        Try
            If Session("PickerProvinsi.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                TxtSwiftInIdenPenerimaNas_Ind_provinsi.Text = strData(1)
                hfSwiftInIdenPenerimaNas_Ind_provinsi_dom.Value = strData(0)
                Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
                If hfSwiftInIdenPenerimaNas_Ind_provinsi_dom.Value = ParameterProvinsi.MsSystemParameter_Value Then
                    Me.trSwiftInIdenPenerimaNas_Ind_provinsiLain.Visible = True
                Else
                    Me.trSwiftInIdenPenerimaNas_Ind_provinsiLain.Visible = False
                End If
                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.Propinsi)
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButton_SwiftInPenerimaNas_KotaIden_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_SwiftInPenerimaNas_KotaIden.Click
        Try
            If Session("PickerKotaKab.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
                TxtSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Text = strData(1)
                hfSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Value = strData(0)

                hfSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Value = IFTIBLL.GetProvinceIDByKotaKab(strData(0))
                TxtSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Text = IFTIBLL.GetProvincetextByKotaKab(strData(0))

                Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
                If hfSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Value = ParameterKota.MsSystemParameter_Value Then
                    Me.trSwiftInIdenPenerimaNas_Ind_KotaLainIden.Visible = True
                Else
                    Me.trSwiftInIdenPenerimaNas_Ind_KotaLainIden.Visible = False
                End If
                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.KabupatenKota)
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButton_SwiftInPenerimaNas_ProvinsiIden_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_SwiftInPenerimaNas_ProvinsiIden.Click
        Try
            If Session("PickerProvinsi.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                TxtSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Text = strData(1)
                hfSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Value = strData(0)
                Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
                If hfSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Value = ParameterProvinsi.MsSystemParameter_Value Then
                    Me.trSwiftInIdenPenerimaNas_Ind_ProvinsilainIden.Visible = True
                Else
                    Me.trSwiftInIdenPenerimaNas_Ind_ProvinsilainIden.Visible = False
                End If
                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.Propinsi)
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButton_SwiftInPenerimaNas_Korp_bidangUsaha_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_SwiftInPenerimaNas_Korp_bidangUsaha.Click
        Try
            If Session("PickerBidangUsaha.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerBidangUsaha.Data")).Split(";")
                txtSwiftInPenerimaNasabah_Korp_BidangUsahaKorp.Text = strData(1)
                hfSwiftInPenerimaNasabah_Korp_BidangUsahaKorp.Value = strData(0)
                Dim ParameterBidangUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BidangUsaha)
                If hfSwiftInPenerimaNasabah_Korp_BidangUsahaKorp.Value = ParameterBidangUsaha.MsSystemParameter_Value Then
                    Me.trSwiftInPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Visible = True
                Else
                    Me.trSwiftInPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Visible = False

                End If
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imagebutton_SwiftInPenerimaNasabah_Korp_kotakab_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imagebutton_SwiftInPenerimaNasabah_Korp_kotakab.Click
        Try
            If Session("PickerKotaKab.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
                txtSwiftInPenerimaNasabah_Korp_Kotakab.Text = strData(1)
                hfSwiftInPenerimaNasabah_Korp_kotakab.Value = strData(0)

                hfSwiftInPenerimaNasabah_Korp_propinsi.Value = IFTIBLL.GetProvinceIDByKotaKab(strData(0))
                txtSwiftInPenerimaNasabah_Korp_propinsi.Text = IFTIBLL.GetProvincetextByKotaKab(strData(0))


                Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
                If hfSwiftInPenerimaNasabah_Korp_kotakab.Value = ParameterKota.MsSystemParameter_Value Then
                    Me.trSwiftInPenerimaNasabah_Korp_kotaLain.Visible = True
                Else
                    Me.trSwiftInPenerimaNasabah_Korp_kotaLain.Visible = False
                End If
                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.KabupatenKota)
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImageButton_SwiftInPenerimaNasabah_Korp_propinsi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton_SwiftInPenerimaNasabah_Korp_propinsi.Click
        Try
            If Session("PickerProvinsi.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                txtSwiftInPenerimaNasabah_Korp_propinsi.Text = strData(1)
                hfSwiftInPenerimaNasabah_Korp_propinsi.Value = strData(0)
                Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
                If hfSwiftInPenerimaNasabah_Korp_propinsi.Value = ParameterProvinsi.MsSystemParameter_Value Then
                    Me.trSwiftInPenerimaNasabah_Korp_propinsilain.Visible = True
                Else
                    Me.trSwiftInPenerimaNasabah_Korp_propinsilain.Visible = False
                End If
                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.Propinsi)
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButton_SwiftInPenerimaPenerus_IND_Negara_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_SwiftInPenerimaPenerus_IND_Negara.Click
        Try
            If Session("PickerNegara.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
                TxtSwiftInPenerimaPenerus_IND_negara.Text = strData(1)
                Me.hfSwiftInPenerimaPenerus_IND_negara.Value = strData(0)
                Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
                If hfSwiftInPenerimaPenerus_IND_negara.Value = ParameterNegara.MsSystemParameter_Value Then
                    Me.negaraLainPenerimaPenerusInd.Visible = True
                Else
                    Me.negaraLainPenerimaPenerusInd.Visible = False
                End If
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButton_SwiftInPenerimaPenerus_IND_Pekerjaan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_SwiftInPenerimaPenerus_IND_Pekerjaan.Click
        Try
            If Session("PickerPekerjaan.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerPekerjaan.Data")).Split(";")
                TxtSwiftInPenerimaPenerus_IND_pekerjaan.Text = strData(1)
                hfSwiftInPenerimaPenerus_IND_pekerjaan.Value = strData(0)
                Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)
                If hfSwiftInPenerimaPenerus_IND_pekerjaan.Value = ParameterPekerjaan.MsSystemParameter_Value Then
                    Me.trSwiftInPenerimaPenerus_IND_pekerjaanLain.Visible = True
                Else
                    Me.trSwiftInPenerimaPenerus_IND_pekerjaanLain.Visible = False
                End If
            End If
        Catch ex As Exception
            ' LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButton_SwiftInPenerimaPenerus_IND_KotaDom_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_SwiftInPenerimaPenerus_IND_KotaDom.Click
        Try
            If Session("PickerKotaKab.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
                TxtSwiftInPenerimaPenerus_IND_kotaDom.Text = strData(1)
                hfSwiftInPenerimaPenerus_IND_kotaDom.Value = strData(0)

                hfSwiftInPenerimaPenerus_IND_ProvDom.Value = IFTIBLL.GetProvinceIDByKotaKab(strData(0))
                TxtSwiftInPenerimaPenerus_IND_ProvDom.Text = IFTIBLL.GetProvincetextByKotaKab(strData(0))


                Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
                If hfSwiftInPenerimaPenerus_IND_kotaDom.Value = ParameterKota.MsSystemParameter_Value Then
                    Me.trSwiftInPenerimaPenerus_IND_kotaLainDom.Visible = True
                Else
                    Me.trSwiftInPenerimaPenerus_IND_kotaLainDom.Visible = False
                End If
                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.KabupatenKota)
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub


    Protected Sub imgButton_SwiftInPenerimaPenerus_IND_ProvinsiDom_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_SwiftInPenerimaPenerus_IND_ProvinsiDom.Click
        Try
            If Session("PickerProvinsi.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                TxtSwiftInPenerimaPenerus_IND_ProvDom.Text = strData(1)
                hfSwiftInPenerimaPenerus_IND_ProvDom.Value = strData(0)
                Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
                If hfSwiftInPenerimaPenerus_IND_ProvDom.Value = ParameterProvinsi.MsSystemParameter_Value Then
                    Me.trSwiftInPenerimaPenerus_IND_ProvLainDom.Visible = True
                Else
                    Me.trSwiftInPenerimaPenerus_IND_ProvLainDom.Visible = False
                End If
                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.Propinsi)
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButton_SwiftInPenerimaPenerus_IND_KotaIden_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_SwiftInPenerimaPenerus_IND_KotaIden.Click
        Try
            If Session("PickerKotaKab.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
                TxtSwiftInPenerimaPenerus_IND_kotaIden.Text = strData(1)
                hfSwiftInPenerimaPenerus_IND_kotaIden.Value = strData(0)

                hfSwiftInPenerimaPenerus_IND_ProvIden.Value = IFTIBLL.GetProvinceIDByKotaKab(strData(0))
                TxtSwiftInPenerimaPenerus_IND_ProvIden.Text = IFTIBLL.GetProvincetextByKotaKab(strData(0))


                Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
                If hfSwiftInPenerimaPenerus_IND_kotaIden.Value = ParameterKota.MsSystemParameter_Value Then
                    Me.trSwiftInPenerimaPenerus_IND_KotaLainIden.Visible = True
                Else
                    Me.trSwiftInPenerimaPenerus_IND_KotaLainIden.Visible = False
                End If
                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.KabupatenKota)
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButton_SwiftInPenerimaPenerus_IND_ProvinsiIden_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_SwiftInPenerimaPenerus_IND_ProvinsiIden.Click
        Try
            If Session("PickerProvinsi.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                TxtSwiftInPenerimaPenerus_IND_ProvIden.Text = strData(1)
                hfSwiftInPenerimaPenerus_IND_ProvIden.Value = strData(0)
                Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
                If hfSwiftInPenerimaPenerus_IND_ProvIden.Value = ParameterProvinsi.MsSystemParameter_Value Then
                    Me.trSwiftInPenerimaPenerus_IND_ProvLainIden.Visible = True
                Else
                    Me.trSwiftInPenerimaPenerus_IND_ProvLainIden.Visible = False
                End If
                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.Propinsi)
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButton_SwiftInPenerimaPenerus_Korp_bidangUsaha_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_SwiftInPenerimaPenerus_Korp_bidangUsaha.Click
        Try
            If Session("PickerBidangUsaha.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerBidangUsaha.Data")).Split(";")
                txtSwiftInPenerimaPenerus_Korp_BidangUsahaKorp.Text = strData(1)
                hfSwiftInPenerimaPenerus_Korp_BidangUsahaKorp.Value = strData(0)
                Dim ParameterBidangUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BidangUsaha)
                If hfSwiftInPenerimaPenerus_Korp_BidangUsahaKorp.Value = ParameterBidangUsaha.MsSystemParameter_Value Then
                    Me.trSwiftInPenerimaPenerus_Korp_BidangUsahaKorpLainnya.Visible = True
                Else
                    Me.trSwiftInPenerimaPenerus_Korp_BidangUsahaKorpLainnya.Visible = False

                End If
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub imgButton_SwiftInPenerimaPenerus_Korp_KotaLengkap_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_SwiftInPenerimaPenerus_Korp_KotaLengkap.Click
        Try
            If Session("PickerKotaKab.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
                txtSwiftInPenerimaPenerus_Korp_kotakorp.Text = strData(1)
                hfSwiftInPenerimaPenerus_Korp_kotakorp.Value = strData(0)

                hfSwiftInPenerimaPenerus_Korp_ProvinsiKorp.Value = IFTIBLL.GetProvinceIDByKotaKab(strData(0))
                txtSwiftInPenerimaPenerus_Korp_ProvinsiKorp.Text = IFTIBLL.GetProvincetextByKotaKab(strData(0))


                Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
                If hfSwiftInPenerimaPenerus_Korp_kotakorp.Value = ParameterKota.MsSystemParameter_Value Then
                    Me.trSwiftInPenerimaPenerus_Korp_kotalainnyakorp.Visible = True
                Else
                    Me.trSwiftInPenerimaPenerus_Korp_kotalainnyakorp.Visible = False
                End If
                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.KabupatenKota)
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButton_SwiftInPenerimaPenerus_Korp_ProvinsiLengkap_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_SwiftInPenerimaPenerus_Korp_ProvinsiLengkap.Click
        Try
            If Session("PickerProvinsi.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                txtSwiftInPenerimaPenerus_Korp_ProvinsiKorp.Text = strData(1)
                hfSwiftInPenerimaPenerus_Korp_ProvinsiKorp.Value = strData(0)
                Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
                If hfSwiftInPenerimaPenerus_Korp_ProvinsiKorp.Value = ParameterProvinsi.MsSystemParameter_Value Then
                    Me.trSwiftInPenerimaPenerus_Korp_provinsiLainnyaKorp.Visible = True
                Else
                    Me.trSwiftInPenerimaPenerus_Korp_provinsiLainnyaKorp.Visible = False
                End If
                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.Propinsi)
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImageButton_Transaksi_SwiftIncurrency_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton_Transaksi_SwiftIncurrency.Click
        Try
            If Session("PickerMataUang.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerMataUang.Data")).Split(";")
                Dim ParameterMataUang As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.MataUang)
                'Transaksi_SwiftIncurrency0.Text = strData(0) & " - " & strData(1)
                'hfTransaksi_SwiftIncurrency.Value = strData(0)
                Using objMatauang As TList(Of MsCurrency) = DataRepository.MsCurrencyProvider.GetPaged("Code = '" & strData(0).Trim & "'", "", 0, 1, 0)
                    If objMatauang.Count > 0 Then
                        Transaksi_SwiftIncurrency0.Text = objMatauang(0).Code
                        hfTransaksi_SwiftIncurrency.Value = objMatauang(0).IdCurrency
                        If hfTransaksi_SwiftIncurrency.Value = ParameterMataUang.MsSystemParameter_Value Then
                            Me.trSwiftIncurrencyLainnya.Visible = True
                        Else
                            Me.trSwiftIncurrencyLainnya.Visible = False
                        End If
                    End If
                End Using
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    Protected Sub ImageButton_Transaksi_SwiftInMataUangTransaksi0_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton_Transaksi_SwiftInMataUangTransaksi0.Click
        Try
            If Session("PickerMataUang.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerMataUang.Data")).Split(";")
                Dim ParameterMataUang As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.MataUang)
                'Transaksi_SwiftInMataUangTransaksi0.Text = strData(0) & " - " & strData(1)
                'hfTransaksi_SwiftInMataUangTransaksi0.Value = strData(0)
                Using objMatauang As TList(Of MsCurrency) = DataRepository.MsCurrencyProvider.GetPaged("Code = '" & strData(0).Trim & "'", "", 0, 1, 0)
                    If objMatauang.Count > 0 Then
                        Transaksi_SwiftInMataUangTransaksi0.Text = objMatauang(0).Code
                        hfTransaksi_SwiftInMataUangTransaksi0.Value = objMatauang(0).IdCurrency
                        If hfTransaksi_SwiftInMataUangTransaksi0.Value = ParameterMataUang.MsSystemParameter_Value Then
                            Me.trSwiftInMataUangTransaksi0Lainnya.Visible = True
                        Else
                            Me.trSwiftInMataUangTransaksi0Lainnya.Visible = False
                        End If
                    End If
                End Using
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
#End Region
#Region "Radio Button"
    Protected Sub RbSwiftInPengirimNasabah_IND_Warganegara_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RbSwiftInPengirimNasabah_IND_Warganegara.SelectedIndexChanged
        If RbSwiftInPengirimNasabah_IND_Warganegara.SelectedValue = 1 Then
            Me.negaraPengirimInd.Visible = False
            Me.negaraLainPengirimInd.Visible = False
        Else
            Me.negaraPengirimInd.Visible = True
            'Me.negaraLainPengirimInd.Visible = True
        End If
    End Sub

    Protected Sub RbSwiftInPenerimaNasabah_IND_Kewarganegaraan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RbSwiftInPenerimaNasabah_IND_Kewarganegaraan.SelectedIndexChanged
        If RbSwiftInPenerimaNasabah_IND_Kewarganegaraan.SelectedValue = 1 Then
            Me.negaraPenerimaInd.Visible = False
            Me.negaraLainPenerimaInd.Visible = False
        Else
            Me.negaraPenerimaInd.Visible = True
            'Me.negaraLainPenerimaInd.Visible = True
        End If
    End Sub

    Protected Sub RBSwiftInPenerimaPenerus_Ind_Kewarganegaraan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RBSwiftInPenerimaPenerus_Ind_Kewarganegaraan.SelectedIndexChanged
        If RBSwiftInPenerimaPenerus_Ind_Kewarganegaraan.SelectedValue = 1 Then
            Me.negaraPenerimaPenerusInd.Visible = False
            Me.negaraLainPenerimaPenerusInd.Visible = False
        Else
            Me.negaraPenerimaPenerusInd.Visible = True
            'Me.negaraLainPenerimaPenerusInd.Visible = True
        End If
    End Sub
#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                clearSession()
                AuditTrailBLL.InsertAuditTrailUserAccess(Sahassa.AML.Commonly.SessionCurrentPage)
                SetCOntrolLoad()
                TransactionSwiftInc()
                'loadIFTIToField()
                'loadResume()

                'If MultiView1.ActiveViewIndex = 0 Then
                '    ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('IdTerlapor','Img1','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
                '    ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('Transaksi','Img2','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
                'End If
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            'LogError(ex)

        End Try

    End Sub

    Protected Sub cvalPageErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles cvalPageErr.PreRender
        If cvalPageErr.IsValid = False Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
        End If
    End Sub
    Protected Sub ImageButtonSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSave.Click
        'If SessionPkUserId = 1 Then
        '    SaveSwiftIncoDirect()
        'Else
        SaveSwiftInco()
        'End If

    End Sub
    Protected Sub CheckSwiftInPenerimaNas_Ind_alamat_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckSwiftInPenerimaNas_Ind_alamat.CheckedChanged
        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)

        If CheckSwiftInPenerimaNas_Ind_alamat.Checked = True Then
            Me.TxtSwiftInIdenPenerimaNas_Ind_alamatIdentitas.Text = TxtSwiftInIdenPenerimaNas_Ind_Alamat.Text
            Me.TxtSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Text = TxtSwiftInIdenPenerimaNas_Ind_Kota.Text
            hfSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Value = hfSwiftInIdenPenerimaNas_Ind_Kota_dom.Value
            If hfSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Value = ParameterKota.MsSystemParameter_Value Then
                Me.TxtSwiftInIdenPenerimaNas_Ind_KotaLainIden.Text = TxtSwiftInIdenPenerimaNas_Ind_kotalain.Text
                Me.trSwiftInIdenPenerimaNas_Ind_KotaLainIden.Visible = True
            Else
                Me.trSwiftInIdenPenerimaNas_Ind_KotaLainIden.Visible = False
            End If

            Me.TxtSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Text = TxtSwiftInIdenPenerimaNas_Ind_provinsi.Text
            hfSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Value = hfSwiftInIdenPenerimaNas_Ind_provinsi_dom.Value
            If hfSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Value = ParameterProvinsi.MsSystemParameter_Value Then
                Me.TxtSwiftInIdenPenerimaNas_Ind_ProvinsilainIden.Text = TxtSwiftInIdenPenerimaNas_Ind_provinsiLain.Text
                Me.trSwiftInIdenPenerimaNas_Ind_ProvinsilainIden.Visible = True
            Else
                Me.trSwiftInIdenPenerimaNas_Ind_ProvinsilainIden.Visible = False
            End If


        Else
            Me.TxtSwiftInIdenPenerimaNas_Ind_alamatIdentitas.Text = ""
            Me.TxtSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Text = ""
            hfSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Value = ""
            Me.trSwiftInIdenPenerimaNas_Ind_KotaLainIden.Visible = False
            Me.TxtSwiftInIdenPenerimaNas_Ind_KotaLainIden.Text = ""
            Me.TxtSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Text = ""
            hfSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Value = ""
            Me.trSwiftInIdenPenerimaNas_Ind_ProvinsilainIden.Visible = False
            Me.TxtSwiftInIdenPenerimaNas_Ind_ProvinsilainIden.Text = ""

        End If
    End Sub

    Protected Sub imgOKMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgOKMsg.Click
        Response.Redirect("iftiview.aspx")
    End Sub

    Protected Sub ImageButtonCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonCancel.Click
        cboSwiftInPenerima_TipePJKBank.Enabled = True
        RbSwiftInPenerima_TipePenerima.Enabled = True
        Response.Redirect("iftiview.aspx")
    End Sub


#End Region

    Protected Sub GridDataView_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridDataView.DeleteCommand
        If e.Item.ItemType <> ListItemType.Header And e.Item.ItemType <> ListItemType.Footer Then
            Select Case e.CommandName
                Case "delete"
                    ReceiverDataTable.Rows.RemoveAt(e.Item.ItemIndex + (SetnGetCurrentPageReceiver * GetDisplayedTotalRow))

                    LoadReceiver()
            End Select
        End If
    End Sub

    Protected Sub GridDataView_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridDataView.EditCommand
        Dim PKIFTIBeneficiary As Integer
        Try
            PKIFTIBeneficiary = CInt(e.Item.Cells(4).Text)
            'getIFTIBeneficiaryTempPK = PKIFTIBeneficiary

            'End Using
            Dim dr As Data.DataRow = ReceiverDataTable.Rows(e.Item.ItemIndex + (SetnGetCurrentPageReceiver * Sahassa.AML.Commonly.GetDisplayedTotalRow))
            getIFTIBeneficiaryTempPK = (e.Item.ItemIndex + (SetnGetCurrentPageReceiver * Sahassa.AML.Commonly.GetDisplayedTotalRow)) + 1


            'Me.txtPenerima_rekening.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_NoRekening
            cboSwiftInPenerima_TipePJKBank.Enabled = False
            RbSwiftInPenerima_TipePenerima.Enabled = False

            ' cek ReceiverType
            Dim receiverType As Integer = dr("FK_IFTI_NasabahType_ID")
            Select Case (receiverType)
                Case 1
                    'penerimaPerorangan
                    'Me.txtSwiftInPenerimaNasabah_Rekening.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_NoRekening
                    Me.cboSwiftInPenerima_TipePJKBank.SelectedValue = 1
                    RbSwiftInPenerima_TipePenerima.SelectedValue = 1
                    RbSwiftInPenerima_TipeNasabah.SelectedValue = 1
                    Me.trSwiftInTipepengirim.Visible = True
                    Me.trSwiftInPenerimaTipeNasabah.Visible = True
                    Me.MultiViewSwiftInPenerima.ActiveViewIndex = 0
                    Me.MultiViewSwiftInPenAkhirJenis.ActiveViewIndex = 0
                    Me.MultiViewSwiftINPenerimaAkhirNasabah.ActiveViewIndex = 0
                    FieldSwiftInReceiverCase1()
                    Me.ImageButton_savePenerimaInd.Visible = True
                    Me.ImageButton_AddPenerimaInd.Visible = False
                Case 2
                    'penerimakorporasi
                    'Me.txtSwiftInPenerimaNasabah_Rekening.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_NoRekening
                    Me.cboSwiftInPenerima_TipePJKBank.SelectedValue = 1
                    RbSwiftInPenerima_TipePenerima.SelectedValue = 1
                    RbSwiftInPenerima_TipeNasabah.SelectedValue = 2
                    Me.trSwiftInTipepengirim.Visible = True
                    Me.trSwiftInPenerimaTipeNasabah.Visible = True
                    Me.MultiViewSwiftInPenerima.ActiveViewIndex = 0
                    Me.MultiViewSwiftInPenAkhirJenis.ActiveViewIndex = 0
                    Me.MultiViewSwiftINPenerimaAkhirNasabah.ActiveViewIndex = 1
                    FieldSwiftInReceiverCase2()
                    Me.ImageButton_savePenerimaKorp.Visible = True
                    Me.ImageButton_AddPenerimaKorp.Visible = False
                Case 3
                    'PengirimNonNasabah
                    Me.cboSwiftInPenerima_TipePJKBank.SelectedValue = 1
                    RbSwiftInPenerima_TipePenerima.SelectedValue = 2
                    Me.trSwiftInTipepengirim.Visible = True
                    Me.trSwiftInPenerimaTipeNasabah.Visible = False
                    Me.MultiViewSwiftInPenerima.ActiveViewIndex = 0
                    Me.MultiViewSwiftInPenAkhirJenis.ActiveViewIndex = 1
                    FieldSwiftInReceiverCase3()
                    Me.ImageButton_SavePenerimaNonNasabah.Visible = True
                    Me.ImageButton_AddPenerimaNonNasabah.Visible = False
                Case 4
                    'PenyelenggaraIndividu
                    ' Me.TxtSwiftInPenerimaPenerus_Rekening.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_NoRekening
                    Me.cboSwiftInPenerima_TipePJKBank.SelectedValue = 2
                    RbSwiftInPenerima_TipeNasabah.SelectedValue = 1
                    Me.trSwiftInTipepengirim.Visible = False
                    Me.trSwiftInPenerimaTipeNasabah.Visible = True
                    Me.MultiViewSwiftInPenerima.ActiveViewIndex = 1
                    Me.MultiViewSwiftInPenerimaPenerus.ActiveViewIndex = 0
                    FieldSwiftInReceiverCase4()
                    Me.ImageButton_SavePenerimaPenerusInd.Visible = True
                    Me.ImageButton_AddPenerimaPenerusInd.Visible = False
                Case 5
                    'PenyelenggaraKorp
                    ' Me.TxtSwiftInPenerimaPenerus_Rekening.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_NoRekening
                    Me.cboSwiftInPenerima_TipePJKBank.SelectedValue = 2
                    RbSwiftInPenerima_TipeNasabah.SelectedValue = 2
                    Me.MultiViewSwiftInPenerima.ActiveViewIndex = 1
                    Me.MultiViewSwiftInPenerimaPenerus.ActiveViewIndex = 1
                    Me.trSwiftInTipepengirim.Visible = False
                    Me.trSwiftInTipeNasabah.Visible = True
                    FieldSwiftInReceiverCase5()
                    Me.ImageButton_SavePenerimaPenerusKorp.Visible = True
                    Me.ImageButton_AddPenerimaPenerusKorp.Visible = False
            End Select

            GridDataView.Enabled = False
        Catch ex As Exception
            'LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub


    Protected Sub GridDataView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridDataView.ItemDataBound
        Try
            Dim CollCount As Integer = e.Item.Cells.Count - 1
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Dim chkBox As CheckBox = CType(e.Item.FindControl("CheckBoxExporttoExcel"), CheckBox)
                chkBox.Checked = SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)
                If BindGridFromExcel = True Then
                    e.Item.Cells(4).Text = CStr((e.Item.ItemIndex + 1))
                Else
                    e.Item.Cells(4).Text = CStr((e.Item.ItemIndex + 1) + (SetnGetCurrentPage * GetDisplayedTotalRow))
                End If
            End If
        Catch ex As Exception
            'LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Me.LoadReceiver()
    End Sub

    Protected Sub ImageButton_savePenerimaInd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton_savePenerimaInd.Click
        cboSwiftInPenerima_TipePJKBank.Enabled = True
        RbSwiftInPenerima_TipePenerima.Enabled = True
        EditReceiver(getIFTIBeneficiaryTempPK - 1)
    End Sub

    Protected Sub ImageButton_AddPenerimaInd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton_AddPenerimaInd.Click
        AddReceiver()
    End Sub

    Protected Sub ImageButton_SavePenerimaPenerusKorp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton_SavePenerimaPenerusKorp.Click
        cboSwiftInPenerima_TipePJKBank.Enabled = True
        RbSwiftInPenerima_TipePenerima.Enabled = True
        EditReceiver(getIFTIBeneficiaryTempPK - 1)
    End Sub
    Protected Sub ImageButton_SavePenerimaPenerusInd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton_SavePenerimaPenerusInd.Click
        EditReceiver(getIFTIBeneficiaryTempPK - 1)
    End Sub
    Protected Sub ImageButton_SavePenerimaNonNasabah_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton_SavePenerimaNonNasabah.Click
        cboSwiftInPenerima_TipePJKBank.Enabled = True
        RbSwiftInPenerima_TipePenerima.Enabled = True
        EditReceiver(getIFTIBeneficiaryTempPK - 1)
    End Sub
    Protected Sub ImageButton_savePenerimaKorp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton_savePenerimaKorp.Click
        cboSwiftInPenerima_TipePJKBank.Enabled = True
        RbSwiftInPenerima_TipePenerima.Enabled = True
        EditReceiver(getIFTIBeneficiaryTempPK - 1)
    End Sub
    Protected Sub ImageButton_CancelPenerimaInd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton_CancelPenerimaInd.Click
        LoadReceiver()
        cboSwiftInPenerima_TipePJKBank.Enabled = True
        RbSwiftInPenerima_TipePenerima.Enabled = True
        txtSwiftInPenerimaNasabah_Rekening.Text = ""
        txtSwiftInPenerimaNasabah_IND_nama.Text = ""
        txtSwiftInPenerimaNasabah_IND_TanggalLahir.Text = ""
        RbSwiftInPenerimaNasabah_IND_Kewarganegaraan.SelectedIndex = 0
        hfSwiftInPenerimaNasabah_IND_negara.Value = ""
        txtSwiftInPenerimaNasabah_IND_negara.Text = ""
        txtSwiftInPenerimaNasabah_IND_negaralain.Text = ""
        hfSwiftInIdenPenerimaNas_Ind_pekerjaan.Value = ""
        txtSwiftInPenerimaNasabah_IND_pekerjaan.Text = ""
        Me.TxtSwiftInIdenPenerimaNas_Ind_Pekerjaanlain.Text = ""
        TxtSwiftInIdenPenerimaNas_Ind_Alamat.Text = ""
        Me.hfSwiftInIdenPenerimaNas_Ind_Kota_dom.Value = ""
        TxtSwiftInIdenPenerimaNas_Ind_Kota.Text = ""
        TxtSwiftInIdenPenerimaNas_Ind_kotalain.Text = ""
        hfSwiftInIdenPenerimaNas_Ind_provinsi_dom.Value = ""
        TxtSwiftInIdenPenerimaNas_Ind_provinsi.Text = ""
        TxtSwiftInIdenPenerimaNas_Ind_provinsiLain.Text = ""
        TxtSwiftInIdenPenerimaNas_Ind_alamatIdentitas.Text = ""
        hfSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Value = ""
        TxtSwiftInIdenPenerimaNas_Ind_Kota.Text = ""
        TxtSwiftInIdenPenerimaNas_Ind_KotaLainIden.Text = ""
        TxtSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Text = ""
        hfSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Value = ""
        TxtSwiftInIdenPenerimaNas_Ind_provinsiLain.Text = ""
        TxtSwiftInIdenPenerimaNas_Ind_ProvinsilainIden.Text = ""
        TxtISwiftIndenPenerimaNas_Ind_noTelp.Text = ""
        cbo_SwiftInpenerimaNas_Ind_jenisidentitas.SelectedIndex = 0
        Me.TxtISwiftIndenPenerimaNas_Ind_noIdentitas.Text = ""
        txtSwiftInIdenPenerimaNas_IND_NilaiTransaksiKeuangan.Text = ""

        GridDataView.Enabled = True
        Me.ImageButton_savePenerimaInd.Visible = False
        Me.ImageButton_AddPenerimaInd.Visible = True
    End Sub

    Protected Sub ImageButton_CancelPenerimaKorp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton_CancelPenerimaKorp.Click
        LoadReceiver()
        txtSwiftInPenerimaNasabah_Rekening.Text = ""
        cboSwiftInPenerima_TipePJKBank.Enabled = True
        RbSwiftInPenerima_TipePenerima.Enabled = True
        txtSwiftInPenerimaNasabah_Korp_namaKorp.Text = ""
        txtSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLainnya.Text = ""
        cbo_SwiftInPenerimaNasabah_Korp_BentukBadanUsaha.SelectedIndex = 0
        hfSwiftInPenerimaNasabah_Korp_BidangUsahaKorp.Value = ""
        txtSwiftInPenerimaNasabah_Korp_BidangUsahaKorp.Text = ""
        txtSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLainnya.Text = ""
        txtSwiftInPenerimaNasabah_Korp_AlamatKorp.Text = ""
        hfSwiftInPenerimaNasabah_Korp_kotakab.Value = ""
        txtSwiftInPenerimaNasabah_Korp_Kotakab.Text = ""
        Me.txtSwiftInPenerimaNasabah_Korp_kotaLain.Text = ""
        hfSwiftInPenerimaNasabah_Korp_propinsi.Value = ""
        txtSwiftInPenerimaNasabah_Korp_propinsi.Text = ""
        txtSwiftInPenerimaNasabah_Korp_propinsilain.Text = ""
        txtSwiftInPenerimaNasabah_Korp_NoTelp.Text = ""
        txtSwiftInPenerimaNasabah_Korp_NilaiTransaksiKeuangan.Text = ""

        GridDataView.Enabled = True
        Me.ImageButton_savePenerimaKorp.Visible = False
        Me.ImageButton_AddPenerimaKorp.Visible = True
    End Sub

    Protected Sub ImageButton_CancelPenerimaPenerusInd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton_CancelPenerimaPenerusInd.Click
        LoadReceiver()
        cboSwiftInPenerima_TipePJKBank.Enabled = True
        RbSwiftInPenerima_TipePenerima.Enabled = True
        TxtSwiftInPenerimaPenerus_Rekening.Text = ""
        TxtSwiftInPenerimaPenerus_Ind_namaBank.Text = ""
        TxtSwiftInPenerimaPenerus_Ind_nama.Text = ""
        TxtSwiftInPenerimaPenerus_Ind_TanggalLahir.Text = ""
        RBSwiftInPenerimaPenerus_Ind_Kewarganegaraan.SelectedValue = 1
        hfSwiftInPenerimaPenerus_IND_negara.Value = ""
        TxtSwiftInPenerimaPenerus_IND_negara.Text = ""
        TxtSwiftInPenerimaPenerus_IND_negaralain.Text = ""
        hfSwiftInPenerimaPenerus_IND_pekerjaan.Value = ""
        TxtSwiftInPenerimaPenerus_IND_pekerjaan.Text = ""
        TxtSwiftInPenerimaPenerus_IND_pekerjaanLain.Text = ""
        TxtSwiftInPenerimaPenerus_IND_alamatDom.Text = ""
        hfSwiftInPenerimaPenerus_IND_kotaDom.Value = ""
        TxtSwiftInPenerimaPenerus_IND_kotaDom.Text = ""
        TxtSwiftInPenerimaPenerus_IND_kotaLainDom.Text = ""
        hfSwiftInPenerimaPenerus_IND_ProvDom.Value = ""
        TxtSwiftInPenerimaPenerus_IND_ProvDom.Text = ""
        TxtSwiftInPenerimaPenerus_IND_ProvLainDom.Text = ""
        TxtSwiftInPenerimaPenerus_IND_alamatIden.Text = ""
        hfSwiftInPenerimaPenerus_IND_kotaIden.Value = ""
        TxtSwiftInPenerimaPenerus_IND_kotaIden.Text = ""
        TxtSwiftInPenerimaPenerus_IND_KotaLainIden.Text = ""
        hfSwiftInPenerimaPenerus_IND_ProvIden.Value = ""
        TxtSwiftInPenerimaPenerus_IND_ProvIden.Text = ""
        TxtSwiftInPenerimaPenerus_IND_ProvLainIden.Text = ""
        TxtSwiftInPenerimaPenerus_IND_NoTelp.Text = ""
        CBoSwiftInPenerimaPenerus_IND_jenisIdentitas.SelectedIndex = 0
        'cbo_SwiftInpenerimaNas_Ind_jenisidentitas.SelectedIndex = 6
        TxtSwiftInPenerimaPenerus_IND_nomoridentitas.Text = ""
        TxtSwiftInPenerimaPenerus_IND_NIlaiTransaksi.Text = ""

        GridDataView.Enabled = True
        Me.ImageButton_SavePenerimaPenerusInd.Visible = False
        Me.ImageButton_AddPenerimaPenerusKorp.Visible = True
    End Sub

    Protected Sub ImageButton_CancelPenerimaPenerusKorp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton_CancelPenerimaPenerusKorp.Click
        LoadReceiver()
        TxtSwiftInPenerimaPenerus_Rekening.Text = ""
        txtSwiftInPenerimaPenerus_Korp_namabank.Text = ""
        txtSwiftInPenerimaPenerus_Korp_NamaKorp.Text = ""
        txtSwiftInPenerimaPenerus_Korp_BentukBadanUsahaLain.Text = ""
        cbo_SwiftInPenerimaPenerus_Korp_BentukBadanUsaha.SelectedIndex = 0
        hfSwiftInPenerimaPenerus_Korp_BidangUsahaKorp.Value = ""
        txtSwiftInPenerimaPenerus_Korp_BidangUsahaKorp.Text = ""
        Me.txtSwiftInPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Text = ""
        txtSwiftInPenerimaPenerus_Korp_alamatkorp.Text = ""
        hfSwiftInPenerimaPenerus_Korp_kotakorp.Value = ""
        txtSwiftInPenerimaPenerus_Korp_kotakorp.Text = ""
        txtSwiftInPenerimaPenerus_Korp_kotalainnyakorp.Text = ""
        hfSwiftInPenerimaPenerus_Korp_ProvinsiKorp.Value = ""
        txtSwiftInPenerimaPenerus_Korp_ProvinsiKorp.Text = ""
        txtSwiftInPenerimaPenerus_Korp_provinsiLainnyaKorp.Text = ""
        txtSwiftInPenerimaPenerus_Korp_NoTelp.Text = ""
        txtSwiftInPenerimaPenerus_Korp_NilaiTransaksi.Text = ""
        GridDataView.Enabled = True
        Me.ImageButton_SavePenerimaPenerusKorp.Visible = False
        Me.ImageButton_AddPenerimaPenerusKorp.Visible = True
    End Sub

    Protected Sub ImageButton_AddPenerimaKorp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton_AddPenerimaKorp.Click
        AddReceiver()
    End Sub

    Protected Sub ImageButton_AddPenerimaNonNasabah_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton_AddPenerimaNonNasabah.Click
        AddReceiver()
    End Sub

    Protected Sub ImageButton_AddPenerimaPenerusInd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton_AddPenerimaPenerusInd.Click
        AddReceiver()
    End Sub

    Protected Sub ImageButton_AddPenerimaPenerusKorp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton_AddPenerimaPenerusKorp.Click
        AddReceiver()
    End Sub

    Protected Sub ImageButton_CancelPenerimaNonNasabah_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton_CancelPenerimaNonNasabah.Click
        LoadReceiver()
        TxtSwiftInPenerimaPenerus_Rekening.Text = ""

        TxtSwiftInPenerimaNonNasabah_nama.Text = ""
        TxtSwiftInPenerimaNonNasabah_TanggalLahir.Text = ""
        TxtSwiftInPenerimaNonNasabah_alamatiden.Text = ""
        TxtSwiftInPenerimaNonNasabah_NoTelepon.Text = ""
        CboSwiftInPenerimaNonNasabah_JenisDokumen.SelectedIndex = 0
        TxtSwiftInPenerimaNonNasabah_NomorIden.Text = ""
        TxtSwiftInPenerimaNonNasabah_NilaiTransaksiKeuangan.Text = ""

        GridDataView.Enabled = True
        Me.ImageButton_SavePenerimaNonNasabah.Visible = False
        Me.ImageButton_AddPenerimaNonNasabah.Visible = True
    End Sub


    Protected Sub cboSwiftInPenerima_TipePJKBank_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSwiftInPenerima_TipePJKBank.SelectedIndexChanged
        If cboSwiftInPenerima_TipePJKBank.SelectedValue = 1 Then
            trSwiftInTipepengirim.Visible = True
            trSwiftInPenerimaTipeNasabah.Visible = False

            RbSwiftInPenerima_TipeNasabah.SelectedValue = 1
            'pengirimNasabahIndividu
            Me.cboSwiftInPenerima_TipePJKBank.SelectedValue = 1
            RbSwiftInPenerima_TipePenerima.SelectedValue = 1
            RbSwiftInPenerima_TipeNasabah.SelectedValue = 1
            Me.trSwiftInTipepengirim.Visible = True
            Me.trSwiftInPenerimaTipeNasabah.Visible = True
            Me.MultiViewSwiftInPenerima.ActiveViewIndex = 0
            Me.MultiViewSwiftInPenAkhirJenis.ActiveViewIndex = 0
            Me.MultiViewSwiftINPenerimaAkhirNasabah.ActiveViewIndex = 0
            RbSwiftInPenerimaNasabah_IND_Kewarganegaraan.SelectedIndex = 0
            RbSwiftInPenerimaNasabah_IND_Kewarganegaraan.SelectedIndex = 0

        ElseIf cboSwiftInPenerima_TipePJKBank.SelectedValue = 2 Then
            trSwiftInPenerimaTipeNasabah.Visible = True
            trSwiftInTipepengirim.Visible = False

            RbSwiftInPenerima_TipeNasabah.SelectedValue = 1
            'PenyelenggaraIndividu
            Me.cboSwiftInPenerima_TipePJKBank.SelectedValue = 2
            RbSwiftInPenerima_TipeNasabah.SelectedValue = 1
            Me.trSwiftInTipepengirim.Visible = False
            Me.trSwiftInPenerimaTipeNasabah.Visible = True
            Me.MultiViewSwiftInPenerima.ActiveViewIndex = 1
            Me.MultiViewSwiftInPenerimaPenerus.ActiveViewIndex = 0
            RBSwiftInPenerimaPenerus_Ind_Kewarganegaraan.SelectedIndex = 0
            'Me.MultiViewSwiftInPenerima.ActiveViewIndex = 1
        End If
    End Sub

    Protected Sub RbSwiftInPenerima_TipePenerima_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RbSwiftInPenerima_TipePenerima.SelectedIndexChanged
        If RbSwiftInPenerima_TipePenerima.SelectedValue = 1 Then
            trSwiftInPenerimaTipeNasabah.Visible = True
            trSwiftInTipepengirim.Visible = True
            Me.MultiViewSwiftInPenAkhirJenis.ActiveViewIndex = 0

        ElseIf RbSwiftInPenerima_TipePenerima.SelectedValue = 2 Then
            trSwiftInPenerimaTipeNasabah.Visible = False
            trSwiftInTipepengirim.Visible = True
            Me.MultiViewSwiftInPenAkhirJenis.ActiveViewIndex = 1
            Me.cboSwiftInPenerima_TipePJKBank.SelectedValue = 1
            Me.trSwiftInTipepengirim.Visible = True
            Me.MultiViewSwiftInPenerima.ActiveViewIndex = 0
        End If
    End Sub

    Protected Sub RbSwiftInPenerima_TipeNasabah_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RbSwiftInPenerima_TipeNasabah.SelectedIndexChanged

        If Me.cboSwiftInPenerima_TipePJKBank.SelectedValue = 1 Then
            If RbSwiftInPenerima_TipeNasabah.SelectedValue = 1 Then
                'pengirimNasabahIndividu
                Me.cboSwiftInPenerima_TipePJKBank.SelectedValue = 1
                RbSwiftInPenerima_TipePenerima.SelectedValue = 1
                RbSwiftInPenerima_TipeNasabah.SelectedValue = 1
                Me.trSwiftInTipepengirim.Visible = True
                Me.trSwiftInPenerimaTipeNasabah.Visible = True
                Me.MultiViewSwiftInPenerima.ActiveViewIndex = 0
                Me.MultiViewSwiftInPenAkhirJenis.ActiveViewIndex = 0
                Me.MultiViewSwiftINPenerimaAkhirNasabah.ActiveViewIndex = 0
                RbSwiftInPenerimaNasabah_IND_Kewarganegaraan.SelectedIndex = 0
                RbSwiftInPenerimaNasabah_IND_Kewarganegaraan.SelectedIndex = 0
                If Me.GridDataView.Enabled = False Then
                    ImageButton_savePenerimaInd.Visible = True
                    ImageButton_AddPenerimaInd.Visible = False
                End If
                'FieldSwiftInReceiverCase1()
            ElseIf RbSwiftInPenerima_TipeNasabah.SelectedValue = 2 Then
                'pengirimNasabahKorp
                Me.cboSwiftInPenerima_TipePJKBank.SelectedValue = 1
                RbSwiftInPenerima_TipePenerima.SelectedValue = 1
                RbSwiftInPenerima_TipeNasabah.SelectedValue = 2
                Me.trSwiftInTipepengirim.Visible = True
                Me.trSwiftInPenerimaTipeNasabah.Visible = True
                Me.MultiViewSwiftInPenerima.ActiveViewIndex = 0
                Me.MultiViewSwiftInPenAkhirJenis.ActiveViewIndex = 0
                Me.MultiViewSwiftINPenerimaAkhirNasabah.ActiveViewIndex = 1
                If Me.GridDataView.Enabled = False Then
                    ImageButton_savePenerimaKorp.Visible = True
                    ImageButton_AddPenerimaKorp.Visible = False
                End If
            End If
        ElseIf Me.cboSwiftInPenerima_TipePJKBank.SelectedValue = 2 Then
            If RbSwiftInPenerima_TipeNasabah.SelectedValue = 1 Then
                'PenyelenggaraIndividu
                Me.cboSwiftInPenerima_TipePJKBank.SelectedValue = 2
                RbSwiftInPenerima_TipeNasabah.SelectedValue = 1
                Me.trSwiftInTipepengirim.Visible = False
                Me.trSwiftInPenerimaTipeNasabah.Visible = True
                Me.MultiViewSwiftInPenerima.ActiveViewIndex = 1
                Me.MultiViewSwiftInPenerimaPenerus.ActiveViewIndex = 0
                RBSwiftInPenerimaPenerus_Ind_Kewarganegaraan.SelectedIndex = 0
                If Me.GridDataView.Enabled = False Then
                    ImageButton_savePenerimaInd.Visible = True
                    ImageButton_AddPenerimaInd.Visible = False
                End If
            ElseIf RbSwiftInPenerima_TipeNasabah.SelectedValue = 2 Then
                'PenyelenggaraKorp
                Me.cboSwiftInPenerima_TipePJKBank.SelectedValue = 2
                RbSwiftInPenerima_TipeNasabah.SelectedValue = 2
                Me.MultiViewSwiftInPenerima.ActiveViewIndex = 1
                Me.MultiViewSwiftInPenerimaPenerus.ActiveViewIndex = 1
                Me.trSwiftInTipepengirim.Visible = False
                Me.trSwiftInTipeNasabah.Visible = True
                If Me.GridDataView.Enabled = False Then
                    ImageButton_savePenerimaKorp.Visible = True
                    ImageButton_AddPenerimaKorp.Visible = False
                End If
            End If
        ElseIf Me.cboSwiftInPenerima_TipePJKBank.SelectedValue = 2 Then
            RbSwiftInPenerima_TipePenerima.SelectedValue = 2
            Me.trSwiftInTipepengirim.Visible = True
            Me.trSwiftInPenerimaTipeNasabah.Visible = False
            Me.MultiViewSwiftInPenerima.ActiveViewIndex = 0
            Me.MultiViewSwiftInPenAkhirJenis.ActiveViewIndex = 1
        End If

    End Sub


    Protected Sub rbrbSwiftIn_TipePengirim_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbrbSwiftIn_TipePengirim.SelectedIndexChanged
        If rbrbSwiftIn_TipePengirim.SelectedValue = 1 Then
            Me.trSwiftInTipeNasabah.Visible = True
            MultiViewSwiftInIdenPengirim.ActiveViewIndex = 0
            rbSwiftIn_TipeNasabah.SelectedValue = 1
            Me.MultiViewSwiftInIdenPengirim.Visible = True
            Me.MultiViewSwiftInNasabahType.ActiveViewIndex = 0
        Else
            Me.trSwiftInTipeNasabah.Visible = False
            Me.MultiViewSwiftInIdenPengirim.ActiveViewIndex = 1
            Me.MultiViewSwiftInIdenPengirim.Visible = True
        End If
    End Sub

    Protected Sub rbSwiftIn_TipeNasabah_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbSwiftIn_TipeNasabah.SelectedIndexChanged
        MultiViewSwiftInIdenPengirim.ActiveViewIndex = 0
        If rbSwiftIn_TipeNasabah.SelectedValue = 1 Then
            Me.MultiViewSwiftInIdenPengirim.Visible = True
            Me.MultiViewSwiftInNasabahType.ActiveViewIndex = 0
            RbSwiftInPengirimNasabah_IND_Warganegara.SelectedIndex = 0
        Else
            Me.MultiViewSwiftInIdenPengirim.Visible = True
            Me.MultiViewSwiftInNasabahType.ActiveViewIndex = 1
        End If
    End Sub

    Protected Sub CheckBoxSwiftInPenerimaPenerus_IND_samaIden_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSwiftInPenerimaPenerus_IND_samaIden.CheckedChanged
        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)

        If CheckBoxSwiftInPenerimaPenerus_IND_samaIden.Checked = True Then
            TxtSwiftInPenerimaPenerus_IND_alamatIden.Text = Me.TxtSwiftInPenerimaPenerus_IND_alamatDom.Text
            TxtSwiftInPenerimaPenerus_IND_kotaIden.Text = Me.TxtSwiftInPenerimaPenerus_IND_kotaDom.Text
            hfSwiftInPenerimaPenerus_IND_kotaIden.Value = hfSwiftInPenerimaPenerus_IND_kotaDom.Value
            If hfSwiftInPenerimaPenerus_IND_kotaIden.Value = ParameterKota.MsSystemParameter_Value Then
                TxtSwiftInPenerimaPenerus_IND_KotaLainIden.Text = Me.TxtSwiftInPenerimaPenerus_IND_kotaLainDom.Text
                Me.trSwiftInPenerimaPenerus_IND_KotaLainIden.Visible = True
            Else
                Me.trSwiftInPenerimaPenerus_IND_KotaLainIden.Visible = False
            End If

            TxtSwiftInPenerimaPenerus_IND_ProvIden.Text = Me.TxtSwiftInPenerimaPenerus_IND_ProvDom.Text
            hfSwiftInPenerimaPenerus_IND_ProvIden.Value = hfSwiftInPenerimaPenerus_IND_ProvDom.Value
            If hfSwiftInPenerimaPenerus_IND_ProvIden.Value = ParameterProvinsi.MsSystemParameter_Value Then
                TxtSwiftInPenerimaPenerus_IND_ProvLainIden.Text = Me.TxtSwiftInPenerimaPenerus_IND_ProvLainDom.Text
                Me.trSwiftInPenerimaPenerus_IND_ProvLainIden.Visible = True
            Else
                Me.trSwiftInPenerimaPenerus_IND_ProvLainIden.Visible = False
            End If

        Else
            TxtSwiftInPenerimaPenerus_IND_alamatIden.Text = ""
            TxtSwiftInPenerimaPenerus_IND_kotaIden.Text = ""
            hfSwiftInPenerimaPenerus_IND_kotaIden.Value = ""
            Me.trSwiftInPenerimaPenerus_IND_KotaLainIden.Visible = False
            TxtSwiftInPenerimaPenerus_IND_KotaLainIden.Text = ""
            TxtSwiftInPenerimaPenerus_IND_ProvIden.Text = ""
            hfSwiftInPenerimaPenerus_IND_ProvIden.Value = ""
            Me.trSwiftInPenerimaPenerus_IND_ProvLainIden.Visible = False
            TxtSwiftInPenerimaPenerus_IND_ProvLainIden.Text = ""

        End If
    End Sub

    Protected Sub RMSwiftInPengirimNas_IND_negaraWarga_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMSwiftInPengirimNas_IND_negaraWarga.Click
        txtSwiftInPengirimNasabah_IND_negara.Text = ""
        hfSwiftInPengirimNasabah_IND_negara.Value = Nothing
    End Sub

    Protected Sub RMSwiftInPengirimNas_IND_negaraIden_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMSwiftInPengirimNas_IND_negaraIden.Click
        txtSwiftInPengirimNasabah_IND_negaraIden.Text = ""
        hfSwiftInPengirimNasabah_IND_negaraIden.Value = Nothing
    End Sub

    Protected Sub RMSwiftInPengirimNasabah_Korp_NegaraKorp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMSwiftInPengirimNasabah_Korp_NegaraKorp.Click
        txtSwiftInPengirimNasabah_Korp_NegaraKorp.Text = ""
        hfSwiftInPengirimNasabah_Korp_NegaraKorp.Value = Nothing
    End Sub

    Protected Sub RMSwiftInPengirimNonNasabah_negara_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMSwiftInPengirimNonNasabah_negara.Click
        txtSwiftInPengirimNonNasabah_Negara.Text = ""
        hfSwiftInPengirimNonNasabah_Negara.Value = Nothing
    End Sub

    Protected Sub RMSwiftInPenerimaNasabah_IND_negara_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMSwiftInPenerimaNasabah_IND_negara.Click
        txtSwiftInPenerimaNasabah_IND_negara.Text = ""
        hfSwiftInPenerimaNasabah_IND_negara.Value = Nothing
    End Sub

    Protected Sub RMSwiftInPenerimaNas_Pekerjaan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMSwiftInPenerimaNas_Pekerjaan.Click
        txtSwiftInPenerimaNasabah_IND_pekerjaan.Text = ""
        hfSwiftInIdenPenerimaNas_Ind_pekerjaan.Value = Nothing
    End Sub

    Protected Sub RMSwiftInPenerimaNas_KotaDom_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMSwiftInPenerimaNas_KotaDom.Click
        TxtSwiftInIdenPenerimaNas_Ind_Kota.Text = ""
        hfSwiftInIdenPenerimaNas_Ind_Kota_dom.Value = Nothing
    End Sub

    Protected Sub RMSwiftInPenerimaNas_ProvinsiDom_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMSwiftInPenerimaNas_ProvinsiDom.Click
        TxtSwiftInIdenPenerimaNas_Ind_provinsi.Text = ""
        hfSwiftInIdenPenerimaNas_Ind_provinsi_dom.Value = Nothing
    End Sub

    Protected Sub RMSwiftInPenerimaNas_KotaIden_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMSwiftInPenerimaNas_KotaIden.Click
        TxtSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Text = ""
        hfSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Value = Nothing
    End Sub

    Protected Sub RMSwiftInPenerimaNas_ProvinsiIden_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMSwiftInPenerimaNas_ProvinsiIden.Click
        TxtSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Text = ""
        hfSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Value = Nothing
    End Sub

    Protected Sub RMSwiftInPenerimaNas_Korp_bidangUsaha_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMSwiftInPenerimaNas_Korp_bidangUsaha.Click
        txtSwiftInPenerimaNasabah_Korp_BidangUsahaKorp.Text = ""
        hfSwiftInPenerimaNasabah_Korp_BidangUsahaKorp.Value = Nothing
    End Sub

    Protected Sub RMSwiftInPenerimaNasabah_Korp_kotakab_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMSwiftInPenerimaNasabah_Korp_kotakab.Click
        txtSwiftInPenerimaNasabah_Korp_Kotakab.Text = ""
        hfSwiftInPenerimaNasabah_Korp_kotakab.Value = Nothing
    End Sub

    Protected Sub RMSwiftInPenerimaNasabah_Korp_propinsi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMSwiftInPenerimaNasabah_Korp_propinsi.Click
        txtSwiftInPenerimaNasabah_Korp_propinsi.Text = ""
        hfSwiftInPenerimaNasabah_Korp_propinsi.Value = Nothing
    End Sub

    Protected Sub RMSwiftInPenerimaPenerus_IND_Negara_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMSwiftInPenerimaPenerus_IND_Negara.Click
        TxtSwiftInPenerimaPenerus_IND_negara.Text = ""
        hfSwiftInPenerimaPenerus_IND_negara.Value = Nothing
    End Sub

    Protected Sub RMSwiftInPenerimaPenerus_IND_Pekerjaan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMSwiftInPenerimaPenerus_IND_Pekerjaan.Click
        TxtSwiftInPenerimaPenerus_IND_pekerjaan.Text = ""
        hfSwiftInPenerimaPenerus_IND_pekerjaan.Value = Nothing
    End Sub

    Protected Sub RMSwiftInPenerimaPenerus_IND_KotaDom_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMSwiftInPenerimaPenerus_IND_KotaDom.Click
        TxtSwiftInPenerimaPenerus_IND_kotaDom.Text = ""
        hfSwiftInPenerimaPenerus_IND_kotaDom.Value = Nothing
    End Sub

    Protected Sub RMSwiftInPenerimaPenerus_IND_ProvinsiDom_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMSwiftInPenerimaPenerus_IND_ProvinsiDom.Click
        TxtSwiftInPenerimaPenerus_IND_ProvDom.Text = ""
        hfSwiftInPenerimaPenerus_IND_ProvDom.Value = Nothing
    End Sub

    Protected Sub RMSwiftInPenerimaPenerus_IND_KotaIden_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMSwiftInPenerimaPenerus_IND_KotaIden.Click
        TxtSwiftInPenerimaPenerus_IND_kotaIden.Text = ""
        hfSwiftInPenerimaPenerus_IND_kotaIden.Value = Nothing
    End Sub

    Protected Sub RMSwiftInPenerimaPenerus_IND_ProvinsiIden_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMSwiftInPenerimaPenerus_IND_ProvinsiIden.Click
        TxtSwiftInPenerimaPenerus_IND_ProvIden.Text = ""
        hfSwiftInPenerimaPenerus_IND_ProvIden.Value = Nothing
    End Sub

    Protected Sub RMSwiftInPenerimaPenerus_Korp_bidangUsaha_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMSwiftInPenerimaPenerus_Korp_bidangUsaha.Click
        txtSwiftInPenerimaPenerus_Korp_BidangUsahaKorp.Text = ""
        hfSwiftInPenerimaPenerus_Korp_BidangUsahaKorp.Value = Nothing
    End Sub

    Protected Sub RMSwiftInPenerimaPenerus_Korp_KotaLengkap_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMSwiftInPenerimaPenerus_Korp_KotaLengkap.Click
        txtSwiftInPenerimaPenerus_Korp_kotakorp.Text = ""
        hfSwiftInPenerimaPenerus_Korp_kotakorp.Value = Nothing
    End Sub

    Protected Sub RMSwiftInPenerimaPenerus_Korp_ProvinsiLengkap_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMSwiftInPenerimaPenerus_Korp_ProvinsiLengkap.Click
        txtSwiftInPenerimaPenerus_Korp_ProvinsiKorp.Text = ""
        hfSwiftInPenerimaPenerus_Korp_ProvinsiKorp.Value = Nothing
    End Sub

    Protected Sub RMTransaksi_SwiftInMataUangTransaksi0_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMTransaksi_SwiftInMataUangTransaksi0.Click
        Transaksi_SwiftInMataUangTransaksi0.Text = ""
        hfTransaksi_SwiftInMataUangTransaksi0.Value = Nothing
    End Sub

    Protected Sub RMTransaksi_SwiftIncurrency_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMTransaksi_SwiftIncurrency.Click
        Transaksi_SwiftIncurrency0.Text = ""
        hfTransaksi_SwiftIncurrency.Value = Nothing
    End Sub

    Protected Sub cbo_SwiftInPenerimaPenerus_Korp_BentukBadanUsaha_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_SwiftInPenerimaPenerus_Korp_BentukBadanUsaha.SelectedIndexChanged
        Dim ParameterBidangUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BidangUsaha)
        If cbo_SwiftInPenerimaPenerus_Korp_BentukBadanUsaha.SelectedValue = ParameterBidangUsaha.MsSystemParameter_Value Then
            Me.trSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLainnya.Visible = True
        Else
            Me.trSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLainnya.Visible = True
        End If
    End Sub

    Protected Sub cbo_SwiftInPenerimaNasabah_Korp_BentukBadanUsaha_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_SwiftInPenerimaNasabah_Korp_BentukBadanUsaha.SelectedIndexChanged
        Dim ParameterBadanUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BentukBadanUsaha)
        If cbo_SwiftInPenerimaNasabah_Korp_BentukBadanUsaha.SelectedValue = ParameterBadanUsaha.MsSystemParameter_Value Then
            Me.trSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLainnya.Visible = True
        Else
            Me.trSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLainnya.Visible = False
        End If
    End Sub
End Class
