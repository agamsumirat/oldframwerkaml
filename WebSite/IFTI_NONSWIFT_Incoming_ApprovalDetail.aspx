﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="IFTI_NONSWIFT_Incoming_ApprovalDetail.aspx.vb" Inherits="IFTI_NONSWIFT_Incoming_ApprovalDetail" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
    <div id="divcontent" class="divcontent">
        <table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
            <tr>
                <td bgcolor="White">
                    <ajax:AjaxPanel ID="AjaxPanel1" runat="server" Width="100%">
                        <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                            <asp:View ID="ViewSwiftInAppDet" runat="server">
                                <table id="TblSearchSWIFTIncoming" border="2" bordercolor="#ffffff" cellpadding="2"
                                    cellspacing="1" style="border-top-style: none; border-right-style: none; border-left-style: none;
                                    border-bottom-style: none" width="100%">
                                    <tr>
                                        <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                                            border-right-style: none; border-left-style: none; border-bottom-style: none">
                                            <img src="Images/dot_title.gif" width="17" height="17" />
                                            <strong>
                                                <asp:Label ID="Label5" runat="server" Text="IFTI Non Swift Incoming - Approval Detail "></asp:Label>
                                            </strong>
                                            <hr />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ajax:AjaxPanel ID="AjaxPanel2" runat="server" Width="885px" meta:resourcekey="AjaxPanel1Resource1">
                                                <table style="height: 100%">
                                                    <tr>
                                                        <td colspan="3" style="height: 7px">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap style="height: 26px; width: 107px;">
                                                            <asp:Label ID="LabelMsUser_StaffName" runat="server" Text="User"></asp:Label>
                                                        </td>
                                                        <td style="width: 19px;">
                                                            :
                                                        </td>
                                                        <td style="height: 26px; width: 765px;">
                                                            <asp:TextBox ID="txtMsUser_StaffName" runat="server" CssClass="searcheditbox" TabIndex="2"
                                                                Width="308px" Enabled="False" EnableTheming="True"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap style="height: 26px; width: 107px;">
                                                            <asp:Label ID="LabelRequestedDate" runat="server" Text="Requested Date"></asp:Label>
                                                        </td>
                                                        <td style="width: 19px;">
                                                            :
                                                        </td>
                                                        <td style="height: 26px; width: 765px;">
                                                            <asp:TextBox ID="txtRequestDate" runat="server" CssClass="textBox" MaxLength="1000"
                                                                TabIndex="2" Width="308px" ToolTip="RequestedDate" Enabled="False"></asp:TextBox>
                                                            &nbsp;<span defaultbutton="ImgBtnSearch"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ajax:AjaxPanel>
                                        </td>
                                    </tr>
                                </table>
                                <table id="UMUM" align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2"
                                    width="100%" bgcolor="#dddddd" border="2">
                                    <tr>
                                        <td background="Images/search-bar-background.gif" valign="middle" align="left" style="height: 6px;
                                            width: 100%;" width="100%">
                                            <table cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td class="formtext">
                                                        <asp:Label ID="Label25" runat="server" Text="A. Umum" Font-Bold="True"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <a href="#" onclick="javascript:ShowHidePanel('SwiftIN_Umum','Img13','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                            title="click to minimize or maximize">
                                                            <img id="Img13" src="Images/search-bar-minimize.gif" border="0" height="12px" width="12px" /></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr id="SwiftIN_Umum">
                                        <td valign="top" bgcolor="#ffffff" style="height: 125px; width: 100%;" width="100%">
                                            <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                <tr>
                                                    <td class="formtext" align="center" bgcolor="#999999" style="width: 287px">
                                                        <asp:Label ID="Label29" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                    </td>
                                                    <td class="formtext" align="center" bgcolor="#999999" style="width: 367px">
                                                        <asp:Label ID="Label30" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                                    </td>
                                                    <td class="formtext" align="left" bgcolor="#999999" style="width: 383px">
                                                        <asp:Label ID="Label31" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%; background-color: #fff7e6; height: 22px;">
                                                        <asp:Label ID="Label27" runat="server" Text="a. No. LTDLN "></asp:Label><asp:Label
                                                            ID="Label28" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                    <td style="height: 22px; width: 15%;">
                                                        <asp:TextBox ID="SwiftInUmum_LTDNOld" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                    <td style="width: 20%">
                                                        <asp:TextBox ID="SwiftInUmum_LTDNNew" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%; background-color: #fff7e6; height: 22px;">
                                                        <asp:Label ID="Label50" runat="server" Text="b. No. LTDLN Koreksi " Width="168px"></asp:Label></td>
                                                    <td style="width: 15%; height: 22px;">
                                                        <asp:TextBox ID="SwiftInUmum_LtdnKoreksiOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                    <td style="width: 20%">
                                                        <asp:TextBox ID="SwiftInUmum_LtdnKoreksiNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%; background-color: #fff7e6; height: 22px;">
                                                        <asp:Label ID="Label66" runat="server" Text="c. Tanggal Laporan "></asp:Label><asp:Label
                                                            ID="Label67" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                    <td style="width: 15%; height: 22px;">
                                                        <asp:TextBox ID="SwiftInUmum_TanggalLaporanOld" runat="server" CssClass="searcheditbox"
                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 20%">
                                                        <asp:TextBox ID="SwiftInUmum_TanggalLaporanNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%; background-color: #fff7e6; height: 22px;">
                                                        <asp:Label ID="Label68" runat="server" Text="d. Nama PJK Bank Pelapor " Width="171px"></asp:Label>
                                                        <asp:Label ID="Label72" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                    <td style="width: 15%; height: 22px;">
                                                        <asp:TextBox ID="SwiftInUmum_NamaPJKBankOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="250px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 20%">
                                                        <asp:TextBox ID="SwiftInUmum_NamaPJKBankNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="250px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%; background-color: #fff7e6; height: 22px;">
                                                        <asp:Label ID="Label73" runat="server" Text="e. Nama Pejabat PJK Bank Pelapor" Width="207px"></asp:Label>
                                                        <asp:Label ID="Label74" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                    <td style="height: 22px; width: 15%;">
                                                        <asp:TextBox ID="SwiftInUmum_NamaPejabatPJKBankOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                    <td style="width: 20%">
                                                        <asp:TextBox ID="SwiftInUmum_NamaPejabatPJKBankNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%; background-color: #fff7e6; height: 22px;">
                                                        <asp:Label ID="Label75" runat="server" Text="f. Jenis Laporan"></asp:Label>
                                                        <asp:Label ID="Label76" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                    <td style="width: 15%; height: 22px;" align="left">
                                                        <asp:RadioButtonList ID="RbSwiftInUmum_JenisLaporanOld" runat="server" RepeatColumns="2"
                                                            RepeatDirection="Horizontal" Enabled="False">
                                                            <asp:ListItem Value="1">Baru</asp:ListItem>
                                                            <asp:ListItem Value="2">Recall</asp:ListItem>
                                                            <asp:ListItem Value="3">Koreksi</asp:ListItem>
                                                            <asp:ListItem Value="4">Reject</asp:ListItem>
                                                        </asp:RadioButtonList></td>
                                                    <td align="left" style="width: 20%">
                                                        <asp:RadioButtonList ID="RbSwiftInUmum_JenisLaporanNew" runat="server" RepeatColumns="2"
                                                            RepeatDirection="Horizontal" Enabled="False">
                                                            <asp:ListItem Value="1">Baru</asp:ListItem>
                                                            <asp:ListItem Value="2">Recall</asp:ListItem>
                                                            <asp:ListItem Value="3">Koreksi</asp:ListItem>
                                                            <asp:ListItem Value="4">Reject</asp:ListItem>
                                                        </asp:RadioButtonList></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%; height: 22px; background-color: #fff7e6;">
                                                    </td>
                                                    <td style="width: 15%; height: 22px;">
                                                    </td>
                                                    <td style="width: 20%">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <table id="PENGIRIM" align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2"
                                    width="100%" bgcolor="#dddddd" border="2">
                                    <tr>
                                        <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                            style="height: 6px">
                                            <table cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td class="formtext">
                                                        <asp:Label ID="Label10" runat="server" Text="B Identitas Pengirim / Pengirim Asal"
                                                            Font-Bold="True"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <a href="#" onclick="javascript:ShowHidePanel('BSWIFTOutIdentitasPengirim','Img8','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                            title="click to minimize or maximize">
                                                            <img id="Img8" src="Images/search-bar-minimize.gif" border="0" height="12px" width="12px" /></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr id="SWIFTOutIdentitasPengirim">
                                        <td valign="top" bgcolor="#ffffff" style="height: 125px" width="100%">
                                            <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                border-right-style: none; border-left-style: none; border-bottom-style: none">
                                            </table>
                                            <table width="100%">
                                                <tr>
                                                    <td class="formtext" align="center" bgcolor="#999999" style="width: 287px">
                                                        <asp:Label ID="Label32" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                    </td>
                                                    <td class="formtext" align="center" bgcolor="#999999" style="width: 367px">
                                                        <asp:Label ID="Label33" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                                    </td>
                                                    <td class="formtext" align="left" bgcolor="#999999" style="width: 383px">
                                                        <asp:Label ID="Label34" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%; background-color: #fff7e6; height: 27px;">
                                                        <asp:Label ID="Label190" runat="server" Text="No. Rek " Width="45px"></asp:Label>
                                                    </td>
                                                    <td style="width: 15%; height: 27px;" align="left">
                                                        <asp:TextBox ID="TxtNonSWIFTInPengirim_rekeningOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                    <td style="height: 27px; width: 20%;" align="left">
                                                        <asp:TextBox ID="TxtNonSWIFTInPengirim_rekeningNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr id="trSwiftOutPengirimTipePengirim" runat="server">
                                                    <td style="width: 15%; background-color: #fff7e6; height: 27px;">
                                                        <asp:Label ID="Label192" runat="server" Text="       "></asp:Label>
                                                        <asp:Label ID="Label193" runat="server" Text="       Tipe Pengirim"></asp:Label>
                                                        <asp:Label ID="Label194" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                    </td>
                                                    <td style="width: 15%; height: 27px;" align="left">
                                                        <ajax:AjaxPanel ID="AjaxPanel5" runat="server" Height="16px">
                                                            <asp:RadioButtonList ID="RbPengirimNasabah_TipePengirimOld" runat="server" RepeatDirection="Horizontal"
                                                                AutoPostBack="True" Width="195px" Enabled="False">
                                                                <asp:ListItem Value="1">Nasabah</asp:ListItem>
                                                                <asp:ListItem Value="2">Non Nasabah</asp:ListItem>
                                                            </asp:RadioButtonList></ajax:AjaxPanel></td>
                                                    <td style="height: 27px; width: 20%;" align="left">
                                                        <ajax:AjaxPanel ID="AjaxPanel3" runat="server" Height="16px" Enabled="False">
                                                            <asp:RadioButtonList ID="RbPengirimNasabah_TipePengirimNew" runat="server" RepeatDirection="Horizontal"
                                                                AutoPostBack="True" Width="195px">
                                                                <asp:ListItem Value="1">Nasabah</asp:ListItem>
                                                                <asp:ListItem Value="2">Non Nasabah</asp:ListItem>
                                                            </asp:RadioButtonList></ajax:AjaxPanel>
                                                    </td>
                                                </tr>
                                                <tr id="trSwiftOutPengirimTipeNasabah" runat="server" visible="false">
                                                    <td style="width: 15%; height: 27px; background-color: #fff7e6">
                                                        <asp:Label ID="Label9256" runat="server" Text="Tipe Nasabah "></asp:Label>
                                                        <asp:Label ID="Label9257" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                    </td>
                                                    <td style="width: 15%; height: 27px" align="left">
                                                        <ajax:AjaxPanel ID="AjaxPanel779" runat="server">
                                                            <asp:RadioButtonList ID="Rb_IdenPengirimNas_TipeNasabahOld" runat="server" AutoPostBack="True"
                                                                RepeatDirection="Horizontal" Enabled="False">
                                                                <asp:ListItem Value="1">Perorangan</asp:ListItem>
                                                                <asp:ListItem Value="2">Korporasi</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </ajax:AjaxPanel>
                                                    </td>
                                                    <td style="height: 27px; width: 20%;" align="left">
                                                        <ajax:AjaxPanel ID="AjaxPanel4" runat="server">
                                                            <asp:RadioButtonList ID="Rb_IdenPengirimNas_TipeNasabahNew" runat="server" AutoPostBack="True"
                                                                RepeatDirection="Horizontal" Enabled="False">
                                                                <asp:ListItem Value="1">Perorangan</asp:ListItem>
                                                                <asp:ListItem Value="2">Korporasi</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </ajax:AjaxPanel>
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:MultiView ID="MultiViewSWIFTOutIdenPengirim" runat="server" ActiveViewIndex="0">
                                                <asp:View ID="ViewSWIFTOutIdenPengirimNasabah" runat="server">
                                                    <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                        bgcolor="#dddddd" border="2">
                                                        <tr>
                                                            <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                                style="height: 6px">
                                                                <table cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td class="formtext">
                                                                            <asp:Label ID="Label11" runat="server" Text="Nasabah" Font-Bold="True"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <a href="#" onclick="javascript:ShowHidePanel('BSWIFTOutIdentitasPengirim1nasabah','Imgs9','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                                title="click to minimize or maximize">
                                                                                <img id="Imgs9" src="Images/search-bar-minimize.gif" border="0" height="12px" width="12px" /></a>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr id="BSWIFTOutIdentitasPengirim1nasabah">
                                                            <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                </table>
                                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                    <tr>
                                                                        <td style="width: 100%">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 100%">
                                                                            <asp:MultiView ID="MultiViewPengirimNasabah" runat="server" ActiveViewIndex="0">
                                                                                <asp:View ID="ViewPengirimNasabah_IND" runat="server">
                                                                                    <table width="100%">
                                                                                        <tr>
                                                                                            <td style="width: 15%; height: 15px; background-color: #fff7e6">
                                                                                                <asp:Label ID="Label195OLD" runat="server" Text="Perorangan " Font-Bold="True"></asp:Label>
                                                                                            </td>
                                                                                            <td style="height: 15px; width: 15%;">
                                                                                            </td>
                                                                                            <%--<td style="height: 15px; width: 20%;">
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="formtext" align="center" bgcolor="#999999" style="width: 15%">
                                                                                                <asp:Label ID="Label35OLD" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                                                            </td>
                                                                                            <td class="formtext" align="center" bgcolor="#999999" style="width: 15%">
                                                                                                <asp:Label ID="Label36" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                                                                            </td>
                                                                                            <%-- <td class="formtext" align="left" bgcolor="#999999" style="width: 20%">
                                                                                                <asp:Label ID="Label37" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label197OLD" runat="server" Text="a. Nama Lengkap"></asp:Label>
                                                                                                <asp:Label ID="Label125OLD" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                            <td style="width: 15%; height: 15px;">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_namaOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                                            <%-- <td style="width: 20%; height: 15px;" align="left">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_namaNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label198OLD" runat="server" Text="b. Tanggal lahir (tgl/bln/thn)"
                                                                                                    Width="183px"></asp:Label></td>
                                                                                            <td style="width: 15%; height: 15px;">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_TanggalLahirOld" runat="server" CssClass="searcheditbox"
                                                                                                    TabIndex="2" Width="176px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                            <%--<td style="width: 20%; height: 15px;" align="left">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_TanggalLahirNew" runat="server" CssClass="searcheditbox"
                                                                                                    TabIndex="2" Width="172px" Enabled="False"></asp:TextBox>
                                                                                                &nbsp;</td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label200OLD" runat="server" Text="c. Kewarganegaraan (Pilih salah satu) "
                                                                                                    Width="223px"></asp:Label></td>
                                                                                            <td style="width: 15%; height: 15px;" align="left">
                                                                                                <asp:RadioButtonList ID="RbSwiftInPengirimNasabah_IND_WargaNegaraOld" runat="server"
                                                                                                    RepeatDirection="Horizontal" Enabled="False">
                                                                                                    <asp:ListItem Value="1">WNI</asp:ListItem>
                                                                                                    <asp:ListItem Value="2">WNA</asp:ListItem>
                                                                                                </asp:RadioButtonList></td>
                                                                                            <%--<td style="width: 20%; height: 15px;" align="left">
                                                                                                <asp:RadioButtonList ID="RbSwiftInPengirimNasabah_IND_WargaNegaraNew" runat="server"
                                                                                                    RepeatDirection="Horizontal" Enabled="False">
                                                                                                    <asp:ListItem Value="1">WNI</asp:ListItem>
                                                                                                    <asp:ListItem Value="2">WNA</asp:ListItem>
                                                                                                </asp:RadioButtonList>
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label201OLD" runat="server" Text="d. Negara "></asp:Label>
                                                                                                <br />
                                                                                                <asp:Label ID="Label202" runat="server" Font-Italic="True" Text="(Wajib diisi apabila kewarganegaraan WNA)"
                                                                                                    Width="258px"></asp:Label></td>
                                                                                            <td style="width: 15%; height: 15px;" align="left">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_negaraOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hfSwiftInPengirimNasabah_IND_negaraOld" runat="server" />
                                                                                            </td>
                                                                                            <%-- <td style="width: 20%; height: 15px;" align="left">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_negaraNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hfSwiftInPengirimNasabah_IND_negaraNew" runat="server" />
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label203OLD" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                                            <td style="width: 15%; height: 15px;">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_negaraLainOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                            <%--<td style="width: 20%; height: 15px;" align="left">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_negaraLainNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label213OLD" runat="server" Text="e. Alamat Sesuai Bukti Identitas"></asp:Label></td>
                                                                                            <td style="height: 15px; width: 15%;">
                                                                                                &nbsp;</td>
                                                                                            <%-- <td style="width: 20%; height: 15px;" align="left">
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label214OLD" runat="server" Text="Alamat"></asp:Label></td>
                                                                                            <td style="width: 15%; height: 15px;">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_alamatIdenOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                                            <%--<td style="width: 20%; height: 15px;" align="left">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_alamatIdenNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label205OLD" runat="server" Text="Negara Bagian/ Kota"></asp:Label></td>
                                                                                            <td style="width: 15%; height: 15px;" align="left">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_NegaraKotaOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hfSwiftInPengirimNasabah_IND_KotaOld" runat="server" />
                                                                                            </td>
                                                                                            <%--<td style="width: 20%; height: 15px;" align="left">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_NegaraKotaNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hfSwiftInPengirimNasabah_IND_KotaNew" runat="server" />
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; height: 15px; background-color: #fff7e6">
                                                                                                <asp:Label ID="Label204OLD" runat="server" Text="Negara"></asp:Label>
                                                                                                <asp:Label ID="Label9317OLD" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                            </td>
                                                                                            <td style="height: 15px; width: 15%;" align="left">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_negaraIdenOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hfSwiftInPengirimNasabahINDnegaraIdenOld" runat="server" />
                                                                                            </td>
                                                                                            <%-- <td style="width: 20%; height: 15px;" align="left">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_negaraIdenNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hfSwiftInPengirimNasabahINDnegaraIdenNew" runat="server" />
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label206OLD" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                                            <td style="height: 15px; width: 15%;">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_negaraLainIdenOld" runat="server"
                                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                            <%--<td align="left" style="width: 20%; height: 15px">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_negaraLainIdenNew" runat="server"
                                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label7OLD" runat="server" Text="f. No Telp"></asp:Label></td>
                                                                                            <td style="width: 15%; height: 15px;">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_NoTelpOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                            <%--<td style="width: 20%; height: 15px;" align="left">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_NoTelpNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label220OLD" runat="server" Text="g. Jenis Dokument Identitas"></asp:Label>
                                                                                            </td>
                                                                                            <td style="width: 15%; height: 15px;">
                                                                                                <asp:DropDownList ID="CBOSwiftInPengirimNasabah_IND_JenisIdenOld" runat="server"
                                                                                                    Enabled="False">
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                            <%--<td style="width: 20%; height: 15px;" align="left">
                                                                                                <asp:DropDownList ID="CBOSwiftInPengirimNasabah_IND_JenisIdenNew" runat="server"
                                                                                                    Enabled="False">
                                                                                                </asp:DropDownList>
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label221OLD" runat="server" Text="Nomor Identitas"></asp:Label></td>
                                                                                            <td style="height: 15px; width: 15%;">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_NomorIdenOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                            <%--<td style="width: 20%; height: 15px;" align="left">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_NomorIdenNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                            </td>
                                                                                            <td style="height: 15px; width: 15%;">
                                                                                            </td>
                                                                                            <%--<td style="width: 20%; height: 15px;">
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                    </table>
                                                                                </asp:View>
                                                                                <asp:View ID="ViewPengirimNasabah_Korp" runat="server">
                                                                                    <table style="height: 31px" width="100%">
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6;">
                                                                                                <asp:Label ID="Label8OLD" runat="server" Text="Korporasi" Font-Bold="True"></asp:Label>
                                                                                            </td>
                                                                                            <td style="height: 15px; color: #000000; width: 15%;">
                                                                                            </td>
                                                                                            <%--<td style="width: 20%; color: #000000; height: 15px">
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="formtext" align="center" bgcolor="#999999" style="width: 15%">
                                                                                                <asp:Label ID="Label38OLD" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                                                            </td>
                                                                                            <td class="formtext" align="center" bgcolor="#999999" style="width: 15%">
                                                                                                <asp:Label ID="Label39" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                                                                            </td>
                                                                                            <%--<td class="formtext" align="left" bgcolor="#999999" style="width: 20%">
                                                                                                <asp:Label ID="Label40" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6;">
                                                                                                <asp:Label ID="Label12OLD" runat="server" Text="a. Nama Korporasi" Width="128px"></asp:Label>
                                                                                                <asp:Label ID="Label9325OLD" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                            </td>
                                                                                            <td style="width: 15%; color: #000000; height: 15px;">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_Korp_namaKorpOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                            <%--<td style="width: 20%; color: #000000; height: 15px;">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_Korp_namaKorpNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6;">
                                                                                                <asp:Label ID="Label16OLD" runat="server" Text="b. Alamat Korporasi"></asp:Label></td>
                                                                                            <td style="width: 15%; color: #000000; height: 15px;">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_Korp_AlamatLengkapOld" runat="server"
                                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                            <%--<td style="width: 20%; color: #000000; height: 15px;">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_Korp_AlamatLengkapNew" runat="server"
                                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6;">
                                                                                                <asp:Label ID="Label207OLD" runat="server" Text="Negara Bagian/ Kota"></asp:Label></td>
                                                                                            <td style="width: 15%; color: #000000; height: 15px;" align="left">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_Korp_KotaOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hfSwiftInPengirimNasabah_Korp_KotaOld" runat="server" />
                                                                                            </td>
                                                                                            <%--<td style="width: 20%; color: #000000; height: 15px;">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_Korp_KotaNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hfSwiftInPengirimNasabah_Korp_KotaNew" runat="server" />
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6;">
                                                                                                <asp:Label ID="Label208OLD" runat="server" Text="Negara"></asp:Label>
                                                                                                <asp:Label ID="Label9OLD" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                            </td>
                                                                                            <td style="width: 15%; height: 15px; color: #000000;">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_Korp_NegaraOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hfSwiftInPengirimNasabah_Korp_NegaraOld" runat="server" />
                                                                                            </td>
                                                                                            <%-- <td style="width: 20%; color: #000000; height: 15px;">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_Korp_NegaraNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hfSwiftInPengirimNasabah_Korp_NegaraNew" runat="server" />
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6;">
                                                                                                <asp:Label ID="Label209OLD" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                                            <td style="height: 15px; width: 15%; color: #000000;">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_Korp_NegaraLainnyaKorpOld" runat="server"
                                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                            <%-- <td style="width: 20%; color: #000000; height: 15px">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_Korp_NegaraLainnyaKorpNew" runat="server"
                                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6;">
                                                                                                <asp:Label ID="Label17OLD" runat="server" Text="c. No Telp"></asp:Label>
                                                                                            </td>
                                                                                            <td style="height: 15px; width: 15%; color: #000000;">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_Korp_NoTelpOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                            <%--<td style="width: 20%; color: #000000; height: 15px">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_Korp_NoTelpNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>--%>
                                                                                        </tr>
                                                                                    </table>
                                                                                </asp:View>
                                                                            </asp:MultiView>
                                                                        </td>
                                                                        <td style="width: 100%">
                                                                            <asp:MultiView ID="MultiViewPengirimNasabah_new" runat="server" ActiveViewIndex="0">
                                                                                <asp:View ID="ViewPengirimNasabah_IND_new" runat="server">
                                                                                    <table width="100%">
                                                                                        <tr>
                                                                                            <td style="width: 15%; height: 15px; background-color: #fff7e6">
                                                                                                <asp:Label ID="Label52" runat="server" Text="Perorangan " Font-Bold="True"></asp:Label>
                                                                                            </td>
                                                                                            <%-- <td style="height: 15px; width: 15%;">
                                                                                            </td>--%>
                                                                                            <td style="height: 15px; width: 20%;">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="formtext" align="center" bgcolor="#999999" style="width: 15%">
                                                                                                <asp:Label ID="Label55" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                                                            </td>
                                                                                            <%-- <td class="formtext" align="center" bgcolor="#999999" style="width: 15%">
                                                                                                <asp:Label ID="Label57" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                                                                            </td>--%>
                                                                                            <td class="formtext" align="left" bgcolor="#999999" style="width: 20%">
                                                                                                <asp:Label ID="Label70" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label71" runat="server" Text="a. Nama Lengkap"></asp:Label>
                                                                                                <asp:Label ID="Label78" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                            <%-- <td style="width: 15%; height: 15px;">
                                                                                                <asp:TextBox ID="TextBox1" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>--%>
                                                                                            <td style="width: 20%; height: 15px;" align="left">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_namaNew" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                                    TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label81" runat="server" Text="b. Tanggal lahir (tgl/bln/thn)" Width="183px"></asp:Label></td>
                                                                                            <%--<td style="width: 15%; height: 15px;">
                                                                                                <asp:TextBox ID="TextBox3" runat="server" CssClass="searcheditbox"
                                                                                                    TabIndex="2" Width="176px" Enabled="False"></asp:TextBox>
                                                                                            </td>--%>
                                                                                            <td style="width: 20%; height: 15px;" align="left">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_TanggalLahirNew" runat="server" CssClass="searcheditbox" TabIndex="2" Width="172px"
                                                                                                    Enabled="False"></asp:TextBox>
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label82" runat="server" Text="c. Kewarganegaraan (Pilih salah satu) "
                                                                                                    Width="223px"></asp:Label></td>
                                                                                            <%--<td style="width: 15%; height: 15px;" align="left">
                                                                                                <asp:RadioButtonList ID="RadioButtonList1" runat="server"
                                                                                                    RepeatDirection="Horizontal" Enabled="False">
                                                                                                    <asp:ListItem Value="1">WNI</asp:ListItem>
                                                                                                    <asp:ListItem Value="2">WNA</asp:ListItem>
                                                                                                </asp:RadioButtonList></td>--%>
                                                                                            <td style="width: 20%; height: 15px;" align="left">
                                                                                                <asp:RadioButtonList ID="RbSwiftInPengirimNasabah_IND_WargaNegaraNew" runat="server" RepeatDirection="Horizontal"
                                                                                                    Enabled="False">
                                                                                                    <asp:ListItem Value="1">WNI</asp:ListItem>
                                                                                                    <asp:ListItem Value="2">WNA</asp:ListItem>
                                                                                                </asp:RadioButtonList>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label83" runat="server" Text="d. Negara "></asp:Label>
                                                                                                <br />
                                                                                                <asp:Label ID="Label84" runat="server" Font-Italic="True" Text="(Wajib diisi apabila kewarganegaraan WNA)"
                                                                                                    Width="258px"></asp:Label></td>
                                                                                            <%--<td style="width: 15%; height: 15px;" align="left">
                                                                                                <asp:TextBox ID="TextBox5" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="HiddenField1" runat="server" />
                                                                                            </td>--%>
                                                                                            <td style="width: 20%; height: 15px;" align="left">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_negaraNew" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hfSwiftInPengirimNasabah_IND_negaraNew" runat="server" />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label85" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                                            <%--<td style="width: 15%; height: 15px;">
                                                                                                <asp:TextBox ID="TextBox7" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>--%>
                                                                                            <td style="width: 20%; height: 15px;" align="left">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_negaraLainNew" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label86" runat="server" Text="e. Alamat Sesuai Bukti Identitas"></asp:Label></td>
                                                                                            <%--<td style="height: 15px; width: 15%;">
                                                                                                &nbsp;</td>--%>
                                                                                            <td style="width: 20%; height: 15px;" align="left">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label87" runat="server" Text="Alamat"></asp:Label></td>
                                                                                            <%--<td style="width: 15%; height: 15px;">
                                                                                                <asp:TextBox ID="TextBox9" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>--%>
                                                                                            <td style="width: 20%; height: 15px;" align="left">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_alamatIdenNew" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                                    TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label88" runat="server" Text="Negara Bagian/ Kota"></asp:Label></td>
                                                                                            <%--<td style="width: 15%; height: 15px;" align="left">
                                                                                                <asp:TextBox ID="TextBox11" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="HiddenField3" runat="server" />
                                                                                            </td>--%>
                                                                                            <td style="width: 20%; height: 15px;" align="left">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_NegaraKotaNew" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hfSwiftInPengirimNasabah_IND_KotaNew" runat="server" />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; height: 15px; background-color: #fff7e6">
                                                                                                <asp:Label ID="Label89" runat="server" Text="Negara"></asp:Label>
                                                                                                <asp:Label ID="Label91" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                            </td>
                                                                                            <%--<td style="height: 15px; width: 15%;" align="left">
                                                                                                <asp:TextBox ID="TextBox13" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="HiddenField5" runat="server" />
                                                                                            </td>--%>
                                                                                            <td style="width: 20%; height: 15px;" align="left">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_negaraIdenNew" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hfSwiftInPengirimNasabahINDnegaraIdenNew" runat="server" />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label92" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                                            <%--<td style="height: 15px; width: 15%;">
                                                                                                <asp:TextBox ID="TextBox15" runat="server"
                                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>--%>
                                                                                            <td align="left" style="width: 20%; height: 15px">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_negaraLainIdenNew" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label94" runat="server" Text="f. No Telp"></asp:Label></td>
                                                                                            <%--<td style="width: 15%; height: 15px;">
                                                                                                <asp:TextBox ID="TextBox17" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>--%>
                                                                                            <td style="width: 20%; height: 15px;" align="left">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_NoTelpNew" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label95" runat="server" Text="g. Jenis Dokument Identitas"></asp:Label>
                                                                                            </td>
                                                                                            <%--<td style="width: 15%; height: 15px;">
                                                                                                <asp:DropDownList ID="DropDownList1" runat="server"
                                                                                                    Enabled="False">
                                                                                                </asp:DropDownList>
                                                                                            </td>--%>
                                                                                            <td style="width: 20%; height: 15px;" align="left">
                                                                                                <asp:DropDownList ID="CBOSwiftInPengirimNasabah_IND_JenisIdenNew" runat="server" Enabled="False">
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                                <asp:Label ID="Label96" runat="server" Text="Nomor Identitas"></asp:Label></td>
                                                                                            <%--<td style="height: 15px; width: 15%;">
                                                                                                <asp:TextBox ID="TextBox19" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>--%>
                                                                                            <td style="width: 20%; height: 15px;" align="left">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_NomorIdenNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6; height: 15px;">
                                                                                            </td>
                                                                                            <%--  <td style="height: 15px; width: 15%;">
                                                                                            </td>--%>
                                                                                            <td style="width: 20%; height: 15px;">
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </asp:View>
                                                                                <asp:View ID="ViewPengirimNasabah_Korp_new" runat="server">
                                                                                    <table style="height: 31px" width="100%">
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6;">
                                                                                                <asp:Label ID="Label8" runat="server" Text="Korporasi" Font-Bold="True"></asp:Label>
                                                                                            </td>
                                                                                            <%-- <td style="height: 15px; color: #000000; width: 15%;">
                                                                                            </td>--%>
                                                                                            <td style="width: 20%; color: #000000; height: 15px">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="formtext" align="center" bgcolor="#999999" style="width: 15%">
                                                                                                <asp:Label ID="Label38" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                                                            </td>
                                                                                            <%--<td class="formtext" align="center" bgcolor="#999999" style="width: 15%">
                                                                                                <asp:Label ID="Label39" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                                                                            </td>--%>
                                                                                            <td class="formtext" align="left" bgcolor="#999999" style="width: 20%">
                                                                                                <asp:Label ID="Label40" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6;">
                                                                                                <asp:Label ID="Label12" runat="server" Text="a. Nama Korporasi" Width="128px"></asp:Label>
                                                                                                <asp:Label ID="Label9325" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                            </td>
                                                                                            <%-- <td style="width: 15%; color: #000000; height: 15px;">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_Korp_namaKorpOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>--%>
                                                                                            <td style="width: 20%; color: #000000; height: 15px;">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_Korp_namaKorpNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6;">
                                                                                                <asp:Label ID="Label16" runat="server" Text="b. Alamat Korporasi"></asp:Label></td>
                                                                                            <%--<td style="width: 15%; color: #000000; height: 15px;">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_Korp_AlamatLengkapOld" runat="server"
                                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                                            </td>--%>
                                                                                            <td style="width: 20%; color: #000000; height: 15px;">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_Korp_AlamatLengkapNew" runat="server"
                                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6;">
                                                                                                <asp:Label ID="Label207" runat="server" Text="Negara Bagian/ Kota"></asp:Label></td>
                                                                                            <%--<td style="width: 15%; color: #000000; height: 15px;" align="left">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_Korp_KotaOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hfSwiftInPengirimNasabah_Korp_KotaOld" runat="server" />
                                                                                            </td>--%>
                                                                                            <td style="width: 20%; color: #000000; height: 15px;">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_Korp_KotaNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hfSwiftInPengirimNasabah_Korp_KotaNew" runat="server" />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6;">
                                                                                                <asp:Label ID="Label208" runat="server" Text="Negara"></asp:Label>
                                                                                                <asp:Label ID="Label9" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                            </td>
                                                                                            <%--<td style="width: 15%; height: 15px; color: #000000;">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_Korp_NegaraOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hfSwiftInPengirimNasabah_Korp_NegaraOld" runat="server" />
                                                                                            </td>--%>
                                                                                            <td style="width: 20%; color: #000000; height: 15px;">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_Korp_NegaraNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hfSwiftInPengirimNasabah_Korp_NegaraNew" runat="server" />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6;">
                                                                                                <asp:Label ID="Label209" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                                            <%--<td style="height: 15px; width: 15%; color: #000000;">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_Korp_NegaraLainnyaKorpOld" runat="server"
                                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>--%>
                                                                                            <td style="width: 20%; color: #000000; height: 15px">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_Korp_NegaraLainnyaKorpNew" runat="server"
                                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 15%; background-color: #fff7e6;">
                                                                                                <asp:Label ID="Label17" runat="server" Text="c. No Telp"></asp:Label>
                                                                                            </td>
                                                                                            <%--<td style="height: 15px; width: 15%; color: #000000;">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_Korp_NoTelpOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>--%>
                                                                                            <td style="width: 20%; color: #000000; height: 15px">
                                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_Korp_NoTelpNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </asp:View>
                                                                            </asp:MultiView>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:View>
                                                <asp:View ID="ViewSWIFTOutIdenPengirimNonNasabah" runat="server">
                                                    <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                        bgcolor="#dddddd" border="2">
                                                        <tr>
                                                            <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                                style="height: 6px">
                                                                <table cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td class="formtext">
                                                                            <asp:Label ID="Label18" runat="server" Text="Non Nasabah" Font-Bold="True"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <a href="#" onclick="javascript:ShowHidePanel('BSwiftInIdentitasPengirimNonNasabah','Imgs10','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                                title="click to minimize or maximize">
                                                                                <img id="Imgs10" src="Images/search-bar-minimize.gif" border="0" height="12" width="12" /></a>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr id="BSwiftInIdentitasPengirimNonNasabah">
                                                            <td valign="top" bgcolor="#ffffff" style="height: 125px" width="100%">
                                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                </table>
                                                                <table style="height: 49px" width="100%">
                                                                    <tr>
                                                                        <td class="formtext" align="center" bgcolor="#999999" style="width: 15%">
                                                                            <asp:Label ID="Label41" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                                        </td>
                                                                        <td class="formtext" align="center" bgcolor="#999999" style="width: 15%">
                                                                            <asp:Label ID="Label42" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                                                        </td>
                                                                        <td class="formtext" align="left" bgcolor="#999999" style="width: 20%">
                                                                            <asp:Label ID="Label43" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 15%; background-color: #fff7e6">
                                                                            <asp:Label ID="Label163" runat="server" Text="a. No. Rek "></asp:Label>
                                                                        </td>
                                                                        <td style="width: 15%">
                                                                            <asp:TextBox ID="txtSwiftInPengirimNonNasabah_rekeningOld" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                        <td style="width: 20%" align="left">
                                                                            <asp:TextBox ID="txtSwiftInPengirimNonNasabah_rekeningNew" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 15%; background-color: #fff7e6">
                                                                            <asp:Label ID="Label9272" runat="server" Text="b. Nama Bank"></asp:Label>
                                                                            <asp:Label ID="Label9326" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 15%">
                                                                            <asp:TextBox ID="txtSwiftInPengirimNonNasabah_namabankOld" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                        <td style="width: 20%" align="left">
                                                                            <asp:TextBox ID="txtSwiftInPengirimNonNasabah_namabankNew" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 15%; background-color: #fff7e6">
                                                                            <asp:Label ID="Label1146" runat="server" Text="c. Nama Lengkap"></asp:Label>
                                                                            <asp:Label ID="Label1147" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                        <td style="width: 15%">
                                                                            <asp:TextBox ID="txtSwiftInPengirimNonNasabah_NamaOld" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                        <td style="width: 20%" align="left">
                                                                            <asp:TextBox ID="txtSwiftInPengirimNonNasabah_NamaNew" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 15%; background-color: #fff7e6">
                                                                            <asp:Label ID="Label210" runat="server" Text="d. TanggalLahir" Height="14px" Width="242px"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 15%">
                                                                            <asp:TextBox ID="txtSwiftInPengirimNonNasabah_TanggalLahirOld" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                        <td style="width: 20%" align="left">
                                                                            <asp:TextBox ID="txtSwiftInPengirimNonNasabah_TanggalLahirNew" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 15%; background-color: #fff7e6">
                                                                            <asp:Label ID="Label1150" runat="server" Text="e. Alamat"></asp:Label></td>
                                                                        <td style="width: 15%">
                                                                            <asp:TextBox ID="txtSwiftInPengirimNonNasabah_AlamatOld" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="341px" Height="16px" Enabled="False"></asp:TextBox></td>
                                                                        <td style="width: 20%" align="left">
                                                                            <asp:TextBox ID="txtSwiftInPengirimNonNasabah_AlamatNew" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="341px" Height="16px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 15%; background-color: #fff7e6">
                                                                            <asp:Label ID="Label9316" runat="server" Text="Negara Bagian/ Kota"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 15%" align="left">
                                                                            <asp:TextBox ID="txtSwiftInPengirimNonNasabah_KotaOld" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            <asp:HiddenField ID="hfSwiftInPengirimNonNasabah_KotaOld" runat="server" />
                                                                        </td>
                                                                        <td style="width: 20%" align="left">
                                                                            <asp:TextBox ID="txtSwiftInPengirimNonNasabah_KotaNew" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            <asp:HiddenField ID="hfSwiftInPengirimNonNasabah_KotaNew" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 15%; background-color: #fff7e6">
                                                                            <asp:Label ID="Label211" runat="server" Text="Negara"></asp:Label>
                                                                            <asp:Label ID="Label165" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 15%" align="left">
                                                                            <asp:TextBox ID="txtSwiftInPengirimNonNasabah_NegaraOld" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            <asp:HiddenField ID="hfSwiftInPengirimNonNasabah_NegaraOld" runat="server" />
                                                                        </td>
                                                                        <td style="width: 20%" align="left">
                                                                            <asp:TextBox ID="txtSwiftInPengirimNonNasabah_NegaraNew" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            <asp:HiddenField ID="hfSwiftInPengirimNonNasabah_NegaraNew" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 15%; background-color: #fff7e6">
                                                                            <asp:Label ID="Label212" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                        <td style="width: 15%">
                                                                            <asp:TextBox ID="txtSwiftInPengirimNonNasabah_negaraLainOld" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                        <td style="width: 20%" align="left">
                                                                            <asp:TextBox ID="txtSwiftInPengirimNonNasabah_negaraLainNew" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 15%; background-color: #fff7e6">
                                                                            <asp:Label ID="Label9274" runat="server" Text="f. No Telp"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 15%">
                                                                            <asp:TextBox ID="txtSwiftInPengirimNonNasabah_NoTelpOld" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                        <td style="width: 20%" align="left">
                                                                            <asp:TextBox ID="txtSwiftInPengirimNonNasabah_NoTelpNew" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:View>
                                            </asp:MultiView>
                                        </td>
                                    </tr>
                                </table>
                                <table id="PENERIMA" align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2"
                                    width="100%" bgcolor="#dddddd" border="2">
                                    <tr>
                                        <td background="Images/search-bar-background.gif" valign="middle" align="left" style="height: 11px;
                                            width: 100%;" width="100%">
                                            <table cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td class="formtext">
                                                        <asp:Label ID="Label49" runat="server" Text="C. Identitas Penerima" Font-Bold="True"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <a href="#" onclick="javascript:ShowHidePanel('C1SwiftInidentitasPENERIMA','Img1E4','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                            title="click to minimize or maximize">
                                                            <img id="Img1E4" src="Images/search-bar-minimize.gif" border="0" height="12px" width="12px" /></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr id="CSwiftInidentitasPENERIMA">
                                        <td valign="top" bgcolor="#ffffff" style="height: 125px; width: 100%;" width="100%">
                                            <table style="width: 100%">
                                                <tr id="Tr1" runat="server" visible="True">
                                                    <td class="formtext" align="center" bgcolor="#999999" style="width: 15%">
                                                        <asp:Label ID="Label44" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                    </td>
                                                    <td class="formtext" align="center" bgcolor="#999999" style="width: 15%">
                                                        <asp:Label ID="Label45" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                                    </td>
                                                    <td class="formtext" align="left" bgcolor="#999999" style="width: 20%">
                                                        <asp:Label ID="Label46" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr id="trSwiftInTipepengirim" runat="server" visible="True">
                                                    <td style="width: 15%">
                                                        <asp:Label ID="Label53" runat="server" Text="Tipe Penerima"></asp:Label>
                                                    </td>
                                                    <td style="border: thin dashed silver; width: 15%;" align="left">
                                                        <ajax:AjaxPanel ID="AjaxPanel7" runat="server">
                                                            <asp:RadioButtonList ID="RbSwiftInPenerimaNonNasabah_TipePenerimaOld" runat="server"
                                                                AutoPostBack="True" RepeatDirection="Horizontal" Width="189px" Enabled="False">
                                                                <asp:ListItem Value="1">Nasabah</asp:ListItem>
                                                                <asp:ListItem Value="2">Non Nasabah</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </ajax:AjaxPanel>
                                                    </td>
                                                    <td style="width: 20%; border-right: silver thin dashed; border-top: silver thin dashed;
                                                        border-left: silver thin dashed; border-bottom: silver thin dashed;" align="left">
                                                        <ajax:AjaxPanel ID="AjaxPanel6" runat="server">
                                                            <asp:RadioButtonList ID="RbSwiftInPenerimaNonNasabah_TipePenerimaNew" runat="server"
                                                                AutoPostBack="True" RepeatDirection="Horizontal" Width="189px" Enabled="False">
                                                                <asp:ListItem Value="1">Nasabah</asp:ListItem>
                                                                <asp:ListItem Value="2">Non Nasabah</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </ajax:AjaxPanel>
                                                    </td>
                                                </tr>
                                                <tr id="trSwiftInTipeNasabah" runat="server" visible="false">
                                                    <td style="width: 15%">
                                                        <asp:Label ID="Label54" runat="server" Text="Tipe Nasabah "></asp:Label>
                                                    </td>
                                                    <td style="border: thin dashed silver; width: 15%;" align="left">
                                                        <ajax:AjaxPanel ID="AjaxPanel8" runat="server">
                                                            <asp:RadioButtonList ID="RbSwiftInPenerimaNonNasabah_TipeNasabahOld" runat="server"
                                                                AutoPostBack="True" RepeatDirection="Horizontal" Enabled="False">
                                                                <asp:ListItem Value="1">Perorangan</asp:ListItem>
                                                                <asp:ListItem Value="2">Korporasi</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </ajax:AjaxPanel>
                                                    </td>
                                                    <td style="width: 20%; border-right: silver thin dashed; border-top: silver thin dashed;
                                                        border-left: silver thin dashed; border-bottom: silver thin dashed;" align="left">
                                                        <ajax:AjaxPanel ID="AjaxPanel11" runat="server">
                                                            <asp:RadioButtonList ID="RbSwiftInPenerimaNonNasabah_TipeNasabahNew" runat="server"
                                                                AutoPostBack="True" RepeatDirection="Horizontal" Enabled="False">
                                                                <asp:ListItem Value="1">Perorangan</asp:ListItem>
                                                                <asp:ListItem Value="2">Korporasi</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </ajax:AjaxPanel>
                                                    </td>
                                                </tr>
                                            </table>
                                            <ajax:AjaxPanel ID="AjaxPanel9" runat="server" Width="100%">
                                                <asp:MultiView ID="MultiViewSwiftInPenerima" runat="server" ActiveViewIndex="0">
                                                    <asp:View ID="ViewSwiftInPenerimaNasabah" runat="server">
                                                        <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                            bgcolor="#dddddd" border="2" id="TABLE3" onclick="return TABLE1_onclick()">
                                                            <tr>
                                                                <td>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                                    style="height: 6px">
                                                                    <table cellpadding="0" cellspacing="0" border="0">
                                                                        <tr>
                                                                            <td class="formtext" style="height: 14px">
                                                                                <asp:Label ID="Label56" runat="server" Text="Nasabah" Font-Bold="True"></asp:Label>
                                                                            </td>
                                                                            <td style="height: 14px">
                                                                                <a href="#" onclick="javascript:ShowHidePanel('B1sWIFTiNidentitasPENerima1nasabah','Img1e5','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                                    title="click to minimize or maximize">
                                                                                    <img id="Img1e5" src="Images/search-bar-minimize.gif" border="0" height="12" width="12" /></a>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr id="B1sWIFTiNidentitasPENerima1nasabah">
                                                                <td valign="top" bgcolor="#ffffff" style="height: 125px" width="100%">
                                                                    <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                        border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <table style="width: 100%">
                                                                                    <tr>
                                                                                        <td class="formtext" align="center" bgcolor="#999999" style="width: 15%">
                                                                                            <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                                                        </td>
                                                                                        <td class="formtext" align="center" bgcolor="#999999" style="width: 15%">
                                                                                            <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                                                                        </td>
                                                                                        <td class="formtext" align="left" bgcolor="#999999" style="width: 20%">
                                                                                            <asp:Label ID="Label3" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 15%; background-color: #fff7e6">
                                                                                            <asp:Label ID="Label77" runat="server" Text="No. Rek " Width="48px"></asp:Label>
                                                                                            <asp:Label ID="Label79" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 15%">
                                                                                            <asp:TextBox ID="txtSwiftInPenerimaNasabah_IND_RekeningOld" runat="server" CssClass="searcheditbox"
                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                        </td>
                                                                                        <td style="width: 20%">
                                                                                            <asp:TextBox ID="txtSwiftInPenerimaNasabah_IND_RekeningNew" runat="server" CssClass="searcheditbox"
                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="100%">
                                                                                <asp:MultiView ID="MultiViewPenerimaAkhirNasabah" runat="server" ActiveViewIndex="0">
                                                                                    <asp:View ID="VwSwiftIn_PEN_IND" runat="server">
                                                                                        <ajax:AjaxPanel ID="AjaxPanel10" runat="server" Width="100%">
                                                                                            <table style="width: 100%; height: 49px">
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label80" runat="server" Text="Perorangan " Font-Bold="True"></asp:Label>
                                                                                                        <asp:Label ID="Label90" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                    <td style="height: 15px; width: 15%;" align="left">
                                                                                                    </td>
                                                                                                    <%-- <td style="height: 15px; width: 20%;">
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="formtext" align="center" bgcolor="#999999" style="width: 15%">
                                                                                                        <asp:Label ID="Label47" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                                                                    </td>
                                                                                                    <td class="formtext" align="center" bgcolor="#999999" style="width: 15%">
                                                                                                        <asp:Label ID="Label48" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                                                                                    </td>
                                                                                                    <%--<td class="formtext" align="left" bgcolor="#999999" style="width: 20%">
                                                                                                        <asp:Label ID="Label51" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label93" runat="server" Text="a. Nama Lengkap"></asp:Label>
                                                                                                        <asp:Label ID="Label102" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                    <td style="width: 15%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="txtSwiftInPenerimaNasabah_IND_namaOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                    <%-- <td style="width: 20%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="txtSwiftInPenerimaNasabah_IND_namaNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label123" runat="server" Text="b. Tanggal lahir (tgl/bln/thn)" Width="176px"></asp:Label>
                                                                                                        <asp:Label ID="Label137" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                    <td style="width: 15%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="txtSwiftInPenerimaNasabah_IND_TanggalLahirOld" runat="server" CssClass="searcheditbox"
                                                                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <%--<td style="width: 20%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="txtSwiftInPenerimaNasabah_IND_TanggalLahirNew" runat="server" CssClass="searcheditbox"
                                                                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        &nbsp;</td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label138" runat="server" Text="c. Kewarganegaraan (Pilih salah satu) "
                                                                                                            Width="223px"></asp:Label></td>
                                                                                                    <td style="width: 15%; height: 15px;" align="left">
                                                                                                        <asp:RadioButtonList ID="RbSwiftInPenerimaNasabah_IND_KewarganegaraanOld" runat="server"
                                                                                                            RepeatDirection="Horizontal" Enabled="False">
                                                                                                            <asp:ListItem Value="1">WNI</asp:ListItem>
                                                                                                            <asp:ListItem Value="2">WNA</asp:ListItem>
                                                                                                        </asp:RadioButtonList></td>
                                                                                                    <%--<td style="width: 20%; height: 15px;" align="left">
                                                                                                        <asp:RadioButtonList ID="RbSwiftInPenerimaNasabah_IND_KewarganegaraanNew" runat="server"
                                                                                                            RepeatDirection="Horizontal" Enabled="False">
                                                                                                            <asp:ListItem Value="1">WNI</asp:ListItem>
                                                                                                            <asp:ListItem Value="2">WNA</asp:ListItem>
                                                                                                        </asp:RadioButtonList>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label139" runat="server" Text="d. Negara "></asp:Label>
                                                                                                        <br />
                                                                                                        <asp:Label ID="Label142" runat="server" Font-Italic="True" Text="(Wajib diisi apabila kewarganegaraan WNA)"
                                                                                                            Width="254px"></asp:Label></td>
                                                                                                    <td style="width: 15%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="txtSwiftInPenerimaNasabah_IND_negaraOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="hfSwiftInPenerimaNasabah_IND_negaraOld" runat="server" />
                                                                                                    </td>
                                                                                                    <%-- <td style="width: 20%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="txtSwiftInPenerimaNasabah_IND_negaraNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="hfSwiftInPenerimaNasabah_IND_negaraNew" runat="server" />
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label143" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                                                    <td style="width: 15%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="txtSwiftInPenerimaNasabah_IND_negaralainOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                    <%-- <td style="width: 20%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="txtSwiftInPenerimaNasabah_IND_negaralainNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelq144" runat="server" Text="e. Pekerjaan"></asp:Label>
                                                                                                        <asp:Label ID="Labelq145" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                    <td style="width: 15%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="txtSwiftInPenerimaNasabah_IND_pekerjaanOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="hfSwiftInIdenPenerimaNas_Ind_pekerjaanOld" runat="server" />
                                                                                                    </td>
                                                                                                    <%-- <td style="width: 20%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="txtSwiftInPenerimaNasabah_IND_pekerjaanNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="hfSwiftInIdenPenerimaNas_Ind_pekerjaanNew" runat="server" />
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelq45" runat="server" Text="Pekerjaan Lainnya"></asp:Label></td>
                                                                                                    <td style="width: 15%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_PekerjaanlainOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                    <%--<td style="width: 20%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_PekerjaanlainNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelq37" runat="server" Text="f. Alamat Domisili"></asp:Label></td>
                                                                                                    <td style="width: 15%; height: 15px;" align="left">
                                                                                                    </td>
                                                                                                    <%--<td style="width: 20%; height: 15px;" align="left">
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6;">
                                                                                                        <asp:Label ID="Labelq38" runat="server" Text="Alamat"></asp:Label>
                                                                                                    </td>
                                                                                                    <td style="width: 15%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_AlamatOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                                                    <%--<td style="width: 20%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_AlamatNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelq39" runat="server" Text="Kota / Kabupaten "></asp:Label></td>
                                                                                                    <td style="height: 15px; width: 15%;" align="left">
                                                                                                        <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_KotaOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="hfSwiftInIdenPenerimaNas_Ind_Kota_domOld" runat="server" />
                                                                                                    </td>
                                                                                                    <%-- <td style="height: 15px; width: 20%;" align="left">
                                                                                                        <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_KotaNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="hfSwiftInIdenPenerimaNas_Ind_Kota_domNew" runat="server" />
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelq57" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                                                    <td style="width: 15%; height: 15px" align="left">
                                                                                                        <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_kotalainOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                    <%--<td style="width: 20%; height: 15px" align="left">
                                                                                                        <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_kotalainNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelq41" runat="server" Text="Provinsi"></asp:Label></td>
                                                                                                    <td style="width: 15%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_provinsiOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="hfSwiftInIdenPenerimaNas_Ind_provinsi_domOld" runat="server" />
                                                                                                    </td>
                                                                                                    <%-- <td align="left" style="width: 20%; height: 15px">
                                                                                                        <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_provinsiNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="hfSwiftInIdenPenerimaNas_Ind_provinsi_domNew" runat="server" />
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelq186" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                                                    <td style="width: 15%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_provinsiLainOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                    <%--<td style="width: 20%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_provinsiLainNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelq43" runat="server" Text="Alamat"></asp:Label>
                                                                                                        <asp:Label ID="Labelq267" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                    <td style="width: 15%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_alamatIdentitasOld" runat="server"
                                                                                                            CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                                                    <%--<td style="width: 20%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_alamatIdentitasNew" runat="server"
                                                                                                            CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelq44" runat="server" Text="Kota / Kabupaten"></asp:Label>
                                                                                                        <asp:Label ID="Labelq268" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                    <td style="width: 15%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_kotaIdentitasOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="hfSwiftInIdenPenerimaNas_Ind_kotaIdentitasOld" runat="server" />
                                                                                                    </td>
                                                                                                    <%-- <td style="width: 20%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_kotaIdentitasNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="hfSwiftInIdenPenerimaNas_Ind_kotaIdentitasNew" runat="server" />
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labeql83" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                                                    <td style="width: 15%; height: 15px" align="left">
                                                                                                        <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_KotaLainIdenOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                    <%--<td style="width: 20%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_KotaLainIdenNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6;">
                                                                                                        <asp:Label ID="Labeql46" runat="server" Text="Provinsi"></asp:Label>
                                                                                                        <asp:Label ID="Labelq269" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                    <td style="height: 15px; width: 15%;" align="left">
                                                                                                        <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_ProvinsiIdenOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="hfSwiftInIdenPenerimaNas_Ind_ProvinsiIdenOld" runat="server" />
                                                                                                    </td>
                                                                                                    <%-- <td align="left" style="width: 20%; height: 15px">
                                                                                                        <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_ProvinsiIdenNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="hfSwiftInIdenPenerimaNas_Ind_ProvinsiIdenNew" runat="server" />
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelq62" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                                                    <td style="width: 15%; height: 15px" align="left">
                                                                                                        <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_ProvinsilainIdenOld" runat="server"
                                                                                                            CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                    <%-- <td style="width: 20%; height: 15px" align="left">
                                                                                                        <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_ProvinsilainIdenNew" runat="server"
                                                                                                            CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label9307" runat="server" Text="h. No Telp"></asp:Label>
                                                                                                    </td>
                                                                                                    <td style="width: 15%; height: 15px" align="left">
                                                                                                        <asp:TextBox ID="TxtISwiftIndenPenerimaNas_Ind_noTelpOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <%--<td style="width: 20%; height: 15px" align="left">
                                                                                                        <asp:TextBox ID="TxtISwiftIndenPenerimaNas_Ind_noTelpNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelq48" runat="server" Text="i. Jenis Dokument Identitas"></asp:Label>
                                                                                                        <asp:Label ID="Labelq270" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                    <td style="width: 15%; height: 15px;" align="left">
                                                                                                        <asp:DropDownList ID="cbo_SwiftInpenerimaNas_Ind_jenisidentitasOld" runat="server"
                                                                                                            Enabled="False">
                                                                                                        </asp:DropDownList></td>
                                                                                                    <%--<td style="width: 20%; height: 15px;" align="left">
                                                                                                        <asp:DropDownList ID="cbo_SwiftInpenerimaNas_Ind_jenisidentitasNew" runat="server"
                                                                                                            Enabled="False">
                                                                                                        </asp:DropDownList>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6;">
                                                                                                        <asp:Label ID="Labelq49" runat="server" Text="Nomor Identitas"></asp:Label>
                                                                                                        <asp:Label ID="LabelIq271" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                    <td style="height: 15px; width: 15%;" align="left">
                                                                                                        <asp:TextBox ID="TxtISwiftIndenPenerimaNas_Ind_noIdentitasOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                    <%--<td style="width: 20%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="TxtISwiftIndenPenerimaNas_Ind_noIdentitasNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6;">
                                                                                                    </td>
                                                                                                    <td style="height: 15px; width: 15%;" align="left">
                                                                                                    </td>
                                                                                                    <%-- <td style="width: 20%; height: 15px;">
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </ajax:AjaxPanel>
                                                                                    </asp:View>
                                                                                    <asp:View ID="VwSwiftIn_PEN_Corp" runat="server">
                                                                                        <table style="height: 31px" width="100%">
                                                                                            <tr>
                                                                                                <td style="width: 15%; background-color: #fff7e6;">
                                                                                                    <asp:Label ID="Label223" runat="server" Text="Korporasi" Font-Bold="True"></asp:Label>
                                                                                                </td>
                                                                                                <td style="height: 15px; color: #000000; width: 15%;" align="left">
                                                                                                </td>
                                                                                                <%--<td style="width: 20%; color: #000000; height: 15px" align="left">
                                                                                                </td>--%>
                                                                                            </tr>
                                                                                            <tr style="color: #000000">
                                                                                                <td class="formtext" align="center" bgcolor="#999999" style="width: 15%">
                                                                                                    <asp:Label ID="Label58" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                                                                </td>
                                                                                                <td class="formtext" align="center" bgcolor="#999999" style="width: 15%">
                                                                                                    <asp:Label ID="Label59" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                                                                                </td>
                                                                                                <%--<td class="formtext" align="left" bgcolor="#999999" style="width: 20%">
                                                                                                    <asp:Label ID="Label60" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                                                                                </td>--%>
                                                                                            </tr>
                                                                                            <tr style="color: #000000">
                                                                                                <td style="width: 15%; background-color: #fff7e6;">
                                                                                                    <asp:Label ID="Label225" runat="server" Text="a. Bentuk Badan Usaha "></asp:Label></td>
                                                                                                <td style="width: 15%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_BentukBadanUsahaOld" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                <%--<td style="width: 20%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_BentukBadanUsahaNew" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                </td>--%>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 15%; background-color: #fff7e6;">
                                                                                                    <asp:Label ID="Label226" runat="server" Text="Bentuk Badan Usaha Lainnya " Width="167px"></asp:Label></td>
                                                                                                <td style="width: 15%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLainnyaOld" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                <%--<td style="width: 20%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLainnyaNew" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                </td>--%>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 15%; background-color: #fff7e6;">
                                                                                                    <asp:Label ID="Label227" runat="server" Text="b. Nama Korporasi" Width="122px"></asp:Label>
                                                                                                    <asp:Label ID="Label149" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                <td style="width: 15%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_namaKorpOld" runat="server" CssClass="searcheditbox"
                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                <%--<td style="width: 20%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_namaKorpNew" runat="server" CssClass="searcheditbox"
                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                </td>--%>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 15%; background-color: #fff7e6;">
                                                                                                    <asp:Label ID="Label228" runat="server" Text="c. Bidang Usaha Korporasi"></asp:Label>
                                                                                                    <asp:Label ID="Label9308" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                </td>
                                                                                                <td style="width: 15%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_BidangUsahaKorpOld" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    <asp:HiddenField ID="hfSwiftInPenerimaNasabah_Korp_BidangUsahaOld" runat="server" />
                                                                                                </td>
                                                                                                <%--<td style="width: 20%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_BidangUsahaKorpNew" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    <asp:HiddenField ID="hfSwiftInPenerimaNasabah_Korp_BidangUsahaNew" runat="server" />
                                                                                                </td>--%>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="background-color: #fff7e6; width: 15%;">
                                                                                                    <asp:Label ID="Label19" runat="server" Text="Bidang Usaha Korporasi Lainnya" Width="190px"></asp:Label>
                                                                                                </td>
                                                                                                <td style="width: 15%; color: #000000; height: 15px">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_BidangUsahalainKorpOld" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                </td>
                                                                                                <%--<td style="width: 20%; color: #000000; height: 15px">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_BidangUsahalainKorpNew" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                </td>--%>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 15%; background-color: #fff7e6;">
                                                                                                    <asp:Label ID="Label229" runat="server" Text="d. Alamat Lengkap Korporasi"></asp:Label></td>
                                                                                                <td style="width: 15%; color: #000000; height: 15px;" align="left">
                                                                                                </td>
                                                                                                <%--<td style="width: 20%; color: #000000; height: 15px;" align="left">
                                                                                                </td>--%>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 15%; background-color: #fff7e6;">
                                                                                                    <asp:Label ID="Label4" runat="server" Text="Alamat"></asp:Label>
                                                                                                    <asp:Label ID="Label9319" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                </td>
                                                                                                <td style="width: 15%; height: 15px; color: #000000;" align="left">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_AlamatKorpOld" runat="server" CssClass="searcheditbox"
                                                                                                        MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                                                <%--<td style="width: 20%; height: 15px; color: #000000;" align="left">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_AlamatKorpNew" runat="server" CssClass="searcheditbox"
                                                                                                        MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                                                </td>--%>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 15%; background-color: #fff7e6">
                                                                                                    <asp:Label ID="Label6" runat="server" Text="Kota / Kabupaten "></asp:Label>
                                                                                                    <asp:Label ID="Label9321" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                </td>
                                                                                                <td style="height: 15px; width: 15%; color: #000000;" align="left">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_KotakabOld" runat="server" CssClass="searcheditbox"
                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    <asp:HiddenField ID="hfSwiftInPenerimaNasabah_Korp_kotakabOld" runat="server" />
                                                                                                </td>
                                                                                                <%-- <td style="height: 15px; width: 20%; color: #000000;" align="left">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_KotakabNew" runat="server" CssClass="searcheditbox"
                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    <asp:HiddenField ID="hfSwiftInPenerimaNasabah_Korp_kotakabNew" runat="server" />
                                                                                                </td>--%>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 15%; background-color: #fff7e6">
                                                                                                    <asp:Label ID="Label13" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                                                <td style="width: 15%; height: 15px; color: #000000;" align="left">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_kotaLainOld" runat="server" CssClass="searcheditbox"
                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                <%-- <td style="width: 20%; height: 15px; color: #000000;" align="left">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_kotaLainNew" runat="server" CssClass="searcheditbox"
                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                </td>--%>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 15%; background-color: #fff7e6">
                                                                                                    <asp:Label ID="Label14" runat="server" Text="Provinsi"></asp:Label>
                                                                                                    <asp:Label ID="Label9320" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                </td>
                                                                                                <td style="width: 15%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_propinsiOld" runat="server" CssClass="searcheditbox"
                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    <asp:HiddenField ID="hfSwiftInPenerimaNasabah_Korp_propinsiOld" runat="server" />
                                                                                                </td>
                                                                                                <%-- <td align="left" style="width: 20%; color: #000000; height: 15px">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_propinsiNew" runat="server" CssClass="searcheditbox"
                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    <asp:HiddenField ID="hfSwiftInPenerimaNasabah_Korp_propinsiNew" runat="server" />
                                                                                                </td>--%>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 15%; background-color: #fff7e6">
                                                                                                    <asp:Label ID="Label15" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                                                <td style="width: 15%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_propinsilainOld" runat="server" CssClass="searcheditbox"
                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                <%-- <td style="width: 20%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_propinsilainNew" runat="server" CssClass="searcheditbox"
                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                </td>--%>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 15%; background-color: #fff7e6">
                                                                                                    <asp:Label ID="Label9310" runat="server" Text="e. No. Telp"></asp:Label>
                                                                                                </td>
                                                                                                <td style="width: 15%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_NoTelpOld" runat="server" CssClass="searcheditbox"
                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                </td>
                                                                                                <%--<td style="width: 20%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_NoTelpNew" runat="server" CssClass="searcheditbox"
                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                </td>--%>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </asp:View>
                                                                                </asp:MultiView>
                                                                            </td>
                                                                            <td width="100%">
                                                                                <asp:MultiView ID="MultiViewPenerimaAkhirNasabahNew" runat="server" ActiveViewIndex="0">
                                                                                    <asp:View ID="ViewVwSwiftIn_PEN_IND_New" runat="server">
                                                                                        <ajax:AjaxPanel ID="AjaxPanel12" runat="server" Width="100%">
                                                                                            <table style="width: 100%; height: 49px">
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label7_New" runat="server" Text="Perorangan " Font-Bold="True"></asp:Label>
                                                                                                        <asp:Label ID="Label35_New" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                    <%--<td style="height: 15px; width: 15%;" align="left">
                                                                                                    </td>--%>
                                                                                                    <td style="height: 15px; width: 20%;">
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="formtext" align="center" bgcolor="#999999" style="width: 15%">
                                                                                                        <asp:Label ID="Label37_New" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                                                                    </td>
                                                                                                    <%-- <td class="formtext" align="center" bgcolor="#999999" style="width: 15%">
                                                                                                        <asp:Label ID="Label57" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                                                                                    </td>--%>
                                                                                                    <td class="formtext" align="left" bgcolor="#999999" style="width: 20%">
                                                                                                        <asp:Label ID="Label97_New" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label98_New" runat="server" Text="a. Nama Lengkap"></asp:Label>
                                                                                                        <asp:Label ID="Label99_New" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                    <%--<td style="width: 15%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="TextBox1" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>--%>
                                                                                                    <td style="width: 20%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="txtSwiftInPenerimaNasabah_IND_namaNew" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label100_New" runat="server" Text="b. Tanggal lahir (tgl/bln/thn)"
                                                                                                            Width="176px"></asp:Label>
                                                                                                        <asp:Label ID="Label101_New" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                    <%-- <td style="width: 15%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="TextBox5" runat="server" CssClass="searcheditbox" TabIndex="2" Width="125px"
                                                                                                            Enabled="False"></asp:TextBox>
                                                                                                    </td>--%>
                                                                                                    <td style="width: 20%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="txtSwiftInPenerimaNasabah_IND_TanggalLahirNew" runat="server" CssClass="searcheditbox" TabIndex="2"
                                                                                                            Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        &nbsp;</td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label103_New" runat="server" Text="c. Kewarganegaraan (Pilih salah satu) "
                                                                                                            Width="223px"></asp:Label></td>
                                                                                                    <%-- <td style="width: 15%; height: 15px;" align="left">
                                                                                                        <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal"
                                                                                                            Enabled="False">
                                                                                                            <asp:ListItem Value="1">WNI</asp:ListItem>
                                                                                                            <asp:ListItem Value="2">WNA</asp:ListItem>
                                                                                                        </asp:RadioButtonList></td>--%>
                                                                                                    <td style="width: 20%; height: 15px;" align="left">
                                                                                                        <asp:RadioButtonList ID="RbSwiftInPenerimaNasabah_IND_KewarganegaraanNew" runat="server" RepeatDirection="Horizontal"
                                                                                                            Enabled="False">
                                                                                                            <asp:ListItem Value="1">WNI</asp:ListItem>
                                                                                                            <asp:ListItem Value="2">WNA</asp:ListItem>
                                                                                                        </asp:RadioButtonList>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label104_New" runat="server" Text="d. Negara "></asp:Label>
                                                                                                        <br />
                                                                                                        <asp:Label ID="Label105_New" runat="server" Font-Italic="True" Text="(Wajib diisi apabila kewarganegaraan WNA)"
                                                                                                            Width="254px"></asp:Label></td>
                                                                                                    <%--<td style="width: 15%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="TextBox9" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="HiddenField1" runat="server" />
                                                                                                    </td>--%>
                                                                                                    <td style="width: 20%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="txtSwiftInPenerimaNasabah_IND_negaraNew" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="hfSwiftInPenerimaNasabah_IND_negaraNew" runat="server" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label106" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                                                    <%--<td style="width: 15%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="TextBox13" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>--%>
                                                                                                    <td style="width: 20%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="txtSwiftInPenerimaNasabah_IND_negaralainNew" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label107" runat="server" Text="e. Pekerjaan"></asp:Label>
                                                                                                        <asp:Label ID="Label108" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                    <%-- <td style="width: 15%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="TextBox17" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="HiddenField5" runat="server" />
                                                                                                    </td>--%>
                                                                                                    <td style="width: 20%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="txtSwiftInPenerimaNasabah_IND_pekerjaanNew" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="hfSwiftInIdenPenerimaNas_Ind_pekerjaanNew" runat="server" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label109" runat="server" Text="Pekerjaan Lainnya"></asp:Label></td>
                                                                                                    <%--<td style="width: 15%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="TextBox20" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>--%>
                                                                                                    <td style="width: 20%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_PekerjaanlainNew" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label110" runat="server" Text="f. Alamat Domisili"></asp:Label></td>
                                                                                                    <%--<td style="width: 15%; height: 15px;" align="left">
                                                                                                    </td>--%>
                                                                                                    <td style="width: 20%; height: 15px;" align="left">
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6;">
                                                                                                        <asp:Label ID="Label111" runat="server" Text="Alamat"></asp:Label>
                                                                                                    </td>
                                                                                                    <%--<td style="width: 15%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="TextBox22" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                                            TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>--%>
                                                                                                    <td style="width: 20%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_AlamatNew" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                                            TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label112" runat="server" Text="Kota / Kabupaten "></asp:Label></td>
                                                                                                    <%--<td style="height: 15px; width: 15%;" align="left">
                                                                                                        <asp:TextBox ID="TextBox24" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="HiddenField8" runat="server" />
                                                                                                    </td>--%>
                                                                                                    <td style="height: 15px; width: 20%;" align="left">
                                                                                                        <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_KotaNew" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="hfSwiftInIdenPenerimaNas_Ind_Kota_domNew" runat="server" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label113" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                                                    <%-- <td style="width: 15%; height: 15px" align="left">
                                                                                                        <asp:TextBox ID="TextBox26" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>--%>
                                                                                                    <td style="width: 20%; height: 15px" align="left">
                                                                                                        <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_kotalainNew" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label114" runat="server" Text="Provinsi"></asp:Label></td>
                                                                                                    <%--<td style="width: 15%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="TextBox28" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="hfSwiftInIdenPenerimaNas_Ind_provinsi_domOld" runat="server" />
                                                                                                    </td>--%>
                                                                                                    <td align="left" style="width: 20%; height: 15px">
                                                                                                        <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_provinsiNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="hfSwiftInIdenPenerimaNas_Ind_provinsi_domNew" runat="server" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelq186_new" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                                                    <%--<td style="width: 15%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_provinsiLainOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>--%>
                                                                                                    <td style="width: 20%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_provinsiLainNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelq43_new" runat="server" Text="Alamat"></asp:Label>
                                                                                                        <asp:Label ID="Labelq267_new" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                    <%-- <td style="width: 15%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_alamatIdentitasOld" runat="server"
                                                                                                            CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>--%>
                                                                                                    <td style="width: 20%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_alamatIdentitasNew" runat="server"
                                                                                                            CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelq44_new" runat="server" Text="Kota / Kabupaten"></asp:Label>
                                                                                                        <asp:Label ID="Labelq268_new" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                    <%--<td style="width: 15%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_kotaIdentitasOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="hfSwiftInIdenPenerimaNas_Ind_kotaIdentitasOld" runat="server" />
                                                                                                    </td>--%>
                                                                                                    <td style="width: 20%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_kotaIdentitasNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="hfSwiftInIdenPenerimaNas_Ind_kotaIdentitasNew" runat="server" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labeql83_new" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                                                    <%-- <td style="width: 15%; height: 15px" align="left">
                                                                                                        <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_KotaLainIdenOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>--%>
                                                                                                    <td style="width: 20%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_KotaLainIdenNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6;">
                                                                                                        <asp:Label ID="Labeql46_new" runat="server" Text="Provinsi"></asp:Label>
                                                                                                        <asp:Label ID="Labelq269_new" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                    <%--<td style="height: 15px; width: 15%;" align="left">
                                                                                                        <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_ProvinsiIdenOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="hfSwiftInIdenPenerimaNas_Ind_ProvinsiIdenOld" runat="server" />
                                                                                                    </td>--%>
                                                                                                    <td align="left" style="width: 20%; height: 15px">
                                                                                                        <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_ProvinsiIdenNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="hfSwiftInIdenPenerimaNas_Ind_ProvinsiIdenNew" runat="server" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelq62_new" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                                                    <%--<td style="width: 15%; height: 15px" align="left">
                                                                                                        <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_ProvinsilainIdenOld" runat="server"
                                                                                                            CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>--%>
                                                                                                    <td style="width: 20%; height: 15px" align="left">
                                                                                                        <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_ProvinsilainIdenNew" runat="server"
                                                                                                            CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label9307_new" runat="server" Text="h. No Telp"></asp:Label>
                                                                                                    </td>
                                                                                                    <%-- <td style="width: 15%; height: 15px" align="left">
                                                                                                        <asp:TextBox ID="TxtISwiftIndenPenerimaNas_Ind_noTelpOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>--%>
                                                                                                    <td style="width: 20%; height: 15px" align="left">
                                                                                                        <asp:TextBox ID="TxtISwiftIndenPenerimaNas_Ind_noTelpNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelq48_new" runat="server" Text="i. Jenis Dokument Identitas"></asp:Label>
                                                                                                        <asp:Label ID="Labelq270_new" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                    <%-- <td style="width: 15%; height: 15px;" align="left">
                                                                                                        <asp:DropDownList ID="cbo_SwiftInpenerimaNas_Ind_jenisidentitasOld" runat="server"
                                                                                                            Enabled="False">
                                                                                                        </asp:DropDownList></td>--%>
                                                                                                    <td style="width: 20%; height: 15px;" align="left">
                                                                                                        <asp:DropDownList ID="cbo_SwiftInpenerimaNas_Ind_jenisidentitasNew" runat="server"
                                                                                                            Enabled="False">
                                                                                                        </asp:DropDownList>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6;">
                                                                                                        <asp:Label ID="Labelq49_new" runat="server" Text="Nomor Identitas"></asp:Label>
                                                                                                        <asp:Label ID="LabelIq271_new" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                    <%-- <td style="height: 15px; width: 15%;" align="left">
                                                                                                        <asp:TextBox ID="TxtISwiftIndenPenerimaNas_Ind_noIdentitasOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>--%>
                                                                                                    <td style="width: 20%; height: 15px;" align="left">
                                                                                                        <asp:TextBox ID="TxtISwiftIndenPenerimaNas_Ind_noIdentitasNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 15%; background-color: #fff7e6;">
                                                                                                    </td>
                                                                                                    <%-- <td style="height: 15px; width: 15%;" align="left">
                                                                                                    </td>--%>
                                                                                                    <td style="width: 20%; height: 15px;">
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </ajax:AjaxPanel>
                                                                                    </asp:View>
                                                                                    <asp:View ID="VwSwiftIn_PEN_Corp_NEW" runat="server">
                                                                                        <table style="height: 31px" width="100%">
                                                                                            <tr>
                                                                                                <td style="width: 15%; background-color: #fff7e6;">
                                                                                                    <asp:Label ID="Label223_new" runat="server" Text="Korporasi" Font-Bold="True"></asp:Label>
                                                                                                </td>
                                                                                                <%-- <td style="height: 15px; color: #000000; width: 15%;" align="left">
                                                                                                </td>--%>
                                                                                                <td style="width: 20%; color: #000000; height: 15px" align="left">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr style="color: #000000">
                                                                                                <td class="formtext" align="center" bgcolor="#999999" style="width: 15%">
                                                                                                    <asp:Label ID="Label58_new" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                                                                </td>
                                                                                                <%--<td class="formtext" align="center" bgcolor="#999999" style="width: 15%">
                                                                                                    <asp:Label ID="Label59" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                                                                                </td>--%>
                                                                                                <td class="formtext" align="left" bgcolor="#999999" style="width: 20%">
                                                                                                    <asp:Label ID="Label60" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr style="color: #000000">
                                                                                                <td style="width: 15%; background-color: #fff7e6;">
                                                                                                    <asp:Label ID="Label225_new" runat="server" Text="a. Bentuk Badan Usaha "></asp:Label></td>
                                                                                                <%-- <td style="width: 15%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_BentukBadanUsahaOld" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>--%>
                                                                                                <td style="width: 20%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_BentukBadanUsahaNew" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 15%; background-color: #fff7e6;">
                                                                                                    <asp:Label ID="Label226_new" runat="server" Text="Bentuk Badan Usaha Lainnya " Width="167px"></asp:Label></td>
                                                                                                <%-- <td style="width: 15%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLainnyaOld" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>--%>
                                                                                                <td style="width: 20%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLainnyaNew" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 15%; background-color: #fff7e6;">
                                                                                                    <asp:Label ID="Label227_new" runat="server" Text="b. Nama Korporasi" Width="122px"></asp:Label>
                                                                                                    <asp:Label ID="Label149_new" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                <%-- <td style="width: 15%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_namaKorpOld" runat="server" CssClass="searcheditbox"
                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>--%>
                                                                                                <td style="width: 20%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_namaKorpNew" runat="server" CssClass="searcheditbox"
                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 15%; background-color: #fff7e6;">
                                                                                                    <asp:Label ID="Label228_new" runat="server" Text="c. Bidang Usaha Korporasi"></asp:Label>
                                                                                                    <asp:Label ID="Label9308_new" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                </td>
                                                                                                <%-- <td style="width: 15%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_BidangUsahaKorpOld" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    <asp:HiddenField ID="hfSwiftInPenerimaNasabah_Korp_BidangUsahaOld" runat="server" />
                                                                                                </td>--%>
                                                                                                <td style="width: 20%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_BidangUsahaKorpNew" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    <asp:HiddenField ID="hfSwiftInPenerimaNasabah_Korp_BidangUsahaNew" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="background-color: #fff7e6; width: 15%;">
                                                                                                    <asp:Label ID="Label19_new" runat="server" Text="Bidang Usaha Korporasi Lainnya" Width="190px"></asp:Label>
                                                                                                </td>
                                                                                                <%--<td style="width: 15%; color: #000000; height: 15px">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_BidangUsahalainKorpOld" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                </td>--%>
                                                                                                <td style="width: 20%; color: #000000; height: 15px">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_BidangUsahalainKorpNew" runat="server"
                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 15%; background-color: #fff7e6;">
                                                                                                    <asp:Label ID="Label229_new" runat="server" Text="d. Alamat Lengkap Korporasi"></asp:Label></td>
                                                                                                <%--<td style="width: 15%; color: #000000; height: 15px;" align="left">
                                                                                                </td>--%>
                                                                                                <td style="width: 20%; color: #000000; height: 15px;" align="left">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 15%; background-color: #fff7e6;">
                                                                                                    <asp:Label ID="Label4_new" runat="server" Text="Alamat"></asp:Label>
                                                                                                    <asp:Label ID="Label9319_new" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                </td>
                                                                                                <%-- <td style="width: 15%; height: 15px; color: #000000;" align="left">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_AlamatKorpOld" runat="server" CssClass="searcheditbox"
                                                                                                        MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>--%>
                                                                                                <td style="width: 20%; height: 15px; color: #000000;" align="left">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_AlamatKorpNew" runat="server" CssClass="searcheditbox"
                                                                                                        MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 15%; background-color: #fff7e6">
                                                                                                    <asp:Label ID="Label6_new" runat="server" Text="Kota / Kabupaten "></asp:Label>
                                                                                                    <asp:Label ID="Label9321_new" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                </td>
                                                                                                <%--<td style="height: 15px; width: 15%; color: #000000;" align="left">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_KotakabOld" runat="server" CssClass="searcheditbox"
                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    <asp:HiddenField ID="hfSwiftInPenerimaNasabah_Korp_kotakabOld" runat="server" />
                                                                                                </td>--%>
                                                                                                <td style="height: 15px; width: 20%; color: #000000;" align="left">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_KotakabNew" runat="server" CssClass="searcheditbox"
                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    <asp:HiddenField ID="hfSwiftInPenerimaNasabah_Korp_kotakabNew" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 15%; background-color: #fff7e6">
                                                                                                    <asp:Label ID="Label13_new" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                                                <%--<td style="width: 15%; height: 15px; color: #000000;" align="left">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_kotaLainOld" runat="server" CssClass="searcheditbox"
                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>--%>
                                                                                                <td style="width: 20%; height: 15px; color: #000000;" align="left">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_kotaLainNew" runat="server" CssClass="searcheditbox"
                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 15%; background-color: #fff7e6">
                                                                                                    <asp:Label ID="Label14_new" runat="server" Text="Provinsi"></asp:Label>
                                                                                                    <asp:Label ID="Label9320_new" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                </td>
                                                                                                <%--    <td style="width: 15%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_propinsiOld" runat="server" CssClass="searcheditbox"
                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    <asp:HiddenField ID="hfSwiftInPenerimaNasabah_Korp_propinsiOld" runat="server" />
                                                                                                </td>--%>
                                                                                                <td align="left" style="width: 20%; color: #000000; height: 15px">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_propinsiNew" runat="server" CssClass="searcheditbox"
                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    <asp:HiddenField ID="hfSwiftInPenerimaNasabah_Korp_propinsiNew" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 15%; background-color: #fff7e6">
                                                                                                    <asp:Label ID="Label15_new" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                                                <%--<td style="width: 15%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_propinsilainOld" runat="server" CssClass="searcheditbox"
                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>--%>
                                                                                                <td style="width: 20%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_propinsilainNew" runat="server" CssClass="searcheditbox"
                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="width: 15%; background-color: #fff7e6">
                                                                                                    <asp:Label ID="Label9310_new" runat="server" Text="e. No. Telp"></asp:Label>
                                                                                                </td>
                                                                                                <%--<td style="width: 15%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_NoTelpOld" runat="server" CssClass="searcheditbox"
                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                </td>--%>
                                                                                                <td style="width: 20%; color: #000000; height: 15px;" align="left">
                                                                                                    <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_NoTelpNew" runat="server" CssClass="searcheditbox"
                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </asp:View>
                                                                                </asp:MultiView>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:View>
                                                    <asp:View ID="ViewSwiftInPenerimaNonNasabah" runat="server">
                                                        <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                            bgcolor="#dddddd" border="2">
                                                            <tr>
                                                                <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                                    style="height: 7px">
                                                                    <table cellpadding="0" cellspacing="0" border="0">
                                                                        <tr>
                                                                            <td class="formtext" style="height: 14px">
                                                                                <asp:Label ID="LabelSwiftIn4" runat="server" Text="Non Nasabah" Font-Bold="True"></asp:Label>
                                                                            </td>
                                                                            <td style="height: 14px">
                                                                                <a href="#" onclick="javascript:ShowHidePanel('B1SwiftInidentitasPENerimaonNasabah','Imgt23','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                                    title="click to minimize or maximize">
                                                                                    <img id="Imgt23" src="Images/search-bar-minimize.gif" border="0" height="12" width="12" /></a>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr id="B1SwiftInidentitasPENerimaonNasabah">
                                                                <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                                    <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                        border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                    </table>
                                                                    <table style="height: 49px; width: 100%;">
                                                                        <tr>
                                                                            <td class="formtext" align="center" bgcolor="#999999" style="width: 15%">
                                                                                <asp:Label ID="Label61" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                                            </td>
                                                                            <td class="formtext" align="center" bgcolor="#999999" style="width: 15%">
                                                                                <asp:Label ID="Label62" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                                                            </td>
                                                                            <td class="formtext" align="left" bgcolor="#999999" style="width: 20%">
                                                                                <asp:Label ID="Label63" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 15%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label21" runat="server" Text="a. Kode Rahasia"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 15%">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaNonNasabah_KodeRahasaOld" runat="server" CssClass="searcheditbox"
                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                            <td style="width: 20%">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaNonNasabah_KodeRahasaNew" runat="server" CssClass="searcheditbox"
                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 15%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label23" runat="server" Text="b. No. Rek"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 15%">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaNonNasabah_NoRekOld" runat="server" CssClass="searcheditbox"
                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                            <td style="width: 20%">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaNonNasabah_NoRekNew" runat="server" CssClass="searcheditbox"
                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 15%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label26" runat="server" Text="c. Nama Bank"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 15%">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaNonNasabah_namaBankOld" runat="server" CssClass="searcheditbox"
                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                            <td style="width: 20%">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaNonNasabah_namaBankNew" runat="server" CssClass="searcheditbox"
                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 15%; background-color: #fff7e6">
                                                                                <asp:Label ID="Labelq81" runat="server" Text="d. Nama Lengkap"></asp:Label>
                                                                                <asp:Label ID="Labelq85" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                            <td style="width: 15%">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaNonNasabah_namaOld" runat="server" CssClass="searcheditbox"
                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                            <td style="width: 20%">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaNonNasabah_namaNew" runat="server" CssClass="searcheditbox"
                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 15%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn82" runat="server" Text="e. Tanggal lahir (tgl/bln/thn)"
                                                                                    Width="183px"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 15%">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaNonNasabah_TanggalLahirOld" runat="server" CssClass="searcheditbox"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                            <td style="width: 20%">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaNonNasabah_TanggalLahirNew" runat="server" CssClass="searcheditbox"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                &nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 15%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn95" runat="server" Text="f. Alamat Sesuai Bukti Identitas"></asp:Label></td>
                                                                            <td style="width: 15%">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaNonNasabah_alamatidenOld" runat="server" CssClass="searcheditbox"
                                                                                    MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                            <td style="width: 20%">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaNonNasabah_alamatidenNew" runat="server" CssClass="searcheditbox"
                                                                                    MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 15%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn84" runat="server" Text="g. No Telepon"></asp:Label></td>
                                                                            <td style="width: 15%;">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaNonNasabah_NoTeleponOld" runat="server" CssClass="searcheditbox"
                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                            <td style="width: 20%;">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaNonNasabah_NoTeleponNew" runat="server" CssClass="searcheditbox"
                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 15%; background-color: #fff7e6;">
                                                                                <asp:Label ID="LabelSwiftIn100" runat="server" Text="h. Jenis Dokument Identitas"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 15%;">
                                                                                <asp:DropDownList ID="CboSwiftInPenerimaNonNasabah_JenisDokumenOld" runat="server"
                                                                                    Enabled="False">
                                                                                </asp:DropDownList></td>
                                                                            <td style="width: 20%;">
                                                                                <asp:DropDownList ID="CboSwiftInPenerimaNonNasabah_JenisDokumenNew" runat="server"
                                                                                    Enabled="False">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 15%; background-color: #fff7e6;">
                                                                                <asp:Label ID="LabelSwiftIn101" runat="server" Text="Nomor Identitas"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 15%;">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaNonNasabah_NomorIdenOld" runat="server" CssClass="searcheditbox"
                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                            <td style="width: 20%;">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaNonNasabah_NomorIdenNew" runat="server" CssClass="searcheditbox"
                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:View>
                                                </asp:MultiView>
                                            </ajax:AjaxPanel>
                                        </td>
                                    </tr>
                                </table>
                                <table id="TRANSAKSI" align="center" bgcolor="#dddddd" border="2" bordercolor="#ffffff"
                                    cellpadding="2" cellspacing="1" width="99%">
                                    <tr>
                                        <td align="left" background="Images/search-bar-background.gif" style="height: 6px;
                                            width: 100%;" valign="middle">
                                            <table border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td class="formtext">
                                                        <asp:Label ID="Label9275" runat="server" Font-Bold="True" Text="D. Transaksi"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <a href="#" onclick="javascript:ShowHidePanel('ESwiftIntransaksi','Imgrr25','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                            title="click to minimize or maximize">
                                                            <img id="Imgrr25" src="Images/search-bar-minimize.gif" border="0" height="12px" width="12px" />
                                                        </a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:Label ID="Label9276" runat="server" Font-Italic="True" Text="(Diisi oleh PJK Bank  yang bertindak sebagai Penyelenggara Pengirim Asal atau sebagai Penyelenggara Penerus)"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr id="ESwiftIntransaksi">
                                        <td bgcolor="#ffffff" style="height: 125px; width: 100%;" valign="top">
                                            <table border="0" cellpadding="2" cellspacing="1" style="border-top-style: none;
                                                border-right-style: none; border-left-style: none; border-bottom-style: none"
                                                width="100%">
                                            </table>
                                            <table style="width: 100%; height: 49px">
                                                <tr>
                                                    <td class="formtext" align="center" bgcolor="#999999" style="width: 15%">
                                                        <asp:Label ID="Label64" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                    </td>
                                                    <td class="formtext" align="center" bgcolor="#999999" style="width: 15%">
                                                        <asp:Label ID="Label65" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                                    </td>
                                                    <td class="formtext" align="left" bgcolor="#999999" style="width: 20%">
                                                        <asp:Label ID="Label69" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%; height: 18px; background-color: #fff7e6">
                                                        <asp:Label ID="Label9277" runat="server" Text="a. Tanggal Transaksi (tgl/bln/thn) "></asp:Label>
                                                        <asp:Label ID="Label9278" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                    </td>
                                                    <td style="width: 15%">
                                                        <asp:TextBox ID="Transaksi_SwiftIntanggalOld" runat="server" CssClass="searcheditbox"
                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                        &nbsp;</td>
                                                    <td style="width: 15%">
                                                        <asp:TextBox ID="Transaksi_SwiftIntanggalNew" runat="server" CssClass="searcheditbox"
                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="Label9287" runat="server" Text="b. Kantor Cabang Penyelenggara Pengirim Asal"></asp:Label>
                                                    </td>
                                                    <td style="width: 15%">
                                                        <asp:TextBox ID="Transaksi_SwiftInkantorCabangPengirimOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="250px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 15%">
                                                        <asp:TextBox ID="Transaksi_SwiftInkantorCabangPengirimNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="250px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="Label9290" runat="server" Text=" c. Tanggal Transaksi/Mata Uang/Nilai Transaksi"></asp:Label>
                                                    </td>
                                                    <td style="width: 15%">
                                                    </td>
                                                    <td style="width: 15%">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="Label9291" runat="server" Text="Tanggal Transaksi (Value Date)"></asp:Label>
                                                        <asp:Label ID="Label9292" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                    </td>
                                                    <td style="width: 15%">
                                                        <asp:TextBox ID="Transaksi_SwiftInValueTanggalTransaksiOld" runat="server" CssClass="searcheditbox"
                                                            TabIndex="2" Width="122px" Enabled="False"></asp:TextBox>
                                                        &nbsp;</td>
                                                    <td style="width: 15%">
                                                        <asp:TextBox ID="Transaksi_SwiftInValueTanggalTransaksiNew" runat="server" CssClass="searcheditbox"
                                                            TabIndex="2" Width="122px" Enabled="False"></asp:TextBox>
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%; height: 18px; background-color: #fff7e6">
                                                        <asp:Label ID="Label9293" runat="server" Text="Nilai Transaksi (Amount of The EFT)"></asp:Label>
                                                        <asp:Label ID="Label9315" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                    </td>
                                                    <td style="width: 15%">
                                                        <asp:TextBox ID="TxtnilaitransaksiOld" runat="server" CssClass="searcheditbox" TabIndex="2"
                                                            Width="122px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 15%">
                                                        <asp:TextBox ID="TxtnilaitransaksiNew" runat="server" CssClass="searcheditbox" TabIndex="2"
                                                            Width="122px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%; height: 18px; background-color: #fff7e6">
                                                        <asp:Label ID="Label9294" runat="server" Text="Mata Uang Transaksi (currency of the EFT)"></asp:Label>
                                                        <asp:Label ID="Label9295" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                    </td>
                                                    <td style="width: 15%">
                                                        <asp:TextBox ID="Transaksi_SwiftInMataUangTransaksiOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 15%">
                                                        <asp:TextBox ID="Transaksi_SwiftInMataUangTransaksiNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%; height: 18px; background-color: #fff7e6">
                                                        <asp:Label ID="Label20" runat="server" Text="Mata Uang Lainnya"></asp:Label>
                                                        <asp:Label ID="Label22" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                    </td>
                                                    <td style="width: 15%">
                                                        <asp:TextBox ID="Transaksi_SwiftInMataUangTransaksiLainnyaOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 15%">
                                                        <asp:TextBox ID="Transaksi_SwiftInMataUangTransaksiLainnyaNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="Label9296" runat="server" Text="Nilai Transaksi Keuangan yang diterima dalam Rupiah"></asp:Label>
                                                        <asp:Label ID="Label9327" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                    </td>
                                                    <td style="width: 15%">
                                                        <asp:TextBox ID="Transaksi_SwiftInAmountdalamRupiahOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 15%">
                                                        <asp:TextBox ID="Transaksi_SwiftInAmountdalamRupiahNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="Label9297" runat="server" Text="d. Mata uang/Nilai Transaksi Keuangan "></asp:Label>
                                                    </td>
                                                    <td style="width: 15%">
                                                    </td>
                                                    <td style="width: 15%">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="Label9298" runat="server" Text="Mata Uang yang diistruksikan"></asp:Label>
                                                    </td>
                                                    <td style="width: 15%">
                                                        <asp:TextBox ID="Transaksi_SwiftIncurrencyOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 15%">
                                                        <asp:TextBox ID="Transaksi_SwiftIncurrencyNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="Label24" runat="server" Text="Mata Uang Lainnya"></asp:Label>
                                                    </td>
                                                    <td style="width: 15%">
                                                        <asp:TextBox ID="Transaksi_SwiftIncurrencyLainnyaOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 15%">
                                                        <asp:TextBox ID="Transaksi_SwiftIncurrencyLainnyaNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="Label9299" runat="server" Text="Nilai Transaksi Keuangan yang diinstruksikan"></asp:Label>
                                                    </td>
                                                    <td style="width: 15%">
                                                        <asp:TextBox ID="Transaksi_SwiftIninstructedAmountOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 15%">
                                                        <asp:TextBox ID="Transaksi_SwiftIninstructedAmountNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="Label9304" runat="server" Text="e. Tujuan Transaksi "></asp:Label>
                                                    </td>
                                                    <td style="width: 15%">
                                                        <asp:TextBox ID="Transaksi_SwiftInTujuanTransaksiOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 15%">
                                                        <asp:TextBox ID="Transaksi_SwiftInTujuanTransaksiNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="Label9305" runat="server" Text="f. Sumber Penggunaan Dana "></asp:Label>
                                                    </td>
                                                    <td style="width: 15%">
                                                        <asp:TextBox ID="Transaksi_SwiftInSumberPenggunaanDanaOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="250px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 15%">
                                                        <asp:TextBox ID="Transaksi_SwiftInSumberPenggunaanDanaNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="250px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%; background-color: #fff7e6; height: 18px;">
                                                    </td>
                                                    <td style="width: 15%">
                                                    </td>
                                                    <td style="width: 15%">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <table id="FUNCTION BUTTON" width="100%">
                                    <tr>
                                        <td style="width: 387px">
                                        </td>
                                        <td style="width: 122px">
                                            <asp:Button ID="BtnSave" runat="server" Text="Accept" />
                                        </td>
                                        <td style="width: 107px">
                                            <asp:Button ID="BtnReject" runat="server" Text="Reject" />
                                        </td>
                                        <td>
                                            <asp:Button ID="BtnCancel" runat="server" Text="<<Back" />
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </asp:View>
                            <asp:View ID="VwConfirmation" runat="server">
                                <table style="width: 100%" bordercolor="#ffffff" cellspacing="1" cellpadding="2"
                                    width="100%" bgcolor="#dddddd" border="0">
                                    <tr bgcolor="#ffffff">
                                        <td colspan="2" align="center" style="height: 17px">
                                            <asp:Label ID="LblConfirmation" runat="server" meta:resourcekey="LblConfirmationResource1"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr bgcolor="#ffffff">
                                        <td align="center" colspan="2">
                                            <asp:ImageButton ID="ImgBtnAdd" runat="server" ImageUrl="~/images/button/Ok.gif"
                                                CausesValidation="False" meta:resourcekey="ImgBtnAddResource1" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:View>
                        </asp:MultiView>
                    </ajax:AjaxPanel>
                </td>
            </tr>
        </table>
        <ajax:AjaxPanel ID="AjaxPanel14" runat="server" meta:resourcekey="AjaxPanel14Resource1">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" background="Images/button-bground.gif" valign="middle">
                        <img height="1" src="Images/blank.gif" width="5" />
                    </td>
                    <td align="left" background="Images/button-bground.gif" valign="middle">
                        <img height="15" src="images/arrow.gif" width="15" />&nbsp;
                    </td>
                    <td background="Images/button-bground.gif">
                        &nbsp;
                    </td>
                    <td background="Images/button-bground.gif">
                        &nbsp;
                    </td>
                    <td background="Images/button-bground.gif">
                        &nbsp;
                    </td>
                    <td background="Images/button-bground.gif" width="99%">
                        <img height="1" src="Images/blank.gif" width="1" />
                        <asp:CustomValidator ID="CvalHandleErr" runat="server" Display="None" meta:resourcekey="CvalHandleErrResource1"
                            ValidationGroup="handle"></asp:CustomValidator>
                        <asp:CustomValidator ID="CvalPageErr" runat="server" Display="None" meta:resourcekey="CvalPageErrResource1"></asp:CustomValidator>
                    </td>
                    <td>
                        
                    </td>
                </tr>
            </table>
            <asp:CustomValidator ID="CustomValidator1" runat="server" Display="None" meta:resourcekey="CustomValidator1Resource1"
                ValidationGroup="handle"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </asp:CustomValidator>
            <asp:CustomValidator ID="CustomValidator2" runat="server" Display="None" meta:resourcekey="CustomValidator2Resource1"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </asp:CustomValidator>
        </ajax:AjaxPanel>
    </div>
</asp:Content>
