#Region "Imports..."
Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports System.Collections.Generic
Imports AMLbll
Imports AMLbll.ValidateBLL
Imports AMLbll.DataType
Imports Sahassa.AML.Commonly
#End Region
Partial Class IFTI_Detail_NonSwift_Incoming
    Inherits Parent
#Region "properties..."
    ReadOnly Property getIFTIPK() As Integer
        Get
            If Session("IFTIEdit.IFTIPK") = Nothing Then
                Session("IFTIEdit.IFTIPK") = CInt(Request.Params("ID"))
            End If
            Return Session("IFTIEdit.IFTIPK")
        End Get
    End Property
    Private Property SetnGetSenderAccount() As String
        Get
            Return IIf(Session("IFTIEdit.SenderAccount") Is Nothing, "", Session("IFTIEdit.SenderAccount"))
        End Get
        Set(ByVal Value As String)
            Session("IFTIEdit.SenderAccount") = Value
        End Set
    End Property

    'ReadOnly Property getSenderAccount() As Integer
    '    Get
    '        If Session("IFTIEdit.SenderAccount") = Nothing Then
    '            Session("IFTIEdit.SenderAccount") = CInt(Request.Params("ID"))
    '        End If
    '        Return Session("IFTIEdit.SenderAccount")
    '    End Get
    'End Property
#End Region
#Region "Validation"
#Region "Validation Sender"
    Sub ValidasiControl()
        If ObjectAntiNull(NonSwiftInUmum_TanggalLaporan.Text) = False Then Throw New Exception("Tanggal Laporan harus diisi  ")
        If Me.RbNonSwiftInUmum_JenisLaporan.SelectedValue <> "1" And ObjectAntiNull(Me.NonSwiftInUmum_LtdnKoreksi.Text) = False Then Throw New Exception("No. LTDLN Koreksi harus diisi lebih dari 2 karakter!  ")
        If Sahassa.aml.Commonly.IsDateValid("dd-MMM-yyyy", NonSwiftInUmum_TanggalLaporan.Text) = False Then
            Throw New Exception("Tanggal Laporan tidak valid")
        End If
        If ObjectAntiNull(NonSwiftInUmum_NamaPJKBank.Text) = False Or NonSwiftInUmum_NamaPJKBank.Text.Length < 2 Then Throw New Exception("Nama PJK Bank Pelapor harus diisi lebih dari 2 karakter!   ")
        If ObjectAntiNull(NonSwiftInUmum_NamaPejabatPJKBank.Text) = False Or NonSwiftInUmum_NamaPejabatPJKBank.Text.Length < 2 Then Throw New Exception("Nama Pejabat PJK Bank Pelapor harus diisi lebih dari 2 karakter!   ")
        If RbNonSwiftInUmum_JenisLaporan.SelectedIndex = -1 Then Throw New Exception("Jenis Laporan harus dipilih  ")
    End Sub
    Sub validasiSenderIndividu()
        'senderIndividu

        If ObjectAntiNull(txtNonSwiftInPengirimNasabah_IND_Rekening.Text) = False And ObjectAntiNull(TxtNonSwiftInIdenPengirimNas_Ind_alamat.Text) = False Then Throw New Exception(" Pengirim Nasabah Perorangan : No Rekening harus diisi apabila alamat tidak diisi, atau sebaliknya! ")
        If rbrbNonSwiftIn_TipePengirim.SelectedIndex = -1 Then Throw New Exception("Tipe Pengirim harus diisi  ")
        If RB_NonSwiftIn_TipeNasabah.SelectedIndex = -1 Then Throw New Exception("Tipe Nasabah harus diisi  ")

        If ObjectAntiNull(TxtNonSwiftInIdenPengirimNas_Ind_NamaLengkap.Text) = False Or TxtNonSwiftInIdenPengirimNas_Ind_NamaLengkap.Text.Length < 2 Then Throw New Exception("Pengirim Nasabah Perorangan : Nama Lengkap harus diisi lebih dari 2 karakter! ")
        If ObjectAntiNull(TxtNonSwiftInIdenPengirimNas_Ind_Negara.Text) = True And ObjectAntiNull(TxtNonSwiftInIdenPengirimNas_Ind_NegaraLainnya.Text) = True Then Throw New Exception("Pengirim Nasabah Perorangan : Negara kewarganegaraan harus diisi salah satu! ")

        If ObjectAntiNull(TxtNonSwiftInIdenPengirimNas_Ind_NegaraIden.Text) = False And ObjectAntiNull(TxtNonSwiftInIdenPengirimNas_Ind_NegaralainIden.Text) = False Then Throw New Exception("Pengirim Nasabah Perorangan : Negara harus diisi  ")
        If ObjectAntiNull(TxtNonSwiftInIdenPengirimNas_Ind_NegaraIden.Text) = True And ObjectAntiNull(TxtNonSwiftInIdenPengirimNas_Ind_NegaralainIden.Text) = True Then Throw New Exception("Pengirim Nasabah Perorangan : Negara harus diisi salah satu! ")
        If TxtNonSwiftInIdenPengirimNas_Ind_noIdentitas.Text <> "" Then
            If Not Regex.Match(TxtNonSwiftInIdenPengirimNas_Ind_noIdentitas.Text.Trim(), "^[a-zA-Z0-9]*$").Success Then
                Throw New Exception("Nomor Identitas hanya boleh menggunakan huruf dan angka!")
            End If
        End If
    End Sub
    Sub validasiSenderKorporasi()
        'senderKorp
        If ObjectAntiNull(txtNonSwiftInPengirimNasabah_IND_Rekening.Text) = False Then Throw New Exception("Pengirim Nasabah Korporasi : No Rekening harus diisi  ")
        If rbrbNonSwiftIn_TipePengirim.SelectedIndex = -1 Then Throw New Exception("Tipe Pengirim harus diisi  ")
        If RB_NonSwiftIn_TipeNasabah.SelectedIndex = -1 Then Throw New Exception("Tipe Nasabah harus diisi  ")

        If ObjectAntiNull(TxtNonSwiftInIdenPengirimNas_Korporasi_NamaKorporasi.Text) = False Or TxtNonSwiftInIdenPengirimNas_Korporasi_NamaKorporasi.Text.Length < 2 Then Throw New Exception("Pengirim Nasabah Korporasi : Nama Lengkap harus diisi lebih dari 2 karakter! ")
        If ObjectAntiNull(TxtNonSwiftInIdenPengirimNas_Korporasi_Negara.Text) = False And ObjectAntiNull(txtNonSwiftInIdenPengirimNas_Korporasi_NegaraLainnya.Text) = False Then Throw New Exception("Pengirim Nasabah Korporasi : Negara harus diisi  ")
        If ObjectAntiNull(TxtNonSwiftInIdenPengirimNas_Korporasi_Negara.Text) = True And ObjectAntiNull(txtNonSwiftInIdenPengirimNas_Korporasi_NegaraLainnya.Text) = True Then Throw New Exception("Pengirim Nasabah Korporasi : Negara harus diisi salah satu!")
    End Sub
    Sub validasiSenderNonNasabah()
        'senderNOnNasabah
        If RB_NonSwiftIn_TipeNasabah.SelectedIndex = -1 Then Throw New Exception("Tipe Nasabah harus diisi  ")

        'If ObjectAntiNull(TxtNonSwiftInPengirimNonNasabah_NamaBank.Text) = False Then Throw New Exception("Pengirim Non Nasabah : Nama Bank harus diisi  ")
        If ObjectAntiNull(TxtNonSwiftInPengirimNonNasabah_NamaLengkap.Text) = False Or TxtNonSwiftInPengirimNonNasabah_NamaLengkap.Text.Length < 2 Then Throw New Exception("Pengirim Non Nasabah :Nama Lengkap harus diisi lebih dari 2 karakter! ")

        If ObjectAntiNull(TxtNonSwiftInPengirimNonNasabah_Alamat.Text) = True And TxtNonSwiftInPengirimNonNasabah_Alamat.Text.Length < 2 Then Throw New Exception("Pengirim Non Nasabah :Alamat harus diisi lebih dari 2 karakter! ")
        If ObjectAntiNull(TxtNonSwiftInPengirimNonNasabah_Negara.Text) = False And ObjectAntiNull(TxtNonSwiftInPengirimNonNasabah_NegaraLainnya.Text) = False Then Throw New Exception("Pengirim Non Nasabah :Negara harus diisi  ")
        If ObjectAntiNull(TxtNonSwiftInPengirimNonNasabah_Negara.Text) = True And ObjectAntiNull(TxtNonSwiftInPengirimNonNasabah_NegaraLainnya.Text) = True Then Throw New Exception("Pengirim Non Nasabah :Negara harus diisi salah satu saja! ")
    End Sub
#End Region
#Region "Validation Receiver"
    Sub validasiReceiver1()
        'penerimaIndividu
        If Rb_IdenPenerima_TipePenerima.SelectedIndex = -1 Then Throw New Exception("Tipe Pengirim harus diisi  ")
        If Rb_IdenPenerima_TipeNasabah.SelectedIndex = -1 Then Throw New Exception("Tipe Nasabah harus diisi  ")

        If ObjectAntiNull(TxtNonSwiftInPenerimaNasabah_Rekening.Text) = False Then Throw New Exception("Penerima Nasabah perorangan: No Rekening harus diisi  ")

        If ObjectAntiNull(TxtNonSwiftInPenerimaNasabah_IND_nama.Text) = False Or TxtNonSwiftInPenerimaNasabah_IND_nama.Text.Length < 2 Then Throw New Exception("Penerima Nasabah perorangan: Nama Lengkap harus diisi lebih dari 2 karakter! ")
        If ObjectAntiNull(TxtNonSwiftInPenerimaNasabah_IND_negara.Text) = True And ObjectAntiNull(TxtNonSwiftInPenerimaNasabah_IND_negaralain.Text) = True Then Throw New Exception("Penerima Nasabah Perorangan : Negara kewarganegaraan harus diisi salah satu! ")
        If ObjectAntiNull(TxtNonSwiftInPenerimaNasabah_IND_TanggalLahir.Text) = True Then
            If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", TxtNonSwiftInPenerimaNasabah_IND_TanggalLahir.Text) = False Then
                Throw New Exception("Penerima Nasabah perorangan: Tanggal Lahir tidak valid")
            End If
        End If
        If ObjectAntiNull(TxtNonSwiftInPenerimaNasabah_IND_pekerjaan.Text) = False And ObjectAntiNull(TxtNonSwiftInIdenPenerimaNas_Ind_Pekerjaanlain.Text) = False Then Throw New Exception("Penerima Nasabah perorangan: Negara harus diisi  ")
        If ObjectAntiNull(TxtNonSwiftInPenerimaNasabah_IND_pekerjaan.Text) = True And ObjectAntiNull(TxtNonSwiftInIdenPenerimaNas_Ind_Pekerjaanlain.Text) = True Then Throw New Exception("Penerima Nasabah perorangan: Negara harus diisi salah satu saja!  ")
        'dom
        If ObjectAntiNull(TxtNonSwiftInIdenPenerimaNas_Ind_Kota.Text) = True And ObjectAntiNull(TxtNonSwiftInIdenPenerimaNas_Ind_kotalain.Text) = True Then Throw New Exception("Penerima Nasabah perorangan: Kota/Kab Domisili harus diisi salah satu saja ")
        If ObjectAntiNull(TxtNonSwiftInIdenPenerimaNas_Ind_provinsi.Text) = True And ObjectAntiNull(TxtNonSwiftInIdenPenerimaNas_Ind_provinsiLain.Text) = True Then Throw New Exception("Penerima Nasabah perorangan: Provinsi Domisili harus diisi salah satu! ")

        If ObjectAntiNull(TxtNonSwiftInIdenPenerimaNas_Ind_alamatIdentitas.Text) = False Or TxtNonSwiftInIdenPenerimaNas_Ind_alamatIdentitas.Text.Length < 2 Then Throw New Exception("Penerima Nasabah perorangan: Alamat Identitas harus diisi lebih dari 2 karakter! ")
        If ObjectAntiNull(TxtNonSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Text) = False And ObjectAntiNull(TxtNonSwiftInIdenPenerimaNas_Ind_KotaLainIden.Text) = False Then Throw New Exception("Penerima Nasabah perorangan: Kota/Kab harus diisi  ")
        If ObjectAntiNull(TxtNonSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Text) = True And ObjectAntiNull(TxtNonSwiftInIdenPenerimaNas_Ind_KotaLainIden.Text) = True Then Throw New Exception("Penerima Nasabah perorangan: Kota/Kab harus diisi salah satu saja ")
        If ObjectAntiNull(TxtNonSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Text) = False And ObjectAntiNull(TxtNonSwiftInIdenPenerimaNas_Ind_ProvinsilainIden.Text) = False Then Throw New Exception("Penerima Nasabah perorangan: Provinsi harus diisi  ")
        If ObjectAntiNull(TxtNonSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Text) = True And ObjectAntiNull(TxtNonSwiftInIdenPenerimaNas_Ind_ProvinsilainIden.Text) = True Then Throw New Exception("Penerima Nasabah perorangan: Provinsi harus diisi salah satu! ")
        If cbo_NonSwiftInpenerimaNas_Ind_jenisidentitas.SelectedIndex = -1 Then Throw New Exception("Penerima Nasabah perorangan: Jenis Dokumen identitas harus diisi  ")
        If ObjectAntiNull(TxtINonSwiftIndenPenerimaNas_Ind_noIdentitas.Text) = False Then Throw New Exception("Penerima Nasabah perorangan: Nomor Identitas harus diisi  ")
        If TxtINonSwiftIndenPenerimaNas_Ind_noIdentitas.Text <> "" Then
            If Not Regex.Match(TxtINonSwiftIndenPenerimaNas_Ind_noIdentitas.Text.Trim(), "^[a-zA-Z0-9]*$").Success Then
                Throw New Exception("Nomor Identitas hanya boleh menggunakan huruf dan angka!")
            End If
        End If
    End Sub
    Sub validasiReceiver2()
        'pennerimaKorporasi
        If Rb_IdenPenerima_TipePenerima.SelectedIndex = -1 Then Throw New Exception("Tipe Pengirim harus diisi  ")
        If Rb_IdenPenerima_TipeNasabah.SelectedIndex = -1 Then Throw New Exception("Tipe Nasabah harus diisi  ")

        If ObjectAntiNull(TxtNonSwiftInPenerimaNasabah_Rekening.Text) = False Then Throw New Exception("Penerima Nasabah korporasi: No Rekening harus diisi  ")

        If ObjectAntiNull(TxtNonSwiftInPenerimaNasabah_Korp_namaKorp.Text) = False Or TxtNonSwiftInPenerimaNasabah_Korp_namaKorp.Text.Length < 2 Then Throw New Exception("Penerima Nasabah korporasi: Nama Korporasi harus diisi lebih dari 2 karakter! ")
        If ObjectAntiNull(TxtNonSwiftInPenerimaNasabah_Korp_BidangUsahaKorp.Text) = False And ObjectAntiNull(TxtNonSwiftInPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Text) = False Then Throw New Exception("Penerima Nasabah korporasi: Bidang Usaha Korporasi harus diisi  ")
        If ObjectAntiNull(TxtNonSwiftInPenerimaNasabah_Korp_BidangUsahaKorp.Text) = True And ObjectAntiNull(TxtNonSwiftInPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Text) = True Then Throw New Exception("Penerima Nasabah korporasi: Bidang Usaha Korporasi harus diisi salah satu saja!")
        If ObjectAntiNull(TxtNonSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLainnya.Text) = True And cbo_NonSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLain.SelectedIndex > 0 Then Throw New Exception("Penerima Nasabah korporasi: Bentuk Badan Usaha Korporasi harus diisi salah satu saja!")

        If ObjectAntiNull(TxtNonSwiftInPenerimaNasabah_Korp_AlamatKorp.Text) = False Then Throw New Exception("Penerima Nasabah korporasi: Alamat Identitas harus diisi  ")
        If ObjectAntiNull(TxtNonSwiftInPenerimaNasabah_Korp_Kotakab.Text) = False And ObjectAntiNull(TxtNonSwiftInPenerimaNasabah_Korp_kotaLain.Text) = False Then Throw New Exception("Penerima Nasabah korporasi: Kota/Kabupaten harus diisi  ")
        If ObjectAntiNull(TxtNonSwiftInPenerimaNasabah_Korp_Kotakab.Text) = True And ObjectAntiNull(TxtNonSwiftInPenerimaNasabah_Korp_kotaLain.Text) = True Then Throw New Exception("Penerima Nasabah korporasi: Kota/Kabupaten harus diisi salah satu saja! ")
        If ObjectAntiNull(TxtNonSwiftInPenerimaNasabah_Korp_propinsi.Text) = False And ObjectAntiNull(TxtNonSwiftInPenerimaNasabah_Korp_propinsilain.Text) = False Then Throw New Exception("Penerima Nasabah korporasi: Propinsi harus diisi  ")
        If ObjectAntiNull(TxtNonSwiftInPenerimaNasabah_Korp_propinsi.Text) = True And ObjectAntiNull(TxtNonSwiftInPenerimaNasabah_Korp_propinsilain.Text) = True Then Throw New Exception("Penerima Nasabah korporasi: Propinsi harus diisi salah satu saja! ")
    End Sub
    Sub validasiReceiver3()
        'penerimaNOnNasabah
        If Rb_IdenPenerima_TipePenerima.SelectedIndex = -1 Then Throw New Exception("Tipe Pengirim harus diisi  ")
        If TxtNonSwiftInPenerimaNonNasabah_KodeRahasia.Text = "" And TxtNonSwiftInPenerimaNonNasabah_NomorRek.Text = "" Then Throw New Exception("Kode Rahasia atau No Rekening harus diisi salah satu! ")
        If TxtNonSwiftInPenerimaNonNasabah_NomorRek.Text <> "" And TxtNonSwiftInPenerimaNonNasabah_NamaBank.Text = "" Then Throw New Exception("Nama Bank harus diisi apabila no Rekening diisi! ")
        If ObjectAntiNull(TxtNonSwiftInPenerimaNonNasabah_nama.Text) = False Or TxtNonSwiftInPenerimaNonNasabah_nama.Text.Length < 2 Then Throw New Exception("Penerima Non Nasabah: Nama Lengkap harus diisi lebih dari 2 karakter! ")
        If ObjectAntiNull(TxtNonSwiftInPenerimaNonNasabah_TanggalLahir.Text) = True Then
            If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", TxtNonSwiftInPenerimaNonNasabah_TanggalLahir.Text) = False Then
                Throw New Exception("Penerima Non Nasabah: Tanggal Lahir tidak valid")
            End If
        End If
        If TxtNonSwiftInPenerimaNonNasabah_NomorIden.Text <> "" Then
            If Not Regex.Match(TxtNonSwiftInPenerimaNonNasabah_NomorIden.Text.Trim(), "^[a-zA-Z0-9]*$").Success Then
                Throw New Exception("Nomor Identitas hanya boleh menggunakan huruf dan angka!")
            End If
        End If
    End Sub

#End Region
    Sub validasiTransaksi()

        If ObjectAntiNull(TxtTransaksi_NonSwiftIntanggal0.Text) = False Then Throw New Exception("Tanggal Transaksi harus diisi  ")
        If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", TxtTransaksi_NonSwiftIntanggal0.Text) = False Then
            Throw New Exception("Tanggal Transaksi tidak valid")
        End If
        If ObjectAntiNull(TxtTransaksi_NonSwiftInValueTanggalTransaksi.Text) = False Then Throw New Exception("Tanggal Transaksi(value date) harus diisi  ")
        If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", TxtTransaksi_NonSwiftInValueTanggalTransaksi.Text) = False Then
            Throw New Exception("Tanggal Transaksi tidak valid")
        End If
        If ObjectAntiNull(Transaksi_NonSwiftInnilaitransaksi0.Text) = False Then Throw New Exception("Nilai Transaksi harus diisi  ")
        If ObjectAntiNull(Transaksi_NonSwiftInnilaitransaksi0.Text) = True Then
            If Not IsNumeric(Transaksi_NonSwiftInnilaitransaksi0.Text) Then Throw New Exception("Nilai Transaksi harus diisi angka ")
        End If
        If ObjectAntiNull(TxtTransaksi_NonSwiftInMataUangTransaksi.Text) = False And ObjectAntiNull(TxtTransaksi_NonSwiftInMataUangTransaksiLainnya.Text) = False Then Throw New Exception("Mata Uang Transaksi harus diisi! ")
        If ObjectAntiNull(TxtTransaksi_NonSwiftInMataUangTransaksi.Text) = True And ObjectAntiNull(TxtTransaksi_NonSwiftInMataUangTransaksiLainnya.Text) = True Then Throw New Exception("Mata Uang Transaksi harus diisi salah satu! ")
        If ObjectAntiNull(TxtTransaksi_NonSwiftInAmountdalamRupiah.Text) = False Then Throw New Exception("Amount dalam rupiah harus diisi  ")
        If Not ObjectAntiNull(TxtTransaksi_NonSwiftInAmountdalamRupiah.Text) = True Then
            If Not IsValidDecimal(15, 2, TxtTransaksi_NonSwiftInAmountdalamRupiah.Text) Then Throw New Exception("Amount dalam rupiah harus diisi angka ")
        End If
        If ObjectAntiNull(TxtTransaksi_NonSwiftIncurrency.Text) = True And ObjectAntiNull(TxtTransaksi_NonSwiftIncurrencyLainnya.Text) = True Then Throw New Exception("Mata Uang Transaksi harus diisi salah satu! ")
        If Not IsValidDecimal(15, 2, TxtTransaksi_NonSwiftIninstructedAmount.Text) Then Throw New Exception(" Instructed Amount harus diisi angka ")
    End Sub
#End Region
#Region "Save"
    Sub SaveNonSwiftInco()
        Try

            Using OTrans As TransactionManager = New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)

                Try
                    OTrans.BeginTransaction()
                    Page.Validate("handle")
                    If Page.IsValid Then
                        ValidasiControl()

                        ' =========== Insert Header Approval
                        Dim KeyHeaderApproval As Integer
                        Using objIftiApproval As New IFTI_Approval
                            With objIftiApproval
                                FillOrNothing(.FK_MsMode_Id, 2, True, oInt)
                                FillOrNothing(.RequestedBy, SessionPkUserId)
                                FillOrNothing(.RequestedDate, Date.Now)
                                'FillOrNothing(.IsUpload, False)
                            End With
                            DataRepository.IFTI_ApprovalProvider.Save(OTrans, objIftiApproval)
                            KeyHeaderApproval = objIftiApproval.PK_IFTI_Approval_Id
                        End Using

                        '============ Insert Detail Approval
                        Using objIfti_ApprovalDetail As New IFTI_Approval_Detail()
                            Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)

                                Dim senderType As Integer = objIfti.Sender_FK_IFTI_NasabahType_ID
                                With objIfti_ApprovalDetail
                                    'FK
                                    .PK_IFTI_ID_Old = objIfti.PK_IFTI_ID
                                    .PK_IFTI_ID = objIfti.PK_IFTI_ID
                                    .FK_IFTI_Approval_Id = KeyHeaderApproval
                                    .FK_IFTI_Type_ID_Old = objIfti.FK_IFTI_Type_ID
                                    .FK_IFTI_Type_ID = 4
                                    'umum
                                    '--old--
                                    FillOrNothing(.LTDLNNo_Old, objIfti.LTDLNNo)
                                    FillOrNothing(.LTDLNNoKoreksi_Old, objIfti.LTDLNNoKoreksi)
                                    FillOrNothing(.TanggalLaporan_Old, objIfti.TanggalLaporan)
                                    FillOrNothing(.NamaPJKBankPelapor_Old, objIfti.NamaPJKBankPelapor)
                                    FillOrNothing(.NamaPejabatPJKBankPelapor_Old, objIfti.NamaPejabatPJKBankPelapor)
                                    FillOrNothing(.JenisLaporan_Old, objIfti.JenisLaporan)
                                    '--new--
                                    FillOrNothing(.LTDLNNo, Me.NonSwiftInUmum_LTDN, True, Ovarchar)
                                    FillOrNothing(.LTDLNNoKoreksi, Me.NonSwiftInUmum_LtdnKoreksi.Text, True, Ovarchar)
                                    FillOrNothing(.TanggalLaporan, Me.NonSwiftInUmum_TanggalLaporan.Text, True, oDate)
                                    FillOrNothing(.NamaPJKBankPelapor, Me.NonSwiftInUmum_NamaPJKBank.Text, True, Ovarchar)
                                    FillOrNothing(.NamaPejabatPJKBankPelapor, Me.NonSwiftInUmum_NamaPejabatPJKBank.Text, True, Ovarchar)
                                    FillOrNothing(.JenisLaporan, Me.RbNonSwiftInUmum_JenisLaporan.SelectedValue, True, oInt)
                                    'cekPengirim
                                    Select Case (senderType)
                                        Case 1
                                            'pengirimNasabahIndividu
                                            'cekvalidasi
                                            validasiSenderIndividu()
                                            FillOrNothing(.Sender_FK_IFTI_NasabahType_ID_Old, objIfti.Sender_FK_IFTI_NasabahType_ID)
                                            FillOrNothing(.Sender_FK_IFTI_NasabahType_ID, senderType, False, oInt)

                                            FillOrNothing(.Sender_Nasabah_INDV_NoRekening_Old, objIfti.Sender_Nasabah_INDV_NoRekening)
                                            FillOrNothing(.Sender_Nasabah_INDV_NoRekening, Me.txtNonSwiftInPengirimNasabah_IND_Rekening.Text, False, Ovarchar)

                                            FillOrNothing(.Sender_Nasabah_INDV_NamaLengkap_Old, objIfti.Sender_Nasabah_INDV_NamaLengkap, )
                                            FillOrNothing(.Sender_Nasabah_INDV_NamaLengkap, Me.TxtNonSwiftInIdenPengirimNas_Ind_NamaLengkap.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_Nasabah_INDV_TanggalLahir_Old, objIfti.Sender_Nasabah_INDV_TanggalLahir)
                                            FillOrNothing(.Sender_Nasabah_INDV_TanggalLahir, Me.TxtNonSwiftInIdenPengirimNas_Ind_TglLahir.Text, False, oDate)
                                            FillOrNothing(.Sender_Nasabah_INDV_KewargaNegaraan_Old, objIfti.Sender_Nasabah_INDV_KewargaNegaraan)
                                            FillOrNothing(.Sender_Nasabah_INDV_KewargaNegaraan, Me.RbNonSwiftInIdenPengirimNas_Ind_Kewarganegaraan.SelectedValue, False, oInt)
                                            FillOrNothing(.Sender_Nasabah_INDV_Negara_Old, objIfti.Sender_Nasabah_INDV_Negara)
                                            FillOrNothing(.Sender_Nasabah_INDV_NegaraLainnya_Old, objIfti.Sender_Nasabah_INDV_NegaraLainnya)
                                            If Me.RbNonSwiftInIdenPengirimNas_Ind_Kewarganegaraan.SelectedValue = "2" Then
                                                If ObjectAntiNull(TxtNonSwiftInIdenPengirimNas_Ind_Negara.Text) = True Then
                                                    FillOrNothing(.Sender_Nasabah_INDV_Negara, Me.hfNonSwiftInIdenPengirimNas_Ind_Negara.Value, False, oInt)
                                                Else
                                                    FillOrNothing(.Sender_Nasabah_INDV_Negara, Nothing)
                                                End If
                                                FillOrNothing(.Sender_Nasabah_INDV_NegaraLainnya, Me.TxtNonSwiftInIdenPengirimNas_Ind_NegaraLainnya.Text, False, Ovarchar)
                                            End If
                                            FillOrNothing(.Sender_Nasabah_INDV_ID_Alamat_Old, objIfti.Sender_Nasabah_INDV_ID_Alamat)
                                            FillOrNothing(.Sender_Nasabah_INDV_ID_Alamat, Me.TxtNonSwiftInIdenPengirimNas_Ind_alamat.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_Nasabah_INDV_ID_NegaraKota_Old, objIfti.Sender_Nasabah_INDV_ID_NegaraKota)
                                            FillOrNothing(.Sender_Nasabah_INDV_ID_NegaraKota, Me.TxtNonSwiftInIdenPengirimNas_Ind_NegaraBagian.Text, False, oInt)
                                            FillOrNothing(.Sender_Nasabah_INDV_ID_Negara_Old, objIfti.Sender_Nasabah_INDV_ID_Negara)
                                            If ObjectAntiNull(TxtNonSwiftInIdenPengirimNas_Ind_NegaraIden.Text) = True Then
                                                FillOrNothing(.Sender_Nasabah_INDV_ID_Negara, Me.hfNonSwiftInIdenPengirimNas_Ind_NegaraIden.Value, False, oInt)
                                            Else
                                                FillOrNothing(.Sender_Nasabah_INDV_ID_Negara, Nothing)
                                            End If
                                            FillOrNothing(.Sender_Nasabah_INDV_ID_NegaraLainnya_Old, objIfti.Sender_Nasabah_INDV_ID_NegaraLainnya)
                                            FillOrNothing(.Sender_Nasabah_INDV_ID_NegaraLainnya, Me.TxtNonSwiftInIdenPengirimNas_Ind_NegaralainIden.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_Nasabah_INDV_NoTelp_Old, objIfti.Sender_Nasabah_INDV_NoTelp)
                                            FillOrNothing(.Sender_Nasabah_INDV_NoTelp, Me.TxtNonSwiftInIdenPengirimNas_Ind_NoTelp.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_Nasabah_INDV_FK_IFTI_IDType_Old, objIfti.Sender_Nasabah_INDV_FK_IFTI_IDType)
                                            FillOrNothing(.Sender_Nasabah_INDV_FK_IFTI_IDType, Me.cbo_NonSwiftInIdenPengirimNas_Ind_jenisidentitas.SelectedValue, False, oInt)
                                            FillOrNothing(.Sender_Nasabah_INDV_NomorID_Old, objIfti.Sender_Nasabah_INDV_NomorID)
                                            FillOrNothing(.Sender_Nasabah_INDV_NomorID, Me.TxtNonSwiftInIdenPengirimNas_Ind_noIdentitas.Text, False, Ovarchar)

                                        Case 2
                                            'pengirimNasabahKorporasi
                                            'cekvalidasi
                                            validasiSenderKorporasi()
                                            FillOrNothing(.Sender_FK_IFTI_NasabahType_ID_Old, objIfti.Sender_FK_IFTI_NasabahType_ID)
                                            FillOrNothing(.Sender_FK_IFTI_NasabahType_ID, senderType, False, Ovarchar)

                                            FillOrNothing(.Sender_Nasabah_CORP_NoRekening_Old, objIfti.Sender_Nasabah_CORP_NoRekening)
                                            FillOrNothing(.Sender_Nasabah_CORP_NoRekening, Me.txtNonSwiftInPengirimNasabah_IND_Rekening.Text, False, Ovarchar)

                                            FillOrNothing(.Sender_Nasabah_CORP_NamaKorporasi_Old, objIfti.Sender_Nasabah_CORP_NamaKorporasi)
                                            FillOrNothing(.Sender_Nasabah_CORP_NamaKorporasi, Me.TxtNonSwiftInIdenPengirimNas_Korporasi_NamaKorporasi.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_Nasabah_CORP_AlamatLengkap_Old, objIfti.Sender_Nasabah_CORP_AlamatLengkap)
                                            FillOrNothing(.Sender_Nasabah_CORP_AlamatLengkap, Me.TxtNonSwiftInIdenPengirimNas_Korporasi_alamat.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_Nasabah_CORP_NegaraKota_Old, objIfti.Sender_Nasabah_CORP_NegaraKota)
                                            FillOrNothing(.Sender_Nasabah_CORP_NegaraKota, Me.TxtNonSwiftInIdenPengirimNas_Korporasi_NegaraBagian.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_Nasabah_CORP_Negara_Old, objIfti.Sender_Nasabah_CORP_NegaraKota)
                                            If ObjectAntiNull(TxtNonSwiftInPengirimNonNasabah_Negara.Text) = True Then
                                                FillOrNothing(.Sender_Nasabah_CORP_Negara, Me.hfNonSwiftInPengirimNonNasabah_Negara.Value, False, oInt)
                                            Else
                                                FillOrNothing(.Sender_Nasabah_CORP_Negara, Nothing)
                                            End If
                                            FillOrNothing(.Sender_Nasabah_CORP_NegaraLainnya_Old, objIfti.Sender_Nasabah_CORP_NegaraLainnya)
                                            FillOrNothing(.Sender_Nasabah_CORP_NegaraLainnya, Me.txtNonSwiftInIdenPengirimNas_Korporasi_NegaraLainnya.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_Nasabah_CORP_NoTelp_Old, objIfti.Sender_Nasabah_CORP_NoTelp)
                                            FillOrNothing(.Sender_Nasabah_CORP_NoTelp, Me.txtNonSwiftInIdenPengirimNas_Korporasi_NoTelp.Text, False, Ovarchar)
                                        Case 3
                                            'PengirimNonNasabah
                                            'cekvalidasi
                                            validasiSenderNonNasabah()
                                            FillOrNothing(.Sender_FK_IFTI_NasabahType_ID_Old, objIfti.Sender_FK_IFTI_NasabahType_ID)
                                            FillOrNothing(.Sender_FK_IFTI_NasabahType_ID, senderType, False, Ovarchar)

                                            FillOrNothing(.Sender_NonNasabah_NoRekening_Old, objIfti.Sender_NonNasabah_NoRekening)
                                            FillOrNothing(.Sender_NonNasabah_NoRekening, Me.TxtNonSwiftInPengirimNonNasabah_NomorRek.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_NonNasabah_NamaLengkap_Old, objIfti.Sender_NonNasabah_NamaLengkap)
                                            FillOrNothing(.Sender_NonNasabah_NamaLengkap, Me.TxtNonSwiftInPengirimNonNasabah_NamaLengkap.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_NonNasabah_NamaBank_Old, objIfti.Sender_NonNasabah_NamaBank)
                                            FillOrNothing(.Sender_NonNasabah_NamaBank, Me.TxtNonSwiftInPengirimNonNasabah_NamaBank.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_NonNasabah_TanggalLahir_Old, objIfti.Sender_NonNasabah_TanggalLahir)
                                            FillOrNothing(.Sender_NonNasabah_TanggalLahir, Me.TxtNonSwiftInPengirimNonNasabah_TanggalLhr.Text, False, oDate)
                                            FillOrNothing(.Sender_NonNasabah_ID_Alamat_Old, objIfti.Sender_NonNasabah_ID_Alamat)
                                            FillOrNothing(.Sender_NonNasabah_ID_Alamat, Me.TxtNonSwiftInPengirimNonNasabah_Alamat.Text, False, Ovarchar)

                                            FillOrNothing(.Sender_NonNasabah_ID_NegaraBagian_Old, objIfti.Sender_NonNasabah_ID_NegaraBagian)
                                            FillOrNothing(.Sender_NonNasabah_ID_NegaraBagian, Me.TxtNonSwiftInPengirimNonNasabah_Kota.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_NonNasabah_ID_Negara_Old, objIfti.Sender_NonNasabah_ID_Negara)
                                            If ObjectAntiNull(TxtNonSwiftInPengirimNonNasabah_Negara.Text) = True Then
                                                FillOrNothing(.Sender_NonNasabah_ID_Negara, Me.hfNonSwiftInPengirimNonNasabah_Negara.Value, False, oInt)
                                            Else
                                                FillOrNothing(.Sender_NonNasabah_ID_Negara, Nothing)
                                            End If
                                            FillOrNothing(.Sender_NonNasabah_ID_Negara_Old, objIfti.Sender_NonNasabah_ID_Negara)
                                            FillOrNothing(.Sender_NonNasabah_ID_Negara, Me.TxtNonSwiftInPengirimNonNasabah_NegaraLainnya.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_NonNasabah_NoTelp_Old, objIfti.Sender_NonNasabah_NoTelp)
                                            FillOrNothing(.Sender_NonNasabah_NoTelp, Me.TxtNonSwiftInPengirimNonNasabah_NoTelp.Text, False, Ovarchar)

                                    End Select


                                    'cekPenerima
                                    Using objReceiver As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("FK_IFTI_ID=" & getIFTIPK, "", 0, Integer.MaxValue, 0)
                                        Dim KeyBeneficiary As Integer = objReceiver(0).PK_IFTI_Beneficiary_ID
                                        Dim TipePenerima As Integer = objReceiver(0).FK_IFTI_NasabahType_ID
                                        Using objReceiverAppDetail As New IFTI_Approval_Beneficiary
                                            With objReceiverAppDetail

                                                Select Case (TipePenerima)
                                                    Case 1
                                                        validasiReceiver1()
                                                        .FK_IFTI_NasabahType_ID = 1
                                                        .FK_IFTI_ID = objIfti.PK_IFTI_ID
                                                        .FK_IFTI_Approval_Id = KeyHeaderApproval
                                                        .FK_IFTI_Beneficiary_ID = KeyBeneficiary

                                                        FillOrNothing(.Beneficiary_Nasabah_INDV_NoRekening, Me.TxtNonSwiftInPenerimaNasabah_Rekening.Text)

                                                        FillOrNothing(.Beneficiary_Nasabah_INDV_NamaLengkap, Me.TxtNonSwiftInPenerimaNasabah_IND_nama.Text, False, Ovarchar)
                                                        FillOrNothing(.Beneficiary_Nasabah_INDV_TanggalLahir, Me.TxtNonSwiftInPenerimaNasabah_IND_TanggalLahir.Text, False, oDate)
                                                        FillOrNothing(.Beneficiary_Nasabah_INDV_KewargaNegaraan, Me.RbNonSwiftInPenerimaNasabah_IND_Kewarganegaraan.SelectedValue, False, oInt)
                                                        If RbNonSwiftInPenerimaNasabah_IND_Kewarganegaraan.SelectedValue = "2" Then
                                                            If ObjectAntiNull(TxtNonSwiftInPenerimaNasabah_IND_negara.Text) = True Then
                                                                FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Negara, Me.hfNonSwiftInPenerimaNasabah_IND_negara.Value, False, oInt)
                                                            Else
                                                                FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Negara, Nothing)
                                                                FillOrNothing(.Beneficiary_Nasabah_INDV_NegaraLainnya, Me.TxtNonSwiftInPenerimaNasabah_IND_negaralain.Text, False, Ovarchar)

                                                            End If
                                                        End If
                                                        FillOrNothing(.Beneficiary_Nasabah_INDV_Pekerjaan, hfNonSwiftInIdenPenerimaNas_Ind_pekerjaan.Value, False, oInt)
                                                        FillOrNothing(.Beneficiary_Nasabah_INDV_PekerjaanLainnya, Me.TxtNonSwiftInIdenPenerimaNas_Ind_Pekerjaanlain.Text, False, Ovarchar)
                                                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Alamat_Dom, Me.TxtNonSwiftInIdenPenerimaNas_Ind_Alamat.Text, False, Ovarchar)
                                                        If ObjectAntiNull(Me.TxtNonSwiftInIdenPenerimaNas_Ind_Kota.Text) = True Then
                                                            FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKab_Dom, Me.hfNonSwiftInIdenPenerimaNas_Ind_Kota_dom.Value, False, oInt)
                                                        Else
                                                            FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKab_Dom, Nothing)
                                                        End If
                                                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom, Me.TxtNonSwiftInIdenPenerimaNas_Ind_kotalain.Text, False, Ovarchar)
                                                        If ObjectAntiNull(TxtNonSwiftInIdenPenerimaNas_Ind_provinsi.Text) = True Then
                                                            FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Propinsi_Dom, Me.hfNonSwiftInIdenPenerimaNas_Ind_provinsi_dom.Value, False, oInt)
                                                        Else
                                                            FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Propinsi_Dom, Nothing)
                                                        End If
                                                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom, Me.TxtNonSwiftInIdenPenerimaNas_Ind_provinsiLain.Text, False, Ovarchar)
                                                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Alamat_Iden, Me.TxtNonSwiftInIdenPenerimaNas_Ind_alamatIdentitas.Text, False, Ovarchar)
                                                        If ObjectAntiNull(TxtNonSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Text) = True Then
                                                            FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKab_Iden, Me.hfNonSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Value, False, oInt)
                                                        Else
                                                            FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKab_Iden, Nothing)
                                                        End If
                                                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden, Me.TxtNonSwiftInIdenPenerimaNas_Ind_kotalain.Text, False, Ovarchar)
                                                        If ObjectAntiNull(TxtNonSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Text) = True Then
                                                            FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Propinsi_Iden, Me.HfNonSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Value, False, oInt)
                                                        Else
                                                            FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Propinsi_Iden, Nothing)
                                                        End If
                                                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden, Me.TxtNonSwiftInIdenPenerimaNas_Ind_ProvinsilainIden.Text, False, Ovarchar)


                                                        FillOrNothing(.Beneficiary_Nasabah_INDV_NoTelp, Me.TxtINonSwiftIndenPenerimaNas_Ind_noTelp.Text, False, Ovarchar)
                                                        FillOrNothing(.Beneficiary_Nasabah_INDV_FK_IFTI_IDType, Me.cbo_NonSwiftInpenerimaNas_Ind_jenisidentitas.SelectedValue, False, oInt)
                                                        FillOrNothing(.Beneficiary_Nasabah_INDV_NomorID, Me.TxtINonSwiftIndenPenerimaNas_Ind_noIdentitas.Text, False, Ovarchar)
                                                    Case 2
                                                        validasiReceiver2()
                                                        .FK_IFTI_NasabahType_ID = 2
                                                        .FK_IFTI_ID = objIfti.PK_IFTI_ID
                                                        .FK_IFTI_Approval_Id = KeyHeaderApproval
                                                        .FK_IFTI_Beneficiary_ID = KeyBeneficiary
                                                        FillOrNothing(.Beneficiary_Nasabah_CORP_NoRekening, Me.txtNonSwiftInPengirimNasabah_IND_Rekening.Text)

                                                        If cbo_NonSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLain.SelectedIndex > 0 Then
                                                            FillOrNothing(.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id, cbo_NonSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLain.SelectedValue, False, oInt)
                                                        Else
                                                            FillOrNothing(.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id, Nothing)
                                                        End If
                                                        FillOrNothing(.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya, Me.TxtNonSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLainnya.Text, False, Ovarchar)
                                                        FillOrNothing(.Beneficiary_Nasabah_CORP_NamaKorporasi, Me.TxtNonSwiftInPenerimaNasabah_Korp_namaKorp.Text, False, Ovarchar)
                                                        If ObjectAntiNull(TxtNonSwiftInPenerimaNasabah_Korp_BidangUsahaKorp.Text) = True Then
                                                            FillOrNothing(.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id, Me.hfNonSwiftInPenerimaNasabah_Korp_BidangUsahaKorp.Value, False, oInt)
                                                        Else
                                                            FillOrNothing(.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id, Nothing)
                                                        End If
                                                        FillOrNothing(.Beneficiary_Nasabah_CORP_BidangUsahaLainnya, Me.TxtNonSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLainnya.Text, False, Ovarchar)
                                                        FillOrNothing(.Beneficiary_Nasabah_CORP_AlamatLengkap, Me.TxtNonSwiftInPenerimaNasabah_Korp_AlamatKorp.Text, TipePenerima, Ovarchar)
                                                        If ObjectAntiNull(TxtNonSwiftInPenerimaNasabah_Korp_Kotakab.Text) = True Then
                                                            FillOrNothing(.Beneficiary_Nasabah_CORP_ID_KotaKab, Me.hfNonSwiftInPenerimaNasabah_Korp_kotakab.Value, False, oInt)
                                                        Else
                                                            FillOrNothing(.Beneficiary_Nasabah_CORP_ID_KotaKab, Nothing)

                                                        End If
                                                        FillOrNothing(.Beneficiary_Nasabah_CORP_ID_KotaKabLainnya, Me.TxtNonSwiftInPenerimaNasabah_Korp_kotaLain.Text, False, Ovarchar)
                                                        If ObjectAntiNull(TxtNonSwiftInPenerimaNasabah_Korp_propinsi.Text) = True Then
                                                            FillOrNothing(.Beneficiary_Nasabah_CORP_ID_Propinsi, Me.hfNonSwiftInPenerimaNasabah_Korp_propinsi.Value, False, oInt)
                                                        Else
                                                            FillOrNothing(.Beneficiary_Nasabah_CORP_ID_Propinsi, Nothing)
                                                        End If
                                                        FillOrNothing(.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya, Me.TxtNonSwiftInPenerimaNasabah_Korp_propinsilain.Text, False, oInt)
                                                        FillOrNothing(.Beneficiary_Nasabah_CORP_NoTelp, Me.TxtNonSwiftInPenerimaNasabah_Korp_NoTelp.Text.Trim, False, Ovarchar)

                                                    Case 3
                                                        validasiReceiver3()
                                                        .FK_IFTI_NasabahType_ID = 3
                                                        .FK_IFTI_ID = objIfti.PK_IFTI_ID
                                                        .FK_IFTI_Approval_Id = KeyHeaderApproval
                                                        .FK_IFTI_Beneficiary_ID = KeyBeneficiary

                                                        FillOrNothing(.Beneficiary_NonNasabah_KodeRahasia, Me.TxtNonSwiftInPenerimaNonNasabah_KodeRahasia.Text, False, Ovarchar)
                                                        FillOrNothing(.Beneficiary_NonNasabah_NoRekening, Me.TxtNonSwiftInPenerimaNonNasabah_NomorRek.Text, False, Ovarchar)
                                                        FillOrNothing(.Beneficiary_NonNasabah_NamaBank, Me.TxtNonSwiftInPenerimaNonNasabah_NamaBank.Text, False, Ovarchar)
                                                        FillOrNothing(.Beneficiary_NonNasabah_NamaLengkap, Me.TxtNonSwiftInPenerimaNonNasabah_nama.Text, False, Ovarchar)
                                                        FillOrNothing(.Beneficiary_NonNasabah_TanggalLahir, Me.TxtNonSwiftInPenerimaNonNasabah_TanggalLahir.Text, False, oDate)
                                                        FillOrNothing(.Beneficiary_NonNasabah_ID_Alamat, Me.TxtNonSwiftInPenerimaNonNasabah_alamatiden.Text, False, Ovarchar)
                                                        FillOrNothing(.Beneficiary_NonNasabah_NoTelp, Me.TxtNonSwiftInPenerimaNonNasabah_NoTelepon.Text, False, Ovarchar)
                                                        FillOrNothing(.Beneficiary_NonNasabah_FK_IFTI_IDType, Me.CboNonSwiftInPenerimaNonNasabah_JenisDokumen.SelectedValue, False, oInt)
                                                        FillOrNothing(.Beneficiary_NonNasabah_NomorID, Me.TxtNonSwiftInPenerimaNonNasabah_NomorIden.Text, False, Ovarchar)


                                                End Select
                                            End With
                                            'save beneficiarysave
                                            DataRepository.IFTI_Approval_BeneficiaryProvider.Save(OTrans, objReceiverAppDetail)
                                        End Using

                                    End Using

                                    'cekTransaksi
                                    '----old----
                                    validasiTransaksi()
                                    FillOrNothing(.TanggalTransaksi_Old, objIfti.TanggalTransaksi)
                                    FillOrNothing(.KantorCabangPenyelengaraPengirimAsal_Old, objIfti.KantorCabangPenyelengaraPengirimAsal)
                                    FillOrNothing(.ValueDate_TanggalTransaksi_Old, objIfti.ValueDate_TanggalTransaksi)
                                    FillOrNothing(.ValueDate_NilaiTransaksi_Old, objIfti.ValueDate_NilaiTransaksi)
                                    FillOrNothing(.ValueDate_FK_Currency_ID_Old, objIfti.ValueDate_FK_Currency_ID)
                                    FillOrNothing(.ValueDate_CurrencyLainnya_Old, objIfti.ValueDate_CurrencyLainnya)
                                    FillOrNothing(.ValueDate_NilaiTransaksiIDR_Old, objIfti.ValueDate_NilaiTransaksiIDR)
                                    FillOrNothing(.Instructed_Currency_Old, objIfti.Instructed_Currency)
                                    FillOrNothing(.Instructed_CurrencyLainnya_Old, objIfti.Instructed_CurrencyLainnya)
                                    FillOrNothing(.Instructed_Amount_Old, objIfti.Instructed_Amount)
                                    FillOrNothing(.TujuanTransaksi_Old, objIfti.TujuanTransaksi)
                                    FillOrNothing(.SumberPenggunaanDana_Old, objIfti.SumberPenggunaanDana)

                                    '-----new---
                                    FillOrNothing(.TanggalTransaksi, Me.TxtTransaksi_NonSwiftIntanggal0.Text, False, oDate)
                                    FillOrNothing(.KantorCabangPenyelengaraPengirimAsal, Me.TxtTransaksi_NonSwiftInkantorCabangPengirim.Text, False, Ovarchar)
                                    FillOrNothing(.ValueDate_TanggalTransaksi, Me.TxtTransaksi_NonSwiftInValueTanggalTransaksi.Text, False, oDate)
                                    FillOrNothing(.ValueDate_NilaiTransaksi, Me.Transaksi_NonSwiftInnilaitransaksi0.Text, False, oDecimal)
                                    Using objCurrency As TList(Of MsCurrency) = DataRepository.MsCurrencyProvider.GetPaged(MsCurrencyColumn.Code.ToString & _
                                                                       " like '%" & Me.hfTransaksi_NonSwiftInMataUangTransaksi.Value & "%'", "", 0, Integer.MaxValue, 0)

                                        FillOrNothing(.ValueDate_FK_Currency_ID, objCurrency(0).IdCurrency, False, oInt)
                                    End Using
                                    FillOrNothing(.ValueDate_CurrencyLainnya, objIfti.ValueDate_CurrencyLainnya, False, Ovarchar)
                                    ' FillOrNothing(.ValueDate_FK_Currency_ID, Me.hfTransaksi_NonSwiftInMataUangTransaksi.Value, False, oInt)
                                    FillOrNothing(.ValueDate_NilaiTransaksiIDR, Me.TxtTransaksi_NonSwiftInAmountdalamRupiah.Text, False, oDecimal)
                                    Using objCurrency2 As TList(Of MsCurrency) = DataRepository.MsCurrencyProvider.GetPaged(MsCurrencyColumn.Code.ToString & _
                                                                                             " like '%" & Me.hfTransaksi_NonSwiftIncurrency.Value & "%'", "", 0, Integer.MaxValue, 0)

                                        FillOrNothing(.Instructed_Currency, objCurrency2(0).IdCurrency, False, oInt)
                                    End Using
                                    FillOrNothing(.Instructed_CurrencyLainnya, objIfti.Instructed_CurrencyLainnya, False, Ovarchar)
                                    ' FillOrNothing(.Instructed_Currency, Me.hfTransaksi_NonSwiftIncurrency.Value, False, oInt)
                                    FillOrNothing(.Instructed_Amount, Me.TxtTransaksi_NonSwiftIninstructedAmount.Text, False, oDecimal)
                                    FillOrNothing(.TujuanTransaksi, Me.TxtTransaksi_NonSwiftInTujuanTransaksi.Text, False, Ovarchar)
                                    FillOrNothing(.SumberPenggunaanDana, Me.TxtTransaksi_NonSwiftInSumberPenggunaanDana.Text, False, Ovarchar)
                                    'saving
                                End With

                                DataRepository.IFTI_Approval_DetailProvider.Save(OTrans, objIfti_ApprovalDetail)
                            End Using
                            ''Send Email
                            'SendEmail("Ifti Edit (User Id : " & SessionUserId & "-" & SessionGroupName & ")", _
                            '          "Ifti", SessionIsBankWide, SessionGroupName, "", "Ifti_Approval_view.aspx")
                            'SendEmail("Ifti Edit (User Id : " & SessionUserId & "-" & SessionGroupName & ")", _
                            '          "Ifti", SessionGroupName, "", "Ifti_Approval_view.aspx")
                            ImageButtonCancel.Visible = False
                            lblMsg.Text = "Data has been edited and waiting for approval"
                            MultiViewEditNonSwiftIn.ActiveViewIndex = 1
                        End Using
                    End If
                    OTrans.Commit()
                Catch ex As Exception
                    OTrans.Rollback()
                    Throw
                End Try
            End Using
        Catch ex As Exception
            'LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

#End Region
#Region "Function..."
    Function IsValidDecimal(ByVal intPrecision As Integer, ByVal intScale As Integer, ByVal strDatatoValidate As String) As Boolean

        Return Regex.IsMatch(strDatatoValidate, "^[\d]{1," & intPrecision & "}(\.[\d]{1," & intScale & "})?$")

    End Function
    Private Sub SetCOntrolLoad()
        'bind JenisID
        Using objJenisId As TList(Of IFTI_IDType) = DataRepository.IFTI_IDTypeProvider.GetAll
            If objJenisId.Count > 0 Then
                Me.cbo_NonSwiftInIdenPengirimNas_Ind_jenisidentitas.Items.Clear()
                cbo_NonSwiftInIdenPengirimNas_Ind_jenisidentitas.Items.Add("-Select-")

                Me.cbo_NonSwiftInpenerimaNas_Ind_jenisidentitas.Items.Clear()
                cbo_NonSwiftInpenerimaNas_Ind_jenisidentitas.Items.Add("-Select-")

                Me.CboNonSwiftInPenerimaNonNasabah_JenisDokumen.Items.Clear()
                CboNonSwiftInPenerimaNonNasabah_JenisDokumen.Items.Add("-Select-")

                For i As Integer = 0 To objJenisId.Count - 1
                    cbo_NonSwiftInIdenPengirimNas_Ind_jenisidentitas.Items.Add(New ListItem(objJenisId(i).IDType, objJenisId(i).PK_IFTI_IDType.ToString))
                    cbo_NonSwiftInpenerimaNas_Ind_jenisidentitas.Items.Add(New ListItem(objJenisId(i).IDType, objJenisId(i).PK_IFTI_IDType.ToString))
                    CboNonSwiftInPenerimaNonNasabah_JenisDokumen.Items.Add(New ListItem(objJenisId(i).IDType, objJenisId(i).PK_IFTI_IDType.ToString))
                Next
            End If
            'Bind MsBentukBadanUsaha
            Using objBentukBadanUsaha As TList(Of MsBentukBidangUsaha) = DataRepository.MsBentukBidangUsahaProvider.GetAll
                If objBentukBadanUsaha.Count > 0 Then
                    cbo_NonSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLain.Items.Clear()
                    cbo_NonSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLain.Items.Add("-Select-")

                    For i As Integer = 0 To objBentukBadanUsaha.Count - 1
                        cbo_NonSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLain.Items.Add(New ListItem(objBentukBadanUsaha(i).BentukBidangUsaha, objBentukBadanUsaha(i).IdBentukBidangUsaha.ToString))
                    Next
                End If
            End Using
        End Using

    End Sub
    Sub clearSession()
        'Session("IFTIEdit.grvTRXKMDetilValutaAsingDATA") = Nothing
        'Session("WICEdit.grvDetilKasKeluarDATA") = Nothing
        'Session("WICEdit.ResumeKasMasukKasKeluar") = Nothing
        'Session("WICEdit.RowEdit") = Nothing
        Session("IFTIEdit.IFTIPK") = Nothing
        'Session("PickerProvinsi.Data") = Nothing
    End Sub
#Region "NOnSwiftInSender..."
    Sub FieldNonSwiftInPengirimNasabahPerorangan()
        'PengirimNasabahPerorangan

        Dim objNegara As TList(Of MsNegara) = DataRepository.MsNegaraProvider.GetPaged(MsNegaraColumn.Activation.ToString & "=1", MsNegaraColumn.NamaNegara.ToString, 0, Integer.MaxValue, 0)
        Dim objSnegara As MsNegara 'Penampung hasil search negara yang pke picker

        Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)
            SetnGetSenderAccount = objIfti.Sender_Nasabah_INDV_NoRekening
            Me.txtNonSwiftInPengirimNasabah_IND_Rekening.Text = objIfti.Sender_Nasabah_INDV_NoRekening
            Me.TxtNonSwiftInIdenPengirimNas_Ind_NamaLengkap.Text = objIfti.Sender_Nasabah_INDV_NamaLengkap
            Me.TxtNonSwiftInIdenPengirimNas_Ind_TglLahir.Text = FormatDate(objIfti.Sender_Nasabah_INDV_TanggalLahir)
            If Not IsNothing(objIfti.Sender_Nasabah_INDV_KewargaNegaraan) Then
                Me.RbNonSwiftInIdenPengirimNas_Ind_Kewarganegaraan.SelectedValue = objIfti.Sender_Nasabah_INDV_KewargaNegaraan
                If objIfti.Sender_Nasabah_INDV_KewargaNegaraan = "2" Then
                    Me.Negara_warganegaraPengirim.Visible = True
                    Me.NegaraLain_warganegaraPengirim.Visible = True
                    objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, objIfti.Sender_Nasabah_INDV_Negara)
                    If Not IsNothing(objSnegara) Then
                        TxtNonSwiftInIdenPengirimNas_Ind_Negara.Text = Safe(objSnegara.NamaNegara)
                        hfNonSwiftInIdenPengirimNas_Ind_Negara.Value = Safe(objSnegara.IDNegara)
                    End If
                    Me.TxtNonSwiftInIdenPengirimNas_Ind_NegaraLainnya.Text = objIfti.Sender_Nasabah_INDV_NegaraLainnya
                End If
            End If

            Me.TxtNonSwiftInIdenPengirimNas_Ind_alamat.Text = objIfti.Sender_Nasabah_INDV_ID_Alamat
            Me.TxtNonSwiftInIdenPengirimNas_Ind_NegaraBagian.Text = objIfti.Sender_Nasabah_INDV_ID_NegaraKota
            objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, objIfti.Sender_Nasabah_INDV_ID_Negara)
            If Not IsNothing(objSnegara) Then
                TxtNonSwiftInIdenPengirimNas_Ind_NegaraIden.Text = Safe(objSnegara.NamaNegara)
                hfNonSwiftInIdenPengirimNas_Ind_NegaraIden.Value = Safe(objSnegara.IDNegara)
            End If
            Me.TxtNonSwiftInIdenPengirimNas_Ind_NegaralainIden.Text = objIfti.Sender_Nasabah_INDV_ID_NegaraLainnya
            Me.TxtNonSwiftInIdenPengirimNas_Ind_NoTelp.Text = objIfti.Sender_Nasabah_INDV_NoTelp
            Me.cbo_NonSwiftInIdenPengirimNas_Ind_jenisidentitas.SelectedValue = objIfti.Sender_Nasabah_INDV_FK_IFTI_IDType
            Me.TxtNonSwiftInIdenPengirimNas_Ind_noIdentitas.Text = objIfti.Sender_Nasabah_INDV_NomorID
        End Using
    End Sub
    Sub FieldNonSwiftInPengirimNasabahKorporasi()
        'PengirimNasabahKorporasi
        Dim objNegara As TList(Of MsNegara) = DataRepository.MsNegaraProvider.GetPaged(MsNegaraColumn.Activation.ToString & "=1", MsNegaraColumn.NamaNegara.ToString, 0, Integer.MaxValue, 0)
        Dim objSnegara As MsNegara 'Penampung hasil search negara yang pke picker

        Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)
            SetnGetSenderAccount = objIfti.Sender_Nasabah_CORP_NoRekening
            Me.txtNonSwiftInPengirimNasabah_IND_Rekening.Text = objIfti.Sender_Nasabah_CORP_NoRekening
            Me.TxtNonSwiftInIdenPengirimNas_Korporasi_NamaKorporasi.Text = objIfti.Sender_Nasabah_CORP_NamaKorporasi
            Me.TxtNonSwiftInIdenPengirimNas_Korporasi_alamat.Text = objIfti.Sender_Nasabah_CORP_AlamatLengkap
            Me.TxtNonSwiftInIdenPengirimNas_Korporasi_NegaraBagian.Text = objIfti.Sender_Nasabah_CORP_NegaraKota
            objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, objIfti.Sender_Nasabah_CORP_Negara)
            If Not IsNothing(objSnegara) Then
                TxtNonSwiftInIdenPengirimNas_Korporasi_Negara.Text = Safe(objSnegara.NamaNegara)
                HfNonSwiftInIdenPengirimNas_Korporasi_Negara.Value = Safe(objSnegara.IDNegara)
            End If
            Me.txtNonSwiftInIdenPengirimNas_Korporasi_NegaraLainnya.Text = objIfti.Sender_Nasabah_CORP_NegaraLainnya
            Me.txtNonSwiftInIdenPengirimNas_Korporasi_NoTelp.Text = objIfti.Sender_Nasabah_CORP_NoTelp
        End Using
    End Sub
    Sub FieldNonSwiftInPengirimNonNasabah()
        'PenerimaNonNasabah

        Dim objNegara As TList(Of MsNegara) = DataRepository.MsNegaraProvider.GetPaged(MsNegaraColumn.Activation.ToString & "=1", MsNegaraColumn.NamaNegara.ToString, 0, Integer.MaxValue, 0)
        Dim objSnegara As MsNegara 'Penampung hasil search negara yang pke picker

        Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)
            SetnGetSenderAccount = objIfti.Sender_NonNasabah_NoRekening
            Me.TxtNonSwiftInPengirimNonNasabah_NomorRek.Text = objIfti.Sender_NonNasabah_NoRekening
            Me.TxtNonSwiftInPengirimNonNasabah_NamaBank.Text = objIfti.Sender_NonNasabah_NamaBank
            Me.TxtNonSwiftInPengirimNonNasabah_NamaLengkap.Text = objIfti.Sender_NonNasabah_NamaLengkap
            Me.TxtNonSwiftInPengirimNonNasabah_TanggalLhr.Text = FormatDate(objIfti.Sender_NonNasabah_TanggalLahir)
            Me.TxtNonSwiftInPengirimNonNasabah_Alamat.Text = objIfti.Sender_NonNasabah_ID_Alamat
            Me.TxtNonSwiftInPengirimNonNasabah_Kota.Text = objIfti.Sender_NonNasabah_ID_NegaraBagian
            objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, objIfti.Sender_NonNasabah_ID_Negara)
            If Not IsNothing(objSnegara) Then
                TxtNonSwiftInPengirimNonNasabah_Negara.Text = Safe(objSnegara.NamaNegara)
                hfNonSwiftInPengirimNonNasabah_Negara.Value = Safe(objSnegara.IDNegara)
            End If
            Me.TxtNonSwiftInPengirimNonNasabah_NegaraLainnya.Text = objIfti.Sender_NonNasabah_ID_NegaraLainnya
            Me.TxtNonSwiftInPengirimNonNasabah_NoTelp.Text = objIfti.Sender_NonNasabah_NoTelp
        End Using
    End Sub
#End Region
#Region "SwiftInReceiverType..."
    Sub FieldNonSwiftInReceiverCase1()
        'PengirimNasabahIndividu
        Dim objNegara As TList(Of MsNegara) = DataRepository.MsNegaraProvider.GetPaged(MsNegaraColumn.Activation.ToString & "=1", MsNegaraColumn.NamaNegara.ToString, 0, Integer.MaxValue, 0)
        Dim objSnegara As MsNegara 'Penampung hasil search negara yang pke picker
        Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetPaged(MsKotaKabColumn.Activation.ToString & "=1", MsKotaKabColumn.NamaKotaKab.ToString, 0, Integer.MaxValue, 0)
        Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
        Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetPaged(MsProvinceColumn.activation.ToString & "=1", MsProvinceColumn.Nama.ToString, 0, Integer.MaxValue, 0)
        Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi
        Dim objPekerjaan As TList(Of MsPekerjaan) = DataRepository.MsPekerjaanProvider.GetPaged(MsPekerjaanColumn.Activation.ToString & "=1", MsPekerjaanColumn.NamaPekerjaan.ToString, 0, Integer.MaxValue, 0)
        Dim objSPekerjaan As MsPekerjaan 'Penampung hasil search pekerjaan

        Using objIftiBeneficiary As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("FK_IFTI_ID=" & getIFTIPK, "", 0, 1, 0)
            'SetnGetSenderAccount = objIfti.Sender_Nasabah_INDV_NoRekening
            Me.TxtNonSwiftInPenerimaNasabah_Rekening.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_NoRekening
            Me.TxtNonSwiftInPenerimaNasabah_IND_nama.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_NamaLengkap
            Me.TxtNonSwiftInPenerimaNasabah_IND_TanggalLahir.Text = FormatDate(objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_TanggalLahir)
            Me.RbNonSwiftInPenerimaNasabah_IND_Kewarganegaraan.SelectedValue = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_KewargaNegaraan
            If objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_KewargaNegaraan = "2" Then
                Me.Negara_warganegaraPenerima.Visible = True
                Me.NegaraLain_warganegaraPenerima.Visible = True
                objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_Negara)
                If Not IsNothing(objSnegara) Then
                    TxtNonSwiftInPenerimaNasabah_IND_negara.Text = Safe(objSnegara.NamaNegara)
                    hfNonSwiftInPenerimaNasabah_IND_negara.Value = Safe(objSnegara.IDNegara)
                End If
                Me.TxtNonSwiftInPenerimaNasabah_IND_negaralain.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_NegaraLainnya
            End If
            objSPekerjaan = objPekerjaan.Find(MsPekerjaanColumn.IDPekerjaan, objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_Pekerjaan)
            If Not IsNothing(objSPekerjaan) Then
                TxtNonSwiftInPenerimaNasabah_IND_pekerjaan.Text = Safe(objSPekerjaan.NamaPekerjaan)
                hfNonSwiftInIdenPenerimaNas_Ind_pekerjaan.Value = Safe(objSPekerjaan.IDPekerjaan)
            End If
            Me.TxtNonSwiftInIdenPenerimaNas_Ind_Pekerjaanlain.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_PekerjaanLainnya
            ' Alamat(Domisili)
            Me.TxtNonSwiftInIdenPenerimaNas_Ind_Alamat.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_ID_Alamat_Dom
            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_ID_KotaKab_Dom)
            If Not IsNothing(objSkotaKab) Then
                Me.TxtNonSwiftInIdenPenerimaNas_Ind_Kota.Text = Safe(objSkotaKab.NamaKotaKab)
                hfNonSwiftInIdenPenerimaNas_Ind_Kota_dom.Value = Safe(objSkotaKab.IDKotaKab)
            End If
            Me.TxtNonSwiftInIdenPenerimaNas_Ind_kotalain.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom
            objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(SafeNumIndex(objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_ID_Propinsi_Dom))
            If Not IsNothing(objSProvinsi) Then
                Me.TxtNonSwiftInIdenPenerimaNas_Ind_provinsi.Text = Safe(objSProvinsi.nama)
                hfNonSwiftInIdenPenerimaNas_Ind_provinsi_dom.Value = Safe(objSProvinsi.IdProvince)
            End If
            Me.TxtNonSwiftInIdenPenerimaNas_Ind_provinsiLain.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom

            'Alamat Identitas 
            Me.TxtNonSwiftInIdenPenerimaNas_Ind_alamatIdentitas.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_ID_Alamat_Iden
            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_ID_KotaKab_Iden)
            If Not IsNothing(objSkotaKab) Then
                Me.TxtNonSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Text = Safe(objSkotaKab.NamaKotaKab)
                hfNonSwiftInIdenPenerimaNas_Ind_kotaIdentitas.Value = Safe(objSkotaKab.IDKotaKab)
            End If
            Me.TxtNonSwiftInIdenPenerimaNas_Ind_KotaLainIden.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden
            objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(SafeNumIndex(objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_ID_Propinsi_Iden))
            If Not IsNothing(objSProvinsi) Then
                Me.TxtNonSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Text = Safe(objSProvinsi.Nama)
                HfNonSwiftInIdenPenerimaNas_Ind_ProvinsiIden.Value = Safe(objSProvinsi.IdProvince)
            End If
            Me.TxtNonSwiftInIdenPenerimaNas_Ind_ProvinsilainIden.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden
            Me.TxtINonSwiftIndenPenerimaNas_Ind_noTelp.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_NoTelp
            Me.cbo_NonSwiftInpenerimaNas_Ind_jenisidentitas.SelectedValue = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_FK_IFTI_IDType
            Me.TxtINonSwiftIndenPenerimaNas_Ind_noIdentitas.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_NomorID
        End Using

    End Sub
    Sub FieldNonSwiftInReceiverCase2()
        'PengirimNasabahKorporasi

        Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetPaged(MsKotaKabColumn.Activation.ToString & "=1", MsKotaKabColumn.NamaKotaKab.ToString, 0, Integer.MaxValue, 0)
        Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
        Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetPaged(MsProvinceColumn.activation.ToString & "=1", MsProvinceColumn.Nama.ToString, 0, Integer.MaxValue, 0)
        Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi
        Dim objBidangUsaha As TList(Of MsBidangUsaha) = DataRepository.MsBidangUsahaProvider.GetAll
        Dim objSBidangUsaha As MsBidangUsaha 'Penampung hasil search bidang usaha

        Using objIftiBeneficiary As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("FK_IFTI_ID=" & getIFTIPK, "", 0, 1, 0)
            Me.TxtNonSwiftInPenerimaNasabah_Rekening.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_NoRekening
            Using objBentukBadanUsaha As MsBentukBidangUsaha = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault(0))
                Me.cbo_NonSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLain.SelectedValue = objBentukBadanUsaha.BentukBidangUsaha
            End Using
            Me.TxtNonSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLainnya.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya
            Me.TxtNonSwiftInPenerimaNasabah_Korp_namaKorp.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_NamaKorporasi
            objSBidangUsaha = objBidangUsaha.Find(MsBidangUsahaColumn.IdBidangUsaha, objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id)
            If Not IsNothing(objSBidangUsaha) Then
                TxtNonSwiftInPenerimaNasabah_Korp_BidangUsahaKorp.Text = Safe(objSBidangUsaha.NamaBidangUsaha)
                hfNonSwiftInPenerimaNasabah_Korp_BidangUsahaKorp.Value = Safe(objSBidangUsaha.IdBidangUsaha)
            End If
            TxtNonSwiftInPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_BidangUsahaLainnya
            Me.TxtNonSwiftInPenerimaNasabah_Korp_AlamatKorp.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_AlamatLengkap
            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_ID_KotaKab)
            If Not IsNothing(objSkotaKab) Then
                Me.TxtNonSwiftInPenerimaNasabah_Korp_Kotakab.Text = Safe(objSkotaKab.NamaKotaKab)
                hfNonSwiftInPenerimaNasabah_Korp_kotakab.Value = Safe(objSkotaKab.IDKotaKab)
            End If
            Me.TxtNonSwiftInPenerimaNasabah_Korp_kotaLain.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_ID_KotaKabLainnya
            objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(SafeNumIndex(objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_ID_Propinsi))
            If Not IsNothing(objSProvinsi) Then
                Me.TxtNonSwiftInPenerimaNasabah_Korp_propinsi.Text = Safe(objSProvinsi.Nama)
                hfNonSwiftInPenerimaNasabah_Korp_propinsi.Value = Safe(objSProvinsi.IdProvince)
            End If
            Me.TxtNonSwiftInPenerimaNasabah_Korp_propinsilain.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_ID_PropinsiLainnya
            Me.TxtNonSwiftInPenerimaNasabah_Korp_NoTelp.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_NoTelp
        End Using
    End Sub
    Sub FieldNonSwiftInReceiverCase3()
        'PengirimNonNasabah

        Using objIftiBeneficiary As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("FK_IFTI_ID=" & getIFTIPK, "", 0, 1, 0)
            Me.TxtNonSwiftInPenerimaNonNasabah_KodeRahasia.Text = objIftiBeneficiary(0).Beneficiary_NonNasabah_KodeRahasia
            TxtNonSwiftInPenerimaNonNasabah_NomorRek.Text = objIftiBeneficiary(0).Beneficiary_NonNasabah_NoRekening
            Me.TxtNonSwiftInPenerimaNonNasabah_NamaBank.Text = objIftiBeneficiary(0).Beneficiary_NonNasabah_NamaBank
            Me.TxtNonSwiftInPenerimaNonNasabah_nama.Text = objIftiBeneficiary(0).Beneficiary_NonNasabah_NamaLengkap
            Me.TxtNonSwiftInPenerimaNonNasabah_TanggalLahir.Text = FormatDate(objIftiBeneficiary(0).Beneficiary_NonNasabah_TanggalLahir)
            Me.TxtNonSwiftInPenerimaNonNasabah_alamatiden.Text = objIftiBeneficiary(0).Beneficiary_NonNasabah_ID_Alamat
            Me.TxtNonSwiftInPenerimaNonNasabah_NoTelepon.Text = objIftiBeneficiary(0).Beneficiary_NonNasabah_NoTelp
            Me.CboNonSwiftInPenerimaNonNasabah_JenisDokumen.SelectedValue = objIftiBeneficiary(0).Beneficiary_NonNasabah_FK_IFTI_IDType.GetValueOrDefault(0)
            Me.TxtNonSwiftInPenerimaNonNasabah_NomorIden.Text = objIftiBeneficiary(0).Beneficiary_NonNasabah_NomorID
        End Using
    End Sub
#End Region
#Region "TransactionType..."
    Private Sub TransactionNonSwiftInc()
        Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)
            SafeDefaultValue = ""
            If Not IsNothing(objIfti) Then
                'umum
                Me.NonSwiftInUmum_LTDN.Text = Safe(objIfti.LTDLNNo)
                Me.NonSwiftInUmum_LtdnKoreksi.Text = Safe(objIfti.LTDLNNoKoreksi)
                Me.NonSwiftInUmum_TanggalLaporan.Text = FormatDate(Safe(objIfti.TanggalLaporan))
                Me.NonSwiftInUmum_NamaPJKBank.Text = Safe(objIfti.NamaPJKBankPelapor)
                Me.NonSwiftInUmum_NamaPejabatPJKBank.Text = Safe(objIfti.NamaPejabatPJKBankPelapor)
                Me.RbNonSwiftInUmum_JenisLaporan.SelectedValue = Safe(objIfti.JenisLaporan)

                'Identitas Pengirim
                ' cek ReceiverTyp
                Dim SenderType As Integer = objIfti.Sender_FK_IFTI_NasabahType_ID
                Select Case (SenderType)
                    Case 1
                        Me.rbrbNonSwiftIn_TipePengirim.SelectedValue = 1
                        Me.trNonSwiftInTipeNasabah.Visible = True
                        Me.RB_NonSwiftIn_TipeNasabah.SelectedValue = 1
                        Me.MultiViewNonSwiftInPengirim.ActiveViewIndex = 0
                        Me.MultiViewNonSwiftInPengirimAkhir.ActiveViewIndex = 0
                        MultiViewPengirimAkhirNasabah.ActiveViewIndex = 0
                        FieldNonSwiftInPengirimNasabahPerorangan()
                    Case 2
                        Me.rbrbNonSwiftIn_TipePengirim.SelectedValue = 1
                        Me.trNonSwiftInTipeNasabah.Visible = True
                        Me.RB_NonSwiftIn_TipeNasabah.SelectedValue = 2
                        Me.MultiViewNonSwiftInPengirim.ActiveViewIndex = 0
                        Me.MultiViewNonSwiftInPengirimAkhir.ActiveViewIndex = 1
                        MultiViewPengirimAkhirNasabah.ActiveViewIndex = 1
                        FieldNonSwiftInPengirimNasabahKorporasi()
                    Case 3
                        Me.rbrbNonSwiftIn_TipePengirim.SelectedValue = 1
                        Me.trNonSwiftInTipeNasabah.Visible = False
                        Me.MultiViewNonSwiftInPengirim.ActiveViewIndex = 0
                        MultiViewNonSwiftInPengirimAkhir.ActiveViewIndex = 1
                        Me.trNonSwiftInTipeNasabah.Visible = False
                        FieldNonSwiftInPengirimNonNasabah()
                End Select

                'Identitas Penerima
                'cekTipe Penyelenggara
                Using objIftiBeneficiary As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("FK_IFTI_ID = " & getIFTIPK, "", 0, Integer.MaxValue, 0)
                    If objIftiBeneficiary.Count > 0 Then
                        Dim ReceiverType As Integer = objIftiBeneficiary(0).FK_IFTI_NasabahType_ID
                        Select Case (ReceiverType)
                            Case 1
                                'penerimaNasabahIndividu
                                Me.TxtNonSwiftInPenerimaNasabah_Rekening.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_INDV_NoRekening
                                ' Me.cboSwiftInPenerimaNonNasabah_TipePJKBank.SelectedValue = 0
                                Rb_IdenPenerima_TipePenerima.SelectedValue = 1
                                Rb_IdenPenerima_TipeNasabah.SelectedValue = 1
                                Me.trNonSwiftInTipepengirim.Visible = True
                                Me.trNonSwiftInPenerimaTipeNasabah.Visible = True
                                Me.MultiViewNonSwiftInPenerima.ActiveViewIndex = 0
                                Me.MultiViewNonSwiftInPenerimaAkhir.ActiveViewIndex = 0
                                Me.MultiViewPenerimaAkhirNasabah.ActiveViewIndex = 0
                                FieldNonSwiftInReceiverCase1()
                            Case 2
                                'penerimaNasabahKorp
                                Me.TxtNonSwiftInPenerimaNasabah_Rekening.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_NoRekening
                                'Me.cboSwiftInPenerimaNonNasabah_TipePJKBank.SelectedValue = 0
                                Rb_IdenPenerima_TipePenerima.SelectedValue = 1
                                Rb_IdenPenerima_TipeNasabah.SelectedValue = 2
                                Me.trNonSwiftInTipepengirim.Visible = True
                                Me.trNonSwiftInPenerimaTipeNasabah.Visible = True
                                Me.MultiViewNonSwiftInPenerima.ActiveViewIndex = 0
                                Me.MultiViewNonSwiftInPenerimaAkhir.ActiveViewIndex = 0
                                Me.MultiViewPenerimaAkhirNasabah.ActiveViewIndex = 1
                                FieldNonSwiftInReceiverCase2()
                            Case 3
                                'PenerimaNonNasabah
                                '.cboSwiftInPenerimaNonNasabah_TipePJKBank.SelectedValue = 0
                                Rb_IdenPenerima_TipePenerima.SelectedValue = 2
                                Me.trNonSwiftInTipepengirim.Visible = True
                                Me.trNonSwiftInPenerimaTipeNasabah.Visible = False
                                Me.MultiViewNonSwiftInPenerima.ActiveViewIndex = 0
                                Me.MultiViewNonSwiftInPenerimaAkhir.ActiveViewIndex = 1
                                FieldNonSwiftInReceiverCase3()
                        End Select

                    End If

                End Using

                'Transaksi
                Me.TxtTransaksi_NonSwiftIntanggal0.Text = FormatDate(objIfti.TanggalTransaksi)
                Me.TxtTransaksi_NonSwiftInkantorCabangPengirim.Text = objIfti.KantorCabangPenyelengaraPengirimAsal
                Me.TxtTransaksi_NonSwiftInValueTanggalTransaksi.Text = FormatDate(objIfti.ValueDate_TanggalTransaksi)
                Me.Transaksi_NonSwiftInnilaitransaksi0.Text = objIfti.ValueDate_NilaiTransaksi
                Using objCurrency As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(objIfti.ValueDate_FK_Currency_ID.GetValueOrDefault(0))
                    If Not IsNothing(objCurrency) Then
                        Me.TxtTransaksi_NonSwiftInMataUangTransaksi.Text = objCurrency.Code
                        hfTransaksi_NonSwiftInMataUangTransaksi.Value = objCurrency.Code
                    End If
                End Using
                Me.TxtTransaksi_NonSwiftInMataUangTransaksiLainnya.Text = objIfti.ValueDate_CurrencyLainnya
                Me.TxtTransaksi_NonSwiftInAmountdalamRupiah.Text = objIfti.ValueDate_NilaiTransaksiIDR
                Using objCurrency As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(objIfti.Instructed_Currency.GetValueOrDefault(0))
                    If Not IsNothing(objCurrency) Then
                        Me.TxtTransaksi_NonSwiftIncurrency.Text = objCurrency.Code
                        hfTransaksi_NonSwiftIncurrency.Value = objCurrency.Code
                    End If
                End Using
                Me.TxtTransaksi_NonSwiftIncurrencyLainnya.Text = objIfti.Instructed_CurrencyLainnya
                If Not IsNothing(objIfti.Instructed_Amount) Then
                    Me.TxtTransaksi_NonSwiftIninstructedAmount.Text = objIfti.Instructed_Amount
                End If
                Me.TxtTransaksi_NonSwiftInTujuanTransaksi.Text = objIfti.TujuanTransaksi
                Me.TxtTransaksi_NonSwiftInSumberPenggunaanDana.Text = objIfti.SumberPenggunaanDana

            End If
        End Using

    End Sub
#End Region
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
		clearSession()
                'AuditTrailBLL.InsertAuditTrailUserAccess(Sahassa.AML.Commonly.SessionCurrentPage)
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                Using ObjAuditTrailUserAccess As AuditTrail_UserAccess = New AuditTrail_UserAccess
                    With ObjAuditTrailUserAccess
                        .AuditTrail_UserAccessUserid = Sahassa.AML.Commonly.SessionUserId
                        .AuditTrail_UserAccessActionDate = DateTime.Now
                        .AuditTrail_UserAccessAction = UserAccessAction
                    End With
                    DataRepository.AuditTrail_UserAccessProvider.Save(ObjAuditTrailUserAccess)
                End Using
                SetCOntrolLoad()
                TransactionNonSwiftInc()
                'loadIFTIToField()
                'loadResume()

                'If MultiView1.ActiveViewIndex = 0 Then
                '    ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('IdTerlapor','Img1','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
                '    ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('Transaksi','Img2','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
                'End If
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            'LogError(ex)
        End Try
    End Sub


    Protected Sub RbNonSwiftInIdenPengirimNas_Ind_Kewarganegaraan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RbNonSwiftInIdenPengirimNas_Ind_Kewarganegaraan.SelectedIndexChanged
        If RbNonSwiftInIdenPengirimNas_Ind_Kewarganegaraan.SelectedValue = 1 Then
            Me.Negara_warganegaraPengirim.Visible = False
            Me.NegaraLain_warganegaraPengirim.Visible = False
        ElseIf RbNonSwiftInIdenPengirimNas_Ind_Kewarganegaraan.SelectedValue = 2 Then
            Me.Negara_warganegaraPengirim.Visible = True
            Me.NegaraLain_warganegaraPengirim.Visible = True
        End If

    End Sub

    Protected Sub RbNonSwiftInPenerimaNasabah_IND_Kewarganegaraan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RbNonSwiftInPenerimaNasabah_IND_Kewarganegaraan.SelectedIndexChanged
        If RbNonSwiftInPenerimaNasabah_IND_Kewarganegaraan.SelectedValue = 1 Then
            Me.Negara_warganegaraPenerima.Visible = False
            Me.NegaraLain_warganegaraPenerima.Visible = False
        ElseIf RbNonSwiftInPenerimaNasabah_IND_Kewarganegaraan.SelectedValue = 2 Then
            Me.Negara_warganegaraPenerima.Visible = True
            Me.NegaraLain_warganegaraPenerima.Visible = True
        End If
    End Sub
    Protected Sub cvalPageErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles cvalPageErr.PreRender
        If cvalPageErr.IsValid = False Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
        End If
    End Sub

    Protected Sub imgOKMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgOKMsg.Click
        Response.Redirect("iftiview.aspx")
    End Sub

    Protected Sub ImageButtonCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonCancel.Click
        Response.Redirect("Iftiview.aspx")
    End Sub
End Class
