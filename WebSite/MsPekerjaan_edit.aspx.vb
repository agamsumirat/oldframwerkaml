#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports System.Collections.Generic
#End Region

Partial Class MsPekerjaan_edit
    Inherits Parent

#Region "Function"

    Private Sub LoadData()
        Dim ObjMsPekerjaan As MsPekerjaan = DataRepository.MsPekerjaanProvider.GetPaged(MsPekerjaanColumn.IDPekerjaan.ToString & "=" & parID, "", 0, 1, Nothing)(0)
        If ObjMsPekerjaan Is Nothing Then Throw New Exception("Data Not Found")
        With ObjMsPekerjaan
            SafeDefaultValue = "-"
            txtIDPekerjaan.Text = Safe(.IDPekerjaan)
            txtNamaPekerjaan.Text = Safe(.NamaPekerjaan)

            chkactivation.checked = SafeBoolean(.Activation)
            Dim L_objMappingMsPekerjaanNCBSPPATK As TList(Of MappingMsPekerjaanNCBSPPATK)
            L_objMappingMsPekerjaanNCBSPPATK = DataRepository.MappingMsPekerjaanNCBSPPATKProvider.GetPaged(MappingMsPekerjaanNCBSPPATKColumn.IdPekerjaan.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)


            Listmaping.AddRange(L_objMappingMsPekerjaanNCBSPPATK)

        End With
    End Sub

    Private Sub BindListMapping()
        LBMapping.DataSource = ListMappingDisplay
        LBMapping.DataBind()
    End Sub

    ''' <summary>
    ''' Validasi men-handle seluruh syarat2 data valid
    ''' data yang lolos dari validasi dipastikan lolos ke database 
    ''' karena menggunakan FillorNothing
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ValidasiControl()
        '======================== Validasi textbox :  ID canot Null ====================================================
        If ObjectAntiNull(txtIDPekerjaan.Text) = False Then Throw New Exception("ID  must be filled  ")
        If Not IsNumeric(Me.txtIDPekerjaan.Text) Then
            Throw New Exception("ID must be in number format")
        End If
        '======================== Validasi textbox :  Nama canot Null ====================================================
        If ObjectAntiNull(txtNamaPekerjaan.Text) = False Then Throw New Exception("Nama  must be filled  ")

        If DataRepository.MsPekerjaanProvider.GetPaged("IDPekerjaan = '" & txtIDPekerjaan.Text & "' AND IDPekerjaan <> '" & parID & "'", "", 0, Integer.MaxValue, 0).Count > 0 Then
            Throw New Exception("ID already exist in the database")
        End If

        If DataRepository.MsPekerjaan_ApprovalDetailProvider.GetPaged(MsPekerjaan_ApprovalDetailColumn.IDPekerjaan.ToString & "='" & txtIDPekerjaan.Text & "' AND IDPekerjaan <> '" & parID & "'", "", 0, 1, Nothing).Count > 0 Then
            Throw New Exception("Data exist in List Waiting Approval")
        End If
        'If LBMapping.Items.Count = 0 Then Throw New Exception("data tidak punya list Mapping")
    End Sub

#End Region

#Region "events..."

    Protected Sub imgBrowse_click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBrowse.Click
        Try
            If Not Get_DataFromPicker Is Nothing Then
                Listmaping.AddRange(Get_DataFromPicker)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgDeleteItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgDeleteItem.Click
        Try
            If LBMapping.SelectedIndex < 0 Then
                Throw New Exception("No data Selected")
            End If
            Listmaping.RemoveAt(LBMapping.SelectedIndex)

        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                'AuditTrailBLL.InsertAuditTrailUserAccess(Sahassa.CommonCTRWeb.Commonly.SessionCurrentPage)
                Listmaping = New TList(Of MappingMsPekerjaanNCBSPPATK)
                LoadData()
            Catch ex As Exception
                LogError(ex)
                CvalPageErr.IsValid = False
                CvalPageErr.ErrorMessage = ex.Message
            End Try
        End If

    End Sub

    Protected Sub ImgBtnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnSave.Click
        Try
            Page.Validate("handle")
            If Page.IsValid Then
                ValidasiControl()
                ' =========== Insert Header Approval
                Dim KeyHeaderApproval As Integer
                Using ObjMsPekerjaan_Approval As New MsPekerjaan_Approval
                    With ObjMsPekerjaan_Approval
                        FillOrNothing(.FK_MsMode_Id, 2, True, oInt)
                        FillOrNothing(.RequestedBy, SessionPkUserId)
                        FillOrNothing(.RequestedDate, Date.Now)
                        .IsUpload = False
                    End With
                    DataRepository.MsPekerjaan_ApprovalProvider.Save(ObjMsPekerjaan_Approval)
                    KeyHeaderApproval = ObjMsPekerjaan_Approval.PK_MsPekerjaan_Approval_Id
                End Using
                '============ Insert Detail Approval
                Using objMsPekerjaan_ApprovalDetail As New MsPekerjaan_ApprovalDetail()
                    With objMsPekerjaan_ApprovalDetail
                        Dim ObjMsPekerjaan As MsPekerjaan = DataRepository.MsPekerjaanProvider.GetByIDPekerjaan(parID)
                        FillOrNothing(.IDPekerjaan, ObjMsPekerjaan.IDPekerjaan)
                        FillOrNothing(.NamaPekerjaan, ObjMsPekerjaan.NamaPekerjaan)
                        FillOrNothing(.Activation, ObjMsPekerjaan.Activation)
                        FillOrNothing(.CreatedDate, ObjMsPekerjaan.CreatedDate)
                        FillOrNothing(.CreatedBy, ObjMsPekerjaan.CreatedBy)
                        FillOrNothing(.FK_MsPekerjaan_Approval_Id, KeyHeaderApproval)

                        '==== Edit data =============
                        FillOrNothing(.IDPekerjaan, txtIDPekerjaan.Text, True, oInt)
                        FillOrNothing(.NamaPekerjaan, txtNamaPekerjaan.Text, True, Ovarchar)

                        FillOrNothing(.Activation, chkActivation.checked, True, oBit)
                        FillOrNothing(.LastUpdatedBy, SessionUserId)
                        FillOrNothing(.LastUpdatedDate, Date.Now)

                    End With
                    DataRepository.MsPekerjaan_ApprovalDetailProvider.Save(objMsPekerjaan_ApprovalDetail)
                    Dim keyHeaderApprovalDetail As Integer = objMsPekerjaan_ApprovalDetail.PK_MsPekerjaan_ApprovalDetail_Id
                    '========= Insert mapping item 
                    Dim LobjMappingMsPekerjaanNCBSPPATK_Approval_Detail As New TList(Of MappingMsPekerjaanNCBSPPATK_Approval_Detail)
                    For Each objMappingMsPekerjaanNCBSPPATK As MappingMsPekerjaanNCBSPPATK In Listmaping
                        Dim Mapping As New MappingMsPekerjaanNCBSPPATK_Approval_Detail
                        With Mapping
                            FillOrNothing(.IdPekerjaan, objMsPekerjaan_ApprovalDetail.IDPekerjaan)
                            FillOrNothing(.IdPekerjaanNCBS, objMappingMsPekerjaanNCBSPPATK.IdPekerjaanNCBS)
                            FillOrNothing(.PK_MsPekerjaan_approval_id, KeyHeaderApproval)
                            FillOrNothing(.Nama, objMappingMsPekerjaanNCBSPPATK.Nama)
                            FillOrNothing(.PK_MappingMsPekerjaanNCBSPPATK_Approval_detail_ID, keyHeaderApprovalDetail)
                            LobjMappingMsPekerjaanNCBSPPATK_Approval_Detail.Add(Mapping)
                        End With
                    Next
                    DataRepository.MappingMsPekerjaanNCBSPPATK_Approval_DetailProvider.Save(LobjMappingMsPekerjaanNCBSPPATK_Approval_Detail)
                    'session Maping item Clear
                    Listmaping.Clear()
                    ImgBtnSave.Visible = False
                    ImgBackAdd.Visible = False
                    LblConfirmation.Text = "Data has been added and waiting for approval"
                    MtvMsUser.ActiveViewIndex = 1
                End Using
            End If
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub



    Protected Sub ImgBackAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBackAdd.Click
        Response.Redirect("MsPekerjaan_View.aspx")
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            BindListMapping()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBack.Click
        Response.Redirect("MsPekerjaan_View.aspx")
    End Sub
#End Region

#Region "Property..."
    Public ReadOnly Property parID() As String
        Get
            Return Request.Item("ID")
        End Get
    End Property
    ''' <summary>
    ''' Mengambil Data dari Picker
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Get_DataFromPicker() As TList(Of MappingMsPekerjaanNCBSPPATK)
        Get
            Dim Temp As New TList(Of MappingMsPekerjaanNCBSPPATK)
            Dim L_msPekerjaanNCBS As TList(Of msPekerjaanNCBS)
            Try
                If Session("PickermsPekerjaanNCBS.Data") Is Nothing Then
                    Throw New Exception
                End If
                L_msPekerjaanNCBS = Session("PickermsPekerjaanNCBS.Data")
                For Each i As msPekerjaanNCBS In L_msPekerjaanNCBS
                    Dim Tempmapping As New MappingMsPekerjaanNCBSPPATK
                    With Tempmapping
                        .IdPekerjaanNCBS = i.IDPekerjaanNCBS
                        .Nama = i.NamaPekerjaan
                    End With
                    Temp.Add(Tempmapping)
                Next
            Catch ex As Exception
                L_msPekerjaanNCBS = Nothing
            End Try
            Return Temp
        End Get
    End Property
    ''' <summary>
    ''' Menyimpan Item untuk mapping sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Listmaping() As TList(Of MappingMsPekerjaanNCBSPPATK)
        Get
            Return Session("Listmaping.data")
        End Get
        Set(ByVal value As TList(Of MappingMsPekerjaanNCBSPPATK))
            Session("Listmaping.data") = value
        End Set
    End Property
    ''' <summary>
    ''' List yang tampil di ListBox
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplay() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingMsPekerjaanNCBSPPATK In Listmaping.FindAllDistinct("IDPekerjaanNCBS")
                Temp.Add(i.IdPekerjaanNCBS.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property
#End Region

End Class



