
Imports System.Data
Imports System.Data.SqlClient
Imports AML2015Nettier.Data

Partial Class CaseManagementWorkflowView
    Inherits Parent

#Region " Property "
    ''' <summary>
    ''' selected item store
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("CaseManagementWorkflowSelected") Is Nothing, New ArrayList, Session("CaseManagementWorkflowSelected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("CaseManagementWorkflowSelected") = value
        End Set
    End Property
    ''' <summary>
    ''' setnget search field
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetFieldSearch() As String
        Get
            Return IIf(Session("CaseManagementWorkflowFieldSearch") Is Nothing, "", Session("CaseManagementWorkflowFieldSearch"))
        End Get
        Set(ByVal Value As String)
            Session("CaseManagementWorkflowFieldSearch") = Value
        End Set
    End Property
    ''' <summary>
    ''' search value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetValueSearch() As String
        Get
            Return IIf(Session("CaseManagementWorkflowValueSearch") Is Nothing, "", Session("CaseManagementWorkflowValueSearch"))
        End Get
        Set(ByVal Value As String)
            Session("CaseManagementWorkflowValueSearch") = Value
        End Set
    End Property
    ''' <summary>
    ''' sort expresion
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("CaseManagementWorkflowSort") Is Nothing, "", Session("CaseManagementWorkflowSort"))
        End Get
        Set(ByVal Value As String)
            Session("CaseManagementWorkflowSort") = Value
        End Set
    End Property
    ''' <summary>
    ''' current page index
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("CaseManagementWorkflowCurrentPage") Is Nothing, 0, Session("CaseManagementWorkflowCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("CaseManagementWorkflowCurrentPage") = Value
        End Set
    End Property
    ''' <summary>
    ''' total pages
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    ''' <summary>
    ''' row total
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("CaseManagementWorkflowRowTotal") Is Nothing, 0, Session("CaseManagementWorkflowRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("CaseManagementWorkflowRowTotal") = Value
        End Set
    End Property
    ''' <summary>
    ''' save bind table
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetBindTable() As Data.DataTable
        Get
            Return IIf(Session("CaseManagementWorkflowData") Is Nothing, New AMLDAL.AMLDataSet.GroupDataTable, Session("CaseManagementWorkflowData"))
        End Get
        Set(ByVal value As Data.DataTable)
            Session("CaseManagementWorkflowData") = value
        End Set
    End Property
#End Region

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' fill search
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub FillSearch()
        Try
            Me.ComboSearch.Items.Add(New ListItem("...", ""))
            Me.ComboSearch.Items.Add(New ListItem("Account Owner", "AccountOwnerName Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Status", "StatusConfigure Like '-=Search=-'"))
            Me.ComboSearch.Items.Add(New ListItem("VIPCode", "VIPCode Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Segment", "Segment Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("SBU", "SBU Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Sub-SBU", "SubSBU Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("RM", "RM Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("InsiderCode", "InsiderCode Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("CaseAlertDescription", "CaseAlertDescription Like '%-=Search=-%'"))
        Catch
            Throw
        End Try
    End Sub

    Private Sub ClearThisPageSessions()
        Session("CaseManagementWorkflowSelected") = Nothing
        Session("CaseManagementWorkflowFieldSearch") = Nothing
        Session("CaseManagementWorkflowValueSearch") = Nothing
        Session("CaseManagementWorkflowSort") = Nothing
        Session("CaseManagementWorkflowCurrentPage") = Nothing
        Session("CaseManagementWorkflowRowTotal") = Nothing
        Session("CaseManagementWorkflowData") = Nothing
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not Me.IsPostBack Then
                Me.ClearThisPageSessions()
                Me.ComboSearch.Attributes.Add("onchange", "javascript:CheckNoPopUp('" & Me.TextSearch.ClientID & "');")

                Using AccessCaseManagement As New AMLDAL.CaseManagementWorkflowTableAdapters.SelectCaseManagementWorkflowTableAdapter


                    Me.SetnGetBindTable = AccessCaseManagement.GetData
                End Using
                Me.FillSearch()
                Me.GridCaseManagementWorkflow.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using

                Me.BindGrid()
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' page navigate button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' bind grid
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Sub BindGrid()
        Try
            'Dim Rows() As AMLDAL.CaseManagementWorkflow.SelectCaseManagementWorkflowRow = Me.SetnGetBindTable.Select(Me.SetnGetFieldSearch.Replace("-=Search=-", Me.SetnGetValueSearch.Replace("'", "''")), Me.SetnGetSort)
            Dim dt As DataTable = LoadCaseManagementWorkflow()
            Me.GridCaseManagementWorkflow.DataSource = dt
            Me.SetnGetRowTotal = dt.Rows.Count
            Me.GridCaseManagementWorkflow.CurrentPageIndex = Me.SetnGetCurrentPage
            Me.GridCaseManagementWorkflow.VirtualItemCount = Me.SetnGetRowTotal
            Me.GridCaseManagementWorkflow.DataBind()
        Catch
            Throw
        End Try
    End Sub

    Private Function LoadCaseManagementWorkflow() As DataTable
        Dim dt As Data.DataTable = New Data.DataTable
        Try
            Dim reader As Data.SqlClient.SqlDataReader
            Dim sql As String
            sql = "SELECT a.AccountOwnerId, " & vbCrLf _
            & "       a.CaseAlertDescription, " & vbCrLf _
            & "       CONVERT(VARCHAR(40), a.AccountOwnerId) + ' - ' + ISNULL(ao.AccountOwnerName, '')AccountOwnerName, " & vbCrLf _
            & "       a.VIPCode, " & vbCrLf _
            & "       a.InsiderCode, " & vbCrLf _
            & "       a.Segment, " & vbCrLf _
            & "       a.SBU, " & vbCrLf _
            & "       a.SubSBU, " & vbCrLf _
            & "       a.RM, " & vbCrLf _
            & "       CASE  " & vbCrLf _
            & "            WHEN cmw.Pk_CMW_Id IS NULL THEN 'UnConfigured' " & vbCrLf _
            & "            ELSE 'Configured' " & vbCrLf _
            & "       END AS StatusConfigure " & vbCrLf _
            & "FROM   ListWorkflow a " & vbCrLf _
            & "       LEFT JOIN AccountOwner ao " & vbCrLf _
            & "            ON  ao.AccountOwnerId = a.AccountOwnerId " & vbCrLf _
            & "       LEFT JOIN CaseManagementWorkflow cmw " & vbCrLf _
            & "            ON  cmw.AccountOwnerId = a.AccountOwnerId " & vbCrLf _
            & "            AND ISNULL(cmw.VIPCode, '') = ISNULL(a.VIPCode, '') " & vbCrLf _
            & "            AND ISNULL(cmw.InsiderCode, '') = ISNULL(a.InsiderCode, '') " & vbCrLf _
            & "            AND ISNULL(cmw.Segment, '') = ISNULL(a.Segment, '') " & vbCrLf _
            & "            AND ISNULL(cmw.DescriptionCaseAlert, '') = ISNULL(a.CaseAlertDescription, '') " & vbCrLf _
            & "            AND ISNULL(cmw.SBU, '') = ISNULL(a.SBU, '') " & vbCrLf _
            & "            AND ISNULL(cmw.SubSBU, '') = ISNULL(a.SubSBU, '') " & vbCrLf _
            & "            AND ISNULL(cmw.RM, '') = ISNULL(a.RM, '')"
            Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommand(sql)
                reader = DataRepository.Provider.ExecuteReader(objCommand)
                dt.Load(reader)
            End Using

            Return dt

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' set navigate info
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.ComboSearch.SelectedValue = Me.SetnGetFieldSearch
            Me.TextSearch.Text = Me.SetnGetValueSearch
        Catch
            Throw
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' change sort expression
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridCaseManagementWorkflow_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridCaseManagementWorkflow.SortCommand
        Dim GridCaseManagementWorkflow As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridCaseManagementWorkflow.Columns(Sahassa.AML.Commonly.IndexSort(GridCaseManagementWorkflow, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' image button search
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Try
            Me.SetnGetFieldSearch = Me.ComboSearch.SelectedValue
            'If Me.ComboSearch.SelectedIndex = 0 Then
            '    Me.TextSearch.Text = ""
            'End If
            'Me.SetnGetValueSearch = Me.TextSearch.Text
            'Me.SetnGetCurrentPage = 0

            Dim SearchFilterStakeholder As String = ComboSearch.SelectedValue.Replace("-=Search=-", TextSearch.Text.Trim)

            Using objtemp As Data.DataView = LoadCaseManagementWorkflow.DefaultView
                objtemp.RowFilter = SearchFilterStakeholder
                GridCaseManagementWorkflow.DataSource = objtemp
                GridCaseManagementWorkflow.DataBind()
            End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' grid edit command handler
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridCaseManagementWorkflow.EditCommand
        Try
            Using oAccessCountApproval As New AMLDAL.CaseManagementWorkflowTableAdapters.CountAccountOwnerCaseManagementApprovalTableAdapter
                Dim IntCountApproval As Integer = oAccessCountApproval.Fill(e.Item.Cells(0).Text)
                If IntCountApproval > 0 Then
                    Throw New Exception("Case Management Workflow For Account Owner '" & e.Item.Cells(1).Text & "' is in pending approval. And waiting to approved.")
                Else
                    Me.Response.Redirect("CaseManagementWorkflowDetail.aspx?AccountOwnerID=" & e.Item.Cells(0).Text & "&Status=" & e.Item.Cells(9).Text & "&AccountOwnerName=" & e.Item.Cells(1).Text & "&CaseAlertDescription=" & e.Item.Cells(2).Text & "&VIPCode=" & e.Item.Cells(3).Text & "&InsiderCode=" & e.Item.Cells(4).Text & "&Segment=" & e.Item.Cells(5).Text & "&SBU=" & e.Item.Cells(6).Text & "&SubSBU=" & e.Item.Cells(7).Text & "&RM=" & e.Item.Cells(8).Text, False)
                End If
            End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' go button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	14/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be a positive number greater than zero.")
                End If
            Else
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' prerender event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            'Me.BindGrid()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class
