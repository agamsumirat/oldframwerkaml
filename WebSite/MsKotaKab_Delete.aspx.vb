#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports Sahassa.AML
#End Region

Partial Class MsKotaKab_Delete
Inherits Parent

#Region "Function"

	Sub ChangeMultiView(index As Integer)
		MtvMsUser.ActiveViewIndex = index
	End Sub

	Private Sub BindListMapping()
		LBMapping.DataSource = Listmaping
		LBMapping.DataTextField = "NamaKotaKab_NCBS"
		LBMapping.DataValueField = "IDKotaKab_NCBS"
		LBMapping.DataBind()
	End Sub
  

#End Region

#Region "events..."

	Public ReadOnly Property parID As String
		Get
			Return Request.Item("ID")
		End Get
	End Property

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Not Page.IsPostBack Then
			Try
                'AuditTrailBLL.InsertAuditTrailUserAccess(SessionCurrentPage)
				Listmaping = New TList(Of MsKotaKab_NCBS)
				LoadData()
			Catch ex As Exception
				LogError(ex)
				CvalPageErr.IsValid = False
				CvalPageErr.ErrorMessage = ex.Message
			End Try
		End If

	End Sub

	Private Sub LoadData()
		Dim ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetPaged(MsKotaKabColumn.IDKotaKab.ToString & "=" & parID, "", 0, 1, Nothing)(0)
		 If  ObjMsKotaKab Is Nothing Then Throw New Exception("Data Not Found")
		With ObjMsKotaKab
			SafeDefaultValue = "-"
			 Try
                If .IDPropinsi <> "" Then lblIDPropinsi.Text = DataRepository.MsProvinceProvider.GetPaged("IdProvince= '" & CInt(.IDPropinsi) & "'", "", 0, 1, Nothing)(0).Nama Else lblIDPropinsi.Text = SafeDefaultValue
 Catch ex As Exception
     lblIDPropinsi.Text = SafeDefaultValue
 End Try
 lblIDKotaKab.Text = .IDKotaKab
lblNamaKotaKab.Text = Safe(.NamaKotaKab)

			Dim L_objMappingMsKotaKabNCBSPPATK As TList(Of MappingMsKotaKabNCBSPPATK)
			L_objMappingMsKotaKabNCBSPPATK = DataRepository.MappingMsKotaKabNCBSPPATKProvider.GetPaged(MappingMsKotaKabNCBSPPATKColumn.IDKotaKab.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)

			For Each i As MappingMsKotaKabNCBSPPATK In L_objMappingMsKotaKabNCBSPPATK
				Dim TempObj As MsKotaKab_NCBS = DataRepository.MsKotaKab_NCBSProvider.GetByIDKotaKab_NCBS(i.PK_MappingMsKotaKabNCBSPPATK_Id)
				  If TempObj Is Nothing Then Continue For
				Listmaping.Add(TempObj)
			Next
		End With
	End Sub

	Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
		Try
			BindListMapping()
		Catch ex As Exception
			LogError(ex)
			CvalPageErr.IsValid = False
			CvalPageErr.ErrorMessage = ex.Message
		End Try
	End Sub

#End Region

#Region "Property..."

	''' <summary>
	''' Menyimpan Item untuk mapping sementara
	''' </summary>
	''' <value></value>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Property Listmaping As TList(Of MsKotaKab_NCBS)
		Get
			Return Session("Listmaping.data")
		End Get
		Set(value As TList(Of MsKotaKab_NCBS))
			Session("Listmaping.data") = value
		End Set
	End Property

#End Region

#Region "events..."
	Protected Sub ImageCancel_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
		Response.Redirect("MsKotaKab_View.aspx")
	End Sub

	Protected Sub imgOk_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles imgOk.Click
		Try
			Page.Validate("handle")
			If Page.IsValid Then

				' =========== Insert Header Approval
				Dim KeyHeaderApproval As Integer
				Using ObjMsKotaKab_Approval As New MsKotaKab_Approval
					With ObjMsKotaKab_Approval
						.FK_MsMode_Id = 3
						FillOrNothing(.RequestedBy, SessionPkUserId)
						FillOrNothing(.RequestedDate, Date.Now)
                        .IsUpload = False
					End With
					DataRepository.MsKotaKab_ApprovalProvider.Save(ObjMsKotaKab_Approval)
					KeyHeaderApproval = ObjMsKotaKab_Approval.PK_MsKotaKab_Approval_Id
				End Using
				'============ Insert Detail Approval
				Using objMsKotaKab_ApprovalDetail As New MsKotaKab_ApprovalDetail()
					With objMsKotaKab_ApprovalDetail
						Dim ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(parID)
                        FillOrNothing(.IDPropinsi, lblIDPropinsi.Text, True, oInt)
FillOrNothing(.IDKotaKab, lblIDKotaKab.Text,true,Oint)
FillOrNothing(.NamaKotaKab, lblNamaKotaKab.Text,true,Ovarchar)

						FillOrNothing(.IDKotaKab, ObjMsKotaKab.IDKotaKab)
						FillOrNothing(.Activation, ObjMsKotaKab.Activation)
						FillOrNothing(.CreatedDate, ObjMsKotaKab.CreatedDate)
						FillOrNothing(.CreatedBy, ObjMsKotaKab.CreatedBy)
						FillOrNothing(.FK_MsKotaKab_Approval_Id, KeyHeaderApproval)
					End With
					DataRepository.MsKotaKab_ApprovalDetailProvider.Save(objMsKotaKab_ApprovalDetail)
					Dim keyHeaderApprovalDetail As Integer = objMsKotaKab_ApprovalDetail.PK_MsKotaKab_ApprovalDetail_Id

					'========= Insert mapping item 
					Dim LobjMappingMsKotaKabNCBSPPATK_Approval_Detail As New TList(Of MappingMsKotaKabNCBSPPATK_Approval_Detail)

                    Dim L_ObjMappingMsKotaKabNCBSPPATK As TList(Of MappingMsKotaKabNCBSPPATK) = DataRepository.MappingMsKotaKabNCBSPPATKProvider.GetPaged(MappingMsKotaKabNCBSPPATKColumn.idKotakab.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)

                    For Each objMappingMsKotaKabNCBSPPATK As MappingMsKotaKabNCBSPPATK In L_ObjMappingMsKotaKabNCBSPPATK
                        Dim objMappingMsKotaKabNCBSPPATK_Approval_Detail As New MappingMsKotaKabNCBSPPATK_Approval_Detail
                        With objMappingMsKotaKabNCBSPPATK_Approval_Detail
                            FillOrNothing(.IDKotaKab, objMappingMsKotaKabNCBSPPATK.PK_MappingMsKotaKabNCBSPPATK_Id)
                            FillOrNothing(.IDKotaKabNCBS, objMappingMsKotaKabNCBSPPATK.idKotakabNCBS)
                            FillOrNothing(.PK_MsKotaKab_Approval_Id, KeyHeaderApproval)
                            FillOrNothing(.PK_MappingMsKotaKabNCBSPPATK_Id, objMappingMsKotaKabNCBSPPATK.PK_MappingMsKotaKabNCBSPPATK_Id)
                            LobjMappingMsKotaKabNCBSPPATK_Approval_Detail.Add(objMappingMsKotaKabNCBSPPATK_Approval_Detail)
                        End With
                        DataRepository.MappingMsKotaKabNCBSPPATK_Approval_DetailProvider.Save(LobjMappingMsKotaKabNCBSPPATK_Approval_Detail)
                    Next
					'session Maping item Clear
					Listmaping.Clear()
                    Me.imgOk.Visible = False
                    Me.ImageCancel.Visible = False
					LblConfirmation.Text = "Data has been Delete and waiting for approval"
					MtvMsUser.ActiveViewIndex = 1
				End Using
			End If
		Catch ex As Exception
			LogError(ex)
			CvalPageErr.IsValid = False
			CvalPageErr.ErrorMessage = ex.Message
		End Try

	End Sub

	Protected Sub ImgBtnAdd_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAdd.Click
		Response.Redirect("MsKotaKab_View.aspx")
	End Sub

#End Region
	
End Class



