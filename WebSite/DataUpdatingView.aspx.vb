
Partial Class DataUpdatingView
    Inherits Parent

#Region " Property "

    ''' <summary>
    ''' selected item store
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("DataUpdatingViewSelected") Is Nothing, New ArrayList, Session("DataUpdatingViewSelected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("DataUpdatingViewSelected") = value
        End Set
    End Property
    ''' <summary>
    ''' setnget search field
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetFieldSearch() As String
        Get
            Return IIf(Session("DataUpdatingViewFieldSearch") Is Nothing, "", Session("DataUpdatingViewFieldSearch"))
        End Get
        Set(ByVal Value As String)
            Session("DataUpdatingViewFieldSearch") = Value
        End Set
    End Property
    ''' <summary>
    ''' search value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetValueSearch() As String
        Get
            Return IIf(Session("DataUpdatingViewValueSearch") Is Nothing, "", Session("DataUpdatingViewValueSearch"))
        End Get
        Set(ByVal Value As String)
            Session("DataUpdatingViewValueSearch") = Value
        End Set
    End Property
    ''' <summary>
    ''' sort expresion
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("DataUpdatingViewSort") Is Nothing, "WorkingUnitParent  asc", Session("DataUpdatingViewSort"))
        End Get
        Set(ByVal Value As String)
            Session("DataUpdatingViewSort") = Value
        End Set
    End Property
    ''' <summary>
    ''' current page index
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("DataUpdatingViewCurrentPage") Is Nothing, 0, Session("DataUpdatingViewCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("DataUpdatingViewCurrentPage") = Value
        End Set
    End Property
    ''' <summary>
    ''' total pages
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    ''' <summary>
    ''' row total
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("DataUpdatingViewRowTotal") Is Nothing, 0, Session("DataUpdatingViewRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("DataUpdatingViewRowTotal") = Value
        End Set
    End Property
    ''' <summary>
    ''' save bind table
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetBindTable() As Data.DataTable
        Get
            Return IIf(Session("DataUpdatingViewData") Is Nothing, New Data.DataTable, Session("DataUpdatingViewData"))
        End Get
        Set(ByVal value As Data.DataTable)
            Session("DataUpdatingViewData") = value
        End Set
    End Property
#End Region

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' fill search
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub FillSearch()
        Try
            Me.ComboSearch.Items.Add(New ListItem("...", ""))
            Me.ComboSearch.Items.Add(New ListItem("Created Date", "DataUpdatingDate >= '-=Search=- 00:00' AND DataUpdatingDate <= '-=Search=- 23:59'"))
            Me.ComboSearch.Items.Add(New ListItem("WorkingUnit Parent", "WorkingUnitParent Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Evaluating Branch", "WorkingUnit Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("CIF No", "CIFNo Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("CIF Name", "CIFName Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("AML/CFT Risk Rating", "DataUpdatingReason Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Sales Officer", "SalesOfficercode Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Last CDD Updated", "LastModified >= '-=Search=- 00:00' AND LastModified <= '-=Search=- 23:59'"))

        Catch
            Throw
        End Try
    End Sub

    Private Sub ClearThisPageSessions()
        Session("DataUpdatingViewSelected") = Nothing
        Session("DataUpdatingViewFieldSearch") = Nothing
        Session("DataUpdatingViewValueSearch") = Nothing
        Session("DataUpdatingViewSort") = Nothing
        Session("DataUpdatingViewCurrentPage") = Nothing
        Session("DataUpdatingViewRowTotal") = Nothing
        Session("DataUpdatingViewData") = Nothing
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not Me.IsPostBack Then
                Me.ClearThisPageSessions()

                Me.ComboSearch.Attributes.Add("onchange", "javascript:Check4Calendar(this, '" & Me.TextSearch.ClientID & "', '" & Me.popUp.ClientID & "', 1,8);")

                'Me.ComboSearch.Attributes.Add("onchange", "javascript:Check(this, 8, '" & Me.TextSearch.ClientID & "', '" & Me.popUp.ClientID & "');")
                Me.popUp.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TextSearch.ClientID & "'), 'dd-mmm-yyyy')")
                Me.popUp.Style.Add("display", "none")
                'Using AccessDataUpdating As New AMLDAL.DataUpdatingTableAdapters.Select_DataUpdatingTableAdapter
                '    Me.SetnGetBindTable = AccessDataUpdating.GetData
                'End Using
                'Me.GetDataBindGrid()
                Me.FillSearch()
                Me.GridMSUserView.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' page navigate button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try

            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
            Me.CollectSelected()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    'Public Function GetDataUpdatingView() As ArrayList
    '    Dim ArrWorkingUnit As New ArrayList
    '    Try
    '        Using AccessUserWorkingUnitAssignment As New AMLDAL.AMLDataSetTableAdapters.UserWorkingUnitAssignmentTableAdapter
    '            'Cari WorkingUnitID2 yg diassign ke UserID tsb
    '            Dim WorkingUnitsAssignedTable As AMLDAL.AMLDataSet.UserWorkingUnitAssignmentDataTable = AccessUserWorkingUnitAssignment.GetWorkingUnitsByUserID(Sahassa.AML.Commonly.SessionUserId)

    '            If WorkingUnitsAssignedTable.Rows.Count > 0 Then

    '                'Untuk Setiap WorkingUnitID yg diassign ke UserID tsb maka cari CountDataUpdatingPendingTaskList utk WorkingUnitID tsb lalu cari terus anaknya sampai berulang2
    '                For Each WorkingUnitsAssignedRow As AMLDAL.AMLDataSet.UserWorkingUnitAssignmentRow In WorkingUnitsAssignedTable.Rows
    '                    Dim CurrentWorkingUnitID As Int32 = WorkingUnitsAssignedRow.WorkingUnitID

    '                    'Cari anak2 dari WorkingUnitID yg skrg
    '                    Dim CurrentWorkingUnitChildrenTable As AMLDAL.AMLDataSet.WorkingUnitDataTable = GetChildWorkingUnitHierarchy(CurrentWorkingUnitID)
    '                    If CurrentWorkingUnitChildrenTable.Rows.Count > 0 Then
    '                        For Each CurrentWorkingUnitChildrenRow As AMLDAL.AMLDataSet.WorkingUnitRow In CurrentWorkingUnitChildrenTable.Rows
    '                            Using AccessDataUpdatingPending As New AMLDAL.DataUpdatingTableAdapters.SelectDataUpdatingPendingTaskListPerLevelSPTableAdapter
    '                                Dim objtable As Data.DataTable = AccessDataUpdatingPending.GetData(CurrentWorkingUnitChildrenRow.WorkingUnitID)

    '                                If objtable.Rows.Count > 0 Then
    '                                    Dim objRow As AMLDAL.DataUpdating.SelectDataUpdatingPendingTaskListPerLevelSPRow = objtable.Rows(0)
    '                                    ArrWorkingUnit.Add(objRow.WorkingUnitID)
    '                                End If
    '                            End Using
    '                        Next
    '                    Else
    '                        Using AccessDataUpdatingPending As New AMLDAL.DataUpdatingTableAdapters.SelectDataUpdatingPendingTaskListPerLevelSPTableAdapter
    '                            Dim objtable As Data.DataTable = AccessDataUpdatingPending.GetData(CurrentWorkingUnitID)

    '                            If objtable.Rows.Count > 0 Then
    '                                Dim objRow As AMLDAL.DataUpdating.SelectDataUpdatingPendingTaskListPerLevelSPRow = objtable.Rows(0)
    '                                ArrWorkingUnit.Add(objRow.WorkingUnitID)
    '                            End If
    '                        End Using
    '                    End If
    '                Next
    '            End If
    '        End Using
    '        Return ArrWorkingUnit
    '    Catch
    '        Throw
    '    End Try
    'End Function

    'Private Function GetChildWorkingUnitHierarchy(ByVal WorkingUnitId As Integer) As AMLDAL.AMLDataSet.WorkingUnitDataTable
    '    Dim WorkingUnitAdapter As New AMLDAL.AMLDataSetTableAdapters.WorkingUnitTableAdapter()
    '    Dim WorkingUnitTable As New AMLDAL.AMLDataSet.WorkingUnitDataTable
    '    Dim WorkingUnitChildTable As New AMLDAL.AMLDataSet.WorkingUnitDataTable
    '    Dim WorkingUnitReturnTable As New AMLDAL.AMLDataSet.WorkingUnitDataTable
    '    Dim WorkingUnitRow As AMLDAL.AMLDataSet.WorkingUnitRow

    '    WorkingUnitChildTable = WorkingUnitAdapter.GetWorkingUnitChildrenByWorkingUnitID(WorkingUnitId)
    '    For Each WorkingUnitRow In WorkingUnitChildTable.Rows
    '        WorkingUnitReturnTable = GetChildWorkingUnitHierarchy(WorkingUnitRow.WorkingUnitID)
    '        If WorkingUnitReturnTable.Rows.Count = 0 Then
    '            WorkingUnitTable.ImportRow(WorkingUnitRow)
    '        Else
    '            WorkingUnitTable.Merge(WorkingUnitReturnTable)
    '        End If
    '    Next
    '    Return WorkingUnitTable
    'End Function

    'Private Sub GetDataBindGrid()
    '    Try
    '        Dim dtTemp As New Data.DataTable
    '        Dim drTemp As Data.DataRow

    '        dtTemp.Columns.Add("DataUpdatingId", GetType(Integer))
    '        dtTemp.Columns.Add("WorkingUnitParent", GetType(String))
    '        dtTemp.Columns.Add("WorkingUnit", GetType(String))
    '        dtTemp.Columns.Add("AccountNo", GetType(String))
    '        dtTemp.Columns.Add("CIFNo", GetType(String))
    '        dtTemp.Columns.Add("AccountName", GetType(String))
    '        dtTemp.Columns.Add("DataUpdatingReason", GetType(String))
    '        dtTemp.Columns.Add("DataUpdatingDate", GetType(DateTime))

    '        Dim ArrWU As ArrayList = GetDataUpdatingView()
    '        Try
    '            Using AccessDataUpdating As New AMLDAL.DataUpdatingTableAdapters.Select_DataUpdatingTableAdapter
    '                For i As Integer = 0 To ArrWU.Count - 1
    '                    Using dtDataUpdating As Data.DataTable = AccessDataUpdating.GetData(CInt(ArrWU(i)))
    '                        If dtDataUpdating.Rows.Count > 0 Then
    '                            For Each RowDataUpdating As AMLDAL.DataUpdating.Select_DataUpdatingRow In dtDataUpdating.Rows
    '                                drTemp = dtTemp.NewRow
    '                                drTemp(0) = RowDataUpdating.DataUpdatingId
    '                                drTemp(1) = RowDataUpdating.WorkingUnitParent
    '                                drTemp(2) = RowDataUpdating.WorkingUnit
    '                                drTemp(3) = RowDataUpdating.AccountNo
    '                                drTemp(4) = RowDataUpdating.CIFNo
    '                                drTemp(5) = RowDataUpdating.AccountName
    '                                drTemp(6) = RowDataUpdating.DataUpdatingReason
    '                                drTemp(7) = RowDataUpdating.DataUpdatingDate
    '                                dtTemp.Rows.Add(drTemp)
    '                            Next
    '                        End If
    '                    End Using
    '                Next
    '                Me.SetnGetBindTable = dtTemp
    '            End Using
    '        Catch
    '            Throw
    '        End Try
    '    Catch
    '        Throw
    '    End Try
    'End Sub

    Private Sub GetDataBindGrid()

        Using AccessDataUpdating As New AMLDAL.DataUpdatingTableAdapters.Select_DataUpdating_ByHierarchyTableAdapter
            Using dtDataUpdating As Data.DataTable = AccessDataUpdating.GetData(Sahassa.AML.Commonly.SessionUserId)
                Me.SetnGetBindTable = dtDataUpdating
            End Using
        End Using
    End Sub
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' bind grid
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Sub BindGrid()
        Try
            Me.GetDataBindGrid()
            Me.GridMSUserView.DataSource = Me.GetDataGridFilter
            Me.SetnGetRowTotal = Me.GetDataGridFilter.Rows.Count
            Me.GridMSUserView.CurrentPageIndex = Me.SetnGetCurrentPage
            Me.GridMSUserView.VirtualItemCount = Me.SetnGetRowTotal
            Me.GridMSUserView.DataBind()
        Catch
            Throw
        End Try
    End Sub

    Private Function GetDataGridFilter() As Data.DataTable
        Dim dtTempFilter As New Data.DataTable
        dtTempFilter.Columns.Add("DataUpdatingId", GetType(Integer))
        dtTempFilter.Columns.Add("WorkingUnitParent", GetType(String))
        dtTempFilter.Columns.Add("WorkingUnit", GetType(String))
        dtTempFilter.Columns.Add("DataUpdatingDate", GetType(DateTime))

        dtTempFilter.Columns.Add("CIFNo", GetType(String))
        dtTempFilter.Columns.Add("CIFName", GetType(String))
        dtTempFilter.Columns.Add("DataUpdatingReason", GetType(String))
        dtTempFilter.Columns.Add("LastModified", GetType(DateTime))
        dtTempFilter.Columns.Add("SalesOfficercode", GetType(String))

        Try
            For Each DataRows As Data.DataRow In Me.SetnGetBindTable.Select(Me.SetnGetFieldSearch.Replace("-=Search=-", Me.SetnGetValueSearch.Replace("'", "''")), Me.SetnGetSort)
                dtTempFilter.ImportRow(DataRows)
            Next
        Catch
            Throw
        End Try
        Return dtTempFilter
    End Function
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' set navigate info
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.ComboSearch.SelectedValue = Me.SetnGetFieldSearch
            Me.TextSearch.Text = Me.SetnGetValueSearch
        Catch
            Throw
        End Try
    End Sub

    Protected Sub GridMSUserView_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridMSUserView.EditCommand
        Try
            Me.Response.Redirect("DataUpdatingViewDetail.aspx?CIFNo=" & e.Item.Cells(4).Text & "&CIFName=" & e.Item.Cells(5).Text & "&DataUpdatingReason=" & e.Item.Cells(6).Text & "&DataUpdatingDate=" & e.Item.Cells(7).Text, False)
        Catch
            Throw
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' change sort expression
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridMSUserView.SortCommand
        Dim GridUser As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' image button search
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Try
            'Dim AccountRegex As RegularExpressions.Regex
            ''c.Match(Me.TextBox1.Text, "/b/d+/b")
            'AccountRegex = New Regex("\b\d+\b")

            'If Me.ComboSearch.SelectedIndex = 3 Then
            '    If Not AccountRegex.IsMatch(Me.TextSearch.Text) Then
            '        Throw New Exception("Account No must numeric.")
            '    End If
            'End If
            CollectSelected()
            Me.SetnGetFieldSearch = Me.ComboSearch.SelectedValue
            If Me.ComboSearch.SelectedIndex = 0 Then
                Me.TextSearch.Text = ""
                Me.SetnGetValueSearch = Me.TextSearch.Text
            ElseIf Me.ComboSearch.SelectedIndex = 7 Then
                Dim DateSearch As DateTime
                Try
                    DateSearch = DateTime.Parse(Me.TextSearch.Text, New System.Globalization.CultureInfo("id-ID"))
                Catch ex As ArgumentOutOfRangeException
                    Throw New Exception("Input character cannot be convert to datetime.")
                Catch ex As FormatException
                    Throw New Exception("Unknown format date")
                End Try
                Me.SetnGetValueSearch = DateSearch.ToString("dd-MMM-yyyy")
            Else
                Me.SetnGetValueSearch = Me.TextSearch.Text
            End If
            Me.SetnGetValueSearch = Me.TextSearch.Text
            Me.SetnGetCurrentPage = 0
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' go button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	14/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be a positive number greater than zero.")
                End If
            Else
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    ''' <summary>
    ''' collect sub
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = gridRow.FindControl("CheckBoxExporttoExcel")
                Dim DataUpdatingId As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
                If ArrTarget.Contains(DataUpdatingId) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(DataUpdatingId)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(DataUpdatingId)
                End If
                Me.SetnGetSelectedItem = ArrTarget
            End If
        Next
    End Sub

    ''' <summary>
    ''' prerender event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    ''' <summary>
    ''' clear all control except control
    ''' </summary>
    ''' <param name="control">excluded control</param>
    ''' <remarks></remarks>
    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub 'ClearControls

    ''' <summary>
    ''' bind selected item
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub BindSelected()

        Try
            Dim dtTempFilter As New Data.DataTable
            dtTempFilter.Columns.Add("DataUpdatingId", GetType(Integer))
            dtTempFilter.Columns.Add("DataUpdatingDate", GetType(DateTime))
            dtTempFilter.Columns.Add("WorkingUnitParent", GetType(String))
            dtTempFilter.Columns.Add("WorkingUnit", GetType(String))
            dtTempFilter.Columns.Add("CIFNo", GetType(String))
            dtTempFilter.Columns.Add("cifName", GetType(String))
            dtTempFilter.Columns.Add("DataUpdatingReason", GetType(String))

            dtTempFilter.Columns.Add("LastModified", GetType(DateTime))
            dtTempFilter.Columns.Add("SalesOfficercode", GetType(String))
            Try
                For Each IdPk As Int32 In Me.SetnGetSelectedItem
                    For Each DataRows As Data.DataRow In Me.SetnGetBindTable.Select("DataUpdatingId = '" & IdPk & "'")
                        dtTempFilter.ImportRow(DataRows)
                    Next
                Next
            Catch ex As Exception
                Me.cvalPageError.IsValid = False
                Me.cvalPageError.ErrorMessage = ex.Message
            End Try
            Me.GridMSUserView.DataSource = dtTempFilter
            Me.GridMSUserView.AllowPaging = False
            Me.GridMSUserView.DataBind()

            'Sembunyikan kolom ke 0,1,4 & 5 agar tidak ikut diekspor ke excel
            Me.GridMSUserView.Columns(0).Visible = False
            Me.GridMSUserView.Columns(1).Visible = False
            'Me.GridMSUserView.Columns(9).Visible = False

        Catch
            Throw
        End Try
    End Sub

    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

        ' Verifies that the control is rendered 

    End Sub
    ''' <summary>
    ''' export button 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Me.CollectSelected()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelected()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=DataUpdatingView.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridMSUserView)
            GridMSUserView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get item bound
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridMSUserView.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Dim chkBox As CheckBox = e.Item.FindControl("CheckBoxExporttoExcel")
                chkBox.Checked = Me.SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)
                Dim objtemp As Data.DataRowView = e.Item.DataItem
                CType(e.Item.FindControl("Lnkcif"), LinkButton).Text = objtemp("cifno")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' select all
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(1).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItem.Contains(StrKey) = False Then
                        ArrTarget.Add(StrKey)
                    End If
                Else
                    If Me.SetnGetSelectedItem.Contains(StrKey) Then
                        ArrTarget.Remove(StrKey)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItem = ArrTarget
    End Sub

    Sub BindExportAll()
        Try
            Dim dtTempFilter As New Data.DataTable
            dtTempFilter.Columns.Add("DataUpdatingId", GetType(Integer))
            dtTempFilter.Columns.Add("DataUpdatingDate", GetType(DateTime))
            dtTempFilter.Columns.Add("WorkingUnitParent", GetType(String))
            dtTempFilter.Columns.Add("WorkingUnit", GetType(String))

            dtTempFilter.Columns.Add("CIFNo", GetType(String))
            dtTempFilter.Columns.Add("CIFName", GetType(String))
            dtTempFilter.Columns.Add("DataUpdatingReason", GetType(String))

            dtTempFilter.Columns.Add("LastModified", GetType(DateTime))
            dtTempFilter.Columns.Add("SalesOfficercode", GetType(String))
            Try
                For Each DataRows As Data.DataRow In Me.SetnGetBindTable.Select(Me.SetnGetFieldSearch.Replace("-=Search=-", Me.SetnGetValueSearch.Replace("'", "''")), Me.SetnGetSort)
                    dtTempFilter.ImportRow(DataRows)
                Next

            Catch ex As Exception
                Me.cvalPageError.IsValid = False
                Me.cvalPageError.ErrorMessage = ex.Message
            End Try
            Me.GridMSUserView.DataSource = dtTempFilter
            Me.GridMSUserView.AllowPaging = False
            Me.GridMSUserView.DataBind()

            'Sembunyikan kolom ke 0,1,4 & 5 agar tidak ikut diekspor ke excel
            Me.GridMSUserView.Columns(0).Visible = False
            Me.GridMSUserView.Columns(1).Visible = False
            'Me.GridMSUserView.Columns(8).Visible = False

        Catch
            Throw
        End Try
    End Sub
      
    Protected Sub LnkBtnExportAllToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LnkBtnExportAllToExcel.Click

        Try
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindExportAll()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=DataUpdateing.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridMSUserView)
            GridMSUserView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()

        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub LnkCIF_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim objlinkcif As LinkButton = CType(sender, LinkButton)
            Response.Redirect("customerInformationDetail.aspx?CIFNo=" & objlinkcif.Text, False)

        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub Lnkcifdetail_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try


            Dim objlinkcif As LinkButton = CType(sender, LinkButton)

            Response.Redirect("LastModifiedCIFEdit.aspx?CIFNo=" & objlinkcif.ToolTip & "&sumber=1 &dataupdatingid=" & CType(objlinkcif.NamingContainer, DataGridItem).Cells(1).Text, False)

        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
End Class
