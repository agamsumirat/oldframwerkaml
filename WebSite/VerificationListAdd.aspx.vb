Imports System.Windows.Forms
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Imports System.Text.RegularExpressions

Partial Class VerificationListAdd
    Inherits Parent

#Region " Insert Verification List dan Audit trail"
    Private Sub InsertVerificationListBySU()
        Dim oSQLTrans As SqlTransaction = Nothing

        Try
            Dim DateOfData As Nullable(Of DateTime)
            If Me.TextDateOfData.Text.Trim.Length <> 0 Then
                Try
                    DateOfData = DateTime.Parse(Me.TextDateOfData.Text, New System.Globalization.CultureInfo("id-ID"))
                Catch ex As ArgumentOutOfRangeException
                    Throw New Exception("DateOfData: Input character cannot be convert to datetime.")
                Catch ex As FormatException
                    Throw New Exception("DateOfData: Unknown format date")
                End Try
            End If

            Dim objTableAliases As Data.DataTable = CType(Session("AliasesDatatable_Add"), DataTable)
            If objTableAliases.Rows.Count = 0 Then
                Throw New Exception("You need to enter at least one Name/Alias for the Verification List.")
            End If

            Dim objTableAddresses As Data.DataTable = CType(Session("AddressesDatatable_Add"), DataTable)
            Dim objTableIDNos As Data.DataTable = CType(Session("IDNosDatatable_Add"), DataTable)
            Dim objTableCustomRemarks As Data.DataTable = CType(Session("CustomRemarksDatatable_Add"), DataTable)
            Dim DisplayName As String = objTableAliases.Rows(0)("Aliases").ToString

            Dim DOB As Nullable(Of DateTime)
            If Me.TextCustomerDOB.Text.Trim.Length <> 0 Then
                Try
                    DOB = DateTime.Parse(Me.TextCustomerDOB.Text, New System.Globalization.CultureInfo("id-ID"))
                Catch ex As ArgumentOutOfRangeException
                    Throw New Exception("DateOfBirth: Input character cannot be convert to datetime.")
                Catch ex As FormatException
                    Throw New Exception("DateOfBirth: Unknown format date")
                End Try
            End If
            If TxtBirthPlace.Text.Trim = "" Then
                Throw New Exception("Please Enter Birth Place.")
            End If

            Dim ListType As String = Me.DropDownListType.SelectedValue
            Dim CategoryID As Int32 = Me.DropDownCategoryID.SelectedValue
            Dim RefId As String = ""

            Dim CustomerRemarks As String() = New String(4) {"", "", "", "", ""}
            Dim i As Integer = 0

            For Each row As Data.DataRow In objTableCustomRemarks.Rows
                CustomerRemarks(i) = CType(row("CustomRemarks"), String)
                i += 1
            Next

            Dim VerificationListID As Int64

            'Using TransScope As New Transactions.TransactionScope
            Using AccessVerificationList As New AMLDAL.AMLDataSetTableAdapters.VerificationList_MasterTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessVerificationList, Data.IsolationLevel.ReadUncommitted)
                VerificationListID = AccessVerificationList.InsertVerificationListMaster(Now, Now, DateOfData, DisplayName, DOB, ListType, CategoryID, CustomerRemarks(0), CustomerRemarks(1), CustomerRemarks(2), CustomerRemarks(3), CustomerRemarks(4), RefId, TxtBirthPlace.Text, TxtNationality.Text.Trim)
            End Using

            Using AccessVerificationListAlias As New AMLDAL.AMLDataSetTableAdapters.VerificationList_AliasTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessVerificationListAlias, oSQLTrans)
                For Each row As Data.DataRow In objTableAliases.Rows
                    AccessVerificationListAlias.Insert(VerificationListID, row("Aliases"))
                Next
            End Using

            Using AccessVerificationListAddress As New AMLDAL.AMLDataSetTableAdapters.VerificationList_AddressTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessVerificationListAddress, oSQLTrans)
                For Each row As Data.DataRow In objTableAddresses.Rows
                    AccessVerificationListAddress.Insert(VerificationListID, row("Addresses"), CInt(row("AddressType")), CBool(row("IsLocalAddress")))
                Next
            End Using

            Using AccessVerificationListIDNumber As New AMLDAL.AMLDataSetTableAdapters.VerificationList_IDNumberTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessVerificationListIDNumber, oSQLTrans)
                For Each row As Data.DataRow In objTableIDNos.Rows
                    AccessVerificationListIDNumber.Insert(VerificationListID, row("IDNos"))
                Next
            End Using

            oSQLTrans.Commit()
            'End Using
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub

    Private Sub InsertAuditTrail()
        Dim oSQLTrans As SqlTransaction = Nothing

        Try
            Dim DateOfData As Nullable(Of DateTime)
            If Me.TextDateOfData.Text.Trim.Length <> 0 Then
                Try
                    DateOfData = DateTime.Parse(Me.TextDateOfData.Text, New System.Globalization.CultureInfo("id-ID"))
                Catch ex As ArgumentOutOfRangeException
                    Throw New Exception("DateOfData: Input character cannot be convert to datetime.")
                Catch ex As FormatException
                    Throw New Exception("DateOfData: Unknown format date")
                End Try
            End If

            Dim objTableAliases As Data.DataTable = CType(Session("AliasesDatatable_Add"), DataTable)
            If objTableAliases.Rows.Count = 0 Then
                Throw New Exception("You need to enter at least one Name/Alias for the Verification List.")
            End If

            Dim objTableAddresses As Data.DataTable = CType(Session("AddressesDatatable_Add"), DataTable)
            Dim objTableIDNos As Data.DataTable = CType(Session("IDNosDatatable_Add"), DataTable)
            Dim objTableCustomRemarks As Data.DataTable = CType(Session("CustomRemarksDatatable_Add"), DataTable)
            Dim DisplayName As String = objTableAliases.Rows(0)("Aliases").ToString

            Dim DOB As Nullable(Of DateTime)
            If Me.TextCustomerDOB.Text.Trim.Length <> 0 Then
                Try
                    DOB = DateTime.Parse(Me.TextCustomerDOB.Text, New System.Globalization.CultureInfo("id-ID"))
                Catch ex As ArgumentOutOfRangeException
                    Throw New Exception("DateOfBirth: Input character cannot be convert to datetime.")
                Catch ex As FormatException
                    Throw New Exception("DateOfBirth: Unknown format date")
                End Try
            End If

            Dim ListType As String = Me.DropDownListType.SelectedItem.Text
            Dim CategoryID As String = Me.DropDownCategoryID.SelectedItem.Text
            Dim RefId As String = ""

            Dim counter As Int32 = 13 + objTableAliases.Rows.Count + (3 * objTableAddresses.Rows.Count) + objTableIDNos.Rows.Count

            Dim CustomerRemarks As String() = New String(4) {"", "", "", "", ""}
            Dim i As Integer = 0

            For Each row As Data.DataRow In objTableCustomRemarks.Rows
                CustomerRemarks(i) = CType(row("CustomRemarks"), String)
                i += 1
            Next

            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(counter)
            'Using TransScope As New Transactions.TransactionScope
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAudit, Data.IsolationLevel.ReadUncommitted)
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List", "DateEntered", "Add", "", Now.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List", "DateUpdated", "Add", "", Now.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                If DateOfData.HasValue Then
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List", "DateOfData", "Add", "", DateOfData, "Accepted")
                Else
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List", "DateOfData", "Add", "", "", "Accepted")
                End If
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List", "DisplayName", "Add", "", DisplayName, "Accepted")

                If DOB.HasValue Then
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List", "DateOfBirth", "Add", "", DOB, "Accepted")
                Else
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List", "DateOfBirth", "Add", "", "", "Accepted")
                End If

                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List", "VerificationListTypeId", "Add", "", ListType, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List", "VerificationListCategoryId", "Add", "", CategoryID, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List", "CustomRemark1", "Add", "", CustomerRemarks(0), "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List", "CustomRemark2", "Add", "", CustomerRemarks(1), "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List", "CustomRemark3", "Add", "", CustomerRemarks(2), "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List", "CustomRemark4", "Add", "", CustomerRemarks(3), "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List", "CustomRemark5", "Add", "", CustomerRemarks(4), "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List", "RefId", "Add", "", RefId, "Accepted")

                For Each row As Data.DataRow In objTableAliases.Rows
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List Alias", "Name", "Add", "", row("Aliases"), "Accepted")
                Next

                For Each row As Data.DataRow In objTableAddresses.Rows
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List Address", "Address", "Add", "", row("Addresses"), "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List Address", "AddressTypeId", "Add", "", row("AddressType"), "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List Address", "IsLocalAddress", "Add", "", row("IsLocalAddress"), "Accepted")
                Next

                For Each row As Data.DataRow In objTableIDNos.Rows
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List ID Number", "Name", "Add", "", row("IDNos"), "Accepted")
                Next

                oSQLTrans.Commit()
            End Using
            'End Using
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub
#End Region

    ''' <summary>
    ''' cancel handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "VerificationListView.aspx"
        Me.Response.Redirect("VerificationListView.aspx", False)
    End Sub

    Private Sub ResetControls()
        Me.TextDateOfData.Text = ""
        Me.TextBoxAlias.Text = ""
        Me.TextCustomerDOB.Text = ""
        Me.TextAddress.Text = ""
        Me.TextboxCustomRemarks.Text = ""

        Me.DropDownListType.SelectedIndex = 0
        Me.DropDownCategoryID.SelectedIndex = 0
        Me.DropDownListAddressType.SelectedIndex = 0
        Me.CheckBoxIsLocalAddress.Checked = True

        Dim myDt As New DataTable()
        myDt = CreateDataTableAliases()
        Session("AliasesDatatable_Add") = myDt
        Me.GridViewAliases.DataSource = (CType(Session("AliasesDatatable_Add"), DataTable)).DefaultView
        Me.GridViewAliases.DataBind()
        Me.GridViewAliasesRow.Visible = False
        Me.SpacerAliases.Visible = False

        Dim myDt2 As New DataTable()
        myDt2 = CreateDataTableAddresses()
        Session("AddressesDatatable_Add") = myDt2
        Me.GridViewAddresses.DataSource = (CType(Session("AddressesDatatable_Add"), DataTable)).DefaultView
        Me.GridViewAddresses.DataBind()
        Me.GridViewAddressesRow.Visible = False
        Me.SpacerAddresses.Visible = False

        Dim myDt3 As New DataTable()
        myDt3 = CreateDataTableIDNos()
        Session("IDNosDatatable_Add") = myDt3
        Me.GridViewIDNos.DataSource = (CType(Session("IDNosDatatable_Add"), DataTable)).DefaultView
        Me.GridViewIDNos.DataBind()
        Me.GridViewIDNosRow.Visible = False
        Me.SpacerIDNos.Visible = False

        Dim myDt4 As New DataTable()
        myDt4 = CreateDataTableCustomRemarks()
        Session("CustomRemarksDatatable_Add") = myDt4
        Me.GridViewCustomRemarks.DataSource = (CType(Session("CustomRemarksDatatable_Add"), DataTable)).DefaultView
        Me.GridViewCustomRemarks.DataBind()
        Me.GridViewCustomRemarksRow.Visible = False

    End Sub

    ''' <summary>
    ''' update handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim oSQLTrans As SqlTransaction = Nothing
        Page.Validate()

        If Page.IsValid Then
            Try
                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                    Me.InsertAuditTrail()
                    Me.InsertVerificationListBySU()
                    Me.LblSuccess.Text = "A New Verification List has been added."
                    Me.LblSuccess.Visible = True
                    Me.ResetControls()
                Else
                    Dim DateOfData As Nullable(Of DateTime)
                    If Me.TextDateOfData.Text.Trim.Length <> 0 Then
                        Try
                            DateOfData = DateTime.Parse(Me.TextDateOfData.Text, New System.Globalization.CultureInfo("id-ID"))
                        Catch ex As ArgumentOutOfRangeException
                            Throw New Exception("DateOfData: Input character cannot be convert to datetime.")
                        Catch ex As FormatException
                            Throw New Exception("DateOfData: Unknown format date")
                        End Try
                    End If
                    'If TxtBirthPlace.Text.Trim = "" Then
                    '    Throw New Exception("Please Enter Birth Place.")
                    'End If
                    Dim objTableAliases As Data.DataTable = CType(Session("AliasesDatatable_Add"), DataTable)
                    If objTableAliases.Rows.Count = 0 Then
                        Throw New Exception("You need to enter at least one Name/Alias for the Verification List.")
                    End If

                    Dim objTableAddresses As Data.DataTable = CType(Session("AddressesDatatable_Add"), DataTable)
                    Dim objTableIDNos As Data.DataTable = CType(Session("IDNosDatatable_Add"), DataTable)
                    Dim objTableCustomRemarks As Data.DataTable = CType(Session("CustomRemarksDatatable_Add"), DataTable)

                    Dim DisplayName As String = objTableAliases.Rows(0)("Aliases").ToString
                    Dim ListType As String = Me.DropDownListType.SelectedValue
                    Dim CategoryID As Int32 = Me.DropDownCategoryID.SelectedValue

                    Dim DOB As Nullable(Of DateTime)
                    If Me.TextCustomerDOB.Text.Trim.Length <> 0 Then
                        Try
                            DOB = DateTime.Parse(Me.TextCustomerDOB.Text, New System.Globalization.CultureInfo("id-ID"))
                        Catch ex As ArgumentOutOfRangeException
                            Throw New Exception("DateOfBirth: Input character cannot be convert to datetime.")
                        Catch ex As FormatException
                            Throw New Exception("DateOfBirth: Unknown format date")
                        End Try
                    End If

                    'Using TranScope As New Transactions.TransactionScope
                    Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Master_PendingApprovalTableAdapter
                        oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessPending, Data.IsolationLevel.ReadUncommitted)
                        Using AccessApproval As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Master_ApprovalTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessApproval, oSQLTrans)
                            Dim PendingApprovalID As Int64 = AccessPending.InsertVerificationList_Master_PendingApproval(Sahassa.AML.Commonly.SessionUserId, Now, "Verification List Add", 1)

                            Dim CustomerRemarks As String() = New String(4) {"", "", "", "", ""}
                            Dim i As Integer = 0

                            For Each row As Data.DataRow In objTableCustomRemarks.Rows
                                CustomerRemarks(i) = CType(row("CustomRemarks"), String)
                                i += 1
                            Next

                            AccessApproval.Insert(PendingApprovalID, 1, 0, Now, Now, DateOfData, DisplayName, DOB, ListType, CategoryID, CustomerRemarks(0), CustomerRemarks(1), CustomerRemarks(2), CustomerRemarks(3), CustomerRemarks(4), Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, "", Nothing, TxtBirthPlace.Text.Trim, "", TxtNationality.Text.Trim, "")

                            Using AccessApprovalAliases As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Alias_ApprovalTableAdapter
                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessApprovalAliases, oSQLTrans)
                                For Each row As Data.DataRow In objTableAliases.Rows
                                    AccessApprovalAliases.Insert(PendingApprovalID, 1, 0, 0, row("Aliases"), Nothing, Nothing, Nothing)
                                Next
                            End Using

                            Using AccessApprovalAddresses As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Address_ApprovalTableAdapter
                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessApprovalAddresses, oSQLTrans)
                                For Each row As Data.DataRow In objTableAddresses.Rows
                                    AccessApprovalAddresses.Insert(PendingApprovalID, 1, 0, 0, row("Addresses"), CInt(row("AddressType")), CBool(row("IsLocalAddress")), Nothing, Nothing, Nothing, Nothing, True)
                                Next
                            End Using

                            Using AccessApprovalIDNos As New AMLDAL.AMLDataSetTableAdapters.VerificationList_IDNumber_ApprovalTableAdapter
                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessApprovalIDNos, oSQLTrans)
                                For Each row As Data.DataRow In objTableIDNos.Rows
                                    AccessApprovalIDNos.Insert(PendingApprovalID, 1, 0, 0, row("IDNos"), Nothing, Nothing, Nothing)
                                Next
                            End Using

                            oSQLTrans.Commit()

                            Dim MessagePendingID As Integer = 81001 'MessagePendingID 81001 = Verification List Add 
                            Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & DisplayName

                            Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & DisplayName, False)
                        End Using
                    End Using
                    'End Using
                End If
            Catch ex As Exception
                Me.cvalPageError.IsValid = False
                Me.cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
                If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Finally
                If Not oSQLTrans Is Nothing Then
                    oSQLTrans.Dispose()
                    oSQLTrans = Nothing
                End If
            End Try
        End If
    End Sub

    ''' <summary>
    ''' fill group
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillGroupAddressType()
        Using AccessAddressType As New AMLDAL.AMLDataSetTableAdapters.AddressTypeTableAdapter
            Me.DropDownListAddressType.DataSource = AccessAddressType.GetData
            Me.DropDownListAddressType.DataTextField = "AddressTypeDescription"
            Me.DropDownListAddressType.DataValueField = "AddressTypeId"
            Me.DropDownListAddressType.DataBind()
        End Using
    End Sub

    ''' <summary>
    ''' fill group
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillGroupCategory()
        Using AccessCategory As New AMLDAL.AMLDataSetTableAdapters.VerificationListCategoryTableAdapter
            Me.DropDownCategoryID.DataSource = AccessCategory.GetData
            Me.DropDownCategoryID.DataTextField = "CategoryName"
            Me.DropDownCategoryID.DataValueField = "CategoryID"
            Me.DropDownCategoryID.DataBind()
        End Using
    End Sub

    ''' <summary>
    ''' fill list type
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillGroupListType()
        Using AccessCategory As New AMLDAL.AMLDataSetTableAdapters.VerificationListTypeTableAdapter
            Me.DropDownListType.DataSource = AccessCategory.GetData
            Me.DropDownListType.DataTextField = "ListTypeName"
            Me.DropDownListType.DataValueField = "pk_Verification_List_Type_Id"
            Me.DropDownListType.DataBind()
        End Using
    End Sub


    ''' <summary>
    ''' page load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.FillGroupAddressType()
                Me.FillGroupCategory()
                Me.FillGroupListType()

                Dim myDt As New DataTable()
                myDt = CreateDataTableAliases()
                Session("AliasesDatatable_Add") = myDt
                Me.GridViewAliases.DataSource = (CType(Session("AliasesDatatable_Add"), DataTable)).DefaultView
                Me.GridViewAliases.DataBind()
                Me.GridViewAliasesRow.Visible = False
                Me.SpacerAliases.Visible = False

                Dim myDt2 As New DataTable()
                myDt2 = CreateDataTableAddresses()
                Session("AddressesDatatable_Add") = myDt2
                Me.GridViewAddresses.DataSource = (CType(Session("AddressesDatatable_Add"), DataTable)).DefaultView
                Me.GridViewAddresses.DataBind()
                Me.GridViewAddressesRow.Visible = False
                Me.SpacerAddresses.Visible = False

                Dim myDt3 As New DataTable()
                myDt3 = CreateDataTableIDNos()
                Session("IDNosDatatable_Add") = myDt3
                Me.GridViewIDNos.DataSource = (CType(Session("IDNosDatatable_Add"), DataTable)).DefaultView
                Me.GridViewIDNos.DataBind()
                Me.GridViewIDNosRow.Visible = False
                Me.SpacerIDNos.Visible = False

                Dim myDt4 As New DataTable()
                myDt4 = CreateDataTableCustomRemarks()
                Session("CustomRemarksDatatable_Add") = myDt4
                Me.GridViewCustomRemarks.DataSource = (CType(Session("CustomRemarksDatatable_Add"), DataTable)).DefaultView
                Me.GridViewCustomRemarks.DataBind()
                Me.GridViewCustomRemarksRow.Visible = False

                'Tambahkan event handler OnClick yang akan menampilkan kalender javascript
                Me.cmdDODDatePicker.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TextDateOfData.ClientID & "'), 'dd-mmm-yyyy')")
                Me.cmdDOBDatePicker.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TextCustomerDOB.ClientID & "'), 'dd-mmm-yyyy')")

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    '        Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Function CreateDataTableAliases() As DataTable
        Dim AliasesDatatable_Add As DataTable = New DataTable()

        Dim AliasesDataColumn As DataColumn

        AliasesDataColumn = New DataColumn()
        AliasesDataColumn.DataType = Type.GetType("System.String")
        AliasesDataColumn.ColumnName = "id"
        AliasesDatatable_Add.Columns.Add(AliasesDataColumn)

        AliasesDataColumn = New DataColumn()
        AliasesDataColumn.DataType = Type.GetType("System.String")
        AliasesDataColumn.ColumnName = "Aliases"
        AliasesDatatable_Add.Columns.Add(AliasesDataColumn)

        Return AliasesDatatable_Add
    End Function

    Private Sub AddAliasesToTable(ByVal id As String, ByVal aliases As String, ByRef myTable As DataTable)
        Dim row As DataRow

        row = myTable.NewRow()

        row("id") = id
        row("Aliases") = aliases

        myTable.Rows.Add(row)

    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' grid delete event handler
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    '''     [Hendry] 15/06/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridViewAliases_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridViewAliases.DeleteCommand
        Dim id As String = e.Item.Cells(0).Text

        Try
            Dim objTable As Data.DataTable = CType(Session("AliasesDatatable_Add"), DataTable)

            'hapus row tsb
            objTable.Rows.RemoveAt(e.Item.ItemIndex)
            Dim i As Integer = 1

            For Each Row As Data.DataRow In objTable.Rows
                Row("id") = i
                i += 1
            Next

            Session("AliasesDatatable_Add") = objTable

            Me.GridViewAliases.DataSource = CType(Session("AliasesDatatable_Add"), DataTable).DefaultView
            Me.GridViewAliases.DataBind()

            If objTable.Rows.Count = 0 Then
                Me.GridViewAliasesRow.Visible = False
                Me.SpacerAliases.Visible = False
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Function CreateDataTableAddresses() As DataTable
        Dim AddressesDatatable_Add As DataTable = New DataTable()

        Dim AddressesDataColumn As DataColumn

        AddressesDataColumn = New DataColumn()
        AddressesDataColumn.DataType = Type.GetType("System.String")
        AddressesDataColumn.ColumnName = "id"
        AddressesDatatable_Add.Columns.Add(AddressesDataColumn)

        AddressesDataColumn = New DataColumn()
        AddressesDataColumn.DataType = Type.GetType("System.String")
        AddressesDataColumn.ColumnName = "Addresses"
        AddressesDatatable_Add.Columns.Add(AddressesDataColumn)

        AddressesDataColumn = New DataColumn()
        AddressesDataColumn.DataType = Type.GetType("System.Int16")
        AddressesDataColumn.ColumnName = "AddressType"
        AddressesDatatable_Add.Columns.Add(AddressesDataColumn)

        AddressesDataColumn = New DataColumn()
        AddressesDataColumn.DataType = Type.GetType("System.String")
        AddressesDataColumn.ColumnName = "AddressTypeLabel"
        AddressesDatatable_Add.Columns.Add(AddressesDataColumn)

        AddressesDataColumn = New DataColumn()
        AddressesDataColumn.DataType = Type.GetType("System.Boolean")
        AddressesDataColumn.ColumnName = "IsLocalAddress"
        AddressesDatatable_Add.Columns.Add(AddressesDataColumn)

        Return AddressesDatatable_Add
    End Function

    Private Sub AddAddressesToTable(ByVal id As String, ByVal addresses As String, ByVal addresstype As Int16, ByVal AddressTypeLabel As String, ByVal islocaladdress As Boolean, ByRef myTable As DataTable)
        Dim row As DataRow

        row = myTable.NewRow()

        row("id") = id
        row("Addresses") = addresses
        row("AddressType") = addresstype
        row("AddressTypeLabel") = AddressTypeLabel
        row("IsLocalAddress") = islocaladdress

        myTable.Rows.Add(row)

    End Sub


    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' grid delete event handler
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    '''     [Hendry] 15/06/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridViewAddAddress_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridViewAddresses.DeleteCommand
        Dim id As String = e.Item.Cells(0).Text

        Try
            Dim objTable As Data.DataTable = CType(Session("AddressesDatatable_Add"), DataTable)

            'hapus row tsb
            objTable.Rows.RemoveAt(e.Item.ItemIndex)
            Dim i As Integer = 1

            For Each Row As Data.DataRow In objTable.Rows
                Row("id") = i
                i += 1
            Next

            Session("AddressesDatatable_Add") = objTable

            Me.GridViewAddresses.DataSource = CType(Session("AddressesDatatable_Add"), DataTable).DefaultView
            Me.GridViewAddresses.DataBind()

            If objTable.Rows.Count = 0 Then
                Me.GridViewAddressesRow.Visible = False
                Me.SpacerAddresses.Visible = False
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Function CreateDataTableIDNos() As DataTable
        Dim IDNosDatatable_Add As DataTable = New DataTable()

        Dim IDNosDataColumn As DataColumn

        IDNosDataColumn = New DataColumn()
        IDNosDataColumn.DataType = Type.GetType("System.String")
        IDNosDataColumn.ColumnName = "id"
        IDNosDatatable_Add.Columns.Add(IDNosDataColumn)

        IDNosDataColumn = New DataColumn()
        IDNosDataColumn.DataType = Type.GetType("System.String")
        IDNosDataColumn.ColumnName = "IDNos"
        IDNosDatatable_Add.Columns.Add(IDNosDataColumn)

        Return IDNosDatatable_Add
    End Function

    Private Sub AddIDNosToTable(ByVal id As String, ByVal idnos As String, ByRef myTable As DataTable)
        Dim row As DataRow

        row = myTable.NewRow()

        row("id") = id
        row("IDNos") = idnos

        myTable.Rows.Add(row)

    End Sub


    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' grid delete event handler
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    '''     [Hendry] 15/06/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridViewIDNos_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridViewIDNos.DeleteCommand
        Dim id As String = e.Item.Cells(0).Text

        Try
            Dim objTable As Data.DataTable = CType(Session("IDNosDatatable_Add"), DataTable)

            'hapus row tsb
            objTable.Rows.RemoveAt(e.Item.ItemIndex)
            Dim i As Integer = 1

            For Each Row As Data.DataRow In objTable.Rows
                Row("id") = i
                i += 1
            Next

            Session("IDNosDatatable_Add") = objTable

            Me.GridViewIDNos.DataSource = CType(Session("IDNosDatatable_Add"), DataTable).DefaultView
            Me.GridViewIDNos.DataBind()

            If objTable.Rows.Count = 0 Then
                Me.GridViewIDNosRow.Visible = False
                Me.SpacerIDNos.Visible = False
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Function CreateDataTableCustomRemarks() As DataTable
        Dim CustomRemarksDatatable_Add As DataTable = New DataTable()

        Dim CustomRemarksDataColumn As DataColumn

        CustomRemarksDataColumn = New DataColumn()
        CustomRemarksDataColumn.DataType = Type.GetType("System.String")
        CustomRemarksDataColumn.ColumnName = "id"
        CustomRemarksDatatable_Add.Columns.Add(CustomRemarksDataColumn)

        CustomRemarksDataColumn = New DataColumn()
        CustomRemarksDataColumn.DataType = Type.GetType("System.String")
        CustomRemarksDataColumn.ColumnName = "CustomRemarks"
        CustomRemarksDatatable_Add.Columns.Add(CustomRemarksDataColumn)

        Return CustomRemarksDatatable_Add
    End Function

    Private Sub AddCustomRemarksToTable(ByVal id As String, ByVal CustomRemarks As String, ByRef myTable As DataTable)
        Dim row As DataRow

        row = myTable.NewRow()

        row("id") = id
        row("CustomRemarks") = CustomRemarks

        myTable.Rows.Add(row)

    End Sub


    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' grid delete event handler
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    '''     [Hendry] 15/06/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridViewCustomRemarks_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridViewCustomRemarks.DeleteCommand
        Dim id As String = e.Item.Cells(0).Text

        Try
            Dim objTable As Data.DataTable = CType(Session("CustomRemarksDatatable_Add"), DataTable)

            'hapus row tsb
            objTable.Rows.RemoveAt(e.Item.ItemIndex)
            Dim i As Integer = 1

            For Each Row As Data.DataRow In objTable.Rows
                Row("id") = i
                i += 1
            Next

            Session("CustomRemarksDatatable_Add") = objTable

            Me.GridViewCustomRemarks.DataSource = CType(Session("CustomRemarksDatatable_Add"), DataTable).DefaultView
            Me.GridViewCustomRemarks.DataBind()

            If objTable.Rows.Count = 0 Then
                Me.GridViewCustomRemarksRow.Visible = False
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub LinkButtonAddAlias_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonAddAlias.Click
        If Me.TextBoxAlias.Text.Trim.Length <> 0 Then
            Try
                Me.GridViewAliasesRow.Visible = True
                Me.SpacerAliases.Visible = True

                'Proses semua item2 lama dlm DataGridAliases
                Dim objTable As Data.DataTable = CType(Session("AliasesDatatable_Add"), DataTable)
                AddAliasesToTable((objTable.Rows.Count + 1), Me.TextBoxAlias.Text.Trim(), objTable)
                Session("AliasesDatatable_Add") = objTable

                Me.GridViewAliases.DataSource = CType(Session("AliasesDatatable_Add"), DataTable).DefaultView

                Me.GridViewAliases.DataBind()

                Me.TextBoxAlias.Text = ""
            Catch ex As Exception
                Me.cvalPageError.IsValid = False
                Me.cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            End Try
        End If
    End Sub

    Protected Sub LinkButtonAddAddress_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonAddAddress.Click
        If Me.TextAddress.Text.Trim.Length <> 0 Then
            Try
                Me.GridViewAddressesRow.Visible = True
                Me.SpacerAddresses.Visible = True

                'Proses semua item2 lama dlm DataGridAliases
                Dim objTable As Data.DataTable = CType(Session("AddressesDatatable_Add"), DataTable)
                Dim Address As String
                If Me.TextAddress.Text.Length <= 255 Then
                    Address = Me.TextAddress.Text
                Else
                    Address = Me.TextAddress.Text.Substring(0, 255)
                End If

                AddAddressesToTable((objTable.Rows.Count + 1), Address, Me.DropDownListAddressType.SelectedValue, "(" & Me.DropDownListAddressType.SelectedItem.Text & ")", Me.CheckBoxIsLocalAddress.Checked, objTable)

                Session("AddressesDatatable_Add") = objTable

                Me.GridViewAddresses.DataSource = CType(Session("AddressesDatatable_Add"), DataTable).DefaultView

                Me.GridViewAddresses.DataBind()

                Me.TextAddress.Text = ""
                Me.DropDownListAddressType.SelectedIndex = 0
                Me.CheckBoxIsLocalAddress.Checked = True
            Catch ex As Exception
                Me.cvalPageError.IsValid = False
                Me.cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            End Try
        End If
    End Sub

    Protected Sub LinkButtonAddIDNumber_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonAddIDNumber.Click
        If Me.TextBoxIDNo.Text.Trim.Length <> 0 Then
            Try
                Me.GridViewIDNosRow.Visible = True
                Me.SpacerIDNos.Visible = True

                'Proses semua item2 lama dlm DataGridIDNos
                Dim objTable As Data.DataTable = CType(Session("IDNosDatatable_Add"), DataTable)
                AddIDNosToTable((objTable.Rows.Count + 1), Me.TextBoxIDNo.Text.Trim(), objTable)
                Session("IDNosDatatable_Add") = objTable

                Me.GridViewIDNos.DataSource = CType(Session("IDNosDatatable_Add"), DataTable).DefaultView
                Me.GridViewIDNos.DataBind()

                Me.TextBoxIDNo.Text = ""
            Catch ex As Exception
                Me.cvalPageError.IsValid = False
                Me.cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            End Try
        End If
    End Sub

    Protected Sub LinkButtonAddCustomRemarks_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonAddCustomRemarks.Click
        If Me.TextboxCustomRemarks.Text.Trim.Length <> 0 Then
            Try
                Dim objTable As Data.DataTable = CType(Session("CustomRemarksDatatable_Add"), DataTable)
                If objTable.Rows.Count < 5 Then
                    Me.GridViewCustomRemarksRow.Visible = True

                    Dim CustomRemarks As String
                    If Me.TextboxCustomRemarks.Text.Length <= 255 Then
                        CustomRemarks = Me.TextboxCustomRemarks.Text
                    Else
                        CustomRemarks = Me.TextboxCustomRemarks.Text.Substring(0, 255)
                    End If

                    'Proses semua item2 lama dlm DataGridIDNos
                    AddCustomRemarksToTable((objTable.Rows.Count + 1), CustomRemarks, objTable)
                    Session("CustomRemarksDatatable_Add") = objTable

                    Me.GridViewCustomRemarks.DataSource = CType(Session("CustomRemarksDatatable_Add"), DataTable).DefaultView
                    Me.GridViewCustomRemarks.DataBind()

                    Me.TextboxCustomRemarks.Text = ""
                Else
                    Me.TextboxCustomRemarks.Text = ""
                    Throw New Exception("Unable to add more than 5 Custom Remarks for this Verification List.")
                End If
            Catch ex As Exception
                Me.cvalPageError.IsValid = False
                Me.cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            End Try
        End If
    End Sub
End Class