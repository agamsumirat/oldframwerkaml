Partial Class CDDLNPMessage
    Inherits Parent

    Private ReadOnly Property strMessageID() As String
        Get
            Return Me.Request.Params("ID")
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.lblMessageID.Text = "[Message ID :" & strMessageID & "]"

            Select Case strMessageID.Length
                Case 5
                    'Format strMessageID : 5 chars
                    'Index 0 - 2 : Module ID
                    'Index 3 : Request Action ID
                    'Index 4 : Approval Action ID
                    'eg. 10431 : Request [delete] for [CDD Risk Fact Question] has been [submitted and it is currently waiting for approval.]
                    Me.lblMessage.Text = "Request " & Me.getRequestAction & " for " & Me.getModule & " has been " & Me.getApprovalAction

                Case Else
                    Select Case strMessageID
                        Case 404 : Me.lblMessage.Text = "Data not found."
                        Case 3441 : Me.lblMessage.Text = "You have submitted the CDD LNP form."
                        Case 3442 : Me.lblMessage.Text = "You have approved the CDD LNP form."
                        Case 3443 : Me.lblMessage.Text = "You have declined the CDD LNP form."
                        Case 3444 : Me.lblMessage.Text = "You have decided to return the CDD to the initiator for corrections."
                    End Select

            End Select

        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageButtonOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonOK.Click
        Try
            Select Case strMessageID.Length
                Case 5
                    Sahassa.AML.Commonly.SessionIntendedPage = Me.getIntendedPage()
                    Me.Response.Redirect(Sahassa.AML.Commonly.SessionIntendedPage, False)

                Case Else
                    Select Case strMessageID.Substring(0, 3).ToString
                        Case 344
                            Me.Response.Redirect("CDDLNPApproval.aspx", False)

                        Case Else
                            Me.Controls.Add(New LiteralControl("<script>window.history.go(-2);</script> "))

                    End Select

            End Select
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Function getModule() 'Karakter 1 sd 3
        Select Case strMessageID.Substring(0, 3).ToString
            Case 100 : Return "CDD LNP Mapping Question"
            Case 101 : Return "CDD LNP Question"
            Case 102 : Return "CDD LNP Workflow Parameter"
            Case 103 : Return "CDD LNP Workflow Upload"
            Case 104 : Return "CDD Risk Fact Question"
            Case 105 : Return "CDD LNP Header"
            Case Else : Return "[Unknown Module]"
        End Select
    End Function

    Private Function getRequestAction() 'Karakter ke 4
        Select Case strMessageID.Substring(3, 1).ToString
            Case 1 : Return "add"
            Case 2 : Return "edit"
            Case 3 : Return "delete"
            Case 4 : Return "activation"
            Case 5 : Return "upload"
            Case Else : Return "[Unknown Request Action]"
        End Select
    End Function

    Private Function getApprovalAction() 'Karakter ke 5
        Select Case strMessageID.Substring(4, 1).ToString
            Case 1 : Return "submitted and it is currently waiting for approval."
            Case 2 : Return "rejected"
            Case 3 : Return "accepted"
            Case Else : Return "[Unknown Approval Action]"
        End Select
    End Function

    Private Function getIntendedPage()
        Select Case strMessageID.Substring(4, 1).ToString
            Case 1 : Return Me.getModule.ToString.Replace(" ", String.Empty) & "View.aspx"
            Case 2, 3 : Return Me.getModule.ToString.Replace(" ", String.Empty) & "Approval.aspx"
            Case Else : Return "Default.aspx"
        End Select
    End Function
End Class