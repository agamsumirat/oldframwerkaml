Option Strict On
Option Explicit On
Imports amlbll
Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports SahassaNettier.Services
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Imports System.IO
Imports System.Collections.Generic

Partial Class MappingGroupMenuHiportJiveAdd
    Inherits Parent

    Public Property SetnGetMode() As String
        Get
            If Not Session("MappingGroupMenuHIPORTJIVEAdd.Mode") Is Nothing Then
                Return CStr(Session("MappingGroupMenuHIPORTJIVEAdd.Mode"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("MappingGroupMenuHIPORTJIVEAdd.Mode") = value
        End Set
    End Property

    Private Sub LoadMode()
        Session("MappingGroupMenuHIPORTJIVEAdd.Mode") = Nothing
        CboGroupMenu.Items.Clear()
        Using ObjHIPORTJIVEBll As New AMLBLL.MappingGroupMenuHiportJiveBLL
            CboGroupMenu.AppendDataBoundItems = True
            CboGroupMenu.DataSource = ObjHIPORTJIVEBll.GeModeData
            CboGroupMenu.DataTextField = GroupColumn.GroupName.ToString
            CboGroupMenu.DataValueField = GroupColumn.GroupID.ToString
            CboGroupMenu.DataBind()
            CboGroupMenu.Items.Insert(0, New ListItem("...", ""))
        End Using
    End Sub

    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim oTrans As SqlTransaction = Nothing
        Try
            Dim GroupID As Integer
            If CboGroupMenu.Text = "" Then
                GroupID = 0
            Else
                GroupID = CInt(CboGroupMenu.Text)
            End If

            Dim GroupName As String = ""
            If GroupID > 0 Then

                AMLBLL.MappingGroupMenuHiportJiveBLL.IsDataValidAddApproval(CStr(GroupID))

                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                    Using ObjMappingGroupMenuHIPORTJIVE As New MappingGroupMenuHiportJive
                        ObjMappingGroupMenuHIPORTJIVE.FK_Group_ID = GroupID
                        DataRepository.MappingGroupMenuHiportJiveProvider.Save(ObjMappingGroupMenuHIPORTJIVE)
                        Using ObjGroup As TList(Of Group) = DataRepository.GroupProvider.GetPaged("GroupID = '" & GroupID & "'", "", 0, Integer.MaxValue, 0)
                            AMLBLL.MappingGroupMenuHiportJiveBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "MappingGroupMenuHIPORTJIVE", "GroupName", "Add", "", ObjGroup(0).GroupName, "Acc")
                            Me.LblSuccess.Text = "Data saved successfully"
                            Me.LblSuccess.Visible = True
                            LoadMode()
                        End Using
                    End Using
                Else
                    Using ObjMappingGroupMenuHIPORTJIVE_Approval As New MappingGroupMenuHiportJive_Approval
                        Using ObjMappingGroupMenuHIPORTJIVEList As TList(Of MappingGroupMenuHiportJive) = DataRepository.MappingGroupMenuHiportJiveProvider.GetPaged("FK_Group_ID = '" & GroupID & "'", "", 0, Integer.MaxValue, 0)
                            If ObjMappingGroupMenuHIPORTJIVEList.Count > 0 Then
                                Sahassa.AML.Commonly.SessionIntendedPage = "MappingGroupMenuHIPORTJIVEView.aspx"
                                Me.Response.Redirect("MappingGroupMenuHIPORTJIVEView.aspx", False)

                            Else
                                Using objgroup As TList(Of Group) = DataRepository.GroupProvider.GetPaged("GroupID = '" & GroupID & "'", "", 0, Integer.MaxValue, 0)
                                    GroupName = objgroup(0).GroupName
                                    AMLBLL.MappingGroupMenuHiportJiveBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "MappingGroupMenuHIPORTJIVE", "GroupName", "Add", "", GroupName, "PendingApproval")
                                    ObjMappingGroupMenuHIPORTJIVE_Approval.FK_MsUserID = Sahassa.AML.Commonly.SessionPkUserId
                                    ObjMappingGroupMenuHIPORTJIVE_Approval.GroupMenu = GroupName
                                    ObjMappingGroupMenuHIPORTJIVE_Approval.FK_ModeID = CInt(Sahassa.AML.Commonly.TypeMode.Add)
                                    ObjMappingGroupMenuHIPORTJIVE_Approval.CreatedDate = Now
                                    DataRepository.MappingGroupMenuHiportJive_ApprovalProvider.Save(ObjMappingGroupMenuHIPORTJIVE_Approval)

                                    Using ObjMappingGroupMenuHIPORTJIVE_approvalDetail As New MappingGroupMenuHiportJive_ApprovalDetail
                                        ObjMappingGroupMenuHIPORTJIVE_approvalDetail.FK_MappingGroupMenuHiportJive_Approval_ID = ObjMappingGroupMenuHIPORTJIVE_Approval.PK_MappingGroupMenuHiportJive_Approval_ID
                                        ObjMappingGroupMenuHIPORTJIVE_approvalDetail.FK_Group_ID = GroupID
                                        DataRepository.MappingGroupMenuHiportJive_ApprovalDetailProvider.Save(ObjMappingGroupMenuHIPORTJIVE_approvalDetail)
                                    End Using
                                End Using
                                Using ObjGroup As TList(Of Group) = DataRepository.GroupProvider.GetPaged("GroupID = '" & GroupID & "'", "", 0, Integer.MaxValue, 0)
                                    Me.LblSuccess.Text = "Data saved successfully and waiting for Approval."
                                    Me.LblSuccess.Visible = True
                                    LoadMode()
                                End Using
                            End If
                        End Using

                    End Using
                End If
            Else
                Me.LblSuccess.Text = "Please select Group Menu"
                Me.LblSuccess.Visible = True
            End If
            'End If

        Catch ex As Exception
            If Not oTrans Is Nothing Then oTrans.Rollback()
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not oTrans Is Nothing Then
                oTrans.Dispose()
                oTrans = Nothing
            End If
        End Try
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "MappingGroupMenuHIPORTJIVEView.aspx"
            Me.Response.Redirect("MappingGroupMenuHIPORTJIVEView.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'Using Transcope As New Transactions.TransactionScope
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                '    Transcope.Complete()
            End Using
            If Not Page.IsPostBack Then
                LoadMode()
            End If
            'End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class

