<%@ Register Src="webcontrol/footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Src="webcontrol/menu.ascx" TagName="menu" TagPrefix="uc1" %>
<%@ Register Src="webcontrol/userinfo.ascx" TagName="userinfo" TagPrefix="uc1" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CaseManagementWorkflowDetail.aspx.vb" Inherits="CaseManagementWorkflowDetail" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>:: Anti Money Laundering ::</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="theme/aml.css" rel="stylesheet" type="text/css">
	<link href="theme/popcalendar.css" type="text/css" rel="stylesheet">
	<script src="script/x_load.js"></script>
	<script src="script/stm31.js"></script>
	<script language="javascript">xInclude('script/x_core.js');</script>
	<script language="javascript" src="script/popcalendar.js"></script>
	<script language="javascript" src="script/aml.js" ></script>
	<link href="TabCss.css" rel="stylesheet" type="text/css">
	<script language=javascript>

function popWin(llInp)
{
    var clientId = llInp
    window.open("popupwindow.aspx?ClientId=" + clientId,"aa","width=300,height=300,scrollbars=yes");
    return false;
}
</script>
</head>

<body leftmargin="0" topmargin="0" scroll="yes" background="images/main-contentbg.gif">
<form id="form1" runat="server">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
  	<td id="header"><table background="images/main-header-bg.gif" height="81" width="100%" cellpadding="0" cellspacing="0" border="0">
  	    <tr>
            <td width="153"><img src="images/main-uang-dijemur-dan-logo.jpg" width="153" height="81" /></td>
  	        <td width="357"><img src="images/main-title.jpg" width="357" height="25" /></td>
  	        <td width="99%">&nbsp;</td>
  	        <td align="right" background="images/main-bar-merah-kanan.jpg" valign="top" class="toprightmenu"><img src="images/blank.gif" width="425" height="6"><br>
                  <uc1:userinfo ID="UserInfo1" runat="server" />
                </td>
  	    </tr>
  	</table></td>
  </tr>
  <tr>
  	  <td><uc1:menu ID="Menu1" runat="server" /></td>
  </tr>
  <tr>
  	  <td id="tdcontent" height="99%" valign="top"><div id="divcontent" class="divcontent" ><table cellpadding="0" cellspacing="0" border="0" width="100%">
	  	<tr>
			<td background="images/main-menu-bg-shadow.gif" height="10"></td>
		</tr>
		<tr>
			<td class="divcontentinside">
			<ajax:ajaxpanel id="a" runat="server"  Width="100%"> <asp:validationsummary id="ValidationSummary1" runat="server" HeaderText="There were errors on the page:" Width="100%" CssClass="validation" ></asp:validationsummary>
                
 <table id="title"  border="2" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr>
            <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>
                    <asp:Label ID="LblHeader" runat="server" Text="Case Management Workflow - Detail "></asp:Label>
                    <hr />
                </strong>
                <asp:Label ID="LblSuccess" runat="server" CssClass="validationok" Visible="False"
                    Width="93%">Success to save case management workflow.</asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" colspan="4" style="border-top-style: none; border-right-style: none;
                border-left-style: none; height: 24px; border-bottom-style: none"><span style="color: #ff0000">* Required<asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></span></td>
        </tr>
    </table>
    <asp:MultiView ID="MultiViewCaseManagement" runat="server" ActiveViewIndex="0">
        <asp:View ID="CaseManagementWorkflow" runat="server">
        <table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2" height="72" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
        <tr class="formText">
            <td bgcolor="#ffffff" width="2%">
            </td>
            <td bgcolor="#ffffff" width="10%">
                Account Owner</td>
            <td bgcolor="#ffffff" width="1%">
                :</td>
            <td bgcolor="#ffffff" colspan="3">
                <asp:Label ID="LblAccountOwner" runat="server"></asp:Label></td>
        </tr>
                <tr class="formText">
            <td bgcolor="#ffffff" width="2%">
            </td>
            <td bgcolor="#ffffff" width="10%">
                Alert Case Description</td>
            <td bgcolor="#ffffff" width="1%">
                :</td>
            <td bgcolor="#ffffff" colspan="3">
                <asp:Label ID="LblAlertCaseDescription" runat="server"></asp:Label></td>
        </tr>
            <tr class="formText">
            <td bgcolor="#ffffff" width="2%">
            </td>
            <td bgcolor="#ffffff" width="10%">
                VIP Code</td>
            <td bgcolor="#ffffff" width="1%">
                :</td>
            <td bgcolor="#ffffff" colspan="3">
                <asp:Label ID="LblVipCode" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" width="2%">
            </td>
            <td bgcolor="#ffffff" width="10%">
                Insider Code</td>
            <td bgcolor="#ffffff" width="1%">
                :</td>
            <td bgcolor="#ffffff" colspan="3">
                <asp:Label ID="LblInsiderCode" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" width="2%">
            </td>
            <td bgcolor="#ffffff" width="10%">
                Segment</td>
            <td bgcolor="#ffffff" width="1%">
                :</td>
            <td bgcolor="#ffffff" colspan="3">
                <asp:Label ID="LblSegment" runat="server"></asp:Label></td>
        </tr>
             <tr class="formText">
            <td bgcolor="#ffffff" width="2%">
            </td>
            <td bgcolor="#ffffff" width="10%">
                SBU</td>
            <td bgcolor="#ffffff" width="1%">
                :</td>
            <td bgcolor="#ffffff" colspan="3">
                <asp:Label ID="LblSBU" runat="server"></asp:Label></td>
        </tr>
             <tr class="formText">
            <td bgcolor="#ffffff" width="2%">
            </td>
            <td bgcolor="#ffffff" width="10%">
                Sub - SBU</td>
            <td bgcolor="#ffffff" width="1%">
                :</td>
            <td bgcolor="#ffffff" colspan="3">
                <asp:Label ID="LblSubSBU" runat="server"></asp:Label></td>
        </tr>
             <tr class="formText">
            <td bgcolor="#ffffff" width="2%">
            </td>
            <td bgcolor="#ffffff" width="10%">
                RM</td>
            <td bgcolor="#ffffff" width="1%">
                :</td>
            <td bgcolor="#ffffff" colspan="3">
                <asp:Label ID="LblRM" runat="server"></asp:Label></td>
        </tr>
         <tr class="formText">
            <td bgcolor="#ffffff" width="2%">
            </td>
            <td bgcolor="#ffffff" width="10%">
                Status</td>
            <td bgcolor="#ffffff" width="1%">
                :</td>
            <td bgcolor="#ffffff" colspan="3">
                <asp:Label ID="LblStatus" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff">
            </td>
            <td bgcolor="#ffffff" colspan="5">
                Workflow</td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff">
            </td>
            <td bgcolor="#ffffff" width="10%">
                &nbsp; &nbsp;&nbsp; # of Serial</td>
            <td bgcolor="#ffffff" width="1%">
                :</td>
            <td bgcolor="#ffffff" width="8%">&nbsp;
                <asp:textbox id="TxtJumlahSerial" runat="server" CssClass="textBox" MaxLength="50" Width="31px" ></asp:textbox>
                <strong><span style="color: #ff0000">*</span></strong></td>
            <td bgcolor="#ffffff" colspan="2">
                &nbsp;<strong><span style="color: #ff0000">
                    <asp:ImageButton ID="ImageButtonUpdate" runat="server" SkinID="UpdateButton" />
                    </span></strong></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" colspan="6" style="height: 24px">
            <ajax:AjaxPanel ID="GridAjax" runat="server">
                <hr style="font-size: 1px" />
                <asp:GridView ID="GridViewParent" runat="server" AutoGenerateColumns="False" BackColor="White"
                    BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black"
                    GridLines="Vertical" Width="100%">
                    <Columns>
                <asp:BoundField DataField="IDSerial" HeaderText="# Serial">
                    <ItemStyle VerticalAlign="Top" HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="Workflow Step">
                    <ItemTemplate>
                        <asp:TextBox ID="TxtWorkflowStep" runat="server" Width="97px" CssClass="textbox"></asp:TextBox>
                    </ItemTemplate>
                    <ItemStyle VerticalAlign="Top" HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="# of Paralel">
                    <ItemTemplate>
                        <table border="0">
                            <tr>
                                <td>
                        <asp:TextBox ID="TxtofParalel" runat="server" Width="37px" CssClass="textbox"></asp:TextBox></td>
                                <td>
                        <asp:ImageButton ID="ImageButtonParalel" runat="server" OnClick="ImageButtonParalel_Click"
                            SkinID="GoButton" /></td>
                            </tr>
                        </table>
                    </ItemTemplate>
                    <ItemStyle VerticalAlign="Top" HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Approvers">
                    <ItemTemplate>
                        <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" ShowHeader="False" GridLines="None" >
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:TextBox ID="TxtApprovers" runat="server" Enabled="False" CssClass="textbox"></asp:TextBox>&nbsp;
                                        <asp:Button ID="ButtonApprovers" runat="server" Text="..." OnInit="ButtonApprovers_Init" CssClass="buttonicon"  />&nbsp;
                                    </ItemTemplate>
                                    <ItemStyle VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="RowIndexApprovers">
                                    <ItemStyle Font-Size="1px" ForeColor="White" />
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        &nbsp;
                    </ItemTemplate>
                    <ItemStyle VerticalAlign="Top" HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Due Date (days)">
                    <ItemTemplate>
                        <asp:GridView ID="GridView4" runat="server" AutoGenerateColumns="False" ShowHeader="False" GridLines="None">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:TextBox ID="TxtDueDate" runat="server" Width="34px" CssClass="textbox"></asp:TextBox>
                                    </ItemTemplate>
                                    <ItemStyle VerticalAlign="Top" HorizontalAlign="Center" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Notify Others" ShowHeader="False" >
                    <ItemTemplate>
                        <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" ShowHeader="False" Width="100%" GridLines="None">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:TextBox ID="TxtNotifyOther" runat="server" CssClass="textbox" Enabled="false" ></asp:TextBox>&nbsp;<asp:Button ID="BtnNotifyOther"
                                            runat="server" Text="..." OnInit="BtnNotifyOther_Init" CssClass="buttonicon" />
                                    </ItemTemplate>
                                    <ItemStyle VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="RowIndexNotifyOthers">
                                    <ItemStyle Font-Size="1px" ForeColor="White" />
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Button ID="BtnChangeEmailTemplate" runat="server" Text="Change Email Template"
                            Width="134px" Font-Bold="True" Font-Size="X-Small" Height="22px" OnClick="BtnChangeEmailTemplate_Click" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                </asp:TemplateField>
            </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <RowStyle BackColor="#F7F7DE" />
                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView>
                </ajax:AjaxPanel>
                </td>
        </tr>
        
		<tr class="formText" bgColor="#dddddd" height="30">
			<td style="width: 24px"><IMG height="15" src="images/arrow.gif" width="15"></td>
			<td colSpan="5" style="height: 9px">
				<table cellSpacing="0" cellPadding="3" border="0">
					<tr>
						<td><asp:imagebutton id="ImageSave" runat="server" CausesValidation="True" 
                                SkinID="savebutton" ></asp:imagebutton></td>
						<td><asp:imagebutton id="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton"></asp:imagebutton></td>
					</tr>
				</table>               
                </td>
		</tr>
	</table>
        </asp:View>
        
        <asp:View id="ChangeEmailTemplate" runat="server">
        <table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2" height="72" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
        <tr class="formText">
            <td bgcolor="#ffffff" style="height: 24px" width="2%">
            </td>
            <td bgcolor="#ffffff" colspan="3" style="height: 24px">
                Serial # :
                <asp:Label ID="LblSerialEmail" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" style="height: 24px" width="2%">
            </td>
            <td bgcolor="#ffffff" colspan="3" style="height: 24px">
                Workflow Step Name:&nbsp;
                <asp:Label ID="LblWorkflowStepNameEmail" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" style="height: 24px" width="2%">
            </td>
            <td bgcolor="#ffffff" colspan="3" style="height: 24px">
            </td>
        </tr>
		<tr class="formText">
			<td bgColor="#ffffff" style="height: 24px" width="2%"><br />
                </td>
			<td width="7%" bgColor="#ffffff" style="height: 24px">
                Subject</td>
			<td width="1%" bgColor="#ffffff" style="height: 24px">:</td>
			<td width="80%" bgColor="#ffffff" style="height: 24px"><asp:textbox id="TxtSubjectEmail" runat="server" CssClass="textBox" MaxLength="50" Width="200px" ></asp:textbox>
                <strong><span style="color: #ff0000">*</span></strong></td>
		</tr>
		<tr class="formText">
			<td bgColor="#ffffff" style="height: 24px;" width="2%">
                <br />
                <br />
                <br />
                <br />
                &nbsp;</td>
			<td bgColor="#ffffff" style="height: 24px" valign="top" width="7%">
                Body<br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" style="height: 24px" valign="top" width="1%">
                :<br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" style="height: 24px">
                <asp:textbox id="TxtBodyEmail" runat="server" CssClass="textBox" MaxLength="255" Width="478px" Height="133px" TextMode="MultiLine"></asp:textbox></td>
		</tr>
		<tr class="formText" bgColor="#dddddd" height="30">
			<td style="width: 24px"><IMG height="15" src="images/arrow.gif" width="15"></td>
			<td colSpan="3" style="height: 9px">
				<table cellSpacing="0" cellPadding="3" border="0">
					<tr>
						<td><asp:imagebutton id="ImageSaveEmail" runat="server" CausesValidation="True" SkinID="SaveButton"></asp:imagebutton></td>
						<td><asp:imagebutton id="ImageCancelEmail" runat="server" CausesValidation="False" SkinID="CancelButton"></asp:imagebutton></td>
					</tr>
				</table>               </td>
		</tr>
	</table>
        </asp:View>
    </asp:MultiView>	
    
	

</ajax:ajaxpanel></td>
		</tr>
	  </table></div></td>
  </tr>
  <tr>
  	<td><uc1:footer ID="Footer1" runat="server" /></td>
  </tr>
</table></form><script>arrangeView();</script></body>
</html>