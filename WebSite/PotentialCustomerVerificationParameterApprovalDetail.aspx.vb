Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Partial Class PotentialCustomerVerificationParameterApprovalDetail
    Inherits Parent

#Region " Property "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get confirm type
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property ParamType() As Sahassa.AML.Commonly.TypeConfirm
        Get
            Return Me.Request.Params("TypeConfirm")
        End Get
    End Property

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get param pk RiskRating management
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    15/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property PotentialCustomerVerificationParameterApprovalID() As Int64
        Get
            Return Me.Request.Params("ApprovalID")
        End Get
    End Property

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get param Parameters_PendingApprovalID
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    15/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property ParametersPendingApprovalID() As Int64
        Get
            Return Me.Request.Params("PendingApprovalID")
        End Get
    End Property

    ''' <summary>
    ''' get user id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetUserID() As String
        Get
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.Parameters_PendingApprovalTableAdapter
                Return AccessPending.SelectParameters_PendingApprovalUserID(Me.ParametersPendingApprovalID)
            End Using
        End Get
    End Property
#End Region

#Region " Load "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' hide all
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub HideAll()
        Dim StrId As String = ""
        Select Case Me.ParamType
            Case Sahassa.AML.Commonly.TypeConfirm.PotentialCustomerVerificationParameterAdd
                StrId = "UserAdd"
            Case Sahassa.AML.Commonly.TypeConfirm.PotentialCustomerVerificationParameterEdit
                StrId = "UserEdi"
            Case Else
                Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
        End Select
        For Each ObjRow As HtmlControls.HtmlTableRow In Me.TableDetail.Rows
            ObjRow.Visible = ObjRow.ID = "Button11" Or ObjRow.ID.Substring(0, 7) = StrId
        Next
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load LoadPotentialCustomerVerificationParameterAdd 
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    22/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub LoadPotentialCustomerVerificationParameterAdd()
        Me.LabelTitle.Text = "Activity: Add Potential Customer Verification Parameter"
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.PotentialCustomerVerificationParameter_ApprovalTableAdapter
            Using ObjTable As Data.DataTable = AccessPending.GetPotentialCustomerVerificationParameterApprovalData(Me.PotentialCustomerVerificationParameterApprovalID)
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.PotentialCustomerVerificationParameter_ApprovalRow = ObjTable.Rows(0)

                    Me.LabelMinimumResultPercentageAdd.Text = rowData.MinimumResultPercentage
                End If
            End Using
        End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load PotentialCustomerVerificationParameter edit
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    15/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub LoadPotentialCustomerVerificationParameterEdit()
        Me.LabelTitle.Text = "Activity: Edit Potential Customer Verification Parameter"
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.PotentialCustomerVerificationParameter_ApprovalTableAdapter
            Using ObjTable As Data.DataTable = AccessPending.GetPotentialCustomerVerificationParameterApprovalData(Me.PotentialCustomerVerificationParameterApprovalID)
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.PotentialCustomerVerificationParameter_ApprovalRow = ObjTable.Rows(0)
                    Me.LabelMinimumResultPercentageNew.Text = rowData.MinimumResultPercentage

                    Me.LabelMinimumResultPercentageOld.Text = rowData.MinimumResultPercentage_Old
                End If
            End Using
        End Using
    End Sub
#End Region

#Region " Accept "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' AcceptPotentialCustomerVerificationParameter add accept
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    21/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptPotentialCustomerVerificationParameterAdd()
        Dim oSQLTrans As SqlTransaction = Nothing

        Using AccessPotentialCustomerVerificationParameter As New AMLDAL.AMLDataSetTableAdapters.PotentialCustomerVerificationParameterTableAdapter
            oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessPotentialCustomerVerificationParameter, Data.IsolationLevel.ReadUncommitted)
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.PotentialCustomerVerificationParameter_ApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, oSQLTrans)
                Using AccessParametersApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_ApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessParametersApproval, oSQLTrans)
                    Using AccessParametersPendingApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_PendingApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessParametersPendingApproval, oSQLTrans)
                        Dim ObjTable As Data.DataTable = AccessPending.GetPotentialCustomerVerificationParameterApprovalData(Me.PotentialCustomerVerificationParameterApprovalID)
                        If ObjTable.Rows.Count > 0 Then
                            Dim rowData As AMLDAL.AMLDataSet.PotentialCustomerVerificationParameter_ApprovalRow = ObjTable.Rows(0)
                            'tambahkan item tersebut dalam tabel PotentialCustomerVerificationParameter
                            AccessPotentialCustomerVerificationParameter.Insert(rowData.MinimumResultPercentage, rowData.CreatedDate, rowData.LastUpdateDate)

                            'catat aktifitas dalam tabel Audit Trail
                            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)

                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - PotentialCustomerVerificationParameter", "MinimumResultPercentage", "Add", "", rowData.MinimumResultPercentage, "Accepted")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - PotentialCustomerVerificationParameter", "CreatedDate", "Add", "", rowData.CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - PotentialCustomerVerificationParameter", "LastUpdateDate", "Add", "", rowData.LastUpdateDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")

                                'hapus item tsb dalam tabel Parameters_Approval
                                AccessParametersApproval.DeleteParametersApproval(Me.ParametersPendingApprovalID)

                                'hapus item tsb dalam tabel PotentialCustomerVerificationParameter_Approval
                                AccessPending.DeletePotentialCustomerVerificationParameterApproval(rowData.ApprovalID)

                                'hapus item tsb dalam tabel Parameters_PendingApproval
                                AccessParametersPendingApproval.DeleteParametersPendingApproval(Me.ParametersPendingApprovalID)
                            End Using
                        End If
                    End Using
                End Using
            End Using
        End Using

        oSQLTrans.Commit()
        'End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' PotentialCustomerVerificationParameter edit accept
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    15/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptPotentialCustomerVerificationParameterEdit()
        Dim oSQLTrans As SqlTransaction = Nothing
        'Using TranScope As New Transactions.TransactionScope
        Using AccessPotentialCustomerVerificationParameter As New AMLDAL.AMLDataSetTableAdapters.PotentialCustomerVerificationParameterTableAdapter
            oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessPotentialCustomerVerificationParameter, Data.IsolationLevel.ReadUncommitted)
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.PotentialCustomerVerificationParameter_ApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, oSQLTrans)
                Dim ObjTable As Data.DataTable = AccessPending.GetPotentialCustomerVerificationParameterApprovalData(Me.PotentialCustomerVerificationParameterApprovalID)
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.PotentialCustomerVerificationParameter_ApprovalRow = ObjTable.Rows(0)
                    'update item tersebut dalam tabel PotentialCustomerVerificationParameter
                    AccessPotentialCustomerVerificationParameter.UpdatePotentialCustomerVerificationParameter(rowData.MinimumResultPercentage, rowData.LastUpdateDate)

                    'catat aktifitas dalam tabel Audit Trail
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)

                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - PotentialCustomerVerificationParameter", "MinimumResultPercentage", "Edit", rowData.MinimumResultPercentage_Old, rowData.MinimumResultPercentage, "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - PotentialCustomerVerificationParameter", "CreatedDate", "Edit", rowData.CreatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), rowData.CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - PotentialCustomerVerificationParameter", "LastUpdateDate", "Edit", rowData.LastUpdateDate_Old.ToString("dd-MMMM-yyyy HH:mm"), rowData.LastUpdateDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")

                        'hapus item tersebut dalam tabel PotentialCustomerVerificationParameter_Approval
                        AccessPending.DeletePotentialCustomerVerificationParameterApproval(Me.PotentialCustomerVerificationParameterApprovalID)

                        'hapus item tersebut dalam tabel Parameters_Approval
                        Using AccessParametersApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_ApprovalTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessParametersApproval, oSQLTrans)
                            AccessParametersApproval.DeleteParametersApproval(ParametersPendingApprovalID)

                            'hapus item tersebut dalam tabel Parameters_PendingApproval
                            Using AccessParametersPendingApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_PendingApprovalTableAdapter
                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessParametersPendingApproval, oSQLTrans)
                                AccessParametersPendingApproval.DeleteParametersPendingApproval(ParametersPendingApprovalID)
                            End Using
                        End Using
                    End Using
                End If
            End Using
        End Using

        oSQLTrans.Commit()
        'End Using
    End Sub

#End Region

#Region " Reject "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' reject PotentialCustomerVerificationParameter add
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    21/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectPotentialCustomerVerificationParameterAdd()
        Dim oSQLTrans As SqlTransaction = Nothing

        'Using TranScope As New Transactions.TransactionScope
        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
            oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAudit, Data.IsolationLevel.ReadUncommitted)
            Using AccessPotentialCustomerVerificationParameter As New AMLDAL.AMLDataSetTableAdapters.PotentialCustomerVerificationParameter_ApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPotentialCustomerVerificationParameter, oSQLTrans)
                Using AccessParametersApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_ApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessParametersApproval, oSQLTrans)
                    Using AccessParametersPendingApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_PendingApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessParametersPendingApproval, oSQLTrans)
                        Using TablePotentialCustomerVerificationParameter As Data.DataTable = AccessPotentialCustomerVerificationParameter.GetPotentialCustomerVerificationParameterApprovalData(Me.PotentialCustomerVerificationParameterApprovalID)
                            If TablePotentialCustomerVerificationParameter.Rows.Count > 0 Then
                                Dim ObjRow As AMLDAL.AMLDataSet.PotentialCustomerVerificationParameter_ApprovalRow = TablePotentialCustomerVerificationParameter.Rows(0)

                                'catat aktifitas dalam tabel Audit Trail
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - PotentialCustomerVerificationParameter", "MinimumResultPercentage", "Add", "", ObjRow.MinimumResultPercentage, "Rejected")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - PotentialCustomerVerificationParameter", "CreatedDate", "Add", "", ObjRow.CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - PotentialCustomerVerificationParameter", "LastUpdateDate", "Add", "", ObjRow.LastUpdateDate.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")

                                'hapus item tersebut dalam tabel PotentialCustomerVerificationParameter_Approval
                                AccessPotentialCustomerVerificationParameter.DeletePotentialCustomerVerificationParameterApproval(Me.PotentialCustomerVerificationParameterApprovalID)

                                'hapus item tersebut dalam tabel Parameters_Approval
                                AccessParametersApproval.DeleteParametersApproval(Me.ParametersPendingApprovalID)

                                'hapus item tersebut dalam tabel Parameters_PendingApproval
                                AccessParametersPendingApproval.DeleteParametersPendingApproval(Me.ParametersPendingApprovalID)
                            End If
                        End Using
                    End Using
                End Using
            End Using
            'End Using

            oSQLTrans.Commit()
        End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' PotentialCustomerVerificationParameter reject edit
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    15/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectPotentialCustomerVerificationParameterEdit()
        Dim oSQLTrans As SqlTransaction = Nothing
        'Using TranScope As New Transactions.TransactionScope
        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
            oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAudit, Data.IsolationLevel.ReadUncommitted)

            Using AccessPotentialCustomerVerificationParameter As New AMLDAL.AMLDataSetTableAdapters.PotentialCustomerVerificationParameter_ApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPotentialCustomerVerificationParameter, oSQLTrans)
                Using AccessParametersApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_ApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessParametersApproval, oSQLTrans)
                    Using AccessParametersPendingApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_PendingApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessParametersPendingApproval, oSQLTrans)
                        Using TablePotentialCustomerVerificationParameter As Data.DataTable = AccessPotentialCustomerVerificationParameter.GetPotentialCustomerVerificationParameterApprovalData(Me.PotentialCustomerVerificationParameterApprovalID)
                            If TablePotentialCustomerVerificationParameter.Rows.Count > 0 Then
                                Dim ObjRow As AMLDAL.AMLDataSet.PotentialCustomerVerificationParameter_ApprovalRow = TablePotentialCustomerVerificationParameter.Rows(0)

                                'catat aktifitas dalam tabel Audit Trail
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - PotentialCustomerVerificationParameter", "MinimumResultPercentage", "Edit", ObjRow.MinimumResultPercentage_Old, ObjRow.MinimumResultPercentage, "Rejected")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - PotentialCustomerVerificationParameter", "CreatedDate", "Edit", ObjRow.CreatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), ObjRow.CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - PotentialCustomerVerificationParameter", "LastUpdateDate", "Edit", ObjRow.LastUpdateDate_Old.ToString("dd-MMMM-yyyy HH:mm"), ObjRow.LastUpdateDate.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")

                                'hapus item tersebut dalam tabel PotentialCustomerVerificationParameter_Approval
                                AccessPotentialCustomerVerificationParameter.DeletePotentialCustomerVerificationParameterApproval(Me.PotentialCustomerVerificationParameterApprovalID)


                                'hapus item tersebut dalam tabel Parameters_Approval
                                AccessParametersApproval.DeleteParametersApproval(Me.ParametersPendingApprovalID)


                                'hapus item tersebut dalam tabel Parameters_PendingApproval
                                AccessParametersPendingApproval.DeleteParametersPendingApproval(Me.ParametersPendingApprovalID)
                            End If
                        End Using
                    End Using
                End Using
            End Using
            'End Using

            oSQLTrans.Commit()
        End Using
    End Sub

#End Region

    ''' <summary>
    ''' page load event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.ImageAccept.Attributes.Add("onclick", "javascript: if(confirm('Do you want to accept this activity ?')== false) return false;")
                Me.ImageReject.Attributes.Add("onclick", "javascript: if(confirm('Do you want to reject this activity ?')== false) return false;")
                Me.HideAll()
                Select Case Me.ParamType
                    Case Sahassa.AML.Commonly.TypeConfirm.PotentialCustomerVerificationParameterAdd
                        Me.LoadPotentialCustomerVerificationParameterAdd()
                    Case Sahassa.AML.Commonly.TypeConfirm.PotentialCustomerVerificationParameterEdit
                        Me.LoadPotentialCustomerVerificationParameterEdit()
                    Case Else
                        Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
                End Select

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' accept button handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click
        Try
            Select Case Me.ParamType
                Case Sahassa.AML.Commonly.TypeConfirm.PotentialCustomerVerificationParameterAdd
                    Me.AcceptPotentialCustomerVerificationParameterAdd()
                Case Sahassa.AML.Commonly.TypeConfirm.PotentialCustomerVerificationParameterEdit
                    Me.AcceptPotentialCustomerVerificationParameterEdit()
                Case Else
                    Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
            End Select


            Sahassa.AML.Commonly.SessionIntendedPage = "PotentialCustomerVerificationParameterManagementApproval.aspx"

            Me.Response.Redirect("PotentialCustomerVerificationParameterManagementApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' reject button handlert
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click
        Try
            Select Case Me.ParamType
                Case Sahassa.AML.Commonly.TypeConfirm.PotentialCustomerVerificationParameterAdd
                    Me.RejectPotentialCustomerVerificationParameterAdd()
                Case Sahassa.AML.Commonly.TypeConfirm.PotentialCustomerVerificationParameterEdit
                    Me.RejectPotentialCustomerVerificationParameterEdit()
                Case Else
                    Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
            End Select

            Sahassa.AML.Commonly.SessionIntendedPage = "PotentialCustomerVerificationParameterManagementApproval.aspx"

            Me.Response.Redirect("PotentialCustomerVerificationParameterManagementApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' back button handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "PotentialCustomerVerificationParameterManagementApproval.aspx"

        Me.Response.Redirect("PotentialCustomerVerificationParameterManagementApproval.aspx", False)
    End Sub
End Class