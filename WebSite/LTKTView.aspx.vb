Imports System.Collections.Generic
Imports System.Data
Imports Sahassa.AML
Imports AMLBLL
Imports System.IO
Imports Sahassanettier.Data
Imports Sahassanettier.Entities
Imports Ionic.Zlib
Imports Ionic.Zip
Imports CookComputing.XmlRpc
Imports System.Data.SqlClient

Partial Class LTKTView
    Inherits Parent
    Private BindGridFromExcel As Boolean = False

#Region " Property "
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("LTKTViewSelected") Is Nothing, New ArrayList, Session("LTKTViewSelected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("LTKTViewSelected") = value
        End Set
    End Property
    Private Property SetnGetFieldSearch() As String
        Get
            Return IIf(Session("LTKTViewFieldSearch") Is Nothing, "", Session("LTKTViewFieldSearch"))
        End Get
        Set(ByVal Value As String)
            Session("LTKTViewFieldSearch") = Value
        End Set
    End Property
    Private Property SetnGetValueSearch() As String
        Get
            Return IIf(Session("LTKTViewValueSearch") Is Nothing, "", Session("LTKTViewValueSearch"))
        End Get
        Set(ByVal Value As String)
            Session("LTKTViewValueSearch") = Value
        End Set
    End Property
    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("LTKTViewSort") Is Nothing, "Pk_LTKT_Id  desc", Session("LTKTViewSort"))
        End Get
        Set(ByVal Value As String)
            Session("LTKTViewSort") = Value
        End Set
    End Property
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("LTKTViewCurrentPage") Is Nothing, 0, Session("LTKTViewCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("LTKTViewCurrentPage") = Value
        End Set
    End Property
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("LTKTViewRowTotal") Is Nothing, 0, Session("LTKTViewRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("LTKTViewRowTotal") = Value
        End Set
    End Property
    Private Function SetnGetBindTable() As VList(Of Vw_LTKTView)

        Return DataRepository.Vw_LTKTViewProvider.GetPaged(SearchFilter, SetnGetSort, SetnGetCurrentPage, Sahassa.AML.Commonly.GetDisplayedTotalRow, SetnGetRowTotal)


    End Function
    Private Property SetAndGetSearchingCriteria() As String
        Get
            Return IIf(Session("LTKTViewSearchCriteria") Is Nothing, "", Session("LTKTViewSearchCriteria"))
        End Get
        Set(ByVal Value As String)
            Session("LTKTViewSearchCriteria") = Value
        End Set
    End Property
#End Region

#Region "Bind Combo Box"
   
#End Region

    Private Function GetAllIDSelected(ByVal arrayList As ArrayList) As String
        Dim strVal As String = ""
        For Each pkid As String In arrayList
            strVal = strVal & pkid & ","
        Next
        Dim strRetVal As String = strVal.Substring(0, strVal.LastIndexOf(","))
        Return strRetVal
    End Function
 Function isNotExistInApproval(ByVal PK_LTKT As Integer)
        Try
            Using objLTKTApprovalDetail As TList(Of LTKT_ApprovalDetail) = DataRepository.LTKT_ApprovalDetailProvider.GetPaged(LTKT_ApprovalDetailColumn.PK_LTKT_Id.ToString & " = " & PK_LTKT, "", 0, Integer.MaxValue, 0)
                If objLTKTApprovalDetail.Count > 0 Then
                    Using objLTKTApproval As LTKT_Approval = DataRepository.LTKT_ApprovalProvider.GetByPK_LTKT_Approval_Id(objLTKTApprovalDetail(0).FK_LTKT_Approval_Id.GetValueOrDefault)
                        If Not IsNothing(objLTKTApproval) Then
                            If objLTKTApproval.FK_MsMode_Id.GetValueOrDefault = 2 Then
                                Using objMsUser As User = DataRepository.UserProvider.GetBypkUserID(objLTKTApproval.RequestedBy)
                                    If Not IsNothing(objMsUser) Then
                                        Throw New Exception("LTKT data has edited by " & objMsUser.UserName & " and waiting for approval")
                                    End If
                                End Using
                            ElseIf objLTKTApproval.FK_MsMode_Id.GetValueOrDefault = 3 Then
                                Using objMsUser As User = DataRepository.UserProvider.GetBypkUserID(objLTKTApproval.RequestedBy)
                                    If Not IsNothing(objMsUser) Then
                                        Throw New Exception("LTKT data has deleted by " & objMsUser.UserName & " and waiting for approval")
                                    End If
                                End Using
                            End If
                        End If
                    End Using
                End If
            End Using

            Return True
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
            Return False
        End Try
    End Function
    
        Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
            Me.CollectSelected()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    
    Function isValidSearch()
        Try
            If txtTransactionDateStart.Text <> "" Then
                If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", txtTransactionDateStart.Text) = False Then
                    Throw New Exception("Transaction date format not valid")
                End If
                If txtTransactionDateStart.Text > Now Then
                    Throw New Exception("Transactin date should be not greater than today")
                End If
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
            Return False
        End Try
        Return True
    End Function



    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
        Catch
            Throw
        End Try
    End Sub
    Public Sub BindGrid()
        Me.GridDataView.DataSource = Me.SetnGetBindTable

        'Me.GridDataView.CurrentPageIndex = Me.SetnGetCurrentPage
        Me.GridDataView.VirtualItemCount = Me.SetnGetRowTotal
        Me.GridDataView.DataBind()
    End Sub
    Private Sub ClearThisPageSessions()
        Session("LTKTViewSelected") = Nothing
        Session("LTKTViewFieldSearch") = Nothing
        Session("LTKTViewValueSearch") = Nothing
        Session("LTKTViewSort") = Nothing
        Session("LTKTViewCurrentPage") = Nothing
        Session("LTKTViewRowTotal") = Nothing
        Session("LTKTViewData") = Nothing
    End Sub
    Private Function SearchFilter() As String
        Dim StrSearch As String = ""
        Try
            If isValidSearch() Then


                If Me.TxtFullName.Text <> "" Then
                    If StrSearch = "" Then
                        StrSearch = StrSearch & " NamaLengkap like '%" & Me.TxtFullName.Text.Replace("'", "''") & "%' "
                    Else
                        StrSearch = StrSearch & " And NamaLengkap like '%" & Me.TxtFullName.Text.Replace("'", "''") & "%' "
                    End If
                End If

                If Me.TxtCIFNo.Text <> "" Then
                    If StrSearch = "" Then
                        StrSearch = StrSearch & " CIFNo = '" & Me.TxtCIFNo.Text.Replace("'", "''") & "' "
                    Else
                        StrSearch = StrSearch & " And CIFNo = '" & Me.TxtCIFNo.Text.Replace("'", "''") & "' "
                    End If
                End If

               


                If Me.txtTransactionDateStart.Text <> "" And txtTransactionDateEnd.Text.Trim <> "" Then
                    If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", txtTransactionDateStart.Text) AndAlso Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", txtTransactionDateEnd.Text) Then

                        If DateDiff(DateInterval.Day, Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", Me.txtTransactionDateStart.Text.Replace("'", "''")), Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", Me.txtTransactionDateEnd.Text.Replace("'", "''"))) >= 0 Then



                            If StrSearch = "" Then
                                StrSearch = StrSearch & " (TanggalTransaksi between '" & Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", Me.txtTransactionDateStart.Text.Replace("'", "''")) & "' and   '" & Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", Me.txtTransactionDateEnd.Text.Replace("'", "''")) & "') "
                            Else
                                StrSearch = StrSearch & " and (TanggalTransaksi between '" & Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", Me.txtTransactionDateStart.Text.Replace("'", "''")) & "' and   '" & Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", Me.txtTransactionDateEnd.Text.Replace("'", "''")) & "') "

                            End If
                        Else
                            Throw New Exception("Tanggal transaksi End Must Greater than Tanggal transaksi End ")
                        End If
                    Else
                        Throw New Exception("Tanggal transaksi Start or Tanggal transaksi End Invalid")
                    End If
                    End If



                If Me.TxtlastUpdateStart.Text <> "" Then
                    If StrSearch = "" Then
                        StrSearch = StrSearch & " (LastUpdateDate between '" & Me.TxtlastUpdateStart.Text.Replace("'", "''") & "' and '" & Me.TxtlastUpdateStart.Text.Replace("'", "''") & " 23:59:59') "
                    Else
                        StrSearch = StrSearch & " and (LastUpdateDate between '" & Me.TxtlastUpdateStart.Text.Replace("'", "''") & "' and '" & Me.TxtlastUpdateStart.Text.Replace("'", "''") & " 23:59:59') "
                    End If
                End If



                If Me.txtTransNominalBCashIn.Text <> "" And txtTransNominalUCashIn.Text <> "" Then
                    If isValidNominal("cashin") Then
                        If StrSearch = "" Then
                            StrSearch = StrSearch & " CashInNominal between " & Me.txtTransNominalBCashIn.Text.Replace("'", "''") & " and " & Me.txtTransNominalUCashIn.Text.Replace("'", "''")
                        Else
                            StrSearch = StrSearch & " and CashInNominal between " & Me.txtTransNominalBCashIn.Text.Replace("'", "''") & " and " & Me.txtTransNominalUCashIn.Text.Replace("'", "''")
                        End If
                    End If
                End If


                If Me.txtTransNominalBCashOut.Text <> "" And txtTransNominalUCashOut.Text <> "" Then
                    If isValidNominal("cashout") Then
                        If StrSearch = "" Then
                            StrSearch = StrSearch & " CashOutNominal between " & Me.txtTransNominalBCashOut.Text.Replace("'", "''") & " and " & Me.txtTransNominalUCashOut.Text.Replace("'", "''")
                        Else
                            StrSearch = StrSearch & " and CashOutNominal between " & Me.txtTransNominalBCashOut.Text.Replace("'", "''") & " and " & Me.txtTransNominalUCashOut.Text.Replace("'", "''")
                        End If
                    End If
                End If



                'If Me.txtPJKOffice.Text <> "" Then
                '    If StrSearch = "" Then
                '        StrSearch = StrSearch & " KantorPJKCashIn like '%" & Me.txtPJKOffice.Text.Replace("'", "''") & "%' or KantorPJKCashOut like '%" & txtPJKOffice.Text & "%' "
                '    Else
                '        StrSearch = StrSearch & " and (KantorPJKCashIn like '%" & Me.txtPJKOffice.Text.Replace("'", "''") & "%' or KantorPJKCashOut like '%" & txtPJKOffice.Text & "%') "
                '    End If
                'End If


                Return StrSearch
            End If
        Catch
            Throw
            Return ""
        End Try
    End Function

    Function isValidNominal(ByVal type As String) As Boolean
        Try
            If type.ToLower = "cashin" Then
                If IsNumeric(txtTransNominalBCashIn.Text) = False Then
                    Throw New Exception("Nominal CashIn must be filled by number")
                End If
                If IsNumeric(txtTransNominalUCashIn.Text) = False Then
                    Throw New Exception("Nominal CashIn must be filled by number")
                End If
            ElseIf type.ToLower = "cashout" Then
                If IsNumeric(txtTransNominalBCashOut.Text) = False Then
                    Throw New Exception("Nominal CashOut must be filled by number")
                End If
                If IsNumeric(txtTransNominalUCashOut.Text) = False Then
                    Throw New Exception("Nominal CashOut must be filled by number")
                End If
            End If
            Return True

        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
            Return False
        End Try
    End Function





#Region "Excel Export Event"
    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In Me.GridDataView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = gridRow.FindControl("CheckBoxExporttoExcel")
                Dim PkId As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
                If ArrTarget.Contains(PkId) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(PkId)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(PkId)
                End If
                Me.SetnGetSelectedItem = ArrTarget
            End If
        Next
    End Sub

    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub 'ClearControls

    Private Sub BindSelected()
        Dim Rows As New ArrayList

        Dim SbPk As New StringBuilder
        For Each IdPk As Int64 In Me.SetnGetSelectedItem
            SbPk.Append(IdPk.ToString & ", ")
        Next
        If SbPk.ToString <> "" Then
            Dim rowData As VList(Of Vw_LTKTView) = DataRepository.Vw_LTKTViewProvider.GetPaged("Pk_LTKT_Id in (" & SbPk.ToString.Substring(0, SbPk.Length - 2) & ") ", "", 0, Integer.MaxValue, 0)
            For Each rowList As Vw_LTKTView In rowData
                Rows.Add(rowList)
            Next
            SbPk = Nothing
        End If

        Me.GridDataView.DataSource = Rows
        Me.GridDataView.AllowPaging = False
        Me.GridDataView.DataBind()

        Me.GridDataView.Columns(0).Visible = False
        Me.GridDataView.Columns(1).Visible = False
        Me.GridDataView.Columns(7).Visible = False
    End Sub

    Private Sub BindSelectedAll()
        Try

            Me.GridDataView.DataSource = DataRepository.Vw_LTKTViewProvider.GetPaged(SearchFilter, Me.SetnGetSort, Me.SetnGetCurrentPage, Sahassa.AML.Commonly.GetDisplayedTotalRow, SetnGetRowTotal)

            Me.GridDataView.AllowPaging = False
            Me.GridDataView.DataBind()

            Me.GridDataView.Columns(0).Visible = False
            Me.GridDataView.Columns(1).Visible = False
            Me.GridDataView.Columns(7).Visible = False
        Catch
            Throw
        End Try

    End Sub





    Private Function GetPKLTKTId(ByVal vwLtktViews As VList(Of Vw_LTKTView)) As String
        Dim sb As New StringBuilder
        For Each item As Vw_LTKTView In vwLtktViews
            sb.Append(item.PK_LTKT_Id & ",")
        Next
        Return sb.ToString().Substring(0, sb.ToString().LastIndexOf(",") - 1)
    End Function

    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        'For Each gridRow As DataGridItem In Me.GridDataView.Items
        '    If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
        '        Dim PkId As String = gridRow.Cells(1).Text
        '        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        '        If Me.SetnGetSelectedItem.Contains(PkId) Then
        '            If Me.CheckBoxSelectAll.Checked Then
        '                If Not ArrTarget.Contains(PkId) Then
        '                    ArrTarget.Add(PkId)
        '                End If
        '            Else
        '                ArrTarget.Remove(PkId)
        '            End If
        '        Else
        '            If Me.CheckBoxSelectAll.Checked Then
        '                If Not ArrTarget.Contains(PkId) Then
        '                    ArrTarget.Add(PkId)
        '                End If
        '            End If
        '        End If
        '        Me.SetnGetSelectedItem = ArrTarget
        '    End If
        'Next

        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        For Each gridRow As DataGridItem In Me.GridDataView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(1).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItem.Contains(StrKey) = False Then
                        ArrTarget.Add(StrKey)
                    End If
                Else
                    If Me.SetnGetSelectedItem.Contains(StrKey) Then
                        ArrTarget.Remove(StrKey)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItem = ArrTarget
        Me.BindGrid()
    End Sub

    Private Sub SetCheckedAll()
        Dim i As Int16 = 0
        Dim totalrow As Int16 = 0
        For Each gridRow As DataGridItem In Me.GridDataView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = CType(gridRow.FindControl("CheckBoxExporttoExcel"), CheckBox)
                If chkBox.Checked Then
                    i = CType(i + 1, Int16)
                End If
                totalrow = CType(totalrow + 1, Int16)
            End If
        Next
        If i = totalrow Then
            Me.CheckBoxSelectAll.Checked = True
        Else
            Me.CheckBoxSelectAll.Checked = False
        End If
    End Sub

#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                'Using AccessAudit As New AMLBLL.AuditTrailBLL
                '    AccessAudit.InsertAuditTrailUserAccess(Sahassa.AML.Commonly.SessionNIK, Sahassa.AML.Commonly.SessionUserId, "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded")
                'End Using

                Me.ClearThisPageSessions()

                Me.PopTransactionEndDate.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtTransactionDateEnd.ClientID & "'), 'dd-mmm-yyyy')")
                Me.PopTransactionEndDate.Style.Add("display", "")
                Me.popUpTransactionDate.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtTransactionDateStart.ClientID & "'), 'dd-mmm-yyyy')")
                Me.popUpTransactionDate.Style.Add("display", "")

                Me.btnupdateStart.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TxtlastUpdateStart.ClientID & "'), 'dd-mmm-yyyy')")
                Me.btnupdateStart.Style.Add("display", "")

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                'Using AccessAudit As New AMLBLL.AuditTrailBLL
                '    AccessAudit.InsertAuditTrailUserAccess(Sahassa.AML.Commonly.SessionNIK, Sahassa.AML.Commonly.SessionUserId, "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded")
                'End Using

                'Using ControlerGetUserAccess As New AMLBLL.AccessAuthorityBLL
                '    If ControlerGetUserAccess.BolIsUserHasAccess(Sahassa.AML.Commonly.SessionFkGroupMenuId, "LTKTView.aspx") = True Then
                '        Me.LinkButtonAddNew.Visible = True
                '    Else
                '        Me.LinkButtonAddNew.Visible = False
                '    End If
                'End Using

                Me.GridDataView.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow
                'BindIdType()
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.CollectSelected()
            Me.BindGrid()
            SetCheckedAll()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Imagebutton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imagebutton1.Click
        Try
            Me.SetAndGetSearchingCriteria = Nothing
            Me.TxtFullName.Text = ""
            Me.txtTransactionDateEnd.Text = ""
            Me.TxtCIFNo.Text = ""
            
            Me.txtTransactionDateStart.Text = ""
            Me.txtTransNominalBCashIn.Text = ""
            Me.txtTransNominalUCashIn.Text = ""
            Me.txtTransNominalBCashOut.Text = ""
            Me.txtTransNominalUCashOut.Text = ""



            TxtlastUpdateStart.Text = ""
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub





    Protected Sub GridDataView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridDataView.ItemDataBound
        Try

            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then



                'If e.Item.Cells(19).Text <> "" And e.Item.Cells(19).Text <> "&bnsp;" Then
                '    e.Item.Cells(19).Text = ValidateBLL.FormatMoneyWithComma(e.Item.Cells(19).Text)
                'End If
                'If e.Item.Cells(20).Text <> "" And e.Item.Cells(20).Text <> "&bnsp;" Then
                '    e.Item.Cells(20).Text = ValidateBLL.FormatMoneyWithComma(e.Item.Cells(20).Text)
                'End If


                'Using objLblStatus As Label = CType(e.Item.FindControl("LblStatus"), Label)
                '    Using objExistApproval As TList(Of LTKT_ApprovalDetail) = DataRepository.LTKT_ApprovalDetailProvider.GetPaged(LTKT_ApprovalDetailColumn.PK_LTKT_Id.ToString & " = " & e.Item.Cells(1).Text.ToString, "", 0, Integer.MaxValue, 0)
                '        If objExistApproval.Count > 0 Then
                '            objLblStatus.Text = "Pending approval"
                '        End If
                '    End Using
                'End Using

                'Using objLblNPWP As Label = CType(e.Item.FindControl("LblNPWP"), Label)
                '    If e.Item.Cells(3).Text = "0" Then
                '        objLblNPWP.Text = e.Item.Cells(4).Text
                '    Else
                '        objLblNPWP.Text = e.Item.Cells(5).Text
                '    End If
                'End Using

                'If e.Item.Cells(2).Text <> "&nbsp;" Then
                '    Using objLblIDType As Label = CType(e.Item.FindControl("LblIDType"), Label)
                '        Using ObjIDType As MsIDType = DataRepository.MsIDTypeProvider.GetByPk_MsIDType_Id(CInt(e.Item.Cells(2).Text))
                '            If Not IsNothing(ObjIDType) Then
                '                objLblIDType.Text = ObjIDType.MsIDType_Name
                '            End If
                '        End Using
                '    End Using
                'End If

                'Using objLblName As Label = CType(e.Item.FindControl("lblName"), Label)
                '    If e.Item.Cells(3).Text = "0" Then
                '        objLblName.Text = e.Item.Cells(6).Text
                '    Else
                '        objLblName.Text = e.Item.Cells(7).Text
                '    End If
                'End Using


                Dim chkBox As CheckBox = e.Item.FindControl("CheckBoxExporttoExcel")
                chkBox.Checked = Me.SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)
                If BindGridFromExcel = True Then
                    e.Item.Cells(2).Text = CStr((e.Item.ItemIndex + 1))
                Else
                    e.Item.Cells(2).Text = CStr((e.Item.ItemIndex + 1) + (Me.SetnGetCurrentPage * Sahassa.AML.Commonly.GetDisplayedTotalRow))
                End If

                'If e.Item.Cells(10).Text <> "" And e.Item.Cells(10).Text <> "&nbsp;" Then
                '    e.Item.Cells(10).Text = Convert.ToDateTime(e.Item.Cells(10).Text).ToString("dd-MMM-yyyy")
                'End If
                'If e.Item.Cells(17).Text <> "" And e.Item.Cells(17).Text <> "&nbsp;" Then
                '    e.Item.Cells(17).Text = Convert.ToDateTime(e.Item.Cells(17).Text).ToString("dd-MMM-yyyy")
                'End If
                'If e.Item.Cells(18).Text <> "" And e.Item.Cells(18).Text <> "&nbsp;" Then
                '    e.Item.Cells(18).Text = Convert.ToDateTime(e.Item.Cells(18).Text).ToString("dd-MMM-yyyy")
                'End If
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridDataView_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridDataView.SortCommand
        Dim GridUser As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub



    Protected Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            Me.CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be less than or equal to the total page count.")
                End If
            Else
                Throw New Exception("Page number must be less than or equal to the total page count.")
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub    

    Protected Sub cvalPageErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles cvalPageErr.PreRender
        If Me.cvalPageErr.IsValid = False Then
            'Sahassa.AML.Commonly.SessionErrorMessageCount = Sahassa.AML.Commonly.SessionErrorMessageCount + 1
            'ClientScript.RegisterStartupScript(Page.GetType, "ErrorMessage" & Sahassa.AML.Commonly.SessionErrorMessageCount.ToString, "alert('There was uncompleteness in this page: \n" & Me.cvalPageErr.ErrorMessage.Replace("'", "\'").Replace(vbCrLf, "\n").Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("<br>", "\n") & "');", True)
        End If
    End Sub

    

   

    Protected Sub GridDataView_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridDataView.ItemCommand
        If e.CommandName.ToLower = "detail" Then
            Response.Redirect("LTKTViewDetail.aspx?LTKT_ID=" & e.Item.Cells(1).Text)
        ElseIf e.CommandName.ToLower = "edit" Then
            If isNotExistInApproval(CInt(e.Item.Cells(1).Text)) Then
                Response.Redirect("LTKTEdit.aspx?LTKT_ID=" & e.Item.Cells(1).Text)
            End If
        ElseIf e.CommandName.ToLower = "delete" Then
            If isNotExistInApproval(CInt(e.Item.Cells(1).Text)) Then
                Response.Redirect("LTKTDelete.aspx?LTKT_ID=" & e.Item.Cells(1).Text)
            End If
        End If
    End Sub

   

    Protected Sub lnkExportData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExportData.Click
        Dim lstFile As New List(Of String)
        Dim dirPath As String = Server.MapPath(Request.ServerVariables("PATH_INFO")).Substring(0, Server.MapPath(Request.ServerVariables("PATH_INFO")).LastIndexOf("\") + 1)
        Dim dateDownload As DateTime = Now

        Try
            Me.CollectSelected()
            Dim total As Integer = 0
            Dim objVwLtkt As VList(Of Vw_LTKTView) = DataRepository.Vw_LTKTViewProvider.GetPaged(SearchFilter, "", 0, Integer.MaxValue, total)

            If Me.SetnGetSelectedItem.Count = 0 AndAlso rblData.SelectedIndex = 0 Then
                Throw New Exception("There are no data for export")
            ElseIf total > 65536 Then
                Throw New Exception("Can not export to excel more than 65536 rows (xls limitation)")
            Else
                Me.BindSelected()
                If cboFormatFile.SelectedValue = "Excel" Then
                    If rblData.SelectedIndex = 0 Then
                        'selected data
                        Dim strID As String = GetAllIDSelected(Me.SetnGetSelectedItem)
                        Using checkBlocking As TList(Of LTKT_ApprovalDetail) = DataRepository.LTKT_ApprovalDetailProvider.GetPaged("Pk_LTKT_id in (" & strID & ")", "", 0, Integer.MaxValue, Nothing)
                            If checkBlocking.Count > 0 Then
                                BindGrid()
                                Throw New Exception("One or more items  are Wating For Approval")
                            End If
                        End Using
                        Dim tanggalGenerated As DateTime = Now

                        ' ''memakai services
                        'Dim proxy As AMLBLL.ICTRGeneratorServices
                        'Dim Successrun As Boolean = False
                        'proxy = CType(XmlRpcProxyGen.Create(GetType(AMLBLL.ICTRGeneratorServices)), AMLBLL.ICTRGeneratorServices)
                        'proxy.Url = System.Configuration.ConfigurationManager.AppSettings("ProxyURLPath")
                        'Successrun = proxy.CallProcessGenerateXlsLTKTReportFromLTKTView(strID, tanggalGenerated.ToString("dd-MM-yyyy"), Commonly.SessionStaffName)
                        CTRNewFileGeneratorBll.CallProcessGenerateXlsLTKTReportFromLTKTView(strID, tanggalGenerated.ToString("yyyy-MM-dd"), Commonly.SessionUserId, dirPath)


                        Using obj As TList(Of LTKTGeneratedFileXls) = DataRepository.LTKTGeneratedFileXlsProvider.GetPaged("DATEDIFF(DAY, TransactionDate,'" & tanggalGenerated.ToString("yyyy-MM-dd") & "')=0", "CreatedDate desc", 0, 1, 0)
                            If obj.Count > 0 Then
                                Response.Redirect("DownloadAttachment.aspx?IsBinary=Y&ext=xls&id=" & obj(0).PK_LTKTGeneratedFileXls_Id & "&DataSource=LTKT")
                            End If
                        End Using
                    Else
                        'all data
                        'Dim sql As String
                        'sql = "SELECT COUNT(*) FROM Vw_LTKTView WHERE "
                        'Dim sqlCmd As New SqlCommand(sql)
                        'sqlCmd.CommandTimeout = 7000
                        'sqlCmd.CommandType = CommandType.Text

                        'Dim total As Integer = CInt(DataRepository.Provider.ExecuteScalar(sqlCmd))
                        'Dim total As Integer = 0
                        'Dim objVwLtkt As VList(Of Vw_LTKTView) = DataRepository.Vw_LTKTViewProvider.GetPaged(SearchFilter, "", 0, Integer.MaxValue, total)
                        'If total > 65536 Then
                        '    BindGrid()
                        '    Throw New Exception("Can not export to excel more than 65536 rows (xls limitation)")
                        'Else
                            'Dim strID As String = GetPKLTKTId(objVwLtkt)
                        Dim tanggalGenerated As DateTime = Now

                        ''memakai services
                        'Dim proxy As AMLBLL.ICTRGeneratorServices
                        'Dim Successrun As Boolean = False
                        'proxy = CType(XmlRpcProxyGen.Create(GetType(AMLBLL.ICTRGeneratorServices)), AMLBLL.ICTRGeneratorServices)
                        'proxy.Url = System.Configuration.ConfigurationManager.AppSettings("ProxyURLPath")
                        ''Successrun = proxy.CallProcessGenerateXlsAllLTKTReportFromLTKTView(tanggalGenerated.ToString("dd-MM-yyyy"), Commonly.SessionStaffName)
                        'Successrun = proxy.CallProcessGenerateXlsAllLTKTReportFromLTKTViewUsingFilter(tanggalGenerated.ToString("dd-MM-yyyy"), Commonly.SessionStaffName, SearchFilter())
                        CTRNewFileGeneratorBll.CallProcessGenerateXlsAllLTKTReportFromLTKTViewUsingFilter(tanggalGenerated.ToString("yyyy-MM-dd"), Commonly.SessionUserId, SearchFilter(), dirPath)

                        Using objError As LTKTGeneratedFileProgressXls = DataRepository.LTKTGeneratedFileProgressXlsProvider.GetByPK_LTKTGeneratedFileProgressXls_Id(1)
                            If Not objError Is Nothing Then
                                If objError.IsError Then
                                    Throw New Exception(objError.ErrorDescription)
                                End If
                            End If
                        End Using

                        Using obj As TList(Of LTKTGeneratedFileXls) = DataRepository.LTKTGeneratedFileXlsProvider.GetPaged("DATEDIFF(DAY, TransactionDate,'" & tanggalGenerated.ToString("yyyy-MM-dd") & "')=0", "CreatedDate desc", 0, 1, 0)
                            If obj.Count > 0 Then
                                Response.Redirect("DownloadAttachment.aspx?IsBinary=Y&ext=xls&id=" & obj(0).PK_LTKTGeneratedFileXls_Id & "&DataSource=LTKT")
                            End If
                        End Using
                        ' End If
                    End If
                Else
                    'xml format
                    If rblData.SelectedIndex = 0 Then
                        'selected data
                        Dim strID As String = GetAllIDSelected(Me.SetnGetSelectedItem)

                        Using objLTKTList As TList(Of LTKT) = DataRepository.LTKTProvider.GetPaged("PK_LTKT_ID in (" & strID & ")", "", 0, Integer.MaxValue, 0)
                            If objLTKTList.Count > 0 Then
                                For i As Integer = 0 To objLTKTList.Count - 1
                                    Dim fileName As String = ""

                                    Dim pathReport As String = dirPath & "FolderExport\"
                                    fileName = objLTKTList(i).CreatedDate.GetValueOrDefault().ToString("yyyyMMdd") & "-" & objLTKTList(i).CIFNo.ToString() & ".xml"
                                    If File.Exists(pathReport & fileName) Then
                                        File.Delete(pathReport & fileName)
                                    End If
                                    CTRNewFileGeneratorBll.GenerateGRISPXMLNasabah(pathReport & fileName, objLTKTList(i), objLTKTList(i).CreatedDate.GetValueOrDefault(), CStr(objLTKTList(i).CIFNo))

                                    'masukkan ke list
                                    lstFile.Add(pathReport & fileName)
                                Next

                                'setelah proses semuanya maka lakukan zipping akan bisa didownload
                                If lstFile.Count > 0 Then
                                    Dim fileNameLTKT As String = dirPath & "FolderExport\" & "LTKT-" & dateDownload.ToString("yyyyMMdd") & ".zip"
                                    Using zip As New ZipFile()
                                        zip.CompressionLevel = CompressionLevel.Level9
                                        zip.AddFiles(lstFile, "")

                                        zip.Save(fileNameLTKT)
                                    End Using
                                End If
                            End If

                            'download attachment
                            Response.Redirect("DownloadAttachment.aspx?IsFile=Y&FileName=" & dirPath.Replace("\", "\\") & "FolderExport\\" & "LTKT-" & dateDownload.ToString("yyyyMMdd") & ".zip")
                        End Using
                    Else
                        'all data
                        'Dim total As Integer = 0
                        'Using objVwLtkt As VList(Of Vw_LTKTView) = DataRepository.Vw_LTKTViewProvider.GetPaged(SearchFilter, "", 0, Integer.MaxValue, total)
                        If objVwLtkt.Count > 0 Then
                            For i As Integer = 0 To objVwLtkt.Count - 1
                                Using objLTKT As LTKT = DataRepository.LTKTProvider.GetByPK_LTKT_Id(objVwLtkt(i).PK_LTKT_Id)
                                    If objLTKT IsNot Nothing Then
                                        Dim fileName As String = ""

                                        Dim pathReport As String = dirPath & "FolderExport\"
                                        fileName = objLTKT.CreatedDate.GetValueOrDefault().ToString("yyyyMMdd") & "-" & objLTKT.CIFNo.ToString() & ".xml"
                                        If File.Exists(pathReport & fileName) Then
                                            File.Delete(pathReport & fileName)
                                        End If
                                        CTRNewFileGeneratorBll.GenerateGRISPXMLNasabah(pathReport & fileName, objLTKT, objLTKT.CreatedDate.GetValueOrDefault(), CStr(objLTKT.CIFNo))

                                        'masukkan ke list
                                        lstFile.Add(pathReport & fileName)

                                        'setelah proses semuanya maka lakukan zipping akan bisa didownload
                                        If lstFile.Count > 0 Then
                                            Dim fileNameLTKT As String = dirPath & "FolderExport\" & "LTKT-" & dateDownload.ToString("yyyyMMdd") & ".zip"
                                            Using zip As New ZipFile()
                                                zip.CompressionLevel = CompressionLevel.Level9
                                                For j As Integer = 0 To lstFile.Count - 1
                                                    zip.AddFile(lstFile(j), "")
                                                Next
                                                zip.Save(fileNameLTKT)
                                            End Using
                                        End If
                                    End If
                                End Using
                            Next

                            'download attachment
                            Response.Redirect("DownloadAttachment.aspx?IsFile=Y&FileName=" & dirPath.Replace("\", "\\") & "FolderExport\\" & "LTKT-" & dateDownload.ToString("yyyyMMdd") & ".zip")
                        End If
                        'End Using


                    End If

                End If
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    
End Class