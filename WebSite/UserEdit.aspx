<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="UserEdit.aspx.vb" Inherits="UserEdit" title="User Edit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
	<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2" height="231">
        <tr class="formText">
            <td bgcolor="#ffffff" colspan="4">
                <strong><span style="font-size: 18px">User - Edit 
                    <hr />
                </span></strong>
                <asp:Label ID="LblSuccess" runat="server" CssClass="validationok" Visible="False"
                    Width="100%"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" colspan="4"><span style="color: #ff0000">* Required</span></td>
        </tr>
		<tr class="formText">
			<td width="5" bgColor="#ffffff" height="24">
                <br />
                </td>
			<td width="20%" bgColor="#ffffff">User ID</td>
			<td width="5" bgColor="#ffffff">:</td>
			<td width="80%" bgColor="#ffffff"><asp:Label ID="TextUserId" runat="server"></asp:Label>
                </td>
		</tr>
		<tr class="formText">
			<td bgColor="#ffffff" height="24">
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorFirstName" runat="server" ControlToValidate="TextUserName"
                    Display="Dynamic" ErrorMessage="User Name cannot be blank">*</asp:RequiredFieldValidator><br />
                <asp:RegularExpressionValidator ID="RegularExpressionValidatorFirstName" runat="server"
                    ControlToValidate="TextUserName" ErrorMessage="User Name must starts with a letter then it can be followed by other characters."
                    ValidationExpression="[a-zA-Z](\w*\s*)*">*</asp:RegularExpressionValidator></td>
			<td bgColor="#ffffff">
                User Name</td>
			<td bgColor="#ffffff">:</td>
			<td bgColor="#ffffff"><asp:textbox id="TextUserName" runat="server" CssClass="textBox" MaxLength="50" Width="200px"></asp:textbox>
                <strong><span style="color: #ff0000">*</span></strong></td>
		</tr>
		<tr class="formText">
			<td bgColor="#ffffff" height="24">
                </td>
			<td bgColor="#ffffff">Password</td>
			<td bgColor="#ffffff">:</td>
			<td bgColor="#ffffff"><asp:textbox id="TextPassword" runat="server" CssClass="textBox" MaxLength="50" TextMode="Password" Width="200px" ></asp:textbox>
                <span style="color: #ff0000">(leave password blank if you dont want to change it)<strong>&nbsp;</strong></span></td>
		</tr>
		<tr class="formText">
			<td bgColor="#ffffff" height="24">
                <asp:CompareValidator ID="CompareValidatorPassword" runat="server" ControlToCompare="TextBoxRetype"
                    ControlToValidate="TextPassword" Display="Dynamic"
                    ErrorMessage="Entry in Confirm Password textbox doesn't match the entry in Password textbox">*</asp:CompareValidator></td>
			<td bgColor="#ffffff">
                Confirm Password</td>
			<td bgColor="#ffffff">:</td>
			<td bgColor="#ffffff"><asp:textbox id="TextBoxRetype" runat="server" CssClass="textBox" MaxLength="50" TextMode="Password" Width="200px" ></asp:textbox>
                </td>
		</tr>
		<tr class="formText">
			<td bgColor="#ffffff" height="24">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TextboxEmailAddr"
                    Display="Dynamic" ErrorMessage="Email Address cannot be blank ">*</asp:RequiredFieldValidator><br />
                <asp:RegularExpressionValidator
                        ID="RegularExpressionValidatorEmailAddress" runat="server" ControlToValidate="TextboxEmailAddr"
                        ErrorMessage="Email format is incorrect" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator></td>
			<td bgColor="#ffffff">
                Email Address</td>
			<td bgColor="#ffffff">:</td>
			<td bgColor="#ffffff">
                <asp:TextBox ID="TextboxEmailAddr" runat="server" CssClass="textBox" MaxLength="50" Width="200px" ></asp:TextBox>
                <strong><span style="color: #ff0000">*</span></strong></td>
		</tr>
		<tr class="formText">
			<td bgColor="#ffffff" height="24">
                <asp:RegularExpressionValidator ID="RegularExpressionValidatorMobilePhone" runat="server"
                    ControlToValidate="TextboxMobilePhone" ErrorMessage="Mobile Phone must be digits only and at least 8 digits long"
                    ValidationExpression="\d{8}\d*">*</asp:RegularExpressionValidator></td>
			<td bgColor="#ffffff">
                Mobile Phone</td>
			<td bgColor="#ffffff">:</td>
			<td bgColor="#ffffff">
                <asp:TextBox ID="TextboxMobilePhone" runat="server" CssClass="textBox" MaxLength="15" Width="200px" ></asp:TextBox></td>
		</tr>
        <tr class="formText">
			<td bgColor="#ffffff" height="24">
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorTellerID" runat="server" ControlToValidate="TextboxTellerID"
                    Display="Dynamic" ErrorMessage="Teller ID cannot be blank">*</asp:RequiredFieldValidator><br />
			<td bgColor="#ffffff">
                Teller ID</td>
			<td bgColor="#ffffff">:</td>
			<td bgColor="#ffffff">
                <asp:TextBox ID="TextboxTellerID" runat="server" CssClass="textBox" MaxLength="10" Width="200px" ></asp:TextBox>
                <strong><span style="color: #ff0000">*</span></strong>
			</td>
		</tr>
		<tr class="formText">
			<td bgColor="#ffffff" height="24">&nbsp;</td>
			<td bgColor="#ffffff">
                Group</td>
			<td bgColor="#ffffff">:</td>
			<td bgColor="#ffffff"><asp:dropdownlist id="DropdownlistGroup" runat="server" CssClass="comboBox"></asp:dropdownlist>
                <strong><span style="color: #ff0000">*</span></strong></td>
		</tr>
		<tr class="formText" bgColor="#dddddd" height="30">
			<td width="15"><IMG height="15" src="images/arrow.gif" width="15"></td>
			<td colSpan="3">
				<table cellSpacing="0" cellPadding="3" border="0">
					<tr>
						<td><asp:imagebutton id="ImageSave" runat="server" CausesValidation="True" SkinID="UpdateButton"></asp:imagebutton></td>
						<td><asp:imagebutton id="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton"></asp:imagebutton></td>
					</tr>
				</table>               
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
		</tr>
	</table>
	<script language="javascript" type="text/javascript">
	    document.getElementById('<%=GetFocusID %>').focus();
	</script>
</asp:Content>