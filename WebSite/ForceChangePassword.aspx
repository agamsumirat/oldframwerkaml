<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ForceChangePassword.aspx.vb" Inherits="ForceChangePassword" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<%@ Register Src="webcontrol/footer.ascx" TagName="footer" TagPrefix="uc1" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>:: Anti Money Laundering ::</title>
		<script src="script/x_load.js" type="text/javascript"></script>
		<script language="javascript">xInclude('script/x_core.js');</script>
		<script src="script/aml.js" ></script>	
		<link href="theme/aml.css" rel="stylesheet" type="text/css">
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	</HEAD>
	<body bgColor="#ffffff" leftMargin="0" topMargin="0" marginheight="0" marginwidth="0">
	 <form id="form1" runat="server">
	<table background="images/main-header-bg.gif" height="81" width="100%" cellpadding="0" cellspacing="0" border="0">
  	    <tr>
            <td width="153"><img src="images/main-uang-dijemur-dan-logo.jpg" width="153" height="81" /></td>
  	        <td width="357"><img src="images/main-title.jpg" width="357" height="25" /></td>
  	        <td width="99%">&nbsp;</td>
  	        <td align="right" background="images/main-bar-merah-kanan.jpg" valign="top" class="toprightmenu"><img src="images/blank.gif" width="425" height="6">
  	        </td>               
  	    </tr>
  	</table>
    <table width="100%" ="width: 100%; height: 184px" cellpadding="0" cellspacing="0" border="0" >
        <tr>
    
            <td colspan="3" rowspan="1" style="width: 654px" id="tdcontent" valign="top">
                <div id="divcontent" class="divcontent" >
                <table cellSpacing="0" cellPadding="0" width="100%">
  													
								<tr>
									<td class="maintitle" vAlign="bottom" width="99%" bgColor="#ffffff"><asp:label id="lblTitle" runat="server">Mandatory Password Change</asp:label></td>
								</tr>
								<tr>
									<td colSpan="4" bgColor="#ffffff">
										<p><font color="#ff0000"><b>You Password has expired!</b></font>.<br>
                                            You must change your password now before continuing to use the AML System.
                                            <br />
                                            After you have successully changed your password, you will be logged out and then
                                            <br />
                                            you have to log back in to use the AML System.</p>
                                        <p>
                                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
                                            &nbsp;</p>
									</td>
								</tr>								
                                <tr>
                                    <td bgcolor="#ffffff" colspan="4"><span style="color: #ff0000">* Required</span></td>
                                </tr>
								<tr>
                                    <td>
									 <table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
											border="2">
											<tr class="formText">
												<td width="5" bgColor="#ffffff" height="24">&nbsp;</td>
												<td width="20%" bgColor="#ffffff">User Id</td>
												<td width="5" bgColor="#ffffff">:</td>
												<td bgColor="#ffffff"><asp:label id="LabelUserId" runat="server"></asp:label></td>
											</tr>
											<tr class="formText">
												<td width="5" bgColor="#ffffff" height="24">
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextPassword"
                                                        ErrorMessage="Password is required">*</asp:RequiredFieldValidator></td>
												<td width="20%" bgColor="#ffffff" height="24">Password</td>
												<td bgColor="#ffffff" height="24">:</td>
												<td width="80%" bgColor="#ffffff" height="24"><asp:textbox id="TextPassword" runat="server" CssClass="textBox" TextMode="Password" MaxLength="50"></asp:textbox>
                                                    <strong><span style="color: #ff0000">*</span></strong></td>
											</tr>
											
											<tr class="formText">
												<td bgColor="#ffffff" height="24">
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBoxRetype"
                                                        Display="Dynamic" ErrorMessage="Password Confirmation is required">*</asp:RequiredFieldValidator><br />											
                                                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="TextPassword"
                                                        ControlToValidate="TextBoxRetype" Display="Dynamic" ErrorMessage="Password Confirmation does not match">*</asp:CompareValidator>&nbsp;</td>
												<td bgColor="#ffffff">
                                                    Confirm Password</td>
												<td bgColor="#ffffff">:</td>
												<td bgColor="#ffffff"><asp:textbox id="TextBoxRetype" runat="server" CssClass="textBox" TextMode="Password" MaxLength="50"></asp:textbox>
                                                    <strong><span style="color: #ff0000">*</span></strong></td>
											</tr>
											<tr class="formText" bgColor="#dddddd" height="30">
												<td width="15"><IMG height="15" src="images/arrow.gif" width="15"></td>
												<td colSpan="3">
													<table cellSpacing="0" cellPadding="3" border="0">
														<tr>
															<td><asp:imagebutton id="ImageSave" runat="server" CausesValidation="True" skinid="savebutton"></asp:imagebutton></td>
															<td><asp:imagebutton id="ImageCancel" runat="server" CausesValidation="False" skinid="cancelButton"></asp:imagebutton></td>
														</tr>
													</table>
													<asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
											</tr>
										</table>
                                    </td>
								</tr>

					</table>                 
                </div>
            </td>
        </tr>
		<tr>
            <td align="left" colspan="4" valign="top">
                <uc1:footer ID="Footer1" runat="server"/>
            </td>
        </tr>        
    </table>         	
	<script>document.getElementById('<%=GetFocusID %>').focus();</script>
	</form>
	<script>arrangeViewNoMenu();
	window.onresize=arrangeViewNoMenu;
	</script>
	</body>
</HTML>

