<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ChangeEmailTemplate.aspx.vb" Inherits="ChangeEmailTemplate" MasterPageFile="~/MasterPage.master" %>

<asp:Content ID="EmailTemplate" runat="server" ContentPlaceHolderID="cpContent">
	<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2" height="72" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
        <tr class="formText">
            <td bgcolor="#ffffff" style="height: 24px" width="2%">
            </td>
            <td bgcolor="#ffffff" colspan="3" style="height: 24px">
                Serial # :
                <asp:Label ID="LblSerialEmail" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" style="height: 24px" width="2%">
            </td>
            <td bgcolor="#ffffff" colspan="3" style="height: 24px">
                Workflow Step Name:&nbsp;
                <asp:Label ID="LblWorkflowStepNameEmail" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" style="height: 24px" width="2%">
            </td>
            <td bgcolor="#ffffff" colspan="3" style="height: 24px">
            </td>
        </tr>
		<tr class="formText">
			<td bgColor="#ffffff" style="height: 24px" width="2%"><asp:requiredfieldvalidator id="RequiredFieldValidatorEmail" runat="server" ErrorMessage="Subject Email  is required"
					ControlToValidate="TxtSubjectEmail" Width="1px">*</asp:requiredfieldvalidator><br />
                </td>
			<td width="7%" bgColor="#ffffff" style="height: 24px">
                Subject</td>
			<td width="1%" bgColor="#ffffff" style="height: 24px">:</td>
			<td width="80%" bgColor="#ffffff" style="height: 24px"><asp:textbox id="TxtSubjectEmail" runat="server" CssClass="textBox" MaxLength="50" Width="200px" ></asp:textbox>
                <strong><span style="color: #ff0000">*</span></strong></td>
		</tr>
		<tr class="formText">
			<td bgColor="#ffffff" style="height: 24px;" rowspan="6" width="2%">
                <br />
                <br />
                <br />
                <br />
                &nbsp;</td>
			<td bgColor="#ffffff" rowspan="6" style="height: 24px" valign="top" width="7%">
                Body<br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" rowspan="6" style="height: 24px" valign="top" width="1%">
                :<br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" rowspan="6" style="height: 24px">
                <asp:textbox id="TxtBodyEmail" runat="server" CssClass="textBox" MaxLength="255" Width="478px" Height="133px" TextMode="MultiLine"></asp:textbox></td>
		</tr>
		<tr class="formText">
		</tr>
		<tr class="formText">
		</tr>
		<tr class="formText">
		</tr>
		<tr class="formText">
		</tr>
		<tr class="formText">
		</tr>
		<tr class="formText" bgColor="#dddddd" height="30">
			<td style="width: 24px"><IMG height="15" src="images/arrow.gif" width="15"></td>
			<td colSpan="3" style="height: 9px">
				<table cellSpacing="0" cellPadding="3" border="0">
					<tr>
						<td><asp:imagebutton id="ImageSaveEmail" runat="server" CausesValidation="True" SkinID="AddButton"></asp:imagebutton></td>
						<td><asp:imagebutton id="ImageCancelEmail" runat="server" CausesValidation="False" SkinID="CancelButton"></asp:imagebutton></td>
					</tr>
				</table>               </td>
		</tr>
	</table>
	<script>
	    document.getElementById('<%=GetFocusID %>').focus();
	</script>
</asp:Content>