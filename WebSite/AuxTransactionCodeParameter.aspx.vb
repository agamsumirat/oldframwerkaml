Imports System.IO
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports AjaxControlToolkit
Imports System.Web.UI





Partial Class AuxTransactionCodeParameter
    Inherits Parent





    Private Shared ArrayListExcludeFile As New System.Collections.ArrayList

    Public Sub FillSelectedFile()
        Try            
            Using AccessProduct As New AMLDAL.AuxTransactionCodeTableAdapters.AuxTransactionCodeTableAdapter
                Me.ListSelectedFile.Items.Clear()
                Me.ListSelectedFile.DataSource = AccessProduct.SelectSelected
                Me.ListSelectedFile.DataTextField = "TransactionDescription"
                Me.ListSelectedFile.DataValueField = "TransactionCode"
                Me.ListSelectedFile.DataBind()
            End Using
        Catch
            Throw New Exception("Unable to fill selected list")
        End Try
    End Sub

    Public Sub FillAvailableFile()
        Try
            Using AccessProduct As New AMLDAL.AuxTransactionCodeTableAdapters.AuxTransactionCodeTableAdapter
                Me.ListAvailableFile.Items.Clear()
                Me.ListAvailableFile.DataSource = AccessProduct.SelectAvailable
                Me.ListAvailableFile.DataTextField = "TransactionDescription"
                Me.ListAvailableFile.DataValueField = "TransactionCode"
                Me.ListAvailableFile.DataBind()
            End Using
        Catch
            Throw New Exception("Unable to fill Available List")
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Page.ClientScript.RegisterStartupScript(GetType(), "removeWebForm_InitCallback" & Guid.NewGuid.ToString, "WebForm_InitCallback=function() {};", True)
            Me.lblSuccess.Visible = False
            If Not Me.IsPostBack Then
                FillSelectedFile()
                FillAvailableFile()
            End If
            Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
            End Using
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub ImageAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAdd.Click
        Dim ObjListItem As ListItem
        Dim ObjArrayRemove As New ArrayList
        Dim i As Integer
        Dim j As Integer = 0
        Try
            For Each ObjListItem In ListAvailableFile.Items
                If ObjListItem.Selected Then
                    If Not ListSelectedFile.Items.Contains(ObjListItem) Then
                        ListSelectedFile.Items.Add(ListAvailableFile.Items(j))
                        ObjArrayRemove.Add(ListAvailableFile.Items(j))
                    End If
                End If
                j += 1
            Next
            For i = 0 To ObjArrayRemove.Count - 1
                ListAvailableFile.Items.Remove(ObjArrayRemove(i))
            Next
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            ObjArrayRemove = Nothing
            ObjListItem = Nothing
        End Try
    End Sub

    Protected Sub ImageRemove_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageRemove.Click
        If ListSelectedFile.Items.Count > 0 Then
            Dim ObjItem As ListItem
            Dim ObjArrayRemove As New ArrayList
            Dim i As Integer
            Try
                For Each ObjItem In ListSelectedFile.Items
                    If ObjItem.Selected Then
                        ObjArrayRemove.Add(ObjItem)
                    End If
                Next
                For i = 0 To ObjArrayRemove.Count - 1
                    ListSelectedFile.Items.Remove(ObjArrayRemove(i))
                    ListAvailableFile.Items.Add(ObjArrayRemove(i))
                Next
            Catch ex As Exception
                Me.cvalPageErr.IsValid = False
                Me.cvalPageErr.ErrorMessage = ex.Message
                LogError(ex)
            Finally
                ObjItem = Nothing
                ObjArrayRemove = Nothing
            End Try
        End If
    End Sub

    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim Trans As SqlTransaction
        Try
            If Page.IsValid Then
                Dim transactionCode As String = ""
                Dim DebitOrCredit As String = ""
                Dim Description As String = ""
                Dim myString As String = ""
                Dim abc As String = ""
                'jika Super User
                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                    Try
                        Using AccessTable As New AMLDAL.AuxTransactionCodeTableAdapters.AuxTransactionCodeTableAdapter
                            Trans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessTable)
                            ' Hapus semua data pada table AuxTransactionCodeParameter
                            AccessTable.DeleteByTransactionCode()

                            Using AccessAuditTrail As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter

                                'Masukkan data baru ke table AuxTransactionCodeParameter
                                For x As Integer = 0 To Me.ListSelectedFile.Items.Count - 1
                                    Me.ListSelectedFile.SelectedIndex = x
                                    AccessTable.InsertNewTransactionCode(Me.ListSelectedFile.SelectedValue, Now)

                                    AccessAuditTrail.Insert(Now, Nothing, Sahassa.AML.Commonly.SessionUserId, "AuxTransactionCodeParameter", "AuxTransactionCode", "Add", "", Me.ListSelectedFile.SelectedValue, "Accepted")
                                    AccessAuditTrail.Insert(Now, Nothing, Sahassa.AML.Commonly.SessionUserId, "AuxTransactionCodeParameter", "LastUpdatedDate", "Add", "", Now, "Accepted")
                                Next
                            End Using
                            Trans.Commit()
                        End Using
                    Catch ex As Exception
                        If Not Trans Is Nothing Then Trans.Rollback()
                        Me.cvalPageErr.IsValid = False
                        Me.cvalPageErr.ErrorMessage = ex.Message
                        LogError(ex)
                    Finally
                        If Not Trans Is Nothing Then
                            Trans.Dispose()
                        End If
                    End Try
                Else ' jika bukan super user 
                    Try
                        Dim PendingId As New Long
                        Using AccessTable As New AMLDAL.AuxTransactionCodeTableAdapters.InsertTransactionPendingApprovalTableAdapter
                            Trans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessTable)
                            'Masukkan data ke table AuxTransactionCode_PendingApproval
                            PendingId = AccessTable.InsertTransactionPendingApproval(Sahassa.AML.Commonly.SessionUserId, Now)

                            Using AccessAuditTrail As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                                'Masukkan data baru ke table AuxTransactionCodeParameter_Approval
                                For x As Integer = 0 To Me.ListSelectedFile.Items.Count - 1
                                    Me.ListSelectedFile.SelectedIndex = x
                                    AccessTable.InsertTransactionApproval(PendingId, Me.ListSelectedFile.SelectedValue)

                                    AccessAuditTrail.Insert(Now, Nothing, Sahassa.AML.Commonly.SessionUserId, "AuxTransactionCodeParameter", "Fx_AuxTransactionCodeParameter_PendingApprovalId", "Add", "", PendingId, "Accepted")
                                    AccessAuditTrail.Insert(Now, Nothing, Sahassa.AML.Commonly.SessionUserId, "AuxTransactionCodeParameter", "AuxTransactionCode", "Add", "", Me.ListSelectedFile.SelectedValue, "Accepted")
                                Next
                            End Using
                            Trans.Commit()
                        End Using

                        Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=81303"
                        Me.Response.Redirect("MessagePending.aspx?MessagePendingID=81303", False)
                    Catch ex As Exception
                        If Not Trans Is Nothing Then Trans.Rollback()
                        Me.cvalPageErr.IsValid = False
                        Me.cvalPageErr.ErrorMessage = ex.Message
                        LogError(ex)
                    Finally
                        If Not Trans Is Nothing Then
                            Trans.Dispose()
                        End If
                    End Try
                End If

            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "Default.aspx"
        Me.Response.Redirect("Default.aspx", False)
    End Sub

End Class