﻿Imports SahassaNettier.Entities
Imports SahassaNettier.Data
Imports AMLBLL

Partial Class InsiderCodeEdit
    Inherits Parent


    ReadOnly Property GetPk_InsiderCode As Long
        Get
            If IsNumeric(Request.Params("InsiderCodeID")) Then
                If Not IsNothing(Session("InsiderCodeEdit.PK")) Then
                    Return CLng(Session("InsiderCodeEdit.PK"))
                Else
                    Session("InsiderCodeEdit.PK") = Request.Params("InsiderCodeID")
                    Return CLng(Session("InsiderCodeEdit.PK"))
                End If
            End If
            Return 0
        End Get
    End Property

    Sub ClearSession()
        Session("InsiderCodeEdit.PK") = Nothing
    End Sub

    Property SetnGetInsiderCodeID As String
        Get
            Return Session("InsiderCodeEdit.InsiderCodeID")
        End Get
        Set(ByVal value As String)
            Session("InsiderCodeEdit.InsiderCodeID") = value
        End Set
    End Property



    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "InsiderCodeView.aspx"

            Me.Response.Redirect("InsiderCodeView.aspx", False)
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Function isValidData() As Boolean
        Try
            If txtInsiderCodeID.Text.Trim.Length = 0 Then
                Throw New Exception("InsiderCode Code must be filled")
            End If
            If txtInsiderCodeID.Text.ToLower <> SetnGetInsiderCodeID.ToLower Then
                If Not InsiderCodeBLL.IsUniqueInsiderCode(txtInsiderCodeID.Text) Then
                    Throw New Exception("Sement Code " & txtInsiderCodeID.Text & " existed")
                End If
            End If
            Return True
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
            Return False
        End Try
    End Function

    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Try
            If isValidData() Then
                Using objUser As User = AMLBLL.UserBLL.GetUserByPkUserID(Sahassa.AML.Commonly.SessionPkUserId)
                    If Not IsNothing(objUser) Then
                        Using objInsiderCode As InsiderCode = InsiderCodeBLL.getInsiderCodeByPk(GetPk_InsiderCode)
                            If Not IsNothing(objInsiderCode) Then
                                objInsiderCode.InsiderCodeID = txtInsiderCodeID.Text
                                objInsiderCode.InsiderCodeName = txtInsiderCodeName.Text
                                objInsiderCode.Activation = True
                                'objInsiderCode.CreatedDate = Now
                                'objInsiderCode.CreatedBy = objUser.UserName
                                objInsiderCode.LastUpdateDate = Now
                                objInsiderCode.LastUpdateBy = objUser.UserName


                                If Sahassa.AML.Commonly.SessionGroupName.ToLower = "superuser" Then
                                    If InsiderCodeBLL.SaveEdit(objInsiderCode) Then
                                        lblMessage.Text = "Edit Data Success"
                                        mtvInsiderCodeAdd.ActiveViewIndex = 1
                                    End If
                                Else
                                    If InsiderCodeBLL.SaveEditApproval(objInsiderCode) Then
                                        lblMessage.Text = "Edited Data has been inserted to Approval"
                                        mtvInsiderCodeAdd.ActiveViewIndex = 1
                                    End If
                                End If
                            End If
                        End Using

                    End If
                End Using
            End If

        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Sub LoadData()
        Using objInsiderCode As InsiderCode = InsiderCodeBLL.getInsiderCodeByPk(GetPk_InsiderCode)
            If Not IsNothing(objInsiderCode) Then
                txtInsiderCodeName.Text = objInsiderCode.InsiderCodeName.ToString
                txtInsiderCodeID.Text = objInsiderCode.InsiderCodeID.ToString
                SetnGetInsiderCodeID = objInsiderCode.InsiderCodeID
            Else
                ImageSave.Visible = False
            End If
        End Using
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                mtvInsiderCodeAdd.ActiveViewIndex = 0
                ClearSession()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
                LoadData()
            End If
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnOK.Click
        Try
            Response.Redirect("InsiderCodeView.aspx")
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

End Class


