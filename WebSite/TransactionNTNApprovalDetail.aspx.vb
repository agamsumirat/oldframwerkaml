Option Explicit On
Option Strict On

Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports AMLBLL

Partial Class TransactionNTNApprovalDetail
    Inherits Parent

    Public ReadOnly Property PKTransactionNTNApprovalID() As Integer
        Get
            If Session("TransactionNTNApprovalDetail.PK") Is Nothing Then
                Dim StrTmppkTransactionNTNApprovalId As String

                StrTmppkTransactionNTNApprovalId = Request.Params("PKTransactionNTNApprovalID")
                If Integer.TryParse(StrTmppkTransactionNTNApprovalId, 0) Then
                    Session("TransactionNTNApprovalDetail.PK") = StrTmppkTransactionNTNApprovalId
                Else
                    Throw New SahassaException("Parameter PKTransactionNTNApprovalID is not valid.")
                End If

                Return CInt(Session("TransactionNTNApprovalDetail.PK"))
            Else
                Return CInt(Session("TransactionNTNApprovalDetail.PK"))
            End If
        End Get
    End Property

    Public ReadOnly Property ObjTransactionNTNApproval() As TList(Of TransactionNTN_Approval)
        Get
            Dim PkApprovalid As Integer
            Try
                If Session("TransactionNTNApprovalDetail.ObjTransactionNTNApproval") Is Nothing Then
                    PkApprovalid = PKTransactionNTNApprovalID
                    Session("TransactionNTNApprovalDetail.ObjTransactionNTNApproval") = DataRepository.TransactionNTN_ApprovalProvider.GetPaged("PK_TransactionNTN_ApprovalID = " & PkApprovalid, "", 0, Integer.MaxValue, 0)
                End If
                Return CType(Session("TransactionNTNApprovalDetail.ObjTransactionNTNApproval"), TList(Of TransactionNTN_Approval))
            Finally
            End Try
        End Get
    End Property

    Private Sub LoadDataApproval()
        If ObjTransactionNTNApproval.Count > 0 Then
            If ObjTransactionNTNApproval(0).FK_ModeID.GetValueOrDefault(0) = 2 Then
                Me.LblAction.Text = "Edit"
            End If

            Dim strRequestBy As String = ""
            Using objRequestBy As User = SahassaNettier.Data.DataRepository.UserProvider.GetBypkUserID(ObjTransactionNTNApproval(0).FK_MsUserID.GetValueOrDefault(0))
                If Not objRequestBy Is Nothing Then
                    strRequestBy = objRequestBy.UserID & " ( " & objRequestBy.UserName & " ) "
                End If
            End Using

            LblRequestBy.Text = strRequestBy
            LblRequestDate.Text = ObjTransactionNTNApproval(0).CreatedDate.GetValueOrDefault().ToString("dd-MM-yyyy")

            If ObjTransactionNTNApproval(0).FK_ModeID.GetValueOrDefault(0) = 2 Then
                'Old

                Me.GrdVwTransactionNTNOld.DataSource = DataRepository.TransactionNTNProvider.GetPaged("", "Tier", 0, Integer.MaxValue, 0)
                Me.GrdVwTransactionNTNOld.DataBind()

                'New
                Me.GrdVwTransactionNTNNew.DataSource = DataRepository.TransactionNTN_ApprovalDetailProvider.GetPaged("", "Tier", 0, Integer.MaxValue, 0)
                Me.GrdVwTransactionNTNNew.DataBind()
            End If
        Else
            'kalau ternyata sudah di approve /reject dan memakai tombol back, maka redirect ke viewapproval
            Response.Redirect("TransactionNTNApproval.aspx", False)
        End If
    End Sub

    Private Sub ClearSession()
        Session("TransactionNTNApprovalDetail.PK") = Nothing
        Session("TransactionNTNApprovalDetail.ObjTransactionNTNApproval") = Nothing
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                ClearSession()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using

                LoadDataApproval()
            End If
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImgBtnAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAccept.Click
        Try
            If ObjTransactionNTNApproval.Count > 0 Then
                Using ObjTransactionNTNBll As New TransactionNTNBLL
                    If ObjTransactionNTNBll.AcceptEdit Then
                        Response.Redirect("TransactionNTNApproval.aspx", False)
                    End If
                End Using
            Else
                Response.Redirect("TransactionNTNApproval.aspx", False)
            End If

        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBtnReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnReject.Click
        Try
            If ObjTransactionNTNApproval.Count > 0 Then
                Using ObjTransactionNTNBll As New TransactionNTNBLL
                    If ObjTransactionNTNBll.RejectEdit Then
                        Response.Redirect("TransactionNTNApproval.aspx", False)
                    End If
                End Using
            Else
                Response.Redirect("TransactionNTNApproval.aspx", False)
            End If
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBackAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBackAdd.Click
        Try
            Response.Redirect("TransactionNTNApproval.aspx", False)
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    'Protected Sub CvalHandleErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles CvalHandleErr.PreRender
    '    If CvalHandleErr.IsValid = False Then
    '        Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
    '    End If
    'End Sub

    'Protected Sub CvalPageErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles CvalPageErr.PreRender
    '    If CvalPageErr.IsValid = False Then
    '        Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
    '    End If
    'End Sub
End Class