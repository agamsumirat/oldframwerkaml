#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports Sahassa.AML
#End Region

Partial Class MsKelurahan_Delete
    Inherits Parent

#Region "Function"

    Sub ChangeMultiView(ByVal index As Integer)
        MtvMsUser.ActiveViewIndex = index
    End Sub

    Private Sub BindListMapping()
        LBMapping.DataSource = Listmaping
        LBMapping.DataTextField = "Nama"
        LBMapping.DataValueField = "IDKelurahanNCBS"
        LBMapping.DataBind()
    End Sub


#End Region

#Region "events..."

    Public ReadOnly Property parID() As String
        Get
            Return Request.Item("ID")
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                'AuditTrailBLL.InsertAuditTrailUserAccess(SessionCurrentPage)
                Listmaping = New TList(Of MappingMsKelurahanNCBSPPATK)
                LoadData()
            Catch ex As Exception
                LogError(ex)
                CvalPageErr.IsValid = False
                CvalPageErr.ErrorMessage = ex.Message
            End Try
        End If

    End Sub

    Private Sub LoadData()
        Dim ObjMsKelurahan As MsKelurahan = DataRepository.MsKelurahanProvider.GetPaged(MsKelurahanColumn.IDKelurahan.ToString & "=" & parID, "", 0, 1, Nothing)(0)
        If ObjMsKelurahan Is Nothing Then Throw New Exception("Data Not Found")
        With ObjMsKelurahan
            SafeDefaultValue = "-"
            HFKecamatan.Value = Safe(.IDKecamatan)
            Dim OKecamatan As MsKecamatan = DataRepository.MsKecamatanProvider.GetByIDKecamatan(.IDKecamatan.GetValueOrDefault)
            If OKecamatan IsNot Nothing Then LBSearchNamaKecamatan.Text = Safe(OKecamatan.NamaKecamatan)

            lblIDKelurahan.Text = .IDKelurahan
            lblNamaKelurahan.Text = Safe(.NamaKelurahan)

            'other info
            Dim Omsuser As TList(Of User)
            Omsuser = DataRepository.UserProvider.GetByUserID(.CreatedBy)
            If Omsuser.Count > 0 Then
                lblCreatedBy.Text = Omsuser(0).UserName
            End If
            lblCreatedDate.Text = FormatDate(.CreatedDate)
            Omsuser = DataRepository.UserProvider.GetByUserID(.LastUpdatedBy)
            If Omsuser.Count > 0 Then
                lblUpdatedby.Text = Omsuser(0).UserName
            End If
            lblUpdatedDate.Text = FormatDate(.LastUpdatedDate)
            lblActivation.Text = SafeActiveInactive(.Activation)
            Dim L_objMappingMsKelurahanNCBSPPATK As TList(Of MappingMsKelurahanNCBSPPATK)
            L_objMappingMsKelurahanNCBSPPATK = DataRepository.MappingMsKelurahanNCBSPPATKProvider.GetPaged(MappingMsKelurahanNCBSPPATKColumn.IDKelurahan.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)

            Listmaping.AddRange(L_objMappingMsKelurahanNCBSPPATK)
        End With
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            BindListMapping()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

#End Region

#Region "Property..."

    ''' <summary>
    ''' Menyimpan Item untuk mapping sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Listmaping() As TList(Of MappingMsKelurahanNCBSPPATK)
        Get
            Return Session("Listmaping.data")
        End Get
        Set(ByVal value As TList(Of MappingMsKelurahanNCBSPPATK))
            Session("Listmaping.data") = value
        End Set
    End Property

#End Region

#Region "events..."
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Response.Redirect("MsKelurahan_View.aspx")
    End Sub

    Protected Sub imgOk_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgOk.Click
        Try
            Page.Validate("handle")
            If Page.IsValid Then

                ' =========== Insert Header Approval
                Dim KeyHeaderApproval As Integer
                Using ObjMsKelurahan_Approval As New MsKelurahan_Approval
                    With ObjMsKelurahan_Approval
                        .FK_MsMode_Id = 3
                        FillOrNothing(.RequestedBy, SessionPkUserId)
                        FillOrNothing(.RequestedDate, Date.Now)
                        .IsUpload = False
                    End With
                    DataRepository.MsKelurahan_ApprovalProvider.Save(ObjMsKelurahan_Approval)
                    KeyHeaderApproval = ObjMsKelurahan_Approval.PK_MsKelurahan_Approval_Id
                End Using
                '============ Insert Detail Approval
                Using objMsKelurahan_ApprovalDetail As New MsKelurahan_ApprovalDetail()
                    With objMsKelurahan_ApprovalDetail
                        Dim ObjMsKelurahan As MsKelurahan = DataRepository.MsKelurahanProvider.GetByIDKelurahan(parID)
                        .IDKecamatan = HFKecamatan.Value.ToString
                        FillOrNothing(.IDKelurahan, lblIDKelurahan.Text, True, oInt)
                        FillOrNothing(.NamaKelurahan, lblNamaKelurahan.Text, True, Ovarchar)

                        FillOrNothing(.IDKelurahan, ObjMsKelurahan.IDKelurahan)
                        FillOrNothing(.Activation, ObjMsKelurahan.Activation)
                        FillOrNothing(.CreatedDate, ObjMsKelurahan.CreatedDate)
                        FillOrNothing(.CreatedBy, ObjMsKelurahan.CreatedBy)
                        FillOrNothing(.FK_MsKelurahan_Approval_Id, KeyHeaderApproval)
                    End With
                    DataRepository.MsKelurahan_ApprovalDetailProvider.Save(objMsKelurahan_ApprovalDetail)
                    Dim keyHeaderApprovalDetail As Integer = objMsKelurahan_ApprovalDetail.PK_MsKelurahan_ApprovalDetail_Id

                    '========= Insert mapping item 
                    Dim LobjMappingMsKelurahanNCBSPPATK_Approval_Detail As New TList(Of MappingMsKelurahanNCBSPPATK_Approval_Detail)

                    Dim L_ObjMappingMsKelurahanNCBSPPATK As TList(Of MappingMsKelurahanNCBSPPATK) = DataRepository.MappingMsKelurahanNCBSPPATKProvider.GetPaged(MappingMsKelurahanNCBSPPATKColumn.IDKelurahan.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)

                    For Each objMappingMsKelurahanNCBSPPATK As MappingMsKelurahanNCBSPPATK In L_ObjMappingMsKelurahanNCBSPPATK
                        Dim objMappingMsKelurahanNCBSPPATK_Approval_Detail As New MappingMsKelurahanNCBSPPATK_Approval_Detail
                        With objMappingMsKelurahanNCBSPPATK_Approval_Detail
                            FillOrNothing(.IDKelurahan, objMappingMsKelurahanNCBSPPATK.PK_MappingMsKelurahanNCBSPPATK_Id)
                            FillOrNothing(.IDKelurahanNCBS, objMappingMsKelurahanNCBSPPATK.IDKelurahanNCBS)
                            FillOrNothing(.PK_MsKelurahan_Approval_Id, KeyHeaderApproval)
                            FillOrNothing(.PK_MappingMsKelurahanNCBSPPATK_Id, objMappingMsKelurahanNCBSPPATK.PK_MappingMsKelurahanNCBSPPATK_Id)
                            .Nama = objMappingMsKelurahanNCBSPPATK.Nama
                            LobjMappingMsKelurahanNCBSPPATK_Approval_Detail.Add(objMappingMsKelurahanNCBSPPATK_Approval_Detail)
                        End With
                        DataRepository.MappingMsKelurahanNCBSPPATK_Approval_DetailProvider.Save(LobjMappingMsKelurahanNCBSPPATK_Approval_Detail)
                    Next
                    'session Maping item Clear
                    Listmaping.Clear()
                    Me.imgOk.Visible = False
                    Me.ImageCancel.Visible = False
                    LblConfirmation.Text = "Data has been Delete and waiting for approval"
                    MtvMsUser.ActiveViewIndex = 1
                End Using
            End If
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImgBtnAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAdd.Click
        Response.Redirect("MsKelurahan_View.aspx")
    End Sub

#End Region

End Class



