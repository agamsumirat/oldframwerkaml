<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="CTRFileUploadReferenceNumber.aspx.vb" Inherits="CTRFileUploadReferenceNumber" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td>
                <img src="Images/blank.gif" width="5" height="1" /></td>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                       <td>&nbsp;</td> <td width="99%" bgcolor="#FFFFFF">
                            <img src="Images/blank.gif" width="1" height="1" /></td>
                        <td bgcolor="#FFFFFF">
                            <img src="Images/blank.gif" width="1" height="1" /></td>
                            
                    </tr>
                </table>
            </td>
            <td>
                <img src="Images/blank.gif" width="5" height="1" /></td>
        </tr>
        <tr>
            <td>
            </td>
            <td id="tdcontent" height="99%" valign="top" bgcolor="#FFFFFF">
                <div id="divcontent" class="divcontent">
                    <table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td>
                                <img src="Images/blank.gif" width="20" height="100%" /></td>
                            <td class="divcontentinside" bgcolor="#FFFFFF">
                                <ajax:AjaxPanel ID="AjaxPanel5" runat="server" Width="100%">
                                    <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
                                        style="border-top-style: none; border-right-style: none; border-left-style: none;
                                        border-bottom-style: none" width="100%">
                                        <tr>
                                            <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                                                border-right-style: none; border-left-style: none; border-bottom-style: none;
                                                font-family: Tahoma">
                                                <img src="Images/dot_title.gif" width="17" height="17">&nbsp;<b><asp:Label ID="Label1"
                                                    runat="server" Text="Upload Reference Number"></asp:Label></b><hr />
                                                        <ajax:ajaxpanel id="AjaxPanel3" runat="server"><asp:Label ID="LblSucces" CssClass="validationok" Width="94%" runat="server" Visible="False" ></asp:Label></ajax:ajaxpanel>
                                                    
                                            </td>
                                        </tr>
                                    </table>
                                    <table border="0" cellpadding="0" cellspacing="0" style="background-color: White;
                                        border-color: White;" width="97%">
                                        <tr>
                                            <td>
                                                <table align="left" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                    bgcolor="#dddddd" border="2">
                                                    <tr>
                                                        <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                            style="height: 6px">
                                                            <table cellpadding="0" cellspacing="0" border="0">
                                                                <tr>
                                                                    <td class="formtext">
                                                                        <asp:Label ID="Label9" runat="server" Text="CTR Report - PPATK Format" Font-Bold="True"></asp:Label>&nbsp;</td>
                                                                    <td>
                                                                        <a href="#" onclick="javascript:ShowHidePanel('CTRReportPPATKFormat','ImgCTRReportPPATKFormat','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                            title="click to minimize or maximize">
                                                                            <img id="ImgCTRReportPPATKFormat" src="Images/search-bar-minimize.gif" border="0"
                                                                                height="12px" width="12px"></a></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr id="CTRReportPPATKFormat">
                                                        <td valign="top" bgcolor="#ffffff">
                                                            <ajax:AjaxPanel ID="AjaxPanel1" runat="server" Width="100%">
                                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                    <tr class="formText">
                                                                        <td width="15%" bgcolor="#FFF7E6">
                                                                            <asp:Label ID="Label20" runat="server" Text="Text File Name"></asp:Label></td>
                                                                        <td width="1%" bgcolor="#ffffff">
                                                                            :</td>
                                                                        <td width="80%" bgcolor="#ffffff">
                                                                            <asp:Label ID="LblTextFileName" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="formText">
                                                                        <td width="15%" bgcolor="#FFF7E6">
                                                                            <asp:Label ID="Label24" runat="server" Text="Total # CIF"></asp:Label>/# WIC</td>
                                                                        <td width="1%" bgcolor="#ffffff">
                                                                            :</td>
                                                                        <td width="80%" bgcolor="#ffffff">
                                                                            <asp:Label ID="LblTotalNumberofCIF" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="formText">
                                                                        <td width="15%" bgcolor="#FFF7E6">
                                                                            <asp:Label ID="Label3" runat="server" Text="Total Amount"></asp:Label></td>
                                                                        <td width="1%" bgcolor="#ffffff">
                                                                            :</td>
                                                                        <td width="80%" bgcolor="#ffffff">
                                                                            (IDR-Cash In&nbsp; )&nbsp;<asp:Label ID="LblTotalAmountCashIn" runat="server"></asp:Label>
                                                                            <br />
                                                                            (IDR-Cash Out)&nbsp;<asp:Label ID="LblTotalAmountCashOut" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="formText">
                                                                        <td width="15%" bgcolor="#FFF7E6">
                                                                            <asp:Label ID="Label5" runat="server" Text="Report Format"></asp:Label></td>
                                                                        <td width="1%" bgcolor="#ffffff">
                                                                            :</td>
                                                                        <td width="80%" bgcolor="#ffffff">
                                                                            <asp:Label ID="LblCustomerType" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="formText">
                                                                        <td width="15%" bgcolor="#FFF7E6">
                                                                            <asp:Label ID="Label7" runat="server" Text="Report Type"></asp:Label></td>
                                                                        <td width="1%" bgcolor="#ffffff">
                                                                            :</td>
                                                                        <td width="80%" bgcolor="#ffffff">
                                                                            <asp:Label ID="LblReportType" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ajax:AjaxPanel>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;</td>
                                        </tr>
                                         <tr>
                                            <td>
                                                <table align="left" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                    bgcolor="#dddddd" border="2">
                                                    <tr>
                                                        <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                            style="height: 6px">
                                                            <table cellpadding="0" cellspacing="0" border="0">
                                                                <tr>
                                                                    <td class="formtext">
                                                                        <asp:Label ID="Label2" runat="server" Text="Upload Reference Number from PPATK"
                                                                            Font-Bold="True"></asp:Label>&nbsp;</td>
                                                                    <td>
                                                                        <a href="#" onclick="javascript:ShowHidePanel('ExcelCTR','ImgExcelCTR','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                            title="click to minimize or maximize">
                                                                            <img id="ImgExcelCTR" src="Images/search-bar-minimize.gif" border="0" height="12px"
                                                                                width="12px"></a></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr id="ExcelCTR">
                                                        <td valign="top" bgcolor="#ffffff">
                                                            <ajax:AjaxPanel ID="AjaxPanel2" runat="server" Width="100%">
                                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                    <tr class="formText">
                                                                        <td width="15%" bgcolor="#FFF7E6">
                                                                            <asp:Label ID="Label4" runat="server" Text="Upload File"></asp:Label></td>
                                                                        <td width="1%" bgcolor="#ffffff">
                                                                            :</td>
                                                                        <td width="80%" bgcolor="#ffffff">
                                                                             <asp:FileUpload ID="FileUploadFileReferenceNumber" runat="server" CssClass="textbox" Width="300px" BorderStyle="None" />
                                                                             <br />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="3" align="left">
                                                                            &nbsp;</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="3" align="left">
                                                                            <asp:ImageButton ID="ImgUploadBasedonRefNo" TabIndex="3" runat="server" ImageUrl="~/Images/button/upload.gif"
                                                                                CausesValidation="False" ajaxcall="none" 
                                                                                OnClientClick="aspnetForm.encoding = 'multipart/form-data';">
                                                                            </asp:ImageButton><asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
                                                                    </tr>
                                                                </table>
                                                            </ajax:AjaxPanel>
                                                        </td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table>
                                </ajax:AjaxPanel>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td background="Images/button-bground.gif" align="left" valign="middle">
                            <img src="Images/blank.gif" width="5" height="1" /></td>
                        <td background="Images/button-bground.gif" align="left" valign="middle">
                            <img src="images/arrow.gif" width="15" height="15" /></td>
                        <td background="Images/button-bground.gif" align="left" valign="middle">
                            &nbsp;<ajax:AjaxPanel ID="AjaxPanel22" runat="server"><asp:ImageButton ID="ImgBack"
                                runat="server" ImageUrl="~/Images/button/back.gif"></asp:ImageButton></ajax:AjaxPanel></td>
                        <td width="99%" background="Images/button-bground.gif">
                            <img src="Images/blank.gif" width="1" height="1" /></td>
                        <td>
                            s</td>
                    </tr>
                </table>
            </td>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>
