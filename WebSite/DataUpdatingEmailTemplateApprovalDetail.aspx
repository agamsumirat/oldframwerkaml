<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="DataUpdatingEmailTemplateApprovalDetail.aspx.vb" Inherits="DataUpdatingEmailTemplateApprovalDetail" title="Data Updating Email Template Approval Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
	<table cellSpacing="4" cellPadding="0" width="100%">
		<tr>
			<td vAlign="bottom" align="left"><IMG height="15" src="images/dot_title.gif" width="15"></td>
			<td class="maintitle" vAlign="bottom" width="99%"><asp:label id="LabelTitle" runat="server"></asp:label></td>
		</tr>
	</table>
    <TABLE id="TableDetail" borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="99%"
	    bgColor="#dddddd" border="2" runat="server">
	    <TR class="formText" id="UserAdd1">
		    <TD bgColor="#ffffff" height="24">&nbsp;</TD>
		    <TD bgColor="#ffffff" colspan="2">
                To </TD>
            <td bgcolor="#ffffff" colspan="1" style="width: 1%">
                :</td>
		    <TD bgColor="#ffffff"><asp:label id="LabelToAdd" runat="server"></asp:label></TD>
	    </TR>
        <tr class="formText" id="UserAdd2">
            <td bgcolor="#ffffff" height="24">
            </td>
            <td bgcolor="#ffffff" colspan="2">
                CC </td>
            <td bgcolor="#ffffff" colspan="1">
                :</td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelCCAdd" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText" id="UserAdd3">
            <td bgcolor="#ffffff" height="24">
            </td>
            <td bgcolor="#ffffff" colspan="2">
                BCC </td>
            <td bgcolor="#ffffff" colspan="1">
                :</td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelBCCAdd" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText" id="UserAdd4">
            <td bgcolor="#ffffff" height="24">
            </td>
            <td bgcolor="#ffffff" colspan="2">
                Subject </td>
            <td bgcolor="#ffffff" colspan="1">
                :</td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelSubjectAdd" runat="server"></asp:Label></td>
        </tr>
	    <tr class="formText" id="UserAdd5">
		    <td bgcolor="#ffffff" height="24">&nbsp;</td>
		    <td bgcolor="#ffffff" colspan="2">
                Body <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" colspan="1" valign="top">
                :</td>
		    <td bgcolor="#ffffff">
                <asp:TextBox ID="TextBodyAdd" runat="server" MaxLength="1000" ReadOnly="True"
                    Rows="5" TextMode="MultiLine" Width="312px"></asp:TextBox></td>
	    </tr>
        <tr class="formText" id="UserAdd6">
            <td bgcolor="#ffffff" height="24">
            </td>
            <td bgcolor="#ffffff" colspan="4">
                Include attachment data to be updated :
                <asp:Label ID="LabelIncludeAttachmentAdd" runat="server"></asp:Label></td>
        </tr>
	    <tr class="formText" id="UserEdi1">
		    <td  colspan="5" bgcolor="#ffffcc">&nbsp;Old Values</td>
		    <td colspan="5" bgcolor="#ffffcc">&nbsp;New Values</td>
	    </tr>
	    <tr class="formText" id="UserEdi2">
		    <td bgcolor="#ffffff" style="width: 1%;">&nbsp;</td>
		    <td bgcolor="#ffffff" colspan="2" style="width: 5%">
                To </td>
            <td bgcolor="#ffffff" colspan="1" style="width: 1%">
                :</td>
		    <td bgcolor="#ffffff" style="width: 25%"><asp:label id="LabelOldTo" runat="server"></asp:label></td>
            <td bgcolor="#ffffff" style="width: 2%" width="5">
            </td>
		    <td bgcolor="#ffffff" colspan="2" style="width: 5%">
                To </td>
            <td bgcolor="#ffffff" colspan="1" style="width: 1%">
                :</td>
		    <td width="40%" bgcolor="#ffffff"><asp:label id="LabelNewTo" runat="server"></asp:label></td>
	    </tr>
	    <tr class="formText" id="UserEdi3">
		    <td bgcolor="#ffffff" >&nbsp;</td>
		    <td bgcolor="#ffffff" colspan="2">
                CC </td>
            <td bgcolor="#ffffff" colspan="1">
                :</td>
		    <td bgcolor="#ffffff"><asp:label id="LabelOldCC" runat="server"></asp:label></td>
            <td bgcolor="#ffffff">
            </td>
		    <td bgcolor="#ffffff" colspan="2">
                CC </td>
            <td bgcolor="#ffffff" colspan="1">
                :</td>
		    <td bgcolor="#ffffff"><asp:label id="LabelNewCC" runat="server"></asp:label></td>
	    </tr>
        <tr class="formText" id="UserEdi4">
            <td bgcolor="#ffffff">
            </td>
            <td bgcolor="#ffffff" colspan="2">
                BCC </td>
            <td bgcolor="#ffffff" colspan="1">
                :</td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelOldBCC" runat="server"></asp:Label></td>
            <td bgcolor="#ffffff">
            </td>
            <td bgcolor="#ffffff" colspan="2">
                BCC </td>
            <td bgcolor="#ffffff" colspan="1">
                :</td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelNewBCC" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText" id="UserEdi5">
            <td bgcolor="#ffffff">
            </td>
            <td bgcolor="#ffffff" colspan="2">
                Subject </td>
            <td bgcolor="#ffffff" colspan="1">
                :</td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelOldSubject" runat="server"></asp:Label></td>
            <td bgcolor="#ffffff">
            </td>
            <td bgcolor="#ffffff" colspan="2">
                Subject </td>
            <td bgcolor="#ffffff" colspan="1">
                :</td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelNewSubject" runat="server"></asp:Label></td>
        </tr>
	    <tr class="formText" id="UserEdi6">
		    <td bgcolor="#ffffff">&nbsp;</td>
		    <td bgcolor="#ffffff" colspan="2">
                Body 
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" colspan="1" valign="top">
                :</td>
		    <td bgcolor="#ffffff">
                <asp:TextBox ID="TextOldBody" runat="server"
                    MaxLength="1000" ReadOnly="True" Rows="5" TextMode="MultiLine" Width="312px"></asp:TextBox></td>
            <td bgcolor="#ffffff">
            </td>
		    <td bgcolor="#ffffff" colspan="2">
                Body <br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" colspan="1" valign="top">
                :</td>
		    <td bgcolor="#ffffff">
                <asp:TextBox ID="TextNewBody" runat="server" MaxLength="1000"
                    ReadOnly="True" Rows="5" TextMode="MultiLine" Width="312px"></asp:TextBox></td>
	    </tr>
        <tr class="formText" id="UserEdi7">
            <td bgcolor="#ffffff">
            </td>
            <td bgcolor="#ffffff" colspan="4">
                Include attachment data to be updated :
                <asp:Label ID="LabelOldIncludeAttachment" runat="server"></asp:Label></td>
            <td bgcolor="#ffffff">
            </td>
            <td bgcolor="#ffffff" colspan="4">
                Include attachment data to be updated :
                <asp:Label ID="LabelNewIncludeAttachment" runat="server"></asp:Label></td>
        </tr>
	    <TR class="formText" id="Button11" bgColor="#dddddd" height="30">
		    <TD style="height: 36px"><IMG height="15" src="images/arrow.gif" width="15"></TD>
		    <TD colSpan="9">
			    <TABLE cellSpacing="0" cellPadding="3" border="0">
				    <TR>
					    <TD><asp:imagebutton id="ImageAccept" runat="server" CausesValidation="False" SkinID="AcceptButton" ></asp:imagebutton></TD>
					    <TD><asp:imagebutton id="ImageReject" runat="server" CausesValidation="False" SkinID="RejectButton"></asp:imagebutton></TD>
					    <td><asp:imagebutton id="ImageBack" runat="server" CausesValidation="False" SkinID="BackButton"></asp:imagebutton></td>
                        <td>
                <asp:CustomValidator ID="cvalPageErr" runat="server" Display="Dynamic"></asp:CustomValidator></td>
				    </TR>
			    </TABLE>
                </TD>
	    </TR>
    </TABLE>
</asp:Content>