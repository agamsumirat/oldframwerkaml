Option Strict On
Option Explicit On
Imports amlbll
Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports SahassaNettier.Services
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Imports System.IO
Imports System.Collections.Generic

Partial Class QuestionaireSTRActivation
    Inherits Parent

#Region "Sesion"
    Public ReadOnly Property PKMsQuestionaireSTRID() As Integer
        Get
            Dim strTemp As String = Request.Params("PKMsQuestionaireSTRID")
            Dim intResult As Integer
            If Integer.TryParse(strTemp, intResult) Then
                Return intResult
            Else
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = "Parameter PKMsQuestionaireSTRID is invalid."

            End If
        End Get
    End Property
    Public ReadOnly Property ObjMsQuestionaireSTR() As MsQuestionaireSTR
        Get
            If Session("QuestionaireSTRActivation.ObjMsQuestionaireSTR") Is Nothing Then

                Session("QuestionaireSTRActivation.ObjMsQuestionaireSTR") = DataRepository.MsQuestionaireSTRProvider.GetByPK_MsQuestionaireSTR_ID(PKMsQuestionaireSTRID)

            End If
            Return CType(Session("QuestionaireSTRActivation.ObjMsQuestionaireSTR"), MsQuestionaireSTR)
        End Get
    End Property
    Public ReadOnly Property ObjMsQuestionaireSTRMultipleChoice() As TList(Of MsQuestionaireSTRMultipleChoice)
        Get
            If Session("QuestionaireSTRActivation.ObjMsQuestionaireSTRMultipleChoice") Is Nothing Then

                Session("QuestionaireSTRActivation.ObjMsQuestionaireSTRMultipleChoice") = DataRepository.MsQuestionaireSTRMultipleChoiceProvider.GetPaged("FK_MsQuestionaireSTR_ID = '" & PKMsQuestionaireSTRID & "'", "", 0, Integer.MaxValue, 0)

            End If
            Return CType(Session("QuestionaireSTRActivation.ObjMsQuestionaireSTRMultipleChoice"), TList(Of MsQuestionaireSTRMultipleChoice))
        End Get
    End Property
    Private Property SetnGetCurrentPage() As Integer
        Get
            Return CInt(IIf(Session("SetnGetCurrentPage") Is Nothing, "", Session("SetnGetCurrentPage")))
        End Get
        Set(ByVal value As Integer)
            Session("SetnGetCurrentPage") = value
        End Set
    End Property

#End Region

#Region "ClearSession"
    Sub ClearSession()
        Session("QuestionaireSTRActivation.ObjMsQuestionaireSTR") = Nothing
        Session("QuestionaireSTRActivation.ObjMsQuestionaireSTRMultipleChoice") = Nothing
        Session("SetnGetCurrentPage") = Nothing
    End Sub
#End Region

#Region "load mode"
    Private Sub loaddata()

        If Not ObjMsQuestionaireSTR Is Nothing Then
            If ObjMsQuestionaireSTR.FK_QuestionType_ID = 1 Then

                cde.Visible = False

                LblDescriptionAlertSTR.Text = ObjMsQuestionaireSTR.DescriptionAlertSTR
                Using ObjQuestionType As QuestionType = DataRepository.QuestionTypeProvider.GetByPK_QuestionType_ID(ObjMsQuestionaireSTR.FK_QuestionType_ID)
                    LblQuestiontype.Text = ObjQuestionType.QuestionType
                End Using
                LblQuestionNo.Text = CStr(ObjMsQuestionaireSTR.QuestionNo)
                TxtQuestion.Text = ObjMsQuestionaireSTR.Question
                LblActivationValue.Text = CStr(ObjMsQuestionaireSTR.Activation)
                If LblActivationValue.Text = "True" Then
                    LblActivationValue.Text = "Active"
                    lblNewActivationValue.Text = " (Not Active)"
                Else
                    LblActivationValue.Text = "Not Active"
                    lblNewActivationValue.Text = " (Active)"
                End If
                LblCreatedBy.Text = ObjMsQuestionaireSTR.CreatedBy & " / " & CStr(ObjMsQuestionaireSTR.CreatedDate)
                LblUpdatedBy.Text = ObjMsQuestionaireSTR.LastUpdateBy & " / " & CStr(ObjMsQuestionaireSTR.LastUpdateDate)
                If ObjMsQuestionaireSTR.ApprovedBy = "" Then
                    LblApprovedBy.Text = ""
                Else
                    LblApprovedBy.Text = ObjMsQuestionaireSTR.ApprovedBy & " / " & CStr(ObjMsQuestionaireSTR.ApprovedDate)
                End If

            ElseIf ObjMsQuestionaireSTR.FK_QuestionType_ID = 2 Then

                cde.Visible = True

                LblDescriptionAlertSTR.Text = ObjMsQuestionaireSTR.DescriptionAlertSTR
                Using ObjQuestionType As QuestionType = DataRepository.QuestionTypeProvider.GetByPK_QuestionType_ID(ObjMsQuestionaireSTR.FK_QuestionType_ID)
                    LblQuestiontype.Text = ObjQuestionType.QuestionType
                End Using
                LblQuestionNo.Text = CStr(ObjMsQuestionaireSTR.QuestionNo)
                TxtQuestion.Text = ObjMsQuestionaireSTR.Question
                gridView.DataSource = ObjMsQuestionaireSTRMultipleChoice
                gridView.DataBind()
                LblActivationValue.Text = CStr(ObjMsQuestionaireSTR.Activation)
                If LblActivationValue.Text = "True" Then
                    LblActivationValue.Text = "Active"
                    lblNewActivationValue.Text = " (Not Active)"
                Else
                    LblActivationValue.Text = "Not Active"
                    lblNewActivationValue.Text = " (Active)"
                End If
                LblCreatedBy.Text = ObjMsQuestionaireSTR.CreatedBy & " / " & CStr(ObjMsQuestionaireSTR.CreatedDate)
                LblUpdatedBy.Text = ObjMsQuestionaireSTR.LastUpdateBy & " / " & CStr(ObjMsQuestionaireSTR.LastUpdateDate)
                If ObjMsQuestionaireSTR.ApprovedBy = "" Then
                    LblApprovedBy.Text = ""
                Else
                    LblApprovedBy.Text = ObjMsQuestionaireSTR.ApprovedBy & " / " & CStr(ObjMsQuestionaireSTR.ApprovedDate)
                End If
            End If

        Else
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = QuestionaireSTRBLL.DATA_NOTVALID
        End If
    End Sub
#End Region

#Region "IMGSAVE"
    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim oTrans As SqlTransaction = Nothing
        Try

            LblSuccess.Visible = False
            Using ObjList As MsQuestionaireSTR = DataRepository.MsQuestionaireSTRProvider.GetByPK_MsQuestionaireSTR_ID(ObjMsQuestionaireSTR.PK_MsQuestionaireSTR_ID)
                If ObjList Is Nothing Then
                    Throw New AMLBLL.SahassaException(MsQuestionaireSTREnum.DATA_NOTVALID)
                End If
            End Using

            Dim ObjCboAlertSTRList As String = LblDescriptionAlertSTR.Text
            Dim ObjCboQuestionTypeList As String = LblQuestiontype.Text
            Dim ObjQuestionNumber As String = LblQuestionNo.Text
            Dim ObjQuestion As String = TxtQuestion.Text

            Using ObjQuestionaireSTRBLL As New QuestionaireSTRBLL
                If ObjQuestionaireSTRBLL.IsDataValidDeleteAndActivationApproval(ObjMsQuestionaireSTR, ObjCboAlertSTRList, ObjQuestionNumber) Then
                End If
            End Using

            Using ObjMsQuestionaireSTRActivation As MsQuestionaireSTR = DataRepository.MsQuestionaireSTRProvider.GetByPK_MsQuestionaireSTR_ID(PKMsQuestionaireSTRID)
                If ObjMsQuestionaireSTRActivation Is Nothing Then
                    Me.LblSuccess.Text = "DescriptionAlertSTR " & ObjCboAlertSTRList & " AND Question number " & ObjQuestionNumber & " has been Deleted"
                    Me.LblSuccess.Visible = True
                Else
                    'SuperUser
                    If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                        If lblNewActivationValue.Text = " (Active)" Then
                            ObjMsQuestionaireSTR.Activation = True
                        Else
                            ObjMsQuestionaireSTR.Activation = False
                        End If
                        DataRepository.MsQuestionaireSTRProvider.Save(ObjMsQuestionaireSTR)
                        AMLBLL.QuestionaireSTRBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "QuestionaireSTR", "DescriptionAlertSTR", lblNewActivationValue.Text, "", ObjCboAlertSTRList, "Acc")
                        AMLBLL.QuestionaireSTRBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "QuestionaireSTR", "QuestionType", lblNewActivationValue.Text, "", ObjCboQuestionTypeList, "Acc")
                        AMLBLL.QuestionaireSTRBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "QuestionaireSTR", "QuestionNo", lblNewActivationValue.Text, "", ObjQuestionNumber, "Acc")
                        AMLBLL.QuestionaireSTRBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "QuestionaireSTR", "Question", lblNewActivationValue.Text, "", ObjQuestion, "Acc")
                        For Each ObjSaveMsQuestionaireSTRMultipleChoice As MsQuestionaireSTRMultipleChoice In ObjMsQuestionaireSTRMultipleChoice
                            Using ObjNewMsQuestionaireSTRMultipleChoice As New MsQuestionaireSTRMultipleChoice
                                AMLBLL.QuestionaireSTRBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "QuestionaireSTR", "MultipleChoice", lblNewActivationValue.Text, "", ObjSaveMsQuestionaireSTRMultipleChoice.MultipleChoice, "Acc")
                            End Using
                        Next

                        Sahassa.AML.Commonly.SessionIntendedPage = "QuestionaireSTRView.aspx"
                        Me.Response.Redirect("QuestionaireSTRView.aspx", False)
                    Else 'bukan SuperUser
                        Using ObjMsQuestionaireSTR_approval As New MsQuestionaireSTR_Approval
                            With ObjMsQuestionaireSTR_approval
                                .FK_MsUserID = Sahassa.AML.Commonly.SessionPkUserId
                                .DescriptionAlertSTR = ObjCboAlertSTRList
                                .QuestionNo = CInt(ObjQuestionNumber)
                                .FK_ModeID = CInt(Sahassa.AML.Commonly.TypeMode.Activation)
                                .CreatedDate = Date.Now
                            End With
                            DataRepository.MsQuestionaireSTR_ApprovalProvider.Save(ObjMsQuestionaireSTR_approval)
                            Using ObjMsQuestionaireSTR_approvalDetail As New MsQuestionaireSTR_ApprovalDetail
                                With ObjMsQuestionaireSTR_approvalDetail
                                    .FK_MsQuestionaireSTR_ApprovalID = ObjMsQuestionaireSTR_approval.PK_MsQuestionaireSTR_ApprovalID

                                    If lblNewActivationValue.Text = " (Active)" Then
                                        ObjMsQuestionaireSTR.Activation = True
                                    Else
                                        ObjMsQuestionaireSTR.Activation = False
                                    End If

                                    .PK_MsQuestionaireSTR_ID = ObjMsQuestionaireSTR.PK_MsQuestionaireSTR_ID
                                    .DescriptionAlertSTR = ObjCboAlertSTRList
                                    .FK_QuestionType_ID = ObjMsQuestionaireSTR.FK_QuestionType_ID
                                    .QuestionNo = CInt(ObjQuestionNumber)
                                    .Question = ObjQuestion
                                    .Activation = ObjMsQuestionaireSTR.Activation
                                    .CreatedBy = ObjMsQuestionaireSTR.CreatedBy
                                    .CreatedDate = ObjMsQuestionaireSTR.CreatedDate
                                    .LastUpdateBy = ObjMsQuestionaireSTR.LastUpdateBy
                                    .LastUpdateDate = ObjMsQuestionaireSTR.LastUpdateDate
                                    .LastUpdateBy_Old = Sahassa.AML.Commonly.SessionUserId
                                    .LastUpdateDate_Old = Date.Now

                                End With
                                DataRepository.MsQuestionaireSTR_ApprovalDetailProvider.Save(ObjMsQuestionaireSTR_approvalDetail)
                                AMLBLL.QuestionaireSTRBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "QuestionaireSTR", "DescriptionAlertSTR", lblNewActivationValue.Text, "", ObjCboAlertSTRList, "PendingApproval")
                                AMLBLL.QuestionaireSTRBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "QuestionaireSTR", "QuestionType", lblNewActivationValue.Text, "", ObjCboQuestionTypeList, "PendingApproval")
                                AMLBLL.QuestionaireSTRBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "QuestionaireSTR", "QuestionNo", lblNewActivationValue.Text, "", ObjQuestionNumber, "PendingApproval")
                                AMLBLL.QuestionaireSTRBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "QuestionaireSTR", "Question", lblNewActivationValue.Text, "", ObjQuestion, "PendingApproval")
                                For Each ObjSaveMsQuestionaireSTRMultipleChoice As MsQuestionaireSTRMultipleChoice In ObjMsQuestionaireSTRMultipleChoice
                                    Using ObjMsQuestionaireSTRMultipleChoice_approvaldetail As New MsQuestionaireSTRMultipleChoice_ApprovalDetail
                                        With ObjMsQuestionaireSTRMultipleChoice_approvaldetail
                                            .FK_MsQuestionaireSTR_ID = ObjMsQuestionaireSTR.PK_MsQuestionaireSTR_ID
                                            .FK_MsQuestionaireSTRMultipleChoice_ID = ObjSaveMsQuestionaireSTRMultipleChoice.PK_MsQuestionaireSTRMultipleChoice_ID
                                            .FK_MsQuestionaireSTR_ApprovalID = CInt(ObjMsQuestionaireSTR_approval.PK_MsQuestionaireSTR_ApprovalID)
                                            .MultipleChoice = ObjSaveMsQuestionaireSTRMultipleChoice.MultipleChoice
                                        End With
                                        DataRepository.MsQuestionaireSTRMultipleChoice_ApprovalDetailProvider.Save(ObjMsQuestionaireSTRMultipleChoice_approvaldetail)
                                        AMLBLL.QuestionaireSTRBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "QuestionaireSTR", "MultipleChoice", lblNewActivationValue.Text, "", ObjSaveMsQuestionaireSTRMultipleChoice.MultipleChoice, "PendingApproval")
                                    End Using
                                Next
                                Me.LblSuccess.Text = "DescriptionAlertSTR " & ObjCboAlertSTRList & " AND Question number " & ObjQuestionNumber & " has add to Pending Approval"
                                Me.LblSuccess.Visible = True
                            End Using
                        End Using
                    End If
                End If
            End Using

        Catch ex As Exception
            If Not oTrans Is Nothing Then oTrans.Rollback()
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not oTrans Is Nothing Then
                oTrans.Dispose()
                oTrans = Nothing
            End If
        End Try
    End Sub
#End Region

#Region "IMGCANCEL"
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "QuestionaireSTRView.aspx"
            Me.Response.Redirect("QuestionaireSTRView.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'Using Transcope As New Transactions.TransactionScope
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                '    Transcope.Complete()
            End Using

            If Not Page.IsPostBack Then
                ClearSession()
                loaddata()
                'abc.Visible = False
                'cde.Visible = False
            End If
            'End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

#Region "Grid"
    Protected Sub gridview_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles gridView.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then

                Dim attachment As String = e.Item.Cells(1).Text

                e.Item.Cells(0).Text = CStr((e.Item.ItemIndex + 1))
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub gridView_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles gridView.PageIndexChanged
        gridView.DataSource = ObjMsQuestionaireSTRMultipleChoice
        SetnGetCurrentPage = e.NewPageIndex
        gridView.CurrentPageIndex = SetnGetCurrentPage
        gridView.DataBind()
    End Sub
#End Region

    
End Class

