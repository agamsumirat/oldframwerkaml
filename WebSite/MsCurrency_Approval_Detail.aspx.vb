#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports Sahassa.AML
Imports System.Collections.Generic
#End Region

Partial Class MsCurrency_APPROVAL_Detail
    Inherits Parent

#Region "Function"

    Sub HideControl()
        imgReject.Visible = False
        imgApprove.Visible = False
        ImageCancel.Visible = False
    End Sub

    Sub ChangeMultiView(ByVal index As Integer)
        MtvMsUser.ActiveViewIndex = index
    End Sub

#End Region

#Region "events..."

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Response.Redirect("MsCurrency_Approval_View.aspx")
    End Sub

    Protected Sub imgReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgReject.Click
        Try
            Rejected()
            ChangeMultiView(1)
            LblConfirmation.Text = "Data has been rejected"
            HideControl()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgApprove_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgApprove.Click
        Try
            Using ObjMsCurrency_Approval As MsCurrency_Approval = DataRepository.MsCurrency_ApprovalProvider.GetByPK_MsCurrencyPPATK_Approval_Id(parID)
                If ObjMsCurrency_Approval.FK_MsMode_Id.GetValueOrDefault = 1 Then
                    Inserdata(ObjMsCurrency_Approval)
                ElseIf ObjMsCurrency_Approval.FK_MsMode_Id.GetValueOrDefault = 2 Then
                    UpdateData(ObjMsCurrency_Approval)
                ElseIf ObjMsCurrency_Approval.FK_MsMode_Id.GetValueOrDefault = 3 Then
                    DeleteData(ObjMsCurrency_Approval)
                End If
            End Using
            ChangeMultiView(1)
            LblConfirmation.Text = "Data approved successful"
            HideControl()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
                ListmapingNew = New TList(Of MappingMsCurrencyNCBSPPATK_Approval_Detail)
                ListmapingOld = New TList(Of MappingMsCurrencyNCBSPPATK)
                LoadData()
            Catch ex As Exception
                LogError(ex)
                CvalPageErr.IsValid = False
                CvalPageErr.ErrorMessage = ex.Message
            End Try
        End If

    End Sub

    Private Sub LoadData()
        Dim PkObject As String
        'Load new data
        Using ObjMsCurrency_ApprovalDetail As MsCurrency_ApprovalDetail = DataRepository.MsCurrency_ApprovalDetailProvider.GetPaged(MsCurrency_ApprovalDetailColumn.PK_MsCurrency_Approval_id.ToString & _
   "=" & _
   parID, "", 0, 1, Nothing)(0)
            Dim CekMode As MsCurrency_Approval = DataRepository.MsCurrency_ApprovalProvider.GetByPK_MsCurrencyPPATK_Approval_Id(ObjMsCurrency_ApprovalDetail.PK_MsCurrency_Approval_id)
            Dim nama As String = CekMode.FK_MsMode_Id
            If nama = 1 Then
                baru.Visible = True
                lama.Visible = False
                baruNew.Visible = True
                lamaOld.Visible = False
            ElseIf nama = 2 Then
                baru.Visible = True
                lama.Visible = True
                baruNew.Visible = True
                lamaOld.Visible = True
            ElseIf nama = 3 Then
                baru.Visible = True
                lama.Visible = False
                baruNew.Visible = True
                lamaOld.Visible = False
            ElseIf nama = 4 Then
                baru.Visible = True
                lama.Visible = False
                baruNew.Visible = True
                lamaOld.Visible = False
            End If
            With ObjMsCurrency_ApprovalDetail
                SafeDefaultValue = "-"
                txtIdCurrencynew.Text = Safe(.IdCurrency)
                txtCodenew.Text = Safe(.Code)
                txtNamenew.Text = Safe(.Name)


                'other info
                Dim Omsuser As TList(Of User)
                Omsuser = DataRepository.UserProvider.GetByUserID(.CreatedBy)
                If Omsuser.Count > 0 Then
                    lblCreatedByNew.Text = Omsuser(0).UserName
                Else
                    lblCreatedByNew.Text = SafeDefaultValue
                End If
                lblCreatedDateNew.Text = FormatDate(.CreatedDate)
                Omsuser = DataRepository.UserProvider.GetByUserID(.LastUpdatedBy)
                If Omsuser.Count > 0 Then
                    lblUpdatedbyNew.Text = Omsuser(0).UserName
                Else
                    lblUpdatedbyNew.Text = SafeDefaultValue
                End If
                lblUpdatedDateNew.Text = FormatDate(.LastUpdatedDate)
                txtActivationNew.Text = SafeActiveInactive(.Activation)
                Dim L_objMappingMsCurrencyNCBSPPATK_Approval_Detail As TList(Of MappingMsCurrencyNCBSPPATK_Approval_Detail)
                L_objMappingMsCurrencyNCBSPPATK_Approval_Detail = DataRepository.MappingMsCurrencyNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsCurrencyNCBSPPATK_Approval_DetailColumn.PK_MappingMsCurrencyNCBSPPATK_Approval_Id.ToString & _
                 "=" & _
                 parID, "", 0, Integer.MaxValue, Nothing)
                'Add mapping
                ListmapingNew.AddRange(L_objMappingMsCurrencyNCBSPPATK_Approval_Detail)

            End With
            PkObject = ObjMsCurrency_ApprovalDetail.IdCurrency
        End Using

        'Load Old Data
        Using ObjMsCurrency As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(PkObject)
            If ObjMsCurrency Is Nothing Then Return
            With ObjMsCurrency
                SafeDefaultValue = "-"
                txtIdCurrencyOld.Text = Safe(.IdCurrency)
                txtCodeOld.Text = Safe(.Code)
                txtNameOld.Text = Safe(.Name)


                'other info
                Dim Omsuser As TList(Of User)
                Omsuser = DataRepository.UserProvider.GetByUserID(.CreatedBy)
                If Omsuser.Count > 0 Then
                    lblCreatedByOld.Text = Omsuser(0).UserName
                Else
                    lblCreatedByOld.Text = SafeDefaultValue
                End If
                lblCreatedDateNew.Text = FormatDate(.CreatedDate)
                Omsuser = DataRepository.UserProvider.GetByUserID(.LastUpdatedBy)
                If Omsuser.Count > 0 Then
                    lblUpdatedbyOld.Text = Omsuser(0).UserName
                Else
                    lblUpdatedbyOld.Text = SafeDefaultValue
                End If
                lblUpdatedDateOld.Text = FormatDate(.LastUpdatedDate)
                Dim L_objMappingMsCurrencyNCBSPPATK As TList(Of MappingMsCurrencyNCBSPPATK)
                txtActivationOld.Text = SafeActiveInactive(.Activation)
                L_objMappingMsCurrencyNCBSPPATK = DataRepository.MappingMsCurrencyNCBSPPATKProvider.GetPaged(MappingMsCurrencyNCBSPPATKColumn.IdCurrency.ToString & _
                   "=" & _
                   PkObject, "", 0, Integer.MaxValue, Nothing)
                ListmapingOld.AddRange(L_objMappingMsCurrencyNCBSPPATK)
            End With
        End Using
    End Sub



    Protected Sub imgBtnConfirmation_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnConfirmation.Click
        Try
            Response.Redirect("MsCurrency_approval_view.aspx")
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub


    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            BindListMapping()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub BindListMapping()
        'bind new value
        LBMappingNew.DataSource = ListMappingDisplayNew
        LBMappingNew.DataBind()
        'Bind OldValue
        LBmappingOld.DataSource = ListMappingDisplayOld
        LBmappingOld.DataBind()
    End Sub

#End Region

#Region "Property..."

    Public ReadOnly Property parID() As String
        Get
            Return Request.Item("ID")
        End Get
    End Property
    ''' <summary>
    ''' Menyimpan Item untuk mapping New sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ListmapingNew() As TList(Of MappingMsCurrencyNCBSPPATK_Approval_Detail)
        Get
            Return Session("ListmapingNew.data")
        End Get
        Set(ByVal value As TList(Of MappingMsCurrencyNCBSPPATK_Approval_Detail))
            Session("ListmapingNew.data") = value
        End Set
    End Property

    ''' <summary>
    ''' Menyimpan Item untuk mapping Old sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ListmapingOld() As TList(Of MappingMsCurrencyNCBSPPATK)
        Get
            Return Session("ListmapingOld.data")
        End Get
        Set(ByVal value As TList(Of MappingMsCurrencyNCBSPPATK))
            Session("ListmapingOld.data") = value
        End Set
    End Property

    ''' <summary>
    ''' List yang tampil di ListBox
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplayNew() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingMsCurrencyNCBSPPATK_Approval_Detail In ListmapingNew.FindAllDistinct("Pk_MsCurrency_Id")
                Temp.Add(i.Pk_MsCurrencyPPATK_Id.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property

    ''' <summary>
    ''' List yang tampil di ListBox
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplayOld() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingMsCurrencyNCBSPPATK In ListmapingOld.FindAllDistinct("IdCurrencyNCBS")
                Temp.Add(i.IdCurrencyNCBS.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property

#End Region

#Region "Action Approval..."

    Sub Rejected()
        Dim ObjMsCurrency_Approval As MsCurrency_Approval = DataRepository.MsCurrency_ApprovalProvider.GetByPK_MsCurrencyPPATK_Approval_Id(parID)
        Dim ObjMsCurrency_ApprovalDetail As MsCurrency_ApprovalDetail = DataRepository.MsCurrency_ApprovalDetailProvider.GetPaged(MsCurrency_ApprovalDetailColumn.PK_MsCurrency_Approval_id.ToString & "=" & ObjMsCurrency_Approval.PK_MsCurrencyPPATK_Approval_Id, "", 0, 1, Nothing)(0)
        Dim L_ObjMappingMsCurrencyNCBSPPATK_Approval_Detail As TList(Of MappingMsCurrencyNCBSPPATK_Approval_Detail) = DataRepository.MappingMsCurrencyNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsCurrencyNCBSPPATK_Approval_DetailColumn.PK_MappingMsCurrencyNCBSPPATK_Approval_Id.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)
        DeleteApproval(ObjMsCurrency_Approval, ObjMsCurrency_ApprovalDetail, L_ObjMappingMsCurrencyNCBSPPATK_Approval_Detail)
    End Sub

    Sub DeleteApproval(ByRef objMsCurrency_Approval As MsCurrency_Approval, ByRef ObjMsCurrency_ApprovalDetail As MsCurrency_ApprovalDetail, ByRef L_ObjMappingMsCurrencyNCBSPPATK_Approval_Detail As TList(Of MappingMsCurrencyNCBSPPATK_Approval_Detail))
        DataRepository.MsCurrency_ApprovalProvider.Delete(objMsCurrency_Approval)
        DataRepository.MsCurrency_ApprovalDetailProvider.Delete(ObjMsCurrency_ApprovalDetail)
        DataRepository.MappingMsCurrencyNCBSPPATK_Approval_DetailProvider.Delete(L_ObjMappingMsCurrencyNCBSPPATK_Approval_Detail)
    End Sub

    ''' <summary>
    ''' Delete Data dari  asli dari approval
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub DeleteData(ByVal objMsCurrency_Approval As MsCurrency_Approval)
        '   '================= Delete Header ===========================================================
        Dim LObjMsCurrency_ApprovalDetail As TList(Of MsCurrency_ApprovalDetail) = DataRepository.MsCurrency_ApprovalDetailProvider.GetPaged(MsCurrency_ApprovalDetailColumn.PK_MsCurrency_Approval_id.ToString & "=" & objMsCurrency_Approval.PK_MsCurrencyPPATK_Approval_Id, "", 0, 1, Nothing)
        If LObjMsCurrency_ApprovalDetail.Count = 0 Then Throw New Exception("Data Not Found")
        Dim ObjMsCurrency_ApprovalDetail As MsCurrency_ApprovalDetail = LObjMsCurrency_ApprovalDetail(0)
        Using ObjNewMsCurrency As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(ObjMsCurrency_ApprovalDetail.IdCurrency)
            DataRepository.MsCurrencyProvider.Delete(ObjNewMsCurrency)
        End Using

        '======== Delete MAPPING =========================================
        Dim L_ObjMappingMsCurrencyNCBSPPATK_Approval_Detail As TList(Of MappingMsCurrencyNCBSPPATK_Approval_Detail) = DataRepository.MappingMsCurrencyNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsCurrencyNCBSPPATK_Approval_DetailColumn.PK_MappingMsCurrencyNCBSPPATK_Approval_Id.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)


        For Each c As MappingMsCurrencyNCBSPPATK_Approval_Detail In L_ObjMappingMsCurrencyNCBSPPATK_Approval_Detail
            Using ObjNewMappingMsCurrencyNCBSPPATK As MappingMsCurrencyNCBSPPATK = DataRepository.MappingMsCurrencyNCBSPPATKProvider.GetByPK_MappingMsCurrencyNCBSPPATK_Id(c.PK_MappingMsCurrencyNCBSPPATK_Id)
                DataRepository.MappingMsCurrencyNCBSPPATKProvider.Delete(ObjNewMappingMsCurrencyNCBSPPATK)
            End Using
        Next

        '======== Delete Approval data ======================================
        DeleteApproval(objMsCurrency_Approval, ObjMsCurrency_ApprovalDetail, L_ObjMappingMsCurrencyNCBSPPATK_Approval_Detail)
    End Sub

    ''' <summary>
    ''' Update Data dari approval ke table asli 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub UpdateData(ByVal objMsCurrency_Approval As MsCurrency_Approval)
        'Dim objAuditTrailType As MsAuditTrailEntityType = DataRepository.MsAuditTrailEntityTypeProvider.GetByPk_MsAuditTrailEntityTypeId(18)
        '   '================= Update Header ===========================================================
        Dim LObjMsCurrency_ApprovalDetail As TList(Of MsCurrency_ApprovalDetail) = DataRepository.MsCurrency_ApprovalDetailProvider.GetPaged(MsCurrency_ApprovalDetailColumn.PK_MsCurrency_Approval_id.ToString & "=" & objMsCurrency_Approval.PK_MsCurrencyPPATK_Approval_Id, "", 0, 1, Nothing)
        If LObjMsCurrency_ApprovalDetail.Count = 0 Then Throw New Exception("Data Not Found")
        Dim ObjMsCurrency_ApprovalDetail As MsCurrency_ApprovalDetail = LObjMsCurrency_ApprovalDetail(0)
        Dim ObjMsCurrencyNew As MsCurrency
        Using ObjMsCurrencyOld As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(ObjMsCurrency_ApprovalDetail.IdCurrency)
            If ObjMsCurrencyOld Is Nothing Then Throw New Exception("Data Not Found")
            ObjMsCurrencyNew = ObjMsCurrencyOld.Clone
            With ObjMsCurrencyNew
                FillOrNothing(.Name, ObjMsCurrency_ApprovalDetail.Name)
                FillOrNothing(.CreatedBy, ObjMsCurrency_ApprovalDetail.CreatedBy)
                FillOrNothing(.CreatedDate, ObjMsCurrency_ApprovalDetail.CreatedDate)
                FillOrNothing(.ApprovedBy, SessionUserId, True, oString)
                FillOrNothing(.ApprovedDate, Date.Now)
            End With
            DataRepository.MsCurrencyProvider.Save(ObjMsCurrencyNew)
            'Insert auditrail
            'AuditTrailBLL.InsertAuditTrail(AuditTrailOperation.Add, objAuditTrailType, Now(), Sahassa.AML.Commonly.SessionNIK, Sahassa.AML.Commonly.SessionStaffName, "MsBentukBidangUsaha has Approved to Update data", ObjMsBentukBidangUsahaNew, ObjMsBentukBidangUsahaOld)
            '======== Update MAPPING =========================================
            'delete mapping item
            Using L_ObjMappingMsCurrencyNCBSPPATK As TList(Of MappingMsCurrencyNCBSPPATK) = DataRepository.MappingMsCurrencyNCBSPPATKProvider.GetPaged(MappingMsCurrencyNCBSPPATKColumn.IdCurrency.ToString & _
             "=" & _
             ObjMsCurrencyNew.IdCurrency, "", 0, Integer.MaxValue, Nothing)
                DataRepository.MappingMsCurrencyNCBSPPATKProvider.Delete(L_ObjMappingMsCurrencyNCBSPPATK)
            End Using
            'Insert mapping item
            Dim L_ObjMappingMsCurrencyNCBSPPATK_Approval_Detail As TList(Of MappingMsCurrencyNCBSPPATK_Approval_Detail) = DataRepository.MappingMsCurrencyNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsCurrencyNCBSPPATK_Approval_DetailColumn.PK_MappingMsCurrencyNCBSPPATK_Approval_Id.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)

            Dim L_ObjNewMappingMsCurrencyNCBSPPATK As New TList(Of MappingMsCurrencyNCBSPPATK)
            For Each c As MappingMsCurrencyNCBSPPATK_Approval_Detail In L_ObjMappingMsCurrencyNCBSPPATK_Approval_Detail
                Dim ObjNewMappingMsCurrencyNCBSPPATK As New MappingMsCurrencyNCBSPPATK '= DataRepository.MappingMsCurrencyNCBSPPATKProvider.GetByPK_MappingMsCurrencyNCBSPPATK_Id(c.PK_MappingMsCurrencyNCBSPPATK_Id.GetValueOrDefault)
                With ObjNewMappingMsCurrencyNCBSPPATK
                    FillOrNothing(.IdCurrency, ObjMsCurrencyNew.IdCurrency)
                    FillOrNothing(.Nama, c.Nama)
                    FillOrNothing(.IdCurrencyNCBS, c.Pk_MsCurrencyPPATK_Id)
                    L_ObjNewMappingMsCurrencyNCBSPPATK.Add(ObjNewMappingMsCurrencyNCBSPPATK)
                End With
            Next
            DataRepository.MappingMsCurrencyNCBSPPATKProvider.Save(L_ObjNewMappingMsCurrencyNCBSPPATK)
            '======== Delete Approval data ======================================
            DeleteApproval(objMsCurrency_Approval, ObjMsCurrency_ApprovalDetail, L_ObjMappingMsCurrencyNCBSPPATK_Approval_Detail)
        End Using
    End Sub
    ''' <summary>
    ''' Insert Data dari approval ke table asli 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub Inserdata(ByVal objMsCurrency_Approval As MsCurrency_Approval)
        'Dim objAuditTrailType As MsAuditTrailEntityType = DataRepository.MsAuditTrailEntityTypeProvider.GetByPk_MsAuditTrailEntityTypeId(18)
        '   '================= Save Header ===========================================================
        Dim LObjMsCurrency_ApprovalDetail As TList(Of MsCurrency_ApprovalDetail) = DataRepository.MsCurrency_ApprovalDetailProvider.GetPaged(MsCurrency_ApprovalDetailColumn.PK_MsCurrency_Approval_id.ToString & "=" & objMsCurrency_Approval.PK_MsCurrencyPPATK_Approval_Id, "", 0, 1, Nothing)
        'jika data tidak ada di database
        If LObjMsCurrency_ApprovalDetail.Count = 0 Then Throw New Exception("Data Not Found")
        'binding approval ke table asli
        Dim ObjMsCurrency_ApprovalDetail As MsCurrency_ApprovalDetail = LObjMsCurrency_ApprovalDetail(0)
        Using ObjNewMsCurrency As New MsCurrency()
            With ObjNewMsCurrency
                FillOrNothing(.IdCurrency, ObjMsCurrency_ApprovalDetail.IdCurrency)
                FillOrNothing(.Name, ObjMsCurrency_ApprovalDetail.Name)
                FillOrNothing(.CreatedBy, ObjMsCurrency_ApprovalDetail.CreatedBy)
                FillOrNothing(.CreatedDate, ObjMsCurrency_ApprovalDetail.CreatedDate)
                FillOrNothing(.ApprovedBy, SessionUserId, True, oString)
                FillOrNothing(.ApprovedDate, Date.Now)
                .Activation = True
            End With
            DataRepository.MsCurrencyProvider.Save(ObjNewMsCurrency)
            'Insert auditrail
            'AuditTrailBLL.InsertAuditTrail(AuditTrailOperation.Add, objAuditTrailType, Now(), Sahassa.AML.Commonly.SessionNIK, Sahassa.AML.Commonly.SessionStaffName, "MsBentukBidangUsaha has Approved to Insert data", ObjNewMsBentukBidangUsaha, Nothing)
            '======== Insert MAPPING =========================================
            Dim L_ObjMappingMsCurrencyNCBSPPATK_Approval_Detail As TList(Of MappingMsCurrencyNCBSPPATK_Approval_Detail) = DataRepository.MappingMsCurrencyNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsCurrencyNCBSPPATK_Approval_DetailColumn.PK_MappingMsCurrencyNCBSPPATK_Approval_Id.ToString & _
    "=" & _
    parID, "", 0, Integer.MaxValue, Nothing)
            Dim L_ObjNewMappingMsCurrencyNCBSPPATK As New TList(Of MappingMsCurrencyNCBSPPATK)()
            For Each c As MappingMsCurrencyNCBSPPATK_Approval_Detail In L_ObjMappingMsCurrencyNCBSPPATK_Approval_Detail
                Dim ObjNewMappingMsCurrencyNCBSPPATK As New MappingMsCurrencyNCBSPPATK()
                With ObjNewMappingMsCurrencyNCBSPPATK
                    FillOrNothing(.IdCurrency, ObjNewMsCurrency.IdCurrency)
                    FillOrNothing(.Nama, c.Nama)
                    FillOrNothing(.IdCurrencyNCBS, c.Pk_MsCurrencyPPATK_Id)
                    L_ObjNewMappingMsCurrencyNCBSPPATK.Add(ObjNewMappingMsCurrencyNCBSPPATK)
                End With
            Next
            DataRepository.MappingMsCurrencyNCBSPPATKProvider.Save(L_ObjNewMappingMsCurrencyNCBSPPATK)
            '======== Delete Approval data ======================================
            DeleteApproval(objMsCurrency_Approval, ObjMsCurrency_ApprovalDetail, L_ObjMappingMsCurrencyNCBSPPATK_Approval_Detail)
        End Using
    End Sub

#End Region

End Class


