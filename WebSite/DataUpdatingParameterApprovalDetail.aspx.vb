Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Partial Class DataUpdatingParameterApprovalDetail
    Inherits Parent

#Region " Property "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get confirm type
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property ParamType() As Sahassa.AML.Commonly.TypeConfirm
        Get
            Return Me.Request.Params("TypeConfirm")
        End Get
    End Property

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get DataUpdatingParameterApprovalID
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    15/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property DataUpdatingParameterApprovalID() As Int64
        Get
            Return Me.Request.Params("ApprovalID")
        End Get
    End Property

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get param Parameters_PendingApprovalID
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    15/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property ParametersPendingApprovalID() As Int64
        Get
            Return Me.Request.Params("PendingApprovalID")
        End Get
    End Property

    ''' <summary>
    ''' get user id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetUserID() As String
        Get
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.Parameters_PendingApprovalTableAdapter
                Return AccessPending.SelectParameters_PendingApprovalUserID(Me.ParametersPendingApprovalID)
            End Using
        End Get
    End Property
#End Region

#Region " Load "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' hide all
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    15/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub HideAll()
        Dim StrId As String = ""
        Select Case Me.ParamType
            Case Sahassa.AML.Commonly.TypeConfirm.DataUpdatingParameterAdd
                StrId = "UserAdd"
            Case Sahassa.AML.Commonly.TypeConfirm.DataUpdatingParameterEdit
                StrId = "UserEdi"
            Case Else
                Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
        End Select
        For Each ObjRow As HtmlControls.HtmlTableRow In Me.TableDetail.Rows
            ObjRow.Visible = ObjRow.ID = "Button11" Or ObjRow.ID.Substring(0, 7) = StrId
        Next
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load LoadDataUpdatingParameterAdd 
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    22/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub LoadDataUpdatingParameterAdd()
        Me.LabelTitle.Text = "Activity: Add Data Updating Parameter"
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.DataUpdatingParameter_ApprovalTableAdapter
            Using ObjTable As Data.DataTable = AccessPending.GetDataUpdatingParameterApprovalData(Me.DataUpdatingParameterApprovalID)
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.DataUpdatingParameter_ApprovalRow = ObjTable.Rows(0)

                    Me.LabelAbnormalTransAdd.Text = rowData.AbnormalTransactionCount
                End If
            End Using
        End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load DataUpdatingParameter edit
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    15/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub LoadDataUpdatingParameterEdit()
        Me.LabelTitle.Text = "Activity: Edit Data Updating Parameter"
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.DataUpdatingParameter_ApprovalTableAdapter
            Using ObjTable As Data.DataTable = AccessPending.GetDataUpdatingParameterApprovalData(Me.DataUpdatingParameterApprovalID)
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.DataUpdatingParameter_ApprovalRow = ObjTable.Rows(0)
                    Me.LabelAbnormalTransNew.Text = rowData.AbnormalTransactionCount

                    Me.LabelAbnormalTransOld.Text = rowData.AbnormalTransactionCount_Old
                End If
            End Using
        End Using
    End Sub
#End Region

#Region " Accept "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' AcceptDataUpdatingParameter add accept
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    21/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptDataUpdatingParameterAdd()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            'Using TranScope As New Transactions.TransactionScope
            Using AccessDataUpdatingParameter As New AMLDAL.AMLDataSetTableAdapters.DataUpdatingParameterTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessDataUpdatingParameter, Data.IsolationLevel.ReadUncommitted)
                Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.DataUpdatingParameter_ApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, oSQLTrans)
                    Using AccessParametersApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_ApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessParametersApproval, oSQLTrans)
                        Using AccessParametersPendingApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_PendingApprovalTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessParametersPendingApproval, oSQLTrans)
                            Dim ObjTable As Data.DataTable = AccessPending.GetDataUpdatingParameterApprovalData(Me.DataUpdatingParameterApprovalID)
                            If ObjTable.Rows.Count > 0 Then
                                Dim rowData As AMLDAL.AMLDataSet.DataUpdatingParameter_ApprovalRow = ObjTable.Rows(0)
                                'tambahkan item tersebut dalam tabel DataUpdatingParameter
                                AccessDataUpdatingParameter.Insert(rowData.AbnormalTransactionCount, rowData.CreatedDate, rowData.LastUpdateDate)

                                If Sahassa.AML.AuditTrailAlert.AuditTrailChecking(3) Then
                                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingParameter", "AbnormalTransactionCount", "Add", "", rowData.AbnormalTransactionCount, "Accepted")
                                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingParameter", "CreatedDate", "Add", "", rowData.CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                                        AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingParameter", "LastUpdateDate", "Add", "", rowData.LastUpdateDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")

                                        DeleteAllApproval()
                                    End Using
                                End If
                            End If
                        End Using
                    End Using
                End Using
            End Using

            oSQLTrans.Commit()
            'End Using

            Me.Response.Redirect("DataUpdatingParameterManagementApproval.aspx", False)
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' DataUpdatingParameter edit accept
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    15/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptDataUpdatingParameterEdit()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            'Using TranScope As New Transactions.TransactionScope
            Using AccessDataUpdatingParameter As New AMLDAL.AMLDataSetTableAdapters.DataUpdatingParameterTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessDataUpdatingParameter, Data.IsolationLevel.ReadUncommitted)
                Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.DataUpdatingParameter_ApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, oSQLTrans)
                    Dim ObjTable As Data.DataTable = AccessPending.GetDataUpdatingParameterApprovalData(Me.DataUpdatingParameterApprovalID)
                    If ObjTable.Rows.Count > 0 Then
                        Dim rowData As AMLDAL.AMLDataSet.DataUpdatingParameter_ApprovalRow = ObjTable.Rows(0)
                        'update item tersebut dalam tabel DataUpdatingParameter
                        AccessDataUpdatingParameter.UpdateDataUpdatingParameter(rowData.AbnormalTransactionCount, rowData.LastUpdateDate)
                        Sahassa.AML.AuditTrailAlert.AuditTrailChecking(3)
                        'catat aktifitas dalam tabel Audit Trail
                        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingParameter", "AbnormalTransactionCount", "Edit", rowData.AbnormalTransactionCount_Old, rowData.AbnormalTransactionCount, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingParameter", "CreatedDate", "Edit", rowData.CreatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), rowData.CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingParameter", "LastUpdateDate", "Edit", rowData.LastUpdateDate_Old.ToString("dd-MMMM-yyyy HH:mm"), rowData.LastUpdateDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                            DeleteAllApproval()
                        End Using
                    End If
                End Using
            End Using

            oSQLTrans.Commit()
            'End Using
            Me.Response.Redirect("DataUpdatingParameterManagementApproval.aspx", False)
        Catch
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try

    End Sub

#End Region

#Region " Reject "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' reject DataUpdatingParameter add
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    21/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectDataUpdatingParameterAdd()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            'Using TranScope As New Transactions.TransactionScope
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAudit, Data.IsolationLevel.ReadUncommitted)
                Using AccessDataUpdatingParameter As New AMLDAL.AMLDataSetTableAdapters.DataUpdatingParameter_ApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessDataUpdatingParameter, oSQLTrans)
                    Using AccessParametersApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_ApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessParametersApproval, oSQLTrans)
                        Using AccessParametersPendingApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_PendingApprovalTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessParametersPendingApproval, oSQLTrans)
                            Using TableDataUpdatingParameter As Data.DataTable = AccessDataUpdatingParameter.GetDataUpdatingParameterApprovalData(Me.DataUpdatingParameterApprovalID)
                                If TableDataUpdatingParameter.Rows.Count > 0 Then
                                    Dim ObjRow As AMLDAL.AMLDataSet.DataUpdatingParameter_ApprovalRow = TableDataUpdatingParameter.Rows(0)
                                    Sahassa.AML.AuditTrailAlert.AuditTrailChecking(3)
                                    'catat aktifitas dalam tabel Audit Trail
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingParameter", "AbnormalTransactionCount", "Add", "", ObjRow.AbnormalTransactionCount, "Rejected")
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingParameter", "CreatedDate", "Add", "", ObjRow.CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingParameter", "LastUpdateDate", "Add", "", ObjRow.LastUpdateDate.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")
                                    DeleteAllApproval()
                                End If
                            End Using
                        End Using
                    End Using
                End Using
            End Using

            oSQLTrans.Commit()
            'End Using
            Me.Response.Redirect("DataUpdatingParameterManagementApproval.aspx", False)

        Catch
            Throw
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' DataUpdatingParameter reject edit
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    15/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectDataUpdatingParameterEdit()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            'Using TranScope As New Transactions.TransactionScope
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAudit, Data.IsolationLevel.ReadUncommitted)
                Using AccessDataUpdatingParameter As New AMLDAL.AMLDataSetTableAdapters.DataUpdatingParameter_ApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessDataUpdatingParameter, oSQLTrans)
                    Using AccessParametersApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_ApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessParametersApproval, oSQLTrans)
                        Using AccessParametersPendingApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_PendingApprovalTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessParametersPendingApproval, oSQLTrans)
                            Using TableDataUpdatingParameter As Data.DataTable = AccessDataUpdatingParameter.GetDataUpdatingParameterApprovalData(Me.DataUpdatingParameterApprovalID)
                                'Sahassa.AML.TableAdapterHelper.SetTransaction(TableDataUpdatingParameter, oSQLTrans)
                                If TableDataUpdatingParameter.Rows.Count > 0 Then
                                    Dim ObjRow As AMLDAL.AMLDataSet.DataUpdatingParameter_ApprovalRow = TableDataUpdatingParameter.Rows(0)
                                    Sahassa.AML.AuditTrailAlert.AuditTrailChecking(3)
                                    'catat aktifitas dalam tabel Audit Trail
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingParameter", "AbnormalTransactionCount", "Edit", ObjRow.AbnormalTransactionCount_Old, ObjRow.AbnormalTransactionCount, "Rejected")
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingParameter", "CreatedDate", "Edit", ObjRow.CreatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), ObjRow.CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - DataUpdatingParameter", "LastUpdateDate", "Edit", ObjRow.LastUpdateDate_Old.ToString("dd-MMMM-yyyy HH:mm"), ObjRow.LastUpdateDate.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")
                                    DeleteAllApproval()
                                End If
                            End Using
                        End Using
                    End Using
                End Using
            End Using
            oSQLTrans.Commit()
            'End Using
            Me.Response.Redirect("DataUpdatingParameterManagementApproval.aspx", False)
        Catch
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try

    End Sub

#End Region

#Region "Delete All Approval"
    ''' <summary>
    ''' delete all approval
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function DeleteAllApproval() As Boolean
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            'Using TransScope As New Transactions.TransactionScope
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.DataUpdatingParameter_ApprovalTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessPending, Data.IsolationLevel.ReadUncommitted)
                Using AccessParametersApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_ApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessParametersApproval, oSQLTrans)
                    Using AccessParametersPendingApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_PendingApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessParametersPendingApproval, oSQLTrans)
                        Dim ObjTable As Data.DataTable = AccessPending.GetDataUpdatingParameterApprovalData(Me.DataUpdatingParameterApprovalID)
                        If ObjTable.Rows.Count > 0 Then
                            Dim rowData As AMLDAL.AMLDataSet.DataUpdatingParameter_ApprovalRow = ObjTable.Rows(0)
                            'hapus item tsb dalam tabel Parameters_Approval
                            AccessParametersApproval.DeleteParametersApproval(Me.ParametersPendingApprovalID)

                            'hapus item tsb dalam tabel PotentialCustomerVerificationParameter_Approval
                            AccessPending.DeleteDataUpdatingParameterApproval(rowData.ApprovalID)

                            'hapus item tsb dalam tabel Parameters_PendingApproval
                            AccessParametersPendingApproval.DeleteParametersPendingApproval(Me.ParametersPendingApprovalID)
                        End If
                    End Using
                End Using
            End Using
            oSQLTrans.Commit()
            Return True
            'End Using
        Catch
            Return False
        End Try
    End Function

#End Region



    ''' <summary>
    ''' page load event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.ImageAccept.Attributes.Add("onclick", "javascript: if(confirm('Do you want to accept this activity ?')== false) return false;")
                Me.ImageReject.Attributes.Add("onclick", "javascript: if(confirm('Do you want to reject this activity ?')== false) return false;")
                Me.HideAll()
                Select Case Me.ParamType
                    Case Sahassa.AML.Commonly.TypeConfirm.DataUpdatingParameterAdd
                        Me.LoadDataUpdatingParameterAdd()
                    Case Sahassa.AML.Commonly.TypeConfirm.DataUpdatingParameterEdit
                        Me.LoadDataUpdatingParameterEdit()
                    Case Else
                        Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
                End Select

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' accept button handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click
        Try
            Select Case Me.ParamType
                Case Sahassa.AML.Commonly.TypeConfirm.DataUpdatingParameterAdd
                    Me.AcceptDataUpdatingParameterAdd()
                Case Sahassa.AML.Commonly.TypeConfirm.DataUpdatingParameterEdit
                    Me.AcceptDataUpdatingParameterEdit()
                Case Else
                    Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
            End Select

            Sahassa.AML.Commonly.SessionIntendedPage = "DataUpdatingParameterManagementApproval.aspx"

            Me.Response.Redirect("DataUpdatingParameterManagementApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' reject button handlert
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click
        Try
            Select Case Me.ParamType
                Case Sahassa.AML.Commonly.TypeConfirm.DataUpdatingParameterAdd
                    Me.RejectDataUpdatingParameterAdd()
                Case Sahassa.AML.Commonly.TypeConfirm.DataUpdatingParameterEdit
                    Me.RejectDataUpdatingParameterEdit()
                Case Else
                    Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
            End Select

            Sahassa.AML.Commonly.SessionIntendedPage = "DataUpdatingParameterManagementApproval.aspx"

            Me.Response.Redirect("DataUpdatingParameterManagementApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' back button handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "DataUpdatingParameterManagementApproval.aspx"

        Me.Response.Redirect("DataUpdatingParameterManagementApproval.aspx", False)
    End Sub
End Class