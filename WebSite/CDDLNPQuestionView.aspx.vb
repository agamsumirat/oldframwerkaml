Imports CDDLNPNettier.Data
Imports CDDLNPNettier.Entities
Imports AMLBLL
Imports System.Collections.Generic
Imports System.IO

Partial Class CDDLNPQuestionView
    Inherits Parent
    Private BindGridFromExcel As Boolean = False

#Region " Property "
    Private Property SetnGetCDDQuestion() As String
        Get
            Return CType(IIf(Session("CDD_Question.CDDQuestion") Is Nothing, String.Empty, Session("CDD_Question.CDDQuestion")), String)
        End Get
        Set(ByVal Value As String)
            Session("CDD_Question.CDDQuestion") = Value
        End Set
    End Property
    Private Property SetnGetCDDQuestionType() As String
        Get
            Return CType(IIf(Session("CDD_Question.CDDQuestionType") Is Nothing, String.Empty, Session("CDD_Question.CDDQuestionType")), String)
        End Get
        Set(ByVal Value As String)
            Session("CDD_Question.CDDQuestionType") = Value
        End Set
    End Property
    Private Property SetnGetIsRequired() As String
        Get
            Return CType(IIf(Session("CDD_Question.IsRequired") Is Nothing, String.Empty, Session("CDD_Question.IsRequired")), String)
        End Get
        Set(ByVal Value As String)
            Session("CDD_Question.IsRequired") = Value
        End Set
    End Property

    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("CDD_Question.Selected") Is Nothing, New ArrayList, Session("CDD_Question.Selected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("CDD_Question.Selected") = value
        End Set
    End Property
    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("CDD_Question.Sort") Is Nothing, "SortNo  asc", Session("CDD_Question.Sort"))
        End Get
        Set(ByVal Value As String)
            Session("CDD_Question.Sort") = Value
        End Set
    End Property
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("CDD_Question.CurrentPage") Is Nothing, 0, Session("CDD_Question.CurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("CDD_Question.CurrentPage") = Value
        End Set
    End Property
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("CDD_Question.RowTotal") Is Nothing, 0, Session("CDD_Question.RowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("CDD_Question.RowTotal") = Value
        End Set
    End Property
    Private Function SetnGetBindTable() As VList(Of vw_CDD_Question)
        Return DataRepository.vw_CDD_QuestionProvider.GetPaged(SearchFilter, SetnGetSort, SetnGetCurrentPage, Sahassa.AML.Commonly.GetDisplayedTotalRow, SetnGetRowTotal)
    End Function
#End Region

    Private Sub ClearThisPageSessions()
        Me.SetnGetCDDQuestion = String.Empty
        Me.SetnGetCDDQuestionType = String.Empty
        Me.SetnGetIsRequired = String.Empty

        Me.SetnGetSelectedItem = Nothing
        Me.SetnGetSort = Nothing
        Me.SetnGetCurrentPage = Nothing
        Me.SetnGetRowTotal = Nothing
    End Sub

    Private Sub BindComboBox()
        Try
            Using objQuestionType As TList(Of CDD_QuestionType) = DataRepository.CDD_QuestionTypeProvider.GetAll
                cboQuestionType.Items.Clear()
                cboQuestionType.Items.Add(New ListItem("[All]", String.Empty))
                For Each item As CDD_QuestionType In objQuestionType
                    cboQuestionType.Items.Add(New ListItem(item.QuestionTypeName, item.PK_CDD_QuestionType_ID))
                Next
            End Using

            cboIsRequired.Items.Clear()
            cboIsRequired.Items.Add(New ListItem("[All]", String.Empty))
            cboIsRequired.Items.Add(New ListItem("False", "0"))
            cboIsRequired.Items.Add(New ListItem("True", "1"))
        Catch
            Throw
        End Try
    End Sub

    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknown Commandname " & e.CommandName)
            End Select
            Me.CollectSelected()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
        Catch
            Throw
        End Try
    End Sub

    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub 'ClearControls

    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        'For Each gridRow As DataGridItem In Me.GridViewCDDQuestion.Items
        '    If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
        '        Dim PkId As String = gridRow.Cells(1).Text
        '        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        '        If Me.SetnGetSelectedItem.Contains(PkId) Then
        '            If Me.CheckBoxSelectAll.Checked Then
        '                If Not ArrTarget.Contains(PkId) Then
        '                    ArrTarget.Add(PkId)
        '                End If
        '            Else
        '                ArrTarget.Remove(PkId)
        '            End If
        '        Else
        '            If Me.CheckBoxSelectAll.Checked Then
        '                If Not ArrTarget.Contains(PkId) Then
        '                    ArrTarget.Add(PkId)
        '                End If
        '            End If
        '        End If
        '        Me.SetnGetSelectedItem = ArrTarget
        '    End If
        'Next

        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        For Each gridRow As DataGridItem In Me.GridViewCDDQuestion.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(1).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItem.Contains(StrKey) = False Then
                        ArrTarget.Add(StrKey)
                    End If
                Else
                    If Me.SetnGetSelectedItem.Contains(StrKey) Then
                        ArrTarget.Remove(StrKey)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItem = ArrTarget
        Me.BindGrid()
    End Sub

    Private Sub SetCheckedAll()
        Dim i As Int16 = 0
        Dim totalrow As Int16 = 0
        For Each gridRow As DataGridItem In Me.GridViewCDDQuestion.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = CType(gridRow.FindControl("CheckBoxExporttoExcel"), CheckBox)
                If chkBox.Checked Then
                    i = CType(i + 1, Int16)
                End If
                totalrow = CType(totalrow + 1, Int16)
            End If
        Next
        If i = totalrow Then
            Me.CheckBoxSelectAll.Checked = True
        Else
            Me.CheckBoxSelectAll.Checked = False
        End If
    End Sub

    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In Me.GridViewCDDQuestion.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = gridRow.FindControl("CheckBoxExporttoExcel")
                Dim PkId As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
                If ArrTarget.Contains(PkId) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(PkId)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(PkId)
                End If
                Me.SetnGetSelectedItem = ArrTarget
            End If
        Next
    End Sub

    Protected Sub GridViewCDDQuestion_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridViewCDDQuestion.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Dim chkBox As CheckBox = e.Item.FindControl("CheckBoxExporttoExcel")
                chkBox.Checked = Me.SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)
                If BindGridFromExcel = True Then
                    e.Item.Cells(2).Text = CStr((e.Item.ItemIndex + 1))
                Else
                    e.Item.Cells(2).Text = CStr((e.Item.ItemIndex + 1) + (Me.SetnGetCurrentPage * Sahassa.AML.Commonly.GetDisplayedTotalRow))
                End If

                If DataRepository.CDD_Question_ApprovalDetailProvider.GetTotalItems("PK_CDD_Question_ID=" & e.Item.Cells(1).Text, 0) > 0 Then
                    Dim LnkEdit As LinkButton = e.Item.FindControl("LnkEdit")
                    LnkEdit.Enabled = False
                    LnkEdit.Text = "On Appr."

                    Dim LnkDelete As LinkButton = e.Item.FindControl("LnkDelete")
                    LnkDelete.Visible = False
                End If
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridViewCDDQuestion_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridViewCDDQuestion.SortCommand
        Dim GridUser As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridViewCDDQuestion_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridViewCDDQuestion.ItemCommand
        Select Case e.CommandName.ToLower
            Case "edit"
                Response.Redirect("CDDLNPQuestionEdit.aspx?ID=" & e.Item.Cells(1).Text)

            Case "delete"
                Response.Redirect("CDDLNPQuestionDelete.aspx?ID=" & e.Item.Cells(1).Text)

        End Select
    End Sub

    Protected Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            Me.CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be less than or equal to the total page count.")
                End If
            Else
                Throw New Exception("Page number must be less than or equal to the total page count.")
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageClear.Click
        Try
            Me.ClearThisPageSessions()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Me.SetnGetCDDQuestion = txtQuestion.Text
        Me.SetnGetCDDQuestionType = cboQuestionType.SelectedValue
        Me.SetnGetIsRequired = cboIsRequired.SelectedValue
        Me.SetnGetCurrentPage = 0
    End Sub

    Private Sub BindGrid()
        Me.SettingControl()

        Me.GridViewCDDQuestion.DataSource = Me.SetnGetBindTable
        Me.GridViewCDDQuestion.VirtualItemCount = Me.SetnGetRowTotal
        Me.GridViewCDDQuestion.DataBind()
    End Sub

    Private Function SearchFilter() As String
        Dim strWhereClause(-1) As String

        If Me.SetnGetCurrentPage > Me.GetPageTotal - 1 And Me.GetPageTotal - 1 > 0 Then
            Me.SetnGetCurrentPage = Me.GetPageTotal - 1
        ElseIf Me.SetnGetCurrentPage = -1 Then
            Me.SetnGetCurrentPage = 0
        End If

        Try
            If SetnGetCDDQuestion.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = vw_CDD_QuestionColumn.CDD_Question.ToString & " LIKE '%" & SetnGetCDDQuestion & "%'"
            End If

            If SetnGetCDDQuestionType.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = vw_CDD_QuestionColumn.FK_CDD_QuestionType_ID.ToString & " = " & SetnGetCDDQuestionType
            End If

            If SetnGetIsRequired.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = vw_CDD_QuestionColumn.IsRequired.ToString & " = '" & SetnGetIsRequired & "'"
            End If

            Return String.Join(" AND ", strWhereClause)
        Catch
            Return String.Empty
        End Try
    End Function

    Private Sub SettingControl()
        txtQuestion.Text = Me.SetnGetCDDQuestion
        cboQuestionType.SelectedValue = Me.SetnGetCDDQuestionType
        cboIsRequired.SelectedValue = Me.SetnGetIsRequired
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using

                Me.ClearThisPageSessions()

                Me.GridViewCDDQuestion.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow
                BindComboBox()
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
            Me.SetInfoNavigate()
            Me.SetCheckedAll()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub BindSelected()
        Try
            Dim SbPk As New StringBuilder
            SbPk.Append("0, ")
            For Each IdPk As Int64 In Me.SetnGetSelectedItem
                SbPk.Append(IdPk.ToString & ", ")
            Next

            Me.GridViewCDDQuestion.DataSource = DataRepository.vw_CDD_QuestionProvider.GetPaged("PK_CDD_Question_ID in (" & SbPk.ToString.Substring(0, SbPk.Length - 2) & ") ", Me.SetnGetSort, 0, Integer.MaxValue, 0)
            Me.GridViewCDDQuestion.AllowPaging = False
            Me.GridViewCDDQuestion.DataBind()

            Me.GridViewCDDQuestion.Columns(0).Visible = False
            Me.GridViewCDDQuestion.Columns(1).Visible = False
            Me.GridViewCDDQuestion.Columns(GridViewCDDQuestion.Columns.Count - 2).Visible = False
            Me.GridViewCDDQuestion.Columns(GridViewCDDQuestion.Columns.Count - 1).Visible = False
        Catch
            Throw
        End Try
    End Sub

    Private Sub BindSelectedAll()
        Try
            Me.GridViewCDDQuestion.DataSource = DataRepository.vw_CDD_QuestionProvider.GetPaged(SearchFilter, Me.SetnGetSort, 0, Int32.MaxValue, 0)

            Me.GridViewCDDQuestion.AllowPaging = False
            Me.GridViewCDDQuestion.DataBind()

            Me.GridViewCDDQuestion.Columns(0).Visible = False
            Me.GridViewCDDQuestion.Columns(1).Visible = False
            Me.GridViewCDDQuestion.Columns(GridViewCDDQuestion.Columns.Count - 2).Visible = False
            Me.GridViewCDDQuestion.Columns(GridViewCDDQuestion.Columns.Count - 1).Visible = False
        Catch
            Throw
        End Try
    End Sub

    Protected Sub lnkExportData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExportData.Click
        Try
            BindGridFromExcel = True
            Me.CollectSelected()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelected()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=CDD_Question.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(Me.GridViewCDDQuestion)
            Me.GridViewCDDQuestion.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub lnkExportAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExportAll.Click
        Try
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelectedAll()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=CDD_Question.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(Me.GridViewCDDQuestion)
            Me.GridViewCDDQuestion.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub LinkButtonAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonAddNew.Click
        Me.Response.Redirect("CDDLNPQuestionAdd.aspx")
    End Sub
End Class