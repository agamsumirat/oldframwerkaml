#Region "Imports..."
Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports System.Data
Imports System.Collections.Generic
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
#End Region
Partial Class IFTI_Detail_Swift_Outgoing
    Inherits Parent
    Private BindGridFromExcel As Boolean = False
#Region "properties..."
    ReadOnly Property getIFTIPK() As Integer
        Get
            If Session("IFTIEdit.IFTIPK") = Nothing Then
                Session("IFTIEdit.IFTIPK") = CInt(Request.Params("ID"))
            End If
            Return Session("IFTIEdit.IFTIPK")
        End Get
    End Property
    Public Property getIFTIBeneficiaryTempPK() As Integer
        Get
            Return IIf(Session("IFTIEdit.IFTIBeneficiaryTempPK") Is Nothing, Nothing, Session("IFTIEdit.IFTIBeneficiaryTempPK"))
        End Get
        Set(ByVal value As Integer)
            Session("IFTIEdit.IFTIBeneficiaryTempPK") = value
        End Set

    End Property
    'Public Property ListObjBeneficiary() As TList(Of IFTI_Beneficiary)
    '    Get
    '        Return Session("ListObjBeneficiary.data")
    '    End Get
    '    Set(ByVal value As TList(Of IFTI_Beneficiary))
    '        Session("ListObjBeneficiary.data") = value
    '    End Set
    'End Property
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("IFTIViewSelected") Is Nothing, New ArrayList, Session("IFTIViewSelected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("IFTIViewSelected") = value
        End Set
    End Property
    Private Property SetnGetSenderAccount() As String
        Get
            Return IIf(Session("IFTIEdit.SenderAccount") Is Nothing, "", Session("IFTIEdit.SenderAccount"))
        End Get
        Set(ByVal Value As String)
            Session("IFTIEdit.SenderAccount") = Value
        End Set
    End Property
    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("IFTIEditSort") Is Nothing, "Pk_IFTI_Id  asc", Session("IFTIEditSort"))
        End Get
        Set(ByVal Value As String)
            Session("IFTIEditSort") = Value
        End Set
    End Property

    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("IFTIEditCurrentPage") Is Nothing, 0, Session("IFTIEditCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("IFTIEditCurrentPage") = Value
        End Set
    End Property

    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property

    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("IFTIEditRowTotal") Is Nothing, 0, Session("IFTIEditRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("IFTIEditRowTotal") = Value
        End Set
    End Property
    'ReadOnly Property getSenderAccount() As Integer
    '    Get
    '        If Session("IFTIEdit.SenderAccount") = Nothing Then
    '            Session("IFTIEdit.SenderAccount") = CInt(Request.Params("ID"))
    '        End If
    '        Return Session("IFTIEdit.SenderAccount")
    '    End Get
    'End Property
    Private Property SetnGetCurrentPageReceiver() As Integer
        Get
            Dim result As Integer = 0
            If Not (Session("IFTIEdit.CurrentPageReceiver") Is Nothing) Then
                result = CType(Session("IFTIEdit.CurrentPageReceiver"), Integer)
            End If

            Return result
        End Get
        Set(ByVal Value As Integer)
            Session("IFTIEdit.CurrentPageReceiver") = Value
        End Set
    End Property
    Private Property ReceiverDataTable() As Data.DataTable
        Get
            Dim dt As Data.DataTable = DataRepository.IFTI_BeneficiaryProvider.GetPaged(IFTI_BeneficiaryColumn.FK_IFTI_ID.ToString & " = " & getIFTIPK, "", 0, Integer.MaxValue, 0).ToDataSet(False).Tables(0)
            dt = CType(IIf(Session("IFTIEdit.Receiver") Is Nothing, dt, Session("IFTIEdit.Receiver")), Data.DataTable)
            Session("IFTIEdit.Receiver") = dt
            Return dt
        End Get
        Set(ByVal value As Data.DataTable)
            Session("IFTIEdit.Receiver") = value
        End Set
    End Property

#End Region
#Region "Function..."
    'Private Sub bindgrid()
    '    'ValidateBLL.ButtonAccess2(GridDataView, "IFTI_Approval_view.aspx")

    '    Dim objBeneficiary As TList(Of IFTI_Beneficiary_Temporary) = DataRepository.IFTI_Beneficiary_TemporaryProvider.GetAll 'GetPaged("FK_IFTI_ID=" & getIFTIPK, "", 0, Integer.MaxValue, 0)
    '    SetnGetRowTotal = objBeneficiary.Count
    '    If SetnGetCurrentPage > GetPageTotal - 1 And GetPageTotal - 1 > 0 Then
    '        SetnGetCurrentPage = GetPageTotal - 1
    '    End If
    '    GridDataView.DataSource = objBeneficiary
    '    GridDataView.CurrentPageIndex = SetnGetCurrentPage
    '    GridDataView.VirtualItemCount = SetnGetRowTotal
    '    GridDataView.DataBind()
    '    'If SetnGetRowTotal > 0 Then
    '    '    LabelNoRecordFound.Visible = False
    '    'Else
    '    '    LabelNoRecordFound.Visible = True
    '    'End If
    'End Sub
    Private Sub SetCOntrolLoad()
        'bind JenisID
        Using objJenisId As TList(Of IFTI_IDType) = DataRepository.IFTI_IDTypeProvider.GetAll
            If objJenisId.Count > 0 Then
                Me.cboPengirimNasInd_jenisidentitas.Items.Clear()
                cboPengirimNasInd_jenisidentitas.Items.Add("-Select-")
                ' cek(lagi)
                Me.CBoPengirimPenerus_IND_jenisIdentitas.Items.Clear()
                CBoPengirimPenerus_IND_jenisIdentitas.Items.Add("-Select-")

                Me.CBOBenfOwnerNasabah_JenisDokumen.Items.Clear()
                CBOBenfOwnerNasabah_JenisDokumen.Items.Add("-Select-")

                Me.CboPengirimNonNasabah_JenisDokumen.Items.Clear()
                CboPengirimNonNasabah_JenisDokumen.Items.Add("-Select-")

                Me.CBOBenfOwnerNonNasabah_JenisDokumen.Items.Clear()
                Me.CBOBenfOwnerNonNasabah_JenisDokumen.Items.Add("-Select-")

                'cek lagi
                Me.CBOPenerimaNasabah_IND_JenisIden.Items.Clear()
                Me.CBOPenerimaNasabah_IND_JenisIden.Items.Add("-Select-")

                'cek lagi
                For i As Integer = 0 To objJenisId.Count - 1
                    cboPengirimNasInd_jenisidentitas.Items.Add(New ListItem(objJenisId(i).IDType, objJenisId(i).PK_IFTI_IDType.ToString))
                    CBoPengirimPenerus_IND_jenisIdentitas.Items.Add(New ListItem(objJenisId(i).IDType, objJenisId(i).PK_IFTI_IDType.ToString))
                    CBOBenfOwnerNasabah_JenisDokumen.Items.Add(New ListItem(objJenisId(i).IDType, objJenisId(i).PK_IFTI_IDType.ToString))
                    CboPengirimNonNasabah_JenisDokumen.Items.Add(New ListItem(objJenisId(i).IDType, objJenisId(i).PK_IFTI_IDType.ToString))
                    CBOPenerimaNasabah_IND_JenisIden.Items.Add(New ListItem(objJenisId(i).IDType, objJenisId(i).PK_IFTI_IDType.ToString))
                    CBOBenfOwnerNonNasabah_JenisDokumen.Items.Add(New ListItem(objJenisId(i).IDType, objJenisId(i).PK_IFTI_IDType.ToString))
                Next
            End If
        End Using
        'Bind MsBentukBadanUsaha
        Using objBentukBadanUsaha As TList(Of MsBentukBidangUsaha) = DataRepository.MsBentukBidangUsahaProvider.GetAll
            If objBentukBadanUsaha.Count > 0 Then
                CBOIdenPengirimNas_Corp_BentukBadanUsaha.Items.Clear()
                CBOIdenPengirimNas_Corp_BentukBadanUsaha.Items.Add("-Select-")

                CBOPengirimPenerus_Korp_BentukBadanUsaha.Items.Clear()
                CBOPengirimPenerus_Korp_BentukBadanUsaha.Items.Add("-Select-")

                CBOPenerimaNasabah_Korp_BentukBadanUsahaLain.Items.Clear()
                CBOPenerimaNasabah_Korp_BentukBadanUsahaLain.Items.Add("-Select-")

                For i As Integer = 0 To objBentukBadanUsaha.Count - 1
                    CBOIdenPengirimNas_Corp_BentukBadanUsaha.Items.Add(New ListItem(objBentukBadanUsaha(i).BentukBidangUsaha, objBentukBadanUsaha(i).IdBentukBidangUsaha.ToString))
                    CBOPengirimPenerus_Korp_BentukBadanUsaha.Items.Add(New ListItem(objBentukBadanUsaha(i).BentukBidangUsaha, objBentukBadanUsaha(i).IdBentukBidangUsaha.ToString))
                    CBOPenerimaNasabah_Korp_BentukBadanUsahaLain.Items.Add(New ListItem(objBentukBadanUsaha(i).BentukBidangUsaha, objBentukBadanUsaha(i).IdBentukBidangUsaha.ToString))
                Next
            End If
        End Using

    End Sub
    Sub clearSession()
        Session("IFTIEdit.IFTIPK") = Nothing
        Session("IFTIEdit.IFTIBeneficiaryTempPK") = Nothing
        ReceiverDataTable = Nothing
    End Sub

#Region "SwiftOut..."
#Region "SwiftOutSender..."
#Region "SwiftOutSenderType..."
    Sub FieldSwiftOutSenderCase1()
        'PengirimNasabahIndividu

        Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetPaged(MsKotaKabColumn.Activation.ToString & "=1", MsKotaKabColumn.NamaKotaKab.ToString, 0, Integer.MaxValue, 0)
        Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
        Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetPaged(MsProvinceColumn.activation.ToString & "=1", MsProvinceColumn.Nama.ToString, 0, Integer.MaxValue, 0)
        Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi
        Dim objNegara As TList(Of MsNegara) = DataRepository.MsNegaraProvider.GetPaged(MsNegaraColumn.Activation.ToString & "=1", MsNegaraColumn.NamaNegara.ToString, 0, Integer.MaxValue, 0)
        Dim objSnegara As MsNegara 'Penampung hasil search negara yang pke picker
        Dim objPekerjaan As TList(Of MsPekerjaan) = DataRepository.MsPekerjaanProvider.GetPaged(MsPekerjaanColumn.Activation.ToString & "=1", MsPekerjaanColumn.NamaPekerjaan.ToString, 0, Integer.MaxValue, 0)
        Dim objSPekerjaan As MsPekerjaan 'Penampung hasil search pekerjaan

        Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)
            SetnGetSenderAccount = objIfti.Sender_Nasabah_INDV_NoRekening
            Me.TxtIdenPengirimNas_account.Text = SetnGetSenderAccount

            Me.TxtIdenPengirimNas_Ind_NamaLengkap.Text = objIfti.Sender_Nasabah_INDV_NamaLengkap

            TxtIdenPengirimNas_Ind_tanggalLahir.Text = FormatDate(objIfti.Sender_Nasabah_INDV_TanggalLahir)
            If Not IsNothing(objIfti.Sender_Nasabah_INDV_KewargaNegaraan) Then
                Me.Rb_IdenPengirimNas_Ind_kewarganegaraan.SelectedValue = objIfti.Sender_Nasabah_INDV_KewargaNegaraan
            End If
            If Not IsNothing(objIfti.Sender_Nasabah_INDV_KewargaNegaraan) Then
                If objIfti.Sender_Nasabah_INDV_KewargaNegaraan.ToString = "2" Then
                    Me.PengirimIndNegara.Visible = True
                    Me.PengirimIndNegaraLain.Visible = True
                    objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, objIfti.Sender_Nasabah_INDV_Negara)
                    If Not IsNothing(objSnegara) Then
                        TxtIdenPengirimNas_Ind_negara.Text = Safe(objSnegara.NamaNegara)
                        hfIdenPengirimNas_Ind_negara.Value = Safe(objSnegara.IDNegara)
                    End If
                    'Me.TxtIdenPengirimNas_Ind_negara.Text = objIfti.Sender_Nasabah_INDV_Negara
                    Me.TxtIdenPengirimNas_Ind_negaralain.Text = objIfti.Sender_Nasabah_INDV_NegaraLainnya
                End If

            End If
            objSPekerjaan = objPekerjaan.Find(MsPekerjaanColumn.IDPekerjaan, objIfti.Sender_Nasabah_INDV_Pekerjaan)
            If Not IsNothing(objSPekerjaan) Then
                TxtIdenPengirimNas_Ind_pekerjaan.Text = Safe(objSPekerjaan.NamaPekerjaan)
                hfIdenPengirimNas_Ind_pekerjaan.Value = Safe(objSPekerjaan.IDPekerjaan)
            End If
            Me.TxtIdenPengirimNas_Ind_Pekerjaanlain.Text = objIfti.Sender_Nasabah_INDV_PekerjaanLainnya
            Me.TxtIdenPengirimNas_Ind_Alamat.Text = objIfti.Sender_Nasabah_INDV_DOM_Alamat
            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objIfti.Sender_Nasabah_INDV_DOM_KotaKab)
            If Not IsNothing(objSkotaKab) Then
                Me.TxtIdenPengirimNas_Ind_Kota.Text = Safe(objSkotaKab.NamaKotaKab)
                hfIdenPengirimNas_Ind_Kota_dom.Value = Safe(objSkotaKab.IDKotaKab)
            End If
            Me.TxtIdenPengirimNas_Ind_kotalain.Text = objIfti.Sender_Nasabah_INDV_DOM_KotaKabLainnya

            objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(SafeNumIndex(objIfti.Sender_Nasabah_INDV_DOM_Propinsi))
            If Not IsNothing(objSProvinsi) Then
                TxtIdenPengirimNas_Ind_provinsi.Text = Safe(objSProvinsi.Nama)
                hfIdenPengirimNas_Ind_provinsi_dom.Value = Safe(objSProvinsi.IdProvince)
            End If
            Me.TxtIdenPengirimNas_Ind_provinsiLain.Text = objIfti.Sender_Nasabah_INDV_DOM_PropinsiLainnya
            Me.TxtIdenPengirimNas_Ind_alamatIdentitas.Text = objIfti.Sender_Nasabah_INDV_ID_Alamat

            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objIfti.Sender_Nasabah_INDV_ID_KotaKab)
            If Not IsNothing(objSkotaKab) Then
                Me.TxtIdenPengirimNas_Ind_kotaIdentitas.Text = Safe(objSkotaKab.NamaKotaKab)
                hfIdenPengirimNas_Ind_kotaIdentitas.Value = Safe(objSkotaKab.IDKotaKab)
            End If

            Me.TxtIdenPengirimNas_Ind_KotaLainIden.Text = objIfti.Sender_Nasabah_INDV_ID_KotaKabLainnya
            objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(SafeNumIndex(objIfti.Sender_Nasabah_INDV_ID_Propinsi))
            If Not IsNothing(objSProvinsi) Then
                Me.TxtIdenPengirimNas_Ind_ProvinsiIden.Text = Safe(objSProvinsi.Nama)
                hfIdenPengirimNas_Ind_ProvinsiIden.Value = Safe(objSProvinsi.IdProvince)
            End If

            Me.TxtIdenPengirimNas_Ind_ProvinsilainIden.Text = objIfti.Sender_Nasabah_INDV_ID_PropinsiLainnya
            Me.cboPengirimNasInd_jenisidentitas.SelectedValue = objIfti.Sender_Nasabah_INDV_FK_IFTI_IDType
            Me.TxtIdenPengirimNas_Ind_noIdentitas.Text = objIfti.Sender_Nasabah_INDV_NomorID
        End Using



    End Sub
    Sub FieldSwiftOutSenderCase2()
        'PengirimNasabahKorporasi

        Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetPaged(MsKotaKabColumn.Activation.ToString & "=1", MsKotaKabColumn.NamaKotaKab.ToString, 0, Integer.MaxValue, 0)
        Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
        Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetPaged(MsProvinceColumn.activation.ToString & "=1", MsProvinceColumn.Nama.ToString, 0, Integer.MaxValue, 0)
        Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi

        Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)
            SetnGetSenderAccount = objIfti.Sender_Nasabah_INDV_NoRekening
            Me.TxtIdenPengirimNas_account.Text = SetnGetSenderAccount
            Using objBentukBadanUsaha As MsBentukBidangUsaha = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(objIfti.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault(0))
                Me.CBOIdenPengirimNas_Corp_BentukBadanUsaha.SelectedValue = objBentukBadanUsaha.BentukBidangUsaha
            End Using
            Me.TxtIdenPengirimNas_Corp_BentukBadanUsahaLain.Text = objIfti.Sender_Nasabah_CORP_BentukBadanUsahaLainnya
            Me.TxtIdenPengirimNas_Corp_NamaKorp.Text = objIfti.Sender_Nasabah_CORP_NamaKorporasi
            Using objBidangUsaha As MsBidangUsaha = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(objIfti.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id.GetValueOrDefault(0))
                Me.TxtIdenPengirimNas_Corp_BidangUsahaKorp.Text = objBidangUsaha.NamaBidangUsaha
                hfIdenPengirimNas_Corp_BidangUsahaKorp.Value = objBidangUsaha.IdBidangUsaha
            End Using
            Me.TxtIdenPengirimNas_Corp_BidangUsahaKorpLainnya.Text = objIfti.Sender_Nasabah_CORP_BidangUsahaLainnya
            Me.TxtIdenPengirimNas_Corp_alamatkorp.Text = objIfti.Sender_Nasabah_CORP_AlamatLengkap
            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objIfti.Sender_Nasabah_CORP_KotaKab)
            If Not IsNothing(objSkotaKab) Then
                Me.TxtIdenPengirimNas_Corp_kotakorp.Text = Safe(objSkotaKab.NamaKotaKab)
                hfIdenPengirimNas_Corp_kotakorp.Value = Safe(objSkotaKab.IDKotaKab)
            End If

            Me.TxtIdenPengirimNas_Corp_kotakorplain.Text = objIfti.Sender_Nasabah_CORP_KotaKabLainnya
            objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(SafeNumIndex(objIfti.Sender_Nasabah_CORP_Propinsi))
            If Not IsNothing(objSProvinsi) Then
                Me.TxtIdenPengirimNas_Corp_provKorp.Text = Safe(objSProvinsi.Nama)
                hfIdenPengirimNas_Corp_provKorp.Value = Safe(objSProvinsi.IdProvince)
            End If
            Me.TxtIdenPengirimNas_Corp_provKorpLain.Text = objIfti.Sender_Nasabah_CORP_PropinsiLainnya
            Me.TxtIdenPengirimNas_Corp_AlamatAsal.Text = objIfti.Sender_Nasabah_CORP_Alamat_KantorCabangPengirim
            Me.TxtIdenPengirimNas_Corp_AlamatLuar.Text = objIfti.Sender_Nasabah_CORP_LN_AlamatLengkap
        End Using
    End Sub
    Sub FieldSwiftOutSenderCase3()
        'PengirimNonNasabah

        Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetPaged(MsKotaKabColumn.Activation.ToString & "=1", MsKotaKabColumn.NamaKotaKab.ToString, 0, Integer.MaxValue, 0)
        Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
        Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetPaged(MsProvinceColumn.activation.ToString & "=1", MsProvinceColumn.Nama.ToString, 0, Integer.MaxValue, 0)
        Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi

        Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)
            Me.RbPengirimNonNasabah_100Juta.SelectedValue = objIfti.FK_IFTI_NonNasabahNominalType_ID
            If RbPengirimNonNasabah_100Juta.SelectedValue = 2 Then
                jenisdokumen_nonNasabah_mand.Visible = True
            Else
                jenisdokumen_nonNasabah_mand.Visible = False
            End If
            Me.txtPenerimaNonNasabah_Nama.Text = objIfti.Sender_NonNasabah_NamaLengkap
            Me.TxtPengirimNonNasabah_TanggalLahir.Text = FormatDate(objIfti.Sender_NonNasabah_TanggalLahir)
            Me.TxtPengirimNonNasabah_alamatiden.Text = objIfti.Sender_NonNasabah_ID_Alamat
            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objIfti.Sender_NonNasabah_ID_KotaKab)
            If Not IsNothing(objSkotaKab) Then
                Me.TxtPengirimNonNasabah_kotaIden.Text = Safe(objSkotaKab.NamaKotaKab)
                hfPengirimNonNasabah_kotaIden.Value = Safe(objSkotaKab.IDKotaKab)
            End If

            Me.TxtPengirimNonNasabah_KoaLainIden.Text = objIfti.Sender_NonNasabah_ID_KotaKabLainnya
            objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(SafeNumIndex(objIfti.Sender_NonNasabah_ID_Propinsi))
            If Not IsNothing(objSProvinsi) Then
                Me.TxtPengirimNonNasabah_ProvIden.Text = Safe(objSProvinsi.Nama)
                hfPengirimNonNasabah_ProvIden.Value = Safe(objSProvinsi.IdProvince)
            End If
            Me.TxtPengirimNonNasabah_ProvLainIden.Text = objIfti.Sender_NonNasabah_ID_PropinsiLainnya
            Me.CboPengirimNonNasabah_JenisDokumen.TabIndex = objIfti.Sender_NonNasabah_FK_IFTI_IDType
            Me.TxtPengirimNonNasabah_NomorIden.Text = objIfti.Sender_NonNasabah_NomorID
        End Using
    End Sub
    Sub FieldSwiftOutSenderCase4()
        'PenyelenggaraPenerusIndividu

        'Ambil data buat cari PK Negara, Provinsi, KotaKab, dkk (ini variable yang bakal dpke berkali2)
        Dim i As Integer = 0 'buat iterasi
        Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetPaged(MsKotaKabColumn.Activation.ToString & "=1", MsKotaKabColumn.NamaKotaKab.ToString, 0, Integer.MaxValue, 0)
        Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
        Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetPaged(MsProvinceColumn.activation.ToString & "=1", MsProvinceColumn.Nama.ToString, 0, Integer.MaxValue, 0)
        Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi
        Dim objPekerjaan As TList(Of MsPekerjaan) = DataRepository.MsPekerjaanProvider.GetPaged(MsPekerjaanColumn.Activation.ToString & "=1", MsPekerjaanColumn.NamaPekerjaan.ToString, 0, Integer.MaxValue, 0)
        Dim objSPekerjaan As MsPekerjaan 'Penampung hasil search pekerjaan
        Dim objNegara As TList(Of MsNegara) = DataRepository.MsNegaraProvider.GetPaged(MsNegaraColumn.Activation.ToString & "=1", MsNegaraColumn.NamaNegara.ToString, 0, Integer.MaxValue, 0)
        Dim objSnegara As MsNegara 'Penampung hasil search negara yang pke picker

        Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)
            SetnGetSenderAccount = objIfti.Sender_Nasabah_INDV_NoRekening
            Me.TxtPengirimPenerus_Rekening.Text = SetnGetSenderAccount
            Me.txtPengirimPenerus_Korp_namabank.Text = objIfti.Sender_Nasabah_INDV_NamaBank
            Me.TxtPengirimPenerus_Ind_nama.Text = objIfti.Sender_Nasabah_INDV_NamaLengkap
            If Not IsNothing(objIfti.Sender_Nasabah_INDV_TanggalLahir) Then
                Me.TxtPengirimPenerus_Ind_TanggalLahir.Text = objIfti.Sender_Nasabah_INDV_TanggalLahir
            End If
            If Not IsNothing(objIfti.Sender_Nasabah_INDV_KewargaNegaraan) Then
                Me.RBPengirimPenerus_Ind_Kewarganegaraan.SelectedValue = objIfti.Sender_Nasabah_INDV_KewargaNegaraan
                If objIfti.Sender_Nasabah_INDV_KewargaNegaraan = "2" Then
                    Me.PengirimPenerusIndNegara.Visible = True
                    Me.PengirimPenerusIndNegaraLain.Visible = True
                    objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, objIfti.Sender_Nasabah_INDV_Negara)
                    If Not IsNothing(objSnegara) Then
                        TxtPengirimPenerus_IND_negara.Text = Safe(objSnegara.NamaNegara)
                        hfPengirimPenerus_IND_negara.Value = Safe(objSnegara.IDNegara)
                    End If
                    'Me.TxtPengirimPenerus_IND_negara.Text = objIfti.Sender_Nasabah_INDV_Negara
                    Me.TxtPengirimPenerus_IND_negaralain.Text = objIfti.Sender_Nasabah_INDV_NegaraLainnya
                End If
            End If
            objSPekerjaan = objPekerjaan.Find(MsPekerjaanColumn.IDPekerjaan, objIfti.Sender_Nasabah_INDV_Pekerjaan)
            If Not IsNothing(objSPekerjaan) Then
                TxtPengirimPenerus_IND_pekerjaan.Text = Safe(objSPekerjaan.NamaPekerjaan)
                hfPengirimPenerus_IND_pekerjaan.Value = Safe(objSPekerjaan.IDPekerjaan)
            End If
            Me.TxtPengirimPenerus_IND_pekerjaanLain.Text = objIfti.Sender_Nasabah_INDV_PekerjaanLainnya

            Me.TxtPengirimPenerus_IND_alamatDom.Text = objIfti.Sender_Nasabah_INDV_DOM_Alamat
            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objIfti.Sender_Nasabah_INDV_DOM_KotaKab)
            If Not IsNothing(objSkotaKab) Then
                TxtPengirimPenerus_IND_kotaDom.Text = Safe(objSkotaKab.NamaKotaKab)
                hfPengirimPenerus_IND_kotaDom.Value = Safe(objSkotaKab.IDKotaKab)
            End If
            Me.TxtPengirimPenerus_IND_kotaLainDom.Text = objIfti.Sender_Nasabah_INDV_DOM_KotaKabLainnya
            objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(SafeNumIndex(objIfti.Sender_Nasabah_INDV_DOM_Propinsi))
            If Not IsNothing(objSProvinsi) Then
                TxtPengirimPenerus_IND_ProvDom.Text = Safe(objSProvinsi.Nama)
                hfPengirimPenerus_IND_ProvDom.Value = Safe(objSProvinsi.IdProvince)
            End If
            Me.TxtPengirimPenerus_IND_ProvLainDom.Text = objIfti.Sender_Nasabah_INDV_DOM_PropinsiLainnya
            Me.TxtPengirimPenerus_IND_alamatIden.Text = objIfti.Sender_Nasabah_INDV_ID_Alamat
            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objIfti.Sender_Nasabah_INDV_ID_KotaKab)
            If Not IsNothing(objSkotaKab) Then
                Me.TxtPengirimPenerus_IND_kotaIden.Text = objIfti.Sender_Nasabah_INDV_ID_KotaKab
                hfPengirimPenerus_IND_kotaIden.Value = Safe(objSkotaKab.IDKotaKab)
            End If

            Me.TxtPengirimPenerus_IND_KotaLainIden.Text = objIfti.Sender_Nasabah_INDV_ID_KotaKabLainnya
            objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(SafeNumIndex(objIfti.Sender_Nasabah_INDV_ID_Propinsi))
            If Not IsNothing(objSProvinsi) Then
                Me.TxtPengirimPenerus_IND_ProvIden.Text = Safe(objSProvinsi.Nama)
                hfPengirimPenerus_IND_ProvIden.Value = Safe(objSProvinsi.IdProvince)
            End If

            Me.TxtPengirimPenerus_IND_ProvLainIden.Text = objIfti.Sender_Nasabah_INDV_ID_PropinsiLainnya
            Me.CBoPengirimPenerus_IND_jenisIdentitas.SelectedValue = objIfti.Sender_Nasabah_INDV_FK_IFTI_IDType
            Me.TxtPengirimPenerus_IND_nomoridentitas.Text = objIfti.Sender_Nasabah_INDV_NomorID
        End Using

    End Sub
    Sub FieldSwiftOutSenderCase5()
        'PenyelenggaraPenerusKorporasi

        Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetPaged(MsKotaKabColumn.Activation.ToString & "=1", MsKotaKabColumn.NamaKotaKab.ToString, 0, Integer.MaxValue, 0)
        Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
        Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetPaged(MsProvinceColumn.activation.ToString & "=1", MsProvinceColumn.Nama.ToString, 0, Integer.MaxValue, 0)
        Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi

        Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)
            Me.txtPengirimPenerus_Korp_namabank.Text = objIfti.Sender_Nasabah_CORP_NamaBank
            Using objBentukBadanUsaha As MsBentukBidangUsaha = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(objIfti.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault(0))
                Me.CBOPengirimPenerus_Korp_BentukBadanUsaha.SelectedValue = objBentukBadanUsaha.BentukBidangUsaha
            End Using
            Me.txtPengirimPenerus_Korp_BentukBadanUsahaLain.Text = objIfti.Sender_Nasabah_CORP_BentukBadanUsahaLainnya
            Me.txtPengirimPenerus_Korp_NamaKorp.Text = objIfti.Sender_Nasabah_CORP_NamaKorporasi
            Using objBidangUsaha As MsBidangUsaha = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(objIfti.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id.GetValueOrDefault(0))
                Me.txtPengirimPenerus_Korp_BidangUsahaKorp.Text = objBidangUsaha.NamaBidangUsaha
            End Using
            Me.txtPengirimPenerus_Korp_alamatkorp.Text = objIfti.Sender_Nasabah_CORP_AlamatLengkap
            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objIfti.Sender_Nasabah_CORP_KotaKab)
            If Not IsNothing(objSkotaKab) Then
                Me.txtPengirimPenerus_Korp_kotakorp.Text = Safe(objSkotaKab.NamaKotaKab)
                hfPengirimPenerus_Korp_kotakorp.Value = Safe(objSkotaKab.IDKotaKab)
            End If

            Me.txtPengirimPenerus_Korp_kotalainnyakorp.Text = objIfti.Sender_Nasabah_CORP_KotaKabLainnya
            objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(SafeNumIndex(objIfti.Sender_Nasabah_CORP_Propinsi))
            If Not IsNothing(objSProvinsi) Then
                Me.txtPengirimPenerus_Korp_ProvinsiKorp.Text = Safe(objSProvinsi.Nama)
                hfPengirimPenerus_Korp_ProvinsiKorp.Value = Safe(objSProvinsi.IdProvince)
            End If

            Me.txtPengirimPenerus_Korp_provinsiLainnyaKorp.Text = objIfti.Sender_Nasabah_CORP_PropinsiLainnya
            Me.txtPengirimPenerus_Korp_AlamatAsal.Text = objIfti.Sender_Nasabah_CORP_Alamat_KantorCabangPengirim
            Me.txtPengirimPenerus_Korp_AlamatLuar.Text = objIfti.Sender_Nasabah_CORP_LN_AlamatLengkap
        End Using
    End Sub
#End Region

#End Region
#Region "SwiftOutReceiver..."
    Sub FieldSwiftOutPenerimaNasabahPerorangan(ByVal PK_IFTI_Beneficiary_ID As Integer)
        'PenerimaNasabahPerorangan

        Dim objNegara As TList(Of MsNegara) = DataRepository.MsNegaraProvider.GetPaged(MsNegaraColumn.Activation.ToString & "=1", MsNegaraColumn.NamaNegara.ToString, 0, Integer.MaxValue, 0)
        Dim objSnegara As MsNegara 'Penampung hasil search negara yang pke picker

        'Using objIfti As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("PK_IFTI_Beneficiary_ID=" & PK_IFTI_Beneficiary_ID, "", 0, 1, 0)
        '    Me.txtPenerima_rekening.Text = objIfti(0).Beneficiary_Nasabah_INDV_NoRekening
        '    Me.txtPenerimaNasabah_IND_nama.Text = objIfti(0).Beneficiary_Nasabah_INDV_NamaLengkap
        '    Me.txtPenerimaNasabah_IND_TanggalLahir.Text = FormatDate(objIfti(0).Beneficiary_Nasabah_INDV_TanggalLahir)
        '    If Not IsNothing(objIfti(0).Beneficiary_Nasabah_INDV_KewargaNegaraan) Then
        '        Me.RbPenerimaNasabah_IND_Warganegara.SelectedValue = objIfti(0).Beneficiary_Nasabah_INDV_KewargaNegaraan
        '        If objIfti(0).Beneficiary_Nasabah_INDV_KewargaNegaraan = "2" Then
        '            Me.PenerimaIndNegara.Visible = True
        '            Me.PenerimaIndNegaraLain.Visible = True
        '            objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, objIfti(0).Beneficiary_Nasabah_INDV_Negara)
        '            If Not IsNothing(objSnegara) Then
        '                txtPenerimaNasabah_IND_negara.Text = Safe(objSnegara.NamaNegara)
        '                hfPenerimaNasabah_IND_negara.Value = Safe(objSnegara.IDNegara)
        '            End If
        '            Me.txtPenerimaNasabah_IND_negaraLain.Text = objIfti(0).Beneficiary_Nasabah_INDV_NegaraLainnya
        '        End If
        '    End If

        '    Me.txtPenerimaNasabah_IND_alamatIden.Text = objIfti(0).Beneficiary_Nasabah_INDV_ID_Alamat_Iden
        '    Me.txtPenerimaNasabah_IND_negaraBagian.Text = objIfti(0).Beneficiary_Nasabah_INDV_ID_NegaraBagian
        '    objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, objIfti(0).Beneficiary_Nasabah_INDV_ID_Negara)
        '    If Not IsNothing(objSnegara) Then
        '        txtPenerimaNasabah_IND_negaraIden.Text = Safe(objSnegara.NamaNegara)
        '        hfPenerimaNasabah_IND_negaraIden.Value = Safe(objSnegara.IDNegara)
        '    End If
        '    Me.txtPenerimaNasabah_IND_negaraLainIden.Text = objIfti(0).Beneficiary_Nasabah_INDV_ID_NegaraLainnya
        '    Me.CBOPenerimaNasabah_IND_JenisIden.SelectedValue = objIfti(0).Beneficiary_Nasabah_INDV_FK_IFTI_IDType
        '    Me.txtPenerimaNasabah_IND_NomorIden.Text = objIfti(0).Beneficiary_Nasabah_INDV_NomorID
        '    Me.txtPenerimaNasabah_IND_NilaiTransaksiKeuangan.Text = objIfti(0).Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan
        'End Using
        Dim dr As Data.DataRow = ReceiverDataTable.Rows(CInt(getIFTIBeneficiaryTempPK - 1))
        'Using objIfti As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("PK_IFTI_Beneficiary_ID=" & PK_IFTI_Beneficiary_ID, "", 0, 1, 0)
        dr("FK_IFTI_NasabahType_ID") = 1
        Me.txtPenerima_rekening.Text = dr("Beneficiary_Nasabah_INDV_NoRekening").ToString
        Me.txtPenerimaNasabah_IND_nama.Text = dr("Beneficiary_Nasabah_INDV_NamaLengkap").ToString
        Me.txtPenerimaNasabah_IND_TanggalLahir.Text = FormatDate(dr("Beneficiary_Nasabah_INDV_TanggalLahir"))
        If Not IsNothing(dr("Beneficiary_Nasabah_INDV_KewargaNegaraan")) Then
            Me.RbPenerimaNasabah_IND_Warganegara.SelectedValue = CInt(dr("Beneficiary_Nasabah_INDV_KewargaNegaraan"))
            If dr("Beneficiary_Nasabah_INDV_KewargaNegaraan") = "2" Then
                Me.PenerimaIndNegara.Visible = True
                Me.PenerimaIndNegaraLain.Visible = True
                objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, (dr("Beneficiary_Nasabah_INDV_Negara")))
                If Not IsNothing(objSnegara) Then
                    txtPenerimaNasabah_IND_negara.Text = Safe(objSnegara.NamaNegara)
                    hfPenerimaNasabah_IND_negara.Value = Safe(objSnegara.IDNegara)
                End If
                Me.txtPenerimaNasabah_IND_negaraLain.Text = dr("Beneficiary_Nasabah_INDV_NegaraLainnya").ToString
            End If
        End If

        Me.txtPenerimaNasabah_IND_alamatIden.Text = dr("Beneficiary_Nasabah_INDV_ID_Alamat_Iden").ToString
        Me.txtPenerimaNasabah_IND_negaraBagian.Text = dr("Beneficiary_Nasabah_INDV_ID_NegaraBagian").ToString
        objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, (dr("Beneficiary_Nasabah_INDV_ID_Negara")))
        If Not IsNothing(objSnegara) Then
            txtPenerimaNasabah_IND_negaraIden.Text = Safe(objSnegara.NamaNegara)
            hfPenerimaNasabah_IND_negaraIden.Value = Safe(objSnegara.IDNegara)
        End If
        Me.txtPenerimaNasabah_IND_negaraLainIden.Text = dr("Beneficiary_Nasabah_INDV_ID_NegaraLainnya").ToString
        If ObjectAntiNull(dr("Beneficiary_Nasabah_INDV_FK_IFTI_IDType")) = True Then
            Me.CBOPenerimaNasabah_IND_JenisIden.SelectedValue = CInt(dr("Beneficiary_Nasabah_INDV_FK_IFTI_IDType"))
        Else
            Me.CBOPenerimaNasabah_IND_JenisIden.SelectedIndex = 0
        End If

        Me.txtPenerimaNasabah_IND_NomorIden.Text = dr("Beneficiary_Nasabah_INDV_NomorID").ToString
        Me.txtPenerimaNasabah_IND_NilaiTransaksiKeuangan.Text = dr("Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan").ToString
        'End Using
    End Sub
    Sub FieldSwiftOutPenerimaNasabahKorporasi(ByVal PK_IFTI_Beneficiary_ID As Integer)
        'PenerimaNasabahKorporasi
        Dim objNegara As TList(Of MsNegara) = DataRepository.MsNegaraProvider.GetPaged(MsNegaraColumn.Activation.ToString & "=1", MsNegaraColumn.NamaNegara.ToString, 0, Integer.MaxValue, 0)
        Dim objSnegara As MsNegara 'Penampung hasil search negara yang pke picker

        'Using objIfti As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("PK_IFTI_Beneficiary_ID=" & PK_IFTI_Beneficiary_ID, "", 0, 1, 0)
        '    Me.txtPenerima_rekening.Text = objIfti(0).Beneficiary_Nasabah_CORP_NoRekening
        '    Using objBadanUsaha As MsBentukBidangUsaha = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(objIfti(0).Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id)
        '        If Not IsNothing(objBadanUsaha) Then
        '            Me.CBOPenerimaNasabah_Korp_BentukBadanUsahaLain.SelectedValue = objBadanUsaha.BentukBidangUsaha
        '        End If
        '    End Using
        '    Me.txtPenerimaNasabah_Korp_BentukBadanUsahaLainnya.Text = objIfti(0).Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya
        '    Me.txtPenerimaNasabah_Korp_namaKorp.Text = objIfti(0).Beneficiary_Nasabah_CORP_NamaKorporasi
        '    Using objBidangUsaha As MsBidangUsaha = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(objIfti(0).Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id)
        '        If Not IsNothing(objBidangUsaha) Then
        '            Me.txtPenerimaNasabah_Korp_BidangUsahaKorp.Text = objBidangUsaha.NamaBidangUsaha
        '            hfPenerimaNasabah_Korp_BidangUsahaKorp.Value = objBidangUsaha.IdBidangUsaha
        '        End If
        '    End Using
        '    Me.txtPenerimaNasabah_Korp_AlamatKorp.Text = objIfti(0).Beneficiary_Nasabah_CORP_AlamatLengkap
        '    Me.txtPenerimaNasabah_Korp_negaraKota.Text = objIfti(0).Beneficiary_Nasabah_CORP_ID_NegaraBagian
        '    objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, objIfti(0).Beneficiary_Nasabah_CORP_ID_Negara)
        '    If Not IsNothing(objSnegara) Then
        '        txtPenerimaNasabah_Korp_NegaraKorp.Text = Safe(objSnegara.NamaNegara)
        '        hfPenerimaNasabah_Korp_NegaraKorp.Value = Safe(objSnegara.IDNegara)
        '    End If
        '    Me.txtPenerimaNasabah_Korp_NegaraLainnyaKorp.Text = objIfti(0).Beneficiary_Nasabah_CORP_ID_NegaraLainnya
        '    Me.txtPenerimaNasabah_Korp_NilaiTransaksiKeuangan.Text = objIfti(0).Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan
        'End Using
        Dim dr As Data.DataRow = ReceiverDataTable.Rows(PK_IFTI_Beneficiary_ID - 1)

        Me.txtPenerima_rekening.Text = dr("Beneficiary_Nasabah_CORP_NoRekening").ToString
        If ObjectAntiNull(dr("Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id")) = True Then
            Using objBadanUsaha As MsBentukBidangUsaha = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(dr("Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id"))
                If Not IsNothing(objBadanUsaha) Then
                    Me.CBOPenerimaNasabah_Korp_BentukBadanUsahaLain.SelectedValue = objBadanUsaha.IdBentukBidangUsaha
                End If
            End Using
        Else
            Me.CBOPenerimaNasabah_Korp_BentukBadanUsahaLain.SelectedIndex = 0
        End If

        Me.txtPenerimaNasabah_Korp_BentukBadanUsahaLainnya.Text = dr("Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya").ToString
        Me.txtPenerimaNasabah_Korp_namaKorp.Text = dr("Beneficiary_Nasabah_CORP_NamaKorporasi").ToString
        If ObjectAntiNull(dr("Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id")) = True Then
            Using objBidangUsaha As MsBidangUsaha = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(dr("Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id"))
                If Not IsNothing(objBidangUsaha) Then
                    Me.txtPenerimaNasabah_Korp_BidangUsahaKorp.Text = objBidangUsaha.NamaBidangUsaha
                    hfPenerimaNasabah_Korp_BidangUsahaKorp.Value = objBidangUsaha.IdBidangUsaha
                End If
            End Using
        End If
        Me.txtPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Text = dr("Beneficiary_Nasabah_CORP_BidangUsahaLainnya").ToString
        Me.txtPenerimaNasabah_Korp_AlamatKorp.Text = dr("Beneficiary_Nasabah_CORP_AlamatLengkap").ToString
        Me.txtPenerimaNasabah_Korp_negaraKota.Text = dr("Beneficiary_Nasabah_CORP_ID_NegaraBagian").ToString
        objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, dr("Beneficiary_Nasabah_CORP_ID_Negara"))
        If Not IsNothing(objSnegara) Then
            txtPenerimaNasabah_Korp_NegaraKorp.Text = Safe(objSnegara.NamaNegara)
            hfPenerimaNasabah_Korp_NegaraKorp.Value = Safe(objSnegara.IDNegara)
        End If
        Me.txtPenerimaNasabah_Korp_NegaraLainnyaKorp.Text = dr("Beneficiary_Nasabah_CORP_ID_NegaraLainnya").ToString
        Me.txtPenerimaNasabah_Korp_NilaiTransaksiKeuangan.Text = dr("Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan").ToString

    End Sub
    Sub FieldSwiftOutPenerimaNonNasabah(ByVal PK_IFTI_Beneficiary_ID As Integer)
        'PenerimaNonNasabah

        Dim objNegara As TList(Of MsNegara) = DataRepository.MsNegaraProvider.GetAll
        Dim objSnegara As MsNegara 'Penampung hasil search negara yang pke picker

        'Using objIfti As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("PK_IFTI_Beneficiary_ID=" & PK_IFTI_Beneficiary_ID, "", 0, 1, 0)

        '    Dim dr As Data.DataRow = ReceiverDataTable.Rows(getIFTIBeneficiaryTempPK - 1)
        '    getIFTIBeneficiaryTempPK = (e.Item.ItemIndex + (SetnGetCurrentPageReceiver * SahassaCommonly.Commonly.GetDisplayedTotalRow)).ToString

        '    Me.txtPenerima_rekening.Text = objIfti(0).Beneficiary_NonNasabah_NoRekening
        '    Me.txtPenerimaNonNasabah_namabank.Text = objIfti(0).Beneficiary_NonNasabah_NamaBank
        '    Me.txtPenerimaNonNasabah_Nama.Text = objIfti(0).Beneficiary_NonNasabah_NamaLengkap
        '    Me.txtPenerimaNonNasabah_Alamat.Text = objIfti(0).Beneficiary_NonNasabah_ID_Alamat
        '    objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, objIfti(0).Beneficiary_NonNasabah_ID_Negara)
        '    If Not IsNothing(objSnegara) Then
        '        txtPenerimaNonNasabah_Negara.Text = Safe(objSnegara.NamaNegara)
        '        hfPenerimaNonNasabah_Negara.Value = Safe(objSnegara.IDNegara)
        '    End If
        '    Me.txtPenerimaNonNasabah_negaraLain.Text = objIfti(0).Beneficiary_Nasabah_CORP_ID_NegaraLainnya
        '    If Not IsNothing(objIfti(0).Beneficiary_NonNasabah_NilaiTransaksikeuangan) Then
        '        Me.txtPenerimaNonNasabah_nilaiTransaksiKeuangan.Text = objIfti(0).Beneficiary_NonNasabah_NilaiTransaksikeuangan
        '    End If
        'End Using


        Dim dr As Data.DataRow = ReceiverDataTable.Rows(PK_IFTI_Beneficiary_ID - 1)

        Me.txtPenerima_rekening.Text = dr("Beneficiary_NonNasabah_NoRekening").ToString
        Me.txtPenerimaNonNasabah_namabank.Text = dr("Beneficiary_NonNasabah_NamaBank").ToString
        Me.txtPenerimaNonNasabah_Nama.Text = dr("Beneficiary_NonNasabah_NamaLengkap").ToString
        Me.txtPenerimaNonNasabah_Alamat.Text = dr("Beneficiary_NonNasabah_ID_Alamat").ToString
        objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, dr("Beneficiary_NonNasabah_ID_Negara"))
        If Not IsNothing(objSnegara) Then
            txtPenerimaNonNasabah_Negara.Text = Safe(objSnegara.NamaNegara)
            hfPenerimaNonNasabah_Negara.Value = Safe(objSnegara.IDNegara)
        End If
        Me.txtPenerimaNonNasabah_negaraLain.Text = dr("Beneficiary_NonNasabah_ID_NegaraLainnya").ToString
        If Not IsNothing(dr("Beneficiary_NonNasabah_NilaiTransaksikeuangan")) Then
            Me.txtPenerimaNonNasabah_nilaiTransaksiKeuangan.Text = dr("Beneficiary_NonNasabah_NilaiTransaksikeuangan").ToString
        End If


    End Sub
#End Region
#Region "SwiftOutBOwner..."
    Sub FieldSwiftOutBOwnerNasabah()
        'BeneficialOwnerNasabah
        Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetPaged(MsKotaKabColumn.Activation.ToString & "=1", MsKotaKabColumn.NamaKotaKab.ToString, 0, Integer.MaxValue, 0)
        Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
        Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetPaged(MsProvinceColumn.activation.ToString & "=1", MsProvinceColumn.Nama.ToString, 0, Integer.MaxValue, 0)
        Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi

        Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)
            SafeDefaultValue = ""
            If Not IsNothing(objIfti) Then
                Me.BenfOwnerNasabah_rekening.Text = objIfti.BeneficialOwner_NoRekening
                Me.BenfOwnerNasabah_Nama.Text = objIfti.BeneficialOwner_NamaLengkap
                Me.BenfOwnerNasabah_tanggalLahir.Text = FormatDate(objIfti.BeneficialOwner_TanggalLahir)
                Me.BenfOwnerNasabah_AlamatIden.Text = objIfti.BeneficialOwner_ID_Alamat
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objIfti.BeneficialOwner_ID_KotaKab)
                If Not IsNothing(objSkotaKab) Then
                    Me.BenfOwnerNasabah_KotaIden.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfBenfOwnerNasabah_KotaIden.Value = Safe(objSkotaKab.IDKotaKab)
                End If

                Me.BenfOwnerNasabah_kotaLainIden.Text = objIfti.BeneficialOwner_ID_KotaKabLainnya
                objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(SafeNumIndex(objIfti.BeneficialOwner_ID_Propinsi))
                If Not IsNothing(objSProvinsi) Then
                    Me.BenfOwnerNasabah_ProvinsiIden.Text = Safe(objSProvinsi.Nama)
                    hfBenfOwnerNasabah_ProvinsiIden.Value = Safe(objSProvinsi.IdProvince)
                End If

                Me.BenfOwnerNasabah_ProvinsiLainIden.Text = objIfti.BeneficialOwner_ID_PropinsiLainnya
                If Not IsNothing(objIfti.BeneficialOwner_FK_IFTI_IDType) Then
                    Me.CBOBenfOwnerNasabah_JenisDokumen.SelectedValue = objIfti.BeneficialOwner_FK_IFTI_IDType
                Else
                    Me.CBOBenfOwnerNasabah_JenisDokumen.SelectedIndex = 0
                End If

                Me.BenfOwnerNasabah_NomorIden.Text = objIfti.BeneficialOwner_NomorID
            End If
        End Using


    End Sub
    Sub FieldSwiftOutBOwnerNonNasabah()
        'BeneficialOwnerNonNasabah
        Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetPaged(MsKotaKabColumn.Activation.ToString & "=1", MsKotaKabColumn.NamaKotaKab.ToString, 0, Integer.MaxValue, 0)
        Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
        Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetAll
        Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi

        Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)
            SafeDefaultValue = ""
            If Not IsNothing(objIfti) Then
                Me.BenfOwnerNonNasabah_Nama.Text = objIfti.BeneficialOwner_NamaLengkap
                If Not IsNothing(objIfti.BeneficialOwner_TanggalLahir) Then
                    Me.BenfOwnerNonNasabah_TanggalLahir.Text = objIfti.BeneficialOwner_TanggalLahir
                End If
                Me.BenfOwnerNonNasabah_AlamatIden.Text = objIfti.BeneficialOwner_ID_Alamat
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objIfti.BeneficialOwner_ID_KotaKab)
                If Not IsNothing(objSkotaKab) Then
                    Me.BenfOwnerNonNasabah_KotaIden.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfBenfOwnerNonNasabah_KotaIden.Value = Safe(objSkotaKab.IDKotaKab)
                End If

                Me.BenfOwnerNonNasabah_KotaLainIden.Text = objIfti.BeneficialOwner_ID_KotaKabLainnya
                objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(SafeNumIndex(objIfti.BeneficialOwner_ID_Propinsi))
                If Not IsNothing(objSProvinsi) Then
                    Me.BenfOwnerNonNasabah_ProvinsiIden.Text = Safe(objSProvinsi.Nama)
                    hfBenfOwnerNonNasabah_ProvinsiIden.Value = Safe(objSProvinsi.IdProvince)
                End If

                Me.BenfOwnerNonNasabah_ProvinsiLainIden.Text = objIfti.BeneficialOwner_ID_PropinsiLainnya
                If Not IsNothing(objIfti.BeneficialOwner_FK_IFTI_IDType) Then
                    Me.CBOBenfOwnerNonNasabah_JenisDokumen.SelectedValue = objIfti.BeneficialOwner_FK_IFTI_IDType
                End If
                Me.BenfOwnerNonNasabah_NomorIdentitas.Text = objIfti.BeneficialOwner_NomorID
            End If
        End Using
    End Sub
#End Region
#Region "SwiftOutValidation..."
#Region "Validation Sender"
    Sub ValidasiControl()
        If ObjectAntiNull(TxtUmum_TanggalLaporan.Text) = False Then Throw New Exception("Tanggal Laporan harus diisi  ")
        If RbUmum_JenisLaporan.SelectedValue <> "1" And ObjectAntiNull(TxtUmum_LTDLNKoreksi.Text) = False Then Throw New Exception("No. LTDLN Koreksi harus diisi  ")
        If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", TxtUmum_TanggalLaporan.Text) = False Then
            Throw New Exception("Tanggal Laporan tidak valid")
        End If
        If ObjectAntiNull(TxtUmum_PJKBankPelapor.Text) = False Or TxtUmum_PJKBankPelapor.Text.Length < 2 Then Throw New Exception("Nama PJK Bank Pelapor harus diisi minimal 3 karakter! ")
        If ObjectAntiNull(TxtUmum_NamaPejabatPJKBankPelapor.Text) = False Or TxtUmum_NamaPejabatPJKBankPelapor.Text.Length < 2 Then Throw New Exception("Nama Pejabat PJK Bank Pelapor harus diisi minimal 3 karakter!")
        If RbUmum_JenisLaporan.SelectedIndex = -1 Then Throw New Exception("Jenis Laporan harus dipilih  ")
    End Sub
    Sub validasiSender1()
        'senderIndividu
        If CboSwiftOutPengirim.SelectedIndex = -1 Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Tipe PJK Bank harus diisi  ")
        If Rb_IdenPengirimNas_TipePengirim.SelectedIndex = -1 Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Tipe Pengirim harus diisi  ")
        If Rb_IdenPengirimNas_TipeNasabah.SelectedIndex = -1 Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Tipe Nasabah harus diisi  ")
        If ObjectAntiNull(TxtIdenPengirimNas_account.Text) = False Or TxtIdenPengirimNas_account.Text.Length < 2 Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: No Rekening harus diisi minimal 3 karakter!")

        If ObjectAntiNull(TxtIdenPengirimNas_Ind_NamaLengkap.Text) = False Or TxtIdenPengirimNas_Ind_NamaLengkap.Text.Length < 2 Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Nama Lengkap harus diisi minimal 3 karakter! ")
        If ObjectAntiNull(TxtIdenPengirimNas_Ind_tanggalLahir.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Tanggal Lahir harus diisi  ")
        If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", TxtIdenPengirimNas_Ind_tanggalLahir.Text) = False Then
            Throw New Exception("Identitas Pengirim Nasabah Perorangan: Tanggal Lahir tidak valid")
        End If
        If Rb_IdenPengirimNas_Ind_kewarganegaraan.SelectedValue = "2" Then
            If ObjectAntiNull(TxtIdenPengirimNas_Ind_negara.Text) = False And ObjectAntiNull(TxtIdenPengirimNas_Ind_negaralain.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Negara Kewarganegaraan harus diisi  ")
            If ObjectAntiNull(TxtIdenPengirimNas_Ind_negara.Text) = True And ObjectAntiNull(TxtIdenPengirimNas_Ind_negaralain.Text) = True Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Negara Kewarganegaraan harus diisi salah satu saja  ")
        End If
        If ObjectAntiNull(hfIdenPengirimNas_Ind_pekerjaan.Value) = False And ObjectAntiNull(TxtIdenPengirimNas_Ind_Pekerjaanlain.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Pekerjaan harus diisi  ")
        If ObjectAntiNull(hfIdenPengirimNas_Ind_pekerjaan.Value) = True And ObjectAntiNull(TxtIdenPengirimNas_Ind_Pekerjaanlain.Text) = True Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Pekerjaan harus diisi salah satu saja  ")

        If cboPengirimNasInd_jenisidentitas.SelectedIndex = -1 Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Jenis Dokumen Identitas harus dipilih ")
        'alamat domisili
        If ObjectAntiNull(TxtIdenPengirimNas_Ind_Kota.Text) = True And ObjectAntiNull(TxtIdenPengirimNas_Ind_kotalain.Text) = True Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Kota/Kabupaten harus diisi salah satu saja ")
        If ObjectAntiNull(TxtIdenPengirimNas_Ind_provinsi.Text) = True And ObjectAntiNull(TxtIdenPengirimNas_Ind_provinsiLain.Text) = True Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Provinsi harus diisi salah satu saja ")

        'alamat identitas
        If ObjectAntiNull(TxtIdenPengirimNas_Ind_alamatIdentitas.Text) = False Or TxtIdenPengirimNas_Ind_alamatIdentitas.Text.Length < 2 Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Alamat Identitas harus diisi minimal 3 karakter! ")
        If ObjectAntiNull(TxtIdenPengirimNas_Ind_kotaIdentitas.Text) = False And ObjectAntiNull(TxtIdenPengirimNas_Ind_KotaLainIden.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Kota/Kabupaten harus diisi  ")
        If ObjectAntiNull(TxtIdenPengirimNas_Ind_kotaIdentitas.Text) = True And ObjectAntiNull(TxtIdenPengirimNas_Ind_KotaLainIden.Text) = True Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Kota/Kabupaten harus diisi salah satu saja ")
        If ObjectAntiNull(TxtIdenPengirimNas_Ind_ProvinsiIden.Text) = False And ObjectAntiNull(TxtIdenPengirimNas_Ind_ProvinsilainIden.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Provinsi harus diisi  ")
        If ObjectAntiNull(TxtIdenPengirimNas_Ind_ProvinsiIden.Text) = True And ObjectAntiNull(TxtIdenPengirimNas_Ind_ProvinsilainIden.Text) = True Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Provinsi harus diisi salah satu saja ")
        If ObjectAntiNull(TxtIdenPengirimNas_Ind_ProvinsiIden.Text) = True And ObjectAntiNull(TxtIdenPengirimNas_Ind_ProvinsilainIden.Text) = True Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Nomor Identitas tidak valid atau kurang dari 3 digit! ")
        If ObjectAntiNull(TxtIdenPengirimNas_Ind_noIdentitas.Text) = False Or TxtIdenPengirimNas_Ind_noIdentitas.Text.Length < 2 Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Nomor Identitas harus diisi lebih dari 2 karakter!  ")
        If TxtIdenPengirimNas_Ind_noIdentitas.Text <> "" Then
            If Not Regex.Match(TxtIdenPengirimNas_Ind_noIdentitas.Text.Trim(), "^[a-zA-Z0-9]*$").Success Then
                Throw New Exception("Nomor Identitas hanya boleh menggunakan huruf dan angka!")
            End If
        End If
    End Sub
    Sub validasiSender2()
        'senderKorp
        If CboSwiftOutPengirim.SelectedIndex = -1 Then Throw New Exception("Identitas Pengirim Nasabah korporasi: Tipe PJK Bank harus diisi  ")
        If Rb_IdenPengirimNas_TipePengirim.SelectedIndex = -1 Then Throw New Exception("Identitas Pengirim Nasabah korporasi: Tipe Pengirim harus diisi  ")
        If Rb_IdenPengirimNas_TipeNasabah.SelectedIndex = -1 Then Throw New Exception("Identitas Pengirim Nasabah korporasi: Tipe Nasabah harus diisi  ")
        If ObjectAntiNull(TxtIdenPengirimNas_account.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah korporasi: No Rekening harus diisi  ")

        If ObjectAntiNull(TxtIdenPengirimNas_Corp_NamaKorp.Text) = False Or TxtIdenPengirimNas_Corp_NamaKorp.Text.Length < 2 Then Throw New Exception("Identitas Pengirim Nasabah korporasi:Nama Lengkap harus diisi minimal 3 karakter! ")
        If ObjectAntiNull(TxtIdenPengirimNas_Corp_BidangUsahaKorp.Text) = False And ObjectAntiNull(Me.TxtIdenPengirimNas_Corp_BidangUsahaKorpLainnya.Text) = False Then Throw New Exception("Bidang Usaha Korporasi harus diisi  ")
        If ObjectAntiNull(TxtIdenPengirimNas_Corp_BidangUsahaKorp.Text) = True And ObjectAntiNull(Me.TxtIdenPengirimNas_Corp_BidangUsahaKorpLainnya.Text) = True Then Throw New Exception("Identitas Pengirim Nasabah korporasi: Bidang Usaha Korporasi harus diisi salah satu saja")
        'alamat
        If ObjectAntiNull(TxtIdenPengirimNas_Corp_alamatkorp.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah korporasi: Alamat Identitas harus diisi  ")
        If ObjectAntiNull(TxtIdenPengirimNas_Corp_kotakorp.Text) = False And ObjectAntiNull(TxtIdenPengirimNas_Corp_kotakorplain.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah korporasi: Kota/Kabupaten harus diisi  ")
        If ObjectAntiNull(TxtIdenPengirimNas_Corp_kotakorp.Text) = True And ObjectAntiNull(TxtIdenPengirimNas_Corp_kotakorplain.Text) = True Then Throw New Exception("Identitas Pengirim Nasabah korporasi: Kota/Kabupaten harus diisi salah satu saja ")
        If ObjectAntiNull(TxtIdenPengirimNas_Corp_provKorp.Text) = False And ObjectAntiNull(TxtIdenPengirimNas_Corp_provKorpLain.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah korporasi: Provinsi harus diisi  ")
        If ObjectAntiNull(TxtIdenPengirimNas_Corp_provKorp.Text) = True And ObjectAntiNull(TxtIdenPengirimNas_Corp_provKorpLain.Text) = True Then Throw New Exception("Identitas Pengirim Nasabah korporasi: Provinsi harus diisi salah satu saja  ")
    End Sub
    Sub validasiSender3()
        'senderNOnNasabah
        If CboSwiftOutPengirim.SelectedIndex = -1 Then Throw New Exception("Identitas Pengirim Non Nasabah: Tipe PJK Bank harus diisi  ")
        If Rb_IdenPengirimNas_TipePengirim.SelectedIndex = -1 Then Throw New Exception("Identitas Pengirim Non Nasabah: Tipe Pengirim harus diisi  ")

        If RbPengirimNonNasabah_100Juta.SelectedIndex = -1 Then Throw New Exception("Identitas Pengirim Non Nasabah: Jumlah Transaksi harus diisi  ")
        If ObjectAntiNull(TxtPengirimNonNasabah_nama.Text) = False Or TxtPengirimNonNasabah_nama.Text.Length < 2 Then Throw New Exception("Identitas Pengirim Non Nasabah: Nama Lengkap harus diisi minimal 3 karakter! ")
        If ObjectAntiNull(TxtPengirimNonNasabah_alamatiden.Text) = False Then Throw New Exception("Identitas Pengirim Non Nasabah: Alamat harus diisi  ")
        If ObjectAntiNull(TxtPengirimNonNasabah_kotaIden.Text) = True And ObjectAntiNull(Me.TxtPengirimNonNasabah_KoaLainIden.Text) = True Then Throw New Exception("Identitas Pengirim Non Nasabah: Kota/Kabupaten harus diisi salah satu saja ")
        If ObjectAntiNull(TxtPengirimNonNasabah_ProvIden.Text) = True And ObjectAntiNull(TxtPengirimNonNasabah_ProvLainIden.Text) = True Then Throw New Exception("Identitas Pengirim Non Nasabah: Provinsi harus diisi salah satu saja  ")
        If CboPengirimNonNasabah_JenisDokumen.SelectedIndex = -1 Then Throw New Exception("Identitas Pengirim Non Nasabah: Jenis Dokumen Identitas harus dipilih ")
        If ObjectAntiNull(TxtPengirimNonNasabah_NomorIden.Text) = False Then Throw New Exception("Identitas Pengirim Non Nasabah: Nomor Identitas harus diisi  ")
        If TxtPengirimNonNasabah_NomorIden.Text <> "" Then
            If Not Regex.Match(TxtPengirimNonNasabah_NomorIden.Text.Trim(), "^[a-zA-Z0-9]*$").Success Then
                Throw New Exception("Nomor Identitas hanya boleh menggunakan huruf dan angka!")
            End If
        End If
    End Sub
    Sub validasiSender4()
        'sender penerus Individu
        If CboSwiftOutPengirim.SelectedIndex = -1 Then Throw New Exception("Identitas Pengirim Perorangan: Tipe PJK Bank harus diisi  ")
        If Rb_IdenPengirimNas_TipeNasabah.SelectedIndex = -1 Then Throw New Exception("Identitas Pengirim Perorangan: Tipe Nasabah harus diisi  ")
        If ObjectAntiNull(TxtPengirimPenerus_Rekening.Text) = False Or TxtPengirimPenerus_Rekening.Text.Length < 2 Then Throw New Exception("Identitas Pengirim  Perorangan: Nomor rekening harus diisi minimal 3 karakter! ")
        If ObjectAntiNull(TxtPengirimPenerus_Ind_namaBank.Text) = False Or TxtPengirimPenerus_Ind_namaBank.Text.Length < 2 Then Throw New Exception("Identitas Pengirim Perorangan: Nama Bank harus diisi minimal 3 karakter! ")
        If ObjectAntiNull(TxtPengirimPenerus_Ind_nama.Text) = False Then Throw New Exception("Identitas Pengirim  Perorangan: Nama Lengkap harus diisi  ")

        If RBPengirimPenerus_Ind_Kewarganegaraan.SelectedValue = "2" Then
            If ObjectAntiNull(TxtPengirimPenerus_IND_negara.Text) = False And ObjectAntiNull(TxtPengirimPenerus_IND_negaralain.Text) = False Then Throw New Exception("Identitas Pengirim Perorangan: Negara Kewarganegaraan harus diisi  ")
            If ObjectAntiNull(TxtPengirimPenerus_IND_negara.Text) = True And ObjectAntiNull(TxtPengirimPenerus_IND_negaralain.Text) = True Then Throw New Exception("Identitas Pengirim Perorangan: Negara Kewarganegaraan harus diisi salah satu saja  ")
        End If
        If ObjectAntiNull(hfPengirimPenerus_IND_pekerjaan.Value) = False And ObjectAntiNull(TxtPengirimPenerus_IND_pekerjaanLain.Text) = False Then Throw New Exception("Identitas Pengirim Perorangan: Pekerjaan harus diisi  ")
        If ObjectAntiNull(hfPengirimPenerus_IND_pekerjaan.Value) = True And ObjectAntiNull(TxtPengirimPenerus_IND_pekerjaanLain.Text) = True Then Throw New Exception("Identitas Pengirim Perorangan: Pekerjaan harus diisi salah satu saja  ")
        'alamat domisili
        If ObjectAntiNull(TxtPengirimPenerus_IND_kotaDom.Text) = True And ObjectAntiNull(TxtPengirimPenerus_IND_kotaLainDom.Text) = True Then Throw New Exception("Identitas Pengirim Perorangan: Kota/Kabupaten harus diisi salah satu saja ")
        If ObjectAntiNull(Me.TxtPengirimPenerus_IND_ProvDom.Text) = True And ObjectAntiNull(TxtPengirimPenerus_IND_ProvLainDom.Text) = True Then Throw New Exception("Identitas Pengirim Perorangan: Provinsi harus diisi salah satu saja ")

        'alamat identitas
        If ObjectAntiNull(TxtPengirimPenerus_IND_kotaIden.Text) = True And ObjectAntiNull(TxtPengirimPenerus_IND_KotaLainIden.Text) = True Then Throw New Exception("Identitas Pengirim Perorangan: Kota/Kabupaten harus diisi salah satu saja ")
        If ObjectAntiNull(TxtPengirimPenerus_IND_ProvIden.Text) = True And ObjectAntiNull(TxtPengirimPenerus_IND_ProvLainIden.Text) = True Then Throw New Exception("Identitas Pengirim Perorangan: Provinsi harus diisi salah satu saja ")
        If ObjectAntiNull(TxtPengirimPenerus_IND_nomoridentitas.Text) = True And TxtPengirimPenerus_IND_ProvLainIden.Text.Length < 2 Then Throw New Exception("Identitas Pengirim Perorangan: Nomor Identitas kurang dari 2 digit!")
        If TxtPengirimPenerus_IND_nomoridentitas.Text <> "" Then
            If Not Regex.Match(TxtPengirimPenerus_IND_nomoridentitas.Text.Trim(), "^[a-zA-Z0-9]*$").Success Then
                Throw New Exception("Nomor Identitas hanya boleh menggunakan huruf dan angka!")
            End If
        End If
    End Sub
    Sub validasiSender5()
        'sender penerus korporasi
        If CboSwiftOutPengirim.SelectedIndex = -1 Then Throw New Exception("Identitas Pengirim  korporasi: Tipe PJK Bank harus diisi  ")
        If Rb_IdenPengirimNas_TipeNasabah.SelectedIndex = -1 Then Throw New Exception("Identitas Pengirim  korporasi: Tipe Nasabah harus diisi  ")

        If ObjectAntiNull(txtPengirimPenerus_Korp_namabank.Text) = False Then Throw New Exception("Identitas Pengirim  korporasi: Nama Bank harus diisi  ")
        If ObjectAntiNull(txtPengirimPenerus_Korp_NamaKorp.Text) = False Or txtPengirimPenerus_Korp_NamaKorp.Text.Length < 2 Then Throw New Exception("Identitas Pengirim  korporasi: Nama Korporasi harus diisi minimal 3 karakter!  ")
        If ObjectAntiNull(txtPengirimPenerus_Korp_BidangUsahaKorp.Text) = True And ObjectAntiNull(Me.txtPengirimPenerus_Korp_BidangUsahaKorplain.Text) = True Then Throw New Exception("Identitas Pengirim  korporasi: Bidang Usaha Korporasi harus diisi salah satu saja")

        If ObjectAntiNull(txtPengirimPenerus_Korp_kotakorp.Text) = True And ObjectAntiNull(Me.txtPengirimPenerus_Korp_kotalainnyakorp.Text) = True Then Throw New Exception("Identitas Pengirim  Korporasi: Kota/Kabupaten harus diisi salah satu saja ")
        If ObjectAntiNull(Me.txtPengirimPenerus_Korp_ProvinsiKorp.Text) = True And ObjectAntiNull(Me.txtPengirimPenerus_Korp_provinsiLainnyaKorp.Text) = True Then Throw New Exception("Identitas Pengirim  Korporasi: Provinsi harus diisi salah satu saja ")

    End Sub
#End Region
#Region "Validation Receiver"
    Function IsValidDecimal(ByVal intPrecision As Integer, ByVal intScale As Integer, ByVal strDatatoValidate As String) As Boolean

        Return Regex.IsMatch(strDatatoValidate, "^[\d]{1," & intPrecision & "}(\.[\d]{1," & intScale & "})?$")

    End Function
    Private Function ValidateReceiver() As Boolean
        Dim result As Boolean = True
        If RBPenerimaNasabah_TipePengirim.SelectedValue = 1 Then
            ' fj xf k
            If Rb_IdenPenerimaNas_TipeNasabah.SelectedValue = 1 Then
                'validasiReceiverInd()
                If ObjectAntiNull(txtPenerima_rekening.Text) = False Then
                    result = False
                    Throw New Exception("Identitas Penerima Nasabah Perorangan: Nomor rekening harus diisi  ")
                End If

                If RBPenerimaNasabah_TipePengirim.SelectedIndex = -1 Then
                    result = False
                    Throw New Exception("Identitas Penerima Nasabah Perorangan: Tipe Pengirim harus diisi  ")
                End If

                If Rb_IdenPenerimaNas_TipeNasabah.SelectedIndex = -1 Then
                    result = False
                    Throw New Exception("Identitas Penerima Nasabah Perorangan: Tipe Nasabah harus diisi  ")
                End If
                If ObjectAntiNull(txtPenerimaNasabah_IND_nama.Text) = False Or txtPenerimaNasabah_IND_nama.Text.Length < 2 Then
                    result = False
                    Throw New Exception("Identitas Penerima Nasabah Perorangan: Nama Lengkap harus diisi minimal 3 karakter! ")
                End If

                If RbPenerimaNasabah_IND_Warganegara.SelectedValue = "2" Then
                    If ObjectAntiNull(txtPenerimaNasabah_IND_negara.Text) = False And ObjectAntiNull(txtPenerimaNasabah_IND_negaraLain.Text) = False Then
                        result = False
                        Throw New Exception("Identitas Penerima Nasabah Perorangan: Negara Kewarganegaraan harus diisi  ")
                    End If

                    If ObjectAntiNull(txtPenerimaNasabah_IND_negara.Text) = True And ObjectAntiNull(txtPenerimaNasabah_IND_negaraLain.Text) = True Then
                        result = False
                        Throw New Exception("Identitas Penerima Nasabah Perorangan: Negara Kewarganegaraan harus diisi salah satu saja  ")
                    End If
                End If

                If ObjectAntiNull(txtPenerimaNasabah_IND_alamatIden.Text) = False Or txtPenerimaNasabah_IND_alamatIden.Text.Length < 2 Then
                    result = False
                    Throw New Exception("Identitas Penerima Nasabah Perorangan: Alamat Identitas harus diisi minimal 3 karakter! ")
                End If
                If ObjectAntiNull(txtPenerimaNasabah_IND_negaraIden.Text) = False And ObjectAntiNull(txtPenerimaNasabah_IND_negaraLainIden.Text) = False Then
                    result = False
                    Throw New Exception("Identitas Penerima Nasabah Perorangan: Negara harus diisi  ")
                End If
                If ObjectAntiNull(txtPenerimaNasabah_IND_negaraIden.Text) = True And ObjectAntiNull(txtPenerimaNasabah_IND_negaraLainIden.Text) = True Then
                    result = False
                    Throw New Exception("Identitas Penerima Nasabah Perorangan: Negara harus diisi salah satu saja ")
                End If
                'If ObjectAntiNull(txtPenerimaNasabah_IND_NomorIden.Text) = True And Not IsNothing(txtPenerimaNasabah_IND_NomorIden.Text) Or txtPenerimaNasabah_IND_NomorIden.Text.Length < 2 Then
                '    result = False
                '    Throw New Exception("Identitas Penerima Nasabah Perorangan: Nomor identitas tidak valid! ")
                'End If
                If ReceiverDataTable.Rows.Count > 1 Then
                    If ObjectAntiNull(txtPenerimaNasabah_IND_NilaiTransaksiKeuangan.Text) = False Then
                        result = False
                        Throw New Exception("Identitas Penerima Nasabah Perorangan: Nilai Transaksi harus diisi apabila penerima lebih dari 1  ")
                    End If
                End If
                If ObjectAntiNull(txtPenerimaNasabah_IND_NilaiTransaksiKeuangan.Text) = True Then
                    If Not IsValidDecimal(15, 2, txtPenerimaNasabah_IND_NilaiTransaksiKeuangan.Text) Then
                        result = False
                        Throw New Exception("Identitas Penerima Nasabah Perorangan: Nilai Transaksi tidak valid!  ")
                    End If
                End If
            Else
                'validasiReceiverKorp()
                If ObjectAntiNull(txtPenerima_rekening.Text) = False Then
                    result = False
                    Throw New Exception("Identitas Penerima Nasabah Korporasi: Nomor rekening harus diisi  ")
                End If
                If RBPenerimaNasabah_TipePengirim.SelectedIndex = -1 Then
                    result = False
                    Throw New Exception("Identitas Penerima Nasabah Korporasi: Tipe Pengirim harus diisi  ")
                End If
                If Rb_IdenPenerimaNas_TipeNasabah.SelectedIndex = -1 Then
                    result = False
                    Throw New Exception("Identitas Penerima Nasabah Korporasi: Tipe Nasabah harus diisi  ")
                End If

                If ObjectAntiNull(txtPenerimaNasabah_Korp_namaKorp.Text) = False Or txtPenerimaNasabah_Korp_namaKorp.Text.Length < 2 Then
                    result = False
                    Throw New Exception("Identitas Penerima Nasabah Korporasi: Nama Korporasi harus diisi minimal 3 karakter! ")
                End If
                If ObjectAntiNull(txtPenerimaNasabah_Korp_BidangUsahaKorp.Text) = True And ObjectAntiNull(txtPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Text) = True Then
                    result = False
                    Throw New Exception("Identitas Penerima Nasabah Korporasi: Bidang Usaha harus diisi salah satu saja ")
                End If
                If ObjectAntiNull(txtPenerimaNasabah_Korp_AlamatKorp.Text) = False Or txtPenerimaNasabah_Korp_AlamatKorp.Text.Length < 2 Then
                    result = False
                    Throw New Exception("Identitas Penerima Nasabah Korporasi: Alamat Identitas harus diisi  ")
                End If
                If ObjectAntiNull(txtPenerimaNasabah_Korp_NegaraKorp.Text) = False And ObjectAntiNull(txtPenerimaNasabah_Korp_NegaraLainnyaKorp.Text) = False Then
                    result = False
                    Throw New Exception("Identitas Penerima Nasabah Korporasi: Negara harus diisi  ")
                End If
                If ObjectAntiNull(txtPenerimaNasabah_Korp_NegaraKorp.Text) = True And ObjectAntiNull(txtPenerimaNasabah_Korp_NegaraLainnyaKorp.Text) = True Then
                    result = False
                    Throw New Exception("Identitas Penerima Nasabah Korporasi: Negara harus diisi salah satu saja ")
                End If

                If ReceiverDataTable.Rows.Count > 1 Then
                    If ObjectAntiNull(txtPenerimaNasabah_Korp_NilaiTransaksiKeuangan.Text) = False Then
                        result = False
                        Throw New Exception("Identitas Penerima Nasabah Korporasi: Nilai Transaksi harus diisi apabila penerima lebih dari 1  ")
                    End If
                End If
                If ObjectAntiNull(txtPenerimaNasabah_Korp_NilaiTransaksiKeuangan.Text) = True Then
                    If Not IsValidDecimal(15, 2, txtPenerimaNasabah_Korp_NilaiTransaksiKeuangan.Text) Then
                        result = False
                        Throw New Exception("Identitas Penerima Nasabah Korporasi: Nilai Transaksi tidak valid!  ")
                    End If
                End If
            End If

        Else
            'validasiReceiverNonNasabah()
            If ObjectAntiNull(txtPenerima_rekening.Text) = False Then
                'result = False
                'Throw New Exception("Identitas Penerima Non Nasabah: Nomor rekening harus diisi  ")
                If ObjectAntiNull(Me.txtPenerimaNonNasabah_namabank.Text) = False Then
                    result = False
                    Throw New Exception("Identitas Penerima Non Nasabah: Nama Bank harus diisi apabila no rekening kosong!  ")
                End If
                If ObjectAntiNull(Me.txtPenerimaNonNasabah_Alamat.Text) = False Then
                    result = False
                    Throw New Exception("Identitas Penerima Non Nasabah: Alamat Identitas harus diisi apabila no rekening kosong! ")
                End If
            End If
            If RBPenerimaNasabah_TipePengirim.SelectedIndex = -1 Then
                result = False
                Throw New Exception("Identitas Penerima Non Nasabah: Tipe Pengirim harus diisi  ")
            End If

            If ObjectAntiNull(txtPenerimaNonNasabah_Nama.Text) = False Or txtPenerimaNonNasabah_Nama.Text.Length < 2 Then
                result = False
                Throw New Exception("Identitas Penerima Non Nasabah: Nama Lengkap harus diisi minimal 3 karakter! ")
            End If

            If ObjectAntiNull(txtPenerimaNonNasabah_Negara.Text) = False And ObjectAntiNull(txtPenerimaNonNasabah_negaraLain.Text) = False Then
                result = False
                Throw New Exception("Identitas Penerima Non Nasabah: Negara harus diisi  ")
            End If
            If ObjectAntiNull(txtPenerimaNonNasabah_Negara.Text) = True And ObjectAntiNull(txtPenerimaNonNasabah_negaraLain.Text) = True Then
                result = False
                Throw New Exception("Identitas Penerima Non Nasabah: Negara harus diisi salah satu saja ")
            End If
            If ReceiverDataTable.Rows.Count > 1 Then
                If ObjectAntiNull(txtPenerimaNonNasabah_nilaiTransaksiKeuangan.Text) = False Then
                    result = False
                    Throw New Exception("Identitas Penerima Non Nasabah: Nilai Transaksi harus diisi apabila penerima lebih dari 1  ")
                End If
            End If
            If ObjectAntiNull(txtPenerimaNonNasabah_nilaiTransaksiKeuangan.Text) = True Then
                If Not IsValidDecimal(15, 2, txtPenerimaNonNasabah_nilaiTransaksiKeuangan.Text) Then
                    result = False
                    Throw New Exception("Identitas Penerima Non Nasabah: Nilai Transaksi tidak valid!  ")
                End If
            End If

        End If
        Return result
    End Function
    Sub validasiReceiverInd()
        'penerimaIndividu
        If ObjectAntiNull(txtPenerima_rekening.Text) = False Then Throw New Exception("Identitas Penerima Nasabah Perorangan: Nomor rekening harus diisi  ")
        If RBPenerimaNasabah_TipePengirim.SelectedIndex = -1 Then Throw New Exception("Identitas Penerima Nasabah Perorangan: Tipe Pengirim harus diisi  ")
        If Rb_IdenPenerimaNas_TipeNasabah.SelectedIndex = -1 Then Throw New Exception("Identitas Penerima Nasabah Perorangan: Tipe Nasabah harus diisi  ")

        If ObjectAntiNull(txtPenerimaNasabah_IND_nama.Text) = False Then Throw New Exception("Identitas Penerima Nasabah Perorangan: Nama Lengkap harus diisi  ")
        If RbPenerimaNasabah_IND_Warganegara.SelectedValue = "2" Then
            If ObjectAntiNull(txtPenerimaNasabah_IND_negara.Text) = False And ObjectAntiNull(txtPenerimaNasabah_IND_negaraLain.Text) = False Then Throw New Exception("Identitas Penerima Nasabah Perorangan: Negara Kewarganegaraan harus diisi  ")
            If ObjectAntiNull(txtPenerimaNasabah_IND_negara.Text) = True And ObjectAntiNull(txtPenerimaNasabah_IND_negaraLain.Text) = True Then Throw New Exception("Identitas Penerima Nasabah Perorangan: Negara Kewarganegaraan harus diisi salah satu saja  ")
        End If

        If ObjectAntiNull(txtPenerimaNasabah_IND_alamatIden.Text) = False Then Throw New Exception("Identitas Penerima Nasabah Perorangan: Alamat Identitas harus diisi  ")
        If ObjectAntiNull(txtPenerimaNasabah_IND_negaraIden.Text) = False And ObjectAntiNull(txtPenerimaNasabah_IND_negaraLainIden.Text) = False Then Throw New Exception("Identitas Penerima Nasabah Perorangan: Negara harus diisi  ")
        If ObjectAntiNull(txtPenerimaNasabah_IND_negaraIden.Text) = True And ObjectAntiNull(txtPenerimaNasabah_IND_negaraLainIden.Text) = True Then Throw New Exception("Identitas Penerima Nasabah Perorangan: Negara harus diisi salah satu saja ")
    End Sub
    Sub validasiReceiverKorp()
        'pennerimaKorporasi
        If ObjectAntiNull(txtPenerima_rekening.Text) = False Then Throw New Exception("Identitas Penerima Nasabah Korporasi: Nomor rekening harus diisi  ")
        If RBPenerimaNasabah_TipePengirim.SelectedIndex = -1 Then Throw New Exception("Identitas Penerima Nasabah Korporasi: Tipe Pengirim harus diisi  ")
        If Rb_IdenPenerimaNas_TipeNasabah.SelectedIndex = -1 Then Throw New Exception("Identitas Penerima Nasabah Korporasi: Tipe Nasabah harus diisi  ")

        If ObjectAntiNull(txtPenerimaNasabah_Korp_namaKorp.Text) = False Then Throw New Exception("Identitas Penerima Nasabah Korporasi: Nama Korporasi harus diisi  ")
        If ObjectAntiNull(txtPenerimaNasabah_Korp_BidangUsahaKorp.Text) = True And ObjectAntiNull(txtPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Text) = True Then Throw New Exception("Identitas Penerima Nasabah Korporasi: Bidang Usaha harus diisi salah satu saja ")
        If ObjectAntiNull(txtPenerimaNasabah_Korp_AlamatKorp.Text) = False Then Throw New Exception("Identitas Penerima Nasabah Korporasi: Alamat Identitas harus diisi  ")
        If ObjectAntiNull(txtPenerimaNasabah_Korp_NegaraKorp.Text) = False And ObjectAntiNull(txtPenerimaNasabah_IND_negaraLainIden.Text) = False Then Throw New Exception("Identitas Penerima Nasabah Korporasi: Negara harus diisi  ")
        If ObjectAntiNull(txtPenerimaNasabah_Korp_NegaraKorp.Text) = True And ObjectAntiNull(txtPenerimaNasabah_IND_negaraLainIden.Text) = True Then Throw New Exception("Identitas Penerima Nasabah Korporasi: Negara harus diisi salah satu saja ")
    End Sub
    Sub validasiReceiverNonNasabah()
        'penerimaNOnNasabah
        If ObjectAntiNull(txtPenerima_rekening.Text) = False Then Throw New Exception("Identitas Penerima Non Nasabah: Nomor rekening harus diisi  ")
        If RBPenerimaNasabah_TipePengirim.SelectedIndex = -1 Then Throw New Exception("Identitas Penerima Non Nasabah: Tipe Pengirim harus diisi  ")

        If ObjectAntiNull(txtPenerimaNonNasabah_Nama.Text) = False Then Throw New Exception("Identitas Penerima Non Nasabah: Nama Lengkap harus diisi  ")
        If ObjectAntiNull(txtPenerimaNasabah_Korp_AlamatKorp.Text) = False Then Throw New Exception("Identitas Penerima Non Nasabah: Alamat Identitas harus diisi  ")
        If ObjectAntiNull(txtPenerimaNonNasabah_Negara.Text) = False And ObjectAntiNull(txtPenerimaNasabah_IND_negaraLainIden.Text) = False Then Throw New Exception("Identitas Penerima Non Nasabah: Negara harus diisi  ")
        If ObjectAntiNull(txtPenerimaNonNasabah_Negara.Text) = True And ObjectAntiNull(txtPenerimaNasabah_IND_negaraLainIden.Text) = True Then Throw New Exception("Identitas Penerima Non Nasabah: Negara harus diisi salah satu saja ")
    End Sub
#End Region
    Sub validasiTransaksi()

        If ObjectAntiNull(Transaksi_tanggal.Text) = False Then Throw New Exception("Tanggal Transaksi harus diisi  ")
        If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", Transaksi_tanggal.Text) = False Then
            Throw New Exception("Tanggal Transaksi tidak valid")
        End If
        If ObjectAntiNull(Transaksi_Sender.Text) = False Or Transaksi_Sender.Text.Length < 2 Then Throw New Exception("Sender's Reference harus diisi  ")
        If ObjectAntiNull(Transaksi_BankOperationCode.Text) = False Then Throw New Exception("Bank Operation Code harus diisi  ")
        If ObjectAntiNull(Me.Transaksi_kantorCabangPengirim.Text) = False And Me.CboSwiftOutPengirim.SelectedValue = 2 Then Throw New Exception("Kantor Cabang Penyelenggara harus diisi!  ")
        If ObjectAntiNull(Transaksi_ValueTanggalTransaksi.Text) = False Then Throw New Exception("Tanggal Transaksi(value date) harus diisi  ")
        If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", Transaksi_ValueTanggalTransaksi.Text) = False Then
            Throw New Exception("Tanggal Transaksi tidak valid")
        End If
        If ObjectAntiNull(Transaksi_nilaitransaksi.Text) = False Then Throw New Exception("Nilai Transaksi harus diisi  ")
        If Not IsNumeric(Transaksi_nilaitransaksi.Text) Then Throw New Exception("Nilai Transaksi harus diisi angka ")
        If ObjectAntiNull(Transaksi_MataUangTransaksi.Text) = False And ObjectAntiNull(Transaksi_MataUangTransaksiLain.Text) = False Then Throw New Exception("Mata Uang Transaksi harus diisi  ")
        If ObjectAntiNull(Transaksi_MataUangTransaksi.Text) = True And ObjectAntiNull(Transaksi_MataUangTransaksiLain.Text) = True Then Throw New Exception("Mata Uang Transaksi harus diisi salah satu saja! ")
        If ObjectAntiNull(Transaksi_AmountdalamRupiah.Text) = False Then Throw New Exception("Amount dalam rupiah harus diisi  ")
        If Not IsNumeric(Transaksi_AmountdalamRupiah.Text) Then Throw New Exception("Amount dalam rupiah harus diisi angka ")
        If ObjectAntiNull(Transaksi_currency.Text) = False And ObjectAntiNull(Transaksi_currencyLain.Text) = False Then Throw New Exception("Mata Uang Transaksi harus diisi  ")
        If ObjectAntiNull(Transaksi_currency.Text) = True And ObjectAntiNull(Transaksi_currencyLain.Text) = True Then Throw New Exception("Mata Uang Transaksi harus diisi salah satu saja! ")
        If ObjectAntiNull(Transaksi_instructedAmount.Text) = False Then
            If Not IsNumeric(Transaksi_instructedAmount.Text) Then Throw New Exception("Amount dalam rupiah harus diisi angka ")
        End If
        If Me.Rb_IdenPengirimNas_TipePengirim.SelectedValue = 1 And CInt(Transaksi_AmountdalamRupiah.Text) > 100000000 And ObjectAntiNull(Transaksi_SumberPenggunaanDana.Text) = False Then Throw New Exception("Sumber Penggunaan Dana harus diisi! ")
        If ObjectAntiNull(Transaksi_nilaiTukar.Text) = False Then
            If Not IsNumeric(Transaksi_nilaiTukar.Text) Then Throw New Exception(" NIlai Tukar harus diisi angka ")
        End If
        If CboSwiftOutPengirim.SelectedIndex = -1 Then Throw New Exception("Tipe PJK Bank harus diisi  ")

    End Sub
    Sub validationBOwnerNasabah()
        If ObjectAntiNull(BenfOwnerNasabah_tanggalLahir.Text) = False Then Throw New Exception("Identitas Beneficial Owner Nasabah: Tanggal Lahir harus diisi  ")
        If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", BenfOwnerNasabah_tanggalLahir.Text) = False Then
            Throw New Exception("Identitas Beneficial Owner Nasabah: Tanggal Lahir tidak valid")
        End If

        If ObjectAntiNull(BenfOwnerNasabah_rekening.Text) = False Or BenfOwnerNasabah_rekening.Text.Length < 2 Then Throw New Exception("Identitas Beneficial Owner Nasabah: No Rekening harus diisi minimal 3 karakter!  ")
        If ObjectAntiNull(BenfOwnerNasabah_Nama.Text) = False Or BenfOwnerNasabah_Nama.Text.Length < 2 Then Throw New Exception("Identitas Beneficial Owner Nasabah: Nama Lengkap harus diisi minimal 3 karakter! ")
        If ObjectAntiNull(BenfOwnerNasabah_AlamatIden.Text) = False Then Throw New Exception("Identitas Beneficial Owner Nasabah: Alamat harus diisi  ")
        If ObjectAntiNull(BenfOwnerNasabah_KotaIden.Text) = False And ObjectAntiNull(BenfOwnerNasabah_kotaLainIden.Text) = False Then Throw New Exception("Identitas Beneficial Owner Nasabah: Kota/Kabupaten harus diisi! ")
        If ObjectAntiNull(BenfOwnerNasabah_ProvinsiIden.Text) = False And ObjectAntiNull(BenfOwnerNasabah_ProvinsiLainIden.Text) = False Then Throw New Exception("Identitas Beneficial Owner Nasabah: Provinsi harus diisi! ")
        If ObjectAntiNull(BenfOwnerNasabah_KotaIden.Text) = True And ObjectAntiNull(BenfOwnerNasabah_kotaLainIden.Text) = True Then Throw New Exception("Identitas Beneficial Owner Nasabah: Kota/Kabupaten harus diisi salah satu saja ")
        If ObjectAntiNull(BenfOwnerNasabah_ProvinsiIden.Text) = True And ObjectAntiNull(BenfOwnerNasabah_ProvinsiLainIden.Text) = True Then Throw New Exception("Identitas Beneficial Owner Nasabah: Provinsi harus diisi salah satu saja ")
        If CBOBenfOwnerNasabah_JenisDokumen.SelectedIndex = 0 Then Throw New Exception("Identitas Beneficial Owner Nasabah: Jenis Dokumen identitas harus diisi salah satu saja ")
        If ObjectAntiNull(BenfOwnerNasabah_NomorIden.Text) = True And BenfOwnerNasabah_NomorIden.Text.Length < 2 Then Throw New Exception("Identitas Beneficial Owner Nasabah: Nomor Identitas harus diisi lebih dari 2 karakter! ")
        If BenfOwnerNasabah_NomorIden.Text <> "" Then
            If Not Regex.Match(BenfOwnerNasabah_NomorIden.Text.Trim(), "^[a-zA-Z0-9]*$").Success Then
                Throw New Exception("Nomor Identitas hanya boleh menggunakan huruf dan angka!")
            End If
        End If
    End Sub
    Sub validationBOwnerNonNasabah()
        If ObjectAntiNull(BenfOwnerNonNasabah_TanggalLahir.Text) = False Then Throw New Exception("Identitas Beneficial Owner Non Nasabah: Tanggal Lahir harus diisi  ")
        If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", BenfOwnerNonNasabah_TanggalLahir.Text) = False Then
            Throw New Exception("Identitas Beneficial Owner Nasabah: Tanggal Lahir tidak valid")
        End If
        If ObjectAntiNull(BenfOwnerNonNasabah_Nama.Text) = False Or BenfOwnerNonNasabah_Nama.Text.Length < 2 Then Throw New Exception("Identitas Beneficial Owner No Nasabah: Nama Lengkap harus diisi!")
        If ObjectAntiNull(BenfOwnerNonNasabah_AlamatIden.Text) = False Or BenfOwnerNonNasabah_AlamatIden.Text.Length < 2 Then Throw New Exception("Identitas Beneficial Owner No Nasabah: Alamat harus diisi!")
        If ObjectAntiNull(BenfOwnerNasabah_KotaIden.Text) = False And ObjectAntiNull(BenfOwnerNasabah_kotaLainIden.Text) = False Then Throw New Exception("Identitas Beneficial Owner Non Nasabah: Kota/Kabupaten harus diisi! ")
        If ObjectAntiNull(BenfOwnerNasabah_KotaIden.Text) = True And ObjectAntiNull(BenfOwnerNasabah_kotaLainIden.Text) = True Then Throw New Exception("Identitas Beneficial Owner Non Nasabah: Kota/Kabupaten harus diisi salah satu saja ")
        If ObjectAntiNull(BenfOwnerNasabah_ProvinsiIden.Text) = True And ObjectAntiNull(BenfOwnerNasabah_ProvinsiLainIden.Text) = True Then Throw New Exception("Identitas Beneficial Owner Non Nasabah: Provinsi harus diisi salah satu saja ")
        If ObjectAntiNull(BenfOwnerNasabah_ProvinsiIden.Text) = False And ObjectAntiNull(BenfOwnerNasabah_ProvinsiLainIden.Text) = False Then Throw New Exception("Identitas Beneficial Owner Non Nasabah: Provinsi harus diisi! ")
        If ObjectAntiNull(BenfOwnerNonNasabah_NomorIdentitas.Text) = True And BenfOwnerNonNasabah_NomorIdentitas.Text.Length < 2 Then Throw New Exception("Identitas Beneficial Owner Non Nasabah: Nomor Identitas harus diisi! ")
        If BenfOwnerNonNasabah_NomorIdentitas.Text <> "" Then
            If Not Regex.Match(BenfOwnerNonNasabah_NomorIdentitas.Text.Trim(), "^[a-zA-Z0-9]*$").Success Then
                Throw New Exception("Nomor Identitas hanya boleh menggunakan huruf dan angka!")
            End If
        End If
    End Sub
#End Region
#End Region
#Region "TransactionType..."
    Sub TransactionSwiftOut()
        Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)
            SafeDefaultValue = ""
            If Not IsNothing(objIfti) Then
                Me.MultiViewNasabahTypeAll.ActiveViewIndex = 0
                'umum
                Me.TxtUmum_LTDLN.Text = Safe(objIfti.LTDLNNo)
                Me.TxtUmum_LTDLNKoreksi.Text = Safe(objIfti.LTDLNNoKoreksi)
                Me.TxtUmum_TanggalLaporan.Text = FormatDate(Safe(objIfti.TanggalLaporan))
                Me.TxtUmum_PJKBankPelapor.Text = Safe(objIfti.NamaPJKBankPelapor)
                Me.TxtUmum_NamaPejabatPJKBankPelapor.Text = Safe(objIfti.NamaPejabatPJKBankPelapor)
                Me.RbUmum_JenisLaporan.SelectedValue = Safe(objIfti.JenisLaporan)

                'Identitas Pengirim

                'cekTipe Penyelenggara
                Dim senderType As Integer
                If ObjectAntiNull(objIfti.Sender_FK_IFTI_NasabahType_ID) = True Then
                    senderType = objIfti.Sender_FK_IFTI_NasabahType_ID
                    Me.CboSwiftOutPengirim.SelectedValue = objIfti.Sender_FK_PJKBank_Type
                    Select Case (senderType)
                        Case 1
                            'pengirimNasabahIndividu
                            Me.CboSwiftOutPengirim.SelectedValue = 1
                            Rb_IdenPengirimNas_TipePengirim.SelectedValue = 1
                            Rb_IdenPengirimNas_TipeNasabah.SelectedValue = 1
                            Me.trTipePengirim.Visible = True
                            Me.trTipeNasabah.Visible = True
                            Me.MultiViewSwiftOUtIdentitasPengirim.ActiveViewIndex = 0
                            Me.MultiViewSwiftOutJenisNasabah.ActiveViewIndex = 0
                            Me.MultiViewPengirimNasabah.ActiveViewIndex = 0
                            FieldSwiftOutSenderCase1()
                        Case 2
                            'pengirimNasabahKorp
                            Me.CboSwiftOutPengirim.SelectedValue = 1
                            Rb_IdenPengirimNas_TipePengirim.SelectedValue = 1
                            Rb_IdenPengirimNas_TipeNasabah.SelectedValue = 2
                            Me.trTipePengirim.Visible = True
                            Me.trTipeNasabah.Visible = True
                            Me.MultiViewSwiftOUtIdentitasPengirim.ActiveViewIndex = 0
                            Me.MultiViewSwiftOutJenisNasabah.ActiveViewIndex = 0
                            Me.MultiViewPengirimNasabah.ActiveViewIndex = 1
                            FieldSwiftOutSenderCase2()
                        Case 3
                            'PengirimNonNasabah
                            Me.CboSwiftOutPengirim.SelectedValue = 1
                            Rb_IdenPengirimNas_TipePengirim.SelectedValue = 2
                            Me.trTipePengirim.Visible = True
                            Me.MultiViewSwiftOUtIdentitasPengirim.ActiveViewIndex = 0
                            Me.MultiViewSwiftOutJenisNasabah.ActiveViewIndex = 1
                            FieldSwiftOutSenderCase3()
                        Case 4
                            'PenyelenggaraIndividu
                            Me.CboSwiftOutPengirim.SelectedValue = 2
                            Rb_IdenPengirimNas_TipeNasabah.SelectedValue = 1
                            Me.trTipeNasabah.Visible = True
                            Me.MultiViewSwiftOUtIdentitasPengirim.ActiveViewIndex = 1
                            Me.MultiViewPengirimPenerus.ActiveViewIndex = 0
                            FieldSwiftOutSenderCase4()
                        Case 5
                            'PenyelenggaraKorp
                            Me.CboSwiftOutPengirim.SelectedValue = 2
                            Rb_IdenPengirimNas_TipeNasabah.SelectedValue = 2
                            Me.MultiViewSwiftOUtIdentitasPengirim.ActiveViewIndex = 1
                            Me.MultiViewPengirimPenerus.ActiveViewIndex = 1
                            Me.trSwiftOutPenerimaTipeNasabah.Visible = True
                            FieldSwiftOutSenderCase5()
                    End Select
                Else
                    Me.CboSwiftOutPengirim.SelectedIndex = 0
                End If


                'Beneficiary Owner
                'check if having Beneficiary Owner
                'check if FK_IFTI_BeneficialOwnerType_ID not nothing
                If ObjectAntiNull(objIfti.FK_IFTI_KeterlibatanBeneficialOwner_ID) = True Then
                    Rb_BOwnerNas_ApaMelibatkan.SelectedValue = objIfti.FK_IFTI_KeterlibatanBeneficialOwner_ID
                    If Rb_BOwnerNas_ApaMelibatkan.SelectedValue = 1 Then
                        Me.trBenfOwnerHubungan.Visible = True
                        Me.trBenfOwnerTipePengirim.Visible = True
                        Me.BenfOwnerHubunganPemilikDana.Text = objIfti.BeneficialOwner_HubunganDenganPemilikDana
                        Me.Rb_BOwnerNas_TipePengirim.SelectedValue = objIfti.FK_IFTI_BeneficialOwnerType_ID
                        Dim BOwnerID As Integer = objIfti.FK_IFTI_BeneficialOwnerType_ID
                        Select Case (BOwnerID)
                            Case 1
                                MultiViewSwiftOutBOwner.ActiveViewIndex = 0
                                FieldSwiftOutBOwnerNasabah()
                            Case 2
                                MultiViewSwiftOutBOwner.ActiveViewIndex = 1
                                FieldSwiftOutBOwnerNonNasabah()
                        End Select
                    End If
                End If




                'Transaksi
                Me.Transaksi_tanggal.Text = FormatDate(objIfti.TanggalTransaksi)
                Me.Transaksi_waktutransaksi.Text = objIfti.TimeIndication
                Me.Transaksi_Sender.Text = objIfti.SenderReference
                Me.Transaksi_BankOperationCode.Text = objIfti.BankOperationCode
                Me.Transaksi_InstructionCode.Text = objIfti.InstructionCode
                Me.Transaksi_kantorCabangPengirim.Text = objIfti.KantorCabangPenyelengaraPengirimAsal
                Me.Transaksi_kodeTipeTransaksi.Text = objIfti.TransactionCode
                Me.Transaksi_ValueTanggalTransaksi.Text = FormatDate(objIfti.ValueDate_TanggalTransaksi)
                Me.Transaksi_nilaitransaksi.Text = objIfti.ValueDate_NilaiTransaksi
                'Using objCurrency As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(objIfti.ValueDate_FK_Currency_ID)
                '    If Not IsNothing(objCurrency) Then
                '        Me.Transaksi_MataUangTransaksi.Text = objCurrency.MsCurrency_Code
                '        hfTransaksi_MataUangTransaksi.Value = objCurrency.Pk_MsCurrency_Id
                '    End If
                'End Using
                'Dim objCurrency As TList(Of MsCurrency) = DataRepository.MsCurrencyProvider.GetPaged(MsCurrencyColumn.Pk_MsCurrency_Id.ToString & _
                '                                                             " like '%" & objIfti.ValueDate_FK_Currency_ID.ToString & "%'", "", 0, Integer.MaxValue, 0)
                'Me.Transaksi_MataUangTransaksi.Text = objCurrency(0).CurrencyCode & "-" & objCurrency(0).CurrencyName
                'hfTransaksi_MataUangTransaksi.Value = objCurrency(0).Pk_MsCurrencyPPATK_Id
                Using objCurrency As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(objIfti.ValueDate_FK_Currency_ID.GetValueOrDefault(0))
                    If Not IsNothing(objCurrency) Then
                        Me.Transaksi_MataUangTransaksi.Text = objCurrency.Code
                        hfTransaksi_MataUangTransaksi.Value = objCurrency.Code
                    End If
                End Using
                Me.Transaksi_MataUangTransaksiLain.Text = objIfti.ValueDate_CurrencyLainnya
                Me.Transaksi_AmountdalamRupiah.Text = objIfti.ValueDate_NilaiTransaksiIDR
                'Using objCurrency2 As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(objIfti.Instructed_Currency)
                '    If Not IsNothing(objCurrency) Then
                '        Me.Transaksi_currency.Text = objCurrency2.MsCurrency_Code
                '        hfTransaksi_currency.Value = objCurrency2.Pk_MsCurrency_Id
                '    End If
                'End Using
                '               Dim objCurrency2 As TList(Of MsCurrency) = DataRepository.MsCurrencyProvider.GetPaged(MsCurrencyColumn.Pk_MsCurrency_Id.ToString & _
                '" like '%" & objIfti.Instructed_Currency.ToString & "%'", "", 0, Integer.MaxValue, 0)
                '               Me.Transaksi_currency.Text = objCurrency2(0).CurrencyCode & "-" & objCurrency(0).CurrencyName
                '               hfTransaksi_currency.Value = objCurrency2(0).Pk_MsCurrencyPPATK_Id
                Using objCurrency As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(objIfti.Instructed_Currency.GetValueOrDefault(0))
                    If Not IsNothing(objCurrency) Then
                        Me.Transaksi_currency.Text = objCurrency.Code
                        hfTransaksi_currency.Value = objCurrency.Code
                    End If
                End Using
                Me.Transaksi_currencyLain.Text = objIfti.Instructed_CurrencyLainnya
                If Not IsNothing(objIfti.Instructed_Amount) Then
                    Me.Transaksi_instructedAmount.Text = objIfti.Instructed_Amount
                End If
                If Not IsNothing(objIfti.ExchangeRate) Then
                    Me.Transaksi_nilaiTukar.Text = objIfti.ExchangeRate
                End If


                Me.Transaksi_sendingInstitution.Text = objIfti.SendingInstitution
                Me.Transaksi_TujuanTransaksi.Text = objIfti.TujuanTransaksi
                Me.Transaksi_SumberPenggunaanDana.Text = objIfti.SumberPenggunaanDana


                'Informasi Lainnya

                Me.InformasiLainnya_Sender.Text = objIfti.InformationAbout_SenderCorrespondent
                InformasiLainnya_receiver.Text = objIfti.InformationAbout_ReceiverCorrespondent
                InformasiLainnya_thirdReimbursement.Text = objIfti.InformationAbout_Thirdreimbursementinstitution
                InformasiLainnya_intermediary.Text = objIfti.InformationAbout_IntermediaryInstitution
                InformasiLainnya_Remittance.Text = objIfti.RemittanceInformation
                InformasiLainnya_SenderToReceiver.Text = objIfti.SendertoReceiverInformation
                InformasiLainnya_RegulatoryReport.Text = objIfti.RegulatoryReporting
                InformasiLainnya_EnvelopeContents.Text = objIfti.EnvelopeContents
            End If
        End Using

    End Sub
#End Region

#End Region
#Region "Receiver"
    Private Sub LoadReceiver()
        Me.GridDataView.DataSource = ReceiverDataTable
        GridDataView.DataBind()

        'lnkBtnDeleteAllChronology.Enabled = False
        'If (ReceiverDataTable.Rows.Count > 0) Then
        '    lnkBtnDeleteAllChronology.Enabled = True
        'End If

        'lblTotalChronologyValue.Text = ReceiverDataTable.Rows.Count.ToString
    End Sub

    'Private Sub AddReceiver()
    '    Try
    '        If ValidateReceiver() Then
    '            Dim dr As Data.DataRow = Nothing
    '            Dim tipepenerima As Integer = RBPenerimaNasabah_TipePengirim.SelectedValue
    '            Dim tipenasabah As Integer
    '            Dim nasabahtype As Integer

    '            dr = ReceiverDataTable.NewRow
    '            dr("FK_IFTI_ID") = getIFTIPK

    '            If tipepenerima = 1 Then
    '                tipenasabah = Rb_IdenPenerimaNas_TipeNasabah.SelectedValue
    '                If tipenasabah = 1 Then
    '                    nasabahtype = 1
    '                    dr("FK_IFTI_NasabahType_ID") = nasabahtype

    '                    dr("Beneficiary_Nasabah_INDV_NoRekening") = txtPenerima_rekening.Text
    '                    dr("Beneficiary_Nasabah_INDV_NamaLengkap") = txtPenerimaNasabah_IND_nama.Text
    '                    If txtPenerimaNasabah_IND_TanggalLahir.Text.Trim <> "" Then
    '                        dr("Beneficiary_Nasabah_INDV_TanggalLahir") = txtPenerimaNasabah_IND_TanggalLahir.Text.Trim() 'Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", txtPenerimaNasabah_IND_TanggalLahir.Text.Trim())
    '                    End If
    '                    dr("Beneficiary_Nasabah_INDV_KewargaNegaraan") = RbPenerimaNasabah_IND_Warganegara.SelectedValue
    '                    If RbPenerimaNasabah_IND_Warganegara.SelectedValue = 2 Then
    '                        If txtPenerimaNasabah_IND_negara.Text <> "" Then
    '                            dr("Beneficiary_Nasabah_INDV_Negara") = hfPenerimaNasabah_IND_negara.Value
    '                        Else
    '                            dr("Beneficiary_Nasabah_INDV_Negara") = DBNull.Value
    '                        End If

    '                        dr("Beneficiary_Nasabah_INDV_NegaraLainnya") = txtPenerimaNasabah_IND_negaraLain.Text
    '                    End If

    '                    dr("Beneficiary_Nasabah_INDV_ID_Alamat_Iden") = txtPenerimaNasabah_IND_alamatIden.Text
    '                    dr("Beneficiary_Nasabah_INDV_ID_NegaraBagian") = txtPenerimaNasabah_IND_negaraBagian.Text
    '                    If txtPenerimaNasabah_IND_negaraIden.Text <> "" Then
    '                        dr("Beneficiary_Nasabah_INDV_ID_Negara") = hfPenerimaNasabah_IND_negaraIden.Value
    '                    End If

    '                    dr("Beneficiary_Nasabah_INDV_ID_NegaraLainnya") = txtPenerimaNasabah_IND_negaraLainIden.Text
    '                    If CBOPenerimaNasabah_IND_JenisIden.SelectedIndex <> 0 Then
    '                        dr("Beneficiary_Nasabah_INDV_FK_IFTI_IDType") = CBOPenerimaNasabah_IND_JenisIden.SelectedValue
    '                    End If
    '                    dr("Beneficiary_Nasabah_INDV_NomorID") = txtPenerimaNasabah_IND_NomorIden.Text
    '                    dr("Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan") = txtPenerimaNasabah_IND_NilaiTransaksiKeuangan.Text

    '                    ReceiverDataTable.Rows.Add(dr)
    '                    txtPenerima_rekening.Text = ""
    '                    txtPenerimaNasabah_IND_nama.Text = ""
    '                    txtPenerimaNasabah_IND_TanggalLahir.Text = ""
    '                    RbPenerimaNasabah_IND_Warganegara.SelectedIndex = 0
    '                    hfPenerimaNasabah_IND_negara.Value = ""
    '                    txtPenerimaNasabah_IND_negaraLain.Text = ""
    '                    txtPenerimaNasabah_IND_alamatIden.Text = ""
    '                    txtPenerimaNasabah_IND_negaraBagian.Text = ""
    '                    hfPenerimaNasabah_IND_negaraIden.Value = ""
    '                    txtPenerimaNasabah_IND_negaraIden.Text = ""
    '                    txtPenerimaNasabah_IND_negaraLainIden.Text = ""
    '                    CBOPenerimaNasabah_IND_JenisIden.SelectedIndex = 0
    '                    txtPenerimaNasabah_IND_NomorIden.Text = ""
    '                    txtPenerimaNasabah_IND_NilaiTransaksiKeuangan.Text = ""
    '                Else
    '                    nasabahtype = 2
    '                    dr("FK_IFTI_NasabahType_ID") = nasabahtype

    '                    dr("Beneficiary_Nasabah_corp_NoRekening") = txtPenerima_rekening.Text

    '                    dr("Beneficiary_Nasabah_CORP_NamaKorporasi") = txtPenerimaNasabah_Korp_namaKorp.Text
    '                    dr("Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya") = txtPenerimaNasabah_Korp_BentukBadanUsahaLainnya.Text
    '                    If (CBOPenerimaNasabah_Korp_BentukBadanUsahaLain.SelectedIndex <> 0) Then
    '                        dr("Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id") = CInt(CBOPenerimaNasabah_Korp_BentukBadanUsahaLain.SelectedValue)
    '                    End If

    '                    If ObjectAntiNull(txtPenerimaNasabah_Korp_BidangUsahaKorp.Text) = True Then
    '                        dr("Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id") = hfPenerimaNasabah_Korp_BidangUsahaKorp.Value
    '                    End If

    '                    dr("Beneficiary_Nasabah_CORP_BidangUsahaLainnya") = txtPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Text
    '                    dr("Beneficiary_Nasabah_CORP_AlamatLengkap") = txtPenerimaNasabah_Korp_AlamatKorp.Text
    '                    If txtPenerimaNasabah_Korp_NegaraKorp.Text <> "" Then
    '                        dr("Beneficiary_Nasabah_CORP_ID_Negara") = hfPenerimaNasabah_Korp_NegaraKorp.Value
    '                    End If

    '                    dr("Beneficiary_Nasabah_CORP_ID_NegaraBagian") = txtPenerimaNasabah_Korp_negaraKota.Text
    '                    dr("Beneficiary_Nasabah_CORP_ID_NegaraLainnya") = txtPenerimaNasabah_Korp_NegaraLainnyaKorp.Text
    '                    dr("Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan") = txtPenerimaNasabah_Korp_NilaiTransaksiKeuangan.Text

    '                    ReceiverDataTable.Rows.Add(dr)

    '                    txtPenerima_rekening.Text = ""

    '                    txtPenerimaNasabah_Korp_namaKorp.Text = ""
    '                    txtPenerimaNasabah_Korp_BentukBadanUsahaLainnya.Text = ""
    '                    CBOPenerimaNasabah_Korp_BentukBadanUsahaLain.SelectedIndex = 0
    '                    hfPenerimaNasabah_Korp_BidangUsahaKorp.Value = Nothing
    '                    txtPenerimaNasabah_Korp_BidangUsahaKorp.Text = ""
    '                    txtPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Text = ""
    '                    txtPenerimaNasabah_Korp_AlamatKorp.Text = ""
    '                    hfPenerimaNasabah_Korp_NegaraKorp.Value = Nothing
    '                    txtPenerimaNasabah_Korp_NegaraKorp.Text = ""
    '                    txtPenerimaNasabah_Korp_negaraKota.Text = ""
    '                    txtPenerimaNasabah_Korp_NegaraLainnyaKorp.Text = ""
    '                    txtPenerimaNasabah_Korp_NilaiTransaksiKeuangan.Text = ""

    '                End If
    '            ElseIf tipepenerima = 2 Then
    '                nasabahtype = 3
    '                dr("FK_IFTI_NasabahType_ID") = nasabahtype

    '                dr("Beneficiary_NonNasabah_NoRekening") = txtPenerima_rekening.Text

    '                dr("Beneficiary_NonNasabah_NamaBank") = txtPenerimaNonNasabah_namabank.Text
    '                dr("Beneficiary_NonNasabah_NamaLengkap") = txtPenerimaNonNasabah_Nama.Text
    '                dr("Beneficiary_NonNasabah_ID_Alamat") = txtPenerimaNonNasabah_Alamat.Text
    '                If txtPenerimaNonNasabah_Negara.Text <> "" Then
    '                    dr("Beneficiary_NonNasabah_ID_Negara") = hfPenerimaNonNasabah_Negara.Value
    '                End If
    '                dr("Beneficiary_NonNasabah_ID_NegaraLainnya") = txtPenerimaNonNasabah_negaraLain.Text
    '                dr("Beneficiary_NonNasabah_NilaiTransaksikeuangan") = txtPenerimaNonNasabah_nilaiTransaksiKeuangan.Text

    '                ReceiverDataTable.Rows.Add(dr)
    '                txtPenerima_rekening.Text = ""

    '                txtPenerimaNonNasabah_namabank.Text = ""
    '                txtPenerimaNonNasabah_Nama.Text = ""
    '                txtPenerimaNonNasabah_Alamat.Text = ""
    '                hfPenerimaNonNasabah_Negara.Value = ""
    '                txtPenerimaNonNasabah_Negara.Text = ""
    '                txtPenerimaNonNasabah_negaraLain.Text = ""
    '                txtPenerimaNonNasabah_nilaiTransaksiKeuangan.Text = ""

    '            End If

    '            LoadReceiver()
    '        End If
    '    Catch ex As Exception
    '        Me.cvalPageErr.IsValid = False
    '        Me.cvalPageErr.ErrorMessage = ex.Message
    '    End Try
    'End Sub
    'Private Sub EditReceiver(ByVal index As Integer)
    '    Try
    '        If ValidateReceiver() Then
    '            Dim dr As Data.DataRow = Nothing

    '            dr = ReceiverDataTable.Rows(index)
    '            dr.BeginEdit()

    '            Dim tipepenerima As Integer = RBPenerimaNasabah_TipePengirim.SelectedValue

    '            Dim tipenasabah As Integer

    '            Dim nasabahtype As Integer

    '            dr("FK_IFTI_ID") = getIFTIPK

    '            If tipepenerima = 1 Then
    '                tipenasabah = Rb_IdenPenerimaNas_TipeNasabah.SelectedValue
    '                If tipenasabah = 1 Then
    '                    nasabahtype = 1
    '                    dr("FK_IFTI_NasabahType_ID") = nasabahtype

    '                    dr("Beneficiary_Nasabah_INDV_NoRekening") = txtPenerima_rekening.Text
    '                    dr("Beneficiary_Nasabah_INDV_NamaLengkap") = txtPenerimaNasabah_IND_nama.Text
    '                    If txtPenerimaNasabah_IND_TanggalLahir.Text.Trim() <> "" Then
    '                        dr("Beneficiary_Nasabah_INDV_TanggalLahir") = txtPenerimaNasabah_IND_TanggalLahir.Text.Trim() 'Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", txtPenerimaNasabah_IND_TanggalLahir.Text.Trim())
    '                    Else
    '                        dr("Beneficiary_Nasabah_INDV_TanggalLahir") = DBNull.Value
    '                    End If

    '                    dr("Beneficiary_Nasabah_INDV_KewargaNegaraan") = RbPenerimaNasabah_IND_Warganegara.SelectedValue
    '                    If RbPenerimaNasabah_IND_Warganegara.SelectedValue = 2 Then
    '                        dr("Beneficiary_Nasabah_INDV_Negara") = hfPenerimaNasabah_IND_negara.Value
    '                        dr("Beneficiary_Nasabah_INDV_NegaraLainnya") = txtPenerimaNasabah_IND_negaraLain.Text
    '                    Else
    '                        dr("Beneficiary_Nasabah_INDV_Negara") = DBNull.Value
    '                        dr("Beneficiary_Nasabah_INDV_NegaraLainnya") = ""
    '                    End If
    '                    dr("Beneficiary_Nasabah_INDV_NegaraLainnya") = txtPenerimaNasabah_IND_negaraLain.Text
    '                    dr("Beneficiary_Nasabah_INDV_ID_Alamat_Iden") = txtPenerimaNasabah_IND_alamatIden.Text
    '                    dr("Beneficiary_Nasabah_INDV_ID_NegaraBagian") = txtPenerimaNasabah_IND_negaraBagian.Text
    '                    dr("Beneficiary_Nasabah_INDV_ID_Negara") = hfPenerimaNasabah_IND_negaraIden.Value
    '                    dr("Beneficiary_Nasabah_INDV_ID_NegaraLainnya") = txtPenerimaNasabah_IND_negaraLainIden.Text
    '                    If CBOPenerimaNasabah_IND_JenisIden.SelectedIndex <> 0 Then
    '                        dr("Beneficiary_Nasabah_INDV_FK_IFTI_IDType") = CBOPenerimaNasabah_IND_JenisIden.SelectedValue
    '                    Else
    '                        dr("Beneficiary_Nasabah_INDV_FK_IFTI_IDType") = DBNull.Value
    '                    End If

    '                    dr("Beneficiary_Nasabah_INDV_NomorID") = txtPenerimaNasabah_IND_NomorIden.Text
    '                    dr("Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan") = txtPenerimaNasabah_IND_NilaiTransaksiKeuangan.Text

    '                    ReceiverDataTable.AcceptChanges()
    '                    txtPenerima_rekening.Text = ""
    '                    txtPenerimaNasabah_IND_nama.Text = ""
    '                    txtPenerimaNasabah_IND_TanggalLahir.Text = ""
    '                    RbPenerimaNasabah_IND_Warganegara.SelectedIndex = 0
    '                    hfPenerimaNasabah_IND_negara.Value = ""
    '                    txtPenerimaNasabah_IND_negara.Text = ""
    '                    txtPenerimaNasabah_IND_negaraLain.Text = ""
    '                    txtPenerimaNasabah_IND_alamatIden.Text = ""
    '                    txtPenerimaNasabah_IND_negaraBagian.Text = ""
    '                    hfPenerimaNasabah_IND_negaraIden.Value = ""
    '                    txtPenerimaNasabah_IND_negaraIden.Text = ""
    '                    txtPenerimaNasabah_IND_negaraLainIden.Text = ""
    '                    CBOPenerimaNasabah_IND_JenisIden.SelectedIndex = 0
    '                    txtPenerimaNasabah_IND_NomorIden.Text = ""
    '                    txtPenerimaNasabah_IND_NilaiTransaksiKeuangan.Text = ""
    '                Else
    '                    nasabahtype = 2
    '                    dr("FK_IFTI_NasabahType_ID") = nasabahtype

    '                    dr("Beneficiary_Nasabah_INDV_NoRekening") = txtPenerima_rekening.Text

    '                    dr("Beneficiary_Nasabah_CORP_NamaKorporasi") = txtPenerimaNasabah_Korp_namaKorp.Text
    '                    dr("Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya") = txtPenerimaNasabah_Korp_BentukBadanUsahaLainnya.Text
    '                    If CBOPenerimaNasabah_Korp_BentukBadanUsahaLain.SelectedIndex > 0 Then
    '                        dr("Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id") = CBOPenerimaNasabah_Korp_BentukBadanUsahaLain.SelectedValue
    '                    End If
    '                    If ObjectAntiNull(hfPenerimaNasabah_Korp_BidangUsahaKorp.Value) = True Then
    '                        dr("Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id") = hfPenerimaNasabah_Korp_BidangUsahaKorp.Value
    '                    Else
    '                        dr("Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id") = DBNull.Value
    '                    End If
    '                    dr("Beneficiary_Nasabah_CORP_BidangUsahaLainnya") = txtPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Text
    '                    dr("Beneficiary_Nasabah_CORP_AlamatLengkap") = txtPenerimaNasabah_Korp_AlamatKorp.Text
    '                    If ObjectAntiNull(hfPenerimaNasabah_Korp_NegaraKorp.Value) = True Then
    '                        dr("Beneficiary_Nasabah_CORP_ID_Negara") = hfPenerimaNasabah_Korp_NegaraKorp.Value
    '                    Else
    '                        dr("Beneficiary_Nasabah_CORP_ID_Negara") = DBNull.Value
    '                    End If

    '                    dr("Beneficiary_Nasabah_CORP_ID_NegaraBagian") = txtPenerimaNasabah_Korp_negaraKota.Text
    '                    dr("Beneficiary_Nasabah_CORP_ID_NegaraLainnya") = txtPenerimaNasabah_Korp_NegaraLainnyaKorp.Text
    '                    dr("Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan") = txtPenerimaNasabah_Korp_NilaiTransaksiKeuangan.Text

    '                    ReceiverDataTable.AcceptChanges()

    '                    txtPenerima_rekening.Text = ""

    '                    txtPenerimaNasabah_Korp_namaKorp.Text = ""
    '                    txtPenerimaNasabah_Korp_BentukBadanUsahaLainnya.Text = ""
    '                    CBOPenerimaNasabah_Korp_BentukBadanUsahaLain.SelectedIndex = 0
    '                    hfPenerimaNasabah_Korp_BidangUsahaKorp.Value = ""
    '                    txtPenerimaNasabah_Korp_BidangUsahaKorp.Text = ""
    '                    txtPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Text = ""
    '                    txtPenerimaNasabah_Korp_AlamatKorp.Text = ""
    '                    hfPenerimaNasabah_Korp_NegaraKorp.Value = ""
    '                    txtPenerimaNasabah_Korp_NegaraKorp.Text = ""
    '                    txtPenerimaNasabah_Korp_negaraKota.Text = ""
    '                    txtPenerimaNasabah_Korp_NegaraLainnyaKorp.Text = ""
    '                    txtPenerimaNasabah_Korp_NilaiTransaksiKeuangan.Text = ""
    '                End If
    '            ElseIf tipepenerima = 2 Then
    '                nasabahtype = 3
    '                dr("FK_IFTI_NasabahType_ID") = nasabahtype

    '                dr("Beneficiary_Nasabah_INDV_NoRekening") = txtPenerima_rekening.Text

    '                dr("Beneficiary_NonNasabah_NamaBank") = txtPenerimaNonNasabah_namabank.Text
    '                dr("Beneficiary_NonNasabah_NamaLengkap") = txtPenerimaNonNasabah_Nama.Text
    '                dr("Beneficiary_NonNasabah_ID_Alamat") = txtPenerimaNonNasabah_Alamat.Text
    '                If ObjectAntiNull(txtPenerimaNonNasabah_Negara.Text) = True Then
    '                    dr("Beneficiary_NonNasabah_ID_Negara") = hfPenerimaNonNasabah_Negara.Value
    '                Else
    '                    dr("Beneficiary_NonNasabah_ID_Negara") = DBNull.Value
    '                End If

    '                dr("Beneficiary_NonNasabah_ID_NegaraLainnya") = txtPenerimaNonNasabah_negaraLain.Text
    '                dr("Beneficiary_NonNasabah_NilaiTransaksikeuangan") = txtPenerimaNonNasabah_nilaiTransaksiKeuangan.Text

    '                ReceiverDataTable.AcceptChanges()
    '                txtPenerima_rekening.Text = ""

    '                txtPenerimaNonNasabah_namabank.Text = ""
    '                txtPenerimaNonNasabah_Nama.Text = ""
    '                txtPenerimaNonNasabah_Alamat.Text = ""
    '                hfPenerimaNonNasabah_Negara.Value = ""
    '                txtPenerimaNonNasabah_Negara.Text = ""
    '                txtPenerimaNonNasabah_negaraLain.Text = ""
    '                txtPenerimaNonNasabah_nilaiTransaksiKeuangan.Text = ""
    '            End If

    '            ReceiverDataTable.AcceptChanges()

    '            GridDataView.Enabled = True
    '            getIFTIBeneficiaryTempPK = Nothing
    '            LoadReceiver()
    '        End If
    '    Catch ex As Exception
    '        Me.cvalPageErr.IsValid = False
    '        Me.cvalPageErr.ErrorMessage = ex.Message
    '    End Try
    'End Sub
#End Region
#Region "Event"
    '#Region "Browse"
    '    Protected Sub imgButtonPengirimNasnegara_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButtonPengirimNasnegara.Click
    '        Try
    '            If Session("PickerNegara.Data") IsNot Nothing Then
    '                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
    '                TxtIdenPengirimNas_Ind_negara.Text = strData(1)
    '                Me.hfIdenPengirimNas_Ind_negara.Value = strData(0)
    '            End If
    '        Catch ex As Exception
    '            'LogError(ex)
    '            Me.cvalPageErr.IsValid = False
    '            Me.cvalPageErr.ErrorMessage = ex.Message
    '        End Try
    '    End Sub

    '    Protected Sub imgButton_PengirimPenerus_IND_Negara_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_PengirimPenerus_IND_Negara.Click
    '        Try
    '            If Session("PickerNegara.Data") IsNot Nothing Then
    '                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
    '                TxtPengirimPenerus_IND_negara.Text = strData(1)
    '                hfPengirimPenerus_IND_negara.Value = strData(0)
    '            End If
    '        Catch ex As Exception
    '            'LogError(ex)
    '            Me.cvalPageErr.IsValid = False
    '            Me.cvalPageErr.ErrorMessage = ex.Message
    '        End Try
    '    End Sub


    '    Protected Sub imgButton_PenerimaNas_IND_negara_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_PenerimaNasabah_IND_negara.Click
    '        Try
    '            If Session("PickerNegara.Data") IsNot Nothing Then
    '                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
    '                txtPenerimaNasabah_IND_negara.Text = strData(1)
    '                hfPenerimaNasabah_IND_negara.Value = strData(0)
    '            End If
    '        Catch ex As Exception
    '            'LogError(ex)
    '            Me.cvalPageErr.IsValid = False
    '            Me.cvalPageErr.ErrorMessage = ex.Message
    '        End Try
    '    End Sub


    '    'Protected Sub imgButton_PenerimaNasabah_IND_negaraIden_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_PenerimaNasabah_IND_negaraIden.Click
    '    '    Try
    '    '        If Session("PickerNegara.Data") IsNot Nothing Then
    '    '            Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
    '    '            txtPenerimaNasabah_IND_negaraIden.Text = strData(1)
    '    '            hfPenerimaNasabah_IND_negaraIden.Value = strData(0)
    '    '        End If
    '    '    Catch ex As Exception
    '    '        'LogError(ex)
    '    '        Me.cvalPageErr.IsValid = False
    '    '        Me.cvalPageErr.ErrorMessage = ex.Message
    '    '    End Try
    '    'End Sub

    '    Protected Sub ImageButton27_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton27.Click
    '        Try
    '            If Session("PickerNegara.Data") IsNot Nothing Then
    '                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
    '                txtPenerimaNonNasabah_Negara.Text = strData(1)
    '                hfPenerimaNonNasabah_Negara.Value = strData(0)
    '            End If
    '        Catch ex As Exception
    '            'LogError(ex)
    '            Me.cvalPageErr.IsValid = False
    '            Me.cvalPageErr.ErrorMessage = ex.Message
    '        End Try
    '    End Sub

    '    Protected Sub imgButtonPengirimNasKotaDom_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButtonPengirimNasKotaDom.Click
    '        Try
    '            If Session("PickerKotaKab.Data") IsNot Nothing Then
    '                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
    '                TxtIdenPengirimNas_Ind_Kota.Text = strData(1)
    '                hfIdenPengirimNas_Ind_Kota_dom.Value = strData(0)
    '                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.KabupatenKota)
    '            End If
    '        Catch ex As Exception
    '            'LogError(ex)
    '            Me.cvalPageErr.IsValid = False
    '            Me.cvalPageErr.ErrorMessage = ex.Message
    '        End Try
    '    End Sub

    '    Protected Sub imgButtonPengirimNasKotaIden_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButtonPengirimNasKotaIden.Click
    '        Try
    '            If Session("PickerKotaKab.Data") IsNot Nothing Then
    '                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
    '                TxtIdenPengirimNas_Ind_kotaIdentitas.Text = strData(1)
    '                hfIdenPengirimNas_Ind_kotaIdentitas.Value = strData(0)
    '                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.KabupatenKota)
    '            End If
    '        Catch ex As Exception
    '            'LogError(ex)
    '            Me.cvalPageErr.IsValid = False
    '            Me.cvalPageErr.ErrorMessage = ex.Message
    '        End Try

    '    End Sub

    '    Protected Sub imgButtonPengirimNasKorp_kotaLengkap_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButtonPengirimNasKorp_kotaLengkap.Click

    '        Try
    '            If Session("PickerKotaKab.Data") IsNot Nothing Then
    '                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
    '                TxtIdenPengirimNas_Corp_kotakorp.Text = strData(1)
    '                hfIdenPengirimNas_Corp_kotakorp.Value = strData(0)
    '                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.KabupatenKota)
    '            End If
    '        Catch ex As Exception
    '            'LogError(ex)
    '            Me.cvalPageErr.IsValid = False
    '            Me.cvalPageErr.ErrorMessage = ex.Message
    '        End Try
    '    End Sub


    '    Protected Sub imgButton_PengirimNonNas_Kota_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_PengirimNonNas_Kota.Click

    '        Try
    '            If Session("PickerKotaKab.Data") IsNot Nothing Then
    '                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
    '                TxtPengirimNonNasabah_kotaIden.Text = strData(1)
    '                hfPengirimNonNasabah_kotaIden.Value = strData(0)
    '                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.KabupatenKota)
    '            End If
    '        Catch ex As Exception
    '            'LogError(ex)
    '            Me.cvalPageErr.IsValid = False
    '            Me.cvalPageErr.ErrorMessage = ex.Message
    '        End Try
    '    End Sub

    '    Protected Sub imgButton_PengirimPenerus_IND_KotaDom_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_PengirimPenerus_IND_KotaDom.Click
    '        Try
    '            If Session("PickerKotaKab.Data") IsNot Nothing Then
    '                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
    '                TxtPengirimPenerus_IND_kotaDom.Text = strData(1)
    '                hfPengirimPenerus_IND_kotaDom.Value = strData(0)
    '                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.KabupatenKota)
    '            End If
    '        Catch ex As Exception
    '            'LogError(ex)
    '            Me.cvalPageErr.IsValid = False
    '            Me.cvalPageErr.ErrorMessage = ex.Message
    '        End Try
    '    End Sub


    '    Protected Sub imgButton_PengirimPenerus_IND_KotaIden_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_PengirimPenerus_IND_KotaIden.Click

    '        Try
    '            If Session("PickerKotaKab.Data") IsNot Nothing Then
    '                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
    '                TxtPengirimPenerus_IND_kotaIden.Text = strData(1)
    '                hfPengirimPenerus_IND_kotaIden.Value = strData(0)
    '                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.KabupatenKota)
    '            End If
    '        Catch ex As Exception
    '            'LogError(ex)
    '            Me.cvalPageErr.IsValid = False
    '            Me.cvalPageErr.ErrorMessage = ex.Message
    '        End Try
    '    End Sub

    '    Protected Sub imgButton_PengirimPenerus_Korp_KotaLengkap_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_PengirimPenerus_Korp_KotaLengkap.Click
    '        Try
    '            If Session("PickerKotaKab.Data") IsNot Nothing Then
    '                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
    '                txtPengirimPenerus_Korp_kotakorp.Text = strData(1)
    '                hfPengirimPenerus_Korp_kotakorp.Value = strData(0)
    '                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.KabupatenKota)
    '            End If
    '        Catch ex As Exception
    '            'LogError(ex)
    '            Me.cvalPageErr.IsValid = False
    '            Me.cvalPageErr.ErrorMessage = ex.Message
    '        End Try
    '    End Sub

    '    Protected Sub imgButton_BOwnerNas_KotaIden_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_BOwnerNas_KotaIden.Click

    '        Try
    '            If Session("PickerKotaKab.Data") IsNot Nothing Then
    '                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
    '                BenfOwnerNasabah_KotaIden.Text = strData(1)
    '                hfBenfOwnerNasabah_KotaIden.Value = strData(0)
    '                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.KabupatenKota)
    '            End If
    '        Catch ex As Exception
    '            'LogError(ex)
    '            Me.cvalPageErr.IsValid = False
    '            Me.cvalPageErr.ErrorMessage = ex.Message
    '        End Try
    '    End Sub

    '    Protected Sub imgButton_BOwnerNonNas_kota_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_BOwnerNonNas_kota.Click

    '        Try
    '            If Session("PickerKotaKab.Data") IsNot Nothing Then
    '                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
    '                BenfOwnerNonNasabah_KotaIden.Text = strData(1)
    '                hfBenfOwnerNonNasabah_KotaIden.Value = strData(0)
    '                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.KabupatenKota)
    '            End If
    '        Catch ex As Exception
    '            'LogError(ex)
    '            Me.cvalPageErr.IsValid = False
    '            Me.cvalPageErr.ErrorMessage = ex.Message
    '        End Try
    '    End Sub

    '    Protected Sub imgButtonPengirimNasProvinsiDom_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButtonPengirimNasProvinsiDom.Click
    '        Try
    '            If Session("PickerProvinsi.Data") IsNot Nothing Then
    '                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
    '                TxtIdenPengirimNas_Ind_provinsi.Text = strData(1)
    '                hfIdenPengirimNas_Ind_provinsi_dom.Value = strData(0)
    '                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.Propinsi)
    '            End If
    '        Catch ex As Exception
    '            'LogError(ex)
    '            Me.cvalPageErr.IsValid = False
    '            Me.cvalPageErr.ErrorMessage = ex.Message
    '        End Try
    '    End Sub

    '    Protected Sub imgButtonPengirimNasProvinsiIden_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButtonPengirimNasProvinsiIden.Click

    '        Try
    '            If Session("PickerProvinsi.Data") IsNot Nothing Then
    '                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
    '                TxtIdenPengirimNas_Ind_ProvinsiIden.Text = strData(1)
    '                hfIdenPengirimNas_Ind_ProvinsiIden.Value = strData(0)
    '                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.Propinsi)
    '            End If
    '        Catch ex As Exception
    '            'LogError(ex)
    '            Me.cvalPageErr.IsValid = False
    '            Me.cvalPageErr.ErrorMessage = ex.Message
    '        End Try
    '    End Sub

    '    Protected Sub imgButtonPengirimNasKorp_ProvinsiLengkap_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButtonPengirimNasKorp_ProvinsiLengkap.Click

    '        Try
    '            If Session("PickerProvinsi.Data") IsNot Nothing Then
    '                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
    '                TxtIdenPengirimNas_Corp_provKorp.Text = strData(1)
    '                hfIdenPengirimNas_Corp_provKorp.Value = strData(0)
    '                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.Propinsi)
    '            End If
    '        Catch ex As Exception
    '            'LogError(ex)
    '            Me.cvalPageErr.IsValid = False
    '            Me.cvalPageErr.ErrorMessage = ex.Message
    '        End Try

    '    End Sub

    '    Protected Sub imgButton_PengirimNonNas__Provinsi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_PengirimNonNas__Provinsi.Click

    '        Try
    '            If Session("PickerProvinsi.Data") IsNot Nothing Then
    '                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
    '                TxtPengirimNonNasabah_ProvIden.Text = strData(1)
    '                hfPengirimNonNasabah_ProvIden.Value = strData(0)
    '                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.Propinsi)
    '            End If
    '        Catch ex As Exception
    '            'LogError(ex)
    '            Me.cvalPageErr.IsValid = False
    '            Me.cvalPageErr.ErrorMessage = ex.Message
    '        End Try
    '    End Sub

    '    Protected Sub imgButton_PengirimPenerus_IND_ProvinsiDom_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_PengirimPenerus_IND_ProvinsiDom.Click

    '        Try
    '            If Session("PickerProvinsi.Data") IsNot Nothing Then
    '                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
    '                TxtPengirimPenerus_IND_ProvDom.Text = strData(1)
    '                hfPengirimPenerus_IND_ProvDom.Value = strData(0)
    '                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.Propinsi)
    '            End If
    '        Catch ex As Exception
    '            'LogError(ex)
    '            Me.cvalPageErr.IsValid = False
    '            Me.cvalPageErr.ErrorMessage = ex.Message
    '        End Try
    '    End Sub

    '    Protected Sub imgButton_PengirimPenerus_IND_ProvinsiIden_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_PengirimPenerus_IND_ProvinsiIden.Click
    '        Try
    '            If Session("PickerProvinsi.Data") IsNot Nothing Then
    '                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
    '                TxtPengirimPenerus_IND_ProvIden.Text = strData(1)
    '                hfPengirimPenerus_IND_ProvIden.Value = strData(0)
    '                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.Propinsi)
    '            End If
    '        Catch ex As Exception
    '            'LogError(ex)
    '            Me.cvalPageErr.IsValid = False
    '            Me.cvalPageErr.ErrorMessage = ex.Message
    '        End Try
    '    End Sub

    '    Protected Sub imgButton_PengirimPenerus_Korp_ProvinsiLengkap_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_PengirimPenerus_Korp_ProvinsiLengkap.Click

    '        Try
    '            If Session("PickerProvinsi.Data") IsNot Nothing Then
    '                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
    '                txtPengirimPenerus_Korp_ProvinsiKorp.Text = strData(1)
    '                hfPengirimPenerus_Korp_ProvinsiKorp.Value = strData(0)
    '                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.Propinsi)
    '            End If
    '        Catch ex As Exception
    '            'LogError(ex)
    '            Me.cvalPageErr.IsValid = False
    '            Me.cvalPageErr.ErrorMessage = ex.Message
    '        End Try
    '    End Sub

    '    Protected Sub imgButton_BOwnerNas_ProvinsiIden_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_BOwnerNas_ProvinsiIden.Click

    '        Try
    '            If Session("PickerProvinsi.Data") IsNot Nothing Then
    '                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
    '                BenfOwnerNasabah_ProvinsiIden.Text = strData(1)
    '                hfBenfOwnerNasabah_ProvinsiIden.Value = strData(0)
    '                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.Propinsi)
    '            End If
    '        Catch ex As Exception
    '            'LogError(ex)
    '            Me.cvalPageErr.IsValid = False
    '            Me.cvalPageErr.ErrorMessage = ex.Message
    '        End Try
    '    End Sub

    '    Protected Sub imgButton_BOwnerNonNas_Provinsi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_BOwnerNonNas_Provinsi.Click

    '        Try
    '            If Session("PickerProvinsi.Data") IsNot Nothing Then
    '                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
    '                BenfOwnerNonNasabah_ProvinsiIden.Text = strData(1)
    '                hfBenfOwnerNonNasabah_ProvinsiIden.Value = strData(0)
    '                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.Propinsi)
    '            End If
    '        Catch ex As Exception
    '            'LogError(ex)
    '            Me.cvalPageErr.IsValid = False
    '            Me.cvalPageErr.ErrorMessage = ex.Message
    '        End Try
    '    End Sub

    '    Protected Sub imgButtonPengirimNasPekerjaan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButtonPengirimNasPekerjaan.Click
    '        Try
    '            If Session("PickerPekerjaan.Data") IsNot Nothing Then
    '                Dim strData() As String = CStr(Session("PickerPekerjaan.Data")).Split(";")
    '                TxtIdenPengirimNas_Ind_pekerjaan.Text = strData(1)
    '                hfIdenPengirimNas_Ind_pekerjaan.Value = strData(0)
    '            End If
    '        Catch ex As Exception
    '            ' LogError(ex)
    '            Me.cvalPageErr.IsValid = False
    '            Me.cvalPageErr.ErrorMessage = ex.Message
    '        End Try
    '    End Sub

    '    Protected Sub imgButton_PengirimPenerus_IND_Pekerjaan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_PengirimPenerus_IND_Pekerjaan.Click

    '        Try
    '            If Session("PickerPekerjaan.Data") IsNot Nothing Then
    '                Dim strData() As String = CStr(Session("PickerPekerjaan.Data")).Split(";")
    '                TxtPengirimPenerus_IND_pekerjaan.Text = strData(1)
    '                hfPengirimPenerus_IND_pekerjaan.Value = strData(0)
    '            End If
    '        Catch ex As Exception
    '            ' LogError(ex)
    '            Me.cvalPageErr.IsValid = False
    '            Me.cvalPageErr.ErrorMessage = ex.Message
    '        End Try
    '    End Sub

    '    Protected Sub imgButton_PenerimaNasabah_IND_negaraIden_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_PenerimaNasabah_IND_negaraIden.Click
    '        Try
    '            If Session("PickerNegara.Data") IsNot Nothing Then
    '                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
    '                txtPenerimaNasabah_IND_negaraIden.Text = strData(1)
    '                hfPenerimaNasabah_IND_negaraIden.Value = strData(0)
    '            End If
    '        Catch ex As Exception
    '            'LogError(ex)
    '            Me.cvalPageErr.IsValid = False
    '            Me.cvalPageErr.ErrorMessage = ex.Message
    '        End Try
    '    End Sub
    '    Protected Sub imgButtonPengirimNasCorp_BidangUsaha_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButtonPengirimNasCorp_BidangUsaha.Click
    '        Try
    '            If Session("PickerBidangUsaha.Data") IsNot Nothing Then
    '                Dim strData() As String = CStr(Session("PickerBidangUsaha.Data")).Split(";")
    '                TxtIdenPengirimNas_Corp_BidangUsahaKorp.Text = strData(1)
    '                hfIdenPengirimNas_Corp_BidangUsahaKorp.Value = strData(0)
    '            End If
    '        Catch ex As Exception
    '            'LogError(ex)
    '            Me.cvalPageErr.IsValid = False
    '            Me.cvalPageErr.ErrorMessage = ex.Message
    '        End Try
    '    End Sub

    '    Protected Sub imgButton_PengirimPenerus_Korp_bidangUsaha_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_PengirimPenerus_Korp_bidangUsaha.Click

    '        Try
    '            If Session("PickerBidangUsaha.Data") IsNot Nothing Then
    '                Dim strData() As String = CStr(Session("PickerBidangUsaha.Data")).Split(";")
    '                txtPengirimPenerus_Korp_BidangUsahaKorp.Text = strData(1)
    '                hfPengirimPenerus_Korp_BidangUsahaKorp.Value = strData(0)
    '            End If
    '        Catch ex As Exception
    '            'LogError(ex)
    '            Me.cvalPageErr.IsValid = False
    '            Me.cvalPageErr.ErrorMessage = ex.Message
    '        End Try
    '    End Sub

    '    Protected Sub imgButton_PenerimaNas_Korp_bidangUsaha_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_PenerimaNas_Korp_bidangUsaha.Click
    '        Try
    '            If Session("PickerBidangUsaha.Data") IsNot Nothing Then
    '                Dim strData() As String = CStr(Session("PickerBidangUsaha.Data")).Split(";")
    '                txtPenerimaNasabah_Korp_BidangUsahaKorp.Text = strData(1)
    '                hfPenerimaNasabah_Korp_BidangUsahaKorp.Value = strData(0)
    '            End If
    '        Catch ex As Exception
    '            'LogError(ex)
    '            Me.cvalPageErr.IsValid = False
    '            Me.cvalPageErr.ErrorMessage = ex.Message
    '        End Try
    '    End Sub
    '    Protected Sub imgButton_PenerimaNasabah_Korp_NegaraKorp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_PenerimaNasabah_Korp_NegaraKorp.Click
    '        Try
    '            If Session("PickerNegara.Data") IsNot Nothing Then
    '                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
    '                txtPenerimaNasabah_Korp_NegaraKorp.Text = strData(1)
    '                Me.hfPenerimaNasabah_Korp_NegaraKorp.Value = strData(0)
    '            End If
    '        Catch ex As Exception
    '            'LogError(ex)
    '            Me.cvalPageErr.IsValid = False
    '            Me.cvalPageErr.ErrorMessage = ex.Message
    '        End Try
    '    End Sub
    '    Protected Sub ImageButton_Transaksi_currency_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton_Transaksi_currency.Click
    '        Try
    '            If Session("PickerMataUang.Data") IsNot Nothing Then
    '                Dim strData() As String = CStr(Session("PickerMataUang.Data")).Split(";")
    '                Transaksi_currency.Text = strData(0) & " - " & strData(1)
    '                hfTransaksi_currency.Value = strData(0)
    '                Using objMatauang As TList(Of MsCurrencyRate) = DataRepository.MsCurrencyRateProvider.GetPaged("Code = '" & strData(0).Trim & "'", "CurrencyRateDate desc", 0, 1, 0)
    '                    If objMatauang.Count > 0 Then
    '                        Transaksi_currency.Text = ValidateBLL.FormatMoneyWithComma(objMatauang(0).CurrencyRate)
    '                    End If
    '                End Using
    '            End If
    '        Catch ex As Exception
    '            'LogError(ex)
    '            Me.cvalPageErr.IsValid = False
    '            Me.cvalPageErr.ErrorMessage = ex.Message
    '        End Try
    '    End Sub

    '    Protected Sub ImageButton_Transaksi_MataUangTransaksi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton_Transaksi_MataUangTransaksi.Click
    '        Try
    '            If Session("PickerMataUang.Data") IsNot Nothing Then
    '                Dim strData() As String = CStr(Session("PickerMataUang.Data")).Split(";")
    '                Transaksi_MataUangTransaksi.Text = strData(0) & " - " & strData(1)
    '                hfTransaksi_MataUangTransaksi.Value = strData(0)
    '                Using objMatauang As TList(Of MsCurrencyRate) = DataRepository.MsCurrencyRateProvider.GetPaged("Code = '" & strData(0).Trim & "'", "CurrencyRateDate desc", 0, 1, 0)
    '                    If objMatauang.Count > 0 Then
    '                        Transaksi_MataUangTransaksi.Text = ValidateBLL.FormatMoneyWithComma(objMatauang(0).CurrencyRate)
    '                    End If
    '                End Using
    '            End If
    '        Catch ex As Exception
    '            'LogError(ex)
    '            Me.cvalPageErr.IsValid = False
    '            Me.cvalPageErr.ErrorMessage = ex.Message
    '        End Try
    '    End Sub

    '#End Region
#Region "Radio Button"
    Protected Sub RBPenerimaNasabah_TipePengirim_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RBPenerimaNasabah_TipePengirim.SelectedIndexChanged
        'MultiViewPenerimaNasabah.ActiveViewIndex = CInt(Me.RBPenerimaNasabah_TipePengirim.SelectedValue) - 1
        If RBPenerimaNasabah_TipePengirim.SelectedValue = "1" Then
            Me.trSwiftOutPenerimaTipeNasabah.Visible = True
            ' Me.MultiViewPenerimaNasabah.ActiveViewIndex = 0
        Else
            Me.trSwiftOutPenerimaTipeNasabah.Visible = False
            Me.MultiViewSwiftOutIdenPenerima.ActiveViewIndex = 1
        End If
    End Sub
    Protected Sub Rb_IdenPengirimNas_TipePengirim_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Rb_IdenPengirimNas_TipePengirim.SelectedIndexChanged
        If Me.Rb_IdenPengirimNas_TipePengirim.SelectedValue = 1 Then
            Me.trTipeNasabah.Visible = True
        Else
            Me.trTipeNasabah.Visible = False
            Me.MultiViewSwiftOUtIdentitasPengirim.ActiveViewIndex = 0
            Me.MultiViewSwiftOutJenisNasabah.ActiveViewIndex = 1
        End If
    End Sub
    Protected Sub Rb_IdenPengirimNas_TipeNasabah_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Rb_IdenPengirimNas_TipeNasabah.SelectedIndexChanged
        If Me.Rb_IdenPengirimNas_TipeNasabah.SelectedValue = 1 Then
            Me.MultiViewSwiftOUtIdentitasPengirim.ActiveViewIndex = 0
            Me.MultiViewSwiftOutJenisNasabah.ActiveViewIndex = 0
            Me.MultiViewPengirimNasabah.ActiveViewIndex = 0
        Else
            Me.MultiViewSwiftOUtIdentitasPengirim.ActiveViewIndex = 0
            Me.MultiViewSwiftOutJenisNasabah.ActiveViewIndex = 0
            Me.MultiViewPengirimNasabah.ActiveViewIndex = 1
        End If
        If Me.CboSwiftOutPengirim.SelectedValue = 2 Then
            If Me.Rb_IdenPengirimNas_TipeNasabah.SelectedValue = 1 Then
                Me.MultiViewPengirimPenerus.ActiveViewIndex = 0
                Me.MultiViewSwiftOUtIdentitasPengirim.ActiveViewIndex = 1
            Else
                Me.MultiViewPengirimPenerus.ActiveViewIndex = 1
                Me.MultiViewSwiftOUtIdentitasPengirim.ActiveViewIndex = 1
            End If
        End If
    End Sub

    Protected Sub Rb_BOwnerNas_TipePengirim_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Rb_BOwnerNas_TipePengirim.SelectedIndexChanged
        If Rb_BOwnerNas_TipePengirim.SelectedValue = 1 Then
            Me.MultiViewSwiftOutBOwner.ActiveViewIndex = 0
        Else
            Me.MultiViewSwiftOutBOwner.ActiveViewIndex = 1
        End If
    End Sub

    Protected Sub Rb_IdenPengirimNas_TipeNasabah1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Rb_IdenPenerimaNas_TipeNasabah.SelectedIndexChanged
        If Rb_IdenPenerimaNas_TipeNasabah.SelectedValue = 1 Then
            Me.MultiViewSwiftOutIdenPenerima.ActiveViewIndex = 0
            MultiViewPenerimaNasabah.ActiveViewIndex = 0
            RbPenerimaNasabah_IND_Warganegara.SelectedValue = 1
        Else
            Me.MultiViewSwiftOutIdenPenerima.ActiveViewIndex = 0
            MultiViewPenerimaNasabah.ActiveViewIndex = 1
        End If
    End Sub
    Protected Sub RbPenerimaNasabah_IND_Warganegara_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RbPenerimaNasabah_IND_Warganegara.SelectedIndexChanged
        If RbPenerimaNasabah_IND_Warganegara.SelectedValue = 1 Then
            Me.PenerimaIndNegara.Visible = False
            Me.PenerimaIndNegaraLain.Visible = False
        Else
            Me.PenerimaIndNegara.Visible = True
            Me.PenerimaIndNegaraLain.Visible = True
        End If
    End Sub

    Protected Sub RBPengirimPenerus_Ind_Kewarganegaraan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RBPengirimPenerus_Ind_Kewarganegaraan.SelectedIndexChanged
        If RBPengirimPenerus_Ind_Kewarganegaraan.SelectedValue = 1 Then
            Me.PengirimPenerusIndNegara.Visible = False
            Me.PengirimPenerusIndNegaraLain.Visible = False
        Else
            Me.PengirimPenerusIndNegara.Visible = True
            Me.PengirimPenerusIndNegaraLain.Visible = True
        End If
    End Sub

    Protected Sub Rb_IdenPengirimNas_Ind_kewarganegaraan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Rb_IdenPengirimNas_Ind_kewarganegaraan.SelectedIndexChanged
        If Rb_IdenPengirimNas_Ind_kewarganegaraan.SelectedValue = 1 Then
            Me.PengirimIndNegara.Visible = False
            Me.PengirimIndNegaraLain.Visible = False
        Else
            Me.PengirimIndNegara.Visible = True
            Me.PengirimIndNegaraLain.Visible = True
        End If
    End Sub

    'Protected Sub Rb_IdenPengirimNas_TipePengirim_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Rb_IdenNasabahNas_TipePengirim.SelectedIndexChanged
    '    ' Me.MultiViewPengirimNasabah.ActiveViewIndex = CInt(Me.Rb_IdenPengirimNas_TipePengirim.SelectedValue) - 1
    '    If Rb_IdenNasabahNas_TipePengirim.SelectedValue = "1" Then
    '        Me.MultiViewPengirimNasabah.ActiveViewIndex = 0
    '    Else
    '        Me.MultiViewPengirimNasabah.ActiveViewIndex = 1
    '    End If
    'End Sub

    'Protected Sub RbPengirimPenerus_TipePengirim_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RbPengirimPenerus_TipePengirim.SelectedIndexChanged
    '    'Me.MultiViewPengirimPenerus.ActiveViewIndex = CInt(Me.RBPenerimaNasabah_TipePengirim.SelectedValue) - 1
    '    If RbPengirimPenerus_TipePengirim.SelectedValue = "1" Then
    '        Me.MultiViewPengirimPenerus.ActiveViewIndex = 0
    '    Else
    '        Me.MultiViewPengirimPenerus.ActiveViewIndex = 1
    '    End If
    'End Sub


#End Region
#Region "combobox"
    Protected Sub CboSwiftOutPengirim_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CboSwiftOutPengirim.SelectedIndexChanged
        If Me.CboSwiftOutPengirim.SelectedValue = 1 Then
            Me.trTipePengirim.Visible = True
            Me.trTipeNasabah.Visible = False
        Else
            Me.trTipePengirim.Visible = False
            Me.trTipeNasabah.Visible = True
        End If
    End Sub


    'Protected Sub CboSwiftOutPengirim_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CboSwiftOutPengirim.TextChanged
    '    If Me.CboSwiftOutPengirim.SelectedValue = 1 Then
    '        Me.trTipePengirim.Visible = True
    '        Me.trTipeNasabah.Visible = False
    '    Else
    '        Me.trTipePengirim.Visible = False
    '        Me.trTipeNasabah.Visible = True
    '    End If
    'End Sub

#End Region
#Region "Button for receiver"
    Protected Sub ImageButton_CancelPenerimaNonNasabah_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton_CancelPenerimaNonNasabah.Click
        MultiViewSwiftOutIdenPenerima.Visible = False
        LoadReceiver()
        txtPenerima_rekening.Text = ""

        txtPenerimaNonNasabah_namabank.Text = ""
        txtPenerimaNonNasabah_Nama.Text = ""
        txtPenerimaNonNasabah_Alamat.Text = ""
        hfPenerimaNonNasabah_Negara.Value = ""
        txtPenerimaNonNasabah_Negara.Text = ""
        txtPenerimaNonNasabah_negaraLain.Text = ""
        txtPenerimaNonNasabah_nilaiTransaksiKeuangan.Text = ""
        GridDataView.Enabled = True
    End Sub

    Protected Sub ImageButton_CancelPenerimaKorp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton_CancelPenerimaKorp.Click
        MultiViewSwiftOutIdenPenerima.Visible = False
        LoadReceiver()
        txtPenerima_rekening.Text = ""

        txtPenerimaNasabah_Korp_namaKorp.Text = ""
        txtPenerimaNasabah_Korp_BentukBadanUsahaLainnya.Text = ""
        CBOPenerimaNasabah_Korp_BentukBadanUsahaLain.SelectedIndex = 0
        hfPenerimaNasabah_Korp_BidangUsahaKorp.Value = ""
        txtPenerimaNasabah_Korp_BidangUsahaKorp.Text = ""
        txtPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Text = ""
        txtPenerimaNasabah_Korp_AlamatKorp.Text = ""
        hfPenerimaNasabah_Korp_NegaraKorp.Value = ""
        txtPenerimaNasabah_Korp_NegaraKorp.Text = ""
        txtPenerimaNasabah_Korp_negaraKota.Text = ""
        txtPenerimaNasabah_Korp_NegaraLainnyaKorp.Text = ""
        txtPenerimaNasabah_Korp_NilaiTransaksiKeuangan.Text = ""
        GridDataView.Enabled = True
    End Sub

    Protected Sub ImageButton_CancelPenerimaInd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton_CancelPenerimaInd.Click
        MultiViewSwiftOutIdenPenerima.Visible = False
        LoadReceiver()
        txtPenerima_rekening.Text = ""
        txtPenerimaNasabah_IND_nama.Text = ""
        txtPenerimaNasabah_IND_TanggalLahir.Text = ""
        RbPenerimaNasabah_IND_Warganegara.SelectedIndex = 0
        hfPenerimaNasabah_IND_negara.Value = ""
        txtPenerimaNasabah_IND_negara.Text = ""
        txtPenerimaNasabah_IND_negaraLain.Text = ""
        txtPenerimaNasabah_IND_alamatIden.Text = ""
        txtPenerimaNasabah_IND_negaraBagian.Text = ""
        hfPenerimaNasabah_IND_negaraIden.Value = ""
        txtPenerimaNasabah_IND_negaraIden.Text = ""
        txtPenerimaNasabah_IND_negaraLainIden.Text = ""
        CBOPenerimaNasabah_IND_JenisIden.SelectedIndex = 0
        txtPenerimaNasabah_IND_NomorIden.Text = ""
        txtPenerimaNasabah_IND_NilaiTransaksiKeuangan.Text = ""
        GridDataView.Enabled = True
    End Sub

#End Region
    Protected Sub ImageButtonCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonCancel.Click
        Response.Redirect("IFTIView.aspx")
    End Sub

    Protected Sub cvalPageErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles cvalPageErr.PreRender
        If cvalPageErr.IsValid = False Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
        End If
    End Sub

    Protected Sub imgOKMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgOKMsg.Click
        Response.Redirect("IFTIView.aspx")
    End Sub


    Protected Sub GridDataView_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridDataView.DeleteCommand
        If e.Item.ItemType <> ListItemType.Header And e.Item.ItemType <> ListItemType.Footer Then
            Select Case e.CommandName
                Case "delete"
                    ReceiverDataTable.Rows.RemoveAt(e.Item.ItemIndex + (SetnGetCurrentPageReceiver * GetDisplayedTotalRow))

                    LoadReceiver()
            End Select
        End If
    End Sub


    Protected Sub GridDataView_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridDataView.EditCommand
        Dim PKIFTIBeneficiary As Integer
        Try
            PKIFTIBeneficiary = CInt(e.Item.Cells(4).Text)
            'getIFTIBeneficiaryTempPK = PKIFTIBeneficiary

            Me.tableTipePenerima.Visible = True
            MultiViewSwiftOutIdenPenerima.Visible = True
            Dim dr As Data.DataRow = ReceiverDataTable.Rows(e.Item.ItemIndex + (SetnGetCurrentPageReceiver * Sahassa.AML.Commonly.GetDisplayedTotalRow))
            getIFTIBeneficiaryTempPK = (e.Item.ItemIndex + (SetnGetCurrentPageReceiver * Sahassa.AML.Commonly.GetDisplayedTotalRow)) + 1


            'Me.txtPenerima_rekening.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_NoRekening

            ' cek ReceiverType
            Dim receiverType As Integer = dr("FK_IFTI_NasabahType_ID")
            Select Case (receiverType)
                Case 1
                    Me.RBPenerimaNasabah_TipePengirim.SelectedValue = 1
                    Me.trSwiftOutPenerimaTipeNasabah.Visible = True
                    Me.Rb_IdenPenerimaNas_TipeNasabah.SelectedValue = 1
                    Me.MultiViewSwiftOutIdenPenerima.ActiveViewIndex = 0
                    Me.MultiViewPenerimaNasabah.ActiveViewIndex = 0
                    FieldSwiftOutPenerimaNasabahPerorangan(PKIFTIBeneficiary)
                Case 2
                    Me.RBPenerimaNasabah_TipePengirim.SelectedValue = 1
                    Me.Rb_IdenPenerimaNas_TipeNasabah.SelectedValue = 2
                    Me.trSwiftOutPenerimaTipeNasabah.Visible = True
                    Me.MultiViewSwiftOutIdenPenerima.ActiveViewIndex = 0
                    Me.MultiViewPenerimaNasabah.ActiveViewIndex = 1
                    FieldSwiftOutPenerimaNasabahKorporasi(PKIFTIBeneficiary)
                Case 3
                    Me.RBPenerimaNasabah_TipePengirim.SelectedValue = 2
                    Me.trSwiftOutPenerimaTipeNasabah.Visible = False
                    Me.MultiViewSwiftOutIdenPenerima.ActiveViewIndex = 1
                    Me.trSwiftOutPenerimaTipeNasabah.Visible = False
                    FieldSwiftOutPenerimaNonNasabah(PKIFTIBeneficiary)
            End Select


            'GridDataView.Enabled = False
        Catch ex As Exception
            'LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    'Protected Sub GridDataView_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridDataView.ItemCommand
    '    If e.CommandName.ToLower = "detail" Then
    '        Dim PKIFTIBeneficiary As Integer
    '        Try
    '            PKIFTIBeneficiary = CInt(e.Item.Cells(1).Text)
    '            getIFTIBeneficiaryTempPK = PKIFTIBeneficiary
    '            Me.tableTipePenerima.Visible = True

    '            'Identitas Penerima
    '            Using objIftiBeneficiary As TList(Of IFTI_Beneficiary_Temporary) = DataRepository.IFTI_Beneficiary_TemporaryProvider.GetPaged("PK_IFTI_Beneficiary_Temporary_ID = " & PKIFTIBeneficiary, "", 0, Integer.MaxValue, 0)
    '                If objIftiBeneficiary.Count > 0 Then
    '                    'Me.txtPenerima_rekening.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_NoRekening

    '                    ' cek ReceiverType
    '                    Dim receiverType As Integer = objIftiBeneficiary(0).FK_IFTI_NasabahType_ID
    '                    Select Case (receiverType)
    '                        Case 1
    '                            Me.RBPenerimaNasabah_TipePengirim.SelectedValue = 1
    '                            Me.trSwiftOutPenerimaTipeNasabah.Visible = True
    '                            Me.Rb_IdenPenerimaNas_TipeNasabah.SelectedValue = 1
    '                            Me.MultiViewSwiftOutIdenPenerima.ActiveViewIndex = 0
    '                            Me.MultiViewPenerimaNasabah.ActiveViewIndex = 0
    '                            FieldSwiftOutPenerimaNasabahPerorangan(PKIFTIBeneficiary)
    '                            'enable false
    '                            RBPenerimaNasabah_TipePengirim.Enabled = False
    '                            Rb_IdenPenerimaNas_TipeNasabah.Enabled = False
    '                            Me.txtPenerima_rekening.Enabled = False
    '                            Me.txtPenerimaNasabah_IND_nama.Enabled = False
    '                            Me.txtPenerimaNasabah_IND_TanggalLahir.Enabled = False
    '                            Me.RbPenerimaNasabah_IND_Warganegara.Enabled = False
    '                            txtPenerimaNasabah_IND_negara.Enabled = False
    '                            Me.txtPenerimaNasabah_IND_negaraLain.Enabled = False
    '                            Me.txtPenerimaNasabah_IND_alamatIden.Enabled = False
    '                            Me.txtPenerimaNasabah_IND_negaraBagian.Enabled = False
    '                            txtPenerimaNasabah_IND_negaraIden.Enabled = False
    '                            Me.txtPenerimaNasabah_IND_negaraLainIden.Enabled = False
    '                            Me.CBOPenerimaNasabah_IND_JenisIden.Enabled = False
    '                            Me.txtPenerimaNasabah_IND_NomorIden.Enabled = False
    '                            Me.txtPenerimaNasabah_IND_NilaiTransaksiKeuangan.Enabled = False
    '                            popUpTanggalLahirPenerimaNasabah_Ind.Visible = False
    '                            imgButton_PenerimaNasabah_IND_negara.Visible = False
    '                            imgButton_PenerimaNasabah_IND_negaraIden.Visible = False
    '                            ImageButton_savePenerimaInd.Visible = False
    '                        Case 2
    '                            Me.RBPenerimaNasabah_TipePengirim.SelectedValue = 1
    '                            Me.Rb_IdenPenerimaNas_TipeNasabah.SelectedValue = 2
    '                            Me.trSwiftOutPenerimaTipeNasabah.Visible = True
    '                            Me.MultiViewSwiftOutIdenPenerima.ActiveViewIndex = 0
    '                            Me.MultiViewPenerimaNasabah.ActiveViewIndex = 1
    '                            FieldSwiftOutPenerimaNasabahKorporasi(PKIFTIBeneficiary)
    '                        Case 3
    '                            Me.RBPenerimaNasabah_TipePengirim.SelectedValue = 1
    '                            Me.trSwiftOutPenerimaTipeNasabah.Visible = False
    '                            Me.MultiViewSwiftOutIdenPenerima.ActiveViewIndex = 1
    '                            Me.trSwiftOutPenerimaTipeNasabah.Visible = False
    '                            FieldSwiftOutPenerimaNonNasabah(PKIFTIBeneficiary)
    '                    End Select
    '                End If

    '            End Using
    '        Catch ex As Exception
    '            'LogError(ex)
    '            cvalPageErr.IsValid = False
    '            cvalPageErr.ErrorMessage = ex.Message
    '        End Try
    '    End If
    'End Sub

    Protected Sub GridDataView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridDataView.ItemDataBound
        Try
            Dim CollCount As Integer = e.Item.Cells.Count - 1
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Dim chkBox As CheckBox = CType(e.Item.FindControl("CheckBoxExporttoExcel"), CheckBox)
                chkBox.Checked = SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)
                If BindGridFromExcel = True Then
                    e.Item.Cells(4).Text = CStr((e.Item.ItemIndex + 1))
                Else
                    e.Item.Cells(4).Text = CStr((e.Item.ItemIndex + 1) + (SetnGetCurrentPage * GetDisplayedTotalRow))
                End If
                'Dim nasabahType As Integer = e.Item.Cells(3).Text

                'Select Case (nasabahType)
                '    Case 1
                '        GridDataView.Columns(9).Visible = False
                '        GridDataView.Columns(10).Visible = False
                '        GridDataView.Columns(11).Visible = False
                '        GridDataView.Columns(12).Visible = False
                '        GridDataView.Columns(13).Visible = False
                '        GridDataView.Columns(14).Visible = False
                '        GridDataView.Columns(15).Visible = False
                '        GridDataView.Columns(16).Visible = False
                '        GridDataView.Columns(17).Visible = False
                '    Case 2
                '        GridDataView.Columns(5).Visible = False
                '        GridDataView.Columns(6).Visible = False
                '        GridDataView.Columns(7).Visible = False
                '        GridDataView.Columns(8).Visible = False
                '        GridDataView.Columns(13).Visible = False
                '        GridDataView.Columns(14).Visible = False
                '        GridDataView.Columns(15).Visible = False
                '        GridDataView.Columns(16).Visible = False
                '        GridDataView.Columns(17).Visible = False

                '        'e.Item.Cells(5).Visible = False
                '        'e.Item.Cells(6).Visible = False
                '        'e.Item.Cells(7).Visible = False
                '        'e.Item.Cells(8).Visible = False

                '        'e.Item.Cells(13).Visible = False
                '        'e.Item.Cells(14).Visible = False
                '        'e.Item.Cells(15).Visible = False
                '        'e.Item.Cells(16).Visible = False
                '        'e.Item.Cells(17).Visible = False
                '    Case 3
                '        GridDataView.Columns(9).Visible = False
                '        GridDataView.Columns(10).Visible = False
                '        GridDataView.Columns(11).Visible = False
                '        GridDataView.Columns(12).Visible = False
                '        GridDataView.Columns(5).Visible = False
                '        GridDataView.Columns(6).Visible = False
                '        GridDataView.Columns(7).Visible = False
                '        GridDataView.Columns(8).Visible = False
                'End Select
                'Using CekIFTI_ApprovalDetail As TList(Of IFTI_Approval_Detail) = DataRepository.IFTI_Approval_DetailProvider.GetPaged("PK_IFTI_Approval_Detail_Id = '" & CInt(e.Item.Cells(1).Text) & "'", "", 0, Integer.MaxValue, 0)
                '    If CekIFTI_ApprovalDetail.Count > 0 Then
                '        e.Item.Cells(CollCount - 1).Enabled = False
                '    End If
                'End Using
            End If
        Catch ex As Exception
            'LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
clearSession()
                AuditTrailBLL.InsertAuditTrailUserAccess(Sahassa.AML.Commonly.SessionCurrentPage)
                SetCOntrolLoad()
                TransactionSwiftOut()
                
                'If MultiView1.ActiveViewIndex = 0 Then
                '    ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('IdTerlapor','Img1','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
                '    ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('Transaksi','Img2','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
                'End If
                Me.MultiViewNasabahTypeAll.ActiveViewIndex = 0
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            'LogError(ex)
        End Try
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'bindgrid()
        Me.LoadReceiver()
    End Sub
#End Region
    Protected Sub Rb_BOwnerNas_ApaMelibatkan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Rb_BOwnerNas_ApaMelibatkan.SelectedIndexChanged
        If Rb_BOwnerNas_ApaMelibatkan.SelectedValue = 1 Then
            Me.trBenfOwnerHubungan.Visible = True
            Me.trBenfOwnerTipePengirim.Visible = True
            MultiViewSwiftOutBOwner.Visible = True
        Else
            Me.trBenfOwnerTipePengirim.Visible = False
            Me.trBenfOwnerHubungan.Visible = False
            MultiViewSwiftOutBOwner.Visible = False
        End If
    End Sub

    Protected Sub RbPengirimNonNasabah_100Juta_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RbPengirimNonNasabah_100Juta.SelectedIndexChanged
        If RbPengirimNonNasabah_100Juta.SelectedValue = 2 Then
            jenisdokumen_nonNasabah_mand.Visible = True
        Else
            jenisdokumen_nonNasabah_mand.Visible = False
        End If
    End Sub
End Class
