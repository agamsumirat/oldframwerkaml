
Partial Class TransactionTypeAuxiliaryMappingApproval
    Inherits Parent

#Region "Set Session"
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("TTAMappingApprovalPageSelected") Is Nothing, New ArrayList, Session("TTAMappingApprovalPageSelected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("TTAMappingApprovalPageSelected") = value
        End Set
    End Property
    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("TTAMappingApprovalPageSort") Is Nothing, "Pk_TransactionTypeAuxiliaryTransactionCodeMappingApproval asc", Session("TTAMappingApprovalPageSort"))
        End Get
        Set(ByVal Value As String)
            Session("TTAMappingApprovalPageSort") = Value
        End Set
    End Property
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("TTAMappingApprovalCurrentPage") Is Nothing, 0, Session("TTAMappingApprovalCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("TTAMappingApprovalCurrentPage") = Value
        End Set
    End Property
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("TTAMappingApprovalRowTotal") Is Nothing, 0, Session("TTAMappingApprovalRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("TTAMappingApprovalRowTotal") = Value
        End Set
    End Property
    Private Property SetAndGetSearchingCriteria() As String
        Get
            Return IIf(Session("TTAMappingApprovalSearchCriteria") Is Nothing, "", Session("TTAMappingApprovalSearchCriteria"))
        End Get
        Set(ByVal Value As String)
            Session("TTAMappingApprovalSearchCriteria") = Value
        End Set
    End Property
    Private Function SetAllSearchingCriteria() As String
        'Dim objErrorMessage As New ORMsController.ErrorMessage
        Dim StrSearch As String = ""
        Try
            If Me.TxtAuxTrans.Text <> "" Then
                If StrSearch = "" Then
                    StrSearch = StrSearch & " TransactionDescription like '%" & Me.TxtAuxTrans.Text.Replace("'", "''") & "%' "
                Else
                    StrSearch = StrSearch & " And TransactionDescription like '%" & Me.TxtAuxTrans.Text.Replace("'", "''") & "%' "
                End If
            End If

            If Me.TxtUserId.Text <> "" Then
                If StrSearch = "" Then
                    StrSearch = StrSearch & " TransactionTypeAuxiliaryTransactionCodeMappingApproval_UserId like '%" & Me.TxtUserId.Text.Replace("'", "''") & "%' "
                Else
                    StrSearch = StrSearch & " And TransactionTypeAuxiliaryTransactionCodeMappingApproval_UserId like '%" & Me.TxtUserId.Text.Replace("'", "''") & "%' "
                End If
            End If
            Return StrSearch
        Catch
            Throw
            Return ""
        End Try
    End Function
    Private Property SetnGetBindTable() As Data.DataTable
        Get
            Return IIf(Session("TTAMappingApprovalViewData") Is Nothing, Sahassa.AML.Commonly.GetDatasetPaging(Me.TablesRelated, Me.Pk, Me.SetnGetSort, Me.SetnGetCurrentPage + 1, Sahassa.AML.Commonly.GetDisplayedTotalRow, Me.Fields, Me.SetAndGetSearchingCriteria, ""), Session("TTAMappingApprovalViewData"))
        End Get
        Set(ByVal value As Data.DataTable)
            Session("TTAMappingApprovalViewData") = value
        End Set
    End Property
#End Region

    Private Sub ClearThisPageSessions()
        Session("TTAMappingApprovalPageSelected") = Nothing
        Session("TTAMappingApprovalPageSort") = Nothing
        Session("TTAMappingApprovalCurrentPage") = Nothing
        Session("TTAMappingApprovalRowTotal") = Nothing
        Session("TTAMappingApprovalSearchCriteria") = Nothing
        Session("TTAMappingApprovalViewData") = Nothing
    End Sub

#Region "Set Property For Table Bind Grid"
    Private ReadOnly Property TablesRelated() As String
        Get
            Return "TransactionTypeAuxiliaryTransactionCodeMapping_Approval Inner Join Mode On TransactionTypeAuxiliaryTransactionCodeMapping_Approval.ModeId = Mode.MsModeId INNER JOIN TransactionTypeAuxiliaryTransactionCodeMapping_PendingApproval ON TransactionTypeAuxiliaryTransactionCodeMapping_Approval.PK_TransactionTypeAuxiliaryTransactionCodeMappingApproval=TransactionTypeAuxiliaryTransactionCodeMapping_PendingApproval.FK_TransactionTypeAuxiliaryTransactionCodeMapping_Approval INNER JOIN vw_AuxTransactionCode ON TransactionTypeAuxiliaryTransactionCodeMapping_PendingApproval.AuxiliaryTransactionCode=vw_AuxTransactionCode.TransactionCode"
        End Get
    End Property
    Private ReadOnly Property Pk() As String
        Get
            Return "TransactionTypeAuxiliaryTransactionCodeMapping_Approval.Pk_TransactionTypeAuxiliaryTransactionCodeMappingApproval"
        End Get
    End Property
    Private ReadOnly Property Fields() As String
        Get
            Return "TransactionTypeAuxiliaryTransactionCodeMapping_Approval.Pk_TransactionTypeAuxiliaryTransactionCodeMappingApproval, TransactionTypeAuxiliaryTransactionCodeMapping_Approval.TransactionTypeAuxiliaryTransactionCodeMappingApproval_EntryDate, TransactionTypeAuxiliaryTransactionCodeMapping_Approval.TransactionTypeAuxiliaryTransactionCodeMappingApproval_UserId, TransactionTypeAuxiliaryTransactionCodeMapping_Approval.ModeId, Mode.Nama, vw_AuxTransactionCode.TransactionDescription"
        End Get
    End Property
#End Region

#Region "Bind Grid View"
    Public Sub BindGrid()
        Me.SetnGetBindTable = Sahassa.AML.Commonly.GetDatasetPaging(Me.TablesRelated, Me.Pk, Me.SetnGetSort, Me.SetnGetCurrentPage + 1, Sahassa.AML.Commonly.GetDisplayedTotalRow, Me.Fields, Me.SetAndGetSearchingCriteria, "")
        Me.GridView.DataSource = Me.SetnGetBindTable
        Me.SetnGetRowTotal = Sahassa.AML.Commonly.GetTableCount(Me.TablesRelated, Me.SetAndGetSearchingCriteria)
        Me.GridView.CurrentPageIndex = Me.SetnGetCurrentPage
        Me.GridView.VirtualItemCount = Me.SetnGetRowTotal
        Me.GridView.DataBind()
    End Sub
#End Region

#Region "Paging Event"
    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
        Catch
            Throw
        End Try
    End Sub
    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
            Me.CollectSelected()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Protected Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            Me.CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be a positive number greater than zero.")
                End If
            Else
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If
            CollectSelected()
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
#End Region
#Region "Sorting Event"

    Protected Sub GridView_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridView.EditCommand
        Dim PkApprovalId As String = e.Item.Cells(1).Text
        Dim ModeId As String = e.Item.Cells(4).Text

        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "TransactionTypeAuxiliaryMappingApprovalDetail.aspx"
            MagicAjax.AjaxCallHelper.Redirect("TransactionTypeAuxiliaryMappingApprovalDetail.aspx?PkApprovalId=" & PkApprovalId & "&TypeConfirm=" & ModeId)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub GridView_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridView.SortCommand
        Dim GridRETBaselViewPage As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridRETBaselViewPage.Columns(Sahassa.AML.Commonly.IndexSort(GridView, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
#End Region
#Region "Excel Export Event"
    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In Me.GridView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = gridRow.FindControl("CheckBoxExporttoExcel")
                Dim RETBaselPkId As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
                If ArrTarget.Contains(RETBaselPkId) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(RETBaselPkId)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(RETBaselPkId)
                End If
                Me.SetnGetSelectedItem = ArrTarget
            End If
        Next
    End Sub
    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub 'ClearControls
    Private Sub BindSelected()
        Dim Rows As New ArrayList
        Dim oTableRETBaselView As New Data.DataTable
        Dim oRowRETBaselView As Data.DataRow
        oTableRETBaselView.Columns.Add("Pk_TransactionTypeAuxiliaryTransactionCodeMappingApproval", GetType(Int64))
        oTableRETBaselView.Columns.Add("TransactionTypeAuxiliaryTransactionCodeMappingApproval_EntryDate", GetType(String))
        oTableRETBaselView.Columns.Add("TransactionTypeAuxiliaryTransactionCodeMappingApproval_UserId", GetType(String))
        oTableRETBaselView.Columns.Add("ModeId", GetType(String))
        oTableRETBaselView.Columns.Add("Nama", GetType(String))
        oTableRETBaselView.Columns.Add("TransactionDescription", GetType(String))
        For Each IdPk As Int64 In Me.SetnGetSelectedItem
            Dim rowData() As Data.DataRow = Me.SetnGetBindTable.Select("Pk_TransactionTypeAuxiliaryTransactionCodeMappingApproval = " & IdPk & "")
            If rowData.Length > 0 Then
                oRowRETBaselView = oTableRETBaselView.NewRow
                oRowRETBaselView(0) = rowData(0)("Pk_TransactionTypeAuxiliaryTransactionCodeMappingApproval")
                oRowRETBaselView(1) = rowData(0)("TransactionTypeAuxiliaryTransactionCodeMappingApproval_EntryDate")
                oRowRETBaselView(2) = rowData(0)("TransactionTypeAuxiliaryTransactionCodeMappingApproval_UserId")
                oRowRETBaselView(3) = rowData(0)("ModeId")
                oRowRETBaselView(4) = rowData(0)("Nama")
                oRowRETBaselView(5) = rowData(0)("TransactionDescription")
                oTableRETBaselView.Rows.Add(oRowRETBaselView)
            End If
        Next
        Me.GridView.DataSource = oTableRETBaselView
        Me.GridView.AllowPaging = False
        Me.GridView.DataBind()

        'Sembunyikan kolom agar tidak ikut diekspor ke excel
        Me.GridView.Columns(1).Visible = False
        Me.GridView.Columns(4).Visible = False
    End Sub
    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Me.CollectSelected()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelected()
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=TransactionTypeAuxiliaryMappingApproval.xls")
            Response.Charset = ""
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridView)
            GridView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        'For Each gridRow As DataGridItem In Me.GridView.Items
        '    If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
        '        Dim RETBaselViewId As String = gridRow.Cells(1).Text
        '        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        '        If Me.SetnGetSelectedItem.Contains(RETBaselViewId) Then
        '            If Me.CheckBoxSelectAll.Checked Then
        '                ArrTarget.Add(RETBaselViewId)
        '            Else
        '                ArrTarget.Remove(RETBaselViewId)
        '            End If
        '        Else
        '            If Me.CheckBoxSelectAll.Checked Then
        '                ArrTarget.Add(RETBaselViewId)
        '            End If
        '        End If
        '        Me.SetnGetSelectedItem = ArrTarget
        '    End If
        'Next
        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        For Each gridRow As DataGridItem In Me.GridView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(1).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItem.Contains(StrKey) = False Then
                        ArrTarget.Add(StrKey)
                    End If
                Else
                    If Me.SetnGetSelectedItem.Contains(StrKey) Then
                        ArrTarget.Remove(StrKey)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItem = ArrTarget
    End Sub
#End Region

    Protected Sub GridView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridView.ItemDataBound
        'bikin indexing
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Dim chkBox As CheckBox = e.Item.FindControl("CheckBoxExporttoExcel")
                chkBox.Checked = Me.SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)
                'e.Item.Cells(1).Text = e.Item.ItemIndex + 1
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                Me.GridView.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow
                Me.SetAndGetSearchingCriteria = Me.SetAllSearchingCriteria

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using
                'End Using

            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Try
            Me.SetAndGetSearchingCriteria = Me.SetAllSearchingCriteria
            Me.SetnGetCurrentPage = 0

            Me.BindGrid()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class
