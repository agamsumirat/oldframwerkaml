﻿Imports CDDLNPNettier.Data
Imports CDDLNPNettier.Entities
Imports AMLBLL
Imports System.Data

Partial Class CDDRiskFactQuestionEdit
    Inherits Parent

    Private ReadOnly Property GetPK_CDDRiskFactQuestion_ID() As Int32
        Get
            Return Request.QueryString("ID")
        End Get
    End Property

    Property Edit_index() As Integer
        Get
            Return IIf(Session("CDDRiskFactQuestionEdit.index") Is Nothing, -1, Session("CDDRiskFactQuestionEdit.index"))
        End Get
        Set(ByVal Value As Integer)
            Session("CDDRiskFactQuestionEdit.index") = Value
        End Set
    End Property

    Private Property SetnGetBindTable() As TList(Of CDD_RiskFactQuestionDetail_ApprovalDetail)
        Get
            Return CType(Session("CDDRiskFactQuestionDetailEdit"), TList(Of CDD_RiskFactQuestionDetail_ApprovalDetail))
        End Get
        Set(ByVal value As TList(Of CDD_RiskFactQuestionDetail_ApprovalDetail))
            Session("CDDRiskFactQuestionDetailEdit") = value
        End Set
    End Property

    Private Sub LoadData()
        Using objCDDQuestion As CDD_RiskFactQuestion = DataRepository.CDD_RiskFactQuestionProvider.GetByPK_CDD_RiskFactQuestion_ID(GetPK_CDDRiskFactQuestion_ID)
            If objCDDQuestion IsNot Nothing Then
                Me.txtQuestion.Text = objCDDQuestion.Question
                Me.ddlRiskFact.SelectedValue = objCDDQuestion.FK_Risk_Fact_ID

                For Each objCDDRiskFactQuestionDetail As CDD_RiskFactQuestionDetail In DataRepository.CDD_RiskFactQuestionDetailProvider.GetPaged("FK_CDD_RiskFactQuestion_ID = " & GetPK_CDDRiskFactQuestion_ID, String.Empty, 0, Int32.MaxValue, 0)
                    SetnGetBindTable.Add(New CDD_RiskFactQuestionDetail_ApprovalDetail)
                    SetnGetBindTable(SetnGetBindTable.Count - 1).RiskFactDetail = objCDDRiskFactQuestionDetail.RiskFactDetail
                    SetnGetBindTable(SetnGetBindTable.Count - 1).RiskFactScore = objCDDRiskFactQuestionDetail.RiskFactScore
                Next
            End If
        End Using
    End Sub

    Private Sub BindComboBox()
        ddlRiskFact.Items.Clear()
        For Each objRiskFact As RiskFact In DataRepository.RiskFactProvider.GetAll()
            ddlRiskFact.Items.Add(New ListItem(objRiskFact.RiskFactName, objRiskFact.Pk_RiskFact_ID))
        Next
    End Sub

    Protected Sub CvalPageErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles cvalPageError.PreRender
        If cvalPageError.IsValid = False Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
        End If
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not Page.IsPostBack Then
                SetnGetBindTable = New TList(Of CDD_RiskFactQuestionDetail_ApprovalDetail)
                BindComboBox()
                LoadData()
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
            End If
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub

    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim objTransManager As New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)
        Dim objApproval As New CDD_RiskFactQuestion_Approval
        Dim objCDDRiskFactQuestion As New CDD_RiskFactQuestion_ApprovalDetail
        Dim objCDDRiskFactQuestionDetail As New CDD_RiskFactQuestionDetail_ApprovalDetail

        Try
            objTransManager.BeginTransaction()

            If DataRepository.CDD_RiskFactQuestion_ApprovalDetailProvider.GetTotalItems("PK_CDD_RiskFactQuestion_ID = " & Me.GetPK_CDDRiskFactQuestion_ID, 0) > 0 Then
                Throw New Exception("Question masih dalam pending approval")
            End If

            If txtQuestion.Text.Trim = String.Empty Then
                Throw New Exception("Question harus diisi")
            End If

            If SetnGetBindTable.Count = 0 Then
                Throw New Exception("Question Detail harus diisi")
            End If

            objApproval.RequestedBy = Sahassa.AML.Commonly.SessionPkUserId
            objApproval.RequestedDate = Date.Now
            objApproval.FK_MsMode_Id = 2
            objApproval.IsUpload = False
            DataRepository.CDD_RiskFactQuestion_ApprovalProvider.Save(objTransManager, objApproval)

            objCDDRiskFactQuestion.FK_CDD_RiskFactQuestion_Approval_Id = objApproval.PK_CDD_RiskFactQuestion_Approval_Id
            objCDDRiskFactQuestion.Question = txtQuestion.Text.Trim
            objCDDRiskFactQuestion.FK_Risk_Fact_ID = ddlRiskFact.SelectedValue
            objCDDRiskFactQuestion.PK_CDD_RiskFactQuestion_ID = GetPK_CDDRiskFactQuestion_ID
            DataRepository.CDD_RiskFactQuestion_ApprovalDetailProvider.Save(objTransManager, objCDDRiskFactQuestion) 'Save Question


            For Each rowCDDQuestionDetail As CDD_RiskFactQuestionDetail_ApprovalDetail In SetnGetBindTable
                rowCDDQuestionDetail.FK_CDD_RiskFactQuestion_Approval_Id = objApproval.PK_CDD_RiskFactQuestion_Approval_Id
            Next
            DataRepository.CDD_RiskFactQuestionDetail_ApprovalDetailProvider.Save(objTransManager, SetnGetBindTable)


            objTransManager.Commit()

            Me.Response.Redirect("CDDLNPMessage.aspx?ID=10421", False)

        Catch ex As Exception
            objTransManager.Rollback()
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        Finally
            If Not objTransManager Is Nothing Then
                objTransManager.Dispose()
                objTransManager = Nothing
            End If
            If Not objCDDRiskFactQuestion Is Nothing Then
                objCDDRiskFactQuestion.Dispose()
                objCDDRiskFactQuestion = Nothing
            End If
            If Not objCDDRiskFactQuestionDetail Is Nothing Then
                objCDDRiskFactQuestionDetail.Dispose()
                objCDDRiskFactQuestionDetail = Nothing
            End If
        End Try
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Me.Response.Redirect("CDDRiskFactQuestionView.aspx")
    End Sub

    Protected Sub ImageDetail_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageEdit.Click, ImageAdd.Click, ImageCancelEdit.Click
        Try
            'Validasi
            Select Case CType(sender, Image).ID
                Case "ImageAdd", "ImageEdit"
                    If txtQuestionDetail.Text.Trim = String.Empty Then
                        txtQuestionDetail.Focus()
                        Throw New Exception("Question Detail harus diisi")
                    End If

                    If Not IsNumeric(txtRiskFactScore.Text) Then
                        txtRiskFactScore.Focus()
                        Throw New Exception("Risk Fact Score harus angka")
                    End If

                Case "ImageCancelEdit"
                    'Do nothing

            End Select

            'Set Question Detail
            Select Case CType(sender, Image).ID
                Case "ImageAdd"
                    SetnGetBindTable.Add(New CDD_RiskFactQuestionDetail_ApprovalDetail)
                    SetnGetBindTable(SetnGetBindTable.Count - 1).RiskFactDetail = txtQuestionDetail.Text
                    SetnGetBindTable(SetnGetBindTable.Count - 1).RiskFactScore = txtRiskFactScore.Text

                Case "ImageEdit"
                    SetnGetBindTable(Edit_index).RiskFactDetail = txtQuestionDetail.Text
                    SetnGetBindTable(Edit_index).RiskFactScore = txtRiskFactScore.Text

                Case "ImageCancelEdit"
                    'Do Nothing

            End Select

            txtQuestionDetail.Focus()
            txtQuestionDetail.Text = String.Empty
            txtRiskFactScore.Text = String.Empty
            ImageEdit.Visible = False
            ImageCancelEdit.Visible = False
            ImageAdd.Visible = True
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub

    Protected Sub GridViewCDDQuestionDetail_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridViewCDDQuestionDetail.ItemCommand
        Try
            Select Case e.CommandName.ToLower
                Case "edit"
                    Edit_index = e.Item.ItemIndex
                    txtQuestionDetail.Text = SetnGetBindTable(Edit_index).RiskFactDetail
                    txtRiskFactScore.Text = SetnGetBindTable(Edit_index).RiskFactScore
                    txtQuestionDetail.Focus()
                    ImageEdit.Visible = True
                    ImageCancelEdit.Visible = True
                    ImageAdd.Visible = False

                Case "delete"
                    SetnGetBindTable.RemoveAt(e.Item.ItemIndex)
                    
            End Select
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            GridViewCDDQuestionDetail.DataSource = SetnGetBindTable
            GridViewCDDQuestionDetail.DataBind()
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub

End Class