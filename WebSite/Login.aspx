<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Login.aspx.vb" Inherits="Login"
    Title=":: Anti Money Laundering ::" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>:: Anti Money Laundering ::</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="theme/aml.css" rel="stylesheet" type="text/css">
</head>
<body leftmargin="0" topmargin="0" bgcolor="#ffffff">
    <form id="form1" runat="server">
    <table style="height: 100%; width: 100%" id="tablebasic" cellspacing="0" cellpadding="0"
        border="0">
        <tr>
            <td background="images/validationbground.gif">
                <ajax:AjaxPanel ID="a" runat="server">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="validation"
                        Width="100%" HeaderText="There were errors on the page:"></asp:ValidationSummary>
                </ajax:AjaxPanel>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <img src="images/front-titleV2.gif" width="417" height="152">
                        </td>
                    </tr>
                    <tr>
                        <td height="130" background="images/front-bghitam.gif">
                            <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="147" style="height: 130px">
                                        <img src="images/front-uang-dijemur.jpg" width="147" height="130">
                                    </td>
                                    <td width="99%" style="padding-left: 20px; height: 130px;">
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td class="logintext">
                                                    <span style="color: white">Username&nbsp; : </span>&nbsp;
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="TextUsername" runat="server"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator
                                                        ID="RequiredFieldUsername" runat="server" ControlToValidate="TextUsername" EnableClientScript="False"
                                                        ErrorMessage="Username cannot be blank">*</asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="logintext">
                                                    <span style="color: white">Password&nbsp; : </span>&nbsp;
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="TextPassword" runat="server" TextMode="Password" Width="149px"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator
                                                        ID="RequiredFieldPassword" runat="server" ControlToValidate="TextPassword" EnableClientScript="False"
                                                        ErrorMessage="Password cannot be blank">*</asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" colspan="2" style="height: 32px">
                                                    &nbsp;<asp:CustomValidator ID="CustomValidatorLogin" runat="server" Display="None"
                                                        EnableClientScript="False"></asp:CustomValidator>
                                                    <asp:ImageButton ID="LoginButton" runat="server" SkinID="loginButton" />
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" colspan="2" style="height: 19px">
                                                    <%-- <asp:HyperLink ID="HyperLinkSignInProblem" runat="server" NavigateUrl="~/SignInProblemHelp.aspx"
                                                TabIndex="3" Target="_self">Problem with sign in?</asp:HyperLink>--%>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="height: 130px">
                                        <img src="images/front-bar-merah-kananv2.jpg" width="319" height="130">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td id="tdcontent" valign="top" style="height: 98%">
                            <table height="100%" width="100%">
                                <tr>
                                    <td style="padding-left: 20px; padding-top: 20px; width: 50%;" valign="top" align="left">
                                        <table id="tableNews" runat="server" cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tr>
                                                <td class="stdbold" style="height: 22px">
                                                    <strong><span style="font-size: 14pt">Latest News</span></strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <%--<td style="height: 5px"><img src="images/front-line-for-news.gif" height="5"><br /></td>--%>
                                                <td style="height: 5px">
                                                    <hr />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Repeater ID="NewsRepeater" runat="server">
                                                        <ItemTemplate>
                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td style="font-size: 20px">
                                                                        <%#Eval("NewsTitle")%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <%#Eval("NewsCreatedDate")%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <%#Eval("NewsSummary")%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        [<asp:LinkButton ID="ShowMoreNews" runat="server" CommandArgument='<%# Eval("NewsID") %>'
                                                                            CommandName="ShowMoreNews">Read More...</asp:LinkButton>]
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                        <SeparatorTemplate>
                                                            <hr />
                                                            <%--<img src="images/front-line-for-news.gif" height="5">--%>
                                                        </SeparatorTemplate>
                                                    </asp:Repeater>
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <td align="right">
                                                 &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <%--<td><img src="images/front-line-for-news.gif" height="5"></td>--%>
                                                <td style="height: 5px">
                                                    <hr />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="padding-top: 20px; width: 50%;" valign="top" align="right">
                                        <table id="tableNews1" runat="server" cellpadding="0" cellspacing="0" border="0"
                                            width="100%">
                                            <tr>
                                                <td class="stdbold" style="height: 22px">
                                                    <%--<strong><span style="font-size: 14pt">Latest News</span></strong>--%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <%--<td style="height: 5px"><img src="images/front-line-for-news.gif" height="5"><br /></td>--%>
                                                <td style="height: 5px">
                                                    <hr />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <asp:Repeater ID="NewsRepeater1" runat="server">
                                                        <ItemTemplate>
                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td style="font-size: 20px">
                                                                        <%#Eval("NewsTitle")%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <%#Eval("NewsCreatedDate")%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <%#Eval("NewsSummary")%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        [<asp:LinkButton ID="ShowMoreNews" runat="server" CommandArgument='<%# Eval("NewsID") %>'
                                                                            CommandName="ShowMoreNews">Read More...</asp:LinkButton>]
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                        <SeparatorTemplate>
                                                            <hr />
                                                            <%--<img src="images/front-line-for-news.gif" height="5">--%>
                                                        </SeparatorTemplate>
                                                    </asp:Repeater>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <%--<asp:HyperLink ID="HyperLinkMoreNews" runat="server" NavigateUrl="~/DisplayAllNews.aspx"
                                                    Target="_self">More >></asp:HyperLink><a href="more.aspx" class="stdbold"></a>--%>
                                                    <a href="DisplayAllNews.aspx" class="stdbold">More >></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <%--<td><img src="images/front-line-for-news.gif" height="5"></td>--%>
                                                <td style="height: 5px">
                                                    <hr />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom" align="right" width="99%">
                            &nbsp;<table>
                                <tr>
                                    <td align="right" style="padding-right: 10px; width: 296px;">
                                        Copyright &copy; Bank CIMB Niaga 2010-2011, All Rights Reserved<br>
                                        Version 2.0.0 (Build 2999)<br>
                                        <br>
                                        Developed by <a href="http://www.sahassa.co.id" class="stdbold">Sahassa</a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
    <script>
        document.getElementById('<%=GetUserElemetId %>').focus();
    </script>
</body>
</html>
