Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper


Public Class PotentialCustomerVerificationParameter
    Inherits Parent

#Region " Property "
    ''' <summary>
    ''' get focus id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property GetFocusID() As String
        Get
            Return Me.TextMinimumResultPercentage.ClientID
        End Get
    End Property
#End Region

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' cek approved
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	09/06/2006	Created
    '''     [Hendry] 15/06/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Function CekApproved() As Boolean
        Using AccessParameters As New AMLDAL.AMLDataSetTableAdapters.Parameters_ApprovalTableAdapter
            'Periksa apakah Potential Customer Verification Parameter ada dalam Pending Approval 
            Dim count As Int32 = AccessParameters.CountParametersApproval(3)
            If count > 0 Then
                Return False
            Else
                Return True
            End If
        End Using
    End Function

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get data Potential Customer Verification Parameter
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	03/07/2006	Created
    '''     [Hendry] 15/06/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GetData()
        Using AccessPotentialCustomerVerificationParameter As New AMLDAL.AMLDataSetTableAdapters.PotentialCustomerVerificationParameterTableAdapter
            Using TablePotentialCustomerVerificationParameter As AMLDAL.AMLDataSet.PotentialCustomerVerificationParameterDataTable = AccessPotentialCustomerVerificationParameter.GetData
                If TablePotentialCustomerVerificationParameter.Rows.Count > 0 Then
                    Dim TableRowPotentialCustomerVerificationParameter As AMLDAL.AMLDataSet.PotentialCustomerVerificationParameterRow = TablePotentialCustomerVerificationParameter.Rows(0)

                    ViewState("MinimumResultPercentage_Old") = CType(TableRowPotentialCustomerVerificationParameter.MinimumResultPercentage, Int16)
                    Me.TextMinimumResultPercentage.Text = ViewState("MinimumResultPercentage_Old")

                    ViewState("CreatedDate_Old") = CType(TableRowPotentialCustomerVerificationParameter.CreatedDate, Date)
                    ViewState("LastUpdateDate_Old") = CType(TableRowPotentialCustomerVerificationParameter.LastUpdateDate, Date)

                End If
            End Using
        End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load page handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>  
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	09/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Me.CekApproved() Then
                If Not Me.IsPostBack Then
                    Me.GetData()

                    Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                    'Using Transcope As New Transactions.TransactionScope
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                        Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                        AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                        'Transcope.Complete()
                    End Using
                    'End Using
                End If
            Else
                Throw New Exception("Cannot update Potential Customer Verification Parameter because it is currently waiting for approval")
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' cancel event handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	09/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "Default.aspx"

        Me.Response.Redirect("Default.aspx", False)
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' save handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>    
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry] 15/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageSave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim oSQLTrans As SqlTransaction = Nothing

        Try
            If Me.CekApproved Then
                Page.Validate()
                If Page.IsValid Then
                    'Using TranScope As New Transactions.TransactionScope
                    Using ParametersPending As New AMLDAL.AMLDataSetTableAdapters.Parameters_PendingApprovalTableAdapter
                        oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(ParametersPending, Data.IsolationLevel.ReadUncommitted)

                        'Perika apakah sudah ada entry dalam tabel PotentialCustomerVerificationParameter, bila belum ada, maka buat entry baru dlm tabel PotentialCustomerVerificationParameter,
                        'tapi bila entry sudah ada dalam tabel PotentialCustomerVerificationParameter, maka update entry tsb
                        Using AccessPotentialCustomerVerificationParameter As New AMLDAL.AMLDataSetTableAdapters.PotentialCustomerVerificationParameterTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPotentialCustomerVerificationParameter, oSQLTrans)

                            Using TablePotentialCustomerVerificationParameter As AMLDAL.AMLDataSet.PotentialCustomerVerificationParameterDataTable = AccessPotentialCustomerVerificationParameter.GetData
                                'Sahassa.AML.TableAdapterHelper.SetTransaction(TablePotentialCustomerVerificationParameter, oSQLTrans)

                                Using PotentialCustomerVerificationParameterApproval As New AMLDAL.AMLDataSetTableAdapters.PotentialCustomerVerificationParameter_ApprovalTableAdapter
                                    Sahassa.AML.TableAdapterHelper.SetTransaction(PotentialCustomerVerificationParameterApproval, oSQLTrans)

                                    'Buat variabel untuk menampung nilai-nilai baru
                                    Dim MinimumResultPercentage As Int16 = Me.TextMinimumResultPercentage.Text

                                    Dim ModeID As Int16
                                    Dim PotentialCustomerVerificationParameterApprovalID As Int64

                                    If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                                        If TablePotentialCustomerVerificationParameter.Rows.Count > 0 Then
                                            Dim MinimumResultPercentage_Old As Int16 = ViewState("MinimumResultPercentage_Old")
                                            Dim CreatedDate_Old As DateTime = ViewState("CreatedDate_Old")
                                            Dim LastUpdate_Old As DateTime = ViewState("LastUpdateDate_Old")
                                            If Me.InsertAuditTrailSU("Edit", "Accepted") Then
                                                If AccessPotentialCustomerVerificationParameter.UpdatePotentialCustomerVerificationParameter(CInt(Me.TextMinimumResultPercentage.Text), Now) Then
                                                    Me.LblSucces.Visible = True
                                                    Me.LblSucces.Text = "Success to update PotentialCustomer Verification Parameter."
                                                Else
                                                    Throw New Exception("Failed to Update Potential Customer Verification Parameter")
                                                End If
                                            Else
                                                Throw New Exception("Failed to insert Audit Trail.")
                                            End If
                                        Else
                                            If Me.InsertAuditTrailSU("Add", "Accepted") Then
                                                If Not AccessPotentialCustomerVerificationParameter.Insert(CInt(Me.TextMinimumResultPercentage.Text), Now, Now) Then
                                                    Throw New Exception("Failed to Insert Potential Customer Verification Parameter")
                                                End If
                                            Else
                                                Throw New Exception("Failed to insert Audit Trail.")
                                            End If
                                        End If
                                    Else
                                        Dim IDMessage As Integer
                                        'Bila tabel PotentialCustomerVerificationParameter tidak kosong, maka update entry tsb & set ModeID = 2 (Edit)
                                        If TablePotentialCustomerVerificationParameter.Rows.Count > 0 Then
                                            'Buat variabel untuk menampung nilai-nilai lama
                                            Dim MinimumResultPercentage_Old As Int16 = ViewState("MinimumResultPercentage_Old")
                                            Dim CreatedDate_Old As DateTime = ViewState("CreatedDate_Old")
                                            Dim LastUpdate_Old As DateTime = ViewState("LastUpdateDate_Old")
                                            ModeID = 2
                                            IDMessage = 8832
                                            'PotentialCustomerVerificationParameterApprovalID adalah primary key dari tabel PotentialCustomerVerificationParameter_Approval yang sifatnya identity dan baru dibentuk setelah row baru dibuat
                                            PotentialCustomerVerificationParameterApprovalID = PotentialCustomerVerificationParameterApproval.InsertPotentialCustomerVerificationParameter_Approval(MinimumResultPercentage, CreatedDate_Old, Now, MinimumResultPercentage_Old, CreatedDate_Old, LastUpdate_Old)
                                        Else 'Bila tabel PotentialCustomerVerificationParameter kosong, maka buat entry baru & set ModeID = 1 (Add)
                                            ModeID = 1
                                            IDMessage = 8831
                                            'PotentialCustomerVerificationParameterApprovalID adalah primary key dari tabel PotentialCustomerVerificationParameter_Approval yang sifatnya identity dan baru dibentuk setelah row baru dibuat
                                            PotentialCustomerVerificationParameterApprovalID = PotentialCustomerVerificationParameterApproval.InsertPotentialCustomerVerificationParameter_Approval(MinimumResultPercentage, Now, Now, Nothing, Nothing, Nothing)
                                        End If

                                        'ParametersPendingApprovalID adalah primary key dari tabel Parameters_PendingApproval yang sifatnya identity dan baru dibentuk setelah row baru dibuat 
                                        Dim ParametersPendingApprovalID As Int64 = ParametersPending.InsertParameters_PendingApproval(Sahassa.AML.Commonly.SessionUserId, Now, ModeID)

                                        Using ParametersApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_ApprovalTableAdapter
                                            Sahassa.AML.TableAdapterHelper.SetTransaction(ParametersApproval, oSQLTrans)
                                            'ParameterItemID 3 = PotentialCustomerVerificationParameter
                                            ParametersApproval.Insert(ParametersPendingApprovalID, ModeID, 3, PotentialCustomerVerificationParameterApprovalID)
                                        End Using

                                        Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & IDMessage

                                        Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & IDMessage, False)
                                    End If
                                End Using
                            End Using
                        End Using
                    End Using

                    oSQLTrans.Commit()
                    'End Using
                End If
            Else
                Throw New Exception("Cannot update Potential Customer Verification Parameter because it is currently waiting for approval")
            End If
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()

            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub
    ''' <summary>
    ''' Insert Audittrail SU
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function InsertAuditTrailSU(ByVal mode As String, ByVal Action As String) As Boolean
        Dim oSQLTrans As SqlTransaction = Nothing

        Try
            'Using TransScope As New Transactions.TransactionScope
            Using AccessAuditTrailParameter As New AMLDAL.AMLDataSetTableAdapters.AuditTrailParameterTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAuditTrailParameter, Data.IsolationLevel.ReadUncommitted)
                Using dtAuditTrailParameter As AMLDAL.AMLDataSet.AuditTrailParameterDataTable = AccessAuditTrailParameter.GetData
                    Dim RowAuditTrailParameter As AMLDAL.AMLDataSet.AuditTrailParameterRow = dtAuditTrailParameter.Rows(0)
                    If RowAuditTrailParameter.AlertOptions = 1 Then 'alert Overwrite
                        If Not Sahassa.AML.AuditTrailAlert.CheckOverwrite(3) Then
                            Throw New Exception("There is a problem in checking audittrail alert overwrite")
                        End If
                    ElseIf RowAuditTrailParameter.AlertOptions = 2 Then 'alert mail
                        If Not Sahassa.AML.AuditTrailAlert.CheckMailAlert(3) Then
                            Throw New Exception("There is a problem in checking audittrail alert by mail.")
                        End If
                    End If ' yg manual tidak perlu d cek krn ada audittrail maintenance
                End Using
            End Using

            Using AccessPotentialCustomerVerificationParameter As New AMLDAL.AMLDataSetTableAdapters.PotentialCustomerVerificationParameterTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPotentialCustomerVerificationParameter, oSQLTrans)
                Using dtPotentialCustomerVerification As AMLDAL.AMLDataSet.PotentialCustomerVerificationParameterDataTable = AccessPotentialCustomerVerificationParameter.GetData
                    Dim Row As AMLDAL.AMLDataSet.PotentialCustomerVerificationParameterRow = dtPotentialCustomerVerification.Rows(0)
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                        If mode.ToLower = "add" Then
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - PotentialCustomerVerification", "MinimumResultPercentage", mode, "", Me.TextMinimumResultPercentage.Text, Action)
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - PotentialCustomerVerification", "CreatedDate", mode, Row.CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), Row.CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), Action)
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - PotentialCustomerVerification", "LastUpdateDate", mode, Now.ToString("dd-MMMM-yyyy HH:mm"), Now.ToString("dd-MMMM-yyyy HH:mm"), Action)
                        Else 'Edit
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - PotentialCustomerVerification", "MinimumResultPercentage", mode, Row.MinimumResultPercentage.ToString, Me.TextMinimumResultPercentage.Text, Action)
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - PotentialCustomerVerification", "CreatedDate", mode, Row.CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), Row.CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), Action)
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - PotentialCustomerVerification", "LastUpdateDate", mode, Row.LastUpdateDate.ToString("dd-MMMM-yyyy HH:mm"), Now.ToString("dd-MMMM-yyyy HH:mm"), Action)
                        End If
                    End Using
                End Using
            End Using

            oSQLTrans.Commit()
            'End Using
            Return True
        Catch
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Return False
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Function

End Class