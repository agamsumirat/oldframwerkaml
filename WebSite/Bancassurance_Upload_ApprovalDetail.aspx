﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Bancassurance_Upload_ApprovalDetail.aspx.vb" Inherits="Bancassurance_Upload_ApprovalDetail" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
<script src="script/popcalendar.js"></script>
    <div id="div1" class="divcontent">
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<tr>
				<td id="tdcontent" height="99%" valign="top" bgcolor="#FFFFFF" width="100%">
					<div id="divcontent" class="divcontent">
						<table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>								
								<td class="divcontentinside" bgcolor="#FFFFFF">                                
                                    <ajax:AjaxPanel ID="a" runat="server" meta:resourcekey="aResource1">
                                                            <asp:ValidationSummary ID="ValidationSummaryunhandle" runat="server" 
                                                                CssClass="validation" 
                                                                meta:resourcekey="ValidationSummaryunhandleResource1" Width="590px" />
                                                            <asp:ValidationSummary ID="ValidationSummaryhandle" runat="server" 
                                                                CssClass="validationok" ForeColor="Black" 
                                                                meta:resourcekey="ValidationSummaryhandleResource1" ValidationGroup="handle" />
                                    </ajax:AjaxPanel>
                                    <ajax:AjaxPanel ID="AjaxPanel5" runat="server" Width="100%" meta:resourcekey="AjaxPanel5Resource1">
                                <asp:MultiView ID="MultiViewUpload" runat="server" ActiveViewIndex="0">
                                    <asp:View ID="VwUpload" runat="server">
                                            <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
											                height="72" style="border-top-style: none; border-right-style: none; border-left-style: none;
											                border-bottom-style: none" bgcolor="#dddddd" width="100%">
											            <tr bgcolor="#ffffff">
												<td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
													border-right-style: none; border-left-style: none; border-bottom-style: none; width: 100%;">
													<strong>
														<img src="Images/dot_title.gif" width="17" height="17">
														<asp:Label ID="Label7" runat="server" Text="BANCASSURANCE UPLOAD APPROVAL DETAIL" 
                                                        Font-Size="Small"></asp:Label>
														<hr />
													</strong>&nbsp;&nbsp;&nbsp;&nbsp;<ajax:AjaxPanel ID="AjxMessage" runat="server" meta:resourcekey="AjxMessageResource1">
														<asp:Label ID="LblMessage" background="images/validationbground.gif" runat="server"
															CssClass="validationok" Width="100%" meta:resourcekey="LblMessageResource1"></asp:Label>
													</ajax:AjaxPanel>
												</td>
											            </tr>
										            </table>										            
											<table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
												border="2">
												<tr id="searchbox">
													<td colspan="2" valign="top" width="100%" bgcolor="#ffffff">
														<table cellpadding="0" width="100%" border="0">
															<tr>
																<td valign="middle" nowrap>
																	<ajax:AjaxPanel ID="AjaxPanel1" runat="server" Width="100%" 
                                                                        meta:resourcekey="AjaxPanel1Resource1">
																		<table style="height: 100%">
                                                                            <tr>
                                                                                <td colspan="3" style="height: 7px">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td nowrap style="height: 26px; width: 119px;">
                                                                                    <asp:Label ID="LabelMsUser_StaffName" runat="server" Text="Name"></asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    :
                                                                                </td>
                                                                                <td style="height: 26px; width: 765px;">
                                                                                    &nbsp;&nbsp;<asp:TextBox ID="txtMsUser_StaffName" runat="server" CssClass="searcheditbox" 
                                                                                        TabIndex="2" Width="296px"></asp:TextBox></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td nowrap style="height: 26px; width: 119px;">
                                                                                    <asp:Label ID="LabelRequestedDate" runat="server" Text="Requested Date"></asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    :
                                                                                </td>
                                                                                <td style="height: 26px; width: 765px;">
                                                                                    &nbsp;&nbsp;<asp:TextBox ID="txtRequestedDate" runat="server" CssClass="textBox" 
                                                                                        MaxLength="1000" TabIndex="2" ToolTip="RequestedDate" Width="296px"></asp:TextBox>&nbsp;&nbsp;&nbsp;
                                                                                </td>
                                                                            </tr>
                                                                        </table>
																	</ajax:AjaxPanel>
																</td>
															</tr>
														</table>
														<ajax:AjaxPanel ID="AjaxPanel3" runat="server" 
                                                            meta:resourcekey="AjaxPanel3Resource1" Width="100%">
															<asp:CustomValidator ID="CvalPageErr" runat="server" Display="None" meta:resourcekey="CvalPageErrResource1"></asp:CustomValidator></ajax:AjaxPanel>
														<ajax:AjaxPanel ID="Ajaxpanel2" runat="server" 
                                                            meta:resourcekey="Ajaxpanel2Resource1" Width="100%">
															<asp:CustomValidator ID="CvalHandleErr" runat="server" ValidationGroup="handle" Display="None"
																meta:resourcekey="CvalHandleErrResource1"></asp:CustomValidator></ajax:AjaxPanel>
													</td>
												</tr>
											</table>
										            <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
											               border="2">										
                                                        <tr>                                                    
                                                        <td bgcolor="#ffffff">
                                                            <ajax:AjaxPanel ID="AjaxPanel4" runat="server" 
                                                                meta:resourcekey="AjaxPanel4Resource1" Width="100%">                                                                
                                                                <asp:DataGrid ID="DataGrid1" runat="server" AllowPaging="True" 
                                                                    AllowSorting="True" AutoGenerateColumns="False" BorderColor="#003300" 
                                                                    BorderStyle="Solid" CellPadding="4" Font-Bold="False" Font-Italic="False" 
                                                                    Font-Overline="False" Font-Strikeout="False" Font-Underline="False" 
                                                                    ForeColor="White" GridLines="None" HorizontalAlign="Center" Width="100%" AllowCustomPaging="True"><PagerStyle 
                                                                        BackColor="#666666" ForeColor="White" HorizontalAlign="Center" Mode="NumericPages" /><HeaderStyle 
                                                                        BackColor="#1C5E55" Font-Bold="True" Font-Italic="False" Font-Overline="False" 
                                                                        Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                        HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" /><Columns><asp:BoundColumn 
                                                                            HeaderText="No"><HeaderStyle Font-Bold="False" Font-Italic="False" 
                                                                            Font-Overline="False" Font-Strikeout="False" Font-Underline="False" 
                                                                            ForeColor="White" /></asp:BoundColumn><asp:BoundColumn 
                                                                            DataField="PK_Bancassurance_Upload_id" HeaderText="PK_Bancassurance_Upload_id" 
                                                                            SortExpression="PK_Bancassurance_Upload_id asc" Visible="False"></asp:BoundColumn><asp:BoundColumn 
                                                                            DataField="Cif_No" HeaderText="Cif_No" SortExpression="Cif_No asc"></asp:BoundColumn><asp:BoundColumn 
                                                                            DataField="Account_Owner" HeaderText="Account_Owner"><HeaderStyle 
                                                                            Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                            Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                            HorizontalAlign="Center" VerticalAlign="Middle" /><ItemStyle 
                                                                            Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                            Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                            VerticalAlign="Middle" /></asp:BoundColumn><asp:BoundColumn 
                                                                            DataField="Account_No" HeaderText="Account_No"><HeaderStyle 
                                                                            Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                            Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                            HorizontalAlign="Center" VerticalAlign="Middle" /><ItemStyle 
                                                                            Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                            Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                            VerticalAlign="Middle" /></asp:BoundColumn><asp:BoundColumn 
                                                                            DataField="Name" HeaderText="Name"><HeaderStyle Font-Bold="False" 
                                                                            Font-Italic="False" Font-Overline="False" Font-Strikeout="False" 
                                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" 
                                                                            VerticalAlign="Middle" /><ItemStyle Font-Bold="False" Font-Italic="False" 
                                                                            Font-Overline="False" Font-Strikeout="False" Font-Underline="False" 
                                                                            HorizontalAlign="Center" VerticalAlign="Middle" /></asp:BoundColumn><asp:BoundColumn 
                                                                            DataField="Cif_Relation" HeaderText="Cif_Relation"><HeaderStyle 
                                                                            Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                            Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                            HorizontalAlign="Center" VerticalAlign="Middle" /><ItemStyle 
                                                                            Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                            Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                            VerticalAlign="Middle" /></asp:BoundColumn><asp:BoundColumn 
                                                                            DataField="Account_Type" HeaderText="Account_Type"><HeaderStyle 
                                                                            Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                            Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                            HorizontalAlign="Center" VerticalAlign="Middle" /><ItemStyle 
                                                                            Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                            Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                            VerticalAlign="Middle" /></asp:BoundColumn><asp:BoundColumn 
                                                                            DataField="Opening_Date" HeaderText="Opening_Date"><HeaderStyle 
                                                                            Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                            Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                            HorizontalAlign="Center" VerticalAlign="Middle" /><ItemStyle 
                                                                            Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                            Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                            VerticalAlign="Middle" /></asp:BoundColumn><asp:BoundColumn 
                                                                            DataField="Status" HeaderText="Status"><HeaderStyle Font-Bold="False" 
                                                                            Font-Italic="False" Font-Overline="False" Font-Strikeout="False" 
                                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" 
                                                                            VerticalAlign="Middle" /><ItemStyle Font-Bold="False" Font-Italic="False" 
                                                                            Font-Overline="False" Font-Strikeout="False" Font-Underline="False" 
                                                                            HorizontalAlign="Center" VerticalAlign="Middle" /></asp:BoundColumn><asp:BoundColumn 
                                                                            DataField="Joint_Account" HeaderText="Joint_Account"><HeaderStyle 
                                                                            Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                            Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                            HorizontalAlign="Center" VerticalAlign="Middle" /><ItemStyle 
                                                                            Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                            Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                            VerticalAlign="Middle" /></asp:BoundColumn><asp:BoundColumn 
                                                                            DataField="Account_OwnerShip_Type" HeaderText="Account_OwnerShip_Type"><HeaderStyle 
                                                                            Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                            Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                            HorizontalAlign="Center" VerticalAlign="Middle" /><ItemStyle 
                                                                            Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                            Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                            VerticalAlign="Middle" /></asp:BoundColumn><asp:BoundColumn 
                                                                            DataField="Product_Description" HeaderText="Product_Description"><HeaderStyle 
                                                                            Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                            Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                            HorizontalAlign="Center" VerticalAlign="Middle" /><ItemStyle 
                                                                            Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                            Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                            VerticalAlign="Middle" /></asp:BoundColumn><asp:BoundColumn 
                                                                            DataField="Currency" HeaderText="Currency"><HeaderStyle Font-Bold="False" 
                                                                            Font-Italic="False" Font-Overline="False" Font-Strikeout="False" 
                                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" 
                                                                            VerticalAlign="Middle" /><ItemStyle Font-Bold="False" Font-Italic="False" 
                                                                            Font-Overline="False" Font-Strikeout="False" Font-Underline="False" 
                                                                            HorizontalAlign="Center" VerticalAlign="Middle" /></asp:BoundColumn><asp:BoundColumn 
                                                                            DataField="Saldo" HeaderText="Saldo"><HeaderStyle Font-Bold="False" 
                                                                            Font-Italic="False" Font-Overline="False" Font-Strikeout="False" 
                                                                            Font-Underline="False" ForeColor="White" HorizontalAlign="Center" 
                                                                            VerticalAlign="Middle" /><ItemStyle Font-Bold="False" Font-Italic="False" 
                                                                            Font-Overline="False" Font-Strikeout="False" Font-Underline="False" 
                                                                            HorizontalAlign="Center" VerticalAlign="Middle" /></asp:BoundColumn>
                                                                    </Columns>
                                                                    <EditItemStyle BackColor="#7C6F57" />
                                                                    <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                                                                    <SelectedItemStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                                                                    <AlternatingItemStyle BackColor="White" />
                                                                    <ItemStyle BackColor="#E3EAEB" />
                                                                </asp:DataGrid>
                                                            </ajax:AjaxPanel>
                                                        </td>
                                                    </tr>                                                       
                                                        <tr>
                                                            <td>
                                                                <br />
                                                                <ajax:AjaxPanel ID="ajax10" runat="server" Width="464px" >
                                                                    <asp:Button ID="BtnAccept" runat="server" Text="Accept" />
                                                                    &nbsp;
                                                                    <asp:Button ID="BtnReject" runat="server" Text="Reject" />
                                                                    &nbsp;
                                                                    <asp:Button ID="BtnCancel" runat="server" Text="&lt;&lt; Back" />
                                                                </ajax:AjaxPanel>
                                                            </td>
                                                        </tr>
                                                        </table>
                                                    
                                    </asp:View>
                                    <asp:View ID="VwConfirmation" runat="server">
											<table style="width: 100%" bordercolor="#ffffff" cellspacing="1" cellpadding="2"
												width="100%" bgcolor="#dddddd" border="0">
												<tr bgcolor="#ffffff">
													<td colspan="2" align="center" style="height: 17px">
														<asp:Label ID="LblConfirmation" runat="server" meta:resourcekey="LblConfirmationResource1"></asp:Label>
													</td>
												</tr>
												<tr bgcolor="#ffffff">
													<td align="center" colspan="2">
														<asp:ImageButton ID="ImgBtnAdd" runat="server" ImageUrl="~/images/button/Ok.gif"
															CausesValidation="False" meta:resourcekey="ImgBtnAddResource1" />
													</td>
												</tr>
											</table>
										</asp:View>
                                </asp:MultiView> 
                                </ajax:AjaxPanel>
                               </td>
                            </tr>
						</table>
                    </div> 								
				</td>
			</tr>
			<tr>							
					<ajax:AjaxPanel ID="AjaxPanel14" runat="server" meta:resourcekey="AjaxPanel14Resource1">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td align="left" background="Images/button-bground.gif" valign="middle">
									<img height="1" src="Images/blank.gif" width="5" />
								</td>
								<td align="left" background="Images/button-bground.gif" valign="middle">
									<img height="15" src="images/arrow.gif" width="15" />&nbsp;
								</td>
								<td background="Images/button-bground.gif">
									&nbsp;
								</td>
								<td background="Images/button-bground.gif">
									&nbsp;
								</td>
								<td background="Images/button-bground.gif">
									&nbsp;
								</td>
								<td background="Images/button-bground.gif" width="99%">
									<img height="1" src="Images/blank.gif" width="1" />
								</td>
								<td>
									
								</td>
							</tr>
						</table>
						<asp:CustomValidator ID="CustomValidator1" runat="server" Display="None" ValidationGroup="handle"
							    meta:resourcekey="CustomValidator1Resource1"></asp:CustomValidator>
                        <asp:CustomValidator ID="CustomValidator2" runat="server" Display="None" 
                                meta:resourcekey="CustomValidator2Resource1"></asp:CustomValidator>
                        </ajax:AjaxPanel>
            </tr>	
	        </table>
    </div>
</asp:Content>

