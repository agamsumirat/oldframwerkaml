﻿Imports CDDLNPNettier.Data
Imports CDDLNPNettier.Entities
Imports AMLBLL
Imports AMLBLL.CDDLNPBLL
Imports System.Data
Imports System.Drawing

Partial Class CDDLNPDelete
    Inherits Parent

    Private ReadOnly Property GetPK_CDDLNP_ID() As Int32
        Get
            Return Request.QueryString("CID")
        End Get
    End Property

    Private ReadOnly Property GetFK_CDD_Type_ID() As Int32
        Get
            Return Request.QueryString("TID")
        End Get
    End Property

    Private ReadOnly Property GetFK_CDD_NasabahType_ID() As Int32
        Get
            Return Request.QueryString("NTID")
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not Page.IsPostBack Then
                'Audit Trail Access Page
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using

                Using tblCDDLNP As VList(Of vw_CDDLNP_View) = DataRepository.vw_CDDLNP_ViewProvider.GetPaged("PK_CDDLNP_ID = " & Me.GetPK_CDDLNP_ID & " AND CreatedBy = '" & Sahassa.AML.Commonly.SessionUserId & "'", String.Empty, 0, 1, 0)
                    If tblCDDLNP.Count > 0 Then
                        'Set CDD Customer Type
                        SessionCustomerType = tblCDDLNP(0).CDD_NasabahType_ID

                        'Set CDD Type
                        lblHeader.Text = "CDD " & tblCDDLNP(0).CDD_Type

                        Using objCDDLNP As New CDDLNPBLL
                            objCDDLNP.ConstructFormDelete(Me.TableCategory, Me.GetFK_CDD_Type_ID, Me.GetFK_CDD_NasabahType_ID, Me.GetPK_CDDLNP_ID)
                            objCDDLNP.LoadAnswerView(Me.TableCategory, Me.GetPK_CDDLNP_ID)
                            objCDDLNP.HideUncheck(Me.TableCategory)
                        End Using

                        Dim lblAMLResult As Label = CType(TableCategory.FindControl("lblAMLResult"), Label)
                        lblAMLResult.Text = tblCDDLNP(0).AMLRiskRatingPersonal
                    Else
                        Using objCDDLNP As New CDDLNPBLL
                            Me.Response.Redirect(objCDDLNP.GetCDDLNPUrl(Me.GetFK_CDD_Type_ID))
                        End Using
                    End If
                End Using
            End If
        Catch ex As Exception
            cvalPageErr.ErrorMessage = ex.Message
            cvalPageErr.IsValid = False
        End Try
    End Sub

    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Try
            Using objCDDLNP As New CDDLNPBLL
                objCDDLNP.CDDLNPDelete(Me.GetPK_CDDLNP_ID)
                ClientScript.RegisterStartupScript(Page.GetType(), "Success", "alert('" & lblHeader.Text & " data has been successfully deleted.');window.location='" & objCDDLNP.GetCDDLNPUrl(Me.GetFK_CDD_Type_ID) & "';", True)
            End Using
        Catch ex As Exception
            cvalPageErr.ErrorMessage = ex.Message
            cvalPageErr.IsValid = False
        End Try
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Try
            Using objCDDLNP As New CDDLNPBLL
                Me.Response.Redirect(objCDDLNP.GetCDDLNPUrl(Me.GetFK_CDD_Type_ID))
            End Using
        Catch ex As Exception
            cvalPageErr.ErrorMessage = ex.Message
            cvalPageErr.IsValid = False
        End Try
    End Sub
End Class