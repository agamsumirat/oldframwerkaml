﻿#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports System.Collections.Generic
Imports System.IO
Imports Sahassa.AML.Commonly
Imports AMLBLL.ValidateBLL
Imports System.Drawing
#End Region
Partial Class MsSystemParameterView
    Inherits Parent

#Region "Member"
    Private ReadOnly BindGridFromExcel As Boolean
#End Region

#Region "Property"

    Public ReadOnly Property SetnGetUserID() As String
        Get
            Return SessionPkUserId
        End Get


    End Property

    Public Property SetMsSystemParameter_Name() As String
        Get
            If Not Session("MsSystemParameter.SetMsSystemParameter_Name") Is Nothing Then
                Return CStr(Session("MsSystemParameter.SetMsSystemParameter_Name"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("MsSystemParameter.SetMsSystemParameter_Name") = value
        End Set
    End Property
    Public Property SetMsSystemParameter_Value() As String
        Get
            If Not Session("MsSystemParameter.SetMsSystemParameter_Value") Is Nothing Then
                Return CStr(Session("MsSystemParameter.SetMsSystemParameter_Value"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("MsSystemParameter.SetMsSystemParameter_Value") = value
        End Set
    End Property


    Public Property SetActivation() As String
        Get
            If Not Session("MsSystemParameter.SetActivation") Is Nothing Then
                Return CStr(Session("MsSystemParameter.SetActivation"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("MsSystemParameter.SetActivation") = value
        End Set
    End Property
    Private Property SetnGetSort() As String
        Get
            Return CType(IIf(Session("MsSystemParameter.Sort") Is Nothing, " Pk_MsSystemParameter_Id  asc", Session("MsSystemParameter.Sort")), String)
        End Get
        Set(ByVal Value As String)
            Session("MsSystemParameter.Sort") = Value
        End Set
    End Property
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return CType(IIf(Session("MsSystemParameter.SelectedItem") Is Nothing, New ArrayList, Session("MsSystemParameter.SelectedItem")), ArrayList)
        End Get
        Set(ByVal value As ArrayList)
            Session("MsSystemParameter.SelectedItem") = value
        End Set
    End Property
    Private Property SetnGetRowTotal() As Int32
        Get
            Return CType(IIf(Session("MsSystemParameter.RowTotal") Is Nothing, 0, Session("MsSystemParameter.RowTotal")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("MsSystemParameter.RowTotal") = Value
        End Set
    End Property
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return iTotalPages(SetnGetRowTotal)
        End Get
    End Property
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return CType(IIf(Session("MsSystemParameter.CurrentPage") Is Nothing, 0, Session("MsSystemParameter.CurrentPage")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("MsSystemParameter.CurrentPage") = Value
        End Set
    End Property
    Public ReadOnly Property SetnGetBindTable() As VList(Of Vw_MsSystemParameter)
        Get
            Dim strWhereClause(-1) As String
            Dim strAllWhereClause As String = ""
            If SetnGetCurrentPage > GetPageTotal - 1 And GetPageTotal - 1 > 0 Then
                SetnGetCurrentPage = GetPageTotal - 1
            End If

            If SetMsSystemParameter_Name.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = Vw_MsSystemParameterColumn.MsSystemParameter_Name.ToString & " like '%" & SetMsSystemParameter_Name.Trim.Replace("'", "''") & "%'"
            End If
            If SetMsSystemParameter_Value.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = Vw_MsSystemParameterColumn.MsSystemParameter_Value.ToString & " like '%" & SetMsSystemParameter_Value.Trim.Replace("'", "''") & "%'"
            End If


            strAllWhereClause = String.Join(" and ", strWhereClause)
            Return DataRepository.Vw_MsSystemParameterProvider.GetPaged(strAllWhereClause, SetnGetSort, SetnGetCurrentPage, GetDisplayedTotalRow, SetnGetRowTotal)




        End Get

    End Property


#End Region

#Region "Function"

    Private Sub BindSelected()
        Dim Rows As New ArrayList

        For Each IdDIN As Int64 In SetnGetSelectedItem
            Using rowData As VList(Of Vw_MsSystemParameter) = DataRepository.Vw_MsSystemParameterProvider.GetPaged(" Pk_MsSystemParameter_Id = " & IdDIN & "", "", 0, Integer.MaxValue, 0)
                If rowData.Count > 0 Then
                    Rows.Add(rowData(0))
                End If
            End Using
        Next

        GridMsUserView.DataSource = Rows
        GridMsUserView.AllowPaging = False
        GridMsUserView.DataBind()
        For i As Integer = 0 To GridMsUserView.Items.Count - 1
            For y As Integer = 0 To GridMsUserView.Columns.Count - 1
                GridMsUserView.Items(i).Cells(y).Attributes.Add("class", "text")
            Next
        Next

        HideButtonGrid()

    End Sub
    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In GridMsUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = CType(gridRow.FindControl("CheckBoxExporttoExcel"), CheckBox)
                Dim PKMsUserId As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = SetnGetSelectedItem
                If ArrTarget.Contains(PKMsUserId) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(PKMsUserId)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(PKMsUserId)
                End If
                SetnGetSelectedItem = ArrTarget
            End If
        Next
    End Sub

    Private Sub CollectSelected2()
        Dim i As Integer = 0
        Dim j As Integer = 0
        For Each gridRow As DataGridItem In GridMsUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = CType(gridRow.FindControl("CheckBoxExporttoExcel"), CheckBox)
                j = j + 1
                If chkBox.Checked = False Then
                    i = i + 1
                End If
                Dim PKMsUserId As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = SetnGetSelectedItem
                If ArrTarget.Contains(PKMsUserId) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(PKMsUserId)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(PKMsUserId)
                End If
                SetnGetSelectedItem = ArrTarget
            End If
        Next

        If i = j Then
            Throw New Exception("No Data Selected")
        End If
    End Sub
    Private Sub SettingControlSearching()
        txtMsSystemParameter_Name.Text = SetMsSystemParameter_Name
        txtMsSystemParameter_Value.Text = SetMsSystemParameter_Value



    End Sub
    Private Sub SettingPropertySearching()
        SetnGetCurrentPage = 0
        SetMsSystemParameter_Name = txtMsSystemParameter_Name.Text.Trim
        SetMsSystemParameter_Value = txtMsSystemParameter_Value.Text.Trim


    End Sub

    Private Sub ClearThisPageSessions()
        Clearproperty()
        LblMessage.Visible = False
        LblMessage.Text = ""
        SetnGetRowTotal = Nothing
        SetnGetSort = Nothing
        SetnGetCurrentPage = 0
    End Sub

    Private Sub Clearproperty()
        SetMsSystemParameter_Name = Nothing
        SetMsSystemParameter_Value = Nothing

        SetActivation = 2
    End Sub

    Private Sub bindgrid()
        SettingControlSearching()
        GridMsUserView.DataSource = SetnGetBindTable
        GridMsUserView.CurrentPageIndex = SetnGetCurrentPage
        GridMsUserView.VirtualItemCount = SetnGetRowTotal
        GridMsUserView.DataBind()
        If SetnGetRowTotal > 0 Then
            LabelNoRecordFound.Visible = False
        Else
            LabelNoRecordFound.Visible = True
        End If
    End Sub

#End Region

#Region "events..."



    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try

            LblMessage.Text = ""
            LblMessage.Visible = False
            If Not Page.IsPostBack Then
                ClearThisPageSessions()
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
            End If
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : SetnGetCurrentPage = 0
                Case "Prev" : SetnGetCurrentPage -= 1
                Case "Next" : SetnGetCurrentPage += 1
                Case "Last" : SetnGetCurrentPage = GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
            CollectSelected()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    Protected Sub ImgBtnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnSearch.Click

        Try
            SettingPropertySearching()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub
    Private Sub SetInfoNavigate()
        PageCurrentPage.Text = (SetnGetCurrentPage + 1).ToString
        PageTotalPages.Text = GetPageTotal.ToString
        PageTotalRows.Text = SetnGetRowTotal.ToString
        LinkButtonNext.Enabled = (Not SetnGetCurrentPage + 1 = GetPageTotal) AndAlso GetPageTotal <> 0
        LinkButtonLast.Enabled = (Not SetnGetCurrentPage + 1 = GetPageTotal) AndAlso GetPageTotal <> 0
        First.Enabled = Not SetnGetCurrentPage = 0
        LinkButtonPrevious.Enabled = Not SetnGetCurrentPage = 0
    End Sub
    Private Sub SetCheckedAll()
        Dim i As Int16 = 0
        Dim TotalRow As Int16 = 0
        For Each gridRow As DataGridItem In GridMsUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = CType(gridRow.FindControl("CheckBoxExporttoExcel"), CheckBox)
                If chkBox.Checked Then
                    i = CType(i + 1, Int16)
                End If
                TotalRow = CType(TotalRow + 1, Int16)
            End If
        Next
        If TotalRow = 0 Then
            CheckBoxSelectAll.Checked = False
        Else
            If i = TotalRow Then
                CheckBoxSelectAll.Checked = True
            Else
                CheckBoxSelectAll.Checked = False
            End If
        End If
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            bindgrid()
            SetInfoNavigate()
            SetCheckedAll()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImgBtnClearSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnClearSearch.Click
        Try
            Clearproperty()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        'For Each gridRow As DataGridItem In GridMsUserView.Items
        '    If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
        '        Dim pkid As String = gridRow.Cells(1).Text
        '        Dim ArrTarget As ArrayList = SetnGetSelectedItem
        '        If SetnGetSelectedItem.Contains(pkid) Then
        '            If CheckBoxSelectAll.Checked Then
        '                If Not ArrTarget.Contains(pkid) Then
        '                    ArrTarget.Add(pkid)
        '                End If
        '            Else
        '                ArrTarget.Remove(pkid)
        '            End If
        '        Else
        '            If CheckBoxSelectAll.Checked Then
        '                If Not ArrTarget.Contains(pkid) Then
        '                    ArrTarget.Add(pkid)
        '                End If
        '            End If
        '        End If
        '        SetnGetSelectedItem = ArrTarget
        '    End If
        'Next
        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        For Each gridRow As DataGridItem In Me.GridMsUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(1).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItem.Contains(StrKey) = False Then
                        ArrTarget.Add(StrKey)
                    End If
                Else
                    If Me.SetnGetSelectedItem.Contains(StrKey) Then
                        ArrTarget.Remove(StrKey)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItem = ArrTarget
    End Sub


    Protected Sub GridMsUserView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridMsUserView.ItemDataBound
        Try
            Dim CollCount As Integer = e.Item.Cells.Count - 1
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Dim chkBox As CheckBox = CType(e.Item.FindControl("CheckBoxExporttoExcel"), CheckBox)
                chkBox.Checked = SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)
                If BindGridFromExcel = True Then
                    e.Item.Cells(2).Text = CStr((e.Item.ItemIndex + 1))
                Else
                    e.Item.Cells(2).Text = CStr((e.Item.ItemIndex + 1) + (SetnGetCurrentPage * GetDisplayedTotalRow))
                End If

                'Show mapping item
                'Dim StringMaping As New StringBuilder
                'Dim L_O As TList(Of )
                'L_O = DataRepository.Provider.GetPaged(Column.Pk_MsSystemParameter_Id.ToString & "= " & e.Item.Cells(1).Text, "", 0, Integer.MaxValue, Nothing)
                'If L_O.Count > 0 Then
                '	Dim countField As Integer = 7
                '	Dim lengthField As Integer = 0
                '	For Each idx As  In L_O.FindAllDistinct("")
                '		With idx
                '			If lengthField > countField Then
                '				StringMaping.AppendLine(idx.Nama & "(" & idx..GetValueOrDefault & ");")
                '				lengthField = 0
                '			Else
                '				StringMaping.Append(idx.Nama & "(" & idx..GetValueOrDefault & ");")
                '				lengthField += 1
                '			End If

                '		End With
                '	Next
                'Else
                '	StringMaping.Append("-")
                'End If
                'e.Item.Cells(CollCount - 5).Text = StringMaping.ToString

                '   Try
                '	If e.Item.Cells(CollCount - 4).Text = True Then
                '		e.Item.Cells(CollCount - 4).Text = "Active"
                '	Else
                '		e.Item.Cells(CollCount - 4).Text = "inActive"
                '	End If
                'Catch ex As Exception
                '	e.Item.Cells(CollCount - 4).Text = "-"
                'End Try

                'Using CekMsSystemParameter_ApprovalDetail As TList(Of MsSystemParameter_ApprovalDetail) = DataRepository.MsSystemParameter_ApprovalDetailProvider.GetPaged("Pk_MsSystemParameter_Id = '" & cint(e.Item.Cells(1).Text) & "'", "", 0, Integer.MaxValue, 0)
                '  If CekMsSystemParameter_ApprovalDetail.Count > 0 Then
                '			e.Item.Cells(CollCount - 3).Text = "Pending Approval"
                '			e.Item.Cells(CollCount - 1).Enabled = False
                '			e.Item.Cells(CollCount).Enabled = False

                '	   End If

                'End Using
            End If
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub
    Protected Sub GridMsUserView_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridMsUserView.SortCommand
        Dim GridUser As DataGrid = CType(source, DataGrid)
        Try
            SetnGetSort = ChangeSortCommand(e.SortExpression)
            GridUser.Columns(IndexSort(GridUser, e.SortExpression)).SortExpression = SetnGetSort
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub HideButtonGrid()
        With GridMsUserView
            .Columns(.Columns.Count - 4).Visible = False
            .Columns(.Columns.Count - 3).Visible = False
            .Columns(.Columns.Count - 2).Visible = False
            .Columns(.Columns.Count - 1).Visible = False
        End With
    End Sub
    Private Sub BindSelectedAll()
        Try
            GridMsUserView.DataSource = DataRepository.Vw_MsSystemParameterProvider.GetAll
            GridMsUserView.AllowPaging = False
            GridMsUserView.DataBind()

            For i As Integer = 0 To GridMsUserView.Items.Count - 1
                For y As Integer = 0 To GridMsUserView.Columns.Count - 1
                    GridMsUserView.Items(i).Cells(y).Attributes.Add("class", "text")
                Next
            Next
            HideButtonGrid()
        Catch
            Throw
        End Try


    End Sub
    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                    'Skip
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub
    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click

        Try

            Me.CollectSelected2()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelected()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=SystemParameterView.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridMsUserView)
            GridMsUserView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()

        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            Me.CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) AndAlso (CInt(Me.TextGoToPage.Text) > 0) Then
                If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                    Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                Else
                    'Throw New Exception("SahassaCommonly.CommonlyEnum.EXECPTION_PAGENUMBER_GREATER_TOTALPAGENUMBER")
                    Throw New Exception("Page number must be less than or equal to the total page count.")
                End If
            Else
                'Throw New Exception("SahassaCommonly.CommonlyEnum.EXECPTION_PAGENUMBER_NUMERICPOSITIF")
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If
            CollectSelected()
        Catch ex As Exception
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    Protected Sub CvalHandleErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles CvalHandleErr.PreRender
        If CvalHandleErr.IsValid = False Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
        End If
    End Sub


    'Protected Sub CvalPageErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles CvalPageErr.PreRender
    '    If CvalPageErr.IsValid = False Then
    '        Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
    '    End If
    'End Sub

    Protected Sub lnkExportAllData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExportAllData.Click
        Try

            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelectedAll()
            Response.Clear()
            Response.ClearHeaders()
            Response.Charset = ""
            Response.AddHeader("content-disposition", "attachment;filename=SystemParameterView.xls")
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridMsUserView)
            GridMsUserView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub


#End Region

End Class


