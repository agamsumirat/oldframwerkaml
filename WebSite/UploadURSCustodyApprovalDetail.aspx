<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="UploadURSCustodyApprovalDetail.aspx.vb" Inherits="UploadUSRCustodyApprovalDetail" title="Untitled Page" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    <table cellspacing="4" cellpadding="0" width="100%">
        <tr>
            <td valign="bottom" align="left">
                <img height="15" src="images/dot_title.gif" width="15"></td>
            <td class="maintitle" valign="bottom" width="99%">
                <asp:Label ID="LabelTitle" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td align="left" valign="bottom">
            </td>
            <td class="maintitle" valign="bottom" width="99%">
                Activity :<asp:Label ID="LabelActivity" runat="server"></asp:Label></td>
        </tr>
    </table>
    <table style="width: 100%" id="TableDetail" runat="server">
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" 
                    CellPadding="4" EnableModelValidation="True" ForeColor="Black" 
                    GridLines="Vertical" AutoGenerateColumns="False">
                    <AlternatingRowStyle BackColor="White" />
                    <FooterStyle BackColor="#CCCC99" />
                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                    <RowStyle BackColor="#F7F7DE" />
                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                    <Columns>
                        <asp:BoundField DataField="TanggalHolding" DataFormatString="{0: dd-MMM-yyyy}" HeaderText="Tanggal Holding"
                            HtmlEncode="False" />
                        <asp:BoundField DataField="NamaInvestor" HeaderText="Nama Investor" />
                        <asp:BoundField DataField="CIF" HeaderText="CIF" />
                        <asp:BoundField DataField="Fund_ID" HeaderText="Fund_ID" />
                        <asp:BoundField DataField="NamaFund" HeaderText="NamaFund" />
                        <asp:BoundField DataField="Unit" HeaderText="Unit" />
                        <asp:BoundField DataField="NAVPerUnit" HeaderText="NAVPerUnit" />
                        <asp:BoundField DataField="Amount" HeaderText="Amount" />
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        
    </table>
    <table width="100%">
           <TR class="formText" id="Button11" bgColor="#dddddd" height="30" >
		    <TD style="width: 22px"><IMG height="15" src="images/arrow.gif" width="15"></TD>
		    <TD colSpan="7">
			    <TABLE cellSpacing="0" cellPadding="3" border="0">
				    <TR>
					    <TD><asp:imagebutton id="ImageAccept" runat="server" CausesValidation="True" SkinID="AcceptButton"></asp:imagebutton></TD>
					    <TD><asp:imagebutton id="ImageReject" runat="server" CausesValidation="False" SkinID="RejectButton"></asp:imagebutton></TD>
					    <td ><asp:imagebutton id="ImageBack" runat="server" 
                                CausesValidation="False" SkinID="BackButton"></asp:imagebutton></td>
				    </TR>
			    </TABLE>
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="none"></asp:CustomValidator></TD>
	    </TR>
	    </table> 
</asp:Content>

