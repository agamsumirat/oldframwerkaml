#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports AMLBLL.AuditTrailBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports Sahassa.AML
Imports System.Collections.Generic
#End Region

Partial Class MsKelurahan_UploadApprovalDetail_Detail
    Inherits Parent

#Region "Function"

    Sub HideControl()
        ImageCancel.Visible = False
    End Sub

    Sub ChangeMultiView(index As Integer)
        MtvMsUser.ActiveViewIndex = index
    End Sub

#End Region

#Region "events..."

    Protected Sub ImageCancel_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Dim LngId As Long = 0
        Using ObjMsKelurahan_ApprovalDetail As TList(Of MsKelurahan_ApprovalDetail) = DataRepository.MsKelurahan_ApprovalDetailProvider.GetPaged("PK_MsKelurahan_ApprovalDetail_Id = " & parID, "", 0, Integer.MaxValue, 0)
            If ObjMsKelurahan_ApprovalDetail.Count > 0 Then
                LngId = ObjMsKelurahan_ApprovalDetail(0).FK_MsKelurahan_Approval_Id
            End If
        End Using
        Response.Redirect("MsKelurahan_UploadApprovalDetail_View.aspx?ID=" & LngId)
    End Sub






    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                'AuditTrailBLL.InsertAuditTrailUserAccess(SessionCurrentPage)
                ListmapingNew = New TList(Of MappingMsKelurahanNCBSPPATK_Approval_Detail)
                ListmapingOld = New TList(Of MappingMsKelurahanNCBSPPATK)
                LoadData()
            Catch ex As Exception
                LogError(ex)
                CvalPageErr.IsValid = False
                CvalPageErr.ErrorMessage = ex.Message
            End Try
        End If

    End Sub

    Private Sub LoadData()
        Dim PkObject As String
        'Load new data
        Using ObjMsKelurahan_ApprovalDetail As MsKelurahan_ApprovalDetail = DataRepository.MsKelurahan_ApprovalDetailProvider.GetPaged(MsKelurahan_ApprovalDetailColumn.PK_MsKelurahan_ApprovalDetail_Id.ToString & _
            "=" & _
            parID, "", 0, 1, Nothing)(0)
            With ObjMsKelurahan_ApprovalDetail
                SafeDefaultValue = "-"
               HFKecamatanNew.value = Safe(.IDKecamatan)
dim OKecamatan as  MsKecamatan = Datarepository.MsKecamatanProvider.GetByIDKecamatan(.IDKecamatan.GetValueOrDefault)
if OKecamatan isnot nothing then LBSearchNamaKecamatanNew.text = safe(OKecamatan.NamaKecamatan)

txtIDKelurahanNew.Text = Safe(.IDKelurahan)
txtNamaKelurahanNew.Text = Safe(.NamaKelurahan)

                'other info
                Dim Omsuser As TList(Of User)
                Omsuser = DataRepository.UserProvider.GetByUserID(.CreatedBy)
                If Omsuser.Count > 0 Then
                    lblCreatedByNew.Text = Omsuser(0).UserName
                Else
                    lblCreatedByNew.Text = SafeDefaultValue
                End If
                lblCreatedDateNew.Text = FormatDate(.CreatedDate)
                Omsuser = DataRepository.UserProvider.GetByUserID(.LastUpdatedBy)
                If Omsuser.Count > 0 Then
                    lblUpdatedbyNew.Text = Omsuser(0).UserName
                Else
                    lblUpdatedbyNew.Text = SafeDefaultValue
                End If
                lblUpdatedDateNew.Text = FormatDate(.LastUpdatedDate)
                'txtActivationNew.Text = SafeActiveInactive(.Activation)
                Dim L_objMappingMsKelurahanNCBSPPATK_Approval_Detail As TList(Of MappingMsKelurahanNCBSPPATK_Approval_Detail)
                L_objMappingMsKelurahanNCBSPPATK_Approval_Detail = DataRepository.MappingMsKelurahanNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsKelurahanNCBSPPATK_Approval_DetailColumn.IDKelurahan.ToString & _
                 "=" & _
                 ObjMsKelurahan_ApprovalDetail.IDKelurahan, "", 0, Integer.MaxValue, Nothing)
              
                'Add mapping
                ListmapingNew.AddRange(L_objMappingMsKelurahanNCBSPPATK_Approval_Detail)
               
            End With
            PkObject = ObjMsKelurahan_ApprovalDetail.IDKelurahan
        End Using



        'Load Old Data
        Using objMsKelurahan As MsKelurahan = DataRepository.MsKelurahanProvider.GetByIDKelurahan(PkObject)
            If objMsKelurahan Is Nothing Then
                lamaOld.Visible = False
                lama.Visible = False
                Return
            End If

            With objMsKelurahan
                SafeDefaultValue = "-"
               HFKecamatanOld.value = Safe(.IDKecamatan)
dim OKecamatan as  MsKecamatan = Datarepository.MsKecamatanProvider.GetByIDKecamatan(.IDKecamatan.GetValueOrDefault)
if OKecamatan isnot nothing then LBSearchNamaKecamatanOld.text = safe(OKecamatan.NamaKecamatan)

txtIDKelurahanOld.Text = Safe(.IDKelurahan)
txtNamaKelurahanOld.Text = Safe(.NamaKelurahan)

                
                'other info
                Dim Omsuser As TList(Of User)
                Omsuser = DataRepository.UserProvider.GetByUserID(.CreatedBy)
                If Omsuser.Count > 0 Then
                    lblCreatedByOld.Text = Omsuser(0).UserName
                Else
                    lblCreatedByOld.Text = SafeDefaultValue
                End If
                lblCreatedDateNew.Text = FormatDate(.CreatedDate)
                Omsuser = DataRepository.UserProvider.GetByUserID(.LastUpdatedBy)
                If Omsuser.Count > 0 Then
                    lblUpdatedbyOld.Text = Omsuser(0).UserName
                Else
                    lblUpdatedbyOld.Text = SafeDefaultValue
                End If
                lblUpdatedDateOld.Text = FormatDate(.LastUpdatedDate)
                Dim l_objMappingMsKelurahanNCBSPPATK As TList(Of MappingMsKelurahanNCBSPPATK)
                'txtNamaKelurahanOld.Text = Safe(.NamaKelurahan)
                l_objMappingMsKelurahanNCBSPPATK = DataRepository.MappingMsKelurahanNCBSPPATKProvider.GetPaged(MappingMsKelurahanNCBSPPATKColumn.IDKelurahan.ToString & _
                   "=" & _
                   PkObject, "", 0, Integer.MaxValue, Nothing)
               
                ListmapingOld.AddRange(l_objMappingMsKelurahanNCBSPPATK)
            End With
        End Using
    End Sub






    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            BindListMapping()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub BindListMapping()
        'bind new value
        LBMappingNew.DataSource = ListMappingDisplayNew '(0).PK_MappingMsBentukBidangUsahaNCBSPPATK_Id
        LBMappingNew.DataBind()
        'Bind OldValue
        LBmappingOld.DataSource = ListMappingDisplayOld
        LBmappingOld.DataBind()
    End Sub

#End Region

#Region "Property..."

    Public ReadOnly Property parID As String
        Get
            Return Request.Item("ID")
        End Get
    End Property
    ''' <summary>
    ''' Menyimpan Item untuk mapping New sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ListmapingNew As TList(Of MappingMsKelurahanNCBSPPATK_Approval_Detail)
        Get
            Return Session("ListmapingNew.data")
        End Get
        Set(ByVal value As TList(Of MappingMsKelurahanNCBSPPATK_Approval_Detail))
            Session("ListmapingNew.data") = value
        End Set
    End Property

    ''' <summary>
    ''' Menyimpan Item untuk mapping Old sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ListmapingOld As TList(Of MappingMsKelurahanNCBSPPATK)
        Get
            Return Session("ListmapingOld.data")
        End Get
        Set(ByVal value As TList(Of MappingMsKelurahanNCBSPPATK))
            Session("ListmapingOld.data") = value
        End Set
    End Property

    ''' <summary>
    ''' List yang tampil di ListBox New
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplayNew() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingMsKelurahanNCBSPPATK_Approval_Detail In ListmapingNew.FindAllDistinct("IDKelurahanNCBS")
                Temp.Add(i.IDKelurahanNCBS.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property

    ''' <summary>
    ''' List yang tampil di ListBox Old
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplayOld() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingMsKelurahanNCBSPPATK In ListmapingOld.FindAllDistinct("IDKelurahanNCBS")
                Temp.Add(i.IDKelurahanNCBS.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property
#End Region



End Class



