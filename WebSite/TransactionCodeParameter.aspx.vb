Imports System.IO
Imports System

Partial Class TransactionCodeParameter
    Inherits Parent
    Private Shared ArrayListExcludeFile As New System.Collections.ArrayList

    ''' <summary>
    ''' Fill Selected File
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub FillSelectedFile()
        Try
            Me.ListSelectedFile.Items.Clear()
            Dim flag As Boolean = 0
            Using AccessFileSecurity As New AMLDAL.CTRExemptionListTableAdapters.TransactionCodeMasterTableAdapter
                For Each RowFileSecurity As AMLDAL.CTRExemptionList.TransactionCodeMasterRow In AccessFileSecurity.GetData
                    Me.ListSelectedFile.Items.Add( _
                    RowFileSecurity.TransactionCode & "-" & _
                    RowFileSecurity.DebitORCredit & "-" & _
                    RowFileSecurity.TransactionDescription)
                    flag = 1
                Next
                If flag = 0 Then
                    Me.ListSelectedFile.Items.Clear()
                End If
            End Using
        Catch
            Throw
        End Try
    End Sub

    ''' <summary>
    ''' FillAvailableFile()
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub FillAvailableFile()
        Dim ObjArraylist As New ArrayList
        Try
            Me.ListAvailableFile.Items.Clear()
            Dim x As Integer
            Using AccessTransactionCode As New AMLDAL.CTRExemptionListTableAdapters.TransactionCodeTableAdapter
                Using TableTransactionCode As AMLDAL.CTRExemptionList.TransactionCodeDataTable = AccessTransactionCode.GetData
                    If TableTransactionCode.Rows.Count > 0 Then
                        Dim TransactionCodeRow As AMLDAL.CTRExemptionList.TransactionCodeRow
                        For x = 0 To TableTransactionCode.Rows.Count - 1
                            TransactionCodeRow = TableTransactionCode.Rows(x)
                            ListAvailableFile.Items.Add( _
                            TransactionCodeRow.TransactionCode & "-" & _
                            TransactionCodeRow.DebitORCredit & "-" & _
                            Replace(Replace(TransactionCodeRow.TransactionDescription, "<", ""), ">", ""))
                        Next
                    Else
                    End If
                End Using
            End Using

        Catch
            Throw
        Finally
            ObjArraylist.Clear()
            ObjArraylist = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.lblSuccess.Visible = False
            If Not Page.IsPostBack Then
                FillSelectedFile()
                FillAvailableFile()
            End If
            Me.Page.GetPostBackEventReference(Me)

            Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

            Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    Transcope.Complete()
                End Using
            End Using
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' Add File Security
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAdd.Click
        Dim ObjListItem As ListItem
        Dim ObjArrayRemove As New ArrayList
        Dim i As Integer
        Dim j As Integer = 0
        Try
            For Each ObjListItem In ListAvailableFile.Items
                If ObjListItem.Selected Then
                    If Not ListSelectedFile.Items.Contains(ObjListItem) Then
                        ListSelectedFile.Items.Add(ListAvailableFile.Items(j))
                        ObjArrayRemove.Add(ListAvailableFile.Items(j))
                    End If
                End If
                j += 1
            Next
            For i = 0 To ObjArrayRemove.Count - 1
                ListAvailableFile.Items.Remove(ObjArrayRemove(i))
            Next
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            ObjArrayRemove = Nothing
            ObjListItem = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Remove FileSecurity
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageRemove_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageRemove.Click
        If ListSelectedFile.Items.Count > 0 Then
            Dim ObjItem As ListItem
            Dim ObjArrayRemove As New ArrayList
            Dim i As Integer
            Try
                For Each ObjItem In ListSelectedFile.Items
                    If ObjItem.Selected Then
                        ObjArrayRemove.Add(ObjItem)
                    End If
                Next
                For i = 0 To ObjArrayRemove.Count - 1
                    ListSelectedFile.Items.Remove(ObjArrayRemove(i))
                    ListAvailableFile.Items.Add(ObjArrayRemove(i))
                Next
            Catch ex As Exception
                Me.cvalPageErr.IsValid = False
                Me.cvalPageErr.ErrorMessage = ex.Message
                LogError(ex)
            Finally
                ObjItem = Nothing
                ObjArrayRemove = Nothing
            End Try
        End If
    End Sub

    ''' <summary>
    ''' Save File Security
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Try
            If Page.IsValid Then
                Dim transactionCode As String = ""
                Dim DebitOrCredit As String = ""
                Dim Description As String = ""
                Dim myString As String = ""
                Dim abc As String = ""
                'jika Super User
                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then

                    'Tambahkan ke dalam tabel TransactionCode_PendingApproval dengan ModeID = 3 (Delete) 
                    Using TransactionCodePendingApproval As New AMLDAL.CTRExemptionListTableAdapters.TransactionCodeMasterTableAdapter
                        Dim x As Integer

                        Using TransScope As New Transactions.TransactionScope()
                            Using TableCodeMaster As AMLDAL.CTRExemptionList.TransactionCodeMasterDataTable = TransactionCodePendingApproval.GetData

                                'update data pada tabel transaction code master
                                'delete data pada tabel transaction code pending approval
                                For Each RowFileSecurity As AMLDAL.CTRExemptionList.TransactionCodeMasterRow In TransactionCodePendingApproval.GetData
                                    TransactionCodePendingApproval.DeleteByTransactionCode(RowFileSecurity.TransactionCodeParameterId)
                                Next

                                'insert data baru pada tabel transaction code master & audit trail
                                Using AccessAuditTrail As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                                    For x = 0 To ListSelectedFile.Items.Count - 1
                                        ListSelectedFile.SelectedIndex = x
                                        myString = ListSelectedFile.SelectedValue
                                        Dim y As Integer = 0
                                        myString = ""
                                        While ListSelectedFile.SelectedValue.Substring(y, 1) <> "-"
                                            myString = myString & ListSelectedFile.SelectedValue.Substring(y, 1)
                                            y = y + 1
                                        End While
                                        transactionCode = myString
                                        y = y + 1
                                        DebitOrCredit = ListSelectedFile.SelectedValue.Substring(y, 1)
                                        y = y + 2
                                        Description = ListSelectedFile.SelectedValue.Substring(y)
                                        '                                        abc = abc & transactionCode & DebitOrCredit & Description
                                        TransactionCodePendingApproval.Insert("DD", transactionCode, DebitOrCredit, Now, Description)
                                        AccessAuditTrail.Insert(Now, Nothing, Sahassa.AML.Commonly.SessionUserId, "TransactionCodeParameter", "TransactionCode", "Add", "", transactionCode, "Accepted")
                                        AccessAuditTrail.Insert(Now, Nothing, Sahassa.AML.Commonly.SessionUserId, "TransactionCodeParameter", "DebitOrCredit", "Add", "", DebitOrCredit, "Accepted")
                                        AccessAuditTrail.Insert(Now, Nothing, Sahassa.AML.Commonly.SessionUserId, "TransactionCodeParameter", "Description", "Add", "", Description, "Accepted")
                                    Next
                                End Using

                            End Using
                            TransScope.Complete()
                        End Using
                    End Using

                Else ' jika bukan super user 

                    'Tambahkan ke dalam tabel TransactionCode_PendingApproval dengan ModeID = 2 (Edit)
                    Using TransactionCodePendingApproval As New AMLDAL.CTRExemptionListTableAdapters.TransactionCodePendingApprovalTableAdapter
                        TransactionCodePendingApproval.Insert(Sahassa.AML.Commonly.SessionUserId, Now, "edit", 2)

                        Dim CTRPending_Id As Int64
                        Dim x As Integer

                        'Tambahkan ke dalam tabel TransactionCode_Approval dengan ModeID = 2 (Edit)
                        Using TransScope As New Transactions.TransactionScope()
                            Using TableCTRExemption As AMLDAL.CTRExemptionList.TransactionCodePendingApprovalDataTable = TransactionCodePendingApproval.GetData
                                Dim TableRowCTRExemption As AMLDAL.CTRExemptionList.TransactionCodePendingApprovalRow = TableCTRExemption.Rows(TableCTRExemption.Rows.Count - 1)
                                CTRPending_Id = TransactionCodePendingApproval.TransactionCodePending_GetPendingId
                                Using TransactionCodeApproval As New AMLDAL.CTRExemptionListTableAdapters.TransactionCodeApprovalTableAdapter
                                    For x = 0 To ListSelectedFile.Items.Count - 1
                                        ListSelectedFile.SelectedIndex = x
                                        myString = ListSelectedFile.SelectedValue
                                        Dim y As Integer = 0
                                        myString = ""
                                        While ListSelectedFile.SelectedValue.Substring(y, 1) <> "-"
                                            myString = myString & ListSelectedFile.SelectedValue.Substring(y, 1)
                                            y = y + 1
                                        End While
                                        transactionCode = myString
                                        y = y + 1
                                        DebitOrCredit = ListSelectedFile.SelectedValue.Substring(y, 1)
                                        y = y + 2
                                        Description = ListSelectedFile.SelectedValue.Substring(y)
                                        '                                        abc = abc & transactionCode & DebitOrCredit & Description
                                        TransactionCodeApproval.Insert(CTRPending_Id, "DD", transactionCode, Description, DebitOrCredit)
                                    Next
                                End Using
                            End Using
                            TransScope.Complete()
                        End Using
                    End Using

                    Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=81303"
                    Me.Response.Redirect("MessagePending.aspx?MessagePendingID=81303", False)

                End If
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Me.Response.Redirect("Default.aspx", False)
    End Sub
End Class
