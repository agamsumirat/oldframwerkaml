<%@ Page Language="VB" AutoEventWireup="false" CodeFile="DisplayAllNews.aspx.vb" Inherits="DisplayAllNews" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<%@ Register Src="webcontrol/footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>:: Anti Money Laundering ::</title>
	<script src="script/x_load.js" type="text/javascript"></script>
	<script language="javascript">xInclude('script/x_core.js');</script>
    <link href="theme/aml.css" rel="stylesheet" type="text/css">
    <script src="script/aml.js" ></script>	
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	</HEAD>
	<body bgColor="#ffffff" leftMargin="0" topMargin="0" marginheight="0" marginwidth="0">
	 <form id="form1" runat="server">
	 <table background="images/main-header-bg.gif" height="81" width="100%" cellpadding="0" cellspacing="0" border="0">
  	    <tr>
            <td width="153"><img src="images/main-uang-dijemur-dan-logo.jpg" width="153" height="81" /></td>
  	        <td width="357"><img src="images/main-title.jpg" width="357" height="25" /></td>
  	        <td width="99%">&nbsp;</td>
  	        <td align="right" background="images/main-bar-merah-kanan.jpg" valign="top" class="toprightmenu"><img src="images/blank.gif" width="425" height="6"><br>
                  </td>               
  	    </tr>
  	</table> 
    <table id="tablebasic" style="height: 184px" runat="server" width="100%" cellpadding="0" cellspacing="0" border="0" >
        <tr>
            <td align="left" colspan="1" style="width: 10px">
                &nbsp;&nbsp; &nbsp;</td>
            <td colspan="3" align="left" style="width: 638px" id="tdcontent" valign="top">
            <div id="divcontent" class="divcontent" >
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Repeater ID="NewsRepeater" runat="server">
                                <ItemTemplate>
                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td style="background-color:Gray">&nbsp;</td>
                                        </tr>                                    
                                        <tr>
                                            <td style="font-size:20px"><%#Eval("NewsTitle")%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><%#Eval("NewsCreatedDate")%></td>
                                        </tr>
                                        <tr>
                                            <td>
                                            
                                            
                                                <asp:Label ID="TextNewsSummary" runat="server" Text='<%#Eval("NewsSummary")%>'></asp:Label>
                                            
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><br>
                                    [<asp:LinkButton ID="ShowMoreNews" runat="server" CommandArgument='<%# Eval("NewsID") %>'
                                        CommandName="ShowMoreNews">Read More...</asp:LinkButton>]<br><br></td>
                                        </tr>                                        
                                    </table>        
                                </ItemTemplate>
                                <SeparatorTemplate>
                                    <hr />
                                </SeparatorTemplate>
                            </asp:Repeater>
                            <br />
                            <hr />
                            &nbsp;<asp:HyperLink ID="HyperLinkBack" runat="server" NavigateUrl="~/Login.aspx" Target="_self">[Back to Login Page]</asp:HyperLink>                        
                        </td>
                    </tr>
                </table>
                </div> 
                </td>                         
        </tr>
     <%--   <tr>
            <td  colspan="4" background="images/main-footer-bgV2.gif" height="24" align="right" class="logintext" style="padding-right:10px; color: #ffffff;">Copyright &copy; Bank Niaga 2007, All Right Reserved - Version 1.0.0 (Build 1235) - Developed by Sahassa</td>
        </tr>    --%> 
        <tr>
            <td align="left" colspan="4" valign="top">
            <uc1:footer ID="Footer1" runat="server" />
            </td>
        </tr>
    </table>
    	</form>
<script>arrangeViewNoMenu();
window.onresize=arrangeViewNoMenu;
</script>
	</body>	
</HTML>