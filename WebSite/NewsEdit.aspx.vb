Imports System.Windows.Forms
Imports System.Data
Imports System.Data.SqlClient
Imports Sahassa.AML.TableAdapterHelper
Imports SahassaNettier.Data
Partial Class NewsEdit
    Inherits Parent

    ''' <summary>
    ''' get user id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetNewsId() As String
        Get
            Return Me.Request.Params("NewsID")
        End Get
    End Property


    ''' <summary>
    ''' cancel handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Using adapter As New AMLDAL.AMLDataSetTableAdapters.NewsAttachmentTableAdapter
            adapter.DeleteNewsAttachmentbyUserId(Sahassa.AML.Commonly.SessionPkUserId)
        End Using
        Sahassa.AML.Commonly.SessionIntendedPage = "NewsView.aspx"
        Me.Response.Redirect("NewsView.aspx", False)
    End Sub

    ''' <summary>
    ''' update handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageUpdate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageUpdate.Click
        Dim oSQLTrans As SqlTransaction = Nothing
        Page.Validate()

        If Page.IsValid Then
            Try
                'Buat variabel untuk menampung nilai-nilai baru
                Dim NewsID As String = Me.LabelNewsID.Text
                Dim NewsTitle As String = Trim(Me.TextNewsTitleCK.Text)

                'Jika tidak ada perubahan NewsTitle
                If NewsTitle = ViewState("NewsTitle_Old") Then
                    GoTo Add
                Else 'Jika ada perubahan NewsTitle 

                    Dim counter As Int32
                    'Periksa apakah NewsTitle yg baru tsb sdh ada dalam tabel News atau belum
                    Using AccessNews As New AMLDAL.AMLDataSetTableAdapters.NewsTableAdapter
                        counter = AccessNews.CountMatchingNews(NewsTitle)
                    End Using

                    'Counter = 0 berarti NewsTitle tersebut belum pernah ada dalam tabel LevelType
                    If counter = 0 Then
Add:
                        'Periksa apakah NewsTitle tsb dalam status pending approval atau tidak
                        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.News_ApprovalTableAdapter
                            oSQLTrans = BeginTransaction(AccessPending, IsolationLevel.ReadUncommitted)
                            counter = AccessPending.CountNewsApprovalByNewsTitle(NewsTitle)

                            'Counter = 0 berarti NewsTitle yg baru tsb tidak dalam status pending approval
                            If counter = 0 Then
                                Dim NewsSummary As String
                                If Me.TextNewsSummary.Text.Length <= 255 Then
                                    NewsSummary = Me.TextNewsSummary.Text
                                Else
                                    NewsSummary = Me.TextNewsSummary.Text.Substring(0, 255)
                                End If

                                Dim NewsContent As String
                                If Me.TextNewsContent.Text.Length <= 8000 Then
                                    NewsContent = Me.TextNewsContent.Text
                                Else
                                    NewsContent = Me.TextNewsContent.Text.Substring(0, 8000)
                                End If

                                Dim NewsStartDate As DateTime = CDate(Me.TextStartDate.Text)
                                Dim NewsEndDate As DateTime = CDate(Me.TextEndDate.Text)

                                If DateDiff(DateInterval.Day, NewsStartDate, NewsEndDate) < 0 Then
                                    Throw New Exception("Start Date must be less than or equal to End Date.")
                                End If

                                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                                    Me.InsertAuditTrail()
                                    Me.UpdateNewsBySU()
                                    Me.LblSuccess.Visible = True
                                    Me.LblSuccess.Text = "Success to Update News."
                                Else
                                    'Buat variabel untuk menampung nilai-nilai lama
                                    Dim NewsID_Old As Int64 = ViewState("NewsID_Old")
                                    Dim NewsTitle_Old As String = ViewState("NewsTitle_Old")
                                    Dim NewsSummary_Old As String = ViewState("NewsSummary_Old")
                                    Dim NewsContent_Old As String = ViewState("NewsContent_Old")
                                    Dim NewsStartDate_Old As DateTime = ViewState("NewsStartDate_Old")
                                    Dim NewsEndDate_Old As DateTime = ViewState("NewsEndDate_Old")
                                    Dim NewsCreatedDate_Old As DateTime = ViewState("NewsCreatedDate_Old")

                                    'variabel untuk menampung nilai identity dari tabel NewsPendingApprovalID
                                    Dim NewsPendingApprovalID As Int64

                                    'Using TransScope As New Transactions.TransactionScope()
                                    Using AccessNewsPendingApproval As New AMLDAL.AMLDataSetTableAdapters.News_PendingApprovalTableAdapter
                                        SetTransaction(AccessNewsPendingApproval, oSQLTrans)
                                        'Tambahkan ke dalam tabel News_PendingApproval dengan ModeID = 2 (Edit) 
                                        NewsPendingApprovalID = AccessNewsPendingApproval.InsertNews_PendingApproval(Sahassa.AML.Commonly.SessionUserId, Now, "News Edit", 2)

                                        'Tambahkan ke dalam tabel News_Approval dengan ModeID = 2 (Edit) 
                                        AccessPending.Insert(NewsPendingApprovalID, 2, NewsID, NewsTitle, NewsSummary, NewsContent, NewsStartDate, NewsEndDate, Now, NewsID_Old, NewsTitle_Old, NewsSummary_Old, NewsContent_Old, NewsStartDate_Old, NewsEndDate_Old, NewsCreatedDate_Old)

                                        oSQLTrans.Commit()
                                        'TransScope.Complete()
                                    End Using
                                    'End Using

                                    'Penambahan Attach
                                    Using adapter As New AMLDAL.AMLDataSetTableAdapters.NewsAttachmentTableAdapter
                                        adapter.UpdateNewsAttachmentFk_NewsApprovalIdbyUserId(Sahassa.AML.Commonly.SessionPkUserId, NewsPendingApprovalID)
                                    End Using
                                    '------------------


                                    Dim MessagePendingID As Integer = 8102 'MessagePendingID 8102 = News Edit 

                                    Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & (Regex.Replace(NewsTitle_Old, "<.*?>", "")).Trim
                                    Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & (Regex.Replace(NewsTitle_Old, "<.*?>", "")).Trim, False)

                                    'Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & NewsTitle_Old
                                    'Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & NewsTitle_Old, False)

                                End If
                            Else 'Counter != 0 berarti NewsTitle tersebut dalam status pending approval
                                Throw New Exception("Cannot edit the following News: '" & NewsTitle & "' because it is currently waiting for approval")
                            End If
                        End Using
                    Else 'Counter != 0 berarti NewsTitle tersebut sudah ada dalam tabel News
                        Throw New Exception("Cannot change to the following News Title: '" & NewsTitle & "' because that News Title already exists in the database.")
                    End If
                End If
            Catch ex As Exception
                If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
                Me.cvalPageError.IsValid = False
                Me.cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            Finally
                If Not oSQLTrans Is Nothing Then
                    oSQLTrans.Dispose()
                    oSQLTrans = Nothing
                End If
            End Try
        End If
    End Sub

    ''' <summary>
    ''' filledit data
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillEditData()
        Using AccessNews As New AMLDAL.AMLDataSetTableAdapters.NewsTableAdapter
            Using TableNews As AMLDAL.AMLDataSet.NewsDataTable = AccessNews.GetDataByNewsID(Me.GetNewsId)
                If TableNews.Rows.Count > 0 Then
                    Dim TableRowNews As AMLDAL.AMLDataSet.NewsRow = TableNews.Rows(0)

                    ViewState("NewsID_Old") = TableRowNews.NewsID
                    Me.LabelNewsID.Text = ViewState("NewsID_Old")

                    ViewState("NewsTitle_Old") = TableRowNews.NewsTitle
                    Me.TextNewsTitleCK.Text = ViewState("NewsTitle_Old")

                    ViewState("NewsSummary_Old") = TableRowNews.NewsSummary
                    Me.TextNewsSummary.Text = ViewState("NewsSummary_Old")

                    ViewState("NewsContent_Old") = TableRowNews.NewsContent
                    Me.TextNewsContent.Text = ViewState("NewsContent_Old")

                    ViewState("NewsStartDate_Old") = CType(TableRowNews.NewsStartDate, Date)
                    Me.TextStartDate.Text = String.Format("{0:dd-MMM-yyyy}", ViewState("NewsStartDate_Old"))

                    ViewState("NewsEndDate_Old") = CType(TableRowNews.NewsEndDate, Date)
                    Me.TextEndDate.Text = String.Format("{0:dd-MMM-yyyy}", ViewState("NewsEndDate_Old"))

                    ViewState("NewsCreatedDate_Old") = TableRowNews.NewsCreatedDate


                    'attach
                    Session("FirstLoad.NewsEdit") = 1
                    Using objcmd As New System.Data.SqlClient.SqlCommand("INSERTNewsAttachmentbyFK_NewsIDforEdit")
                        objcmd.CommandType = System.Data.CommandType.StoredProcedure
                        objcmd.CommandTimeout = System.Configuration.ConfigurationManager.AppSettings("SQLCommandTimeout")
                        objcmd.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FK_NewsID", TableRowNews.NewsID))
                        objcmd.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FK_UserID", Sahassa.AML.Commonly.SessionPkUserId))
                        DataRepository.Provider.ExecuteScalar(objcmd)
                    End Using
                    BindListAttachment()
                    Session("FirstLoad.NewsEdit") = Nothing
                End If
            End Using
        End Using
    End Sub

    ''' <summary>
    ''' page load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Using adapter As New AMLDAL.AMLDataSetTableAdapters.NewsAttachmentTableAdapter
                    adapter.DeleteNewsAttachmentbyUserId(Sahassa.AML.Commonly.SessionPkUserId)
                End Using
                Me.FillEditData()
                Me.cmdStartDatePicker.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TextStartDate.ClientID & "'), 'dd-mmm-yyyy')")
                Me.cmdEndDatePicker.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TextEndDate.ClientID & "'), 'dd-mmm-yyyy')")

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
#Region "Insert News By SU dan Audit Trail"

    ''' <summary>
    '''  insert news
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub UpdateNewsBySU()
        Try
            Dim NewsSummary As String
            If Me.TextNewsSummary.Text.Length <= 255 Then
                NewsSummary = Me.TextNewsSummary.Text
            Else
                NewsSummary = Me.TextNewsSummary.Text.Substring(0, 255)
            End If

            Dim NewsContent As String
            If Me.TextNewsContent.Text.Length <= 8000 Then
                NewsContent = Me.TextNewsContent.Text
            Else
                NewsContent = Me.TextNewsContent.Text.Substring(0, 8000)
            End If

            Using AccessNews As New AMLDAL.AMLDataSetTableAdapters.NewsTableAdapter
                AccessNews.UpdateNews(CInt(Me.LabelNewsID.Text), Me.TextNewsTitleCK.Text, NewsSummary, NewsContent, CDate(Me.TextStartDate.Text), CDate(Me.TextEndDate.Text))
            End Using
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' audit trail
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub InsertAuditTrail()
        Try
            Using AccessNews As New AMLDAL.AMLDataSetTableAdapters.NewsTableAdapter
                Using dt As Data.DataTable = AccessNews.GetDataByNewsID(CInt(Me.GetNewsId))
                    Dim RowNews As AMLDAL.AMLDataSet.NewsRow = dt.Rows(0)
                    Sahassa.AML.AuditTrailAlert.AuditTrailChecking(7)

                    Dim NewsSummary As String
                    If Me.TextNewsSummary.Text.Length <= 255 Then
                        NewsSummary = Me.TextNewsSummary.Text
                    Else
                        NewsSummary = Me.TextNewsSummary.Text.Substring(0, 255)
                    End If

                    Dim NewsContent As String
                    If Me.TextNewsContent.Text.Length <= 8000 Then
                        NewsContent = Me.TextNewsContent.Text
                    Else
                        NewsContent = Me.TextNewsContent.Text.Substring(0, 8000)
                    End If

                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "News", "NewsID", "Edit", RowNews.NewsID, RowNews.NewsID, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "News", "NewsTitle", "Edit", RowNews.NewsTitle, Me.TextNewsTitleCK.Text, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "News", "NewsSummary", "Edit", NewsSummary, Me.TextNewsSummary.Text, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "News", "NewsContent", "Edit", NewsContent, Me.TextNewsContent.Text, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "News", "NewsStartDate", "Edit", CDate(RowNews.NewsStartDate).ToString("dd-MMMM-yyyy HH:mm"), CDate(Me.TextStartDate.Text).ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "News", "NewsEndDate", "Edit", CDate(RowNews.NewsEndDate).ToString("dd-MMMM-yyyy HH:mm"), CDate(Me.TextEndDate.Text).ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "News", "NewsCreatedDate", "Edit", RowNews.NewsCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), Now.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                    End Using
                End Using
            End Using
        Catch
            Throw
        End Try
    End Sub
#End Region

    'Penambahan Attachment.
    Protected Sub ButtonAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        Try

            If FileUpload1.HasFile Then
                Using adapter As New AMLDAL.AMLDataSetTableAdapters.NewsAttachmentTableAdapter
                    adapter.Insert(0, FileUpload1.FileName, FileUpload1.FileBytes, Sahassa.AML.Commonly.SessionPkUserId, FileUpload1.PostedFile.ContentType)
                End Using
                BindListAttachment()
            End If

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Private Sub BindListAttachment()
        Try
            Using adapter As New AMLDAL.AMLDataSetTableAdapters.NewsAttachmentTableAdapter

                Me.ListAttachment.DataSource = adapter.GetNewsAttachmentByUserIdCreateFirst(Sahassa.AML.Commonly.SessionPkUserId)
                Me.ListAttachment.DataTextField = "filename"
                Me.ListAttachment.DataValueField = "PK_NewsAttachment"
                Me.ListAttachment.DataBind()
            End Using
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Sub
    Protected Sub ButtonDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonDelete.Click
        Try
            If ListAttachment.SelectedIndex <> -1 Then
                Using adapter As New AMLDAL.AMLDataSetTableAdapters.NewsAttachmentTableAdapter
                    adapter.DeleteNewsAttachmentbyPkId(ListAttachment.SelectedValue)
                End Using
                BindListAttachment()
            End If

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class