#Region "Imports..."
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Excel
Imports Sahassa.AML.Commonly
Imports Sahassanettier.Data
Imports Sahassanettier.Entities
Imports Sahassanettier.Data.SqlClient
Imports System
Imports System.Collections.Generic
Imports System.Data
Imports System.IO
Imports AMLBLL.DataType
#End Region

Partial Class MsNegara_upload
    Inherits Parent

#Region "Structure..."

    Structure GrouP_MsNegara_success
        Dim MsNegara As MsNegara_ApprovalDetail
        Dim L_MsNegaraNCBS As TList(Of MsNegaraNCBS_ApprovalDetail)

    End Structure

#End Region

#Region "Property umum"
    Private Property GroupTableSuccessData() As List(Of GrouP_MsNegara_success)
        Get
            Return Session("GroupTableSuccessData")
        End Get
        Set(ByVal value As List(Of GrouP_MsNegara_success))
            Session("GroupTableSuccessData") = value
        End Set
    End Property

    Private Property GroupTableAnomalyData() As List(Of AMLBLL.MsNegaraBll.DT_Group_MsNegara)
        Get
            Return Session("GroupTableAnomalyData")
        End Get
        Set(ByVal value As List(Of AMLBLL.MsNegaraBll.DT_Group_MsNegara))
            Session("GroupTableAnomalyData") = value
        End Set
    End Property

    Public ReadOnly Property GET_PKUserID() As Integer
        Get
            Return SessionPkUserId
        End Get
    End Property

    Public ReadOnly Property GET_UserName() As String
        Get
            Return SessionUserId
            'Return SessionNIK
        End Get
    End Property

    Private Property SetnGetDataTableForSuccessData() As TList(Of MsNegara_ApprovalDetail)
        Get
            Return CType(IIf(Session("MsNegara_upload.TableSuccessUpload") Is Nothing, Nothing, Session("MsNegara_upload.TableSuccessUpload")), TList(Of MsNegara_ApprovalDetail))
        End Get
        Set(ByVal value As TList(Of MsNegara_ApprovalDetail))
            Session("MsNegara_upload.TableSuccessUpload") = value
        End Set
    End Property

    Private Property SetnGetDataTableForAnomalyData() As Data.DataTable
        Get
            Return CType(IIf(Session("MsNegara_upload.TableAnomalyUpload") Is Nothing, Nothing, Session("MsNegara_upload.TableAnomalyUpload")), Data.DataTable)
        End Get
        Set(ByVal value As Data.DataTable)
            Session("MsNegara_upload.TableAnomalyUpload") = value
        End Set
    End Property

    Private Property DSexcelTemp() As Data.DataSet
        Get
            Return CType(IIf(Session("MsNegara_upload.DSexcelTemp") Is Nothing, Nothing, Session("MsNegara_upload.DSexcelTemp")), DataSet)
        End Get
        Set(ByVal value As Data.DataSet)
            Session("MsNegara_upload.DSexcelTemp") = value
        End Set
    End Property

#End Region

#Region "Property Tabel Temporary"
    Private Property DT_AllMsNegara() As Data.DataTable
        Get
            Return CType(IIf(Session("MsNegara_upload.DT_MsNegara") Is Nothing, Nothing, Session("MsNegara_upload.DT_MsNegara")), Data.DataTable)
        End Get
        Set(ByVal value As Data.DataTable)
            Session("MsNegara_upload.DT_MsNegara") = value
        End Set
    End Property

    Private Property DT_AllMsNegaraNCBS() As Data.DataTable
        Get
            Return CType(IIf(Session("MsNegara_upload.DT_MsNegaraNCBS") Is Nothing, Nothing, Session("MsNegara_upload.DT_MsNegaraNCBS")), Data.DataTable)
        End Get
        Set(ByVal value As Data.DataTable)
            Session("MsNegara_upload.DT_MsNegaraNCBS") = value
        End Set
    End Property

#End Region

#Region "Property Table Sukses Data"

    Private Property TableSuccessMsNegara() As TList(Of MsNegara_ApprovalDetail)
        Get
            Return Session("TableSuccessMsNegara")
        End Get
        Set(ByVal value As TList(Of MsNegara_ApprovalDetail))
            Session("TableSuccessMsNegara") = value
        End Set
    End Property

    Private Property TableSuccessMsNegaraNCBS() As TList(Of MsNegaraNCBS)
        Get
            Return Session("TableSuccessMsNegaraNCBS")
        End Get
        Set(ByVal value As TList(Of MsNegaraNCBS))
            Session("TableSuccessMsNegaraNCBS") = value
        End Set
    End Property


#End Region

#Region "Property Table Anomaly Data"

    Private Property TableAnomalyMsNegara() As Data.DataTable
        Get
            Return Session("TableAnomalyMsNegara")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("TableAnomalyMsNegara") = value
        End Set
    End Property

    Private Property TableAnomalyMsNegaraNCBS() As Data.DataTable
        Get
            Return Session("TableAnomalyMsNegaraNCBS")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("TableAnomalyMsNegaraNCBS") = value
        End Set
    End Property

#End Region

#Region "validasi.."

    Private Function ValidateInput(ByVal MsNegara As AMLBLL.MsNegaraBll.DT_Group_MsNegara) As List(Of String)
        Dim temp As New List(Of String)
        Dim msgError As New StringBuilder
        Dim errorline As New StringBuilder
        Try
            'validasi
            ValidateMsNegara(MsNegara.MsNegara, msgError, errorline)
            If msgError.ToString = "" Then
                For Each ci As DataRow In MsNegara.L_MsNegaraNCBS.Rows
                    'validasi NCBS
                    ValidateMsNegaraNCBS(ci, msgError, errorline)
                Next
            End If
            temp.Add(msgError.ToString)
            temp.Add(errorline.ToString)
        Catch ex As Exception
            msgError.Append(ex.Message)
            temp.Add(msgError.ToString)
            temp.Add(errorline.ToString)
        End Try
        Return temp
    End Function

    Private Shared Sub ValidateMsNegaraNCBS(ByVal DTR As DataRow, ByVal msgError As StringBuilder, ByVal errorLine As StringBuilder)
        Try

            '======================== Validasi textbox :  IDNegaraNCBS canot Null (#)====================================================
            If ObjectAntiNull(DTR("IDNegaraBank")) = False Then
                msgError.AppendLine(" &bull; IDNegaraBank must be filled <BR/>")
                errorLine.AppendLine(" &bull; MsNegaraBank : Line " & DTR("NO") & "<BR/>")
            End If

            '======================== Validasi textbox :  NamaNegara canot Null (#)====================================================
            If ObjectAntiNull(DTR("NamaNegaraBank")) = False Then
                msgError.AppendLine(" &bull; NamaNegaraBank must be filled <BR/>")
                errorLine.AppendLine(" &bull; MsNegaraBank : Line " & DTR("NO") & "<BR/>")
            End If

        Catch ex As Exception
            msgError.AppendLine(" &bull;" & ex.Message & "<BR/>")
            errorLine.AppendLine(" &bull;MsNegaraBank : Line " & DTR("NO") & "<BR/>")
        End Try
    End Sub

    Private Shared Sub ValidateMsNegara(ByVal DTR As DataRow, ByVal msgError As StringBuilder, ByVal errorline As StringBuilder)
        Try

            Using checkBlocking As TList(Of MsNegara_ApprovalDetail) = DataRepository.MsNegara_ApprovalDetailProvider.GetPaged("IDNegara=" & DTR("IDNegara"), "", 0, 1, Nothing)
                If checkBlocking.Count > 0 Then
                    msgError.AppendLine(" &bull;MsNegara is waiting for approval <BR/>")
                    errorline.AppendLine(" &bull;MsNegara : Line " & DTR("NO") & "<BR/>")
                End If
            End Using

            '======================== Validasi textbox :  IDNegara   canot Null (#)====================================================
            If ObjectAntiNull(DTR("IDNegara")) = False Then
                msgError.AppendLine(" &bull; IDNegara must be filled <BR/>")
                errorline.AppendLine(" &bull; MsNegara : Line " & DTR("NO") & "<BR/>")
            End If
            If ObjectAntiNull(DTR("IDNegara")) Then
                If objectNumOnly(DTR("IDNegara")) = False Then
                    msgError.AppendLine(" &bull; IDNegara must be Numeric <BR/>")
                    errorline.AppendLine(" &bull; MsNegara : Line " & DTR("NO") & "<BR/>")
                End If
            End If
            '======================== Validasi textbox :  NamaNegara   canot Null (#)====================================================
            If ObjectAntiNull(DTR("NamaNegara")) = False Then
                msgError.AppendLine(" &bull; Nama Negara must be filled <BR/>")
                errorline.AppendLine(" &bull; MsNegara : Line " & DTR("NO") & "<BR/>")
            End If

        Catch ex As Exception
            msgError.AppendLine(" &bull;" & ex.Message & "<BR/>")
            errorline.AppendLine(" &bull;MsNegara  : Line " & DTR("NO") & "<BR/>")
            LogError(ex)
        End Try
    End Sub

#End Region

#Region "Upload File"

    Public Function SaveFileImportToSave() As String
        Dim filePath As String
        Dim FileNameToGetData As String = ""
        Try
            If FileUploadKPI.PostedFile.ContentLength = 0 Then
                Return FileNameToGetData
            End If

            If Not FileUploadKPI.HasFile Then
                Return FileNameToGetData
            End If

            filePath = Path.Combine(Server.MapPath("Upload"), FileUploadKPI.FileName)

            FileNameToGetData = FileUploadKPI.FileName
            If System.IO.File.Exists(filePath) Then
                Dim bexist As Boolean = True
                Dim i As Integer = 1
                While bexist
                    filePath = Path.Combine(Server.MapPath("Upload"), FileUploadKPI.FileName.Split(CChar("."))(0) & "-" & i.ToString & "." & FileUploadKPI.FileName.Split(CChar("."))(1))
                    FileNameToGetData = FileUploadKPI.FileName.Split(CChar("."))(0) & "-" & i.ToString & "." & Me.FileUploadKPI.FileName.Split(CChar("."))(1)
                    If Not System.IO.File.Exists(filePath) Then
                        bexist = False
                        Exit While
                    End If
                    i = i + 1
                End While
            End If
            Me.FileUploadKPI.PostedFile.SaveAs(filePath)
            Session("Filepath") = filePath
            Return FileNameToGetData
        Catch
            Throw
        End Try
    End Function

    ReadOnly Property FilePath() As String
        Get
            Return Session("FilePath")
        End Get
    End Property

    Private Sub ProcessExcelFile(ByVal FullPathFileNameToExportExcel As String)

        Using stream As FileStream = File.Open(FullPathFileNameToExportExcel, FileMode.Open, FileAccess.Read)
            Using excelReader As IExcelDataReader = ExcelReaderFactory.CreateBinaryReader(stream)
                excelReader.IsFirstRowAsColumnNames = True
                DSexcelTemp = excelReader.AsDataSet()
                If DSexcelTemp.Tables("MsNegara").Columns(0).ColumnName Like "*Column*" Then
                    DSexcelTemp.Tables("MsNegara").Rows.Clear()
                End If

            End Using
        End Using

    End Sub


#End Region

#Region "Function..."

    Function getErrorNo(ByVal Id As String, ByVal nama As String) As String
        Dim temp As String = ""
        Try
            Dim DV As New DataView(DT_AllMsNegaraNCBS)
            'jika Null maka harus is null bukan kosong disini belum di handle
            DV.RowFilter = "IDNegara = '" & Id & "' and NamaNegara = '" & nama & "'"

            Dim DT As Data.DataTable = DV.ToTable
            If DT.Rows.Count > 0 Then
                temp = Safe(DT.Rows(0)("NO"))
            End If
        Catch ex As Exception
            temp = ex.Message
        End Try

        Return temp
    End Function
    ''' <summary>
    ''' Merubah excel ke Object Databale sekaligus memecahnya
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ImportExcelTempToDTproperty()
        Dim DT As New Data.DataTable
        'DT_MsNegara = DSexcelTemp.Tables("MsNegara")
        DT = DSexcelTemp.Tables("MsNegara")
        Dim DV As New DataView(DT)
        DT_AllMsNegara = DV.ToTable(True, "IDNegara", "NamaNegara", "activation")
        DT_AllMsNegaraNCBS = DSexcelTemp.Tables("MsNegara")
        DT_AllMsNegara.Columns.Add("NO")
        For Each i As DataRow In DT_AllMsNegara.Rows
            Dim PK As String = Safe(i("IDNegara"))
            Dim nama As String = Safe(i("NamaNegara"))
            i("NO") = getErrorNo(PK, nama)
        Next
    End Sub

    Private Sub ClearThisPageSessions()
        DT_AllMsNegara = Nothing
        DT_AllMsNegaraNCBS = Nothing
        GroupTableSuccessData = Nothing
        GroupTableAnomalyData = Nothing
        DSexcelTemp = Nothing
        SetnGetDataTableForAnomalyData = Nothing
        SetnGetDataTableForSuccessData = Nothing
    End Sub


    Function ConvertDTtoMsNegaraNCBS(ByVal DT As Data.DataTable) As TList(Of MsNegaraNCBS_ApprovalDetail)
        Dim temp As New TList(Of MsNegaraNCBS_ApprovalDetail)
        For Each DTR As DataRow In DT.Rows
            Using MsNegaraNCBS As New MsNegaraNCBS_ApprovalDetail
                With MsNegaraNCBS
                    FillOrNothing(.IDNegaraNCBS, DTR("IDNegaraBank"), True, oInt)
                    FillOrNothing(.NamaNegara, DTR("NamaNegaraBank"), True, oString)
                    FillOrNothing(.Activation, 1, True, oBit)
                    FillOrNothing(.CreatedBy, GET_UserName, True, oString)
                    FillOrNothing(.CreatedDate, Now, True, oDate)
                    temp.Add(MsNegaraNCBS)
                End With
            End Using

        Next

        Return temp
    End Function



    Function ConvertDTtoMsNegara(ByVal DTR As DataRow) As MsNegara_ApprovalDetail
        Dim temp As New MsNegara_ApprovalDetail
        Using MsNegara As New MsNegara_ApprovalDetail()
            With MsNegara
                FillOrNothing(.IDNegara, DTR("IDNegara"), True, oInt)
                FillOrNothing(.NamaNegara, DTR("NamaNegara"), True, Ovarchar)

                FillOrNothing(.Activation, DTR("Activation"), True, oBit)
                FillOrNothing(.CreatedBy, GET_UserName, True, oString)
                FillOrNothing(.CreatedDate, Now, True, oDate)
            End With
            temp = MsNegara
        End Using

        Return temp
    End Function

    Function ConvertDTtoMsNegara(ByVal DT As Data.DataTable) As TList(Of MsNegara_ApprovalDetail)
        Dim temp As New TList(Of MsNegara_ApprovalDetail)
        For Each DTR As DataRow In DT.Rows
            Using MsNegara As New MsNegara_ApprovalDetail()
                With MsNegara
                    FillOrNothing(.IDNegara, DTR("IDNegara"), True, oInt)
                    FillOrNothing(.NamaNegara, DTR("NamaNegara"), True, Ovarchar)

                    FillOrNothing(.Activation, DTR("Activation"), True, oBit)
                    FillOrNothing(.CreatedBy, GET_UserName, True, oString)
                    FillOrNothing(.CreatedDate, Now, True, oDate)
                    temp.Add(MsNegara)
                End With
            End Using
        Next
        Return temp
    End Function

    Private Sub ChangeMultiView(ByVal index As Integer)
        MultiViewKPIOvertimeUpload.ActiveViewIndex = index
        If index = 0 Then
            Imgbtnsave.Visible = False
            Imgbtncancel.Visible = False
        ElseIf index = 1 Then
            Imgbtncancel.Visible = True
        ElseIf index = 2 Then
            Imgbtncancel.Visible = False
            Imgbtnsave.Visible = False
        End If
    End Sub

    Private Function GenerateGroupingMsNegara() As List(Of AMLBLL.MsNegaraBll.DT_Group_MsNegara)
        Dim Ltemp As New List(Of AMLBLL.MsNegaraBll.DT_Group_MsNegara)
        For i As Integer = 0 To DT_AllMsNegara.Rows.Count - 1
            Dim MsNegara As New AMLBLL.MsNegaraBll.DT_Group_MsNegara
            With MsNegara
                .MsNegara = DT_AllMsNegara.Rows(i)
                .L_MsNegaraNCBS = DT_AllMsNegaraNCBS.Clone

                'Insert MsNegaraNCBS
                Dim MsNegara_ID As String = Safe(.MsNegara("IDNegara"))
                Dim MsNegara_nama As String = Safe(.MsNegara("NamaNegara"))
                Dim MsNegara_Activation As String = Safe(.MsNegara("Activation"))
                If DT_AllMsNegaraNCBS.Rows.Count > 0 Then
                    Using DV As New DataView(DT_AllMsNegaraNCBS)
                        DV.RowFilter = "IDNegara='" & MsNegara_ID & "' and " & _
                                                  "NamaNegara='" & MsNegara_nama & "' and " & _
                                                  "activation='" & MsNegara_Activation & "'"
                        If DV.Count > 0 Then
                            Dim temp As Data.DataTable = DV.ToTable
                            For Each H As DataRow In temp.Rows
                                .L_MsNegaraNCBS.ImportRow(H)
                            Next
                        End If
                    End Using
                End If




            End With
            Ltemp.Add(MsNegara)
        Next
        Return Ltemp
    End Function

    Public Sub BindGrid()
        lblValueFailedData.Text = SetnGetDataTableForAnomalyData.Rows.Count
        GridAnomalyList.DataSource = SetnGetDataTableForAnomalyData
        GridAnomalyList.DataBind()
        lblValueSuccessData.Text = SetnGetDataTableForSuccessData.Count
        GridSuccessList.DataSource = SetnGetDataTableForSuccessData
        lblValueuploadeddata.Text = DT_AllMsNegara.Rows.Count
        GridSuccessList.DataBind()
    End Sub

#End Region

#Region "events..."


    Protected Sub ImgBtnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imgbtncancel.Click
        ClearThisPageSessions()
        ChangeMultiView(0)
        LinkButtonExportExcel.Visible = True
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Try
            If IsPostBack Then
                Return
            End If

            ClearThisPageSessions()
            SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
            ChangeMultiView(0)
        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub ImgBtnAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAdd.Click
        ChangeMultiView(0)
    End Sub

    Protected Sub GridAnomalyList_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles GridAnomalyList.PageIndexChanged
        GridAnomalyList.CurrentPageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub GridSuccessList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridSuccessList.ItemDataBound
        Try
            Dim CollCount As Integer = e.Item.Cells.Count - 1
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then

                'Show mapping item
                Dim StringMaping As New StringBuilder
                Dim DV As New DataView(DT_AllMsNegaraNCBS)
                Dim DT As New Data.DataTable
                DV.RowFilter = "IDNegara='" & e.Item.Cells(0).Text & "' and " & "NamaNegara='" & e.Item.Cells(1).Text & "'"
                DT = DV.ToTable
                If DT.Rows.Count > 0 Then
                    Dim countField As Integer = 7
                    Dim lengthField As Integer = 0
                    For Each idx As DataRow In DT.Rows
                        With idx
                            If lengthField > countField Then
                                StringMaping.AppendLine(idx("NamaNegaraBank") & "(" & idx("IDNegaraBank") & ");")
                                lengthField = 0
                            Else
                                StringMaping.AppendLine(idx("NamaNegaraBank") & "(" & idx("IDNegaraBank") & ");")
                                lengthField += 1
                            End If

                        End With
                    Next
                Else
                    StringMaping.Append("-")
                End If
                e.Item.Cells(CollCount).Text = StringMaping.ToString

            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub GridSuccessList_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles GridSuccessList.PageIndexChanged
        Dim GridTarget As DataGrid = CType(source, DataGrid)
        Try
            GridSuccessList.CurrentPageIndex = e.NewPageIndex
            BindGrid()
        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgSaveSuccessData_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imgbtnsave.Click
        Try
            'File belum diupload
            If DT_AllMsNegara Is Nothing Then
                Throw New Exception("Load data failed")
            End If

            'Save MsNegara Approval (Header)
            Dim KeyApp As String
            Using Aproval As New MsNegara_Approval()
                With Aproval
                    .RequestedBy = GET_PKUserID
                    .RequestedDate = Date.Now
                    .FK_MsMode_Id = 2 'Edit
                    .IsUpload = True
                End With
                DataRepository.MsNegara_ApprovalProvider.Save(Aproval)
                'ambil key approval
                KeyApp = Aproval.PK_MsNegara_Approval_Id
            End Using

            For k As Integer = 0 To GroupTableSuccessData.Count - 1

                Dim MsNegara As GrouP_MsNegara_success = GroupTableSuccessData(k)


                With MsNegara
                    'save MsNegara approval detil
                    .MsNegara.FK_MsNegara_Approval_Id = KeyApp
                    DataRepository.MsNegara_ApprovalDetailProvider.Save(.MsNegara)
                    'ambil key MsNegara approval detil
                    Dim IdMsNegara As String = .MsNegara.IDNegara
                    Dim L_MappingMsNegaraNCBSPPATK_Approval_Detail As New TList(Of MappingMsNegaraNCBSPPATK_Approval_Detail)
                    'save mapping dan NCBS
                    For Each OMsNegaraNCBS_ApprovalDetail As MsNegaraNCBS_ApprovalDetail In .L_MsNegaraNCBS
                        OMsNegaraNCBS_ApprovalDetail.PK_MsNegara_Approval_Id = KeyApp
                        Using OMappingMsNegaraNCBSPPATK_Approval_Detail As New MappingMsNegaraNCBSPPATK_Approval_Detail
                            With OMappingMsNegaraNCBSPPATK_Approval_Detail
                                FillOrNothing(.IDNegaraNCBS, OMsNegaraNCBS_ApprovalDetail.IDNegaraNCBS, True, oInt)
                                FillOrNothing(.IDNegara, IdMsNegara, True, oInt)
                                FillOrNothing(.PK_MsNegara_Approval_Id, KeyApp, True, oInt)
                                FillOrNothing(.Nama, OMsNegaraNCBS_ApprovalDetail.NamaNegara, True, oString)
                            End With
                            L_MappingMsNegaraNCBSPPATK_Approval_Detail.Add(OMappingMsNegaraNCBSPPATK_Approval_Detail)
                        End Using
                    Next
                    'save maping dan NCBS
                    DataRepository.MappingMsNegaraNCBSPPATK_Approval_DetailProvider.Save(L_MappingMsNegaraNCBSPPATK_Approval_Detail)
                    DataRepository.MsNegaraNCBS_ApprovalDetailProvider.Save(.L_MsNegaraNCBS)

                End With
            Next

            LblConfirmation.Text = "MsNegara data has edited (" & SetnGetDataTableForSuccessData.Count & " Rows / MsNegara)"
            ChangeMultiView(2)

        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgUpload_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgUpload.Click
        Dim CombinedFileName As String
        GroupTableSuccessData = New List(Of GrouP_MsNegara_success)
        GroupTableAnomalyData = New List(Of AMLBLL.MsNegaraBll.DT_Group_MsNegara)
        Try
            'validasi File
            If FileUploadKPI.FileName = "" Then
                Throw New Exception("Data required")
            End If

            If Path.GetExtension(FileUploadKPI.FileName) <> ".xls" Then
                Throw New Exception("Invalid File Upload Extension")
            End If

            CombinedFileName = SaveFileImportToSave()
            If String.Compare(CombinedFileName, "", False) <> 0 Then
                Try
                    ProcessExcelFile(FilePath)
                Catch ex As Exception
                    Throw New Exception("Invalid File Format")
                End Try
            Else
                Throw New Exception("Invalid File Format")
            End If

            'Import from Excell to property Global
            ImportExcelTempToDTproperty()

            Dim LG_MsNegara As List(Of AMLBLL.MsNegaraBll.DT_Group_MsNegara) = GenerateGroupingMsNegara()
            'tambahkan kolom untuk tampilan di grid anomaly
            DT_AllMsNegara.Columns.Add("Description")
            DT_AllMsNegara.Columns.Add("errorline")

            'Membuat tempory untuk yang anomali dan yang sukses
            Dim DTnormal As Data.DataTable = DT_AllMsNegara.Clone
            Dim DTAnomaly As Data.DataTable = DT_AllMsNegara.Clone

            'proses pemilahan data anomaly dan data sukses melalui validasi 
            For i As Integer = 0 To LG_MsNegara.Count - 1
                Dim Lerror As List(Of String) = ValidateInput(LG_MsNegara(i))
                Dim MSG As String = Lerror(0)
                Dim LINE As String = Lerror(1)
                If MSG = "" Then
                    DTnormal.ImportRow(LG_MsNegara(i).MsNegara)
                    Dim GroupMsNegara As New GrouP_MsNegara_success
                    With GroupMsNegara
                        'konversi dari datatable ke Object Nettier
                        .MsNegara = ConvertDTtoMsNegara(LG_MsNegara(i).MsNegara)
                        .L_MsNegaraNCBS = ConvertDTtoMsNegaraNCBS(LG_MsNegara(i).L_MsNegaraNCBS)
                    End With
                    'masukan ke group sukses
                    GroupTableSuccessData.Add(GroupMsNegara)
                Else
                    DT_AllMsNegara.Rows(i)("Description") = MSG
                    DT_AllMsNegara.Rows(i)("errorline") = LINE
                    DTAnomaly.ImportRow(DT_AllMsNegara.Rows(i))
                    GroupTableAnomalyData.Add(LG_MsNegara(i))
                End If

            Next

            SetnGetDataTableForSuccessData = ConvertDTtoMsNegara(DTnormal)
            SetnGetDataTableForAnomalyData = DTAnomaly

            If DTAnomaly.Rows.Count > 0 Then
                Imgbtnsave.Visible = False
                Imgbtncancel.Visible = True
            Else
                Imgbtnsave.Visible = True
                Imgbtncancel.Visible = True
            End If

            BindGrid()
            If GridAnomalyList.Items.Count < 1 Then
                LinkButtonExportExcel.Visible = False
            Else
                LinkButtonExportExcel.Visible = True
            End If
            ChangeMultiView(1)
        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

#End Region

    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Dim Exp As New AMLBLL.MsNegaraBll
            Dim FolderPath As String = Server.MapPath("FolderTemplate")

            Dim buffer As Byte() = Exp.ExportDTtoExcel(GroupTableAnomalyData, FolderPath)
            Response.AddHeader("Content-type", "application/vnd.ms-excel")
            Response.AddHeader("Content-Disposition", "attachment; filename=MsNegaraUpload.xls")
            Response.BinaryWrite(buffer)
            Response.Flush()
            Response.End()

        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

        ' Verifies that the control is rendered 

    End Sub

    Protected Sub GridSuccessList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridSuccessList.SelectedIndexChanged

    End Sub
End Class






