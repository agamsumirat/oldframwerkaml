Imports System.Data.SqlClient
Partial Class AdvancedRulesAdd
    Inherits Parent
    Public ReadOnly Property strSessionName() As String
        Get
            Return "AdvanceRuleQueryBuilder"
        End Get

    End Property
    Public ReadOnly Property oQueryBuilder() As Sahassa.AML.QueryBuilder
        Get
            If Session(strSessionName) Is Nothing Then

                Dim TempQuery As Sahassa.AML.QueryBuilder = New Sahassa.AML.QueryBuilder
                TempQuery.AddTableString("[TransactionDetail]", "[TransactionDetail]")
                Session(strSessionName) = TempQuery
                Return Session(strSessionName)
            Else
                Return CType(Session(strSessionName), Sahassa.AML.QueryBuilder)
            End If
        End Get

    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                SetSession()
            End If
            Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)


            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)


            End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Menu1_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs) Handles Menu1.MenuItemClick
        MultiView1.ActiveViewIndex = Int32.Parse(e.Item.Value)
        Tablestest1.ActiveIndex = MultiView1.ActiveViewIndex
        Fields1.ActiveIndex = MultiView1.ActiveViewIndex
        WhereClausesTest1.ActiveIndex = MultiView1.ActiveViewIndex
        GroupBy1.ActiveIndex = MultiView1.ActiveViewIndex
        HavingCondition1.ActiveIndex = MultiView1.ActiveViewIndex
        SQLExpression1.ActiveIndex = MultiView1.ActiveViewIndex

    End Sub
    Private Sub SetSession()
        Tablestest1.OQueryBuilder = Me.oQueryBuilder
        Fields1.OQueryBuilder = Me.oQueryBuilder
        WhereClausesTest1.OQueryBuilder = Me.oQueryBuilder
        GroupBy1.OQueryBuilder = Me.oQueryBuilder
        HavingCondition1.OQueryBuilder = Me.oQueryBuilder
        SQLExpression1.OQueryBuilder = Me.oQueryBuilder
    End Sub
    Private Sub AbandonSession()
        Session(strSessionName) = Nothing
        Tablestest1.OQueryBuilder = Nothing
        Fields1.OQueryBuilder = Nothing
        WhereClausesTest1.OQueryBuilder = Nothing
        GroupBy1.OQueryBuilder = Nothing
        HavingCondition1.OQueryBuilder = Nothing
        SQLExpression1.OQueryBuilder = Nothing
    End Sub

    Private Sub InsertAdvanceRulesBySU()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try

            Using adapter As New AMLDAL.RulesAdvancedTableAdapters.RulesAdvancedTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(adapter, Data.IsolationLevel.ReadUncommitted)
                adapter.Insert(TextRuleAdvanceName.Text.Trim, TextDescription.Text.Trim, chkEnabled.Checked, cboGroupingBy.SelectedValue, SQLExpression1.OQueryBuilder.SQlQuery, Now.ToString("yyyy-MM-dd HH:mm:ss"), True)

                InsertAuditTrail(oSQLTrans)
                oSQLTrans.Commit()
            End Using
            AbandonSession()
            SetSession()
            Me.LblSuccess.Visible = True
            Me.LblSuccess.Text = "Success to Insert Advanced Rules."
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If

        End Try
    End Sub
    Private Sub InsertAuditTrail(ByRef oSQLTrans As SqlTransaction)

        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(5)

            Dim AdvanceRuleDescription As String
            If Me.TextDescription.Text.Trim.Length <= 255 Then
                AdvanceRuleDescription = Me.TextDescription.Text.Trim
            Else
                AdvanceRuleDescription = Me.TextDescription.Text.Substring(0, 255)
            End If

            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Advanced Rule Name", "Add", "", Me.TextRuleAdvanceName.Text.Trim, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Description", "Add", "", AdvanceRuleDescription, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Enabled", "Add", "", chkEnabled.Checked, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Case Grouping By", "Add", "", Me.cboGroupingBy.SelectedValue, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Advanced Rule Created Date", "Add", "", Now.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
            End Using
        Catch
            Throw
        End Try
    End Sub
    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Try
            If Page.IsValid Then
                If IsDataValid() Then
                    If Sahassa.AML.Commonly.SessionUserId.ToLower = "superuser" Then
                        InsertAdvanceRulesBySU()
                    Else
                        InsertAdvaceRulesToPendingApproval()
                    End If
                End If
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    ''' <summary>
    ''' untuk menyimpan data ke pending approval
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function InsertAdvaceRulesToPendingApproval() As Boolean
        Dim intHeader As Integer
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            'insert to header

            Using adapter As New AMLDAL.RulesAdvancedTableAdapters.RulesAdvancedPendingApprovalTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(adapter, Data.IsolationLevel.ReadUncommitted)
                intHeader = adapter.InsertRulesAdvancedPendingApproval(TextRuleAdvanceName.Text.Trim, Sahassa.AML.Commonly.SessionUserId, 1, "Advanced Rules Add", Now.ToString("dd MMMM yyyy HH:mm:ss"))

                'insert to detail
                Using adapter1 As New AMLDAL.RulesAdvancedTableAdapters.RulesAdvancedApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(adapter1, oSQLTrans)
                    adapter1.Insert(intHeader, 0, TextRuleAdvanceName.Text.Trim, TextDescription.Text.Trim, chkEnabled.Checked, cboGroupingBy.SelectedValue, SQLExpression1.OQueryBuilder.SQlQuery, Now, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
                    oSQLTrans.Commit()
                End Using
            End Using
            AbandonSession()
            SetSession()
            Dim MessagePendingID As Integer = 81401 'MessagePendingID 81401 = Advanced Rules Add 

            Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & TextRuleAdvanceName.Text.Trim
            Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & TextRuleAdvanceName.Text.Trim, False)


        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If

        End Try
    End Function
    ''' <summary>
    ''' Untuk mengecek apakah data yang di input sudah valid
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function IsDataValid() As Boolean


        'cek rules name sudah ada belum di table rulesAdvance
        Using adapter As New AMLDAL.RulesAdvancedTableAdapters.RulesAdvancedTableAdapter
            Dim intJmlAdvanceRule As Nullable(Of Integer) = adapter.CountRulesAdvancedByRulesAdvanceName(TextRuleAdvanceName.Text.Trim)
            If intJmlAdvanceRule.GetValueOrDefault(0) > 0 Then

                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = "Rules Name already exits in Rules Advanced"
                Return False
            End If
        End Using

        'cek apakah rules name ada di tabel approval
        Using Adapter As New AMLDAL.RulesAdvancedTableAdapters.RulesAdvancedPendingApprovalTableAdapter
            Dim intJmlApproval As Integer = Adapter.GetCountRulesAdvancePendingByUniqueKey(TextRuleAdvanceName.Text.Trim)
            If intJmlApproval > 0 Then

                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = "Rules Name already exits in Rules Advanced Pending Approval."
                Return False
            End If
        End Using

        'cek apakah cbogrouping by = field yang ada
        If Not Me.oQueryBuilder.SqlFieldString.ToLower.Contains(cboGroupingBy.SelectedValue.ToLower) Then

            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = "Selected Case Grouping by not equal with selected Field."
            Return False
        End If
        'cek fieldnya harus hanya 1
        If Not Me.oQueryBuilder.SQLFieldDataset.Tables(0).Rows.Count = 1 Then
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = "selected field must 1 field."
            Return False
        End If
        'cek tablesnya harus mengandung transactiondetail
        If Not Me.oQueryBuilder.SqlTableString.ToLower.Contains("transactiondetail") Then
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = "Tables must contain 'transactiondetail'."
            Return False
        End If
        Return True
    End Function
    Protected Sub CustomValidator1_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles CustomValidator1.ServerValidate
        If SQLExpression1.OQueryBuilder.SQlQuery = "" Then
            args.IsValid = False

            CustomValidator1.ErrorMessage = "Please configure sql expression first!"

        Else
            Try
                SQLExpression1.OQueryBuilder.ParseSQLQuery()
                args.IsValid = True
                CustomValidator1.ErrorMessage = ""
            Catch ex As Exception
                CustomValidator1.ErrorMessage = "There is an error in sql expression.Please configure sql expression again !"
                args.IsValid = False
            End Try



        End If
    End Sub
  
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Response.Redirect("AdvancedRulesView.aspx", False)
    End Sub
End Class
