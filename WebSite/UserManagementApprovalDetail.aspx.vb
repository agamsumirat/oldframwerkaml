Imports System.Data.SqlClient

Partial Class UserManagementApprovalDetail
    Inherits Parent

#Region " Property "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get confirm type
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property ParamType() As Sahassa.AML.Commonly.TypeConfirm
        Get
            Return Me.Request.Params("TypeConfirm")
        End Get
    End Property
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get param pk user management
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property ParamPkUserManagement() As Int64
        Get
            Return Me.Request.Params("Id")
        End Get
    End Property
    ''' <summary>
    ''' get user id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetUserId() As String
        Get
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.User_PendingApprovalTableAdapter
                Return AccessPending.SelectUser_PendingApprovalUserId(Me.ParamPkUserManagement)
            End Using
        End Get
    End Property
#End Region
#Region " Load "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' hide all
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    24/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub HideAll()
        Try
            Dim StrId As String = ""
            Select Case Me.ParamType
                Case Sahassa.AML.Commonly.TypeConfirm.UserAdd
                    StrId = "UserAdd"
                Case Sahassa.AML.Commonly.TypeConfirm.UserEdit
                    StrId = "UserEdi"
                Case Sahassa.AML.Commonly.TypeConfirm.UserDelete
                    StrId = "UserDel"
                Case Else
                    Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
            End Select
            For Each ObjRow As HtmlControls.HtmlTableRow In Me.TableDetail.Rows
                ObjRow.Visible = ObjRow.ID = "Button11" Or ObjRow.ID.Substring(0, 7) = StrId
            Next
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load user add
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    24/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub LoadUserAdd()
        Me.LabelTitle.Text = "Activity: Add User"
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.User_ApprovalTableAdapter
            Using ObjTable As Data.DataTable = AccessPending.GetUserApprovalData(Me.ParamPkUserManagement)
                'Bila ObjTable.Rows.Count > 0 berarti User tsb masih ada dlm tabel User_Approval
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.User_ApprovalRow = ObjTable.Rows(0)
                    Me.LabelUserIdAdd.Text = rowData.UserID
                    Me.LabelUserNameAdd.Text = rowData.UserName
                    Me.LabelEmailAddressAdd.Text = rowData.UserEmailAddress
                    Me.LabelMobilePhoneAdd.Text = rowData.UserMobilePhone
                    Me.LabelTellerIDAdd.Text = rowData.UserTellerID

                    Using objGroupName As New AMLDAL.AMLDataSetTableAdapters.GroupTableAdapter
                        Me.LabelGroupNameAdd.Text = objGroupName.GetGroupNameByGroupID(rowData.UserGroupId)
                    End Using
                Else 'Bila ObjTable.Rows.Count = 0 berarti User tsb sudah tidak lagi berada dlm tabel User_Approval
                    Throw New Exception("Cannot load data from the following User: " & Me.LabelUserIdAdd.Text & " because that User is no longer in the approval table.")
                End If
            End Using
        End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load user edit
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    24/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub LoadUserEdit()
        Me.LabelTitle.Text = "Activity: Edit User"
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.User_ApprovalTableAdapter
            Using ObjTable As Data.DataTable = AccessPending.GetUserApprovalData(Me.ParamPkUserManagement)
                'Bila ObjTable.Rows.Count > 0 berarti User tsb masih ada dlm tabel User_Approval
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.User_ApprovalRow = ObjTable.Rows(0)
                    Me.LabelUserIdNew.Text = rowData.UserID
                    Me.LabelUserNameNew.Text = rowData.UserName
                    Me.LabelEmailAddressNew.Text = rowData.UserEmailAddress
                    Me.LabelMobilePhoneNew.Text = rowData.UserMobilePhone
                    Me.LabelTellerIDNew.Text = rowData.UserTellerID

                    Using objGroupNameNew As New AMLDAL.AMLDataSetTableAdapters.GroupTableAdapter
                        Me.LabelGroupNameNew.Text = objGroupNameNew.GetGroupNameByGroupID(rowData.UserGroupId)
                    End Using

                    Me.LabelUserIdOld.Text = rowData.UserID_Old
                    Me.LabelUserNameOld.Text = rowData.UserName_Old
                    Me.LabelEmailAddressOld.Text = rowData.UserEmailAddress_Old
                    Me.LabelMobilePhoneOld.Text = rowData.UserMobilePhone_Old
                    Me.LabelTellerIDOld.Text = rowData.UserTellerID_Old

                    Using objGroupNameOld As New AMLDAL.AMLDataSetTableAdapters.GroupTableAdapter
                        Me.LabelGroupNameOld.Text = objGroupNameOld.GetGroupNameByGroupID(rowData.UserGroupId_Old)
                    End Using
                Else 'Bila ObjTable.Rows.Count = 0 berarti User tsb sudah tidak lagi berada dlm tabel User_Approval
                    Throw New Exception("Cannot load data from the following User: " & Me.LabelUserIdOld.Text & " because that User is no longer in the approval table.")
                End If
            End Using
        End Using
    End Sub
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load user delete
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    ''' 	[Hendry]	24/05/2007	Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub LoadUserDelete()
        Me.LabelTitle.Text = "Activity: Delete User"
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.User_ApprovalTableAdapter
            Using ObjTable As Data.DataTable = AccessPending.GetUserApprovalData(Me.ParamPkUserManagement)
                'Bila ObjTable.Rows.Count > 0 berarti User tsb masih ada dlm tabel User_Approval
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.User_ApprovalRow = ObjTable.Rows(0)
                    Me.LabelUserIdDelete.Text = rowData.UserID
                    Me.LabelUserNameDelete.Text = rowData.UserName
                    Me.LabelEmailAddressDelete.Text = rowData.UserEmailAddress
                    Me.LabelMobilePhoneDelete.Text = rowData.UserMobilePhone
                    Me.LabelTellerIDDelete.Text = rowData.UserTellerID

                    Using AccessGroup As New AMLDAL.AMLDataSetTableAdapters.GroupTableAdapter
                        Me.LabelGroupNameDelete.Text = AccessGroup.GetGroupNameByGroupID(rowData.UserGroupId)
                    End Using
                Else 'Bila ObjTable.Rows.Count = 0 berarti User tsb sudah tidak lagi berada dlm tabel User_Approval
                    Throw New Exception("Cannot load data from the following User: " & Me.LabelUserIdDelete.Text & " because that User is no longer in the approval table.")
                End If
            End Using
        End Using
    End Sub
#End Region

#Region " Accept "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' user add accept
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    ''' 	[Hendry]	25/05/2007	Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptUserAdd()
        Dim oSQLTrans As SqlTransaction = Nothing
        'Using TranScope As New Transactions.TransactionScope
        Using AccessUser As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
            oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessUser, Data.IsolationLevel.ReadUncommitted)
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.User_ApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, oSQLTrans)
                Dim ObjTable As Data.DataTable = AccessPending.GetUserApprovalData(Me.ParamPkUserManagement)

                'Bila ObjTable.Rows.Count > 0 berarti User tsb masih ada dlm tabel User_Approval
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.User_ApprovalRow = ObjTable.Rows(0)

                    'Periksa apakah UserID yg baru tsb sudah ada dlm tabel User atau belum. 
                    Dim counter As Int32 = AccessUser.CountMatchingUser(rowData.UserID)

                    'Bila counter = 0 berarti UserID tsb belum ada dlm tabel User, maka boleh ditambahkan
                    If counter = 0 Then
                        'tambahkan item tersebut dalam tabel User
                        AccessUser.Insert(rowData.UserID, rowData.UserEmailAddress, rowData.UserMobilePhone, rowData.UserPassword, rowData.UserPasswordSalt, rowData.UserLastChangedPassword, rowData.UserIPAddress, rowData.UserInUsed, rowData.UserCreatedDate, rowData.UserIsDisabled, rowData.UserGroupId, rowData.UserLastLogin, rowData.UserWorkingUnitMappingStatus, False, rowData.UserName, rowData.UserTellerID)
                        Using AccessHistoryPassword As New AMLDAL.AMLDataSetTableAdapters.HistoryPasswordTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessHistoryPassword, oSQLTrans)
                            AccessHistoryPassword.Insert(rowData.UserID, rowData.UserPassword, Now)
                        End Using
                        Sahassa.AML.AuditTrailAlert.AuditTrailChecking(15)
                        'catat aktifitas dalam tabel Audit Trail
                        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserID", "Add", "", rowData.UserID, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserName", "Add", "", rowData.UserName, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserEmailAddress", "Add", "", rowData.UserEmailAddress, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserMobilePhone", "Add", "", rowData.UserMobilePhone, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserPassword", "Add", "", rowData.UserPassword, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserPasswordSalt", "Add", "", rowData.UserPasswordSalt, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserLastChangedPassword", "Add", "", rowData.UserLastChangedPassword.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserIPAddress", "Add", "", rowData.UserIPAddress, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserInUsed", "Add", "", rowData.UserInUsed, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserCreatedDate", "Add", "", rowData.UserCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserIsDisabled", "Add", "", rowData.UserIsDisabled, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserGroupId", "Add", "", rowData.UserGroupId, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserLastLogin", "Add", "", rowData.UserLastLogin.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserWorkingUnitMappingStatus", "Add", "", rowData.UserWorkingUnitMappingStatus, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "isCreatedBySU", "Add", "", rowData.isCreatedBySU, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserTellerID", "Add", "", rowData.UserTellerID, "Accepted")

                            'hapus item tersebut dalam tabel User_Approval
                            AccessPending.DeleteUserApproval(rowData.User_PendingApprovalID)

                            'hapus item tersebut dalam tabel User_PendingApproval
                            Using AccessPendingWorkingUnit As New AMLDAL.AMLDataSetTableAdapters.User_PendingApprovalTableAdapter
                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPendingWorkingUnit, oSQLTrans)
                                AccessPendingWorkingUnit.DeleteUserPendingApproval(rowData.User_PendingApprovalID)
                            End Using
                        End Using

                        oSQLTrans.Commit()
                    Else 'Bila counter != 0 berarti UserID tsb sudah ada dlm tabel User, maka AcceptUserAdd gagal
                        Throw New Exception("Cannot add the following User: " & rowData.UserID & " because that UserID already exists in the database.")
                    End If
                Else 'Bila ObjTable.Rows.Count = 0 berarti User tsb sudah tidak lagi berada dlm tabel User_Approval
                    Throw New Exception("Cannot add the following User: " & Me.LabelUserIdAdd.Text & " because that User is no longer in the approval table.")
                End If
            End Using
        End Using
        'End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' user edit accept
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    ''' 	[Hendry]	25/05/2007	Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptUserEdit()
        Dim oSQLTrans As SqlTransaction = Nothing
        'Using TranScope As New Transactions.TransactionScope
        Using AccessUser As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
            oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessUser, Data.IsolationLevel.ReadUncommitted)
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.User_ApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, oSQLTrans)
                Dim ObjTable As Data.DataTable = AccessPending.GetUserApprovalData(Me.ParamPkUserManagement)

                'Bila ObjTable.Rows.Count > 0 berarti User tsb masih ada dlm tabel User_Approval
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.User_ApprovalRow = ObjTable.Rows(0)

                    'Periksa apakah UserID yg baru tsb sudah ada dlm tabel User atau belum. 
                    Dim counter As Int32 = AccessUser.CountMatchingUser(rowData.UserID)


                    'Bila tidak ada perubahan UserID
                    If rowData.UserID = rowData.UserID_Old Then
                        GoTo Edit
                    Else 'Bila ada perubahan UserID
                        'Counter = 0 berarti UserID tersebut tidak ada dalam tabel User
                        If counter = 0 Then
Edit:
                            'update item tersebut dalam tabel User
                            AccessUser.UpdateUser(rowData.pkUserID, rowData.UserID, rowData.UserName, rowData.UserEmailAddress, rowData.UserMobilePhone, rowData.UserPassword, rowData.UserPasswordSalt, rowData.UserLastChangedPassword, rowData.UserIPAddress, rowData.UserInUsed, rowData.UserCreatedDate, rowData.UserIsDisabled, rowData.UserGroupId, rowData.UserLastLogin, rowData.UserWorkingUnitMappingStatus, rowData.isCreatedBySU, rowData.UserTellerID)
                            If rowData.UserPassword <> rowData.UserPassword_Old Then
                                Using AccessHistoryPassword As New AMLDAL.AMLDataSetTableAdapters.HistoryPasswordTableAdapter
                                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessHistoryPassword, oSQLTrans)
                                    AccessHistoryPassword.Insert(rowData.UserID, rowData.UserPassword, Now)
                                End Using
                            End If
                            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(15)
                            'catat aktifitas dalam tabel Audit Trail
                            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                                AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserID", "Edit", rowData.UserID_Old, rowData.UserID, "Accepted")
                                AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserName", "Edit", rowData.UserName_Old, rowData.UserName, "Accepted")
                                AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserEmailAddress", "Edit", rowData.UserEmailAddress_Old, rowData.UserEmailAddress, "Accepted")
                                AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserMobilePhone", "Edit", rowData.UserMobilePhone_Old, rowData.UserMobilePhone, "Accepted")
                                AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserPassword", "Edit", rowData.UserPassword_Old, rowData.UserPassword, "Accepted")
                                AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserPasswordSalt", "Edit", rowData.UserPasswordSalt_Old, rowData.UserPasswordSalt, "Accepted")
                                AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserLastChangedPassword", "Edit", rowData.UserLastChangedPassword_Old.ToString("dd-MMMM-yyyy HH:mm"), rowData.UserLastChangedPassword.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                                AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserIPAddress", "Edit", rowData.UserIPAddress_Old, rowData.UserIPAddress, "Accepted")
                                AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserInUsed", "Edit", rowData.UserInUsed_Old, rowData.UserInUsed, "Accepted")
                                AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserCreatedDate", "Edit", rowData.UserCreatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), rowData.UserCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                                AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserIsDisabled", "Edit", rowData.UserIsDisabled_Old, rowData.UserIsDisabled, "Accepted")
                                AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserGroupId", "Edit", rowData.UserGroupId_Old, rowData.UserGroupId, "Accepted")
                                AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserLastLogin", "Edit", rowData.UserLastLogin_Old.ToString("dd-MMMM-yyyy HH:mm"), rowData.UserLastLogin.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                                AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserWorkingUnitMappingStatus", "Edit", rowData.UserWorkingUnitMappingStatus_Old, rowData.UserWorkingUnitMappingStatus, "Accepted")
                                AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "isCreatedBySU", "Edit", rowData.isCreatedBySU_Old, rowData.isCreatedBySU, "Accepted")
                                AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserTellerID", "Edit", rowData.UserTellerID_Old, rowData.UserTellerID, "Accepted")

                                'hapus item tersebut dalam tabel User_Approval
                                AccessPending.DeleteUserApproval(rowData.User_PendingApprovalID)

                                'hapus item tersebut dalam tabel User_PendingApproval
                                Using AccessPendingUser As New AMLDAL.AMLDataSetTableAdapters.User_PendingApprovalTableAdapter
                                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPendingUser, oSQLTrans)
                                    AccessPendingUser.DeleteUserPendingApproval(rowData.User_PendingApprovalID)
                                End Using
                                oSQLTrans.Commit()
                            End Using
                        Else 'Bila counter != 0 berarti UserID tsb telah ada dlm tabel User, maka AcceptUserEdit gagal
                            Throw New Exception("Cannot change to the following User ID: " & rowData.UserID & " because that UserID already exists in the database.")
                        End If
                    End If
                Else 'Bila ObjTable.Rows.Count = 0 berarti User tsb sudah tidak lagi berada dlm tabel User_Approval
                    Throw New Exception("Cannot edit the following User: " & Me.LabelUserIdOld.Text & " because that User is no longer in the approval table.")
                End If
            End Using
        End Using
        'End Using
    End Sub
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' user delete accept
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    ''' 	[Hendry]	25/05/2007	Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptUserDelete()
        Dim oSQLTrans As SqlTransaction = Nothing
        'Using TranScope As New Transactions.TransactionScope
        Using AccessUser As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
            oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessUser, Data.IsolationLevel.ReadUncommitted)
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.User_ApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, oSQLTrans)
                Dim ObjTable As Data.DataTable = AccessPending.GetUserApprovalData(Me.ParamPkUserManagement)

                'Bila ObjTable.Rows.Count > 0 berarti User tsb masih ada dlm tabel User_Approval
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.User_ApprovalRow = ObjTable.Rows(0)

                    'Periksa apakah User yg hendak didelete tsb ada dlm tabelnya atau tidak. 
                    Dim counter As Int32 = AccessUser.CountMatchingUser(rowData.UserID)

                    'Bila counter = 0 berarti User tsb tidak ada dlm tabel User, maka AcceptUserDelete gagal
                    If counter = 0 Then
                        Throw New Exception("Cannot delete the following UserID: " & rowData.UserID & " because that User does not exist in the database anymore.")
                    Else 'Bila counter != 0, maka User tsb bisa didelete
                        'hapus item tersebut dari tabel User
                        AccessUser.DeleteUser(rowData.pkUserID)
                        Using AccessHistoryPassword As New AMLDAL.AMLDataSetTableAdapters.HistoryPasswordTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessHistoryPassword, oSQLTrans)
                            AccessHistoryPassword.DeleteHistoryPasswordByUserId(rowData.UserID)
                        End Using
                        Sahassa.AML.AuditTrailAlert.AuditTrailChecking(15)
                        'catat aktifitas dalam tabel Audit Trail
                        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserID", "Delete", rowData.UserID_Old, rowData.UserID, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserName", "Delete", rowData.UserName_Old, rowData.UserName, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserEmailAddress", "Delete", rowData.UserEmailAddress_Old, rowData.UserEmailAddress, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserMobilePhone", "Delete", rowData.UserMobilePhone_Old, rowData.UserMobilePhone, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserPassword", "Delete", rowData.UserPassword_Old, rowData.UserPassword, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserPasswordSalt", "Delete", rowData.UserPasswordSalt_Old, rowData.UserPasswordSalt, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserLastChangedPassword", "Delete", rowData.UserLastChangedPassword_Old.ToString("dd-MMMM-yyyy HH:mm"), rowData.UserLastChangedPassword.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserIPAddress", "Delete", rowData.UserIPAddress_Old, rowData.UserIPAddress, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserInUsed", "Delete", rowData.UserInUsed_Old, rowData.UserInUsed, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserCreatedDate", "Delete", rowData.UserCreatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), rowData.UserCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserIsDisabled", "Delete", rowData.UserIsDisabled_Old, rowData.UserIsDisabled, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserGroupId", "Delete", rowData.UserGroupId_Old, rowData.UserGroupId, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserLastLogin", "Delete", rowData.UserLastLogin_Old.ToString("dd-MMMM-yyyy HH:mm"), rowData.UserLastLogin.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserWorkingUnitMappingStatus", "Delete", rowData.UserWorkingUnitMappingStatus_Old, rowData.UserWorkingUnitMappingStatus, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "isCreatedBySU", "Delete", rowData.isCreatedBySU_Old, rowData.isCreatedBySU, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserTellerID", "Delete", rowData.UserTellerID_Old, rowData.UserTellerID, "Accepted")

                            'hapus item tersebut dalam tabel User_Approval
                            AccessPending.DeleteUserApproval(rowData.User_PendingApprovalID)

                            'hapus item tersebut dalam tabel User_PendingApproval
                            Using AccessPendingUser As New AMLDAL.AMLDataSetTableAdapters.User_PendingApprovalTableAdapter
                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPendingUser, oSQLTrans)
                                AccessPendingUser.DeleteUserPendingApproval(rowData.User_PendingApprovalID)
                            End Using

                            oSQLTrans.Commit()
                        End Using
                    End If
                Else 'Bila ObjTable.Rows.Count = 0 berarti User tsb sudah tidak lagi berada dlm tabel User_Approval
                    Throw New Exception("Cannot delete the following User: " & Me.LabelUserIdOld.Text & " because that User is no longer in the approval table.")
                End If
            End Using
        End Using
        'End Using
    End Sub
#End Region

#Region " Reject "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' reject user add
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    ''' 	[Hendry]	25/05/2007	Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectUserAdd()
        Dim oSQLTrans As SqlTransaction = Nothing
        'Using TranScope As New Transactions.TransactionScope
        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
            oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAudit, Data.IsolationLevel.ReadUncommitted)
            Using AccessUser As New AMLDAL.AMLDataSetTableAdapters.User_ApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessUser, oSQLTrans)
                Using AccessUserPending As New AMLDAL.AMLDataSetTableAdapters.User_PendingApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessUserPending, oSQLTrans)
                    Using TableUser As Data.DataTable = AccessUser.GetUserApprovalData(Me.ParamPkUserManagement)

                        'Bila TableUser.Rows.Count > 0 berarti User tsb masih ada dlm tabel User_Approval
                        If TableUser.Rows.Count > 0 Then
                            Dim ObjRow As AMLDAL.AMLDataSet.User_ApprovalRow = TableUser.Rows(0)

                            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(15)
                            'catat aktifitas dalam tabel Audit Trail
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserID", "Add", "", ObjRow.UserID, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserName", "Add", "", ObjRow.UserName, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserEmailAddress", "Add", "", ObjRow.UserEmailAddress, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserMobilePhone", "Add", "", ObjRow.UserMobilePhone, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserPassword", "Add", "", ObjRow.UserPassword, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserPasswordSalt", "Add", "", ObjRow.UserPasswordSalt, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserLastChangedPassword", "Add", "", ObjRow.UserLastChangedPassword.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserIPAddress", "Add", "", ObjRow.UserIPAddress, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserInUsed", "Add", "", ObjRow.UserInUsed, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserCreatedDate", "Add", "", ObjRow.UserCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserIsDisabled", "Add", "", ObjRow.UserIsDisabled, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserGroupId", "Add", "", ObjRow.UserGroupId, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserLastLogin", "Add", "", ObjRow.UserLastLogin.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserWorkingUnitMappingStatus", "Add", "", ObjRow.UserWorkingUnitMappingStatus, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "isCreatedBySU", "Add", "", ObjRow.isCreatedBySU, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserTellerID", "Add", "", ObjRow.UserTellerID, "Rejected")

                            'hapus item tersebut dalam tabel User_Approval
                            AccessUser.DeleteUserApproval(Me.ParamPkUserManagement)

                            'hapus item tersebut dalam tabel User_PendingApproval
                            AccessUserPending.DeleteUserPendingApproval(Me.ParamPkUserManagement)
                        Else 'Bila TableUser.Rows.Count = 0 berarti User tsb sudah tidak lagi berada dlm tabel User_Approval
                            Throw New Exception("Operation failed. The following User: " & Me.LabelUserIdAdd.Text & " is no longer in the approval table.")
                        End If
                    End Using
                End Using
            End Using
        End Using

        oSQLTrans.Commit()

        'End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' reject user edit
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    ''' 	[Hendry]	25/05/2007	Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectUserEdit()
        Dim oSQLTrans As SqlTransaction = Nothing
        'Using TranScope As New Transactions.TransactionScope
        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
            oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAudit, Data.IsolationLevel.ReadUncommitted)
            Using AccessUser As New AMLDAL.AMLDataSetTableAdapters.User_ApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessUser, oSQLTrans)
                Using AccessUserPending As New AMLDAL.AMLDataSetTableAdapters.User_PendingApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessUserPending, oSQLTrans)
                    Using TableUser As Data.DataTable = AccessUser.GetUserApprovalData(Me.ParamPkUserManagement)
                        'Bila TableUser.Rows.Count > 0 berarti User tsb masih ada dlm tabel User_Approval
                        If TableUser.Rows.Count > 0 Then
                            Dim ObjRow As AMLDAL.AMLDataSet.User_ApprovalRow = TableUser.Rows(0)

                            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(15)
                            'catat aktifitas dalam tabel Audit Trail
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserID", "Edit", ObjRow.UserID_Old, ObjRow.UserID, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserName", "Edit", ObjRow.UserName_Old, ObjRow.UserName, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserEmailAddress", "Edit", ObjRow.UserEmailAddress_Old, ObjRow.UserEmailAddress, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserMobilePhone", "Edit", ObjRow.UserMobilePhone_Old, ObjRow.UserMobilePhone, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserPassword", "Edit", ObjRow.UserPassword_Old, ObjRow.UserPassword, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserPasswordSalt", "Edit", ObjRow.UserPasswordSalt_Old, ObjRow.UserPasswordSalt, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserLastChangedPassword", "Edit", ObjRow.UserLastChangedPassword_Old.ToString("dd-MMMM-yyyy HH:mm"), ObjRow.UserLastChangedPassword.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserIPAddress", "Edit", ObjRow.UserIPAddress_Old, ObjRow.UserIPAddress, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserInUsed", "Edit", ObjRow.UserInUsed_Old, ObjRow.UserInUsed, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserCreatedDate", "Edit", ObjRow.UserCreatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), ObjRow.UserCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserIsDisabled", "Edit", ObjRow.UserIsDisabled_Old, ObjRow.UserIsDisabled, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserGroupId", "Edit", ObjRow.UserGroupId_Old, ObjRow.UserGroupId, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserLastLogin", "Edit", ObjRow.UserLastLogin_Old.ToString("dd-MMMM-yyyy HH:mm"), ObjRow.UserLastLogin.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserWorkingUnitMappingStatus", "Edit", ObjRow.UserWorkingUnitMappingStatus_Old, ObjRow.UserWorkingUnitMappingStatus, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "isCreatedBySU", "Edit", ObjRow.isCreatedBySU_Old, ObjRow.isCreatedBySU, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserTellerID", "Edit", ObjRow.UserTellerID_Old, ObjRow.UserTellerID, "Rejected")

                            'hapus item tersebut dalam tabel User_Approval
                            AccessUser.DeleteUserApproval(Me.ParamPkUserManagement)

                            'hapus item tersebut dalam tabel User_PendingApproval
                            AccessUserPending.DeleteUserPendingApproval(Me.ParamPkUserManagement)
                            oSQLTrans.Commit()
                        Else 'Bila TableUser.Rows.Count = 0 berarti User tsb sudah tidak lagi berada dlm tabel User_Approval
                            Throw New Exception("Operation failed. The following User: " & Me.LabelUserIdOld.Text & " is no longer in the approval table.")
                        End If
                    End Using
                End Using
            End Using
        End Using
        'End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' reject user delete
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    ''' 	[Hendry]	25/05/2007	Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectUserDelete()
        Dim oSQLTrans As SqlTransaction = Nothing

        'Using TranScope As New Transactions.TransactionScope
        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
            oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAudit, Data.IsolationLevel.ReadUncommitted)
            Using AccessUser As New AMLDAL.AMLDataSetTableAdapters.User_ApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessUser, oSQLTrans)
                Using AccessUserPending As New AMLDAL.AMLDataSetTableAdapters.User_PendingApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessUserPending, oSQLTrans)
                    Using TableUser As Data.DataTable = AccessUser.GetUserApprovalData(Me.ParamPkUserManagement)
                        'Bila TableUser.Rows.Count > 0 berarti User tsb masih ada dlm tabel User_Approval
                        If TableUser.Rows.Count > 0 Then
                            Dim ObjRow As AMLDAL.AMLDataSet.User_ApprovalRow = TableUser.Rows(0)

                            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(15)
                            'catat aktifitas dalam tabel Audit Trail
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserID", "Delete", ObjRow.UserID_Old, ObjRow.UserID, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserName", "Delete", ObjRow.UserName_Old, ObjRow.UserName, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserEmailAddress", "Delete", ObjRow.UserEmailAddress_Old, ObjRow.UserEmailAddress, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserMobilePhone", "Delete", ObjRow.UserMobilePhone_Old, ObjRow.UserMobilePhone, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserPassword", "Delete", ObjRow.UserPassword_Old, ObjRow.UserPassword, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserPasswordSalt", "Delete", ObjRow.UserPasswordSalt_Old, ObjRow.UserPasswordSalt, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserLastChangedPassword", "Delete", ObjRow.UserLastChangedPassword_Old.ToString("dd-MMMM-yyyy HH:mm"), ObjRow.UserLastChangedPassword.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserIPAddress", "Delete", ObjRow.UserIPAddress_Old, ObjRow.UserIPAddress, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserInUsed", "Delete", ObjRow.UserInUsed_Old, ObjRow.UserInUsed, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserCreatedDate", "Delete", ObjRow.UserCreatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), ObjRow.UserCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserIsDisabled", "Delete", ObjRow.UserIsDisabled_Old, ObjRow.UserIsDisabled, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserGroupId", "Delete", ObjRow.UserGroupId_Old, ObjRow.UserGroupId, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserLastLogin", "Delete", ObjRow.UserLastLogin_Old.ToString("dd-MMMM-yyyy HH:mm"), ObjRow.UserLastLogin.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserWorkingUnitMappingStatus", "Delete", ObjRow.UserWorkingUnitMappingStatus_Old, ObjRow.UserWorkingUnitMappingStatus, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "isCreatedBySU", "Delete", ObjRow.isCreatedBySU_Old, ObjRow.isCreatedBySU, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserTellerID", "Delete", ObjRow.UserTellerID_Old, ObjRow.UserTellerID, "Rejected")

                            'hapus item tersebut dalam tabel User_Approval
                            AccessUser.DeleteUserApproval(Me.ParamPkUserManagement)

                            'hapus item tersebut dalam tabel User_PendingApproval
                            AccessUserPending.DeleteUserPendingApproval(Me.ParamPkUserManagement)

                        Else 'Bila TableUser.Rows.Count = 0 berarti User tsb sudah tidak lagi berada dlm tabel User_Approval
                            Throw New Exception("Operation failed. The following User: " & Me.LabelUserIdDelete.Text & " is no longer in the approval table.")
                        End If
                    End Using
                End Using
            End Using
        End Using

        oSQLTrans.Commit()
        'End Using
    End Sub
#End Region

    ''' <summary>
    ''' page load event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.ImageAccept.Attributes.Add("onclick", "javascript: if(confirm('Do you want to accept this activity ?')== false) return false;")
                Me.ImageReject.Attributes.Add("onclick", "javascript: if(confirm('Do you want to reject this activity ?')== false) return false;")
                Me.HideAll()
                Select Case Me.ParamType
                    Case Sahassa.AML.Commonly.TypeConfirm.UserAdd
                        Me.LoadUserAdd()
                    Case Sahassa.AML.Commonly.TypeConfirm.UserEdit
                        Me.LoadUserEdit()
                    Case Sahassa.AML.Commonly.TypeConfirm.UserDelete
                        Me.LoadUserDelete()
                    Case Else
                        Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
                End Select

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' accept button handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click
        Try
            Select Case Me.ParamType
                Case Sahassa.AML.Commonly.TypeConfirm.UserAdd
                    Me.AcceptUserAdd()
                Case Sahassa.AML.Commonly.TypeConfirm.UserEdit
                    Me.AcceptUserEdit()
                Case Sahassa.AML.Commonly.TypeConfirm.UserDelete
                    Me.AcceptUserDelete()
                Case Else
                    Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
            End Select

            Sahassa.AML.Commonly.SessionIntendedPage = "UserManagementApproval.aspx"

            Me.Response.Redirect("UserManagementApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' reject button handlert
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click
        Try
            Select Case Me.ParamType
                Case Sahassa.AML.Commonly.TypeConfirm.UserAdd
                    Me.RejectUserAdd()
                Case Sahassa.AML.Commonly.TypeConfirm.UserEdit
                    Me.RejectUserEdit()
                Case Sahassa.AML.Commonly.TypeConfirm.UserDelete
                    Me.RejectUserDelete()
                Case Else
                    Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
            End Select

            Sahassa.AML.Commonly.SessionIntendedPage = "UserManagementApproval.aspx"

            Me.Response.Redirect("UserManagementApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' back button handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "UserManagementApproval.aspx"

        Me.Response.Redirect("UserManagementApproval.aspx", False)
    End Sub
End Class