Imports System.Collections.Generic
Imports System.Data
Imports Sahassa.AML
Imports System.IO
Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports SahassaNettier.Services


Partial Class CustomerTicketNumberView
    Inherits Parent
    Private BindGridFromExcel As Boolean = False
  
#Region " Property "
    ReadOnly Property GetTanggalTransaksi() As String
        Get
            If Not Request.Params("TanggalTransaksi") Is Nothing Then
                Return CStr(Request.Params("TanggalTransaksi")).Replace("'", "''")
            Else
                Return ""
            End If

        End Get
    End Property

    ReadOnly Property GetTipeTransaksi() As String
        Get
            If Not Request.Params("TipeTransaksi") Is Nothing Then
                Return CStr(Request.Params("TipeTransaksi")).Replace("'", "''")
            Else
                Return ""
            End If

        End Get
    End Property

    ReadOnly Property GetNomorTiketTransaksi() As String
        Get
            If Not Request.Params("NomorTiketTransaksi") Is Nothing Then
                Return CStr(Request.Params("NomorTiketTransaksi")).Replace("'", "''")
            Else
                Return ""
            End If
        End Get
    End Property

    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("CustomerTicketNumberView.Selected") Is Nothing, New ArrayList, Session("CustomerTicketNumberView.Selected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("CustomerTicketNumberView.Selected") = value
        End Set
    End Property

    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("CustomerTicketNumberView.Sort") Is Nothing, "TransactionDate  asc", Session("CustomerTicketNumberView.Sort"))
        End Get
        Set(ByVal Value As String)
            Session("CustomerTicketNumberView.Sort") = Value
        End Set
    End Property


    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("CustomerTicketNumberView.CurrentPage") Is Nothing, 0, Session("CustomerTicketNumberView.CurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("CustomerTicketNumberView.CurrentPage") = Value
        End Set
    End Property



    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property


    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("CustomerTicketNumberView.RowTotal") Is Nothing, 0, Session("CustomerTicketNumberView.RowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("CustomerTicketNumberView.RowTotal") = Value
        End Set
    End Property

    'Private Function SetnGetBindTable() As TList(Of TransactionDetailCTRGrips)
    '    Return DataRepository.TransactionDetailCTRGripsProvider.GetPaged(SetAndGetSearchingCriteria, SetnGetSort, SetnGetCurrentPage, Sahassa.AML.Commonly.GetDisplayedTotalRow, SetnGetRowTotal)
    'End Function
    Private Function SetnGetBindTable() As DataTable
        Return Me.TaskListCTRPelakuGetPaged(SetAndGetSearchingCriteria, SetnGetSort, SetnGetCurrentPage, Sahassa.AML.Commonly.GetDisplayedTotalRow, SetnGetRowTotal)
    End Function

    Private Property SetAndGetSearchingCriteria() As String
        Get
            Return IIf(Session("CustomerTicketNumberView.SearchCriteria") Is Nothing, SearchFilter, Session("CustomerTicketNumberView.SearchCriteria"))
        End Get
        Set(ByVal Value As String)
            Session("CustomerTicketNumberView.SearchCriteria") = Value
        End Set
    End Property

#End Region 'done

#Region "Function..."
    Private Function TaskListCTRPelakuGetPaged(ByVal strWhereClause As String, ByVal strOrderBy As String, ByVal intPageIndex As Int32, ByVal intPageSize As Int32, ByRef intTotalRow As Int32) As Data.DataTable
        Dim sql As String = "usp_TaskListCTRPelaku_GetPaged"
        Dim objCmdSelect As New Data.SqlClient.SqlCommand(sql)
        objCmdSelect.CommandType = CommandType.StoredProcedure
        objCmdSelect.Parameters.Add("@WhereClause", SqlDbType.VarChar)
        objCmdSelect.Parameters.Add("@OrderBy", SqlDbType.VarChar)
        objCmdSelect.Parameters.Add("@PageIndex", SqlDbType.Int)
        objCmdSelect.Parameters.Add("@PageSize", SqlDbType.Int)
        objCmdSelect.Parameters("@WhereClause").Value = strWhereClause
        objCmdSelect.Parameters("@OrderBy").Value = strOrderBy
        objCmdSelect.Parameters("@PageIndex").Value = intPageIndex
        objCmdSelect.Parameters("@PageSize").Value = intPageSize

        Using dsTaskListCTRPelaku As Data.DataSet = DataRepository.Provider.ExecuteDataSet(objCmdSelect)
            intTotalRow = dsTaskListCTRPelaku.Tables(1).Rows(0).Item("TotalRowCount")
            Return dsTaskListCTRPelaku.Tables(0)
        End Using
    End Function

    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
            Me.CollectSelected()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Function isValidSearch() As Boolean
        Try
            If TxtTransactionDate.Text <> "" Then
                If Sahassa.AML.Commonly.IsDateValid("yyyy-MM-dd", TxtTransactionDate.Text) = False Then
                    Throw New Exception("Transaction date tidak valid, gunakan format yyyy-MM-dd")
                End If
            End If
            If TxtAmount.Text <> "" Then
                If Not IsNumeric(TxtAmount.Text) Then
                    Throw New Exception("Amount harus di isi dengan angka")
                End If
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
            Return False
        End Try
        Return True
    End Function

    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
        Catch
            Throw
        End Try
    End Sub

    Public Sub BindGrid()

        Me.GridDataView.DataSource = Me.SetnGetBindTable
        Me.GridDataView.VirtualItemCount = Me.SetnGetRowTotal
        Me.GridDataView.DataBind()
    End Sub

    Private Sub ClearThisPageSessions()
        Session("CustomerTicketNumberView.Selected") = Nothing
        Session("CustomerTicketNumberView.IsSearch") = Nothing
        Session("CustomerTicketNumberView.Sort") = Nothing
        Session("CustomerTicketNumberView.CurrentPage") = Nothing
        Session("CustomerTicketNumberView.RowTotal") = Nothing
        Session("CustomerTicketNumberView.SearchCriteria") = Nothing
    End Sub

    Private Function SearchFilter() As String
        Dim StrSearch As String = ""
        Try
            If isValidSearch() Then
                If Me.TxtCIFNumber.Text <> "" Then
                    If StrSearch = "" Then
                        StrSearch = StrSearch & " CIFNo like '%" & Me.TxtCIFNumber.Text.Replace("'", "''") & "%'"
                    Else
                        StrSearch = StrSearch & " And CIFNo like '%" & Me.TxtCIFNumber.Text.Replace("'", "''") & "%' "
                    End If
                End If
                If Me.TxtAccountNumber.Text <> "" Then
                    If StrSearch = "" Then
                        StrSearch = StrSearch & " AccountNo like '%" & Me.TxtAccountNumber.Text.Replace("'", "''") & "%'"
                    Else
                        StrSearch = StrSearch & " And AccountNo like '%" & Me.TxtAccountNumber.Text.Replace("'", "''") & "%' "
                    End If
                End If
                If Me.TxtName.Text <> "" Then
                    If StrSearch = "" Then
                        StrSearch = StrSearch & " CustomerName like '%" & Me.TxtName.Text.Replace("'", "''") & "%'"
                    Else
                        StrSearch = StrSearch & " And CustomerName like '%" & Me.TxtName.Text.Replace("'", "''") & "%' "
                    End If
                End If

                If Me.TxtTransactionDate.Text <> "" Then
                    If StrSearch = "" Then
                        StrSearch = StrSearch & " TransactionDate = '" & Me.TxtTransactionDate.Text.Replace("'", "''") & "' "
                    Else
                        StrSearch = StrSearch & " And TransactionDate = '" & Me.TxtTransactionDate.Text.Replace("'", "''") & "' "
                    End If
                End If
                If Me.TxtAccountOwnerID.Text <> "" Then
                    If StrSearch = "" Then
                        StrSearch = StrSearch & " AccountOwnerId like '%" & Me.TxtAccountOwnerID.Text.Replace("'", "''") & "%'"
                    Else
                        StrSearch = StrSearch & " And AccountOwnerId like '%" & Me.TxtAccountOwnerID.Text.Replace("'", "''") & "%' "
                    End If
                End If
                If Me.TxtAccountOwnerName.Text <> "" Then
                    If StrSearch = "" Then
                        StrSearch = StrSearch & " AccountOwnerName like '%" & Me.TxtAccountOwnerName.Text.Replace("'", "''") & "%'"
                    Else
                        StrSearch = StrSearch & " And AccountOwnerName like '%" & Me.TxtAccountOwnerName.Text.Replace("'", "''") & "%' "
                    End If
                End If
                If Me.TxtDebitOrCredit.Text <> "" Then
                    If StrSearch = "" Then
                        StrSearch = StrSearch & " DebetCredit like '%" & Me.TxtDebitOrCredit.Text.Replace("'", "''") & "%' "
                    Else
                        StrSearch = StrSearch & " and DebetCredit like '%" & Me.TxtDebitOrCredit.Text.Replace("'", "''") & "%' "
                    End If
                End If

                If Me.txtCurrencyType.Text <> "" Then
                    If StrSearch = "" Then
                        StrSearch = StrSearch & " CurrencyType like '%" & Me.txtCurrencyType.Text.Replace("'", "''") & "%' "
                    Else
                        StrSearch = StrSearch & " and CurrencyType like '%" & Me.txtCurrencyType.Text.Replace("'", "''") & "%'"
                    End If
                End If


                If Me.TxtAmount.Text <> "" Then
                    If StrSearch = "" Then
                        StrSearch = StrSearch & " TransactionAmountOriginal like '%" & Me.TxtAmount.Text.Replace("'", "''") & "%' "
                    Else
                        StrSearch = StrSearch & " and TransactionAmountOriginal like '%" & Me.TxtAmount.Text.Replace("'", "''") & "%'"
                    End If
                End If

                If Me.TxtTransactionTicketNumber.Text <> "" Then
                    If StrSearch = "" Then
                        StrSearch = StrSearch & " TransactionTicketNo like '%" & Me.TxtTransactionTicketNumber.Text.Replace("'", "''") & "%' "
                    Else
                        StrSearch = StrSearch & " and TransactionTicketNo like '%" & Me.TxtTransactionTicketNumber.Text.Replace("'", "''") & "%'"
                    End If
                End If

                If Me.TxtAging.Text <> "" Then
                    If StrSearch = "" Then
                        StrSearch = StrSearch & " Aging like '%" & Me.TxtAging.Text.Replace("'", "''") & "%' "
                    Else
                        StrSearch = StrSearch & " and Aging like '%" & Me.TxtAging.Text.Replace("'", "''") & "%'"
                    End If
                End If

                If StrSearch = "" Then
                    StrSearch = StrSearch & " UserIDAML LIKE '%" & Sahassa.AML.Commonly.SessionUserId.Replace("'", "''") & "%'" ' AND FK_CustomerCTR_ID = 0"
                Else
                    StrSearch = StrSearch & " And UserIDAML LIKE '%" & Sahassa.AML.Commonly.SessionUserId.Replace("'", "''") & "%'" ' AND FK_CustomerCTR_ID = 0"
                End If

                Return StrSearch
            End If
        Catch
            Throw
            Return ""
        End Try
    End Function

#End Region 'done

#Region "Excel Export Event"

    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In Me.GridDataView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = gridRow.FindControl("CheckBoxExporttoExcel")
                Dim PkId As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
                If ArrTarget.Contains(PkId) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(PkId)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(PkId)
                End If
                Me.SetnGetSelectedItem = ArrTarget
            End If
        Next
    End Sub

    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub 'ClearControls

    Private Sub BindSelected()
        Dim Rows As New ArrayList

        Dim SbPk As New StringBuilder
        SbPk.Append("0, ")
        For Each IdPk As Int64 In Me.SetnGetSelectedItem
            SbPk.Append(IdPk.ToString & ", ")
        Next
        Me.GridDataView.DataSource = Me.TaskListCTRPelakuGetPaged("TransactionDetailCTRGrips_ID in (" & SbPk.ToString.Substring(0, SbPk.Length - 2) & ") ", "", 0, Int32.MaxValue, 0)
        Me.GridDataView.AllowPaging = False
        Me.GridDataView.DataBind()

        Me.GridDataView.Columns(0).Visible = False
        Me.GridDataView.Columns(15).Visible = False
    End Sub

    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        For Each gridRow As DataGridItem In Me.GridDataView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(1).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItem.Contains(StrKey) = False Then
                        ArrTarget.Add(StrKey)
                    End If
                Else
                    If Me.SetnGetSelectedItem.Contains(StrKey) Then
                        ArrTarget.Remove(StrKey)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItem = ArrTarget
        Me.BindGrid()
    End Sub

    Private Sub SetCheckedAll()
        Dim i As Int16 = 0
        Dim totalrow As Int16 = 0
        For Each gridRow As DataGridItem In Me.GridDataView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = CType(gridRow.FindControl("CheckBoxExporttoExcel"), CheckBox)
                If chkBox.Checked Then
                    i = CType(i + 1, Int16)
                End If
                totalrow = CType(totalrow + 1, Int16)
            End If
        Next
        If i = totalrow Then
            Me.CheckBoxSelectAll.Checked = True
        Else
            Me.CheckBoxSelectAll.Checked = False
        End If
    End Sub

#End Region

#Region "events..."

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using

                Me.ClearThisPageSessions()

                Me.popUpTransactionDate.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TxtTransactionDate.ClientID & "'), 'yyyy-mm-dd')")
                Me.popUpTransactionDate.Style.Add("display", "")

                Me.GridDataView.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow

            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.CollectSelected()
            Me.BindGrid()
            SetCheckedAll()
            Me.SetInfoNavigate()

        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub ClearSearch()
        Me.SetAndGetSearchingCriteria = Nothing
        Me.TxtCIFNumber.Text = ""
        Me.TxtAccountNumber.Text = ""
        Me.TxtName.Text = ""
        Me.TxtTransactionDate.Text = ""
        Me.TxtAccountOwnerID.Text = ""
        Me.TxtDebitOrCredit.Text = ""
        Me.txtCurrencyType.Text = ""
        Me.TxtAmount.Text = ""
        Me.TxtTransactionTicketNumber.Text = ""
        Me.TxtAging.Text = ""
    End Sub

    Protected Sub BtnClearSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnClearSearch.Click
        Try
            Me.ClearSearch()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridDataView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridDataView.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                If BindGridFromExcel = True Then
                    e.Item.Cells(2).Text = CStr((e.Item.ItemIndex + 1))
                Else
                    e.Item.Cells(2).Text = CStr((e.Item.ItemIndex + 1) + (Me.SetnGetCurrentPage * Sahassa.AML.Commonly.GetDisplayedTotalRow))
                End If

                Dim chkBox As CheckBox = e.Item.FindControl("CheckBoxExporttoExcel")
                chkBox.Checked = Me.SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridDataView_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridDataView.SortCommand
        Dim GridUser As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            Me.CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be less than or equal to the total page count.")
                End If
            Else
                Throw New Exception("Page number must be less than or equal to the total page count.")
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub GridDataView_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridDataView.ItemCommand
        If e.CommandName.ToLower = "edit" Then
            Dim StrParam As String
            StrParam = "TanggalTransaksi=" & e.Item.Cells(6).Text & "&TipeTransaksi=" & e.Item.Cells(9).Text & "&NomorTiketTransaksi=" & e.Item.Cells(13).Text
            Response.Redirect("CustomerCTRView.aspx?" & StrParam)
        End If
    End Sub


#End Region

    Protected Sub ImageButtonSearch_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Try
            If isValidSearch() Then
                SetAndGetSearchingCriteria = SearchFilter()
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try

    End Sub

    Protected Sub LnkExportToExcel_Click(sender As Object, e As System.EventArgs) Handles LnkExportToExcel.Click
        Try
            BindGridFromExcel = True
            Me.CollectSelected()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelected()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=CustomerTicketNumber.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridDataView)
            GridDataView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class '687