#Region "Imports..."
Imports Sahassanettier.Data
Imports Sahassanettier.Entities
Imports System.Collections.Generic
Imports amlbll
Imports amlbll.ValidateBLL
Imports amlbll.DataType
Imports Sahassa.aml.Commonly
#End Region
Partial Class IFTI_Detail_NonSwift_Outgoing
    Inherits Parent
#Region "properties..."
    ReadOnly Property getIFTIPK() As Integer
        Get
            If Session("IFTIEdit.IFTIPK") = Nothing Then
                Session("IFTIEdit.IFTIPK") = CInt(Request.Params("ID"))
            End If
            Return Session("IFTIEdit.IFTIPK")
        End Get
    End Property
    Private Property SetnGetSenderAccount() As String
        Get
            Return IIf(Session("IFTIEdit.SenderAccount") Is Nothing, "", Session("IFTIEdit.SenderAccount"))
        End Get
        Set(ByVal Value As String)
            Session("IFTIEdit.SenderAccount") = Value
        End Set
    End Property

    'ReadOnly Property getSenderAccount() As Integer
    '    Get
    '        If Session("IFTIEdit.SenderAccount") = Nothing Then
    '            Session("IFTIEdit.SenderAccount") = CInt(Request.Params("ID"))
    '        End If
    '        Return Session("IFTIEdit.SenderAccount")
    '    End Get
    'End Property
#End Region
#Region "Validation"
#Region "Validation Sender"
    Public Function IsValidate() As Boolean
        Dim result As Boolean = True
        ValidasiControl()
        '============ Insert Detail Approval
        Using objIfti_ApprovalDetail As New IFTI_Approval_Detail()
            Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)

                Dim senderType As Integer = objIfti.Sender_FK_IFTI_NasabahType_ID

                'cekPengirim
                Select Case (senderType)
                    Case 1
                        'pengirimNasabahIndividu
                        validasiSender1()
                    Case 2
                        'pengirimNasabahKorporasi
                        validasiSender2()
                    Case 3
                        'PengirimNonNasabah
                        validasiSender3()
                End Select
                'cekBOwner
                If Not IsNothing(objIfti.FK_IFTI_BeneficialOwnerType_ID) Then
                    Dim BOwnerID As Integer = objIfti.FK_IFTI_BeneficialOwnerType_ID
                    Select Case (BOwnerID)
                        Case 1
                            'Nasabah
                            ValidasiBOwnerNasabah()
                        Case 2
                            'NonNasabah
                            ValidasiBOwnerNonNasabah()
                    End Select
                End If
                'cekPenerima
                Using objReceiver As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("FK_IFTI_ID=" & getIFTIPK, "", 0, Integer.MaxValue, 0)
                    Dim TipePenerima As Integer = objReceiver(0).FK_IFTI_NasabahType_ID
                    Select Case (TipePenerima)
                        Case 1
                            validasiReceiverInd()
                        Case 2
                            validasiReceiverKorp()
                        Case 3
                            validasiReceiverNonNasabah()
                    End Select
                End Using
                'cekTransaksi
                validasiTransaksi()
            End Using
        End Using
        Return result
    End Function
    Sub ValidasiControl()
        If ObjectAntiNull(TxtUmum_NonSwiftOutTanggalLaporan.Text) = False Then Throw New Exception("Tanggal Laporan harus diisi  ")
        If Me.Rb_Umum_NonSwiftOut_jenislaporan.SelectedValue <> "1" And ObjectAntiNull(TxtUmum_NonSwiftOutLTDLNKoreksi.Text) = False Then Throw New Exception("No. LTDLN Koreksi harus diisi  ")

        If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", TxtUmum_NonSwiftOutTanggalLaporan.Text) = False Then
            Throw New Exception("Tanggal Laporan tidak valid")
        End If
        If ObjectAntiNull(TxtUmum_NonSwiftOutPJKBankPelapor.Text) = False Or TxtUmum_NonSwiftOutPJKBankPelapor.Text.Length < 2 Then Throw New Exception("Nama PJK Bank Pelapor harus diisi lebih dari 2 karakter! ")
        If ObjectAntiNull(TxtUmum_NonSwiftOutNamaPejabatPJKBankPelapor.Text) = False Or TxtUmum_NonSwiftOutNamaPejabatPJKBankPelapor.Text.Length < 2 Then Throw New Exception("Nama Pejabat PJK Bank Pelapor harus diisi lebih dari 2 karakter! ")
        If Rb_Umum_NonSwiftOut_jenislaporan.SelectedIndex = -1 Then Throw New Exception("Jenis Laporan harus dipilih  ")

    End Sub
    Sub validasiSender1()
        'senderIndividu
        'If CboSwiftOutPengirim.SelectedIndex = -1 Then Throw New Exception("Tipe PJK Bank harus diisi  ")
        If Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipePengirim.SelectedIndex = -1 Then Throw New Exception("Tipe Pengirim harus diisi  ")
        If Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipeNasabah.SelectedIndex = -1 Then Throw New Exception("Tipe Nasabah harus diisi  ")

        If ObjectAntiNull(TxtIdenNonSwiftOutPengirim_Norekening.Text) = False Or TxtIdenNonSwiftOutPengirim_Norekening.Text.Length < 2 Then Throw New Exception("Pengirim Nasabah Perorangan: No Rekening harus diisi lebih dari 2 karakter! ")
        If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_NamaLengkap.Text) = False Then Throw New Exception("Pengirim Nasabah Perorangan: Nama Lengkap harus diisi  ")
        If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_TanggalLahir.Text) = False Then Throw New Exception("Pengirim Nasabah Perorangan: Tanggal Lahir harus diisi  ")
        If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", TxtIdenNonSwiftOutPengirimNas_Ind_TanggalLahir.Text) = False Then
            Throw New Exception("Pengirim Nasabah Perorangan :Tanggal Lahir tidak valid")
        End If
        If RbIdenNonSwiftOutPengirimNas_Ind_Kewarganegaraan.SelectedValue = "2" Then
            If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_Negara.Text) = False And ObjectAntiNull(Me.TxtIdenNonSwiftOutPengirimNas_Ind_NegaraLain.Text) = False Then Throw New Exception("Pengirim Nasabah Perorangan: Negara harus diisi  ")
            If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_Negara.Text) = True And ObjectAntiNull(Me.TxtIdenNonSwiftOutPengirimNas_Ind_NegaraLain.Text) = True Then Throw New Exception("Pengirim Nasabah Perorangan: Negara harus diisi salah satu ")
        End If
        If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_Pekerjaan.Text) = False And ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_Pekerjaanlain.Text) = False Then Throw New Exception("Pengirim Nasabah Perorangan: Pekerjaan harus diisi  ")
        If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_Pekerjaan.Text) = True And ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_Pekerjaanlain.Text) = True Then Throw New Exception("Pengirim Nasabah Perorangan: Pekerjaan harus diisi salah satu ")
        'alamat
        'dom
        If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_Kota.Text) = True And ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_kotalain.Text) = True Then Throw New Exception("Pengirim Nasabah Perorangan: Kota/Kabupaten harus diisi Salah satu  ")
        If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_provinsi.Text) = True And ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_provinsiLain.Text) = True Then Throw New Exception("Pengirim Nasabah Perorangan: Provinsi harus diisi salah satu ")

        'iden
        If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_alamatIdentitas.Text) = False Or TxtIdenNonSwiftOutPengirimNas_Ind_alamatIdentitas.Text.Length < 2 Then Throw New Exception("Pengirim Nasabah Perorangan: Alamat Identitas harus diisi lebih dari 2 karakter! ")
        If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_kotaIdentitas.Text) = False And ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_KotaLainIden.Text) = False Then Throw New Exception("Pengirim Nasabah Perorangan: Kota/Kabupaten harus diisi  ")
        If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_kotaIdentitas.Text) = True And ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_KotaLainIden.Text) = True Then Throw New Exception("Pengirim Nasabah Perorangan: Kota/Kabupaten harus diisi Salah satu  ")
        If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_ProvinsiIden.Text) = False And ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_ProvinsilainIden.Text) = False Then Throw New Exception("Pengirim Nasabah Perorangan: Provinsi harus diisi  ")
        If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_ProvinsiIden.Text) = True And ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_ProvinsilainIden.Text) = True Then Throw New Exception("Pengirim Nasabah Perorangan: Provinsi harus diisi salah satu ")
        If cboNonSwiftOutPengirimNasInd_jenisidentitas.SelectedIndex = -1 Then Throw New Exception("Pengirim Nasabah Perorangan:Jenis Dokumen Identitas harus dipilih ")
        If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_noIdentitas.Text) = False Then Throw New Exception("Pengirim Nasabah Perorangan: Nomor Identitas harus diisi  ")
        If TxtIdenNonSwiftOutPengirimNas_Ind_noIdentitas.Text <> "" Then
            If Not Regex.Match(TxtIdenNonSwiftOutPengirimNas_Ind_noIdentitas.Text.Trim(), "^[a-zA-Z0-9]*$").Success Then
                Throw New Exception("Nomor Identitas hanya boleh menggunakan huruf dan angka!")
            End If
        End If
    End Sub
    Sub validasiSender2()
        'senderKorp
        ' If CboSwiftOutPengirim.SelectedIndex = -1 Then Throw New Exception("Tipe PJK Bank harus diisi  ")
        If Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipePengirim.SelectedIndex = -1 Then Throw New Exception("Tipe Pengirim harus diisi  ")
        If Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipeNasabah.SelectedIndex = -1 Then Throw New Exception("Tipe Nasabah harus diisi  ")

        If ObjectAntiNull(TxtIdenNonSwiftOutPengirim_Norekening.Text) = False Then Throw New Exception("Pengirim Nasabah Korporasi: No Rekening harus diisi  ")
        If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Corp_NamaKorp.Text) = False Or TxtIdenNonSwiftOutPengirimNas_Corp_NamaKorp.Text.Length < 2 Then Throw New Exception("Pengirim Nasabah Korporasi:Nama Lengkap harus diisi salah satu saja! ")
        If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Corp_BidangUsahaKorp.Text) = False Then Throw New Exception("Pengirim Nasabah Korporasi: Bidang Usaha Korporasi harus diisi  ")
        If cboIdenNonSwiftOutPengirimNas_Corp_BentukBadanUsaha.SelectedIndex > 0 And ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Corp_BentukBadanUsahaLain.Text) = True Then Throw New Exception("Pengirim Nasabah Korporasi: Bentuk Usaha Korporasi diisi salah satu saja! ")
        'alamat
        If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Corp_alamatkorp.Text) = False Or TxtIdenNonSwiftOutPengirimNas_Corp_alamatkorp.Text.Length < 2 Then Throw New Exception("Pengirim Nasabah Korporasi: Alamat Identitas harus diisi salah satu saja! ")
        If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Corp_kotakorp.Text) = False And ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Corp_kotakorplain.Text) = False Then Throw New Exception("Pengirim Nasabah Korporasi: Kota/Kabupaten harus diisi  ")
        If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Corp_kotakorp.Text) = True And ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Corp_kotakorplain.Text) = True Then Throw New Exception("Pengirim Nasabah Korporasi: Kota/Kabupaten harus diisi salah satu ")
        If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Corp_provKorp.Text) = False And ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Corp_provKorpLain.Text) = False Then Throw New Exception("Pengirim Nasabah Korporasi: Provinsi harus diisi  ")
        If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Corp_provKorp.Text) = True And ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Corp_provKorpLain.Text) = True Then Throw New Exception("Pengirim Nasabah Korporasi: Provinsi harus diisi salah satu ")
    End Sub
    Sub validasiSender3()
        'senderNOnNasabah
        ' If CboSwiftOutPengirim.SelectedIndex = -1 Then Throw New Exception("Tipe PJK Bank harus diisi  ")
        If Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipeNasabah.SelectedIndex = -1 Then Throw New Exception("Pengirim Nasabah :Tipe Nasabah harus diisi  ")

        If RbNonSwiftOutPengirimNonNasabah_100Juta.SelectedIndex = -1 Then Throw New Exception("Pengirim Non Nasabah: Transaksi harus diisi  ")
        If ObjectAntiNull(TxtNonSwiftOutPengirimNonNasabah_nama.Text) = False Or TxtNonSwiftOutPengirimNonNasabah_nama.Text.Length < 2 Then Throw New Exception("Pengirim Non Nasabah: Nama Lengkap harus diisi lebih dari 2 karakter! ")
        If ObjectAntiNull(TxtNonSwiftOutPengirimNonNasabah_alamatiden.Text) = False Or TxtNonSwiftOutPengirimNonNasabah_alamatiden.Text.Length < 2 Then Throw New Exception("Pengirim Non Nasabah: Alamat harus diisi lebih dari 2 karakter! ")
        If ObjectAntiNull(TxtNonSwiftOutPengirimNonNasabah_kotaIden.Text) = False And ObjectAntiNull(TxtNonSwiftOutPengirimNonNasabah_KoaLainIden.Text) = False Then Throw New Exception("Pengirim Non Nasabah : Kota/Kabupaten harus diisi  ")
        If ObjectAntiNull(TxtNonSwiftOutPengirimNonNasabah_kotaIden.Text) = True And ObjectAntiNull(TxtNonSwiftOutPengirimNonNasabah_KoaLainIden.Text) = True Then Throw New Exception("Pengirim Non Nasabah : Kota/Kabupaten harus diisi salah satu ")
        If ObjectAntiNull(TxtNonSwiftOutPengirimNonNasabah_ProvIden.Text) = False And ObjectAntiNull(TxtNonSwiftOutPengirimNonNasabah_ProvLainIden.Text) = False Then Throw New Exception("Pengirim Non Nasabah : Provinsi harus diisi  ")
        If ObjectAntiNull(TxtNonSwiftOutPengirimNonNasabah_ProvIden.Text) = True And ObjectAntiNull(TxtNonSwiftOutPengirimNonNasabah_ProvLainIden.Text) = True Then Throw New Exception("Pengirim Non Nasabah: Provinsi harus diisi salah satu ")
        If CboNonSwiftOutPengirimNonNasabah_JenisDokumen.SelectedIndex = -1 Then Throw New Exception("Pengirim Non Nasabah:Jenis Dokumen Identitas harus dipilih ")
        'If ObjectAntiNull(TxtNonSwiftOutPengirimNonNasabah_NomorIden.Text) = False Then Throw New Exception("Pengirim Non Nasabah:Nomor Identitas harus diisi  ")
        If TxtNonSwiftOutPengirimNonNasabah_NomorIden.Text <> "" Then
            If Not Regex.Match(TxtNonSwiftOutPengirimNonNasabah_NomorIden.Text.Trim(), "^[a-zA-Z0-9]*$").Success Then
                Throw New Exception("Nomor Identitas hanya boleh menggunakan huruf dan angka!")
            End If
        End If
    End Sub
#End Region
#Region "Validation Receiver"
    Sub validasiReceiverInd()
        'penerimaIndividu
        If ObjectAntiNull(txtPenerimaNonSwiftOut_rekening.Text) = False Or txtPenerimaNonSwiftOut_rekening.Text.Length < 2 Then Throw New Exception("Penerima  : Nomor rekening harus diisi lebih dari 2 karakter! ")
        If RBNonSwiftOutPenerimaNasabah_TipePengirim.SelectedIndex = -1 Then Throw New Exception("Penerima  : Tipe Pengirim harus diisi  ")
        If Rb_NonSwiftOutIdenPenerimaNas_TipeNasabah.SelectedIndex = -1 Then Throw New Exception("Penerima  :Tipe Nasabah harus diisi  ")

        If ObjectAntiNull(txtPenerimaNonSwiftOutNasabah_IND_nama.Text) = False Or txtPenerimaNonSwiftOutNasabah_IND_nama.Text.Length < 2 Then Throw New Exception("Penerima Nasabah Individu: Nama Lengkap harus diisi lebih dari 2 karakter! ")
        If ObjectAntiNull(txtPenerimaNonSwiftOutNasabah_IND_TanggalLahir.Text) = True Then
            If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", txtPenerimaNonSwiftOutNasabah_IND_TanggalLahir.Text) = False Then
                Throw New Exception("Penerima Nasabah Individu: Tanggal Lahir tidak valid")
            End If
        End If
        If ObjectAntiNull(txtPenerimaNonSwiftOutNasabah_IND_alamatIden.Text) = False Or txtPenerimaNonSwiftOutNasabah_IND_alamatIden.Text.Length < 2 Then Throw New Exception("Penerima Nasabah Individu: Alamat Identitas harus diisi lebih dari 2 karakter! ")
        If ObjectAntiNull(txtPenerimaNonSwiftOutNasabah_IND_negaraIden.Text) = False And ObjectAntiNull(txtPenerimaNonSwiftOutNasabah_IND_negaraLain.Text) = False Then Throw New Exception("Penerima Nasabah Individu: Negara harus diisi  ")
        If ObjectAntiNull(txtPenerimaNonSwiftOutNasabah_IND_negaraIden.Text) = True And ObjectAntiNull(txtPenerimaNonSwiftOutNasabah_IND_negaraLain.Text) = True Then Throw New Exception("Penerima Nasabah Individu: Negara harus diisi salah satu ")
        If txtPenerimaNonSwiftOutNasabah_IND_NomorIden.Text <> "" Then
            If Not Regex.Match(txtPenerimaNonSwiftOutNasabah_IND_NomorIden.Text.Trim(), "^[a-zA-Z0-9]*$").Success Then
                Throw New Exception("Nomor Identitas hanya boleh menggunakan huruf dan angka!")
            End If
        End If
    End Sub
    Sub validasiReceiverKorp()
        'pennerimaKorporasi
        If ObjectAntiNull(txtPenerimaNonSwiftOut_rekening.Text) = False Or txtPenerimaNonSwiftOut_rekening.Text.Length < 2 Then Throw New Exception(" Nomor rekening harus diisi lenih dari 2 karakter! ")
        If RBNonSwiftOutPenerimaNasabah_TipePengirim.SelectedIndex = -1 Then Throw New Exception("Tipe Pengirim harus diisi  ")
        If Rb_NonSwiftOutIdenPenerimaNas_TipeNasabah.SelectedIndex = -1 Then Throw New Exception("Tipe Nasabah harus diisi  ")
        If ObjectAntiNull(txtPenerimaNonSwiftOutNasabah_Korp_BentukBadanUsahaLainnya.Text) = True And cboPenerimaNonSwiftOutNasabah_Korp_BentukBadanUsahaLain.SelectedIndex > 0 Then Throw New Exception("Penerima Nasabah Korporasi: Bentuk badan usaha harus diisi salah satu ")
        If ObjectAntiNull(txtPenerimaNonSwiftOutNasabah_Korp_namaKorp.Text) = False Or txtPenerimaNonSwiftOutNasabah_Korp_namaKorp.Text.Length < 2 Then Throw New Exception("Penerima Nasabah Korporasi: Nama Korporasi harus diisi lenih dari 2 karakter! ")
        If ObjectAntiNull(txtPenerimaNonSwiftOutNasabah_Korp_AlamatKorp.Text) = False Or txtPenerimaNonSwiftOutNasabah_Korp_AlamatKorp.Text.Length < 2 Then Throw New Exception("Penerima Nasabah Korporasi: Alamat Identitas harus diisi lebih dari 2 karakter! ")
        If ObjectAntiNull(txtPenerimaNonSwiftOutNasabah_Korp_BidangUsahaKorp.Text) = True And ObjectAntiNull(txtPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Text) = True Then Throw New Exception("Penerima Nasabah Korporasi: Bidang Usaha Lainnya harus diisi salah satu saja! ")
        If ObjectAntiNull(txtPenerimaNonSwiftOutNasabah_Korp_NegaraKorp.Text) = False And ObjectAntiNull(txtPenerimaNonSwiftOutNasabah_Korp_NegaraLainnyaKorp.Text) = False Then Throw New Exception("Penerima Nasabah Korporasi: Negara harus diisi  ")
        If ObjectAntiNull(txtPenerimaNonSwiftOutNasabah_Korp_NegaraKorp.Text) = True And ObjectAntiNull(txtPenerimaNonSwiftOutNasabah_Korp_NegaraLainnyaKorp.Text) = True Then Throw New Exception("Penerima Nasabah Korporasi: Negara harus diisi salah satu ")
    End Sub
    Sub validasiReceiverNonNasabah()
        'penerimaNOnNasabah
        If Rb_NonSwiftOutIdenPenerimaNas_TipeNasabah.SelectedIndex = -1 Then Throw New Exception("Tipe Pengirim harus diisi  ")
        If txtPenerimaNonSwiftOutNonNasabah_KodeRahasia.Text = "" And txtPenerimaNonSwiftOutNonNasabah_rekening.Text = "" Then Throw New Exception("Kode Rahasia atau No Rekening harus diisi salah satu! ")
        If txtPenerimaNonSwiftOutNonNasabah_rekening.Text <> "" And txtPenerimaNonSwiftOutNonNasabah_namabank.Text = "" Then Throw New Exception("Nama Bank harus diisi apabila no Rekening diisi! ")

        If ObjectAntiNull(txtPenerimaNonSwiftOutNonNasabah_Nama.Text) = False Or txtPenerimaNonSwiftOutNonNasabah_Nama.Text.Length < 2 Then Throw New Exception("Penerima Non Nasabah: Nama Lengkap harus diisi lebih dari 2 karakter! ")
        If ObjectAntiNull(txtPenerimaNonSwiftOutNonNasabah_Negara.Text) = False And ObjectAntiNull(txtPenerimaNonSwiftOutNonNasabah_Negara.Text) = False Then Throw New Exception("Penerima Non Nasabah: Negara harus diisi  ")
        If ObjectAntiNull(txtPenerimaNonSwiftOutNonNasabah_Negara.Text) = True And ObjectAntiNull(txtPenerimaNonSwiftOutNonNasabah_Negara.Text) = True Then Throw New Exception("Penerima Non Nasabah: Negara harus diisi salah satu saja ")
    End Sub
#End Region
    Sub validasiTransaksi()

        If ObjectAntiNull(Transaksi_NonSwiftOut_tanggal.Text) = False Then Throw New Exception("Tanggal Transaksi harus diisi  ")
        If Sahassa.aml.Commonly.IsDateValid("dd-MMM-yyyy", Transaksi_NonSwiftOut_tanggal.Text) = False Then
            Throw New Exception("Tanggal Transaksi tidak valid")
        End If
        If ObjectAntiNull(Transaksi_NonSwiftOut_ValueTanggalTransaksi.Text) = False Then Throw New Exception("Tanggal Transaksi(value date) harus diisi  ")
        If Sahassa.aml.Commonly.IsDateValid("dd-MMM-yyyy", Transaksi_NonSwiftOut_ValueTanggalTransaksi.Text) = False Then
            Throw New Exception("Tanggal Transaksi(value date) tidak valid")
        End If
        If ObjectAntiNull(Transaksi_NonSwiftOut_nilaitransaksi.Text) = False Then Throw New Exception("Sender's Reference harus diisi  ")
        If ObjectAntiNull(Transaksi_NonSwiftOut_MataUangTransaksi.Text) = False And ObjectAntiNull(Transaksi_NonSwiftOut_MataUangTransaksiLain.Text) = False Then Throw New Exception("Mata Uang Transaksi harus diisi! ")
        If ObjectAntiNull(Transaksi_NonSwiftOut_MataUangTransaksi.Text) = True And ObjectAntiNull(Transaksi_NonSwiftOut_MataUangTransaksiLain.Text) = True Then Throw New Exception("Mata Uang Transaksi harus diisi salah satu saja! ")
        If ObjectAntiNull(Transaksi_NonSwiftOut_AmountdalamRupiah.Text) = False Then Throw New Exception("Amount dalam rupiah harus diisi  ")
        If Not IsValidDecimal(15, 2, Transaksi_NonSwiftOut_AmountdalamRupiah.Text) Then Throw New Exception("Amount dalam rupiah harus diisi angka ")
        If ObjectAntiNull(Transaksi_NonSwiftOut_currency.Text) = True And ObjectAntiNull(Transaksi_NonSwiftOut_currencyLain.Text) = True Then Throw New Exception("Mata Uang yang diinstruksikan harus diisi salah satu saja! ")
        If ObjectAntiNull(Transaksi_NonSwiftOut_instructedAmount.Text) = False Then
            'Dim objresult As Decimal
            If Not IsValidDecimal(15, 2, Transaksi_NonSwiftOut_instructedAmount.Text) Then Throw New Exception("Amount dalam rupiah harus diisi angka ")
            'If Not IsNumeric(Transaksi_NonSwiftOut_instructedAmount.Text) Then Throw New Exception("Amount dalam rupiah harus diisi angka ")
        End If
        If Me.Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipePengirim.SelectedValue = 1 And CInt(Me.Transaksi_NonSwiftOut_AmountdalamRupiah.Text) > 100000000 And ObjectAntiNull(Me.Transaksi_NonSwiftOut_SumberPenggunaanDana.Text) = False Then Throw New Exception("Sumber Penggunaan Dana harus diisi! ")
    End Sub
    Sub ValidasiBOwnerNasabah()
        If ObjectAntiNull(BenfOwnerNonSwiftOutNasabah_tanggalLahir.Text) = True Then
            If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", BenfOwnerNonSwiftOutNasabah_tanggalLahir.Text) = False Then
                Throw New Exception("Beneficiary Owner: Tanggal Transaksi tidak valid")
            End If
        End If
        'If ObjectAntiNull(BenfOwnerNonSwiftOutNasabah_KotaIden.Text) = False And ObjectAntiNull(BenfOwnerNonSwiftOutNasabah_kotaLainIden.Text) = False Then Throw New Exception("Beneficiary Owner: Kota/Kabupaten harus diisi! ")
        'If ObjectAntiNull(BenfOwnerNonSwiftOutNasabah_ProvinsiIden.Text) = False And ObjectAntiNull(BenfOwnerNonSwiftOutNasabah_ProvinsiLainIden.Text) = False Then Throw New Exception("Beneficiary Owner: Provinsi harus diisi! ")

        If ObjectAntiNull(BenfOwnerNonSwiftOutNasabah_rekening.Text) = False Or BenfOwnerNonSwiftOutNasabah_rekening.Text.Length < 2 Then Throw New Exception("Identitas Beneficial Owner Nasabah: No Rekening harus diisi minimal 3 karakter!  ")
        If ObjectAntiNull(BenfOwnerNonSwiftOutNasabah_Nama.Text) = False Or BenfOwnerNonSwiftOutNasabah_Nama.Text.Length < 2 Then Throw New Exception("Identitas Beneficial Owner Nasabah: Nama Lengkap harus diisi minimal 3 karakter! ")
        If ObjectAntiNull(BenfOwnerNonSwiftOutNasabah_AlamatIden.Text) = False Then Throw New Exception("Identitas Beneficial Owner Nasabah: Alamat harus diisi  ")
        If ObjectAntiNull(BenfOwnerNonSwiftOutNasabah_KotaIden.Text) = False And ObjectAntiNull(BenfOwnerNonSwiftOutNasabah_kotaLainIden.Text) = False Then Throw New Exception("Identitas Beneficial Owner Nasabah: Kota/Kabupaten harus diisi! ")
        If ObjectAntiNull(BenfOwnerNonSwiftOutNasabah_ProvinsiIden.Text) = False And ObjectAntiNull(BenfOwnerNonSwiftOutNasabah_ProvinsiLainIden.Text) = False Then Throw New Exception("Identitas Beneficial Owner Nasabah: Provinsi harus diisi! ")
        If ObjectAntiNull(BenfOwnerNonSwiftOutNasabah_KotaIden.Text) = True And ObjectAntiNull(BenfOwnerNonSwiftOutNasabah_kotaLainIden.Text) = True Then Throw New Exception("Identitas Beneficial Owner Nasabah: Kota/Kabupaten harus diisi salah satu saja ")
        If ObjectAntiNull(BenfOwnerNonSwiftOutNasabah_ProvinsiIden.Text) = True And ObjectAntiNull(BenfOwnerNonSwiftOutNasabah_ProvinsiLainIden.Text) = True Then Throw New Exception("Identitas Beneficial Owner Nasabah: Provinsi harus diisi salah satu saja ")
        If CBOBenfOwnerNonSwiftOutNasabah_JenisDokumen.SelectedIndex = 0 Then Throw New Exception("Identitas Beneficial Owner Nasabah: Jenis Dokumen identitas harus diisi salah satu saja ")
        If ObjectAntiNull(BenfOwnerNonSwiftOutNasabah_NomorIden.Text) = True And BenfOwnerNonSwiftOutNasabah_NomorIden.Text.Length < 2 Then Throw New Exception("Identitas Beneficial Owner Nasabah: Nomor Identitas harus diisi! ")
        If BenfOwnerNonSwiftOutNasabah_NomorIden.Text <> "" Then
            If Not Regex.Match(BenfOwnerNonSwiftOutNasabah_NomorIden.Text.Trim(), "^[a-zA-Z0-9]*$").Success Then
                Throw New Exception("Nomor Identitas hanya boleh menggunakan huruf dan angka!")
            End If
        End If
    End Sub
    Sub ValidasiBOwnerNonNasabah()
        If ObjectAntiNull(BenfOwnerNonSwiftOutNonNasabah_TanggalLahir.Text) = True Then
            If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", BenfOwnerNonSwiftOutNonNasabah_TanggalLahir.Text) = False Then
                Throw New Exception("Beneficiary Owner: Tanggal Transaksi tidak valid")
            End If
        End If
        If ObjectAntiNull(BenfOwnerNonSwiftOutNonNasabah_KotaIden.Text) = False And ObjectAntiNull(BenfOwnerNonSwiftOutNonNasabah_KotaLainIden.Text) = False Then Throw New Exception("Beneficiary Owner: Kota/Kabupaten harus diisi! ")
        If ObjectAntiNull(BenfOwnerNonSwiftOutNonNasabah_ProvinsiLainIden.Text) = False And ObjectAntiNull(BenfOwnerNonSwiftOutNonNasabah_ProvinsiLainIden.Text) = False Then Throw New Exception("Beneficiary Owner: Provinsi harus diisi! ")

        If ObjectAntiNull(BenfOwnerNonSwiftOutNonNasabah_Nama.Text) = False Or BenfOwnerNonSwiftOutNonNasabah_Nama.Text.Length < 2 Then Throw New Exception("Identitas Beneficial Owner No Nasabah: Nama Lengkap harus diisi!")
        If ObjectAntiNull(BenfOwnerNonSwiftOutNonNasabah_AlamatIden.Text) = False Or BenfOwnerNonSwiftOutNonNasabah_AlamatIden.Text.Length < 2 Then Throw New Exception("Identitas Beneficial Owner No Nasabah: Alamat harus diisi!")
        If ObjectAntiNull(BenfOwnerNonSwiftOutNonNasabah_KotaIden.Text) = False And ObjectAntiNull(BenfOwnerNonSwiftOutNonNasabah_KotaLainIden.Text) = False Then Throw New Exception("Identitas Beneficial Owner Non Nasabah: Kota/Kabupaten harus diisi! ")
        If ObjectAntiNull(BenfOwnerNonSwiftOutNonNasabah_KotaIden.Text) = True And ObjectAntiNull(BenfOwnerNonSwiftOutNonNasabah_KotaLainIden.Text) = True Then Throw New Exception("Identitas Beneficial Owner Non Nasabah: Kota/Kabupaten harus diisi salah satu saja ")
        If ObjectAntiNull(BenfOwnerNonSwiftOutNonNasabah_ProvinsiIden.Text) = True And ObjectAntiNull(BenfOwnerNonSwiftOutNonNasabah_ProvinsiLainIden.Text) = True Then Throw New Exception("Identitas Beneficial Owner Non Nasabah: Provinsi harus diisi salah satu saja ")
        If ObjectAntiNull(BenfOwnerNonSwiftOutNonNasabah_ProvinsiIden.Text) = False And ObjectAntiNull(BenfOwnerNonSwiftOutNonNasabah_ProvinsiLainIden.Text) = False Then Throw New Exception("Identitas Beneficial Owner Non Nasabah: Provinsi harus diisi! ")
        If ObjectAntiNull(BenfOwnerNonSwiftOutNonNasabah_NomorIdentitas.Text) = True And BenfOwnerNonSwiftOutNonNasabah_NomorIdentitas.Text.Length < 2 Then Throw New Exception("Identitas Beneficial Owner Non Nasabah: Nomor Identitas harus diisi! ")
        If BenfOwnerNonSwiftOutNonNasabah_NomorIdentitas.Text <> "" Then
            If Not Regex.Match(BenfOwnerNonSwiftOutNonNasabah_NomorIdentitas.Text.Trim(), "^[a-zA-Z0-9]*$").Success Then
                Throw New Exception("Nomor Identitas hanya boleh menggunakan huruf dan angka!")
            End If
        End If
    End Sub
#End Region
#Region "Save"
    Sub SaveNonSwiftOut()
        Try
            Using OTrans As TransactionManager = New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)

                Try
                    OTrans.BeginTransaction()
                    Page.Validate("handle")
                    'If IsValidate() Then
                    ValidasiControl()
                    '============ Insert Detail Approval
                    Using objIfti_ApprovalDetail As New IFTI_Approval_Detail()
                        Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)

                            Dim senderType As Integer = objIfti.Sender_FK_IFTI_NasabahType_ID

                            'cekPengirim
                            Select Case (senderType)
                                Case 1
                                    'pengirimNasabahIndividu
                                    validasiSender1()
                                Case 2
                                    'pengirimNasabahKorporasi
                                    validasiSender2()
                                Case 3
                                    'PengirimNonNasabah
                                    validasiSender3()
                            End Select
                            'cekBOwner
                            If Rb_BOwnerNas_ApaMelibatkan.SelectedValue = 2 Then
                                Dim BOwnerID As Integer = objIfti.FK_IFTI_BeneficialOwnerType_ID

                                Select Case (BOwnerID)
                                    Case 1
                                        'Nasabah
                                        ValidasiBOwnerNasabah()
                                    Case 2
                                        'NonNasabah
                                        ValidasiBOwnerNonNasabah()
                                End Select
                            End If
                            'cekPenerima
                            Using objReceiver As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("FK_IFTI_ID=" & getIFTIPK, "", 0, Integer.MaxValue, 0)
                                Dim TipePenerima As Integer = objReceiver(0).FK_IFTI_NasabahType_ID
                                Select Case (TipePenerima)
                                    Case 1
                                        validasiReceiverInd()
                                    Case 2
                                        validasiReceiverKorp()
                                    Case 3
                                        validasiReceiverNonNasabah()
                                End Select
                            End Using
                            'cekTransaksi
                            validasiTransaksi()
                        End Using
                    End Using

                    'ValidasiControl()

                    ' =========== Insert Header Approval
                    Dim KeyHeaderApproval As Integer
                    Using objIftiApproval As New IFTI_Approval
                        With objIftiApproval
                            FillOrNothing(.FK_MsMode_Id, 2, False, oInt)
                            FillOrNothing(.RequestedBy, SessionPkUserId)
                            FillOrNothing(.RequestedDate, Date.Now)
                            'FillOrNothing(.IsUpload, False)
                        End With
                        DataRepository.IFTI_ApprovalProvider.Save(OTrans, objIftiApproval)
                        KeyHeaderApproval = objIftiApproval.PK_IFTI_Approval_Id
                    End Using

                    '============ Insert Detail Approval
                    Using objIfti_ApprovalDetail As New IFTI_Approval_Detail()
                        Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)

                            Dim senderType As Integer = objIfti.Sender_FK_IFTI_NasabahType_ID
                            With objIfti_ApprovalDetail
                                'FK
                                .PK_IFTI_ID_Old = objIfti.PK_IFTI_ID
                                .PK_IFTI_ID = objIfti.PK_IFTI_ID
                                .FK_IFTI_Approval_Id = KeyHeaderApproval
                                .FK_IFTI_Type_ID_Old = objIfti.FK_IFTI_Type_ID
                                .FK_IFTI_Type_ID = 3
                                'umum
                                '--old--
                                FillOrNothing(.LTDLNNo_Old, objIfti.LTDLNNo)
                                FillOrNothing(.LTDLNNoKoreksi_Old, objIfti.LTDLNNoKoreksi)
                                FillOrNothing(.TanggalLaporan_Old, objIfti.TanggalLaporan)
                                FillOrNothing(.NamaPJKBankPelapor_Old, objIfti.NamaPJKBankPelapor)
                                FillOrNothing(.NamaPejabatPJKBankPelapor_Old, objIfti.NamaPejabatPJKBankPelapor)
                                FillOrNothing(.JenisLaporan_Old, objIfti.JenisLaporan)
                                '--new--
                                FillOrNothing(.LTDLNNo, Me.TxtUmum_NonSwiftOutLTDLN.Text, False, Ovarchar)
                                FillOrNothing(.LTDLNNoKoreksi, Me.TxtUmum_NonSwiftOutLTDLNKoreksi.Text, False, Ovarchar)
                                FillOrNothing(.TanggalLaporan, Me.TxtUmum_NonSwiftOutTanggalLaporan.Text, False, oDate)
                                FillOrNothing(.NamaPJKBankPelapor, Me.TxtUmum_NonSwiftOutPJKBankPelapor.Text, False, Ovarchar)
                                FillOrNothing(.NamaPejabatPJKBankPelapor, Me.TxtUmum_NonSwiftOutNamaPejabatPJKBankPelapor.Text, False, Ovarchar)
                                FillOrNothing(.JenisLaporan, Me.Rb_Umum_NonSwiftOut_jenislaporan.SelectedValue, False, oInt)
                                'cekPengirim
                                Select Case (senderType)
                                    Case 1
                                        'pengirimNasabahIndividu
                                        'cekvalidasi
                                        'validasiSender1()
                                        FillOrNothing(.Sender_FK_IFTI_NasabahType_ID_Old, objIfti.Sender_FK_IFTI_NasabahType_ID)
                                        FillOrNothing(.Sender_FK_IFTI_NasabahType_ID, senderType, False, oInt)

                                        FillOrNothing(.Sender_Nasabah_INDV_NoRekening_Old, objIfti.Sender_Nasabah_INDV_NoRekening)
                                        FillOrNothing(.Sender_Nasabah_INDV_NoRekening, Me.TxtIdenNonSwiftOutPengirim_Norekening.Text, False, Ovarchar)

                                        FillOrNothing(.Sender_Nasabah_INDV_NamaLengkap_Old, objIfti.Sender_Nasabah_INDV_NamaLengkap)
                                        FillOrNothing(.Sender_Nasabah_INDV_NamaLengkap, Me.TxtIdenNonSwiftOutPengirimNas_Ind_NamaLengkap.Text, False, Ovarchar)
                                        FillOrNothing(.Sender_Nasabah_INDV_TanggalLahir_Old, objIfti.Sender_Nasabah_INDV_TanggalLahir)
                                        FillOrNothing(.Sender_Nasabah_INDV_TanggalLahir, Me.TxtIdenNonSwiftOutPengirimNas_Ind_TanggalLahir.Text, False, oDate)
                                        FillOrNothing(.Sender_Nasabah_INDV_KewargaNegaraan_Old, objIfti.Sender_Nasabah_INDV_KewargaNegaraan)
                                        FillOrNothing(.Sender_Nasabah_INDV_KewargaNegaraan, Me.RbIdenNonSwiftOutPengirimNas_Ind_Kewarganegaraan.SelectedValue, False, oInt)
                                        FillOrNothing(.Sender_Nasabah_INDV_Negara_Old, objIfti.Sender_Nasabah_INDV_Negara)
                                        FillOrNothing(.Sender_Nasabah_INDV_NegaraLainnya_Old, objIfti.Sender_Nasabah_INDV_NegaraLainnya)
                                        If Me.RbIdenNonSwiftOutPengirimNas_Ind_Kewarganegaraan.SelectedValue = "2" Then
                                            If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_Negara.Text) = True Then
                                                FillOrNothing(.Sender_Nasabah_INDV_Negara, Me.hfIdenNonSwiftOutPengirimNas_Ind_Negara.Value, False, oInt)
                                            Else
                                                FillOrNothing(.Sender_Nasabah_INDV_Negara, Nothing)
                                            End If
                                            FillOrNothing(.Sender_Nasabah_INDV_NegaraLainnya, Me.TxtIdenNonSwiftOutPengirimNas_Ind_NegaraLain.Text, False, Ovarchar)
                                        End If
                                        FillOrNothing(.Sender_Nasabah_INDV_Pekerjaan_Old, objIfti.Sender_Nasabah_INDV_Pekerjaan)
                                        If ObjectAntiNull(TxtIdenNonSwiftOutPengirimNas_Ind_Pekerjaan.Text) = True Then
                                            FillOrNothing(.Sender_Nasabah_INDV_Pekerjaan, Me.hfIdenNonSwiftOutPengirimNas_Ind_pekerjaan.Value, False, oInt)
                                        Else
                                            FillOrNothing(.Sender_Nasabah_INDV_Pekerjaan, Nothing)
                                        End If
                                        FillOrNothing(.Sender_Nasabah_INDV_PekerjaanLainnya_Old, objIfti.Sender_Nasabah_INDV_PekerjaanLainnya)
                                        FillOrNothing(.Sender_Nasabah_INDV_PekerjaanLainnya, Me.TxtIdenNonSwiftOutPengirimNas_Ind_Pekerjaanlain.Text, Ovarchar)
                                        FillOrNothing(.Sender_Nasabah_INDV_DOM_Alamat_Old, objIfti.Sender_Nasabah_INDV_ID_Alamat)
                                        FillOrNothing(.Sender_Nasabah_INDV_DOM_Alamat, Me.TxtIdenNonSwiftOutPengirimNas_Ind_Alamat.Text, False, Ovarchar)
                                        FillOrNothing(.Sender_Nasabah_INDV_DOM_KotaKab_Old, objIfti.Sender_Nasabah_INDV_ID_KotaKab)
                                        If ObjectAntiNull(Me.TxtIdenNonSwiftOutPengirimNas_Ind_Kota.Text) = True Then
                                            FillOrNothing(.Sender_Nasabah_INDV_DOM_KotaKab, Me.hfIdenNonSwiftOutPengirimNas_Ind_Kota_dom.Value, False, oInt)
                                        Else
                                            FillOrNothing(.Sender_Nasabah_INDV_DOM_KotaKab, Nothing)
                                        End If
                                        FillOrNothing(.Sender_Nasabah_INDV_DOM_KotaKabLainnya_Old, objIfti.Sender_Nasabah_INDV_ID_KotaKabLainnya)
                                        FillOrNothing(.Sender_Nasabah_INDV_DOM_KotaKabLainnya, Me.TxtIdenNonSwiftOutPengirimNas_Ind_KotaLainIden.Text, False, Ovarchar)
                                        FillOrNothing(.Sender_Nasabah_INDV_DOM_Propinsi_Old, objIfti.Sender_Nasabah_INDV_ID_Propinsi)
                                        If ObjectAntiNull(Me.TxtIdenNonSwiftOutPengirimNas_Ind_provinsi.Text) = True Then
                                            FillOrNothing(.Sender_Nasabah_INDV_DOM_Propinsi, Me.hfIdenNonSwiftOutPengirimNas_Ind_provinsi_dom.Value, False, oInt)
                                        Else
                                            FillOrNothing(.Sender_Nasabah_INDV_DOM_Propinsi, Nothing)
                                        End If
                                        FillOrNothing(.Sender_Nasabah_INDV_DOM_PropinsiLainnya_Old, objIfti.Sender_Nasabah_INDV_ID_PropinsiLainnya)
                                        FillOrNothing(.Sender_Nasabah_INDV_DOM_PropinsiLainnya, Me.TxtIdenNonSwiftOutPengirimNas_Ind_provinsiLain.Text, False, Ovarchar)
                                        FillOrNothing(.Sender_Nasabah_INDV_ID_Alamat_Old, objIfti.Sender_Nasabah_INDV_ID_Alamat)
                                        FillOrNothing(.Sender_Nasabah_INDV_ID_Alamat, Me.TxtIdenNonSwiftOutPengirimNas_Ind_alamatIdentitas.Text, False, Ovarchar)
                                        If ObjectAntiNull(Me.TxtIdenNonSwiftOutPengirimNas_Ind_kotaIdentitas.Text) = True Then
                                            FillOrNothing(.Sender_Nasabah_INDV_ID_KotaKab, Me.hfIdenNonSwiftOutPengirimNas_Ind_kotaIdentitas.Value, False, oInt)
                                        Else
                                            FillOrNothing(.Sender_Nasabah_INDV_ID_KotaKab, Nothing)
                                        End If
                                        FillOrNothing(.Sender_Nasabah_INDV_ID_KotaKab_Old, objIfti.Sender_Nasabah_INDV_ID_KotaKab)

                                        FillOrNothing(.Sender_Nasabah_INDV_ID_KotaKabLainnya_Old, objIfti.Sender_Nasabah_INDV_ID_KotaKabLainnya)
                                        FillOrNothing(.Sender_Nasabah_INDV_ID_KotaKabLainnya, Me.TxtIdenNonSwiftOutPengirimNas_Ind_kotalain.Text, False, Ovarchar)
                                        FillOrNothing(.Sender_Nasabah_INDV_ID_Propinsi_Old, objIfti.Sender_Nasabah_INDV_ID_Propinsi)
                                        If ObjectAntiNull(Me.TxtIdenNonSwiftOutPengirimNas_Ind_ProvinsiIden.Text) = True Then
                                            FillOrNothing(.Sender_Nasabah_INDV_ID_Propinsi, Me.hfIdenNonSwiftOutPengirimNas_Ind_ProvinsiIden.Value, False, oInt)
                                        Else
                                            FillOrNothing(.Sender_Nasabah_INDV_ID_Propinsi, Nothing)
                                        End If
                                        FillOrNothing(.Sender_Nasabah_INDV_ID_PropinsiLainnya_Old, objIfti.Sender_Nasabah_INDV_ID_PropinsiLainnya)
                                        FillOrNothing(.Sender_Nasabah_INDV_ID_PropinsiLainnya, Me.TxtIdenNonSwiftOutPengirimNas_Ind_ProvinsilainIden.Text, False, Ovarchar)
                                        FillOrNothing(.Sender_Nasabah_INDV_FK_IFTI_IDType_Old, objIfti.Sender_Nasabah_INDV_FK_IFTI_IDType)
                                        FillOrNothing(.Sender_Nasabah_INDV_FK_IFTI_IDType, Me.cboNonSwiftOutPengirimNasInd_jenisidentitas.SelectedValue, False, oInt)
                                        FillOrNothing(.Sender_Nasabah_INDV_NomorID_Old, objIfti.Sender_Nasabah_INDV_NomorID)
                                        FillOrNothing(.Sender_Nasabah_INDV_NomorID, Me.TxtIdenNonSwiftOutPengirimNas_Ind_noIdentitas.Text, False, Ovarchar)

                                    Case 2
                                        'pengirimNasabahKorporasi
                                        'cekvalidasi
                                        'validasiSender2()
                                        FillOrNothing(.Sender_FK_IFTI_NasabahType_ID_Old, objIfti.Sender_FK_IFTI_NasabahType_ID)
                                        FillOrNothing(.Sender_FK_IFTI_NasabahType_ID, senderType, False, Ovarchar)

                                        FillOrNothing(.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id_Old, objIfti.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id)
                                        FillOrNothing(.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id, Me.cboIdenNonSwiftOutPengirimNas_Corp_BentukBadanUsaha.Text, False, Ovarchar)
                                        FillOrNothing(.Sender_Nasabah_CORP_BentukBadanUsahaLainnya_Old, objIfti.Sender_Nasabah_CORP_BentukBadanUsahaLainnya)
                                        FillOrNothing(.Sender_Nasabah_CORP_BentukBadanUsahaLainnya, Me.TxtIdenNonSwiftOutPengirimNas_Corp_BentukBadanUsahaLain.Text, False, Ovarchar)
                                        FillOrNothing(.Sender_Nasabah_CORP_NamaKorporasi_Old, objIfti.Sender_Nasabah_CORP_NamaKorporasi)
                                        FillOrNothing(.Sender_Nasabah_CORP_NamaKorporasi, Me.TxtIdenNonSwiftOutPengirimNas_Corp_NamaKorp.Text, False, Ovarchar)
                                        FillOrNothing(.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id_Old, objIfti.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id)
                                        FillOrNothing(.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id, Me.hfIdenNonSwiftOutPengirimNas_Corp_BidangUsahaKorp.Value, False, oInt)
                                        FillOrNothing(.Sender_Nasabah_CORP_AlamatLengkap_Old, objIfti.Sender_Nasabah_CORP_AlamatLengkap)
                                        FillOrNothing(.Sender_Nasabah_CORP_AlamatLengkap, Me.TxtIdenNonSwiftOutPengirimNas_Corp_alamatkorp.Text, False, Ovarchar)
                                        FillOrNothing(.Sender_Nasabah_COR_KotaKab_Old, objIfti.Sender_Nasabah_CORP_KotaKab)
                                        FillOrNothing(.Sender_Nasabah_COR_KotaKab, Me.hfIdenNonSwiftOutPengirimNas_Corp_kotakorp.Value, False, oInt)
                                        FillOrNothing(.Sender_Nasabah_COR_KotaKabLainnya_Old, objIfti.Sender_Nasabah_CORP_KotaKabLainnya)
                                        FillOrNothing(.Sender_Nasabah_COR_KotaKabLainnya, Me.TxtIdenNonSwiftOutPengirimNas_Ind_KotaLainIden.Text, False, Ovarchar)
                                        FillOrNothing(.Sender_Nasabah_CORP_Propinsi_Old, objIfti.Sender_Nasabah_CORP_Propinsi)
                                        If ObjectAntiNull(Me.TxtIdenNonSwiftOutPengirimNas_Corp_provKorp.Text) = True Then
                                            FillOrNothing(.Sender_Nasabah_CORP_Propinsi, Me.hfIdenNonSwiftOutPengirimNas_Corp_provKorp.Value, False, oInt)
                                        Else
                                            FillOrNothing(.Sender_Nasabah_CORP_Propinsi, Nothing)
                                        End If
                                        FillOrNothing(.Sender_Nasabah_CORP_PropinsiLainnya_Old, objIfti.Sender_Nasabah_CORP_PropinsiLainnya)
                                        FillOrNothing(.Sender_Nasabah_CORP_PropinsiLainnya, Me.TxtIdenNonSwiftOutPengirimNas_Corp_provKorpLain.Text, False, Ovarchar)
                                        FillOrNothing(.Sender_Nasabah_CORP_LN_AlamatLengkap_Old, objIfti.Sender_Nasabah_CORP_LN_AlamatLengkap)
                                        FillOrNothing(.Sender_Nasabah_CORP_LN_AlamatLengkap, Me.TxtIdenNonSwiftOutPengirimNas_Corp_AlamatLuar.Text, False, Ovarchar)
                                        FillOrNothing(.Sender_Nasabah_CORP_Alamat_KantorCabangPengirim_Old, objIfti.Sender_Nasabah_CORP_Alamat_KantorCabangPengirim)
                                        FillOrNothing(.Sender_Nasabah_CORP_Alamat_KantorCabangPengirim, Me.TxtIdenNonSwiftOutPengirimNas_Corp_AlamatAsal.Text, False, Ovarchar)

                                    Case 3
                                        'PengirimNonNasabah
                                        'cekvalidasi
                                        'validasiSender3()
                                        FillOrNothing(.Sender_FK_IFTI_NasabahType_ID_Old, objIfti.Sender_FK_IFTI_NasabahType_ID)
                                        FillOrNothing(.Sender_FK_IFTI_NasabahType_ID, senderType, False, Ovarchar)

                                        FillOrNothing(.FK_IFTI_NonNasabahNominalType_ID_Old, objIfti.FK_IFTI_NonNasabahNominalType_ID)
                                        FillOrNothing(.FK_IFTI_NonNasabahNominalType_ID, Me.RbNonSwiftOutPengirimNonNasabah_100Juta.SelectedValue, False, oInt)
                                        FillOrNothing(.Sender_NonNasabah_NamaLengkap_Old, objIfti.Sender_NonNasabah_NamaLengkap)
                                        FillOrNothing(.Sender_NonNasabah_NamaLengkap, Me.TxtNonSwiftOutPengirimNonNasabah_nama.Text, False, Ovarchar)
                                        FillOrNothing(.Sender_NonNasabah_TanggalLahir_Old, objIfti.Sender_NonNasabah_TanggalLahir)
                                        FillOrNothing(.Sender_NonNasabah_TanggalLahir, Me.TxtNonSwiftOutPengirimNonNasabah_TanggalLahir.Text, False, oDate)
                                        FillOrNothing(.Sender_NonNasabah_ID_Alamat_Old, objIfti.Sender_NonNasabah_ID_Alamat)
                                        FillOrNothing(.Sender_NonNasabah_ID_Alamat, Me.TxtNonSwiftOutPengirimNonNasabah_alamatiden.Text, False, Ovarchar)
                                        FillOrNothing(.Sender_NonNasabah_ID_KotaKab_Old, objIfti.Sender_NonNasabah_ID_KotaKab)
                                        If ObjectAntiNull(Me.TxtNonSwiftOutPengirimNonNasabah_kotaIden.Text) = True Then
                                            FillOrNothing(.Sender_NonNasabah_ID_KotaKab, Me.hfNonSwiftOutPengirimNonNasabah_kotaIden.Value, False, oInt)
                                        Else
                                            FillOrNothing(.Sender_NonNasabah_ID_KotaKab, Nothing)
                                        End If
                                        FillOrNothing(.Sender_NonNasabah_ID_KotaKabLainnya_Old, objIfti.Sender_NonNasabah_ID_KotaKabLainnya)
                                        FillOrNothing(.Sender_NonNasabah_ID_KotaKabLainnya, Me.TxtNonSwiftOutPengirimNonNasabah_kotaIden.Text, False, Ovarchar)
                                        FillOrNothing(.Sender_NonNasabah_ID_Propinsi_Old, objIfti.Sender_NonNasabah_ID_Propinsi)
                                        If ObjectAntiNull(Me.TxtNonSwiftOutPengirimNonNasabah_ProvIden.Text) = True Then
                                            FillOrNothing(.Sender_NonNasabah_ID_Propinsi, Me.hfNonSwiftOutPengirimNonNasabah_ProvIden.Value, False, oInt)
                                        Else
                                            FillOrNothing(.Sender_NonNasabah_ID_Propinsi, Nothing)
                                        End If
                                        FillOrNothing(.Sender_NonNasabah_ID_PropinsiLainnya_Old, objIfti.Sender_NonNasabah_ID_PropinsiLainnya)
                                        FillOrNothing(.Sender_NonNasabah_ID_PropinsiLainnya, Me.hfNonSwiftOutPengirimNonNasabah_ProvIden, False, oInt)
                                        FillOrNothing(.Sender_NonNasabah_FK_IFTI_IDType_Old, objIfti.Sender_Nasabah_INDV_FK_IFTI_IDType)
                                        FillOrNothing(.Sender_NonNasabah_FK_IFTI_IDType, Me.CboNonSwiftOutPengirimNonNasabah_JenisDokumen.SelectedValue, False, oInt)
                                        FillOrNothing(.Sender_NonNasabah_NomorID_Old, objIfti.Sender_NonNasabah_NomorID)
                                        FillOrNothing(.Sender_NonNasabah_NomorID, Me.TxtNonSwiftOutPengirimNonNasabah_NomorIden.Text, False, Ovarchar)

                                End Select
                                'cekBOwner
                                If Not IsNothing(objIfti.FK_IFTI_BeneficialOwnerType_ID) Then
                                    FillOrNothing(.BeneficialOwner_Keterlibatan_Old, objIfti.FK_IFTI_KeterlibatanBeneficialOwner_ID)
                                    FillOrNothing(.BeneficialOwner_Keterlibatan, Me.Rb_BOwnerNas_ApaMelibatkan.SelectedValue, False, oInt)
                                    FillOrNothing(.BeneficialOwner_HubunganDenganPemilikDana_Old, objIfti.BeneficialOwner_HubunganDenganPemilikDana)
                                    FillOrNothing(.BeneficialOwner_HubunganDenganPemilikDana, Me.BenfOwnerNonSwiftOutHubunganPemilikDana.Text, False, Ovarchar)
                                    Dim BOwnerID As Integer = objIfti.FK_IFTI_BeneficialOwnerType_ID
                                    Select Case (BOwnerID)
                                        Case 1
                                            'Nasabah
                                            FillOrNothing(.BeneficialOwner_NoRekening_Old, objIfti.BeneficialOwner_NoRekening)
                                            FillOrNothing(.BeneficialOwner_NoRekening, Me.BenfOwnerNonSwiftOutNasabah_rekening.Text, False, Ovarchar)
                                            FillOrNothing(.BeneficialOwner_NamaLengkap_Old, objIfti.BeneficialOwner_NamaLengkap)
                                            FillOrNothing(.BeneficialOwner_NamaLengkap, Me.BenfOwnerNonSwiftOutNasabah_Nama.Text, False, Ovarchar)
                                            FillOrNothing(.BeneficialOwner_TanggalLahir_Old, objIfti.BeneficialOwner_TanggalLahir)
                                            FillOrNothing(.BeneficialOwner_TanggalLahir, Me.BenfOwnerNonSwiftOutNasabah_tanggalLahir, False, oDate)
                                            FillOrNothing(.BeneficialOwner_ID_Alamat_Old, objIfti.BeneficialOwner_ID_Alamat)
                                            FillOrNothing(.BeneficialOwner_ID_Alamat, Me.BenfOwnerNonSwiftOutNasabah_AlamatIden.Text, False, Ovarchar)
                                            FillOrNothing(.BeneficialOwner_ID_KotaKab, Me.hfBenfOwnerNonSwiftOutNasabah_KotaIden.Value, False, oInt)
                                            FillOrNothing(.BeneficialOwner_ID_KotaKab_Old, objIfti.BeneficialOwner_ID_KotaKab)
                                            FillOrNothing(.BeneficialOwner_ID_KotaKabLainnya_Old, objIfti.BeneficialOwner_ID_KotaKabLainnya)
                                            FillOrNothing(.BeneficialOwner_ID_KotaKabLainnya, Me.BenfOwnerNonSwiftOutNasabah_kotaLainIden.Text, False, Ovarchar)
                                            FillOrNothing(.BeneficialOwner_ID_Propinsi_Old, objIfti.BeneficialOwner_ID_Propinsi)
                                            FillOrNothing(.BeneficialOwner_ID_Propinsi, Me.BenfOwnerNonSwiftOutNasabah_ProvinsiIden.Text, False, oInt)
                                            FillOrNothing(.BeneficialOwner_ID_PropinsiLainnya_Old, objIfti.BeneficialOwner_ID_PropinsiLainnya)
                                            FillOrNothing(.BeneficialOwner_ID_PropinsiLainnya, Me.BenfOwnerNonSwiftOutNasabah_ProvinsiLainIden.Text, False, Ovarchar)
                                            FillOrNothing(.BeneficialOwner_FK_IFTI_IDType_Old, objIfti.BeneficialOwner_FK_IFTI_IDType)
                                            FillOrNothing(.BeneficialOwner_FK_IFTI_IDType, Me.CBOBenfOwnerNonSwiftOutNasabah_JenisDokumen.SelectedValue, False, oInt)
                                            FillOrNothing(.BeneficialOwner_NomorID_Old, objIfti.BeneficialOwner_NomorID)
                                            FillOrNothing(.BeneficialOwner_NomorID, Me.BenfOwnerNonSwiftOutNasabah_NomorIden.Text, False, Ovarchar)

                                        Case 2
                                            'NonNasabah
                                            FillOrNothing(.BeneficialOwner_NamaLengkap_Old, objIfti.BeneficialOwner_NamaLengkap)
                                            FillOrNothing(.BeneficialOwner_NamaLengkap, Me.BenfOwnerNonSwiftOutNonNasabah_Nama.Text, False, Ovarchar)
                                            FillOrNothing(.BeneficialOwner_TanggalLahir_Old, objIfti.BeneficialOwner_TanggalLahir)
                                            FillOrNothing(.BeneficialOwner_TanggalLahir, Me.BenfOwnerNonSwiftOutNonNasabah_TanggalLahir, False, oDate)
                                            FillOrNothing(.BeneficialOwner_ID_Alamat_Old, objIfti.BeneficialOwner_ID_Alamat)
                                            FillOrNothing(.BeneficialOwner_ID_Alamat, Me.BenfOwnerNonSwiftOutNonNasabah_AlamatIden.Text, False, Ovarchar)
                                            If ObjectAntiNull(BenfOwnerNonSwiftOutNonNasabah_KotaIden.Text) = True Then
                                                FillOrNothing(.BeneficialOwner_ID_KotaKab, Me.hfBenfOwnerNonSwiftOutNonNasabah_KotaIden.Value, False, oInt)
                                            Else
                                                FillOrNothing(.BeneficialOwner_ID_KotaKab, Nothing)
                                            End If
                                            FillOrNothing(.BeneficialOwner_ID_KotaKab_Old, objIfti.BeneficialOwner_ID_KotaKab)
                                            FillOrNothing(.BeneficialOwner_ID_KotaKabLainnya_Old, objIfti.BeneficialOwner_ID_KotaKabLainnya)
                                            FillOrNothing(.BeneficialOwner_ID_KotaKabLainnya, Me.BenfOwnerNonSwiftOutNonNasabah_KotaLainIden.Text, False, Ovarchar)
                                            FillOrNothing(.BeneficialOwner_ID_Propinsi_Old, objIfti.BeneficialOwner_ID_Propinsi)
                                            If ObjectAntiNull(BenfOwnerNonSwiftOutNonNasabah_ProvinsiIden.Text) = True Then
                                                FillOrNothing(.BeneficialOwner_ID_Propinsi, Me.hfBenfOwnerNonSwiftOutNonNasabah_ProvinsiIden.Value, False, oInt)
                                            Else
                                                FillOrNothing(.BeneficialOwner_ID_Propinsi, Nothing)
                                            End If
                                            FillOrNothing(.BeneficialOwner_ID_PropinsiLainnya_Old, objIfti.BeneficialOwner_ID_PropinsiLainnya)
                                            FillOrNothing(.BeneficialOwner_ID_PropinsiLainnya, Me.BenfOwnerNonSwiftOutNonNasabah_ProvinsiLainIden.Text, False, Ovarchar)
                                            FillOrNothing(.BeneficialOwner_FK_IFTI_IDType_Old, objIfti.BeneficialOwner_FK_IFTI_IDType)
                                            FillOrNothing(.BeneficialOwner_FK_IFTI_IDType, Me.CBOBenfOwnerNonSwiftOutNonNasabah_JenisDokumen.SelectedValue, False, oInt)
                                            FillOrNothing(.BeneficialOwner_NomorID_Old, objIfti.BeneficialOwner_NomorID)
                                            FillOrNothing(.BeneficialOwner_NomorID, Me.BenfOwnerNonSwiftOutNonNasabah_NomorIdentitas.Text, False, Ovarchar)
                                    End Select

                                End If

                                'cekPenerima
                                Using objReceiver As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("FK_IFTI_ID=" & getIFTIPK, "", 0, Integer.MaxValue, 0)
                                    Dim KeyBeneficiary As Integer = objReceiver(0).PK_IFTI_Beneficiary_ID
                                    Dim TipePenerima As Integer = objReceiver(0).FK_IFTI_NasabahType_ID
                                    Using objReceiverAppDetail As New IFTI_Approval_Beneficiary ' = 'DataRepository.IFTI_Approval_BeneficiaryProvider.GetPaged("FK_IFTI_ID=" & getIFTIPK, "", 0, Integer.MaxValue, 0)
                                        With objReceiverAppDetail

                                            Select Case (TipePenerima)
                                                Case 1
                                                    'validasiReceiverInd()
                                                    .FK_IFTI_NasabahType_ID = 1
                                                    .FK_IFTI_ID = objIfti.PK_IFTI_ID
                                                    .FK_IFTI_Approval_Id = KeyHeaderApproval
                                                    .FK_IFTI_Beneficiary_ID = KeyBeneficiary

                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_NoRekening, Me.txtPenerimaNonSwiftOut_rekening.Text)

                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_NamaLengkap, Me.txtPenerimaNonSwiftOutNasabah_IND_nama.Text, False, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_TanggalLahir, Me.txtPenerimaNonSwiftOutNasabah_IND_TanggalLahir.Text, False, oDate)
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_KewargaNegaraan, Me.RbNonSwiftOutPenerimaNasabah_IND_Warganegara.SelectedValue, False, oInt)
                                                    If RbNonSwiftOutPenerimaNasabah_IND_Warganegara.SelectedValue = "2" Then
                                                        If ObjectAntiNull(Me.TxtIdenNonSwiftOutPengirimNas_Ind_Negara.Text) = True Then
                                                            FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Negara, Me.hfNonSwiftOutPengirimNasabah_IND_negara.Value, False, oInt)
                                                        Else
                                                            FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Negara, Nothing)
                                                        End If
                                                        FillOrNothing(.Beneficiary_Nasabah_INDV_NegaraLainnya, Me.txtPenerimaNonSwiftOutNasabah_IND_negaraLain.Text, False, Ovarchar)
                                                    End If
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Alamat_Iden, Me.txtPenerimaNonSwiftOutNasabah_IND_alamatIden.Text, False, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_NegaraBagian, Me.txtPenerimaNonSwiftOutNasabah_IND_negaraBagian.Text, False, Ovarchar)
                                                    If ObjectAntiNull(Me.txtPenerimaNonSwiftOutNasabah_IND_negaraIden.Text) = True Then
                                                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Negara, Me.hfPenerimaNonSwiftOutNasabah_Korp_NegaraKorp.Value, False, oInt)
                                                    Else
                                                        FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Negara, Nothing)
                                                    End If
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_NegaraLainnya, Me.txtPenerimaNonSwiftOutNasabah_IND_negaraLainIden.Text, False, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_FK_IFTI_IDType, Me.CBONonSwiftOutPenerimaNasabah_IND_JenisIden.SelectedValue, False, oInt)
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_NomorID, Me.txtPenerimaNonSwiftOutNasabah_IND_NomorIden.Text, False, Ovarchar)

                                                Case 2
                                                    'validasiReceiverKorp()
                                                    .FK_IFTI_NasabahType_ID = 2
                                                    .FK_IFTI_ID = objIfti.PK_IFTI_ID
                                                    .FK_IFTI_Approval_Id = KeyHeaderApproval
                                                    .FK_IFTI_Beneficiary_ID = KeyBeneficiary
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_NoRekening, Me.txtPenerimaNonSwiftOut_rekening.Text)

                                                    If cboPenerimaNonSwiftOutNasabah_Korp_BentukBadanUsahaLain.SelectedIndex > 0 Then
                                                        FillOrNothing(.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id, cboPenerimaNonSwiftOutNasabah_Korp_BentukBadanUsahaLain.SelectedValue, False, oInt)
                                                    Else
                                                        FillOrNothing(.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id, Nothing)
                                                    End If
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya, Me.txtPenerimaNonSwiftOutNasabah_Korp_BentukBadanUsahaLainnya.Text, False, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_NamaKorporasi, Me.txtPenerimaNonSwiftOutNasabah_Korp_namaKorp.Text, False, Ovarchar)
                                                    If ObjectAntiNull(Me.TxtIdenNonSwiftOutPengirimNas_Corp_BidangUsahaKorp.Text) = True Then
                                                        FillOrNothing(.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id, Me.hfNonSwiftOutPengirimNasabah_Korp_BidangUsahaKorp.Value, False, oInt)
                                                    Else
                                                        FillOrNothing(.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id, Nothing)
                                                    End If
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_BidangUsahaLainnya, Me.txtPenerimaNonSwiftOutNasabah_Korp_BentukBadanUsahaLainnya.Text, False, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_AlamatLengkap, Me.txtPenerimaNonSwiftOutNasabah_Korp_AlamatKorp.Text, TipePenerima, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_ID_NegaraBagian, Me.txtPenerimaNonSwiftOutNasabah_Korp_Kota.Text, False, Ovarchar)
                                                    If ObjectAntiNull(Me.txtPenerimaNonSwiftOutNasabah_Korp_NegaraKorp.Text) = True Then
                                                        FillOrNothing(.Beneficiary_Nasabah_CORP_ID_Negara, Me.hfPenerimaNonSwiftOutNasabah_Korp_NegaraKorp.Value, False, oInt)
                                                    Else
                                                        FillOrNothing(.Beneficiary_Nasabah_CORP_ID_Negara, Nothing)
                                                    End If
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_ID_NegaraLainnya, Me.txtPenerimaNonSwiftOutNasabah_Korp_NegaraLainnyaKorp.Text, False, Ovarchar)

                                                Case 3
                                                    'validasiReceiverNonNasabah()
                                                    .FK_IFTI_NasabahType_ID = 3
                                                    .FK_IFTI_ID = objIfti.PK_IFTI_ID
                                                    .FK_IFTI_Approval_Id = KeyHeaderApproval
                                                    .FK_IFTI_Beneficiary_ID = KeyBeneficiary
                                                    FillOrNothing(.Beneficiary_NonNasabah_KodeRahasia, Me.txtPenerimaNonSwiftOutNonNasabah_KodeRahasia.Text, False, Ovarchar)
                                                    FillOrNothing(.Beneficiary_NonNasabah_NoRekening, Me.txtPenerimaNonSwiftOutNonNasabah_rekening.Text)

                                                    FillOrNothing(.Beneficiary_NonNasabah_NamaBank, Me.txtPenerimaNonSwiftOutNonNasabah_namabank.Text, False, Ovarchar)
                                                    FillOrNothing(.Beneficiary_NonNasabah_NamaLengkap, Me.txtPenerimaNonSwiftOutNonNasabah_Nama.Text, False, Ovarchar)
                                                    FillOrNothing(.Beneficiary_NonNasabah_ID_Alamat, Me.txtPenerimaNonSwiftOutNonNasabah_Alamat.Text, False, Ovarchar)
                                                    If ObjectAntiNull(Me.txtPenerimaNonSwiftOutNonNasabah_Negara.Text) = True Then
                                                        FillOrNothing(.Beneficiary_NonNasabah_ID_Negara, Me.hfPenerimaNonSwiftOutNasabah_Korp_NegaraKorp.Value, False, oInt)
                                                    Else
                                                        FillOrNothing(.Beneficiary_NonNasabah_ID_Negara, Nothing)
                                                    End If
                                                    FillOrNothing(.Beneficiary_NonNasabah_ID_NegaraLainnya, Me.txtPenerimaNonSwiftOutNonNasabah_negaraLain.Text, False, Ovarchar)
                                            End Select
                                        End With
                                        'save beneficiary
                                        DataRepository.IFTI_Approval_BeneficiaryProvider.Save(OTrans, objReceiverAppDetail)
                                    End Using
                                End Using

                                'cekTransaksi
                                'validasiTransaksi()
                                '----old----
                                FillOrNothing(.TanggalTransaksi_Old, objIfti.TanggalTransaksi)
                                FillOrNothing(.TimeIndication_Old, objIfti.TimeIndication)
                                FillOrNothing(.SenderReference_Old, objIfti.SenderReference)
                                FillOrNothing(.BankOperationCode_Old, objIfti.BankOperationCode)
                                FillOrNothing(.InstructionCode_Old, objIfti.InstructionCode)
                                FillOrNothing(.KantorCabangPenyelengaraPengirimAsal_Old, objIfti.KantorCabangPenyelengaraPengirimAsal)
                                FillOrNothing(.TransactionCode, objIfti.TransactionCode)
                                FillOrNothing(.ValueDate_TanggalTransaksi_Old, objIfti.ValueDate_TanggalTransaksi)
                                FillOrNothing(.ValueDate_NilaiTransaksi_Old, objIfti.ValueDate_NilaiTransaksi)
                                FillOrNothing(.ValueDate_FK_Currency_ID_Old, objIfti.ValueDate_FK_Currency_ID)
                                FillOrNothing(.ValueDate_CurrencyLainnya_Old, objIfti.ValueDate_CurrencyLainnya)
                                FillOrNothing(.ValueDate_NilaiTransaksiIDR_Old, objIfti.ValueDate_NilaiTransaksiIDR)
                                FillOrNothing(.Instructed_Currency_Old, objIfti.Instructed_Currency)
                                FillOrNothing(.Instructed_CurrencyLainnya_Old, objIfti.Instructed_CurrencyLainnya)
                                FillOrNothing(.Instructed_Amount_Old, objIfti.Instructed_Amount)
                                FillOrNothing(.ExchangeRate_Old, objIfti.ExchangeRate)
                                FillOrNothing(.SendingInstitution_Old, objIfti.SendingInstitution)
                                FillOrNothing(.TujuanTransaksi_Old, objIfti.TujuanTransaksi)
                                FillOrNothing(.SumberPenggunaanDana_Old, objIfti.SumberPenggunaanDana)

                                '-----new---
                                FillOrNothing(.TanggalTransaksi, Me.Transaksi_NonSwiftOut_tanggal.Text, False, oDate)
                                FillOrNothing(.KantorCabangPenyelengaraPengirimAsal, Me.Transaksi_NonSwiftOut_kantorCabangPengirim.Text, False, Ovarchar)
                                FillOrNothing(.ValueDate_TanggalTransaksi, Me.Transaksi_NonSwiftOut_ValueTanggalTransaksi.Text, False, oDate)
                                FillOrNothing(.ValueDate_NilaiTransaksi, Me.Transaksi_NonSwiftOut_nilaitransaksi.Text, False, oDecimal)
                                Using objCurrency As TList(Of MsCurrency) = DataRepository.MsCurrencyProvider.GetPaged(MsCurrencyColumn.Code.ToString & _
                                                                         " like '%" & Me.hfTransaksi_NonSwiftOut_MataUangTransaksi.Value & "%'", "", 0, Integer.MaxValue, 0)

                                    FillOrNothing(.ValueDate_FK_Currency_ID, objCurrency(0).IdCurrency, False, oInt)
                                End Using
                                FillOrNothing(.ValueDate_CurrencyLainnya, objIfti.ValueDate_CurrencyLainnya, False, Ovarchar)
                                FillOrNothing(.ValueDate_NilaiTransaksiIDR, Me.Transaksi_NonSwiftOut_AmountdalamRupiah.Text, False, oDecimal)
                                Using objCurrency2 As TList(Of MsCurrency) = DataRepository.MsCurrencyProvider.GetPaged(MsCurrencyColumn.Code.ToString & _
                                                                         " like '%" & Me.hfTransaksi_NonSwiftOut_currency.Value & "%'", "", 0, Integer.MaxValue, 0)
                                    FillOrNothing(.Instructed_Currency, objCurrency2(0).IdCurrency, False, oInt)
                                End Using
                                FillOrNothing(.Instructed_CurrencyLainnya, objIfti.Instructed_CurrencyLainnya, False, Ovarchar)
                                FillOrNothing(.Instructed_Amount, Me.Transaksi_NonSwiftOut_instructedAmount.Text, False, oDecimal)
                                FillOrNothing(.TujuanTransaksi, Me.Transaksi_NonSwiftOut_TujuanTransaksi.Text, False, Ovarchar)
                                FillOrNothing(.SumberPenggunaanDana, Me.Transaksi_NonSwiftOut_SumberPenggunaanDana.Text, False, Ovarchar)

                                'saving
                            End With

                            DataRepository.IFTI_Approval_DetailProvider.Save(objIfti_ApprovalDetail)
                        End Using
                        ''Send Email
                        'SendEmail("Ifti Edit (User Id : " & SessionNIK & "-" & SessionGroupMenuName & ")", _
                        '          "Ifti", SessionIsBankWide, SessionFkGroupApprovalId, "", "Ifti_Approval_view.aspx")
                        ImageButtonCancel.Visible = False
                        lblMsg.Text = "Data has been edited and waiting for approval"
                        MultiViewEditNonSwiftOut.ActiveViewIndex = 1
                    End Using
                    'End If
                    OTrans.Commit()
                Catch ex As Exception
                    OTrans.Rollback()
                    Throw
                End Try

            End Using
        Catch ex As Exception
            'LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
#End Region
#Region "Function..."
    Function IsValidDecimal(ByVal intPrecision As Integer, ByVal intScale As Integer, ByVal strDatatoValidate As String) As Boolean

        Return Regex.IsMatch(strDatatoValidate, "^[\d]{1," & intPrecision & "}(\.[\d]{1," & intScale & "})?$")

    End Function
    Private Sub SetCOntrolLoad()
        'bind JenisID
        Using objJenisId As TList(Of IFTI_IDType) = DataRepository.IFTI_IDTypeProvider.GetAll
            If objJenisId.Count > 0 Then
                Me.cboNonSwiftOutPengirimNasInd_jenisidentitas.Items.Clear()
                cboNonSwiftOutPengirimNasInd_jenisidentitas.Items.Add("-Select-")

                Me.CboNonSwiftOutPengirimNonNasabah_JenisDokumen.Items.Clear()
                CboNonSwiftOutPengirimNonNasabah_JenisDokumen.Items.Add("-Select-")

                Me.CBOBenfOwnerNonSwiftOutNasabah_JenisDokumen.Items.Clear()
                CBOBenfOwnerNonSwiftOutNasabah_JenisDokumen.Items.Add("-Select-")

                Me.CBOBenfOwnerNonSwiftOutNonNasabah_JenisDokumen.Items.Clear()
                CBOBenfOwnerNonSwiftOutNonNasabah_JenisDokumen.Items.Add("-Select-")

                Me.CBONonSwiftOutPenerimaNasabah_IND_JenisIden.Items.Clear()
                Me.CBONonSwiftOutPenerimaNasabah_IND_JenisIden.Items.Add("-Select-")

                For i As Integer = 0 To objJenisId.Count - 1
                    cboNonSwiftOutPengirimNasInd_jenisidentitas.Items.Add(New ListItem(objJenisId(i).IDType, objJenisId(i).PK_IFTI_IDType.ToString))
                    CboNonSwiftOutPengirimNonNasabah_JenisDokumen.Items.Add(New ListItem(objJenisId(i).IDType, objJenisId(i).PK_IFTI_IDType.ToString))
                    CBOBenfOwnerNonSwiftOutNasabah_JenisDokumen.Items.Add(New ListItem(objJenisId(i).IDType, objJenisId(i).PK_IFTI_IDType.ToString))
                    CBOBenfOwnerNonSwiftOutNonNasabah_JenisDokumen.Items.Add(New ListItem(objJenisId(i).IDType, objJenisId(i).PK_IFTI_IDType.ToString))
                    CBONonSwiftOutPenerimaNasabah_IND_JenisIden.Items.Add(New ListItem(objJenisId(i).IDType, objJenisId(i).PK_IFTI_IDType.ToString))
                Next
            End If

            'Bind MsBentukBadanUsaha
            Using objBentukBadanUsaha As TList(Of MsBentukBidangUsaha) = DataRepository.MsBentukBidangUsahaProvider.GetAll
                If objBentukBadanUsaha.Count > 0 Then
                    cboIdenNonSwiftOutPengirimNas_Corp_BentukBadanUsaha.Items.Clear()
                    cboIdenNonSwiftOutPengirimNas_Corp_BentukBadanUsaha.Items.Add("-Select-")

                    cboPenerimaNonSwiftOutNasabah_Korp_BentukBadanUsahaLain.Items.Clear()
                    cboPenerimaNonSwiftOutNasabah_Korp_BentukBadanUsahaLain.Items.Add("-Select-")

                    For i As Integer = 0 To objBentukBadanUsaha.Count - 1
                        cboIdenNonSwiftOutPengirimNas_Corp_BentukBadanUsaha.Items.Add(New ListItem(objBentukBadanUsaha(i).BentukBidangUsaha, objBentukBadanUsaha(i).IdBentukBidangUsaha.ToString))
                        cboPenerimaNonSwiftOutNasabah_Korp_BentukBadanUsahaLain.Items.Add(New ListItem(objBentukBadanUsaha(i).BentukBidangUsaha, objBentukBadanUsaha(i).IdBentukBidangUsaha.ToString))
                    Next
                End If
            End Using

        End Using

    End Sub
    Sub clearSession()
        'Session("IFTIEdit.grvTRXKMDetilValutaAsingDATA") = Nothing
        'Session("WICEdit.grvDetilKasKeluarDATA") = Nothing
        'Session("WICEdit.ResumeKasMasukKasKeluar") = Nothing
        'Session("WICEdit.RowEdit") = Nothing
        Session("IFTIEdit.IFTIPK") = Nothing
        'Session("PickerProvinsi.Data") = Nothing
    End Sub
    Sub TransactionNonSwiftOut()
        Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)
            SafeDefaultValue = ""
            If Not IsNothing(objIfti) Then
                'umum
                Me.TxtUmum_NonSwiftOutLTDLN.Text = Safe(objIfti.LTDLNNo)
                Me.TxtUmum_NonSwiftOutLTDLNKoreksi.Text = Safe(objIfti.LTDLNNoKoreksi)
                Me.TxtUmum_NonSwiftOutTanggalLaporan.Text = FormatDate(Safe(objIfti.TanggalLaporan))
                Me.TxtUmum_NonSwiftOutPJKBankPelapor.Text = Safe(objIfti.NamaPJKBankPelapor)
                Me.TxtUmum_NonSwiftOutNamaPejabatPJKBankPelapor.Text = Safe(objIfti.NamaPejabatPJKBankPelapor)
                Me.Rb_Umum_NonSwiftOut_jenislaporan.SelectedValue = Safe(objIfti.JenisLaporan)

                'Identitas Pengirim

                'cekTipe Penyelenggara
                Dim senderType As Integer = objIfti.Sender_FK_IFTI_NasabahType_ID
                Select Case (senderType)
                    Case 1
                        'pengirimNasabahIndividu
                        ' Me.CboSwiftOutPengirim.SelectedValue = 0
                        Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipePengirim.SelectedValue = 1
                        Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipeNasabah.SelectedValue = 1
                        Me.trNonSwiftOutTipeNasabah.Visible = True
                        Me.MultiViewNonSwiftOutJenisNasabah.ActiveViewIndex = 0
                        Me.MultiViewNonSwiftOutJenisNasabah.ActiveViewIndex = 0
                        FieldSwiftOutSenderCase1()
                    Case 2
                        'pengirimNasabahKorp
                        ' Me.CboSwiftOutPengirim.SelectedValue = 0
                        Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipePengirim.SelectedValue = 1
                        Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipeNasabah.SelectedValue = 2
                        Me.trNonSwiftOutTipeNasabah.Visible = True
                        Me.MultiViewNonSwiftOutJenisNasabah.ActiveViewIndex = 0
                        Me.MultiViewNonSwiftOutJenisNasabah.ActiveViewIndex = 1
                        FieldSwiftOutSenderCase2()
                    Case 3
                        'PengirimNonNasabah
                        ' Me.CboSwiftOutPengirim.SelectedValue = 0
                        Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipePengirim.SelectedValue = 2
                        Me.trNonSwiftOutTipeNasabah.Visible = False
                        Me.MultiViewNonSwiftOutJenisNasabah.ActiveViewIndex = 1
                        FieldSwiftOutSenderCase3()
                End Select

                'Beneficiary Owner
                'check if having Beneficiary Owner
                'check if FK_IFTI_BeneficialOwnerType_ID not nothing
                If Not IsNothing(objIfti.FK_IFTI_BeneficialOwnerType_ID) Then
                    Rb_BOwnerNas_ApaMelibatkan.SelectedValue = objIfti.FK_IFTI_KeterlibatanBeneficialOwner_ID
                    Me.BenfOwnerTipePengirim.Visible = True
                    Me.BenfOwnerHubungan.Visible = True
                    Me.BenfOwnerNonSwiftOutHubunganPemilikDana.Text = objIfti.BeneficialOwner_HubunganDenganPemilikDana
                    Me.Rb_NonSwiftOutBOwnerNas_TipePengirim.SelectedValue = objIfti.FK_IFTI_BeneficialOwnerType_ID
                    Dim BOwnerID As Integer = objIfti.FK_IFTI_BeneficialOwnerType_ID
                    Select Case (BOwnerID)
                        Case 1
                            MultiViewNonSwiftOutBOwner.ActiveViewIndex = 0
                            FieldSwiftOutBOwnerNasabah()
                        Case 2
                            MultiViewNonSwiftOutBOwner.ActiveViewIndex = 1
                            FieldSwiftOutBOwnerNonNasabah()
                    End Select
                End If

                'Identitas Penerima
                Using objIftiBeneficiary As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("FK_IFTI_ID = " & getIFTIPK, "", 0, Integer.MaxValue, 0)
                    If objIftiBeneficiary.Count > 0 Then
                        ' cek ReceiverType
                        Dim receiverType As Integer = objIftiBeneficiary(0).FK_IFTI_NasabahType_ID
                        Select Case (receiverType)
                            Case 1
                                Me.RBNonSwiftOutPenerimaNasabah_TipePengirim.SelectedValue = 1
                                Me.trNonSwiftOutPenerimaTipePengirim.Visible = True
                                Me.Rb_NonSwiftOutIdenPenerimaNas_TipeNasabah.SelectedValue = 1
                                Me.MultiViewNonSwiftOutIdenPenerima.ActiveViewIndex = 0
                                Me.MultiViewNonSwiftOutPenerimaNasabah.ActiveViewIndex = 0
                                FieldNonSwiftOutPenerimaNasabahPerorangan()
                            Case 2
                                Me.RBNonSwiftOutPenerimaNasabah_TipePengirim.SelectedValue = 1
                                Me.Rb_NonSwiftOutIdenPenerimaNas_TipeNasabah.SelectedValue = 2
                                Me.trNonSwiftOutPenerimaTipePengirim.Visible = True
                                Me.MultiViewNonSwiftOutIdenPenerima.ActiveViewIndex = 0
                                Me.MultiViewNonSwiftOutPenerimaNasabah.ActiveViewIndex = 1
                                FieldNonSwiftOutPenerimaNasabahKorporasi()
                            Case 3
                                Me.RBNonSwiftOutPenerimaNasabah_TipePengirim.SelectedValue = 1
                                Me.trNonSwiftOutPenerimaTipePengirim.Visible = False
                                Me.MultiViewNonSwiftOutIdenPenerima.ActiveViewIndex = 1
                                Me.trNonSwiftOutPenerimaTipeNasabah.Visible = False
                                FieldNonSwiftOutPenerimaNonNasabah()
                        End Select
                    End If

                End Using

                'Transaksi
                Me.Transaksi_NonSwiftOut_tanggal.Text = FormatDate(objIfti.TanggalTransaksi)
                Me.Transaksi_NonSwiftOut_kantorCabangPengirim.Text = objIfti.KantorCabangPenyelengaraPengirimAsal
                Me.Transaksi_NonSwiftOut_ValueTanggalTransaksi.Text = FormatDate(objIfti.ValueDate_TanggalTransaksi)
                Me.Transaksi_NonSwiftOut_nilaitransaksi.Text = objIfti.ValueDate_NilaiTransaksi
                Using objCurrency As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(objIfti.ValueDate_FK_Currency_ID.GetValueOrDefault(0))
                    If Not IsNothing(objCurrency) Then
                        Me.Transaksi_NonSwiftOut_MataUangTransaksi.Text = objCurrency.Code
                        hfTransaksi_NonSwiftOut_MataUangTransaksi.Value = objCurrency.Code
                    End If
                End Using
                Me.Transaksi_NonSwiftOut_MataUangTransaksiLain.Text = objIfti.ValueDate_CurrencyLainnya
                Me.Transaksi_NonSwiftOut_AmountdalamRupiah.Text = objIfti.ValueDate_NilaiTransaksiIDR
                Using objCurrency As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(objIfti.Instructed_Currency.GetValueOrDefault(0))
                    If Not IsNothing(objCurrency) Then
                        Me.Transaksi_NonSwiftOut_currency.Text = objCurrency.Code
                        hfTransaksi_NonSwiftOut_currency.Value = objCurrency.Code
                    End If
                End Using
                Me.Transaksi_NonSwiftOut_currencyLain.Text = objIfti.Instructed_CurrencyLainnya
                If Not IsNothing(objIfti.Instructed_Amount) Then
                    Me.Transaksi_NonSwiftOut_instructedAmount.Text = objIfti.Instructed_Amount
                End If
                Me.Transaksi_NonSwiftOut_TujuanTransaksi.Text = objIfti.TujuanTransaksi
                Me.Transaksi_NonSwiftOut_SumberPenggunaanDana.Text = objIfti.SumberPenggunaanDana
            End If
        End Using
    End Sub
#End Region
#Region "NonSwiftOutBOwner..."
    Sub FieldSwiftOutBOwnerNasabah()
        'BeneficialOwnerNasabah
        Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetPaged(MsKotaKabColumn.Activation.ToString & "=1", MsKotaKabColumn.NamaKotaKab.ToString, 0, Integer.MaxValue, 0)
        Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
        Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetPaged(MsProvinceColumn.activation.ToString & "=1", MsProvinceColumn.Nama.ToString, 0, Integer.MaxValue, 0)
        Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi

        Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)
            SafeDefaultValue = ""
            If Not IsNothing(objIfti) Then
                Me.BenfOwnerNonSwiftOutNasabah_rekening.Text = objIfti.BeneficialOwner_NoRekening
                Me.BenfOwnerNonSwiftOutNasabah_Nama.Text = objIfti.BeneficialOwner_NamaLengkap
                If ObjectAntiNull(objIfti.BeneficialOwner_TanggalLahir) = True Then
                    Me.BenfOwnerNonSwiftOutNasabah_tanggalLahir.Text = FormatDate(objIfti.BeneficialOwner_TanggalLahir)
                End If

                Me.BenfOwnerNonSwiftOutNasabah_AlamatIden.Text = objIfti.BeneficialOwner_ID_Alamat
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objIfti.BeneficialOwner_ID_KotaKab)
                If Not IsNothing(objSkotaKab) Then
                    Me.BenfOwnerNonSwiftOutNasabah_KotaIden.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfBenfOwnerNonSwiftOutNasabah_KotaIden.Value = Safe(objSkotaKab.IDKotaKab)
                End If

                Me.BenfOwnerNonSwiftOutNasabah_kotaLainIden.Text = objIfti.BeneficialOwner_ID_KotaKabLainnya
                objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(SafeNumIndex(objIfti.BeneficialOwner_ID_Propinsi))
                If Not IsNothing(objSProvinsi) Then
                    Me.BenfOwnerNonSwiftOutNasabah_ProvinsiIden.Text = Safe(objSProvinsi.Nama)
                    hfBenfOwnerNonSwiftOutNasabah_ProvinsiIden.Value = Safe(objSProvinsi.IdProvince)
                End If

                Me.BenfOwnerNonSwiftOutNasabah_ProvinsiLainIden.Text = objIfti.BeneficialOwner_ID_PropinsiLainnya

                If ObjectAntiNull(objIfti.BeneficialOwner_FK_IFTI_IDType) = True Then
                    Me.CBOBenfOwnerNonSwiftOutNasabah_JenisDokumen.SelectedValue = objIfti.BeneficialOwner_FK_IFTI_IDType
                Else
                    Me.CBOBenfOwnerNonSwiftOutNasabah_JenisDokumen.SelectedIndex = 0
                End If

                Me.BenfOwnerNonSwiftOutNasabah_NomorIden.Text = objIfti.BeneficialOwner_NomorID
            End If
        End Using


    End Sub
    Sub FieldSwiftOutBOwnerNonNasabah()
        'BeneficialOwnerNonNasabah
        Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetPaged(MsKotaKabColumn.Activation.ToString & "=1", MsKotaKabColumn.NamaKotaKab.ToString, 0, Integer.MaxValue, 0)
        Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
        Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetPaged(MsProvinceColumn.activation.ToString & "=1", MsProvinceColumn.Nama.ToString, 0, Integer.MaxValue, 0)
        Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi

        Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)
            SafeDefaultValue = ""
            If Not IsNothing(objIfti) Then
                Me.BenfOwnerNonSwiftOutNonNasabah_Nama.Text = objIfti.BeneficialOwner_NamaLengkap
                If Not IsNothing(objIfti.BeneficialOwner_TanggalLahir) Then
                    Me.BenfOwnerNonSwiftOutNonNasabah_TanggalLahir.Text = objIfti.BeneficialOwner_TanggalLahir
                End If
                Me.BenfOwnerNonSwiftOutNonNasabah_AlamatIden.Text = objIfti.BeneficialOwner_ID_Alamat
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objIfti.BeneficialOwner_ID_KotaKab)
                If Not IsNothing(objSkotaKab) Then
                    Me.BenfOwnerNonSwiftOutNonNasabah_KotaIden.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfBenfOwnerNonSwiftOutNonNasabah_KotaIden.Value = Safe(objSkotaKab.IDKotaKab)
                End If

                Me.BenfOwnerNonSwiftOutNonNasabah_KotaLainIden.Text = objIfti.BeneficialOwner_ID_KotaKabLainnya
                objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(SafeNumIndex(objIfti.BeneficialOwner_ID_Propinsi))
                If Not IsNothing(objSProvinsi) Then
                    Me.BenfOwnerNonSwiftOutNonNasabah_ProvinsiIden.Text = Safe(objSProvinsi.Nama)
                    hfBenfOwnerNonSwiftOutNonNasabah_ProvinsiIden.Value = Safe(objSProvinsi.IdProvince)
                End If

                Me.BenfOwnerNonSwiftOutNonNasabah_ProvinsiLainIden.Text = objIfti.BeneficialOwner_ID_PropinsiLainnya
                If Not IsNothing(objIfti.BeneficialOwner_FK_IFTI_IDType) Then
                    Me.CBOBenfOwnerNonSwiftOutNonNasabah_JenisDokumen.SelectedValue = objIfti.BeneficialOwner_FK_IFTI_IDType.GetValueOrDefault(0)
                End If
                Me.BenfOwnerNonSwiftOutNonNasabah_NomorIdentitas.Text = objIfti.BeneficialOwner_NomorID
            End If
        End Using
    End Sub
#End Region
#Region "NonSwiftOutReceiver..."
    Sub FieldNonSwiftOutPenerimaNasabahPerorangan()
        'PenerimaNasabahPerorangan

        Dim objNegara As TList(Of MsNegara) = DataRepository.MsNegaraProvider.GetPaged(MsNegaraColumn.Activation.ToString & "=1", MsNegaraColumn.NamaNegara.ToString, 0, Integer.MaxValue, 0)
        Dim objSnegara As MsNegara 'Penampung hasil search negara yang pke picker

        Using objIfti As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("FK_IFTI_ID=" & getIFTIPK, "", 0, 1, 0)
            Me.txtPenerimaNonSwiftOut_rekening.Text = objIfti(0).Beneficiary_Nasabah_INDV_NoRekening
            Me.txtPenerimaNonSwiftOutNasabah_IND_nama.Text = objIfti(0).Beneficiary_Nasabah_INDV_NamaLengkap
            Me.txtPenerimaNonSwiftOutNasabah_IND_TanggalLahir.Text = FormatDate(objIfti(0).Beneficiary_Nasabah_INDV_TanggalLahir)
            If Not IsNothing(objIfti(0).Beneficiary_Nasabah_INDV_KewargaNegaraan) Then
                Me.RbNonSwiftOutPenerimaNasabah_IND_Warganegara.SelectedValue = objIfti(0).Beneficiary_Nasabah_INDV_KewargaNegaraan
                If objIfti(0).Beneficiary_Nasabah_INDV_KewargaNegaraan = "2" Then
                    Me.NegaraNonSwift_penerimaPerorangan.Visible = True
                    Me.NegaraLainNonSwift_penerimaPerorangan.Visible = True
                    objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, objIfti(0).Beneficiary_Nasabah_INDV_Negara)
                    If Not IsNothing(objSnegara) Then
                        txtPenerimaNonSwiftOutNasabah_IND_negara.Text = Safe(objSnegara.NamaNegara)
                        hfNonSwiftOutPengirimNasabah_IND_negara.Value = Safe(objSnegara.IDNegara)
                    End If
                    Me.txtPenerimaNonSwiftOutNasabah_IND_negaraLain.Text = objIfti(0).Beneficiary_Nasabah_INDV_NegaraLainnya
                End If
            End If

            Me.txtPenerimaNonSwiftOutNasabah_IND_alamatIden.Text = objIfti(0).Beneficiary_Nasabah_INDV_ID_Alamat_Iden
            Me.txtPenerimaNonSwiftOutNasabah_IND_negaraBagian.Text = objIfti(0).Beneficiary_Nasabah_INDV_ID_NegaraBagian
            objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, objIfti(0).Beneficiary_Nasabah_INDV_ID_Negara)
            If Not IsNothing(objSnegara) Then
                txtPenerimaNonSwiftOutNasabah_IND_negaraIden.Text = Safe(objSnegara.NamaNegara)
                hfNonSwiftOutPengirimNasabah_IND_negaraIden.Value = Safe(objSnegara.IDNegara)
            End If
            Me.txtPenerimaNonSwiftOutNasabah_IND_negaraLainIden.Text = objIfti(0).Beneficiary_Nasabah_INDV_ID_NegaraLainnya

            Me.CBONonSwiftOutPenerimaNasabah_IND_JenisIden.SelectedValue = objIfti(0).Beneficiary_Nasabah_INDV_FK_IFTI_IDType
            Me.txtPenerimaNonSwiftOutNasabah_IND_NomorIden.Text = objIfti(0).Beneficiary_Nasabah_INDV_NomorID
        End Using
    End Sub
    Sub FieldNonSwiftOutPenerimaNasabahKorporasi()
        'PenerimaNasabahKorporasi
        Dim objNegara As TList(Of MsNegara) = DataRepository.MsNegaraProvider.GetPaged(MsNegaraColumn.Activation.ToString & "=1", MsNegaraColumn.NamaNegara.ToString, 0, Integer.MaxValue, 0)
        Dim objSnegara As MsNegara 'Penampung hasil search negara yang pke picker

        Using objIfti As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("FK_IFTI_ID=" & getIFTIPK, "", 0, 1, 0)
            Me.txtPenerimaNonSwiftOut_rekening.Text = objIfti(0).Beneficiary_Nasabah_CORP_NoRekening
            Using objBadanUsaha As MsBentukBidangUsaha = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(objIfti(0).Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault(0))
                If Not IsNothing(objBadanUsaha) Then
                    Me.cboPenerimaNonSwiftOutNasabah_Korp_BentukBadanUsahaLain.SelectedValue = objBadanUsaha.BentukBidangUsaha
                End If
            End Using
            Me.txtPenerimaNonSwiftOutNasabah_Korp_BentukBadanUsahaLainnya.Text = objIfti(0).Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya
            Me.txtPenerimaNonSwiftOutNasabah_Korp_namaKorp.Text = objIfti(0).Beneficiary_Nasabah_CORP_NamaKorporasi
            Using objBidangUsaha As MsBidangUsaha = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(objIfti(0).Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id.GetValueOrDefault(0))
                If Not IsNothing(objBidangUsaha) Then
                    Me.txtPenerimaNonSwiftOutNasabah_Korp_BidangUsahaKorp.Text = objBidangUsaha.NamaBidangUsaha
                End If
            End Using
            Me.txtPenerimaNonSwiftOutNasabah_Korp_AlamatKorp.Text = objIfti(0).Beneficiary_Nasabah_CORP_AlamatLengkap
            Me.txtPenerimaNonSwiftOutNasabah_Korp_Kota.Text = objIfti(0).Beneficiary_Nasabah_CORP_ID_NegaraBagian
            objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, objIfti(0).Beneficiary_Nasabah_CORP_ID_Negara)
            If Not IsNothing(objSnegara) Then
                txtPenerimaNonSwiftOutNasabah_Korp_NegaraKorp.Text = Safe(objSnegara.NamaNegara)
                hfPenerimaNonSwiftOutNasabah_Korp_NegaraKorp.Value = Safe(objSnegara.IDNegara)
            End If
            Me.txtPenerimaNonSwiftOutNasabah_Korp_NegaraLainnyaKorp.Text = objIfti(0).Beneficiary_Nasabah_CORP_ID_NegaraLainnya
        End Using
    End Sub
    Sub FieldNonSwiftOutPenerimaNonNasabah()
        'PenerimaNonNasabah

        Dim objNegara As TList(Of MsNegara) = DataRepository.MsNegaraProvider.GetPaged(MsNegaraColumn.Activation.ToString & "=1", MsNegaraColumn.NamaNegara.ToString, 0, Integer.MaxValue, 0)
        Dim objSnegara As MsNegara 'Penampung hasil search negara yang pke picker

        Using objIfti As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("FK_IFTI_ID=" & getIFTIPK, "", 0, 1, 0)
            Me.txtPenerimaNonSwiftOutNonNasabah_KodeRahasia.Text = objIfti(0).Beneficiary_NonNasabah_KodeRahasia
            'If objIfti(0).Beneficiary_NonNasabah_ID_Alamat Is Nothing Then
            Me.txtPenerimaNonSwiftOutNonNasabah_rekening.Text = objIfti(0).Beneficiary_NonNasabah_NoRekening
            'End If
            Me.txtPenerimaNonSwiftOutNonNasabah_namabank.Text = objIfti(0).Beneficiary_NonNasabah_NamaBank
            Me.txtPenerimaNonSwiftOutNonNasabah_Nama.Text = objIfti(0).Beneficiary_NonNasabah_NamaLengkap
            Me.txtPenerimaNonSwiftOutNonNasabah_Alamat.Text = objIfti(0).Beneficiary_NonNasabah_ID_Alamat
            objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, objIfti(0).Beneficiary_NonNasabah_ID_Negara)
            If Not IsNothing(objSnegara) Then
                txtPenerimaNonSwiftOutNonNasabah_Negara.Text = Safe(objSnegara.NamaNegara)
                hfSwiftInNonSwiftOutPengirimNonNasabah_Negara.Value = Safe(objSnegara.IDNegara)
            End If
            Me.txtPenerimaNonSwiftOutNonNasabah_negaraLain.Text = objIfti(0).Beneficiary_NonNasabah_ID_NegaraLainnya
        End Using

    End Sub
#End Region
#Region "SwiftOutSenderType..."
    Sub FieldSwiftOutSenderCase1()
        'PengirimNasabahIndividu

        Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetPaged(MsKotaKabColumn.Activation.ToString & "=1", MsKotaKabColumn.NamaKotaKab.ToString, 0, Integer.MaxValue, 0)
        Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
        Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetPaged(MsProvinceColumn.activation.ToString & "=1", MsProvinceColumn.Nama.ToString, 0, Integer.MaxValue, 0)
        Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi
        Dim objNegara As TList(Of MsNegara) = DataRepository.MsNegaraProvider.GetPaged(MsNegaraColumn.Activation.ToString & "=1", MsNegaraColumn.NamaNegara.ToString, 0, Integer.MaxValue, 0)
        Dim objSnegara As MsNegara 'Penampung hasil search negara yang pke picker
        Dim objPekerjaan As TList(Of MsPekerjaan) = DataRepository.MsPekerjaanProvider.GetPaged(MsPekerjaanColumn.Activation.ToString & "=1", MsPekerjaanColumn.NamaPekerjaan.ToString, 0, Integer.MaxValue, 0)
        Dim objSPekerjaan As MsPekerjaan 'Penampung hasil search pekerjaan

        Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)
            SetnGetSenderAccount = objIfti.Sender_Nasabah_INDV_NoRekening
            Me.TxtIdenNonSwiftOutPengirim_Norekening.Text = SetnGetSenderAccount
            Me.TxtIdenNonSwiftOutPengirimNas_Ind_NamaLengkap.Text = objIfti.Sender_Nasabah_INDV_NamaLengkap
            Me.TxtIdenNonSwiftOutPengirimNas_Ind_TanggalLahir.Text = FormatDate(objIfti.Sender_Nasabah_INDV_TanggalLahir)
            Me.RbIdenNonSwiftOutPengirimNas_Ind_Kewarganegaraan.SelectedValue = objIfti.Sender_Nasabah_INDV_KewargaNegaraan
            If objIfti.Sender_Nasabah_INDV_KewargaNegaraan = "2" Then
                Me.NegaraNonSwift_pengirimPerorangan.Visible = True
                Me.NegaraLainNonSwift_pengirimPerorangan.Visible = True
                objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, objIfti.Sender_Nasabah_INDV_Negara)
                If Not IsNothing(objSnegara) Then
                    TxtIdenNonSwiftOutPengirimNas_Ind_Negara.Text = Safe(objSnegara.NamaNegara)
                    hfIdenNonSwiftOutPengirimNas_Ind_Negara.Value = Safe(objSnegara.IDNegara)
                End If
                Me.TxtIdenNonSwiftOutPengirimNas_Ind_NegaraLain.Text = objIfti.Sender_Nasabah_INDV_NegaraLainnya
            End If
            objSPekerjaan = objPekerjaan.Find(MsPekerjaanColumn.IDPekerjaan, objIfti.Sender_Nasabah_INDV_Pekerjaan)
            If Not IsNothing(objSPekerjaan) Then
                TxtIdenNonSwiftOutPengirimNas_Ind_Pekerjaan.Text = Safe(objSPekerjaan.NamaPekerjaan)
                hfIdenNonSwiftOutPengirimNas_Ind_pekerjaan.Value = Safe(objSPekerjaan.IDPekerjaan)
            End If

            Me.TxtIdenNonSwiftOutPengirimNas_Ind_Pekerjaanlain.Text = objIfti.Sender_Nasabah_INDV_PekerjaanLainnya
            Me.TxtIdenNonSwiftOutPengirimNas_Ind_Alamat.Text = objIfti.Sender_Nasabah_INDV_DOM_Alamat

            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objIfti.Sender_Nasabah_INDV_DOM_KotaKab)
            If Not IsNothing(objSkotaKab) Then
                Me.TxtIdenNonSwiftOutPengirimNas_Ind_Kota.Text = Safe(objSkotaKab.NamaKotaKab)
                hfIdenNonSwiftOutPengirimNas_Ind_Kota_dom.Value = Safe(objSkotaKab.IDKotaKab)
            End If

            Me.TxtIdenNonSwiftOutPengirimNas_Ind_kotalain.Text = objIfti.Sender_Nasabah_INDV_DOM_KotaKabLainnya
            objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(SafeNumIndex(objIfti.Sender_Nasabah_INDV_DOM_Propinsi))
            If Not IsNothing(objSProvinsi) Then
                TxtIdenNonSwiftOutPengirimNas_Ind_provinsi.Text = Safe(objSProvinsi.Nama)
                hfIdenNonSwiftOutPengirimNas_Ind_provinsi_dom.Value = Safe(objSProvinsi.IdProvince)
            End If
            Me.TxtIdenNonSwiftOutPengirimNas_Ind_provinsiLain.Text = objIfti.Sender_Nasabah_INDV_DOM_PropinsiLainnya
            Me.TxtIdenNonSwiftOutPengirimNas_Ind_alamatIdentitas.Text = objIfti.Sender_Nasabah_INDV_ID_Alamat

            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objIfti.Sender_Nasabah_INDV_ID_KotaKab)
            If Not IsNothing(objSkotaKab) Then
                Me.TxtIdenNonSwiftOutPengirimNas_Ind_kotaIdentitas.Text = Safe(objSkotaKab.NamaKotaKab)
                hfIdenNonSwiftOutPengirimNas_Ind_kotaIdentitas.Value = Safe(objSkotaKab.IDKotaKab)
            End If
            Me.TxtIdenNonSwiftOutPengirimNas_Ind_KotaLainIden.Text = objIfti.Sender_Nasabah_INDV_ID_KotaKabLainnya
            objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(SafeNumIndex(objIfti.Sender_Nasabah_INDV_ID_Propinsi))
            If Not IsNothing(objSProvinsi) Then
                Me.TxtIdenNonSwiftOutPengirimNas_Ind_ProvinsiIden.Text = Safe(objSProvinsi.Nama)
                hfIdenNonSwiftOutPengirimNas_Ind_ProvinsiIden.Value = Safe(objSProvinsi.IdProvince)
            End If

            Me.TxtIdenNonSwiftOutPengirimNas_Ind_ProvinsilainIden.Text = objIfti.Sender_Nasabah_INDV_ID_PropinsiLainnya
            Me.cboNonSwiftOutPengirimNasInd_jenisidentitas.SelectedValue = objIfti.Sender_Nasabah_INDV_FK_IFTI_IDType
            Me.TxtIdenNonSwiftOutPengirimNas_Ind_noIdentitas.Text = objIfti.Sender_Nasabah_INDV_NomorID
        End Using
    End Sub
    Sub FieldSwiftOutSenderCase2()
        'PengirimNasabahKorporasi

        Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetPaged(MsKotaKabColumn.Activation.ToString & "=1", MsKotaKabColumn.NamaKotaKab.ToString, 0, Integer.MaxValue, 0)
        Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
        Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetPaged(MsProvinceColumn.activation.ToString & "=1", MsProvinceColumn.Nama.ToString, 0, Integer.MaxValue, 0)
        Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi

        Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)
            SetnGetSenderAccount = objIfti.Sender_Nasabah_INDV_NoRekening
            Me.TxtIdenNonSwiftOutPengirim_Norekening.Text = SetnGetSenderAccount

            Using objBentukBadanUsaha As MsBentukBidangUsaha = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(objIfti.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault(0))
                Me.cboIdenNonSwiftOutPengirimNas_Corp_BentukBadanUsaha.SelectedValue = objBentukBadanUsaha.BentukBidangUsaha
            End Using
            Me.TxtIdenNonSwiftOutPengirimNas_Corp_BentukBadanUsahaLain.Text = objIfti.Sender_Nasabah_CORP_BentukBadanUsahaLainnya
            Me.TxtIdenNonSwiftOutPengirimNas_Corp_NamaKorp.Text = objIfti.Sender_Nasabah_CORP_NamaKorporasi
            Using objBidangUsaha As MsBidangUsaha = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(objIfti.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id.GetValueOrDefault(0))
                Me.TxtIdenNonSwiftOutPengirimNas_Corp_BidangUsahaKorp.Text = objBidangUsaha.NamaBidangUsaha
                hfIdenNonSwiftOutPengirimNas_Corp_BidangUsahaKorp.Value = objBidangUsaha.IdBidangUsaha
            End Using
            Me.TxtIdenNonSwiftOutPengirimNas_Corp_alamatkorp.Text = objIfti.Sender_Nasabah_CORP_AlamatLengkap
            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objIfti.Sender_Nasabah_CORP_KotaKab)
            If Not IsNothing(objSkotaKab) Then
                Me.TxtIdenNonSwiftOutPengirimNas_Corp_kotakorp.Text = Safe(objSkotaKab.NamaKotaKab)
                hfIdenNonSwiftOutPengirimNas_Corp_kotakorp.Value = Safe(objSkotaKab.IDKotaKab)
            End If

            Me.TxtIdenNonSwiftOutPengirimNas_Corp_kotakorplain.Text = objIfti.Sender_Nasabah_CORP_KotaKabLainnya
            objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(SafeNumIndex(objIfti.Sender_Nasabah_CORP_Propinsi))
            If Not IsNothing(objSProvinsi) Then
                Me.TxtIdenNonSwiftOutPengirimNas_Corp_provKorp.Text = Safe(objSProvinsi.Nama)
                hfIdenNonSwiftOutPengirimNas_Corp_provKorp.Value = Safe(objSProvinsi.IdProvince)
            End If
            Me.TxtIdenNonSwiftOutPengirimNas_Corp_provKorpLain.Text = objIfti.Sender_Nasabah_CORP_PropinsiLainnya
            Me.TxtIdenNonSwiftOutPengirimNas_Corp_AlamatLuar.Text = objIfti.Sender_Nasabah_CORP_Alamat_KantorCabangPengirim
            Me.TxtIdenNonSwiftOutPengirimNas_Corp_AlamatAsal.Text = objIfti.Sender_Nasabah_CORP_LN_AlamatLengkap
        End Using
    End Sub
    Sub FieldSwiftOutSenderCase3()
        'PengirimNonNasabah

        Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetPaged(MsKotaKabColumn.Activation.ToString & "=1", MsKotaKabColumn.NamaKotaKab.ToString, 0, Integer.MaxValue, 0)
        Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
        Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetPaged(MsProvinceColumn.activation.ToString & "=1", MsProvinceColumn.Nama.ToString, 0, Integer.MaxValue, 0)
        Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi

        Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)
            Me.RbNonSwiftOutPengirimNonNasabah_100Juta.SelectedValue = objIfti.FK_IFTI_NonNasabahNominalType_ID
            Me.TxtNonSwiftOutPengirimNonNasabah_nama.Text = objIfti.Sender_NonNasabah_NamaLengkap
            Me.TxtNonSwiftOutPengirimNonNasabah_TanggalLahir.Text = FormatDate(objIfti.Sender_NonNasabah_TanggalLahir)
            Me.TxtNonSwiftOutPengirimNonNasabah_alamatiden.Text = objIfti.Sender_NonNasabah_ID_Alamat
            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objIfti.Sender_NonNasabah_ID_KotaKab)
            If Not IsNothing(objSkotaKab) Then
                Me.TxtNonSwiftOutPengirimNonNasabah_kotaIden.Text = Safe(objSkotaKab.NamaKotaKab)
                hfNonSwiftOutPengirimNonNasabah_kotaIden.Value = Safe(objSkotaKab.IDKotaKab)
            End If

            Me.TxtNonSwiftOutPengirimNonNasabah_KoaLainIden.Text = objIfti.Sender_NonNasabah_ID_KotaKabLainnya
            objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(SafeNumIndex(objIfti.Sender_NonNasabah_ID_Propinsi.GetValueOrDefault(0)))
            If Not IsNothing(objSProvinsi) Then
                Me.TxtNonSwiftOutPengirimNonNasabah_ProvIden.Text = Safe(objSProvinsi.Nama)
                hfNonSwiftOutPengirimNonNasabah_ProvIden.Value = Safe(objSProvinsi.IdProvince)
            End If
            Me.TxtNonSwiftOutPengirimNonNasabah_ProvLainIden.Text = objIfti.Sender_NonNasabah_ID_PropinsiLainnya
            Me.CboNonSwiftOutPengirimNonNasabah_JenisDokumen.SelectedValue = objIfti.Sender_NonNasabah_FK_IFTI_IDType.GetValueOrDefault(0)
            Me.TxtNonSwiftOutPengirimNonNasabah_NomorIden.Text = objIfti.Sender_NonNasabah_NomorID
        End Using
    End Sub
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                clearSession()
                'AuditTrailBLL.InsertAuditTrailUserAccess(Sahassa.AML.Commonly.SessionCurrentPage)
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                Using ObjAuditTrailUserAccess As AuditTrail_UserAccess = New AuditTrail_UserAccess
                    With ObjAuditTrailUserAccess
                        .AuditTrail_UserAccessUserid = Sahassa.AML.Commonly.SessionUserId
                        .AuditTrail_UserAccessActionDate = DateTime.Now
                        .AuditTrail_UserAccessAction = UserAccessAction
                    End With
                    DataRepository.AuditTrail_UserAccessProvider.Save(ObjAuditTrailUserAccess)
                End Using
                SetCOntrolLoad()
                TransactionNonSwiftOut()
                'loadIFTIToField()
                'loadResume()

                'If MultiView1.ActiveViewIndex = 0 Then
                '    ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('IdTerlapor','Img1','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
                '    ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('Transaksi','Img2','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
                'End If
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            'LogError(ex)
        End Try
    End Sub

    Protected Sub ImageButtonCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonCancel.Click
        Response.Redirect("IFTIView.aspx")
    End Sub

    Protected Sub RbIdenNonSwiftOutPengirimNas_Ind_Kewarganegaraan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RbIdenNonSwiftOutPengirimNas_Ind_Kewarganegaraan.SelectedIndexChanged
        If RbIdenNonSwiftOutPengirimNas_Ind_Kewarganegaraan.SelectedValue = 1 Then
            Me.NegaraNonSwift_pengirimPerorangan.Visible = False
            Me.NegaraLainNonSwift_pengirimPerorangan.Visible = False
        Else
            Me.NegaraNonSwift_pengirimPerorangan.Visible = True
            Me.NegaraLainNonSwift_pengirimPerorangan.Visible = True

        End If
    End Sub

    Protected Sub RbNonSwiftOutPenerimaNasabah_IND_Warganegara_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RbNonSwiftOutPenerimaNasabah_IND_Warganegara.SelectedIndexChanged
        If RbNonSwiftOutPenerimaNasabah_IND_Warganegara.SelectedValue = 1 Then
            Me.NegaraNonSwift_penerimaPerorangan.Visible = False
            Me.NegaraLainNonSwift_penerimaPerorangan.Visible = False
        Else
            Me.NegaraNonSwift_penerimaPerorangan.Visible = True
            Me.NegaraLainNonSwift_penerimaPerorangan.Visible = True
        End If
    End Sub
    Protected Sub cvalPageErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles cvalPageErr.PreRender
        If cvalPageErr.IsValid = False Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
        End If
    End Sub
    Protected Sub imgOKMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgOKMsg.Click
        Response.Redirect("Iftiview.aspx")
    End Sub

    Protected Sub Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipePengirim_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipePengirim.SelectedIndexChanged
        If Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipePengirim.SelectedValue = 1 Then
            If Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipeNasabah.SelectedValue = 1 Then
                Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipePengirim.SelectedValue = 1
                Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipeNasabah.SelectedValue = 1
                Me.trNonSwiftOutTipeNasabah.Visible = True
                Me.MultiViewNonSwiftOutJenisNasabah.ActiveViewIndex = 0
                Me.MultiViewNonSwiftOutJenisNasabah.ActiveViewIndex = 0
            Else
                Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipePengirim.SelectedValue = 1
                Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipeNasabah.SelectedValue = 2
                Me.trNonSwiftOutTipeNasabah.Visible = True
                Me.MultiViewNonSwiftOutJenisNasabah.ActiveViewIndex = 0
                Me.MultiViewNonSwiftOutJenisNasabah.ActiveViewIndex = 1
            End If

        Else
            Rb_NonSwiftOutIdenNonSwiftOutPengirimNas_TipePengirim.SelectedValue = 2
            Me.trNonSwiftOutTipeNasabah.Visible = False
            Me.MultiViewNonSwiftOutJenisNasabah.ActiveViewIndex = 1
        End If
    End Sub


    Protected Sub Rb_BOwnerNas_ApaMelibatkan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Rb_BOwnerNas_ApaMelibatkan.SelectedIndexChanged
        If Rb_BOwnerNas_ApaMelibatkan.SelectedValue = 1 Then
            Me.BenfOwnerTipePengirim.Visible = True
            Me.BenfOwnerHubungan.Visible = True
            MultiViewNonSwiftOutBOwner.Visible = True
        Else
            Me.BenfOwnerTipePengirim.Visible = False
            Me.BenfOwnerHubungan.Visible = False
            MultiViewNonSwiftOutBOwner.Visible = False
        End If
    End Sub
End Class
