Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Partial Class LevelTypeDelete
    Inherits Parent

    ''' <summary>
    ''' get user id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetLevelTypeId() As String
        Get
            Return Me.Request.Params("LevelTypeID")
        End Get
    End Property

    ''' <summary>
    ''' cancel handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "LevelTypeView.aspx"

        Me.Response.Redirect("LevelTypeView.aspx", False)
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub DeleteLevelTypeBySU()
        Try
            Using AccessLevelType As New AMLDAL.AMLDataSetTableAdapters.LevelTypeTableAdapter
                AccessLevelType.DeleteLevelType(Me.GetLevelTypeId)
            End Using
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub InsertAuditTrail()
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(4)
            Using AccessLevelType As New AMLDAL.AMLDataSetTableAdapters.LevelTypeTableAdapter
                Using dt As Data.DataTable = AccessLevelType.GetDataByLevelTypeID(CInt(Me.GetLevelTypeId))
                    Dim row As AMLDAL.AMLDataSet.LevelTypeRow = dt.Rows(0)
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "LevelType", "LevelTypeID", "Delete", "", row.LevelTypeID, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "LevelType", "LevelTypeName", "Delete", "", Me.TextLevelTypeName.Text, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "LevelType", "LevelTypeDesc", "Delete", "", Me.TextLevelTypeDescription.Text, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "LevelType", "LevelTypeCreatedDate", "Delete", row.LevelTypeCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), row.LevelTypeCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                    End Using
                End Using
            End Using
        Catch
            Throw
        End Try
    End Sub

    ''' <summary>
    ''' update handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageUpdate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageUpdate.Click
        Dim oSQLTrans As SqlTransaction = Nothing
        Page.Validate()

        If Page.IsValid Then
            Try
                'Buat variabel untuk menampung nilai-nilai baru
                Dim LevelTypeID As String = Me.LabelTypeID.Text
                Dim LevelTypeName As String = Me.TextLevelTypeName.Text
                Dim LevelTypeDescription As String = Me.TextLevelTypeDescription.Text

                'Periksa apakah LevelTypeName tsb sdh ada dalam tabel Group atau belum
                Using AccessLevelType As New AMLDAL.AMLDataSetTableAdapters.LevelTypeTableAdapter
                    Dim counter As Int32 = AccessLevelType.CountMatchingLevelType(LevelTypeName)

                    'Counter > 0 berarti LevelTypeName tersebut masih ada dalam tabel LevelType dan bisa didelete
                    If counter > 0 Then
                        'Periksa apakah LevelTypeName tsb dalam status pending approval atau tidak
                        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.LevelType_ApprovalTableAdapter
                            oSQLTrans = BeginTransaction(AccessPending, IsolationLevel.ReadUncommitted)
                            counter = AccessPending.CountLevelTypeApproval(LevelTypeID)

                            'Counter = 0 berarti LevelTypeName tersebut tidak dalam status pending approval
                            If counter = 0 Then
                                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                                    Me.InsertAuditTrail()
                                    Me.DeleteLevelTypeBySU()
                                    Me.LblSuccess.Text = "Success to Delete Level Type."
                                    Me.LblSuccess.Visible = True
                                Else
                                    'Buat variabel untuk menampung nilai-nilai lama
                                    Dim LevelTypeID_Old As Int32 = ViewState("LevelTypeID_Old")
                                    Dim LevelTypeName_Old As String = ViewState("LevelTypeName_Old")
                                    Dim LevelTypeDescription_Old As String = ViewState("LevelTypeDescription_Old")
                                    Dim LevelTypeCreatedDate_Old As DateTime = ViewState("LevelTypeCreatedDate_Old")

                                    'variabel untuk menampung nilai identity dari tabel LevelTypePendingApprovalID
                                    Dim LevelTypePendingApprovalID As Int64

                                    'Using TransScope As New Transactions.TransactionScope()
                                    Using AccessLevelTypePendingApproval As New AMLDAL.AMLDataSetTableAdapters.LevelType_PendingApprovalTableAdapter
                                        SetTransaction(AccessLevelTypePendingApproval, oSQLTrans)
                                        'Tambahkan ke dalam tabel LevelType_PendingApproval dengan ModeID = 3 (Delete) 
                                        LevelTypePendingApprovalID = AccessLevelTypePendingApproval.InsertLevelType_PendingApproval(Sahassa.AML.Commonly.SessionUserId, Now, "Level Type Delete", 3)

                                        'Tambahkan ke dalam tabel LevelType_Approval dengan ModeID = 3 (Delete) 
                                        AccessPending.Insert(LevelTypePendingApprovalID, 3, LevelTypeID, LevelTypeName, LevelTypeDescription, Now, LevelTypeID_Old, LevelTypeName_Old, LevelTypeDescription_Old, LevelTypeCreatedDate_Old)

                                        oSQLTrans.Commit()
                                        '        TransScope.Complete()
                                    End Using
                                    'End Using

                                    Dim MessagePendingID As Integer = 8403 'MessagePendingID 8403 = LevelType Delete 

                                    Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & LevelTypeName

                                    Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & LevelTypeName, False)
                                End If
                            Else
                                Throw New Exception("Cannot delete the following Level Type: '" & LevelTypeName & "' because it is currently waiting for approval.")
                            End If
                        End Using
                    Else 'Counter = 0 berarti LevelTypeName tersebut tidak ada dalam tabel LevelType
                        Throw New Exception("Cannot delete the following Level Type: '" & LevelTypeName & "' because that Level Type Name does not exist in the database anymore.")
                    End If
                End Using
            Catch ex As Exception
                If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
                Me.cvalPageError.IsValid = False
                Me.cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            Finally
                If Not oSQLTrans Is Nothing Then
                    oSQLTrans.Dispose()
                    oSQLTrans = Nothing
                End If
            End Try
        End If
    End Sub

    ''' <summary>
    ''' filledit data
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillEditData()
        Try
            Using AccessLevelType As New AMLDAL.AMLDataSetTableAdapters.LevelTypeTableAdapter
                Using TableLevelType As AMLDAL.AMLDataSet.LevelTypeDataTable = AccessLevelType.GetDataByLevelTypeID(Me.GetLevelTypeId)
                    If TableLevelType.Rows.Count > 0 Then
                        Dim TableRowLevelType As AMLDAL.AMLDataSet.LevelTypeRow = TableLevelType.Rows(0)

                        ViewState("LevelTypeID_Old") = TableRowLevelType.LevelTypeID
                        Me.LabelTypeID.Text = ViewState("LevelTypeID_Old")

                        ViewState("LevelTypeName_Old") = TableRowLevelType.LevelTypeName
                        Me.TextLevelTypeName.Text = ViewState("LevelTypeName_Old")

                        ViewState("LevelTypeDescription_Old") = TableRowLevelType.LevelTypeDesc
                        Me.TextLevelTypeDescription.Text = ViewState("LevelTypeDescription_Old")

                        ViewState("LevelTypeCreatedDate_Old") = TableRowLevelType.LevelTypeCreatedDate
                    End If
                End Using
            End Using
        Catch
            Throw
        End Try
    End Sub


    ''' <summary>
    ''' page load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.FillEditData()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    '    Transcope.Complete()
                    'End Using
                End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class