#Region "Imports..."
Option Explicit On
Imports AMLBLL

Imports AMLBLL.ValidateBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports System.Collections.Generic
#End Region

Partial Class MsKecamatan_edit
    Inherits Parent

#Region "Function"

    Private Sub LoadData()
        Dim ObjMsKecamatan As MsKecamatan = DataRepository.MsKecamatanProvider.GetPaged(MsKecamatanColumn.IDKecamatan.ToString & "=" & parID, "", 0, 1, Nothing)(0)
        If ObjMsKecamatan Is Nothing Then Throw New Exception("Data Not Found")
        With ObjMsKecamatan
            SafeDefaultValue = "-"
            HFKotaKab.Value = Safe(.IDKotaKabupaten)
            Dim OKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(.IDKotaKabupaten.GetValueOrDefault)
            If OKotaKab IsNot Nothing Then LBSearchNamaKotaKab.Text = Safe(OKotaKab.NamaKotaKab)

            txtIDKecamatan.Text = Safe(.IDKecamatan)
            txtNamaKecamatan.Text = Safe(.NamaKecamatan)

            chkActivation.Checked = SafeBoolean(.Activation)
            Dim L_objMappingMsKecamatanNCBSPPATK As TList(Of MappingMsKecamatanNCBSPPATK)
            L_objMappingMsKecamatanNCBSPPATK = DataRepository.MappingMsKecamatanNCBSPPATKProvider.GetPaged(MappingMsKecamatanNCBSPPATKColumn.IDKecamatan.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)


            Listmaping.AddRange(L_objMappingMsKecamatanNCBSPPATK)

        End With
    End Sub

    Private Sub BindListMapping()
        LBMapping.DataSource = ListMappingDisplay
        LBMapping.DataBind()
    End Sub

    ''' <summary>
    ''' Validasi men-handle seluruh syarat2 data valid
    ''' data yang lolos dari validasi dipastikan lolos ke database 
    ''' karena menggunakan FillorNothing
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ValidasiControl()
        '========================== validasi popup ref :  KotaKab canot null or empty string ==================================================
        If ObjectAntiNull(HFKotaKab.Value) = False Then Throw New Exception("KotaKab Must Be Filled ")
        '======================== Validasi textbox :  ID canot Null ====================================================
        If ObjectAntiNull(txtIDKecamatan.Text) = False Then Throw New Exception("ID  must be filled  ")
        If Not IsNumeric(Me.txtIDKecamatan.Text) Then
            Throw New Exception("ID must be in number format")
        End If
        '======================== Validasi textbox :  Nama canot Null ====================================================
        If ObjectAntiNull(txtNamaKecamatan.Text) = False Then Throw New Exception("Nama  must be filled  ")

        If DataRepository.MsKecamatanProvider.GetPaged("IDKecamatan = '" & txtIDKecamatan.Text & "' AND IDKecamatan <> '" & parID & "'", "", 0, Integer.MaxValue, 0).Count > 0 Then
            Throw New Exception("ID already exist in the database")
        End If

        If DataRepository.MsKecamatan_ApprovalDetailProvider.GetPaged(MsKecamatan_ApprovalDetailColumn.IDKecamatan.ToString & "='" & txtIDKecamatan.Text & "' AND IDKecamatan <> '" & parID & "'", "", 0, 1, Nothing).Count > 0 Then
            Throw New Exception("Data exist in List Waiting Approval")
        End If
        'If LBMapping.Items.Count = 0 Then Throw New Exception("data tidak punya list Mapping")
    End Sub

#End Region

#Region "events..."
    Protected Sub imgBrowseNameKotaKab_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBrowseNameKotaKab.Click
        Try
            If Session("PickerKotaKab.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
                LBSearchNamaKotaKab.Text = strData(1)
                HFKotaKab.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgBrowse_click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBrowse.Click
        Try
            If Not Get_DataFromPicker Is Nothing Then
                Listmaping.AddRange(Get_DataFromPicker)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgDeleteItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgDeleteItem.Click
        Try
            If LBMapping.SelectedIndex < 0 Then
                Throw New Exception("No data Selected")
            End If
            Listmaping.RemoveAt(LBMapping.SelectedIndex)

        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                'AuditTrailBLL.InsertAuditTrailUserAccess(Sahassa.CommonCTRWeb.Commonly.SessionCurrentPage)
                Listmaping = New TList(Of MappingMsKecamatanNCBSPPATK)
                LoadData()
            Catch ex As Exception
                LogError(ex)
                CvalPageErr.IsValid = False
                CvalPageErr.ErrorMessage = ex.Message
            End Try
        End If

    End Sub

    Protected Sub ImgBtnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnSave.Click
        Try
            Page.Validate("handle")
            If Page.IsValid Then
                ValidasiControl()
                ' =========== Insert Header Approval
                Dim KeyHeaderApproval As Integer
                Using ObjMsKecamatan_Approval As New MsKecamatan_Approval
                    With ObjMsKecamatan_Approval
                        FillOrNothing(.FK_MsMode_Id, 2, True, oInt)
                        FillOrNothing(.RequestedBy, SessionPkUserId)
                        FillOrNothing(.RequestedDate, Date.Now)
                        .IsUpload = False
                    End With
                    DataRepository.MsKecamatan_ApprovalProvider.Save(ObjMsKecamatan_Approval)
                    KeyHeaderApproval = ObjMsKecamatan_Approval.PK_MsKecamatan_Approval_Id
                End Using
                '============ Insert Detail Approval
                Using objMsKecamatan_ApprovalDetail As New MsKecamatan_ApprovalDetail()
                    With objMsKecamatan_ApprovalDetail
                        Dim ObjMsKecamatan As MsKecamatan = DataRepository.MsKecamatanProvider.GetByIDKecamatan(parID)
                        FillOrNothing(.IDKotaKabupaten_Old, ObjMsKecamatan.IDKotaKabupaten)
                        FillOrNothing(.IDKecamatan_Old, ObjMsKecamatan.IDKecamatan)
                        FillOrNothing(.NamaKecamatan_Old, ObjMsKecamatan.NamaKecamatan)
                        FillOrNothing(.Activation_Old, ObjMsKecamatan.Activation)
                        FillOrNothing(.CreatedDate_Old, ObjMsKecamatan.CreatedDate)
                        FillOrNothing(.CreatedBy_Old, ObjMsKecamatan.CreatedBy)
                        FillOrNothing(.FK_MsKecamatan_Approval_Id, KeyHeaderApproval)

                        '==== Edit data =============
                        FillOrNothing(.IDKotaKabupaten, HFKotaKab.Value, True, oInt)
                        FillOrNothing(.IDKecamatan, txtIDKecamatan.Text, True, oInt)
                        FillOrNothing(.NamaKecamatan, txtNamaKecamatan.Text, True, Ovarchar)

                        FillOrNothing(.Activation, chkActivation.Checked, True, oBit)
                        FillOrNothing(.LastUpdatedBy, SessionUserId)
                        FillOrNothing(.LastUpdatedDate, Date.Now)

                    End With
                    DataRepository.MsKecamatan_ApprovalDetailProvider.Save(objMsKecamatan_ApprovalDetail)
                    Dim keyHeaderApprovalDetail As Integer = objMsKecamatan_ApprovalDetail.PK_MsKecamatan_ApprovalDetail_Id
                    '========= Insert mapping item 
                    Dim LobjMappingMsKecamatanNCBSPPATK_Approval_Detail As New TList(Of MappingMsKecamatanNCBSPPATK_Approval_Detail)
                    For Each objMappingMsKecamatanNCBSPPATK As MappingMsKecamatanNCBSPPATK In Listmaping
                        Dim Mapping As New MappingMsKecamatanNCBSPPATK_Approval_Detail
                        With Mapping
                            FillOrNothing(.IDKecamatan, objMsKecamatan_ApprovalDetail.IDKecamatan)
                            FillOrNothing(.IDKecamatanNCBS, objMappingMsKecamatanNCBSPPATK.IDKecamatanNCBS)
                            FillOrNothing(.PK_MsKecamatan_Approval_Id, KeyHeaderApproval)
                            FillOrNothing(.nama, objMappingMsKecamatanNCBSPPATK.Nama)
                            FillOrNothing(.PK_MappingMsKecamatanNCBSPPATK_approval_detail_Id, keyHeaderApprovalDetail)
                            LobjMappingMsKecamatanNCBSPPATK_Approval_Detail.Add(Mapping)
                        End With
                    Next
                    DataRepository.MappingMsKecamatanNCBSPPATK_Approval_DetailProvider.Save(LobjMappingMsKecamatanNCBSPPATK_Approval_Detail)
                    'session Maping item Clear
                    Listmaping.Clear()
                    ImgBtnSave.Visible = False
                    ImgBackAdd.Visible = False
                    LblConfirmation.Text = "Data has been added and waiting for approval"
                    MtvMsUser.ActiveViewIndex = 1
                End Using
            End If
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub



    Protected Sub ImgBackAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBackAdd.Click
        Response.Redirect("MsKecamatan_View.aspx")
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            BindListMapping()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBack.Click
        Response.Redirect("MsKecamatan_View.aspx")
    End Sub
#End Region

#Region "Property..."
    Public ReadOnly Property parID() As String
        Get
            Return Request.Item("ID")
        End Get
    End Property
    ''' <summary>
    ''' Mengambil Data dari Picker
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Get_DataFromPicker() As TList(Of MappingMsKecamatanNCBSPPATK)
        Get
            Dim Temp As New TList(Of MappingMsKecamatanNCBSPPATK)
            Dim L_MsKecamatanNCBS As TList(Of MsKecamatanNCBS)
            Try
                If Session("PickerMsKecamatanNCBS.Data") Is Nothing Then
                    Throw New Exception
                End If
                L_MsKecamatanNCBS = Session("PickerMsKecamatanNCBS.Data")
                For Each i As MsKecamatanNCBS In L_MsKecamatanNCBS
                    Dim Tempmapping As New MappingMsKecamatanNCBSPPATK
                    With Tempmapping
                        .IDKecamatanNCBS = i.IDKecamatanNCBS
                        .Nama = i.NamaKecamatanNCBS
                    End With
                    Temp.Add(Tempmapping)
                Next
            Catch ex As Exception
                L_MsKecamatanNCBS = Nothing
            End Try
            Return Temp
        End Get
    End Property
    ''' <summary>
    ''' Menyimpan Item untuk mapping sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Listmaping() As TList(Of MappingMsKecamatanNCBSPPATK)
        Get
            Return Session("Listmaping.data")
        End Get
        Set(ByVal value As TList(Of MappingMsKecamatanNCBSPPATK))
            Session("Listmaping.data") = value
        End Set
    End Property
    ''' <summary>
    ''' List yang tampil di ListBox
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplay() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingMsKecamatanNCBSPPATK In Listmaping.FindAllDistinct("IDKecamatanNCBS")
                Temp.Add(i.IDKecamatanNCBS.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property
#End Region

End Class


