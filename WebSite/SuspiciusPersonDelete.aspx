<%@ Page Language="VB" MasterPageFile="~/masterpage.master" AutoEventWireup="false" CodeFile="SuspiciusPersonDelete.aspx.vb" Inherits="SuspiciusPersonDelete" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
<table id="title"  border="2" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr>
            <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Suspicius Person- Delete&nbsp;
                    <hr />
                </strong>
                <asp:Label ID="LblSuccess" runat="server" CssClass="validationok" Visible="False"
                    Width="100%"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" colspan="4" style="border-top-style: none; border-right-style: none;
                border-left-style: none; height: 24px; border-bottom-style: none"><span style="color: #ff0000">* Required</span></td>
        </tr>
    </table>	
	<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2" height="72" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
		<tr class="formText">
			<td bgColor="#ffffff" style="width: 24px; height: 24px"><br />
                </td>
			<td width="20%" bgColor="#ffffff" style="height: 24px">
                Name</td>
			<td width="5" bgColor="#ffffff" style="height: 24px">:</td>
			<td width="80%" bgColor="#ffffff" style="height: 24px">
                &nbsp;<asp:Label ID="lblName" runat="server"></asp:Label></td>
		</tr>
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
            </td>
            <td bgcolor="#ffffff" style="height: 24px" width="20%">
                Birth Date</td>
            <td bgcolor="#ffffff" style="height: 24px" width="5">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px" width="80%">
                
                    <asp:Label ID="lblTglLahir" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
            </td>
            <td bgcolor="#ffffff" style="height: 24px" width="20%">
                Birth Place</td>
            <td bgcolor="#ffffff" style="height: 24px" width="5">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px" width="80%">
                <asp:Label ID="lblTempatLahir" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
            </td>
            <td bgcolor="#ffffff" style="height: 24px" width="20%">
                Identity No</td>
            <td bgcolor="#ffffff" style="height: 24px" width="5">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px" width="80%">
                <asp:Label ID="lblIdentityNo" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
            </td>
            <td bgcolor="#ffffff" style="height: 24px" valign="top" width="20%">
                Address</td>
            <td bgcolor="#ffffff" style="height: 24px" valign="top" width="5">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px" valign="top" width="80%">
                <asp:Label ID="lblAddress" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
            </td>
            <td bgcolor="#ffffff" style="height: 24px" width="20%">
                Kecamatan/Kelurahan</td>
            <td bgcolor="#ffffff" style="height: 24px" width="5">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px" width="80%">
                <asp:Label ID="lblKecamatanKelurahan" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
            </td>
            <td bgcolor="#ffffff" style="height: 24px" width="20%">
                Phone No</td>
            <td bgcolor="#ffffff" style="height: 24px" width="5">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px" width="80%">
                <asp:Label ID="lblPhoneNumber" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
            </td>
            <td bgcolor="#ffffff" style="height: 24px" width="20%">
                Job</td>
            <td bgcolor="#ffffff" style="height: 24px" width="5">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px" width="80%">
                <asp:Label ID="lblJob" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
            </td>
            <td bgcolor="#ffffff" style="height: 24px" width="20%">
                Work Place</td>
            <td bgcolor="#ffffff" style="height: 24px" width="5">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px" width="80%">
                <asp:Label ID="lblworkplace" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
            </td>
            <td bgcolor="#ffffff" style="height: 24px" width="20%">
                NPWP</td>
            <td bgcolor="#ffffff" style="height: 24px" width="5">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px" width="80%">
                <asp:Label ID="lblnpwp" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
            </td>
            <td bgcolor="#ffffff" style="height: 24px" width="20%">
                Description</td>
            <td bgcolor="#ffffff" style="height: 24px" width="5">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px" width="80%">
                <asp:Label ID="lbldescription" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
            </td>
            <td bgcolor="#ffffff" style="height: 24px" width="20%">
                Created By</td>
            <td bgcolor="#ffffff" style="height: 24px" width="5">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px" width="80%">
                <asp:Label ID="lblcreatedby" runat="server"></asp:Label></td>
        </tr>

	
		<tr class="formText" bgColor="#dddddd" height="30">
			<td style="width: 24px"><IMG height="15" src="images/arrow.gif" width="15"></td>
			<td colSpan="3" style="height: 9px">
				<table cellSpacing="0" cellPadding="3" border="0">
					<tr>
						<td><asp:imagebutton id="ImageSave" runat="server" CausesValidation="True" SkinID="DeleteButton"></asp:imagebutton></td>
						<td><asp:imagebutton id="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton"></asp:imagebutton></td>
					</tr>
				</table>               
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
		</tr>
        <tr bgcolor="#dddddd" class="formText" height="30">
            <td style="width: 24px">
            </td>
            <td colspan="3" style="height: 9px">
            </td>
        </tr>
	</table>
	
</asp:Content>



