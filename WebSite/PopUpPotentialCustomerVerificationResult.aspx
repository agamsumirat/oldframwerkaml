<%@ Page Language="VB" MasterPageFile="~/CDDLNPPrintMasterPage.master" AutoEventWireup="false"
    CodeFile="PopUpPotentialCustomerVerificationResult.aspx.vb" Inherits="PotentialCustomerVerificationResult" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="a" ContentPlaceHolderID="cpContent" runat="server">
    <ajax:AjaxPanel ID="panel" runat="server" Width="100%">
        <asp:Panel ID="PanelResult" runat="server">

            <table style="width: 100%;">
                <tr>
                    <td colspan="3" style="width: 100%">
                        <strong>Customer Screening - Search Result &nbsp; <asp:Label ID="lblSearching" runat="server" Text="Label"></asp:Label> </strong>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="width: 100%">
                        <asp:GridView ID="GrdResult" runat="server" AllowPaging="True" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" EnableModelValidation="True" ForeColor="Black" GridLines="Vertical" Width="90%" AutoGenerateColumns="False">
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:CommandField SelectText="Detail" ShowSelectButton="True" />
                                <asp:BoundField DataField="ID" HeaderText="ID" Visible="False" />
                                <asp:BoundField DataField="VerificationListID" HeaderText="VerificationListID" />
                                <asp:BoundField DataField="Nama" HeaderText="Nama" />
                                <asp:BoundField DataField="Nationality" HeaderText="Nationality" />
                                <asp:BoundField DataField="DOB" HeaderText="DOB" />
                                <asp:BoundField DataField="YOB" HeaderText="YOB" />
                                <asp:BoundField DataField="percenmatch" HeaderText="PercentMatch" />
                                
                            </Columns>
                            <FooterStyle BackColor="#CCCC99" />
                            <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                            <RowStyle BackColor="#F7F7DE"   />
                            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />                                            
                        </asp:GridView>
                    </td>
                </tr>
                </table>
            <table style="width: 100%;" >
                <tr>
                    <td style="width: 10px">&nbsp;</td>
                    <td style="width: 49%">&nbsp;</td>
                    <td style="width: 49%">&nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10px">&nbsp;</td>
                    <td style="width: 49%"><strong>Searching Data</strong></td>
                    <td style="width: 49%"><strong>Match With</strong></td>
                </tr>
                <tr>
                    <td style="width: 10px">&nbsp;</td>
                    <td style="width: 49%" valign="top">
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 70px">Name</td>
                                <td style="width: 10px">:</td>
                                <td style="width: 90%">
                                    <asp:Label ID="LblName" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70px">DOB</td>
                                <td>:</td>
                                <td>
                                    <asp:Label ID="LblDOB" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 70px">Nationality</td>
                                <td>:</td>
                                <td>
                                    <asp:Label ID="LblNationality" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 49%">
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 150px">Nama</td>
                                <td style="width: 5px">:</td>
                                <td style="width: 70%">
                                    <asp:Label ID="LblNameWatchList" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 150px">DOB</td>
                                <td style="width: 5px">:</td>
                                <td style="width: 70%">
                                    <asp:Label ID="LblDOBWatchList" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 150px">YOB</td>
                                <td style="width: 5px">:</td>
                                <td style="width: 70%">
                                    <asp:Label ID="LblYOBWatchlist" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 150px">Nationality</td>
                                <td style="width: 5px">:</td>
                                <td style="width: 70%">
                                    <asp:Label ID="LblNationalityWatchList" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 150px">Birth Place</td>
                                <td style="width: 5px">:</td>
                                <td style="width: 70%">
                                    <asp:Label ID="LblBirthPlaceWatchList" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 150px">List Type</td>
                                <td style="width: 5px">:</td>
                                <td style="width: 70%">
                                    <asp:Label ID="LblListTypeWatchList" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 150px">Category Type</td>
                                <td style="width: 5px">:</td>
                                <td style="width: 70%">
                                    <asp:Label ID="LblCategoryType" runat="server"></asp:Label>
                                    &nbsp;<asp:LinkButton ID="linkcategory" runat="server" Visible="False"></asp:LinkButton>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 150px">Custom Remark 1</td>
                                <td style="width: 5px">:</td>
                                <td style="width: 70%">
                                    <asp:Label ID="LblCustomRemarks1" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 150px">Custom Remark 2</td>
                                <td style="width: 5px">:</td>
                                <td style="width: 70%">
                                    <asp:Label ID="LblCustomRemarks2" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 150px">Custom Remaks 3</td>
                                <td style="width: 5px">:</td>
                                <td style="width: 70%">
                                    <asp:Label ID="LblCustomRemarks3" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 150px">Custom Remaks 4</td>
                                <td style="width: 5px">:</td>
                                <td style="width: 70%">
                                    <asp:Label ID="LblCustomRemarks4" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 150px">Custom Remaks 5</td>
                                <td style="width: 5px">:</td>
                                <td style="width: 70%">
                                    <asp:Label ID="LblCustomRemarks5" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 150px">
                                    &nbsp;</td>
                                <td style="width: 5px">&nbsp;</td>
                                <td style="width: 70%">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="formText" bgColor="#dddddd" >
                    <td style="width: 15px">
                        <img height="15" src="images/arrow.gif" width="15"> </img>&nbsp;</td>
                    <td colspan="2" style="width:90%">
                        <asp:ImageButton ID="ImageBack" runat="server" CausesValidation="False" 
                                         SkinID="BackButton" OnClientClick="javascript:window.close();opener.PostBackOnMainPage();" />
                    </td>
                    <td style="width: 7px">
                        <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator>
                    </td>
                </tr>
            </table>
        </asp:Panel>

    </ajax:AjaxPanel>
</asp:Content>
