
Partial Class CaseManagementConfirmationNumberViewDetail
    Inherits Parent
#Region "Property"

    Private ReadOnly Property PK_CaseManagementID() As String
        Get
            Dim temp As String
            temp = Request.Params("PK_CaseManagementID")
            If temp = "" OrElse Not IsNumeric(temp) Then
                Throw New Exception("PKManagementID is not valid")
            Else
                Return temp
            End If
        End Get
    End Property

    Private _oRowCaseManagement As AMLDAL.CaseManagement.SelectCaseManagementRow
    Private _getCIFtoPrint As String
    Private _PKCaseManagementTransactionID As String
    Private ReadOnly Property getCIFtoPrint() As String
        Get
            If _getCIFtoPrint = "" Then
                Using adapter As New AMLDAL.CaseManagementTableAdapters.MapCaseManagementTransactionTableAdapter
                    Using otables As AMLDAL.CaseManagement.MapCaseManagementTransactionDataTable = adapter.GetMapCaseManagementTransactionByFK(oRowCaseManagement.PK_CaseManagementID)
                        If otables.Rows.Count > 0 Then
                            Dim orows As AMLDAL.CaseManagement.MapCaseManagementTransactionRow = otables.Rows(0)
                            _PKCaseManagementTransactionID = orows.PK_MapCaseManagementTransactionID
                            If Not orows.IsCIFNoNull Then
                                _getCIFtoPrint = orows.CIFNo
                                Return _getCIFtoPrint
                            Else
                                _getCIFtoPrint = ""
                                Return _getCIFtoPrint
                            End If
                        Else
                            _getCIFtoPrint = ""
                            Return _getCIFtoPrint
                        End If
                    End Using
                End Using
            Else
                Return _getCIFtoPrint
            End If
        End Get
    End Property
    Private ReadOnly Property oRowCaseManagement() As AMLDAL.CaseManagement.SelectCaseManagementRow
        Get
            Try
                If Not _oRowCaseManagement Is Nothing Then
                    Return _oRowCaseManagement
                Else
                    Using adapter As New AMLDAL.CaseManagementTableAdapters.SelectCaseManagementTableAdapter

                        Using oTable As AMLDAL.CaseManagement.SelectCaseManagementDataTable = adapter.GetVw_CaseManagementByPK(Me.PK_CaseManagementID)
                            If oTable.Rows.Count > 0 Then
                                _oRowCaseManagement = oTable.Rows(0)
                                Return _oRowCaseManagement
                            Else
                                Throw New Exception("There is no data for PKManagementID " & Me.PK_CaseManagementID)
                            End If
                        End Using
                    End Using
                End If

            Catch ex As Exception
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            End Try
        End Get

    End Property
#End Region
    Private Sub LoadData()
        Try
            LabelCaseNumber.Text = Me.oRowCaseManagement.PK_CaseManagementID
            If Not oRowCaseManagement.IsCaseDescriptionNull Then
                LabelDescription.Text = Me.oRowCaseManagement.CaseDescription
            End If
            If Not oRowCaseManagement.IsCaseDescriptionNull Then
                LabelStatus.Text = Me.oRowCaseManagement.CaseStatusDescription
            End If
            If Not oRowCaseManagement.IsCreatedDateNull Then
                LabelCreatedDate.Text = Me.oRowCaseManagement.CreatedDate.ToString("dd MMM yyyy")
            End If
            If Not oRowCaseManagement.IsLastUpdatedNull Then
                LabelLastUpdatedDate.Text = Me.oRowCaseManagement.LastUpdated.ToString("dd MMM yyyy")
            End If
            If Not oRowCaseManagement.IsProposedByNull Then
                LabelProposedBy.Text = Me.oRowCaseManagement.ProposedBy
            End If

            If Not oRowCaseManagement.IsAccountOwnerNameNull Then
                LabelAccountOwnerName.Text = Me.oRowCaseManagement.AccountOwnerName
            End If

            If Not oRowCaseManagement.IsPICNull Then
                LabelPIC.Text = Me.oRowCaseManagement.PIC
            End If
            If Not oRowCaseManagement.IsHasOpenIssueNull Then
                LabelHasOpenIssue.Text = Me.oRowCaseManagement.HasOpenIssue
            End If
            If Not oRowCaseManagement.IsProposedActionNull Then
                LabelLastProposedAction.Text = Me.oRowCaseManagement.ProposedAction
            End If
            If Not oRowCaseManagement.IsIsReportedtoRegulatorNull Then
                LabelReportedtoRegulator.Text = Me.oRowCaseManagement.IsReportedtoRegulator.ToString
                CheckBoxReported.Checked = Me.oRowCaseManagement.IsReportedtoRegulator
                
            Else
                CheckBoxReported.Checked = False
                LabelReportedtoRegulator.Text = "False"
            End If
            If Not oRowCaseManagement.IsPPATKConfirmationnoNull Then
                LabelConfirmationNo.Text = Me.oRowCaseManagement.PPATKConfirmationno
                TextBoxConfirmationNo.Text = Me.oRowCaseManagement.PPATKConfirmationno
            Else
                TextBoxConfirmationNo.Text = ""
            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub
    Protected Sub ImageButtonSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSave.Click
        Try

            If Page.IsValid Then
                Using adapter As New AMLDAL.CaseManagementTableAdapters.SelectCaseManagementTableAdapter
                    adapter.UpdateCaseManagementRegulatorConfirmationNo(CheckBoxReported.Checked, TextBoxConfirmationNo.Text.Trim, Me.PK_CaseManagementID, Sahassa.AML.Commonly.SessionUserId)

                    Response.Redirect("CaseManagementConfirmationNumberView.aspx", False)
                End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        Try
            Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)


            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)


            End Using
        Catch ex As Exception

        End Try
        
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            LoadData()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try

    End Sub

    Protected Sub ImagePrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImagePrint.Click
        Response.Redirect("PrintSTR.aspx?CIFNO=" & Me.getCIFtoPrint & "&PKCaseManagementID=" & PK_CaseManagementID & "&PKCaseManagementTransactionID=" & Me._PKCaseManagementTransactionID & "&asal=1", False)
    End Sub
End Class
