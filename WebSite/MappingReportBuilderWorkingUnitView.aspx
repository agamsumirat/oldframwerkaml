<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="MappingReportBuilderWorkingUnitView.aspx.vb" Inherits="MappingReportBuilderWorkingUnitView"  %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">


<table id="title"  border="2" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr>

            <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Mapping Report&nbsp; Builder Working Unit &nbsp;&nbsp; - View
                    <hr />
                </strong>
            </td>
        </tr>
    </table>
    <TABLE borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2">
        <TR>
            <TD align="left" bgColor="#eeeeee"><IMG height="15" src="images/arrow.gif" width="15"></TD>
				<TD vAlign="top" width="98%" bgColor="#ffffff"><TABLE cellSpacing="4" cellPadding="0" width="100%" border="0">
						<TR>
							<TD class="Regtext" noWrap>Search By :
							</TD>
							<TD vAlign="middle" noWrap><ajax:ajaxpanel id="AjaxPanel1" runat="server">
                                &nbsp; <TABLE cellSpacing="1" cellPadding="2" width="100%"
					border="0" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
                                    <tr>
                                        <TD style="width: 15%; height: 22px; background-color: #fff7e6" nowrap="noWrap">
                                            <asp:Label id="subject" runat="server" 
                                                meta:resourcekey="LblGroupMenuNameResource1" Text="Query Name"></asp:Label>
                                        </td>
                                        <td nowrap="nowrap" style="width: 367px; height: 22px; background-color: #fff7e6"><asp:TextBox id="TxtQueryName" tabIndex=2 runat="server" CssClass="searcheditbox" Width="200px" meta:resourcekey="TxtGroupMenuNameResource1" MaxLength="100"></asp:TextBox> </td>
                                    </tr>
                                     <tr>
                                        <TD style="width: 15%; height: 22px; background-color: #fff7e6" nowrap="noWrap">
                                            <asp:Label id="Label1" runat="server" meta:resourcekey="LblGroupMenuNameResource1" Text="Working Unit"></asp:Label>
                                        </td>
                                        <td nowrap="nowrap" style="width: 367px; height: 22px; background-color: #fff7e6"><asp:TextBox id="TxtWorkingunit" tabIndex=2 runat="server" CssClass="searcheditbox" Width="200px" meta:resourcekey="TxtGroupMenuNameResource1" MaxLength="100"></asp:TextBox> </td>
                                    </tr>
                                    
                                </table>
                            </ajax:ajaxpanel></TD>
							<TD vAlign="middle" width="99%"><ajax:ajaxpanel id="AjaxPanel2" runat="server">
									<asp:imagebutton id="ImageButtonSearch" tabIndex="3" runat="server" SkinID="SearchButton"></asp:imagebutton>
                                <asp:ImageButton ID="ImageButtonSearchCancel" runat="server"
                                    TabIndex="3" ImageUrl="~/Images/button/clearsearch.gif" /></ajax:ajaxpanel>
								</TD>
						</TR>
					</TABLE><ajax:ajaxpanel id="AjaxPanel3" runat="server">
                    <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></ajax:ajaxpanel></TD>
			</TR>
	</TABLE>
	
<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2">
  <tr> 
    <td bgColor="#ffffff">
        <ajax:AjaxPanel ID="AjaxPanel14"
                runat="server" Width="672px"><asp:DataGrid ID="GridRBWU" runat="server" AllowPaging="True"
                    AllowSorting="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#DEDFDE"
                    BorderStyle="None" BorderWidth="1px" CellPadding="4" Font-Size="XX-Small" ForeColor="Black"
                    GridLines="Vertical" Width="100%" AllowCustomPaging="True">
                    <FooterStyle BackColor="#CCCC99" />
                    <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages"
                        Visible="False" />
                    <AlternatingItemStyle BackColor="White" />
                    <ItemStyle BackColor="#F7F7DE" Font-Bold="False" Font-Italic="False" 
                        Font-Overline="False" Font-Strikeout="False" Font-Underline="False" 
                        VerticalAlign="Top" />
                    <Columns>
                        <asp:BoundColumn HeaderText="No">
                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                Font-Underline="False" ForeColor="#FFFFFF" />
                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                Font-Strikeout="False" Font-Underline="False" VerticalAlign="Top" />
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="PK_MappingReportBuilderWorkingUnit_ID" Visible="False">
                            <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                Font-Underline="False" Wrap="False" />
                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                Font-Strikeout="False" Font-Underline="False" VerticalAlign="Top" />
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="QueryName" HeaderText="Query Name" 
                            SortExpression="QueryName desc">
                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                Font-Strikeout="False" Font-Underline="False" VerticalAlign="Top" />
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Working Unit">
                            <ItemTemplate>
                                <asp:DataGrid ID="gridView" runat="server" AllowCustomPaging="True" AllowPaging="True"
                                    AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" CellSpacing="1"
                                    Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Size="XX-Small"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="#333333" GridLines="None"
                                    HorizontalAlign="Left" ShowHeader="False" SkinID="gridview" Width="97%">
                                    <FooterStyle BackColor="#CCCC99" />
                                    <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages"
                                        Visible="False" />
                                    <AlternatingItemStyle BackColor="White" />
                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Top" />
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="No." Visible="False">
                                            <EditItemTemplate>
                                                <asp:TextBox runat="server"></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="LabelNo" runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="WorkingUnitName" HeaderText="Working Unit">
                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                Font-Underline="False" ForeColor="Black" />
                                        </asp:BoundColumn>
                                    </Columns>
                                    <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                        Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Top" Width="25%"
                                        Wrap="False" />
                                </asp:DataGrid>
                            </ItemTemplate>
                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                Font-Underline="False" ForeColor="White" />
                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                Font-Strikeout="False" Font-Underline="False" VerticalAlign="Top" />
                        </asp:TemplateColumn>
                        <asp:EditCommandColumn EditText="Edit">
                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                Font-Strikeout="False" Font-Underline="False" VerticalAlign="Top" />
                        </asp:EditCommandColumn>
                        <asp:ButtonColumn CommandName="Delete" Text="Delete">
                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                Font-Strikeout="False" Font-Underline="False" VerticalAlign="Top" />
                        </asp:ButtonColumn>
                    </Columns>
                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" Font-Size="XX-Small" ForeColor="White" />
                </asp:DataGrid>
            </ajax:AjaxPanel>
    </td>
  </tr>
  <tr> 
    <td style="background-color:#ffffff"><table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td nowrap>
                &nbsp;</td>
			 <td width="99%">&nbsp;&nbsp;</td>
			<td align="right" nowrap><asp:LinkButton ID="LinkButtonAddNew" runat="server">Add 
	        New</asp:LinkButton>&nbsp;&nbsp;</td>
		</tr>
      </table></td>
  </tr>
  <tr> 
    <td bgColor="#ffffff"> <TABLE id="Table3" borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#ffffff"
					border="2">
        <TR class="regtext" align="center" bgColor="#dddddd"> 
          <TD vAlign="top" align="left" width="50%" bgColor="#ffffff">Page&nbsp;<ajax:ajaxpanel id="AjaxPanel6" runat="server"> 
            <asp:label id="PageCurrentPage" runat="server" CssClass="regtext">0</asp:label>
            &nbsp;of&nbsp; 
            <asp:label id="PageTotalPages" runat="server" CssClass="regtext">0</asp:label>
            </ajax:ajaxpanel></TD>
          <TD vAlign="top" align="right" width="50%" bgColor="#ffffff">Total Records&nbsp; 
            <ajax:ajaxpanel id="AjaxPanel7" runat="server"> 
            <asp:label id="PageTotalRows" runat="server">0</asp:label>
            </ajax:ajaxpanel></TD>
        </TR>
      </TABLE>
      <TABLE id="Table4" borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#ffffff"
					border="2">
        <TR bgColor="#ffffff"> 
          <TD class="regtext" vAlign="middle" align="left" colSpan="11" height="7"> 
            <HR color="#f40101" noShade SIZE="1"> </TD>
        </TR>
        <TR> 
          <TD class="regtext" vAlign="middle" align="left" width="63" bgColor="#ffffff">Go 
            to page</TD>
          <TD class="regtext" vAlign="middle" align="left" width="5" bgColor="#ffffff"><FONT face="Verdana, Arial, Helvetica, sans-serif" size="1"> 
            <ajax:ajaxpanel id="AjaxPanel8" runat="server"> 
            <asp:textbox id="TextGoToPage" runat="server" Width="38px" CssClass="searcheditbox"></asp:textbox>
            </ajax:ajaxpanel> </FONT></TD>
          <TD class="regtext" vAlign="middle" align="left" bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel9" runat="server"> 
            <asp:imagebutton id="ImageButtonGo" runat="server" SkinID="GoButton"></asp:imagebutton>
            </ajax:ajaxpanel> </TD>
          <TD class="regtext" vAlign="middle" align="left" width="6" bgColor="#ffffff"><IMG height="5" src="images/first.gif" width="6"> 
          </TD>
          <TD class="regtext" vAlign="middle" align="right" width="6" bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel10" runat="server"> 
            <asp:linkbutton id="LinkButtonFirst" runat="server" CssClass="regtext" CommandName="First" OnCommand="PageNavigate">First</asp:linkbutton>
            </ajax:ajaxpanel> </TD>
          <TD class="regtext" vAlign="middle" align="right" width="6" bgColor="#ffffff"><IMG height="5" src="images/prev.gif" width="6"></TD>
          <TD class="regtext" vAlign="middle" align="right" width="14" bgColor="#ffffff"> 
            <ajax:ajaxpanel id="AjaxPanel11" runat="server"> 
            <asp:linkbutton id="LinkButtonPrevious" runat="server" CssClass="regtext" CommandName="Prev" OnCommand="PageNavigate">Previous</asp:linkbutton>
            </ajax:ajaxpanel> </TD>
          <TD class="regtext" vAlign="middle" align="right" width="60" bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel12" runat="server"><A class="pageNav" href="#"> 
            <asp:linkbutton id="LinkButtonNext" runat="server" CssClass="regtext" CommandName="Next" OnCommand="PageNavigate">Next</asp:linkbutton>
            </A></ajax:ajaxpanel></TD>
          <TD class="regtext" vAlign="middle" align="left" width="6" bgColor="#ffffff"><IMG height="5" src="images/next.gif" width="6"></TD>
          <TD class="regtext" vAlign="middle" align="left" width="25" bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel13" runat="server"> 
            <asp:linkbutton id="LinkButtonLast" runat="server" CssClass="regtext" CommandName="Last" OnCommand="PageNavigate">Last</asp:linkbutton>
            </ajax:ajaxpanel> </TD>
          <TD class="regtext" vAlign="middle" align="left" width="6" bgColor="#ffffff"><IMG height="5" src="images/last.gif" width="6"></TD>
        </TR>
      </TABLE></td>
  </tr>
</table>

</asp:Content>




