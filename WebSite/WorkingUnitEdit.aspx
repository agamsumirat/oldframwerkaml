<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="WorkingUnitEdit.aspx.vb" Inherits="WorkingUnitEdit" title="Working Unit Edit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
	<table id="title"  border="2" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr>
            <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Working Unit - Edit&nbsp;
                    <hr />
                </strong>
                <asp:Label ID="LblSuccess" runat="server" CssClass="validationok" Visible="False"
                    Width="100%"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" colspan="4" style="border-top-style: none; border-right-style: none;
                border-left-style: none; height: 24px; border-bottom-style: none"><span style="color: #ff0000">* Required</span></td>
        </tr>
    </table>	
	<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2" height="72" id="TABLE1">      
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; border-top-style: none; border-right-style: none;
                border-left-style: none; height: 24px; text-align: center; border-bottom-style: none">
            </td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none;
                height: 24px; border-bottom-style: none" width="23%">
                Working Unit ID</td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none;
                height: 24px; border-bottom-style: none">
                :</td>
            <td bgcolor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none;
                height: 24px; border-bottom-style: none" width="80%">
                <asp:Label ID="LabelWorkingUnitID" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; text-align: center; border-bottom-style: none;">
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" ControlToValidate="TextWorkingUnitName"
                    ErrorMessage="Working Unit Name is required">*</asp:RequiredFieldValidator></td>
            <td bgcolor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;" width="23%">
                Working Unit Name</td>
            <td bgcolor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;" width="80%">
            <asp:textbox id="TextWorkingUnitName" runat="server" CssClass="textBox" MaxLength="50" Width="190px"></asp:textbox>
                <strong><span style="color: #ff0000">*</span></strong></td>
        </tr>
		<tr class="formText">
			<td bgColor="#ffffff" style="width: 24px; height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; text-align: center; border-bottom-style: none;">&nbsp;</td>
			<td width="23%" bgColor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
                Level Type Name</td>
			<td bgColor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">:</td>
			<td width="77%" bgColor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;"><asp:DropDownList ID="DropDownLevelTypeName" runat="server">
            </asp:DropDownList>
                <strong><span style="color: #ff0000">*</span></strong></td>
		</tr>
        <tr class="formText">
            <td bgcolor="#ffffff" rowspan="1" style="width: 24px; height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; text-align: center; border-bottom-style: none;">
                </td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
                Working Unit Parent</td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
                :</td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
                <asp:DropDownList ID="DropDownWorkingUnitParent" runat="server">
                </asp:DropDownList>
                <strong><span style="color: #ff0000">* </span></strong>
            </td>
        </tr>
		<tr class="formText">
			<td bgColor="#ffffff" style="width: 24px; height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; text-align: center; border-bottom-style: none;" rowspan="6">&nbsp;</td>
			<td bgColor="#ffffff" rowspan="6" style="height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
                Description<br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" rowspan="6" style="height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
                :<br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" rowspan="6" style="height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
                <asp:textbox id="TextWorkingUnitDescription" runat="server" CssClass="textBox" MaxLength="255" Width="200px" Height="63px" TextMode="MultiLine"></asp:textbox></td>
		</tr>
		<tr class="formText">
		</tr>
		<tr class="formText">
		</tr>
		<tr class="formText">
		</tr>
		<tr class="formText">
		</tr>
		<tr class="formText">
		</tr>
		<tr class="formText" bgColor="#dddddd" height="30">
			<td style="width: 24px"><IMG height="15" src="images/arrow.gif" width="15"></td>
			<td colSpan="3" style="height: 9px">
				<table cellSpacing="0" cellPadding="3" border="0">
					<tr>
						<td><asp:imagebutton id="ImageUpdate" runat="server" CausesValidation="True" SkinID="UpdateButton"></asp:imagebutton></td>
						<td><asp:imagebutton id="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton"></asp:imagebutton></td>
					</tr>
				</table>               
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
		</tr>
	</table>
	
</asp:Content>

