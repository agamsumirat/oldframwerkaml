<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ProposalSTRWorkflowSettingsApprovalDetail.aspx.vb" Inherits="ProposalSTRWorkflowSettingsApprovalDetail" MasterPageFile="~/MasterPage.master" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="ContentApprovalDetail" runat="server" ContentPlaceHolderID="cpContent">
<table id="title"  border="2" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr>
            <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Proposal STR Workflow Settings Approval - Detail
                    <hr />
                </strong>
                </td>
        </tr>
    </table>	
	<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2" height="72" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
		<tr class="formText">
			<td width="15%" bgColor="#ffffff">
                </td>
			<td width="1%" bgColor="#ffffff"></td>
			<td width="80%" bgColor="#ffffff">
                </td>
		</tr>
		<tr class="formText">
            <td bgcolor="#ffffff" colspan="3" rowspan="4" style="height: 24px">
				<ajax:ajaxpanel id="AjaxPanel1" runat="server">
                <asp:MultiView ID="MultiViewApprovalDetail" runat="server">
                    <asp:View ID="ViewADD" runat="server">
                    <table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
		<tr class="formText">
			<td bgColor="whitesmoke" colspan="4">
                &nbsp;<strong>New Value</strong></td>
		</tr>
                        <tr class="formText">
                            <td bgcolor="#ffffff" colspan="4">
                                <asp:GridView ID="GridViewAddApproval_General" runat="server" AutoGenerateColumns="False"
                                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px"
                                    CellPadding="4" ForeColor="Black" GridLines="Vertical" Width="100%">
                                    <FooterStyle BackColor="#CCCC99" />
                                    <Columns>
                                        <asp:BoundField DataField="SerialNo_Approval" HeaderText="Serial #" />
                                        <asp:BoundField DataField="WorkflowStep_Approval" HeaderText="Workflow Step" />
                                        <asp:BoundField DataField="Paralel_Approval" HeaderText="# Of Paralel" />
                                        <asp:TemplateField HeaderText="Approvers">
                                            <ItemTemplate>
                                                <asp:GridView ID="GridViewApprovers" runat="server" AutoGenerateColumns="False" CellPadding="4"  OnRowDataBound="GridViewApprovers_RowDataBound"
                                                    ShowHeader="False" BorderWidth="0px">
                                                    <Columns>
                                                        <asp:BoundField DataField="ApproversDetail_Approval" >
                                                        </asp:BoundField>
                                                    </Columns>
                                                    <RowStyle HorizontalAlign="Center" />
                                                </asp:GridView>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="DueDate(Days)">
                                            <ItemTemplate>
                                                <asp:GridView ID="GridViewDueDate" runat="server" AutoGenerateColumns="False"
                                                     BorderStyle="None"  CellPadding="4" ShowHeader="False" BorderWidth="0px">
                                                    <Columns>
                                                        <asp:BoundField DataField="DueDateDetail_Approval" >
                                                        </asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Notify Others">
                                            <ItemTemplate>
                                                <asp:GridView ID="GridViewNotifyOthers" runat="server" AutoGenerateColumns="False"  BorderStyle="None" 
                                                    CellPadding="4" ShowHeader="False" OnRowDataBound="GridViewNotifyOthers_RowDataBound" BorderWidth="0px">
                                                    <Columns>
                                                        <asp:BoundField DataField="NotifyOthersDetail_Approval" >
                                                        </asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Email Template">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="LinkEmail" runat="server" OnClick="LinkEmail_Click">Email Template</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle HorizontalAlign="Center" VerticalAlign="Top" BackColor="#F7F7DE" />
                                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                    <AlternatingRowStyle BackColor="White" />
                                </asp:GridView>
                            </td>
                        </tr>
		</table>
                    </asp:View>
                    <asp:View ID="ViewEdit" runat="server">
                    <table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
                        <tr class="formText">
                            <td bgcolor="whitesmoke" colspan="2" width="44%" style="height: 19px">
                                <strong>Old Value</strong></td>
                            <td width="2%" bgColor="whitesmoke" style="height: 19px">
                            </td>
                            <td width="44%" bgColor="whitesmoke" style="height: 19px">
                                <strong>New Value</strong></td>
                        </tr>
                        <tr class="formText">
                            <td bgcolor="#ffffff" colspan="2" valign="top">
                                <asp:GridView ID="GridViewEdit_Old" runat="server" AutoGenerateColumns="False"
                                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px"
                                    CellPadding="4" ForeColor="Black" GridLines="Vertical" Width="100%">
                                    <FooterStyle BackColor="#CCCC99" />
                                    <Columns>
                                        <asp:BoundField DataField="SerialNo" HeaderText="Serial #" />
                                        <asp:BoundField DataField="WorkflowStep" HeaderText="Workflow Step" />
                                        <asp:BoundField DataField="Paralel" HeaderText="# Of Paralel" />
                                        <asp:TemplateField HeaderText="Approvers">
                                            <ItemTemplate>
                                                <asp:GridView ID="GridViewEditApprovers_Old" runat="server" AutoGenerateColumns="False" BorderStyle="None" CellPadding="4"  OnRowDataBound="GridViewApprovers_RowDataBound" BorderWidth="0px"
                                                    ShowHeader="False">
                                                    <Columns>
                                                        <asp:BoundField DataField="Approvers" >
                                                        </asp:BoundField>
                                                    </Columns>
                                                    <RowStyle HorizontalAlign="Left" />
                                                </asp:GridView>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="DueDate(Days)">
                                            <ItemTemplate>
                                                <asp:GridView ID="GridViewEditDueDate_Old" runat="server" AutoGenerateColumns="False" BorderStyle="None"  CellPadding="4" BorderWidth="0px" ShowHeader="False">
                                                    <Columns>
                                                        <asp:BoundField DataField="DueDate" >
                                                        </asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Notify Others">
                                            <ItemTemplate>
                                                <asp:GridView ID="GridViewEditNotifyOthers_Old" runat="server" AutoGenerateColumns="False" BorderStyle="None" BorderWidth="0px" 
                                                    CellPadding="4" ShowHeader="False" OnRowDataBound="GridViewNotifyOthers_RowDataBound">
                                                    <Columns>
                                                        <asp:BoundField DataField="NotifyOthers" >
                                                        </asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="LinkButtonEmailTemplateOld" runat="server" OnClick="LinkButtonEmailTemplateOld_Click">Email Template</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle HorizontalAlign="Center" VerticalAlign="Top" BackColor="#F7F7DE" />
                                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                    <AlternatingRowStyle BackColor="White" />
                                </asp:GridView>
                            </td>
                            <td bgcolor="#ffffff" width="5">
                            </td>
                            <td bgcolor="#ffffff" width="80%" valign="top">
                                <asp:GridView ID="GridViewEdit_New" runat="server" AutoGenerateColumns="False"
                                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px"
                                    CellPadding="4" ForeColor="Black" GridLines="Vertical" Width="100%">
                                    <FooterStyle BackColor="#CCCC99" />
                                    <Columns>
                                        <asp:BoundField DataField="SerialNo_Approval" HeaderText="Serial #" />
                                        <asp:BoundField DataField="WorkflowStep_Approval" HeaderText="Workflow Step" />
                                        <asp:BoundField DataField="Paralel_Approval" HeaderText="# Of Paralel" />
                                        <asp:TemplateField HeaderText="Approvers">
                                            <ItemTemplate>
                                                <asp:GridView ID="GridViewApprovers" runat="server" AutoGenerateColumns="False" BorderStyle="None"  CellPadding="4" BorderWidth="0px"  OnRowDataBound="GridViewApprovers_RowDataBound"
                                                    ShowHeader="False">
                                                    <Columns>
                                                        <asp:BoundField DataField="ApproversDetail_Approval">                                                        
                                                        </asp:BoundField>
                                                    </Columns>
                                                    <RowStyle HorizontalAlign="Left" />
                                                </asp:GridView>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="DueDate(Days)">
                                            <ItemTemplate>
                                                <asp:GridView ID="GridViewDueDate" runat="server" AutoGenerateColumns="False" BorderStyle="None"  CellPadding="4" BorderWidth="0px" ShowHeader="False">
                                                    <Columns>
                                                        <asp:BoundField DataField="DueDateDetail_Approval" >
                                                        </asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Notify Others">
                                            <ItemTemplate>
                                                <asp:GridView ID="GridViewNotifyOthers" runat="server" AutoGenerateColumns="False" BorderStyle="None"  BorderWidth="0px"
                                                    CellPadding="4" ShowHeader="False" OnRowDataBound="GridViewNotifyOthers_RowDataBound">
                                                    <Columns>
                                                        <asp:BoundField DataField="NotifyOthersDetail_Approval" >
                                                        </asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="LinkButtonEmailTemplateNew" runat="server" OnClick="LinkButtonEmailTemplateNew_Click">Email Template</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle HorizontalAlign="Center" VerticalAlign="Top" BackColor="#F7F7DE" />
                                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                    <AlternatingRowStyle BackColor="White" />
                                </asp:GridView>
                            </td>
                        </tr>
		</table>
                    </asp:View>
                </asp:MultiView>
                <asp:MultiView ID="MultiViewEmailTemplate" runat="server">
                    <asp:View ID="ViewEmailTemplateAdd" runat="server">
                        <table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
                            <tr class="formText">
                                <td bgcolor="whitesmoke" colspan="4">
                                    <strong>Email Template &nbsp;&nbsp; </strong>
                                </td>
                            </tr>
                            <tr class="formText">
                                <td bgcolor="#ffffff" colspan="2" style="height: 1px" width="5%">
                                    Subject</td>
                                <td bgcolor="#ffffff" width="1%" style="height: 1px">
                                    :</td>
                                <td bgcolor="#ffffff" width="80%" style="height: 1px">
                                    <asp:Label ID="LblSubjectAdd" runat="server"></asp:Label></td>
                            </tr>
                            <tr class="formText">
                                <td bgcolor="#ffffff" colspan="2" style="height: 114px" valign="top">
                                    Body</td>
                                <td bgcolor="#ffffff" style="height: 114px" valign="top" width="5">
                                    :</td>
                                <td bgcolor="#ffffff" valign="top" width="80%">
                                    <asp:TextBox ID="TxtBodyAdd" runat="server" Height="100px" TextMode="MultiLine" Width="350px"></asp:TextBox></td>
                            </tr>
                            <tr class="formText">
                                <td bgcolor="#ffffff" colspan="4" valign="top">
                                    <asp:LinkButton ID="LinkHideEmailTemplateAdd" runat="server">Hide</asp:LinkButton></td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="ViewEmailTemplateEdit" runat="server">
                        <table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
                            <tr class="formText">
                                <td bgcolor="whitesmoke" colspan="2" width="49%">
                                    <strong>Old Email Template</strong></td>
                                <td width="2%" bgColor="whitesmoke">
                                </td>
                                <td width="49%" bgColor="whitesmoke">
                                    <strong>New Email Template</strong></td>
                            </tr>
                            <tr class="formText">
                                <td bgcolor="#ffffff" colspan="2">
                                    <table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="90%" bgColor="#dddddd"
		border="2" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
                                        <tr class="formText">
                                            <td bgcolor="#ffffff" colspan="2" width="5%">
                                                Subject</td>
                                            <td bgcolor="#ffffff" width="1%">
                                                :</td>
                                            <td bgcolor="#ffffff" width="80%">
                                                <asp:Label ID="LblSubjectEdit_Old" runat="server"></asp:Label></td>
                                        </tr>
                                        <tr class="formText">
                                            <td bgcolor="#ffffff" colspan="2" style="height: 114px" valign="top">
                                                Body</td>
                                            <td bgcolor="#ffffff" style="height: 114px" valign="top" width="5">
                                                :</td>
                                            <td bgcolor="#ffffff" valign="top" width="80%">
                                                <asp:TextBox ID="TxtBodyEdit_Old" runat="server" Height="100px" TextMode="MultiLine"
                                                    Width="350px"></asp:TextBox></td>
                                        </tr>
                                    </table>
                                </td>
                                <td bgcolor="#ffffff" width="5">
                                </td>
                                <td bgcolor="#ffffff" width="80%">
                                    <table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="90%" bgColor="#dddddd"
		border="2" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
                                        <tr class="formText">
                                            <td bgcolor="#ffffff" colspan="2" width="5%">
                                                Subject</td>
                                            <td bgcolor="#ffffff" width="1%">
                                                :</td>
                                            <td bgcolor="#ffffff" width="80%">
                                                <asp:Label ID="LblSubjectEdit_New" runat="server"></asp:Label></td>
                                        </tr>
                                        <tr class="formText">
                                            <td bgcolor="#ffffff" colspan="2" style="height: 114px" valign="top">
                                                Body</td>
                                            <td bgcolor="#ffffff" style="height: 114px" valign="top" width="5">
                                                :</td>
                                            <td bgcolor="#ffffff" valign="top" width="80%">
                                                <asp:TextBox ID="TxtBodyEdit_New" runat="server" Height="100px" TextMode="MultiLine"
                                                    Width="350px"></asp:TextBox></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr class="formText">
                                <td bgcolor="#ffffff" colspan="4">
                                    <asp:LinkButton ID="LinkHideEmailTemplateEdit" runat="server">Hide</asp:LinkButton></td>
                            </tr>
                        </table>
                    </asp:View>
                </asp:MultiView>
				</ajax:ajaxpanel>
				</td>
		</tr>
		<tr class="formText">
		</tr>
		<tr class="formText">
		</tr>
		<tr class="formText">
		</tr>
		<tr class="formText" bgColor="#dddddd" height="30">
			<td colSpan="3" style="height: 9px">
				<table cellSpacing="0" cellPadding="3" border="0">
					<tr>
						<td><asp:imagebutton id="ImageAccept" runat="server" CausesValidation="True" SkinID="AcceptButton"></asp:imagebutton></td>
						<td><asp:imagebutton id="ImageReject" runat="server" CausesValidation="False" SkinID="RejectButton"></asp:imagebutton></td>
                        <td>
                            <asp:ImageButton ID="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton" /></td>
					</tr>
				</table>               
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
		</tr>
	</table>
</asp:Content>