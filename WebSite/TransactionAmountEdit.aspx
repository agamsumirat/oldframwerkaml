﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="TransactionAmountEdit.aspx.vb" Inherits="TransactionAmountEdit" ValidateRequest="false" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td style="width: 7px">
            </td>
            <td id="tdcontent" height="99%" valign="top" bgcolor="#FFFFFF">
                <div id="divcontent" class="divcontent">
                    <table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td>
                                <img src="Images/blank.gif" width="20" height="100%" /></td>
                            <td class="divcontentinside" bgcolor="#FFFFFF">
                                <ajax:AjaxPanel ID="AjaxPanel5" runat="server" Width="100%">
                                    <asp:MultiView ID="MtvTransactionAmount" runat="server" ActiveViewIndex="0">
                                        <asp:View ID="VwAdd" runat="server">
                                            <table style="width: 100%" bordercolor="#ffffff" cellspacing="1" cellpadding="2"
                                                width="100%" bgcolor="#dddddd" border="0">
                                                <tr>
                                                    <td bgcolor="#ffffff" colspan="6" style="font-size: 18px; border-top-style: none;
                                                        border-right-style: none; border-left-style: none; border-bottom-style: none;
                                                        height: 50px;" valign="top">
                                                        <img src="Images/dot_title.gif" width="17" height="17">
                                                        <asp:Label ID="LblValue" runat="server" Font-Bold="True" Font-Size="Medium" Text="Transaction Amount Edit"></asp:Label>
                                                        <hr />
                                                    </td>
                                                </tr>
                                                <tr bgcolor="#ffffff">
                                                    <td colspan="6">
                                                        <asp:GridView ID="GrdVwTransactionAmount" runat="server" AutoGenerateColumns="False"
                                                            CellPadding="4" ForeColor="#333333" GridLines="None">
                                                            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                                            <Columns>
                                                                <asp:BoundField HeaderText="Tier" />
                                                                <asp:BoundField HeaderText="Start Range" />
                                                                <asp:BoundField HeaderText="End Range" />
                                                                <asp:BoundField DataField="Toleransi" HeaderText="Tolerance" />
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="LinkBtnEdit" runat="server">Edit</asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ShowHeader="False">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lnkButtonRemove" runat="server" CausesValidation="false" CommandName=""
                                                                            Text="Remove"></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                                                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                            <AlternatingRowStyle BackColor="White" />
                                                        </asp:GridView>
                                                        &nbsp; &nbsp;&nbsp;
                                                    </td>
                                                </tr>
                                                <tr bgcolor="#ffffff">
                                                    <td style="width: 27%; height: 24px; background-color: infobackground;" valign="middle">
                                                        Tier :
                                                        <asp:TextBox ID="TxtTier" runat="server" Width="150px"></asp:TextBox><span style="color: #ff0000">*</span></td>
                                                    <td bgcolor="#fff7e6" valign="middle" style="width: 28%; height: 24px; background-color: infobackground;">
                                                        StartRange : &nbsp;<asp:TextBox ID="TxtStartRange" runat="server" Width="150px"></asp:TextBox><span
                                                            style="color: #ff0000">*</span></td>
                                                    <td style="width: 28%; height: 24px; background-color: infobackground" valign="middle">
                                                        <span style="color: #ff0000"><span style="color: #000000; background-color: #ffffe1">
                                                            EndRange : </span>
                                                            <asp:TextBox ID="TxtEndRange" runat="server"></asp:TextBox>*</span></td>
                                                    <td style="width: 28%; height: 24px; background-color: infobackground" valign="middle">
                                                        Tolerance(%) :<br />
                                                        <asp:TextBox ID="txtToleransi" runat="server"></asp:TextBox></td>
                                                    <td valign="middle" style="height: 24px; width: 32%; background-color: infobackground;">
                                                        &nbsp;&nbsp;
                                                        <asp:ImageButton ID="ImageAdd" runat="server" ImageUrl="~/images/button/Add.gif"
                                                            CausesValidation="False" />&nbsp;<asp:ImageButton ID="ImgBtnCancel" runat="server"
                                                                ImageUrl="~/images/button/cancel.gif" Visible="False" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:View>
                                        <asp:View ID="VwConfirmation" runat="server">
                                            <table style="width: 100%" bordercolor="#ffffff" cellspacing="1" cellpadding="2"
                                                width="100%" bgcolor="#dddddd" border="0">
                                                <tr bgcolor="#ffffff">
                                                    <td colspan="2" align="center" style="height: 17px">
                                                        <asp:Label ID="LblConfirmation" runat="server"></asp:Label></td>
                                                </tr>
                                                <tr bgcolor="#ffffff">
                                                    <td align="center" colspan="2">
                                                        <asp:ImageButton ID="ImgBtnAdd" runat="server" ImageUrl="~/images/button/Ok.gif"
                                                            CausesValidation="False" />
                                                        <asp:ImageButton ID="ImgBack" runat="server" ImageUrl="~/images/button/back.gif"
                                                            CausesValidation="False" /></td>
                                                </tr>
                                            </table>
                                        </asp:View>
                                    </asp:MultiView>
                                </ajax:AjaxPanel>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td style="width: 7px">
            </td>
            <td>
                <ajax:AjaxPanel ID="AjaxPanel1" runat="server">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td align="left" background="Images/button-bground.gif" valign="middle">
                                <img height="1" src="Images/blank.gif" width="5" /></td>
                            <td align="left" background="Images/button-bground.gif" valign="middle">
                                <img height="15" src="images/arrow.gif" width="15" />&nbsp;</td>
                            <td background="Images/button-bground.gif">
                                &nbsp;<asp:ImageButton ID="ImgBtnSave" runat="server" ImageUrl="~/images/button/save.gif" /></td>
                            <td background="Images/button-bground.gif">
                                &nbsp;</td>
                            <td background="Images/button-bground.gif">
                                &nbsp;<asp:ImageButton ID="ImgBackAdd" runat="server" ImageUrl="~/images/button/back.gif"
                                    CausesValidation="False" /></td>
                            <td background="Images/button-bground.gif" width="99%">
                                <img height="1" src="Images/blank.gif" width="1" /></td>
                        </tr>
                    </table>
                    <asp:CustomValidator ID="CvalHandleErr" runat="server" Display="None" ValidationGroup="handle"></asp:CustomValidator><asp:CustomValidator
                        ID="CvalPageErr" runat="server" Display="None"></asp:CustomValidator></ajax:AjaxPanel>
            </td>
        </tr>
    </table>
</asp:Content>
