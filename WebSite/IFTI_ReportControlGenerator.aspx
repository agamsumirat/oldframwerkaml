<%@ Page Language="VB" MasterPageFile="~/masterpage.master"  AutoEventWireup="false" CodeFile="IFTI_ReportControlGenerator.aspx.vb" Inherits="IFTI_ReportControlGenerator" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td>
                <img src="Images/blank.gif" width="5" height="1" />
            </td>
            <td>
               
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td width="99%" bgcolor="#FFFFFF">
                            <img src="Images/blank.gif" width="1" height="1" />
                        </td>
                        <td bgcolor="#FFFFFF">
                            <img src="Images/blank.gif" width="1" height="1" />
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="Images/blank.gif" width="5" height="1" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td id="tdcontent" height="99%" valign="top" bgcolor="#FFFFFF">
                <div id="divcontent" class="divcontent">
                    <table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td class="divcontentinside" bgcolor="#FFFFFF">
                                <ajax:AjaxPanel ID="a" runat="server">
                                    
                                </ajax:AjaxPanel>
                                <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" border="0">
                                    <tr>
                                        <td valign="top" bgcolor="#ffffff">
                                            <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                <tr>
                                                    <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                                                        border-right-style: none; border-left-style: none; border-bottom-style: none;">
                                                        <img src="Images/dot_title.gif" width="17" height="17"><strong><asp:Label ID="Label5"
                                                            runat="server" Text="IFTI Control Report Generator"></asp:Label></strong><hr />
                                                    </td>
                                                </tr>
                                                <tr class="formText">
                                                    <td bgcolor="#eaeaea" colspan="4">
                                                        <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="IFTI Report Criteria"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr class="formText">
                                                    <td bgcolor="#FFF7E6" style="width: 1%" valign="top">
                                                        &nbsp;<ajax:AjaxPanel ID="AjaxPanel2" runat="server">
                                                        
                                                        <asp:RequiredFieldValidator
                                                            ID="RFVDescription" runat="server" ErrorMessage="Transaction Date From is required."
                                                            Display="Dynamic" ControlToValidate="textDateFrom">*</asp:RequiredFieldValidator>
                                                            <asp:RequiredFieldValidator
                                                            ID="RequiredFieldValidator1" runat="server" ErrorMessage="Transaction Date To is required."
                                                            Display="Dynamic" ControlToValidate="textDateTo">*</asp:RequiredFieldValidator>
                                                            </ajax:AjaxPanel>
                                                    </td>
                                                    <td bgcolor="#FFF7E6" style="width: 7%" valign="top">
                                                        <asp:Label ID="Label1" runat="server" Text="Transaction Date"></asp:Label><font color="#ff0000"
                                                            style="vertical-align: top"><strong></strong></font>
                                                    </td>
                                                    <td width="1%" bgcolor="#ffffff" valign="top">
                                                        :
                                                    </td>
                                                    <td width="30%" bgcolor="#ffffff" valign="top">
                                                        &nbsp;<asp:TextBox ID="textDateFrom" runat="server" CssClass="textBox" MaxLength="50"
                                                            Width="132px"></asp:TextBox><input id="popUpStartDate" runat="server" onclick="popUpCalendar(this, frmBasicCTRWeb.txtStartDate, 'dd-mmm-yyyy')"
                                                                style="border-right: #ffffff 0px solid; border-top: #ffffff 0px solid; font-size: 11px;
                                                                background-image: url(Script/Calendar/cal.gif); border-left: #ffffff 0px solid;
                                                                width: 16px; border-bottom: #ffffff 0px solid; height: 17px" title="Click to show calendar"
                                                                type="button" />&nbsp; to
                                                        <asp:TextBox ID="textDateTo" runat="server" CssClass="textBox" MaxLength="50" Width="132px"></asp:TextBox><input id="popUpEndDate" runat="server" onclick="popUpCalendar(this, frmBasicCTRWeb.txtStartDate, 'dd-mmm-yyyy')"
                                                                style="border-right: #ffffff 0px solid; border-top: #ffffff 0px solid; font-size: 11px;
                                                                background-image: url(Script/Calendar/cal.gif); border-left: #ffffff 0px solid;
                                                                width: 16px; border-bottom: #ffffff 0px solid; height: 17px" title="Click to show calendar"
                                                                type="button" /></td>
                                                </tr>
                                                <tr class="formText">
                                                    <td colspan="4" style="height: 5px;" bgcolor="#ffffff">
                                                    </td>
                                                </tr>
                                                <tr class="formText">
                                                    <td bgcolor="#eaeaea" colspan="4" valign="top">
                                                        <strong><span>
                                                            <asp:Label ID="Label4" runat="server" Font-Bold="True" Text="Type of Generate"></asp:Label></span></strong>&nbsp;
                                                    </td>
                                                </tr>
                                                <tr class="formText">
                                                    <td bgcolor="#FFF7E6" valign="top" style="width: 1%">
                                                        &nbsp;
                                                    </td>
                                                    <td bgcolor="#FFF7E6" valign="top" style="width: 7%">
                                                        <asp:Label ID="Label8" runat="server" Text="Transaction Type"></asp:Label><strong><span
                                                            style="color: #ff0000"></span></strong>
                                                    </td>
                                                    <td width="1%" bgcolor="#ffffff" valign="top">
                                                        :
                                                    </td>
                                                    <td width="30%" bgcolor="#ffffff" valign="top">
                                                        <asp:RadioButtonList ID="rblTransactionType" runat="server">
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                                <tr class="formText">
                                                    <td bgcolor="#FFF7E6" style="height: 67px; width: 1%;" valign="top">
                                                        &nbsp;
                                                    </td>
                                                    <td bgcolor="#FFF7E6" style="width: 7%; height: 67px;" valign="top">
                                                        <strong><span style="color: #ff0000"></span><span>
                                                            <asp:Label ID="Label6" runat="server" Font-Bold="False" Text="Report Format"></asp:Label></span></strong>
                                                    </td>
                                                    <td width="1%" bgcolor="#ffffff" style="height: 67px" valign="top">
                                                        :
                                                    </td>
                                                    <td width="30%" bgcolor="#ffffff" style="height: 67px" valign="top">
                                                        <asp:RadioButtonList ID="rblReportFormat" runat="server">
                                                            <asp:ListItem Value="2">Xml File Format</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                                <tr class="formText">
                                                    <td bgcolor="#ffffff" colspan="4" style="height: 15px" valign="top">
                                                        &nbsp; &nbsp; &nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td background="Images/button-bground.gif" align="left" valign="middle">
                            <img src="Images/blank.gif" width="5" height="1" />
                        </td>
                        <td background="Images/button-bground.gif" align="left" valign="middle">
                            <img src="images/arrow.gif" width="15" height="15" />&nbsp;
                        </td>
                        <td background="Images/button-bground.gif">
                            &nbsp;<asp:ImageButton ID="btnGenerate" runat="server" ImageUrl="~/Images/button/generate.gif"
                                CausesValidation="true"></asp:ImageButton>
                        </td>
                        <td background="Images/button-bground.gif">
                            &nbsp;&nbsp;
                        </td>
                        <td background="Images/button-bground.gif">
                            &nbsp;
                        </td>
                        <td width="99%" background="Images/button-bground.gif">
                            <img src="Images/blank.gif" width="1" height="1" />
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:CustomValidator ID="cvalPageErr" runat="server" Display="None"></asp:CustomValidator>
    <script language="javascript" type="text/javascript">
        document.getElementById('<%=GetFocusID %>').focus();
    </script>
</asp:Content>
