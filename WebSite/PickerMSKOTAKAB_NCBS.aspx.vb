Imports Sahassanettier.Data
Imports Sahassanettier.Entities
Imports Sahassanettier.Services
Imports System.Collections.Generic

Partial Class PickerMSKOTAKAB_NCBS
	Inherits Parent

#Region "Set Session"


	Private Property SetnGetSort() As String
		Get
			Return IIf(Session("PickerMSKOTAKAB_NCBSSort") Is Nothing, "IDKOTAKAB_NCBS asc", Session("PickerMSKOTAKAB_NCBSSort"))
		End Get
		Set(ByVal Value As String)
			Session("PickerMSKOTAKAB_NCBSSort") = Value
		End Set
	End Property
	Private Property SetnGetCurrentPage() As Int32
		Get
			Return IIf(Session("PickerMSKOTAKAB_NCBSCurrentPage") Is Nothing, 0, Session("PickerMSKOTAKAB_NCBSCurrentPage"))
		End Get
		Set(ByVal Value As Int32)
			Session("PickerMSKOTAKAB_NCBSCurrentPage") = Value
		End Set
	End Property
	Private ReadOnly Property GetPageTotal() As Int32
		Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
		End Get
	End Property
	Private Property SetnGetRowTotal() As Int32
		Get
			Return IIf(Session("PickerMSKOTAKAB_NCBSRowTotal") Is Nothing, 0, Session("PickerMSKOTAKAB_NCBSRowTotal"))
		End Get
		Set(ByVal Value As Int32)
			Session("PickerMSKOTAKAB_NCBSRowTotal") = Value
		End Set
	End Property
	Private Property SetAndGetSearchingCriteria() As String
		Get
			Return IIf(Session("PickerMSKOTAKAB_NCBSSearchCriteria") Is Nothing, "", Session("PickerMSKOTAKAB_NCBSSearchCriteria"))
		End Get
		Set(ByVal Value As String)
			Session("PickerMSKOTAKAB_NCBSSearchCriteria") = Value
		End Set
	End Property
	Private Function SearchFilter() As String
		Dim StrSearch As String = ""
		Try
			StrSearch = " Activation = 1 "
			'// Filter
		   If Me.txtNamaKotaKab_NCBS.Text <> "" Then
                If StrSearch = "" Then
                    StrSearch = StrSearch & " NamaKotaKab_NCBS like '%" & Me.txtNamaKotaKab_NCBS.Text.Replace("'", "''") & "%' "
                Else
                    StrSearch = StrSearch & " And NamaKotaKab_NCBS like '%" & Me.txtNamaKotaKab_NCBS.Text.Replace("'", "''") & "%' "
                End If
            End If



			Return StrSearch
		Catch
			Throw
			Return ""
		End Try
	End Function
	Private Function SetnGetBindTable() As TList(Of MSKOTAKAB_NCBS)
		Return DataRepository.MSKOTAKAB_NCBSProvider.GetPaged(SearchFilter, SetnGetSort, SetnGetCurrentPage, 10, SetnGetRowTotal)
	End Function
#End Region
	Private Property StrMSKOTAKAB_NCBSFilter() As String
		Get
			Return IIf(Session("PickerMSKOTAKAB_NCBSStrMSKOTAKAB_NCBSFilter") Is Nothing, "", Session("PickerMSKOTAKAB_NCBSStrMSKOTAKAB_NCBSFilter"))
		End Get
		Set(ByVal Value As String)
			Session("PickerMSKOTAKAB_NCBSStrMSKOTAKAB_NCBSFilter") = Value
		End Set
	End Property

#Region " Searching Box"
	Protected Sub ImageClearSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageClearSearch.Click
		Try
			'//Clear Control
		 Me.txtNamaKotaKab_NCBS.Text = ""


		Catch
			Throw
		End Try
	End Sub
	Protected Sub ImgSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgSearch.Click
		Try
			Me.SetnGetCurrentPage = 0
		Catch ex As Exception
			Me.cvalPageError.IsValid = False
			Me.cvalPageError.ErrorMessage = ex.Message
			LogError(ex)
		End Try
	End Sub
#End Region
#Region "Sorting Event"
	Protected Sub GridDataView_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridDataView.SortCommand
		Dim GridPickerProductTier2Sort As DataGrid = source
		Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridPickerProductTier2Sort.Columns(Sahassa.AML.Commonly.IndexSort(GridDataView, e.SortExpression)).SortExpression = Me.SetnGetSort
		Catch ex As Exception
			Me.cvalPageError.IsValid = False
			Me.cvalPageError.ErrorMessage = ex.Message
			LogError(ex)
		End Try
	End Sub
#End Region
#Region "Paging Event"
	Private Sub SetInfoNavigate()
		Try
			Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
			Me.PageTotalPages.Text = Me.GetPageTotal
			Me.PageTotalRows.Text = Me.SetnGetRowTotal
			Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
			Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
			Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
			Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
		Catch
			Throw
		End Try
	End Sub
	Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
		Try
			Select Case e.CommandName
				Case "First" : Me.SetnGetCurrentPage = 0
				Case "Prev" : Me.SetnGetCurrentPage -= 1
				Case "Next" : Me.SetnGetCurrentPage += 1
				Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
				Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
			End Select
		Catch ex As Exception
			Me.cvalPageError.IsValid = False
			Me.cvalPageError.ErrorMessage = ex.Message
			LogError(ex)
		End Try
	End Sub
	Protected Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
		Try
			If IsNumeric(Me.TextGoToPage.Text) Then
				If (CInt(Me.TextGoToPage.Text) > 0) Then
					If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
						Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
					Else
						Throw New Exception("Page number must be less than or equal to the total page count.")
					End If
				Else
					Throw New Exception("Page number must be less than or equal to the total page count.")
				End If
			Else
				Throw New Exception("Page number must be in number format.")
			End If
		Catch ex As Exception
			Me.cvalPageError.IsValid = False
			Me.cvalPageError.ErrorMessage = ex.Message
			LogError(ex)
		End Try
	End Sub
#End Region
	Private Sub ClearThisPageSessions()
		Session("PickerMSKOTAKAB_NCBSSort") = Nothing
		Session("PickerMSKOTAKAB_NCBSCurrentPage") = Nothing
		Session("PickerMSKOTAKAB_NCBSRowTotal") = Nothing
		Session("PickerMSKOTAKAB_NCBSSearchCriteria") = Nothing
		Session("PickerProductTier2ViewData") = Nothing
		Session("PickerProductTier2Selected") = Nothing
		Session("PickerMSKOTAKAB_NCBS.Data") = Nothing
	End Sub

	Public Sub BindGrid()
		Me.GridDataView.DataSource = Me.SetnGetBindTable
		Me.GridDataView.VirtualItemCount = Me.SetnGetRowTotal
		Me.GridDataView.DataBind()
	End Sub
	Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		Try
			Response.Cache.SetCacheability(HttpCacheability.NoCache)
			If Not Me.IsPostBack Then
				Me.ClearThisPageSessions()

                Me.GridDataView.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                'Using AccessAudit As New CTRWebBLL.AuditTrailBLL
                '	AccessAudit.InsertAuditTrailUserAccess(Sahassa.CommonCTRWeb.Commonly.SessionNIK, Sahassa.CommonCTRWeb.Commonly.SessionStaffName, "Accesssing page " & Sahassa.CommonCTRWeb.Commonly.SessionCurrentPage & " succeeded")
                'End Using
			End If
		Catch ex As Exception
			Me.cvalPageError.IsValid = False
			Me.cvalPageError.ErrorMessage = ex.Message
			LogError(ex)
		End Try
	End Sub

	Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
		Try
			Me.CollectSelected()
			Me.BindGrid()
			Me.SetInfoNavigate()
		Catch ex As Exception
			Me.cvalPageError.IsValid = False
			Me.cvalPageError.ErrorMessage = ex.Message
			LogError(ex)
		End Try
	End Sub

	Private Property SetnGetSelectedItem() As ArrayList
		Get
			Return IIf(Session("PickerProductTier2Selected") Is Nothing, New ArrayList, Session("PickerProductTier2Selected"))
		End Get
		Set(ByVal value As ArrayList)
			Session("PickerProductTier2Selected") = value
		End Set
	End Property

	Protected Sub GridDataView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridDataView.ItemDataBound
		Try
			If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
				Dim chkBox As CheckBox = e.Item.FindControl("rbSelected")
				chkBox.Checked = Me.SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)
                e.Item.Cells(2).Text = CStr((e.Item.ItemIndex + 1) + (Me.SetnGetCurrentPage * Sahassa.AML.Commonly.GetDisplayedTotalRow))
			End If
		Catch ex As Exception
			Me.cvalPageError.IsValid = False
			Me.cvalPageError.ErrorMessage = ex.Message
			LogError(ex)
		End Try
	End Sub

	Private Sub CollectSelected()
		For Each gridRow As DataGridItem In Me.GridDataView.Items
			If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
				Dim chkBox As CheckBox = gridRow.FindControl("rbSelected")
				Dim PkId As String = gridRow.Cells(1).Text
				Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
				If ArrTarget.Contains(PkId) Then
					If Not chkBox.Checked Then
						ArrTarget.Remove(PkId)
					End If
				Else
					If chkBox.Checked Then ArrTarget.Add(PkId)
				End If
				Me.SetnGetSelectedItem = ArrTarget
			End If
		Next
	End Sub

	Protected Sub ImgSelected_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgSelected.Click
		Dim ObjMSKOTAKAB_NCBS As New TList(Of MSKOTAKAB_NCBS)
		Dim intSelected As Integer = 0

		'For i As Integer = 0 To GridDataView.Items.Count - 1
		'	Dim rbSelected As CheckBox = CType(GridDataView.Items(i).FindControl("rbSelected"), CheckBox)
		'	If rbSelected.Checked Then
		'		Dim Key As String = GridDataView.Items(i).Cells(1).Text
		'		Dim obj As MSKOTAKAB_NCBS = DataRepository.MSKOTAKAB_NCBSProvider.GetByIDKOTAKAB_NCBS(Key)
		'		ObjMSKOTAKAB_NCBS.Add(obj)
		'		intSelected = 1
		'		' Exit For
		'	End If
		'Next

		Dim sbScript As New StringBuilder
        If Session("PickerMSKOTAKAB_NCBS.Data") Is Nothing Then
			sbScript.Append("<script>alert('Please select data');</script>")
			ClientScript.RegisterClientScriptBlock(Page.GetType, "alterting", sbScript.ToString())
		Else
		'	If Session("PickerMSKOTAKAB_NCBS.Data") Is Nothing Then
		'		Session.Add("PickerMSKOTAKAB_NCBS.Data", ObjMSKOTAKAB_NCBS)
		'	Else
		'		Session("PickerMSKOTAKAB_NCBS.Data") = ObjMSKOTAKAB_NCBS
		'	End If
			ClientScript.RegisterClientScriptBlock(Page.GetType, "closing", "javascript:window.close()", True)
		End If

	End Sub

	Private Property ListMSKOTAKAB_NCBSId() As System.Collections.Generic.List(Of Integer)
		Get
			Return IIf(Session("PickerMSKOTAKAB_NCBS_List_MSKOTAKAB_NCBS_Id") Is Nothing, New System.Collections.Generic.List(Of Integer), Session("PickerMSKOTAKAB_NCBS_List_MSKOTAKAB_NCBS_Id"))
		End Get
		Set(ByVal Value As System.Collections.Generic.List(Of Integer))
			Session("PickerMSKOTAKAB_NCBS_List_MSKOTAKAB_NCBS_Id") = Value
		End Set
	End Property


    Protected Sub rbSelected_CheckedChanged(sender As Object, e As System.EventArgs)
        Try
            Dim objcheck As CheckBox = CType(sender, CheckBox)
            Dim objgridviewrow As DataGridItem = objcheck.NamingContainer

            Dim pk As Integer = GridDataView.Items(objgridviewrow.ItemIndex).Cells(1).Text()

            If Session("PickerMSKOTAKAB_NCBS.Data") Is Nothing Then
                Session("PickerMSKOTAKAB_NCBS.Data") = New TList(Of MSKOTAKAB_NCBS)
            End If
            If objcheck.Checked Then

                Using objcoll As TList(Of MSKOTAKAB_NCBS) = CType(Session("PickerMSKOTAKAB_NCBS.Data"), TList(Of MSKOTAKAB_NCBS))
                    If objcoll.Find(MsKotaKab_NCBSColumn.IDKotaKab_NCBS, pk) Is Nothing Then

                        Dim obj As MsKotaKab_NCBS = DataRepository.MsKotaKab_NCBSProvider.GetByIDKotaKab_NCBS(pk)
                        objcoll.Add(obj)
                    End If
                End Using

            Else
                Using objcoll As TList(Of MSKOTAKAB_NCBS) = CType(Session("PickerMSKOTAKAB_NCBS.Data"), TList(Of MSKOTAKAB_NCBS))
                    Using objfind As MsKotaKab_NCBS = objcoll.Find(MsKotaKab_NCBSColumn.IDKotaKab_NCBS, pk)
                        If Not objfind Is Nothing Then
                            objcoll.Remove(objfind)
                        End If

                    End Using
                End Using

            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ChkAll_CheckedChanged(sender As Object, e As System.EventArgs) Handles ChkAll.CheckedChanged
        Try

            For Each item As DataGridItem In GridDataView.Items
                If item.ItemType = ListItemType.AlternatingItem Or item.ItemType = ListItemType.Item Then
                    Dim objrbselected As CheckBox = CType(item.FindControl("rbSelected"), CheckBox)
                    objrbselected.Checked = ChkAll.Checked
                    Dim objgridviewrow As DataGridItem = objrbselected.NamingContainer
                    Dim pk As Integer = GridDataView.Items(objgridviewrow.ItemIndex).Cells(1).Text()

                    If Session("PickerMSKOTAKAB_NCBS.Data") Is Nothing Then
                        Session("PickerMSKOTAKAB_NCBS.Data") = New TList(Of MSKOTAKAB_NCBS)
                    End If

                    If objrbselected.Checked Then

                        Using objcoll As TList(Of MSKOTAKAB_NCBS) = CType(Session("PickerMSKOTAKAB_NCBS.Data"), TList(Of MSKOTAKAB_NCBS))
                            If objcoll.Find(MsKotaKab_NCBSColumn.IDKotaKab_NCBS, pk) Is Nothing Then

                                Dim obj As MsKotaKab_NCBS = DataRepository.MsKotaKab_NCBSProvider.GetByIDKotaKab_NCBS(pk)
                                objcoll.Add(obj)
                            End If
                        End Using
                    Else
                        Using objcoll As TList(Of MSKOTAKAB_NCBS) = CType(Session("PickerMSKOTAKAB_NCBS.Data"), TList(Of MSKOTAKAB_NCBS))
                            Using objfind As MsKotaKab_NCBS = objcoll.Find(MsKotaKab_NCBSColumn.IDKotaKab_NCBS, pk)
                                If Not objfind Is Nothing Then
                                    objcoll.Remove(objfind)
                                End If

                            End Using
                        End Using
                    End If

                End If
            Next

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class


