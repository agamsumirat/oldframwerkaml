﻿Imports CDDLNPNettier.Data
Imports CDDLNPNettier.Entities
Imports AMLBLL
Imports System.Data

Partial Class CDDLNPQuestionAdd
    Inherits Parent

    Property Edit_index() As Integer
        Get
            Return IIf(Session("CDDQuestionDetail.index") Is Nothing, -1, Session("CDDQuestionDetail.index"))
        End Get
        Set(ByVal Value As Integer)
            Session("CDDQuestionDetail.index") = Value
        End Set
    End Property

    Private Property SetnGetBindTable() As TList(Of CDD_QuestionDetail_ApprovalDetail)
        Get
            Return CType(Session("CDDQuestionDetail"), TList(Of CDD_QuestionDetail_ApprovalDetail))
        End Get
        Set(ByVal value As TList(Of CDD_QuestionDetail_ApprovalDetail))
            Session("CDDQuestionDetail") = value
        End Set
    End Property

    Private Sub BindComboBox()
        ddlQuestionType.Items.Clear()
        For Each objQuestionType As CDD_QuestionType In DataRepository.CDD_QuestionTypeProvider.GetAll()
            ddlQuestionType.Items.Add(New ListItem(objQuestionType.QuestionTypeName, objQuestionType.PK_CDD_QuestionType_ID))
        Next

        ddlValidation.Items.Clear()
        For Each objValidation As CDD_VAlidation In DataRepository.CDD_VAlidationProvider.GetAll()
            ddlValidation.Items.Add(New ListItem(objValidation.CDD_VAlidationName, objValidation.PK_CDD_VAlidation_ID))
        Next

        ddlSequence.Items.Clear()
        ddlSequence.Items.Add(New ListItem("Last", 9999))
        For i As Int32 = 1 To DataRepository.CDD_QuestionProvider.GetTotalItems(String.Empty, 0)
            ddlSequence.Items.Add(New ListItem(i, i))
        Next
    End Sub

    Protected Sub CvalPageErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles cvalPageError.PreRender
        If cvalPageError.IsValid = False Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
        End If
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not Page.IsPostBack Then
                SetnGetBindTable = New TList(Of CDD_QuestionDetail_ApprovalDetail)

                BindComboBox()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
            End If
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub

    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim objTransManager As New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)
        Dim objApproval As New CDD_Question_Approval
        Dim objCDDQuestion As New CDD_Question_ApprovalDetail
        Dim objCDDQuestionDetail As New TList(Of CDD_QuestionDetail_ApprovalDetail)

        Try
            objTransManager.BeginTransaction()

            If txtQuestion.Text.Trim = String.Empty Then
                Throw New Exception("Question harus diisi")
            End If

            objApproval.RequestedBy = Sahassa.AML.Commonly.SessionPkUserId
            objApproval.RequestedDate = Date.Now
            objApproval.FK_MsMode_Id = 1
            objApproval.IsUpload = False
            DataRepository.CDD_Question_ApprovalProvider.Save(objTransManager, objApproval)


            objCDDQuestion.FK_CDD_Question_Approval_Id = objApproval.PK_CDD_Question_Approval_Id
            objCDDQuestion.CDD_Question = txtQuestion.Text.Trim
            objCDDQuestion.FK_CDD_QuestionType_ID = ddlQuestionType.SelectedValue
            If hfParentQuestionID.Value <> String.Empty Then
                objCDDQuestion.FK_Parent_Question_ID = hfParentQuestionID.Value
            End If
            objCDDQuestion.IsRequired = chkIsRequired.Checked
            objCDDQuestion.SortNo = ddlSequence.SelectedValue
            DataRepository.CDD_Question_ApprovalDetailProvider.Save(objTransManager, objCDDQuestion) 'Save Question


            For Each rowCDDQuestionDetail As CDD_QuestionDetail_ApprovalDetail In SetnGetBindTable
                rowCDDQuestionDetail.FK_CDD_Question_Approval_Id = objApproval.PK_CDD_Question_Approval_Id
            Next
            DataRepository.CDD_QuestionDetail_ApprovalDetailProvider.Save(objTransManager, SetnGetBindTable)

            objTransManager.Commit()

            Me.Response.Redirect("CDDLNPMessage.aspx?ID=10111", False)

        Catch ex As Exception
            objTransManager.Rollback()
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        Finally
            If Not objTransManager Is Nothing Then
                objTransManager.Dispose()
                objTransManager = Nothing
            End If
            If Not objCDDQuestion Is Nothing Then
                objCDDQuestion.Dispose()
                objCDDQuestion = Nothing
            End If
            If Not objCDDQuestionDetail Is Nothing Then
                objCDDQuestionDetail.Dispose()
                objCDDQuestionDetail = Nothing
            End If
        End Try
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Me.Response.Redirect("CDDLNPQuestionView.aspx")
    End Sub

    Protected Sub ImageAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAdd.Click
        Try
            If txtQuestionDetail.Text.Trim = String.Empty Then
                Throw New Exception("Question Detail harus diisi")
                txtQuestionDetail.Focus()
            Else
                SetnGetBindTable.Add(CDD_QuestionDetail_ApprovalDetail.CreateCDD_QuestionDetail_ApprovalDetail(Nothing, Nothing, Nothing, _
                    txtQuestionDetail.Text.Trim, ddlValidation.SelectedValue > 0, ddlValidation.SelectedValue))

                txtQuestionDetail.Focus()
                txtQuestionDetail.Text = String.Empty
                ddlValidation.SelectedValue = 0
            End If
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub

    Protected Sub ddlQuestionType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlQuestionType.SelectedIndexChanged
        Try
            Select Case ddlQuestionType.SelectedValue
                Case 3, 4, 7
                    RowQuestionDetail.Visible = True

                Case Else
                    RowQuestionDetail.Visible = False

            End Select
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub

    Protected Sub ImageEdit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageEdit.Click
        Try
            If txtQuestionDetail.Text.Trim = String.Empty Then
                Throw New Exception("Question Detail harus diisi")
                txtQuestionDetail.Focus()
            Else
                SetnGetBindTable(Edit_index).QuestionDetail = txtQuestionDetail.Text
                SetnGetBindTable(Edit_index).FK_CDD_VAlidation_ID = ddlValidation.SelectedValue

                txtQuestionDetail.Focus()
                txtQuestionDetail.Text = String.Empty
                ddlValidation.SelectedValue = 0
                ImageEdit.Visible = False
                ImageCancelEdit.Visible = False
                ImageAdd.Visible = True
            End If
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub

    Protected Sub ImageCancelEdit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancelEdit.Click
        Try
            txtQuestionDetail.Focus()
            txtQuestionDetail.Text = String.Empty
            ddlValidation.SelectedValue = 0
            ImageEdit.Visible = False
            ImageCancelEdit.Visible = False
            ImageAdd.Visible = True
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub

    Protected Sub GridViewCDDQuestionDetail_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridViewCDDQuestionDetail.ItemCommand
        Try
            Select Case e.CommandName.ToLower
                Case "edit"
                    Edit_index = e.Item.ItemIndex
                    txtQuestionDetail.Text = SetnGetBindTable(Edit_index).QuestionDetail
                    ddlValidation.SelectedValue = SetnGetBindTable(Edit_index).FK_CDD_VAlidation_ID

                    txtQuestionDetail.Focus()
                    ImageEdit.Visible = True
                    ImageCancelEdit.Visible = True
                    ImageAdd.Visible = False

                Case "delete"
                    SetnGetBindTable.RemoveAt(e.Item.ItemIndex)

            End Select
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub

    Protected Sub GridViewCDDQuestionDetail_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridViewCDDQuestionDetail.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Using objValidation As CDD_VAlidation = DataRepository.CDD_VAlidationProvider.GetByPK_CDD_VAlidation_ID(e.Item.Cells(2).Text)
                    If objValidation IsNot Nothing Then
                        e.Item.Cells(3).Text = objValidation.CDD_VAlidationName
                    End If
                End Using
            End If
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            GridViewCDDQuestionDetail.DataSource = SetnGetBindTable
            GridViewCDDQuestionDetail.DataBind()
            Session("strPK_CDD_Question_ID") = "''"
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub

    Protected Sub ImageBrowse_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBrowse.Click
        Try
            If Session("PopupCDDQuestion.PK_CDD_Question_ID") IsNot Nothing Then
                lblParentQuestion.Text = Session("PopupCDDQuestion.CDD_Question").ToString.Replace("</br>", vbCrLf)
                hfParentQuestionID.Value = Session("PopupCDDQuestion.PK_CDD_Question_ID")
            End If
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub

    Protected Sub ImageRemove_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageRemove.Click
        Try
            lblParentQuestion.Text = String.Empty
            hfParentQuestionID.Value = String.Empty
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub
End Class