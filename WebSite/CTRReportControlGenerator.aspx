<%@ Page Language="VB" MasterPageFile="~/masterpage.master" AutoEventWireup="false"
    CodeFile="CTRReportControlGenerator.aspx.vb" Inherits="CTRReportControlGenerator"  %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    <script src="Script/popcalendar.js"></script>
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td>
                <img src="Images/blank.gif" width="5" height="1" />
            </td>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td>
                          
                        </td>
                        <td width="99%" bgcolor="#FFFFFF">
                            <img src="Images/blank.gif" width="1" height="1" />
                        </td>
                        <td bgcolor="#FFFFFF">
                            <img src="Images/blank.gif" width="1" height="1" />
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="Images/blank.gif" width="5" height="1" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td id="tdcontent" height="99%" valign="top" bgcolor="#FFFFFF">
                
                    <table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td class="divcontentinside" bgcolor="#FFFFFF">
                                <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                                    <asp:View ID="View1" runat="server">
                                     <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" border="0">
                                    <tr>
                                        <td valign="top" bgcolor="#ffffff">
                                            <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                
                                                <tr>
                                                    <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                                                        border-right-style: none; border-left-style: none; border-bottom-style: none;">
                                                        <img src="Images/dot_title.gif" width="17" height="17"><strong><asp:Label ID="Label5"
                                                            runat="server" Text="CTR Control Report Generator"></asp:Label></strong><hr />
                                                    </td>
                                                </tr>
                                                <tr class="formText">
                                                    <td bgcolor="#eaeaea" colspan="4">
                                                        <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="Report Criteria"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr class="formText">
                                                    <td bgcolor="#FFF7E6" style="width: 1%" valign="top">
                                                        &nbsp;<ajax:AjaxPanel ID="AjaxPanel2" runat="server">
                                                            
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <asp:RequiredFieldValidator
                                                            ID="RFVDescription" runat="server" ErrorMessage="Transaction Date is required."
                                                            Display="Dynamic" ControlToValidate="textDate">*</asp:RequiredFieldValidator></ajax:AjaxPanel>
                                                    </td>
                                                    <td bgcolor="#FFF7E6" style="width: 7%" valign="top">
                                                        <asp:Label ID="Label1" runat="server" Text="Transaction Date"></asp:Label><font color="#ff0000"
                                                            style="vertical-align: top"><strong></strong></font>
                                                    </td>
                                                    <td width="1%" bgcolor="#ffffff" valign="top">
                                                        :
                                                    </td>
                                                    <td width="30%" bgcolor="#ffffff" valign="top">
                                                        &nbsp;<asp:TextBox ID="textDate" runat="server" CssClass="textBox" MaxLength="50"
                                                            Width="132px"></asp:TextBox><input id="popUpStartDate" runat="server" onclick="popUpCalendar(this, frmBasicCTRWeb.txtStartDate, 'dd-mmm-yyyy')"
                                                                style="border-right: #ffffff 0px solid; border-top: #ffffff 0px solid; font-size: 11px;
                                                                background-image: url(Script/Calendar/cal.gif); border-left: #ffffff 0px solid;
                                                                width: 16px; border-bottom: #ffffff 0px solid; height: 17px" title="Click to show calendar"
                                                                type="button" />
                                                    </td>
                                                </tr>
                                                <tr class="formText">
                                                    <td colspan="4" style="height: 5px;" bgcolor="#ffffff">
                                                    </td>
                                                </tr>
                                                <tr class="formText">
                                                    <td bgcolor="#eaeaea" colspan="4" valign="top">
                                                        <strong><span>
                                                            <asp:Label ID="Label4" runat="server" Font-Bold="True" Text="Type of Generate"></asp:Label></span></strong>&nbsp;
                                                    </td>
                                                </tr>
                                                <tr class="formText">
                                                    <td bgcolor="#FFF7E6" valign="top" style="width: 1%">
                                                        &nbsp;
                                                    </td>
                                                    <td bgcolor="#FFF7E6" valign="top" style="width: 7%">
                                                        <asp:Label ID="Label8" runat="server" Text="Report Type"></asp:Label><strong><span
                                                            style="color: #ff0000"></span></strong>
                                                    </td>
                                                    <td width="1%" bgcolor="#ffffff" valign="top">
                                                        :
                                                    </td>
                                                    <td width="30%" bgcolor="#ffffff" valign="top">
                                                        <asp:RadioButtonList ID="rblReportType" runat="server">
                                                           
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                                <tr class="formText">
                                                    <td bgcolor="#FFF7E6" style="height: 9px; width: 1%;" valign="top">
                                                        &nbsp;
                                                    </td>
                                                    <td bgcolor="#FFF7E6" style="width: 7%;" valign="top">
                                                        <strong><span style="color: #ff0000"></span><span>
                                                            <asp:Label ID="Label6" runat="server" Font-Bold="False" Text="Report Format"></asp:Label></span></strong>
                                                    </td>
                                                    <td width="1%" bgcolor="#ffffff" style="height: 9px" valign="top">
                                                        :
                                                    </td>
                                                    <td width="30%" bgcolor="#ffffff" style="height: 9px" valign="top">
                                                        <asp:RadioButtonList ID="rblReportFormat" runat="server">
                                                          
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                                <tr class="formText">
                                                    <td bgcolor="#ffffff" colspan="4" style="height: 15px" valign="top">
                                                        <img src="images/arrow.gif" width="15" height="15" />&nbsp;
                                                        <asp:ImageButton ID="btnGenerate" runat="server" CausesValidation="true" 
                                                            ImageUrl="~/Images/button/generate.gif" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                    </asp:View>
                                    <asp:View ID="View2" runat="server">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr align="center">
                                                <td>
                                                 <asp:Label ID="LblMessage" runat="server" Font-Bold="True"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr align="center">
                                                <td>
                                                    <asp:ImageButton ID="ImageButton1" runat="server" 
                                                        ImageUrl="~/Images/button/ok.gif" />
                                                </td>
                                            </tr>
                                        </table>
                                       
                                    </asp:View>
                                </asp:MultiView>

                               
                            </td>
                        </tr>
                    </table>
                
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td background="Images/button-bground.gif" align="left" valign="middle">
                            <img src="Images/blank.gif" width="5" height="1" />
                        </td>
                        <td background="Images/button-bground.gif" align="left" valign="middle">
                            
                        </td>
                        <td background="Images/button-bground.gif">
                            &nbsp;</td>
                        <td background="Images/button-bground.gif">
                            &nbsp;&nbsp;
                        </td>
                        <td background="Images/button-bground.gif">
                            &nbsp;
                        </td>
                        <td width="99%" background="Images/button-bground.gif">
                            <img src="Images/blank.gif" width="1" height="1" />
                        </td>
                        <td>
                          
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:CustomValidator ID="cvalPageErr" runat="server" Display="None"></asp:CustomValidator>
    
</asp:Content>
