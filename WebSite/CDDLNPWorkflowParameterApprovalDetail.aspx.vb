﻿Imports CDDLNPNettier.Data
Imports CDDLNPNettier.Entities
Imports AMLBLL
Imports System.Data

Partial Class CDDLNPWorkflowParameterApprovalDetail
    Inherits Parent

    Private ReadOnly Property GetPK_CDDLNP_WorkflowParameter_Approval_Id() As Int32
        Get
            Return IIf(Request.QueryString("ID") = String.Empty, 0, Request.QueryString("ID"))
        End Get
    End Property

    Private Property SetnGetPK_CDDLNP_WorkflowParameter_id() As Int32
        Get
            Return CType(Session("CDDLNPWorkflowParameterApprovalDetail.PK_CDDLNP_WorkflowParameter_id"), Int32)
        End Get
        Set(ByVal value As Int32)
            Session("CDDLNPWorkflowParameterApprovalDetail.PK_CDDLNP_WorkflowParameter_id") = value
        End Set
    End Property

    Protected Sub CvalPageErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles cvalPageError.PreRender
        If cvalPageError.IsValid = False Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
        End If
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Me.Response.Redirect("CDDLNPWorkflowParameterApproval.aspx")
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not Page.IsPostBack Then
                'Load data Approval Header
                Using objApproval As VList(Of vw_CDDLNP_WorkflowParameter_Approval) = DataRepository.vw_CDDLNP_WorkflowParameter_ApprovalProvider.GetPaged("PK_CDDLNP_WorkflowParameter_Approval_Id = " & Me.GetPK_CDDLNP_WorkflowParameter_Approval_Id, String.Empty, 0, Int32.MaxValue, 0)
                    If objApproval.Count > 0 Then
                        lblRequestedBy.Text = objApproval(0).UserName
                        lblRequestedDate.Text = objApproval(0).RequestedDate.GetValueOrDefault.ToString("dd-MMM-yyyy hh:mm")
                        lblAction.Text = objApproval(0).Mode

                        'Load new data
                        Using objApprovalDetail As TList(Of CDDLNP_WorkflowParameter_ApprovalDetail) = DataRepository.CDDLNP_WorkflowParameter_ApprovalDetailProvider.GetPaged("FK_CDDLNP_WorkflowParameter_Approval_Id = " & Me.GetPK_CDDLNP_WorkflowParameter_Approval_Id, String.Empty, 0, Int32.MaxValue, 0)
                            If objApprovalDetail.Count > 0 Then
                                lblNewLevel.Text = objApprovalDetail(0).Level
                                lblNewGroupWorkflow.Text = objApprovalDetail(0).GroupWorkflow
                                Me.SetnGetPK_CDDLNP_WorkflowParameter_id = objApprovalDetail(0).PK_CDDLNP_WorkflowParameter_id
                            End If
                        End Using

                        'Load old data
                        Using objCDDLNP_WorkflowParameter As CDDLNP_WorkflowParameter = DataRepository.CDDLNP_WorkflowParameterProvider.GetByPK_CDDLNP_WorkflowParameter_id(Me.SetnGetPK_CDDLNP_WorkflowParameter_id)
                            If objCDDLNP_WorkflowParameter IsNot Nothing Then
                                lblOldLevel.Text = objCDDLNP_WorkflowParameter.Level
                                lblOldGroupWorkflow.Text = objCDDLNP_WorkflowParameter.GroupWorkflow
                            End If
                        End Using

                        If lblNewGroupWorkflow.Text <> lblOldGroupWorkflow.Text Then
                            lblNewGroupWorkflow.ForeColor = Drawing.Color.Red
                            lblOldGroupWorkflow.ForeColor = Drawing.Color.Red
                        End If

                        If lblNewLevel.Text <> lblOldLevel.Text Then
                            lblNewLevel.ForeColor = Drawing.Color.Red
                            lblOldLevel.ForeColor = Drawing.Color.Red
                        End If

                        Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                            Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                            AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                        End Using

                    Else 'Handle jika ID tidak ditemukan
                        Me.Response.Redirect("CDDLNPWorkflowParameterApproval.aspx", False)
                    End If
                End Using
            End If
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub

    Protected Sub ImageReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click
        Dim objTransManager As New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)

        Try
            objTransManager.BeginTransaction()

            'Delete Approval
            DataRepository.CDDLNP_WorkflowParameter_ApprovalProvider.Delete(objTransManager, Me.GetPK_CDDLNP_WorkflowParameter_Approval_Id)
            'Delete Approval Detail
            DataRepository.CDDLNP_WorkflowParameter_ApprovalDetailProvider.Delete(objTransManager, DataRepository.CDDLNP_WorkflowParameter_ApprovalDetailProvider.GetPaged("FK_CDDLNP_WorkflowParameter_Approval_Id = " & Me.GetPK_CDDLNP_WorkflowParameter_Approval_Id, String.Empty, 0, Int32.MaxValue, 0))

            objTransManager.Commit()

            Me.Response.Redirect("CDDLNPMessage.aspx?ID=10222", False)
        Catch ex As Exception
            objTransManager.Rollback()
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        Finally
            If Not objTransManager Is Nothing Then
                objTransManager.Dispose()
                objTransManager = Nothing
            End If
        End Try
    End Sub

    Protected Sub ImageAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click
        Dim objTransManager As New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)
        
        Try
            objTransManager.BeginTransaction()

            Using objCDDLNP_WorkflowParameter As CDDLNP_WorkflowParameter = DataRepository.CDDLNP_WorkflowParameterProvider.GetByPK_CDDLNP_WorkflowParameter_id(Me.SetnGetPK_CDDLNP_WorkflowParameter_id)
                If objCDDLNP_WorkflowParameter IsNot Nothing Then
                    objCDDLNP_WorkflowParameter.Level = lblNewLevel.Text
                    objCDDLNP_WorkflowParameter.GroupWorkflow = lblNewGroupWorkflow.Text

                    'Save New Data
                    DataRepository.CDDLNP_WorkflowParameterProvider.Save(objTransManager, objCDDLNP_WorkflowParameter)
                End If
            End Using

            'Delete Approval
            DataRepository.CDDLNP_WorkflowParameter_ApprovalProvider.Delete(objTransManager, Me.GetPK_CDDLNP_WorkflowParameter_Approval_Id)
            'Delete Approval Detail
            DataRepository.CDDLNP_WorkflowParameter_ApprovalDetailProvider.Delete(objTransManager, DataRepository.CDDLNP_WorkflowParameter_ApprovalDetailProvider.GetPaged("FK_CDDLNP_WorkflowParameter_Approval_Id = " & Me.GetPK_CDDLNP_WorkflowParameter_Approval_Id, String.Empty, 0, Int32.MaxValue, 0))

            objTransManager.Commit()

            Me.Response.Redirect("CDDLNPMessage.aspx?ID=10223", False)
        Catch ex As Exception
            objTransManager.Rollback()
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        Finally
            If Not objTransManager Is Nothing Then
                objTransManager.Dispose()
                objTransManager = Nothing
            End If
        End Try
    End Sub
End Class