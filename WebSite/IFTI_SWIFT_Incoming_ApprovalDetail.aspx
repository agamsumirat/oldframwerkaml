﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="IFTI_SWIFT_Incoming_ApprovalDetail.aspx.vb" Inherits="IFTI_SWIFT_ApprovalDetail" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
    <div id="divcontent" class="divcontent">
    <table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
          <tr>
              <td bgcolor="White">
                <ajax:AjaxPanel ID="AjaxPanel1" runat="server" Width="100%">
                    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                        <asp:View ID="ViewSwiftInAppDet" runat="server">
                                <table id="TblSearchSWIFTIncoming" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
                                        style="border-top-style: none; border-right-style: none; border-left-style: none;
                                        border-bottom-style: none" width="100%">
                                        <tr>
                                            <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                                                border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                <img src="Images/dot_title.gif" width="17" height="17" />
                                                <strong>
                                                    <asp:Label ID="Label5" runat="server" Text="IFTI Swift Incoming - Approval Detail "></asp:Label>
                                                </strong>
                                                <hr />
                                             </td>                                      
                                        </tr>
                                        <tr>
                                            <td>
                                                <ajax:AjaxPanel ID="AjaxPanel2" runat="server" Width="885px" meta:resourcekey="AjaxPanel1Resource1">
																	<table style="height: 100%">
																		<tr>
																			<td colspan="3" style="height: 7px">
																			</td>
																		</tr>
																		<tr>
																			<td nowrap style="height: 26px; width: 107px;">
																				<asp:Label ID="LabelMsUser_StaffName" runat="server" Text="User"></asp:Label>
																			</td>
																			<td style="width: 19px;">
																				:
																			</td>
																			<td style="height: 26px; width: 765px;">
																				<asp:TextBox ID="txtMsUser_StaffName" runat="server" CssClass="searcheditbox" TabIndex="2"
																					Width="308px" Enabled="False"></asp:TextBox>
																			</td>
																		</tr>
																		<tr>
																			<td nowrap style="height: 26px; width: 107px;">
																				<asp:Label ID="LabelRequestedDate" runat="server" Text="Requested Date"></asp:Label>
																			</td>
																			<td style="width: 19px;">
																				:
																			</td>
																			<td style="height: 26px; width: 765px;">
																				<asp:TextBox ID="txtRequestedDate1" runat="server" CssClass="textBox" MaxLength="1000"
																					TabIndex="2" Width="308px" ToolTip="RequestedDate" Enabled="False"></asp:TextBox>
																			</td>
																		</tr>																		
																		<tr>
																			<td colspan="3">
																			</td>
																		</tr>
																		<tr>
																			<td colspan="3">
																				&nbsp;&nbsp;&nbsp;
																				</td>
																		</tr>
																	</table>
																	
											    </ajax:AjaxPanel>
                                            </td>
                                        </tr>
                                </table>
                                <table width="100%">
                                    <tr>
                                        <td class="formtext" align="center" bgcolor="#999999" style="width: 287px">
                                            <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                        </td>
                                        <td class="formtext" align="center" bgcolor="#999999" style="width: 367px">
                                            <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                        </td>
                                        <td class="formtext" align="center" bgcolor="#999999" style="width: 383px">
                                            <asp:Label ID="Label3" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                        </td>
                                    </tr>                                  
                                </table>
                                <table id="UMUM" align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                        bgcolor="#dddddd" border="2">
                                        <tr>
                                            <td background="Images/search-bar-background.gif" valign="middle" align="left"
                                                style="height: 6px; width: 100%;" width="100%">
                                                <table cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td class="formtext">
                                                            <asp:Label ID="Label25" runat="server" Text="A. Umum" Font-Bold="True"></asp:Label>
                                                            </td>
                                                        <td>
                                                            <a href="#" onclick="javascript:ShowHidePanel('SwiftIN_Umum','Img13','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                title="click to minimize or maximize">
                                                                <img id="Img13" src="Images/search-bar-minimize.gif" border="0" height="12px"
                                                                    width="12px" /></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr id="SwiftIN_Umum">
                                                <td valign="top" bgcolor="#ffffff" style="height: 125px; width: 100%;" 
                                                    width="100%">
                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                     <tr>
                                        <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                            <asp:Label ID="Label9" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                        </td>
                                        <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                            <asp:Label ID="Label23" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                        </td>
                                        <td class="formtext" align="left" bgcolor="#999999" style="width: 20%">
                                            <asp:Label ID="Label30" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                        </td>
                                    </tr> 
                                             <tr>
                                                <td style="width: 275px; background-color: #fff7e6;">
                                                    <asp:Label ID="Label27" runat="server" Text="a. No. LTDLN "></asp:Label><asp:Label
                                                        ID="Label28" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                <td style="width: 367px;">
                                                    <asp:TextBox ID="SwiftInUmum_LTDNOld" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                        TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>  
                                                <td style="width: 387px">
                                                    <asp:TextBox ID="SwiftInUmum_LTDNNew" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                        TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>                                                                                          
                                             </tr>
                                             <tr>
                                                <td style="width: 275px; background-color: #fff7e6;">
                                                    <asp:Label ID="Label50" runat="server" Text="b. No. LTDLN Koreksi " 
                                                        Width="102px"></asp:Label></td>
                                                <td style="width: 367px">
                                                    <asp:TextBox ID="SwiftInUmum_LtdnKoreksiOld" runat="server" 
                                                        CssClass="searcheditbox" MaxLength="50"
                                                        TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                <td style="width: 387px">
                                                    <asp:TextBox ID="SwiftInUmum_LtdnKoreksiNew" runat="server" 
                                                        CssClass="searcheditbox" MaxLength="50"
                                                        TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>                                                                                            
                                             </tr>
                                             <tr>
                                                <td style="width: 275px; background-color: #fff7e6;">
                                                    <asp:Label ID="Label66" runat="server" Text="c. Tanggal Laporan "></asp:Label><asp:Label
                                                        ID="Label67" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                <td style="width: 367px">
                                                    <asp:TextBox ID="SwiftInUmum_TanggalLaporanOld" runat="server" CssClass="searcheditbox"
                                                        TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                <td style="width: 387px">
                                                    <asp:TextBox ID="SwiftInUmum_TanggalLaporanNew" runat="server" 
                                                        CssClass="searcheditbox" MaxLength="50"
                                                        TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>                                                                                       
                                             </tr>
                                             <tr>
                                                <td style="width: 275px; background-color: #fff7e6;">
                                                    <asp:Label ID="Label68" runat="server" Text="d. Nama PJK Bank Pelapor " 
                                                        Width="127px"></asp:Label>
                                                    <asp:Label ID="Label72" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                <td style="width: 367px">
                                                    <asp:TextBox ID="SwiftInUmum_NamaPJKBankOld" runat="server" 
                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" 
                                                        Enabled="False"></asp:TextBox>
                                                </td>
                                                <td style="width: 387px">
                                                    <asp:TextBox ID="SwiftInUmum_NamaPJKBankNew" runat="server" 
                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" 
                                                        Enabled="False"></asp:TextBox>
                                                </td>                                                                                           
                                             </tr>
                                             <tr>
                                                <td style="width: 275px; background-color: #fff7e6;">
                                                    <asp:Label ID="Label73" runat="server" Text="e. Nama Pejabat PJK Bank Pelapor" 
                                                        Width="167px"></asp:Label>
                                                    <asp:Label ID="Label74" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                <td style="width: 367px;">
                                                    <asp:TextBox ID="SwiftInUmum_NamaPejabatPJKBankOld" runat="server" 
                                                        CssClass="searcheditbox" MaxLength="50"
                                                        TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td> 
                                                <td style="width: 387px">
                                                    <asp:TextBox ID="SwiftInUmum_NamaPejabatPJKBankNew" runat="server" 
                                                        CssClass="searcheditbox" MaxLength="50"
                                                        TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                </td>                                                                                           
                                             </tr>
                                             <tr>
                                                <td style="width: 275px; background-color: #fff7e6;">
                                                    <asp:Label ID="Label75" runat="server" Text="f. Jenis Laporan"></asp:Label>
                                                    <asp:Label ID="Label76" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                <td style="width: 367px" align="left">
                                                    <asp:RadioButtonList ID="RbSwiftInUmum_JenisLaporanOld" runat="server" 
                                                        RepeatColumns="2" RepeatDirection="Horizontal" Enabled="False">
                                                        <asp:ListItem Value="1">Baru</asp:ListItem>
                                                        <asp:ListItem Value="2">Recall</asp:ListItem>
                                                        <asp:ListItem Value="3">Koreksi</asp:ListItem>
                                                        <asp:ListItem Value="4">Reject</asp:ListItem>
                                                    </asp:RadioButtonList></td>
                                                 <td align="left" style="width: 387px">
                                                    <asp:RadioButtonList ID="RbSwiftInUmum_JenisLaporanNew" runat="server" 
                                                         RepeatColumns="2" RepeatDirection="Horizontal" Enabled="False">
                                                        <asp:ListItem Value="1">Baru</asp:ListItem>
                                                        <asp:ListItem Value="2">Recall</asp:ListItem>
                                                        <asp:ListItem Value="3">Koreksi</asp:ListItem>
                                                        <asp:ListItem Value="4">Reject</asp:ListItem>
                                                    </asp:RadioButtonList></td>                                                                                            
                                             </tr>
                                             <tr>
                                                <td style="width: 275px">
                                                </td>
                                                <td style="width: 367px">
                                                </td>
                                                <td style="width: 387px"></td>                                                                                            
                                             </tr>
                                                </table>
                                            </td>
                                            </tr>
                                            </table>
                                <table id="PENGIRIM" align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                        bgcolor="#dddddd" border="2">
                                        <tr>
                                            <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                style="height: 6px">
                                                <table cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td class="formtext">
                                                            <asp:Label ID="Label10" runat="server" Text="B. Identitas Pengirim" 
                                                                Font-Bold="True"></asp:Label>
                                                            </td>
                                                        <td>
                                                            <a href="#" onclick="javascript:ShowHidePanel('BSwiftInIdentitasPengirim','Img8','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                title="click to minimize or maximize">
                                                            <img id="Img8" src="Images/search-bar-minimize.gif" border="0" height="12px"
                                                                    width="12px" /></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr id="BSwiftInIdentitasPengirim">
                                            <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                </table>
                                                            <table width="100%">
                                                             <tr>
                                        <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                            <asp:Label ID="Label11x" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                        </td>
                                        <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                            <asp:Label ID="Label21x" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                        </td>
                                        <td class="formtext" align="center" bgcolor="#999999" style="width: 20%">
                                            <asp:Label ID="Label31x" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                        </td>
                                    </tr> 
                                                                <tr>
                                                                    <td style="width: 5%; background-color: #fff7e6; height: 27px;">
                                                                        <asp:Label ID="Label190" runat="server" Text="No. Rek " Width="42px"></asp:Label>
                                                                        </td>
                                                                    <td style="width: 5%; height: 27px;" align="left">
                                                                        <asp:TextBox ID="txtSwiftInPengirim_rekeningOld" runat="server" 
                                                                            CssClass="searcheditbox" MaxLength="50"
                                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                    <td style="height: 27px;" align="left">
                                                                        <asp:TextBox ID="txtSwiftInPengirim_rekeningNew" runat="server" 
                                                                            CssClass="searcheditbox" MaxLength="50"
                                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trSwiftOutPenerimaTipePengirim" runat="server">
                                                                    <td style="width: 5%; background-color: #fff7e6; height: 27px;">
                                                                        <asp:Label ID="Label192" runat="server" Text="       "></asp:Label>
                                                                        <asp:Label ID="Label193" runat="server" Text="       Tipe Pengirim"></asp:Label>
                                                                        <asp:Label ID="Label194" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 5%; height: 21px;" align="left">
                                                                        <ajax:AjaxPanel ID="AjaxPanel5" runat="server" Height="16px">
                                                                            <asp:RadioButtonList ID="RBPenerimaNasabah_TipePengirimOld" runat="server" 
                                                                            RepeatDirection="Horizontal" AutoPostBack="True" Width="195px" Enabled="False">
                                                                            <asp:ListItem Value="1">Nasabah</asp:ListItem>
                                                                            <asp:ListItem Value="2">Non Nasabah</asp:ListItem>
                                                                        </asp:RadioButtonList></ajax:AjaxPanel></td>
                                                                    <td style="height: 21px;" align="left">
                                                                        <ajax:AjaxPanel ID="AjaxPanel3" runat="server" Height="16px">
                                                                            <asp:RadioButtonList ID="RBPenerimaNasabah_TipePengirimNew" runat="server" 
                                                                            RepeatDirection="Horizontal" AutoPostBack="True" Width="195px" Enabled="False">
                                                                            <asp:ListItem Value="1">Nasabah</asp:ListItem>
                                                                            <asp:ListItem Value="2">Non Nasabah</asp:ListItem>
                                                                        </asp:RadioButtonList></ajax:AjaxPanel>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trSwiftOutPenerimaTipeNasabah" runat="server" visible="False">
                                                                    <td style="width: 5%; height: 27px; background-color: #fff7e6">
                                                                        <asp:Label ID="Label9256" runat="server" Text="Tipe Nasabah "></asp:Label>
                                                                        <asp:Label ID="Label9257" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 5%; height: 34px" align="left">
                                                                        <ajax:AjaxPanel ID="AjaxPanel779" runat="server">
                                                                            <asp:RadioButtonList ID="Rb_IdenPenerimaNas_TipeNasabahOld" runat="server" 
                                                                                AutoPostBack="True" RepeatDirection="Horizontal" Enabled="False">
                                                                                <asp:ListItem Value="1">Perorangan</asp:ListItem>
                                                                                <asp:ListItem Value="2">Korporasi</asp:ListItem>
                                                                            </asp:RadioButtonList>
                                                                        </ajax:AjaxPanel>
                                                                    </td>
                                                                    <td style="height: 34px" align="left">
                                                                        <ajax:AjaxPanel ID="AjaxPanel4" runat="server">
                                                                            <asp:RadioButtonList ID="Rb_IdenPenerimaNas_TipeNasabahNew" runat="server" 
                                                                                AutoPostBack="True" RepeatDirection="Horizontal" Enabled="False">
                                                                                <asp:ListItem Value="1">Perorangan</asp:ListItem>
                                                                                <asp:ListItem Value="2">Korporasi</asp:ListItem>
                                                                            </asp:RadioButtonList>
                                                                        </ajax:AjaxPanel>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                <asp:MultiView ID="MultiViewSwiftInIdenPengirim" runat="server">
                                                    <asp:View ID="ViewSwiftInIdenPengirimNasabah" runat="server">
                                                    
                                                    
                                     <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                            bgcolor="#dddddd" border="2">
                                        <tr>
                                            <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                style="height: 6px">
                                                
                                                <table cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td class="formtext">
                                                            <asp:Label ID="Label11" runat="server" Text="Nasabah" Font-Bold="True"></asp:Label>
                                                            </td>
                                                        <td>
                                                            <a href="#" onclick="javascript:ShowHidePanel('BSwiftInIdentitasPengirim1nasabah','Imgs9','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                title="click to minimize or maximize">
                                                                <img id="Imgs9" src="Images/search-bar-minimize.gif" border="0" height="12px"
                                                                    width="12px" /></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                                
                                            </td>
                                        </tr>
                                        
                                        
                                        
                                        
                                        
                                        <tr id="BSwiftInIdentitasPengirim1nasabah">
                                            <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                            
                                            
                                           <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                    
                                                    <tr>
                                                        <td style="width: 50%">
                                                        
                                                        <!--untuk yang Pengirim OLD -->
                                                        
                                                            <asp:MultiView ID="MultiViewPengirimNasabah_OLD" runat="server" ActiveViewIndex="0">
                                                                
                                                                <asp:View ID="ViewPengirimNasabah_IND_OLD" runat="server">
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td style="width: 5%; height: 15px; background-color: #fff7e6">
                                                                                <asp:Label ID="Label195" runat="server" Text="Perorangan " Font-Bold="True"></asp:Label>
                                                                                </td>
                                                                            <td style="height: 15px; width: 5%;">
                                                                            </td>
                                                                   
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="formtext" align="center" bgcolor="#999999" style="width: 287px">
                                                                                <asp:Label ID="Label65" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                                            </td>
                                                                            <td class="formtext" align="center" bgcolor="#999999" style="width: 367px">
                                                                                <asp:Label ID="Label69" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                                                            </td>
                                                                            
                                                                            
                                                                            
                                                                        </tr> 
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label197" runat="server" Text="a. Nama Lengkap"></asp:Label>
                                                                                <asp:Label ID="Label125" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                            <td style="width: 5%">
                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_namaOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="326px" Enabled="False"></asp:TextBox></td>
                                                                                                                                                     
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label198" runat="server" Text="b. Tanggal lahir (tgl/bln/thn)" Width="183px"></asp:Label></td>
                                                                            <td style="width: 5%">
                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_TanggalLahirOld" runat="server" 
                                                                                    CssClass="searcheditbox" TabIndex="2"
                                                                                    Width="122px" Enabled="False"></asp:TextBox>
                                                                                </td>
                                                                            
                                                                        </tr>                                                                          
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label200" runat="server" Text="c. Kewarganegaraan (Pilih salah satu) "
                                                                                    Width="184px"></asp:Label></td>
                                                                            <td style="width: 5%" align="left">
                                                                                <asp:RadioButtonList ID="RbSwiftInPengirimNasabah_IND_WargaNegaraOld" 
                                                                                    runat="server" RepeatDirection="Horizontal" Enabled="False">
                                                                                    <asp:ListItem Value="1">WNI</asp:ListItem>
                                                                                    <asp:ListItem Value="2">WNA</asp:ListItem>
                                                                                </asp:RadioButtonList></td>
                                                                            
                                                                                                                                                      
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label201" runat="server" Text="d. Negara "></asp:Label>
                                                                                <br />
                                                                                <asp:Label ID="Label202" runat="server" Font-Italic="True" Text="(Wajib diisi apabila kewarganegaraan WNA)"
                                                                                    Width="217px"></asp:Label></td>
                                                                            <td style="width: 5%" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_negaraOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                <asp:HiddenField ID="hfSwiftInPengirimNasabah_IND_negaraOld" runat="server" />
                                                                            </td>
                                                                           
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label203" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                            <td style="width: 5%">
                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_negaraLainOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                          
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label214" runat="server" Text="E. Alamat"></asp:Label></td>
                                                                            <td style="width: 5%">
                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_alamatIdenOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="318px" Enabled="False"></asp:TextBox></td>
                                                                     
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label205" runat="server" Text="Negara Bagian/ Kota"></asp:Label></td>
                                                                            <td style="width: 5%" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_KotaOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                <asp:HiddenField ID="hfSwiftInPengirimNasabah_IND_KotaOld" 
                                                                                    runat="server" />
                                                                            </td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; height: 18px; background-color: #fff7e6">
                                                                                <asp:Label ID="Label204o" runat="server" Text="Negara"></asp:Label>
                                                                                <asp:Label ID="Label9317o" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                            </td>
                                                                            <td style="height: 18px; width: 5%;" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_negaraIdenOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                <asp:HiddenField ID="hfSwiftInPengirimNasabahINDnegaraIdenOld" runat="server" />
                                                                            </td>
                                       
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                                                <asp:Label ID="Label206" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                            <td style="height: 18px; width: 5%;">
                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_negaraLainIdenOld" 
                                                                                    runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                     TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                           
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label7" runat="server" Text="f. No Telp"></asp:Label></td>
                                                                            <td style="width: 5%">
                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_NoTelpOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" 
                                                                                    Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                           
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label220" runat="server" Text="g. Jenis Dokument Identitas"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 5%">
                                                                                <asp:DropDownList ID="CBOSwiftInPengirimNasabah_IND_JenisIdenOld" 
                                                                                    runat="server" Enabled="False">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                          
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                                                <asp:Label ID="Label221" runat="server" Text="Nomor Identitas"></asp:Label></td>
                                                                            <td style="height: 18px; width: 5%;">
                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_NomorIdenOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6; height: 15px;">
                                                                            </td>
                                                                            <td style="height: 15px; width: 5%;">
                                                                            </td>
                                                                          
                                                                        </tr>
                                                                    </table>
                                                                </asp:View>
                                                                
                                                                
                                                                
                                                                <asp:View ID="ViewPengirimNasabah_Korp_OLD" runat="server">
                                                                    <table style="height: 31px" width="100%">
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6; ">
                                                                                <asp:Label ID="Label8" runat="server" Text="Korporasi" Font-Bold="True"></asp:Label>
                                                                                </td>
                                                                            <td style="height: 15px; color: #000000; width: 5%;">
                                                                            </td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="formtext" align="center" bgcolor="#999999" style="width: 287px">
                                                                                <asp:Label ID="Label71" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                                            </td>
                                                                            <td class="formtext" align="center" bgcolor="#999999" style="width: 367px">
                                                                                <asp:Label ID="Label78" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                                                            </td>
                                                                       
                                                                        </tr> 
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6; ">
                                                                                <asp:Label ID="Label12" runat="server" Text="a. Nama Korporasi" Width="120px" 
                                                                                    Height="16px"></asp:Label>
                                                                                <asp:Label ID="Label9325" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                </td>
                                                                            <td style="width: 5%">
                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_Korp_namaKorpOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                        
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6;">
                                                                                <asp:Label ID="Label16" runat="server" Text="b. Alamat Lengkap"></asp:Label></td>
                                                                            <td style="width: 5%">
                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_Korp_AlamatLengkapOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="286px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                         
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6; ">
                                                                                <asp:Label ID="Label207" runat="server" Text="Negara Bagian/ Kota"></asp:Label></td>
                                                                            <td style="width: 5%" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_Korp_KotaOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                <asp:HiddenField ID="hfSwiftInPengirimNasabah_Korp_KotaOld" runat="server" />
                                                                            </td>
                                                                                                                                                   
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6;">
                                                                                <asp:Label ID="Label208" runat="server" Text="Negara"></asp:Label>
                                                                                <asp:Label ID="Label9324" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                </td>
                                                                            <td style="width: 5%; height: 18px">
                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_Korp_NegaraOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                <asp:HiddenField ID="hfSwiftInPengirimNasabah_Korp_NegaraOld" runat="server" />
                                                                            </td>
                                                                          
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6;">
                                                                                <asp:Label ID="Label209" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                            <td style="height: 18px; width: 5%;">
                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_Korp_NegaraLainnyaKorpOld" 
                                                                                    runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                    
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6;">
                                                                                <asp:Label ID="Label17" runat="server" 
                                                                                    Text="c. No Telp"></asp:Label>
                                                                            </td>
                                                                            <td style="height: 18px; width: 5%;">
                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_Korp_NoTelpOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" 
                                                                                    Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                                                                                               
                                                                        </tr>
                                                                    </table>
                                                                </asp:View>
                                                            </asp:MultiView>
                                                            
                                                            
                                                            </td>
                                                            
                                                       
                                                <!-- xEnd pengirim OLD-->
                                                
                                                
                                                
                                                 <td style="width: 50%">
                                                        
                                                        <!--untuk yang Pengirim New -->
                                                        
                                                            <asp:MultiView ID="MultiViewPengirimNasabah_New" runat="server" ActiveViewIndex="0">
                                                                
                                                  
                                                                
                                                                <asp:View ID="ViewPengirimNasabah_IND_New" runat="server">
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td style="width: 5%; height: 15px; background-color: #fff7e6">
                                                                                <asp:Label ID="Label88" runat="server" Text="Perorangan " Font-Bold="True"></asp:Label>
                                                                                </td>
                                                                            <td style="height: 15px; width: 5%;">
                                                                            </td>
                                                                      
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="formtext" align="center" bgcolor="#999999" style="width: 287px">
                                                                                <asp:Label ID="Label89" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                                            </td>
                                                                            <td class="formtext" align="center" bgcolor="#999999" style="width: 367px">
                                                                                <asp:Label ID="Label90" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                                                            </td>
                                                                      
                                                                        </tr> 
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label92" runat="server" Text="a. Nama Lengkap"></asp:Label>
                                                                                <asp:Label ID="Label94" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                           <td style="width: 20%" align="left">
                                                                            <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_namaNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="326px" Enabled="False"></asp:TextBox>
                                                                            </td>  
                                                                                                                                                    
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label95" runat="server" Text="b. Tanggal lahir (tgl/bln/thn)" Width="183px"></asp:Label></td>
                                                                         <td style="width: 20%" align="left">
                                                                            <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_TanggalLahirNew" runat="server" 
                                                                                    CssClass="searcheditbox" TabIndex="2"
                                                                                    Width="122px" Enabled="False"></asp:TextBox>
                                                                                &nbsp;</td>
                                                                            
                                                                        </tr>                                                                          
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label96" runat="server" Text="c. Kewarganegaraan (Pilih salah satu) "
                                                                                    Width="184px"></asp:Label></td>
                                                                            <td style="width: 5%" align="left">
                                                                              <asp:RadioButtonList ID="RbSwiftInPengirimNasabah_IND_WargaNegaraNew" 
                                                                                    runat="server" RepeatDirection="Horizontal" Enabled="False">
                                                                                    <asp:ListItem Value="1">WNI</asp:ListItem>
                                                                                    <asp:ListItem Value="2">WNA</asp:ListItem>
                                                                                </asp:RadioButtonList>
                                                                            </td> 
                                                                                                                                                       
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label97" runat="server" Text="d. Negara "></asp:Label>
                                                                                <br />
                                                                                <asp:Label ID="Label98" runat="server" Font-Italic="True" Text="(Wajib diisi apabila kewarganegaraan WNA)"
                                                                                    Width="217px"></asp:Label></td>
                                                                             <td style="width: 20%" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_negaraNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                <asp:HiddenField ID="hfSwiftInPengirimNasabah_IND_negaraNew" runat="server" />
                                                                            </td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label99" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                           <td style="width: 20%" align="left">
                                                                            <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_negaraLainNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label100" runat="server" Text="E. Alamat"></asp:Label></td>
                                                                          <td style="width: 20%" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_alamatIdenNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="318px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                             
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label101" runat="server" Text="Negara Bagian/ Kota"></asp:Label></td>
                                                                            
                                                                            
                                                                          <td style="width: 20%" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_KotaNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                <asp:HiddenField ID="hfSwiftInPengirimNasabah_IND_KotaNew" 
                                                                                    runat="server" />
                                                                            </td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; height: 18px; background-color: #fff7e6">
                                                                                <asp:Label ID="Label103" runat="server" Text="Negara"></asp:Label>
                                                                                <asp:Label ID="Label104" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                            </td>
                                                                              <td style="width: 20%" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_negaraIdenNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                <asp:HiddenField ID="hfSwiftInPengirimNasabahINDnegaraIdenNew" runat="server" />
                                                                            </td>
                                                          
                                                          
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                                                <asp:Label ID="Label105" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                            <td align="left">
                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_negaraLainIdenNew" 
                                                                                    runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                     TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                        
                                                        
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label106" runat="server" Text="f. No Telp"></asp:Label></td>
                                                                           <td style="width: 20%" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_NoTelpNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" 
                                                                                    Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                          
                                                                          
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label107" runat="server" Text="g. Jenis Dokument Identitas"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 20%" align="left">
                                                                                <asp:DropDownList ID="CBOSwiftInPengirimNasabah_IND_JenisIdenNew" 
                                                                                    runat="server" Enabled="False">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                                                <asp:Label ID="Label108" runat="server" Text="Nomor Identitas"></asp:Label></td>
                                                                           <td style="width: 20%; height: 18px;" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_IND_NomorIdenNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6; height: 15px;">
                                                                            </td>
                                                                            <td style="height: 15px; width: 5%;">
                                                                            </td>
                                                                            
                                                                        </tr>
                                                                    </table>
                                                                </asp:View>
                                                                
                                                                
                                                                
                                                                
                                                                <asp:View ID="ViewPengirimNasabah_Korp_New" runat="server">
                                                                    <table style="height: 31px" width="100%">
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6; ">
                                                                                <asp:Label ID="Label70" runat="server" Text="Korporasi" Font-Bold="True"></asp:Label>
                                                                                </td>
                                                                            <td style="height: 15px; color: #000000; width: 5%;">
                                                                            </td>
                                                         
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="formtext" align="center" bgcolor="#999999" style="width: 287px">
                                                                                <asp:Label ID="Label81" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                                            </td>
                                                                              <td class="formtext" align="center" bgcolor="#999999" style="width: 383px">
                                                                                <asp:Label ID="Label91" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                                                            </td>
                                                                           
                                                                        </tr> 
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6; ">
                                                                                <asp:Label ID="Label109" runat="server" Text="a. Nama Korporasi" Width="120px" 
                                                                                    Height="16px"></asp:Label>
                                                                                <asp:Label ID="Label110" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                </td>
                                                                                
                                                                               <td style="width: 20%">
                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_Korp_namaKorpNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6;">
                                                                                <asp:Label ID="Label111" runat="server" Text="b. Alamat Lengkap"></asp:Label></td>
                                                                             <td style="width: 20%">
                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_Korp_AlamatLengkapNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="286px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6; ">
                                                                                <asp:Label ID="Label112" runat="server" Text="Negara Bagian/ Kota"></asp:Label></td>
                                                                              <td style="width: 20%">
                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_Korp_KotaNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                <asp:HiddenField ID="hfSwiftInPengirimNasabah_Korp_KotaNew" runat="server" />
                                                                            </td>                                                                             
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6;">
                                                                                <asp:Label ID="Label113" runat="server" Text="Negara"></asp:Label>
                                                                                <asp:Label ID="Label114" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                </td>
                                                                             <td style="width: 20%">
                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_Korp_NegaraNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                <asp:HiddenField ID="hfSwiftInPengirimNasabah_Korp_NegaraNew" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6;">
                                                                                <asp:Label ID="Label115" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                             <td>
                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_Korp_NegaraLainnyaKorpNew" 
                                                                                    runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6;">
                                                                                <asp:Label ID="Label116" runat="server" 
                                                                                    Text="c. No Telp"></asp:Label>
                                                                            </td>
                                                                             <td>
                                                                                <asp:TextBox ID="txtSwiftInPengirimNasabah_Korp_NoTelpNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" 
                                                                                    Enabled="False"></asp:TextBox>
                                                                            </td>                                                                         
                                                                        </tr>
                                                                    </table>
                                                                </asp:View>
                                                            </asp:MultiView>
                                                            
                                                            
                                                            </td>        
                                                            
                                                            
                                                    </tr>
                                                </table>
                                                 
                                                
                                            </td> 
                                        
                                            
                                        </tr>
                                           </table> 
                                           
                                            
                                           
                                           
                                      </asp:View>
                                                    
                                                    
                                                    
                                                    <asp:View ID="ViewSwiftInIdenPengirimNonNasabah" runat="server">
                                                            <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                                  bgcolor="#dddddd" border="2">
                                        <tr>
                                            <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                style="height: 6px">
                                                <table cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td class="formtext">
                                                            <asp:Label ID="Label18" runat="server" Text="Non Nasabah" Font-Bold="True"></asp:Label>
                                                            </td>
                                                        <td>
                                                            <a href="#" onclick="javascript:ShowHidePanel('BSwiftInIdentitasPengirimNonNasabah','Imgs10','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                title="click to minimize or maximize">
                                                                <img id="Imgs10" src="Images/search-bar-minimize.gif" border="0" height="12px"
                                                                    width="12px" /></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr id="BSwiftInIdentitasPengirimNonNasabah">
                                            <td valign="top" bgcolor="#ffffff" style="height: 125px" width="100%">
                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">                                             
                                                </table>
                                                <table style="height: 49px" width="100%">
                                                <tr>
                                        <td class="formtext" align="center" bgcolor="#999999" style="width: 287px">
                                            <asp:Label ID="Label82" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                        </td>
                                        <td class="formtext" align="center" bgcolor="#999999" style="width: 367px">
                                            <asp:Label ID="Label83o" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                        </td>
                                        <td class="formtext" align="center" bgcolor="#999999" style="width: 383px">
                                            <asp:Label ID="Label84o" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                        </td>
                                    </tr> 
                                                    <tr>
                                                        <td style="width: 5%; background-color: #fff7e6">
                                                            <asp:Label ID="Label163o" runat="server" Text="a. No. Rek "></asp:Label>
                                                            </td>
                                                        <td style="width: 5%">
                                                            <asp:TextBox ID="txtSwiftInPengirimNonNasabah_rekeningOld" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                        <td style="width: 20%" align="left">
                                                            <asp:TextBox ID="txtSwiftInPengirimNonNasabah_rekeningNew" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 5%; background-color: #fff7e6">
                                                            <asp:Label ID="Label9272o" runat="server" Text="b. Nama Bank"></asp:Label>
                                                        </td>
                                                        <td style="width: 5%">
                                                            <asp:TextBox ID="txtSwiftInPengirimNonNasabah_namabankOld" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" 
                                                                Enabled="False"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 20%" align="left">
                                                            <asp:TextBox ID="txtSwiftInPengirimNonNasabah_namabankNew" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" 
                                                                Enabled="False"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 5%; background-color: #fff7e6">
                                                            <asp:Label ID="Label1146" runat="server" Text="c. Nama Lengkap"></asp:Label>
                                                            <asp:Label ID="Label1147" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                        <td style="width: 5%">
                                                            <asp:TextBox ID="txtSwiftInPengirimNonNasabah_NamaOld" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                        <td style="width: 20%" align="left">
                                                            <asp:TextBox ID="txtSwiftInPengirimNonNasabah_NamaNew" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 5%; background-color: #fff7e6">
                                                            <asp:Label ID="Label210" runat="server" 
                                                                Text="d. TanggalLahir" Height="14px" Width="78px"></asp:Label>
                                                            </td>
                                                        <td style="width: 5%">
                                                            <asp:TextBox ID="txtSwiftInPengirimNonNasabah_TanggalLahirOld" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="236px" Enabled="False"></asp:TextBox></td>
                                                        <td style="width: 20%" align="left">
                                                            <asp:TextBox ID="txtSwiftInPengirimNonNasabah_TanggalLahirNew" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="236px" Enabled="False"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 5%; background-color: #fff7e6">
                                                            <asp:Label ID="Label1150" runat="server" Text="e. Alamat"></asp:Label></td>
                                                        <td style="width: 5%">
                                                            <asp:TextBox ID="txtSwiftInPengirimNonNasabah_AlamatOld" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="238px" Height="16px" Enabled="False"></asp:TextBox></td>
                                                        <td style="width: 20%" align="left">
                                                            <asp:TextBox ID="txtSwiftInPengirimNonNasabah_AlamatNew" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="238px" Height="16px" Enabled="False"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 5%; background-color: #fff7e6">
                                                            <asp:Label ID="Label9316" runat="server" Text="Negara Bagian/ Kota"></asp:Label>
                                                        </td>
                                                        <td style="width: 5%" align="left">
                                                            <asp:TextBox ID="txtSwiftInPengirimNonNasabah_KotaOld" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                            <asp:HiddenField ID="hfSwiftInPengirimNonNasabah_KotaOld" runat="server" />
                                                        </td>
                                                        <td style="width: 20%" align="left">
                                                            <asp:TextBox ID="txtSwiftInPengirimNonNasabah_KotaNew" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                            <asp:HiddenField ID="hfSwiftInPengirimNonNasabah_KotaNew" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 5%; background-color: #fff7e6">
                                                            <asp:Label ID="Label211" runat="server" Text="Negara"></asp:Label>
                                                            <asp:Label ID="Label165" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                        </td>
                                                        <td style="width: 5%" align="left">
                                                            <asp:TextBox ID="txtSwiftInPengirimNonNasabah_NegaraOld" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" 
                                                                Enabled="False"></asp:TextBox>
                                                            <asp:HiddenField ID="hfSwiftInPengirimNonNasabah_NegaraOld" runat="server" />
                                                        </td>
                                                        <td style="width: 20%" align="left">
                                                            <asp:TextBox ID="txtSwiftInPengirimNonNasabah_NegaraNew" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" 
                                                                Enabled="False"></asp:TextBox>
                                                            <asp:HiddenField ID="hfSwiftInPengirimNonNasabah_NegaraNew" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 5%; background-color: #fff7e6">
                                                            <asp:Label ID="Label212" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                        <td style="width: 5%">
                                                            <asp:TextBox ID="txtSwiftInPengirimNonNasabah_negaraLainOld" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                        <td style="width: 20%" align="left">
                                                            <asp:TextBox ID="txtSwiftInPengirimNonNasabah_negaraLainNew" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 5%; background-color: #fff7e6">
                                                            <asp:Label ID="Label9274" runat="server" 
                                                                Text="f. No Telp"></asp:Label>
                                                        </td>
                                                        <td style="width: 5%">
                                                            <asp:TextBox ID="txtSwiftInPengirimNonNasabah_NoTelpOld" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" 
                                                                Enabled="False"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 20%" align="left">
                                                            <asp:TextBox ID="txtSwiftInPengirimNonNasabah_NoTelpNew" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" 
                                                                Enabled="False"></asp:TextBox>
                                                        </td>                                                        
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                                            </table>
                                                    </asp:View>
                                                </asp:MultiView></td>
                                        </tr>
                                </table>
                                <table id="PENERIMA" align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                        bgcolor="#dddddd" border="2">
                                        <tr id="C1SwiftInidentitasPENERIMA">
                                            <td valign="top" bgcolor="#ffffff" style="height: 125px; width: 100%;" 
                                                width="100%">
                                                <table style="width: 100%">
                                                        <tr>
                                                            <td background="Images/search-bar-background.gif" valign="middle" align="left"
                                                                style="height: 11px; width: 100%;" width="100%">
                                                                <table cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td class="formtext">
                                                                            <asp:Label ID="Label31" runat="server" Text="C. Identitas Penerima" 
                                                                                       Font-Bold="True"></asp:Label>
                                                                            <a href="#" 
                                                                                onclick="javascript:ShowHidePanel('CSwiftInidentitasPENERIMA','Img1E4','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')" 
                                                                                title="click to minimize or maximize">
                                                                            <img id="Img1" src="Images/search-bar-minimize.gif" border="0" height="12px"
                                                                                 width="12px" />
                                                                            </a>
                                                                        </td>
                                                                        <td>
                                                                            &nbsp;</td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3" >
                                                                <ajax:AjaxPanel ID="AjaxPanel780" runat="server" 
                                                                            meta:resourcekey="AjaxPanel4Resource1" Width="100%">
                                                                            <asp:DataGrid ID="GV" runat="server" AllowCustomPaging="True" 
                                                                                AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" 
                                                                                BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" 
                                                                                CellPadding="3" CellSpacing="2" Font-Bold="False" Font-Italic="False" 
                                                                                Font-Overline="False" Font-Size="XX-Small" Font-Strikeout="False" 
                                                                                Font-Underline="False" HorizontalAlign="Left" 
                                                                                meta:resourcekey="GridMsUserViewResource1" SkinID="gridview" Width="100%">
                                                                                <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                                                                                <SelectedItemStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                                                                                <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" Mode="NumericPages" 
                                                                                    Visible="False" />
                                                                                <AlternatingItemStyle BackColor="White" />
                                                                                <ItemStyle BackColor="#FFF7E7" ForeColor="#8C4510" HorizontalAlign="Left" 
                                                                                    VerticalAlign="Top" />
                                                                                <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" 
                                                                                    HorizontalAlign="Left" VerticalAlign="Middle" Width="200px" Wrap="False" />
                                                                                <Columns>
                                                                                    <asp:BoundColumn HeaderText="No">
                                                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                            Font-Strikeout="False" Font-Underline="False" ForeColor="White" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="Beneficiary_Nasabah_INDV_NoRekening" 
                                                                                        HeaderText="No. Rekening ( Perorangan )" 
                                                                                        SortExpression="Beneficiary_Nasabah_INDV_NoRekening asc"></asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="Beneficiary_Nasabah_INDV_NamaBank" 
                                                                                        HeaderText="Nama Bank ( Perorangan )" 
                                                                                        SortExpression="Beneficiary_Nasabah_INDV_NamaBank asc"></asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="Beneficiary_Nasabah_INDV_NamaLengkap" 
                                                                                        HeaderText="Nama Lengkap ( Perorangan )" 
                                                                                        SortExpression="Beneficiary_Nasabah_INDV_NamaLengkap asc"></asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="Beneficiary_Nasabah_INDV_TanggalLahir" 
                                                                                        HeaderText="Tanggal Lahir ( Perorangan )" 
                                                                                        SortExpression="Beneficiary_Nasabah_INDV_TanggalLahir asc">
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="Beneficiary_Nasabah_CORP_NoRekening" 
                                                                                        HeaderText="No. Rekening ( Korporasi )" 
                                                                                        SortExpression="Beneficiary_Nasabah_CORP_NoRekening asc"></asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="Beneficiary_Nasabah_CORP_NamaKorporasi" 
                                                                                        HeaderText="Nama Korporasi ( Korporasi )" 
                                                                                        SortExpression="Beneficiary_Nasabah_CORP_NamaKorporasi asc">
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="Beneficiary_Nasabah_CORP_AlamatLengkap" 
                                                                                        HeaderText="Alamat Lengkap ( Korporasi )" 
                                                                                        SortExpression="Beneficiary_Nasabah_CORP_AlamatLengkap asc">
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="Beneficiary_Nasabah_CORP_NoTelp" 
                                                                                        HeaderText="No. Telp ( Korporasi )" 
                                                                                        SortExpression="Beneficiary_Nasabah_CORP_NoTelp asc"></asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="Beneficiary_NonNasabah_NoRekening" 
                                                                                        HeaderText="No. Rekening ( Non Nasabah )" 
                                                                                        SortExpression="Beneficiary_NonNasabah_NoRekening asc"></asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="Beneficiary_NonNasabah_KodeRahasia" 
                                                                                        HeaderText="Kode Rahasia ( Non Nasabah )" 
                                                                                        SortExpression="Beneficiary_NonNasabah_KodeRahasia asc"></asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="Beneficiary_NonNasabah_NamaBank" 
                                                                                        HeaderText="Nama Bank ( Non Nasabah )" 
                                                                                        SortExpression="Beneficiary_NonNasabah_NamaBank asc"></asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="Beneficiary_NonNasabah_NamaLengkap" 
                                                                                        HeaderText="Nama Lengakap ( Non Nasabah )" 
                                                                                        SortExpression="Beneficiary_NonNasabah_NamaLengkap asc"></asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="Beneficiary_NonNasabah_TanggalLahir" 
                                                                                        HeaderText="Tanggal lahir ( Non Nasabah )" 
                                                                                        SortExpression="Beneficiary_NonNasabah_TanggalLahir asc"></asp:BoundColumn>
                                                                                    <asp:EditCommandColumn EditText="Detail"></asp:EditCommandColumn>
                                                                                    <asp:BoundColumn DataField="PK_IFTI_Approval_Beneficiary_ID"></asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="FK_IFTI_Beneficiary_ID" Visible="False">
                                                                                    </asp:BoundColumn>
                                                                                </Columns>
                                                                            </asp:DataGrid>
                                                                </ajax:AjaxPanel>
                                                                            <asp:Label ID="LabelNoRecordFound" runat="server" CssClass="text" 
                                                                                meta:resourcekey="LabelNoRecordFoundResource1" 
                                                                                Text="No record match with your criteria" Visible="False">
                                                                            </asp:Label>
                                                            </td>
                                                        </tr>                                                
                                                </table>
                                                <asp:MultiView ID ="DataPenerima" runat ="server" >
                                                <asp:View ID ="vw12" runat ="server" >
                                                    <table>
                                                    <tr>
                                        <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                            <asp:Label ID="Label85" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                        </td>
                                        <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                            <asp:Label ID="Label86" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                        </td>
                                        <td class="formtext" align="left" bgcolor="#999999" style="width: 20%">
                                            <asp:Label ID="Label87" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                        </td>
                                    </tr> 
                                                        <tr>
                                                            <td style="width: 5%" bgcolor="#FFF7E6">
                                                                <asp:Label ID="Label51" runat="server" Text="Tipe PJK Bank" Width="96px"></asp:Label>
                                                            </td>
                                                            <td style="border: thin dashed silver; width: 5%; " 
                                                                align="left">
                                                                <ajax:AjaxPanel ID="AjaxPanel6" runat="server" Width="100%">
                                                                    <asp:DropDownList ID="cboSwiftInPenerimaNonNasabah_TipePJKBankOld" runat="server" 
                                                                        AutoPostBack="True" Enabled="False">
                                                                        <asp:ListItem Value="0">Penyelenggara Penerima Akhir</asp:ListItem>
                                                                        <asp:ListItem Value="1">Penyelenggara Penerus</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </ajax:AjaxPanel>
                                                            </td>
                                                            <td style="width: 20%; border-right: silver thin dashed; border-top: silver thin dashed; border-left: silver thin dashed; border-bottom: silver thin dashed;" 
                                                                align="left">
                                                                <ajax:AjaxPanel ID="AjaxPanel16" runat="server" Width="100%">
                                                                    <asp:DropDownList ID="cboSwiftInPenerimaNonNasabah_TipePJKBankNew" runat="server" 
                                                                        AutoPostBack="True" Enabled="False">
                                                                        <asp:ListItem Value="0">Penyelenggara Penerima Akhir</asp:ListItem>
                                                                        <asp:ListItem Value="1">Penyelenggara Penerus</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </ajax:AjaxPanel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3">
                                                                <ajax:AjaxPanel ID="AjaxPanel9" runat="server" Width="100%">
                                                <asp:MultiView ID="MultiViewSwiftInPenerima" runat="server">
                                                    <asp:View ID="ViewSwiftInPenerimaAkhir" runat="server">
                                                        <table width="100%">
                                                        <tr>
                                                            <td background="Images/search-bar-background.gif" valign="middle" align="left"
                                                                style="height: 11px; width: 100%;" width="100%">
                                                                <table cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td class="formtext">
                                                                            <asp:Label ID="Label49" runat="server" Text="C.1  Identitas Penerima" 
                                                                                       Font-Bold="True"></asp:Label>
                                                                            <a href="#" 
                                                                                onclick="javascript:ShowHidePanel('C1SwiftInidentitasPENERIMA','Img1E4','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')" 
                                                                                title="click to minimize or maximize">
                                                                            <img id="Img1E4" src="Images/search-bar-minimize.gif" border="0" height="12px"
                                                                                 width="12px" />
                                                                            </a>
                                                                        </td>
                                                                        <td>
                                                                            &nbsp;</td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        </table>
                                                        <table width="100%">
                                                        
                                                        <tr id="trSwiftInTipeNasabah" runat="server" visible="false">
                                                        <td style="width: 5%" bgcolor="#FFF7E6">
                                                            <asp:Label ID="Label54" runat="server" Text="Tipe Nasabah "></asp:Label>
                                                        </td>
                                                        <td style="border: thin dashed silver; width: 5%; " 
                                                            align="left">
                                                            <ajax:AjaxPanel ID="AjaxPanel8" runat="server">
                                                                <asp:RadioButtonList ID="RbSwiftInPenerimaNonNasabah_TipeNasabahOld" 
                                                                    runat="server" AutoPostBack="True" RepeatDirection="Horizontal" 
                                                                    Enabled="False">
                                                                    <asp:ListItem Value="1">Perorangan</asp:ListItem>
                                                                    <asp:ListItem Value="2">Korporasi</asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </ajax:AjaxPanel>
                                                        </td>
                                                        <td style="width: 20%; border-right: silver thin dashed; border-top: silver thin dashed; border-left: silver thin dashed; border-bottom: silver thin dashed;" 
                                                            align="left">
                                                            <ajax:AjaxPanel ID="AjaxPanel15" runat="server">
                                                                <asp:RadioButtonList ID="RbSwiftInPenerimaNonNasabah_TipeNasabahNew" 
                                                                    runat="server" AutoPostBack="True" RepeatDirection="Horizontal" 
                                                                    Enabled="False">
                                                                    <asp:ListItem Value="1">Perorangan</asp:ListItem>
                                                                    <asp:ListItem Value="2">Korporasi</asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </ajax:AjaxPanel>
                                                        </td>
                                                    </tr>
                                                        <tr id="trSwiftInTipepengirim" runat="server" visible="false">
                                                        <td style="width: 5%" bgcolor="#FFF7E6">
                                                            <asp:Label ID="Label53" runat="server" Text="       Tipe Penerima"></asp:Label>
                                                        </td>
                                                        <td style="border: thin dashed silver; width: 5%; " 
                                                            align="left">
                                                            <ajax:AjaxPanel ID="AjaxPanel7" runat="server">
                                                                <asp:RadioButtonList ID="RbSwiftInPenerimaNonNasabah_TipePenerimaOld" 
                                                                    runat="server" AutoPostBack="True" RepeatDirection="Horizontal" 
                                                                    Width="189px" Enabled="False">
                                                                    <asp:ListItem Value="1">Nasabah</asp:ListItem>
                                                                    <asp:ListItem Value="2">Non Nasabah</asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </ajax:AjaxPanel>
                                                        </td>
                                                        <td style="width: 20%; border-right: silver thin dashed; border-top: silver thin dashed; border-left: silver thin dashed; border-bottom: silver thin dashed;" 
                                                            align="left">
                                                            <ajax:AjaxPanel ID="AjaxPanel12" runat="server">
                                                                <asp:RadioButtonList ID="RbSwiftInPenerimaNonNasabah_TipePenerimaNew" 
                                                                    runat="server" AutoPostBack="True" RepeatDirection="Horizontal" 
                                                                    Width="189px" Enabled="False">
                                                                    <asp:ListItem Value="1">Nasabah</asp:ListItem>
                                                                    <asp:ListItem Value="2">Non Nasabah</asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </ajax:AjaxPanel>
                                                        </td>
                                                    </tr>
                                                        </table>
                                                        <asp:MultiView ID="MultiViewSwiftInPenerimaAkhir" runat="server">
                                                            <asp:View ID="ViewSwiftInPenerimaNasabah" runat="server">
                                                             <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                                    bgcolor="#dddddd" border="2" id="TABLE3" ">
                                         <tr>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                style="height: 6px">
                                                <table cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td class="formtext" style="height: 14px">
                                                            <asp:Label ID="Label56" runat="server" Text="Nasabah" Font-Bold="True"></asp:Label>
                                                            </td>
                                                        <td style="height: 14px">
                                                            <a href="#" onclick="javascript:ShowHidePanel('B1sWIFTiNidentitasPENerima1nasabah','Img1e5','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                title="click to minimize or maximize">
                                                                <img id="Img1e5" src="Images/search-bar-minimize.gif" border="0" height="12"
                                                                    width="12" /></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr id="B1sWIFTiNidentitasPENerima1nasabah">
                                            <td valign="top" bgcolor="#ffffff" style="height: 125px" width="100%">
                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                <tr>
                                                    <td colspan ="2">
                                                        <table style="width: 100%">
                                                         <tr>
                                                                <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                                    <asp:Label ID="Label32" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                                </td>
                                                                <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                                    <asp:Label ID="Label33" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                                                </td>
                                                                <td class="formtext" align="left" bgcolor="#999999" style="width: 20%">
                                                                    <asp:Label ID="Label34" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                                                </td>
                                                            </tr> 
                                                            <tr>
                                                                <td style="width: 5%; background-color: #fff7e6">
                                                                    <asp:Label ID="Label77" runat="server" Text="No. Rek " Width="48px"></asp:Label>
                                                                    <asp:Label ID="Label79" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                </td>
                                                                 <td style="width: 5%">
                                                                     <asp:TextBox ID="txtSwiftInPenerimaNasabah_IND_RekeningOld" runat="server" 
                                                                                  CssClass="searcheditbox" MaxLength="50"
                                                                                  TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                 </td>
                                                                 <td style="width: 20%">
                                                                     <asp:TextBox ID="txtSwiftInPenerimaNasabah_IND_RekeningNew" runat="server" 
                                                                                  CssClass="searcheditbox" MaxLength="50"
                                                                                  TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    
                                                </tr>
                                                 <tr>
                                                    <td width="50%">
                                                        <asp:MultiView ID="MultiViewPenerimaAkhirNasabah_OLD" runat="server" ActiveViewIndex="0">                                                       
                                                            <asp:View ID="VwSwiftIn_PEN_IND_OLD" runat="server">
                                                             <ajax:AjaxPanel ID="AjaxPanel10" runat="server" Width="100%">
                                                                   <table style="width: 100%; height: 49px">
                                                                        <tr>
                                                                            <td style="width: 5%; height: 15px; background-color: #fff7e6">
                                                                                <asp:Label ID="Label80" runat="server" Text="Perorangan " Font-Bold="True"></asp:Label>
                                                                                </td>
                                                                            <td style="height: 15px; width: 5%;" align="left">
                                                                            </td>
                                                                             
                                                                        </tr>
                                                                         <tr>
                                                                            <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                                                <asp:Label ID="Label35" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                                            </td>
                                                                            <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                                                <asp:Label ID="Label36" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                                                            </td>
                                                                             
                                                                        </tr> 
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label93" runat="server" Text="a. Nama Lengkap"></asp:Label>
                                                                                <asp:Label ID="Label102" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                            <td style="width: 5%" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaNasabah_IND_namaOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label123" runat="server" Text="b. Tanggal lahir (tgl/bln/thn)" 
                                                                                    Width="135px"></asp:Label>
                                                                                <asp:Label ID="Label137" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                            <td style="width: 5%" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaNasabah_IND_TanggalLahirOld" runat="server" 
                                                                                    CssClass="searcheditbox" TabIndex="2" Width="122px" Enabled="False"></asp:TextBox>
                                                                                </td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label138" runat="server" Text="c. Kewarganegaraan (Pilih salah satu) "
                                                                                    Width="183px"></asp:Label></td>
                                                                            <td style="width: 5%" align="left">
                                                                                <asp:RadioButtonList ID="RbSwiftInPenerimaNasabah_IND_KewarganegaraanOld" 
                                                                                    runat="server" RepeatDirection="Horizontal" Enabled="False">
                                                                                    <asp:ListItem Value="1">WNI</asp:ListItem>
                                                                                    <asp:ListItem Value="2">WNA</asp:ListItem>
                                                                                </asp:RadioButtonList></td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label139" runat="server" Text="d. Negara "></asp:Label>
                                                                                <br />
                                                                                <asp:Label ID="Label142" runat="server" Font-Italic="True" Text="(Wajib diisi apabila kewarganegaraan WNA)"
                                                                                    Width="213px"></asp:Label></td>
                                                                            <td style="width: 5%" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaNasabah_IND_negaraOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                <asp:HiddenField ID="hfSwiftInPenerimaNasabah_IND_negaraOld" runat="server" />
                                                                            </td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                           <td style="width: 5%; background-color: #fff7e6">
                                                                               <asp:Label ID="Label143" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                           <td style="width: 362px" align="left">
                                                                               <asp:TextBox ID="txtSwiftInPenerimaNasabah_IND_negaralainOld" runat="server" 
                                                                                   CssClass="searcheditbox" MaxLength="50"
                                                                                   TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                           
                                                                       </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Labelq144" runat="server" Text="e. Pekerjaan"></asp:Label>
                                                                                <asp:Label ID="Labelq145" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                            <td style="width: 362px" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaNasabah_IND_pekerjaanOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                <asp:HiddenField ID="hfSwiftInIdenPenerimaNas_Ind_pekerjaanOld" runat="server" />
                                                                            </td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                           <td style="width: 5%; background-color: #fff7e6">
                                                                               <asp:Label ID="Labelq45" runat="server" Text="Pekerjaan Lainnya"></asp:Label></td>
                                                                           <td style="width: 362px" align="left">
                                                                                <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_PekerjaanlainOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                           
                                                                       </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Labelq37" runat="server" Text="f. Alamat Domisili"></asp:Label></td>
                                                                            <td style="width: 362px" align="left">
                                                                                </td>
                                                                             
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6; height: 20px;">
                                                                                <asp:Label ID="Labelq38" runat="server" Text="Alamat"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 5%; height: 20px;" align="left">
                                                                                <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_AlamatOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; height: 18px; background-color: #fff7e6">
                                                                                <asp:Label ID="Labelq39" runat="server" Text="Kota / Kabupaten "></asp:Label></td>
                                                                            <td style="height: 18px; width: 5%;" align="left">
                                                                                <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_KotaOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                <asp:HiddenField ID="hfSwiftInIdenPenerimaNas_Ind_Kota_domOld" runat="server" />
                                                                            </td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                           <td style="width: 5%; height: 18px; background-color: #fff7e6">
                                                                               <asp:Label ID="Labelq57" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                           <td style="width: 5%; height: 18px" align="left">
                                                                                <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_kotalainOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                           
                                                                       </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Labelq41" runat="server" Text="Provinsi"></asp:Label></td>
                                                                            <td style="width: 362px" align="left">
                                                                               <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_provinsiOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                   TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                <asp:HiddenField ID="hfSwiftInIdenPenerimaNas_Ind_provinsi_domOld" 
                                                                                    runat="server" />
                                                                            </td>
                                                                                                                                                   
                                                                        </tr>
                                                                        <tr>
                                                                           <td style="width: 5%; background-color: #fff7e6">
                                                                               <asp:Label ID="Labelq186o" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                           <td style="width: 362px" align="left">
                                                                               <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_provinsiLainOld" runat="server" 
                                                                                   CssClass="searcheditbox" MaxLength="50"
                                                                                   TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                           
                                                                       </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6; height: 22px;">
                                                                                <asp:Label ID="Labelq42o" runat="server" Text="g. Alamat Sesuai Bukti Identitas"></asp:Label></td>
                                                                            <td style="width: 5%; height: 22px;" align="left">
                                                                                <%-- <ajax:AjaxPanel ID="AjaxPanelCheckPengirimNas_Ind_alamatOld" runat="server">--%>
                                                                                <asp:CheckBox ID="CheckSwiftInPenerimaNas_Ind_alamatOld" runat="server" 
                                                                                    Text="Sama dengan Alamat Domisili" AutoPostBack="True" Enabled="False" />                                                                                    
                                                                                   <%--</ajax:AjaxPanel>--%></td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                           <td style="width: 5%; background-color: #fff7e6">
                                                                               <asp:Label ID="Labelq43o" runat="server" Text="Alamat"></asp:Label>
                                                                               <asp:Label ID="Labelq267o" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                           <td style="width: 362px" align="left">
                                                                               <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_alamatIdentitasOld" runat="server" 
                                                                                   CssClass="searcheditbox" MaxLength="50"
                                                                                   TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                           
                                                                       </tr>
                                                                        <tr>
                                                                           <td style="width: 5%; background-color: #fff7e6">
                                                                               <asp:Label ID="Labelq44o" runat="server" Text="Kota / Kabupaten"></asp:Label>
                                                                               <asp:Label ID="Labelq268o" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                           <td style="width: 362px" align="left">
                                                                               <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_kotaIdentitasOld" runat="server" 
                                                                                   CssClass="searcheditbox" MaxLength="50"
                                                                                   TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                               <asp:HiddenField ID="hfSwiftInIdenPenerimaNas_Ind_kotaIdentitasOld" 
                                                                                   runat="server" />
                                                                           </td>
                                                                           
                                                                       </tr>
                                                                        <tr>
                                                                           <td style="width: 5%; height: 18px; background-color: #fff7e6">
                                                                               <asp:Label ID="Labeql83o" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                           <td style="width: 5%; height: 18px" align="left">
                                                                               <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_KotaLainIdenOld" runat="server" 
                                                                                   CssClass="searcheditbox" MaxLength="50"
                                                                                   TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                           
                                                                       </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                                                <asp:Label ID="Labeql46o" runat="server" Text="Provinsi"></asp:Label>
                                                                                <asp:Label ID="Labelq269o" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                            <td style="height: 18px; width: 5%;" align="left">
                                                                                <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_ProvinsiIdenOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                <asp:HiddenField ID="hfSwiftInIdenPenerimaNas_Ind_ProvinsiIdenOld" 
                                                                                    runat="server" />
                                                                            </td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                           <td style="width: 5%; height: 18px; background-color: #fff7e6">
                                                                               <asp:Label ID="Labelq62o" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                           <td style="width: 5%; height: 18px" align="left">
                                                                               <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_ProvinsilainIdenOld" runat="server" 
                                                                                   CssClass="searcheditbox" MaxLength="50"
                                                                                   TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                           
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; height: 18px; background-color: #fff7e6">
                                                                                <asp:Label ID="Label9307o" runat="server" Text="h. No Telp"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 5%; height: 18px" align="left">
                                                                                <asp:TextBox ID="TxtISwiftIndenPenerimaNas_Ind_noTelpOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" 
                                                                                    Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Labelq48o" runat="server" Text="i. Jenis Dokument Identitas"></asp:Label>
                                                                                <asp:Label ID="Labelq270o" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                            <td style="width: 362px" align="left">
                                                                                <asp:DropDownList ID="cbo_SwiftInpenerimaNas_Ind_jenisidentitasOld" 
                                                                                    runat="server" Enabled="False">
                                                                                </asp:DropDownList></td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                                                <asp:Label ID="Labelq49o" runat="server" Text="Nomor Identitas"></asp:Label>
                                                                                <asp:Label ID="LabelIq271o" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                            <td style="height: 18px; width: 5%;" align="left">
                                                                                <asp:TextBox ID="TxtISwiftIndenPenerimaNas_Ind_noIdentitasOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                                                <asp:Label ID="Label9270o" runat="server" 
                                                                                    Text="j. Nilai Transaksi Keuangan (dalam Rupiah)"></asp:Label>
                                                                            </td>
                                                                            <td style="height: 18px; width: 5%;" align="left">
                                                                                <asp:TextBox ID="txtSwiftInIdenPenerimaNas_IND_NilaiTransaksiKeuanganOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" 
                                                                                    Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6; height: 15px;">
                                                                            </td>
                                                                            <td style="height: 15px; width: 5%;" align="left">
                                                                            </td>
                                                                            <td style="width: 349px; height: 15px;">
                                                                            </td>
                                                                        </tr>
                                                                   </table>
                                                              </ajax:AjaxPanel>
                                                            </asp:View>
                                                            <asp:View ID="VwSwiftIn_PEN_Corp_OLD" runat="server">
                                                                    <table style="height: 31px" width="100%">
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6;">
                                                                                <asp:Label ID="Label223o" runat="server" Text="Korporasi" Font-Bold="True"></asp:Label>
                                                                                </td>
                                                                            <td style="height: 15px; color: #000000; width: 5%;" align="left">
                                                                            </td>
                                                                            
                                                                        </tr>
                                                                         <tr>
                                                                            <td class="formtext" align="center" bgcolor="#999999" style="width: 287px">
                                                                                <asp:Label ID="Label38o" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                                            </td>
                                                                            <td class="formtext" align="center" bgcolor="#999999" style="width: 367px">
                                                                                <asp:Label ID="Label39o" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                                                            </td>
                                                                          
                                                                        </tr> 
                                                                        <tr style="color: #000000">
                                                                            <td style="width: 5%; background-color: #fff7e6;">
                                                                                <asp:Label ID="Label225o" runat="server" Text="a. Bentuk Badan Usaha "></asp:Label></td>
                                                                            <td style="width: 5%" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_BentukBadanUsahaOld" 
                                                                                    runat="server" CssClass="searcheditbox"
                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6;">
                                                                                <asp:Label ID="Label226o" runat="server" Text="Bentuk Badan Usaha Lainnya " 
                                                                                    Width="139px"></asp:Label></td>
                                                                            <td style="width: 5%" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLainnyaOld" 
                                                                                    runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6;">
                                                                                <asp:Label ID="Label227o" runat="server" Text="b. Nama Korporasi" Width="88px"></asp:Label>
                                                                                <asp:Label ID="Label149o" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                            <td style="width: 5%" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_namaKorpOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6;">
                                                                                <asp:Label ID="Label228o" runat="server" Text="c. Bidang Usaha Korporasi"></asp:Label>
                                                                                <asp:Label ID="Label9308o" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 5%" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_BidangUsahaKorpOld" 
                                                                                    runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>                                                                                
                                                                                <asp:HiddenField ID="hfSwiftInPenerimaNasabah_Korp_BidangUsahaOld" runat="server" />                                                                                
                                                                            </td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6;">
                                                                                <asp:Label ID="Label22o" runat="server" Text="Bidang Usaha Lainnya" Width="110px"></asp:Label>
                                                                                </td>
                                                                            <td style="width: 5%" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_BidangUsahaLainnyaKorpOld" 
                                                                                    runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6;">
                                                                                <asp:Label ID="Label229o" runat="server" Text="d. Alamat Lengkap Korporasi"></asp:Label></td>
                                                                            <td style="width: 5%" align="left">
                                                                            </td>
                                                                           
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6; ">
                                                                                <asp:Label ID="Label4o" runat="server" Text="Alamat"></asp:Label>
                                                                                <asp:Label ID="Label9319o" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 5%; height: 20px;" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_AlamatKorpOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label6o" runat="server" Text="Kota / Kabupaten "></asp:Label>
                                                                                <asp:Label ID="Label9321o" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                            </td>
                                                                            <td style="height: 18px; width: 5%;" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_KotakabOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                <asp:HiddenField ID="hfSwiftInPenerimaNasabah_Korp_kotakabOld" runat="server" />
                                                                            </td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                           <td style="width: 5%; background-color: #fff7e6">
                                                                               <asp:Label ID="Label13o" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                           <td style="width: 5%; height: 18px" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_kotaLainOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                           
                                                                       </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label14o" runat="server" Text="Provinsi"></asp:Label>
                                                                                <asp:Label ID="Label9320o" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 5%" align="left">
                                                                               <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_propinsiOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                   TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                <asp:HiddenField ID="hfSwiftInPenerimaNasabah_Korp_propinsiOld" 
                                                                                    runat="server" />
                                                                            </td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                           <td style="width: 5%; background-color: #fff7e6">
                                                                               <asp:Label ID="Label15o" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                           <td style="width: 5%" align="left">
                                                                               <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_propinsilainOld" runat="server" 
                                                                                   CssClass="searcheditbox" MaxLength="50"
                                                                                   TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                           
                                                                       </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label9310o" runat="server" Text="e. No. Telp"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 5%" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_NoTelpOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" 
                                                                                    Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6;">
                                                                                <asp:Label ID="Label9271o" runat="server" 
                                                                                    Text="f. Nilai Transaksi Keuangan (dalam Rupiah )"></asp:Label>
                                                                            </td>
                                                                            <td style="height: 18px; width: 5%;" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_NilaiTransaksiKeuanganOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" 
                                                                                    Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                            
                                                                        </tr>
                                                                    </table>
                                                            </asp:View>
                                                        </asp:MultiView>
                                                   </td>
                                                    <td width="50%">
                                                    
                                                    <asp:MultiView ID="MultiViewPenerimaAkhirNasabah_New" runat="server" ActiveViewIndex="0">                                                       
                                                            <asp:View ID="VwSwiftIn_PEN_IND_New" runat="server">
                                                             <ajax:AjaxPanel ID="AjaxPanel17" runat="server" Width="100%">
                                                                   <table style="width: 100%; height: 49px">
                                                                        <tr>
                                                                            <td style="width: 5%; height: 15px; background-color: #fff7e6">
                                                                                <asp:Label ID="Label47" runat="server" Text="Perorangan " Font-Bold="True"></asp:Label>
                                                                                </td>
                                                                            <td style="height: 15px; width: 5%;" align="left">
                                                                            </td>
                                                                             
                                                                        </tr>
                                                                         <tr>
                                                                            <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                                                <asp:Label ID="Label48" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                                            </td>
                                                                            <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                                                <asp:Label ID="Label52" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                                                            </td>
                                                                             
                                                                        </tr> 
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label119" runat="server" Text="a. Nama Lengkap"></asp:Label>
                                                                                <asp:Label ID="Label144" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                            <td style="width: 20%" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaNasabah_IND_namaNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label145" runat="server" Text="b. Tanggal lahir (tgl/bln/thn)" 
                                                                                    Width="135px"></asp:Label>
                                                                                <asp:Label ID="Label146" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                            <td style="width: 20%" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaNasabah_IND_TanggalLahirNew" runat="server" 
                                                                                    CssClass="searcheditbox" TabIndex="2" Width="122px" Enabled="False"></asp:TextBox>
                                                                                &nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label147" runat="server" Text="c. Kewarganegaraan (Pilih salah satu) "
                                                                                    Width="183px"></asp:Label></td>
                                                                            <td style="width: 20%" align="left">
                                                                                <asp:RadioButtonList ID="RbSwiftInPenerimaNasabah_IND_KewarganegaraanNew" 
                                                                                    runat="server" RepeatDirection="Horizontal" Enabled="False">
                                                                                    <asp:ListItem Value="1">WNI</asp:ListItem>
                                                                                    <asp:ListItem Value="2">WNA</asp:ListItem>
                                                                                </asp:RadioButtonList>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label148" runat="server" Text="d. Negara "></asp:Label>
                                                                                <br />
                                                                                <asp:Label ID="Label150" runat="server" Font-Italic="True" Text="(Wajib diisi apabila kewarganegaraan WNA)"
                                                                                    Width="213px"></asp:Label></td>
                                                                            <td style="width: 20%" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaNasabah_IND_negaraNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                <asp:HiddenField ID="hfSwiftInPenerimaNasabah_IND_negaraNew" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                           <td style="width: 5%; background-color: #fff7e6">
                                                                               <asp:Label ID="Label151" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                           <td style="width: 349px" align="left">
                                                                               <asp:TextBox ID="txtSwiftInPenerimaNasabah_IND_negaralainNew" runat="server" 
                                                                                   CssClass="searcheditbox" MaxLength="50"
                                                                                   TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                           </td>
                                                                       </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label152" runat="server" Text="e. Pekerjaan"></asp:Label>
                                                                                <asp:Label ID="Label153" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                            <td style="width: 349px" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaNasabah_IND_pekerjaanNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                <asp:HiddenField ID="hfSwiftInIdenPenerimaNas_Ind_pekerjaanNew" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                           <td style="width: 5%; background-color: #fff7e6">
                                                                               <asp:Label ID="Label154" runat="server" Text="Pekerjaan Lainnya"></asp:Label></td>
                                                                           <td style="width: 349px" align="left">
                                                                                <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_PekerjaanlainNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                           </td>
                                                                       </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label155" runat="server" Text="f. Alamat Domisili"></asp:Label></td>
                                                                            <td style="width: 362px" align="left">
                                                                                </td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6; height: 20px;">
                                                                                <asp:Label ID="Label156" runat="server" Text="Alamat"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 20%; height: 20px;" align="left">
                                                                                <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_AlamatNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; height: 18px; background-color: #fff7e6">
                                                                                <asp:Label ID="Label157" runat="server" Text="Kota / Kabupaten "></asp:Label></td>
                                                                            <td style="height: 18px; width: 20%;" align="left">
                                                                                <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_KotaNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                <asp:HiddenField ID="hfSwiftInIdenPenerimaNas_Ind_Kota_domNew" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                           <td style="width: 5%; height: 18px; background-color: #fff7e6">
                                                                               <asp:Label ID="Label158" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                           <td style="width: 20%; height: 18px" align="left">
                                                                                 <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_kotalainNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                           </td>
                                                                       </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label159" runat="server" Text="Provinsi"></asp:Label></td>
                                                                            <td align="left">
                                                                                <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_provinsiNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                   TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                <asp:HiddenField ID="hfSwiftInIdenPenerimaNas_Ind_provinsi_domNew" 
                                                                                    runat="server" />
                                                                            </td>                                                                          
                                                                        </tr>
                                                                        <tr>
                                                                           <td style="width: 5%; background-color: #fff7e6">
                                                                               <asp:Label ID="Labelq186" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                           <td style="width: 349px" align="left">
                                                                                <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_provinsiLainNew" runat="server" 
                                                                                   CssClass="searcheditbox" MaxLength="50"
                                                                                   TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                           </td>
                                                                       </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6; height: 22px;">
                                                                                <asp:Label ID="Labelq42" runat="server" Text="g. Alamat Sesuai Bukti Identitas"></asp:Label></td>
                                                                            <td style="width: 20%; height: 22px;" align="left">
                                                                                 <%-- <ajax:AjaxPanel ID="AjaxPanelCheckPengirimNas_Ind_alamatNew" runat="server">--%>
                                                                                <asp:CheckBox ID="CheckSwiftInPenerimaNas_Ind_alamatNew" runat="server" 
                                                                                    Text="Sama dengan Alamat Domisili" AutoPostBack="True" Enabled="False" />                                                                                    
                                                                                   <%--</ajax:AjaxPanel>--%>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                           <td style="width: 5%; background-color: #fff7e6">
                                                                               <asp:Label ID="Labelq43" runat="server" Text="Alamat"></asp:Label>
                                                                               <asp:Label ID="Labelq267" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                           <td style="width: 349px" align="left">
                                                                                <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_alamatIdentitasNew" runat="server" 
                                                                                   CssClass="searcheditbox" MaxLength="50"
                                                                                   TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                           </td>
                                                                       </tr>
                                                                        <tr>
                                                                           <td style="width: 5%; background-color: #fff7e6">
                                                                               <asp:Label ID="Labelq44" runat="server" Text="Kota / Kabupaten"></asp:Label>
                                                                               <asp:Label ID="Labelq268" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                           <td style="width: 349px" align="left">
                                                                                <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_kotaIdentitasNew" runat="server" 
                                                                                   CssClass="searcheditbox" MaxLength="50"
                                                                                   TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                               <asp:HiddenField ID="hfSwiftInIdenPenerimaNas_Ind_kotaIdentitasNew" 
                                                                                   runat="server" />
                                                                           </td>
                                                                       </tr>
                                                                        <tr>
                                                                           <td style="width: 5%; height: 18px; background-color: #fff7e6">
                                                                               <asp:Label ID="Labeql83" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                           <td style="width: 349px" align="left">
                                                                                <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_KotaLainIdenNew" runat="server" 
                                                                                   CssClass="searcheditbox" MaxLength="50"
                                                                                   TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                           </td>
                                                                       </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                                                <asp:Label ID="Labeql46" runat="server" Text="Provinsi"></asp:Label>
                                                                                <asp:Label ID="Labelq269" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                            <td align="left">
                                                                                <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_ProvinsiIdenNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                <asp:HiddenField ID="hfSwiftInIdenPenerimaNas_Ind_ProvinsiIdenNew" 
                                                                                    runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                           <td style="width: 5%; height: 18px; background-color: #fff7e6">
                                                                               <asp:Label ID="Labelq62" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                           <td style="width: 20%; height: 18px" align="left">
                                                                                <asp:TextBox ID="TxtSwiftInIdenPenerimaNas_Ind_ProvinsilainIdenNew" runat="server" 
                                                                                   CssClass="searcheditbox" MaxLength="50"
                                                                                   TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                           </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; height: 18px; background-color: #fff7e6">
                                                                                <asp:Label ID="Label9307" runat="server" Text="h. No Telp"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 20%; height: 18px" align="left">
                                                                                 <asp:TextBox ID="TxtISwiftIndenPenerimaNas_Ind_noTelpNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" 
                                                                                     Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Labelq48" runat="server" Text="i. Jenis Dokument Identitas"></asp:Label>
                                                                                <asp:Label ID="Labelq270" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                            <td style="width: 349px" align="left">
                                                                                <asp:DropDownList ID="cbo_SwiftInpenerimaNas_Ind_jenisidentitasNew" 
                                                                                    runat="server" Enabled="False">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                                                <asp:Label ID="Labelq49" runat="server" Text="Nomor Identitas"></asp:Label>
                                                                                <asp:Label ID="LabelIq271" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                            <td style="width: 20%; height: 18px;" align="left">
                                                                                <asp:TextBox ID="TxtISwiftIndenPenerimaNas_Ind_noIdentitasNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                                                <asp:Label ID="Label9270" runat="server" 
                                                                                    Text="j. Nilai Transaksi Keuangan (dalam Rupiah)"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 20%; height: 18px;" align="left">
                                                                                <asp:TextBox ID="txtSwiftInIdenPenerimaNas_IND_NilaiTransaksiKeuanganNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" 
                                                                                    Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6; height: 15px;">
                                                                            </td>
                                                                            <td style="height: 15px; width: 5%;" align="left">
                                                                            </td>
                                                                           
                                                                        </tr>
                                                                   </table>
                                                              </ajax:AjaxPanel>
                                                            </asp:View>
                                                            <asp:View ID="VwSwiftIn_PEN_Corp_New" runat="server">
                                                                    <table style="height: 31px" width="100%">
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6;">
                                                                                <asp:Label ID="Label223" runat="server" Text="Korporasi" Font-Bold="True"></asp:Label>
                                                                                </td>
                                                                            <td style="height: 15px; color: #000000; width: 5%;" align="left">
                                                                            </td>
                                                                             
                                                                        </tr>
                                                                         <tr>
                                                                            <td class="formtext" align="center" bgcolor="#999999" style="width: 287px">
                                                                                <asp:Label ID="Label38" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                                            </td>
                                                                            <td class="formtext" align="center" bgcolor="#999999" style="width: 367px">
                                                                                <asp:Label ID="Label39" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                                                            </td>
                                                                             
                                                                        </tr> 
                                                                        <tr style="color: #000000">
                                                                            <td style="width: 5%; background-color: #fff7e6;">
                                                                                <asp:Label ID="Label225" runat="server" Text="a. Bentuk Badan Usaha "></asp:Label></td>
                                                                            <td style="width: 20%" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_BentukBadanUsahaNew" 
                                                                                    runat="server" CssClass="searcheditbox"
                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6;">
                                                                                <asp:Label ID="Label226" runat="server" Text="Bentuk Badan Usaha Lainnya " 
                                                                                    Width="139px"></asp:Label></td>
                                                                           <td style="width: 20%" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLainnyaNew" 
                                                                                    runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6;">
                                                                                <asp:Label ID="Label227" runat="server" Text="b. Nama Korporasi" Width="88px"></asp:Label>
                                                                                <asp:Label ID="Label149" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                            <td style="width: 20%" align="left">
                                                                                 <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_namaKorpNew" runat="server" 
                                                                                     CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6;">
                                                                                <asp:Label ID="Label228" runat="server" Text="c. Bidang Usaha Korporasi"></asp:Label>
                                                                                <asp:Label ID="Label9308" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 20%" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_BidangUsahaKorpNew" 
                                                                                    runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>                                                                                
                                                                                <asp:HiddenField ID="hfSwiftInPenerimaNasabah_Korp_BidangUsahaNew" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6;">
                                                                                <asp:Label ID="Label22" runat="server" Text="Bidang Usaha Lainnya" Width="110px"></asp:Label>
                                                                                </td>
                                                                            <td style="width: 20%" align="left">
                                                                                 <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_BidangUsahaLainnyaKorpNew" 
                                                                                     runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6;">
                                                                                <asp:Label ID="Label229" runat="server" Text="d. Alamat Lengkap Korporasi"></asp:Label></td>
                                                                            <td style="width: 5%" align="left">
                                                                            </td>
                                                                           
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6; ">
                                                                                <asp:Label ID="Label4" runat="server" Text="Alamat"></asp:Label>
                                                                                <asp:Label ID="Label9319" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                            </td>
                                                                           <td style="width: 20%; height: 20px;" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_AlamatKorpNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label6" runat="server" Text="Kota / Kabupaten "></asp:Label>
                                                                                <asp:Label ID="Label9321" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                            </td>
                                                                            <td style="height: 18px; width: 20%;" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_KotakabNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                <asp:HiddenField ID="hfSwiftInPenerimaNasabah_Korp_kotakabNew" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                           <td style="width: 5%; background-color: #fff7e6">
                                                                               <asp:Label ID="Label13" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                           <td style="width: 20%; height: 18px" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_kotaLainNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                           </td>
                                                                       </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label14" runat="server" Text="Provinsi"></asp:Label>
                                                                                <asp:Label ID="Label9320" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                            </td>
                                                                           <td align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_propinsiNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                   TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                <asp:HiddenField ID="hfSwiftInPenerimaNasabah_Korp_propinsiNew" 
                                                                                    runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                           <td style="width: 5%; background-color: #fff7e6">
                                                                               <asp:Label ID="Label15" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                           <td style="width: 20%" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_propinsilainNew" runat="server" 
                                                                                   CssClass="searcheditbox" MaxLength="50"
                                                                                   TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                           </td>
                                                                       </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label9310" runat="server" Text="e. No. Telp"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 20%" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_NoTelpNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" 
                                                                                    Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6;">
                                                                                <asp:Label ID="Label9271" runat="server" 
                                                                                    Text="f. Nilai Transaksi Keuangan (dalam Rupiah )"></asp:Label>
                                                                            </td>
                                                                            <td align="left">
                                                                                 <asp:TextBox ID="txtSwiftInPenerimaNasabah_Korp_NilaiTransaksiKeuanganNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" 
                                                                                     Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                            </asp:View>
                                                        </asp:MultiView>
                                                     </td>                                                
                                                </tr>                                                                                             
                                                </table>
                                            </td>
                                        </tr>
                                                            </table>
                                                            </asp:View>
                                                            
                                                            <asp:View ID="ViewSwiftInPenerimaNonNasabah" runat="server">
                                                                <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                                       bgcolor="#dddddd" border="2">
                                        <tr>
                                            <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                style="height: 7px">
                                                <table cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td class="formtext" style="height: 14px">
                                                            <asp:Label ID="LabelSwiftIn4" runat="server" Text="Non Nasabah" Font-Bold="True"></asp:Label>
                                                            </td>
                                                        <td style="height: 14px">
                                                            <a href="#" onclick="javascript:ShowHidePanel('B1SwiftInidentitasPENerimaonNasabah','Imgt23','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                title="click to minimize or maximize">
                                                                <img id="Imgt23" src="Images/search-bar-minimize.gif" border="0" height="12"
                                                                    width="12" /></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr id="B1SwiftInidentitasPENerimaonNasabah">
                                            <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">                                             
                                                </table>
                                                <table style="height: 49px; width: 100%;">
                                                 <tr>
                                        <td class="formtext" align="center" bgcolor="#999999" style="width: 287px">
                                            <asp:Label ID="Label41" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                        </td>
                                        <td class="formtext" align="center" bgcolor="#999999" style="width: 367px">
                                            <asp:Label ID="Label42" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                        </td>
                                        <td class="formtext" align="left" bgcolor="#999999" style="width: 383px">
                                            <asp:Label ID="Label43" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                        </td>
                                    </tr> 
                                                    <tr>
                                                        <td style="width: 4%; background-color: #fff7e6">
                                                            <asp:Label ID="Labelq81" runat="server" Text="a. Nama Lengkap"></asp:Label>
                                                            <asp:Label ID="Labelq85" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                        <td style="width: 5%">
                                                            <asp:TextBox ID="TxtSwiftInPenerimaNonNasabah_namaOld" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                        <td style="width: 5%">
                                                            <asp:TextBox ID="TxtSwiftInPenerimaNonNasabah_namaNew" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 4%; background-color: #fff7e6">
                                                            <asp:Label ID="LabelSwiftIn82o" runat="server" 
                                                                Text="b. Tanggal lahir (tgl/bln/thn)" Width="136px"></asp:Label>
                                                            </td>
                                                        <td style="width: 5%">
                                                            <asp:TextBox ID="TxtSwiftInPenerimaNonNasabah_TanggalLahirOld" runat="server" 
                                                                CssClass="searcheditbox" TabIndex="2"
                                                                         Width="125px" Enabled="False"></asp:TextBox>
                                                            </td>
                                                        <td style="width: 5%">
                                                            <asp:TextBox ID="TxtSwiftInPenerimaNonNasabah_TanggalLahirNew" runat="server" 
                                                                CssClass="searcheditbox" TabIndex="2"
                                                                         Width="125px" Enabled="False"></asp:TextBox>
                                                            &nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 4%; background-color: #fff7e6">
                                                            <asp:Label ID="LabelSwiftIn95o" runat="server" Text="c. Alamat Sesuai Bukti Identitas"></asp:Label></td>
                                                        <td style="width: 5%">
                                                            <asp:TextBox ID="TxtSwiftInPenerimaNonNasabah_alamatidenOld" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="341px" 
                                                                Enabled="False"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 5%">
                                                             <asp:TextBox ID="TxtSwiftInPenerimaNonNasabah_alamatidenNew" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="341px" 
                                                                 Enabled="False"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 4%; background-color: #fff7e6">
                                                            <asp:Label ID="LabelSwiftIn84o" runat="server" Text="d. No Telepon"></asp:Label></td>
                                                        <td style="width: 5%;">
                                                            <asp:TextBox ID="TxtSwiftInPenerimaNonNasabah_NoTeleponOld" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" 
                                                                Enabled="False"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 5%;">
                                                            <asp:TextBox ID="TxtSwiftInPenerimaNonNasabah_NoTeleponNew" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" 
                                                                Enabled="False"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 4%; background-color: #fff7e6; height: 24px;">
                                                            <asp:Label ID="LabelSwiftIn100o" runat="server" Text="e. Jenis Dokument Identitas"></asp:Label>
                                                            </td>
                                                        <td style="width: 5%; height: 24px;">
                                                            <asp:DropDownList ID="CboSwiftInPenerimaNonNasabah_JenisDokumenOld" 
                                                                runat="server" Enabled="False">
                                                            </asp:DropDownList></td>
                                                        <td style="width: 5%; height: 24px;">
                                                            <asp:DropDownList ID="CboSwiftInPenerimaNonNasabah_JenisDokumenNew" 
                                                                runat="server" Enabled="False">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 4%; background-color: #fff7e6;">
                                                            <asp:Label ID="LabelSwiftIn101o" runat="server" Text="Nomor Identitas"></asp:Label>
                                                            </td>
                                                        <td style="width: 5%;">
                                                            <asp:TextBox ID="TxtSwiftInPenerimaNonNasabah_NomorIdenOld" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                        <td style="width: 5%;">
                                                            <asp:TextBox ID="TxtSwiftInPenerimaNonNasabah_NomorIdenNew" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 4%; background-color: #fff7e6;">
                                                            <asp:Label ID="Label9311" runat="server" 
                                                                Text="f. Nilai Transaksi Keuangan (dalam Rupiah )"></asp:Label>
                                                        </td>
                                                        <td style="width: 5%;">
                                                            <asp:TextBox ID="TxtSwiftInPenerimaNonNasabah_NilaiTransaksiKeuanganOld" 
                                                                runat="server" CssClass="searcheditbox" MaxLength="50" TabIndex="2" 
                                                                Width="125px" Enabled="False"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 5%;">
                                                            <asp:TextBox ID="TxtSwiftInPenerimaNonNasabah_NilaiTransaksiKeuanganNew" 
                                                                runat="server" CssClass="searcheditbox" MaxLength="50" TabIndex="2" 
                                                                Width="125px" Enabled="False"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                                                </table>
                                                            </asp:View>
                                                        </asp:MultiView>
                                                        </asp:View>
                                                    <asp:View ID="ViewSwiftInPenerimaPenerus" runat="server">
                                                             <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                                    bgcolor="#dddddd" border="2" id="TableSwiftInPenerimaPenerus">
                                        <tr>
                                            <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                style="height: 6px">
                                                <table cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td class="formtext">
                                                            &nbsp;</td>
                                                        <td>
                                                            <a href="#" onclick="javascript:ShowHidePanel('B2SwiftInidentitaspenerima','Imge24','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                title="click to minimize or maximize">
                                                                <img id="Imge24" src="Images/search-bar-minimize.gif" border="0" height="12px"
                                                                    width="12px" /></a>
                                                            <asp:Label ID="Label9322" runat="server" Font-Bold="True" 
                                                                Text="C.2  Identitas Penerima"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                                </td>
                                        </tr>
                                        <tr id="B2SwiftInidentitaspenerima">
                                            <td valign="top" bgcolor="#ffffff" style="height: 125px" width="100%">
                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                             
                                                </table><table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                    <tr>
                                                        <td style="width: 100%" colspan ="2">
                                                            <table style="width: 100%; height: 60px">     
                                                             <tr>
                                                                <td class="formtext" align="center" bgcolor="#999999" style="width: 287px">
                                                                    <asp:Label ID="Label44" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                                </td>
                                                                <td class="formtext" align="center" bgcolor="#999999" style="width: 367px">
                                                                    <asp:Label ID="Label45" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                                                </td>
                                                                <td class="formtext" align="center" bgcolor="#999999" style="width: 383px">
                                                                    <asp:Label ID="Label46" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                                                </td>
                                                            </tr>                                                            
                                                                <tr id="tr2" runat="server" visible="false">
                                                                    <td style="width: 16%; background-color: #fff7e6;">
                                                                        <asp:Label ID="Label29" runat="server" Text="Tipe Nasabah "></asp:Label>
                                                                    </td>
                                                                    <td style="border: thin dashed silver; width: 19%; " 
                                                                        align="left">
                                                                        <ajax:AjaxPanel ID="AjaxPanel13" runat="server">
                                                                        <asp:RadioButtonList ID="RbSwiftInPenerimaPenerusNonNasabah_TipePenerimaOld" 
                                                                                runat="server" AutoPostBack="True" RepeatDirection="Horizontal" 
                                                                                Enabled="False">
                                                                        <asp:ListItem Value="1">Perorangan</asp:ListItem>
                                                                        <asp:ListItem Value="2">Korporasi</asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                        </ajax:AjaxPanel>
                                                                    </td>
                                                                    <td style="width: 20%; border-right: silver thin dashed; border-top: silver thin dashed; border-left: silver thin dashed; border-bottom: silver thin dashed;" 
                                                                        align="left">
                                                                        <ajax:AjaxPanel ID="AjaxPanel11" runat="server">
                                                                        <asp:RadioButtonList ID="RbSwiftInPenerimaPenerusNonNasabah_TipePenerimaNew" 
                                                                                runat="server" AutoPostBack="True" RepeatDirection="Horizontal" 
                                                                                Enabled="False">
                                                                        <asp:ListItem Value="1">Perorangan</asp:ListItem>
                                                                        <asp:ListItem Value="2">Korporasi</asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                        </ajax:AjaxPanel>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 16%; background-color: #fff7e6">
                                                                        <asp:Label ID="LabelSwiftIn70o" runat="server" Text="No. Rek " Width="46px"></asp:Label>
                                                                        <asp:Label ID="LabelSwiftIn71o" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                    <td style="width: 124px">
                                                                        <asp:TextBox ID="TxtSwiftInPenerimaPenerus_RekeningOld" runat="server" 
                                                                            CssClass="searcheditbox" MaxLength="50"
                                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                    <td style="width: 53px">
                                                                        <asp:TextBox ID="TxtSwiftInPenerimaPenerus_RekeningNew" runat="server" 
                                                                            CssClass="searcheditbox" MaxLength="50"
                                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                       <td style="width: 50%">
                                                            <asp:MultiView ID="MultiViewSwiftInPenerimaPenerus_OLD" runat="server" ActiveViewIndex="0">
                                                              <asp:View ID="ViewSwiftInPenerimaPenerus_IND_OLD" runat="server">
                                                                    <table style="width: 100%; height: 49px">
                                                                        <tr>
                                                                            <td style="width: 26%; height: 15px; background-color: #fff7e6">
                                                                                <asp:Label ID="Labelw91o" runat="server" Text="Perorangan " Font-Bold="true"></asp:Label>
                                                                                </td>
                                                                            <td style="height: 15px; width: 238px;">
                                                                            </td>
                                                                           
                                                                        </tr>
                                                                         <tr>
                                                                            <td class="formtext" align="center" bgcolor="#999999" style="width: 287px">
                                                                                <asp:Label ID="Label47o" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                                            </td>
                                                                            <td class="formtext" align="center" bgcolor="#999999" style="width: 367px">
                                                                                <asp:Label ID="Label48o" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                                                            </td>
                                                                            
                                                                        </tr> 
                                                                        <tr>
                                                                            <td style="width: 26%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn9268o" runat="server" Text="a. Nama Bank"></asp:Label>
                                                                                <asp:Label ID="LabelSwiftIn92o" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                            <td style="width: 238px">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaPenerus_Ind_namaBankOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" 
                                                                                    Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn9267o" runat="server" Text="b. Nama Lengkap"></asp:Label>
                                                                                <asp:Label ID="LabelSwiftIn9263o" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 238px">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaPenerus_Ind_namaOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" 
                                                                                    Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                           
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; background-color: #fff7e6; height: 20px;">
                                                                                <asp:Label ID="LabelSwiftIn94o" runat="server" Text="c. Tanggal lahir (tgl/bln/thn)" 
                                                                                    Width="136px"></asp:Label></td>
                                                                            <td style="width: 36px; height: 20px;">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaPenerus_Ind_TanggalLahirOld" runat="server" 
                                                                                    CssClass="searcheditbox" TabIndex="2"
                                                                                    Width="125px" Enabled="False"></asp:TextBox>
                                                                                </td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn104o" runat="server" Text="d. Kewarganegaraan (Pilih salah satu) "
                                                                                    Width="182px"></asp:Label></td>
                                                                            <td style="width: 36px">
                                                                                <asp:RadioButtonList ID="RBSwiftInPenerimaPenerus_Ind_KewarganegaraanOld" runat="server" 
                                                                                    RepeatDirection="Horizontal" Enabled="False">
                                                                                    <asp:ListItem Value="1">WNI</asp:ListItem>
                                                                                    <asp:ListItem Value="2">WNA</asp:ListItem>
                                                                                </asp:RadioButtonList></td>
                                                                           
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn105o" runat="server" Text="e. Negara "></asp:Label>
                                                                                <br />
                                                                                <asp:Label ID="LabelSwiftIn106o" runat="server" Font-Italic="True" Text="(Wajib diisi apabila kewarganegaraan WNA)"
                                                                                    Width="214px"></asp:Label></td>
                                                                            <td style="width: 238px">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_negaraOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                <asp:HiddenField ID="hfSwiftInPenerimaPenerus_IND_negaraOld" runat="server" />
                                                                            </td>
                                                                           
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn107o" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                            <td style="width: 36px">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_negaralainOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn108o" runat="server" Text="f. Pekerjaan"></asp:Label></td>
                                                                            <td style="width: 238px">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_pekerjaanOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                <asp:HiddenField ID="hfSwiftInPenerimaPenerus_IND_pekerjaanOld" runat="server" />
                                                                            </td>
                                                                           
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn109o" runat="server" Text="Pekerjaan Lainnya"></asp:Label></td>
                                                                            <td style="width: 36px">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_pekerjaanLainOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn110o" runat="server" Text="g. Alamat Domisili"></asp:Label></td>
                                                                            <td style="width: 36px">
                                                                            </td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn111o" runat="server" Text="Alamat"></asp:Label></td>
                                                                            <td style="width: 36px">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_alamatDomOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                           
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; height: 18px; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn112o" runat="server" Text="Kota / Kabupaten "></asp:Label></td>
                                                                            <td style="height: 18px; width: 238px;">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_kotaDomOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                <asp:HiddenField ID="hfSwiftInPenerimaPenerus_IND_kotaDomOld" runat="server" />
                                                                            </td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; height: 18px; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn113o" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                            <td style="width: 36px; height: 18px">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_kotaLainDomOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn114o" runat="server" Text="Provinsi"></asp:Label></td>
                                                                            <td style="width: 238px">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_ProvDomOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                <asp:HiddenField ID="hfSwiftInPenerimaPenerus_IND_ProvDomOld" runat="server" />
                                                                            </td>
                                                                           
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn175o" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                            <td style="width: 238px">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_ProvLainDomOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                           
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; background-color: #fff7e6; height: 22px;">
                                                                                <asp:Label ID="LabelSwiftIn115o" runat="server" Text="h. Alamat Sesuai Bukti Identitas"></asp:Label></td>
                                                                            <td style="width: 238px; height: 22px;">
                                                                                <asp:CheckBox ID="CheckBoxSwiftInPenerimaPenerus_IND_samaIdenOld" runat="server" 
                                                                                    Text="Sama dengan Alamat Domisili" AutoPostBack="True" Enabled="False" /></td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn116o" runat="server" Text="Alamat"></asp:Label></td>
                                                                            <td style="width: 238px">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_alamatIdenOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn117o" runat="server" Text="Kota / Kabupaten"></asp:Label></td>
                                                                            <td style="width: 238px">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_kotaIdenOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                <asp:HiddenField ID="hfSwiftInPenerimaPenerus_IND_kotaIdenOld" runat="server" />
                                                                            </td>
                                                                           
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; height: 18px; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn118o" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                            <td style="width: 238px; height: 18px">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_KotaLainIdenOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                           
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; background-color: #fff7e6; height: 18px;">
                                                                                <asp:Label ID="Labelv119o" runat="server" Text="Provinsi"></asp:Label></td>
                                                                            <td style="height: 18px; width: 238px;">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_ProvIdenOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                <asp:HiddenField ID="hfSwiftInPenerimaPenerus_IND_ProvIdenOld" runat="server" />
                                                                            </td>
                                                                           
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; height: 18px; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn120o" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                            <td style="width: 238px; height: 18px">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_ProvLainIdenOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                          
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; height: 18px; background-color: #fff7e6">
                                                                                <asp:Label ID="Label9312o" runat="server" Text="i. No. Telp"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 238px; height: 18px">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_NoTelpOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" 
                                                                                    Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                           
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn121o" runat="server" 
                                                                                    Text="j. Jenis Dokument Identitas"></asp:Label></td>
                                                                            <td style="width: 238px">
                                                                                <asp:DropDownList ID="CBoSwiftInPenerimaPenerus_IND_jenisIdentitasOld" 
                                                                                    runat="server" Enabled="False">
                                                                                </asp:DropDownList></td>
                                                                           
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; background-color: #fff7e6; height: 18px;">
                                                                                <asp:Label ID="LabelSwiftIn122o" runat="server" Text="Nomor Identitas"></asp:Label></td>
                                                                            <td style="height: 18px; width: 238px;">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_nomoridentitasOld" 
                                                                                    runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label9313o" runat="server" 
                                                                                    Text="k. Nilai Transaksi Keuangan (dalam Rupiah )"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 238px">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_NIlaiTransaksiOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" 
                                                                                    Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; background-color: #fff7e6; height: 15px;">
                                                                            </td>
                                                                            <td style="height: 15px; width: 238px;">
                                                                            </td>
                                                                            
                                                                        </tr>
                                                                    </table>
                                                                </asp:View>
                                                                <asp:View ID="ViewSwiftInPenerimaPenerus_Korp_OLD" runat="server">
                                                                    <table style="width: 100%; height: 31px">
                                                                        <tr>
                                                                            <td style="width: 5%; height: 15px; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn124o" runat="server" Text="Korporasi" Font-Bold="True"></asp:Label>
                                                                                </td>
                                                                            <td style="height: 15px; color: #000000; width: 393px;" align="left">
                                                                            </td>
                                                                      
                                                                        </tr>
                                                                         <tr>
                                                                        <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                                            <asp:Label ID="Label55o" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                                        </td>
                                                                        <td class="formtext" align="center" bgcolor="#999999" style="width: 367px">
                                                                            <asp:Label ID="Label57o" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                                                        </td>
                                                                    
                                                                    </tr> 
                                                                        <tr style="color: #000000">
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn9264o" runat="server" Text="a. Nama Bank"></asp:Label>
                                                                                <asp:Label ID="LabelSwiftIn9265o" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 5%" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaPenerus_Korp_namabankOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" 
                                                                                    Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        
                                                                        </tr>
                                                                        <tr style="color: #000000">
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn126o" runat="server" Text="b. Bentuk Badan Usaha "></asp:Label>
                                                                            </td>
                                                                            <td style="width: 5%" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaPenerus_Korp_bentukbadanusahaOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" 
                                                                                    Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn127o" runat="server" Text="Bentuk Badan Usaha Lainnya "
                                                                                    Width="139px"></asp:Label></td>
                                                                            <td style="width: 5%" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaPenerus_Korp_BentukBadanUsahaLainOld" 
                                                                                    runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                           
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn128o" runat="server" Text="c. Nama Korporasi" 
                                                                                    Width="89px"></asp:Label>
                                                                                <asp:Label ID="LabelSwiftIn9266o" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 5%" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaPenerus_Korp_NamaKorpOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn129o" runat="server" Text="d. Bidang Usaha Korporasi"></asp:Label></td>
                                                                            <td style="width: 5%" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaPenerus_Korp_BidangUsahaKorpOld" 
                                                                                    runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                <asp:HiddenField ID="hfSwiftInPenerimaPenerus_Korp_BidangUsahaKorpOld" runat="server" />
                                                                            </td>
                                                                           
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label26o" runat="server" Text="Bidang Usaha Lainnya" 
                                                                                    Width="107px" Height="16px"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 5%" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaPenerus_Korp_BidangUsahaKorplainnyaOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn130o" runat="server" Text="e. Alamat Lengkap Korporasi"></asp:Label></td>
                                                                            <td style="width: 5%" align="left">
                                                                            </td>
                                                                        
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn131o" runat="server" Text="Alamat"></asp:Label></td>
                                                                            <td style="width: 5%" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaPenerus_Korp_alamatkorpOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="342px" Enabled="False"></asp:TextBox></td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn132o" runat="server" Text="Kota / Kabupaten "></asp:Label></td>
                                                                            <td style="width: 5%" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaPenerus_Korp_kotakorpOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                <asp:HiddenField ID="hfSwiftInPenerimaPenerus_Korp_kotakorpOld" runat="server" />
                                                                            </td>
                                                                           
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6;">
                                                                                <asp:Label ID="LabelSwiftIn133o" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                            <td style="width: 5%;" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaPenerus_Korp_kotalainnyakorpOld" 
                                                                                    runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn134o" runat="server" Text="Provinsi"></asp:Label></td>
                                                                            <td style="width: 5%" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaPenerus_Korp_ProvinsiKorpOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                <asp:HiddenField ID="hfSwiftInPenerimaPenerus_Korp_ProvinsiKorpOld" runat="server" />
                                                                            </td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn135o" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                            <td style="width: 5%" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaPenerus_Korp_provinsiLainnyaKorpOld" 
                                                                                    runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn141o" runat="server" Text="f. No Telp"></asp:Label></td>
                                                                            <td style="width: 5%" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaPenerus_Korp_NoTelpOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" 
                                                                                    Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label9314o" runat="server" 
                                                                                    Text="g. Nilai Transaksi Keuangan (dalam Rupiah )"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 5%" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaPenerus_Korp_NilaiTransaksiOld" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                &nbsp;</td>
                                                                            <td style="width: 5%">
                                                                                &nbsp;</td>
                                                                         
                                                                        </tr>
                                                                    </table>
                                                                </asp:View>
                                                            </asp:MultiView>
                                                             </td>
                                                              <td style="width: 50%">
                                                    
                                                      <asp:MultiView ID="MultiViewSwiftInPenerimaPenerus_New" runat="server" ActiveViewIndex="0">
                                                              <asp:View ID="ViewSwiftInPenerimaPenerus_IND_New" runat="server">
                                                                    <table style="width: 100%; height: 49px">
                                                                        <tr>
                                                                            <td style="width: 26%; height: 15px; background-color: #fff7e6">
                                                                                <asp:Label ID="Label117" runat="server" Text="Perorangan " Font-Bold="true"></asp:Label>
                                                                                </td>
                                                                            <td style="height: 15px; width: 238px;">
                                                                            </td>
                                                                             
                                                                        </tr>
                                                                         <tr>
                                                                            <td class="formtext" align="center" bgcolor="#999999" style="width: 287px">
                                                                                <asp:Label ID="Label118" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                                            </td>
                                                                    
                                                                            <td class="formtext" align="center" bgcolor="#999999" style="width: 383px">
                                                                                <asp:Label ID="Label120" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                                                            </td>
                                                                        </tr> 
                                                                        <tr>
                                                                            <td style="width: 26%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label121" runat="server" Text="a. Nama Bank"></asp:Label>
                                                                                <asp:Label ID="Label122" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                            <td style="width: 36px" width="100%" align="left">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaPenerus_Ind_namaBankNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" 
                                                                                    Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label124" runat="server" Text="b. Nama Lengkap"></asp:Label>
                                                                                <asp:Label ID="Label126" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 36px" width="100%" align="left">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaPenerus_Ind_namaNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" 
                                                                                    Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; background-color: #fff7e6; height: 20px;">
                                                                                <asp:Label ID="Label127" runat="server" Text="c. Tanggal lahir (tgl/bln/thn)" 
                                                                                    Width="136px"></asp:Label></td>
                                                                          <td style="width: 36px; height: 20px;">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaPenerus_Ind_TanggalLahirNew" runat="server" 
                                                                                    CssClass="searcheditbox" TabIndex="2"
                                                                                    Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label128" runat="server" Text="d. Kewarganegaraan (Pilih salah satu) "
                                                                                    Width="182px"></asp:Label></td>
                                                                            <td style="width: 36px" width="100%" align="left">
                                                                                <asp:RadioButtonList ID="RBSwiftInPenerimaPenerus_Ind_KewarganegaraanNew" runat="server" 
                                                                                    RepeatDirection="Horizontal" Enabled="False">
                                                                                    <asp:ListItem Value="1">WNI</asp:ListItem>
                                                                                    <asp:ListItem Value="2">WNA</asp:ListItem>
                                                                                </asp:RadioButtonList>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label129" runat="server" Text="e. Negara "></asp:Label>
                                                                                <br />
                                                                                <asp:Label ID="Label130" runat="server" Font-Italic="True" Text="(Wajib diisi apabila kewarganegaraan WNA)"
                                                                                    Width="214px"></asp:Label></td>
                                                                            <td style="width: 238px" width="100%" align="left">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_negaraNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                <asp:HiddenField ID="hfSwiftInPenerimaPenerus_IND_negaraNew" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label131" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                            <td style="width: 36px" width="100%" align="left">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_negaralainNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label132" runat="server" Text="f. Pekerjaan"></asp:Label></td>
                                                                            <td style="width: 238px" width="100%" align="left">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_pekerjaanNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                <asp:HiddenField ID="hfSwiftInPenerimaPenerus_IND_pekerjaanNew" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label133" runat="server" Text="Pekerjaan Lainnya"></asp:Label></td>
                                                                           <td style="width: 36px" width="100%" align="left">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_pekerjaanLainNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label134" runat="server" Text="g. Alamat Domisili"></asp:Label></td>
                                                                            <td style="width: 36px">
                                                                            </td>
                                                                             
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label135" runat="server" Text="Alamat"></asp:Label></td>
                                                                             <td style="width: 36px" width="100%" align="left">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_alamatDomNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; height: 18px; background-color: #fff7e6">
                                                                                <asp:Label ID="Label136" runat="server" Text="Kota / Kabupaten "></asp:Label></td>
                                                                            <td style="height: 18px; width: 222px;" align="left">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_kotaDomNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                <asp:HiddenField ID="hfSwiftInPenerimaPenerus_IND_kotaDomNew" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; height: 18px; background-color: #fff7e6">
                                                                                <asp:Label ID="Label140" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                           <td style="width: 36px; height: 18px" width="100%" align="left">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_kotaLainDomNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label141" runat="server" Text="Provinsi"></asp:Label></td>
                                                                           <td width="100%" align="left">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_ProvDomNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                <asp:HiddenField ID="hfSwiftInPenerimaPenerus_IND_ProvDomNew" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn175" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                          <td style="width: 36px" width="100%" align="left">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_ProvLainDomNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; background-color: #fff7e6; height: 22px;">
                                                                                <asp:Label ID="LabelSwiftIn115" runat="server" Text="h. Alamat Sesuai Bukti Identitas"></asp:Label></td>
                                                                           <td style="width: 222px; height: 22px;">
                                                                                <asp:CheckBox ID="CheckBoxSwiftInPenerimaPenerus_IND_samaIdenNew" 
                                                                                    runat="server" Text="Sama dengan Alamat Domisili" AutoPostBack="True" 
                                                                                    Enabled="False" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn116" runat="server" Text="Alamat"></asp:Label></td>
                                                                           <td style="width: 36px" width="100%" align="left">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_alamatIdenNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn117" runat="server" Text="Kota / Kabupaten"></asp:Label></td>
                                                                            <td style="width: 222px"align="left">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_kotaIdenNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                <asp:HiddenField ID="hfSwiftInPenerimaPenerus_IND_kotaIdenNew" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; height: 18px; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn118" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                            <td style="width: 36px" width="100%" align="left">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_KotaLainIdenNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; background-color: #fff7e6; height: 18px;">
                                                                                <asp:Label ID="Labelv119" runat="server" Text="Provinsi"></asp:Label></td>
                                                                             <td width="100%" align="left">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_ProvIdenNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                <asp:HiddenField ID="hfSwiftInPenerimaPenerus_IND_ProvIdenNew" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; height: 18px; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn120" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                             <td style="width: 36px; height: 18px" width="100%" align="left">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_ProvLainIdenNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; height: 18px; background-color: #fff7e6">
                                                                                <asp:Label ID="Label9312" runat="server" Text="i. No. Telp"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 36px; height: 18px" width="100%" align="left">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_NoTelpNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" 
                                                                                    Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn121" runat="server" 
                                                                                    Text="j. Jenis Dokument Identitas"></asp:Label></td>
                                                                           <td style="width: 36px" width="100%" align="left">
                                                                                <asp:DropDownList ID="CBoSwiftInPenerimaPenerus_IND_jenisIdentitasNew" 
                                                                                    runat="server" Enabled="False">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; background-color: #fff7e6; height: 18px;">
                                                                                <asp:Label ID="LabelSwiftIn122" runat="server" Text="Nomor Identitas"></asp:Label></td>
                                                                           <td style="width: 36px; height: 18px;" width="100%" align="left">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_nomoridentitasNew" 
                                                                                    runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label9313" runat="server" 
                                                                                    Text="k. Nilai Transaksi Keuangan (dalam Rupiah )"></asp:Label>
                                                                            </td>
                                                                          <td style="width: 36px" width="100%" align="left">
                                                                                <asp:TextBox ID="TxtSwiftInPenerimaPenerus_IND_NIlaiTransaksiNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" 
                                                                                    Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 26%; background-color: #fff7e6; height: 15px;">
                                                                            </td>
                                                                            <td style="height: 15px; width: 238px;">
                                                                            </td>
                                                                           
                                                                        </tr>
                                                                    </table>
                                                                </asp:View>
                                                                <asp:View ID="ViewSwiftInPenerimaPenerus_Korp_New" runat="server">
                                                                    <table style="width: 100%; height: 31px">
                                                                        <tr>
                                                                            <td style="width: 5%; height: 15px; background-color: #fff7e6">
                                                                                <asp:Label ID="labelkor" runat="server" Text="Korporasi" Font-Bold="True"></asp:Label>
                                                                                </td>
                                                                            <td style="height: 15px; color: #000000; width: 393px;" align="left">
                                                                            </td>
                                                                          
                                                                        </tr>
                                                                        
                                                                         <tr>
                                                                        <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                                                            <asp:Label ID="Label55" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                                                        </td>
                                                              
                                                                        <td class="formtext" align="center" bgcolor="#999999" style="width: 383px">
                                                                            <asp:Label ID="Label58" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                                                        </td>
                                                                    </tr> 
                                                                        <tr style="color: #000000">
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn9264" runat="server" Text="a. Nama Bank"></asp:Label>
                                                                                <asp:Label ID="LabelSwiftIn9265" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                            </td>
                                                                         <td style="width: 310px" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaPenerus_Korp_namabankNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" 
                                                                                    Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="color: #000000">
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn126" runat="server" Text="b. Bentuk Badan Usaha "></asp:Label>
                                                                            </td>
                                                                        <td style="width: 310px" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaPenerus_Korp_bentukbadanusahaNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" 
                                                                                    Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn127" runat="server" Text="Bentuk Badan Usaha Lainnya "
                                                                                    Width="139px"></asp:Label></td>
                                                                             <td style="width: 310px" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaPenerus_Korp_BentukBadanUsahaLainNew" 
                                                                                    runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn128" runat="server" Text="c. Nama Korporasi" 
                                                                                    Width="89px"></asp:Label>
                                                                                <asp:Label ID="LabelSwiftIn9266" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 310px" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaPenerus_Korp_NamaKorpNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn129" runat="server" Text="d. Bidang Usaha Korporasi"></asp:Label></td>
                                                                           <td style="width: 310px" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaPenerus_Korp_BidangUsahaKorpNew" 
                                                                                    runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                <asp:HiddenField ID="hfSwiftInPenerimaPenerus_Korp_BidangUsahaKorpNew" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label26" runat="server" Text="Bidang Usaha Lainnya" 
                                                                                    Width="107px" Height="16px"></asp:Label>
                                                                            </td>
                                                                          <td style="width: 310px" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaPenerus_Korp_BidangUsahaKorplainnyaNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn130" runat="server" Text="e. Alamat Lengkap Korporasi"></asp:Label></td>
                                                                            <td style="width: 5%" align="left">
                                                                            </td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn131" runat="server" Text="Alamat"></asp:Label></td>
                                                                           <td style="width: 310px" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaPenerus_Korp_alamatkorpNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="342px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn132" runat="server" Text="Kota / Kabupaten "></asp:Label></td>
                                                                             <td style="width: 310px" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaPenerus_Korp_kotakorpNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                <asp:HiddenField ID="hfSwiftInPenerimaPenerus_Korp_kotakorpNew" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6;">
                                                                                <asp:Label ID="LabelSwiftIn133" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                            <td style="width: 310px; height: 20px;" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaPenerus_Korp_kotalainnyakorpNew" 
                                                                                    runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn134" runat="server" Text="Provinsi"></asp:Label></td>
                                                                           <td align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaPenerus_Korp_ProvinsiKorpNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                <asp:HiddenField ID="hfSwiftInPenerimaPenerus_Korp_ProvinsiKorpNew" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn135" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                            <td style="width: 310px" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaPenerus_Korp_provinsiLainnyaKorpNew" 
                                                                                    runat="server" CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="LabelSwiftIn141" runat="server" Text="f. No Telp"></asp:Label></td>
                                                                           <td style="width: 310px" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaPenerus_Korp_NoTelpNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" 
                                                                                    Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                <asp:Label ID="Label9314" runat="server" 
                                                                                    Text="g. Nilai Transaksi Keuangan (dalam Rupiah )"></asp:Label>
                                                                            </td>
                                                                           <td style="width: 310px" align="left">
                                                                                <asp:TextBox ID="txtSwiftInPenerimaPenerus_Korp_NilaiTransaksiNew" runat="server" 
                                                                                    CssClass="searcheditbox" MaxLength="50"
                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                &nbsp;</td>
                                                                            <td style="width: 5%">
                                                                                &nbsp;</td>
                                                                            
                                                                        </tr>
                                                                    </table>
                                                                </asp:View>
                                                            </asp:MultiView>
                                                    
                                                    </td>
                                                    </tr>
                                                  
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                                    
                                                    </asp:View>
                                                </asp:MultiView>
                                                </ajax:AjaxPanel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:View>
												
                                                </asp:MultiView>
                                                </td>
                                        </tr>
                                    </table>
                                <table id="TRANSAKSI" align="center" bgcolor="#dddddd" border="2" bordercolor="#ffffff" 
                                       cellpadding="2" cellspacing="1" width="99%">
                                    <tr>
                                        <td align="left" background="Images/search-bar-background.gif" 
                                            style="height: 6px; width: 100%;" valign="middle">
                                            <table border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td class="formtext">
                                                        <asp:Label ID="Label9275" runat="server" Font-Bold="True" Text="E. Transaksi"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <a href="#" 
                                                           onclick="javascript:ShowHidePanel('ESwiftIntransaksi','Imgrr25','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')" 
                                                           title="click to minimize or maximize">
                                                        <img id="Imgrr25" src="Images/search-bar-minimize.gif" border="0" height="12px"
                                                             width="12px" />
                                                        </a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:Label ID="Label9276" runat="server" Font-Italic="True" 
                                                       Text="(Diisi oleh PJK Bank  yang bertindak sebagai Penyelenggara Pengirim Asal atau sebagai Penyelenggara Penerus)"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr ID="ESwiftIntransaksi">
                                        <td bgcolor="#ffffff" style="height: 125px; width: 100%;" valign="top">
                                            <table border="0" cellpadding="2" cellspacing="1" style="border-top-style: none;
                                                   border-right-style: none; border-left-style: none; border-bottom-style: none" width="100%">
                                            </table>
                                            <table style="width: 100%; height: 49px">
                                             <tr>
                                        <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                            <asp:Label ID="Label59" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                        </td>
                                        <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                            <asp:Label ID="Label60" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                        </td>
                                        <td class="formtext" align="left" bgcolor="#999999" style="width: 20%">
                                            <asp:Label ID="Label61" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                        </td>
                                    </tr> 
                                                <tr>
                                                    <td style="width: 5%; height: 18px; background-color: #fff7e6">
                                                        <asp:Label ID="Label9277" runat="server" 
                                                            Text="a. Tanggal Transaksi (tgl/bln/thn) "></asp:Label>
                                                        <asp:Label ID="Label9278" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                    </td>
                                                    <td style="width: 296px">
                                                        <asp:TextBox ID="Transaksi_SwiftIntanggalOld" runat="server" CssClass="searcheditbox" 
                                                             TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                        &nbsp;</td>
                                                    <td>
                                                        <asp:TextBox ID="Transaksi_SwiftIntanggalNew" runat="server" CssClass="searcheditbox" 
                                                             TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="labelpoy" Text ="b. Waktu Transaksi diproses (Time Indication)" runat="server" ></asp:Label>
                                                    </td>
                                                    <td style="width: 296px">
                                                        <asp:TextBox ID="Transaksi_SwiftInwaktutransaksiOld" runat="server" 
                                                                     CssClass="searcheditbox" MaxLength="50" TabIndex="2" 
                                                            Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="Transaksi_SwiftInwaktutransaksiNew" runat="server" 
                                                                     CssClass="searcheditbox" MaxLength="50" TabIndex="2" 
                                                            Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="Label9280" runat="server" Text="c. Referensi Pengirim " 
                                                                   Width="139px"></asp:Label>
                                                        <asp:Label ID="Label9323" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                    </td>
                                                    <td style="width: 296px">
                                                        <asp:TextBox ID="Transaksi_SwiftInSenderOld" runat="server" CssClass="searcheditbox" 
                                                                     MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="Transaksi_SwiftInSenderNew" runat="server" CssClass="searcheditbox" 
                                                                     MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>                                                
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="Label9284" runat="server" Text="d. Kode Operasi Bank"></asp:Label>
                                                        <asp:Label ID="Label9285" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                    </td>
                                                    <td style="width: 296px">
                                                        <asp:TextBox ID="Transaksi_SwiftInBankOperationCodeOld" runat="server" 
                                                                     CssClass="searcheditbox" MaxLength="50" TabIndex="2" 
                                                            Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                         <asp:TextBox ID="Transaksi_SwiftInBankOperationCodeNew" runat="server" 
                                                                     CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" 
                                                             Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="Label9286" runat="server" Text="e. Kode Instruksi "></asp:Label>
                                                    </td>
                                                    <td style="width: 296px">
                                                        <asp:TextBox ID="Transaksi_SwiftInInstructionCodeOld" runat="server" 
                                                                     CssClass="searcheditbox" MaxLength="50" TabIndex="2" 
                                                            Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="Transaksi_SwiftInInstructionCodeNew" runat="server" 
                                                                     CssClass="searcheditbox" MaxLength="50" TabIndex="2" 
                                                            Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="Label9287" runat="server" 
                                                                   Text="f. Kantor Cabang Penyelenggara Pengirim Asal"></asp:Label>
                                                        <asp:Label ID="Label9326" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                    </td>
                                                    <td style="width: 296px">
                                                        <asp:TextBox ID="Transaksi_SwiftInkantorCabangPengirimOld" runat="server" 
                                                                     CssClass="searcheditbox" MaxLength="50" TabIndex="2" 
                                                            Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="Transaksi_SwiftInkantorCabangPengirimNew" runat="server" 
                                                                     CssClass="searcheditbox" MaxLength="50" TabIndex="2" 
                                                            Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="Label9289" runat="server" Text="g. Kode Tipe Transaksi "></asp:Label>
                                                    </td>
                                                    <td style="width: 296px">
                                                        <asp:TextBox ID="Transaksi_SwiftInkodeTipeTransaksiOld" runat="server" 
                                                                     CssClass="searcheditbox" MaxLength="50" TabIndex="2" 
                                                            Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="Transaksi_SwiftInkodeTipeTransaksiNew" runat="server" 
                                                                     CssClass="searcheditbox" MaxLength="50" TabIndex="2" 
                                                            Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="Label9290" runat="server" 
                                                            Text=" h. Tanggal Transaksi/Mata Uang/Nilai Transaksi"></asp:Label>
                                                    </td>
                                                    <td style="width: 296px">
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                                             <asp:Label ID="Label9291" runat="server" Text="Tanggal Transaksi (Value Date)"></asp:Label>
                                                                             <asp:Label ID="Label9292" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                         </td>
                                                    <td style="width: 296px">
                                                        <asp:TextBox ID="Transaksi_SwiftInValueTanggalTransaksiOld" runat="server" 
                                                                     CssClass="searcheditbox" TabIndex="2" Width="125px" 
                                                            Enabled="False"></asp:TextBox>
                                                        &nbsp;</td>
                                                    <td>
                                                        <asp:TextBox ID="Transaksi_SwiftInValueTanggalTransaksiNew" runat="server" 
                                                                     CssClass="searcheditbox" TabIndex="2" Width="125px" 
                                                            Enabled="False"></asp:TextBox>
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; height: 18px; background-color: #fff7e6">
                                                        <asp:Label ID="Label9293" runat="server" 
                                                                   Text="Nilai Transaksi (Amount of The EFT)"></asp:Label>
                                                        <asp:Label ID="Label9315" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                    </td>
                                                    <td style="width: 296px">
                                                        <asp:TextBox ID="TxtNilaitransaksiOld" runat="server" 
                                                                     CssClass="searcheditbox" TabIndex="2" Width="125px" 
                                                            Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                       <asp:TextBox ID="TxtNilaitransaksiNew" runat="server" 
                                                                     CssClass="searcheditbox" TabIndex="2" Width="125px" 
                                                            Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; height: 18px; background-color: #fff7e6">
                                                        <asp:Label ID="Label9294" runat="server" 
                                                                   Text="Mata Uang Transaksi (currency of the EFT)"></asp:Label>
                                                        <asp:Label ID="Label9295" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                    </td>
                                                    <td style="width: 296px">
                                                        <asp:TextBox ID="Transaksi_SwiftInMataUangTransaksiOld" runat="server" 
                                                                     CssClass="searcheditbox" MaxLength="50" TabIndex="2" 
                                                            Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="Transaksi_SwiftInMataUangTransaksiNew" runat="server" 
                                                                     CssClass="searcheditbox" MaxLength="50" TabIndex="2" 
                                                            Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; height: 18px; background-color: #fff7e6">
                                                        <asp:Label ID="Label19" runat="server" Text="Mata Uang Transaksi lainnya"></asp:Label>
                                                        <asp:Label ID="Label20" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                    </td>
                                                    <td style="width: 296px">
                                                        <asp:TextBox ID="Transaksi_SwiftInMataUangTransaksiLainnyaOld" runat="server" 
                                                                     CssClass="searcheditbox" MaxLength="50" TabIndex="2" 
                                                            Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="Transaksi_SwiftInMataUangTransaksiLainnyaNew" runat="server" 
                                                                     CssClass="searcheditbox" MaxLength="50" TabIndex="2" 
                                                            Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="Label9296" runat="server" 
                                                                   Text="Nilai Transaksi Keuangan yang diterima dalam Rupiah "></asp:Label>
                                                    </td>
                                                    <td style="width: 296px">
                                                        <asp:TextBox ID="Transaksi_SwiftInAmountdalamRupiahOld" runat="server" 
                                                                     CssClass="searcheditbox" MaxLength="50" TabIndex="2" 
                                                            Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="Transaksi_SwiftInAmountdalamRupiahNew" runat="server" 
                                                                     CssClass="searcheditbox" MaxLength="50" TabIndex="2" 
                                                            Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="Label9297" runat="server" Text="i. Mata Uang/Nilai Transaksi Keuangan yang diinstruksikan"></asp:Label>
                                                    </td>
                                                    <td style="width: 296px">
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="Label9298" runat="server" Text="Mata Uang yang diinstruksikan "></asp:Label>
                                                    </td>
                                                    <td style="width: 296px">
                                                        <asp:TextBox ID="Transaksi_SwiftIncurrencyOld" runat="server" CssClass="searcheditbox" 
                                                                     MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="Transaksi_SwiftIncurrencyNew" runat="server" CssClass="searcheditbox" 
                                                                     MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="Label24" runat="server" Text="Mata Uang Lainnya"></asp:Label>
                                                    </td>
                                                    <td style="width: 296px">
                                                        <asp:TextBox ID="Transaksi_SwiftIncurrencyLainnyaOld" runat="server" CssClass="searcheditbox" 
                                                                     MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="Transaksi_SwiftIncurrencyLainnyaNew" runat="server" CssClass="searcheditbox" 
                                                                     MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="Label9299" runat="server" Text="Nilai Transaksi Keuangan yang diinstruksikan"></asp:Label>
                                                    </td>
                                                    <td style="width: 296px">
                                                        <asp:TextBox ID="Transaksi_SwiftIninstructedAmountOld" runat="server" 
                                                                     CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="Transaksi_SwiftIninstructedAmountNew" runat="server" 
                                                                     CssClass="searcheditbox" MaxLength="50" TabIndex="2" 
                                                            Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; height: 18px; background-color: #fff7e6">
                                                        <asp:Label ID="Label9300" runat="server" Text="j. Nilai Tukar (Exchange Rate) "></asp:Label>
                                                    </td>
                                                    <td style="width: 296px">
                                                        <asp:TextBox ID="Transaksi_SwiftInnilaiTukarOld" runat="server" CssClass="searcheditbox" 
                                                                     MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="Transaksi_SwiftInnilaiTukarNew" runat="server" CssClass="searcheditbox" 
                                                                     MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="Label9301" runat="server" Text="k. Institusi Pengirim"></asp:Label>
                                                    </td>
                                                    <td style="width: 296px">
                                                        <asp:TextBox ID="Transaksi_SwiftInsendingInstitutionOld" runat="server" 
                                                                     CssClass="searcheditbox" MaxLength="50" TabIndex="2" 
                                                            Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="Transaksi_SwiftInsendingInstitutionNew" runat="server" 
                                                                     CssClass="searcheditbox" MaxLength="50" TabIndex="2" 
                                                            Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>                                                
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="Label9304" runat="server" Text="l. Tujuan Transaksi "></asp:Label>
                                                    </td>
                                                    <td style="width: 296px">
                                                        <asp:TextBox ID="Transaksi_SwiftInTujuanTransaksiOld" runat="server" 
                                                                     CssClass="searcheditbox" MaxLength="50" TabIndex="2" 
                                                            Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="Transaksi_SwiftInTujuanTransaksiNew" runat="server" 
                                                                     CssClass="searcheditbox" MaxLength="50" TabIndex="2" 
                                                            Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="Label9305" runat="server" Text="m. Sumber Penggunaan Dana "></asp:Label>
                                                    </td>
                                                    <td style="width: 296px">
                                                        <asp:TextBox ID="Transaksi_SwiftInSumberPenggunaanDanaOld" runat="server" 
                                                                     CssClass="searcheditbox" MaxLength="50" TabIndex="2" 
                                                            Width="296px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="Transaksi_SwiftInSumberPenggunaanDanaNew" runat="server" 
                                                                     CssClass="searcheditbox" MaxLength="50" TabIndex="2" 
                                                            Width="296px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                    </td>
                                                    <td style="width: 296px">
                                                    </td>
                                                    <td></td>
                                               </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <table id="INFORMASI LAINNYA" align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                        bgcolor="#dddddd" border="2">
                                        <tr>
                                            <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                style="height: 6px">
                                                <table cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td class="formtext">
                                                            <asp:Label ID="Label21" runat="server" Text="F. Informasi Lainnya" Font-Bold="True"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <a href="#" onclick="javascript:ShowHidePanel('FSwiftInInformasiLainnya','Imgw12','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                               title="click to minimize or maximize">
                                                            <img id="Imgw12" src="Images/search-bar-minimize.gif" border="0" height="12px"
                                                                 width="12px" />
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <asp:Label ID="Label230" runat="server" Font-Italic="True" Text="(Diisi oleh PJK Bank  yang bertindak sebagai Penyelenggara Pengirim Asal atau sebagai Penyelenggara Penerus)"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr id="FSwiftInInformasiLainnya">
                                            <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">                                             
                                                </table>
                                                <table style="width: 100%; height: 49px">
                                                 <tr>
                                        <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                            <asp:Label ID="Label62" runat="server" Font-Bold="True" Text="Field"></asp:Label>
                                        </td>
                                        <td class="formtext" align="center" bgcolor="#999999" style="width: 5%">
                                            <asp:Label ID="Label63" runat="server" Font-Bold="True" Text="Old Data"></asp:Label>
                                        </td>
                                        <td class="formtext" align="left" bgcolor="#999999" style="width: 20%">
                                            <asp:Label ID="Label64" runat="server" Font-Bold="True" Text="New Data"></asp:Label>
                                        </td>
                                    </tr> 
                                                    <tr>
                                                        <td style="width: 5%; height: 15px; background-color: #fff7e6">
                                                            <asp:Label ID="Label233" runat="server" Text="a. Informasi Korespondensi Pengirim" Width="226px"></asp:Label>
                                                        </td>
                                                        <td style="height: 15px; width: 5%; color: #000000;">
                                                            <asp:TextBox ID="InformasiLainnya_SwiftInSenderOld" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                        <td style="height: 15px; width: 20%; color: #000000;">
                                                            <asp:TextBox ID="InformasiLainnya_SwiftInSenderNew" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 5%; background-color: #fff7e6; height: 15px;">
                                                            <asp:Label ID="Label249" runat="server" Text="b. Informasi Korespondensi Penerima" Width="223px"></asp:Label></td>
                                                        <td style="width: 5%; height: 15px; color: #000000;">
                                                            <asp:TextBox ID="InformasiLainnya_SwiftInreceiverOld" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                        <td style="width: 20%; height: 15px; color: #000000;">
                                                            <asp:TextBox ID="InformasiLainnya_SwiftInreceiverNew" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 5%; background-color: #fff7e6; height: 15px;">
                                                            <asp:Label ID="Label253" runat="server" Text="c. Informasi Institusi Pengganti Ketiga"
                                                                Width="238px"></asp:Label></td>
                                                        <td style="width: 5%; color: #000000; height: 15px;">
                                                            <asp:TextBox ID="InformasiLainnya_SwiftInthirdReimbursementOld" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                        <td style="width: 20%; color: #000000; height: 15px;">
                                                            <asp:TextBox ID="InformasiLainnya_SwiftInthirdReimbursementNew" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 5%; background-color: #fff7e6; height: 15px;">
                                                            <asp:Label ID="Label254" runat="server" Text="d. Informasi Institusi Perantara Pihak Ketiga "
                                                                Width="258px"></asp:Label></td>
                                                        <td style="width: 5%; color: #000000; height: 15px;">
                                                            <asp:TextBox ID="InformasiLainnya_SwiftInintermediaryOld" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                        <td style="width: 20%; color: #000000; height: 15px;">
                                                            <asp:TextBox ID="InformasiLainnya_SwiftInintermediaryNew" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 5%; background-color: #fff7e6; height: 15px;">
                                                            <asp:Label ID="Label256" runat="server" Text="e. Informasi Remittance" Width="186px"></asp:Label>
                                                        </td>
                                                        <td style="width: 5%; color: #000000; height: 15px;">
                                                            <asp:TextBox ID="InformasiLainnya_SwiftInRemittanceOld" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 20%; color: #000000; height: 15px;">
                                                            <asp:TextBox ID="InformasiLainnya_SwiftInRemittanceNew" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 5%; background-color: #fff7e6; height: 15px;">
                                                            <asp:Label ID="Label257" runat="server" 
                                                                Text="f. Informasi Pengirim ke Penerima" Width="217px"></asp:Label></td>
                                                        <td style="width: 5%; color: #000000; height: 15px;">
                                                            <asp:TextBox ID="InformasiLainnya_SwiftInSenderToReceiverOld" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 20%; color: #000000; height: 15px;">
                                                            <asp:TextBox ID="InformasiLainnya_SwiftInSenderToReceiverNew" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 5%; background-color: #fff7e6; height: 15px;">
                                                            <asp:Label ID="Label258" runat="server" Text="g. Laporan Pemerintah" Width="216px"></asp:Label></td>
                                                        <td style="width: 5%; height: 15px; color: #000000;">
                                                            <asp:TextBox ID="InformasiLainnya_SwiftInRegulatoryReportOld" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                        <td style="width: 20%; height: 15px; color: #000000;">
                                                            <asp:TextBox ID="InformasiLainnya_SwiftInRegulatoryReportNew" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 5%; background-color: #fff7e6; height: 15px;">
                                                            <asp:Label ID="Label259" runat="server" Text="h. Isi Amplop" Width="157px"></asp:Label></td>
                                                        <td style="width: 5%; color: #000000; height: 15px;">
                                                            <asp:TextBox ID="InformasiLainnya_SwiftInEnvelopeContentsOld" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                        <td style="width: 20%; color: #000000; height: 15px;">
                                                            <asp:TextBox ID="InformasiLainnya_SwiftInEnvelopeContentsNew" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50"
                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 5%; background-color: #fff7e6; height: 15px;">
                                                        </td>
                                                        <td style="height: 15px; width: 5%; color: #000000;">
                                                        </td>
                                                        <td style="width: 20%; height: 15px; color: #000000;">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                </table>
                                <table id="FUNCTION BUTTON" width="100%">
                                    <tr>
                                        <td style="width: 387px"></td>
                                        <td style="width: 122px">
                                            <asp:Button ID="BtnSave" runat="server" Text="Accept" />
                                        </td>
                                        <td style="width: 107px">
                                            <asp:Button ID="BtnReject" runat="server" Text="Reject" />
                                        </td>
                                        <td>
                                            <asp:Button ID="BtnCancel" runat="server" Text="<<Back" />
                                        </td>
                                        <td></td>
                                    </tr>
                                </table>
                        </asp:View>
                        <asp:View ID="VwConfirmation" runat="server">
											<table style="width: 100%" bordercolor="#ffffff" cellspacing="1" cellpadding="2"
												width="100%" bgcolor="#dddddd" border="0">
												<tr bgcolor="#ffffff">
													<td colspan="2" align="center" style="height: 17px">
														<asp:Label ID="LblConfirmation" runat="server" meta:resourcekey="LblConfirmationResource1"></asp:Label>
													</td>
												</tr>
												<tr bgcolor="#ffffff">
													<td align="center" colspan="2">
														<asp:ImageButton ID="ImgBtnAdd" runat="server" ImageUrl="~/images/button/Ok.gif"
															CausesValidation="False" meta:resourcekey="ImgBtnAddResource1" />
													</td>
												</tr>
											</table>
										</asp:View>
                    </asp:MultiView>
                </ajax:AjaxPanel>
            </td>
          </tr>
    </table>
    <ajax:AjaxPanel ID="AjaxPanel14" runat="server" 
        meta:resourcekey="AjaxPanel14Resource1"><table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td align="left" background="Images/button-bground.gif" valign="middle">
                    <img height="1" src="Images/blank.gif" width="5" />
                </td>
                <td align="left" background="Images/button-bground.gif" valign="middle">
                    <img height="15" src="images/arrow.gif" width="15" />&nbsp;
                </td>
                <td background="Images/button-bground.gif">
                    &nbsp;
                </td>
                <td background="Images/button-bground.gif">
                    &nbsp;
                </td>
                <td background="Images/button-bground.gif">
                    &nbsp;
                </td>
                <td background="Images/button-bground.gif" width="99%">
                    <img height="1" src="Images/blank.gif" width="1" />
                    <asp:CustomValidator ID="CvalHandleErr" runat="server" Display="None" 
                        meta:resourcekey="CvalHandleErrResource1" ValidationGroup="handle"></asp:CustomValidator>
                    <asp:CustomValidator ID="CvalPageErr" runat="server" Display="None" 
                        meta:resourcekey="CvalPageErrResource1"></asp:CustomValidator>
                </td>
                <td>
                    
                </td>
            </tr>
        </table>
        <asp:CustomValidator ID="CustomValidator1" runat="server" Display="None" meta:resourcekey="CustomValidator1Resource1"
            ValidationGroup="handle"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </asp:CustomValidator>
            <asp:CustomValidator ID="CustomValidator2"
                runat="server" Display="None" 
            meta:resourcekey="CustomValidator2Resource1"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </asp:CustomValidator>
    </ajax:AjaxPanel>
    </div> 
</asp:Content>