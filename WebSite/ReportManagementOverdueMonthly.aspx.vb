Imports OWC11
Imports System.Configuration
Partial Class ReportManagementOverdueMonthly
    Inherits Parent

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Dim strXMLData As String
                strXMLData = GetDataCube()
                ClientScript.RegisterHiddenField("txtXMLData", strXMLData)

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            LogError(ex)
        End Try
    End Sub

    Private Function GetDataCube() As String
        Dim m_XML As String
        Try
            Dim objPivot As PivotTableClass = New PivotTableClass
            Dim objPivotView As PivotView
            Dim FSetAging As PivotFieldSet
            Dim FSetStrukturOrganisasi As PivotFieldSet

            'Dim objPT As PivotTableClass = New PivotTableClass
            objPivot.ConnectionString = System.Configuration.ConfigurationManager.AppSettings.Item("OLAPConnectionStringReport")
            objPivot.DataMember = "Recap AML Solution Alerts"
            objPivotView = objPivot.ActiveView
            objPivotView.TitleBar.Visible = False

            FSetAging = objPivotView.FieldSets("Aging")
            FSetStrukturOrganisasi = objPivotView.FieldSets("Struktur Organisasi")

            objPivotView.DataAxis.InsertTotal(objPivotView.Totals("Alert Of CDD Updating"))
            objPivotView.DataAxis.InsertTotal(objPivotView.Totals("Alert Of STR"))
            'objPivotView.DataAxis.InsertTotal(objPivotView.Totals("Alert Of STR Approver"))
            'objPivotView.DataAxis.InsertTotal(objPivotView.Totals("Alert Of STR Maker"))
            objPivotView.DataAxis.InsertTotal(objPivotView.Totals("Total Alert"))

            objPivotView.RowAxis.InsertFieldSet(FSetStrukturOrganisasi)
            objPivotView.ColumnAxis.InsertFieldSet(FSetAging)

            'objPivotView.ColumnAxis.InsertFieldSet(objPivotView.FieldSets("Data Updating Month"))
            'objPivotView.ColumnAxis.InsertFieldSet(objPivotView.FieldSets("Data Updating Day"))

            'RemoveAlltotals(objPivotView.FieldSets("Data Updating Year"))
            'RemoveAlltotals(objPivotView.FieldSets("Data Updating Month"))
            'RemoveAlltotals(objPivotView.FieldSets("Data Updating Day"))
            'RemoveAlltotals(objPivotView.FieldSets("Risk Level"))
            objPivot.ActiveData.HideDetails()

            objPivotView.ExpandMembers = PivotTableExpandEnum.plExpandNever
            m_XML = objPivot.XMLData

            objPivot = Nothing

        Catch err As Exception
            m_XML = "<err>" & err.Source & " - " & err.Message & "</err>"
        Finally

        End Try
        Return (m_XML)
    End Function

    Private Sub RemoveAlltotals(ByVal varFset As PivotFieldSet)
        Dim i As Integer
        For i = 0 To varFset.Fields.Count - 1
            varFset.Fields(i).Subtotals(i) = False
        Next
    End Sub
End Class
