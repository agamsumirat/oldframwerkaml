<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="UserLockDetail.aspx.vb" Inherits="UserLockDetail" title="User Lock Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
	<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2" height="231">
        <tr class="formText">
            <td bgcolor="#ffffff" colspan="4">
                <strong><span style="font-size: 18px">User Lock Detail
                    <hr />
                </span></strong>
            </td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" colspan="4"><span style="color: #ff0000">* Required</span></td>
        </tr>
		<tr class="formText">
			<td width="5" bgColor="#ffffff" height="24">
                <br />
                </td>
			<td width="20%" bgColor="#ffffff">User ID</td>
			<td width="5" bgColor="#ffffff">:</td>
			<td width="80%" bgColor="#ffffff"><asp:Label ID="LabelUserId" runat="server"></asp:Label>
                </td>
		</tr>
		<tr class="formText">
			<td bgColor="#ffffff" height="24">
                <br />
                </td>
			<td bgColor="#ffffff">
                User Name</td>
			<td bgColor="#ffffff">:</td>
			<td bgColor="#ffffff">
                <asp:Label ID="LabelUserName" runat="server"></asp:Label>&nbsp;<strong><span style="color: #ff0000"></span></strong></td>
		</tr>
		<tr class="formText">
			<td bgColor="#ffffff" height="24" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
                </td>
			<td bgColor="#ffffff">
                IP Address</td>
			<td bgColor="#ffffff">:</td>
			<td bgColor="#ffffff">
                <asp:Label ID="LabelIPAddress" runat="server"></asp:Label>&nbsp;<span style="color: #ff0000"></span></td>
		</tr>
		<tr class="formText">
			<td bgColor="#ffffff" height="24" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
                </td>
			<td bgColor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
                </td>
			<td bgColor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none"></td>
			<td bgColor="#ffffff">
                <asp:CheckBox ID="CheckBoxInUse" runat="server" Text="User In Use" />&nbsp;
                </td>
		</tr>
		<tr class="formText">
			<td bgColor="#ffffff" height="24" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
                <br />
                </td>
			<td bgColor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
                </td>
			<td bgColor="#ffffff" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none"></td>
			<td bgColor="#ffffff">
                <asp:CheckBox ID="CheckBoxDisabled" runat="server" Text="User Disabled" /><strong><span style="color: #ff0000"></span></strong></td>
		</tr>
		<tr class="formText" bgColor="#dddddd" height="30">
			<td width="15"><IMG height="15" src="images/arrow.gif" width="15"></td>
			<td colSpan="3">
				<table cellSpacing="0" cellPadding="3" border="0">
					<tr>
						<td><asp:imagebutton id="ImageSave" runat="server" CausesValidation="True" SkinID="UpdateButton"></asp:imagebutton></td>
						<td><asp:imagebutton id="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton"></asp:imagebutton></td>
					</tr>
				</table>               
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
		</tr>
	</table>
	<script>
	    document.getElementById('<%=GetFocusID %>').focus();
	</script>
</asp:Content>