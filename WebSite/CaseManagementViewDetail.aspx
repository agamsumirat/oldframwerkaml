<%@ Page Language="VB" MasterPageFile="~/masterpage.master" AutoEventWireup="false"
    CodeFile="CaseManagementViewDetail.aspx.vb" Inherits="CaseManagementViewDetail"
    Title="Case Management - View Detail" ValidateRequest="false" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    <script language="javascript">

        function cekproposedaction(ocbo) {

            if (document.getElementById(ocbo).getAttribute("value") == 5) {

                trAccountOwner.style.display = "block";
            }
            else {

                trAccountOwner.style.display = "none";

            }


        }

    </script>
    <table width="100%">
        <tr>
            <td colspan="3">
                <ajax:AjaxPanel ID="AjaxPanel15" runat="server">
                    <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></ajax:AjaxPanel>
                <asp:Label Text="Alert Case Detail" runat="server" Font-Bold="True" Font-Size="Large"
                    ID="abc" />
                <hr />
            </td>
        </tr>
        <tr>
            <td style="width: 250px">
                Case Number
            </td>
            <td style="width: 1px">
                :
            </td>
            <td style="width: 69%">
                <asp:Label Text="" runat="server" ID="LblCaseNumber" />
            </td>
        </tr>
        <tr>
            <td style="width: 250px">
                Description
            </td>
            <td style="width: 1px">
                :
            </td>
            <td style="width: 69%">
                <asp:Label Text="" runat="server" ID="LblDescription" />
            </td>
        </tr>
        <tr>
            <td style="width: 250px">
                Alert Case Status
            </td>
            <td style="width: 1px">
                :
            </td>
            <td style="width: 69%">
                <asp:Label Text="" runat="server" ID="LblCaseStatus" />
            </td>
        </tr>
        <tr>
            <td style="width: 250px">
                Created Date
            </td>
            <td style="width: 1px">
                :
            </td>
            <td style="width: 69%">
                <asp:Label Text="" runat="server" ID="LblCreatedDate" />
            </td>
        </tr>
        <tr>
            <td style="width: 250px">
                Last Updated Date
            </td>
            <td style="width: 1px">
                :
            </td>
            <td style="width: 69%">
                <asp:Label Text="" runat="server" ID="LblLastUpdatedDate" />
            </td>
        </tr>
        <tr>
            <td style="width: 250px">
                Proposed By
            </td>
            <td style="width: 1px">
                :
            </td>
            <td style="width: 69%">
                <asp:Label Text="" runat="server" ID="LblProposedBy" />
            </td>
        </tr>
        <tr>
            <td style="width: 250px">
                Account Owner Name
            </td>
            <td style="width: 1px">
                :
            </td>
            <td style="width: 69%">
                <asp:Label Text="" runat="server" ID="LblAcocuntOwnerName" />
            </td>
        </tr>
        <tr>
            <td style="width: 250px">
                PIC
            </td>
            <td style="width: 1px">
                :
            </td>
            <td style="width: 69%">
                <asp:Label Text="" runat="server" ID="LblPIC" />
            </td>
        </tr>
        <tr>
            <td style="width: 250px">
                AML/CFT Risk
            </td>
            <td style="width: 1px">
                :
            </td>
            <td style="width: 69%">
                <asp:Label Text="" runat="server" ID="LblHighRiskCustomer" />
            </td>
        </tr>
        <tr>
            <td style="width: 250px">
                Last Proposed Action
            </td>
            <td style="width: 1px">
                :
            </td>
            <td style="width: 69%">
                <asp:Label Text="" runat="server" ID="LblLastProposedAction" />
            </td>
        </tr>
        <tr>
            <td style="width: 250px;" valign="top">
                Income&nbsp;
            </td>
            <td style="width: 1px" valign="top">
                :
            </td>
            <td style="width: 69%">
                <asp:GridView runat="server" ID="GrdPenghasilan" AutoGenerateColumns="False" EnableModelValidation="True"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px"
                    CellPadding="4" ForeColor="Black">
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:TemplateField HeaderText="No">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="LabelNo" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="CIF" HeaderText="CIF" />
                        <asp:BoundField DataField="IncomeType" HeaderText="IncomeType" />
                        <asp:BoundField DataField="Income" HeaderText="Income" />
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                    <RowStyle BackColor="#F7F7DE" />
                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td style="width: 250px" valign="top">
                Employement History
            </td>
            <td style="width: 1px" valign="top">
                :
            </td>
            <td style="width: 69%">
                <asp:GridView runat="server" ID="GrdEmployeeHistory" AutoGenerateColumns="False"
                    EnableModelValidation="True" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None"
                    BorderWidth="1px" CellPadding="4" ForeColor="Black" EmptyDataText="NA" AllowPaging="True">
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:TemplateField HeaderText="No">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="LabelNo" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="CIF" HeaderText="CIF" />
                        <asp:BoundField DataField="Employer" HeaderText="Employer" />
                        <asp:BoundField DataField="Title" HeaderText="Title" />
                        <asp:BoundField DataField="StartDate" DataFormatString="{0:dd-MMM-yyyy}" HeaderText="StartDate"
                            HtmlEncode="False" />
                        <asp:BoundField DataField="EndDate" DataFormatString="{0:dd-MMM-yyyy}" HeaderText="EndDate" />
                        <asp:BoundField DataField="WorkingYear" HeaderText="WorkingYear" />
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                    <RowStyle BackColor="#F7F7DE" />
                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td valign="top">
                Same Case History
            </td>
            <td valign="top">
                :
            </td>
            <td>
                <asp:GridView ID="GridViewSameCase" runat="server" AutoGenerateColumns="False" CellPadding="4"
                    ForeColor="Black" GridLines="Vertical" DataKeyNames="PK_CaseManagementSameCaseHistory_ID"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px"
                    EmptyDataText="NA" AllowPaging="True">
                    <RowStyle BackColor="#F7F7DE" />
                    <Columns>
                        <asp:BoundField DataField="PK_CaseManagementSameCaseHistory_ID" HeaderText="Case ID" />
                        <asp:BoundField DataField="CaseDescription" HeaderText="Case Description" />
                        <asp:TemplateField HeaderText="Status">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="LabelStatus" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="CreatedDate" DataFormatString="{0:dd-MMM-yyyy}" HeaderText="Created Date"
                            HtmlEncode="False" />
                        <asp:BoundField DataField="LastUpdated" DataFormatString="{0:dd-MMM-yyyy}" HeaderText="Last Updated Date"
                            HtmlEncode="False" />
                        <asp:TemplateField HeaderText="Assigned Branch">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="LblBranch" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="PIC" HeaderText="PIC" />
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView>
            </td>
        </tr>
        <tr runat="Server" id="CTRReportedToPPATKROW">
            <td>
                CTR Reported To PPATK
            </td>
            <td valign="top">
                :
            </td>
            <td>
                <ajax:AjaxPanel ID="STRReportedToPPATK1" runat="server">
                    <asp:GridView ID="CTRReportedToPPATK" runat="server" AutoGenerateColumns="False"
                        CellPadding="4" ForeColor="Black" GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE"
                        BorderStyle="None" BorderWidth="1px" EmptyDataText="NA" EnableModelValidation="True">
                        <RowStyle BackColor="#F7F7DE" />
                        <Columns>
                            <asp:BoundField DataField="Number" HeaderText="Number" />
                            <asp:BoundField DataField="Total" HeaderText="Total" HtmlEncode="False" />
                            <asp:BoundField DataField="Average" HeaderText="Average" HtmlEncode="False" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="LnkDetailCTRReportedToPPATK" runat="server" ForeColor="#0033CC"
                                        OnClick="CTRReportedToPPATK_Click">Detail</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#CCCC99" />
                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                        <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                        <AlternatingRowStyle BackColor="White" />
                    </asp:GridView>
                </ajax:AjaxPanel>
            </td>
        </tr>
        <tr runat="Server" id="STRReportedToPPATKROW" valign="top">
            <td>
                STR Reported To PPATK
            </td>
            <td valign="top">
                :
            </td>
            <td>
                <asp:GridView ID="STRReportedToPPATK" runat="server" AutoGenerateColumns="False"
                    CellPadding="4" ForeColor="Black" GridLines="Vertical" DataKeyNames="PK_CaseManagementID"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px"
                    EmptyDataText="NA" EnableModelValidation="True">
                    <RowStyle BackColor="#F7F7DE" />
                    <Columns>
                        <asp:BoundField DataField="PK_CaseManagementID" HeaderText="Case ID" />
                        <asp:BoundField DataField="PPATKConfirmationno" HeaderText="No STR PPATK" />
                        <asp:BoundField DataField="CaseDescription" HeaderText="Case Description" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="LnkDetailSTRRePortedToPPATK" runat="server" ForeColor="#0033CC"
                                    OnClick="LnkDetailSTRRePortedToPPATK_Click">Detail</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <hr />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Panel ID="PanelSuspicius" runat="server" Width="100%" Visible="false">
                    <table bgcolor="#dddddd" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
                        height="72" style="border-top-style: none; border-right-style: none; border-left-style: none;
                        border-bottom-style: none" width="100%">
                        <tr class="formText">
                            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
                            </td>
                            <td bgcolor="#ffffff" colspan="3" style="height: 24px">
                                <asp:Label ID="Label15" runat="server" Font-Size="Large" Text="Custom Alert"></asp:Label>
                            </td>
                        </tr>
                        <tr class="formText" style="font-size: 8pt">
                            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
                            </td>
                            <td bgcolor="#ffffff" style="height: 24px" width="20%">
                                CIF No
                            </td>
                            <td bgcolor="#ffffff" style="height: 24px" width="5">
                                :
                            </td>
                            <td bgcolor="#ffffff" style="height: 24px" width="80%">
                                <asp:Label ID="LblCIF" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr class="formText" style="font-size: 8pt">
                            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
                                <br />
                            </td>
                            <td bgcolor="#ffffff" style="height: 24px" width="20%">
                                Name
                            </td>
                            <td bgcolor="#ffffff" style="height: 24px" width="5">
                                :
                            </td>
                            <td bgcolor="#ffffff" style="height: 24px" width="80%">
                                <asp:Label ID="lblName" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr class="formText">
                            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
                            </td>
                            <td bgcolor="#ffffff" style="height: 24px" width="20%">
                                Birth Date
                            </td>
                            <td bgcolor="#ffffff" style="height: 24px" width="5">
                                :
                            </td>
                            <td bgcolor="#ffffff" style="height: 24px" width="80%">
                                <asp:Label ID="lblTglLahir" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr class="formText">
                            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
                            </td>
                            <td bgcolor="#ffffff" style="height: 24px" width="20%">
                                Birth Place
                            </td>
                            <td bgcolor="#ffffff" style="height: 24px" width="5">
                                :
                            </td>
                            <td bgcolor="#ffffff" style="height: 24px" width="80%">
                                <asp:Label ID="lblTempatLahir" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr class="formText">
                            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
                            </td>
                            <td bgcolor="#ffffff" style="height: 24px" width="20%">
                                Identity No
                            </td>
                            <td bgcolor="#ffffff" style="height: 24px" width="5">
                                :
                            </td>
                            <td bgcolor="#ffffff" style="height: 24px" width="80%">
                                <asp:Label ID="lblIdentityNo" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr class="formText">
                            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
                            </td>
                            <td bgcolor="#ffffff" style="height: 24px" valign="top" width="20%">
                                Address
                            </td>
                            <td bgcolor="#ffffff" style="height: 24px" valign="top" width="5">
                                :
                            </td>
                            <td bgcolor="#ffffff" style="height: 24px" valign="top" width="80%">
                                <asp:Label ID="lblAddress" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr class="formText">
                            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
                            </td>
                            <td bgcolor="#ffffff" style="height: 24px" width="20%">
                                Kecamatan/Kelurahan
                            </td>
                            <td bgcolor="#ffffff" style="height: 24px" width="5">
                                :
                            </td>
                            <td bgcolor="#ffffff" style="height: 24px" width="80%">
                                <asp:Label ID="lblKecamatanKelurahan" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr class="formText">
                            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
                            </td>
                            <td bgcolor="#ffffff" style="height: 24px" width="20%">
                                Phone No
                            </td>
                            <td bgcolor="#ffffff" style="height: 24px" width="5">
                                :
                            </td>
                            <td bgcolor="#ffffff" style="height: 24px" width="80%">
                                <asp:Label ID="lblPhoneNumber" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr class="formText">
                            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
                            </td>
                            <td bgcolor="#ffffff" style="height: 24px" width="20%">
                                Position
                            </td>
                            <td bgcolor="#ffffff" style="height: 24px" width="5">
                                :
                            </td>
                            <td bgcolor="#ffffff" style="height: 24px" width="80%">
                                <asp:Label ID="lblJob" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr class="formText">
                            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
                            </td>
                            <td bgcolor="#ffffff" style="height: 24px" width="20%">
                                Company Name
                            </td>
                            <td bgcolor="#ffffff" style="height: 24px" width="5">
                                :
                            </td>
                            <td bgcolor="#ffffff" style="height: 24px" width="80%">
                                <asp:Label ID="lblworkplace" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr class="formText">
                            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
                            </td>
                            <td bgcolor="#ffffff" style="height: 24px" width="20%">
                                NPWP
                            </td>
                            <td bgcolor="#ffffff" style="height: 24px" width="5">
                                :
                            </td>
                            <td bgcolor="#ffffff" style="height: 24px" width="80%">
                                <asp:Label ID="lblnpwp" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr class="formText">
                            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
                            </td>
                            <td bgcolor="#ffffff" style="height: 24px" width="20%">
                                Description
                            </td>
                            <td bgcolor="#ffffff" style="height: 24px" width="5">
                                :
                            </td>
                            <td bgcolor="#ffffff" style="height: 24px" width="80%">
                                <asp:Label ID="Label1" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr class="formText">
                            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
                            </td>
                            <td bgcolor="#ffffff" style="height: 24px" width="20%">
                                Created By
                            </td>
                            <td bgcolor="#ffffff" style="height: 24px" width="5">
                                :
                            </td>
                            <td bgcolor="#ffffff" style="height: 24px" width="80%">
                                <asp:Label ID="lblcreatedby" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Panel runat="server" ID="PanelIdentitas" Visible="false">
                    <table style="width: 100%; vertical-align: top">
                        <tr>
                            <td colspan="3" valign="top" style="width: 300px">
                                <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Size="Large"  Visible="False" Text="Customer AML Risk"></asp:Label>
                            </td>
                            <td valign="top">
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 300px" valign="top">
                                <asp:Label ID="LblKetProfileIdentitas" runat="server" Text="Final Risk Value" 
                                    Font-Bold="True" Visible="False"></asp:Label>
                            </td>
                            <td style="width: 1px" valign="top">
                                &nbsp;
                            </td>
                            <td valign="top">
                                <asp:Label ID="LblResikoIdentitas" runat="server" Font-Bold="True" Visible="False"></asp:Label>
                            </td>
                            <td valign="top">
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 300px" valign="top">
                                <asp:Label ID="Label2" runat="server" Text="List Type" Visible="False"></asp:Label>
                            </td>
                            <td style="width: 1px" valign="top">
                                
                            </td>
                            <td valign="top">
                                <asp:Label ID="LblListType" runat="server" Visible="False"></asp:Label>
                            </td>
                            <td valign="top">
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 300px" valign="top">
                            </td>
                            <td style="width: 1px" valign="top">
                            </td>
                            <td valign="top">
                            </td>
                            <td valign="top">
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Panel runat="server" ID="PanelProfileKeuangan" Visible="false">
                    <table width="100%">
                        <tr>
                            <td colspan="3">
                                <asp:Label ID="Label16" runat="server" Font-Bold="True" Font-Size="Large" Text="Customer Financial Profile"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:Panel runat="server" ID="PanelNewCustomer" Visible="false">
                                    <table width="100%">
                                        <tr>
                                            <td style="width: 250px">
                                                Customer Type
                                            </td>
                                            <td style="width: 1px">
                                                :
                                            </td>
                                            <td style="width: 69%">
                                                <asp:Label ID="LblJeniscustomerNew" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 250px">
                                                <asp:Label ID="Label11" runat="server" Text="Income"></asp:Label>
                                            </td>
                                            <td style="width: 1px">
                                                :
                                            </td>
                                            <td style="width: 69%">
                                                <asp:Label ID="LblPenghasilanNew" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 250px">
                                                <asp:Label ID="Label12" runat="server" Text="Nominal Transaksi Normal"></asp:Label>
                                            </td>
                                            <td style="width: 1px">
                                                :
                                            </td>
                                            <td style="width: 69%">
                                                <asp:Label ID="LblNTNNew" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 250px">
                                                <asp:Label ID="Label8" runat="server" Font-Bold="True" Text="Final Risk Value" Visible="False"></asp:Label>
                                            </td>
                                            <td style="width: 1px">
                                                &nbsp;
                                            </td>
                                            <td style="width: 69%">
                                                <asp:Label ID="LblFinalRiskValueProfileKeuanganNew" runat="server" Font-Bold="True"
                                                    Visible="False"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:Panel runat="server" ID="PanelExistingcustomer" Visible="false">
                                    <table width="100%">
                                        <tr>
                                            <td style="width: 100px">
                                                Customer Type
                                            </td>
                                            <td style="width: 1px">
                                                :
                                            </td>
                                            <td style="width: 99%">
                                                <asp:Label ID="LblJeniscustomerExisting" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <table border="1" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td rowspan="2" style="width: 200px; background-color: silver" valign="top">
                                                        </td>
                                                        <td align="center" colspan="2" style="background-color: silver" valign="top">
                                                            <asp:Label ID="Label7" runat="server" Font-Bold="True" Text="Modus Amount"></asp:Label>
                                                        </td>
                                                        <td align="center" colspan="2" style="background-color: silver" valign="top">
                                                            <asp:Label ID="Label14" runat="server" Font-Bold="True" Text="Modus Freq /Per Period"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" style="width: 170px; background-color: silver" valign="top">
                                                            <strong>Debet</strong>
                                                        </td>
                                                        <td align="center" style="width: 170px; background-color: silver" valign="top">
                                                            <strong>Kredit</strong>
                                                        </td>
                                                        <td align="center" style="width: 170px; background-color: silver" valign="top">
                                                            <strong>Debet</strong>
                                                        </td>
                                                        <td align="center" style="width: 170px; background-color: silver" valign="top">
                                                            <strong>Kredit</strong>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 200px; background-color: silver" valign="top">
                                                            <asp:Label ID="Label17" runat="server" Font-Bold="True" Text="Tiering"></asp:Label>
                                                        </td>
                                                        <td style="width: 270px" valign="top">
                                                            <asp:Label ID="LblMOdusAmountDebet" runat="server"></asp:Label>
                                                        </td>
                                                        <td style="width: 270px" valign="top">
                                                            <asp:Label ID="LblMOdusAmountKredit" runat="server"></asp:Label>
                                                        </td>
                                                        <td style="width: 150px" valign="top">
                                                            <asp:Label ID="LblMOdusFreqPerPeriodDebet" runat="server"></asp:Label>
                                                        </td>
                                                        <td style="width: 150px" valign="top">
                                                            <asp:Label ID="LblMOdusFreqPerPeriodKredit" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 200px; background-color: silver; height: 20px;" valign="top">
                                                            <asp:Label ID="Label4" runat="server" Font-Bold="True" Text="Risk Value"></asp:Label>
                                                        </td>
                                                        <td align="center" style="width: 270px; height: 20px;" valign="top">
                                                            <asp:Label ID="LblRiskValueModusAmountDebet" runat="server"></asp:Label>
                                                        </td>
                                                        <td align="center" style="width: 270px; height: 20px;" valign="top">
                                                            <asp:Label ID="LblRiskValueModusAmountKredit" runat="server"></asp:Label>
                                                        </td>
                                                        <td align="center" style="width: 150px; height: 20px;" valign="top">
                                                            <asp:Label ID="LblRiskValueModusFreqPerPeriodDebet" runat="server"></asp:Label>
                                                        </td>
                                                        <td align="center" style="width: 150px; height: 20px;" valign="top">
                                                            <asp:Label ID="LblRiskValueModusFreqPerPeriodKredit" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 200px; background-color: silver" valign="top">
                                                            <asp:Label ID="Label6" runat="server" Font-Bold="True" Text="Final Risk Value"></asp:Label>
                                                        </td>
                                                        <td align="center" colspan="4" valign="top">
                                                            <asp:Label ID="LblFinalRiskValueProfileKeuangan1" runat="server" Font-Bold="True"
                                                                Font-Size="Larger"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="PanelExistingCustomerOutlier" runat="server">
                                     <table width="100%">
                                        <tr>
                                            <td style="width: 100px">
                                                Customer Type
                                            </td>
                                            <td style="width: 1px">
                                                :
                                            </td>
                                            <td style="width: 99%">
                                                <asp:Label ID="LblJeniscustomerExistingOutlier" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <table border="1" cellpadding="0" cellspacing="0">
                                                    <tr >
                                                        <td style="width: 200px; background-color: silver" align="center" >
                                                            <asp:Label ID="Label13" runat="server" Text="POINT" Font-Bold="True" ></asp:Label>  
                                                        </td>
                                                        <td style="width: 200px; background-color: silver" >
                                                           <asp:Label ID="Label20" runat="server" Text="Keterangan" Font-Bold="True" ></asp:Label>  
                                                        </td>
                                                        <td style="width: 200px; background-color: silver" >
                                                            <asp:Label ID="Label21" runat="server" Text="Debet" Font-Bold="True" ></asp:Label></td>
                                                        <td style="width: 200px; background-color: silver" >
                                                            <asp:Label ID="Label22" runat="server" Text="Credit" Font-Bold="True" ></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 200px; background-color: silver" align="center" >
                                                            <asp:Label ID="Label18" runat="server" Text="A" Font-Bold="True" ></asp:Label>
                                                        </td>
                                                        <td>
                                                       <asp:Label ID="Label23" runat="server" Text="Profil Transaksi Nasabah Per CIF" Font-Bold="True" ></asp:Label>   
                                                        </td>
                                                        <td>
                                                         <asp:Label ID="LblNilaiOutlierDebet" runat="server" Text="" Font-Bold="True" ></asp:Label> &nbsp;
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="LblNilaiOutlierKredit" runat="server" Text="" Font-Bold="True" ></asp:Label>&nbsp;
                                                            </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 200px; background-color: silver" align="center">
                                                            <asp:Label ID="Label19" runat="server" Text="B" Font-Bold="True" ></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="Label24" runat="server" Text="Total Transaksi Per hari Per CIF" Font-Bold="True" ></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="LblTotalTransDebetPerCIF" runat="server" Text="" Font-Bold="True"> </asp:Label>&nbsp;
                                                        </td>
                                                        <td>
                                                               <asp:Label ID="LblTotalTransCreditPerCIF" runat="server" Text="" Font-Bold="True" ></asp:Label>&nbsp;</td>
                                                    </tr>
                                                </table>
                                                <asp:Label ID="Label25" runat="server" 
                                                    Text="**) Alert akan timbul jika point B lebih besar dari A" Font-Bold="True" 
                                                    ForeColor="Red"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>

                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:Panel runat="server" ID="PanelTransKeuanganAmount" Visible="false">
                                    <div id="divTransKeuanganAmount"   runat="server" style="overflow: auto; height: 250px;
                                        width: 1024px">
                                        <asp:Label Text="Transaction that deviates from Amount" runat="server" ID="Lblketamount"
                                            Font-Bold="True" Font-Size="Medium" />
                                        <br />
                                        <ajax:AjaxPanel ID="ajx" runat="server"   >
                                           
                                            <asp:GridView ID="GridViewTransKeuangan" runat="server" AutoGenerateColumns="False"
                                                CellPadding="4" ForeColor="Black" GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE"
                                                BorderStyle="None" BorderWidth="1px" EnableModelValidation="True">
                                                <FooterStyle BackColor="#CCCC99" />
                                                <Columns>
                                                    <asp:BoundField DataField="PK_CaseManagementProfileKeuanganTransaction_Id" HeaderText="PK_CaseManagementProfileKeuanganTransaction_Id"
                                                        Visible="False" />
                                                    <asp:BoundField DataField="FK_CaseManagementID" HeaderText="FK_CaseManagementID"
                                                        Visible="False" />
                                                    <asp:BoundField DataField="TransactionDetailId" HeaderText="TransactionDetailId"
                                                        Visible="False" />
                                                    <asp:BoundField DataField="TransactionDate" DataFormatString="{0:dd MMM yyyy}" HeaderText="TGL"
                                                        HtmlEncode="False" />
                                                                <asp:TemplateField HeaderText="CIFNo">
                                                        <ItemTemplate>
                                                            <%#GenerateCIFLink(Eval("CIFNO"))%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                     <asp:BoundField DataField="AccountName" HeaderText="Nama" />
                                                     <asp:TemplateField HeaderText="AccountNo">
                                                        <ItemTemplate>
                                                            <%#GenerateAccountLink(Eval("AccountNo")) %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                      <asp:BoundField DataField="AuxiliaryTransactionCodeName" HeaderText="AuxiliaryTransactionCode" />
 <asp:BoundField DataField="TransactionAmount" HeaderText="Original Amount" DataFormatString="{0:#,#.00}"
                                                        HtmlEncode="False">
                                                        <ItemStyle HorizontalAlign="Right" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="CurrencyType" HeaderText="CurrencyType" />
                                                     <asp:TemplateField HeaderText="Transaction In IDR Debet">
                                                        
                                                        <ItemTemplate>
                                                            <asp:Label ID="LblTransactionDebet" runat="server" 
                                                                ></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Right" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Transaction In IDR Credit">
                                                        
                                                        <ItemTemplate>
                                                            <asp:Label ID="LblTransactionCredit" runat="server" ></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Right" />
                                                    </asp:TemplateField>
                                                    

                                                    <asp:BoundField DataField="AccountOwner" HeaderText="Transaction Branch" />
                                                   
                                            
                                                    
                                                   
                                                  
                                                   
                                                    
                                                  
                                                 
                                                    <asp:BoundField DataField="TransactionRemark" HeaderText="TransactionRemark" />
                                                 

                                                   
                                                    
                                                  <asp:BoundField DataField="AccountNameAccountLawan" HeaderText="Counterpart Name" />
                                                    <asp:BoundField DataField="AccountLawan" HeaderText="Counterpart Acc No" />
                                                   
                                                    
                                                  
                                                </Columns>
                                                <RowStyle BackColor="#F7F7DE" />
                                                <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                                <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                                <AlternatingRowStyle BackColor="White" />
                                                <EmptyDataTemplate>
                                                    There is no Related Transaction Financial Profile For this Case Management
                                                </EmptyDataTemplate>
                                            </asp:GridView>
                                        </ajax:AjaxPanel>
                                        <table id="table6" bgcolor="#ffffff" border="2" bordercolor="#ffffff" cellpadding="2"
                                            cellspacing="1" width="100%">
                                            <tr align="center" bgcolor="#dddddd" class="regtext">
                                                <td align="left" bgcolor="#ffffff" style="width: 50%" valign="top">
                                                    Page &nbsp;
                                                    <ajax:AjaxPanel ID="ajaxPanel_TotalPages" runat="server">
                                                        <asp:Label ID="PageCurrentPagetransKeuanganAmount" runat="server" CssClass="regtext">
                            0
                                                        </asp:Label>
                                                        &nbsp;of&nbsp;
                                                        <asp:Label ID="PageTotalPagestransKeuanganAmount" runat="server" CssClass="regtext">
                            0
                                                        </asp:Label>
                                                    </ajax:AjaxPanel>
                                                </td>
                                                <td align="right" bgcolor="#ffffff" valign="top" width="50%">
                                                    Total Records &nbsp;
                                                    <ajax:AjaxPanel ID="ajaxPanel_TotalRows" runat="server">
                                                        <asp:Label ID="PageTotalRowstransKeuanganAmount" runat="server">
                            0
                                                        </asp:Label>
                                                    </ajax:AjaxPanel>
                                                </td>
                                            </tr>
                                        </table>
                                        <table id="table7" bgcolor="#ffffff" border="2" bordercolor="#ffffff" cellpadding="2"
                                            cellspacing="1" width="100%">
                                            <tr bgcolor="#ffffff">
                                                <td align="left" class="regtext" colspan="11" height="7" valign="middle">
                                                    <hr color="#f40101" noshade="noshade" size="1" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="63">
                                                    Go to page
                                                </td>
                                                <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="5">
                                                    <font face="Verdana, Arial, Helvetica, sans-serif" size="1">
                                                        <ajax:AjaxPanel ID="ajaxPanel_GoTo" runat="server">
                                                            <asp:TextBox ID="TextGoToPagetransKeuanganAmmount" runat="server" CssClass="searcheditbox"
                                                                Width="38px"></asp:TextBox>
                                                        </ajax:AjaxPanel>
                                                    </font>
                                                </td>
                                                <td align="left" bgcolor="#ffffff" class="regtext" valign="middle">
                                                    <ajax:AjaxPanel ID="ajaxPanel_Go" runat="server">
                                                        <asp:ImageButton ID="ImageButtonGoTransKeuanganAmmount" runat="server" ImageUrl="~/images/button/go.gif" />
                                                    </ajax:AjaxPanel>
                                                </td>
                                                <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="6">
                                                    <img height="5" src="images/first.gif" width="6" />
                                                </td>
                                                <td align="right" bgcolor="#ffffff" class="regtext" valign="middle" width="6">
                                                    <ajax:AjaxPanel ID="ajaxPanel_First" runat="server">
                                                        <asp:LinkButton ID="LinkButtonFirsttransKeuanganAmount" runat="server" CommandName="First"
                                                            CssClass="regtext" OnCommand="PageNavigatetransKeuanganAmount">
                            First
                                                        </asp:LinkButton>
                                                    </ajax:AjaxPanel>
                                                </td>
                                                <td align="right" bgcolor="#ffffff" class="regtext" valign="middle" width="6">
                                                    <img height="5" src="images/prev.gif" width="6" />
                                                </td>
                                                <td align="right" bgcolor="#ffffff" class="regtext" valign="middle" width="14">
                                                    <ajax:AjaxPanel ID="ajaxPanel_Previous" runat="server">
                                                        <asp:LinkButton ID="LinkButtonPrevioustransKeuanganAmount" runat="server" CommandName="Prev"
                                                            CssClass="regtext" OnCommand="PageNavigatetransKeuanganAmount">
                            Previous
                                                        </asp:LinkButton>
                                                    </ajax:AjaxPanel>
                                                </td>
                                                <td align="right" bgcolor="#ffffff" class="regtext" valign="middle" width="60">
                                                    <ajax:AjaxPanel ID="ajaxPanel_Next" runat="server">
                                                        <a class="pageNav" href="#">
                                                            <asp:LinkButton ID="LinkButtonNexttransKeuanganAmount" runat="server" CommandName="Next"
                                                                CssClass="regtext" OnCommand="PageNavigatetransKeuanganAmount">
                                Next
                                                            </asp:LinkButton>
                                                        </a>
                                                    </ajax:AjaxPanel>
                                                </td>
                                                <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="6">
                                                    <img height="5" src="images/next.gif" width="6" />
                                                </td>
                                                <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="25">
                                                    <ajax:AjaxPanel ID="ajaxPanel_Last" runat="server">
                                                        <asp:LinkButton ID="LinkButtonLasttransKeuanganAmount" runat="server" CommandName="Last"
                                                            CssClass="regtext" OnCommand="PageNavigatetransKeuanganAmount">
                            Last
                                                        </asp:LinkButton>
                                                    </ajax:AjaxPanel>
                                                </td>
                                                <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="6">
                                                    <img height="5" src="images/last.gif" width="6" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:Panel runat="server" ID="PanelTransKeuanganFreqPeriod" Visible="false">
                                    <div id="divTransKeuanganFreqPeriod" runat="server" style="overflow: auto; height: 250px;
                                        width: 1024px">
                                        <asp:Label Text="Transaction that deviates from Modus Frequency Per day Per Period"
                                            runat="server" ID="Label5" Font-Bold="True" Font-Size="Medium" />
                                        <br />
                                        <ajax:AjaxPanel ID="ajxfreq" runat="server">
                                              
                    
                                             <asp:GridView ID="GridViewTransKeuanganFreq" runat="server" AutoGenerateColumns="False"
                                                CellPadding="4" ForeColor="Black" GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE"
                                                BorderStyle="None" BorderWidth="1px" EnableModelValidation="True">
                                                <FooterStyle BackColor="#CCCC99" />
                                                <Columns>
                                                    <asp:BoundField DataField="PK_CaseManagementProfileKeuanganTransactionFreqPerPeriod_Id" HeaderText="PK_CaseManagementProfileKeuanganTransactionFreqPerPeriod_Id"
                                                        Visible="False" />
                                                    <asp:BoundField DataField="FK_CaseManagementID" HeaderText="FK_CaseManagementID"
                                                        Visible="False" />
                                                    <asp:BoundField DataField="TransactionDetailId" HeaderText="TransactionDetailId"
                                                        Visible="False" />
                                                    <asp:BoundField DataField="TransactionDate" DataFormatString="{0:dd MMM yyyy}" HeaderText="TGL"
                                                        HtmlEncode="False" />
                                                                <asp:TemplateField HeaderText="CIFNo">
                                                        <ItemTemplate>
                                                            <%#GenerateCIFLink(Eval("CIFNO"))%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                     <asp:BoundField DataField="AccountName" HeaderText="Nama" />
                                                     <asp:TemplateField HeaderText="AccountNo">
                                                        <ItemTemplate>
                                                            <%#GenerateAccountLink(Eval("AccountNo")) %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                      <asp:BoundField DataField="AuxiliaryTransactionCodeName" HeaderText="AuxiliaryTransactionCode" />
 <asp:BoundField DataField="TransactionAmount" HeaderText="Original Amount" DataFormatString="{0:#,#.00}"
                                                        HtmlEncode="False">
                                                        <ItemStyle HorizontalAlign="Right" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="CurrencyType" HeaderText="CurrencyType" />
                                                     <asp:TemplateField HeaderText="Transaction In IDR Debet">
                                                        
                                                        <ItemTemplate>
                                                            <asp:Label ID="LblTransactionDebet" runat="server" 
                                                                ></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Right" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Transaction In IDR Credit">
                                                        
                                                        <ItemTemplate>
                                                            <asp:Label ID="LblTransactionCredit" runat="server" ></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Right" />
                                                    </asp:TemplateField>
                                                    

                                                    <asp:BoundField DataField="AccountOwner" HeaderText="Transaction Branch" />
                                                   
                                            
                                                    
                                                   
                                                  
                                                   
                                                    
                                                  
                                                 
                                                    <asp:BoundField DataField="TransactionRemark" HeaderText="TransactionRemark" />
                                                 

                                                   
                                                    
                                                  <asp:BoundField DataField="AccountNameAccountLawan" HeaderText="Counterpart Name" />
                                                    <asp:BoundField DataField="AccountLawan" HeaderText="Counterpart Acc No" />
                                                   
                                                    
                                                  
                                                </Columns>
                                                <RowStyle BackColor="#F7F7DE" />
                                                <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                                <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                                <AlternatingRowStyle BackColor="White" />
                                                <EmptyDataTemplate>
                                                    There is no Related Transaction Financial Profile For this Case Management
                                                </EmptyDataTemplate>
                                            </asp:GridView>
                                        </ajax:AjaxPanel>
                                        <table id="table1" bgcolor="#ffffff" border="2" bordercolor="#ffffff" cellpadding="2"
                                            cellspacing="1" width="100%">
                                            <tr align="center" bgcolor="#dddddd" class="regtext">
                                                <td align="left" bgcolor="#ffffff" style="width: 50%" valign="top">
                                                    Page &nbsp;
                                                    <ajax:AjaxPanel ID="ajaxPanel1" runat="server">
                                                        <asp:Label ID="PageCurrentPagetransKeuanganFreq" runat="server" CssClass="regtext">
                            0
                                                        </asp:Label>
                                                        &nbsp;of&nbsp;
                                                        <asp:Label ID="PageTotalPagestransKeuanganFreq" runat="server" CssClass="regtext">
                            0
                                                        </asp:Label>
                                                    </ajax:AjaxPanel>
                                                </td>
                                                <td align="right" bgcolor="#ffffff" valign="top" width="50%">
                                                    Total Records &nbsp;
                                                    <ajax:AjaxPanel ID="ajaxPanel2" runat="server">
                                                        <asp:Label ID="PageTotalRowstransKeuanganFreq" runat="server">
                            0
                                                        </asp:Label>
                                                    </ajax:AjaxPanel>
                                                </td>
                                            </tr>
                                        </table>
                                        <table id="table2" bgcolor="#ffffff" border="2" bordercolor="#ffffff" cellpadding="2"
                                            cellspacing="1" width="100%">
                                            <tr bgcolor="#ffffff">
                                                <td align="left" class="regtext" colspan="11" height="7" valign="middle">
                                                    <hr color="#f40101" noshade="noshade" size="1" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="63">
                                                    Go to page
                                                </td>
                                                <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="5">
                                                    <font face="Verdana, Arial, Helvetica, sans-serif" size="1">
                                                        <ajax:AjaxPanel ID="ajaxPanel3" runat="server">
                                                            <asp:TextBox ID="TextGoToPagetransKeuanganFreq" runat="server" CssClass="searcheditbox"
                                                                Width="38px"></asp:TextBox>
                                                        </ajax:AjaxPanel>
                                                    </font>
                                                </td>
                                                <td align="left" bgcolor="#ffffff" class="regtext" valign="middle">
                                                    <ajax:AjaxPanel ID="ajaxPanel4" runat="server">
                                                        <asp:ImageButton ID="ImageButtonGoTransKeuanganFreq" runat="server" ImageUrl="~/images/button/go.gif" />
                                                    </ajax:AjaxPanel>
                                                </td>
                                                <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="6">
                                                    <img height="5" src="images/first.gif" width="6" />
                                                </td>
                                                <td align="right" bgcolor="#ffffff" class="regtext" valign="middle" width="6">
                                                    <ajax:AjaxPanel ID="ajaxPanel5" runat="server">
                                                        <asp:LinkButton ID="LinkButtonFirsttransKeuanganFreq" runat="server" CommandName="First"
                                                            CssClass="regtext" OnCommand="PageNavigatetransKeuanganFreq">
                            First
                                                        </asp:LinkButton>
                                                    </ajax:AjaxPanel>
                                                </td>
                                                <td align="right" bgcolor="#ffffff" class="regtext" valign="middle" width="6">
                                                    <img height="5" src="images/prev.gif" width="6" />
                                                </td>
                                                <td align="right" bgcolor="#ffffff" class="regtext" valign="middle" width="14">
                                                    <ajax:AjaxPanel ID="ajaxPanel6" runat="server">
                                                        <asp:LinkButton ID="LinkButtonPrevioustransKeuanganFreq" runat="server" CommandName="Prev"
                                                            CssClass="regtext" OnCommand="PageNavigatetransKeuanganFreq">
                            Previous
                                                        </asp:LinkButton>
                                                    </ajax:AjaxPanel>
                                                </td>
                                                <td align="right" bgcolor="#ffffff" class="regtext" valign="middle" width="60">
                                                    <ajax:AjaxPanel ID="ajaxPanel7" runat="server">
                                                        <a class="pageNav" href="#">
                                                            <asp:LinkButton ID="LinkButtonNexttransKeuanganFreq" runat="server" CommandName="Next"
                                                                CssClass="regtext" OnCommand="PageNavigatetransKeuanganFreq">
                                Next
                                                            </asp:LinkButton>
                                                        </a>
                                                    </ajax:AjaxPanel>
                                                </td>
                                                <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="6">
                                                    <img height="5" src="images/next.gif" width="6" />
                                                </td>
                                                <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="25">
                                                    <ajax:AjaxPanel ID="ajaxPanel8" runat="server">
                                                        <asp:LinkButton ID="LinkButtonLasttransKeuanganFreq" runat="server" CommandName="Last"
                                                            CssClass="regtext" OnCommand="PageNavigatetransKeuanganFreq">
                            Last
                                                        </asp:LinkButton>
                                                    </ajax:AjaxPanel>
                                                </td>
                                                <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="6">
                                                    <img height="5" src="images/last.gif" width="6" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td colspan="3">
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Panel runat="server" ID="PanelTypologi" Visible="false">
                    <table style="width: 100%">
                        <tr>
                            <td colspan="2" valign="top">
                                <asp:Label ID="Labelketmony" runat="server" Font-Size="Large" Text="Typology of Transactions"
                                    Font-Bold="True"></asp:Label>
                            </td>
                            <td valign="top">
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <table style="width: 100%">
                                    <tr>
                                        <td style="width: 20%" valign="top">
                                            <asp:Label ID="Label9" runat="server" Font-Bold="True" Text="Final Risk Value" Visible="False"></asp:Label>
                                        </td>
                                        <td style="width: 1px" valign="top">
                                        </td>
                                        <td style="width: 80%" valign="top">
                                            <asp:Label ID="lblFinalTopologyValue" runat="server" Font-Bold="True" Visible="False"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top">
                            </td>
                            <td valign="top">
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <asp:Label ID="Label10" runat="server" Text="Typology Type"></asp:Label>
                                <asp:Label ID="lblTopology" runat="server"></asp:Label>
                            </td>
                            <td valign="top">
                            
                            </td>
                            <td valign="top">
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">Description :<asp:Label ID="lblDescTopology" runat="server"></asp:Label>
                            </td>
                            <td valign="top">&nbsp;</td>
                            <td valign="top">&nbsp;</td>
                        </tr>
                        <tr>
                            <td valign="top">
                            CIF No :
                                <asp:LinkButton ID="lblciftipology" runat="server"></asp:LinkButton>
                                <%--<asp:Label ID="lblciftipology" runat="server" Text=""></asp:Label>--%>
                            </td>
                            <td valign="top">
                            </td>
                            <td valign="top">
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                            Account No :
                            <asp:LinkButton ID="AccountNotypology" runat="server"></asp:LinkButton>
                            <%--<asp:Label ID="AccountNotypology" runat="server" Text=""></asp:Label>--%>
                            </td>
                            <td valign="top">
                            </td>
                            <td valign="top">
                            </td>
                        </tr>
                    </table>
                    <br />
                    <div id="divRelatedTransaction" runat="server" style="overflow: auto; height: 250px;
                        width: 1024px">
     <ajax:AjaxPanel ID="ajaxPanel12" runat="server">                     
                       
                      
                                             <asp:GridView ID="GridViewRelatedTransaction" runat="server" AutoGenerateColumns="False"
                                                CellPadding="4" ForeColor="Black" GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE"
                                                BorderStyle="None" BorderWidth="1px" EnableModelValidation="True">
                                                <FooterStyle BackColor="#CCCC99" />
                                                <Columns>
                                                    <asp:BoundField DataField="PK_MapCaseManagementTransactionID" HeaderText="PK_MapCaseManagementTransactionID"
                                                        Visible="False" />
                                                    <asp:BoundField DataField="FK_CaseManagementID" HeaderText="FK_CaseManagementID"
                                                        Visible="False" />
                                                    <asp:BoundField DataField="TransactionDetailId" HeaderText="TransactionDetailId"
                                                        Visible="False" />
                                                    <asp:BoundField DataField="TransactionDate" DataFormatString="{0:dd MMM yyyy}" HeaderText="TGL"
                                                        HtmlEncode="False" />
                                                                <asp:TemplateField HeaderText="CIFNo">
                                                        <ItemTemplate>
                                                            <%#GenerateCIFLink(Eval("CIFNO"))%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                     <asp:BoundField DataField="AccountName" HeaderText="Nama" />
                                                     <asp:TemplateField HeaderText="AccountNo">
                                                        <ItemTemplate>
                                                            <%#GenerateAccountLink(Eval("AccountNo")) %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                      <asp:BoundField DataField="AuxiliaryTransactionCodeName" HeaderText="AuxiliaryTransactionCode" />
 <asp:BoundField DataField="TransactionAmount" HeaderText="Original Amount" DataFormatString="{0:#,#.00}"
                                                        HtmlEncode="False">
                                                        <ItemStyle HorizontalAlign="Right" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="CurrencyType" HeaderText="CurrencyType" />
                                                     <asp:TemplateField HeaderText="Transaction In IDR Debet">
                                                        
                                                        <ItemTemplate>
                                                            <asp:Label ID="LblTransactionDebet" runat="server" 
                                                                ></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Right" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Transaction In IDR Credit">
                                                        
                                                        <ItemTemplate>
                                                            <asp:Label ID="LblTransactionCredit" runat="server" ></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Right" />
                                                    </asp:TemplateField>
                                                    

                                                    <asp:BoundField DataField="AccountOwner" HeaderText="Transaction Branch" />
                                                   
                                            
                                                    
                                                   
                                                  
                                                   
                                                    
                                                  
                                                 
                                                    <asp:BoundField DataField="TransactionRemark" HeaderText="TransactionRemark" />
                                                    <asp:BoundField DataField="AccountNameAccountLawan" HeaderText="Counterpart Name" />
                                                    <asp:BoundField DataField="AccountLawan" HeaderText="Counterpart Acc No" />
                                                 

                                                   
                                                    
                                                 
                                                   
                                                    
                                                  
                                                </Columns>
                                                <RowStyle BackColor="#F7F7DE" />
                                                <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                                <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                                <AlternatingRowStyle BackColor="White" />
                                                <EmptyDataTemplate>
                                                    There is no Related Transaction Financial Profile For this Case Management
                                                </EmptyDataTemplate>
                                            </asp:GridView>

          <asp:GridView ID="GridViewRelatedTransactionNew" runat="server" AllowPaging="True" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" EnableModelValidation="True" ForeColor="Black" GridLines="Vertical">
            <AlternatingRowStyle BackColor="White" />
            <FooterStyle BackColor="#CCCC99" />
            <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
            <RowStyle BackColor="#F7F7DE" />
            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
        </asp:GridView>
                                            </ajax:AjaxPanel>
                    </div>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table style="width: 100%">
                    <tr>
                        <td>
                            <asp:Label ID="Labelwh" runat="server" Font-Bold="True" Font-Size="Large" Text="Workflow History"></asp:Label>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <ajax:AjaxPanel ID="AjaxPanel11" runat="server">
                                <asp:GridView ID="GridViewWorkflowHistory" runat="server" AutoGenerateColumns="False"
                                    CellPadding="4" ForeColor="Black" GridLines="Vertical" DataKeyNames="pk_mapcasemanagementworkflowhistoryid"
                                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px">
                                    <FooterStyle BackColor="#CCCC99" />
                                    <Columns>
                                        <asp:BoundField HeaderText="pk_mapcasemanagementworkflowhistoryid" Visible="False"
                                            DataField="pk_mapcasemanagementworkflowhistoryid">
                                            <ItemStyle VerticalAlign="Top" />
                                        </asp:BoundField>
                                        <asp:BoundField DataFormatString="{0: dd MMM yyyy HH:mm}" HeaderText="Date" HtmlEncode="False"
                                            DataField="CreatedDate">
                                            <ItemStyle VerticalAlign="Top" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="Event Type" DataField="EventTypeDescription">
                                            <ItemStyle VerticalAlign="Top" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="Workflow Step" DataField="workflowstep">
                                            <ItemStyle VerticalAlign="Top" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="User ID" DataField="userid">
                                            <ItemStyle VerticalAlign="Top" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="Investigation Notes" DataField="investigationNotes">
                                            <ItemStyle VerticalAlign="Top" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="Proposed Action" DataField="ProposedAction">
                                            <ItemStyle VerticalAlign="Top" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="description" HeaderText="Description" />
                                        <asp:TemplateField HeaderText="Attach File">
                                            <ItemTemplate>
                                                <asp:DataList ID="DataList1" runat="server">
                                                    <ItemTemplate>
                                                        <%#GenerateLink(Eval("PK_MapCaseManagementWorkflowHistoryAttachmentID"), Eval("Filename"))%>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle BackColor="#F7F7DE" />
                                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                    <AlternatingRowStyle BackColor="White" />
                                </asp:GridView>
                            </ajax:AjaxPanel>
                        </td>
                    </tr>
                    <tr>
                        <%#GenerateCIFLink(Eval("CIFNO"))%>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:Panel ID="PanelQuestionOldTable" runat="server">
                              <ajax:AjaxPanel ID="AjaxPanel9" runat="server">
                                <table style="width: 100%">
                                    <tr>
                                        <td style="height: 15px" nowrap="noWrap" valign="top">
                                            Sumber Dana Berasal Dari
                                        </td>
                                        <td nowrap="nowrap" style="width: 1px; height: 15px" valign="top">
                                            :
                                        </td>
                                        <td nowrap="nowrap" style="width: 90%; height: 15px" valign="top">
                                            <asp:TextBox ID="txtSumberDana" runat="server" Columns="100" MaxLength="1000"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="nowrap" style="height: 15px" valign="top">
                                            Latar Belakang Transaksi
                                            <br />
                                            Lengkapi Pertanyaan disamping (hapus yang tidak perlu)</td>
                                        <td nowrap="nowrap" style="width: 1px; height: 15px" valign="top">
                                            :
                                        </td>
                                        <td nowrap="nowrap" style="width: 90%; height: 15px" valign="top">
                                            <asp:TextBox ID="txtLatarBelakangTransaksi" runat="server" Columns="100" Rows="8"
                                            
                                                TextMode="MultiLine"></asp:TextBox>
                                        </td>
                                    </tr>
                       
                                    <tr>
                                        <td nowrap="nowrap" style="height: 15px" valign="top">
                                        </td>
                                        <td nowrap="nowrap" style="width: 1px; height: 15px" valign="top">
                                        </td>
                                        <td nowrap="nowrap" style="width: 90%; height: 15px" valign="top">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" nowrap="nowrap" style="height: 15px" valign="top">
                                            Sebutkan Informasi terkini data nasabah sehubungan dengan penjelasan transaksi
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="nowrap" style="height: 15px" valign="top">
                                            Jenis Pekerjaan/Usaha
                                        </td>
                                        <td nowrap="nowrap" style="width: 1px; height: 15px" valign="top">
                                            :
                                        </td>
                                        <td nowrap="nowrap" style="width: 90%; height: 15px" valign="top">
                                            <asp:TextBox ID="txtJenisPekerjaan" runat="server" Columns="100" MaxLength="1000"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="nowrap" style="height: 15px" valign="top">
                                            Bidang Pekerjaan/Usaha
                                        </td>
                                        <td nowrap="nowrap" style="width: 1px; height: 15px" valign="top">
                                            :
                                        </td>
                                        <td nowrap="nowrap" style="width: 90%; height: 15px" valign="top">
                                            <asp:TextBox ID="txtBidangPekerjaan" runat="server" Columns="100" MaxLength="1000"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="nowrap" style="height: 15px" valign="top">
                                            Nama Perusahaan dan
                                            Lokasi Pekerjaan/Usaha
                                        </td>
                                        <td nowrap="nowrap" style="width: 1px; height: 15px" valign="top">
                                            :
                                        </td>
                                        <td nowrap="nowrap" style="width: 90%; height: 15px" valign="top">
                                            <asp:TextBox ID="txtLokasiPekerjaan" runat="server" Columns="100" MaxLength="1000"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="nowrap" style="height: 15px" valign="top">
                                            Jabatan/Lama Usaha
                                        </td>
                                        <td nowrap="nowrap" style="width: 1px; height: 15px" valign="top">
                                            :
                                        </td>
                                        <td nowrap="nowrap" style="width: 90%; height: 15px" valign="top">
                                            <asp:TextBox ID="txtJabatan" runat="server" Columns="100" MaxLength="1000"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="nowrap" style="height: 15px" valign="top">
                                        </td>
                                        <td nowrap="nowrap" style="width: 1px; height: 15px" valign="top">
                                        </td>
                                        <td nowrap="nowrap" style="width: 90%; height: 15px" valign="top">
                                        </td>
                                    </tr>
                                                 <tr>
                                        <td nowrap="nowrap" colspan="3" valign="top">
                                            <asp:Panel runat="server" ID="PanelPenipuan" Visible="false">
                                                <table>
                                                    <tr>
                                                        <td nowrap="nowrap" style="height: 15px" valign="top">
                                                            Apakah Sudah dilakukan Verifikasi telepon Rumah /Kantor/Tempat Usaha ? 
                                                        </td>
                                                        <td nowrap="nowrap" style="width: 1px; height: 15px" valign="top">
                                                        </td>
                                                        <td nowrap="nowrap" style="width: 90%; height: 15px" valign="top">
                                                            <asp:RadioButtonList ID="RDlVerifikasiTelepon" runat="server" RepeatDirection="Horizontal">
                                                                <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                                <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <asp:Panel runat="server" ID="PanelOBProcessTunai" Visible="false">
                                                <table>
                                                    <tr>
                                                        <td nowrap="nowrap" style="height: 15px" valign="top">
                                                            Apakah Alasan dilakukan OB Di process Tunai
                                                        </td>
                                                        <td nowrap="nowrap" style="width: 1px; height: 15px" valign="top">
                                                        </td>
                                                        <td nowrap="nowrap" style="width: 90%; height: 15px" valign="top">
                                                            <asp:TextBox ID="TxtAlasanOBProcessTunai" runat="server" Columns="100" MaxLength="1000"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <asp:Panel runat="server" ID="PanelPassThrough" Visible="false">
                                                <table>
                                                    <tr>
                                                        <td nowrap="nowrap" style="height: 15px" valign="top">
                                                            Apakah Transaksi Tersebut sesuai dengan profile pekerjaan/Usaha Nasabah ? [Y/N]
                                                        </td>
                                                        <td nowrap="nowrap" style="width: 1px; height: 15px" valign="top">
                                                        </td>
                                                        <td nowrap="nowrap" style="width: 90%; height: 15px" valign="top">

                                                            <asp:RadioButtonList ID="rdlProfileSesuaiPekerjaan" runat="server" RepeatDirection="Horizontal">
                                                                <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                                <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                                    <table>
                                                    <tr>
                                                        <td nowrap="nowrap" style="height: 15px" valign="top">
                                                            Apakah Anda merasa nyaman bahwa transaksi tersebut sesuai dengan&nbsp;<br />
                                                            <br />
                                                            a. Profil nasabah (pekerjaan/bidang usaha dan income nasabah) ?</td>
                                                        <td nowrap="nowrap" style="width: 1px; height: 15px" valign="top">
                                                        </td>
                                                        <td nowrap="nowrap" style="width: 90%; height: 15px" valign="top">
                                                        <br/>
                                                            <asp:RadioButtonList ID="RdlFrequensiSesuaiKebiasaan" runat="server" RepeatDirection="Horizontal">
                                                                <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                                <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                    </tr>
                                                        <tr>
                                                            <td nowrap="nowrap" style="height: 15px" valign="top">
                                                                <br />
                                                                b. Kebiasaan nasabah bertransaksi selama ini (dari segi nominal,frekuensi&nbsp;
                                                                dan jenis transaksi)?</td>
                                                            <td nowrap="nowrap" style="width: 1px; height: 15px" valign="top">
                                                            </td>
                                                            <td nowrap="nowrap" style="width: 90%; height: 15px" valign="top"><asp:RadioButtonList ID="RdlIsSesuaiKebiasaanNasabah" runat="server" RepeatDirection="Horizontal">
                                                                <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                                <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                                            </asp:RadioButtonList></td>
                                                        </tr>
                                                        <tr>
                                                            <td nowrap="nowrap" style="height: 15px" valign="top">
                                                                <br />
                                                                Berdasarkan Jawaban diatas ,apakah menurut Anda transaksi tersebut
                                                                mencurigakan ?</td>
                                                            <td nowrap="nowrap" style="width: 1px; height: 15px" valign="top">
                                                            </td>
                                                            <td nowrap="nowrap" style="width: 90%; height: 15px" valign="top">
                                                                <asp:RadioButtonList ID="IsTransaksiMencurigakan" runat="server" RepeatDirection="Horizontal">
                                                                    <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                                    <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                                                </asp:RadioButtonList></td>
                                                        </tr>
                                                </table>
                                                
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" nowrap="nowrap" style="height: 15px" valign="top">
                                            <asp:Button ID="btnSaveAsDraftPenjelasanOldMethod" runat="server" Text="Save As Draft " />
                                            <asp:Button ID="btnProposedPenjelasanOldMethod" runat="server" Text="Done & Proposed " />
                                        </td>
                                    </tr>
                                </table>
                            </ajax:AjaxPanel>
                            </asp:Panel>
                            <asp:Panel ID="PanelQuestionNewTable" runat="server">
                            <ajax:AjaxPanel ID="AjaxQuestionnaire" runat="server" Height="100%" Width="100% ">
                                <table id="AnswerTableContainer">
                                    <tr>
                                        <td>
                                            <asp:Table ID="QuestionnaireTable" runat="server">
                                            </asp:Table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="nowrap" style="height: 15px" valign="top">
                                            <asp:Button ID="btnSaveAsDraftPenjelasan" runat="server" Text="Save As Draft " />
                                            <asp:Button ID="btnProposedPenjelasan" runat="server" Text="Done & Proposed " />
                                        </td>
                                    </tr>
                                </table>
                            </ajax:AjaxPanel>
                            </asp:Panel>

                            
                            <asp:Panel ID="Panel1" runat="server" Height="100%" Width="100% ">
                                <table style="width: 100%">
                                    <tr>
                                        <td colspan="3">
                                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="validation"
                                                ValidationGroup="FillInvestigation" Width="100%" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 15px" nowrap="noWrap" valign="top" width="1%">
                                            Investigation Notes<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                                Display="Dynamic" ErrorMessage="Please Fill Investigation Notes" ValidationGroup="FillInvestigation"
                                                ControlToValidate="TextInvestigationNotes">*</asp:RequiredFieldValidator>
                                        </td>
                                        <td style="height: 15px" width="1">
                                            :
                                        </td>
                                        <td style="height: 15px" width="99%">
                                            <asp:TextBox ID="TextInvestigationNotes" runat="server" Columns="5" Rows="5" TextMode="MultiLine"
                                                Width="400px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="noWrap" width="1%">
                                            Proposed Action
                                        </td>
                                        <td width="1">
                                            :
                                        </td>
                                        <td width="99%">
                                        <ajax:AjaxPanel ID="test" runat="server">
                                        <asp:DropDownList ID="CboProposedAction" runat="server" 
                                                AppendDataBoundItems="True" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </ajax:AjaxPanel>
                                            
                                        </td>
                                    </tr>
                                    <tr id="trAccountOwner" style="display: none;">
                                    
                                        <td nowrap="nowrap" width="1%">
                                            New Account Owner
                                        </td>
                                        <td width="1">
                                            :
                                        </td>
                                        <td width="99%">
                                        <ajax:AjaxPanel ID="AjaxPanel10" runat="server">
                                            <asp:DropDownList ID="CboAccountOwner" runat="server">
                                            </asp:DropDownList>
                                            </ajax:AjaxPanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="noWrap" width="1%">
                                            Attachment<asp:CustomValidator ID="CustomValidator1" runat="server" Display="Dynamic"
                                                ErrorMessage="CustomValidator" ValidationGroup="FillInvestigation">*</asp:CustomValidator>
                                        </td>
                                        <td width="1">
                                            :
                                        </td>
                                        <td width="99%">
                                            <asp:FileUpload ID="FileUpload1" runat="server" />
                                            <asp:Button ID="ButtonAdd" runat="server" Text="Add" ValidationGroup="FillInvestigation" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="nowrap" width="1%">
                                        </td>
                                        <td width="1">
                                        </td>
                                        <td valign="top" width="99%">
                                            <asp:ListBox ID="ListAttachment" runat="server" Width="400px"></asp:ListBox>
                                            <asp:Button ID="ButtonDelete" runat="server" Text="Delete" />
                                        </td>
                                    </tr>
                                </table>
                                <asp:Button ID="BtnSaveAsDraftWorklfow" runat="server" Text="Save As Draft " />
                                <asp:Button ID="BtnDoneProposed" runat="server" Text="Done & Proposed " ValidationGroup="FillInvestigation" /></asp:Panel>
                        </td>
                    </tr>
                </table>
                <asp:ImageButton ID="ImagePrintSTR" runat="server" ImageUrl="~/Images/button/printstrform.gif" />
                <asp:ImageButton ID="ImageButton1" runat="server" SkinID="BackButton" OnClientClick="javascript:history.back()" /></asp:Content>
