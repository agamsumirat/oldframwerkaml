Imports System.Data.SqlClient
Partial Class PeerGroupAdd
    Inherits Parent

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                Me.AbandonSession()
                SetSession()
            End If
            Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)


            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)


            End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Public ReadOnly Property strSessionName() As String
        Get
            Return "PeerGroupQueryBuilderAdd"
        End Get

    End Property
    Public ReadOnly Property oQueryBuilder() As Sahassa.AML.QueryBuilder
        Get
            If Session(strSessionName) Is Nothing Then

                Dim TempQuery As Sahassa.AML.QueryBuilder = New Sahassa.AML.QueryBuilder
                TempQuery.AddTableString("[AllAccount_AllInfo]", "[AllAccount_AllInfo]")
                TempQuery.AddTableString("[CFMAST]", " INNER JOIN  [CFMAST] on [AllAccount_AllInfo].[CIFNo]=[CFMAST].[CFCIF#]")
                TempQuery.AddFieldString(" Distinct [AllAccount_AllInfo].[AccountNo] ")
                Session(strSessionName) = TempQuery
                Return Session(strSessionName)
            Else
                Return CType(Session(strSessionName), Sahassa.AML.QueryBuilder)
            End If
        End Get

    End Property
    Protected Sub Menu1_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs) Handles Menu1.MenuItemClick
        MultiView1.ActiveViewIndex = Int32.Parse(e.Item.Value)
        Tablestest1.ActiveIndex = MultiView1.ActiveViewIndex

        WhereClausesTest1.ActiveIndex = MultiView1.ActiveViewIndex + 1
        GroupBy1.ActiveIndex = MultiView1.ActiveViewIndex + 1
        HavingCondition1.ActiveIndex = MultiView1.ActiveViewIndex + 1


    End Sub
    Private Sub SetSession()
        Tablestest1.OQueryBuilder = Me.oQueryBuilder

        WhereClausesTest1.OQueryBuilder = Me.oQueryBuilder
        GroupBy1.OQueryBuilder = Me.oQueryBuilder
        HavingCondition1.OQueryBuilder = Me.oQueryBuilder

    End Sub
    Private Sub AbandonSession()
        Session(strSessionName) = Nothing
        Tablestest1.OQueryBuilder = Nothing

        WhereClausesTest1.OQueryBuilder = Nothing
        GroupBy1.OQueryBuilder = Nothing
        HavingCondition1.OQueryBuilder = Nothing

    End Sub

    Private Sub InsertPeerGroupBySU()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try

            Using adapter As New AMLDAL.PeerGroupTableAdapters.PeerGroupTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(adapter, Data.IsolationLevel.ReadUncommitted)
                adapter.Insert(TextPeerGroupName.Text.Trim, TextDescription.Text.Trim, chkEnabled.Checked, oQueryBuilder.SQlQuery, Now.ToString("yyyy-MM-dd HH:mm:ss"))

                InsertAuditTrail(oSQLTrans)
                oSQLTrans.Commit()
            End Using
            AbandonSession()
            SetSession()
            Me.LblSuccess.Visible = True
            Me.LblSuccess.Text = "Success to Insert Peer Group."
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If

        End Try
    End Sub
    Private Sub InsertAuditTrail(ByRef oSQLTrans As SqlTransaction)

        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(5)

            Dim PeerGroupDescription As String
            If Me.TextDescription.Text.Trim.Length <= 255 Then
                PeerGroupDescription = Me.TextDescription.Text.Trim
            Else
                PeerGroupDescription = Me.TextDescription.Text.Substring(0, 255)
            End If

            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Peer Group", "Peer Group Name", "Add", "", Me.TextPeerGroupName.Text.Trim, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Peer Group", "Description", "Add", "", PeerGroupDescription, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Peer Group", "Enabled", "Add", "", chkEnabled.Checked, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Peer Group", "Expression ", "Add", "", oQueryBuilder.SQlQuery, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Peer Group", "Created Date", "Add", "", Now.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
            End Using
        Catch
            Throw
        End Try
    End Sub
    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Try
            If Page.IsValid Then
                If IsDataValid() Then
                    If Sahassa.AML.Commonly.SessionUserId.ToLower = "superuser" Then
                        InsertPeerGroupBySU()
                    Else
                        InsertPeerGroupToPendingApproval()
                    End If
                    Session(strSessionName) = Nothing
                End If
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    ''' <summary>
    ''' untuk menyimpan data ke pending approval
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function InsertPeerGroupToPendingApproval() As Boolean
        Dim intHeader As Integer
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            'insert to header

            Using adapter As New AMLDAL.PeerGroupTableAdapters.PeerGroupPendingApprovalTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(adapter, Data.IsolationLevel.ReadUncommitted)
                intHeader = adapter.InsertPeerGroupPendingApproval(TextPeerGroupName.Text.Trim, Sahassa.AML.Commonly.SessionUserId, 1, "Peer Group Add", Now.ToString("dd MMMM yyyy HH:mm:ss"))

                'insert to detail
                Using adapter1 As New AMLDAL.PeerGroupTableAdapters.PeerGroupApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(adapter1, oSQLTrans)
                    adapter1.Insert(intHeader, 0, TextPeerGroupName.Text.Trim, TextDescription.Text.Trim, chkEnabled.Checked, oQueryBuilder.SQlQuery, Now, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
                    oSQLTrans.Commit()
                End Using
            End Using
            AbandonSession()
            SetSession()
            Dim MessagePendingID As Integer = 81601 'MessagePendingID 81601 = Peer Group Add 

            Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & TextPeerGroupName.Text.Trim
            Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & TextPeerGroupName.Text.Trim, False)


        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If

        End Try
    End Function
    ''' <summary>
    ''' Untuk mengecek apakah data yang di input sudah valid
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function IsDataValid() As Boolean


        'cek rules name sudah ada belum di table peergroup
        Using adapter As New AMLDAL.PeerGroupTableAdapters.PeerGroupTableAdapter
            Dim intJmlPeerGroup As Nullable(Of Integer) = adapter.CountPeerGroupByPeerGroupName(TextPeerGroupName.Text.Trim)
            If intJmlPeerGroup.GetValueOrDefault(0) > 0 Then

                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = "Peer Group already exits in Peer Group"
                Return False
            End If
        End Using

        'cek apakah rules name ada di tabel approval
        Using Adapter As New AMLDAL.PeerGroupTableAdapters.PeerGroupPendingApprovalTableAdapter
            Dim intJmlApproval As Integer = Adapter.CountPeerGroupPendingApprovalByUniqueKey(TextPeerGroupName.Text.Trim)
            If intJmlApproval > 0 Then

                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = "Peer Group already exits in Peer Group Pending Approval."
                Return False
            End If
        End Using
        Return True
    End Function
    Protected Sub CustomValidator1_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles CustomValidator1.ServerValidate
        If oQueryBuilder.SQlQuery = "" Then
            args.IsValid = False

            CustomValidator1.ErrorMessage = "Please configure sql expression first!"

        Else
            Try
                oQueryBuilder.ParseSQLQuery()
                args.IsValid = True
                CustomValidator1.ErrorMessage = ""
            Catch ex As Exception
                CustomValidator1.ErrorMessage = "There is an error in sql expression.Please configure sql expression again !"
                args.IsValid = False
            End Try



        End If
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Response.Redirect("PeerGroupView.aspx", False)
    End Sub

End Class
