
Partial Class ProposalSTRView
    Inherits Parent
#Region " Property "
    Private ReadOnly Property PK() As String
        Get
            Return "ProposalSTR.PK_ProposalSTRID"
        End Get
    End Property

    Private ReadOnly Property Tables() As String
        Get
            Return "ProposalSTR INNER JOIN ProposalSTRStatus ON ProposalSTR.FK_ProposalSTRStatusID = ProposalSTRStatus.PK_ProposalSTRStatusId"
        End Get
    End Property
    Private ReadOnly Property Fields() As String
        Get

            Return "distinct ProposalSTR.PK_ProposalSTRID, ProposalSTR.FK_CaseManagementID, ProposalSTR.CreatedDate, Left(ProposalSTR.Kesimpulan, 100) AS Kesimpulan, ProposalSTRStatus.ProposalSTRStatusDescription, ProposalSTR.UserID"

        End Get
    End Property
    ''' <summary>
    ''' selected item store
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("ProposalSTRViewViewSelected") Is Nothing, New ArrayList, Session("ProposalSTRViewViewSelected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("ProposalSTRViewViewSelected") = value
        End Set
    End Property
    ''' <summary>
    ''' setnget search field
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetFieldSearch() As String
        Get
            Return IIf(Session("ProposalSTRViewViewFieldSearch") Is Nothing, "", Session("ProposalSTRViewViewFieldSearch"))
        End Get
        Set(ByVal Value As String)
            Session("ProposalSTRViewViewFieldSearch") = Value
        End Set
    End Property
    ''' <summary>
    ''' search value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetValueSearch() As String
        Get
            Return IIf(Session("ProposalSTRViewViewValueSearch") Is Nothing, "", Session("ProposalSTRViewViewValueSearch"))
        End Get
        Set(ByVal Value As String)
            Session("ProposalSTRViewViewValueSearch") = Value
        End Set
    End Property
    ''' <summary>
    ''' sort expresion
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("ProposalSTRViewViewSort") Is Nothing, "PK_ProposalSTRID  asc", Session("ProposalSTRViewViewSort"))
        End Get
        Set(ByVal Value As String)
            Session("ProposalSTRViewViewSort") = Value
        End Set
    End Property
    ''' <summary>
    ''' current page index
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("ProposalSTRViewViewCurrentPage") Is Nothing, 0, Session("ProposalSTRViewViewCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("ProposalSTRViewViewCurrentPage") = Value
        End Set
    End Property
    ''' <summary>
    ''' total pages
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    ''' <summary>
    ''' row total
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("ProposalSTRViewViewRowTotal") Is Nothing, 0, Session("ProposalSTRViewViewRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("ProposalSTRViewViewRowTotal") = Value
        End Set
    End Property
    ''' <summary>
    ''' save bind table
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetBindTable() As Data.DataTable
        Get
            Dim strAllWhereClause As String = ""
            strAllWhereClause = Me.SetnGetFieldSearch.Replace("-=Search=-", Me.SetnGetValueSearch)
            'Return IIf(Session("ProposalSTRViewViewData") Is Nothing, New AMLDAL.RulesAdvanced.RulesAdvancedDataTable, Session("ProposalSTRViewViewData"))
            Me.SetnGetRowTotal = Sahassa.AML.Commonly.GetTableCount(Me.Tables, strAllWhereClause)
            Dim pageSize As Long
            If Sahassa.AML.Commonly.SessionPagingLimit = "" Then
                pageSize = 10
            Else
                pageSize = Sahassa.AML.Commonly.SessionPagingLimit
            End If
            'If strAllWhereClause.Trim.Length > 0 Then

            '    strAllWhereClause += " and ProposalSTR.PK_ProposalSTRID in (select fk_proposalstrid from ProposalSTRWorkflowHistory where (ProposalSTRWorkflowHistory.FK_ProposalSTREventTypeID = 2) AND (ProposalSTRWorkflowHistory.bcomplate = 0) AND (ProposalSTRWorkflowHistory.userid LIKE '%" & Sahassa.AML.Commonly.SessionUserId.Replace("'", "''") & "%'))"

            '    'strAllWhereClause += " and (ProposalSTRWorkflowHistory.FK_ProposalSTREventTypeID = " & EnProposalSTREventType.TaskStarted & ") AND (ProposalSTRWorkflowHistory.bcomplate = 0) AND (ProposalSTRWorkflowHistory.baktifworkflow = 1) AND (ProposalSTRWorkflowHistory.userid LIKE '%" & Sahassa.AML.Commonly.SessionUserId.Replace("'", "''") & "%')"
            'Else
            '    strAllWhereClause += "  ProposalSTR.PK_ProposalSTRID in (select fk_proposalstrid from ProposalSTRWorkflowHistory where (ProposalSTRWorkflowHistory.FK_ProposalSTREventTypeID = 2) AND (ProposalSTRWorkflowHistory.bcomplate = 0) AND (ProposalSTRWorkflowHistory.userid LIKE '%" & Sahassa.AML.Commonly.SessionUserId.Replace("'", "''") & "%'))"
            '    'strAllWhereClause += " (ProposalSTRWorkflowHistory.FK_ProposalSTREventTypeID = " & EnProposalSTREventType.TaskStarted & ") AND (ProposalSTRWorkflowHistory.bcomplate = 0) AND (ProposalSTRWorkflowHistory.baktifworkflow = 1) AND (ProposalSTRWorkflowHistory.userid LIKE '%" & Sahassa.AML.Commonly.SessionUserId.Replace("'", "''") & "%')"
            'End If
            Me.SetnGetRowTotal = Sahassa.AML.Commonly.GetTableCount(Me.Tables, strAllWhereClause)
            Return Sahassa.AML.Commonly.GetDatasetPaging(Me.Tables, Me.PK, Me.SetnGetSort, Me.SetnGetCurrentPage + 1, pageSize, Me.Fields, strAllWhereClause, "")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("ProposalSTRViewViewData") = value
        End Set
    End Property
    Private Property SetnGetBindTableAll() As Data.DataTable
        Get
            Dim strAllWhereClause As String = ""
            strAllWhereClause = Me.SetnGetFieldSearch.Replace("-=Search=-", Me.SetnGetValueSearch)
            'Return IIf(Session("ProposalSTRViewViewData") Is Nothing, New AMLDAL.RulesAdvanced.RulesAdvancedDataTable, Session("ProposalSTRViewViewData"))
            Me.SetnGetRowTotal = Sahassa.AML.Commonly.GetTableCount(Me.Tables, strAllWhereClause)
            Dim pageSize As Long
            If Sahassa.AML.Commonly.SessionPagingLimit = "" Then
                pageSize = 10
            Else
                pageSize = Sahassa.AML.Commonly.SessionPagingLimit
            End If
            'If strAllWhereClause.Trim.Length > 0 Then

            '    strAllWhereClause += " and ProposalSTR.PK_ProposalSTRID in (select fk_proposalstrid from ProposalSTRWorkflowHistory where (ProposalSTRWorkflowHistory.FK_ProposalSTREventTypeID = 2) AND (ProposalSTRWorkflowHistory.bcomplate = 0) AND (ProposalSTRWorkflowHistory.userid LIKE '%" & Sahassa.AML.Commonly.SessionUserId.Replace("'", "''") & "%'))"

            '    'strAllWhereClause += " and (ProposalSTRWorkflowHistory.FK_ProposalSTREventTypeID = " & EnProposalSTREventType.TaskStarted & ") AND (ProposalSTRWorkflowHistory.bcomplate = 0) AND (ProposalSTRWorkflowHistory.baktifworkflow = 1) AND (ProposalSTRWorkflowHistory.userid LIKE '%" & Sahassa.AML.Commonly.SessionUserId.Replace("'", "''") & "%')"
            'Else
            '    strAllWhereClause += "  ProposalSTR.PK_ProposalSTRID in (select fk_proposalstrid from ProposalSTRWorkflowHistory where (ProposalSTRWorkflowHistory.FK_ProposalSTREventTypeID = 2) AND (ProposalSTRWorkflowHistory.bcomplate = 0) AND (ProposalSTRWorkflowHistory.userid LIKE '%" & Sahassa.AML.Commonly.SessionUserId.Replace("'", "''") & "%'))"
            '    'strAllWhereClause += " (ProposalSTRWorkflowHistory.FK_ProposalSTREventTypeID = " & EnProposalSTREventType.TaskStarted & ") AND (ProposalSTRWorkflowHistory.bcomplate = 0) AND (ProposalSTRWorkflowHistory.baktifworkflow = 1) AND (ProposalSTRWorkflowHistory.userid LIKE '%" & Sahassa.AML.Commonly.SessionUserId.Replace("'", "''") & "%')"
            'End If
            Me.SetnGetRowTotal = Sahassa.AML.Commonly.GetTableCount(Me.Tables, strAllWhereClause)
            Return Sahassa.AML.Commonly.GetDatasetPaging(Me.Tables, Me.PK, Me.SetnGetSort, 0, Int32.MaxValue, Me.Fields, strAllWhereClause, "")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("ProposalSTRViewViewData") = value
        End Set
    End Property
#End Region

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' fill search
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub FillSearch()
        Try
            Me.ComboSearch.Items.Add(New ListItem("...", ""))
            Me.ComboSearch.Items.Add(New ListItem("ProposalSTRID", "PK_ProposalSTRID Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Kesimpulan", "Kesimpulan Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("CreatedDate", "CreatedDate between '-=Search=- 00:00:00' and '-=Search=- 23:59:59' "))
            Me.ComboSearch.Items.Add(New ListItem("CreatedBy", "UserID Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Status Workflow", "ProposalSTRStatusDescription Like '%-=Search=-%'"))
        Catch
            Throw
        End Try
    End Sub

    Private Sub ClearThisPageSessions()
        Session("ProposalSTRViewViewSelected") = Nothing
        Session("ProposalSTRViewViewFieldSearch") = Nothing
        Session("ProposalSTRViewViewValueSearch") = Nothing
        Session("ProposalSTRViewViewSort") = Nothing
        Session("ProposalSTRViewViewCurrentPage") = Nothing
        Session("ProposalSTRViewViewRowTotal") = Nothing
        Session("ProposalSTRViewViewData") = Nothing
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not Me.IsPostBack Then
                Me.ClearThisPageSessions()
                'Me.ComboSearch.Attributes.Add("onchange", "javascript:CheckNoPopUp('" & Me.TextSearch.ClientID & "');")
                Me.ComboSearch.Attributes.Add("onchange", "javascript:Check(this, 3, '" & Me.TextSearch.ClientID & "', '" & Me.popUp.ClientID & "');")
                Me.popUp.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TextSearch.ClientID & "'), 'dd-mmm-yyyy')")
                Me.popUp.Style.Add("display", "none")

                Me.FillSearch()
                Me.GridProposalSTRView.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)


                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)


                End Using

            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' page navigate button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
            Me.CollectSelected()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' bind grid
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Sub BindGrid()
        Try


            Me.GridProposalSTRView.DataSource = Me.SetnGetBindTable

            'Me.GridProposalSTRView.CurrentPageIndex = Me.SetnGetCurrentPage
            Me.GridProposalSTRView.VirtualItemCount = Me.SetnGetRowTotal
            Me.GridProposalSTRView.DataBind()
        Catch
            Throw
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' set navigate info
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.ComboSearch.SelectedValue = Me.SetnGetFieldSearch
            Me.TextSearch.Text = Me.SetnGetValueSearch
        Catch
            Throw
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' change sort expression
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridProposalSTRView_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridProposalSTRView.SortCommand
        Dim GridUser As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' image button search
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Try
            Me.CollectSelected()
            Me.SetnGetFieldSearch = Me.ComboSearch.SelectedValue
            If Me.ComboSearch.SelectedIndex = 0 Then
                Me.TextSearch.Text = ""
            End If
            Me.SetnGetValueSearch = Me.TextSearch.Text
            Me.SetnGetCurrentPage = 0
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' grid edit command handler
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridProposalSTRView_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridProposalSTRView.EditCommand
        Dim strProposalSTRID As String = e.Item.Cells(1).Text

        Try
            Me.Response.Redirect("ProposalSTRDetail.aspx?PKProposalSTRID=" & strProposalSTRID, False)


        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub






    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' go button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	14/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            Me.CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be a positive number greater than zero.")
                End If
            Else
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    ''' <summary>
    ''' collect sub
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In Me.GridProposalSTRView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = gridRow.FindControl("CheckBoxExporttoExcel")
                Dim groupID As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
                If ArrTarget.Contains(groupID) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(groupID)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(groupID)
                End If
                Me.SetnGetSelectedItem = ArrTarget
            End If
        Next
    End Sub

    ''' <summary>
    ''' prerender event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    ''' <summary>
    ''' clear all control except control
    ''' </summary>
    ''' <param name="control">excluded control</param>
    ''' <remarks></remarks>
    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub 'ClearControls

    ''' <summary>
    ''' bind selected item
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub BindSelected()
        Try
            ''Dim Rows As New ArrayList
            'Dim oTableProposalSTR As New Data.DataTable
            'Dim oRowProposalSTR As Data.DataRow
            'oTableProposalSTR.Columns.Add("PK_ProposalSTRID", GetType(String))
            'oTableProposalSTR.Columns.Add("Kesimpulan", GetType(String))
            'oTableProposalSTR.Columns.Add("CreatedDate", GetType(DateTime))
            'oTableProposalSTR.Columns.Add("UserID", GetType(String))
            'oTableProposalSTR.Columns.Add("ProposalSTRStatusDescription", GetType(String))


            'For Each IdPk As String In Me.SetnGetSelectedItem
            '    'Using adapter As New AMLDAL.ProposalSTRTableAdapters.ProposalSTRResponseTableAdapter
            '    'Dim rowData() As AMLDAL.ProposalSTR.ProposalSTRResponseRow = adapter.GetProposealSTRNeedResponse(IdPk).Select
            '    'If rowData.Length > 0 Then
            '    'Rows.Add(rowData(0))
            '    'End If
            '    'End Using
            '    Dim rowData() As Data.DataRow = Me.SetnGetBindTable.Select("PK_ProposalSTRID=" & IdPk)
            '    If rowData.Length > 0 Then
            '        oRowProposalSTR = oTableProposalSTR.NewRow
            '        oRowProposalSTR("PK_ProposalSTRID") = rowData(0)("PK_ProposalSTRID")
            '        oRowProposalSTR("Kesimpulan") = rowData(0)("Kesimpulan")
            '        oRowProposalSTR("CreatedDate") = rowData(0)("CreatedDate")
            '        oRowProposalSTR("UserID") = rowData(0)("UserID")
            '        oRowProposalSTR("ProposalSTRStatusDescription") = rowData(0)("ProposalSTRStatusDescription")
            '        oTableProposalSTR.Rows.Add(oRowProposalSTR)
            '    End If
            'Next

            'Me.GridProposalSTRView.DataSource = oTableProposalSTR
            'Me.GridProposalSTRView.AllowPaging = False
            'Me.GridProposalSTRView.DataBind()


            'Me.GridProposalSTRView.Columns(0).Visible = False
            'Me.GridProposalSTRView.Columns(6).Visible = False

            Dim listPK As New System.Collections.Generic.List(Of String)
            listPK.Add("'0'")
            For Each IdPk As String In Me.SetnGetSelectedItem
                listPK.Add("'" & IdPk.ToString & "'")
            Next

            Me.GridProposalSTRView.DataSource = Sahassa.AML.Commonly.GetDatasetPaging(Me.Tables, Me.PK, Me.SetnGetSort, 0, Int32.MaxValue, Me.Fields, "PK_ProposalSTRID IN (" & String.Join(",", listPK.ToArray) & ")", "")
            Me.GridProposalSTRView.AllowPaging = False
            Me.GridProposalSTRView.DataBind()


            Me.GridProposalSTRView.Columns(0).Visible = False
            Me.GridProposalSTRView.Columns(6).Visible = False
        Catch
            Throw
        End Try
    End Sub

    ''' <summary>
    ''' export button 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Me.CollectSelected()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelected()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=ProposalSTRView.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridProposalSTRView)
            GridProposalSTRView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get item bound
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridProposalSTRView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridProposalSTRView.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Dim chkBox As CheckBox = e.Item.FindControl("CheckBoxExporttoExcel")
                chkBox.Checked = Me.SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)

            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' select all
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        For Each gridRow As DataGridItem In Me.GridProposalSTRView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(1).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItem.Contains(StrKey) = False Then
                        ArrTarget.Add(StrKey)
                    End If
                Else
                    If Me.SetnGetSelectedItem.Contains(StrKey) Then
                        ArrTarget.Remove(StrKey)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItem = ArrTarget
    End Sub

    Public Enum EnProposalSTREventType
        WorkFlowStated = 1
        TaskStarted
        TaskCompleted
        TaskCanceledBySystem
        WorkFlowFinish
    End Enum

    Private Sub BindExportAll()
        Try
            Me.GridProposalSTRView.DataSource = Me.SetnGetBindTableAll
            Me.GridProposalSTRView.AllowPaging = False
            Me.GridProposalSTRView.DataBind()


            Me.GridProposalSTRView.Columns(0).Visible = False
            Me.GridProposalSTRView.Columns(6).Visible = False
        Catch
            Throw
        End Try
    End Sub

    Protected Sub LnkBtnExportAllToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LnkBtnExportAllToExcel.Click
        Try
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindExportAll()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=ProposalSTRView.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridProposalSTRView)
            GridProposalSTRView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class
