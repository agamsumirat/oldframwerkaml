<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="LoginParameterApprovalDetail.aspx.vb" Inherits="LoginParameterApprovalDetail" title="Login Parameter Approval Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
	<table cellSpacing="4" cellPadding="0" width="100%">
		<tr>
			<td vAlign="bottom" align="left"><IMG height="15" src="images/dot_title.gif" width="15"></td>
			<td class="maintitle" vAlign="bottom" width="99%"><asp:label id="LabelTitle" runat="server"></asp:label></td>
		</tr>
	</table>
    <TABLE id="TableDetail" borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%"
	    bgColor="#dddddd" border="2" runat="server">
	     <TR class="formText" id="UserAdd1">
		    <TD bgColor="#ffffff" height="24" style="width: 22px; height: 32px;">&nbsp;</TD>
		    <TD bgColor="#ffffff" colspan="1" style="width: 174px">
                Password Expiration Period</TD>
             <td bgcolor="#ffffff" colspan="1" style="width: 9px">
                 :</td>
		    <TD bgColor="#ffffff" colspan=5><asp:label id="LabelPasswordExpiredPeriodAdd" runat="server"></asp:label></TD>
	    </TR>
        <tr class="formText" id="UserAdd2">
            <td bgcolor="#ffffff" height="24" style="width: 22px; height: 32px;">
            </td>
            <td bgcolor="#ffffff" colspan="1" style="width: 174px">
                Minimum Password Length</td>
            <td bgcolor="#ffffff" colspan="1" style="width: 9px">
                :</td>
            <td bgcolor="#ffffff"  colspan=5>
                <asp:Label ID="LabelMinimumPasswordLengthAdd" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText" id="UserAdd3">
            <td bgcolor="#ffffff" height="24" style="width: 22px; height: 32px;">
            </td>
            <td bgcolor="#ffffff" colspan="1" style="width: 174px">
                Password Recycle</td>
            <td bgcolor="#ffffff" colspan="1" style="width: 9px">
                :</td>
            <td bgcolor="#ffffff" colspan=5>
                <asp:Label ID="LabelPasswordRecycleCountAdd" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText" id="UserAdd4">
            <td bgcolor="#ffffff" height="24" style="width: 22px; height: 32px;">
            </td>
            <td bgcolor="#ffffff" colspan="1" style="width: 174px">
                Lock Unused account after</td>
            <td bgcolor="#ffffff" colspan="1" style="width: 9px">
                :</td>
            <td bgcolor="#ffffff" colspan=5>
                <asp:Label ID="LabelLockUnusedAdd" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText" id="UserAdd5">
            <td bgcolor="#ffffff" height="24" style="width: 22px; height: 32px;">
            </td>
            <td bgcolor="#ffffff" colspan="1" style="width: 174px">
                Account Lockout</td>
            <td bgcolor="#ffffff" colspan="1" style="width: 9px">
                :</td>
            <td bgcolor="#ffffff" colspan=5>
                <asp:Label ID="LabelAccountLockoutAdd" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText" id="UserAdd6">
            <td bgcolor="#ffffff" height="24" style="width: 22px; height: 32px;">
            </td>
            <td bgcolor="#ffffff" colspan="1" style="width: 174px">
                Password Char</td>
            <td bgcolor="#ffffff" colspan="1" style="width: 9px">
                :</td>
            <td bgcolor="#ffffff" colspan=5>
                <asp:Label ID="LabelPassworCharAdd" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText" id="UserAdd7">
            <td bgcolor="#ffffff" height="24" style="width: 22px; height: 32px;">
            </td>
            <td bgcolor="#ffffff" colspan="1" style="width: 174px">
                Password Combination</td>
            <td bgcolor="#ffffff" colspan="1" style="width: 9px">
                :</td>
            <td bgcolor="#ffffff" colspan=5>
                <asp:Label ID="LabelPassCombinationAdd" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText" id="UserAdd8">
            <td bgcolor="#ffffff" height="24" style="width: 22px; height: 32px;">
            </td>
            <td bgcolor="#ffffff" colspan="1" style="width: 174px">
                Change Password Login</td>
            <td bgcolor="#ffffff" colspan="1" style="width: 9px">
                :</td>
            <td bgcolor="#ffffff" colspan=5>
                <asp:Label ID="LabelChangePassLoginAdd" runat="server"></asp:Label></td>
        </tr>
	    <tr class="formText" id="UserEdi1">
		    <td colspan="4" bgcolor="#ffffcc" style="height: 30px">&nbsp;Old Values</td>
		    <td colspan="4" bgcolor="#ffffcc" style="height: 30px">&nbsp;New Values</td>
	    </tr>
	    <tr class="formText" id="UserEdi2">
		    <td bgcolor="#ffffff" style="width: 22px; height: 32px">&nbsp;</td>
		    <td bgcolor="#ffffff" style="height: 32px; width: 174px;">
                Password Expiration Period</td>
		    <td bgcolor="#ffffff" style="height: 32px; width: 9px;">:</td>
		    <td bgcolor="#ffffff" style="height: 32px;"><asp:label id="LabelPasswordExpiredPeriodOld" runat="server"></asp:label></td>
            <td bgcolor="#ffffff" style="width: 2%; height: 32px">
            </td>
		    <td width="30%" bgcolor="#ffffff" style="height: 32px; width: 20%;">
                Password Expiration Period</td>
		    <td width="5%" bgcolor="#ffffff" style="height: 32px; width: 1%;">:</td>
		    <td bgcolor="#ffffff" style="height: 32px"><asp:label id="LabelPasswordExpiredPeriodNew" runat="server"></asp:label></td>
	    </tr>
	    <tr class="formText" id="UserEdi3">
		    <td bgcolor="#ffffff" style="width: 22px; height: 32px;">&nbsp;</td>
		    <td bgcolor="#ffffff" style="width: 174px">
                Minimum Password Length</td>
		    <td bgcolor="#ffffff" style="width: 9px">:</td>
		    <td bgcolor="#ffffff"><asp:label id="LabelMinimumPasswordLengthOld" runat="server"></asp:label></td>
            <td bgcolor="#ffffff" style="width: 22px">
            </td>
		    <td bgcolor="#ffffff">
                Minimum Password Length</td>
		    <td bgcolor="#ffffff">:</td>
		    <td bgcolor="#ffffff"><asp:label id="LabelMinimumPasswordLengthNew" runat="server"></asp:label></td>
	    </tr>
        <tr class="formText" id="UserEdi4">
            <td bgcolor="#ffffff" style="width: 22px; height: 32px;">
            </td>
            <td bgcolor="#ffffff" style="width: 174px">
                Password Recycle</td>
            <td bgcolor="#ffffff" style="width: 9px">
                :</td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelPasswordRecycleCountOld" runat="server"></asp:Label></td>
            <td bgcolor="#ffffff" style="width: 22px">
            </td>
            <td bgcolor="#ffffff">
                Password Recycle</td>
            <td bgcolor="#ffffff">
                :</td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelPasswordRecycleCountNew" runat="server"></asp:Label></td>
        </tr>
	    <tr class="formText" id="UserEdi5">
		    <td bgcolor="#ffffff" style="width: 22px; height: 32px;">&nbsp;</td>
		    <td bgcolor="#ffffff" style="width: 174px">
                Lock Unused account after</td>
		    <td bgcolor="#ffffff" style="width: 9px">:</td>
		    <td bgcolor="#ffffff">
                <asp:Label ID="LabelLockUnusedOld" runat="server"></asp:Label></td>
            <td bgcolor="#ffffff" style="width: 22px">
            </td>
		    <td bgcolor="#ffffff">
                Lock Unused account after</td>
		    <td bgcolor="#ffffff">:</td>
		    <td bgcolor="#ffffff">
                <asp:Label ID="LabelLockUnusedNew" runat="server"></asp:Label></td>
	    </tr>
	    <tr class="formText" id="UserEdi6">
		    <td bgcolor="#ffffff" style="width: 22px; height: 32px;">&nbsp;</td>
		    <td bgcolor="#ffffff" style="width: 174px">
                Account Lockout</td>
		    <td bgcolor="#ffffff" style="width: 9px">:</td>
		    <td bgcolor="#ffffff">
                <asp:Label ID="LabelAccountLockoutOld" runat="server"></asp:Label></td>
            <td bgcolor="#ffffff" style="width: 22px">
            </td>
		    <td bgcolor="#ffffff">
                Account Lockout</td>
		    <td bgcolor="#ffffff">:</td>
		    <td bgcolor="#ffffff">
                <asp:Label ID="LabelAccountLockoutNew" runat="server"></asp:Label></td>
	    </tr>
        <tr class="formText" id="UserEdi7">
		    <td bgcolor="#ffffff" style="width: 22px; height: 32px;">&nbsp;</td>
		    <td bgcolor="#ffffff" style="width: 174px">
                Password Char</td>
		    <td bgcolor="#ffffff" style="width: 9px">:</td>
		    <td bgcolor="#ffffff">
                <asp:Label ID="LabelPasswordCharOld" runat="server"></asp:Label></td>
            <td bgcolor="#ffffff" style="width: 22px">
            </td>
		    <td bgcolor="#ffffff">
                Password Char</td>
		    <td bgcolor="#ffffff">:</td>
		    <td bgcolor="#ffffff">
                <asp:Label ID="LabelPasswordCharNew" runat="server"></asp:Label></td>
	    </tr>
        <tr class="formText" id="UserEdi8">
		    <td bgcolor="#ffffff" style="width: 22px; height: 32px;">&nbsp;</td>
		    <td bgcolor="#ffffff" style="width: 174px">
                Password Combination</td>
		    <td bgcolor="#ffffff" style="width: 9px">:</td>
		    <td bgcolor="#ffffff">
                <asp:Label ID="LabelPassCombinationOld" runat="server"></asp:Label></td>
            <td bgcolor="#ffffff" style="width: 22px">
            </td>
		    <td bgcolor="#ffffff">
                Password Combination</td>
		    <td bgcolor="#ffffff">:</td>
		    <td bgcolor="#ffffff">
                <asp:Label ID="LabelPassCombinationNew" runat="server"></asp:Label></td>
	    </tr>
        <tr class="formText" id="UserEdi9">
		    <td bgcolor="#ffffff" style="width: 22px; height: 32px;">&nbsp;</td>
		    <td bgcolor="#ffffff" style="width: 174px">
                Change Password Login</td>
		    <td bgcolor="#ffffff" style="width: 9px">:</td>
		    <td bgcolor="#ffffff">
                <asp:Label ID="LabelChangePassOld" runat="server"></asp:Label></td>
            <td bgcolor="#ffffff" style="width: 22px">
            </td>
		    <td bgcolor="#ffffff">
                Change Password Login</td>
		    <td bgcolor="#ffffff">:</td>
		    <td bgcolor="#ffffff">
                <asp:Label ID="LabelChangePassNew" runat="server"></asp:Label></td>
	    </tr>
	    <TR class="formText" id="Button11" bgColor="#dddddd" height="30">
		    <TD style="width: 22px"><IMG height="15" src="images/arrow.gif" width="15"></TD>
		    <TD colSpan="7">
			    <TABLE cellSpacing="0" cellPadding="3" border="0">
				    <TR>
					    <TD style="width: 35px"><asp:imagebutton id="ImageAccept" runat="server" CausesValidation="True" SkinID="AcceptButton"></asp:imagebutton></TD>
					    <TD><asp:imagebutton id="ImageReject" runat="server" CausesValidation="False" SkinID="RejectButton"></asp:imagebutton></TD>
					    <td><asp:imagebutton id="ImageBack" runat="server" CausesValidation="False" SkinID="BackButton"></asp:imagebutton></td>
				    </TR>
			    </TABLE>
                <asp:CustomValidator ID="cvalPageErr" runat="server" Display="None"></asp:CustomValidator></TD>
	    </TR>
    </TABLE>
</asp:Content>