﻿Imports SahassaNettier.Entities
Imports SahassaNettier.Data
Imports AMLBLL

Partial Class VIPCodeEdit
    Inherits Parent


    ReadOnly Property GetPk_VIPCode As Long
        Get
            If IsNumeric(Request.Params("VIPCodeID")) Then
                If Not IsNothing(Session("VIPCodeEdit.PK")) Then
                    Return CLng(Session("VIPCodeEdit.PK"))
                Else
                    Session("VIPCodeEdit.PK") = Request.Params("VIPCodeID")
                    Return CLng(Session("VIPCodeEdit.PK"))
                End If
            End If
            Return 0
        End Get
    End Property

    Sub ClearSession()
        Session("VIPCodeEdit.PK") = Nothing
    End Sub

    Property SetnGetVIPCode As String
        Get
            Return Session("VIPCodeEdit.VIPCode")
        End Get
        Set(ByVal value As String)
            Session("VIPCodeEdit.VIPCode") = value
        End Set
    End Property



    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "VIPCodeView.aspx"

            Me.Response.Redirect("VIPCodeView.aspx", False)
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Function isValidData() As Boolean
        Try
            If txtVIPCode.Text.Trim.Length = 0 Then
                Throw New Exception("VIPCode Code must be filled")
            End If
            If txtVIPCode.Text.ToLower <> SetnGetVIPCode.ToLower Then
                If Not VIPCodeBLL.IsUniqueVIPCode(txtVIPCode.Text) Then
                    Throw New Exception("Sement Code " & txtVIPCode.Text & " existed")
                End If
            End If
            Return True
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
            Return False
        End Try
    End Function

    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Try
            If isValidData() Then
                Using objUser As User = AMLBLL.UserBLL.GetUserByPkUserID(Sahassa.AML.Commonly.SessionPkUserId)
                    If Not IsNothing(objUser) Then
                        Using objVIPCode As VIPCode = VIPCodeBLL.getVIPCodeByPk(GetPk_VIPCode)
                            If Not IsNothing(objVIPCode) Then
                                objVIPCode.VIPCode = txtVIPCode.Text
                                objVIPCode.VIPCodeDescription = txtDescription.Text
                                objVIPCode.Activation = True
                                'objVIPCode.CreatedDate = Now
                                'objVIPCode.CreatedBy = objUser.UserName
                                objVIPCode.LastUpdateDate = Now
                                objVIPCode.LastUpdateBy = objUser.UserName


                                If Sahassa.AML.Commonly.SessionGroupName.ToLower = "superuser" Then
                                    If VIPCodeBLL.SaveEdit(objVIPCode) Then
                                        lblMessage.Text = "Edit Data Success"
                                        mtvVIPCodeAdd.ActiveViewIndex = 1
                                    End If
                                Else
                                    If VIPCodeBLL.SaveEditApproval(objVIPCode) Then
                                        lblMessage.Text = "Edited Data has been inserted to Approval"
                                        mtvVIPCodeAdd.ActiveViewIndex = 1
                                    End If
                                End If
                            End If
                        End Using

                    End If
                End Using
            End If

        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Sub LoadData()
        Using objVIPCode As VIPCode = VIPCodeBLL.getVIPCodeByPk(GetPk_VIPCode)
            If Not IsNothing(objVIPCode) Then
                txtDescription.Text = objVIPCode.VIPCodeDescription.ToString
                txtVIPCode.Text = objVIPCode.VIPCode.ToString
                SetnGetVIPCode = objVIPCode.VIPCode
            Else
                ImageSave.Visible = False
            End If
        End Using
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                mtvVIPCodeAdd.ActiveViewIndex = 0
                ClearSession()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
                LoadData()
            End If
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnOK.Click
        Try
            Response.Redirect("VIPCodeView.aspx")
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

End Class


