Imports SahassaNettier.Entities
Imports sahassa.aml.Commonly
Imports system.data.SqlClient
Imports SahassaNettier.Data
Imports System.Data

Partial Class CreateCaseSuspiciusPerson
    Inherits Parent
    Public ReadOnly Property PKSuspiciusPersonId() As Integer
        Get
            Dim temp As String = Request.Params("PKSuspiciusPersonId")
            If Not Integer.TryParse(temp, 0) Then
                Throw New Exception("PKSuspiciusPersonId is not integer")
            End If
            Return CInt(temp)
        End Get
    End Property
    Public ReadOnly Property ObjSuspiciusPerson() As SuspiciusPerson
        Get
            If Session("CreateCaseSuspiciusPerson.ObjSuspiciusPerson") Is Nothing Then
                Session("CreateCaseSuspiciusPerson.ObjSuspiciusPerson") = AMLBLL.SuspiciusPersonBLL.GetSuspiciusPersonByPk(Me.PKSuspiciusPersonId)
            End If
            Return Session("CreateCaseSuspiciusPerson.ObjSuspiciusPerson")
        End Get
    End Property
    Sub ClearSession()
        Session("CreateCaseSuspiciusPerson.ObjSuspiciusPerson") = Nothing
    End Sub

    Private _RM As String
    Public Property RM() As String
        Get
            Return _RM
        End Get
        Set(ByVal value As String)
            _RM = value
        End Set
    End Property


    Private _SUBSBU As String
    Public Property SUBSBU() As String
        Get
            Return _SUBSBU
        End Get
        Set(ByVal value As String)
            _SUBSBU = value
        End Set
    End Property

    Private _SBU As String
    Public Property SBU() As String
        Get
            Return _SBU
        End Get
        Set(ByVal value As String)
            _SBU = value
        End Set
    End Property
    Private _InsiderCode As String
    Public Property InsiderCode() As String
        Get
            Return _InsiderCode
        End Get
        Set(ByVal value As String)
            _InsiderCode = value
        End Set
    End Property

    Private _VIPCode As String
    Public Property VIPCode() As String
        Get
            Return _VIPCode
        End Get
        Set(ByVal value As String)
            _VIPCode = value
        End Set
    End Property

    Private _Segment As String
    Public Property Segment() As String
        Get
            Return _Segment
        End Get
        Set(ByVal value As String)
            _Segment = value
        End Set
    End Property

    Private Function GetUserIDbyPK(ByVal PKUserId As String) As String
        Try
            Dim StrUserId As String = ""
            If PKUserId <> "" Then
                Using adapter As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                    Using oTable As AMLDAL.AMLDataSet.UserDataTable = adapter.GetDataByUserID(PKUserId)
                        If oTable.Rows.Count > 0 Then
                            StrUserId = CType(oTable.Rows(0), AMLDAL.AMLDataSet.UserRow).UserID
                        End If
                    End Using
                End Using
            End If
            Return StrUserId
        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Function GetPICByAccountOwnerId(ByVal StrAccountOwnerId As String, strVipcode As String, strInsidercode As String, strSegment As String, strSBU As String, strsubsbu As String, strrm As String) As String
        Dim StrUserId As String = ""
        Using AdapterCaseManagementWorkflow As New AMLDAL.SettingWorkFlowCaseManagementTableAdapters.SettingWorkFlowTableAdapter
            Dim CaseManagementWorkflowTable As New AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowDataTable
            CaseManagementWorkflowTable = AdapterCaseManagementWorkflow.GetSettingWorkFlowCaseManagementByAccountOwnerID(StrAccountOwnerId, strVipcode, strInsidercode, strSegment, "Artificial STR", strSBU, strsubsbu, strrm)
            If Not CaseManagementWorkflowTable Is Nothing Then
                If CaseManagementWorkflowTable.Rows.Count > 0 Then
                    Dim CaseManagementWorkflowTableRows() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow
                    CaseManagementWorkflowTableRows = CaseManagementWorkflowTable.Select("SerialNo=1 AND Paralel=1")
                    If Not CaseManagementWorkflowTableRows Is Nothing Then
                        If CaseManagementWorkflowTableRows.Length > 0 Then
                            Dim CaseManagementWorkflowTableRow As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow
                            CaseManagementWorkflowTableRow = CaseManagementWorkflowTableRows(0)
                            If Not CaseManagementWorkflowTableRow.IsApproversNull Then
                                Dim ArrApprovers() As String = Nothing
                                ArrApprovers = CaseManagementWorkflowTableRow.Approvers.Split("|"c)
                                If Not ArrApprovers Is Nothing Then
                                    If ArrApprovers.Length > 0 Then
                                        StrUserId = Me.GetUserIDbyPK(ArrApprovers(0))
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        End Using
        Return StrUserId
    End Function
    Sub LoadDataSuspicius()
        If Not ObjSuspiciusPerson Is Nothing Then

            
            With ObjSuspiciusPerson
                lblcif.Text = .CIFNo
                lblName.Text = .Nama
                lblTglLahir.Text = .TanggalLahir.GetValueOrDefault(GetDefaultDate).ToString("dd-MMM-yyyy")
                lblTempatLahir.Text = .TempatLahir
                lblIdentityNo.Text = .NoIdentitas
                lblAddress.Text = .Alamat
                lblKecamatanKelurahan.Text = .KecamatanKeluarahan
                lblPhoneNumber.Text = .NoTelp
                lblJob.Text = .Pekerjaan
                lblworkplace.Text = .AlamatTempatKerja
                lblnpwp.Text = .NPWP
                lbldescription.Text = .Description
                lblcreatedby.Text = .UseridCreator

            End With
            If lbldescription.Text.Trim.Length > 0 Then
                TextDescription.Text = "Artificial STR-" & lbldescription.Text
            Else
                TextDescription.Text = "Artificial STR"
            End If
        End If
    End Sub
    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            AddHandler PopUpAccountOwner1.SelectUser, AddressOf SelectUser
            If Not Me.IsPostBack Then
                ClearSession()
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                LoadDataSuspicius()

                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub


    Sub IsiInsidercode(strAccountOwner As String)
        CboInsidercode.Items.Clear()
        For Each item As DataRow In AMLBLL.CreateNewCaseBLL.GetListInsiderCodeWorkflowByAccountOwnerId(strAccountOwner).Rows
            CboInsidercode.Items.Add(New ListItem(item(0).ToString & " - " & item(1).ToString, item(0).ToString))
        Next

    End Sub
    Sub IsiSegment(strAccountOwner As String)
        CboSegment.Items.Clear()
        For Each item As DataRow In AMLBLL.CreateNewCaseBLL.GetListSegmentWorkflowByAccountOwnerId(strAccountOwner).Rows
            CboSegment.Items.Add(New ListItem(item(0).ToString & " - " & item(1).ToString, item(0).ToString))
        Next

    End Sub
    Sub IsiVIPCode(strAccountOwner As String)
        CboVIPCode.Items.Clear()
        For Each item As DataRow In AMLBLL.CreateNewCaseBLL.GetListVIPCodeWorkflowByAccountOwnerId(strAccountOwner).Rows
            CboVIPCode.Items.Add(New ListItem(item(0).ToString & " - " & item(1).ToString, item(0).ToString))
        Next

    End Sub

    Sub SelectUser(ByVal IntUserid As String)
        haccountowner.Value = IntUserid

        LblAccountOwner.Text = AMLBLL.SuspiciusPersonBLL.GetAccountOwnerIDbyPk(IntUserid)

        IsiVIPCode(haccountowner.Value)
        IsiInsidercode(haccountowner.Value)
        IsiSegment(haccountowner.Value)
        IsiPIC()
        'Using objUser As SahassaNettier.Entities.User = UserBLL.GetUserByPkUserID(IntUserid)
        '    If Not objUser Is Nothing Then
        '        LblUserID.Text = objUser.UserID
        '        LabelUserName.Text = objUser.UserName
        '        Session("ResetPassword.SaltUser") = objUser.UserPasswordSalt


        '    Else
        '        LblUserID.Text = ""
        '        LabelUserName.Text = ""
        '        Session("ResetPassword.SaltUser") = ""
        '    End If
        'End Using

    End Sub
    Protected Sub IsiPIC()
        Me.VIPCode = CboVIPCode.SelectedValue
        Me.InsiderCode = CboInsidercode.SelectedValue
        Me.Segment = CboSegment.SelectedValue
        Me.SBU = cbosbu.SelectedValue
        Me.SUBSBU = cbosubsbu.SelectedValue
        Me.RM = CboRM.SelectedValue

        Me.LabelPIC.Text = Me.GetPICByAccountOwnerId(Me.haccountowner.Value, Me.VIPCode, Me.InsiderCode, Me.Segment, Me.SBU, Me.SUBSBU, Me.RM)
    End Sub

    Function IsDataValid() As Boolean
        If haccountowner.Value = "" Then
            Throw New Exception("Please Select Account Owner ID")
        End If
        If LabelPIC.Text = "" Then
            Throw New Exception("Please Setting Account Owner for " & LblAccountOwner.Text)
        End If
        Return True
    End Function


    Private Sub SendEmail(ByVal StrRecipientTo As String, ByVal StrRecipientCC As String, ByVal StrSubject As String, ByVal strbody As String)
        Dim oEmail As Sahassa.AML.EMail
        Try
            oEmail = New Sahassa.AML.EMail
            oEmail.Sender = System.Configuration.ConfigurationManager.AppSettings("FromMailAddress")
            oEmail.Recipient = StrRecipientTo
            oEmail.RecipientCC = StrRecipientCC
            oEmail.Subject = StrSubject
            oEmail.Body = strbody.Replace(vbLf, "<br>")
            oEmail.SendEmail()
        Catch ex As Exception
            LogError(ex)
        End Try
    End Sub
    Private Function GetEmailSubject(ByVal PKCMWID As Long, ByVal intSerialNo As Integer) As String
        Try
            Using adapter As New AMLDAL.CaseManagementTableAdapters.CaseManagementWorkflowEmailTemplateTableAdapter

                Using otable As AMLDAL.CaseManagement.CaseManagementWorkflowEmailTemplateDataTable = adapter.GetEmailTemplate(PKCMWID, intSerialNo)
                    If otable.Rows.Count > 0 Then
                        If Not CType(otable.Rows(0), AMLDAL.CaseManagement.CaseManagementWorkflowEmailTemplateRow).IsSubject_EmailTemplateNull Then
                            Return CType(otable.Rows(0), AMLDAL.CaseManagement.CaseManagementWorkflowEmailTemplateRow).Subject_EmailTemplate
                        Else
                            Return ""
                        End If
                    Else
                        Return ""
                    End If

                End Using
            End Using
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Function

    Private Function GetEmailBody(ByVal PKCMWID As Long, ByVal intSerialNo As Integer) As String
        Try
            Using adapter As New AMLDAL.CaseManagementTableAdapters.CaseManagementWorkflowEmailTemplateTableAdapter

                Using otable As AMLDAL.CaseManagement.CaseManagementWorkflowEmailTemplateDataTable = adapter.GetEmailTemplate(PKCMWID, intSerialNo)
                    If otable.Rows.Count > 0 Then
                        If Not CType(otable.Rows(0), AMLDAL.CaseManagement.CaseManagementWorkflowEmailTemplateRow).IsBody_EmailTemplateNull Then
                            Return CType(otable.Rows(0), AMLDAL.CaseManagement.CaseManagementWorkflowEmailTemplateRow).Body_EmailTemplate
                        Else
                            Return ""
                        End If
                    Else
                        Return ""
                    End If

                End Using
            End Using
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Function

    Private Function GetEmailAddress(ByVal PKUserID As String) As String
        Try
            If PKUserID <> "" Then
                Using adapter As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                    Using otable As AMLDAL.AMLDataSet.UserDataTable = adapter.GetDataByUserID(PKUserID)
                        If otable.Rows.Count > 0 Then
                            If CType(otable.Rows(0), AMLDAL.AMLDataSet.UserRow).IsUserEmailAddressNull Then
                                Return ""
                            Else
                                Return CType(otable.Rows(0), AMLDAL.AMLDataSet.UserRow).UserEmailAddress
                            End If
                        Else
                            Return ""
                        End If
                    End Using
                End Using
            End If
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Function

    Protected Sub ImgBrowse_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBrowse.Click

        Try
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "idpopup2", "popUpDrag('" & CType(sender, ImageButton).ClientID & "','divBrowseUser');", True)
            PopUpAccountOwner1.initData()

        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

        Try

            If Not Page.ClientScript.IsClientScriptIncludeRegistered("idpopup1") Then
                Page.ClientScript.RegisterClientScriptInclude(Me.GetType(), "idpopup1", ResolveClientUrl("script/popupdrag.js"))
            End If


        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

 
    
    Protected Sub ImageButtonSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSave.Click
        Dim Trans As SqlTransaction = Nothing
        Dim intAccountOwnerid As Integer
        Try
            If IsDataValid() Then

                Dim StrWorkflowstep As String = ""
                Dim StrPIC As String = ""
                Dim PK_CaseManagementID As String
                Dim DateCase As DateTime
                Dim CaseManagementWorkflowTable As New AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowDataTable
                Dim CaseManagementWorkflowTableRows() As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow = Nothing
                Dim CaseManagementWorkflowTableRow As AMLDAL.SettingWorkFlowCaseManagement.SettingWorkFlowRow = Nothing


                intAccountOwnerid = haccountowner.Value
                Me.VIPCode = CboVIPCode.SelectedValue
                Me.InsiderCode = CboInsidercode.SelectedValue
                Me.Segment = CboSegment.SelectedValue
                Me.SBU = cbosbu.SelectedValue
                Me.SUBSBU = cbosubsbu.SelectedValue
                Me.RM = CboRM.SelectedValue

                Using AccessTable As New AMLDAL.NewCaseTableAdapters.CaseManagementTableAdapter
                    Trans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessTable)
                    Using AdapterCaseManagementWorkflow As New AMLDAL.SettingWorkFlowCaseManagementTableAdapters.SettingWorkFlowTableAdapter

                        CaseManagementWorkflowTable = AdapterCaseManagementWorkflow.GetSettingWorkFlowCaseManagementByAccountOwnerID(intAccountOwnerid, Me.VIPCode, Me.InsiderCode, Me.Segment, TextDescription.Text, Me.SBU, Me.SUBSBU, Me.RM)
                        If Not CaseManagementWorkflowTable Is Nothing Then
                            If CaseManagementWorkflowTable.Rows.Count > 0 Then

                                'CaseManagementWorkflowTableRows = CaseManagementWorkflowTable.Select("SerialNo=1 AND Paralel=1")
                                CaseManagementWorkflowTableRows = CaseManagementWorkflowTable.Select("SerialNo=1")
                                If Not CaseManagementWorkflowTableRows Is Nothing Then
                                    If CaseManagementWorkflowTableRows.Length > 0 Then

                                        CaseManagementWorkflowTableRow = CaseManagementWorkflowTableRows(0)
                                        If Not CaseManagementWorkflowTableRow.IsApproversNull Then
                                            Dim ArrApprovers() As String = Nothing
                                            ArrApprovers = CaseManagementWorkflowTableRow.Approvers.Split("|"c)
                                            If Not ArrApprovers Is Nothing Then
                                                If ArrApprovers.Length > 0 Then
                                                    StrPIC = Me.GetUserIDbyPK(ArrApprovers(0))
                                                End If
                                            End If
                                        End If
                                        If Not CaseManagementWorkflowTableRow.IsWorkflowStepNull Then
                                            StrWorkflowstep = CaseManagementWorkflowTableRow.WorkflowStep
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End Using
                    DateCase = DateTime.Now()
                    PK_CaseManagementID = AccessTable.InsertCaseManagement(Me.TextDescription.Text, 1, StrWorkflowstep, DateCase, DateCase, intAccountOwnerid, StrPIC, False, 0, False, "", Nothing, Sahassa.AML.Commonly.SessionUserId, "", Nothing, "", ObjSuspiciusPerson.Nama, ObjSuspiciusPerson.CIFNo, "", Me.VIPCode, Me.InsiderCode, Me.Segment)
                    If PK_CaseManagementID > 0 Then
                        ' Insert ke MapCaseManagementTransaction
                        Using objnewCasemanagmentsuspiciusperson As New CaseManagementSuspiciusPerson
                            With objnewCasemanagmentsuspiciusperson
                                .FK_CaseManagement_ID = PK_CaseManagementID
                                .FK_SuspiciusPerson_ID = Me.PKSuspiciusPersonId
                            End With
                            DataRepository.CaseManagementSuspiciusPersonProvider.Save(objnewCasemanagmentsuspiciusperson)
                        End Using

                        'Perbaikan case double button 29 Oktober 2014
                        Using cmdQUESTION As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("usp_GenerateQuestionCaseManagement")
                            cmdQUESTION.Parameters.Add(New SqlParameter("@PKCaseManagementID", PK_CaseManagementID))
                            cmdQUESTION.Parameters.Add(New SqlParameter("@DescriptioinCaseAlert", TextDescription.Text))
                            SahassaNettier.Data.DataRepository.Provider.ExecuteNonQuery(cmdQUESTION)
                        End Using

                        ' Insert ke MapCaseManagementWorkflow
                        Using MapCaseManagementAdapter As New AMLDAL.CaseManagementTableAdapters.MapCaseManagementWorkflowTableAdapter
                            MapCaseManagementAdapter.Insert(PK_CaseManagementID, "SYSTEM", DateCase, DateCase)
                        End Using
                        ' Insert ke MapCaseManagementWorkflowHistory
                        Using MapCaseManagementHistoryAdapter As New AMLDAL.CaseManagementTableAdapters.SelectMapCaseManagementWorkflowHistoryTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(MapCaseManagementHistoryAdapter, Trans)
                            MapCaseManagementHistoryAdapter.InsertMapCaseManagementWorkflowHistory(PK_CaseManagementID, DateCase, 1, StrWorkflowstep, "", "", Nothing, True, "", intAccountOwnerid, Nothing)
                            'MapCaseManagementHistoryAdapter.InsertMapCaseManagementWorkflowHistory(PK_CaseManagementID, DateCase, 2, StrWorkflowstep, StrPIC, "", Nothing, False, "", Me.AccountOwnerId, Nothing)
                            ' Insert to Workflow
                            Dim Counter As Integer
                            Dim ListOfUser As String
                            Dim ApproverCounter As Integer
                            Dim StrEmailRecepients As String
                            Dim StrEmailCC As String
                            Dim StrEmailSubject As String
                            Dim StrEmailBody As String
                            If Not CaseManagementWorkflowTableRows Is Nothing Then
                                If CaseManagementWorkflowTableRows.Length > 0 Then
                                    For Counter = 0 To CaseManagementWorkflowTableRows.Length - 1
                                        CaseManagementWorkflowTableRow = CaseManagementWorkflowTableRows(Counter)
                                        ListOfUser = StrPIC
                                        StrEmailRecepients = ""
                                        StrEmailCC = ""
                                        If Not CaseManagementWorkflowTableRow.IsApproversNull Then
                                            Dim ArrApprovers() As String = Nothing
                                            ArrApprovers = CaseManagementWorkflowTableRow.Approvers.Split("|"c)
                                            If Not ArrApprovers Is Nothing Then
                                                If ArrApprovers.Length > 0 Then
                                                    ListOfUser = ""

                                                    For ApproverCounter = 0 To ArrApprovers.Length - 1
                                                        ListOfUser = ListOfUser & Me.GetUserIDbyPK(ArrApprovers(ApproverCounter)) & ";"
                                                        StrEmailRecepients += GetEmailAddress(ArrApprovers(ApproverCounter)) & ";"
                                                    Next
                                                    If ListOfUser.Length > 0 Then
                                                        ListOfUser = ListOfUser.Remove(ListOfUser.Length - 1)
                                                    End If
                                                    If StrEmailRecepients.Length > 0 Then
                                                        StrEmailRecepients = StrEmailRecepients.Remove(StrEmailRecepients.Length - 1)
                                                    End If
                                                End If
                                            End If
                                        End If
                                        If Not CaseManagementWorkflowTableRow.IsNotifyOthersNull Then
                                            Dim ArrOtherApprovers() As String = Nothing
                                            ArrOtherApprovers = CaseManagementWorkflowTableRow.NotifyOthers.Split("|"c)
                                            If Not ArrOtherApprovers Is Nothing Then
                                                If ArrOtherApprovers.Length > 0 Then

                                                    For ApproverCounter = 0 To ArrOtherApprovers.Length - 1
                                                        StrEmailCC += GetEmailAddress(ArrOtherApprovers(ApproverCounter)) & ";"
                                                    Next
                                                    If StrEmailCC.Length > 0 Then
                                                        StrEmailCC = StrEmailCC.Remove(StrEmailCC.Length - 1)
                                                    End If
                                                End If
                                            End If
                                        End If

                                        MapCaseManagementHistoryAdapter.InsertMapCaseManagementWorkflowHistory(PK_CaseManagementID, DateCase, 2, StrWorkflowstep, ListOfUser, "", Nothing, False, "", intAccountOwnerid, Nothing)
                                        If StrEmailRecepients <> "" Then
                                            StrEmailSubject = GetEmailSubject(CaseManagementWorkflowTableRow.Pk_CMW_Id, CaseManagementWorkflowTableRow.SerialNo)
                                            StrEmailBody = GetEmailBody(CaseManagementWorkflowTableRow.Pk_CMW_Id, CaseManagementWorkflowTableRow.SerialNo)
                                            SendEmail(StrEmailRecepients, StrEmailCC, StrEmailSubject, StrEmailBody)
                                        End If

                                    Next
                                End If
                            End If
                        End Using
                    End If
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, Trans)
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "CreateNewCaseSuspiciusPerson", "CaseDescription", "Add", "", Me.TextDescription.Text, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "CreateNewCaseSuspiciusPerson", "Workflowstep", "Add", "", StrWorkflowstep, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "CreateNewCaseSuspiciusPerson", "CreatedDate", "Add", "", Now.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "CreateNewCaseSuspiciusPerson", "LastUpdated", "Add", "", Now.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "CreateNewCaseSuspiciusPerson", "Fk_AccountOwnerId", "Add", "", intAccountOwnerid, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "CreateNewCaseSuspiciusPerson", "PIC", "Add", "", StrPIC, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "CreateNewCaseSuspiciusPerson", "ProposedBy", "Add", "", Sahassa.AML.Commonly.SessionUserId, "Accepted")
                    End Using

                    Trans.Commit()
                End Using
                Sahassa.AML.Commonly.SessionIntendedPage = "SuspiciusPersonView.aspx"
                Response.Redirect("SuspiciusPersonView.aspx", False)
            End If
        Catch tex As Threading.ThreadAbortException
            ' ignore
        Catch ex As Exception
            If Not Trans Is Nothing Then
                Trans.Rollback()
            End If
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not Trans Is Nothing Then
                Trans.Dispose()
            End If
        End Try

    End Sub

    Protected Sub CboVIPCode_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles CboVIPCode.SelectedIndexChanged
        Try
            IsiPIC()
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub CboInsidercode_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles CboInsidercode.SelectedIndexChanged
        Try
            IsiPIC()
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub CboSegment_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles CboSegment.SelectedIndexChanged
        Try
            IsiPIC()
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
End Class
