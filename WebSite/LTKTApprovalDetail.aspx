<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
	CodeFile="LTKTApprovalDetail.aspx.vb" Inherits="LTKTApprovalDetail" 
	 %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">

	<script src="Script/popcalendar.js"></script>

	<script language="javascript" type="text/javascript">
	  function hidePanel(objhide,objpanel,imgmin,imgmax)
	  {
		document.getElementById(objhide).style.display='none';
		document.getElementById(objpanel).src=imgmax;
	  }
	  // JScript File

	  function popWin2(txtKelurahan, hiddenField)
	  {
		var paramId;
		var paramName;
		var myargs = new Array(paramId,paramName);
		var height = 480;
		var width = 640;
		var left = (screen.availWidth - width)/2;
		var top = (screen.availHeight - height)/2;
		var winSetting = "width=" + width + ",height=" + height + ",left=" + left + ",top=" + top + ",scrollbars=yes,resizable=yes,location=no,menubar=no,toolbar=no";

		var myargs = window.showModalDialog("PickerKelurahan.aspx", myargs, winSetting);
		if(myargs == null) 
		{
			//window.alert("Nothing returns from Picker Kelurahan");
		}else 
		{
			paramId = myargs[0].toString();
			paramName = myargs[1].toString();

			txtKelurahan.value = paramName;
			hiddenField.value = paramId;
		}
	  }
	  
	  function popWinKecamatan(txtKecamatan, hiddenField)
	  {
		var paramId;
		var paramName;
		var myargs = new Array(paramId,paramName);
		var height = 480;
		var width = 640;
		var left = (screen.availWidth - width)/2;
		var top = (screen.availHeight - height)/2;
		var winSetting = "width=" + width + ",height=" + height + ",left=" + left + ",top=" + top + ",scrollbars=yes,resizable=yes,location=no,menubar=no,toolbar=no";

		var myargs = window.showModalDialog("PickerKecamatan.aspx", myargs, winSetting);
		if(myargs == null) 
		{
			//window.alert("Nothing returns from Picker Kelurahan");
		}else 
		{
			paramId = myargs[0].toString();
			paramName = myargs[1].toString();

			txtKecamatan.value = paramName;
			hiddenField.value = paramId;
		}
	  }
	  function popWinNegara(txtNegara, hiddenField)
	  {
		var paramId;
		var paramName;
		var myargs = new Array(paramId,paramName);
		var height = 480;
		var width = 640;
		var left = (screen.availWidth - width)/2;
		var top = (screen.availHeight - height)/2;
		var winSetting = "width=" + width + ",height=" + height + ",left=" + left + ",top=" + top + ",scrollbars=yes,resizable=yes,location=no,menubar=no,toolbar=no";

		var myargs = window.showModalDialog("PickerNegara.aspx", myargs, winSetting);
		if(myargs == null) 
		{
			//window.alert("Nothing returns from Picker Kelurahan");
		}else 
		{
			paramId = myargs[0].toString();
			paramName = myargs[1].toString();

			txtNegara.value = paramName;
			hiddenField.value = paramId;
		}
	  }
	  function popWinMataUang(txtMataUang, hiddenField)
	  {
		var paramId;
		var paramName;
		var myargs = new Array(paramId,paramName);
		var height = 480;
		var width = 640;
		var left = (screen.availWidth - width)/2;
		var top = (screen.availHeight - height)/2;
		var winSetting = "width=" + width + ",height=" + height + ",left=" + left + ",top=" + top + ",scrollbars=yes,resizable=yes,location=no,menubar=no,toolbar=no";

		var myargs = window.showModalDialog("PickerMataUang.aspx", myargs, winSetting);
		if(myargs == null) 
		{
			//window.alert("Nothing returns from Picker Kelurahan");
		}else 
		{
			paramId = myargs[0].toString();
			paramName = myargs[1].toString();

			txtMataUang.value = paramId + ' - ' +paramName;
			hiddenField.value = paramId;
		}
	  }
	  function popWinProvinsi(txtProvinsi, hiddenField)
	  {
		var paramId;
		var paramName;
		var myargs = new Array(paramId,paramName);
		var height = 480;
		var width = 640;
		var left = (screen.availWidth - width)/2;
		var top = (screen.availHeight - height)/2;
		var winSetting = "width=" + width + ",height=" + height + ",left=" + left + ",top=" + top + ",scrollbars=yes,resizable=yes,location=no,menubar=no,toolbar=no";

		var myargs = window.showModalDialog("PickerProvinsi.aspx", myargs, winSetting);
		if(myargs == null) 
		{
			//window.alert("Nothing returns from Picker Kelurahan");
		}else 
		{
		    paramId = myargs[0];
			paramName = myargs[1].toString();

			txtProvinsi.value = paramName;
			hiddenField.value = paramId;
		}
	  }
	  function popWinPekerjaan(txtPekerjaan, hiddenField)
	  {
		var paramId;
		var paramName;
		var myargs = new Array(paramId,paramName);
		var height = 480;
		var width = 640;
		var left = (screen.availWidth - width)/2;
		var top = (screen.availHeight - height)/2;
		var winSetting = "width=" + width + ",height=" + height + ",left=" + left + ",top=" + top + ",scrollbars=yes,resizable=yes,location=no,menubar=no,toolbar=no";

		var myargs = window.showModalDialog("PickerPekerjaan.aspx", myargs, winSetting);
		if(myargs == null) 
		{
			//window.alert("Nothing returns from Picker Kelurahan");
		}else 
		{
			paramId = myargs[0].toString();
			paramName = myargs[1].toString();

			txtPekerjaan.value = paramName;
			hiddenField.value = paramId;
		}
	  }
	  function popWinKotaKab(txtKotaKab, hiddenField)
	  {
		var paramId;
		var paramName;
		var myargs = new Array(paramId,paramName);
		var height = 480;
		var width = 640;
		var left = (screen.availWidth - width)/2;
		var top = (screen.availHeight - height)/2;
		var winSetting = "width=" + width + ",height=" + height + ",left=" + left + ",top=" + top + ",scrollbars=yes,resizable=yes,location=no,menubar=no,toolbar=no";

		var myargs = window.showModalDialog("PickerKotaKab.aspx", myargs, winSetting);
		if(myargs == null) 
		{
			//window.alert("Nothing returns from Picker Kelurahan");
		}else 
		{
			paramId = myargs[0].toString();
			paramName = myargs[1].toString();

			txtKotaKab.value = paramName;
			hiddenField.value = paramId;
		}
	  }
	  function popWinBidangUsaha(txtBidangUsaha, hiddenField)
	  {
		var paramId;
		var paramName;
		var myargs = new Array(paramId,paramName);
		var height = 480;
		var width = 640;
		var left = (screen.availWidth - width)/2;
		var top = (screen.availHeight - height)/2;
		var winSetting = "width=" + width + ",height=" + height + ",left=" + left + ",top=" + top + ",scrollbars=yes,resizable=yes,location=no,menubar=no,toolbar=no";

		var myargs = window.showModalDialog("PickerBidangUsaha.aspx", myargs, winSetting);
		if(myargs == null) 
		{
			//window.alert("Nothing returns from Picker Kelurahan");
		}else 
		{
			paramId = myargs[0].toString();
			paramName = myargs[1].toString();

			txtBidangUsaha.value = paramName;
			hiddenField.value = paramId;
		}
	  }
	</script>

	<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td>
				<img src="Images/blank.gif" width="5" height="1" /></td>
			<td>
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr>
						<td>
							</td>
						<td width="99%" bgcolor="#FFFFFF">
							<img src="Images/blank.gif" width="1" height="1" /></td>
						<td bgcolor="#FFFFFF">
							<img src="Images/blank.gif" width="1" height="1" /></td>
					</tr>
				</table>
			</td>
			<td>
				<img src="Images/blank.gif" width="5" height="1" /></td>
		</tr>
		<tr>
			<td>
			</td>
			<td id="tdcontent" height="99%" valign="top" bgcolor="#FFFFFF">
				<div id="divcontent" >
					<table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
						<tr>
							<td bgcolor="#ffffff" class="divcontentinside" colspan="2">
								<img src="Images/blank.gif" width="20" height="100%" /><ajax:AjaxPanel ID="a" runat="server"><asp:ValidationSummary
									ID="ValidationSummary1" runat="server" HeaderText="There were uncompleteness on the page:"
									Width="95%" CssClass="validation" ShowMessageBox="True"></asp:ValidationSummary>
								</ajax:AjaxPanel>
								<table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" border="0"
									style="border-top-style: none; border-right-style: none; border-left-style: none;
									border-bottom-style: none">
									<tr>
										<td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
											border-right-style: none; border-left-style: none; border-bottom-style: none">
											<img src="Images/dot_title.gif" width="17" height="17">
											<strong>
												<asp:Label ID="Label1" runat="server" Text="LTKT - Approval Detail"></asp:Label></strong>
											<hr />
										</td>
									</tr>
								</table>
								<ajax:AjaxPanel ID="AjaxMultiView" runat="server">
									<asp:MultiView ID="mtvPage" runat="server" ActiveViewIndex="0">
										<asp:View ID="vwTransaksi" runat="server">
											<table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" align="center"
												bgcolor="#dddddd" border="2">
												<tbody>
													<tr>
														<td style="font-size: 18px; border-top-style: none; border-right-style: none; border-left-style: none;
															border-bottom-style: none" bgcolor="#ffffff" colspan="4">
															<ajax:AjaxPanel ID="AjaxPanel1" runat="server">
																&nbsp;<asp:Label ID="LblInfo" runat="server" Text="Are you sure want to approve this data?"
																	CssClass="validationok" Width="100%" Font-Bold="True" Font-Size="12px"></asp:Label></ajax:AjaxPanel>&nbsp;
														</td>
													</tr>
													<tr>
														<td style="height: 6px" valign="middle" align="left" width="100%" background="Images/search-bar-background.gif">
															<table cellspacing="0" cellpadding="0" border="0">
																<tbody>
																	<tr>
																		<td class="formtext">
																			<asp:Label ID="Label7" runat="server" Text="Approval Information" Font-Bold="True"></asp:Label>&nbsp;</td>
																		<td>
																			<a title="click to minimize or maximize" onclick="javascript:ShowHidePanel('DetailInfo','ImgDetailInfo','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
																				href="#">
																				<img id="ImgDetailInfo" height="12" src="Images/search-bar-minimize.gif" width="12"
																					border="0" /></a></td>
																	</tr>
																</tbody>
															</table>
														</td>
													</tr>
													<tr id="DetailInfo">
														<td valign="top" width="98%" bgcolor="#ffffff">
															<table cellspacing="4" cellpadding="0" width="100%" border="0">
																<tbody>
																	<tr>
																		<td valign="middle" nowrap width="100%">
																			<table style="width: 100%; height: 100%">
																				<tbody>
																					<tr>
																						<td style="vertical-align: top; width: 20%; height: 15px; background-color: #fff7e6">
																							<asp:Label ID="Label11" runat="server" Text="Requested by: "></asp:Label></td>
																						<td style="vertical-align: top; width: 25%; height: 15px">
																							<asp:Label ID="LblRequestedByApprovalDetail" runat="server"></asp:Label></td>
																						<td style="vertical-align: top; width: 2%; height: 15px">
																							&nbsp;</td>
																						<td style="vertical-align: top; width: 20%; height: 15px; background-color: white">
																							<asp:Label ID="Label9" runat="server" Text="Requested Date: "></asp:Label>
																						</td>
																						<td style="vertical-align: top; width: 25%; height: 15px">
																							<asp:Label ID="LblRequestedDateApprovalDetail" runat="server"></asp:Label></td>
																						<td style="vertical-align: top; width: 2%; height: 15px">
																							&nbsp;</td>
																					</tr>
																					<tr>
																						<td style="vertical-align: top; width: 20%; height: 15px; background-color: #fff7e6">
																							<asp:Label ID="Label10" runat="server" Text="Action: "></asp:Label></td>
																						<td style="vertical-align: top; width: 25%; height: 15px">
																							<asp:Label ID="LblActionApprovalDetail" runat="server"></asp:Label></td>
																						<td style="vertical-align: top; width: 2%; height: 15px">
																							&nbsp;</td>
																						<td style="vertical-align: top; width: 20%; height: 15px; background-color: white">
																							&nbsp;
																						</td>
																						<td style="vertical-align: top; width: 25%; height: 15px">
																							&nbsp;</td>
																						<td style="vertical-align: top; width: 2%; height: 15px">
																							&nbsp;</td>
																					</tr>
																					<tr>
																						<td style="vertical-align: top; width: 20%; height: 5px; background-color: white">
																						</td>
																						<td style="vertical-align: top; width: 25%; height: 5px">
																						</td>
																						<td style="vertical-align: top; width: 2%; height: 5px">
																						</td>
																						<td style="vertical-align: top; width: 20%; height: 5px; background-color: white">
																						</td>
																						<td style="vertical-align: top; width: 25%; height: 5px">
																						</td>
																						<td style="vertical-align: top; width: 2%; height: 5px">
																						</td>
																					</tr>
																				</tbody>
																			</table>
																		</td>
																	</tr>
																</tbody>
															</table>
														</td>
													</tr>
												</tbody>
											</table>
											<table cellspacing="0" cellpadding="0" border="0">
												<tbody>
													<tr>
														<td>
															<asp:Label ID="LblNewTitle" runat="server" Text="NEW" Font-Bold="True" Font-Size="Small"
																ForeColor="Blue"></asp:Label>
														</td>
														<td>
															<asp:Label ID="LblOldTitle" runat="server" Text="OLD" Font-Bold="True" Font-Size="Small"
																ForeColor="Blue"></asp:Label>
														</td>
													</tr>
													<tr valign="top">
														<td>
															<asp:Panel ID="NewPanel" runat="server">
																<table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
																	border="0">
																	<tbody>
																		<tr>
																			<td style="height: 6px" valign="middle" align="left" width="100%" background="Images/search-bar-background.gif">
																				<table cellspacing="0" cellpadding="0" border="0">
																					<tbody>
																						<tr>
																							<td class="formtext">
																								<asp:Label ID="Label8" runat="server" Text="A. UMUM" Font-Bold="True"></asp:Label>&nbsp;</td>
																							<td>
																								<a title="click to minimize or maximize" onclick="javascript:ShowHidePanel('Umum','searchimage2','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
																									href="#">
																									<img id="searchimage2" height="12" src="Images/search-bar-minimize.gif" width="12"
																										border="0" /></a></td>
																						</tr>
																					</tbody>
																				</table>
																			</td>
																		</tr>
																		<tr id="Umum">
																			<td style="height: 10px" bgcolor="#ffffff" colspan="2">
																				<table>
																					<tbody>
																						<tr>
																							<td class="formtext">
																								<asp:Label ID="Label4" runat="server" Text="1. PIHAK PELAPOR" Font-Bold="True"></asp:Label>&nbsp;
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								<table>
																									<tbody>
																										<tr>
																											<td style="width: 5px">
																											</td>
																											<td class="formText">
																												1.1. Nama PJK Pelapor <span style="color: #cc0000">*</span>
																											</td>
																											<td style="width: 5px">
																												:
																											</td>
																											<td class="formtext">
																												<asp:Label ID="txtUmumPJKPelapor" runat="server" Width="240px"></asp:Label>
																												&nbsp;
																											</td>
																										</tr>
																										<tr>
																											<td style="width: 5px">
																											</td>
																											<td class="formText">
																												1.2. Tanggal Pelaporan <span style="color: #cc0000">*</span>
																											</td>
																											<td style="width: 5px">
																												:
																											</td>
																											<td class="formtext">
																												<asp:Label ID="txtTglLaporan" runat="server" Width="96px" MaxLength="50"></asp:Label>
																											</td>
																										</tr>
																										<tr>
																											<td style="width: 5px">
																											</td>
																											<td class="formText">
																												1.3. Nama Pejabat PJK Pelapor <span style="color: #cc0000">*</span>
																											</td>
																											<td style="width: 5px">
																												:
																											</td>
																											<td class="formtext">
																												<asp:Label ID="txtPejabatPelapor" runat="server" Width="240px"></asp:Label>
																											</td>
																										</tr>
																									</tbody>
																								</table>
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								<asp:Label ID="Label5" runat="server" Text="2. JENIS LAPORAN (PILIH SALAH SATU)"
																									Font-Bold="True"></asp:Label>&nbsp;
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								<ajax:AjaxPanel ID="pnlUpdate" runat="server">
																									<table>
																										<tbody>
																											<tr>
																												<td style="width: 5px">
																												</td>
																												<td class="formText">
																													2.1. Tipe Laporan <span style="color: #cc0000">*</span>
																												</td>
																												<td style="width: 5px">
																													:
																												</td>
																												<td class="formtext">
																													<asp:DropDownList ID="cboTipeLaporan" runat="server" CssClass="combobox" Enabled="False"
																														AutoPostBack="True">
																														<asp:ListItem Value="0">Laporan Baru</asp:ListItem>
																														<asp:ListItem Value="1">Laporan Koreksi</asp:ListItem>
																													</asp:DropDownList>
																												</td>
																											</tr>
																											<tr>
																												<td style="width: 5px">
																												</td>
																												<td class="formtext">
																												</td>
																												<td style="width: 5px">
																													&nbsp;</td>
																												<td class="formtext">
																													<table id="tblLTKTKoreksi" runat="server" visible="False">
																														<tbody>
																															<tr>
																																<td class="formText">
																																	2.1.1. No. LTKT yang dikoreksi <span style="color: #cc0000">*</span>
																																</td>
																																<td style="width: 5px">
																																	:
																																</td>
																																<td class="formtext">
																																	<asp:Label ID="txtNoLTKTKoreksi" runat="server"></asp:Label>
																																	&nbsp;
																																</td>
																															</tr>
																														</tbody>
																													</table>
																												</td>
																											</tr>
																										</tbody>
																									</table>
																								</ajax:AjaxPanel>
																							</td>
																						</tr>
																					</tbody>
																				</table>
																				<table border="0">
																					<tbody>
																						<tr valign="top">
                                                                        <td class="formtext" style="height: 45px">
                                                                            Debet/Credit</td>
                                                                        <td style="width: 2px; height: 45px">
                                                                            :</td>
                                                                        <td class="formtext" style="height: 45px">
                                                                            <ajax:AjaxPanel ID="AjaxPanel7" runat="server">
                                                                                <asp:DropDownList runat="server" ID="cbodebetcredit" CssClass="combobox" Enabled="False">
                                                                                    <asp:ListItem Value="D">Debet</asp:ListItem>
                                                                                    <asp:ListItem Value="C">Credit</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </ajax:AjaxPanel>
                                                                        </td>
                                                                    </tr>
																					</tbody>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td style="height: 6px" valign="middle" align="left" width="100%" background="Images/search-bar-background.gif">
																				<table cellspacing="0" cellpadding="0" border="0">
																					<tbody>
																						<tr>
																							<td class="formtext">
																								<asp:Label ID="Label2" runat="server" Text="B. IDENTITAS TERLAPOR" Font-Bold="True"></asp:Label>&nbsp;</td>
																							<td>
																								<a title="click to minimize or maximize" onclick="javascript:ShowHidePanel('IdTerlapor','Img1','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
																									href="#">
																									<img id="Img1" height="12" src="Images/search-bar-minimize.gif" width="12" border="0" /></a></td>
																						</tr>
																					</tbody>
																				</table>
																			</td>
																		</tr>
																		<tr id="IdTerlapor">
																			<td bgcolor="#ffffff" colspan="2">
																				<ajax:AjaxPanel ID="ajxPnlTerlapor" runat="server" Width="100%">
																					<table width="100%">
																						<tbody>
																							<tr>
																								<td class="formtext">
																									<asp:Label ID="Label6" runat="server" Text="3. TERLAPOR" Font-Bold="True"></asp:Label>&nbsp;
																								</td>
																							</tr>
																							<tr>
																								<td class="formtext">
																									<table>
																										<tbody>
																											<tr>
																												<td style="width: 5px; height: 25px">
																												</td>
																												<td style="height: 25px" class="formtext">
																													<strong>3.1. Kepemilikan</strong> <span style="color: #cc0000">*</span>
																												</td>
																												<td style="width: 5px; height: 25px">
																													:
																												</td>
																												<td style="height: 25px" class="formtext">
																													<asp:DropDownList ID="cboTerlaporKepemilikan" runat="server" CssClass="combobox"
																														Enabled="False">
																													</asp:DropDownList></td>
																											</tr>
																											<tr>
																												<td style="width: 5px">
																												</td>
																												<td class="formtext">
																												</td>
																												<td style="width: 5px">
																												</td>
																												<td class="formtext">
																													<table width="100%">
																														<tbody>
																															<tr>
																																<td style="width: 139px" class="formText">
																																	<strong>3.1.1. No. Rekening</strong> <span style="color: #cc0000">*</span>&nbsp;</td>
																																<td style="width: 5px">
																																	:
																																</td>
																																<td class="formtext">
																																	<asp:Label ID="txtTerlaporNoRekening" runat="server" Width="258px"></asp:Label>
																																</td>
																															</tr>
																															<tr>
																																<td class="formText">
																																	CIF No.</td>
																																<td class="formText">
																																	:</td>
																																<td class="formText">
																																	<asp:Label ID="txtCIFNo" runat="server"></asp:Label></td>
																															</tr>
																															<tr>
																																<td style="width: 139px" class="formText">
																																	Tipe Pelapor</td>
																																<td style="width: 5px">
																																	:</td>
																																<td class="formtext">
																																	<asp:RadioButtonList ID="rblTerlaporTipePelapor" runat="server" Enabled="False" AutoPostBack="True"
																																		RepeatDirection="Horizontal">
																																		<asp:ListItem Value="1">Perorangan</asp:ListItem>
																																		<asp:ListItem Value="2">Korporasi</asp:ListItem>
																																	</asp:RadioButtonList>
																																</td>
																															</tr>
																															<tr>
																																<td class="formtext" colspan="3">
																																	<div id="divPerorangan" runat="server" hidden="false">
																																		<table>
																																			<tbody>
																																				<tr>
																																					<td class="formtext" colspan="3">
																																						<strong>3.1.2. Perorangan</strong>
																																					</td>
																																				</tr>
																																				<tr>
																																					<td class="formText">
																																						a. Gelar</td>
																																					<td style="width: 5px">
																																						:</td>
																																					<td class="formtext">
																																						<asp:Label ID="txtTerlaporGelar" runat="server"></asp:Label></td>
																																				</tr>
																																				<tr>
																																					<td class="formText">
																																						b. Nama Lengkap <span style="color: #cc0000">*</span></td>
																																					<td style="width: 5px">
																																						:</td>
																																					<td class="formtext">
																																						<asp:Label ID="txtTerlaporNamaLengkap" runat="server" Width="370px"></asp:Label></td>
																																				</tr>
																																				<tr>
																																					<td class="formText">
																																						c. Tempat Lahir</td>
																																					<td style="width: 5px">
																																						:</td>
																																					<td class="formtext">
																																						<asp:Label ID="txtTerlaporTempatLahir" runat="server" Width="240px"></asp:Label></td>
																																				</tr>
																																				<tr>
																																					<td class="formText">
																																						d. Tanggal Lahir <span style="color: #cc0000">*</span></td>
																																					<td style="width: 5px">
																																						:</td>
																																					<td class="formtext">
																																						<br />
																																						<asp:Label ID="txtTerlaporTglLahir" runat="server" Width="96px" MaxLength="50"></asp:Label></td>
																																				</tr>
																																				<tr>
																																					<td class="formText">
																																						e. Kewarganegaraan (Pilih salah satu) <span style="color: #cc0000">*</span></td>
																																					<td style="width: 5px">
																																						:</td>
																																					<td class="formtext">
																																						<asp:RadioButtonList ID="rblTerlaporKewarganegaraan" runat="server" Enabled="False"
																																							AutoPostBack="True" RepeatDirection="Horizontal">
																																							<asp:ListItem Value="1">WNI</asp:ListItem>
																																							<asp:ListItem Value="2">WNA</asp:ListItem>
																																						</asp:RadioButtonList></td>
																																				</tr>
																																				<tr>
																																					<td class="formText">
																																						f. Negara <span style="color: #cc0000">*</span></td>
																																					<td style="width: 5px">
																																						:</td>
																																					<td class="formtext">
																																						<asp:DropDownList ID="cboTerlaporNegara" runat="server" CssClass="combobox" Enabled="False">
																																						</asp:DropDownList></td>
																																				</tr>
																																				<tr>
																																					<td class="formText">
																																						g. Alamat Domisili</td>
																																					<td style="width: 5px">
																																					</td>
																																					<td class="formtext">
																																					</td>
																																				</tr>
																																				<tr>
																																					<td class="formtext" colspan="3">
																																						<table>
																																							<tbody>
																																								<tr>
																																									<td class="formtext">
																																										&nbsp; &nbsp;&nbsp;
																																									</td>
																																									<td class="formText">
																																										Nama Jalan</td>
																																									<td style="width: 5px">
																																										:
																																									</td>
																																									<td class="formtext">
																																										<asp:Label ID="txtTerlaporDOMNamaJalan" runat="server" Width="370px"></asp:Label></td>
																																								</tr>
																																								<tr>
																																									<td class="formtext">
																																									</td>
																																									<td class="formText">
																																										RT/RW</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										<asp:Label ID="txtTerlaporDOMRTRW" runat="server"></asp:Label></td>
																																								</tr>
																																								<tr>
																																									<td class="formtext">
																																									</td>
																																									<td class="formText">
																																										Kelurahan</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext" valign="top">
																																										<table style="width: 127px">
																																											<tbody>
																																												<tr>
																																													<td>
																																														<asp:Label ID="txtTerlaporDOMKelurahan" runat="server" Width="167px"></asp:Label></td>
																																													<td>
																																													</td>
																																												</tr>
																																											</tbody>
																																										</table>
																																										<asp:HiddenField ID="hfTerlaporDOMKelurahan" runat="server"></asp:HiddenField>
																																									</td>
																																								</tr>
																																								<tr>
																																									<td class="formtext">
																																									</td>
																																									<td class="formText">
																																										Kecamatan</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										&nbsp;<table style="width: 127px">
																																											<tbody>
																																												<tr>
																																													<td>
																																														<asp:Label ID="txtTerlaporDOMKecamatan" runat="server" Width="167px"></asp:Label></td>
																																													<td>
																																													</td>
																																												</tr>
																																											</tbody>
																																										</table>
																																										<asp:HiddenField ID="hfTerlaporDOMKecamatan" runat="server"></asp:HiddenField>
																																									</td>
																																								</tr>
																																								<tr>
																																									<td class="formtext">
																																									</td>
																																									<td class="formText">
																																										Kota / Kabupaten<span style="color: #cc0000">*</span></td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										<table style="width: 127px">
																																											<tbody>
																																												<tr>
																																													<td>
																																														<asp:Label ID="txtTerlaporDOMKotaKab" runat="server" Width="167px"></asp:Label></td>
																																													<td>
																																													</td>
																																												</tr>
																																											</tbody>
																																										</table>
																																										<asp:HiddenField ID="hfTerlaporDOMKotaKab" runat="server"></asp:HiddenField>
																																									</td>
																																								</tr>
																																								
																																								<tr>
																																									<td class="formtext">
																																									</td>
																																									<td class="formText">
																																										Provinsi</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										<table style="width: 127px">
																																											<tbody>
																																												<tr>
																																													<td>
																																														<asp:Label ID="txtTerlaporDOMProvinsi" runat="server" Width="167px"></asp:Label></td>
																																													<td>
																																													</td>
																																												</tr>
																																											</tbody>
																																										</table>
																																										<asp:HiddenField ID="hfTerlaporDOMProvinsi" runat="server"></asp:HiddenField>
																																									</td>
																																								</tr>
																																								 <tr>
                                                                                                                                                                      <td class="formtext">
                                                                                                                                                                          &nbsp;</td>
                                                                                                                                                                      <td class="formText">
                                                                                                                                                                          Negara</td>
                                                                                                                                                                      <td style="width: 5px">
                                                                                                                                                                          :</td>
                                                                                                                                                                      <td class="formtext">
                                                                                                                                                                          <table style="width: 127px">
                                                                                                                                                                              <tbody>
                                                                                                                                                                                  <tr>
                                                                                                                                                                                      <td>
                                                                                                                                                                                          <asp:Label ID="txtTerlaporDOMNegara" runat="server" Width="167px"></asp:Label>
                                                                                                                                                                                      </td>
                                                                                                                                                                                      <td>
                                                                                                                                                                                      </td>
                                                                                                                                                                                  </tr>
                                                                                                                                                                              </tbody>
                                                                                                                                                                          </table>
                                                                                                                                                                          <asp:HiddenField ID="hfTerlaporDOMNegara" runat="server" />
                                                                                                                                                                      </td>
                                                                                                                                                                </tr>
																																								<tr>
																																									<td class="formtext">
																																									</td>
																																									<td class="formText">
																																										Kode Pos</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										<asp:Label ID="txtTerlaporDOMKodePos" runat="server"></asp:Label></td>
																																								</tr>
																																								
																																							</tbody>
																																						</table>
																																					</td>
																																				</tr>
																																				<tr>
																																					<td class="formText">
																																						h. Alamat Sesuai Bukti Identitas</td>
																																					<td style="width: 5px">
																																					</td>
																																					<td class="formtext">
																																					</td>
																																				</tr>
																																				<tr>
																																					<td class="formtext" colspan="3">
																																						<table>
																																							<tbody>
																																								<tr>
																																									<td class="formText">
																																										&nbsp; &nbsp;&nbsp;
																																									</td>
																																									<td class="formText">
																																										Nama Jalan</td>
																																									<td style="width: 5px">
																																										:
																																									</td>
																																									<td class="formtext">
																																										<asp:Label ID="txtTerlaporIDNamaJalan" runat="server" Width="370px"></asp:Label></td>
																																								</tr>
																																								<tr>
																																									<td class="formText">
																																									</td>
																																									<td class="formText">
																																										RT/RW</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										<asp:Label ID="txtTerlaporIDRTRW" runat="server"></asp:Label></td>
																																								</tr>
																																								<tr>
																																									<td class="formText">
																																									</td>
																																									<td class="formText">
																																										Kelurahan</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext" valign="top">
																																										<table style="width: 127px">
																																											<tbody>
																																												<tr>
																																													<td>
																																														<asp:Label ID="txtTerlaporIDKelurahan" runat="server" Width="167px"></asp:Label></td>
																																													<td>
																																													</td>
																																												</tr>
																																											</tbody>
																																										</table>
																																										<asp:HiddenField ID="hfTerlaporIDKelurahan" runat="server"></asp:HiddenField>
																																									</td>
																																								</tr>
																																								<tr>
																																									<td class="formText">
																																									</td>
																																									<td class="formText">
																																										Kecamatan</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										&nbsp;<table style="width: 127px">
																																											<tbody>
																																												<tr>
																																													<td>
																																														<asp:Label ID="txtTerlaporIDKecamatan" runat="server" Width="167px"></asp:Label></td>
																																													<td>
																																													</td>
																																												</tr>
																																											</tbody>
																																										</table>
																																										<asp:HiddenField ID="hfTerlaporIDKecamatan" runat="server"></asp:HiddenField>
																																									</td>
																																								</tr>
																																								<tr>
																																									<td class="formText">
																																									</td>
																																									<td class="formText">
																																										Kota / Kabupaten<span style="color: #cc0000">*</span></td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										<table style="width: 127px">
																																											<tbody>
																																												<tr>
																																													<td>
																																														<asp:Label ID="txtTerlaporIDKotaKab" runat="server" Width="167px"></asp:Label></td>
																																													<td>
																																													</td>
																																												</tr>
																																											</tbody>
																																										</table>
																																										<asp:HiddenField ID="hfTerlaporIDKotaKab" runat="server"></asp:HiddenField>
																																									</td>
																																								</tr>
																																								
																																								<tr>
																																									<td class="formText">
																																									</td>
																																									<td class="formText">
																																										Provinsi</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										<table style="width: 127px">
																																											<tbody>
																																												<tr>
																																													<td style="height: 22px">
																																														<asp:Label ID="txtTerlaporIDProvinsi" runat="server" Width="167px"></asp:Label></td>
																																													<td style="height: 22px">
																																													</td>
																																												</tr>
																																											</tbody>
																																										</table>
																																										<asp:HiddenField ID="hfTerlaporIDProvinsi" runat="server"></asp:HiddenField>
																																									</td>
																																								</tr>
																																								 <tr>
                                                                                                                                                                      <td class="formtext">
                                                                                                                                                                          &nbsp;</td>
                                                                                                                                                                      <td class="formText">
                                                                                                                                                                          Negara</td>
                                                                                                                                                                      <td style="width: 5px">
                                                                                                                                                                          :</td>
                                                                                                                                                                      <td class="formtext">
                                                                                                                                                                          <table style="width: 127px">
                                                                                                                                                                              <tbody>
                                                                                                                                                                                  <tr>
                                                                                                                                                                                      <td>
                                                                                                                                                                                          <asp:Label ID="txtTerlaporIDNegara" runat="server" Width="167px"></asp:Label>
                                                                                                                                                                                      </td>
                                                                                                                                                                                      <td>
                                                                                                                                                                                      </td>
                                                                                                                                                                                  </tr>
                                                                                                                                                                              </tbody>
                                                                                                                                                                          </table>
                                                                                                                                                                          <asp:HiddenField ID="hfTerlaporIDNegara" runat="server" />
                                                                                                                                                                      </td>
                                                                                                                                                                </tr>
																																								<tr>
																																									<td class="formText">
																																									</td>
																																									<td class="formText">
																																										Kode Pos</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										<asp:Label ID="txtTerlaporIDKodePos" runat="server"></asp:Label></td>
																																								</tr>
																																							</tbody>
																																						</table>
																																					</td>
																																				</tr>
																																				<tr>
																																					<td class="formText">
																																						i. Alamat Sesuai Negara Asal</td>
																																					<td style="width: 5px">
																																					</td>
																																					<td>
																																					</td>
																																				</tr>
																																				<tr>
																																					<td class="formtext" colspan="3">
																																						<table>
																																							<tbody>
																																								<tr>
																																									<td class="formText">
																																									</td>
																																									<td class="formText">
																																										Nama Jalan</td>
																																									<td style="width: 5px">
																																										:
																																									</td>
																																									<td class="formtext">
																																										<asp:Label ID="txtTerlaporNANamaJalan" runat="server" Width="370px"></asp:Label></td>
																																								</tr>
																																								<tr>
																																									<td class="formText">
																																										&nbsp; &nbsp;&nbsp;
																																									</td>
																																									<td class="formText">
																																										Negara</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										<table style="width: 127px">
																																											<tbody>
																																												<tr>
																																													<td>
																																														<asp:Label ID="txtTerlaporNANegara" runat="server" Width="167px"></asp:Label></td>
																																													<td>
																																													</td>
																																												</tr>
																																											</tbody>
																																										</table>
																																										<asp:HiddenField ID="hfTerlaporNANegara" runat="server"></asp:HiddenField>
																																									</td>
																																								</tr>
																																								<tr>
																																									<td class="formText">
																																									</td>
																																									<td class="formText">
																																										Provinsi</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext" valign="top">
																																										<table style="width: 127px">
																																											<tbody>
																																												<tr>
																																													<td>
																																														<asp:Label ID="txtTerlaporNAProvinsi" runat="server" Width="167px"></asp:Label></td>
																																													<td>
																																													</td>
																																												</tr>
																																											</tbody>
																																										</table>
																																										<asp:HiddenField ID="hfTerlaporNAProvinsi" runat="server"></asp:HiddenField>
																																									</td>
																																								</tr>
																																								<tr>
																																									<td class="formText">
																																									</td>
																																									<td class="formText">
																																										Kota
																																									</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										<table style="width: 127px">
																																											<tbody>
																																												<tr>
																																													<td>
																																														<asp:Label ID="txtTerlaporNAKota" runat="server" Width="167px"></asp:Label></td>
																																													<td>
																																													</td>
																																												</tr>
																																											</tbody>
																																										</table>
																																										<asp:HiddenField ID="hfTerlaporIDKota" runat="server"></asp:HiddenField>
																																									</td>
																																								</tr>
																																								<tr>
																																									<td class="formText">
																																									</td>
																																									<td class="formText">
																																										Kode Pos<span style="color: #cc0000">*</span></td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										<asp:Label ID="txtTerlaporNAKodePos" runat="server"></asp:Label></td>
																																								</tr>
																																							</tbody>
																																						</table>
																																					</td>
																																				</tr>
																																				<tr>
																																					<td class="formText">
																																						j. Jenis Dokumen Identitas<span style="color: #cc0000">*</span></td>
																																					<td style="width: 5px">
																																						:</td>
																																					<td class="formtext">
																																						<ajax:AjaxPanel ID="ajPnlDokId" runat="server">
																																							<asp:DropDownList runat="server" ID="cboTerlaporJenisDocID" CssClass="combobox" Enabled="False">
																																							</asp:DropDownList>
																																						</ajax:AjaxPanel>
																																					</td>
																																				</tr>
																																				<tr>
																																					<td id="tdNomorId" class="formtext" colspan="3" runat="server">
																																						<table>
																																							<tbody>
																																								<tr>
																																									<td class="formText">
																																										 Nomor Identitas <span style="color: #cc0000">*</span></td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td style="width: 260px" class="formtext">
																																										<asp:Label ID="txtTerlaporNomorID" runat="server" Width="257px"></asp:Label></td>
																																								</tr>
																																							</tbody>
																																						</table>
																																					</td>
																																				</tr>
																																				<tr>
																																					<td class="formText">
																																						k. Nomor Pokok Wajib Pajak (NPWP)</td>
																																					<td style="width: 5px">
																																						:</td>
																																					<td class="formtext">
																																						<asp:Label ID="txtTerlaporNPWP" runat="server" Width="257px"></asp:Label></td>
																																				</tr>
																																				<tr>
																																					<td class="formText">
																																						l. Pekerjaan</td>
																																					<td style="width: 5px">
																																					</td>
																																					<td class="formtext">
																																					</td>
																																				</tr>
																																				<tr>
																																					<td class="formtext" colspan="3">
																																						<table>
																																							<tbody>
																																								<tr>
																																									<td class="formtext">
																																										&nbsp; &nbsp;&nbsp;
																																									</td>
																																									<td class="formText">
																																										Pekerjaan<span style="color: #cc0000">*</span></td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td style="width: 260px" class="formtext">
																																										<asp:Label ID="txtTerlaporPekerjaan" runat="server" Width="257px"></asp:Label>
																																										<asp:HiddenField ID="hfTerlaporPekerjaan" runat="server"></asp:HiddenField>
																																									</td>
																																								</tr>
																																								<tr>
																																									<td class="formtext">
																																									</td>
																																									<td class="formText">
																																										Jabatan</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td style="width: 260px" class="formtext">
																																										<asp:Label ID="txtTerlaporJabatan" runat="server" Width="257px"></asp:Label></td>
																																								</tr>
																																								<tr>
																																									<td class="formtext">
																																									</td>
																																									<td class="formText">
																																										Penghasilan rata-rata/th (Rp)</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td style="width: 260px" class="formtext">
																																										<asp:Label ID="txtTerlaporPenghasilanRataRata" runat="server" Width="257px"></asp:Label></td>
																																								</tr>
																																								<tr>
																																									<td class="formtext">
																																									</td>
																																									<td class="formText">
																																										Tempat kerja</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td style="width: 260px" class="formtext">
																																										<asp:Label ID="txtTerlaporTempatKerja" runat="server" Width="257px"></asp:Label></td>
																																								</tr>
																																							</tbody>
																																						</table>
																																					</td>
																																				</tr>
																																			</tbody>
																																		</table>
																																	</div>
																																	<div id="divKorporasi" runat="server" hidden="true">
																																		<table>
																																			<tbody>
																																				<tr>
																																					<td class="formtext" colspan="3">
																																						<strong>3.1.3. Korporasi</strong></td>
																																				</tr>
																																				<tr>
																																					<td class="formText">
																																						a. Bentuk Badan Usaha<span style="color: #cc0000">*</span></td>
																																					<td style="width: 5px">
																																						:</td>
																																					<td class="formtext">
																																						<asp:DropDownList ID="cboTerlaporCORPBentukBadanUsaha" runat="server" Enabled="False">
																																						</asp:DropDownList></td>
																																				</tr>
																																				<tr>
																																					<td class="formText">
																																						b. Nama Korporasi <span style="color: #cc0000">*</span></td>
																																					<td style="width: 5px">
																																						:</td>
																																					<td class="formtext">
																																						<asp:Label ID="txtTerlaporCORPNama" runat="server" Width="370px"></asp:Label></td>
																																				</tr>
																																				<tr>
																																					<td class="formText">
																																						c. Bidang Usaha Korporasi<span style="color: #cc0000">*</span></td>
																																					<td style="width: 5px">
																																						:</td>
																																					<td class="formtext">
																																						<table style="width: 127px">
																																							<tbody>
																																								<tr>
																																									<td>
																																										<asp:Label ID="txtTerlaporCORPBidangUsaha" runat="server" Width="167px"></asp:Label></td>
																																									<td>
																																									</td>
																																								</tr>
																																							</tbody>
																																						</table>
																																						<asp:HiddenField ID="hfTerlaporCORPBidangUsaha" runat="server"></asp:HiddenField>
																																					</td>
																																				</tr>
																																				<tr>
																																					<td class="formText">
																																						d. Alamat Korporasi<span style="color: #cc0000">*</span></td>
																																					<td style="width: 5px">
																																						:</td>
																																					<td class="formtext">
																																						<asp:RadioButtonList ID="rblTerlaporCORPTipeAlamat" runat="server" Enabled="False"
																																							AutoPostBack="True" RepeatDirection="Horizontal">
																																							<asp:ListItem>Dalam Negeri</asp:ListItem>
																																							<asp:ListItem>Luar Negeri</asp:ListItem>
																																						</asp:RadioButtonList></td>
																																				</tr>
																																				<tr>
																																					<td class="formText">
																																						e. Alamat Lengkap Korporasi</td>
																																					<td style="width: 5px">
																																					</td>
																																					<td class="formtext">
																																					</td>
																																				</tr>
																																				<tr>
																																					<td class="formtext" colspan="3">
																																						<table>
																																							<tbody>
																																								<tr>
																																									<td class="formtext">
																																										&nbsp; &nbsp;&nbsp;
																																									</td>
																																									<td class="formText">
																																										Nama Jalan</td>
																																									<td style="width: 5px">
																																										:
																																									</td>
																																									<td class="formtext">
																																										<asp:Label ID="txtTerlaporCORPDLNamaJalan" runat="server" Width="370px"></asp:Label></td>
																																								</tr>
																																								<tr>
																																									<td class="formtext">
																																									</td>
																																									<td class="formText">
																																										RT/RW</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										<asp:Label ID="txtTerlaporCORPDLRTRW" runat="server"></asp:Label></td>
																																								</tr>
																																								<tr>
																																									<td class="formtext">
																																									</td>
																																									<td class="formText">
																																										Kelurahan</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext" valign="top">
																																										<table style="width: 127px">
																																											<tbody>
																																												<tr>
																																													<td>
																																														<asp:Label ID="txtTerlaporCORPDLKelurahan" runat="server" Width="167px"></asp:Label></td>
																																													<td>
																																													</td>
																																												</tr>
																																											</tbody>
																																										</table>
																																										<asp:HiddenField ID="hfTerlaporCORPDLKelurahan" runat="server"></asp:HiddenField>
																																									</td>
																																								</tr>
																																								<tr>
																																									<td class="formtext">
																																									</td>
																																									<td class="formText">
																																										Kecamatan</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										&nbsp;<table style="width: 127px">
																																											<tbody>
																																												<tr>
																																													<td>
																																														<asp:Label ID="txtTerlaporCORPDLKecamatan" runat="server" Width="167px"></asp:Label></td>
																																													<td>
																																													</td>
																																												</tr>
																																											</tbody>
																																										</table>
																																										<asp:HiddenField ID="hfTerlaporCORPDLKecamatan" runat="server"></asp:HiddenField>
																																									</td>
																																								</tr>
																																								<tr>
																																									<td class="formtext">
																																									</td>
																																									<td class="formText">
																																										Kota / Kabupaten<span style="color: #cc0000">*</span></td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										<table style="width: 127px">
																																											<tbody>
																																												<tr>
																																													<td>
																																														<asp:Label ID="txtTerlaporCORPDLKotaKab" runat="server" Width="167px"></asp:Label></td>
																																													<td>
																																													</td>
																																												</tr>
																																											</tbody>
																																										</table>
																																										<asp:HiddenField ID="hfTerlaporCORPDLKotaKab" runat="server"></asp:HiddenField>
																																									</td>
																																								</tr>
																																								
																																								<tr>
																																									<td class="formtext">
																																									</td>
																																									<td class="formText">
																																										Provinsi</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										<table style="width: 127px">
																																											<tbody>
																																												<tr>
																																													<td>
																																														<asp:Label ID="txtTerlaporCORPDLProvinsi" runat="server" Width="167px"></asp:Label></td>
																																													<td>
																																													</td>
																																												</tr>
																																											</tbody>
																																										</table>
																																										<asp:HiddenField ID="hfTerlaporCORPDLProvinsi" runat="server"></asp:HiddenField>
																																									</td>
																																								</tr>
																																								<tr>
																																									<td class="formtext">
																																									</td>
																																									<td class="formText">
																																										Negara</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										<table style="width: 127px">
																																											<tbody>
																																												<tr>
																																													<td>
																																														<asp:Label ID="txtTerlaporCORPDLNegara" runat="server" Width="167px"></asp:Label></td>
																																													<td>
																																													</td>
																																												</tr>
																																											</tbody>
																																										</table>
																																										<asp:HiddenField ID="hfTerlaporCORPDLNegara" runat="server"></asp:HiddenField>
																																									</td>
																																								</tr>
																																								<tr>
																																									<td class="formtext">
																																									</td>
																																									<td class="formText">
																																										Kode Pos</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										<asp:Label ID="txtTerlaporCORPDLKodePos" runat="server"></asp:Label></td>
																																								</tr>
																																							</tbody>
																																						</table>
																																					</td>
																																				</tr>
																																				<tr>
																																					<td class="formText">
																																						i. Alamat Korporasi Luar Negeri</td>
																																					<td style="width: 5px">
																																					</td>
																																					<td class="formtext">
																																					</td>
																																				</tr>
																																				<tr>
																																					<td class="formtext" colspan="3">
																																						<table>
																																							<tbody>
																																								<tr>
																																									<td class="formtext">
																																										&nbsp; &nbsp;&nbsp;
																																									</td>
																																									<td style="width: 69px" class="formText">
																																										Nama Jalan</td>
																																									<td style="width: 5px">
																																										:
																																									</td>
																																									<td class="formtext">
																																										<asp:Label ID="txtTerlaporCORPLNNamaJalan" runat="server" Width="370px"></asp:Label></td>
																																								</tr>
																																								<tr>
																																									<td class="formtext">
																																									</td>
																																									<td style="width: 69px" class="formText">
																																										Negara</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										<table style="width: 127px">
																																											<tbody>
																																												<tr>
																																													<td>
																																														<asp:Label ID="txtTerlaporCORPLNNegara" runat="server" Width="167px"></asp:Label></td>
																																													<td>
																																													</td>
																																												</tr>
																																											</tbody>
																																										</table>
																																										<asp:HiddenField ID="hfTerlaporCORPLNNegara" runat="server"></asp:HiddenField>
																																									</td>
																																								</tr>
																																								<tr>
																																									<td class="formtext">
																																									</td>
																																									<td style="width: 69px" class="formText">
																																										Provinsi</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext" valign="top">
																																										<table style="width: 127px">
																																											<tbody>
																																												<tr>
																																													<td style="height: 22px">
																																														<asp:Label ID="txtTerlaporCORPLNProvinsi" runat="server" Width="167px"></asp:Label></td>
																																													<td style="height: 22px">
																																													</td>
																																												</tr>
																																											</tbody>
																																										</table>
																																										<asp:HiddenField ID="hfTerlaporCORPLNProvinsi" runat="server"></asp:HiddenField>
																																									</td>
																																								</tr>
																																								<tr>
																																									<td class="formtext">
																																									</td>
																																									<td style="width: 69px" class="formText">
																																										Kota
																																									</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										<table style="width: 127px">
																																											<tbody>
																																												<tr>
																																													<td>
																																														<asp:Label ID="txtTerlaporCORPLNKota" runat="server" Width="167px"></asp:Label></td>
																																													<td>
																																													</td>
																																												</tr>
																																											</tbody>
																																										</table>
																																										<asp:HiddenField ID="hfTerlaporCORPLNKota" runat="server"></asp:HiddenField>
																																									</td>
																																								</tr>
																																								<tr>
																																									<td class="formtext">
																																									</td>
																																									<td style="width: 69px" class="formText">
																																										Kode Pos<span style="color: #cc0000">*</span></td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										<asp:Label ID="txtTerlaporCORPLNKodePos" runat="server"></asp:Label></td>
																																								</tr>
																																							</tbody>
																																						</table>
																																					</td>
																																				</tr>
																																				<tr>
																																					<td class="formText">
																																						k. Nomor Pokok Wajib Pajak (NPWP)</td>
																																					<td style="width: 5px">
																																						:</td>
																																					<td class="formtext">
																																						<asp:Label ID="txtTerlaporCORPNPWP" runat="server" Width="257px"></asp:Label></td>
																																				</tr>
																																			</tbody>
																																		</table>
																																	</div>
																																</td>
																															</tr>
																														</tbody>
																													</table>
																												</td>
																											</tr>
																										</tbody>
																									</table>
																								</td>
																							</tr>
																						</tbody>
																					</table>
																				</ajax:AjaxPanel>
																			</td>
																		</tr>
																		<tr>
																			<td style="height: 6px" valign="middle" align="left" width="100%" background="Images/search-bar-background.gif">
																				<table cellspacing="0" cellpadding="0" border="0">
																					<tbody>
																						<tr>
																							<td class="formtext">
																								<asp:Label ID="Label3" runat="server" Text="C. TRANSAKSI" Font-Bold="True"></asp:Label>&nbsp;</td>
																							<td>
																								<a title="click to minimize or maximize" onclick="javascript:ShowHidePanel('Transaksi','Img2','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
																									href="#">
																									<img id="Img2" height="12" src="Images/search-bar-minimize.gif" width="12" border="0" /></a></td>
																						</tr>
																					</tbody>
																				</table>
																			</td>
																		</tr>
																		<tr id="Transaksi">
																			<td bgcolor="#ffffff" colspan="2">
																				<ajax:AjaxPanel ID="AjaxPanel4" runat="server" Width="100%">
																					<table>
																						<tbody>
																							<tr>
																								<td>
																									<asp:Menu ID="Menu1" runat="server" CssClass="tabs" StaticEnableDefaultPopOutImage="False"
																										Orientation="Horizontal">
																										<Items>
																											<asp:MenuItem Text="Kas Masuk" Value="0" Selected="True"></asp:MenuItem>
																											<asp:MenuItem Text="Kas Keluar" Value="1"></asp:MenuItem>
																										</Items>
																										<StaticSelectedStyle CssClass="selectedTab" />
																										<StaticMenuItemStyle CssClass="tab" />
																									</asp:Menu>
																									<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
																										<asp:View ID="ViewKasMasuk" runat="server">
																											<table width="100%">
																												<tbody>
																													<tr>
																														<td style="width: 300px" class="formtext">
																															4.1. Tanggal Transaksi (tgl/bln/thn)<span style="color: #cc0000">*</span></td>
																														<td style="width: 2px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="txtTRXKMTanggalTrx" runat="server" Width="96px" MaxLength="50"></asp:Label>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															4.2. Nama Kantor PJK tempat terjadinya transaksi</td>
																														<td style="width: 2px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<div>
																																<table>
																																	<tbody>
																																		<tr>
																																			<td class="formtext">
																																				&nbsp; &nbsp;&nbsp;
																																			</td>
																																			<td class="formtext">
																																				a. Nama Kantor<span style="color: #cc0000">*</span></td>
																																			<td style="width: 5px">
																																				:</td>
																																			<td class="formtext">
																																				<asp:Label ID="txtTRXKMNamaKantor" runat="server" Width="370px"></asp:Label>
																																				&nbsp;</td>
																																		</tr>
																																		<tr>
																																			<td class="formtext">
																																			</td>
																																			<td class="formtext">
																																				b. Kota Kabupaten<span style="color: #cc0000">*</span></td>
																																			<td style="width: 5px">
																																				:</td>
																																			<td class="formtext">
																																				<table style="width: 127px">
																																					<tbody>
																																						<tr>
																																							<td>
																																								<asp:Label ID="txtTRXKMKotaKab" runat="server" Width="167px"></asp:Label></td>
																																							<td>
																																							</td>
																																						</tr>
																																					</tbody>
																																				</table>
																																				<asp:HiddenField ID="hfTRXKMKotaKab" runat="server"></asp:HiddenField>
																																				&nbsp;
																																			</td>
																																		</tr>
																																		<tr>
																																			<td class="formtext">
																																			</td>
																																			<td class="formtext">
																																				c. Provinsi<span style="color: #cc0000">*</span></td>
																																			<td style="width: 5px">
																																				:</td>
																																			<td class="formtext">
																																				<table style="width: 251px">
																																					<tbody>
																																						<tr>
																																							<td style="width: 170px; height: 15px">
																																								<asp:Label ID="txtTRXKMProvinsi" runat="server" Width="167px"></asp:Label></td>
																																						</tr>
																																					</tbody>
																																				</table>
																																				<asp:HiddenField ID="hfTRXKMProvinsi" runat="server"></asp:HiddenField>
																																			</td>
																																		</tr>
																																	</tbody>
																																</table>
																															</div>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															4.3. Detail Kas Masuk</td>
																														<td style="width: 2px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formtext">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td class="formtext">
																																			a. Kas Masuk (Rp)</td>
																																		<td style="width: 2px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTRXKMDetilKasMasuk" runat="server" Width="167px" AutoPostBack="True"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			b. Kas Masuk Dalam valuta asing (Dapat &gt; 1)</td>
																																		<td style="width: 2px">
																																		</td>
																																		<td class="formtext">
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext" colspan="1">
																																		</td>
																																		<td class="formtext" colspan="3">
																																			<table>
																																				<tbody>
																																					<tr>
																																						<td class="formtext">
																																							&nbsp; &nbsp;&nbsp;
																																						</td>
																																						<td class="formtext">
																																							i. Mata Uang</td>
																																						<td style="width: 2px">
																																							:</td>
																																						<td class="formtext">
																																							<table style="width: 127px">
																																								<tbody>
																																									<tr>
																																										<td>
																																											<asp:Label ID="txtTRXKMDetilMataUang" runat="server" Width="167px"></asp:Label></td>
																																										<td>
																																										</td>
																																									</tr>
																																								</tbody>
																																							</table>
																																							<asp:HiddenField ID="hfTRXKMDetilMataUang" runat="server"></asp:HiddenField>
																																						</td>
																																					</tr>
																																					<tr>
																																						<td class="formtext">
																																						</td>
																																						<td class="formtext">
																																							ii. Kurs Transaksi</td>
																																						<td style="width: 2px">
																																							:</td>
																																						<td class="formtext">
																																							<asp:Label ID="txtTRXKMDetailKursTrx" runat="server" Width="167px"></asp:Label></td>
																																					</tr>
																																					<tr>
																																						<td class="formtext">
																																						</td>
																																						<td class="formtext">
																																							iii. Jumlah&nbsp;</td>
																																						<td style="width: 2px">
																																							:</td>
																																						<td class="formtext">
																																							<asp:Label ID="txtTRXKMDetilJumlah" runat="server" Width="167px"></asp:Label></td>
																																					</tr>
																																					<tr>
																																						<td class="formtext">
																																						</td>
																																						<td class="formtext" colspan="3">
																																						</td>
																																					</tr>
																																					<tr>
																																						<td class="formtext">
																																						</td>
																																						<td class="formtext" colspan="3">
																																							<asp:GridView ID="grvTRXKMDetilValutaAsing" runat="server" SkinID="grv2" AutoGenerateColumns="False">
																																								<Columns>
																																									<asp:BoundField HeaderText="No" />
																																									<asp:BoundField HeaderText="Mata Uang" DataField="MataUang" />
																																									<asp:BoundField HeaderText="Kurs Transaksi" DataField="KursTransaksi" />
																																									<asp:BoundField HeaderText="Jumlah" DataField="Jumlah" />
																																									<asp:BoundField DataField="JumlahRp" HeaderText="Jumlah (Rp)" ItemStyle-HorizontalAlign="Right" />
																																								</Columns>
																																							</asp:GridView>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			c. Total Kas Masuk (a+b) (Rp)</td>
																																		<td style="width: 2px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="lblTRXKMDetilValutaAsingJumlahRp" runat="server"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															4.4. Nomor Rekening Nasabah</td>
																														<td style="width: 2px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="txtTRXKMNoRekening" runat="server" Width="167px"></asp:Label></td>
																													</tr>
																													<tr>
																														<td style="height: 15px" class="formtext">
																															4.5. Identitas pihak terkait dengan Terlapor</td>
																														<td style="width: 2px; height: 15px">
																														</td>
																														<td style="height: 15px" class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															&nbsp; &nbsp; &nbsp; Tipe Pihak Terkait</td>
																														<td style="width: 2px">
																															:</td>
																														<td class="formtext">
																															<asp:RadioButtonList ID="rblTRXKMTipePelapor" runat="server" AutoPostBack="True"
                                                                                                                                RepeatDirection="Horizontal" Enabled="False">
                                                                                                                                <asp:ListItem Value="1">Perorangan</asp:ListItem>
                                                                                                                                <asp:ListItem Value="2">Korporasi</asp:ListItem>
																															</asp:RadioButtonList></td>
																													</tr>
																												</tbody>
																											</table>
																											<table id="tblTRXKMTipePelapor" runat="server">
																												<tbody>
																													<tr>
																														<td class="formtext" colspan="3">
																															<strong>4.5.1. Perorangan</strong>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															a. Gelar</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="txtTRXKMINDVGelar" runat="server"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															b. Nama Lengkap
																														</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="txtTRXKMINDVNamaLengkap" runat="server" Width="370px"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															c. Tempat Lahir</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="txtTRXKMINDVTempatLahir" runat="server" Width="240px"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															d. Tanggal Lahir
																														</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<br />
																															<asp:Label ID="txtTRXKMINDVTanggalLahir" runat="server" Width="96px" MaxLength="50"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															e. Kewarganegaraan
																														</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:RadioButtonList ID="rblTRXKMINDVKewarganegaraan" runat="server" Enabled="False"
																																AutoPostBack="True" RepeatDirection="Horizontal">
																																<asp:ListItem Selected="True" Value="1">WNI</asp:ListItem>
																																<asp:ListItem Value="2">WNA</asp:ListItem>
																															</asp:RadioButtonList></td>
																													</tr>
																													<tr style="color: #cc0000">
																														<td class="formtext">
																															f. Negara
																														</td>
																														<td style="width: 5px; color: #000000">
																															:</td>
																														<td class="formtext">
																															<asp:DropDownList ID="cboTRXKMINDVNegara" runat="server" CssClass="combobox" Enabled="False">
																															</asp:DropDownList></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															g. Alamat Domisili</td>
																														<td style="width: 5px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formtext">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td class="formtext">
																																			Nama Jalan</td>
																																		<td style="width: 5px">
																																			:
																																		</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTRXKMINDVDOMNamaJalan" runat="server" Width="370px"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			RT/RW</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTRXKMINDVDOMRTRW" runat="server"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kelurahan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext" valign="top">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTRXKMINDVDOMKelurahan" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTRXKMINDVDOMKelurahan" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kecamatan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			&nbsp;<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTRXKMINDVDOMKecamatan" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTRXKMINDVDOMKecamatan" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kota / Kabupaten</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTRXKMINDVDOMKotaKab" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTRXKMINDVDOMKotaKab" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Provinsi</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTRXKMINDVDOMProvinsi" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTRXKMINDVDOMProvinsi" runat="server"></asp:HiddenField>
																																		</td>
                                                                                                                                        <tr>
                                                                                                                                            <td class="formtext">
                                                                                                                                                &nbsp;</td>
                                                                                                                                            <td class="formText">
                                                                                                                                                Negara</td>
                                                                                                                                            <td style="width: 5px">
                                                                                                                                                :</td>
                                                                                                                                            <td class="formtext">
                                                                                                                                                <table style="width: 127px">
                                                                                                                                                    <tbody>
                                                                                                                                                        <tr>
                                                                                                                                                            <td>
                                                                                                                                                                <asp:Label ID="txtTRXKMINDVDOMNegara" runat="server" Width="167px"></asp:Label>
                                                                                                                                                            </td>
                                                                                                                                                            <td>
                                                                                                                                                            </td>
                                                                                                                                                        </tr>
                                                                                                                                                    </tbody>
                                                                                                                                                </table>
                                                                                                                                                <asp:HiddenField ID="hfTRXKMINDVDOMNegara" runat="server" />
                                                                                                                                            </td>
                                                                                                                                        </tr>
																																	</tr>
																																		<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kode Pos</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTRXKMINDVDOMKodePos" runat="server"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															h. Alamat Sesuai Bukti Identitas</td>
																														<td style="width: 5px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formtext">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td class="formtext">
																																			Nama Jalan</td>
																																		<td style="width: 5px">
																																			:
																																		</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTRXKMINDVIDNamaJalan" runat="server" Width="370px"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			RT/RW</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTRXKMINDVIDRTRW" runat="server"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kelurahan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext" valign="top">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTRXKMINDVIDKelurahan" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTRXKMINDVIDKelurahan" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kecamatan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			&nbsp;<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td style="height: 23px">
																																							<asp:Label ID="txtTRXKMINDVIDKecamatan" runat="server" Width="167px"></asp:Label></td>
																																						<td style="height: 23px">
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTRXKMINDVIDKecamatan" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kota / Kabupaten</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTRXKMINDVIDKotaKab" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTRXKMINDVIDKotaKab" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Provinsi</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTRXKMINDVIDProvinsi" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																							&nbsp;
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTRXKMINDVIDProvinsi" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
                                                                                                                                    <tr>
                                                                                                                                        <td class="formtext">
                                                                                                                                            &nbsp;</td>
                                                                                                                                        <td class="formText">
                                                                                                                                            Negara</td>
                                                                                                                                        <td style="width: 5px">
                                                                                                                                            :</td>
                                                                                                                                        <td class="formtext">
                                                                                                                                            <table style="width: 127px">
                                                                                                                                                <tbody>
                                                                                                                                                    <tr>
                                                                                                                                                        <td>
                                                                                                                                                            <asp:Label ID="txtTRXKMINDVIDNegara" runat="server" Width="167px"></asp:Label>
                                                                                                                                                        </td>
                                                                                                                                                        <td>
                                                                                                                                                        </td>
                                                                                                                                                    </tr>
                                                                                                                                                </tbody>
                                                                                                                                            </table>
                                                                                                                                            <asp:HiddenField ID="hfTRXKMINDVIDNegara" runat="server" />
                                                                                                                                        </td>
                                                                                                                                    </tr>
																																		<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kode Pos</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTRXKMINDVIDKodePos" runat="server"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															i. Alamat Sesuai Negara Asal</td>
																														<td style="width: 5px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formtext">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td class="formtext">
																																			Nama Jalan</td>
																																		<td style="width: 5px">
																																			:
																																		</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTRXKMINDVNANamaJalan" runat="server" Width="370px"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Negara</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td style="height: 22px">
																																							<asp:Label ID="txtTRXKMINDVNANegara" runat="server" Width="167px"></asp:Label></td>
																																						<td style="height: 22px">
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTRXKMINDVNANegara" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Provinsi</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext" valign="top">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTRXKMINDVNAProvinsi" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTRXKMINDVNAProvinsi" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kota
																																		</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTRXKMINDVNAKota" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTRXKMINDVNAKota" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kode Pos</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTRXKMINDVNAKodePos" runat="server"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															j. Jenis Dokumen Identitas</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<ajax:AjaxPanel ID="AjaxPanel2" runat="server">
																																<asp:DropDownList runat="server" ID="cboTRXKMINDVJenisID" CssClass="combobox" Enabled="False">
																																</asp:DropDownList>
																															</ajax:AjaxPanel>
																														</td>
																													</tr>
																													<tr>
																														<td id="Td1" class="formtext" colspan="3" runat="server">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formtext">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td class="formtext">
																																			 Nomor Identitas </td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td style="width: 260px" class="formtext">
																																			<asp:Label ID="txtTRXKMINDVNomorID" runat="server" Width="257px"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															k. Nomor Pokok Wajib Pajak (NPWP)</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="txtTRXKMINDVNPWP" runat="server" Width="257px"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															l. Pekerjaan</td>
																														<td style="width: 5px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<table>
																																<tbody>
																																	<tr>
																																		<td style="height: 40px" class="formtext">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td style="height: 40px" class="formtext">
																																			Pekerjaan</td>
																																		<td style="width: 5px; height: 40px">
																																			:</td>
																																		<td style="width: 260px; height: 40px" class="formtext">
																																			<asp:Label ID="txtTRXKMINDVPekerjaan" runat="server" Width="257px"></asp:Label>
																																			<asp:HiddenField ID="hfTRXKMINDVPekerjaan" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Jabatan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td style="width: 260px" class="formtext">
																																			<asp:Label ID="txtTRXKMINDVJabatan" runat="server" Width="257px"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Penghasilan rata-rata/th (Rp)</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td style="width: 260px" class="formtext">
																																			<asp:Label ID="txtTRXKMINDVPenghasilanRataRata" runat="server" Width="257px"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Tempat kerja</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td style="width: 260px" class="formtext">
																																			<asp:Label ID="txtTRXKMINDVTempatKerja" runat="server" Width="257px"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															m. Tujuan Transaksi</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="txtTRXKMINDVTujuanTrx" runat="server" Width="257px"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															n. Sumber Dana</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="txtTRXKMINDVSumberDana" runat="server" Width="257px"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															o. Rekening lain yang terkait dengan transaksi (apabila ada)</td>
																														<td style="width: 5px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formtext">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td class="formtext">
																																			ii. Nama Bank Lain</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTRXKMINDVNamaBankLain" runat="server" Width="257px"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			ii. Nomor Rekening Tujuan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTRXKMINDVNoRekeningTujuan" runat="server" Width="257px"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																												</tbody>
																											</table>
																											<table id="tblTRXKMTipePelaporKorporasi" runat="server">
																												<tbody>
																													<tr>
																														<td class="formtext" colspan="3">
																															<strong>4.5.2. Korporasi</strong></td>
																													</tr>
																													<tr>
																														<td style="height: 27px" class="formtext">
																															a. Bentuk Badan Usaha</td>
																														<td style="width: 5px; height: 27px">
																															:</td>
																														<td style="height: 27px" class="formtext">
																															&nbsp; &nbsp;
																															<asp:DropDownList ID="cboTRXKMCORPBentukBadanUsaha" runat="server" Enabled="False">
																															</asp:DropDownList></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															b. Nama Korporasi
																														</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="txtTRXKMCORPNama" runat="server" Width="370px"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															c. Bidang Usaha Korporasi</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<table style="width: 127px">
																																<tbody>
																																	<tr>
																																		<td>
																																			<asp:Label ID="txtTRXKMCORPBidangUsaha" runat="server" Width="167px"></asp:Label></td>
																																		<td>
																																		</td>
																																	</tr>
																																</tbody>
																															</table>
																															<asp:HiddenField ID="hfTRXKMCORPBidangUsaha" runat="server"></asp:HiddenField>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															d. Alamat Korporasi</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:RadioButtonList ID="rblTRXKMCORPTipeAlamat" runat="server" Enabled="False" AutoPostBack="True"
																																RepeatDirection="Horizontal">
																																<asp:ListItem Selected="True">Dalam Negeri</asp:ListItem>
																																<asp:ListItem>Luar Negeri</asp:ListItem>
																															</asp:RadioButtonList></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															e. Alamat Lengkap Korporasi</td>
																														<td style="width: 5px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formtext">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td class="formtext">
																																			Nama Jalan</td>
																																		<td style="width: 5px">
																																			:
																																		</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTRXKMCORPDLNamaJalan" runat="server" Width="370px"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			RT/RW</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTRXKMCORPDLRTRW" runat="server"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kelurahan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext" valign="top">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td style="height: 22px">
																																							<asp:Label ID="txtTRXKMCORPDLKelurahan" runat="server" Width="167px"></asp:Label></td>
																																						<td style="height: 22px">
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTRXKMCORPDLKelurahan" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kecamatan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			&nbsp;<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTRXKMCORPDLKecamatan" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTRXKMCORPDLKecamatan" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kota / Kabupaten</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTRXKMCORPDLKotaKab" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTRXKMCORPDLKotaKab" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Provinsi</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td style="height: 22px">
																																							<asp:Label ID="txtTRXKMCORPDLProvinsi" runat="server" Width="167px"></asp:Label></td>
																																						<td style="height: 22px">
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTRXKMCORPDLProvinsi" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Negara</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTRXKMCORPDLNegara" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTRXKMCORPDLNegara" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kode Pos</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTRXKMCORPDLKodePos" runat="server"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															i. Alamat Korporasi Luar Negeri</td>
																														<td style="width: 5px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formtext">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td class="formtext">
																																			Nama Jalan</td>
																																		<td style="width: 5px">
																																			:
																																		</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTRXKMCORPLNNamaJalan" runat="server" Width="370px"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Negara</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTRXKMCORPLNNegara" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTRXKMCORPLNNegara" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Provinsi</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext" valign="top">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTRXKMCORPLNProvinsi" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTRXKMCORPLNProvinsi" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kota
																																		</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTRXKMCORPLNKota" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTRXKMCORPLNKota" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kode Pos</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTRXKMCORPLNKodePos" runat="server"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															k. Nomor Pokok Wajib Pajak (NPWP)</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="txtTRXKMCORPNPWP" runat="server" Width="257px"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															l. Tujuan Transaksi</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="txtTRXKMCORPTujuanTrx" runat="server" Width="257px"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															m. Sumber Dana</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="txtTRXKMCORPSumberDana" runat="server" Width="257px"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															o. Rekening lain yang terkait dengan transaksi (apabila ada)</td>
																														<td style="width: 5px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formtext">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td class="formtext">
																																			ii. Nama Bank Lain</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTRXKMCORPNamaBankLain" runat="server" Width="257px"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			ii. Nomor Rekening Tujuan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTRXKMCORPNoRekeningTujuan" runat="server" Width="257px"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																												</tbody>
																											</table>
																										</asp:View>
																										<asp:View ID="ViewKasKeluar" runat="server">
																											<table width="100%">
																												<tbody>
																													<tr>
																														<td style="width: 300px" class="formtext">
																															5.1. Tanggal Transaksi (tgl/bln/thn)<span style="color: #cc0000">*</span></td>
																														<td style="width: 2px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="txtTRXKKTanggalTransaksi" runat="server" Width="96px" MaxLength="50"></asp:Label>
																														</td>
																													</tr>
																													<tr>
																														<td style="height: 27px" class="formtext">
																															5.2. Nama Kantor PJK tempat terjadinya transaksi</td>
																														<td style="width: 2px; height: 27px">
																														</td>
																														<td style="height: 27px" class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<div>
																																<table>
																																	<tbody>
																																		<tr>
																																			<td class="formtext">
																																				&nbsp; &nbsp;&nbsp;
																																			</td>
																																			<td class="formtext">
																																				a. Nama Kantor<span style="color: #cc0000">*</span></td>
																																			<td style="width: 5px">
																																				:</td>
																																			<td class="formtext">
																																				<asp:Label ID="txtTRXKKNamaKantor" runat="server" Width="370px"></asp:Label>
																																				&nbsp;</td>
																																		</tr>
																																		<tr>
																																			<td class="formtext">
																																			</td>
																																			<td class="formtext">
																																				b. Kota Kabupaten<span style="color: #cc0000">*</span></td>
																																			<td style="width: 5px">
																																				:</td>
																																			<td class="formtext">
																																				<table style="width: 127px">
																																					<tbody>
																																						<tr>
																																							<td>
																																								<asp:Label ID="txtTRXKKKotaKab" runat="server" Width="167px"></asp:Label></td>
																																							<td>
																																							</td>
																																						</tr>
																																					</tbody>
																																				</table>
																																				<asp:HiddenField ID="hfTRXKKKotaKab" runat="server"></asp:HiddenField>
																																				&nbsp;
																																			</td>
																																		</tr>
																																		<tr>
																																			<td class="formtext">
																																			</td>
																																			<td class="formtext">
																																				c. Provinsi<span style="color: #cc0000">*</span></td>
																																			<td style="width: 5px">
																																				:</td>
																																			<td class="formtext">
																																				<table style="width: 127px">
																																					<tbody>
																																						<tr>
																																							<td>
																																								<asp:Label ID="txtTRXKKProvinsi" runat="server" Width="167px"></asp:Label></td>
																																						</tr>
																																					</tbody>
																																				</table>
																																				<asp:HiddenField ID="hfTRXKKProvinsi" runat="server"></asp:HiddenField>
																																				&nbsp;
																																			</td>
																																		</tr>
																																	</tbody>
																																</table>
																															</div>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															5.3. Detail Kas Keluar</td>
																														<td style="width: 2px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formtext">
																																			a. Kas Keluar (Rp)</td>
																																		<td style="width: 2px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTRXKKDetailKasKeluar" runat="server" Width="167px" AutoPostBack="True"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																			b. Kas Keluar Dalam valuta asing (Dapat &gt; 1)</td>
																																		<td style="width: 2px">
																																		</td>
																																		<td class="formtext">
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext" colspan="3">
																																			<table>
																																				<tbody>
																																					<tr>
																																						<td class="formtext">
																																							&nbsp; &nbsp;&nbsp;
																																						</td>
																																						<td class="formtext">
																																							i. Mata Uang</td>
																																						<td style="width: 2px">
																																							:</td>
																																						<td class="formtext">
																																							<table style="width: 127px">
																																								<tbody>
																																									<tr>
																																										<td>
																																											<asp:Label ID="txtTRXKKDetilMataUang" runat="server" Width="167px"></asp:Label></td>
																																										<td>
																																										</td>
																																									</tr>
																																								</tbody>
																																							</table>
																																							<asp:HiddenField ID="hfTRXKKDetilMataUang" runat="server"></asp:HiddenField>
																																						</td>
																																					</tr>
																																					<tr>
																																						<td class="formtext">
																																						</td>
																																						<td class="formtext">
																																							ii. Kurs Transaksi</td>
																																						<td style="width: 2px">
																																							:</td>
																																						<td class="formtext">
																																							<asp:Label ID="txtTRXKKDetilKursTrx" runat="server" Width="167px"></asp:Label></td>
																																					</tr>
																																					<tr>
																																						<td class="formtext">
																																						</td>
																																						<td class="formtext">
																																							iii. Jumlah&nbsp;</td>
																																						<td style="width: 2px">
																																							:</td>
																																						<td class="formtext">
																																							<asp:Label ID="txtTRXKKDetilJumlah" runat="server" Width="167px"></asp:Label></td>
																																					</tr>
																																					<tr>
																																						<td class="formtext">
																																						</td>
																																						<td class="formtext" colspan="3">
																																						</td>
																																					</tr>
																																					<tr>
																																						<td class="formtext">
																																						</td>
																																						<td class="formtext" colspan="3">
																																							<asp:GridView ID="grvDetilKasKeluar" runat="server" Width="290px" SkinID="grv2"
																																								AutoGenerateColumns="False">
																																								<Columns>
																																									<asp:BoundField HeaderText="No" />
																																									<asp:BoundField HeaderText="Mata Uang" DataField="MataUang" />
																																									<asp:BoundField HeaderText="Kurs Transaksi" DataField="KursTransaksi" />
																																									<asp:BoundField HeaderText="Jumlah" DataField="Jumlah" />
																																									<asp:BoundField DataField="JumlahRp" HeaderText="Jumlah (Rp)" ItemStyle-HorizontalAlign="Right" />
																																								</Columns>
																																							</asp:GridView>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																			c. Total Kas Keluar (a+b) (Rp)</td>
																																		<td style="width: 2px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="lblDetilKasKeluarJumlahRp" runat="server"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtext">
                                                                                                                            5.4. Nomor Rekening Nasabah</td>
                                                                                                                        <td style="width: 2px">
                                                                                                                            :</td>
                                                                                                                        <td class="formtext">
																															<asp:Label ID="txtTRXKKRekeningKK" runat="server" Width="257px"></asp:Label></td>
                                                                                                                    </tr>
																													<tr>
																														<td class="formtext">
                                                                                                                            5.5. Identitas pihak terkait dengan laporan</td>
																														<td style="width: 2px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td style="height: 41px" class="formtext">
																															&nbsp; &nbsp; &nbsp; Tipe Pelapor</td>
																														<td style="width: 2px; height: 41px">
																															:</td>
																														<td style="height: 41px" class="formtext">
																															<asp:RadioButtonList ID="rblTRXKKTipePelapor" runat="server" AutoPostBack="True"
                                                                                                                                RepeatDirection="Horizontal" Enabled="False">
                                                                                                                                <asp:ListItem Value="1">Perorangan</asp:ListItem>
                                                                                                                                <asp:ListItem Value="2">Korporasi</asp:ListItem>
																															</asp:RadioButtonList></td>
																													</tr>
																												</tbody>
																											</table>
																											<table id="tblTRXKKTipePelaporPerorangan" runat="server">
																												<tbody>
																													<tr>
																														<td class="formtext" colspan="3">
																															<strong>5.5.1. Perorangan</strong>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															a. Gelar</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="txtTRXKKINDVGelar" runat="server"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															b. Nama Lengkap
																														</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="txtTRXKKINDVNamaLengkap" runat="server" Width="370px"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															c. Tempat Lahir</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="txtTRXKKINDVTempatLahir" runat="server" Width="240px"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															d. Tanggal Lahir
																														</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<br />
																															<asp:Label ID="txtTRXKKINDVTglLahir" runat="server" Width="96px" MaxLength="50"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															e. Kewarganegaraan
																														</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:RadioButtonList ID="rblTRXKKINDVKewarganegaraan" runat="server" Enabled="False"
																																AutoPostBack="True" RepeatDirection="Horizontal">
																																<asp:ListItem Selected="True" Value="1">WNI</asp:ListItem>
																																<asp:ListItem Value="2">WNA</asp:ListItem>
																															</asp:RadioButtonList></td>
																													</tr>
																													<tr style="color: #cc0000">
																														<td class="formtext">
																															f. Negara
																														</td>
																														<td style="width: 5px; color: #000000">
																															:</td>
																														<td class="formtext">
																															<asp:DropDownList ID="cboTRXKKINDVNegara" runat="server" CssClass="combobox" Enabled="False">
																															</asp:DropDownList></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															g. Alamat Domisili</td>
																														<td style="width: 5px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formtext">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td class="formtext">
																																			Nama Jalan</td>
																																		<td style="width: 5px">
																																			:
																																		</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTRXKKINDVDOMNamaJalan" runat="server" Width="370px"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			RT/RW</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTRXKKINDVDOMRTRW" runat="server"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kelurahan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext" valign="top">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTRXKKINDVDOMKelurahan" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTRXKKINDVDOMKelurahan" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kecamatan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			&nbsp;<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTRXKKINDVDOMKecamatan" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTRXKKINDVDOMKecamatan" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kota / Kabupaten</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTRXKKINDVDOMKotaKab" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTRXKKINDVDOMKotaKab" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Provinsi</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTRXKKINDVDOMProvinsi" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTRXKKINDVDOMProvinsi" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
                                                                                                                                    <tr>
                                                                                                                                        <td class="formtext">
                                                                                                                                            &nbsp;</td>
                                                                                                                                        <td class="formText">
                                                                                                                                            Negara</td>
                                                                                                                                        <td style="width: 5px">
                                                                                                                                            :</td>
                                                                                                                                        <td class="formtext">
                                                                                                                                            <table style="width: 127px">
                                                                                                                                                <tbody>
                                                                                                                                                    <tr>
                                                                                                                                                        <td>
                                                                                                                                                            <asp:Label ID="txtTRXKKINDVDOMNegara" runat="server" Width="167px"></asp:Label>
                                                                                                                                                        </td>
                                                                                                                                                        <td>
                                                                                                                                                        </td>
                                                                                                                                                    </tr>
                                                                                                                                                </tbody>
                                                                                                                                            </table>
                                                                                                                                            <asp:HiddenField ID="hfTRXKKINDVDOMNegara" runat="server" />
                                                                                                                                        </td>
                                                                                                                                    </tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kode Pos</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTRXKKINDVDOMKodePos" runat="server"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															h. Alamat Sesuai Bukti Identitas</td>
																														<td style="width: 5px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formtext">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td class="formtext">
																																			Nama Jalan</td>
																																		<td style="width: 5px">
																																			:
																																		</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTRXKKINDVIDNamaJalan" runat="server" Width="370px"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			RT/RW</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTRXKKINDVIDRTRW" runat="server"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kelurahan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext" valign="top">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTRXKKINDVIDKelurahan" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTRXKKINDVIDKelurahan" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kecamatan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			&nbsp;<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTRXKKINDVIDKecamatan" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTRXKKINDVIDKecamatan" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kota / Kabupaten</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTRXKKINDVIDKotaKab" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTRXKKINDVIDKotaKab" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Provinsi</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTRXKKINDVIDProvinsi" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTRXKKINDVIDProvinsi" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
                                                                                                                                    <tr>
                                                                                                                                        <td class="formtext">
                                                                                                                                            &nbsp;</td>
                                                                                                                                        <td class="formText">
                                                                                                                                            Negara</td>
                                                                                                                                        <td style="width: 5px">
                                                                                                                                            :</td>
                                                                                                                                        <td class="formtext">
                                                                                                                                            <table style="width: 127px">
                                                                                                                                                <tbody>
                                                                                                                                                    <tr>
                                                                                                                                                        <td>
                                                                                                                                                            <asp:Label ID="txtTRXKKINDVIDNegara" runat="server" Width="167px"></asp:Label>
                                                                                                                                                        </td>
                                                                                                                                                        <td>
                                                                                                                                                        </td>
                                                                                                                                                    </tr>
                                                                                                                                                </tbody>
                                                                                                                                            </table>
                                                                                                                                            <asp:HiddenField ID="hfTRXKKINDVIDNegara" runat="server" />
                                                                                                                                        </td>
                                                                                                                                    </tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kode Pos</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTRXKKINDVIDKodePos" runat="server"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															i. Alamat Sesuai Negara Asal</td>
																														<td style="width: 5px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formtext">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td class="formtext">
																																			Nama Jalan</td>
																																		<td style="width: 5px">
																																			:
																																		</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTRXKKINDVNANamaJalan" runat="server" Width="370px"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Negara</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTRXKKINDVNANegara" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTRXKKINDVNANegara" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Provinsi</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext" valign="top">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTRXKKINDVNAProvinsi" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTRXKKINDVNAProvinsi" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kota
																																		</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTRXKKINDVNAKota" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTRXKKINDVNAKota" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kode Pos</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTRXKKINDVNAKodePos" runat="server"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															j. Jenis Dokumen Identitas</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<ajax:AjaxPanel ID="AjaxPanel3" runat="server">
																																<asp:DropDownList runat="server" ID="cboTRXKKINDVJenisID" CssClass="combobox" Enabled="False">
																																</asp:DropDownList>
																															</ajax:AjaxPanel>
																														</td>
																													</tr>
																													<tr>
																														<td id="Td2" class="formtext" colspan="3" runat="server">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formtext">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td class="formtext">
																																			 Nomor Identitas </td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td style="width: 260px" class="formtext">
																																			<asp:Label ID="txtTRXKKINDVNomorId" runat="server" Width="257px"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															k. Nomor Pokok Wajib Pajak (NPWP)</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="txtTRXKKINDVNPWP" runat="server" Width="257px"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															l. Pekerjaan</td>
																														<td style="width: 5px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formtext">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td class="formtext">
																																			Pekerjaan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td style="width: 260px" class="formtext">
																																			<asp:Label ID="txtTRXKKINDVPekerjaan" runat="server" Width="257px"></asp:Label>
																																			&nbsp;<asp:HiddenField ID="hfTRXKKINDVPekerjaan" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Jabatan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td style="width: 260px" class="formtext">
																																			<asp:Label ID="txtTRXKKINDVJabatan" runat="server" Width="257px"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Penghasilan rata-rata/th (Rp)</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td style="width: 260px" class="formtext">
																																			<asp:Label ID="txtTRXKKINDVPenghasilanRataRata" runat="server" Width="257px"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Tempat kerja</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td style="width: 260px" class="formtext">
																																			<asp:Label ID="txtTRXKKINDVTempatKerja" runat="server" Width="257px"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															m. Tujuan Transaksi</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="txtTRXKKINDVTujuanTrx" runat="server" Width="257px"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															n. Sumber Dana</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="txtTRXKKINDVSumberDana" runat="server" Width="257px"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															o. Rekening lain yang terkait dengan transaksi (apabila ada)</td>
																														<td style="width: 5px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formtext">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td class="formtext">
																																			ii. Nama Bank Lain</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTRXKKINDVNamaBankLain" runat="server" Width="257px"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			ii. Nomor Rekening Tujuan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTRXKKINDVNoRekTujuan" runat="server" Width="257px"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																												</tbody>
																											</table>
																											<table id="tblTRXKKTipePelaporKorporasi" runat="server">
																												<tbody>
																													<tr>
																														<td class="formtext" colspan="3">
																															<strong>5.5.2. Korporasi</strong></td>
																													</tr>
																													<tr>
																														<td style="height: 28px" class="formtext">
																															a. Bentuk Badan Usaha</td>
																														<td style="width: 5px; height: 28px">
																															:</td>
																														<td style="height: 28px" class="formtext">
																															&nbsp;&nbsp;
																															<asp:DropDownList ID="cboTRXKKCORPBentukBadanUsaha" runat="server" Enabled="False">
																															</asp:DropDownList></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															b. Nama Korporasi
																														</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="txtTRXKKCORPNama" runat="server" Width="370px"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															c. Bidang Usaha Korporasi</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<table style="width: 127px">
																																<tbody>
																																	<tr>
																																		<td>
																																			<asp:Label ID="txtTRXKKCORPBidangUsaha" runat="server" Width="167px"></asp:Label></td>
																																		<td>
																																		</td>
																																	</tr>
																																</tbody>
																															</table>
																															<asp:HiddenField ID="hfTRXKKCORPBidangUsaha" runat="server"></asp:HiddenField>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															d. Alamat Korporasi</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:RadioButtonList ID="rblTRXKKCORPTipeAlamat" runat="server" Enabled="False" AutoPostBack="True"
																																RepeatDirection="Horizontal">
																																<asp:ListItem Selected="True">Dalam Negeri</asp:ListItem>
																																<asp:ListItem>Luar Negeri</asp:ListItem>
																															</asp:RadioButtonList></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															e. Alamat Lengkap Korporasi</td>
																														<td style="width: 5px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formtext">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td class="formtext">
																																			Nama Jalan</td>
																																		<td style="width: 5px">
																																			:
																																		</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTRXKKCORPDLNamaJalan" runat="server" Width="370px"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			RT/RW</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTRXKKCORPDLRTRW" runat="server"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kelurahan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext" valign="top">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTRXKKCORPDLKelurahan" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTRXKKCORPDLKelurahan" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kecamatan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			&nbsp;<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTRXKKCORPDLKecamatan" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTRXKKCORPDLKecamatan" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kota / Kabupaten</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTRXKKCORPDLKotaKab" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTRXKKCORPDLKotaKab" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Provinsi</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTRXKKCORPDLProvinsi" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTRXKKCORPDLProvinsi" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Negara</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTRXKKCORPDLNegara" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTRXKKCORPDLNegara" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kode Pos</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTRXKKCORPDLKodePos" runat="server"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															i. Alamat Korporasi Luar Negeri</td>
																														<td style="width: 5px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formtext">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td class="formtext">
																																			Nama Jalan</td>
																																		<td style="width: 5px">
																																			:
																																		</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTRXKKCORPLNNamaJalan" runat="server" Width="370px"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Negara</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTRXKKCORPLNNegara" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTRXKKCORPLNNegara" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Provinsi</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext" valign="top">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTRXKKCORPLNProvinsi" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTRXKKCORPLNProvinsi" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kota
																																		</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="txtTRXKKCORPLNKota" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="hfTRXKKCORPLNKota" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kode Pos</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTRXKKCORPLNKodePos" runat="server"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															k. Nomor Pokok Wajib Pajak (NPWP)</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="txtTRXKKCORPNPWP" runat="server" Width="257px"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															l. Tujuan Transaksi</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="txtTRXKKCORPTujuanTrx" runat="server" Width="257px"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															m. Sumber Dana</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="txtTRXKKCORPSumberDana" runat="server" Width="257px"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															o. Rekening lain yang terkait dengan transaksi (apabila ada)</td>
																														<td style="width: 5px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formtext">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td class="formtext">
																																			ii. Nama Bank Lain</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTRXKKCORPNamaBankLain" runat="server" Width="257px"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			ii. Nomor Rekening Tujuan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="txtTRXKKCORPNoRekeningTujuan" runat="server" Width="257px"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																												</tbody>
																											</table>
																										</asp:View>
																									</asp:MultiView>
																									<table>
																										<tbody>
																											<tr>
																												<td bgcolor="#ffffff">
																													&nbsp;&nbsp;
																												</td>
																												<td style="width: 76px" bgcolor="#ffffff">
																													&nbsp;
																												</td>
																											</tr>
																										</tbody>
																									</table>
																									<table>
																										<tbody>
																											<tr>
																												<td colspan="3">
																													<asp:GridView ID="grvTransaksi" runat="server" SkinID="grv2" AutoGenerateColumns="False">
																														<Columns>
																															<asp:BoundField HeaderText="No" />
																															<asp:BoundField HeaderText="Kas" DataField="Kas" />
                                                                                                                            <asp:BoundField HeaderText="Transaction Date" DataField="TransactionDate" 
                                                                                                                                DataFormatString="{0:dd-MMM-yyyy}" />
																															<asp:BoundField HeaderText="Branch" DataField="Branch" />
																															<asp:BoundField HeaderText="Account No" DataField="AccountNumber" />
																															<asp:BoundField HeaderText="Customer / LTKT" DataField="Type" Visible="false" />
																															<asp:BoundField HeaderText="Transaction Nominal" DataField="TransactionNominal" ItemStyle-HorizontalAlign="Right" />
																															<asp:TemplateField>
																																<ItemTemplate>
																																	<asp:LinkButton ID="EditResume" runat="server" CausesValidation="False" OnClick="EditResume_Click">View</asp:LinkButton>
																																</ItemTemplate>
																															</asp:TemplateField>
																														</Columns>
																													</asp:GridView>
																												</td>
																											</tr>
																											<tr>
																												<td class="formtext">
																													Total Seluruh Kas Masuk</td>
																												<td style="width: 5px">
																													:</td>
																												<td class="formtext">
																													<asp:Label ID="lblTotalKasMasuk" runat="server"></asp:Label>
																												</td>
																											</tr>
																											<tr>
																												<td style="height: 15px" class="formtext">
																													Total Seluruh Kas Keluar</td>
																												<td style="width: 5px; height: 15px">
																													:</td>
																												<td style="height: 15px" class="formtext">
																													<asp:Label ID="lblTotalKasKeluar" runat="server"></asp:Label>
																												</td>
																											</tr>
																											<tr>
																											<td>
																											<table border="0">
																					<tbody>
																						<tr valign="top">
																							<td style="height: 45px" class="formtext">
																								Sebutkan informasi lainnya yang ada</td>
																							<td style="width: 2px; height: 45px">
																								:</td>
																							<td style="height: 45px" class="formtext">
																								<asp:Label ID="txtLTKTInfoLainnya" runat="server" Width="257px" Height="46px"></asp:Label></td>
																						</tr>
																					</tbody>
																				</table>
																											</td>
																											</tr>
																										</tbody>
																									</table>
																								</td>
																							</tr>
																						</tbody>
																					</table>
																				</ajax:AjaxPanel>
																			</td>
																		</tr>
																	</tbody>
																</table>
															</asp:Panel>
														</td>
														<td>
															<asp:Panel ID="OldPanel" runat="server">
																<table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
																	border="0">
																	<tbody>
																		<tr>
																			<td style="height: 6px" valign="middle" align="left" width="100%" background="Images/search-bar-background.gif">
																				<table cellspacing="0" cellpadding="0" border="0">
																					<tbody>
																						<tr>
																							<td class="formtext">
																								<asp:Label ID="old_Label8" runat="server" Text="A. UMUM" Font-Bold="True"></asp:Label>&nbsp;</td>
																							<td>
																								<a title="click to minimize or maximize" onclick="javascript:ShowHidePanel('old_Umum','searchimage2','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
																									href="#">
																									<img id="old_searchimage2" height="12" src="Images/search-bar-minimize.gif" width="12"
																										border="0" /></a></td>
																						</tr>
																					</tbody>
																				</table>
																			</td>
																		</tr>
																		<tr id="old_Umum">
																			<td style="height: 10px" bgcolor="#ffffff" colspan="2">
																				<table>
																					<tbody>
																						<tr>
																							<td class="formtext">
																								<asp:Label ID="old_Label4" runat="server" Text="1. PIHAK PELAPOR" Font-Bold="True"></asp:Label>&nbsp;
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								<table>
																									<tbody>
																										<tr>
																											<td style="width: 5px">
																											</td>
																											<td class="formText">
																												1.1. Nama PJK Pelapor <span style="color: #cc0000">*</span>
																											</td>
																											<td style="width: 5px">
																												:
																											</td>
																											<td class="formtext">
																												<asp:Label ID="old_txtUmumPJKPelapor" runat="server" Width="240px"></asp:Label>
																												&nbsp;
																											</td>
																										</tr>
																										<tr>
																											<td style="width: 5px">
																											</td>
																											<td class="formText">
																												1.2. Tanggal Pelaporan <span style="color: #cc0000">*</span>
																											</td>
																											<td style="width: 5px">
																												:
																											</td>
																											<td class="formtext">
																												<asp:Label ID="old_txtTglLaporan" runat="server" Width="96px" MaxLength="50"></asp:Label>
																											</td>
																										</tr>
																										<tr>
																											<td style="width: 5px">
																											</td>
																											<td class="formText">
																												1.3. Nama Pejabat PJK Pelapor <span style="color: #cc0000">*</span>
																											</td>
																											<td style="width: 5px">
																												:
																											</td>
																											<td class="formtext">
																												<asp:Label ID="old_txtPejabatPelapor" runat="server" Width="240px"></asp:Label>
																											</td>
																										</tr>
																									</tbody>
																								</table>
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								<asp:Label ID="old_Label5" runat="server" Text="2. JENIS LAPORAN (PILIH SALAH SATU)"
																									Font-Bold="True"></asp:Label>&nbsp;
																							</td>
																						</tr>
																						<tr>
																							<td class="formtext">
																								<ajax:AjaxPanel ID="old_pnlUpdate" runat="server">
																									<table>
																										<tbody>
																											<tr>
																												<td style="width: 5px">
																												</td>
																												<td class="formText">
																													2.1. Tipe Laporan <span style="color: #cc0000">*</span>
																												</td>
																												<td style="width: 5px">
																													:
																												</td>
																												<td class="formtext">
																													<asp:DropDownList ID="old_cboTipeLaporan" runat="server" CssClass="combobox" Enabled="False"
																														AutoPostBack="True">
																														<asp:ListItem Value="0">Laporan Baru</asp:ListItem>
																														<asp:ListItem Value="1">Laporan Koreksi</asp:ListItem>
																													</asp:DropDownList>
																												</td>
																											</tr>
																											<tr>
																												<td style="width: 5px">
																												</td>
																												<td class="formtext">
																												</td>
																												<td style="width: 5px">
																													&nbsp;</td>
																												<td class="formtext">
																													<table id="old_tblLTKTKoreksi" runat="server" visible="False">
																														<tbody>
																															<tr>
																																<td class="formText">
																																	2.1.1. No. LTKT yang dikoreksi <span style="color: #cc0000">*</span>
																																</td>
																																<td style="width: 5px">
																																	:
																																</td>
																																<td class="formtext">
																																	<asp:Label ID="old_txtNoLTKTKoreksi" runat="server"></asp:Label>
																																	&nbsp;
																																</td>
																															</tr>
																														</tbody>
																													</table>
																												</td>
																											</tr>
																										</tbody>
																									</table>
																								</ajax:AjaxPanel>
																							</td>
																						</tr>
																					</tbody>
																				</table>
																				<table border="0">
																					<tbody>
																						<tr valign="top">
                                                                        <td class="formtext" style="height: 45px">
                                                                            Debet/Credit</td>
                                                                        <td style="width: 2px; height: 45px">
                                                                            :</td>
                                                                        <td class="formtext" style="height: 45px">
                                                                            <ajax:AjaxPanel ID="AjaxPanel6" runat="server">
                                                                                <asp:DropDownList runat="server" ID="Old_cboDebitCredit" CssClass="combobox" Enabled="False">
                                                                                    <asp:ListItem Value="D">Debet</asp:ListItem>
                                                                                    <asp:ListItem Value="C">Credit</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </ajax:AjaxPanel>
                                                                        </td>
                                                                    </tr>
																					</tbody>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td style="height: 6px" valign="middle" align="left" width="100%" background="Images/search-bar-background.gif">
																				<table cellspacing="0" cellpadding="0" border="0">
																					<tbody>
																						<tr>
																							<td class="formtext">
																								<asp:Label ID="old_Label2" runat="server" Text="B. IDENTITAS TERLAPOR" Font-Bold="True"></asp:Label>&nbsp;</td>
																							<td>
																								<a title="click to minimize or maximize" onclick="javascript:ShowHidePanel('old_IdTerlapor','old_Img1','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
																									href="#">
																									<img id="old_Img1" height="12" src="Images/search-bar-minimize.gif" width="12" border="0" /></a></td>
																						</tr>
																					</tbody>
																				</table>
																			</td>
																		</tr>
																		<tr id="old_IdTerlapor">
																			<td bgcolor="#ffffff" colspan="2">
																				<ajax:AjaxPanel ID="old_ajxPnlTerlapor" runat="server" Width="100%">
																					<table width="100%">
																						<tbody>
																							<tr>
																								<td class="formtext">
																									<asp:Label ID="old_Label6" runat="server" Text="3. TERLAPOR" Font-Bold="True"></asp:Label>&nbsp;
																								</td>
																							</tr>
																							<tr>
																								<td class="formtext">
																									<table>
																										<tbody>
																											<tr>
																												<td style="width: 5px">
																												</td>
																												<td class="formtext">
																													<strong>3.1. Kepemilikan</strong> <span style="color: #cc0000">*</span>
																												</td>
																												<td style="width: 5px">
																													:
																												</td>
																												<td class="formtext">
																													<asp:DropDownList ID="old_cboTerlaporKepemilikan" runat="server" CssClass="combobox"
																														Enabled="False">
																													</asp:DropDownList></td>
																											</tr>
																											<tr>
																												<td style="width: 5px">
																												</td>
																												<td class="formtext">
																												</td>
																												<td style="width: 5px">
																												</td>
																												<td class="formtext">
																													<table width="100%">
																														<tbody>
																															<tr>
																																<td style="width: 139px" class="formText">
																																	<strong>3.1.1. No. Rekening</strong> <span style="color: #cc0000">*</span>&nbsp;</td>
																																<td style="width: 5px">
																																	:
																																</td>
																																<td class="formtext">
																																	<asp:Label ID="old_txtTerlaporNoRekening" runat="server" Width="258px"></asp:Label>
																																</td>
																															</tr>
																															<tr>
																																<td class="formText">
																																	CIF No.</td>
																																<td class="formText">
																																	:</td>
																																<td class="formText">
																																	<asp:Label ID="old_txtCIFNo" runat="server"></asp:Label></td>
																															</tr>
																															<tr>
																																<td style="width: 139px" class="formText">
																																	Tipe Pelapor</td>
																																<td style="width: 5px">
																																	:</td>
																																<td class="formtext">
																																	<asp:RadioButtonList ID="old_rblTerlaporTipePelapor" runat="server" Enabled="False"
																																		AutoPostBack="True" RepeatDirection="Horizontal">
																																		<asp:ListItem Value="1">Perorangan</asp:ListItem>
																																		<asp:ListItem Value="2">Korporasi</asp:ListItem>
																																	</asp:RadioButtonList>
																																</td>
																															</tr>
																															<tr>
																																<td class="formtext" colspan="3">
																																	<div id="old_divPerorangan" runat="server" hidden="false">
																																		<table>
																																			<tbody>
																																				<tr>
																																					<td class="formtext" colspan="3">
																																						<strong>3.1.2. Perorangan</strong>
																																					</td>
																																				</tr>
																																				<tr>
																																					<td class="formText">
																																						a. Gelar</td>
																																					<td style="width: 5px">
																																						:</td>
																																					<td class="formtext">
																																						<asp:Label ID="old_txtTerlaporGelar" runat="server"></asp:Label></td>
																																				</tr>
																																				<tr>
																																					<td class="formText">
																																						b. Nama Lengkap <span style="color: #cc0000">*</span></td>
																																					<td style="width: 5px">
																																						:</td>
																																					<td class="formtext">
																																						<asp:Label ID="old_txtTerlaporNamaLengkap" runat="server" Width="370px"></asp:Label></td>
																																				</tr>
																																				<tr>
																																					<td class="formText">
																																						c. Tempat Lahir</td>
																																					<td style="width: 5px">
																																						:</td>
																																					<td class="formtext">
																																						<asp:Label ID="old_txtTerlaporTempatLahir" runat="server" Width="240px"></asp:Label></td>
																																				</tr>
																																				<tr>
																																					<td class="formText">
																																						d. Tanggal Lahir <span style="color: #cc0000">*</span></td>
																																					<td style="width: 5px">
																																						:</td>
																																					<td class="formtext">
																																						<br />
																																						<asp:Label ID="old_txtTerlaporTglLahir" runat="server" Width="96px" MaxLength="50"></asp:Label></td>
																																				</tr>
																																				<tr>
																																					<td class="formText">
																																						e. Kewarganegaraan (Pilih salah satu) <span style="color: #cc0000">*</span></td>
																																					<td style="width: 5px">
																																						:</td>
																																					<td class="formtext">
																																						<asp:RadioButtonList ID="old_rblTerlaporKewarganegaraan" runat="server" Enabled="False"
																																							AutoPostBack="True" RepeatDirection="Horizontal">
																																							<asp:ListItem Value="1">WNI</asp:ListItem>
																																							<asp:ListItem Value="2">WNA</asp:ListItem>
																																						</asp:RadioButtonList></td>
																																				</tr>
																																				<tr>
																																					<td class="formText">
																																						f. Negara <span style="color: #cc0000">*</span></td>
																																					<td style="width: 5px">
																																						:</td>
																																					<td class="formtext">
																																						<asp:DropDownList ID="old_cboTerlaporNegara" runat="server" CssClass="combobox" Enabled="False">
																																						</asp:DropDownList></td>
																																				</tr>
																																				<tr>
																																					<td class="formText">
																																						g. Alamat Domisili</td>
																																					<td style="width: 5px">
																																					</td>
																																					<td class="formtext">
																																					</td>
																																				</tr>
																																				<tr>
																																					<td class="formtext" colspan="3">
																																						<table>
																																							<tbody>
																																								<tr>
																																									<td class="formtext">
																																										&nbsp; &nbsp;&nbsp;
																																									</td>
																																									<td class="formText">
																																										Nama Jalan</td>
																																									<td style="width: 5px">
																																										:
																																									</td>
																																									<td class="formtext">
																																										<asp:Label ID="old_txtTerlaporDOMNamaJalan" runat="server" Width="370px"></asp:Label></td>
																																								</tr>
																																								<tr>
																																									<td class="formtext">
																																									</td>
																																									<td class="formText">
																																										RT/RW</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										<asp:Label ID="old_txtTerlaporDOMRTRW" runat="server"></asp:Label></td>
																																								</tr>
																																								<tr>
																																									<td class="formtext">
																																									</td>
																																									<td class="formText">
																																										Kelurahan</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext" valign="top">
																																										<table style="width: 127px">
																																											<tbody>
																																												<tr>
																																													<td>
																																														<asp:Label ID="old_txtTerlaporDOMKelurahan" runat="server" Width="167px"></asp:Label></td>
																																													<td>
																																													</td>
																																												</tr>
																																											</tbody>
																																										</table>
																																										<asp:HiddenField ID="old_hfTerlaporDOMKelurahan" runat="server"></asp:HiddenField>
																																									</td>
																																								</tr>
																																								<tr>
																																									<td class="formtext">
																																									</td>
																																									<td class="formText">
																																										Kecamatan</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										&nbsp;<table style="width: 127px">
																																											<tbody>
																																												<tr>
																																													<td>
																																														<asp:Label ID="old_txtTerlaporDOMKecamatan" runat="server" Width="167px"></asp:Label></td>
																																													<td>
																																													</td>
																																												</tr>
																																											</tbody>
																																										</table>
																																										<asp:HiddenField ID="old_hfTerlaporDOMKecamatan" runat="server"></asp:HiddenField>
																																									</td>
																																								</tr>
																																								<tr>
																																									<td class="formtext">
																																									</td>
																																									<td class="formText">
																																										Kota / Kabupaten<span style="color: #cc0000">*</span></td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										<table style="width: 127px">
																																											<tbody>
																																												<tr>
																																													<td>
																																														<asp:Label ID="old_txtTerlaporDOMKotaKab" runat="server" Width="167px"></asp:Label></td>
																																													<td>
																																													</td>
																																												</tr>
																																											</tbody>
																																										</table>
																																										<asp:HiddenField ID="old_hfTerlaporDOMKotaKab" runat="server"></asp:HiddenField>
																																									</td>
																																								</tr>
																																						
																																								<tr>
																																									<td class="formtext">
																																									</td>
																																									<td class="formText">
																																										Provinsi</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										<table style="width: 127px">
																																											<tbody>
																																												<tr>
																																													<td>
																																														<asp:Label ID="old_txtTerlaporDOMProvinsi" runat="server" Width="167px"></asp:Label></td>
																																													<td>
																																													</td>
																																												</tr>
																																											</tbody>
																																										</table>
																																										<asp:HiddenField ID="old_hfTerlaporDOMProvinsi" runat="server"></asp:HiddenField>
																																									</td>
																																								</tr>
                                                                                                                                                                <tr>
                                                                                                                                                                    <td class="formtext">
                                                                                                                                                                        &nbsp;</td>
                                                                                                                                                                    <td class="formText">
                                                                                                                                                                        Negara</td>
                                                                                                                                                                    <td style="width: 5px">
                                                                                                                                                                        :</td>
                                                                                                                                                                    <td class="formtext">
                                                                                                                                                                        <table style="width: 127px">
                                                                                                                                                                            <tbody>
                                                                                                                                                                                <tr>
                                                                                                                                                                                    <td>
                                                                                                                                                                                        <asp:Label ID="old_txtTerlaporDOMNegara" runat="server" Width="167px"></asp:Label>
                                                                                                                                                                                    </td>
                                                                                                                                                                                    <td>
                                                                                                                                                                                    </td>
                                                                                                                                                                                </tr>
                                                                                                                                                                            </tbody>
                                                                                                                                                                        </table>
                                                                                                                                                                        <asp:HiddenField ID="old_hfTerlaporDOMNegara" runat="server" />
                                                                                                                                                                    </td>
                                                                                                                                                                </tr>
																																										<tr>
																																									<td class="formtext">
																																									</td>
																																									<td class="formText">
																																										Kode Pos</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										<asp:Label ID="old_txtTerlaporDOMKodePos" runat="server"></asp:Label></td>
																																								</tr>
																																							</tbody>
																																						</table>
																																					</td>
																																				</tr>
																																				<tr>
																																					<td class="formText">
																																						h. Alamat Sesuai Bukti Identitas</td>
																																					<td style="width: 5px">
																																					</td>
																																					<td class="formtext">
																																					</td>
																																				</tr>
																																				<tr>
																																					<td class="formtext" colspan="3">
																																						<table>
																																							<tbody>
																																								<tr>
																																									<td class="formText">
																																										&nbsp; &nbsp;&nbsp;
																																									</td>
																																									<td class="formText">
																																										Nama Jalan</td>
																																									<td style="width: 5px">
																																										:
																																									</td>
																																									<td class="formtext">
																																										<asp:Label ID="old_txtTerlaporIDNamaJalan" runat="server" Width="370px"></asp:Label></td>
																																								</tr>
																																								<tr>
																																									<td class="formText">
																																									</td>
																																									<td class="formText">
																																										RT/RW</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										<asp:Label ID="old_txtTerlaporIDRTRW" runat="server"></asp:Label></td>
																																								</tr>
																																								<tr>
																																									<td class="formText">
																																									</td>
																																									<td class="formText">
																																										Kelurahan</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext" valign="top">
																																										<table style="width: 127px">
																																											<tbody>
																																												<tr>
																																													<td>
																																														<asp:Label ID="old_txtTerlaporIDKelurahan" runat="server" Width="167px"></asp:Label></td>
																																													<td>
																																													</td>
																																												</tr>
																																											</tbody>
																																										</table>
																																										<asp:HiddenField ID="old_hfTerlaporIDKelurahan" runat="server"></asp:HiddenField>
																																									</td>
																																								</tr>
																																								<tr>
																																									<td class="formText">
																																									</td>
																																									<td class="formText">
																																										Kecamatan</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										&nbsp;<table style="width: 127px">
																																											<tbody>
																																												<tr>
																																													<td>
																																														<asp:Label ID="old_txtTerlaporIDKecamatan" runat="server" Width="167px"></asp:Label></td>
																																													<td>
																																													</td>
																																												</tr>
																																											</tbody>
																																										</table>
																																										<asp:HiddenField ID="old_hfTerlaporIDKecamatan" runat="server"></asp:HiddenField>
																																									</td>
																																								</tr>
																																								<tr>
																																									<td class="formText">
																																									</td>
																																									<td class="formText">
																																										Kota / Kabupaten<span style="color: #cc0000">*</span></td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										<table style="width: 127px">
																																											<tbody>
																																												<tr>
																																													<td>
																																														<asp:Label ID="old_txtTerlaporIDKotaKab" runat="server" Width="167px"></asp:Label></td>
																																													<td>
																																													</td>
																																												</tr>
																																											</tbody>
																																										</table>
																																										<asp:HiddenField ID="old_hfTerlaporIDKotaKab" runat="server"></asp:HiddenField>
																																									</td>
																																								</tr>
																																								
																																								<tr>
																																									<td class="formText">
																																									</td>
																																									<td class="formText">
																																										Provinsi</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										<table style="width: 127px">
																																											<tbody>
																																												<tr>
																																													<td style="height: 22px">
																																														<asp:Label ID="old_txtTerlaporIDProvinsi" runat="server" Width="167px"></asp:Label></td>
																																													<td style="height: 22px">
																																													</td>
																																												</tr>
																																											</tbody>
																																										</table>
																																										<asp:HiddenField ID="old_hfTerlaporIDProvinsi" runat="server"></asp:HiddenField>
																																									</td>
																																								</tr>
                                                                                                                                                                <tr>
                                                                                                                                                                    <td class="formtext">
                                                                                                                                                                        &nbsp;</td>
                                                                                                                                                                    <td class="formText">
                                                                                                                                                                        Negara</td>
                                                                                                                                                                    <td style="width: 5px">
                                                                                                                                                                        :</td>
                                                                                                                                                                    <td class="formtext">
                                                                                                                                                                        <table style="width: 127px">
                                                                                                                                                                            <tbody>
                                                                                                                                                                                <tr>
                                                                                                                                                                                    <td>
                                                                                                                                                                                        <asp:Label ID="old_txtTerlaporIDNegara" runat="server" Width="167px"></asp:Label>
                                                                                                                                                                                    </td>
                                                                                                                                                                                    <td>
                                                                                                                                                                                    </td>
                                                                                                                                                                                </tr>
                                                                                                                                                                            </tbody>
                                                                                                                                                                        </table>
                                                                                                                                                                        <asp:HiddenField ID="old_hfTerlaporIDNegara" runat="server" />
                                                                                                                                                                    </td>
                                                                                                                                                                </tr>
																																								<tr>
																																									<td class="formText">
																																									</td>
																																									<td class="formText">
																																										Kode Pos</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										<asp:Label ID="old_txtTerlaporIDKodePos" runat="server"></asp:Label></td>
																																								</tr>
																																							</tbody>
																																						</table>
																																					</td>
																																				</tr>
																																				<tr>
																																					<td class="formText">
																																						i. Alamat Sesuai Negara Asal</td>
																																					<td style="width: 5px">
																																					</td>
																																					<td>
																																					</td>
																																				</tr>
																																				<tr>
																																					<td class="formtext" colspan="3">
																																						<table>
																																							<tbody>
																																								<tr>
																																									<td class="formText">
																																									</td>
																																									<td class="formText">
																																										Nama Jalan</td>
																																									<td style="width: 5px">
																																										:
																																									</td>
																																									<td class="formtext">
																																										<asp:Label ID="old_txtTerlaporNANamaJalan" runat="server" Width="370px"></asp:Label></td>
																																								</tr>
																																								<tr>
																																									<td class="formText">
																																										&nbsp; &nbsp;&nbsp;
																																									</td>
																																									<td class="formText">
																																										Negara</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										<table style="width: 127px">
																																											<tbody>
																																												<tr>
																																													<td>
																																														<asp:Label ID="old_txtTerlaporNANegara" runat="server" Width="167px"></asp:Label></td>
																																													<td>
																																													</td>
																																												</tr>
																																											</tbody>
																																										</table>
																																										<asp:HiddenField ID="old_hfTerlaporNANegara" runat="server"></asp:HiddenField>
																																									</td>
																																								</tr>
																																								<tr>
																																									<td class="formText">
																																									</td>
																																									<td class="formText">
																																										Provinsi</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext" valign="top">
																																										<table style="width: 127px">
																																											<tbody>
																																												<tr>
																																													<td>
																																														<asp:Label ID="old_txtTerlaporNAProvinsi" runat="server" Width="167px"></asp:Label></td>
																																													<td>
																																													</td>
																																												</tr>
																																											</tbody>
																																										</table>
																																										<asp:HiddenField ID="old_hfTerlaporNAProvinsi" runat="server"></asp:HiddenField>
																																									</td>
																																								</tr>
																																								<tr>
																																									<td class="formText">
																																									</td>
																																									<td class="formText">
																																										Kota
																																									</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										<table style="width: 127px">
																																											<tbody>
																																												<tr>
																																													<td>
																																														<asp:Label ID="old_txtTerlaporNAKota" runat="server" Width="167px"></asp:Label></td>
																																													<td>
																																													</td>
																																												</tr>
																																											</tbody>
																																										</table>
																																										<asp:HiddenField ID="old_hfTerlaporIDKota" runat="server"></asp:HiddenField>
																																									</td>
																																								</tr>
																																								<tr>
																																									<td class="formText">
																																									</td>
																																									<td class="formText">
																																										Kode Pos<span style="color: #cc0000">*</span></td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										<asp:Label ID="old_txtTerlaporNAKodePos" runat="server"></asp:Label></td>
																																								</tr>
																																							</tbody>
																																						</table>
																																					</td>
																																				</tr>
																																				<tr>
																																					<td class="formText">
																																						j. Jenis Dokumen Identitas<span style="color: #cc0000">*</span></td>
																																					<td style="width: 5px">
																																						:</td>
																																					<td class="formtext">
																																						<ajax:AjaxPanel ID="old_ajPnlDokId" runat="server">
																																							<asp:DropDownList runat="server" ID="old_cboTerlaporJenisDocID" CssClass="combobox"
																																								Enabled="False">
																																							</asp:DropDownList>
																																						</ajax:AjaxPanel>
																																					</td>
																																				</tr>
																																				<tr>
																																					<td id="old_tdNomorId" class="formtext" colspan="3" runat="server">
																																						<table>
																																							<tbody>
																																								<tr>
																																									<td class="formText">
																																										 Nomor Identitas <span style="color: #cc0000">*</span></td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td style="width: 260px" class="formtext">
																																										<asp:Label ID="old_txtTerlaporNomorID" runat="server" Width="257px"></asp:Label></td>
																																								</tr>
																																							</tbody>
																																						</table>
																																					</td>
																																				</tr>
																																				<tr>
																																					<td class="formText">
																																						k. Nomor Pokok Wajib Pajak (NPWP)</td>
																																					<td style="width: 5px">
																																						:</td>
																																					<td class="formtext">
																																						<asp:Label ID="old_txtTerlaporNPWP" runat="server" Width="257px"></asp:Label></td>
																																				</tr>
																																				<tr>
																																					<td class="formText">
																																						l. Pekerjaan</td>
																																					<td style="width: 5px">
																																					</td>
																																					<td class="formtext">
																																					</td>
																																				</tr>
																																				<tr>
																																					<td class="formtext" colspan="3">
																																						<table>
																																							<tbody>
																																								<tr>
																																									<td class="formtext">
																																										&nbsp; &nbsp;&nbsp;
																																									</td>
																																									<td class="formText">
																																										Pekerjaan<span style="color: #cc0000">*</span></td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td style="width: 260px" class="formtext">
																																										<asp:Label ID="old_txtTerlaporPekerjaan" runat="server" Width="257px"></asp:Label>
																																										<asp:HiddenField ID="old_hfTerlaporPekerjaan" runat="server"></asp:HiddenField>
																																									</td>
																																								</tr>
																																								<tr>
																																									<td class="formtext">
																																									</td>
																																									<td class="formText">
																																										Jabatan</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td style="width: 260px" class="formtext">
																																										<asp:Label ID="old_txtTerlaporJabatan" runat="server" Width="257px"></asp:Label></td>
																																								</tr>
																																								<tr>
																																									<td class="formtext">
																																									</td>
																																									<td class="formText">
																																										Penghasilan rata-rata/th (Rp)</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td style="width: 260px" class="formtext">
																																										<asp:Label ID="old_txtTerlaporPenghasilanRataRata" runat="server" Width="257px"></asp:Label></td>
																																								</tr>
																																								<tr>
																																									<td class="formtext">
																																									</td>
																																									<td class="formText">
																																										Tempat kerja</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td style="width: 260px" class="formtext">
																																										<asp:Label ID="old_txtTerlaporTempatKerja" runat="server" Width="257px"></asp:Label></td>
																																								</tr>
																																							</tbody>
																																						</table>
																																					</td>
																																				</tr>
																																			</tbody>
																																		</table>
																																	</div>
																																	<div id="old_divKorporasi" runat="server" hidden="true">
																																		<table>
																																			<tbody>
																																				<tr>
																																					<td class="formtext" colspan="3">
																																						<strong>3.1.3. Korporasi</strong></td>
																																				</tr>
																																				<tr>
																																					<td class="formText">
																																						a. Bentuk Badan Usaha<span style="color: #cc0000">*</span></td>
																																					<td style="width: 5px">
																																						:</td>
																																					<td class="formtext">
																																						<asp:DropDownList ID="old_cboTerlaporCORPBentukBadanUsaha" runat="server" Enabled="False">
																																						</asp:DropDownList></td>
																																				</tr>
																																				<tr>
																																					<td class="formText">
																																						b. Nama Korporasi <span style="color: #cc0000">*</span></td>
																																					<td style="width: 5px">
																																						:</td>
																																					<td class="formtext">
																																						<asp:Label ID="old_txtTerlaporCORPNama" runat="server" Width="370px"></asp:Label></td>
																																				</tr>
																																				<tr>
																																					<td class="formText">
																																						c. Bidang Usaha Korporasi<span style="color: #cc0000">*</span></td>
																																					<td style="width: 5px">
																																						:</td>
																																					<td class="formtext">
																																						<table style="width: 127px">
																																							<tbody>
																																								<tr>
																																									<td>
																																										<asp:Label ID="old_txtTerlaporCORPBidangUsaha" runat="server" Width="167px"></asp:Label></td>
																																									<td>
																																									</td>
																																								</tr>
																																							</tbody>
																																						</table>
																																						<asp:HiddenField ID="old_hfTerlaporCORPBidangUsaha" runat="server"></asp:HiddenField>
																																					</td>
																																				</tr>
																																				<tr>
																																					<td class="formText">
																																						d. Alamat Korporasi<span style="color: #cc0000">*</span></td>
																																					<td style="width: 5px">
																																						:</td>
																																					<td class="formtext">
																																						<asp:RadioButtonList ID="old_rblTerlaporCORPTipeAlamat" runat="server" Enabled="False"
																																							AutoPostBack="True" RepeatDirection="Horizontal">
																																							<asp:ListItem>Dalam Negeri</asp:ListItem>
																																							<asp:ListItem>Luar Negeri</asp:ListItem>
																																						</asp:RadioButtonList></td>
																																				</tr>
																																				<tr>
																																					<td class="formText">
																																						e. Alamat Lengkap Korporasi</td>
																																					<td style="width: 5px">
																																					</td>
																																					<td class="formtext">
																																					</td>
																																				</tr>
																																				<tr>
																																					<td class="formtext" colspan="3">
																																						<table>
																																							<tbody>
																																								<tr>
																																									<td class="formtext">
																																										&nbsp; &nbsp;&nbsp;
																																									</td>
																																									<td class="formText">
																																										Nama Jalan</td>
																																									<td style="width: 5px">
																																										:
																																									</td>
																																									<td class="formtext">
																																										<asp:Label ID="old_txtTerlaporCORPDLNamaJalan" runat="server" Width="370px"></asp:Label></td>
																																								</tr>
																																								<tr>
																																									<td class="formtext">
																																									</td>
																																									<td class="formText">
																																										RT/RW</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										<asp:Label ID="old_txtTerlaporCORPDLRTRW" runat="server"></asp:Label></td>
																																								</tr>
																																								<tr>
																																									<td class="formtext">
																																									</td>
																																									<td class="formText">
																																										Kelurahan</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext" valign="top">
																																										<table style="width: 127px">
																																											<tbody>
																																												<tr>
																																													<td>
																																														<asp:Label ID="old_txtTerlaporCORPDLKelurahan" runat="server" Width="167px"></asp:Label></td>
																																													<td>
																																													</td>
																																												</tr>
																																											</tbody>
																																										</table>
																																										<asp:HiddenField ID="old_hfTerlaporCORPDLKelurahan" runat="server"></asp:HiddenField>
																																									</td>
																																								</tr>
																																								<tr>
																																									<td class="formtext">
																																									</td>
																																									<td class="formText">
																																										Kecamatan</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										&nbsp;<table style="width: 127px">
																																											<tbody>
																																												<tr>
																																													<td>
																																														<asp:Label ID="old_txtTerlaporCORPDLKecamatan" runat="server" Width="167px"></asp:Label></td>
																																													<td>
																																													</td>
																																												</tr>
																																											</tbody>
																																										</table>
																																										<asp:HiddenField ID="old_hfTerlaporCORPDLKecamatan" runat="server"></asp:HiddenField>
																																									</td>
																																								</tr>
																																								<tr>
																																									<td class="formtext">
																																									</td>
																																									<td class="formText">
																																										Kota / Kabupaten<span style="color: #cc0000">*</span></td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										<table style="width: 127px">
																																											<tbody>
																																												<tr>
																																													<td>
																																														<asp:Label ID="old_txtTerlaporCORPDLKotaKab" runat="server" Width="167px"></asp:Label></td>
																																													<td>
																																													</td>
																																												</tr>
																																											</tbody>
																																										</table>
																																										<asp:HiddenField ID="old_hfTerlaporCORPDLKotaKab" runat="server"></asp:HiddenField>
																																									</td>
																																								</tr>
																																								
																																								<tr>
																																									<td class="formtext">
																																									</td>
																																									<td class="formText">
																																										Provinsi</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										<table style="width: 127px">
																																											<tbody>
																																												<tr>
																																													<td>
																																														<asp:Label ID="old_txtTerlaporCORPDLProvinsi" runat="server" Width="167px"></asp:Label></td>
																																													<td>
																																													</td>
																																												</tr>
																																											</tbody>
																																										</table>
																																										<asp:HiddenField ID="old_hfTerlaporCORPDLProvinsi" runat="server"></asp:HiddenField>
																																									</td>
																																								</tr>
																																								<tr>
																																									<td class="formtext">
																																									</td>
																																									<td class="formText">
																																										Negara</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										<table style="width: 127px">
																																											<tbody>
																																												<tr>
																																													<td>
																																														<asp:Label ID="old_txtTerlaporCORPDLNegara" runat="server" Width="167px"></asp:Label></td>
																																													<td>
																																													</td>
																																												</tr>
																																											</tbody>
																																										</table>
																																										<asp:HiddenField ID="old_hfTerlaporCORPDLNegara" runat="server"></asp:HiddenField>
																																									</td>
																																								</tr>
																																								<tr>
																																									<td class="formtext">
																																									</td>
																																									<td class="formText">
																																										Kode Pos</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										<asp:Label ID="old_txtTerlaporCORPDLKodePos" runat="server"></asp:Label></td>
																																								</tr>
																																							</tbody>
																																						</table>
																																					</td>
																																				</tr>
																																				<tr>
																																					<td class="formText">
																																						i. Alamat Korporasi Luar Negeri</td>
																																					<td style="width: 5px">
																																					</td>
																																					<td class="formtext">
																																					</td>
																																				</tr>
																																				<tr>
																																					<td class="formtext" colspan="3">
																																						<table>
																																							<tbody>
																																								<tr>
																																									<td class="formtext">
																																										&nbsp; &nbsp;&nbsp;
																																									</td>
																																									<td style="width: 69px" class="formText">
																																										Nama Jalan</td>
																																									<td style="width: 5px">
																																										:
																																									</td>
																																									<td class="formtext">
																																										<asp:Label ID="old_txtTerlaporCORPLNNamaJalan" runat="server" Width="370px"></asp:Label></td>
																																								</tr>
																																								<tr>
																																									<td class="formtext">
																																									</td>
																																									<td style="width: 69px" class="formText">
																																										Negara</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										<table style="width: 127px">
																																											<tbody>
																																												<tr>
																																													<td>
																																														<asp:Label ID="old_txtTerlaporCORPLNNegara" runat="server" Width="167px"></asp:Label></td>
																																													<td>
																																													</td>
																																												</tr>
																																											</tbody>
																																										</table>
																																										<asp:HiddenField ID="old_hfTerlaporCORPLNNegara" runat="server"></asp:HiddenField>
																																									</td>
																																								</tr>
																																								<tr>
																																									<td class="formtext">
																																									</td>
																																									<td style="width: 69px" class="formText">
																																										Provinsi</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext" valign="top">
																																										<table style="width: 127px">
																																											<tbody>
																																												<tr>
																																													<td style="height: 22px">
																																														<asp:Label ID="old_txtTerlaporCORPLNProvinsi" runat="server" Width="167px"></asp:Label></td>
																																													<td style="height: 22px">
																																													</td>
																																												</tr>
																																											</tbody>
																																										</table>
																																										<asp:HiddenField ID="old_hfTerlaporCORPLNProvinsi" runat="server"></asp:HiddenField>
																																									</td>
																																								</tr>
																																								<tr>
																																									<td class="formtext">
																																									</td>
																																									<td style="width: 69px" class="formText">
																																										Kota
																																									</td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										<table style="width: 127px">
																																											<tbody>
																																												<tr>
																																													<td>
																																														<asp:Label ID="old_txtTerlaporCORPLNKota" runat="server" Width="167px"></asp:Label></td>
																																													<td>
																																													</td>
																																												</tr>
																																											</tbody>
																																										</table>
																																										<asp:HiddenField ID="old_hfTerlaporCORPLNKota" runat="server"></asp:HiddenField>
																																									</td>
																																								</tr>
																																								<tr>
																																									<td class="formtext">
																																									</td>
																																									<td style="width: 69px" class="formText">
																																										Kode Pos<span style="color: #cc0000">*</span></td>
																																									<td style="width: 5px">
																																										:</td>
																																									<td class="formtext">
																																										<asp:Label ID="old_txtTerlaporCORPLNKodePos" runat="server"></asp:Label></td>
																																								</tr>
																																							</tbody>
																																						</table>
																																					</td>
																																				</tr>
																																				<tr>
																																					<td class="formText">
																																						k. Nomor Pokok Wajib Pajak (NPWP)</td>
																																					<td style="width: 5px">
																																						:</td>
																																					<td class="formtext">
																																						<asp:Label ID="old_txtTerlaporCORPNPWP" runat="server" Width="257px"></asp:Label></td>
																																				</tr>
																																			</tbody>
																																		</table>
																																	</div>
																																</td>
																															</tr>
																														</tbody>
																													</table>
																												</td>
																											</tr>
																										</tbody>
																									</table>
																								</td>
																							</tr>
																						</tbody>
																					</table>
																				</ajax:AjaxPanel>
																			</td>
																		</tr>
																		<tr>
																			<td style="height: 6px" valign="middle" align="left" width="100%" background="Images/search-bar-background.gif">
																				<table cellspacing="0" cellpadding="0" border="0">
																					<tbody>
																						<tr>
																							<td class="formtext">
																								<asp:Label ID="old_Label3" runat="server" Text="C. TRANSAKSI" Font-Bold="True"></asp:Label>&nbsp;</td>
																							<td>
																								<a title="click to minimize or maximize" onclick="javascript:ShowHidePanel('old_Transaksi','old_Img2','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
																									href="#">
																									<img id="old_Img2" height="12" src="Images/search-bar-minimize.gif" width="12" border="0" /></a></td>
																						</tr>
																					</tbody>
																				</table>
																			</td>
																		</tr>
																		<tr id="old_Transaksi">
																			<td bgcolor="#ffffff" colspan="2">
																				<ajax:AjaxPanel ID="old_AjaxPanel4" runat="server" Width="100%">
																					<table>
																						<tbody>
																							<tr>
																								<td>
																									<asp:Menu ID="old_Menu1" runat="server" CssClass="tabs" StaticEnableDefaultPopOutImage="False"
																										Orientation="Horizontal">
																										<Items>
																											<asp:MenuItem Text="Kas Masuk" Value="0" Selected="True"></asp:MenuItem>
																											<asp:MenuItem Text="Kas Keluar" Value="1"></asp:MenuItem>
																										</Items>
																										<StaticSelectedStyle CssClass="selectedTab" />
																										<StaticMenuItemStyle CssClass="tab" />
																									</asp:Menu>
																									<asp:MultiView ID="old_MultiView1" runat="server" ActiveViewIndex="0">
																										<asp:View ID="old_ViewKasMasuk" runat="server">
																											<table width="100%">
																												<tbody>
																													<tr>
																														<td style="width: 300px" class="formtext">
																															4.1. Tanggal Transaksi (tgl/bln/thn)<span style="color: #cc0000">*</span></td>
																														<td style="width: 2px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="old_txtTRXKMTanggalTrx" runat="server" Width="96px" MaxLength="50"></asp:Label>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															4.2. Nama Kantor PJK tempat terjadinya transaksi</td>
																														<td style="width: 2px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<div>
																																<table>
																																	<tbody>
																																		<tr>
																																			<td class="formtext">
																																				&nbsp; &nbsp;&nbsp;
																																			</td>
																																			<td class="formtext">
																																				a. Nama Kantor<span style="color: #cc0000">*</span></td>
																																			<td style="width: 5px">
																																				:</td>
																																			<td class="formtext">
																																				<asp:Label ID="old_txtTRXKMNamaKantor" runat="server" Width="370px"></asp:Label>
																																				&nbsp;</td>
																																		</tr>
																																		<tr>
																																			<td class="formtext">
																																			</td>
																																			<td class="formtext">
																																				b. Kota Kabupaten<span style="color: #cc0000">*</span></td>
																																			<td style="width: 5px">
																																				:</td>
																																			<td class="formtext">
																																				<table style="width: 127px">
																																					<tbody>
																																						<tr>
																																							<td>
																																								<asp:Label ID="old_txtTRXKMKotaKab" runat="server" Width="167px"></asp:Label></td>
																																							<td>
																																							</td>
																																						</tr>
																																					</tbody>
																																				</table>
																																				<asp:HiddenField ID="old_hfTRXKMKotaKab" runat="server"></asp:HiddenField>
																																				&nbsp;
																																			</td>
																																		</tr>
																																		<tr>
																																			<td class="formtext">
																																			</td>
																																			<td class="formtext">
																																				c. Provinsi<span style="color: #cc0000">*</span></td>
																																			<td style="width: 5px">
																																				:</td>
																																			<td class="formtext">
																																				<table style="width: 251px">
																																					<tbody>
																																						<tr>
																																							<td style="width: 170px; height: 15px">
																																								<asp:Label ID="old_txtTRXKMProvinsi" runat="server" Width="167px"></asp:Label></td>
																																						</tr>
																																					</tbody>
																																				</table>
																																				<asp:HiddenField ID="old_hfTRXKMProvinsi" runat="server"></asp:HiddenField>
																																			</td>
																																		</tr>
																																	</tbody>
																																</table>
																															</div>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															4.3. Detail Kas Masuk</td>
																														<td style="width: 2px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formtext">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td class="formtext">
																																			a. Kas Masuk (Rp)</td>
																																		<td style="width: 2px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="old_txtTRXKMDetilKasMasuk" runat="server" Width="167px" AutoPostBack="True"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			b. Kas Masuk Dalam valuta asing (Dapat &gt; 1)</td>
																																		<td style="width: 2px">
																																		</td>
																																		<td class="formtext">
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext" colspan="1">
																																		</td>
																																		<td class="formtext" colspan="3">
																																			<table>
																																				<tbody>
																																					<tr>
																																						<td class="formtext">
																																							&nbsp; &nbsp;&nbsp;
																																						</td>
																																						<td class="formtext">
																																							i. Mata Uang</td>
																																						<td style="width: 2px">
																																							:</td>
																																						<td class="formtext">
																																							<table style="width: 127px">
																																								<tbody>
																																									<tr>
																																										<td>
																																											<asp:Label ID="old_txtTRXKMDetilMataUang" runat="server" Width="167px"></asp:Label></td>
																																										<td>
																																										</td>
																																									</tr>
																																								</tbody>
																																							</table>
																																							<asp:HiddenField ID="old_hfTRXKMDetilMataUang" runat="server"></asp:HiddenField>
																																						</td>
																																					</tr>
																																					<tr>
																																						<td class="formtext">
																																						</td>
																																						<td class="formtext">
																																							ii. Kurs Transaksi</td>
																																						<td style="width: 2px">
																																							:</td>
																																						<td class="formtext">
																																							<asp:Label ID="old_txtTRXKMDetailKursTrx" runat="server" Width="167px"></asp:Label></td>
																																					</tr>
																																					<tr>
																																						<td class="formtext">
																																						</td>
																																						<td class="formtext">
																																							iii. Jumlah&nbsp;</td>
																																						<td style="width: 2px">
																																							:</td>
																																						<td class="formtext">
																																							<asp:Label ID="old_txtTRXKMDetilJumlah" runat="server" Width="167px"></asp:Label></td>
																																					</tr>
																																					<tr>
																																						<td class="formtext">
																																						</td>
																																						<td class="formtext" colspan="3">
																																						</td>
																																					</tr>
																																					<tr>
																																						<td class="formtext">
																																						</td>
																																						<td class="formtext" colspan="3">
																																							<asp:GridView ID="old_grvTRXKMDetilValutaAsing" runat="server" SkinID="grv2"
																																								AutoGenerateColumns="False">
																																								<Columns>
																																									<asp:BoundField HeaderText="No" />
																																									<asp:BoundField HeaderText="Mata Uang" DataField="MataUang" />
																																									<asp:BoundField HeaderText="Kurs Transaksi" DataField="KursTransaksi" />
																																									<asp:BoundField HeaderText="Jumlah" DataField="Jumlah" />
																																									<asp:BoundField DataField="JumlahRp" HeaderText="Jumlah (Rp)" ItemStyle-HorizontalAlign="Right" />
																																								</Columns>
																																							</asp:GridView>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			c. Total Kas Masuk (a+b) (Rp)</td>
																																		<td style="width: 2px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="old_lblTRXKMDetilValutaAsingJumlahRp" runat="server"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															4.4. Nomor Rekening Nasabah</td>
																														<td style="width: 2px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="old_txtTRXKMNoRekening" runat="server" Width="167px"></asp:Label></td>
																													</tr>
																													<tr>
																														<td style="height: 15px" class="formtext">
																															4.5. Identitas pihak terkait dengan terlapor</td>
																														<td style="width: 2px; height: 15px">
																														</td>
																														<td style="height: 15px" class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															&nbsp; &nbsp; &nbsp; Tipe Terkait</td>
																														<td style="width: 2px">
																															:</td>
																														<td class="formtext">
																															<asp:RadioButtonList ID="old_rblTRXKMTipePelapor" runat="server" AutoPostBack="True"
                                                                                                                                RepeatDirection="Horizontal" Enabled="False">
                                                                                                                                <asp:ListItem Value="1">Perorangan</asp:ListItem>
                                                                                                                                <asp:ListItem Value="2">Korporasi</asp:ListItem>
																															</asp:RadioButtonList></td>
																													</tr>
																												</tbody>
																											</table>
																											<table id="old_tblTRXKMTipePelapor" runat="server">
																												<tbody>
																													<tr>
																														<td class="formtext" colspan="3">
																															<strong>4.5.1. Perorangan</strong>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															a. Gelar</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="old_txtTRXKMINDVGelar" runat="server"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															b. Nama Lengkap
																														</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="old_txtTRXKMINDVNamaLengkap" runat="server" Width="370px"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															c. Tempat Lahir</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="old_txtTRXKMINDVTempatLahir" runat="server" Width="240px"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext" style="height: 28px">
																															d. Tanggal Lahir
																														</td>
																														<td style="width: 5px; height: 28px;">
																															:</td>
																														<td class="formtext" style="height: 28px">
																															<br />
																															<asp:Label ID="old_txtTRXKMINDVTanggalLahir" runat="server" Width="96px" MaxLength="50"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															e. Kewarganegaraan
																														</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:RadioButtonList ID="old_rblTRXKMINDVKewarganegaraan" runat="server" Enabled="False"
																																AutoPostBack="True" RepeatDirection="Horizontal">
																																<asp:ListItem Selected="True" Value="1">WNI</asp:ListItem>
																																<asp:ListItem Value="2">WNA</asp:ListItem>
																															</asp:RadioButtonList></td>
																													</tr>
																													<tr style="color: #cc0000">
																														<td class="formtext">
																															f. Negara
																														</td>
																														<td style="width: 5px; color: #000000">
																															:</td>
																														<td class="formtext">
																															<asp:DropDownList ID="old_cboTRXKMINDVNegara" runat="server" CssClass="combobox"
																																Enabled="False">
																															</asp:DropDownList></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															g. Alamat Domisili</td>
																														<td style="width: 5px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formtext">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td class="formtext">
																																			Nama Jalan</td>
																																		<td style="width: 5px">
																																			:
																																		</td>
																																		<td class="formtext">
																																			<asp:Label ID="old_txtTRXKMINDVDOMNamaJalan" runat="server" Width="370px"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			RT/RW</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="old_txtTRXKMINDVDOMRTRW" runat="server"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kelurahan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext" valign="top">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="old_txtTRXKMINDVDOMKelurahan" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="old_hfTRXKMINDVDOMKelurahan" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kecamatan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			&nbsp;<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="old_txtTRXKMINDVDOMKecamatan" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="old_hfTRXKMINDVDOMKecamatan" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kota / Kabupaten</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="old_txtTRXKMINDVDOMKotaKab" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="old_hfTRXKMINDVDOMKotaKab" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Provinsi</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="old_txtTRXKMINDVDOMProvinsi" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="old_hfTRXKMINDVDOMProvinsi" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
                                                                                                                                    <tr>
                                                                                                                                        <td class="formtext">
                                                                                                                                            &nbsp;</td>
                                                                                                                                        <td class="formText">
                                                                                                                                            Negara</td>
                                                                                                                                        <td style="width: 5px">
                                                                                                                                            :</td>
                                                                                                                                        <td class="formtext">
                                                                                                                                            <table style="width: 127px">
                                                                                                                                                <tbody>
                                                                                                                                                    <tr>
                                                                                                                                                        <td>
                                                                                                                                                            <asp:Label ID="old_txtTRXKMINDVDOMNegara" runat="server" Width="167px"></asp:Label>
                                                                                                                                                        </td>
                                                                                                                                                        <td>
                                                                                                                                                        </td>
                                                                                                                                                    </tr>
                                                                                                                                                </tbody>
                                                                                                                                            </table>
                                                                                                                                            <asp:HiddenField ID="old_hfTRXKMINDVDOMNegara" runat="server" />
                                                                                                                                        </td>
                                                                                                                                    </tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kode Pos</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="old_txtTRXKMINDVDOMKodePos" runat="server"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															h. Alamat Sesuai Bukti Identitas</td>
																														<td style="width: 5px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formtext">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td class="formtext">
																																			Nama Jalan</td>
																																		<td style="width: 5px">
																																			:
																																		</td>
																																		<td class="formtext">
																																			<asp:Label ID="old_txtTRXKMINDVIDNamaJalan" runat="server" Width="370px"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			RT/RW</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="old_txtTRXKMINDVIDRTRW" runat="server"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kelurahan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext" valign="top">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="old_txtTRXKMINDVIDKelurahan" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="old_hfTRXKMINDVIDKelurahan" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kecamatan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			&nbsp;<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td style="height: 23px">
																																							<asp:Label ID="old_txtTRXKMINDVIDKecamatan" runat="server" Width="167px"></asp:Label></td>
																																						<td style="height: 23px">
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="old_hfTRXKMINDVIDKecamatan" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kota / Kabupaten</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="old_txtTRXKMINDVIDKotaKab" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="old_hfTRXKMINDVIDKotaKab" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Provinsi</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="old_txtTRXKMINDVIDProvinsi" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																							&nbsp;
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="old_hfTRXKMINDVIDProvinsi" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
                                                                                                                                    <tr>
                                                                                                                                        <td class="formtext">
                                                                                                                                            &nbsp;</td>
                                                                                                                                        <td class="formText">
                                                                                                                                            Negara</td>
                                                                                                                                        <td style="width: 5px">
                                                                                                                                            :</td>
                                                                                                                                        <td class="formtext">
                                                                                                                                            <table style="width: 127px">
                                                                                                                                                <tbody>
                                                                                                                                                    <tr>
                                                                                                                                                        <td>
                                                                                                                                                            <asp:Label ID="old_txtTRXKMINDVIDNegara" runat="server" Width="167px"></asp:Label>
                                                                                                                                                        </td>
                                                                                                                                                        <td>
                                                                                                                                                        </td>
                                                                                                                                                    </tr>
                                                                                                                                                </tbody>
                                                                                                                                            </table>
                                                                                                                                            <asp:HiddenField ID="old_hfTRXKMINDVIDNegara" runat="server" />
                                                                                                                                        </td>
                                                                                                                                    </tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kode Pos</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="old_txtTRXKMINDVIDKodePos" runat="server"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															i. Alamat Sesuai Negara Asal</td>
																														<td style="width: 5px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formtext">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td class="formtext">
																																			Nama Jalan</td>
																																		<td style="width: 5px">
																																			:
																																		</td>
																																		<td class="formtext">
																																			<asp:Label ID="old_txtTRXKMINDVNANamaJalan" runat="server" Width="370px"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Negara</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td style="height: 22px">
																																							<asp:Label ID="old_txtTRXKMINDVNANegara" runat="server" Width="167px"></asp:Label></td>
																																						<td style="height: 22px">
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="old_hfTRXKMINDVNANegara" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Provinsi</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext" valign="top">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="old_txtTRXKMINDVNAProvinsi" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="old_hfTRXKMINDVNAProvinsi" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kota
																																		</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="old_txtTRXKMINDVNAKota" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="old_hfTRXKMINDVNAKota" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kode Pos</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="old_txtTRXKMINDVNAKodePos" runat="server"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															j. Jenis Dokumen Identitas</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<ajax:AjaxPanel ID="old_AjaxPanel2" runat="server">
																																<asp:DropDownList runat="server" ID="old_cboTRXKMINDVJenisID" CssClass="combobox"
																																	Enabled="False">
																																</asp:DropDownList>
																															</ajax:AjaxPanel>
																														</td>
																													</tr>
																													<tr>
																														<td id="old_Td1" class="formtext" colspan="3" runat="server">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formtext">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td class="formtext">
																																			 Nomor Identitas <span style="color: #cc0000">*</span></td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td style="width: 260px" class="formtext">
																																			<asp:Label ID="old_txtTRXKMINDVNomorID" runat="server" Width="257px"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															k. Nomor Pokok Wajib Pajak (NPWP)</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="old_txtTRXKMINDVNPWP" runat="server" Width="257px"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															l. Pekerjaan</td>
																														<td style="width: 5px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<table>
																																<tbody>
																																	<tr>
																																		<td style="height: 40px" class="formtext">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td style="height: 40px" class="formtext">
																																			Pekerjaan</td>
																																		<td style="width: 5px; height: 40px">
																																			:</td>
																																		<td style="width: 260px; height: 40px" class="formtext">
																																			<asp:Label ID="old_txtTRXKMINDVPekerjaan" runat="server" Width="257px"></asp:Label>
																																			<asp:HiddenField ID="old_hfTRXKMINDVPekerjaan" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Jabatan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td style="width: 260px" class="formtext">
																																			<asp:Label ID="old_txtTRXKMINDVJabatan" runat="server" Width="257px"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Penghasilan rata-rata/th (Rp)</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td style="width: 260px" class="formtext">
																																			<asp:Label ID="old_txtTRXKMINDVPenghasilanRataRata" runat="server" Width="257px"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Tempat kerja</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td style="width: 260px" class="formtext">
																																			<asp:Label ID="old_txtTRXKMINDVTempatKerja" runat="server" Width="257px"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															m. Tujuan Transaksi</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="old_txtTRXKMINDVTujuanTrx" runat="server" Width="257px"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															n. Sumber Dana</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="old_txtTRXKMINDVSumberDana" runat="server" Width="257px"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															o. Rekening lain yang terkait dengan transaksi (apabila ada)</td>
																														<td style="width: 5px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formtext">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td class="formtext">
																																			ii. Nama Bank Lain</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="old_txtTRXKMINDVNamaBankLain" runat="server" Width="257px"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			ii. Nomor Rekening Tujuan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="old_txtTRXKMINDVNoRekeningTujuan" runat="server" Width="257px"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																												</tbody>
																											</table>
																											<table id="old_tblTRXKMTipePelaporKorporasi" runat="server">
																												<tbody>
																													<tr>
																														<td class="formtext" colspan="3">
																															<strong>4.5.2. Korporasi</strong></td>
																													</tr>
																													<tr>
																														<td style="height: 27px" class="formtext">
																															a. Bentuk Badan Usaha</td>
																														<td style="width: 5px; height: 27px">
																															:</td>
																														<td style="height: 27px" class="formtext">
																															&nbsp; &nbsp;
																															<asp:DropDownList ID="old_cboTRXKMCORPBentukBadanUsaha" runat="server" Enabled="False">
																															</asp:DropDownList></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															b. Nama Korporasi
																														</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="old_txtTRXKMCORPNama" runat="server" Width="370px"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															c. Bidang Usaha Korporasi</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<table style="width: 127px">
																																<tbody>
																																	<tr>
																																		<td>
																																			<asp:Label ID="old_txtTRXKMCORPBidangUsaha" runat="server" Width="167px"></asp:Label></td>
																																		<td>
																																		</td>
																																	</tr>
																																</tbody>
																															</table>
																															<asp:HiddenField ID="old_hfTRXKMCORPBidangUsaha" runat="server"></asp:HiddenField>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															d. Alamat Korporasi</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:RadioButtonList ID="old_rblTRXKMCORPTipeAlamat" runat="server" Enabled="False"
																																AutoPostBack="True" RepeatDirection="Horizontal">
																																<asp:ListItem Selected="True">Dalam Negeri</asp:ListItem>
																																<asp:ListItem>Luar Negeri</asp:ListItem>
																															</asp:RadioButtonList></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															e. Alamat Lengkap Korporasi</td>
																														<td style="width: 5px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formtext">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td class="formtext">
																																			Nama Jalan</td>
																																		<td style="width: 5px">
																																			:
																																		</td>
																																		<td class="formtext">
																																			<asp:Label ID="old_txtTRXKMCORPDLNamaJalan" runat="server" Width="370px"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			RT/RW</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="old_txtTRXKMCORPDLRTRW" runat="server"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kelurahan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext" valign="top">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td style="height: 22px">
																																							<asp:Label ID="old_txtTRXKMCORPDLKelurahan" runat="server" Width="167px"></asp:Label></td>
																																						<td style="height: 22px">
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="old_hfTRXKMCORPDLKelurahan" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kecamatan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			&nbsp;<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="old_txtTRXKMCORPDLKecamatan" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="old_hfTRXKMCORPDLKecamatan" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kota / Kabupaten</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="old_txtTRXKMCORPDLKotaKab" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="old_hfTRXKMCORPDLKotaKab" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Provinsi</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td style="height: 22px">
																																							<asp:Label ID="old_txtTRXKMCORPDLProvinsi" runat="server" Width="167px"></asp:Label></td>
																																						<td style="height: 22px">
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="old_hfTRXKMCORPDLProvinsi" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Negara</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="old_txtTRXKMCORPDLNegara" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="old_hfTRXKMCORPDLNegara" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kode Pos</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="old_txtTRXKMCORPDLKodePos" runat="server"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															i. Alamat Korporasi Luar Negeri</td>
																														<td style="width: 5px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formtext">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td class="formtext">
																																			Nama Jalan</td>
																																		<td style="width: 5px">
																																			:
																																		</td>
																																		<td class="formtext">
																																			<asp:Label ID="old_txtTRXKMCORPLNNamaJalan" runat="server" Width="370px"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Negara</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="old_txtTRXKMCORPLNNegara" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="old_hfTRXKMCORPLNNegara" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Provinsi</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext" valign="top">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="old_txtTRXKMCORPLNProvinsi" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="old_hfTRXKMCORPLNProvinsi" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kota
																																		</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="old_txtTRXKMCORPLNKota" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="old_hfTRXKMCORPLNKota" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kode Pos</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="old_txtTRXKMCORPLNKodePos" runat="server"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															k. Nomor Pokok Wajib Pajak (NPWP)</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="old_txtTRXKMCORPNPWP" runat="server" Width="257px"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															l. Tujuan Transaksi</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="old_txtTRXKMCORPTujuanTrx" runat="server" Width="257px"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															m. Sumber Dana</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="old_txtTRXKMCORPSumberDana" runat="server" Width="257px"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															o. Rekening lain yang terkait dengan transaksi (apabila ada)</td>
																														<td style="width: 5px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formtext">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td class="formtext">
																																			ii. Nama Bank Lain</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="old_txtTRXKMCORPNamaBankLain" runat="server" Width="257px"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			ii. Nomor Rekening Tujuan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="old_txtTRXKMCORPNoRekeningTujuan" runat="server" Width="257px"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																												</tbody>
																											</table>
																										</asp:View>
																										<asp:View ID="old_ViewKasKeluar" runat="server">
																											<table width="100%">
																												<tbody>
																													<tr>
																														<td style="width: 300px" class="formtext">
																															5.1. Tanggal Transaksi (tgl/bln/thn)<span style="color: #cc0000">*</span></td>
																														<td style="width: 2px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="old_txtTRXKKTanggalTransaksi" runat="server" Width="96px" MaxLength="50"></asp:Label>
																														</td>
																													</tr>
																													<tr>
																														<td style="height: 27px" class="formtext">
																															5.2. Nama Kantor PJK tempat terjadinya transaksi</td>
																														<td style="width: 2px; height: 27px">
																														</td>
																														<td style="height: 27px" class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<div>
																																<table>
																																	<tbody>
																																		<tr>
																																			<td class="formtext">
																																				&nbsp; &nbsp;&nbsp;
																																			</td>
																																			<td class="formtext">
																																				a. Nama Kantor<span style="color: #cc0000">*</span></td>
																																			<td style="width: 5px">
																																				:</td>
																																			<td class="formtext">
																																				<asp:Label ID="old_txtTRXKKNamaKantor" runat="server" Width="370px"></asp:Label>
																																				&nbsp;</td>
																																		</tr>
																																		<tr>
																																			<td class="formtext">
																																			</td>
																																			<td class="formtext">
																																				b. Kota Kabupaten<span style="color: #cc0000">*</span></td>
																																			<td style="width: 5px">
																																				:</td>
																																			<td class="formtext">
																																				<table style="width: 127px">
																																					<tbody>
																																						<tr>
																																							<td>
																																								<asp:Label ID="old_txtTRXKKKotaKab" runat="server" Width="167px"></asp:Label></td>
																																							<td>
																																							</td>
																																						</tr>
																																					</tbody>
																																				</table>
																																				<asp:HiddenField ID="old_hfTRXKKKotaKab" runat="server"></asp:HiddenField>
																																				&nbsp;
																																			</td>
																																		</tr>
																																		<tr>
																																			<td class="formtext">
																																			</td>
																																			<td class="formtext">
																																				c. Provinsi<span style="color: #cc0000">*</span></td>
																																			<td style="width: 5px">
																																				:</td>
																																			<td class="formtext">
																																				<table style="width: 127px">
																																					<tbody>
																																						<tr>
																																							<td>
																																								<asp:Label ID="old_txtTRXKKProvinsi" runat="server" Width="167px"></asp:Label></td>
																																						</tr>
																																					</tbody>
																																				</table>
																																				<asp:HiddenField ID="old_hfTRXKKProvinsi" runat="server"></asp:HiddenField>
																																				&nbsp;
																																			</td>
																																		</tr>
																																	</tbody>
																																</table>
																															</div>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															5.3. Detail Kas Keluar</td>
																														<td style="width: 2px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formtext">
																																			a. Kas Keluar (Rp)</td>
																																		<td style="width: 2px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="old_txtTRXKKDetailKasKeluar" runat="server" Width="167px" AutoPostBack="True"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																			b. Kas Keluar Dalam valuta asing (Dapat &gt; 1)</td>
																																		<td style="width: 2px">
																																		</td>
																																		<td class="formtext">
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext" colspan="3">
																																			<table>
																																				<tbody>
																																					<tr>
																																						<td class="formtext">
																																							&nbsp; &nbsp;&nbsp;
																																						</td>
																																						<td class="formtext">
																																							i. Mata Uang</td>
																																						<td style="width: 2px">
																																							:</td>
																																						<td class="formtext">
																																							<table style="width: 127px">
																																								<tbody>
																																									<tr>
																																										<td>
																																											<asp:Label ID="old_txtTRXKKDetilMataUang" runat="server" Width="167px"></asp:Label></td>
																																										<td>
																																										</td>
																																									</tr>
																																								</tbody>
																																							</table>
																																							<asp:HiddenField ID="old_hfTRXKKDetilMataUang" runat="server"></asp:HiddenField>
																																						</td>
																																					</tr>
																																					<tr>
																																						<td class="formtext">
																																						</td>
																																						<td class="formtext">
																																							ii. Kurs Transaksi</td>
																																						<td style="width: 2px">
																																							:</td>
																																						<td class="formtext">
																																							<asp:Label ID="old_txtTRXKKDetilKursTrx" runat="server" Width="167px"></asp:Label></td>
																																					</tr>
																																					<tr>
																																						<td class="formtext">
																																						</td>
																																						<td class="formtext">
																																							iii. Jumlah&nbsp;</td>
																																						<td style="width: 2px">
																																							:</td>
																																						<td class="formtext">
																																							<asp:Label ID="old_txtTRXKKDetilJumlah" runat="server" Width="167px"></asp:Label></td>
																																					</tr>
																																					<tr>
																																						<td class="formtext">
																																						</td>
																																						<td class="formtext" colspan="3">
																																						</td>
																																					</tr>
																																					<tr>
																																						<td class="formtext">
																																						</td>
																																						<td class="formtext" colspan="3">
																																							<asp:GridView ID="old_grvDetilKasKeluar" runat="server" Width="290px" SkinID="grv2"
																																								AutoGenerateColumns="False">
																																								<Columns>
																																									<asp:BoundField HeaderText="No" />
																																									<asp:BoundField HeaderText="Mata Uang" DataField="MataUang" />
																																									<asp:BoundField HeaderText="Kurs Transaksi" DataField="KursTransaksi" />
																																									<asp:BoundField HeaderText="Jumlah" DataField="Jumlah" />
																																									<asp:BoundField DataField="JumlahRp" HeaderText="Jumlah (Rp)" ItemStyle-HorizontalAlign="Right" />
																																								</Columns>
																																							</asp:GridView>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																			c. Total Kas Keluar (a+b) (Rp)</td>
																																		<td style="width: 2px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="old_lblDetilKasKeluarJumlahRp" runat="server"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
                                                                                                                    <tr>
                                                                                                                        <td class="formtext">
                                                                                                                            5.4 Nomor Rekening Nasabah</td>
                                                                                                                        <td style="width: 2px">
                                                                                                                            :</td>
                                                                                                                        <td class="formtext">
																															<asp:Label ID="old_txtTRXKKRekeningKK" runat="server" Width="257px"></asp:Label></td>
                                                                                                                    </tr>
																													<tr>
																														<td class="formtext">
                                                                                                                            5.5. Identitas pihak terkait dengan terlapor</td>
																														<td style="width: 2px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td style="height: 41px" class="formtext">
																															&nbsp; &nbsp; &nbsp; Tipe Pihak Terkait</td>
																														<td style="width: 2px; height: 41px">
																															:</td>
																														<td style="height: 41px" class="formtext">
																															<asp:RadioButtonList ID="old_rblTRXKKTipePelapor" runat="server" AutoPostBack="True"
                                                                                                                                RepeatDirection="Horizontal" Enabled="False">
                                                                                                                                <asp:ListItem Value="1">Perorangan</asp:ListItem>
                                                                                                                                <asp:ListItem Value="2">Korporasi</asp:ListItem>
																															</asp:RadioButtonList></td>
																													</tr>
																												</tbody>
																											</table>
																											<table id="old_tblTRXKKTipePelaporPerorangan" runat="server">
																												<tbody>
																													<tr>
																														<td class="formtext" colspan="3">
																															<strong>5.5.1. Perorangan</strong>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															a. Gelar</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="old_txtTRXKKINDVGelar" runat="server"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															b. Nama Lengkap
																														</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="old_txtTRXKKINDVNamaLengkap" runat="server" Width="370px"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															c. Tempat Lahir</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="old_txtTRXKKINDVTempatLahir" runat="server" Width="240px"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															d. Tanggal Lahir
																														</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<br />
																															<asp:Label ID="old_txtTRXKKINDVTglLahir" runat="server" Width="96px" MaxLength="50"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															e. Kewarganegaraan
																														</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:RadioButtonList ID="old_rblTRXKKINDVKewarganegaraan" runat="server" Enabled="False"
																																AutoPostBack="True" RepeatDirection="Horizontal">
																																<asp:ListItem Selected="True" Value="1">WNI</asp:ListItem>
																																<asp:ListItem Value="2">WNA</asp:ListItem>
																															</asp:RadioButtonList></td>
																													</tr>
																													<tr style="color: #cc0000">
																														<td class="formtext">
																															f. Negara
																														</td>
																														<td style="width: 5px; color: #000000">
																															:</td>
																														<td class="formtext">
																															<asp:DropDownList ID="old_cboTRXKKINDVNegara" runat="server" CssClass="combobox"
																																Enabled="False">
																															</asp:DropDownList></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															g. Alamat Domisili</td>
																														<td style="width: 5px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formtext">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td class="formtext">
																																			Nama Jalan</td>
																																		<td style="width: 5px">
																																			:
																																		</td>
																																		<td class="formtext">
																																			<asp:Label ID="old_txtTRXKKINDVDOMNamaJalan" runat="server" Width="370px"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			RT/RW</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="old_txtTRXKKINDVDOMRTRW" runat="server"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kelurahan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext" valign="top">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="old_txtTRXKKINDVDOMKelurahan" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="old_hfTRXKKINDVDOMKelurahan" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kecamatan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			&nbsp;<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="old_txtTRXKKINDVDOMKecamatan" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="old_hfTRXKKINDVDOMKecamatan" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kota / Kabupaten</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="old_txtTRXKKINDVDOMKotaKab" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="old_hfTRXKKINDVDOMKotaKab" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Provinsi</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="old_txtTRXKKINDVDOMProvinsi" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="old_hfTRXKKINDVDOMProvinsi" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
                                                                                                                                    <tr>
                                                                                                                                        <td class="formtext">
                                                                                                                                            &nbsp;</td>
                                                                                                                                        <td class="formText">
                                                                                                                                            Negara</td>
                                                                                                                                        <td style="width: 5px">
                                                                                                                                            :</td>
                                                                                                                                        <td class="formtext">
                                                                                                                                            <table style="width: 127px">
                                                                                                                                                <tbody>
                                                                                                                                                    <tr>
                                                                                                                                                        <td>
                                                                                                                                                            <asp:Label ID="old_txtTRXKKINDVDOMNegara" runat="server" Width="167px"></asp:Label>
                                                                                                                                                        </td>
                                                                                                                                                        <td>
                                                                                                                                                        </td>
                                                                                                                                                    </tr>
                                                                                                                                                </tbody>
                                                                                                                                            </table>
                                                                                                                                            <asp:HiddenField ID="old_hfTRXKKINDVDOMNegara" runat="server" />
                                                                                                                                        </td>
                                                                                                                                    </tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kode Pos</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="old_txtTRXKKINDVDOMKodePos" runat="server"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															h. Alamat Sesuai Bukti Identitas</td>
																														<td style="width: 5px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formtext">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td class="formtext">
																																			Nama Jalan</td>
																																		<td style="width: 5px">
																																			:
																																		</td>
																																		<td class="formtext">
																																			<asp:Label ID="old_txtTRXKKINDVIDNamaJalan" runat="server" Width="370px"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			RT/RW</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="old_txtTRXKKINDVIDRTRW" runat="server"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kelurahan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext" valign="top">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="old_txtTRXKKINDVIDKelurahan" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="old_hfTRXKKINDVIDKelurahan" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kecamatan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			&nbsp;<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="old_txtTRXKKINDVIDKecamatan" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="old_hfTRXKKINDVIDKecamatan" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kota / Kabupaten</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="old_txtTRXKKINDVIDKotaKab" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="old_hfTRXKKINDVIDKotaKab" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Provinsi</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="old_txtTRXKKINDVIDProvinsi" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="old_hfTRXKKINDVIDProvinsi" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kode Pos</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="old_txtTRXKKINDVIDKodePos" runat="server"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															i. Alamat Sesuai Negara Asal</td>
																														<td style="width: 5px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formtext">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td class="formtext">
																																			Nama Jalan</td>
																																		<td style="width: 5px">
																																			:
																																		</td>
																																		<td class="formtext">
																																			<asp:Label ID="old_txtTRXKKINDVNANamaJalan" runat="server" Width="370px"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Negara</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="old_txtTRXKKINDVNANegara" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="old_hfTRXKKINDVNANegara" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Provinsi</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext" valign="top">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="old_txtTRXKKINDVNAProvinsi" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="old_hfTRXKKINDVNAProvinsi" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kota
																																		</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="old_txtTRXKKINDVNAKota" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="old_hfTRXKKINDVNAKota" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kode Pos</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="old_txtTRXKKINDVNAKodePos" runat="server"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															j. Jenis Dokumen Identitas</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<ajax:AjaxPanel ID="old_AjaxPanel3" runat="server">
																																<asp:DropDownList runat="server" ID="old_cboTRXKKINDVJenisID" CssClass="combobox"
																																	Enabled="False">
																																</asp:DropDownList>
																															</ajax:AjaxPanel>
																														</td>
																													</tr>
																													<tr>
																														<td id="old_Td2" class="formtext" colspan="3" runat="server">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formtext">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td class="formtext">
																																			 Nomor Identitas <span style="color: #cc0000">*</span></td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td style="width: 260px" class="formtext">
																																			<asp:Label ID="old_txtTRXKKINDVNomorId" runat="server" Width="257px"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															k. Nomor Pokok Wajib Pajak (NPWP)</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="old_txtTRXKKINDVNPWP" runat="server" Width="257px"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															l. Pekerjaan</td>
																														<td style="width: 5px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formtext">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td class="formtext">
																																			Pekerjaan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td style="width: 260px" class="formtext">
																																			<asp:Label ID="old_txtTRXKKINDVPekerjaan" runat="server" Width="257px"></asp:Label>
																																			&nbsp;<asp:HiddenField ID="old_hfTRXKKINDVPekerjaan" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Jabatan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td style="width: 260px" class="formtext">
																																			<asp:Label ID="old_txtTRXKKINDVJabatan" runat="server" Width="257px"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Penghasilan rata-rata/th (Rp)</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td style="width: 260px" class="formtext">
																																			<asp:Label ID="old_txtTRXKKINDVPenghasilanRataRata" runat="server" Width="257px"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Tempat kerja</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td style="width: 260px" class="formtext">
																																			<asp:Label ID="old_txtTRXKKINDVTempatKerja" runat="server" Width="257px"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															m. Tujuan Transaksi</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="old_txtTRXKKINDVTujuanTrx" runat="server" Width="257px"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															n. Sumber Dana</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="old_txtTRXKKINDVSumberDana" runat="server" Width="257px"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															o. Rekening lain yang terkait dengan transaksi (apabila ada)</td>
																														<td style="width: 5px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formtext">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td class="formtext">
																																			ii. Nama Bank Lain</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="old_txtTRXKKINDVNamaBankLain" runat="server" Width="257px"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			ii. Nomor Rekening Tujuan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="old_txtTRXKKINDVNoRekTujuan" runat="server" Width="257px"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																												</tbody>
																											</table>
																											<table id="old_tblTRXKKTipePelaporKorporasi" runat="server">
																												<tbody>
																													<tr>
																														<td class="formtext" colspan="3">
																															<strong>5.5.2. Korporasi</strong></td>
																													</tr>
																													<tr>
																														<td style="height: 28px" class="formtext">
																															a. Bentuk Badan Usaha</td>
																														<td style="width: 5px; height: 28px">
																															:</td>
																														<td style="height: 28px" class="formtext">
																															&nbsp;&nbsp;
																															<asp:DropDownList ID="old_cboTRXKKCORPBentukBadanUsaha" runat="server" Enabled="False">
																															</asp:DropDownList></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															b. Nama Korporasi
																														</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="old_txtTRXKKCORPNama" runat="server" Width="370px"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															c. Bidang Usaha Korporasi</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<table style="width: 127px">
																																<tbody>
																																	<tr>
																																		<td>
																																			<asp:Label ID="old_txtTRXKKCORPBidangUsaha" runat="server" Width="167px"></asp:Label></td>
																																		<td>
																																		</td>
																																	</tr>
																																</tbody>
																															</table>
																															<asp:HiddenField ID="old_hfTRXKKCORPBidangUsaha" runat="server"></asp:HiddenField>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															d. Alamat Korporasi</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:RadioButtonList ID="old_rblTRXKKCORPTipeAlamat" runat="server" Enabled="False"
																																AutoPostBack="True" RepeatDirection="Horizontal">
																																<asp:ListItem Selected="True">Dalam Negeri</asp:ListItem>
																																<asp:ListItem>Luar Negeri</asp:ListItem>
																															</asp:RadioButtonList></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															e. Alamat Lengkap Korporasi</td>
																														<td style="width: 5px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formtext">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td class="formtext">
																																			Nama Jalan</td>
																																		<td style="width: 5px">
																																			:
																																		</td>
																																		<td class="formtext">
																																			<asp:Label ID="old_txtTRXKKCORPDLNamaJalan" runat="server" Width="370px"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			RT/RW</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="old_txtTRXKKCORPDLRTRW" runat="server"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kelurahan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext" valign="top">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="old_txtTRXKKCORPDLKelurahan" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="old_hfTRXKKCORPDLKelurahan" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kecamatan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			&nbsp;<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="old_txtTRXKKCORPDLKecamatan" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="old_hfTRXKKCORPDLKecamatan" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kota / Kabupaten</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="old_txtTRXKKCORPDLKotaKab" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="old_hfTRXKKCORPDLKotaKab" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Provinsi</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="old_txtTRXKKCORPDLProvinsi" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="old_hfTRXKKCORPDLProvinsi" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Negara</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="old_txtTRXKKCORPDLNegara" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="old_hfTRXKKCORPDLNegara" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kode Pos</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="old_txtTRXKKCORPDLKodePos" runat="server"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															i. Alamat Korporasi Luar Negeri</td>
																														<td style="width: 5px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formtext">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td class="formtext">
																																			Nama Jalan</td>
																																		<td style="width: 5px">
																																			:
																																		</td>
																																		<td class="formtext">
																																			<asp:Label ID="old_txtTRXKKCORPLNNamaJalan" runat="server" Width="370px"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Negara</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="old_txtTRXKKCORPLNNegara" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="old_hfTRXKKCORPLNNegara" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Provinsi</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext" valign="top">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="old_txtTRXKKCORPLNProvinsi" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="old_hfTRXKKCORPLNProvinsi" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kota
																																		</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<table style="width: 127px">
																																				<tbody>
																																					<tr>
																																						<td>
																																							<asp:Label ID="old_txtTRXKKCORPLNKota" runat="server" Width="167px"></asp:Label></td>
																																						<td>
																																						</td>
																																					</tr>
																																				</tbody>
																																			</table>
																																			<asp:HiddenField ID="old_hfTRXKKCORPLNKota" runat="server"></asp:HiddenField>
																																		</td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			Kode Pos</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="old_txtTRXKKCORPLNKodePos" runat="server"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															k. Nomor Pokok Wajib Pajak (NPWP)</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="old_txtTRXKKCORPNPWP" runat="server" Width="257px"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															l. Tujuan Transaksi</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="old_txtTRXKKCORPTujuanTrx" runat="server" Width="257px"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															m. Sumber Dana</td>
																														<td style="width: 5px">
																															:</td>
																														<td class="formtext">
																															<asp:Label ID="old_txtTRXKKCORPSumberDana" runat="server" Width="257px"></asp:Label></td>
																													</tr>
																													<tr>
																														<td class="formtext">
																															o. Rekening lain yang terkait dengan transaksi (apabila ada)</td>
																														<td style="width: 5px">
																														</td>
																														<td class="formtext">
																														</td>
																													</tr>
																													<tr>
																														<td class="formtext" colspan="3">
																															<table>
																																<tbody>
																																	<tr>
																																		<td class="formtext">
																																			&nbsp; &nbsp;&nbsp;
																																		</td>
																																		<td class="formtext">
																																			ii. Nama Bank Lain</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="old_txtTRXKKCORPNamaBankLain" runat="server" Width="257px"></asp:Label></td>
																																	</tr>
																																	<tr>
																																		<td class="formtext">
																																		</td>
																																		<td class="formtext">
																																			ii. Nomor Rekening Tujuan</td>
																																		<td style="width: 5px">
																																			:</td>
																																		<td class="formtext">
																																			<asp:Label ID="old_txtTRXKKCORPNoRekeningTujuan" runat="server" Width="257px"></asp:Label></td>
																																	</tr>
																																</tbody>
																															</table>
																														</td>
																													</tr>
																												</tbody>
																											</table>
																										</asp:View>
																									</asp:MultiView>
																									<table>
																										<tbody>
																											<tr>
																												<td bgcolor="#ffffff">
																													&nbsp;&nbsp;
																												</td>
																												<td style="width: 76px" bgcolor="#ffffff">
																													&nbsp;
																												</td>
																											</tr>
																										</tbody>
																									</table>
																									<table>
																										<tbody>
																											<tr>
																												<td colspan="3">
																													<asp:GridView ID="old_grvTransaksi" runat="server" SkinID="grv2" AutoGenerateColumns="False">
																														<Columns>
																															<asp:BoundField HeaderText="No" />
																															<asp:BoundField HeaderText="Kas" DataField="Kas" />
                                                                                                                            <asp:BoundField HeaderText="Transaction Date" DataField="TransactionDate" 
                                                                                                                                DataFormatString="{0:dd-MMM-yyyy}" />
																															<asp:BoundField HeaderText="Branch" DataField="Branch" />
																															<asp:BoundField HeaderText="Account No" DataField="AccountNumber" />
																															<asp:BoundField HeaderText="Customer / LTKT" DataField="Type" Visible="false" />
																															<asp:BoundField HeaderText="Transaction Nominal" DataField="TransactionNominal" ItemStyle-HorizontalAlign="Right" />
																															<asp:TemplateField>
																																<ItemTemplate>
																																	<asp:LinkButton ID="old_EditResume" runat="server" CausesValidation="False" OnClick="old_EditResume_Click">View</asp:LinkButton>
																																</ItemTemplate>
																															</asp:TemplateField>
																														</Columns>
																													</asp:GridView>
																												</td>
																											</tr>
																											<tr>
																												<td class="formtext">
																													Total Seluruh Kas Masuk</td>
																												<td style="width: 5px">
																													:</td>
																												<td class="formtext">
																													<asp:Label ID="old_lblTotalKasMasuk" runat="server"></asp:Label>
																												</td>
																											</tr>
																											<tr>
																												<td style="height: 15px" class="formtext">
																													Total Seluruh Kas Keluar</td>
																												<td style="height: 15px; wifdth: 5px">
																													:</td>
																												<td style="height: 15px" class="formtext">
																													<asp:Label ID="old_lblTotalKasKeluar" runat="server"></asp:Label>
																												</td>
																											</tr>
																										</tbody>
																									</table>
																								</td>
																							</tr>
																							<tr>    
																							<td>    
																							<table border="0">
																					<tbody>
																						<tr valign="top">
																							<td style="height: 45px" class="formtext">
																								6.Sebutkan informasi lainnya yang ada</td>
																							<td style="width: 2px; height: 45px">
																								:</td>
																							<td style="height: 45px" class="formtext">
																								<asp:Label ID="old_txtLTKTInfoLainnya" runat="server" Width="257px" Height="46px"></asp:Label></td>
																						</tr>
																					</tbody>
																				</table>
																							</td>
																							</tr>
																						</tbody>
																					</table>
																				</ajax:AjaxPanel>
																			</td>
																		</tr>
																	</tbody>
																</table>
															</asp:Panel>
														</td>
													</tr>
												</tbody>
											</table>
										</asp:View>
										<asp:View ID="vwMessage" runat="server">
											<table width="100%" style="horiz-align: center;">
												<tr>
													<td class="formtext" align="center" style="height: 15px">
														<asp:Label runat="server" ID="lblMsg"></asp:Label>
													</td>
												</tr>
												<tr>
													<td class="formtext" align="center" style="height: 27px">
														<asp:ImageButton ID="imgOKMsg" runat="server" ImageUrl="~/Images/button/Ok.gif" />
													</td>
												</tr>
											</table>
										</asp:View>
										<asp:View ID="RejectReason" runat="server">
											<table width="100%" style="horiz-align: center;">
												<tr>
													<td>
														<table border="0" align="center">
															<tr>
																<td class="formtext" align="left" style="height: 15px">
																	<asp:Label runat="server" ID="lblReject"></asp:Label>
																</td>
															</tr>
															<tr>
																<td>
																	<asp:TextBox ID="txtReasonReject" runat="server" Columns="100" Rows="20" TextMode="MultiLine"></asp:TextBox>
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td class="formtext" align="center" style="height: 27px">
														<asp:ImageButton ID="imgOkRejectReason" runat="server" ImageUrl="~/Images/button/Ok.gif" />
													</td>
												</tr>
											</table>
										</asp:View>
									</asp:MultiView>
								</ajax:AjaxPanel>
							</td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td>
			</td>
			<td>
				<ajax:AjaxPanel ID="AjaxPanel5" runat="server">
					<table cellpadding="0" cellspacing="0" border="0" width="100%">
						<tr>
							<td background="Images/button-bground.gif" align="left" valign="middle">
								<img src="Images/blank.gif" width="5" height="1" /></td>
							<td background="Images/button-bground.gif" align="left" valign="middle">
								<img src="images/arrow.gif" width="15" height="15" /></td>
							<td background="Images/button-bground.gif" style="width: 5px">
								&nbsp;</td>
							<td background="Images/button-bground.gif">
								<asp:ImageButton ID="imgApprove" runat="server" ImageUrl="~/Images/button/accept.gif" />&nbsp;</td>
							<td background="Images/button-bground.gif">
								&nbsp;<asp:ImageButton ID="imgReject" runat="server" CausesValidation="False" 
									ImageUrl="~/Images/button/reject.gif"></asp:ImageButton></td>
							<td background="Images/button-bground.gif">
								&nbsp;<asp:ImageButton ID="ImageCancel" runat="server" CausesValidation="False" 
                                    ImageUrl="~/Images/button/cancel.gif"></asp:ImageButton></td>
							<td width="99%" background="Images/button-bground.gif">
								<img src="Images/blank.gif" width="1" height="1" /></td>
							<td>
								</td>
						</tr>
					</table>
				</ajax:AjaxPanel>
			</td>
		</tr>
	</table>
	<asp:CustomValidator ID="cvalPageErr" runat="server" Display="None"></asp:CustomValidator>
</asp:Content>
