<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="DataUpdatingParameter.aspx.vb" Inherits="DataUpdatingParameter" title="Data Updating Parameter" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
    <table id="title"  border="2" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="99%">
        <tr>
            <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Data Updating Parameter&nbsp;
                    <hr />
                </strong>
                <asp:Label ID="LblSucces" runat="server" CssClass="validationok" Visible="False"
                    Width="94%"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" colspan="4" style="border-top-style: none; border-right-style: none;
                border-left-style: none; height: 24px; border-bottom-style: none"><span style="color: #ff0000">* Required</span></td>
        </tr>
    </table>	
    <table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
	    border="2">
       	    <tr class="formText">
		    <td width="5" bgColor="#ffffff" height="24"><asp:requiredfieldvalidator id="RequiredFieldValidatorAbnormalTrans" runat="server" ErrorMessage="Abnormal Trans cannot be blank"
				    Display="Dynamic" ControlToValidate="TextAbnormalTrans">*</asp:requiredfieldvalidator><br />
                <asp:RegularExpressionValidator ID="RegularExpressionValidatorAbnormalTrans" runat="server"
                    ControlToValidate="TextAbnormalTrans" Display="Dynamic" ErrorMessage="Abnormal Trans must be a numeric value"
                    ValidationExpression="^[0-9]+$">*</asp:RegularExpressionValidator>&nbsp;</td>
		    <td bgColor="#ffffff" colspan="3">
                &nbsp;<asp:Panel ID="PanelDataUpdatingParameter" runat="server" GroupingText="Based on Abnormal Transaction"
                    Height="50px" Width="320px">
                    Abnormal Trans &gt;= &nbsp;<asp:textbox id="TextAbnormalTrans" runat="server" CssClass="textBox" MaxLength="10" Width="50px"></asp:textbox>
                    <strong><span style="color: #ff0000">*</span></strong><br />
                </asp:Panel>
            </td>
	    </tr>
	    <tr class="formText" bgColor="#dddddd" height="30">
		    <td width="15"><IMG height="15" src="images/arrow.gif" width="15"></td>
		    <td colSpan="3">
			    <table cellSpacing="0" cellPadding="3" border="0">
				    <tr>
					    <td><asp:imagebutton id="ImageSave" runat="server" CausesValidation="True" SkinID="SaveButton"></asp:imagebutton></td>
					    <td><asp:imagebutton id="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton"></asp:imagebutton></td>
				    </tr>
			    </table>
                <asp:CustomValidator ID="cvalPageErr" runat="server" Display="None"></asp:CustomValidator></td>
	    </tr>
    </table>
	<script language="javascript">
	    document.getElementById('<%=GetFocusID %>').focus();
	</script>
</asp:Content>