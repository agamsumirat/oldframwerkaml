Partial Class MessagePending
    Inherits Parent

    ''' <summary>
    ''' get user id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetMessagePendingID() As Integer
        Get
            Return Me.Request.Params("MessagePendingID")
        End Get
    End Property

    ''' <summary>
    ''' get user id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetIdentifier() As String
        Get
            Return Me.Request.Params("Identifier")
        End Get
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.LabelMessagePendingID.Text = "[MessagePendingID " & Me.GetMessagePendingID & "]"
            Select Case Me.GetMessagePendingID
                Case 1001 'Change Password
                    Me.LabelMessage.Text = "The password for the following User : " & Me.GetIdentifier & " has been changed."
                Case 1002 'Force Change Password
                    Me.LabelMessage.Text = "The password for the following User : " & Me.GetIdentifier & " has been changed."
                Case 8101 'Sub Modul News - Add
                    Me.LabelMessage.Text = "A News with the following News Title : " & Me.GetIdentifier & " has been added and it is currently waiting for approval."
                Case 8102 'Sub Modul News - Edit
                    Me.LabelMessage.Text = "A News with the following News Title : " & Me.GetIdentifier & " has been edited and it is currently waiting for approval."
                Case 8103 'Sub Modul News - Delete
                    Me.LabelMessage.Text = "A News with the following News Title : " & Me.GetIdentifier & " has been deleted and it is currently waiting for approval."

                Case 8201 'Sub Modul Group - Add
                    Me.LabelMessage.Text = "A Group with the following Group Name : " & Me.GetIdentifier & " has been added and it is currently waiting for approval."
                Case 8202 'Sub Modul Group - Edit
                    Me.LabelMessage.Text = "A Group with the following Group Name : " & Me.GetIdentifier & " has been edited and it is currently waiting for approval."
                Case 8203 'Sub Modul Group - Delete
                    Me.LabelMessage.Text = "A Group with the following Group Name : " & Me.GetIdentifier & " has been deleted and it is currently waiting for approval."

                Case 8301 'Sub Modul User - Add
                    Me.LabelMessage.Text = "A User with the following UserID : " & Me.GetIdentifier & " has been added and it is currently waiting for approval."
                Case 8302 'Sub Modul User - Edit
                    Me.LabelMessage.Text = "A User with the following UserID : " & Me.GetIdentifier & " has been edited and it is currently waiting for approval."
                Case 8303 'Sub Modul User - Delete
                    Me.LabelMessage.Text = "A User with the following UserID : " & Me.GetIdentifier & " has been deleted and it is currently waiting for approval."

                Case 8401 'Sub Modul Level Type - Add
                    Me.LabelMessage.Text = "A Level Type with the following Level Type Name: " & Me.GetIdentifier & " has been added and it is currently waiting for approval."
                Case 8402 'Sub Modul Level Type - Edit
                    Me.LabelMessage.Text = "A Level Type with the following Level Type Name: " & Me.GetIdentifier & " has been edited and it is currently waiting for approval."
                Case 8403 'Sub Modul Level Type - Delete
                    Me.LabelMessage.Text = "A Level Type with the following Level Type Name: " & Me.GetIdentifier & " has been deleted and it is currently waiting for approval."

                Case 8501 'Sub Modul Working Unit - Add
                    Me.LabelMessage.Text = "A Working Unit with the following Working Unit Name: " & Me.GetIdentifier & " has been added and it is currently waiting for approval."
                Case 8502 'Sub Modul Working Unit - Edit
                    Me.LabelMessage.Text = "A Working Unit with the following Working Unit Name: " & Me.GetIdentifier & " has been edited and it is currently waiting for approval."
                Case 8503 'Sub Modul Working Unit - Delete
                    Me.LabelMessage.Text = "A Working Unit with the following Working Unit Name: " & Me.GetIdentifier & " has been deleted and it is currently waiting for approval."

                Case 8622 'Sub Modul Reset Password 
                    Me.LabelMessage.Text = "The password for the following User : " & Me.GetIdentifier & " has been reset."

                Case 8631 'Sub Modul Group Insider Code Mapping - Add
                    Me.LabelMessage.Text = "A Group Insider Code Mapping for the following Group : " & Me.GetIdentifier & " has been added and it is currently waiting for approval."

                Case 8632 'Sub Modul Group Insider Code Mapping - Edit
                    Me.LabelMessage.Text = "A Group Insider Code Mapping for the following Group : " & Me.GetIdentifier & " has been edited and it is currently waiting for approval."

                Case 8633 'Sub Modul Group Insider Code Mapping - Delete
                    Me.LabelMessage.Text = "A Group Insider Code Mapping for the following Group : " & Me.GetIdentifier & " has been deleted and it is currently waiting for approval."

                Case 8811 'Sub Modul Login Parameter - Add
                    Me.LabelMessage.Text = "The Login Parameter has been added and it is currently waiting for approval."
                Case 8812 'Sub Modul Login Parameter - Edit
                    Me.LabelMessage.Text = "The Login Parameter has been edited and it is currently waiting for approval."
                Case 8871 'Sub Modul Account Owner Working Unit Mapping - Add
                    Me.LabelMessage.Text = "An Account Owner Working Unit Mapping for the following Account Owner Name: " & Me.GetIdentifier & " has been added and it is currently waiting for approval."
                Case 8872 'Sub Modul Account Owner Working Unit Mapping - Edit
                    Me.LabelMessage.Text = "An Account Owner Working Unit Mapping for the following Account Owner Name: " & Me.GetIdentifier & " has been edited and it is currently waiting for approval."
                Case 8873 'Sub Modul Account Owner Working Unit Mapping - Delete
                    Me.LabelMessage.Text = "An Account Owner Working Unit Mapping for the following Account Owner Name: " & Me.GetIdentifier & " has been deleted and it is currently waiting for approval."

                Case 8901 'Sub Modul Verification List Category - Add
                    Me.LabelMessage.Text = "A Verification List Category with the following Category Name: " & Me.GetIdentifier & " has been added and it is currently waiting for approval."
                Case 8902 'Sub Modul Verification List Category - Edit
                    Me.LabelMessage.Text = "A Verification List Category with the following Category Name: " & Me.GetIdentifier & " has been edited and it is currently waiting for approval."
                Case 8903 'Sub Modul Verification List Category - Delete
                    Me.LabelMessage.Text = "A Verification List Category with the following Category Name: " & Me.GetIdentifier & " has been deleted and it is currently waiting for approval."

                Case 81001 'Sub Modul Verification List - Add
                    Me.LabelMessage.Text = "A Verification List for the following Person: " & Me.GetIdentifier & " has been added and it is currently waiting for approval."
                Case 81002 'Sub Modul Verification List - Edit
                    Me.LabelMessage.Text = "A Verification List for the following Person: " & Me.GetIdentifier & " has been edited and it is currently waiting for approval."
                Case 81003 'Sub Modul Verification List - Delete
                    Me.LabelMessage.Text = "A Verification List for the following Person: " & Me.GetIdentifier & " has been deleted and it is currently waiting for approval."

                Case 81201 'Sub Modul Risk Rating - Add
                    Me.LabelMessage.Text = "A Risk Rating Category with the following Risk Rating Name: " & Me.GetIdentifier & " has been added and it is currently waiting for approval."
                Case 81202 'Sub Modul Risk Rating - Edit
                    Me.LabelMessage.Text = "A Risk Rating Category with the following Risk Rating Name: " & Me.GetIdentifier & " has been edited and it is currently waiting for approval."
                Case 81203 'Sub Modul Risk Rating - Delete
                    Me.LabelMessage.Text = "A Risk Rating Category with the following Risk Rating Name: " & Me.GetIdentifier & " has been deleted and it is currently waiting for approval."

                Case 8721
                    Me.LabelMessage.Text = "Menu for Group '" & Me.GetIdentifier & " has been added and it is currently waiting for approval."
                Case 8722
                    Me.LabelMessage.Text = "Menu for Group '" & Me.GetIdentifier & " has been edited and it is currently waiting for approval."
                Case 8723
                    Me.LabelMessage.Text = "Menu for Group '" & Me.GetIdentifier & " has been deleted and it is currently waiting for approval."

                Case 8711
                    Me.LabelMessage.Text = "File Authority for Group '" & Me.GetIdentifier & " has been added and it is currently waiting for approval."
                Case 8712
                    Me.LabelMessage.Text = "File Authority for Group '" & Me.GetIdentifier & " has been edited and it is currently waiting for approval."
                Case 8713
                    Me.LabelMessage.Text = "File Authority for Group '" & Me.GetIdentifier & " has been deleted and it is currently waiting for approval."

                Case 8811
                    Me.LabelMessage.Text = "Login Parameter has been added and it is currently waiting for approval."
                Case 8812
                    Me.LabelMessage.Text = "Login Parameter has been edited and it is currently waiting for approval."

                Case 8821
                    Me.LabelMessage.Text = "Suspect Parameter has been added and it is currently waiting for approval."
                Case 8822
                    Me.LabelMessage.Text = "Suspect Parameter has been edited and it is currently waiting for approval."

                Case 8881
                    Me.LabelMessage.Text = "AuditTrail Parameter has been added and it is currently waiting for approval."
                Case 8882
                    Me.LabelMessage.Text = "AuditTrail Parameter has been edited and it is currently waiting for approval."
                Case 8831
                    Me.LabelMessage.Text = "Potential Customer Verification Parameter has been added and it is currently waiting for approval."
                Case 8832
                    Me.LabelMessage.Text = "Potential Customer Verification Parameter has been edited and it is currently waiting for approval."

                Case 8862
                    Me.LabelMessage.Text = "User Working Unit Assignment Parameter for user '" & Me.GetIdentifier & "' has been edited and it is currently waiting for approval."
                Case 8863
                    Me.LabelMessage.Text = "User Working Unit Assignment Parameter for user '" & Me.GetIdentifier & "' has been deleted and it is currently waiting for approval."
                Case 8841
                    Me.LabelMessage.Text = "Data Updating Parameter has been added and it is currently waiting for approval."
                Case 8842
                    Me.LabelMessage.Text = "Data Updating Parameter has been edited and it is currently waiting for approval."
                Case 8851
                    Me.LabelMessage.Text = "Data Updating Email Template Parameter has been added and it is currently waiting for approval."
                Case 8852
                    Me.LabelMessage.Text = "Data Updating Email Template Parameter has been edited and it is currently waiting for approval."
               
                Case 8864
                    Me.LabelMessage.Text = "OFAC SDN List has been uploaded and it is currently waiting for approval."
                Case 8865
                    Me.LabelMessage.Text = "PEP List has been uploaded and it is currently waiting for approval."
                Case 8867
                    Me.LabelMessage.Text = "OFAC SDN List approved has been saved successfully."
                Case 8868
                    Me.LabelMessage.Text = "OFAC SDN List rejected has been saved successfully."
                Case 8869
                    Me.LabelMessage.Text = "PEP List approved has been save successfully."
                Case 8870
                    Me.LabelMessage.Text = "PEP List rejected has been save successfully."
                Case 9002
                    Me.LabelMessage.Text = "UN List has been uploaded and it is currently waiting for approval."
                Case 9003
                    Me.LabelMessage.Text = "UN List approved has been save successfully."
                Case 9004
                    Me.LabelMessage.Text = "UN List rejected has been save successfully."

                Case 8884
                    Me.LabelMessage.Text = "Audit Trail Maintenance has been added and it is currently waiting for approval."
                Case 8885
                    Me.LabelMessage.Text = "Audit Trail User Access Maintenance has been added and it is currently waiting for approval."

                Case 8891, 8892, 8893
                    Me.LabelMessage.Text = "Customer '" & GetIdentifier & "' has been judged."
                Case 9001
                    Me.LabelMessage.Text = "Case management workflow for account owner '" & GetIdentifier & "' has been save and it is currently waiting for approval."
                Case 9002
                    Me.LabelMessage.Text = "Proposal STR Workflow has been save and it is currently waiting for approval."

                Case 81301
                    Me.LabelMessage.Text = "CTR Exemption List has been added and it is currently waiting for approval."
                Case 81302
                    Me.LabelMessage.Text = "CTR Exemption List has been deleted and it is currently waiting for approval."
                Case 81303
                    Me.LabelMessage.Text = "Transaction Code Parameter has been edited and it is currently waiting for approval."
                Case 81304
                    Me.LabelMessage.Text = "Transaction Code Parameter List has been Accepted."
                Case 81304
                    Me.LabelMessage.Text = "Transaction Code Parameter List has been Rejected."
                Case 81305
                    Me.LabelMessage.Text = "Transaction Type Auxiliary Mapping has been edited and it is currently waiting for approval."
                Case 81306
                    Me.LabelMessage.Text = "Transaction Type Auxiliary Mapping has been deleted and it is currently waiting for approval."
                Case 81401
                    Me.LabelMessage.Text = "Advanced Rule has been added and it is currently waiting for approval."
                Case 81402
                    Me.LabelMessage.Text = "Advanced Rule has been updated and it is currently waiting for approval."
                Case 81403
                    Me.LabelMessage.Text = "Advanced Rule has been deleted and it is currently waiting for approval."
                Case 81501
                    Me.LabelMessage.Text = "Risk Fact has been added and it is currently waiting for approval."
                Case 81502
                    Me.LabelMessage.Text = "Risk Fact has been updated and it is currently waiting for approval."
                Case 81503
                    Me.LabelMessage.Text = "Risk Fact has been deleted and it is currently waiting for approval."

                Case 81591
                    Me.LabelMessage.Text = "CTR Parameter Setting has been updated and it is currently waiting for approval."
                Case 81601
                    Me.LabelMessage.Text = "Peer Group has been added and it is currently waiting for approval."
                Case 81602
                    Me.LabelMessage.Text = "Peer Group has been updated and it is currently waiting for approval."
                Case 81603
                    Me.LabelMessage.Text = "Peer Group has been deleted and it is currently waiting for approval."
                Case 82591
                    Me.LabelMessage.Text = "Branch Type Mapping has been added and it is currently waiting for approval."
                Case 82592
                    Me.LabelMessage.Text = "Branch Type Mapping has been updated and it is currently waiting for approval."
                Case 82593
                    Me.LabelMessage.Text = "Branch Type Mapping has been deleted and it is currently waiting for approval."

                Case 88801
                    Me.LabelMessage.Text = "Basic Rule has been added and it is currently waiting for approval."
                Case 88802
                    Me.LabelMessage.Text = "Basic Rule has been updated and it is currently waiting for approval."
                Case 88803
                    Me.LabelMessage.Text = "Basic Rule has been deleted and it is currently waiting for approval."

                Case 89801
                    Me.LabelMessage.Text = "Custom Alert has been added and it is currently waiting for approval."
                Case 89802
                    Me.LabelMessage.Text = "Custom Alert has been updated and it is currently waiting for approval."
                Case 89803
                    Me.LabelMessage.Text = "Custom Alert has been deleted and it is currently waiting for approval."

                Case Else
                    Me.LabelMessage.Text = "Unknown Message Pending ID"
            End Select

            Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

            'Using Transcope As New Transactions.TransactionScope
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                'Transcope.Complete()
            End Using
            'End Using
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageButtonOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonOK.Click
        Select Case Me.GetMessagePendingID
            Case 1001
                Sahassa.AML.Commonly.SessionIntendedPage = "Settings.aspx"
                Me.Response.Redirect("Settings.aspx", False)
            Case 1002 'Force Change Password
                Sahassa.AML.Commonly.SessionIntendedPage = "SignOut.aspx"
                Me.Response.Redirect("SignOut.aspx", False)
            Case 8101 To 8103
                Sahassa.AML.Commonly.SessionIntendedPage = "NewsView.aspx"
                Me.Response.Redirect("NewsView.aspx", False)
            Case 8201 To 8203
                Sahassa.AML.Commonly.SessionIntendedPage = "GroupView.aspx"
                Me.Response.Redirect("GroupView.aspx", False)
            Case 8301 To 8303
                Sahassa.AML.Commonly.SessionIntendedPage = "UserView.aspx"
                Me.Response.Redirect("UserView.aspx", False)
            Case 8401 To 8403
                Sahassa.AML.Commonly.SessionIntendedPage = "LevelTypeView.aspx"
                Me.Response.Redirect("LevelTypeView.aspx", False)
            Case 8501 To 8503
                Sahassa.AML.Commonly.SessionIntendedPage = "WorkingUnitView.aspx"
                Me.Response.Redirect("WorkingUnitView.aspx", False)
            Case 8901 To 8903
                Sahassa.AML.Commonly.SessionIntendedPage = "VerificationListCategoryView.aspx"
                Me.Response.Redirect("VerificationListCategoryView.aspx", False)
            Case 81001 To 81003
                Sahassa.AML.Commonly.SessionIntendedPage = "VerificationListView.aspx"
                Me.Response.Redirect("VerificationListView.aspx", False)
            Case 81201 To 81203
                Sahassa.AML.Commonly.SessionIntendedPage = "RiskRatingView.aspx"
                Me.Response.Redirect("RiskRatingView.aspx", False)
            Case 8631 To 8633
                Sahassa.AML.Commonly.SessionIntendedPage = "GroupInsiderCodeMapping.aspx"
                Me.Response.Redirect("GroupInsiderCodeMapping.aspx", False)
            Case 8862 To 8863
                Sahassa.AML.Commonly.SessionIntendedPage = "UserWorkingUnitAssignmentView.aspx"
                Me.Response.Redirect("UserWorkingUnitAssignmentView.aspx", False)
            Case 8871 To 8873
                Sahassa.AML.Commonly.SessionIntendedPage = "AccountOwnerWorkingUnitMappingView.aspx"
                Me.Response.Redirect("AccountOwnerWorkingUnitMappingView.aspx", False)
            Case 89801 To 89803
                Sahassa.AML.Commonly.SessionIntendedPage = "SuspiciusPersonView.aspx"
                Me.Response.Redirect("SuspiciusPersonView.aspx", False)
            Case 8891
                Sahassa.AML.Commonly.SessionIntendedPage = "CustomerVerificationJudgementView.aspx"
                Me.Response.Redirect("CustomerVerificationJudgementView.aspx", False)
            Case 8892
                Sahassa.AML.Commonly.SessionIntendedPage = "CustomerVerificationJudgementWebAPIView.aspx"
                Me.Response.Redirect("CustomerVerificationJudgementWebAPIView.aspx", False)
            Case 8893
                Sahassa.AML.Commonly.SessionIntendedPage = "CustomerVerificationJudgementByRequestIDWebAPIView.aspx"
                Me.Response.Redirect("CustomerVerificationJudgementByRequestIDWebAPIView.aspx", False)
            Case 81301, 81302
                Sahassa.AML.Commonly.SessionIntendedPage = "CTRExemptionListView.aspx"
                Me.Response.Redirect("CTRExemptionListView.aspx", False)
            Case 81305
                Sahassa.AML.Commonly.SessionIntendedPage = "TransactionTypeAuxiliaryMapping.aspx"
                Me.Response.Redirect("TransactionTypeAuxiliaryMapping.aspx", False)
            Case Else
                Sahassa.AML.Commonly.SessionIntendedPage = "Default.aspx"
                Me.Response.Redirect("Default.aspx", False)
        End Select
    End Sub
End Class
