#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports Sahassa.AML
#End Region

Partial Class MsKecamatan_Delete
Inherits Parent

#Region "Function"

	Sub ChangeMultiView(index As Integer)
		MtvMsUser.ActiveViewIndex = index
	End Sub

	Private Sub BindListMapping()
		LBMapping.DataSource = Listmaping
		LBMapping.DataTextField = "NamaKecamatanNCBS"
		LBMapping.DataValueField = "IDKecamatanNCBS"
		LBMapping.DataBind()
	End Sub
  

#End Region

#Region "events..."

	Public ReadOnly Property parID As String
		Get
			Return Request.Item("ID")
		End Get
	End Property

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Not Page.IsPostBack Then
			Try
                'AuditTrailBLL.InsertAuditTrailUserAccess(SessionCurrentPage)
				Listmaping = New TList(Of MsKecamatanNCBS)
				LoadData()
			Catch ex As Exception
				LogError(ex)
				CvalPageErr.IsValid = False
				CvalPageErr.ErrorMessage = ex.Message
			End Try
		End If

	End Sub

	Private Sub LoadData()
		Dim ObjMsKecamatan As MsKecamatan = DataRepository.MsKecamatanProvider.GetPaged(MsKecamatanColumn.IDKecamatan.ToString & "=" & parID, "", 0, 1, Nothing)(0)
		 If  ObjMsKecamatan Is Nothing Then Throw New Exception("Data Not Found")
		With ObjMsKecamatan
			SafeDefaultValue = "-"
			 Try
                If .IDKotaKabupaten <> "" Then lblIDKotaKabupaten.Text = DataRepository.MsKotaKabProvider.GetPaged("IDKotaKab= '" & CInt(.IDKotaKabupaten) & "'", "", 0, 1, Nothing)(0).NamaKotaKab Else lblIDKotaKabupaten.Text = SafeDefaultValue
 Catch ex As Exception
     lblIDKotaKabupaten.Text = SafeDefaultValue
 End Try
            lblIDKotaKabupaten.Text = FormatMoney(CDbl(.IDKotaKabupaten))
lblNamaKecamatan.Text = Safe(.NamaKecamatan)

			Dim L_objMappingMsKecamatanNCBSPPATK As TList(Of MappingMsKecamatanNCBSPPATK)
			L_objMappingMsKecamatanNCBSPPATK = DataRepository.MappingMsKecamatanNCBSPPATKProvider.GetPaged(MappingMsKecamatanNCBSPPATKColumn.IDKecamatan.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)

			For Each i As MappingMsKecamatanNCBSPPATK In L_objMappingMsKecamatanNCBSPPATK
				Dim TempObj As MsKecamatanNCBS = DataRepository.MsKecamatanNCBSProvider.GetByIDKecamatanNCBS(i.PK_MappingMsKecamatanNCBSPPATK_Id)
				  If TempObj Is Nothing Then Continue For
				Listmaping.Add(TempObj)
			Next
		End With
	End Sub

	Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
		Try
			BindListMapping()
		Catch ex As Exception
			LogError(ex)
			CvalPageErr.IsValid = False
			CvalPageErr.ErrorMessage = ex.Message
		End Try
	End Sub

#End Region

#Region "Property..."

	''' <summary>
	''' Menyimpan Item untuk mapping sementara
	''' </summary>
	''' <value></value>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Property Listmaping As TList(Of MsKecamatanNCBS)
		Get
			Return Session("Listmaping.data")
		End Get
		Set(value As TList(Of MsKecamatanNCBS))
			Session("Listmaping.data") = value
		End Set
	End Property

#End Region

#Region "events..."
	Protected Sub ImageCancel_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
		Response.Redirect("MsKecamatan_View.aspx")
	End Sub

	Protected Sub imgOk_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles imgOk.Click
		Try
			Page.Validate("handle")
			If Page.IsValid Then

				' =========== Insert Header Approval
				Dim KeyHeaderApproval As Integer
				Using ObjMsKecamatan_Approval As New MsKecamatan_Approval
					With ObjMsKecamatan_Approval
						.FK_MsMode_Id = 3
						FillOrNothing(.RequestedBy, SessionPkUserId)
						FillOrNothing(.RequestedDate, Date.Now)
                        .IsUpload = False
					End With
					DataRepository.MsKecamatan_ApprovalProvider.Save(ObjMsKecamatan_Approval)
					KeyHeaderApproval = ObjMsKecamatan_Approval.PK_MsKecamatan_Approval_Id
				End Using
				'============ Insert Detail Approval
				Using objMsKecamatan_ApprovalDetail As New MsKecamatan_ApprovalDetail()
					With objMsKecamatan_ApprovalDetail
						Dim ObjMsKecamatan As MsKecamatan = DataRepository.MsKecamatanProvider.GetByIDKecamatan(parID)
                        FillOrNothing(.IDKotaKabupaten, lblIDKotaKabupaten.Text, True, oInt)
FillOrNothing(.IDKotaKabupaten, lblIDKotaKabupaten.Text,true,Oint)
FillOrNothing(.NamaKecamatan, lblNamaKecamatan.Text,true,Ovarchar)

						FillOrNothing(.IDKecamatan, ObjMsKecamatan.IDKecamatan)
						FillOrNothing(.Activation, ObjMsKecamatan.Activation)
						FillOrNothing(.CreatedDate, ObjMsKecamatan.CreatedDate)
						FillOrNothing(.CreatedBy, ObjMsKecamatan.CreatedBy)
						FillOrNothing(.FK_MsKecamatan_Approval_Id, KeyHeaderApproval)
					End With
					DataRepository.MsKecamatan_ApprovalDetailProvider.Save(objMsKecamatan_ApprovalDetail)
					Dim keyHeaderApprovalDetail As Integer = objMsKecamatan_ApprovalDetail.PK_MsKecamatan_ApprovalDetail_Id

					'========= Insert mapping item 
					Dim LobjMappingMsKecamatanNCBSPPATK_Approval_Detail As New TList(Of MappingMsKecamatanNCBSPPATK_Approval_Detail)

                    Dim L_ObjMappingMsKecamatanNCBSPPATK As TList(Of MappingMsKecamatanNCBSPPATK) = DataRepository.MappingMsKecamatanNCBSPPATKProvider.GetPaged(MappingMsKecamatanNCBSPPATKColumn.IDKecamatan.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)

					For Each objMappingMsKecamatanNCBSPPATK As MappingMsKecamatanNCBSPPATK In L_ObjMappingMsKecamatanNCBSPPATK
						Dim objMappingMsKecamatanNCBSPPATK_Approval_Detail As New MappingMsKecamatanNCBSPPATK_Approval_Detail
						With objMappingMsKecamatanNCBSPPATK_Approval_Detail
							FillOrNothing(.IDKecamatan, objMappingMsKecamatanNCBSPPATK.PK_MappingMsKecamatanNCBSPPATK_Id)
							FillOrNothing(.IDKecamatanNCBS, objMappingMsKecamatanNCBSPPATK.IDKecamatanNCBS)
							FillOrNothing(.PK_MsKecamatan_Approval_Id, KeyHeaderApproval)
							FillOrNothing(.PK_MappingMsKecamatanNCBSPPATK_Id, objMappingMsKecamatanNCBSPPATK.PK_MappingMsKecamatanNCBSPPATK_Id)
							LobjMappingMsKecamatanNCBSPPATK_Approval_Detail.Add(objMappingMsKecamatanNCBSPPATK_Approval_Detail)
						End With
						DataRepository.MappingMsKecamatanNCBSPPATK_Approval_DetailProvider.Save(LobjMappingMsKecamatanNCBSPPATK_Approval_Detail)
					Next
					'session Maping item Clear
					Listmaping.Clear()
                    Me.imgOk.Visible = False
                    Me.ImageCancel.Visible = False
					LblConfirmation.Text = "Data has been Delete and waiting for approval"
					MtvMsUser.ActiveViewIndex = 1
				End Using
			End If
		Catch ex As Exception
			LogError(ex)
			CvalPageErr.IsValid = False
			CvalPageErr.ErrorMessage = ex.Message
		End Try

	End Sub

	Protected Sub ImgBtnAdd_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAdd.Click
		Response.Redirect("MsKecamatan_View.aspx")
	End Sub

#End Region
	
End Class



