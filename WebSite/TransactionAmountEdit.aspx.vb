﻿Option Explicit On
Option Strict On
Imports AMLBLL
Imports SahassaNettier.Entities
Imports SahassaNettier.Data
Imports SahassaNettier.Services
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports Sahassa.AML.Commonly

Partial Class TransactionAmountEdit
    Inherits Parent

    Public Property ObjTransactionAmount() As TList(Of TransactionAmount)
        Get
            If Session("transactionamountadd.ObjTransactionAmount") Is Nothing Then
                Using objreturn As TList(Of TransactionAmount) = DataRepository.TransactionAmountProvider.GetPaged("Tier<>0", "Tier", 0, Integer.MaxValue, 0)
                    Session("transactionamountadd.ObjTransactionAmount") = objreturn
                End Using
            End If
            Return CType(Session("transactionamountadd.ObjTransactionAmount"), TList(Of TransactionAmount))
        End Get
        Set(ByVal value As TList(Of TransactionAmount))
            Session("transactionamountadd.ObjTransactionAmount") = value
        End Set
    End Property
    Public Property ObjEditTransactionAmount() As TransactionAmount
        Get
            Return CType(Session("ObjEditTransactionAmount.ObjTransactionAmount"), TransactionAmount)
        End Get
        Set(ByVal value As TransactionAmount)
            Session("ObjEditTransactionAmount.ObjTransactionAmount") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                'Add Event Calender
                ClearSession()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using

                BindAmount()
            End If
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message

        End Try
    End Sub

    Sub ClearSession()
        Session("TransactionAmountadd.ConfigApproval") = Nothing
        Session("transactionamountadd.objtransactionamount") = Nothing
        Session("objedittransactionamount.objtransactionamount") = Nothing
        Session("objapptransactionamount.objtransactionamount") = Nothing

    End Sub



    Public Sub IsDataValidEdit(ByVal StrTier As String, ByVal StrStartRange As String, ByVal StrEndRange As String, ByVal strToleransi As String)
        'Required Field
        If StrTier = "" Then
            Throw New Exception("Tier value is required")
        End If
        If Not Integer.TryParse(StrTier, 0) Then
            Throw New Exception("Tier must be numeric")
        End If
        If Not Double.TryParse(StrStartRange, 0) Then
            Throw New Exception("Start range must be numeric")
        End If
        If StrStartRange = "" Then
            Throw New Exception("Start range value is required")
        End If
        If Not Double.TryParse(StrEndRange, 0) Then
            Throw New Exception("End range must be numeric")
        End If
        If StrEndRange = "" Then
            Throw New Exception("End range value is required")
        End If

        'Start Range < End Range
        If CDbl(StrStartRange) >= CDbl(StrEndRange) Then
            Throw New Exception("Start Range value must be lower than End Range value")
        End If

        'Value
        ObjTransactionAmount.Sort("Tier")
        Dim objtierbefore As TransactionAmount
        Dim objtierafter As TransactionAmount
        For Each Item As TransactionAmount In ObjTransactionAmount
            If Item.Tier.GetValueOrDefault(0) <> ObjEditTransactionAmount.Tier.GetValueOrDefault(0) Then
                If Item.Tier.GetValueOrDefault(0) = CInt(StrTier) Then
                    Throw New Exception("Tier " & StrTier & " already exist")
                End If

                If Item.Tier.GetValueOrDefault(0) < CInt(StrTier) Then
                    objtierbefore = Item
                End If

                If objtierafter Is Nothing Then
                    If Item.Tier.GetValueOrDefault(0) > CInt(StrTier) Then
                        objtierafter = Item
                    End If
                End If
            End If
        Next

        If Not objtierbefore Is Nothing Then
            If CDbl(StrStartRange) <= objtierbefore.EndRange.GetValueOrDefault(0) Then
                Throw New Exception("Start Range For Tier " & StrTier & " : " & StrStartRange & " must be greater than tier " & objtierbefore.Tier.GetValueOrDefault(0) & " End Range :" & objtierbefore.EndRange.GetValueOrDefault(0))
            End If
            objtierbefore = Nothing
        End If

        If Not objtierafter Is Nothing Then
            If CDbl(StrEndRange) >= objtierafter.StartRange.GetValueOrDefault(0) Then
                Throw New Exception("End Range For Tier " & StrTier & " : " & StrEndRange & " must be lower than tier " & objtierafter.Tier.GetValueOrDefault(0) & " Start Range : " & objtierafter.StartRange.GetValueOrDefault(0))
            End If
            objtierafter = Nothing
        End If
        If Not Double.TryParse(txtToleransi.Text, 0) Then
            Throw New Exception("Toleransi Must Numeric")
        End If
    End Sub

    Public Sub IsDataValid(ByVal StrTier As String, ByVal StrStartRange As String, ByVal StrEndRange As String, ByVal strToleransi As String)
        'Required Field
        If StrTier = "" Then
            Throw New Exception("Tier value is required")
        End If
        If Not Integer.TryParse(StrTier, 0) Then
            Throw New Exception("Tier must be numeric")
        End If
        If Not Double.TryParse(StrStartRange, 0) Then
            Throw New Exception("Start range must be numeric")
        End If
        If StrStartRange = "" Then
            Throw New Exception("Start range value is required")
        End If
        If Not Double.TryParse(StrEndRange, 0) Then
            Throw New Exception("End range must be numeric")
        End If
        If StrEndRange = "" Then
            Throw New Exception("End range value is required")
        End If

        'Start Range < End Range
        If CDbl(StrStartRange) >= CDbl(StrEndRange) Then
            Throw New Exception("Start Range value must be lower than End Range value")
        End If

        'Value
        ObjTransactionAmount.Sort("Tier")
        Dim objtierbefore As TransactionAmount
        Dim objtierafter As TransactionAmount
        For Each Item As TransactionAmount In ObjTransactionAmount
            If Item.Tier.GetValueOrDefault(0) = CInt(StrTier) Then
                Throw New Exception("Tier " & StrTier & " already exist")
            End If

            If Item.Tier.GetValueOrDefault(0) < CInt(StrTier) Then
                objtierbefore = Item
            End If

            If objtierafter Is Nothing Then
                If Item.Tier.GetValueOrDefault(0) > CInt(StrTier) Then
                    objtierafter = Item
                End If
            End If
        Next

        If Not objtierbefore Is Nothing Then
            If CDbl(StrStartRange) <= objtierbefore.EndRange.GetValueOrDefault(0) Then
                Throw New Exception("Start Range For Tier " & StrTier & " : " & StrStartRange & " must be greater than tier " & objtierbefore.Tier.GetValueOrDefault(0) & " End Range :" & objtierbefore.EndRange.GetValueOrDefault(0))
            End If
            objtierbefore = Nothing
        End If

        If Not objtierafter Is Nothing Then
            If CDbl(StrEndRange) >= objtierafter.StartRange.GetValueOrDefault(0) Then
                Throw New Exception("End Range For Tier " & StrTier & " : " & StrEndRange & " must be lower than tier " & objtierafter.Tier.GetValueOrDefault(0) & " Start Range : " & objtierafter.StartRange.GetValueOrDefault(0))
            End If
            objtierafter = Nothing
        End If

        If Not Double.TryParse(txtToleransi.Text, 0) Then
            Throw New Exception("Toleransi Must Numeric")
        End If

    End Sub

    Function CekExcludeEdit(ByVal objcek As TransactionAmount) As Boolean
        If Not ObjEditTransactionAmount Is Nothing Then
            If objcek.Tier.GetValueOrDefault(0) = ObjEditTransactionAmount.Tier.GetValueOrDefault(0) Then
                Return False
            Else
                Return True
            End If
        Else
            Return True
        End If
    End Function

    Private Sub SaveEdit()
        Try
            Using ObjTransactionAmountBll As New TransactionAmountBLL
                If ObjTransactionAmountBll.SaveDirectly(Me.ObjTransactionAmount) Then
                    LblConfirmation.Text = "Success to Edit Transaction Amount."
                    MtvTransactionAmount.ActiveViewIndex = 1
                End If
            End Using
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub SaveToApproval()
        Try
            Using ObjTransactionAmountBll As New TransactionAmountBLL
                If ObjTransactionAmountBll.SaveToApproval(ObjTransactionAmount) Then
                    LblConfirmation.Text = "Transaction Amount parameter has been changed and it is currently waiting for approval."
                    MtvTransactionAmount.ActiveViewIndex = 1
                End If
            End Using
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBtnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnSave.Click
        Try
            'cek apakah sudah ada di pending approval
            Dim IntCount As Integer = 0
            DataRepository.TransactionAmount_ApprovalProvider.GetPaged("", "", 0, 1, IntCount)

            If IntCount = 0 Then
                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                    SaveEdit()
                Else
                    SaveToApproval()
                End If
            Else
                Throw New Exception("Cannot edit transaction amount because it is currently waiting for approval.")
            End If
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBackAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBackAdd.Click
        Try
            Response.Redirect("Default.aspx", False)
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBack.Click
        Try
            Response.Redirect("Default.aspx", False)
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub ClearControl()
        TxtTier.Text = ""
        TxtStartRange.Text = ""
        TxtEndRange.Text = ""
        txtToleransi.Text = ""
    End Sub

    Protected Sub ImgBtnAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAdd.Click
        Try
            ClearControl()
            MtvTransactionAmount.ActiveViewIndex = 0
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    'Protected Sub CvalHandleErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles CvalHandleErr.PreRender
    '    If CvalHandleErr.IsValid = False Then
    '        Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
    '    End If
    'End Sub

    'Protected Sub CvalPageErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles CvalPageErr.PreRender
    '    If CvalPageErr.IsValid = False Then
    '        Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
    '    End If
    'End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            If MtvTransactionAmount.ActiveViewIndex = 0 Then
                ImgBtnSave.Visible = True
                ImgBackAdd.Visible = True
            Else
                ImgBtnSave.Visible = False
                ImgBackAdd.Visible = False
            End If
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Function BindAmount() As Boolean
        Me.GrdVwTransactionAmount.DataSource = Me.ObjTransactionAmount
        Me.GrdVwTransactionAmount.DataBind()
    End Function

    Protected Sub GrdVwTransactionAmount_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GrdVwTransactionAmount.RowDataBound
        Dim PKMsAction As String = Nothing
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim objData As TransactionAmount = CType(e.Row.DataItem, TransactionAmount)
                e.Row.Cells(0).Text = objData.Tier.ToString
                e.Row.Cells(1).Text = objData.StartRange.ToString
                e.Row.Cells(2).Text = objData.EndRange.ToString

                Dim lnkButtonRemove As LinkButton = CType(e.Row.FindControl("lnkButtonRemove"), LinkButton)
                lnkButtonRemove.CommandName = "Remove"
                lnkButtonRemove.CommandArgument = objData.Tier.ToString

                Dim LinkBtnEdit As LinkButton = CType(e.Row.FindControl("LinkBtnEdit"), LinkButton)
                LinkBtnEdit.CommandName = "EditData"
                LinkBtnEdit.CommandArgument = objData.Tier.ToString
            End If
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImageAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAdd.Click
        Try
            If ObjEditTransactionAmount Is Nothing Then
                Me.IsDataValid(Me.TxtTier.Text, Me.TxtStartRange.Text, Me.TxtEndRange.Text, txtToleransi.Text)

                Dim ObjTransAmount As New TransactionAmount
                With ObjTransAmount
                    .Tier = CType(TxtTier.Text.Trim, Global.System.Nullable(Of Integer))
                    .StartRange = CType(TxtStartRange.Text.Trim, Global.System.Nullable(Of Decimal))
                    .EndRange = CType(TxtEndRange.Text.Trim, Global.System.Nullable(Of Decimal))
                    .Toleransi = CType(txtToleransi.Text.Trim, Global.System.Nullable(Of Double))

                End With

                ObjTransactionAmount.Add(ObjTransAmount)

                BindAmount()
                ClearControl()
            Else
                Me.IsDataValidEdit(Me.TxtTier.Text, Me.TxtStartRange.Text, Me.TxtEndRange.Text, txtToleransi.Text)

                ObjEditTransactionAmount.Tier = CType(TxtTier.Text.Trim, Global.System.Nullable(Of Integer))
                ObjEditTransactionAmount.StartRange = CType(TxtStartRange.Text.Trim, Global.System.Nullable(Of Decimal))
                ObjEditTransactionAmount.EndRange = CType(TxtEndRange.Text.Trim, Global.System.Nullable(Of Decimal))
                ObjEditTransactionAmount.Toleransi = CType(txtToleransi.Text.Trim, Global.System.Nullable(Of Double))
                BindAmount()
                ClearControl()
                ObjEditTransactionAmount = Nothing
                ImgBtnCancel.Visible = False
            End If

        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub GrdVwTransactionAmount_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdVwTransactionAmount.RowCommand
        If e.CommandName = "Remove" Then

            Dim idx As Integer = CInt(e.CommandArgument)
            Using ObjDeleted As TransactionAmount = ObjTransactionAmount.Find(TransactionAmountColumn.Tier, idx)
                If Not ObjDeleted Is Nothing Then
                    If Not ObjEditTransactionAmount Is Nothing Then
                        If ObjDeleted.Tier.GetValueOrDefault(0) = ObjEditTransactionAmount.Tier.GetValueOrDefault(0) Then
                            ImgBtnCancel_Click(ImgBtnCancel, Nothing)
                        End If
                    End If

                    ObjTransactionAmount.Remove(ObjDeleted)
                End If
                BindAmount()
            End Using
        ElseIf e.CommandName = "EditData" Then
            Dim idx As Integer = CInt(e.CommandArgument)

            Using ObjEdit As TransactionAmount = ObjTransactionAmount.Find(TransactionAmountColumn.Tier, idx)
                If Not ObjEdit Is Nothing Then
                    ObjEditTransactionAmount = ObjEdit
                    Loadeditdata()
                End If
            End Using
        End If
    End Sub

    Sub Loadeditdata()
        TxtTier.Text = CStr(ObjEditTransactionAmount.Tier)
        TxtStartRange.Text = CStr(ObjEditTransactionAmount.StartRange)
        TxtEndRange.Text = CStr(ObjEditTransactionAmount.EndRange)
        txtToleransi.Text = CStr(ObjEditTransactionAmount.Toleransi)
        ImgBtnCancel.Visible = True
    End Sub

    Protected Sub ImgBtnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnCancel.Click
        Try
            ClearControl()

            ObjEditTransactionAmount = Nothing
            ImgBtnCancel.Visible = False
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message

        End Try
    End Sub
End Class