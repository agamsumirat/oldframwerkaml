<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="ResikoNasabahBaruApproval.aspx.vb" Inherits="ResikoNasabahBaruApproval"
    ValidateRequest="false" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">

    <script src="script/popcalendar.js"></script>

    <span defaultbutton="ImgBtnSearch">
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tr>
                <td>
                </td>
                <td id="tdcontent" height="99%" valign="top" bgcolor="#FFFFFF">
                    <div id="divcontent" class="divcontent">
                        <table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tr>
                                <td>
                                    <img src="Images/blank.gif" width="20" height="100%" /></td>
                                <td class="divcontentinside" bgcolor="#FFFFFF">
                                    <ajax:AjaxPanel ID="AjaxPanel5" runat="server" Width="100%">
                                        <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
                                            height="72" style="border-top-style: none; border-right-style: none; border-left-style: none;
                                            border-bottom-style: none" bgcolor="#dddddd" width="100%">
                                            <tr bgcolor="#ffffff">
                                                <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                    <strong>
                                                        <img src="Images/dot_title.gif" width="17" height="17">
                                                        <asp:Label ID="Label7" runat="server" Text="Resiko Nasabah Baru Approval"></asp:Label>
                                                        <hr />
                                                    </strong>
                                                    <ajax:AjaxPanel ID="AjxMessage" runat="server">
                                                        <asp:Label ID="LblMessage" background="images/validationbground.gif" runat="server"
                                                            CssClass="validationok" Width="100%"></asp:Label>
                                                    </ajax:AjaxPanel>
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:Panel ID="Panel1" runat="server" DefaultButton="ImgBtnSearch" Width="100%">
                                            <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
                                                border="2">
                                                <tr id="searchbox">
                                                    <td colspan="2" valign="top" width="98%" bgcolor="#ffffff">
                                                        <table cellspacing="4" cellpadding="0" width="100%" border="0">
                                                            <tr>
                                                                <td class="Regtext" nowrap valign="top">
                                                                    &nbsp;Search By :</td>
                                                                <td valign="middle" nowrap>
                                                                    <ajax:AjaxPanel ID="AjaxPanel1" runat="server" Width="800px">
                                                                        <table style="height: 100%">
                                                                            <tr>
                                                                                <td colspan="3" style="height: 26px">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td nowrap style="height: 26px">
                                                                                    <asp:Label ID="LblPreparer" runat="server" Text="Preparer"></asp:Label></td>
                                                                                <td style="width: 1px;">
                                                                                    :</td>
                                                                                <td style="height: 26px">
                                                                                    <asp:TextBox ID="TxtPreparer" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td nowrap>
                                                                                    <asp:Label ID="LblEntryDate" runat="server" Text="Created Date"></asp:Label></td>
                                                                                <td style="width: 1px;">
                                                                                    :</td>
                                                                                <td>
                                                                                    <asp:TextBox ID="TxtEntryDateFrom" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox><input
                                                                                        id="popUpEntryDate" runat="server" name="popUpCalc" style="border-right: #ffffff 0px solid;
                                                                                        border-top: #ffffff 0px solid; font-size: 11px; border-left: #ffffff 0px solid;
                                                                                        border-bottom: #ffffff 0px solid; height: 17px; background-image: url(Script/Calendar/cal.gif);
                                                                                        width: 16px;" title="Click to show calendar" type="button" />
                                                                                    <asp:Label ID="Label1" runat="server" Text="Until"></asp:Label>
                                                                                    <asp:TextBox ID="TxtEntryDateUntil" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox><input
                                                                                        id="popUpEntryDateUntil" runat="server" name="popUpCalc" style="border-right: #ffffff 0px solid;
                                                                                        border-top: #ffffff 0px solid; font-size: 11px; border-left: #ffffff 0px solid;
                                                                                        border-bottom: #ffffff 0px solid; height: 17px; background-image: url(Script/Calendar/cal.gif);
                                                                                        width: 16px;" title="Click to show calendar" type="button" />&nbsp;</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="3">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="3">
                                                                                    <asp:ImageButton ID="ImgBtnSearch" TabIndex="3" runat="server" SkinID="SearchButton"
                                                                                        ImageUrl="~/Images/button/search.gif"></asp:ImageButton>
                                                                                    <asp:ImageButton ID="ImgBtnClearSearch" runat="server" ImageUrl="~/Images/button/clearsearch.gif" /></td>
                                                                            </tr>
                                                                        </table>
                                                                        &nbsp;&nbsp;&nbsp;
                                                                    </ajax:AjaxPanel>
                                                                </td>
                                                                <td valign="middle" width="99%">
                                                                    &nbsp;</td>
                                                            </tr>
                                                        </table>
                                                        <ajax:AjaxPanel ID="AjaxPanel3" runat="server">
                                                            <asp:CustomValidator ID="CvalPageErr" runat="server" Display="None"></asp:CustomValidator></ajax:AjaxPanel>
                                                        <ajax:AjaxPanel ID="Ajaxpanel2" runat="server">
                                                            <asp:CustomValidator ID="CvalHandleErr" runat="server" ValidationGroup="handle" Display="None"></asp:CustomValidator></ajax:AjaxPanel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
                                            border="2">
                                            <tr>
                                                <td bgcolor="#ffffff">
                                                    <ajax:AjaxPanel ID="AjaxPanel4" runat="server">
                                                        <asp:DataGrid ID="GridResikoNasabahBaruView" runat="server" AutoGenerateColumns="False"
                                                            Font-Size="XX-Small" BackColor="White" CellPadding="4" BorderWidth="1px" BorderStyle="None"
                                                            AllowPaging="True" Width="100%" GridLines="Vertical" AllowSorting="True" BorderColor="#DEDFDE"
                                                            ForeColor="Black">
                                                            <FooterStyle BackColor="#CCCC99"></FooterStyle>
                                                            <SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#CE5D5A"></SelectedItemStyle>
                                                            <AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
                                                            <ItemStyle BackColor="#F7F7DE"></ItemStyle>
                                                            <HeaderStyle Font-Size="XX-Small" Font-Bold="True" ForeColor="White" BackColor="#6B696B">
                                                            </HeaderStyle>
                                                            <Columns>
                                                                <asp:TemplateColumn>
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="CheckBoxExporttoExcel" runat="server"></asp:CheckBox>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Width="10px" />
                                                                </asp:TemplateColumn>
                                                                <asp:BoundColumn DataField="PK_ResikoNasabahBaru_ApprovalID" Visible="False"></asp:BoundColumn>
                                                                <asp:BoundColumn DataField="FK_MsUserID" HeaderText="Preparer" SortExpression="FK_MsUserID desc">
                                                                    <HeaderStyle Width="24%" />
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn Visible="false" >
                                                                    <HeaderStyle Wrap="False" Font-Italic="False" Font-Strikeout="False" Font-Underline="False"
                                                                        Font-Overline="False" Font-Bold="True" Width="40%"></HeaderStyle>
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="Fk_ModeId" HeaderText="Mode" SortExpression="Fk_ModeId desc">
                                                                    <ItemStyle Wrap="False" Font-Italic="False" Font-Strikeout="False" Font-Underline="False"
                                                                        Font-Overline="False" Font-Bold="False"></ItemStyle>
                                                                    <HeaderStyle Wrap="False" Font-Italic="False" Font-Strikeout="False" Font-Underline="False"
                                                                        Font-Overline="False" Font-Bold="True" Width="24%"></HeaderStyle>
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="CreatedDate" HeaderText="Created Date" SortExpression="CreatedDate  asc"
                                                                    DataFormatString="{0:dd-MM-yyyy}">
                                                                    <ItemStyle Wrap="False" Font-Italic="False" Font-Strikeout="False" Font-Underline="False"
                                                                        Font-Overline="False" Font-Bold="False"></ItemStyle>
                                                                    <HeaderStyle Width="24%" />
                                                                </asp:BoundColumn>
                                                                <asp:EditCommandColumn CancelText="Cancel" EditText="Detail" UpdateText="Update">
                                                                    <HeaderStyle Width="24%" />
                                                                </asp:EditCommandColumn>
                                                            </Columns>
                                                        </asp:DataGrid>
                                                        <asp:Label ID="LabelNoRecordFound" runat="server" Text="No record match with your criteria"
                                                            CssClass="text" Visible="False"></asp:Label></ajax:AjaxPanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="background-color: #ffffff">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td nowrap>
                                                                <ajax:AjaxPanel ID="AjaxPanel6" runat="server">
                                                                    &nbsp;<asp:CheckBox ID="CheckBoxSelectAll" runat="server" Text="Select All" AutoPostBack="True" />
                                                                    &nbsp;
                                                                </ajax:AjaxPanel>
                                                            </td>
                                                            <td width="99%">
                                                                &nbsp;&nbsp;<asp:LinkButton ID="LinkButtonExportExcel" ajaxcall="none" OnClientClick="aspnetForm.encoding = 'multipart/form-data';"
                                                                    runat="server">Export 
		        to Excel</asp:LinkButton>&nbsp;
                                                                <asp:LinkButton ID="lnkExportAllData" runat="server" ajaxcall="none" OnClientClick="aspnetForm.encoding = 'multipart/form-data';"
                                                                    Text="Export All to Excel"></asp:LinkButton></td>
                                                            <td align="right" nowrap>
                                                                &nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#ffffff">
                                                    <table id="Table3" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                        bgcolor="#ffffff" border="2">
                                                        <tr class="regtext" align="center" bgcolor="#dddddd">
                                                            <td valign="top" align="left" width="50%" bgcolor="#ffffff">
                                                                Page&nbsp;<ajax:AjaxPanel ID="AjaxPanel7" runat="server"><asp:Label ID="PageCurrentPage"
                                                                    runat="server" CssClass="regtext">0</asp:Label>&nbsp;of&nbsp;
                                                                    <asp:Label ID="PageTotalPages" runat="server" CssClass="regtext">0</asp:Label></ajax:AjaxPanel></td>
                                                            <td valign="top" align="right" width="50%" bgcolor="#ffffff">
                                                                Total Records&nbsp;
                                                                <ajax:AjaxPanel ID="AjaxPanel8" runat="server">
                                                                    <asp:Label ID="PageTotalRows" runat="server">0</asp:Label></ajax:AjaxPanel></td>
                                                        </tr>
                                                    </table>
                                                    <table id="Table4" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                        bgcolor="#ffffff" border="2">
                                                        <tr bgcolor="#ffffff">
                                                            <td class="regtext" valign="middle" align="left" colspan="11" height="7">
                                                                <hr color="#f40101" noshade size="1">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="regtext" valign="middle" align="left" width="63" bgcolor="#ffffff">
                                                                Go to page</td>
                                                            <td class="regtext" valign="middle" align="left" width="5" bgcolor="#ffffff">
                                                                <font face="Verdana, Arial, Helvetica, sans-serif" size="1">
                                                                    <ajax:AjaxPanel ID="AjaxPanel9" runat="server">
                                                                        <asp:TextBox ID="TextGoToPage" runat="server" Width="38px" CssClass="searcheditbox"></asp:TextBox></ajax:AjaxPanel>
                                                                </font>
                                                            </td>
                                                            <td class="regtext" valign="middle" align="left" bgcolor="#ffffff">
                                                                <ajax:AjaxPanel ID="AjaxPanel10" runat="server">
                                                                    <asp:ImageButton ID="ImageButtonGo" runat="server" SkinID="GoButton" ImageUrl="~/Images/button/go.gif">
                                                                    </asp:ImageButton></ajax:AjaxPanel>
                                                            </td>
                                                            <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                                                                <img height="5" src="images/first.gif" width="6">
                                                            </td>
                                                            <td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff">
                                                                <ajax:AjaxPanel ID="AjaxPanel11" runat="server">
                                                                    <asp:LinkButton ID="LinkButtonFirst" runat="server" CssClass="regtext" CommandName="First"
                                                                        OnCommand="PageNavigate">First</asp:LinkButton></ajax:AjaxPanel>
                                                            </td>
                                                            <td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff">
                                                                <img height="5" src="images/prev.gif" width="6"></td>
                                                            <td class="regtext" valign="middle" align="right" width="14" bgcolor="#ffffff">
                                                                <ajax:AjaxPanel ID="AjaxPanel12" runat="server">
                                                                    <asp:LinkButton ID="LinkButtonPrevious" runat="server" CssClass="regtext" CommandName="Prev"
                                                                        OnCommand="PageNavigate">Previous</asp:LinkButton></ajax:AjaxPanel>
                                                            </td>
                                                            <td class="regtext" valign="middle" align="right" width="60" bgcolor="#ffffff">
                                                                <ajax:AjaxPanel ID="AjaxPanel13" runat="server">
                                                                    <a class="pageNav" href="#">
                                                                        <asp:LinkButton ID="LinkButtonNext" runat="server" CssClass="regtext" CommandName="Next"
                                                                            OnCommand="PageNavigate">Next</asp:LinkButton></a></ajax:AjaxPanel></td>
                                                            <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                                                                <img height="5" src="images/next.gif" width="6"></td>
                                                            <td class="regtext" valign="middle" align="left" width="25" bgcolor="#ffffff">
                                                                <ajax:AjaxPanel ID="AjaxPanel16" runat="server">
                                                                    <asp:LinkButton ID="LinkButtonLast" runat="server" CssClass="regtext" CommandName="Last"
                                                                        OnCommand="PageNavigate">Last</asp:LinkButton></ajax:AjaxPanel>
                                                            </td>
                                                            <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                                                                <img height="5" src="images/last.gif" width="6"></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </ajax:AjaxPanel>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
</asp:Content>
