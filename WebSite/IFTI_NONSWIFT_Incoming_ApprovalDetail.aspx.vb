﻿#Region "Imports"
Option Explicit On
Imports amlbll
Imports amlbll.ValidateBLL
Imports amlbll.AuditTrailBLL
Imports SahassaNettier.Entities
Imports SahassaNettier.Data
Imports amlbll.DataType
Imports Sahassa.AML.Commonly
Imports Sahassa.AML
Imports System.Collections.Generic
Imports System.Data.SqlClient
#End Region

Partial Class IFTI_NONSWIFT_Incoming_ApprovalDetail
    Inherits Parent

#Region "Property"

    Public ReadOnly Property SetnGetUserID() As String
        Get
            Return SessionPkUserId
        End Get

    End Property

    Private Property GetTotalInsert() As Double
        Get
            Return Session("GetTotalInsert")
        End Get
        Set(ByVal value As Double)
            Session("GetTotalInsert") = value
        End Set
    End Property

    Private Property GetTotalUpdate() As Double
        Get
            Return Session("GetTotalUpdate")
        End Get
        Set(ByVal value As Double)
            Session("GetTotalUpdate") = value
        End Set
    End Property

    Public ReadOnly Property parID As String
        Get
            Return Request.Params("PK_IFTI_Approval_Id")
        End Get
    End Property

    Public Property SetIgnoreID() As String
        Get
            If Not Session("IFTI_Approval_Detail.setPK_MsIgnoreList_Approval_Id") Is Nothing Then
                Return CStr(Session("IFTI_Approval_Detail.setPK_MsIgnoreList_Approval_Id"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("IFTI_Approval_Detail.setPK_MsIgnoreList_Approval_Id") = value
        End Set
    End Property

    Public Property SetIgnoreKeyword() As String
        Get
            If Not Session("IFTI_Approval_Detail.SetIgnoreKeyword") Is Nothing Then
                Return CStr(Session("IFTI_Approval_Detail.SetIgnoreKeyword"))
            Else
                Return ""
            End If

        End Get
        Set(ByVal value As String)
            Session("IFTI_Approval_Detail.SetIgnoreKeyword") = value
        End Set
    End Property

    Private Property SetnGetSort() As String
        Get
            Return CType(IIf(Session("IFTI_Approval_Detail.Sort") Is Nothing, "", Session("IFTI_Approval_Detail.Sort")), String)
        End Get
        Set(ByVal Value As String)
            Session("IFTI_Approval_Detail.Sort") = Value
        End Set
    End Property

    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return CType(IIf(Session("IFTI_Approval_Detail.SelectedItem") Is Nothing, New ArrayList, Session("IFTI_Approval_Detail.SelectedItem")), ArrayList)
        End Get
        Set(ByVal value As ArrayList)
            Session("IFTI_Approval_Detail.SelectedItem") = value
        End Set
    End Property

    Private Property SetnGetRowTotal() As Int32
        Get
            Return CType(IIf(Session("IFTI_Approval_Detail.RowTotal") Is Nothing, 0, Session("IFTI_Approval_Detail.RowTotal")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("IFTI_Approval_Detail.RowTotal") = Value
        End Set
    End Property

    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return iTotalPages(SetnGetRowTotal)
        End Get
    End Property

    Private Property SetnGetCurrentPage() As Int32
        Get
            Return CType(IIf(Session("IFTI_Approval_Detail.CurrentPage") Is Nothing, 0, Session("IFTI_Approval_Detail.CurrentPage")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("IFTI_Approval_Detail.CurrentPage") = Value
        End Set
    End Property

    Public ReadOnly Property SetnGetBindTable(Optional ByVal AllRecord As Boolean = False) As TList(Of Ifti_Approval_Detail)
        Get
            Dim txt As TList(Of Ifti_Approval_Detail) = DataRepository.Ifti_Approval_DetailProvider.GetPaged("FK_IFTI_Approval_id = " & parID, "", 0, Integer.MaxValue, 0)
            Dim TotalDisplay As Integer = 0
            Dim strWhereClause(-1) As String
            Dim strAllWhereClause As String = ""
            If SetnGetCurrentPage > GetPageTotal - 1 And GetPageTotal - 1 > 0 Then
                SetnGetCurrentPage = GetPageTotal - 1
            End If

            ReDim Preserve strWhereClause(strWhereClause.Length)
            strWhereClause(strWhereClause.Length - 1) = "FK_IFTI_Type_ID = 4"
            
            If SetIgnoreID.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = IFTI_Approval_DetailColumn.PK_IFTI_Approval_Detail_Id.ToString & " like '%" & SetIgnoreID.Trim.Replace("'", "''") & "%'"
            End If
           
            If AllRecord = False Then
                TotalDisplay = GetDisplayedTotalRow
            Else
                TotalDisplay = Integer.MaxValue
            End If

            strAllWhereClause = String.Join(" and ", strWhereClause)
            Return DataRepository.Ifti_Approval_DetailProvider.GetPaged(strAllWhereClause, SetnGetSort, SetnGetCurrentPage, TotalDisplay, SetnGetRowTotal)

        End Get
    End Property

    Private Sub LoadIDType()
        Me.CBOSwiftInPengirimNasabah_IND_JenisIdenNew.Items.Clear()
        Me.CBOSwiftInPengirimNasabah_IND_JenisIdenNew.Items.Add("-Select-")
        Me.CBOSwiftInPengirimNasabah_IND_JenisIdenNew.AppendDataBoundItems = True
        Me.CBOSwiftInPengirimNasabah_IND_JenisIdenNew.DataSource = DataRepository.IFTI_IDTypeProvider.GetAll()
        Me.CBOSwiftInPengirimNasabah_IND_JenisIdenNew.DataTextField = "IDType"
        Me.CBOSwiftInPengirimNasabah_IND_JenisIdenNew.DataValueField = "PK_IFTI_IDType"
        Me.CBOSwiftInPengirimNasabah_IND_JenisIdenNew.DataBind()

        Me.CBOSwiftInPengirimNasabah_IND_JenisIdenOld.Items.Clear()
        Me.CBOSwiftInPengirimNasabah_IND_JenisIdenOld.Items.Add("-Select-")
        Me.CBOSwiftInPengirimNasabah_IND_JenisIdenOld.AppendDataBoundItems = True
        Me.CBOSwiftInPengirimNasabah_IND_JenisIdenOld.DataSource = DataRepository.IFTI_IDTypeProvider.GetAll()
        Me.CBOSwiftInPengirimNasabah_IND_JenisIdenOld.DataTextField = "IDType"
        Me.CBOSwiftInPengirimNasabah_IND_JenisIdenOld.DataValueField = "PK_IFTI_IDType"
        Me.CBOSwiftInPengirimNasabah_IND_JenisIdenOld.DataBind()

        Me.cbo_SwiftInpenerimaNas_Ind_jenisidentitasOld.Items.Clear()
        Me.cbo_SwiftInpenerimaNas_Ind_jenisidentitasOld.Items.Add("-Select-")
        Me.cbo_SwiftInpenerimaNas_Ind_jenisidentitasOld.AppendDataBoundItems = True
        Me.cbo_SwiftInpenerimaNas_Ind_jenisidentitasOld.DataSource = DataRepository.IFTI_IDTypeProvider.GetAll()
        Me.cbo_SwiftInpenerimaNas_Ind_jenisidentitasOld.DataTextField = "IDType"
        Me.cbo_SwiftInpenerimaNas_Ind_jenisidentitasOld.DataValueField = "PK_IFTI_IDType"
        Me.cbo_SwiftInpenerimaNas_Ind_jenisidentitasOld.DataBind()

        Me.cbo_SwiftInpenerimaNas_Ind_jenisidentitasNew.Items.Clear()
        Me.cbo_SwiftInpenerimaNas_Ind_jenisidentitasNew.Items.Add("-Select-")
        Me.cbo_SwiftInpenerimaNas_Ind_jenisidentitasNew.AppendDataBoundItems = True
        Me.cbo_SwiftInpenerimaNas_Ind_jenisidentitasNew.DataSource = DataRepository.IFTI_IDTypeProvider.GetAll()
        Me.cbo_SwiftInpenerimaNas_Ind_jenisidentitasNew.DataTextField = "IDType"
        Me.cbo_SwiftInpenerimaNas_Ind_jenisidentitasNew.DataValueField = "PK_IFTI_IDType"
        Me.cbo_SwiftInpenerimaNas_Ind_jenisidentitasNew.DataBind()

        Me.CboSwiftInPenerimaNonNasabah_JenisDokumenOld.Items.Clear()
        Me.CboSwiftInPenerimaNonNasabah_JenisDokumenOld.Items.Add("-Select-")
        Me.CboSwiftInPenerimaNonNasabah_JenisDokumenOld.AppendDataBoundItems = True
        Me.CboSwiftInPenerimaNonNasabah_JenisDokumenOld.DataSource = DataRepository.IFTI_IDTypeProvider.GetAll()
        Me.CboSwiftInPenerimaNonNasabah_JenisDokumenOld.DataTextField = "IDType"
        Me.CboSwiftInPenerimaNonNasabah_JenisDokumenOld.DataValueField = "PK_IFTI_IDType"
        Me.CboSwiftInPenerimaNonNasabah_JenisDokumenOld.DataBind()

        Me.CboSwiftInPenerimaNonNasabah_JenisDokumenNew.Items.Clear()
        Me.CboSwiftInPenerimaNonNasabah_JenisDokumenNew.Items.Add("-Select-")
        Me.CboSwiftInPenerimaNonNasabah_JenisDokumenNew.AppendDataBoundItems = True
        Me.CboSwiftInPenerimaNonNasabah_JenisDokumenNew.DataSource = DataRepository.IFTI_IDTypeProvider.GetAll()
        Me.CboSwiftInPenerimaNonNasabah_JenisDokumenNew.DataTextField = "IDType"
        Me.CboSwiftInPenerimaNonNasabah_JenisDokumenNew.DataValueField = "PK_IFTI_IDType"
        Me.CboSwiftInPenerimaNonNasabah_JenisDokumenNew.DataBind()

    End Sub

    Private _DataNonSwiftIn As IFTI_Approval_Detail
    Private PKBeneficiary As Integer
    Private ReadOnly Property DataNonSwiftIn() As IFTI_Approval_Detail
        Get
            Try
                If _DataNonSwiftIn Is Nothing Then
                    Dim otable As IFTI_Approval_Detail = DataRepository.IFTI_Approval_DetailProvider.GetPaged("FK_IFTI_Approval_id =" & parID, "", 0, Integer.MaxValue, 0)(0)
                    If Not otable Is Nothing Then
                        _DataNonSwiftIn = otable
                        Return _DataNonSwiftIn
                    Else
                        Return Nothing
                    End If

                Else
                    Return _DataNonSwiftIn
                End If
            Catch ex As Exception
                LogError(ex)
                Throw
            End Try
        End Get
    End Property
    Private _Databenefeciary As Ifti_Beneficiary
    Private ReadOnly Property Databenefeciary() As Ifti_Beneficiary
        Get
            Try
                If _Databenefeciary Is Nothing Then
                    Using otable As IFTI_Beneficiary = DataRepository.IFTI_BeneficiaryProvider.GetByPK_IFTI_Beneficiary_ID(Me.PKBeneficiary)
                        If Not otable Is Nothing Then
                            _Databenefeciary = otable
                            Return _Databenefeciary
                        Else
                            Return Nothing
                        End If
                    End Using
                Else
                    Return _Databenefeciary
                End If
            Catch ex As Exception
                LogError(ex)
                Throw
            End Try
        End Get
    End Property
    Private _DatabenefeciaryApproval As Ifti_Approval_Beneficiary
    Private ReadOnly Property DatabenefeciaryApproval() As Ifti_Approval_Beneficiary
        Get
            Try
                If _DatabenefeciaryApproval Is Nothing Then
                    Using otable As IFTI_Approval_Beneficiary = DataRepository.IFTI_Approval_BeneficiaryProvider.GetPaged("FK_IFTI_Approval_id =" & parID, "", 0, Integer.MaxValue, 0)(0)
                        If Not otable Is Nothing Then
                            _DatabenefeciaryApproval = otable
                            PKBeneficiary = otable.FK_IFTI_Beneficiary_ID
                            Return _DatabenefeciaryApproval
                        Else
                            Return Nothing
                        End If
                    End Using
                Else
                    Return _DatabenefeciaryApproval
                End If
            Catch ex As Exception
                LogError(ex)
                Throw
            End Try
        End Get
    End Property

    Private Sub bindgrid()
        Try
            If Not DataNonSwiftIn Is Nothing Then

                If Not DataNonSwiftIn.LTDLNNo = "" Then
                    SwiftInUmum_LTDNNew.Text = DataNonSwiftIn.LTDLNNo
                End If
                If Not DataNonSwiftIn.LTDLNNo_Old = "" Then
                    SwiftInUmum_LTDNOld.Text = DataNonSwiftIn.LTDLNNo_Old
                End If
                If Not DataNonSwiftIn.LTDLNNoKoreksi = "" Then
                    SwiftInUmum_LtdnKoreksiNew.Text = DataNonSwiftIn.LTDLNNoKoreksi
                End If
                If Not DataNonSwiftIn.LTDLNNoKoreksi_Old = "" Then
                    SwiftInUmum_LtdnKoreksiOld.Text = DataNonSwiftIn.LTDLNNoKoreksi_Old
                End If
                If Not DataNonSwiftIn.TanggalLaporan = "" Then
                    SwiftInUmum_TanggalLaporanNew.Text = DataNonSwiftIn.TanggalLaporan
                End If
                If Not DataNonSwiftIn.TanggalLaporan_Old = "" Then
                    SwiftInUmum_TanggalLaporanOld.Text = DataNonSwiftIn.TanggalLaporan_Old
                End If
                If Not DataNonSwiftIn.NamaPJKBankPelapor = "" Then
                    SwiftInUmum_NamaPJKBankNew.Text = DataNonSwiftIn.NamaPJKBankPelapor
                End If
                If Not DataNonSwiftIn.NamaPJKBankPelapor_Old = "" Then
                    SwiftInUmum_NamaPJKBankOld.Text = DataNonSwiftIn.NamaPJKBankPelapor_Old
                End If
                If Not DataNonSwiftIn.NamaPejabatPJKBankPelapor = "" Then
                    SwiftInUmum_NamaPejabatPJKBankNew.Text = DataNonSwiftIn.NamaPejabatPJKBankPelapor
                End If
                If Not DataNonSwiftIn.NamaPejabatPJKBankPelapor_Old = "" Then
                    SwiftInUmum_NamaPejabatPJKBankOld.Text = DataNonSwiftIn.NamaPejabatPJKBankPelapor_Old
                End If
                If Not DataNonSwiftIn.JenisLaporan = "" Then
                    RbSwiftInUmum_JenisLaporanNew.SelectedValue = DataNonSwiftIn.JenisLaporan.GetValueOrDefault
                End If
                If Not DataNonSwiftIn.JenisLaporan_Old = "" Then
                    RbSwiftInUmum_JenisLaporanOld.SelectedValue = DataNonSwiftIn.JenisLaporan_Old.GetValueOrDefault
                End If

                '++++++++++++++++++++++++++++++++++++++++PENGIRIM+++++++++++++++++++++++++++++++++++++++++++'
                If Not DataNonSwiftIn.Sender_Nasabah_INDV_NoRekening = "" Then
                    TxtNonSWIFTInPengirim_rekeningNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NoRekening
                End If
                If Not DataNonSwiftIn.Sender_Nasabah_INDV_NoRekening_Old = "" Then
                    TxtNonSWIFTInPengirim_rekeningOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NoRekening_Old
                End If

                If Not DataNonSwiftIn.Sender_FK_IFTI_NasabahType_ID = "" Then
                    Select Case DataNonSwiftIn.Sender_FK_IFTI_NasabahType_ID.GetValueOrDefault
                        Case 1
                            Me.RbPengirimNasabah_TipePengirimOld.SelectedValue = 1
                            MultiViewSWIFTOutIdenPengirim.ActiveViewIndex = 0
                            trSwiftOutPengirimTipeNasabah.visible = True
                        Case 2
                            Me.RbPengirimNasabah_TipePengirimOld.SelectedValue = 1
                            MultiViewSWIFTOutIdenPengirim.ActiveViewIndex = 0
                            trSwiftOutPengirimTipeNasabah.visible = True
                        Case 3
                            Me.RbPengirimNasabah_TipePengirimOld.SelectedValue = 2
                            MultiViewSWIFTOutIdenPengirim.ActiveViewIndex = 1
                            PengirimNonNasabah()
                    End Select
                End If

                If ObjectAntiNull(DataNonSwiftIn.Sender_FK_IFTI_NasabahType_ID_Old) = True Then
                    Select Case DataNonSwiftIn.Sender_FK_IFTI_NasabahType_ID_Old.GetValueOrDefault
                        Case 1
                            Me.RbPengirimNasabah_TipePengirimNew.SelectedValue = 1
                        Case 2
                            Me.RbPengirimNasabah_TipePengirimNew.SelectedValue = 1
                        Case 3
                            Me.RbPengirimNasabah_TipePengirimNew.SelectedValue = 2
                    End Select
                End If

                If ObjectAntiNull(DataNonSwiftIn.Sender_FK_IFTI_NasabahType_ID) = True Then
                    Select Case DataNonSwiftIn.Sender_FK_IFTI_NasabahType_ID.GetValueOrDefault
                        Case 1
                            Me.Rb_IdenPengirimNas_TipeNasabahOld.SelectedValue = 1
                            MultiViewPengirimNasabah_new.ActiveViewIndex = 0
                            PengirimNasabahNew()
                        Case 2
                            Me.Rb_IdenPengirimNas_TipeNasabahOld.SelectedValue = 2
                            MultiViewPengirimNasabah_new.ActiveViewIndex = 1
                            PengirimKorporasiNew()

                    End Select
                End If

                If ObjectAntiNull(DataNonSwiftIn.Sender_FK_IFTI_NasabahType_ID_Old) = True Then
                    Select Case DataNonSwiftIn.Sender_FK_IFTI_NasabahType_ID_Old.GetValueOrDefault
                        Case 1
                            Me.Rb_IdenPengirimNas_TipeNasabahNew.SelectedValue = 1
                            MultiViewPengirimNasabah.ActiveViewIndex = 0
                            PengirimNasabah()
                        Case 2
                            Me.Rb_IdenPengirimNas_TipeNasabahNew.SelectedValue = 2
                            MultiViewPengirimNasabah.ActiveViewIndex = 1
                            PengirimKorporasi()
                    End Select
                End If
            End If

            '+++++++++++++++++++++++++++++++++++++++++++++++PENERIMA++++++++++++++++++++++++++++++++++++++++++++++'

            txtSwiftInPenerimaNasabah_IND_RekeningNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NoRekening
            txtSwiftInPenerimaNasabah_IND_RekeningOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_NoRekening

            If ObjectAntiNull(Databenefeciary.FK_IFTI_NasabahType_ID) = True Then
                Select Case Databenefeciary.FK_IFTI_NasabahType_ID.GetValueOrDefault
                    Case 1
                        Me.RbSwiftInPenerimaNonNasabah_TipePenerimaOld.SelectedValue = 1
                        MultiViewSwiftInPenerima.ActiveViewIndex = 0
                    Case 2
                        Me.RbSwiftInPenerimaNonNasabah_TipePenerimaOld.SelectedValue = 1
                        MultiViewSwiftInPenerima.ActiveViewIndex = 0
                    Case 3
                        Me.RbSwiftInPenerimaNonNasabah_TipePenerimaOld.SelectedValue = 2
                        MultiViewSwiftInPenerima.ActiveViewIndex = 1
                        PenerimaNonNasabah()
                End Select
            End If

            If ObjectAntiNull(Databenefeciary.FK_IFTI_NasabahType_ID) = True Then
                Select Case Databenefeciary.FK_IFTI_NasabahType_ID.GetValueOrDefault
                    Case 1
                        Me.RbSwiftInPenerimaNonNasabah_TipeNasabahOld.SelectedValue = 1
                        MultiViewPenerimaAkhirNasabah.ActiveViewIndex = 0
                        PenerimaNasabah()
                    Case 2
                        Me.RbSwiftInPenerimaNonNasabah_TipeNasabahOld.SelectedValue = 2
                        MultiViewPenerimaAkhirNasabah.ActiveViewIndex = 1
                        PenerimaKorporasi()
                End Select
            End If

            If ObjectAntiNull(DatabenefeciaryApproval.FK_IFTI_NasabahType_ID) = True Then
                Select Case DatabenefeciaryApproval.FK_IFTI_NasabahType_ID.GetValueOrDefault
                    Case 1
                        Me.RbSwiftInPenerimaNonNasabah_TipePenerimaNew.SelectedValue = 1

                    Case 2
                        Me.RbSwiftInPenerimaNonNasabah_TipePenerimaNew.SelectedValue = 1

                    Case 3
                        Me.RbSwiftInPenerimaNonNasabah_TipePenerimaNew.SelectedValue = 2
                      
                End Select
            End If

            If ObjectAntiNull(DatabenefeciaryApproval.FK_IFTI_NasabahType_ID) = True Then
                Select Case DatabenefeciaryApproval.FK_IFTI_NasabahType_ID.GetValueOrDefault
                    Case 1
                        Me.RbSwiftInPenerimaNonNasabah_TipeNasabahOld.SelectedValue = 1
                        Me.MultiViewPenerimaAkhirNasabahNew.ActiveViewIndex = 0
                        PenerimaNasabahNew()
                    Case 2
                        Me.RbSwiftInPenerimaNonNasabah_TipeNasabahOld.SelectedValue = 2
                        MultiViewPenerimaAkhirNasabahNew.ActiveViewIndex = 1
                        PenerimaKorporasiNew()
                End Select
            End If
            Transaksi()

        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    Private Sub PengirimNasabah()

        '---------------------------------------------Negara-------------------------------------'
        'Dim idnegara As String = DataNonSwiftIn.Sender_Nasabah_INDV_Negara.GetValueOrDefault(0)
        Dim idnegara_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_Negara_Old.GetValueOrDefault(0)
        'Dim getnegaratype As MsNegara
        Dim getnegaratype_old As MsNegara
        ' getnegaratype = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara)
        getnegaratype_old = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara_old)

        '---------------------------------------------Negara iden-------------------------------------'
        'Dim idnegaraiden As String = DataNonSwiftIn.Sender_Nasabah_INDV_ID_Negara.GetValueOrDefault(0)
        Dim idnegaraiden_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_ID_Negara_Old.GetValueOrDefault(0)
        'Dim getnegaraidentype As MsNegara
        Dim getnegaraidentype_old As MsNegara
        ' getnegaraidentype = DataRepository.MsNegaraProvider.GetByIDNegara(idnegaraiden)
        getnegaraidentype_old = DataRepository.MsNegaraProvider.GetByIDNegara(idnegaraiden_old)

        '====================================================INDIVIDU================================================'
        'If Not DataNonSwiftIn.Sender_Nasabah_INDV_NamaLengkap = "" Then
        '    txtSwiftInPengirimNasabah_IND_namaNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NamaLengkap
        'End If
        If Not DataNonSwiftIn.Sender_Nasabah_INDV_NamaLengkap_Old = "" Then
            txtSwiftInPengirimNasabah_IND_namaOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NamaLengkap_Old
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_TanggalLahir) = True Then
        '    txtSwiftInPengirimNasabah_IND_TanggalLahirNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_TanggalLahir.GetValueOrDefault
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_TanggalLahir_Old) = True Then
            txtSwiftInPengirimNasabah_IND_TanggalLahirOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_TanggalLahir_Old.GetValueOrDefault
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_KewargaNegaraan) = True Then
        '    RbSwiftInPengirimNasabah_IND_WargaNegaraNew.SelectedValue = DataNonSwiftIn.Sender_Nasabah_INDV_KewargaNegaraan.GetValueOrDefault
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_KewargaNegaraan_Old) = True Then
            RbSwiftInPengirimNasabah_IND_WargaNegaraOld.SelectedValue = DataNonSwiftIn.Sender_Nasabah_INDV_KewargaNegaraan_Old.GetValueOrDefault
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_Negara) = True Then
        '    txtSwiftInPengirimNasabah_IND_negaraNew.Text = getnegaratype.NamaNegara
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_Negara_Old) = True Then
            txtSwiftInPengirimNasabah_IND_negaraOld.Text = getnegaratype_old.NamaNegara
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_NegaraLainnya) = True Then
        '    txtSwiftInPengirimNasabah_IND_negaraLainNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NegaraLainnya
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_NegaraLainnya_Old) = True Then
            txtSwiftInPengirimNasabah_IND_negaraLainOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NegaraLainnya_Old
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_ID_Alamat) = True Then
        '    txtSwiftInPengirimNasabah_IND_alamatIdenNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_Alamat
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_ID_Alamat_Old) = True Then
            txtSwiftInPengirimNasabah_IND_alamatIdenOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_Alamat_Old
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_ID_KotaKab) = True Then
        '    txtSwiftInPengirimNasabah_IND_NegaraKotaNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_NegaraKota
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_ID_KotaKab_Old) = True Then
            txtSwiftInPengirimNasabah_IND_NegaraKotaOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_NegaraKota_Old
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_Negara) = True Then
        '    txtSwiftInPengirimNasabah_IND_negaraIdenNew.Text = getnegaraidentype.NamaNegara
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_ID_NegaraKota_Old) = True Then
            txtSwiftInPengirimNasabah_IND_negaraIdenOld.Text = getnegaraidentype_old.NamaNegara
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_NegaraLainnya) = True Then
        '    txtSwiftInPengirimNasabah_IND_negaraLainIdenNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_NegaraLainnya
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_NegaraLainnya_Old) = True Then
            txtSwiftInPengirimNasabah_IND_negaraLainIdenOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_NegaraLainnya_Old
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_NoTelp) = True Then
        '    txtSwiftInPengirimNasabah_IND_NoTelpNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NoTelp
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_NoTelp_Old) = True Then
            txtSwiftInPengirimNasabah_IND_NoTelpOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NoTelp_Old
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_FK_IFTI_IDType) = True Then
        '    CBOSwiftInPengirimNasabah_IND_JenisIdenOld.SelectedValue = DataNonSwiftIn.Sender_Nasabah_INDV_FK_IFTI_IDType
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_FK_IFTI_IDType_Old) = True Then
            CBOSwiftInPengirimNasabah_IND_JenisIdenNew.SelectedValue = DataNonSwiftIn.Sender_Nasabah_INDV_FK_IFTI_IDType_Old
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_NomorID) = True Then
        '    txtSwiftInPengirimNasabah_IND_NomorIdenNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NomorID
        'End If
        txtSwiftInPengirimNasabah_IND_NomorIdenOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NomorID_Old
    End Sub
    Private Sub PengirimNasabahNew()

        '---------------------------------------------Negara-------------------------------------'
        Dim idnegara As String = DataNonSwiftIn.Sender_Nasabah_INDV_Negara.GetValueOrDefault(0)
        'Dim idnegara_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_Negara_Old.GetValueOrDefault(0)
        Dim getnegaratype As MsNegara
        'Dim getnegaratype_old As MsNegara
        getnegaratype = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara)
        'getnegaratype_old = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara_old)

        '---------------------------------------------Negara iden-------------------------------------'
        Dim idnegaraiden As String = DataNonSwiftIn.Sender_Nasabah_INDV_ID_Negara.GetValueOrDefault(0)
        'Dim idnegaraiden_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_ID_Negara_Old.GetValueOrDefault(0)
        Dim getnegaraidentype As MsNegara
        'Dim getnegaraidentype_old As MsNegara
        getnegaraidentype = DataRepository.MsNegaraProvider.GetByIDNegara(idnegaraiden)
        'getnegaraidentype_old = DataRepository.MsNegaraProvider.GetByIDNegara(idnegaraiden_old)

        '====================================================INDIVIDU================================================'
        If Not DataNonSwiftIn.Sender_Nasabah_INDV_NamaLengkap = "" Then
            txtSwiftInPengirimNasabah_IND_namaNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NamaLengkap
        End If
        'If Not DataNonSwiftIn.Sender_Nasabah_INDV_NamaLengkap_Old = "" Then
        '    txtSwiftInPengirimNasabah_IND_namaOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NamaLengkap_Old
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_TanggalLahir) = True Then
            txtSwiftInPengirimNasabah_IND_TanggalLahirNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_TanggalLahir.GetValueOrDefault
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_TanggalLahir_Old) = True Then
        '    txtSwiftInPengirimNasabah_IND_TanggalLahirOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_TanggalLahir_Old.GetValueOrDefault
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_KewargaNegaraan) = True Then
            RbSwiftInPengirimNasabah_IND_WargaNegaraNew.SelectedValue = DataNonSwiftIn.Sender_Nasabah_INDV_KewargaNegaraan.GetValueOrDefault
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_KewargaNegaraan_Old) = True Then
        '    RbSwiftInPengirimNasabah_IND_WargaNegaraOld.SelectedValue = DataNonSwiftIn.Sender_Nasabah_INDV_KewargaNegaraan_Old.GetValueOrDefault
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_Negara) = True Then
            txtSwiftInPengirimNasabah_IND_negaraNew.Text = getnegaratype.NamaNegara
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_Negara_Old) = True Then
        '    txtSwiftInPengirimNasabah_IND_negaraOld.Text = getnegaratype_old.NamaNegara
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_NegaraLainnya) = True Then
            txtSwiftInPengirimNasabah_IND_negaraLainNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NegaraLainnya
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_NegaraLainnya_Old) = True Then
        '    txtSwiftInPengirimNasabah_IND_negaraLainOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NegaraLainnya_Old
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_ID_Alamat) = True Then
            txtSwiftInPengirimNasabah_IND_alamatIdenNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_Alamat
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_ID_Alamat_Old) = True Then
        '    txtSwiftInPengirimNasabah_IND_alamatIdenOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_Alamat_Old
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_ID_KotaKab) = True Then
            txtSwiftInPengirimNasabah_IND_NegaraKotaNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_NegaraKota
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_ID_KotaKab_Old) = True Then
        '    txtSwiftInPengirimNasabah_IND_NegaraKotaOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_NegaraKota_Old
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_Negara) = True Then
            txtSwiftInPengirimNasabah_IND_negaraIdenNew.Text = getnegaraidentype.NamaNegara
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_Negara_Old) = True Then
        '    txtSwiftInPengirimNasabah_IND_negaraIdenOld.Text = getnegaraidentype_old.NamaNegara
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_NegaraLainnya) = True Then
            txtSwiftInPengirimNasabah_IND_negaraLainIdenNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_NegaraLainnya
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_NegaraLainnya_Old) = True Then
        '    txtSwiftInPengirimNasabah_IND_negaraLainIdenOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_NegaraLainnya_Old
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_NoTelp) = True Then
            txtSwiftInPengirimNasabah_IND_NoTelpNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NoTelp
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_NoTelp_Old) = True Then
        '    txtSwiftInPengirimNasabah_IND_NoTelpOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NoTelp_Old
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_FK_IFTI_IDType) = True Then
            CBOSwiftInPengirimNasabah_IND_JenisIdenOld.SelectedValue = DataNonSwiftIn.Sender_Nasabah_INDV_FK_IFTI_IDType
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_FK_IFTI_IDType_Old) = True Then
        '    CBOSwiftInPengirimNasabah_IND_JenisIdenNew.SelectedValue = DataNonSwiftIn.Sender_Nasabah_INDV_FK_IFTI_IDType_Old
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_NomorID) = True Then
            txtSwiftInPengirimNasabah_IND_NomorIdenNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NomorID
        End If
        'txtSwiftInPengirimNasabah_IND_NomorIdenOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NomorID_Old
    End Sub
    Private Sub PengirimKorporasi()

        '---------------------------------------------Negara-------------------------------------'
        'Dim idnegara As String = DataNonSwiftIn.Sender_Nasabah_CORP_Negara.GetValueOrDefault(0)
        Dim idnegara_old As String = DataNonSwiftIn.Sender_Nasabah_CORP_Negara_Old.GetValueOrDefault(0)
        'Dim getnegaratype As MsNegara
        Dim getnegaratype_old As MsNegara
        'getnegaratype = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara)
        getnegaratype_old = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara_old)

        '=========================================KORPORASI============================================='
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_NamaKorporasi) = True Then
        '    txtSwiftInPengirimNasabah_Korp_namaKorpNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NamaKorporasi
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_NamaKorporasi_Old) = True Then
            txtSwiftInPengirimNasabah_Korp_namaKorpOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NamaKorporasi_Old
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_AlamatLengkap) = True Then
        '    txtSwiftInPengirimNasabah_Korp_AlamatLengkapNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_AlamatLengkap
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_AlamatLengkap_Old) = True Then
            txtSwiftInPengirimNasabah_Korp_AlamatLengkapOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_AlamatLengkap_Old
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_NegaraKota) = True Then
        '    txtSwiftInPengirimNasabah_Korp_KotaNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NegaraKota
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_NegaraKota_Old) = True Then
            txtSwiftInPengirimNasabah_Korp_KotaOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NegaraKota_Old
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_Negara) = True Then
        '    txtSwiftInPengirimNasabah_Korp_NegaraNew.Text = getnegaratype.NamaNegara
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_Negara_Old) = True Then
            txtSwiftInPengirimNasabah_Korp_NegaraOld.Text = getnegaratype_old.NamaNegara
        End If
        'txtSwiftInPengirimNasabah_Korp_NegaraLainnyaKorpNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NegaraLainnya
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_NegaraLainnya_Old) = True Then
            txtSwiftInPengirimNasabah_Korp_NegaraLainnyaKorpOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NegaraLainnya_Old
        End If
        'txtSwiftInPengirimNasabah_Korp_NoTelpNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NoTelp
        txtSwiftInPengirimNasabah_Korp_NoTelpOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NoTelp_Old
    End Sub
    Private Sub PengirimKorporasiNew()

        '---------------------------------------------Negara-------------------------------------'
        Dim idnegara As String = DataNonSwiftIn.Sender_Nasabah_CORP_Negara.GetValueOrDefault(0)
        'Dim idnegara_old As String = DataNonSwiftIn.Sender_Nasabah_CORP_Negara_Old.GetValueOrDefault(0)
        Dim getnegaratype As MsNegara
        'Dim getnegaratype_old As MsNegara
        getnegaratype = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara)
        'getnegaratype_old = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara_old)

        '=========================================KORPORASI============================================='
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_NamaKorporasi) = True Then
            txtSwiftInPengirimNasabah_Korp_namaKorpNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NamaKorporasi
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_NamaKorporasi_Old) = True Then
        '    txtSwiftInPengirimNasabah_Korp_namaKorpOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NamaKorporasi_Old
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_AlamatLengkap) = True Then
            txtSwiftInPengirimNasabah_Korp_AlamatLengkapNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_AlamatLengkap
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_AlamatLengkap_Old) = True Then
        '    txtSwiftInPengirimNasabah_Korp_AlamatLengkapOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_AlamatLengkap_Old
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_NegaraKota) = True Then
            txtSwiftInPengirimNasabah_Korp_KotaNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NegaraKota
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_NegaraKota_Old) = True Then
        '    txtSwiftInPengirimNasabah_Korp_KotaOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NegaraKota_Old
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_Negara) = True Then
            txtSwiftInPengirimNasabah_Korp_NegaraNew.Text = getnegaratype.NamaNegara
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_Negara_Old) = True Then
        '    txtSwiftInPengirimNasabah_Korp_NegaraOld.Text = getnegaratype_old.NamaNegara
        'End If
        txtSwiftInPengirimNasabah_Korp_NegaraLainnyaKorpNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NegaraLainnya
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_NegaraLainnya_Old) = True Then
        '    txtSwiftInPengirimNasabah_Korp_NegaraLainnyaKorpOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NegaraLainnya_Old
        'End If
        txtSwiftInPengirimNasabah_Korp_NoTelpNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NoTelp
        'txtSwiftInPengirimNasabah_Korp_NoTelpOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NoTelp_Old
    End Sub
    Private Sub PengirimNonNasabah()

        '---------------------------------------------Negara-------------------------------------'
        Dim idnegara As String = DataNonSwiftIn.Sender_NonNasabah_ID_Negara.GetValueOrDefault(0)
        Dim idnegara_old As String = DataNonSwiftIn.Sender_NonNasabah_ID_Negara_Old.GetValueOrDefault(0)
        Dim getnegaratype As MsNegara
        Dim getnegaratype_old As MsNegara
        getnegaratype = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara)
        getnegaratype_old = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara_old)

        '==============================================NON NASABAH==========================================='
        If ObjectAntiNull(DataNonSwiftIn.Sender_NonNasabah_NomorID) = True Then
            txtSwiftInPengirimNonNasabah_rekeningNew.Text = DataNonSwiftIn.Sender_NonNasabah_NomorID
        End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_NonNasabah_NomorID_Old) = True Then
            txtSwiftInPengirimNonNasabah_rekeningOld.Text = DataNonSwiftIn.Sender_NonNasabah_NomorID_Old
        End If
        txtSwiftInPengirimNonNasabah_namabankNew.Text = DataNonSwiftIn.Sender_NonNasabah_NamaBank
        txtSwiftInPengirimNonNasabah_namabankOld.Text = DataNonSwiftIn.Sender_NonNasabah_NamaBank_Old
        txtSwiftInPengirimNonNasabah_NamaNew.Text = DataNonSwiftIn.Sender_NonNasabah_NamaLengkap
        txtSwiftInPengirimNonNasabah_NamaOld.Text = DataNonSwiftIn.Sender_NonNasabah_NamaLengkap_Old
        If ObjectAntiNull(DataNonSwiftIn.Sender_NonNasabah_TanggalLahir) = True Then
            txtSwiftInPengirimNonNasabah_TanggalLahirNew.Text = DataNonSwiftIn.Sender_NonNasabah_TanggalLahir
        End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_NonNasabah_TanggalLahir_Old) = True Then
            txtSwiftInPengirimNonNasabah_TanggalLahirOld.Text = DataNonSwiftIn.Sender_NonNasabah_TanggalLahir_Old
        End If
        txtSwiftInPengirimNonNasabah_AlamatNew.Text = DataNonSwiftIn.Sender_NonNasabah_ID_Alamat
        If ObjectAntiNull(DataNonSwiftIn.Sender_NonNasabah_ID_Alamat_Old) = True Then
            txtSwiftInPengirimNonNasabah_AlamatOld.Text = DataNonSwiftIn.Sender_NonNasabah_ID_Alamat_Old
        End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_NonNasabah_ID_KotaKab) = True Then
            txtSwiftInPengirimNonNasabah_KotaNew.Text = DataNonSwiftIn.Sender_NonNasabah_ID_NegaraBagian
        End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_NonNasabah_ID_KotaKab_Old) = True Then
            txtSwiftInPengirimNonNasabah_KotaOld.Text = DataNonSwiftIn.Sender_NonNasabah_ID_NegaraBagian_Old
        End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_NonNasabah_ID_Negara) = True Then
            txtSwiftInPengirimNonNasabah_NegaraNew.Text = getnegaratype.NamaNegara
        End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_NonNasabah_ID_Negara_Old) = True Then
            txtSwiftInPengirimNonNasabah_NegaraOld.Text = getnegaratype_old.NamaNegara
        End If
        txtSwiftInPengirimNonNasabah_negaraLainNew.Text = DataNonSwiftIn.Sender_NonNasabah_ID_NegaraLainnya
        txtSwiftInPengirimNonNasabah_negaraLainOld.Text = DataNonSwiftIn.Sender_NonNasabah_ID_NegaraLainnya_Old
        txtSwiftInPengirimNonNasabah_NoTelpNew.Text = DataNonSwiftIn.Sender_NonNasabah_NoTelp
        txtSwiftInPengirimNonNasabah_NoTelpOld.Text = DataNonSwiftIn.Sender_NonNasabah_NoTelp_Old
    End Sub
    Private Sub PenerimaNasabah()

        '---------------------------------------------Negara-------------------------------------'
        'Dim idnegara As String = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_Negara.GetValueOrDefault(0)
        Dim idnegara_old As String = Databenefeciary.Beneficiary_Nasabah_INDV_Negara.GetValueOrDefault(0)
        ' Dim getnegaratype As MsNegara
        Dim getnegaratype_old As MsNegara
        'getnegaratype = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara)
        getnegaratype_old = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara_old)

        '------------------------------------------------pekerjaan-------------------------------------'
        ' Dim idpekerjaan As String = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_Pekerjaan.GetValueOrDefault(0)
        Dim idpekerjaan_old As String = Databenefeciary.Beneficiary_Nasabah_INDV_Pekerjaan.GetValueOrDefault(0)
        'Dim getpekerjaantype As MsPekerjaan
        Dim getpekerjaantype_old As MsPekerjaan
        ' getpekerjaantype = DataRepository.MsPekerjaanProvider.GetByIDPekerjaan(idpekerjaan)
        getpekerjaantype_old = DataRepository.MsPekerjaanProvider.GetByIDPekerjaan(idpekerjaan_old)

        '------------------------------------------------Kota kab---------------------------------------------'
        'Dim idKotakab As String = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_KotaKab_Dom.GetValueOrDefault(0)
        Dim idKotakab_old As String = Databenefeciary.Beneficiary_Nasabah_INDV_ID_KotaKab_Dom.GetValueOrDefault(0)
        'Dim getKotakabtype As MsKotaKab
        Dim getKotakabtype_old As MsKotaKab
        'getKotakabtype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab)
        getKotakabtype_old = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab_old)

        '-------------------------------------------------propinsi------------------------------------------------'
        ' Dim idPropinsi As String = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Propinsi_Dom.GetValueOrDefault(0)
        Dim idPropinsi_old As String = Databenefeciary.Beneficiary_Nasabah_INDV_ID_Propinsi_Dom.GetValueOrDefault(0)
        ' Dim getPropinsitype As MsProvince
        Dim getPropinsitype_old As MsProvince
        ' getPropinsitype = DataRepository.MsProvinceProvider.GetByPk_MsProvince_Id(idPropinsi)
        getPropinsitype_old = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi_old)

        '------------------------------------------------Kota kab Iden---------------------------------------------'
        ' Dim idKotakabiden As String = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_KotaKab_Iden.GetValueOrDefault(0)
        Dim idKotakabiden_old As String = Databenefeciary.Beneficiary_Nasabah_INDV_ID_KotaKab_Iden.GetValueOrDefault(0)
        'Dim getKotakabidentype As MsKotaKab
        Dim getKotakabidentype_old As MsKotaKab
        'getKotakabidentype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab)
        getKotakabidentype_old = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakabiden_old)

        '-------------------------------------------------propinsi Iden------------------------------------------------'
        ' Dim idPropinsiiden As String = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Propinsi_Iden.GetValueOrDefault(0)
        Dim idPropinsiiden_old As String = Databenefeciary.Beneficiary_Nasabah_INDV_ID_Propinsi_Iden.GetValueOrDefault(0)
        ' Dim getPropinsiidentype As MsProvince
        Dim getPropinsiidentype_old As MsProvince
        ' getPropinsiidentype = DataRepository.MsProvinceProvider.GetByPk_MsProvince_Id(idPropinsi)
        getPropinsiidentype_old = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsiiden_old)
        
        '============================================PERORANGAN==========================================='

        ' txtSwiftInPenerimaNasabah_IND_namaNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NamaLengkap
        txtSwiftInPenerimaNasabah_IND_namaOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_NamaLengkap
        ' txtSwiftInPenerimaNasabah_IND_TanggalLahirNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_TanggalLahir
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_TanggalLahir) = True Then
            txtSwiftInPenerimaNasabah_IND_TanggalLahirOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_TanggalLahir
        End If
        'RbSwiftInPenerimaNasabah_IND_KewarganegaraanNew.SelectedValue = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_KewargaNegaraan
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_KewargaNegaraan) = True Then
            RbSwiftInPenerimaNasabah_IND_KewarganegaraanOld.SelectedValue = Databenefeciary.Beneficiary_Nasabah_INDV_KewargaNegaraan
        End If
        'txtSwiftInPenerimaNasabah_IND_negaraNew.Text = getnegaratype.NamaNegara
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_Negara) = True Then
            txtSwiftInPenerimaNasabah_IND_negaralainOld.Text = getnegaratype_old.NamaNegara
        End If
        ' txtSwiftInPenerimaNasabah_IND_negaralainNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NegaraLainnya
        txtSwiftInPenerimaNasabah_IND_negaralainOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_NegaraLainnya
        'If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_Pekerjaan) = True Then
        '    txtSwiftInPenerimaNasabah_IND_pekerjaanNew.Text = getpekerjaantype.NamaPekerjaan
        'End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_Pekerjaan) = True Then
            txtSwiftInPenerimaNasabah_IND_pekerjaanOld.Text = getpekerjaantype_old.NamaPekerjaan
        End If
        ' TxtSwiftInIdenPenerimaNas_Ind_PekerjaanlainNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_PekerjaanLainnya
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_PekerjaanLainnya) = True Then
            TxtSwiftInIdenPenerimaNas_Ind_PekerjaanlainOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_PekerjaanLainnya
        End If
        ' TxtSwiftInIdenPenerimaNas_Ind_AlamatNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Alamat_Dom
        TxtSwiftInIdenPenerimaNas_Ind_AlamatOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_ID_Alamat_Dom
        'If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_KotaKab_Dom) = True Then
        '    TxtSwiftInIdenPenerimaNas_Ind_KotaNew.Text = getKotakabtype.NamaKotaKab
        'End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_ID_KotaKab_Dom) = True Then
            TxtSwiftInIdenPenerimaNas_Ind_KotaOld.Text = getKotakabtype_old.NamaKotaKab
        End If
        'TxtSwiftInIdenPenerimaNas_Ind_kotalainNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom) = True Then
            TxtSwiftInIdenPenerimaNas_Ind_kotalainOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom
        End If
        'If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Propinsi_Dom) = True Then
        '    TxtSwiftInIdenPenerimaNas_Ind_provinsiNew.Text = getPropinsitype..Nama
        'End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_ID_Propinsi_Dom) = True Then
            TxtSwiftInIdenPenerimaNas_Ind_ProvinsiIdenOld.Text = getPropinsitype_old.Nama
        End If
        'TxtSwiftInIdenPenerimaNas_Ind_provinsiLainNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom) = True Then
            TxtSwiftInIdenPenerimaNas_Ind_provinsiLainOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom
        End If
        'TxtSwiftInIdenPenerimaNas_Ind_alamatIdentitasNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Alamat_Iden
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_ID_Alamat_Iden) = True Then
            TxtSwiftInIdenPenerimaNas_Ind_alamatIdentitasOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_ID_Alamat_Iden
        End If
        'If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_KotaKab_Iden) = True Then
        '    TxtSwiftInIdenPenerimaNas_Ind_kotaIdentitasNew.Text = getKotakabidentype.NamaKotaKab
        'End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_ID_KotaKab_Iden) = True Then
            TxtSwiftInIdenPenerimaNas_Ind_kotaIdentitasOld.Text = getKotakabidentype_old.NamaKotaKab
        End If
        'TxtSwiftInIdenPenerimaNas_Ind_KotaLainIdenNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden) = True Then
            TxtSwiftInIdenPenerimaNas_Ind_KotaLainIdenOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden
        End If
        'If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Propinsi_Iden) = True Then
        '    TxtSwiftInIdenPenerimaNas_Ind_ProvinsiIdenNew.Text = getPropinsiidentype..Nama
        'End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_ID_Propinsi_Iden) = True Then
            TxtSwiftInIdenPenerimaNas_Ind_ProvinsiIdenOld.Text = getPropinsiidentype_old.Nama
        End If
        'TxtSwiftInIdenPenerimaNas_Ind_ProvinsilainIdenNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden) = True Then
            TxtSwiftInIdenPenerimaNas_Ind_ProvinsilainIdenOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden
        End If
        'TxtISwiftIndenPenerimaNas_Ind_noTelpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NoTelp
        TxtISwiftIndenPenerimaNas_Ind_noTelpOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_NoTelp
        'If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_FK_IFTI_IDType) = True Then
        '    cbo_SwiftInpenerimaNas_Ind_jenisidentitasNew.SelectedValue = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_FK_IFTI_IDType
        'End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_FK_IFTI_IDType) = True Then
            cbo_SwiftInpenerimaNas_Ind_jenisidentitasOld.SelectedValue = Databenefeciary.Beneficiary_Nasabah_INDV_FK_IFTI_IDType
        End If
        'If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NomorID) = True Then
        '    TxtISwiftIndenPenerimaNas_Ind_noIdentitasNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NomorID
        'End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_NomorID) = True Then
            TxtISwiftIndenPenerimaNas_Ind_noIdentitasOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_NomorID
        End If
    End Sub
    Private Sub PenerimaNasabahNew()

        '---------------------------------------------Negara-------------------------------------'
        Dim idnegara As String = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_Negara.GetValueOrDefault(0)
        'Dim idnegara_old As String = Databenefeciary.Beneficiary_Nasabah_INDV_Negara.GetValueOrDefault(0)
        Dim getnegaratype As MsNegara
        'Dim getnegaratype_old As MsNegara
        getnegaratype = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara)
        'getnegaratype_old = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara_old)

        '------------------------------------------------pekerjaan-------------------------------------'
        Dim idpekerjaan As String = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_Pekerjaan.GetValueOrDefault(0)
        'Dim idpekerjaan_old As String = Databenefeciary.Beneficiary_Nasabah_INDV_Pekerjaan.GetValueOrDefault(0)
        Dim getpekerjaantype As MsPekerjaan
        ' Dim getpekerjaantype_old As MsPekerjaan
        getpekerjaantype = DataRepository.MsPekerjaanProvider.GetByIDPekerjaan(idpekerjaan)
        'getpekerjaantype_old = DataRepository.MsPekerjaanProvider.GetByIDPekerjaan(idpekerjaan_old)

        '------------------------------------------------Kota kab---------------------------------------------'
        Dim idKotakab As String = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_KotaKab_Dom.GetValueOrDefault(0)
        ' Dim idKotakab_old As String = Databenefeciary.Beneficiary_Nasabah_INDV_ID_KotaKab_Dom.GetValueOrDefault(0)
        Dim getKotakabtype As MsKotaKab
        ' Dim getKotakabtype_old As MsKotaKab
        getKotakabtype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab)
        'getKotakabtype_old = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab_old)

        '-------------------------------------------------propinsi------------------------------------------------'
        Dim idPropinsi As String = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Propinsi_Dom.GetValueOrDefault(0)
        'Dim idPropinsi_old As String = Databenefeciary.Beneficiary_Nasabah_INDV_ID_Propinsi_Dom.GetValueOrDefault(0)
        Dim getPropinsitype As MsProvince
        'Dim getPropinsitype_old As MsProvince
        getPropinsitype = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi)
        'getPropinsitype_old = DataRepository.MsProvinceProvider.GetByPk_MsProvince_Id(idPropinsi_old)

        '------------------------------------------------Kota kab Iden---------------------------------------------'
        Dim idKotakabiden As String = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_KotaKab_Iden.GetValueOrDefault(0)
        'Dim idKotakabiden_old As String = Databenefeciary.Beneficiary_Nasabah_INDV_ID_KotaKab_Iden.GetValueOrDefault(0)
        Dim getKotakabidentype As MsKotaKab
        'Dim getKotakabidentype_old As MsKotaKab
        getKotakabidentype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakabiden)
        'getKotakabidentype_old = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab_old)

        '-------------------------------------------------propinsi Iden------------------------------------------------'
        Dim idPropinsiiden As String = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Propinsi_Iden.GetValueOrDefault(0)
        ' Dim idPropinsiiden_old As String = Databenefeciary.Beneficiary_Nasabah_INDV_ID_Propinsi_Iden.GetValueOrDefault(0)
        Dim getPropinsiidentype As MsProvince
        ' Dim getPropinsiidentype_old As MsProvince
        getPropinsiidentype = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsiiden)
        'getPropinsiidentype_old = DataRepository.MsProvinceProvider.GetByPk_MsProvince_Id(idPropinsi_old)

        '============================================PERORANGAN==========================================='

        txtSwiftInPenerimaNasabah_IND_namaNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NamaLengkap
        ' txtSwiftInPenerimaNasabah_IND_namaOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_NamaLengkap
        txtSwiftInPenerimaNasabah_IND_TanggalLahirNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_TanggalLahir
        'If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_TanggalLahir) = True Then
        '    txtSwiftInPenerimaNasabah_IND_TanggalLahirOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_TanggalLahir
        'End If
        RbSwiftInPenerimaNasabah_IND_KewarganegaraanNew.SelectedValue = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_KewargaNegaraan
        'If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_KewargaNegaraan) = True Then
        '    RbSwiftInPenerimaNasabah_IND_KewarganegaraanOld.SelectedValue = Databenefeciary.Beneficiary_Nasabah_INDV_KewargaNegaraan
        'End If
        txtSwiftInPenerimaNasabah_IND_negaraNew.Text = getnegaratype.NamaNegara
        'If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_Negara) = True Then
        '    txtSwiftInPenerimaNasabah_IND_negaralainOld.Text = getnegaratype_old.NamaNegara
        'End If
        txtSwiftInPenerimaNasabah_IND_negaralainNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NegaraLainnya
        'txtSwiftInPenerimaNasabah_IND_negaralainOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_NegaraLainnya
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_Pekerjaan) = True Then
            txtSwiftInPenerimaNasabah_IND_pekerjaanNew.Text = getpekerjaantype.NamaPekerjaan
        End If
        'If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_Pekerjaan) = True Then
        '    txtSwiftInPenerimaNasabah_IND_pekerjaanOld.Text = getpekerjaantype_old.NamaPekerjaan
        'End If
        TxtSwiftInIdenPenerimaNas_Ind_PekerjaanlainNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_PekerjaanLainnya
        'If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_PekerjaanLainnya) = True Then
        '    TxtSwiftInIdenPenerimaNas_Ind_PekerjaanlainOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_PekerjaanLainnya
        'End If
        TxtSwiftInIdenPenerimaNas_Ind_AlamatNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Alamat_Dom
        ' TxtSwiftInIdenPenerimaNas_Ind_AlamatOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_ID_Alamat_Dom
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_KotaKab_Dom) = True Then
            TxtSwiftInIdenPenerimaNas_Ind_KotaNew.Text = getKotakabtype.NamaKotaKab
        End If
        'If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_ID_KotaKab_Dom) = True Then
        '    TxtSwiftInIdenPenerimaNas_Ind_KotaOld.Text = getKotakabtype_old.NamaKotaKab
        'End If
        TxtSwiftInIdenPenerimaNas_Ind_kotalainNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom
        'If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom) = True Then
        '    TxtSwiftInIdenPenerimaNas_Ind_kotalainOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom
        'End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Propinsi_Dom) = True Then
            TxtSwiftInIdenPenerimaNas_Ind_provinsiNew.Text = getPropinsitype.Nama
        End If
        'If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_ID_Propinsi_Dom) = True Then
        '    TxtSwiftInIdenPenerimaNas_Ind_ProvinsiIdenOld.Text = getPropinsitype_old..Nama
        'End If
        TxtSwiftInIdenPenerimaNas_Ind_provinsiLainNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom
        'If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom) = True Then
        '    TxtSwiftInIdenPenerimaNas_Ind_provinsiLainOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom
        'End If
        TxtSwiftInIdenPenerimaNas_Ind_alamatIdentitasNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Alamat_Iden
        'If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_ID_Alamat_Iden) = True Then
        '    TxtSwiftInIdenPenerimaNas_Ind_alamatIdentitasOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_ID_Alamat_Iden
        'End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_KotaKab_Iden) = True Then
            TxtSwiftInIdenPenerimaNas_Ind_kotaIdentitasNew.Text = getKotakabidentype.NamaKotaKab
        End If
        'If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_ID_KotaKab_Iden) = True Then
        '    TxtSwiftInIdenPenerimaNas_Ind_kotaIdentitasOld.Text = getKotakabidentype_old.NamaKotaKab
        'End If
        TxtSwiftInIdenPenerimaNas_Ind_KotaLainIdenNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden
        'If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden) = True Then
        '    TxtSwiftInIdenPenerimaNas_Ind_KotaLainIdenOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden
        'End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Propinsi_Iden) = True Then
            TxtSwiftInIdenPenerimaNas_Ind_ProvinsiIdenNew.Text = getPropinsiidentype.Nama
        End If
        'If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_ID_Propinsi_Iden) = True Then
        '    TxtSwiftInIdenPenerimaNas_Ind_ProvinsiIdenOld.Text = getPropinsiidentype_old..Nama
        'End If
        TxtSwiftInIdenPenerimaNas_Ind_ProvinsilainIdenNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden
        'If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden) = True Then
        '    TxtSwiftInIdenPenerimaNas_Ind_ProvinsilainIdenOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden
        'End If
        TxtISwiftIndenPenerimaNas_Ind_noTelpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NoTelp
        ' TxtISwiftIndenPenerimaNas_Ind_noTelpOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_NoTelp
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_FK_IFTI_IDType) = True Then
            cbo_SwiftInpenerimaNas_Ind_jenisidentitasNew.SelectedValue = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_FK_IFTI_IDType
        End If
        'If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_FK_IFTI_IDType) = True Then
        '    cbo_SwiftInpenerimaNas_Ind_jenisidentitasOld.SelectedValue = Databenefeciary.Beneficiary_Nasabah_INDV_FK_IFTI_IDType
        'End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NomorID) = True Then
            TxtISwiftIndenPenerimaNas_Ind_noIdentitasNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NomorID
        End If
        'If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_NomorID) = True Then
        '    TxtISwiftIndenPenerimaNas_Ind_noIdentitasOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_NomorID
        'End If
    End Sub
    Private Sub PenerimaKorporasi()

        '--------------------------------------- Bentuk Badan Usaha ---------------------------------------'
        'Dim idBentukBadan As String = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault(0)
        Dim idBentukBadan_old As String = Databenefeciary.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault(0)
        'Dim getBentukbadantype As MsBentukBidangUsaha
        Dim getBentukbadantype_old As MsBentukBidangUsaha
        'getBentukbadantype = DataRepository.MsBentukBidangUsahaProvider.GetByPK_BentukBidangUsaha_ID(idBentukBadan)
        getBentukbadantype_old = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(idBentukBadan_old)

        '------------------------------------------ Badan Usaha -------------------------------------------'
        ' Dim idBadan As String = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id.GetValueOrDefault(0)
        Dim idBadan_old As String = Databenefeciary.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id.GetValueOrDefault(0)
        ' Dim getbadantype As MsBidangUsaha
        Dim getbadantype_old As MsBidangUsaha
        'getbadantype = DataRepository.MsBidangUsahaProvider.GetByBidangUsahaId(idBadan)
        getbadantype_old = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(idBentukBadan_old)

        '------------------------------------------------Kota kab---------------------------------------------'
        'Dim idKotakab As String = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_KotaKab.GetValueOrDefault(0)
        Dim idKotakab_old As String = Databenefeciary.Beneficiary_Nasabah_CORP_ID_KotaKab.GetValueOrDefault(0)
        'Dim getKotakabtype As MsKotaKab
        Dim getKotakabtype_old As MsKotaKab
        ' getKotakabtype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab)
        getKotakabtype_old = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab_old)

        '-------------------------------------------------propinsi------------------------------------------------'
        ' Dim idPropinsi As String = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_Propinsi.GetValueOrDefault(0)
        Dim idPropinsi_old As String = Databenefeciary.Beneficiary_Nasabah_CORP_ID_Propinsi.GetValueOrDefault(0)
        'Dim getPropinsitype As MsProvince
        Dim getPropinsitype_old As MsProvince
        'getPropinsitype = DataRepository.MsProvinceProvider.GetByPk_MsProvince_Id(idPropinsi)
        getPropinsitype_old = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi_old)

        '====================================================KORPORASI============================================='
        'If ObjectAntiNull(getBentukbadantype.BentukBidangUsaha) = True Then
        '    txtSwiftInPenerimaNasabah_Korp_BentukBadanUsahaNew.Text = getBentukbadantype.BentukBidangUsaha
        'End If
        If ObjectAntiNull(getBentukbadantype_old.BentukBidangUsaha) = True Then
            txtSwiftInPenerimaNasabah_Korp_BentukBadanUsahaOld.Text = getBentukbadantype_old.BentukBidangUsaha
        End If
        'txtSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLainnyaNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya) = True Then
            txtSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLainnyaOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya
        End If
        'If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_NamaKorporasi) = True Then
        '    txtSwiftInPenerimaNasabah_Korp_namaKorpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_NamaKorporasi
        'End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_NamaKorporasi) = True Then
            txtSwiftInPenerimaNasabah_Korp_namaKorpOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_NamaKorporasi
        End If
        'If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id) = True Then
        '    txtSwiftInPenerimaNasabah_Korp_BidangUsahaKorpNew.Text = getbadantype.NamaBidangUsaha
        'End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id) = True Then
            txtSwiftInPenerimaNasabah_Korp_BidangUsahaKorpOld.Text = getbadantype_old.NamaBidangUsaha
        End If
        'txtSwiftInPenerimaNasabah_Korp_BidangUsahalainKorpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_BidangUsahaLainnya
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_BidangUsahaLainnya) = True Then
            txtSwiftInPenerimaNasabah_Korp_BidangUsahalainKorpOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_BidangUsahaLainnya
        End If
        'txtSwiftInPenerimaNasabah_Korp_AlamatKorpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_AlamatLengkap
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_AlamatLengkap) = True Then
            txtSwiftInPenerimaNasabah_Korp_AlamatKorpOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_AlamatLengkap
        End If
        'If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_KotaKab) = True Then
        '    txtSwiftInPenerimaNasabah_Korp_KotakabNew.Text = getKotakabtype.NamaKotaKab
        'End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_ID_KotaKab) = True Then
            txtSwiftInPenerimaNasabah_Korp_KotakabOld.Text = getKotakabtype_old.NamaKotaKab
        End If
        'txtSwiftInPenerimaNasabah_Korp_kotaLainNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_KotaKabLainnya
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_ID_KotaKabLainnya) = True Then
            txtSwiftInPenerimaNasabah_Korp_kotaLainOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_ID_KotaKabLainnya
        End If
        'If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_Propinsi) = True Then
        '    txtSwiftInPenerimaNasabah_Korp_propinsiNew.Text = getPropinsitype..Nama
        'End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_ID_Propinsi) = True Then
            txtSwiftInPenerimaNasabah_Korp_propinsiOld.Text = getPropinsitype_old.Nama
        End If
        'If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya) = True Then
        '    txtSwiftInPenerimaNasabah_Korp_propinsilainNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya
        'End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya) = True Then
            txtSwiftInPenerimaNasabah_Korp_propinsilainOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya
        End If
        'If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_NoTelp) = True Then
        '    txtSwiftInPenerimaNasabah_Korp_NoTelpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_NoTelp
        'End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_NoTelp) = True Then
            txtSwiftInPenerimaNasabah_Korp_NoTelpOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_NoTelp
        End If
    End Sub
    Private Sub PenerimaKorporasiNew()

        '--------------------------------------- Bentuk Badan Usaha ---------------------------------------'
        Dim idBentukBadan As String = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault(0)
        'Dim idBentukBadan_old As String = Databenefeciary.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault(0)
        Dim getBentukbadantype As MsBentukBidangUsaha
        'Dim getBentukbadantype_old As MsBentukBidangUsaha
        getBentukbadantype = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(idBentukBadan)
        'getBentukbadantype_old = DataRepository.MsBentukBidangUsahaProvider.GetByPK_BentukBidangUsaha_ID(idBentukBadan)

        '------------------------------------------ Badan Usaha -------------------------------------------'
        Dim idBadan As String = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id.GetValueOrDefault(0)
        'Dim idBadan_old As String = Databenefeciary.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id.GetValueOrDefault(0)
        Dim getbadantype As MsBidangUsaha
        'Dim getbadantype_old As MsBidangUsaha
        getbadantype = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(idBadan)
        'getbadantype_old = DataRepository.MsBidangUsahaProvider.GetByBidangUsahaId(idBadan)

        '------------------------------------------------Kota kab---------------------------------------------'
        Dim idKotakab As String = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_KotaKab.GetValueOrDefault(0)
        'Dim idKotakab_old As String = Databenefeciary.Beneficiary_Nasabah_CORP_ID_KotaKab.GetValueOrDefault(0)
        Dim getKotakabtype As MsKotaKab
        ' Dim getKotakabtype_old As MsKotaKab
        getKotakabtype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab)
        'getKotakabtype_old = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab_old)

        '-------------------------------------------------propinsi------------------------------------------------'
        Dim idPropinsi As String = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_Propinsi.GetValueOrDefault(0)
        'Dim idPropinsi_old As String = Databenefeciary.Beneficiary_Nasabah_CORP_ID_Propinsi.GetValueOrDefault(0)
        Dim getPropinsitype As MsProvince
        'Dim getPropinsitype_old As MsProvince
        getPropinsitype = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi)
        'getPropinsitype_old = DataRepository.MsProvinceProvider.GetByPk_MsProvince_Id(idPropinsi_old)

        '====================================================KORPORASI============================================='
        If ObjectAntiNull(getBentukbadantype.BentukBidangUsaha) = True Then
            txtSwiftInPenerimaNasabah_Korp_BentukBadanUsahaNew.Text = getBentukbadantype.BentukBidangUsaha
        End If
        'If ObjectAntiNull(getBentukbadantype_old.BentukBidangUsaha) = True Then
        '    txtSwiftInPenerimaNasabah_Korp_BentukBadanUsahaOld.Text = getBentukbadantype_old.BentukBidangUsaha
        'End If
        txtSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLainnyaNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya
        'If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya) = True Then
        '    txtSwiftInPenerimaNasabah_Korp_BentukBadanUsahaLainnyaOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya
        'End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_NamaKorporasi) = True Then
            txtSwiftInPenerimaNasabah_Korp_namaKorpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_NamaKorporasi
        End If
        'If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_NamaKorporasi) = True Then
        '    txtSwiftInPenerimaNasabah_Korp_namaKorpOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_NamaKorporasi
        'End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id) = True Then
            txtSwiftInPenerimaNasabah_Korp_BidangUsahaKorpNew.Text = getbadantype.NamaBidangUsaha
        End If
        'If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id) = True Then
        '    txtSwiftInPenerimaNasabah_Korp_BidangUsahaKorpOld.Text = getbadantype_old.NamaBidangUsaha
        'End If
        txtSwiftInPenerimaNasabah_Korp_BidangUsahalainKorpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_BidangUsahaLainnya
        'If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_BidangUsahaLainnya) = True Then
        '    txtSwiftInPenerimaNasabah_Korp_BidangUsahalainKorpOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_BidangUsahaLainnya
        'End If
        txtSwiftInPenerimaNasabah_Korp_AlamatKorpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_AlamatLengkap
        'If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_AlamatLengkap) = True Then
        '    txtSwiftInPenerimaNasabah_Korp_AlamatKorpOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_AlamatLengkap
        'End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_KotaKab) = True Then
            txtSwiftInPenerimaNasabah_Korp_KotakabNew.Text = getKotakabtype.NamaKotaKab
        End If
        'If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_ID_KotaKab) = True Then
        '    txtSwiftInPenerimaNasabah_Korp_KotakabOld.Text = getKotakabtype_old.NamaKotaKab
        'End If
        txtSwiftInPenerimaNasabah_Korp_kotaLainNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_KotaKabLainnya
        'If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_ID_KotaKabLainnya) = True Then
        '    txtSwiftInPenerimaNasabah_Korp_kotaLainOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_ID_KotaKabLainnya
        'End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_Propinsi) = True Then
            txtSwiftInPenerimaNasabah_Korp_propinsiNew.Text = getPropinsitype.Nama
        End If
        'If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_ID_Propinsi) = True Then
        '    txtSwiftInPenerimaNasabah_Korp_propinsiOld.Text = getPropinsitype_old..Nama
        'End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya) = True Then
            txtSwiftInPenerimaNasabah_Korp_propinsilainNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya
        End If
        'If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya) = True Then
        '    txtSwiftInPenerimaNasabah_Korp_propinsilainOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya
        'End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_NoTelp) = True Then
            txtSwiftInPenerimaNasabah_Korp_NoTelpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_NoTelp
        End If
        'If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_NoTelp) = True Then
        '    txtSwiftInPenerimaNasabah_Korp_NoTelpOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_NoTelp
        'End If
    End Sub
    Private Sub PenerimaNonNasabah()
        '==================================================NON NASABAH=========================================='
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_NonNasabah_KodeRahasia) = True Then
            TxtSwiftInPenerimaNonNasabah_KodeRahasaNew.Text = DatabenefeciaryApproval.Beneficiary_NonNasabah_KodeRahasia
        End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_NonNasabah_KodeRahasia) = True Then
            TxtSwiftInPenerimaNonNasabah_KodeRahasaOld.Text = Databenefeciary.Beneficiary_NonNasabah_KodeRahasia
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_NonNasabah_NoRekening) = True Then
            TxtSwiftInPenerimaNonNasabah_NoRekNew.Text = DatabenefeciaryApproval.Beneficiary_NonNasabah_NoRekening
        End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_NonNasabah_NoRekening) = True Then
            TxtSwiftInPenerimaNonNasabah_NoRekOld.Text = Databenefeciary.Beneficiary_NonNasabah_NoRekening
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_NonNasabah_NamaBank) = True Then
            TxtSwiftInPenerimaNonNasabah_namaBankNew.Text = DatabenefeciaryApproval.Beneficiary_NonNasabah_NamaBank
        End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_NonNasabah_NamaBank) = True Then
            TxtSwiftInPenerimaNonNasabah_namaBankOld.Text = Databenefeciary.Beneficiary_NonNasabah_NamaBank
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_NonNasabah_NamaLengkap) = True Then
            TxtSwiftInPenerimaNonNasabah_namaNew.Text = DatabenefeciaryApproval.Beneficiary_NonNasabah_NamaLengkap
        End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_NonNasabah_NamaLengkap) = True Then
            TxtSwiftInPenerimaNonNasabah_namaOld.Text = Databenefeciary.Beneficiary_NonNasabah_NamaLengkap
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_NonNasabah_TanggalLahir) = True Then
            TxtSwiftInPenerimaNonNasabah_TanggalLahirNew.Text = DatabenefeciaryApproval.Beneficiary_NonNasabah_TanggalLahir
        End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_NonNasabah_TanggalLahir) = True Then
            TxtSwiftInPenerimaNonNasabah_TanggalLahirOld.Text = Databenefeciary.Beneficiary_NonNasabah_TanggalLahir
        End If
        TxtSwiftInPenerimaNonNasabah_alamatidenNew.Text = DatabenefeciaryApproval.Beneficiary_NonNasabah_ID_Alamat
        If ObjectAntiNull(Databenefeciary.Beneficiary_NonNasabah_ID_Alamat) = True Then
            TxtSwiftInPenerimaNonNasabah_alamatidenOld.Text = Databenefeciary.Beneficiary_NonNasabah_ID_Alamat
        End If
        TxtSwiftInPenerimaNonNasabah_NoTeleponNew.Text = DatabenefeciaryApproval.Beneficiary_NonNasabah_NoTelp
        TxtSwiftInPenerimaNonNasabah_NoTeleponOld.Text = Databenefeciary.Beneficiary_NonNasabah_NoTelp
        If Not IsNothing(Databenefeciary.Beneficiary_NonNasabah_FK_IFTI_IDType) Then
            CboSwiftInPenerimaNonNasabah_JenisDokumenOld.SelectedValue = Databenefeciary.Beneficiary_NonNasabah_FK_IFTI_IDType.GetValueOrDefault(0)
        End If
        If Not IsNothing(DatabenefeciaryApproval.Beneficiary_NonNasabah_FK_IFTI_IDType) Then
            CboSwiftInPenerimaNonNasabah_JenisDokumenNew.SelectedValue = DatabenefeciaryApproval.Beneficiary_NonNasabah_FK_IFTI_IDType.GetValueOrDefault(0)
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_NonNasabah_NomorID) = True Then
            TxtSwiftInPenerimaNonNasabah_NomorIdenNew.Text = DatabenefeciaryApproval.Beneficiary_NonNasabah_NomorID
        End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_NonNasabah_NomorID) = True Then
            TxtSwiftInPenerimaNonNasabah_NomorIdenOld.Text = Databenefeciary.Beneficiary_NonNasabah_NomorID
        End If
    End Sub
    Private Sub Transaksi()
        '+++++++++++++++++++++++++++++++++++++++++++++TRANSAKSI+++++++++++++++++++++++++++++++++++++++++++++'
        Dim idCurrent As String = DataNonSwiftIn.ValueDate_FK_Currency_ID.GetValueOrDefault(0)
        Dim idcurrent_old As String = DataNonSwiftIn.ValueDate_FK_Currency_ID_Old.GetValueOrDefault(0)
        Dim getbadantype As MsCurrency
        Dim getbadantype_old As MsCurrency
        getbadantype = DataRepository.MsCurrencyProvider.GetByIdCurrency(idCurrent)
        getbadantype_old = DataRepository.MsCurrencyProvider.GetByIdCurrency(idcurrent_old)


        Dim idCurrent1 As String = DataNonSwiftIn.Instructed_Currency.GetValueOrDefault(0)
        Dim idcurrent1_old As String = DataNonSwiftIn.Instructed_Currency_Old.GetValueOrDefault(0)
        Dim getbadantype1 As MsCurrency
        Dim getbadantype1_old As MsCurrency
        getbadantype1 = DataRepository.MsCurrencyProvider.GetByIdCurrency(idCurrent1)
        getbadantype1_old = DataRepository.MsCurrencyProvider.GetByIdCurrency(idcurrent1_old)

        Transaksi_SwiftIntanggalNew.Text = getStringFieldValue(DataNonSwiftIn.TanggalTransaksi)
        Transaksi_SwiftIntanggalOld.Text = getStringFieldValue(DataNonSwiftIn.TanggalTransaksi_Old)
        Transaksi_SwiftInkantorCabangPengirimNew.Text = getStringFieldValue(DataNonSwiftIn.KantorCabangPenyelengaraPengirimAsal)
        Transaksi_SwiftInkantorCabangPengirimOld.Text = getStringFieldValue(DataNonSwiftIn.KantorCabangPenyelengaraPengirimAsal_Old)
        Transaksi_SwiftInValueTanggalTransaksiNew.Text = getStringFieldValue(DataNonSwiftIn.TanggalTransaksi)
        Transaksi_SwiftInValueTanggalTransaksiOld.Text = getStringFieldValue(DataNonSwiftIn.TanggalTransaksi_Old)

        TxtnilaitransaksiNew.Text = getStringFieldValue(DataNonSwiftIn.ValueDate_NilaiTransaksi)
        TxtnilaitransaksiOld.Text = getStringFieldValue(DataNonSwiftIn.ValueDate_NilaiTransaksi_Old)

        If ObjectAntiNull(DataNonSwiftIn.ValueDate_FK_Currency_ID) = True Then
            Transaksi_SwiftInMataUangTransaksiNew.Text = getStringFieldValue(getbadantype.Name)
        End If
        If ObjectAntiNull(DataNonSwiftIn.ValueDate_FK_Currency_ID_Old) = True Then
            Transaksi_SwiftInMataUangTransaksiOld.Text = getStringFieldValue(getbadantype_old.Name)
        End If

        Transaksi_SwiftInMataUangTransaksiLainnyaNew.Text = getStringFieldValue(DataNonSwiftIn.ValueDate_CurrencyLainnya)
        Transaksi_SwiftInMataUangTransaksiLainnyaOld.Text = getStringFieldValue(DataNonSwiftIn.ValueDate_CurrencyLainnya_Old)
        Transaksi_SwiftInAmountdalamRupiahNew.Text = getStringFieldValue(DataNonSwiftIn.ValueDate_NilaiTransaksiIDR)
        Transaksi_SwiftInAmountdalamRupiahOld.Text = getStringFieldValue(DataNonSwiftIn.ValueDate_NilaiTransaksiIDR_Old)
        If ObjectAntiNull(DataNonSwiftIn.Instructed_Currency) = True Then
            Transaksi_SwiftIncurrencyNew.Text = getStringFieldValue(getbadantype1.Code)
        End If
        If ObjectAntiNull(DataNonSwiftIn.Instructed_Currency_Old) = True Then
            Transaksi_SwiftIncurrencyOld.Text = getStringFieldValue(getbadantype1_old.Code)
        End If

        Transaksi_SwiftIncurrencyLainnyaNew.Text = getStringFieldValue(DataNonSwiftIn.Instructed_CurrencyLainnya)
        Transaksi_SwiftIncurrencyLainnyaOld.Text = getStringFieldValue(DataNonSwiftIn.Instructed_CurrencyLainnya_Old)
        Transaksi_SwiftIninstructedAmountNew.Text = getStringFieldValue(DataNonSwiftIn.Instructed_Amount)
        Transaksi_SwiftIninstructedAmountOld.Text = getStringFieldValue(DataNonSwiftIn.Instructed_Amount_Old)
        Transaksi_SwiftInTujuanTransaksiNew.Text = getStringFieldValue(DataNonSwiftIn.TujuanTransaksi)
        Transaksi_SwiftInTujuanTransaksiOld.Text = getStringFieldValue(DataNonSwiftIn.TujuanTransaksi_Old)
        Transaksi_SwiftInSumberPenggunaanDanaNew.Text = getStringFieldValue(DataNonSwiftIn.SumberPenggunaanDana)
        Transaksi_SwiftInSumberPenggunaanDanaOld.Text = getStringFieldValue(DataNonSwiftIn.SumberPenggunaanDana_Old)
    End Sub
#End Region

#Region "Function"
    Private Sub ClearThisPageSessions()

        SetnGetRowTotal = Nothing
        SetnGetSort = Nothing
        SetnGetCurrentPage = 0
    End Sub
    Sub HideControl()
        BtnReject.Visible = False
        BtnSave.Visible = False
        BtnCancel.Visible = False
    End Sub
    Private Sub LoadData()
        Dim PkObject As String
        Dim ObjIFTI_Approval As vw_IFTI_Approval = DataRepository.vw_IFTI_ApprovalProvider.GetPaged("PK_IFTI_Approval_id = " & parID, "", 0, Integer.MaxValue, 0)(0)
        With ObjIFTI_Approval
            SafeDefaultValue = "-"
            txtMsUser_StaffName.Text = .RequestedBy
            txtRequestDate.Text = .RequestedDate

            'other info
            Dim Omsuser As User
            Omsuser = DataRepository.UserProvider.GetBypkUserID(.RequestedBy)
            If Omsuser IsNot Nothing Then
                txtMsUser_StaffName.Text = Omsuser.UserName
            Else
                txtMsUser_StaffName.Text = SafeDefaultValue
            End If

            Omsuser = DataRepository.UserProvider.GetBypkUserID(.RequestedBy)
            If Omsuser IsNot Nothing Then
                txtMsUser_StaffName.Text = Omsuser.UserName
            Else
                txtMsUser_StaffName.Text = SafeDefaultValue
            End If
        End With
        PkObject = ObjIFTI_Approval.PK_IFTI_Approval_Id

    End Sub
#End Region

#Region "Event"

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        Response.Redirect("IFTI_Approvals.aspx")
    End Sub

    Protected Sub BtnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReject.Click
        Try
            Dim Key_ApprovalID As String = "0"
            Dim Idx As IFTI_Approval_Detail = DataRepository.IFTI_Approval_DetailProvider.GetPaged("FK_IFTI_Approval_id = " & parID, "", 0, Integer.MaxValue, 0)(0)
            '----------------------------------------------------------------------------------------------------------------------------------------------------------------
            Dim IFTI_ApprovalDetail As IFTI_Approval_Detail
            Dim IFTI_Approval As IFTI_Approval
            Dim ifti_appBenef As IFTI_Approval_Beneficiary
            'Get IFTI Approval detail
            IFTI_ApprovalDetail = DataRepository.IFTI_Approval_DetailProvider.GetPaged( _
                IFTI_Approval_DetailColumn.PK_IFTI_Approval_Detail_Id.ToString & "=" & Idx.PK_IFTI_Approval_Detail_Id.ToString, _
                "", 0, Integer.MaxValue, Nothing)(0)
            'Get IFTI Approval
            IFTI_Approval = DataRepository.IFTI_ApprovalProvider.GetPaged( _
                IFTI_ApprovalColumn.PK_IFTI_Approval_Id.ToString & "=" & Idx.FK_IFTI_Approval_Id.ToString, _
                "", 0, Integer.MaxValue, Nothing)(0)
            Key_ApprovalID = IFTI_ApprovalDetail.PK_IFTI_ID
            ifti_appBenef = DataRepository.IFTI_Approval_BeneficiaryProvider.GetPaged( _
                  IFTI_Approval_BeneficiaryColumn.FK_IFTI_Approval_Id.ToString & "=" & Idx.FK_IFTI_Approval_Id.ToString, _
                  "", 0, Integer.MaxValue, Nothing)(0)
            'Get Key IFTI
            Dim IFTIID As String = IFTI_ApprovalDetail.PK_IFTI_ID

            'delete IFTI_approval and IFTI_ApprovalDetail

            DataRepository.IFTI_Approval_DetailProvider.Delete(IFTI_ApprovalDetail)
            DataRepository.IFTI_ApprovalProvider.Delete(IFTI_Approval)
            DataRepository.IFTI_Approval_BeneficiaryProvider.Delete(ifti_appBenef)
           
            'MtvMsUser.ActiveViewIndex = 1
            BtnCancel.Visible = False

            MultiView1.ActiveViewIndex = 1
            LblConfirmation.Text = "Rejected Success"

            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = "Rejected Success"
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    Public ReadOnly Property SetnGetBindTableBenef(Optional ByVal AllRecord As Boolean = False) As TList(Of IFTI_Approval_Beneficiary)
        Get
            Dim TotalDisplay As Integer = 0
            Dim strWhereClause(-1) As String
            Dim strAllWhereClause As String = ""
            If SetnGetCurrentPage > GetPageTotal - 1 And GetPageTotal - 1 > 0 Then
                SetnGetCurrentPage = GetPageTotal - 1
            End If

            ReDim Preserve strWhereClause(strWhereClause.Length)
            strWhereClause(strWhereClause.Length - 1) = "FK_IFTI_Approval_id=" & parID

            If AllRecord = False Then
                TotalDisplay = GetDisplayedTotalRow
            Else
                TotalDisplay = Integer.MaxValue
            End If

            strAllWhereClause = String.Join(" and ", strWhereClause)
            Return DataRepository.IFTI_Approval_BeneficiaryProvider.GetPaged(strAllWhereClause, SetnGetSort, SetnGetCurrentPage, TotalDisplay, SetnGetRowTotal)

        End Get

    End Property
    Private Sub AcceptDeleteBeneficiary()
        Dim otrans As TransactionManager = Nothing
        Try
            otrans = New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)
            otrans.BeginTransaction()
            For Each idx As IFTI_Approval_Beneficiary In SetnGetBindTableBenef
                Dim objintFK_IFTI_Beneficiary_ID As Integer = 0
                If idx.FK_IFTI_Beneficiary_ID.ToString = "" Then
                    objintFK_IFTI_Beneficiary_ID = 0
                Else
                    objintFK_IFTI_Beneficiary_ID = idx.FK_IFTI_Beneficiary_ID.ToString
                End If
                Dim countID As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("PK_IFTI_Beneficiary_ID = " & objintFK_IFTI_Beneficiary_ID, "", 0, Integer.MaxValue, 0)
                If countID.Count = 1 Then
                    Dim benefapp As IFTI_Beneficiary = DataRepository.IFTI_BeneficiaryProvider.GetPaged("PK_IFTI_Beneficiary_ID = " & objintFK_IFTI_Beneficiary_ID, "", 0, Integer.MaxValue, 0)(0)
                    With benefapp
                        .FK_IFTI_ID = idx.FK_IFTI_ID
                        .FK_IFTI_NasabahType_ID = idx.FK_IFTI_NasabahType_ID
                        .Beneficiary_Nasabah_INDV_NoRekening = idx.Beneficiary_Nasabah_INDV_NoRekening
                        .Beneficiary_Nasabah_INDV_NamaLengkap = idx.Beneficiary_Nasabah_INDV_NamaLengkap
                        .Beneficiary_Nasabah_INDV_TanggalLahir = idx.Beneficiary_Nasabah_INDV_TanggalLahir
                        .Beneficiary_Nasabah_INDV_KewargaNegaraan = idx.Beneficiary_Nasabah_INDV_KewargaNegaraan
                        .Beneficiary_Nasabah_INDV_Pekerjaan = idx.Beneficiary_Nasabah_INDV_Pekerjaan
                        .Beneficiary_Nasabah_INDV_PekerjaanLainnya = idx.Beneficiary_Nasabah_INDV_PekerjaanLainnya
                        .Beneficiary_Nasabah_INDV_Negara = idx.Beneficiary_Nasabah_INDV_Negara
                        .Beneficiary_Nasabah_INDV_NegaraLainnya = idx.Beneficiary_Nasabah_INDV_NegaraLainnya
                        .Beneficiary_Nasabah_INDV_ID_Alamat_Dom = idx.Beneficiary_Nasabah_INDV_ID_Alamat_Dom
                        .Beneficiary_Nasabah_INDV_ID_KotaKab_Dom = idx.Beneficiary_Nasabah_INDV_ID_KotaKab_Dom
                        .Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom = idx.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom
                        .Beneficiary_Nasabah_INDV_ID_Propinsi_Dom = idx.Beneficiary_Nasabah_INDV_ID_Propinsi_Dom
                        .Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom = idx.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom
                        .Beneficiary_Nasabah_INDV_ID_Alamat_Iden = idx.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom
                        .Beneficiary_Nasabah_INDV_ID_KotaKab_Iden = idx.Beneficiary_Nasabah_INDV_ID_KotaKab_Iden
                        .Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden = idx.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden
                        .Beneficiary_Nasabah_INDV_ID_Propinsi_Iden = idx.Beneficiary_Nasabah_INDV_ID_Propinsi_Iden
                        .Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden = idx.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden
                        .Beneficiary_Nasabah_INDV_ID_NegaraBagian = idx.Beneficiary_Nasabah_INDV_ID_NegaraBagian
                        .Beneficiary_Nasabah_INDV_ID_Negara = idx.Beneficiary_Nasabah_INDV_ID_Negara
                        .Beneficiary_Nasabah_INDV_ID_NegaraLainnya = idx.Beneficiary_Nasabah_INDV_ID_NegaraLainnya
                        .Beneficiary_Nasabah_INDV_NoTelp = idx.Beneficiary_Nasabah_INDV_NoTelp
                        .Beneficiary_Nasabah_INDV_FK_IFTI_IDType = idx.Beneficiary_Nasabah_INDV_FK_IFTI_IDType
                        .Beneficiary_Nasabah_INDV_NomorID = idx.Beneficiary_Nasabah_INDV_NomorID
                        .Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan = idx.Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan
                        .Beneficiary_Nasabah_CORP_NoRekening = idx.Beneficiary_Nasabah_CORP_NoRekening
                        .Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id = idx.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id
                        .Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya = idx.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya
                        .Beneficiary_Nasabah_CORP_NamaKorporasi = idx.Beneficiary_Nasabah_CORP_NamaKorporasi
                        .Beneficiary_Nasabah_CORP_NoTelp = idx.Beneficiary_Nasabah_CORP_NoTelp
                        .Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan = idx.Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan
                        .Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id = idx.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id
                        .Beneficiary_Nasabah_CORP_BidangUsahaLainnya = idx.Beneficiary_Nasabah_CORP_BidangUsahaLainnya
                        .Beneficiary_Nasabah_CORP_AlamatLengkap = idx.Beneficiary_Nasabah_CORP_AlamatLengkap
                        .Beneficiary_Nasabah_CORP_ID_KotaKab = idx.Beneficiary_Nasabah_CORP_ID_KotaKab
                        .Beneficiary_Nasabah_CORP_ID_KotaKabLainnya = idx.Beneficiary_Nasabah_CORP_ID_KotaKabLainnya
                        .Beneficiary_Nasabah_CORP_ID_Propinsi = idx.Beneficiary_Nasabah_CORP_ID_Propinsi
                        .Beneficiary_Nasabah_CORP_ID_PropinsiLainnya = idx.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya
                        .Beneficiary_Nasabah_CORP_ID_NegaraBagian = idx.Beneficiary_Nasabah_CORP_ID_NegaraBagian
                        .Beneficiary_Nasabah_CORP_ID_Negara = idx.Beneficiary_Nasabah_CORP_ID_Negara
                        .Beneficiary_Nasabah_CORP_ID_NegaraLainnya = idx.Beneficiary_Nasabah_CORP_ID_NegaraLainnya
                        .Beneficiary_NonNasabah_NoRekening = idx.Beneficiary_NonNasabah_NoRekening
                        .Beneficiary_NonNasabah_KodeRahasia = idx.Beneficiary_NonNasabah_KodeRahasia
                        .Beneficiary_NonNasabah_NamaLengkap = idx.Beneficiary_NonNasabah_NamaLengkap
                        .Beneficiary_NonNasabah_NamaBank = idx.Beneficiary_NonNasabah_NamaBank
                        .Beneficiary_NonNasabah_TanggalLahir = idx.Beneficiary_NonNasabah_TanggalLahir
                        .Beneficiary_NonNasabah_NoTelp = idx.Beneficiary_NonNasabah_NoTelp
                        .Beneficiary_NonNasabah_NilaiTransaksikeuangan = idx.Beneficiary_NonNasabah_NilaiTransaksikeuangan
                        .Beneficiary_NonNasabah_ID_Alamat = idx.Beneficiary_NonNasabah_ID_Alamat
                        .Beneficiary_NonNasabah_ID_NegaraBagian = idx.Beneficiary_NonNasabah_ID_NegaraBagian
                        .Beneficiary_NonNasabah_ID_Negara = idx.Beneficiary_NonNasabah_ID_Negara
                        .Beneficiary_NonNasabah_ID_NegaraLainnya = idx.Beneficiary_NonNasabah_ID_NegaraLainnya
                        .Beneficiary_NonNasabah_FK_IFTI_IDType = idx.Beneficiary_NonNasabah_FK_IFTI_IDType
                        .Beneficiary_NonNasabah_NomorID = idx.Beneficiary_NonNasabah_NomorID
                    End With
                    DataRepository.IFTI_BeneficiaryProvider.Save(otrans, benefapp)

                Else
                    Dim newbenef As New IFTI_Beneficiary
                    With newbenef
                        .FK_IFTI_ID = idx.FK_IFTI_ID
                        .FK_IFTI_NasabahType_ID = idx.FK_IFTI_NasabahType_ID
                        .Beneficiary_Nasabah_INDV_NoRekening = idx.Beneficiary_Nasabah_INDV_NoRekening
                        .Beneficiary_Nasabah_INDV_NamaLengkap = idx.Beneficiary_Nasabah_INDV_NamaLengkap
                        .Beneficiary_Nasabah_INDV_TanggalLahir = idx.Beneficiary_Nasabah_INDV_TanggalLahir
                        .Beneficiary_Nasabah_INDV_KewargaNegaraan = idx.Beneficiary_Nasabah_INDV_KewargaNegaraan
                        .Beneficiary_Nasabah_INDV_Pekerjaan = idx.Beneficiary_Nasabah_INDV_Pekerjaan
                        .Beneficiary_Nasabah_INDV_PekerjaanLainnya = idx.Beneficiary_Nasabah_INDV_PekerjaanLainnya
                        .Beneficiary_Nasabah_INDV_Negara = idx.Beneficiary_Nasabah_INDV_Negara
                        .Beneficiary_Nasabah_INDV_NegaraLainnya = idx.Beneficiary_Nasabah_INDV_NegaraLainnya
                        .Beneficiary_Nasabah_INDV_ID_Alamat_Dom = idx.Beneficiary_Nasabah_INDV_ID_Alamat_Dom
                        .Beneficiary_Nasabah_INDV_ID_KotaKab_Dom = idx.Beneficiary_Nasabah_INDV_ID_KotaKab_Dom
                        .Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom = idx.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom
                        .Beneficiary_Nasabah_INDV_ID_Propinsi_Dom = idx.Beneficiary_Nasabah_INDV_ID_Propinsi_Dom
                        .Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom = idx.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom
                        .Beneficiary_Nasabah_INDV_ID_Alamat_Iden = idx.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom
                        .Beneficiary_Nasabah_INDV_ID_KotaKab_Iden = idx.Beneficiary_Nasabah_INDV_ID_KotaKab_Iden
                        .Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden = idx.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden
                        .Beneficiary_Nasabah_INDV_ID_Propinsi_Iden = idx.Beneficiary_Nasabah_INDV_ID_Propinsi_Iden
                        .Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden = idx.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden
                        .Beneficiary_Nasabah_INDV_ID_NegaraBagian = idx.Beneficiary_Nasabah_INDV_ID_NegaraBagian
                        .Beneficiary_Nasabah_INDV_ID_Negara = idx.Beneficiary_Nasabah_INDV_ID_Negara
                        .Beneficiary_Nasabah_INDV_ID_NegaraLainnya = idx.Beneficiary_Nasabah_INDV_ID_NegaraLainnya
                        .Beneficiary_Nasabah_INDV_NoTelp = idx.Beneficiary_Nasabah_INDV_NoTelp
                        .Beneficiary_Nasabah_INDV_FK_IFTI_IDType = idx.Beneficiary_Nasabah_INDV_FK_IFTI_IDType
                        .Beneficiary_Nasabah_INDV_NomorID = idx.Beneficiary_Nasabah_INDV_NomorID
                        .Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan = idx.Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan
                        .Beneficiary_Nasabah_CORP_NoRekening = idx.Beneficiary_Nasabah_CORP_NoRekening
                        .Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id = idx.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id
                        .Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya = idx.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya
                        .Beneficiary_Nasabah_CORP_NamaKorporasi = idx.Beneficiary_Nasabah_CORP_NamaKorporasi
                        .Beneficiary_Nasabah_CORP_NoTelp = idx.Beneficiary_Nasabah_CORP_NoTelp
                        .Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan = idx.Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan
                        .Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id = idx.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id
                        .Beneficiary_Nasabah_CORP_BidangUsahaLainnya = idx.Beneficiary_Nasabah_CORP_BidangUsahaLainnya
                        .Beneficiary_Nasabah_CORP_AlamatLengkap = idx.Beneficiary_Nasabah_CORP_AlamatLengkap
                        .Beneficiary_Nasabah_CORP_ID_KotaKab = idx.Beneficiary_Nasabah_CORP_ID_KotaKab
                        .Beneficiary_Nasabah_CORP_ID_KotaKabLainnya = idx.Beneficiary_Nasabah_CORP_ID_KotaKabLainnya
                        .Beneficiary_Nasabah_CORP_ID_Propinsi = idx.Beneficiary_Nasabah_CORP_ID_Propinsi
                        .Beneficiary_Nasabah_CORP_ID_PropinsiLainnya = idx.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya
                        .Beneficiary_Nasabah_CORP_ID_NegaraBagian = idx.Beneficiary_Nasabah_CORP_ID_NegaraBagian
                        .Beneficiary_Nasabah_CORP_ID_Negara = idx.Beneficiary_Nasabah_CORP_ID_Negara
                        .Beneficiary_Nasabah_CORP_ID_NegaraLainnya = idx.Beneficiary_Nasabah_CORP_ID_NegaraLainnya
                        .Beneficiary_NonNasabah_NoRekening = idx.Beneficiary_NonNasabah_NoRekening
                        .Beneficiary_NonNasabah_KodeRahasia = idx.Beneficiary_NonNasabah_KodeRahasia
                        .Beneficiary_NonNasabah_NamaLengkap = idx.Beneficiary_NonNasabah_NamaLengkap
                        .Beneficiary_NonNasabah_NamaBank = idx.Beneficiary_NonNasabah_NamaBank
                        .Beneficiary_NonNasabah_TanggalLahir = idx.Beneficiary_NonNasabah_TanggalLahir
                        .Beneficiary_NonNasabah_NoTelp = idx.Beneficiary_NonNasabah_NoTelp
                        .Beneficiary_NonNasabah_NilaiTransaksikeuangan = idx.Beneficiary_NonNasabah_NilaiTransaksikeuangan
                        .Beneficiary_NonNasabah_ID_Alamat = idx.Beneficiary_NonNasabah_ID_Alamat
                        .Beneficiary_NonNasabah_ID_NegaraBagian = idx.Beneficiary_NonNasabah_ID_NegaraBagian
                        .Beneficiary_NonNasabah_ID_Negara = idx.Beneficiary_NonNasabah_ID_Negara
                        .Beneficiary_NonNasabah_ID_NegaraLainnya = idx.Beneficiary_NonNasabah_ID_NegaraLainnya
                        .Beneficiary_NonNasabah_FK_IFTI_IDType = idx.Beneficiary_NonNasabah_FK_IFTI_IDType
                        .Beneficiary_NonNasabah_NomorID = idx.Beneficiary_NonNasabah_NomorID
                    End With
                    DataRepository.IFTI_BeneficiaryProvider.Save(otrans, newbenef)
                End If

                Dim delete As IFTI_Approval_Beneficiary = DataRepository.IFTI_Approval_BeneficiaryProvider.GetPaged("PK_IFTI_Approval_Beneficiary_ID =" & idx.PK_IFTI_Approval_Beneficiary_ID.ToString, "", 0, Integer.MaxValue, 0)(0)
                DataRepository.IFTI_Approval_BeneficiaryProvider.Delete(otrans, delete)

            Next

            'End Using
            otrans.Commit()
        Catch ex As Exception
            otrans.Rollback()
            Throw
        Finally
            If Not otrans Is Nothing Then
                otrans.Dispose()
                otrans = Nothing
            End If
        End Try
    End Sub
    Protected Sub BtnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        'Dim oSQLTrans As SqlTransaction = Nothing
        Dim otrans As TransactionManager = Nothing
        Try
            otrans = New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)
            otrans.BeginTransaction()


            Using iftiApprovalDetailAdapter As New IFTI_Approval_Detail

                Using RulesiftiApprovalDetailAdapter As IFTI_Approval_Detail = DataRepository.IFTI_Approval_DetailProvider.GetPaged("FK_IFTI_Approval_id =" & parID, "", 0, Integer.MaxValue, 0)(0)

                    Using IFTIAdapter As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(RulesiftiApprovalDetailAdapter.PK_IFTI_ID)
                        'Bila RulesBasic_ApprovalTable.Rows.Count > 0 berarti Basic Rule tsb masih ada dlm tabel RulesBasic_Approval
                        If Not RulesiftiApprovalDetailAdapter Is Nothing Then
                            Dim IFTIApprovalTableRow As IFTI_Approval_Detail
                            IFTIApprovalTableRow = RulesiftiApprovalDetailAdapter


                            Dim Counter As Integer
                            'Using IFTIQueryAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.QueriesTableAdapter
                            'Using IFTIQueryAdapter As TList(Of IFTI) = DataRepository.IFTIProvider.GetPaged("LTDLNNO='" & NonSwiftOutUmum_LTDNNew.Text & "'", "", 0, Integer.MaxValue, 0)
                            '    'Counter = RulesBasicQueryAdapter.Count.ToString(IFTIApprovalTableRow.KYE_RulesBasicName)
                            'End Using

                            'Using RulesIFTIBeneveciaryApproval As IFTI_Approval_Beneficiary = DataRepository.IFTI_Approval_BeneficiaryProvider.GetPaged("FK_IFTI_Approval_id =" & parID, "", 0, Integer.MaxValue, 0)(0)



                            AcceptDeleteBeneficiary()

                            'Dim IFTIBeneveciaryApprovalTableRow As IFTI_Approval_Beneficiary
                            'IFTIBeneveciaryApprovalTableRow = RulesIFTIBeneveciaryApproval

                            'Using IFTI_BeneficiaryAdapter As IFTI_Beneficiary = DataRepository.IFTI_BeneficiaryProvider.GetPaged("PK_IFTI_Beneficiary_ID = " & IFTIBeneveciaryApprovalTableRow.FK_IFTI_Beneficiary_ID.ToString, "", 0, Integer.MaxValue, 0)(0)
                            '    With IFTI_BeneficiaryAdapter
                            '        '.PK_IFTI_Beneficiary_ID = 0
                            '        .FK_IFTI_ID = IFTIBeneveciaryApprovalTableRow.FK_IFTI_ID
                            '        .FK_IFTI_NasabahType_ID = IFTIBeneveciaryApprovalTableRow.FK_IFTI_NasabahType_ID
                            '        .PJKBank_type = IFTIBeneveciaryApprovalTableRow.PJKBank_type
                            '        .Beneficiary_Nasabah_INDV_NoRekening = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_NoRekening
                            '        .Beneficiary_Nasabah_INDV_NamaLengkap = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_NamaLengkap
                            '        .Beneficiary_Nasabah_INDV_TanggalLahir = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_TanggalLahir
                            '        .Beneficiary_Nasabah_INDV_KewargaNegaraan = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_KewargaNegaraan
                            '        .Beneficiary_Nasabah_INDV_Pekerjaan = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_Pekerjaan
                            '        .Beneficiary_Nasabah_INDV_PekerjaanLainnya = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_PekerjaanLainnya
                            '        .Beneficiary_Nasabah_INDV_Negara = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_Negara
                            '        .Beneficiary_Nasabah_INDV_NegaraLainnya = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_NegaraLainnya
                            '        .Beneficiary_Nasabah_INDV_ID_Alamat_Dom = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_ID_Alamat_Dom
                            '        .Beneficiary_Nasabah_INDV_ID_KotaKab_Dom = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_ID_KotaKab_Dom
                            '        .Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom
                            '        .Beneficiary_Nasabah_INDV_ID_Propinsi_Dom = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_ID_Propinsi_Dom
                            '        .Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom
                            '        .Beneficiary_Nasabah_INDV_ID_Alamat_Iden = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom
                            '        .Beneficiary_Nasabah_INDV_ID_KotaKab_Iden = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_ID_KotaKab_Iden
                            '        .Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden
                            '        .Beneficiary_Nasabah_INDV_ID_Propinsi_Iden = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_ID_Propinsi_Iden
                            '        .Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden
                            '        .Beneficiary_Nasabah_INDV_ID_NegaraBagian = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_ID_NegaraBagian
                            '        .Beneficiary_Nasabah_INDV_ID_Negara = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_ID_Negara
                            '        .Beneficiary_Nasabah_INDV_ID_NegaraLainnya = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_ID_NegaraLainnya
                            '        .Beneficiary_Nasabah_INDV_NoTelp = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_NoTelp
                            '        .Beneficiary_Nasabah_INDV_FK_IFTI_IDType = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_FK_IFTI_IDType
                            '        .Beneficiary_Nasabah_INDV_NomorID = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_NomorID
                            '        .Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan
                            '        .Beneficiary_Nasabah_CORP_NoRekening = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_CORP_NoRekening
                            '        .Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id
                            '        .Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya
                            '        .Beneficiary_Nasabah_CORP_NamaKorporasi = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_CORP_NamaKorporasi
                            '        .Beneficiary_Nasabah_CORP_NoTelp = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_CORP_NoTelp
                            '        .Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan
                            '        .Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id
                            '        .Beneficiary_Nasabah_CORP_BidangUsahaLainnya = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_CORP_BidangUsahaLainnya
                            '        .Beneficiary_Nasabah_CORP_AlamatLengkap = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_CORP_AlamatLengkap
                            '        .Beneficiary_Nasabah_CORP_ID_KotaKab = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_CORP_ID_KotaKab
                            '        .Beneficiary_Nasabah_CORP_ID_KotaKabLainnya = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_CORP_ID_KotaKabLainnya
                            '        .Beneficiary_Nasabah_CORP_ID_Propinsi = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_CORP_ID_Propinsi
                            '        .Beneficiary_Nasabah_CORP_ID_PropinsiLainnya = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya
                            '        .Beneficiary_Nasabah_CORP_ID_NegaraBagian = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_CORP_ID_NegaraBagian
                            '        .Beneficiary_Nasabah_CORP_ID_Negara = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_CORP_ID_Negara
                            '        .Beneficiary_Nasabah_CORP_ID_NegaraLainnya = IFTIBeneveciaryApprovalTableRow.Beneficiary_Nasabah_CORP_ID_NegaraLainnya
                            '        .Beneficiary_NonNasabah_NoRekening = IFTIBeneveciaryApprovalTableRow.Beneficiary_NonNasabah_NoRekening
                            '        .Beneficiary_NonNasabah_KodeRahasia = IFTIBeneveciaryApprovalTableRow.Beneficiary_NonNasabah_KodeRahasia
                            '        .Beneficiary_NonNasabah_NamaLengkap = IFTIBeneveciaryApprovalTableRow.Beneficiary_NonNasabah_NamaLengkap
                            '        .Beneficiary_NonNasabah_NamaBank = IFTIBeneveciaryApprovalTableRow.Beneficiary_NonNasabah_NamaBank
                            '        .Beneficiary_NonNasabah_TanggalLahir = IFTIBeneveciaryApprovalTableRow.Beneficiary_NonNasabah_TanggalLahir
                            '        .Beneficiary_NonNasabah_NoTelp = IFTIBeneveciaryApprovalTableRow.Beneficiary_NonNasabah_NoTelp
                            '        .Beneficiary_NonNasabah_NilaiTransaksikeuangan = IFTIBeneveciaryApprovalTableRow.Beneficiary_NonNasabah_NilaiTransaksikeuangan
                            '        .Beneficiary_NonNasabah_ID_Alamat = IFTIBeneveciaryApprovalTableRow.Beneficiary_NonNasabah_ID_Alamat
                            '        .Beneficiary_NonNasabah_ID_NegaraBagian = IFTIBeneveciaryApprovalTableRow.Beneficiary_NonNasabah_ID_NegaraBagian
                            '        .Beneficiary_NonNasabah_ID_Negara = IFTIBeneveciaryApprovalTableRow.Beneficiary_NonNasabah_ID_Negara
                            '        .Beneficiary_NonNasabah_ID_NegaraLainnya = IFTIBeneveciaryApprovalTableRow.Beneficiary_NonNasabah_ID_NegaraLainnya
                            '        .Beneficiary_NonNasabah_FK_IFTI_IDType = IFTIBeneveciaryApprovalTableRow.Beneficiary_NonNasabah_FK_IFTI_IDType
                            '        .Beneficiary_NonNasabah_NomorID = IFTIBeneveciaryApprovalTableRow.Beneficiary_NonNasabah_NomorID
                            '    End With
                            '    DataRepository.IFTI_BeneficiaryProvider.Save(otrans, IFTI_BeneficiaryAdapter)

                            '    Dim delete As IFTI_Approval_Beneficiary = DataRepository.IFTI_Approval_BeneficiaryProvider.GetPaged("FK_IFTI_Approval_id =" & parID, "", 0, Integer.MaxValue, 0)(0)
                            '    delete.FK_IFTI_Beneficiary_ID = parID
                            '    DataRepository.IFTI_Approval_BeneficiaryProvider.Delete(otrans, delete)
                            'End Using
                            'End Using

                            'Bila counter = 0 berarti RulesBasicName tsb belum ada dlm tabel RulesBasic, maka boleh ditambahkan
                            If Counter = 0 Then
                                'Dim IFTIAdapter As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(RulesiftiApprovalDetailAdapter.PK_IFTI_ID)
                                With IFTIAdapter
                                    .PK_IFTI_ID = IFTIApprovalTableRow.PK_IFTI_ID
                                    '.ReportToPPATK
                                    '.Aging
                                    '.PaymentDetail
                                    '.AutoReplace
                                    '.IsDataValid
                                    .FK_IFTI_Type_ID = IFTIApprovalTableRow.FK_IFTI_Type_ID
                                    '.LocalID = IFTIApprovalTableRow.LocalID
                                    .LTDLNNo = IFTIApprovalTableRow.LTDLNNo
                                    .LTDLNNoKoreksi = IFTIApprovalTableRow.LTDLNNoKoreksi
                                    .TanggalLaporan = IFTIApprovalTableRow.TanggalLaporan
                                    .NamaPJKBankPelapor = IFTIApprovalTableRow.NamaPJKBankPelapor
                                    .NamaPejabatPJKBankPelapor = IFTIApprovalTableRow.NamaPejabatPJKBankPelapor
                                    .JenisLaporan = IFTIApprovalTableRow.JenisLaporan
                                    .Sender_FK_IFTI_NasabahType_ID = IFTIApprovalTableRow.Sender_FK_IFTI_NasabahType_ID
                                    .Sender_Nasabah_INDV_NoRekening = IFTIApprovalTableRow.Sender_Nasabah_INDV_NoRekening
                                    .Sender_Nasabah_INDV_NamaLengkap = IFTIApprovalTableRow.Sender_Nasabah_INDV_NamaLengkap
                                    .Sender_Nasabah_INDV_TanggalLahir = IFTIApprovalTableRow.Sender_Nasabah_INDV_TanggalLahir
                                    .Sender_Nasabah_INDV_KewargaNegaraan = IFTIApprovalTableRow.Sender_Nasabah_INDV_KewargaNegaraan
                                    .Sender_Nasabah_INDV_Negara = IFTIApprovalTableRow.Sender_Nasabah_INDV_KewargaNegaraan
                                    .Sender_Nasabah_INDV_NegaraLainnya = IFTIApprovalTableRow.Sender_Nasabah_INDV_NegaraLainnya
                                    .Sender_Nasabah_INDV_Pekerjaan = IFTIApprovalTableRow.Sender_Nasabah_INDV_Pekerjaan
                                    .Sender_Nasabah_INDV_PekerjaanLainnya = IFTIApprovalTableRow.Sender_Nasabah_INDV_PekerjaanLainnya
                                    .Sender_Nasabah_INDV_DOM_Alamat = IFTIApprovalTableRow.Sender_Nasabah_INDV_DOM_Alamat
                                    .Sender_Nasabah_INDV_DOM_Propinsi = IFTIApprovalTableRow.Sender_Nasabah_INDV_DOM_Propinsi
                                    .Sender_Nasabah_INDV_DOM_PropinsiLainnya = IFTIApprovalTableRow.Sender_Nasabah_INDV_DOM_PropinsiLainnya
                                    .Sender_Nasabah_INDV_DOM_KotaKab = IFTIApprovalTableRow.Sender_Nasabah_INDV_DOM_KotaKab
                                    .Sender_Nasabah_INDV_DOM_KotaKabLainnya = IFTIApprovalTableRow.Sender_Nasabah_INDV_DOM_KotaKabLainnya
                                    .Sender_Nasabah_INDV_ID_Alamat = IFTIApprovalTableRow.Sender_Nasabah_INDV_ID_Alamat
                                    .Sender_Nasabah_INDV_ID_Propinsi = IFTIApprovalTableRow.Sender_Nasabah_INDV_ID_Propinsi
                                    .Sender_Nasabah_INDV_ID_PropinsiLainnya = IFTIApprovalTableRow.Sender_Nasabah_INDV_ID_PropinsiLainnya
                                    .Sender_Nasabah_INDV_ID_KotaKab = IFTIApprovalTableRow.Sender_Nasabah_INDV_ID_KotaKab
                                    .Sender_Nasabah_INDV_ID_KotaKabLainnya = IFTIApprovalTableRow.Sender_Nasabah_INDV_ID_KotaKabLainnya
                                    .Sender_Nasabah_INDV_FK_IFTI_IDType = IFTIApprovalTableRow.Sender_Nasabah_INDV_FK_IFTI_IDType
                                    .Sender_Nasabah_INDV_ID_Negara = IFTIApprovalTableRow.Sender_Nasabah_INDV_ID_Negara
                                    .Sender_Nasabah_INDV_NomorID = IFTIApprovalTableRow.Sender_Nasabah_INDV_NomorID
                                    .Sender_Nasabah_CORP_NoRekening = IFTIApprovalTableRow.Sender_Nasabah_CORP_NoRekening
                                    .Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id = IFTIApprovalTableRow.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id
                                    .Sender_Nasabah_CORP_BentukBadanUsahaLainnya = IFTIApprovalTableRow.Sender_Nasabah_CORP_BentukBadanUsahaLainnya
                                    .Sender_Nasabah_CORP_NamaKorporasi = IFTIApprovalTableRow.Sender_Nasabah_CORP_NamaKorporasi
                                    .Sender_Nasabah_CORP_FK_MsBidangUsaha_Id = IFTIApprovalTableRow.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id
                                    .Sender_Nasabah_CORP_BidangUsahaLainnya = IFTIApprovalTableRow.Sender_Nasabah_CORP_BidangUsahaLainnya
                                    .Sender_Nasabah_CORP_AlamatLengkap = IFTIApprovalTableRow.Sender_Nasabah_CORP_AlamatLengkap
                                    .Sender_Nasabah_CORP_Propinsi = IFTIApprovalTableRow.Sender_Nasabah_CORP_Propinsi
                                    .Sender_Nasabah_CORP_PropinsiLainnya = IFTIApprovalTableRow.Sender_Nasabah_CORP_PropinsiLainnya
                                    .Sender_Nasabah_CORP_KotaKab = IFTIApprovalTableRow.Sender_Nasabah_COR_KotaKab
                                    .Sender_Nasabah_CORP_KotaKabLainnya = IFTIApprovalTableRow.Sender_Nasabah_COR_KotaKabLainnya
                                    .Sender_Nasabah_CORP_Negara = IFTIApprovalTableRow.Sender_Nasabah_CORP_Negara
                                    .Sender_Nasabah_CORP_LN_AlamatLengkap = IFTIApprovalTableRow.Sender_Nasabah_CORP_LN_AlamatLengkap
                                    .Sender_Nasabah_CORP_Alamat_KantorCabangPengirim = IFTIApprovalTableRow.Sender_Nasabah_CORP_Alamat_KantorCabangPengirim
                                    .FK_IFTI_NonNasabahNominalType_ID = IFTIApprovalTableRow.FK_IFTI_NonNasabahNominalType_ID
                                    .Sender_NonNasabah_NamaLengkap = IFTIApprovalTableRow.Sender_NonNasabah_NamaLengkap
                                    .Sender_NonNasabah_TanggalLahir = IFTIApprovalTableRow.Sender_NonNasabah_TanggalLahir
                                    .Sender_NonNasabah_ID_Alamat = IFTIApprovalTableRow.Sender_NonNasabah_ID_Alamat
                                    .Sender_NonNasabah_ID_Propinsi = IFTIApprovalTableRow.Sender_NonNasabah_ID_Propinsi
                                    .Sender_NonNasabah_ID_PropinsiLainnya = IFTIApprovalTableRow.Sender_NonNasabah_ID_PropinsiLainnya
                                    .Sender_NonNasabah_ID_KotaKab = IFTIApprovalTableRow.Sender_NonNasabah_ID_KotaKab
                                    .Sender_NonNasabah_ID_KotaKabLainnya = IFTIApprovalTableRow.Sender_NonNasabah_ID_KotaKabLainnya
                                    .Sender_NonNasabah_FK_IFTI_IDType = IFTIApprovalTableRow.Sender_NonNasabah_FK_IFTI_IDType
                                    .Sender_NonNasabah_NomorID = IFTIApprovalTableRow.Sender_NonNasabah_NomorID
                                    .FK_IFTI_BeneficialOwnerType_ID = IFTIApprovalTableRow.FK_IFTI_BeneficialOwnerType_ID
                                    .BeneficialOwner_HubunganDenganPemilikDana = IFTIApprovalTableRow.BeneficialOwner_HubunganDenganPemilikDana
                                    .BeneficialOwner_NoRekening = IFTIApprovalTableRow.BeneficialOwner_NoRekening
                                    .BeneficialOwner_NamaLengkap = IFTIApprovalTableRow.BeneficialOwner_NamaLengkap
                                    .BeneficialOwner_TanggalLahir = IFTIApprovalTableRow.BeneficialOwner_TanggalLahir
                                    .BeneficialOwner_ID_Alamat = IFTIApprovalTableRow.BeneficialOwner_ID_Alamat
                                    .BeneficialOwner_ID_Propinsi = IFTIApprovalTableRow.BeneficialOwner_ID_Propinsi
                                    .BeneficialOwner_ID_PropinsiLainnya = IFTIApprovalTableRow.BeneficialOwner_ID_PropinsiLainnya
                                    .BeneficialOwner_ID_KotaKab = IFTIApprovalTableRow.BeneficialOwner_ID_KotaKab
                                    .BeneficialOwner_ID_KotaKabLainnya = IFTIApprovalTableRow.BeneficialOwner_ID_KotaKabLainnya
                                    .BeneficialOwner_FK_IFTI_IDType = IFTIApprovalTableRow.BeneficialOwner_FK_IFTI_IDType
                                    .BeneficialOwner_NomorID = IFTIApprovalTableRow.BeneficialOwner_NomorID
                                    .TanggalTransaksi = IFTIApprovalTableRow.TanggalTransaksi
                                    .TimeIndication = IFTIApprovalTableRow.TimeIndication
                                    .SenderReference = IFTIApprovalTableRow.SenderReference
                                    .BankOperationCode = IFTIApprovalTableRow.BankOperationCode
                                    .InstructionCode = IFTIApprovalTableRow.InstructionCode
                                    .KantorCabangPenyelengaraPengirimAsal = IFTIApprovalTableRow.KantorCabangPenyelengaraPengirimAsal
                                    .TransactionCode = IFTIApprovalTableRow.TransactionCode
                                    .ValueDate_TanggalTransaksi = IFTIApprovalTableRow.ValueDate_TanggalTransaksi
                                    .ValueDate_NilaiTransaksi = IFTIApprovalTableRow.ValueDate_NilaiTransaksi
                                    .ValueDate_FK_Currency_ID = IFTIApprovalTableRow.ValueDate_FK_Currency_ID
                                    .ValueDate_CurrencyLainnya = IFTIApprovalTableRow.ValueDate_CurrencyLainnya
                                    .ValueDate_NilaiTransaksiIDR = IFTIApprovalTableRow.ValueDate_NilaiTransaksiIDR
                                    .Instructed_Currency = IFTIApprovalTableRow.Instructed_Currency
                                    .Instructed_CurrencyLainnya = IFTIApprovalTableRow.Instructed_CurrencyLainnya
                                    .Instructed_Amount = IFTIApprovalTableRow.Instructed_Amount
                                    .ExchangeRate = IFTIApprovalTableRow.ExchangeRate
                                    .SendingInstitution = IFTIApprovalTableRow.SendingInstitution
                                    .TujuanTransaksi = IFTIApprovalTableRow.TujuanTransaksi
                                    .SumberPenggunaanDana = IFTIApprovalTableRow.SumberPenggunaanDana
                                    .InformationAbout_SenderCorrespondent = IFTIApprovalTableRow.InformationAbout_SenderCorrespondent
                                    .InformationAbout_ReceiverCorrespondent = IFTIApprovalTableRow.InformationAbout_ReceiverCorrespondent
                                    .InformationAbout_Thirdreimbursementinstitution = IFTIApprovalTableRow.InformationAbout_Thirdreimbursementinstitution
                                    .InformationAbout_IntermediaryInstitution = IFTIApprovalTableRow.InformationAbout_IntermediaryInstitution
                                    .RemittanceInformation = IFTIApprovalTableRow.RemittanceInformation
                                    .SendertoReceiverInformation = IFTIApprovalTableRow.SendertoReceiverInformation
                                    .RegulatoryReporting = IFTIApprovalTableRow.RegulatoryReporting
                                    .EnvelopeContents = IFTIApprovalTableRow.EnvelopeContents
                                End With
                                'save ifti
                                DataRepository.IFTIProvider.Save(otrans, IFTIAdapter)

                                'delete item tersebut dalam table ifti_approval & ifti_approval_detail

                                Dim deleteapp As IFTI_Approval = DataRepository.IFTI_ApprovalProvider.GetByPK_IFTI_Approval_Id(parID)
                                deleteapp.PK_IFTI_Approval_Id = parID
                                DataRepository.IFTI_ApprovalProvider.Delete(otrans, deleteapp)

                                Dim DeleteDetail As IFTI_Approval_Detail = DataRepository.IFTI_Approval_DetailProvider.GetPaged("FK_IFTI_Approval_Id =" & parID, "", 0, Integer.MaxValue, 0)(0)
                                DataRepository.IFTI_Approval_DetailProvider.Delete(otrans, DeleteDetail)

                                'delete item error list dari ifti_errorlist
                                Dim DeleteError As TList(Of IFTI_ErrorDescription) = DataRepository.IFTI_ErrorDescriptionProvider.GetPaged("FK_IFTI_ID = " & IFTIAdapter.PK_IFTI_ID, "", 0, Integer.MaxValue, 0)

                                DataRepository.IFTI_ErrorDescriptionProvider.Delete(DeleteError)
                                GenerateListOfGeneratedIFTI(otrans)

                            End If
                        End If
                    End Using
                End Using
            End Using
            otrans.Commit()
            MultiView1.ActiveViewIndex = 1
            LblConfirmation.Text = "Data Accepted to IFTI Table"
            'End Using
        Catch ex As Exception
            If Not otrans Is Nothing Then
                otrans.Rollback()
            End If
            LogError(ex)
            Throw
        Finally
            If Not otrans Is Nothing Then
                otrans.Dispose()
                otrans = Nothing
            End If
        End Try
    End Sub
    Function GenerateListOfGeneratedIFTI(ByVal objtransaction As TransactionManager) As Boolean

        Using objCommand As SqlCommand = Commonly.GetSQLCommandStoreProcedure("usp_GenerateListOfGeneratedIfti")
            DataRepository.Provider.ExecuteNonQuery(objtransaction, objCommand)
        End Using


    End Function
#End Region

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            LoadIDType()
            LoadData()
            bindgrid()
            If Not Page.IsPostBack Then
                ClearThisPageSessions()
                AuditTrailBLL.InsertAuditTrailUserAccess(SessionCurrentPage)
            End If
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBtnAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAdd.Click
        Response.Redirect("IFTI_Approvals.aspx")
    End Sub
End Class
