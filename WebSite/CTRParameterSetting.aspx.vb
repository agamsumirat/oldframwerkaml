Imports AMLDAL
Imports System.Data.SqlClient
Imports Sahassa.AML.TableAdapterHelper
Partial Class CTRParameterSetting
    Inherits Parent

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage += ex.Message
        End Try
    End Sub

    Private _CTRParameterEmailCC As CTRParameter.CTRParameterRow
    Private ReadOnly Property CTRParameterEmailCC() As CTRParameter.CTRParameterRow
        Get
            Using adapter As New AMLDAL.CTRParameterTableAdapters.CTRParameterTableAdapter
                Using otable As CTRParameter.CTRParameterDataTable = adapter.GetData()
                    If otable.Rows.Count > 0 Then
                        Dim orowsTrans() As CTRParameter.CTRParameterRow = otable.Select("PK_CTRParameterId=" & EnCTRParameterType.CTREmailCC)
                        If orowsTrans.Length > 0 Then
                            _CTRParameterEmailCC = orowsTrans(0)
                            Return _CTRParameterEmailCC
                        Else
                            _CTRParameterEmailCC = Nothing
                            Return _CTRParameterEmailCC
                        End If
                    Else
                        _CTRParameterEmailCC = Nothing
                        Return _CTRParameterEmailCC
                    End If
                End Using
            End Using
        End Get

    End Property
    Private _CTRParameterEmailBCC As CTRParameter.CTRParameterRow
    Private ReadOnly Property CTRParameterEmailBCC() As CTRParameter.CTRParameterRow
        Get
            Using adapter As New AMLDAL.CTRParameterTableAdapters.CTRParameterTableAdapter
                Using otable As CTRParameter.CTRParameterDataTable = adapter.GetData()
                    If otable.Rows.Count > 0 Then
                        Dim orowsTrans() As CTRParameter.CTRParameterRow = otable.Select("PK_CTRParameterId=" & EnCTRParameterType.CTREmailBCC)
                        If orowsTrans.Length > 0 Then
                            _CTRParameterEmailBCC = orowsTrans(0)
                            Return _CTRParameterEmailBCC
                        Else
                            _CTRParameterEmailBCC = Nothing
                            Return _CTRParameterEmailBCC
                        End If
                    Else
                        _CTRParameterEmailBCC = Nothing
                        Return _CTRParameterEmailBCC
                    End If
                End Using
            End Using
        End Get

    End Property
    Private _CTRParameterEmailSubject As CTRParameter.CTRParameterRow
    Private ReadOnly Property CTRParameterEmailSubject() As CTRParameter.CTRParameterRow
        Get
            Using adapter As New AMLDAL.CTRParameterTableAdapters.CTRParameterTableAdapter
                Using otable As CTRParameter.CTRParameterDataTable = adapter.GetData()
                    If otable.Rows.Count > 0 Then
                        Dim orowsTrans() As CTRParameter.CTRParameterRow = otable.Select("PK_CTRParameterId=" & EnCTRParameterType.CTREmailSubject)
                        If orowsTrans.Length > 0 Then
                            _CTRParameterEmailSubject = orowsTrans(0)
                            Return _CTRParameterEmailSubject
                        Else
                            _CTRParameterEmailSubject = Nothing
                            Return _CTRParameterEmailSubject
                        End If
                    Else
                        _CTRParameterEmailSubject = Nothing
                        Return _CTRParameterEmailSubject
                    End If
                End Using
            End Using
        End Get

    End Property
    Private _CTRParameterEmailBody As CTRParameter.CTRParameterRow
    Private ReadOnly Property CTRParameterEmailBody() As CTRParameter.CTRParameterRow
        Get
            Using adapter As New AMLDAL.CTRParameterTableAdapters.CTRParameterTableAdapter
                Using otable As CTRParameter.CTRParameterDataTable = adapter.GetData()
                    If otable.Rows.Count > 0 Then
                        Dim orowsTrans() As CTRParameter.CTRParameterRow = otable.Select("PK_CTRParameterId=" & EnCTRParameterType.CTREmailBody)
                        If orowsTrans.Length > 0 Then
                            _CTRParameterEmailBody = orowsTrans(0)
                            Return _CTRParameterEmailBody
                        Else
                            _CTRParameterEmailBody = Nothing
                            Return _CTRParameterEmailBody
                        End If
                    Else
                        _CTRParameterEmailBody = Nothing
                        Return _CTRParameterEmailBody
                    End If
                End Using
            End Using
        End Get

    End Property
    Private _CTRParameterEmailTo As CTRParameter.CTRParameterRow
    Private ReadOnly Property CTRParameterEmailTo() As CTRParameter.CTRParameterRow
        Get
            Using adapter As New AMLDAL.CTRParameterTableAdapters.CTRParameterTableAdapter
                Using otable As CTRParameter.CTRParameterDataTable = adapter.GetData()
                    If otable.Rows.Count > 0 Then
                        Dim orowsTrans() As CTRParameter.CTRParameterRow = otable.Select("PK_CTRParameterId=" & EnCTRParameterType.CTREmailTo)
                        If orowsTrans.Length > 0 Then
                            _CTRParameterEmailTo = orowsTrans(0)
                            Return _CTRParameterEmailTo
                        Else
                            _CTRParameterEmailTo = Nothing
                            Return _CTRParameterEmailTo
                        End If
                    Else
                        _CTRParameterEmailTo = Nothing
                        Return _CTRParameterEmailTo
                    End If
                End Using
            End Using
        End Get

    End Property
    Private _CTRParameterAmmount As CTRParameter.CTRParameterRow
    Private ReadOnly Property CTRParameterAmmount() As CTRParameter.CTRParameterRow
        Get
            Using adapter As New AMLDAL.CTRParameterTableAdapters.CTRParameterTableAdapter
                Using otable As CTRParameter.CTRParameterDataTable = adapter.GetData()
                    If otable.Rows.Count > 0 Then
                        Dim orowsTrans() As CTRParameter.CTRParameterRow = otable.Select("PK_CTRParameterId=" & EnCTRParameterType.TransactionAmmount)
                        If orowsTrans.Length > 0 Then
                            _CTRParameterAmmount = orowsTrans(0)
                            Return _CTRParameterAmmount
                        Else
                            _CTRParameterAmmount = Nothing
                            Return _CTRParameterAmmount
                        End If
                    Else
                        _CTRParameterAmmount = Nothing
                        Return _CTRParameterAmmount
                    End If
                End Using
            End Using
        End Get

    End Property

    Private _CTRParameterPIC As CTRParameter.CTRParameterRow
    Private ReadOnly Property CTRParameterPIC() As CTRParameter.CTRParameterRow
        Get
            Using adapter As New AMLDAL.CTRParameterTableAdapters.CTRParameterTableAdapter
                Using otable As CTRParameter.CTRParameterDataTable = adapter.GetData()
                    If otable.Rows.Count > 0 Then
                        Dim orowsTrans() As CTRParameter.CTRParameterRow = otable.Select("PK_CTRParameterId=" & EnCTRParameterType.CTRPIC)
                        If orowsTrans.Length > 0 Then
                            _CTRParameterPIC = orowsTrans(0)
                            Return _CTRParameterPIC
                        Else
                            _CTRParameterPIC = Nothing
                            Return _CTRParameterPIC
                        End If
                    Else
                        _CTRParameterPIC = Nothing
                        Return _CTRParameterPIC
                    End If
                End Using
            End Using
        End Get

    End Property



    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try

            If Not IsPostBack Then
                LoadDataParameterCTR()
            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage += ex.Message
        End Try
    End Sub
    ''' <summary>
    ''' Load Data Dari table [CTRParameter]
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub LoadDataParameterCTR()
        Try
            If Not Me.CTRParameterAmmount Is Nothing Then
                TextTransactionAmount.Text = Me.CTRParameterAmmount.CTRParameterValue
            End If
            If Not Me.CTRParameterEmailTo Is Nothing Then
                TextCTREmailTo.Text = Me.CTRParameterEmailTo.CTRParameterValue
            End If
            If Not Me.CTRParameterEmailCC Is Nothing Then
                TextEmailCC.Text = Me.CTRParameterEmailCC.CTRParameterValue
            End If
            If Not Me.CTRParameterEmailBCC Is Nothing Then
                TextCTREmailBCC.Text = Me.CTRParameterEmailBCC.CTRParameterValue
            End If

            If Not Me.CTRParameterEmailSubject Is Nothing Then
                TextEmailSubject.Text = Me.CTRParameterEmailSubject.CTRParameterValue
            End If
            If Not Me.CTRParameterEmailBody Is Nothing Then
                TextEmailBody.Text = Me.CTRParameterEmailBody.CTRParameterValue
            End If
            If Not Me.CTRParameterPIC Is Nothing Then
                Me.TextPIC.Text = Me.CTRParameterPIC.CTRParameterValue
            End If
        Catch ex As Exception
            LogError(ex)
            Throw
        Finally

        End Try
    End Sub

    'Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
    '    Dim oTrans As SqlTransaction = Nothing
    '    Try
    '        If Page.IsValid Then

    '            Using adapter As New CTRParameterTableAdapters.CTRParameterTableAdapter
    '                oTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(adapter)
    '                adapter.UpdateCTRValueByPK(Me.TextTransactionAmount.Text.Trim, EnCTRParameterType.TransactionAmmount)
    '                adapter.UpdateCTRValueByPK(Me.TextCTREmailTo.Text.Trim, EnCTRParameterType.CTREmailTo)
    '                adapter.UpdateCTRValueByPK(Me.TextEmailCC.Text.Trim, EnCTRParameterType.CTREmailCC)
    '                adapter.UpdateCTRValueByPK(Me.TextCTREmailBCC.Text.Trim, EnCTRParameterType.CTREmailBCC)
    '                adapter.UpdateCTRValueByPK(Me.TextEmailBody.Text.Trim, EnCTRParameterType.CTREmailBody)
    '                adapter.UpdateCTRValueByPK(Me.TextEmailSubject.Text.Trim, EnCTRParameterType.CTREmailSubject)
    '                oTrans.Commit()
    '            End Using
    '        End If
    '    Catch ex As Exception
    '        If Not oTrans Is Nothing Then
    '            oTrans.Rollback()
    '        End If

    '        LogError(ex)
    '        cvalPageError.IsValid = False
    '        cvalPageError.ErrorMessage += ex.Message
    '    Finally
    '        If Not oTrans Is Nothing Then
    '            oTrans.Dispose()
    '        End If

    '    End Try
    'End Sub
    Public Enum EnCTRParameterType
        TransactionAmmount = 1
        CTREmailTo
        CTREmailCC
        CTREmailBCC
        CTREmailSubject
        CTREmailBody
        CTRPIC
    End Enum
    Private Function InPendingApproval() As Boolean
        Try
            Using adapter As New CTRParameterTableAdapters.CTRParameter_PendingApprovalTableAdapter
                If adapter.GetData.Rows.Count > 0 Then
                    Return True
                Else
                    Return False
                End If
            End Using
        Catch ex As Exception
            Throw
        End Try
    End Function
    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim oTrans As SqlTransaction = Nothing
        Dim intPK As Long = 0
        Try
            If Not InPendingApproval() Then
                Using adapter As New CTRParameterTableAdapters.CTRParameter_PendingApprovalTableAdapter
                    oTrans = BeginTransaction(adapter)
                    'insert header
                    intPK = adapter.InsertCTRParameterPendingApproval(Sahassa.AML.Commonly.SessionUserId, Now, "Edit", Sahassa.AML.Commonly.TypeMode.Edit)
                    'insert detail
                    Using adapterDetail As New CTRParameterTableAdapters.CTRParameter_ApprovalIDTableAdapter
                        If Not Me.CTRParameterAmmount Is Nothing Then
                            adapterDetail.Insert(intPK, CTRParameterAmmount.PK_CTRParameterId, CTRParameterAmmount.CTRParameterName, TextTransactionAmount.Text.Trim, CTRParameterAmmount.PK_CTRParameterId, CTRParameterAmmount.CTRParameterName, CTRParameterAmmount.CTRParameterValue)
                        End If
                        If Not Me.CTRParameterEmailTo Is Nothing Then
                            adapterDetail.Insert(intPK, CTRParameterEmailTo.PK_CTRParameterId, CTRParameterEmailTo.CTRParameterName, TextCTREmailTo.Text.Trim, CTRParameterEmailTo.PK_CTRParameterId, CTRParameterEmailTo.CTRParameterName, CTRParameterEmailTo.CTRParameterValue)
                        End If
                        If Not Me.CTRParameterEmailCC Is Nothing Then
                            adapterDetail.Insert(intPK, CTRParameterEmailCC.PK_CTRParameterId, CTRParameterEmailCC.CTRParameterName, TextEmailCC.Text.Trim, CTRParameterEmailCC.PK_CTRParameterId, CTRParameterEmailCC.CTRParameterName, CTRParameterEmailCC.CTRParameterValue)
                        End If
                        If Not Me.CTRParameterEmailBCC Is Nothing Then
                            adapterDetail.Insert(intPK, CTRParameterEmailBCC.PK_CTRParameterId, CTRParameterEmailBCC.CTRParameterName, TextCTREmailBCC.Text.Trim, CTRParameterEmailBCC.PK_CTRParameterId, CTRParameterEmailBCC.CTRParameterName, CTRParameterEmailBCC.CTRParameterValue)
                        End If
                        If Not Me.CTRParameterEmailSubject Is Nothing Then
                            adapterDetail.Insert(intPK, CTRParameterEmailSubject.PK_CTRParameterId, CTRParameterEmailSubject.CTRParameterName, TextEmailSubject.Text.Trim, CTRParameterEmailSubject.PK_CTRParameterId, CTRParameterEmailSubject.CTRParameterName, CTRParameterEmailSubject.CTRParameterValue)
                        End If
                        If Not Me.CTRParameterEmailBody Is Nothing Then
                            adapterDetail.Insert(intPK, CTRParameterEmailBody.PK_CTRParameterId, CTRParameterEmailBody.CTRParameterName, TextEmailBody.Text.Trim, CTRParameterEmailBody.PK_CTRParameterId, CTRParameterEmailBody.CTRParameterName, CTRParameterEmailBody.CTRParameterValue)
                        End If
                        If Not Me.CTRParameterPIC Is Nothing Then
                            adapterDetail.Insert(intPK, CTRParameterPIC.PK_CTRParameterId, CTRParameterPIC.CTRParameterName, TextPIC.Text.Trim, CTRParameterPIC.PK_CTRParameterId, CTRParameterPIC.CTRParameterName, CTRParameterPIC.CTRParameterValue)
                        End If

                    End Using
                End Using
                oTrans.Commit()
                Dim MessagePendingID As Integer = 81591
                Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=CTRParameter"

                Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=CTRParameter", False)
            Else
                Try
                    Throw New Exception("Cannot edit CTR Parameter  because it is currently waiting for approval.")
                Catch ex1 As Exception
                    cvalPageError.IsValid = False
                    cvalPageError.ErrorMessage = ex1.Message
                End Try
            End If
        Catch ex As Exception
            If Not oTrans Is Nothing Then
                oTrans.Rollback()
            End If
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        Finally
            If Not oTrans Is Nothing Then
                oTrans.Dispose()
            End If
        End Try
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Try
            'Cancel
            Sahassa.AML.Commonly.SessionIntendedPage = "Default.aspx"
            Me.Response.Redirect("Default.aspx", False)
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
End Class
