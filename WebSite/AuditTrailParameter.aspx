<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="AuditTrailParameter.aspx.vb" Inherits="AuditTrailParameter" title="AuditTrail Parameter" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CpContent" Runat="Server">
<ajax:ajaxpanel id="AjaxPanel1" runat="server" Width="100%">

    <table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
	    border="2">
        <tr>
            <td bgcolor="#ffffff" colspan="3" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Audit Trail Parameter
                    <hr />
                </strong>
                <ajax:AjaxPanel ID="AjaxpanelValidSumm" runat="server" Width="100%">
                    &nbsp;<asp:CustomValidator ID="Customvalidator1" runat="server" Display="Dynamic"
                        ErrorMessage="CustomValidator">
												<b></b>
											</asp:CustomValidator>
                    <asp:Label ID="LblSuccess" runat="server" CssClass="validationok" Visible="False"
                        Width="100%"></asp:Label>
                </ajax:AjaxPanel>
            </td>
        </tr>
	    <tr class="formText">
		    <td bgColor="#ffffff" height="24" width="1%"><asp:requiredfieldvalidator id="RequiredFieldValidator2" runat="server" ErrorMessage="Maximun Audit Trail Record  cannot be blank"
				    Display="Dynamic" ControlToValidate="TextMaxRecordAuditTrail">*</asp:requiredfieldvalidator>
			    <asp:RegularExpressionValidator id="RegularExpressionValidator1" runat="server" Display="Dynamic" ErrorMessage="Maximun Audit Trail Record must be a numeric value"
				    ControlToValidate="TextMaxRecordAuditTrail" ValidationExpression="^[0-9]+$">*</asp:RegularExpressionValidator></td>
		    <td bgColor="#ffffff" colspan="2">
                Maximun Audit Trail Record: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                <asp:textbox id="TextMaxRecordAuditTrail" runat="server" CssClass="textBox" MaxLength="20" Width="88px">0</asp:textbox>&nbsp;(0=unlimited)</td>
	    </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" height="24" style="width: 5px">
            </td>
            <td bgcolor="#ffffff" colspan="2">
                When Maximum audit trail record is reached :</td>
        </tr>
	    <tr class="formText">
		    <td bgcolor="#ffffff" height="24" style="width: 14px">
                &nbsp;</td>
            <td bgcolor="#ffffff" colspan="2">
                <asp:RadioButton ID="RadioOverwrite" runat="server" GroupName="option" Text="Overwrite Audit Trail as needed" AutoPostBack="True" /></td>
	    </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" height="24" style="width: 14px">
            </td>
            <td bgcolor="#ffffff" style="width: 289px" colspan="2">
                <asp:RadioButton ID="RadioAlert" runat="server" GroupName="option" Text="Alert Operator id audit trail almost reached threshold" Width="336px" AutoPostBack="True" /></td>
            
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 14px; height: 30px">
            </td>
            <td align="right" bgcolor="#ffffff" style="height: 30px">
                Operator Email Address &nbsp;&nbsp;
            </td>
            <td bgcolor="#ffffff" style="width: 600px; height: 30px">
			    <asp:TextBox id="TextEmail" runat="server" Width="216px" CssClass="textBox"></asp:TextBox><asp:RegularExpressionValidator
                        ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextEmail"
                        Display="Dynamic" ErrorMessage="Operator Email Address must is not valid."
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" height="24" style="width: 14px">
            </td>
            <td align="right" bgcolor="#ffffff" >
                Number of record before reach out&nbsp;&nbsp;</td>
            <td bgcolor="#ffffff" style="width: 243px">
			    <asp:TextBox id="TextRecordBeforeReachOut" runat="server" Width="120px" CssClass="textBox"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="TextRecordBeforeReachOut"
                    Display="Dynamic" ErrorMessage="Number of record before reach out must be a numeric value"
                    ValidationExpression="^[0-9]+$">*</asp:RegularExpressionValidator></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" height="24" style="width: 14px">
            </td>
            <td bgcolor="#ffffff" style="width: 289px" colspan="2">
                <asp:RadioButton ID="RadioManually" runat="server" Text="Do not overwrite audit trail (clear audit trail manualy)"
                    Width="336px" AutoPostBack="True" GroupName="option" /></td>
            
        </tr>
	    <tr class="formText" bgColor="#dddddd" height="30">
		    <td style="width: 14px"><IMG height="15" src="images/arrow.gif" width="15"></td>
		    <td colSpan="2">
			    <table cellSpacing="0" cellPadding="3" border="0">
				    <tr>
					    <td><asp:imagebutton id="ImageSave" runat="server" CausesValidation="True" SkinID="SaveButton"></asp:imagebutton></td>
					    <td><asp:imagebutton id="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton"></asp:imagebutton></td>
				    </tr>
			    </table>
                <asp:CustomValidator ID="cvalPageErr" runat="server" Display="None"></asp:CustomValidator></td>
	    </tr>
    </table>
	</ajax:ajaxpanel>
</asp:Content>