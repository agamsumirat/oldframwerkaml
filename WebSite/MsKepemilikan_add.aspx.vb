#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports System.Collections.Generic
#End Region

Partial Class MsKepemilikan_add
    Inherits Parent



#Region "Function"

    ''' <summary>
    ''' Validasi men-handle seluruh syarat2 data valid
    ''' data yang lolos dari validasi dipastikan lolos ke database 
    ''' karena menggunakan FillorNothing
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ValidasiControl()

        '======================== Validasi textbox :  ID canot Null ====================================================
        If ObjectAntiNull(txtIDKepemilikan.Text) = False Then Throw New Exception("ID  must be filled  ")
        If Not IsNumeric(Me.txtIDKepemilikan.Text) Then
            Throw New Exception("ID must be in number format")
        End If
        '======================== Validasi textbox :  Nama canot Null ====================================================
        If ObjectAntiNull(txtNamaKepemilikan.Text) = False Then Throw New Exception("Nama  must be filled  ")


        If Not DataRepository.MsKepemilikanProvider.GetByIDKepemilikan(txtIDKepemilikan.Text) Is Nothing Then
            Throw New Exception("ID already exist in the database")
        End If

        If DataRepository.MsKepemilikan_ApprovalDetailProvider.GetPaged(MsKepemilikan_ApprovalDetailColumn.IDKepemilikan.ToString & "=" & txtIDKepemilikan.Text, "", 0, 1, Nothing).Count > 0 Then
            Throw New Exception("Data exist in List Waiting Approval")
        End If


        'If LBMapping.Items.Count = 0 Then Throw New Exception("data doesnt have list Mapping")
    End Sub

#End Region

#Region "events..."
    Protected Sub ImgBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBack.Click
        Response.Redirect("MsKepemilikan_View.aspx")
    End Sub
    Protected Sub imgBrowse_click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBrowse.Click
        Try
            If Not Get_DataFromPicker Is Nothing Then
                Listmaping.AddRange(Get_DataFromPicker)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgDeleteItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgDeleteItem.Click
        Try
            If LBMapping.SelectedIndex < 0 Then
                Throw New Exception("No data Selected")
            End If
            Listmaping.RemoveAt(LBMapping.SelectedIndex)

        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                'AuditTrailBLL.InsertAuditTrailUserAccess(Sahassa.CommonCTRWeb.Commonly.SessionCurrentPage)
                Listmaping = New TList(Of MappingMsKepemilikanNCBSPPATK)
            Catch ex As Exception
                LogError(ex)
                CvalPageErr.IsValid = False
                CvalPageErr.ErrorMessage = ex.Message
            End Try
        End If

    End Sub



    Protected Sub ImgBtnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnSave.Click
        Try
            Page.Validate("handle")
            If Page.IsValid Then
                ValidasiControl()
                'create audittrailtype
                'Dim objAuditTrailType As MsAuditTrailEntityType = DataRepository.MsAuditTrailEntityTypeProvider.GetByPk_MsAuditTrailEntityTypeId(18)
                ' =========== Insert Header Approval
                Dim KeyHeaderApproval As Integer
                Using ObjMsNamaKepemilikan_Approval As New MsKepemilikan_Approval
                    With ObjMsNamaKepemilikan_Approval
                        FillOrNothing(.FK_MsMode_Id, 1, True, oInt)
                        FillOrNothing(.RequestedBy, SessionPkUserId)
                        FillOrNothing(.RequestedDate, Date.Now)
                        .IsUpload = False
                    End With
                    DataRepository.MsKepemilikan_ApprovalProvider.Save(ObjMsNamaKepemilikan_Approval)
                    KeyHeaderApproval = ObjMsNamaKepemilikan_Approval.PK_MsKepemilikan_Approval_Id
                End Using
                '============ Insert Detail Approval
                Using objMsKepemilikan_ApprovalDetail As New MsKepemilikan_ApprovalDetail()
                    With objMsKepemilikan_ApprovalDetail
                        FillOrNothing(.IDKepemilikan, txtIDKepemilikan.Text, True, oInt)
                        FillOrNothing(.NamaKepemilikan, txtNamaKepemilikan.Text, True, Ovarchar)

                        FillOrNothing(.Activation, 1, True, oBit)
                        FillOrNothing(.CreatedDate, Date.Now)
                        FillOrNothing(.CreatedBy, SessionUserId)
                        FillOrNothing(.FK_MsKepemilikan_Approval_Id, KeyHeaderApproval)
                    End With
                    DataRepository.MsKepemilikan_ApprovalDetailProvider.Save(objMsKepemilikan_ApprovalDetail)
                    Dim keyHeaderApprovalDetail As Integer = objMsKepemilikan_ApprovalDetail.PK_MsKepemilikan_ApprovalDetail_Id
                    'Insert auditrail
                    'AuditTrailBLL.InsertAuditTrail(AuditTrailOperation.Add, objAuditTrailType, Now(), Sahassa.CommonCTRWeb.Commonly.SessionNIK, Sahassa.CommonCTRWeb.Commonly.SessionStaffName, "Request for approval inserted  data MsNamaKepemilikan", objMsNamaKepemilikan_ApprovalDetail, Nothing)
                    '========= Insert mapping item 
                    Dim LobjMappingMsNamaKepemilikanNCBSPPATK_Approval_Detail As New TList(Of MappingMsKepemilikanNCBSPPATK_Approval_Detail)
                    For Each objMappingMsNamaKepemilikanNCBSPPATK As MappingMsKepemilikanNCBSPPATK In Listmaping
                        Dim Mapping As New MappingMsKepemilikanNCBSPPATK_Approval_Detail
                        With Mapping
                            FillOrNothing(.IDKepemilikan, objMsKepemilikan_ApprovalDetail.IDKepemilikan)
                            FillOrNothing(.IDKepemilikanNCBS, objMappingMsNamaKepemilikanNCBSPPATK.IDKepemilikanNCBS)
                            FillOrNothing(.PK_MsKepemilikan_Approval_Id, KeyHeaderApproval)
                            FillOrNothing(.PK_MappingMsKepemilikanNCBSPPATK_approval_detail_Id, keyHeaderApprovalDetail)
                            FillOrNothing(.Nama, objMappingMsNamaKepemilikanNCBSPPATK.Nama)
                            LobjMappingMsNamaKepemilikanNCBSPPATK_Approval_Detail.Add(Mapping)
                        End With
                    Next
                    DataRepository.MappingMsKepemilikanNCBSPPATK_Approval_DetailProvider.Save(LobjMappingMsNamaKepemilikanNCBSPPATK_Approval_Detail)
                    'session Maping item Clear
                    Listmaping.Clear()
                    LblConfirmation.Text = "MsKepemilikan data is waiting approval for Insert"
                    MtvMsUser.ActiveViewIndex = 1
                    ImgBtnSave.Visible = False
                    ImgBackAdd.Visible = False
                End Using
            End If
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImgBtnAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAdd.Click
        Try
            ClearAllControl(VwAdd)
            MtvMsUser.ActiveViewIndex = 0
            ImgBtnSave.Visible = True
            ImgBackAdd.Visible = True
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBackAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBackAdd.Click
        Response.Redirect("MsKepemilikan_View.aspx")
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            BindListMapping()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub BindListMapping()
        LBMapping.DataSource = ListMappingDisplay
        LBMapping.DataBind()
    End Sub

#End Region

#Region "Property..."
    ''' <summary>
    ''' Mengambil Data dari Picker
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Get_DataFromPicker() As TList(Of MappingMsKepemilikanNCBSPPATK)
        Get
            Dim Temp As New TList(Of MappingMsKepemilikanNCBSPPATK)
            Dim L_MsKepemilikanNCBS As TList(Of MsKepemilikanNCBS)
            Try
                If Session("PickerMsKepemilikanNCBS.Data") Is Nothing Then
                    Throw New Exception
                End If
                L_MsKepemilikanNCBS = Session("PickerMsKepemilikanNCBS.Data")
                For Each i As MsKepemilikanNCBS In L_MsKepemilikanNCBS
                    Dim Tempmapping As New MappingMsKepemilikanNCBSPPATK
                    With Tempmapping
                        .IDKepemilikanNCBS = i.IDKepemilikanNCBS
                        .Nama = i.NamaKepemilikanNCBS
                    End With
                    Temp.Add(Tempmapping)
                Next
            Catch ex As Exception
                L_MsKepemilikanNCBS = Nothing
            End Try
            Return Temp
        End Get
    End Property
    ''' <summary>
    ''' Menyimpan Item untuk mapping sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Listmaping() As TList(Of MappingMsKepemilikanNCBSPPATK)
        Get
            Return Session("Listmaping.data")
        End Get
        Set(ByVal value As TList(Of MappingMsKepemilikanNCBSPPATK))
            Session("Listmaping.data") = value
        End Set
    End Property
    ''' <summary>
    ''' List yang tampil di ListBox
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplay() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingMsKepemilikanNCBSPPATK In Listmaping.FindAllDistinct("IDKepemilikanNCBS")
                Temp.Add(i.IDKepemilikanNCBS.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property

#End Region

End Class



