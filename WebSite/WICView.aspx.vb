Imports System.Collections.Generic
Imports System.Data
Imports Sahassa.AML
Imports AMLBLL
Imports System.IO
Imports Sahassanettier.Data
Imports Sahassanettier.Entities
Imports Sahassanettier.Services
Imports Ionic.Zlib
Imports Ionic.Zip
Imports CookComputing.XmlRpc
Imports System.Data.SqlClient

Partial Class WICView
    Inherits Parent
    Private BindGridFromExcel As Boolean = False

#Region " Property "

    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("WICViewSelected") Is Nothing, New ArrayList, Session("WICViewSelected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("WICViewSelected") = value
        End Set
    End Property

    Private Property SetnGetFieldSearch() As String
        Get
            Return IIf(Session("WICViewFieldSearch") Is Nothing, "", Session("WICViewFieldSearch"))
        End Get
        Set(ByVal Value As String)
            Session("WICViewFieldSearch") = Value
        End Set
    End Property

    Private Property SetnGetValueSearch() As String
        Get
            Return IIf(Session("WICViewValueSearch") Is Nothing, "", Session("WICViewValueSearch"))
        End Get
        Set(ByVal Value As String)
            Session("WICViewValueSearch") = Value
        End Set
    End Property

    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("WICViewSort") Is Nothing, "Pk_WIC_Id  asc", Session("WICViewSort"))
        End Get
        Set(ByVal Value As String)
            Session("WICViewSort") = Value
        End Set
    End Property

    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("WICViewCurrentPage") Is Nothing, 0, Session("WICViewCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("WICViewCurrentPage") = Value
        End Set
    End Property

    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property

    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("WICViewRowTotal") Is Nothing, 0, Session("WICViewRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("WICViewRowTotal") = Value
        End Set
    End Property

    Private Function SetnGetBindTable() As VList(Of Vw_WICView)
        Return DataRepository.Vw_WICViewProvider.GetPaged(SearchFilter, SetnGetSort, SetnGetCurrentPage, Sahassa.AML.Commonly.GetDisplayedTotalRow, SetnGetRowTotal)
    End Function

    Private Function SetnGetBindTableAll() As VList(Of Vw_WICView)
        Return DataRepository.Vw_WICViewProvider.GetPaged(SearchFilter, SetnGetSort, 0, Integer.MaxValue, SetnGetRowTotal)
    End Function

    Private Property SetAndGetSearchingCriteria() As String
        Get
            Return IIf(Session("WICViewSearchCriteria") Is Nothing, "", Session("WICViewSearchCriteria"))
        End Get
        Set(ByVal Value As String)
            Session("WICViewSearchCriteria") = Value
        End Set
    End Property

#End Region 'done

#Region "Bind Combo Box"
    Private Sub BindIdType()
        Try
            Using ObjList As New AMLBLL.WICBLL
                Me.CboIDType.Items.Clear()
                Me.CboIDType.DataSource = ObjList.GetViewIdType("", "MsIDType_Name", 0, Integer.MaxValue, 0)
                Me.CboIDType.DataValueField = "Pk_MsIDType_Id"
                Me.CboIDType.DataTextField = "MsIDType_Name"
                Me.CboIDType.DataBind()
                Me.CboIDType.Items.Insert(0, New ListItem("[All]", "0"))
                Me.CboIDType.SelectedIndex = 0
            End Using
        Catch
            Throw
        End Try
    End Sub
#End Region 'done

#Region "Function..."

    Private Function GetAllIDSelected(ByVal arrayList As ArrayList) As String
        Dim strVal As String = ""
        For Each pkid As String In arrayList
            strVal = strVal & pkid & ","
        Next
        Dim strRetVal As String = strVal.Substring(0, strVal.LastIndexOf(","))
        Return strRetVal
    End Function

    Private Function IsExistInWICGeneratedFileSequence(ByVal objWic As WIC, ByRef ctrxmlwicname As String) As Boolean
        Dim retval As Boolean = False
        Using ObjData As TList(Of WICGeneratedFile) = DataRepository.WICGeneratedFileProvider.GetPaged("FK_WIC_Id = " & CInt(objWic.PK_WIC_Id), "", 0, 1, 0)
            If ObjData.Count > 0 Then
                ctrxmlwicname = ObjData(0).TransactionDate.Value.ToString("yyyyMMdd") & "-" & ObjData(0).CTRWICSequenceNumber & ".xml"
                retval = True
            End If
        End Using
        Return retval
    End Function


    Function isNotExistInApproval(ByVal PK_WIC As Integer) As Boolean
        Try
            Using objWICApprovalDetail As TList(Of WIC_ApprovalDetail) = DataRepository.WIC_ApprovalDetailProvider.GetPaged(WIC_ApprovalDetailColumn.PK_WIC_Id.ToString & " = " & PK_WIC, "", 0, Integer.MaxValue, 0)
                If objWICApprovalDetail.Count > 0 Then
                    Using objWICApproval As WIC_Approval = DataRepository.WIC_ApprovalProvider.GetByPK_WIC_Approval_Id(objWICApprovalDetail(0).FK_WIC_Approval_Id.GetValueOrDefault)
                        If Not IsNothing(objWICApproval) Then
                            If objWICApproval.FK_MsMode_Id.GetValueOrDefault = 2 Then
                                Using objMsUser As User = DataRepository.UserProvider.GetBypkUserID(objWICApproval.RequestedBy)
                                    If Not IsNothing(objMsUser) Then
                                        Throw New Exception("WIC data has edited by " & objMsUser.UserName & " and waiting for approval")
                                    End If
                                End Using
                            ElseIf objWICApproval.FK_MsMode_Id.GetValueOrDefault = 3 Then
                                Using objMsUser As User = DataRepository.UserProvider.GetBypkUserID(objWICApproval.RequestedBy)
                                    If Not IsNothing(objMsUser) Then
                                        Throw New Exception("WIC data has deleted by " & objMsUser.UserName & " and waiting for approval")
                                    End If
                                End Using
                            End If
                        End If
                    End Using
                End If
            End Using

            Return True
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
            Return False
        End Try
    End Function

    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
            CollectSelected()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Function isValidSearch() As Boolean
        Try
            If txtTransactionDate.Text <> "" Then
                If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", txtTransactionDate.Text) = False Then
                    Throw New Exception("Transaction date format not valid")
                End If
                If txtTransactionDate.Text > Now Then
                    Throw New Exception("Transactin date should be not greater than today")
                End If
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
            Return False
        End Try
        Return True
    End Function

    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
        Catch
            Throw
        End Try
    End Sub

    Public Sub BindGrid()
        Me.GridDataView.DataSource = Me.SetnGetBindTable
        Me.GridDataView.VirtualItemCount = Me.SetnGetRowTotal
        Me.GridDataView.DataBind()
    End Sub

    Private Sub ClearThisPageSessions()
        Session("WICViewSelected") = Nothing
        Session("WICViewFieldSearch") = Nothing
        Session("WICViewValueSearch") = Nothing
        Session("WICViewSort") = Nothing
        Session("WICViewCurrentPage") = Nothing
        Session("WICViewRowTotal") = Nothing
        Session("WICViewData") = Nothing
    End Sub

    Private Function SearchFilter() As String
        Dim StrSearch As String = ""
        Try
            If isValidSearch() Then


                If Me.TxtFullName.Text <> "" Then
                    If StrSearch = "" Then
                        StrSearch = StrSearch & " INDV_NamaLengkap like '%" & Me.TxtFullName.Text.Replace("'", "''") & "%'"
                    Else
                        StrSearch = StrSearch & " And INDV_NamaLengkap like '%" & Me.TxtFullName.Text.Replace("'", "''") & "%' "
                    End If
                End If

                If Me.TxtDateOfBirth.Text <> "" Then
                    If StrSearch = "" Then
                        StrSearch = StrSearch & " INDV_TanggalLahir = '" & Me.TxtDateOfBirth.Text.Replace("'", "''") & "' "
                    Else
                        StrSearch = StrSearch & " And INDV_TanggalLahir = '" & Me.TxtDateOfBirth.Text.Replace("'", "''") & "' "
                    End If
                End If

                If Me.CboIDType.SelectedIndex <> 0 Then
                    If StrSearch = "" Then
                        StrSearch = StrSearch & " INDV_FK_MsIDType_Id = '" & Me.CboIDType.SelectedValue & "' "
                    Else
                        StrSearch = StrSearch & " And INDV_FK_MsIDType_Id = '" & Me.CboIDType.SelectedValue & "' "
                    End If
                End If

                If Me.TxtIDNo.Text <> "" Then
                    If StrSearch = "" Then
                        StrSearch = StrSearch & " INDV_NomorId like '%" & Me.TxtIDNo.Text.Replace("'", "''") & "%' "
                    Else
                        StrSearch = StrSearch & " and INDV_NomorId like '%" & Me.TxtIDNo.Text.Replace("'", "''") & "%' "
                    End If
                End If

                If Me.txtNPWP.Text <> "" Then
                    If StrSearch = "" Then
                        StrSearch = StrSearch & " INDV_NPWP like '%" & Me.txtNPWP.Text.Replace("'", "''") & "%' "
                    Else
                        StrSearch = StrSearch & " and INDV_NPWP like '%" & Me.txtNPWP.Text.Replace("'", "''") & "%'"
                    End If
                End If

                If Me.txtTransactionDate.Text <> "" Then
                    If StrSearch = "" Then
                        StrSearch = StrSearch & " (LastTransactionDateCashIn = '" & Me.txtTransactionDate.Text.Replace("'", "''") & "' or LastTransactionDateCashOut = '" & Me.txtTransactionDate.Text.Replace("'", "''") & "') "
                    Else
                        StrSearch = StrSearch & " and (LastTransactionDateCashIn = '" & Me.txtTransactionDate.Text.Replace("'", "''") & "' or LastTransactionDateCashOut = '" & Me.txtTransactionDate.Text.Replace("'", "''") & "') "
                    End If
                End If

                If TxtPPATK.Text.Trim <> "" Then
                    If StrSearch = "" Then
                        StrSearch = StrSearch & " PPATKConfirmationNo like '%" & Me.TxtPPATK.Text.Replace("'", "''") & "%' "
                    Else
                        StrSearch = StrSearch & " and PPATKConfirmationNo like '%" & Me.TxtPPATK.Text.Replace("'", "''") & "%'"
                    End If
                End If
                If Me.TxtlastUpdateStart.Text <> "" Then
                    If StrSearch = "" Then
                        StrSearch = StrSearch & " (LastUpdateDate between '" & Me.TxtlastUpdateStart.Text.Replace("'", "''") & "' and '" & Me.TxtlastUpdateStart.Text.Replace("'", "''") & " 23:59:59') "
                    Else
                        StrSearch = StrSearch & " and (LastUpdateDate between '" & Me.TxtlastUpdateStart.Text.Replace("'", "''") & "' and '" & Me.TxtlastUpdateStart.Text.Replace("'", "''") & " 23:59:59') "
                    End If
                End If


                If Me.txtTransNominalBCashIn.Text <> "" And txtTransNominalUCashIn.Text <> "" Then
                    If isValidNominal("cashin") Then
                        If StrSearch = "" Then
                            StrSearch = StrSearch & " CashInNominal between " & Me.txtTransNominalBCashIn.Text.Replace("'", "''") & " and " & Me.txtTransNominalUCashIn.Text.Replace("'", "''")
                        Else
                            StrSearch = StrSearch & " and CashInNominal between " & Me.txtTransNominalBCashIn.Text.Replace("'", "''") & " and " & Me.txtTransNominalUCashIn.Text.Replace("'", "''")
                        End If
                    End If
                End If


                If Me.txtTransNominalBCashOut.Text <> "" And txtTransNominalUCashOut.Text <> "" Then
                    If isValidNominal("cashout") Then
                        If StrSearch = "" Then
                            StrSearch = StrSearch & " CashOutNominal between " & Me.txtTransNominalBCashOut.Text.Replace("'", "''") & " and " & Me.txtTransNominalUCashOut.Text.Replace("'", "''")
                        Else
                            StrSearch = StrSearch & " and CashOutNominal between " & Me.txtTransNominalBCashOut.Text.Replace("'", "''") & " and " & Me.txtTransNominalUCashOut.Text.Replace("'", "''")
                        End If
                    End If
                End If



                If Me.txtPJKOffice.Text <> "" Then
                    If StrSearch = "" Then
                        StrSearch = StrSearch & " KantorPJKCashIn like '%" & Me.txtPJKOffice.Text.Replace("'", "''") & "%' or KantorPJKCashOut like '%" & txtPJKOffice.Text & "%' "
                    Else
                        StrSearch = StrSearch & " and (KantorPJKCashIn like '%" & Me.txtPJKOffice.Text.Replace("'", "''") & "%' or KantorPJKCashOut like '%" & txtPJKOffice.Text & "%') "
                    End If
                End If


                Return StrSearch
            End If
        Catch
            Throw
            Return ""
        End Try
    End Function

    Function isValidNominal(ByVal type As String) As Boolean
        Try
            If type.ToLower = "cashin" Then
                If IsNumeric(txtTransNominalBCashIn.Text) = False Then
                    Throw New Exception("Nominal CashIn must be filled by number")
                End If
                If IsNumeric(txtTransNominalUCashIn.Text) = False Then
                    Throw New Exception("Nominal CashIn must be filled by number")
                End If
            ElseIf type.ToLower = "cashout" Then
                If IsNumeric(txtTransNominalBCashOut.Text) = False Then
                    Throw New Exception("Nominal CashOut must be filled by number")
                End If
                If IsNumeric(txtTransNominalUCashOut.Text) = False Then
                    Throw New Exception("Nominal CashOut must be filled by number")
                End If
            End If
            Return True

        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
            Return False
        End Try
    End Function
#End Region 'done

#Region "Excel Export Event"

    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In Me.GridDataView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = gridRow.FindControl("CheckBoxExporttoExcel")
                Dim PkId As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
                If ArrTarget.Contains(PkId) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(PkId)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(PkId)
                End If
                Me.SetnGetSelectedItem = ArrTarget
            End If
        Next
    End Sub

    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub 'ClearControls

    Private Sub BindSelected()
        Dim Rows As New ArrayList

        Dim SbPk As New StringBuilder
        For Each IdPk As Int64 In Me.SetnGetSelectedItem
            SbPk.Append(IdPk.ToString & ", ")
        Next
        If SbPk.ToString <> "" Then
            Dim rowData As VList(Of Vw_WICView) = DataRepository.Vw_WICViewProvider.GetPaged("Pk_WIC_Id in (" & SbPk.ToString.Substring(0, SbPk.Length - 2) & ") ", "", 0, Sahassa.AML.Commonly.GetDisplayedTotalRow, 0)
            For Each rowList As Vw_WICView In rowData
                Rows.Add(rowList)
            Next
            SbPk = Nothing
        End If

        Me.GridDataView.DataSource = Rows
        Me.GridDataView.AllowPaging = False
        Me.GridDataView.DataBind()

        Me.GridDataView.Columns(0).Visible = False
        Me.GridDataView.Columns(1).Visible = False
        Me.GridDataView.Columns(7).Visible = False
    End Sub

    Private Function BindSelectedForExcel() As TList(Of WIC)
        Dim Rows As New ArrayList

        Dim SbPk As New StringBuilder
        For Each IdPk As Int64 In Me.SetnGetSelectedItem
            SbPk.Append(IdPk.ToString & ", ")
        Next
        'If SbPk.ToString <> "" Then
        Dim rowData As TList(Of WIC) = DataRepository.WICProvider.GetPaged("Pk_WIC_Id in (" & SbPk.ToString.Substring(0, SbPk.Length - 2) & ") ", "", 0, Sahassa.AML.Commonly.GetDisplayedTotalRow, 0)
        Return rowData
    End Function

    Private Sub BindSelectedAll()
        Try

            Me.GridDataView.DataSource = DataRepository.Vw_WICViewProvider.GetPaged(SearchFilter, Me.SetnGetSort, 0, Sahassa.AML.Commonly.SessionPagingLimit, 0)

            Me.GridDataView.AllowPaging = False
            Me.GridDataView.DataBind()

            Me.GridDataView.Columns(0).Visible = False
            Me.GridDataView.Columns(1).Visible = False
            Me.GridDataView.Columns(7).Visible = False
        Catch
            Throw
        End Try

    End Sub

    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        'For Each gridRow As DataGridItem In Me.GridDataView.Items
        '    If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
        '        Dim PkId As String = gridRow.Cells(1).Text
        '        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        '        If Me.SetnGetSelectedItem.Contains(PkId) Then
        '            If Me.CheckBoxSelectAll.Checked Then
        '                If Not ArrTarget.Contains(PkId) Then
        '                    ArrTarget.Add(PkId)
        '                End If
        '            Else
        '                ArrTarget.Remove(PkId)
        '            End If
        '        Else
        '            If Me.CheckBoxSelectAll.Checked Then
        '                If Not ArrTarget.Contains(PkId) Then
        '                    ArrTarget.Add(PkId)
        '                End If
        '            End If
        '        End If
        '        Me.SetnGetSelectedItem = ArrTarget
        '    End If
        'Next
        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        For Each gridRow As DataGridItem In GridDataView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(1).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItem.Contains(StrKey) = False Then
                        ArrTarget.Add(StrKey)
                    End If
                Else
                    If Me.SetnGetSelectedItem.Contains(StrKey) Then
                        ArrTarget.Remove(StrKey)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItem = ArrTarget
        Me.BindGrid()
    End Sub

    Private Sub SetCheckedAll()
        Dim i As Int16 = 0
        Dim totalrow As Int16 = 0
        For Each gridRow As DataGridItem In Me.GridDataView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = CType(gridRow.FindControl("CheckBoxExporttoExcel"), CheckBox)
                If chkBox.Checked Then
                    i = CType(i + 1, Int16)
                End If
                totalrow = CType(totalrow + 1, Int16)
            End If
        Next
        If i = totalrow Then
            Me.CheckBoxSelectAll.Checked = True
        Else
            Me.CheckBoxSelectAll.Checked = False
        End If
    End Sub

#End Region

#Region "events..."

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using

                Me.ClearThisPageSessions()

                Me.popUpDateOfBirth.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TxtDateOfBirth.ClientID & "'), 'dd-mmm-yyyy')")
                Me.popUpDateOfBirth.Style.Add("display", "")
                Me.popUpTransactionDate.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtTransactionDate.ClientID & "'), 'dd-mmm-yyyy')")
                Me.popUpTransactionDate.Style.Add("display", "")

                'Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                'Using AccessAudit As New AMLBLL.AuditTrailBLL
                '    AccessAudit.InsertAuditTrailUserAccess(Sahassa.CommonCTRWeb.Commonly.SessionNIK, Sahassa.CommonCTRWeb.Commonly.SessionStaffName, "Accesssing page " & Sahassa.CommonCTRWeb.Commonly.SessionCurrentPage & " succeeded")
                'End Using

                'Using ControlerGetUserAccess As New AMLBLL.AccessAuthorityBLL
                '    If ControlerGetUserAccess.BolIsUserHasAccess(Sahassa.AML.Commonly.SessionGroupId, "WICView.aspx") = True Then
                '        Me.LinkButtonAddNew.Visible = True
                '    Else
                '        Me.LinkButtonAddNew.Visible = False
                '    End If
                'End Using
                Me.GridDataView.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow
                BindIdType()
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.CollectSelected()
            Me.BindGrid()
            SetCheckedAll()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Imagebutton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imagebutton1.Click
        Try
            Me.SetAndGetSearchingCriteria = Nothing
            Me.TxtFullName.Text = ""
            Me.TxtDateOfBirth.Text = ""
            Me.CboIDType.SelectedIndex = 0
            Me.TxtIDNo.Text = ""
            Me.txtNPWP.Text = ""
            Me.txtTransactionDate.Text = ""
            Me.txtTransNominalBCashIn.Text = ""
            Me.txtTransNominalUCashIn.Text = ""
            Me.txtTransNominalBCashOut.Text = ""
            Me.txtTransNominalUCashOut.Text = ""
            Me.txtPJKOffice.Text = ""


        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridDataView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridDataView.ItemDataBound
        Try


            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then

                e.Item.Cells(8).Text = CStr((e.Item.ItemIndex + 1) + (Me.SetnGetCurrentPage * Sahassa.AML.Commonly.GetDisplayedTotalRow))

                If e.Item.Cells(19).Text <> "" And e.Item.Cells(19).Text <> "&bnsp;" Then
                    e.Item.Cells(19).Text = ValidateBLL.FormatMoneyWithComma(e.Item.Cells(19).Text)
                End If
                If e.Item.Cells(20).Text <> "" And e.Item.Cells(20).Text <> "&bnsp;" Then
                    e.Item.Cells(20).Text = ValidateBLL.FormatMoneyWithComma(e.Item.Cells(20).Text)
                End If

                Using objLblStatus As Label = CType(e.Item.FindControl("LblStatus"), Label)
                    Using objExistApproval As TList(Of WIC_ApprovalDetail) = DataRepository.WIC_ApprovalDetailProvider.GetPaged(WIC_ApprovalDetailColumn.PK_WIC_Id.ToString & " = " & e.Item.Cells(1).Text.ToString, "", 0, Integer.MaxValue, 0)
                        If objExistApproval.Count > 0 Then
                            objLblStatus.Text = "Waiting for approval"
                        End If
                    End Using
                End Using

                'Using objLblNPWP As Label = CType(e.Item.FindControl("LblNPWP"), Label)
                '    If e.Item.Cells(3).Text = "0" Then
                '        objLblNPWP.Text = e.Item.Cells(4).Text
                '    Else
                '        objLblNPWP.Text = e.Item.Cells(5).Text
                '    End If
                'End Using

                If e.Item.Cells(2).Text <> "&nbsp;" Then
                    Using objLblIDType As Label = CType(e.Item.FindControl("LblIDType"), Label)
                        Using ObjIDType As MsIDType = DataRepository.MsIDTypeProvider.GetByPk_MsIDType_Id(CInt(e.Item.Cells(2).Text))
                            If Not IsNothing(ObjIDType) Then
                                objLblIDType.Text = ObjIDType.MsIDType_Name
                            End If
                        End Using
                    End Using
                End If

                'Using objLblName As Label = CType(e.Item.FindControl("lblName"), Label)
                '    If e.Item.Cells(3).Text = "0" Then
                '        objLblName.Text = e.Item.Cells(6).Text
                '    Else
                '        objLblName.Text = e.Item.Cells(7).Text
                '    End If
                'End Using


                Dim chkBox As CheckBox = e.Item.FindControl("CheckBoxExporttoExcel")
                chkBox.Checked = Me.SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)
                If BindGridFromExcel = True Then
                    e.Item.Cells(2).Text = CStr((e.Item.ItemIndex + 1))
                Else
                    e.Item.Cells(2).Text = CStr((e.Item.ItemIndex + 1) + (Me.SetnGetCurrentPage * Sahassa.AML.Commonly.GetDisplayedTotalRow))
                End If
                If e.Item.Cells(10).Text <> "" And e.Item.Cells(10).Text <> "&nbsp;" Then
                    e.Item.Cells(10).Text = Convert.ToDateTime(e.Item.Cells(10).Text).ToString("dd-MMM-yyyy")
                End If
                If e.Item.Cells(17).Text <> "" And e.Item.Cells(17).Text <> "&nbsp;" Then
                    e.Item.Cells(17).Text = Convert.ToDateTime(e.Item.Cells(17).Text).ToString("dd-MMM-yyyy")
                End If
                If e.Item.Cells(18).Text <> "" And e.Item.Cells(18).Text <> "&nbsp;" Then
                    e.Item.Cells(18).Text = Convert.ToDateTime(e.Item.Cells(18).Text).ToString("dd-MMM-yyyy")
                End If
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridDataView_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridDataView.SortCommand
        Dim GridUser As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            Me.CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be less than or equal to the total page count.")
                End If
            Else
                Throw New Exception("Page number must be less than or equal to the total page count.")
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub LinkButtonAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonAddNew.Click
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "WICAdd.aspx"
            Me.Response.Redirect("WICAdd.aspx", False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridDataView_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridDataView.ItemCommand
        If e.CommandName.ToLower = "detail" Then
            Response.Redirect("WICViewDetail.aspx?WIC_ID=" & e.Item.Cells(1).Text)
        ElseIf e.CommandName.ToLower = "edit" Then
            If isNotExistInApproval(CInt(e.Item.Cells(1).Text)) Then
                Response.Redirect("WICEdit.aspx?WIC_ID=" & e.Item.Cells(1).Text)
            End If
        ElseIf e.CommandName.ToLower = "delete" Then
            If isNotExistInApproval(CInt(e.Item.Cells(1).Text)) Then
                Response.Redirect("WICDelete.aspx?WIC_ID=" & e.Item.Cells(1).Text)
            End If
        End If
    End Sub

    Protected Sub GridDataView_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridDataView.ItemCreated
        Dim lit As ListItemType = e.Item.ItemType
        If ListItemType.Header = lit Then
            e.Item.SetRenderMethodDelegate(New RenderMethod(AddressOf NewRenderMethod))

            'Merupakan control header untuk merge rows setelah dilakukan custom header
            e.Item.Cells(0).RowSpan = 2
            e.Item.Cells(8).RowSpan = 2
            e.Item.Cells(9).RowSpan = 2
            e.Item.Cells(10).RowSpan = 2
            e.Item.Cells(11).RowSpan = 2
            e.Item.Cells(12).RowSpan = 2
            e.Item.Cells(13).RowSpan = 2
            e.Item.Cells(16).RowSpan = 2
            e.Item.Cells(21).RowSpan = 2
            e.Item.Cells(22).RowSpan = 2
            e.Item.Cells(23).RowSpan = 2
            e.Item.Cells(24).RowSpan = 2
            'e.Item.Cells(25).RowSpan = 2


        End If
    End Sub

    Private Sub NewRenderMethod(ByVal writer As HtmlTextWriter, ByVal ctl As Control)
        Dim cell As TableCell = DirectCast(ctl.Controls(ctl.Controls.Count - 1), TableCell)
        For i As Integer = 0 To ctl.Controls.Count - 7   'dikurang sama yang hidden
            If i = 0 Then
                'create header column pertama - rowspan=""2"" untuk membentuk merge rows sebanyak 2 rows
                writer.Write("<TD rowspan=""2"" style=""color: white"" align=""center"">  </TD>" & vbLf)
            ElseIf i = 1 Then
                ctl.Controls(8).RenderControl(writer)
            ElseIf i = 2 Then
                ctl.Controls(9).RenderControl(writer)
            ElseIf i = 3 Then
                ctl.Controls(10).RenderControl(writer)
            ElseIf i = 4 Then
                ctl.Controls(11).RenderControl(writer)
            ElseIf i = 5 Then
                ctl.Controls(12).RenderControl(writer)
            ElseIf i = 6 Then
                ctl.Controls(13).RenderControl(writer)
            ElseIf i = 7 Then
                'create header column kedua samapi ke empat -  colspan=""3"" untuk membentuk merge column sebanyak 3 column 
                writer.Write("<TD colspan=""2"" style=""color: white"" align=""center""> PJK Office </TD>" & vbLf)
            ElseIf i = 9 Then
                writer.Write("<TD colspan=""2"" style=""color: white"" align=""center""> Transaction Date </TD>" & vbLf)
            ElseIf i = 11 Then
                writer.Write("<TD colspan=""2"" style=""color: white"" align=""center""> Transaction Nominal </TD>" & vbLf)
            ElseIf i = 12 Then
                ctl.Controls(21).RenderControl(writer)
            ElseIf i = 13 Then
                ctl.Controls(22).RenderControl(writer)
            ElseIf i = 14 Then
                ctl.Controls(23).RenderControl(writer)
            ElseIf i = 15 Then
                ctl.Controls(24).RenderControl(writer)
            ElseIf i = 16 Then

                writer.Write("<TD colspan=""3"" rowspan=""2"" style=""color: white"" align=""center""> Action </TD>" & vbLf)
            End If
        Next

        'Merupakan control header dari datagrid   



        'ctl.Controls(21).RenderControl(writer)
        'ctl.Controls(22).RenderControl(writer)
        'ctl.Controls(23).RenderControl(writer)
        'ctl.Controls(24).RenderControl(writer)
        'ctl.Controls(25).RenderControl(writer)

        GridDataView.HeaderStyle.AddAttributesToRender(writer)

        '***     Insert the second row 
        writer.RenderBeginTag("TR")

        'For i As Integer = 0 To ctl.Controls.Count - 1
        '    ctl.Controls(i).RenderControl(writer)
        'Next

        ctl.Controls(14).RenderControl(writer)
        ctl.Controls(15).RenderControl(writer)
        ctl.Controls(16).RenderControl(writer)
        ctl.Controls(17).RenderControl(writer)
        ctl.Controls(18).RenderControl(writer)
        ctl.Controls(19).RenderControl(writer)
        ctl.Controls(20).RenderControl(writer)

        writer.RenderEndTag()
    End Sub

    Protected Sub lnkExportData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExportData.Click
        Dim lstFile As New List(Of String)
        Dim dirPath As String = Server.MapPath(Request.ServerVariables("PATH_INFO")).Substring(0, Server.MapPath(Request.ServerVariables("PATH_INFO")).LastIndexOf("\") + 1)
        Dim dateDownload As DateTime = Now

        Try
            Me.CollectSelected()
            If Me.SetnGetSelectedItem.Count = 0 AndAlso rblData.SelectedIndex = 0 Then
                Throw New Exception("There are no data for export")
            Else
                Me.BindSelected()
                If cboFormatFile.SelectedValue = "Excel" Then
                    If rblData.SelectedIndex = 0 Then
                        'selected data
                        Dim strID As String = GetAllIDSelected(Me.SetnGetSelectedItem)
                        Dim tanggalGenerated As DateTime = Now

                        ' ''memakai services
                        'Dim proxy As CTRWebBLL.ICTRGeneratorServices
                        'Dim Successrun As Boolean = False
                        'proxy = CType(XmlRpcProxyGen.Create(GetType(CTRWebBLL.ICTRGeneratorServices)), CTRWebBLL.ICTRGeneratorServices)
                        'proxy.Url = System.Configuration.ConfigurationManager.AppSettings("ProxyURLPath")
                        'Successrun = proxy.CallProcessGenerateXlsWICReportFromWICView(strID, tanggalGenerated.ToString("yyyy-MM-dd"), Commonly.SessionStaffName)
                        CTRNewFileGeneratorBll.CallProcessGenerateXlsWICReportFromWICView(strID, tanggalGenerated.ToString("yyyy-MM-dd"), Commonly.SessionUserId, dirPath)

                        Using objProgress As WICGeneratedFileProgress = DataRepository.WICGeneratedFileProgressProvider.GetByPK_WICGeneratedFileProgress_Id(1)
                            If Not objProgress Is Nothing Then
                                If CBool(objProgress.IsError) = True Then
                                    Throw New Exception(objProgress.ErrorDescription)
                                End If
                            End If
                        End Using

                        Using obj As TList(Of WICGeneratedFileXls) = DataRepository.WICGeneratedFileXlsProvider.GetPaged("DATEDIFF(DAY, TransactionDate,'" & tanggalGenerated.ToString("yyyy-MM-dd") & "')=0", "CreatedDate desc", 0, 1, 0)
                            If obj.Count > 0 Then
                                Response.Redirect("DownloadAttachment.aspx?IsBinary=Y&ext=xls&id=" & obj(0).PK_WICGeneratedFileXls_Id & "&DataSource=WIC")
                            End If
                        End Using
                    Else
                        'all data
                        Dim sql As String
                        sql = "SELECT COUNT(*) FROM WIC l"
                        Dim sqlCmd As New SqlCommand(sql)
                        sqlCmd.CommandTimeout = 7000
                        sqlCmd.CommandType = CommandType.Text

                        Dim total As Integer = CInt(DataRepository.Provider.ExecuteScalar(sqlCmd))
                        If total > 65536 Then
                            Throw New Exception("Can not export to excel more than 65536 rows (xls limitation)")
                        Else
                            Dim tanggalGenerated As DateTime = Now

                            ' ''memakai services
                            'Dim proxy As CTRWebBLL.ICTRGeneratorServices
                            'Dim Successrun As Boolean = False
                            'proxy = CType(XmlRpcProxyGen.Create(GetType(CTRWebBLL.ICTRGeneratorServices)), CTRWebBLL.ICTRGeneratorServices)
                            'proxy.Url = System.Configuration.ConfigurationManager.AppSettings("ProxyURLPath")
                            'Successrun = proxy.CallProcessGenerateXlsAllWICReportFromWICView(tanggalGenerated.ToString("yyyy-MM-dd"), Commonly.SessionStaffName)
                            CTRNewFileGeneratorBll.CallProcessGenerateXlsAllWICReportFromWICView(tanggalGenerated.ToString("yyyy-MM-dd"), Commonly.SessionUserId, dirPath)

                            Using obj As TList(Of WICGeneratedFileXls) = DataRepository.WICGeneratedFileXlsProvider.GetPaged("DATEDIFF(DAY, TransactionDate,'" & tanggalGenerated.ToString("yyyy-MM-dd") & "')=0", "CreatedDate desc", 0, 1, 0)
                                If obj.Count > 0 Then
                                    Response.Redirect("DownloadAttachment.aspx?IsBinary=Y&ext=xls&id=" & obj(0).PK_WICGeneratedFileXls_Id & "&DataSource=WIC")
                                End If
                            End Using
                        End If
                    End If
                Else
                    'xml format
                    If rblData.SelectedIndex = 0 Then
                        'selected data
                        Dim strID As String = GetAllIDSelected(Me.SetnGetSelectedItem)

                        Using objWICList As TList(Of WIC) = DataRepository.WICProvider.GetPaged("PK_WIC_ID in (" & strID & ")", "", 0, Integer.MaxValue, 0)
                            If objWICList.Count > 0 Then
                                For i As Integer = 0 To objWICList.Count - 1
                                    Dim fileName As String = ""
                                    'Dim objBll As New CTRNewFileGeneratorBll
                                    Dim pathReport As String = dirPath & "FolderExport\"
                                    Dim existingFilename As String = ""
                                    If IsExistInWICGeneratedFileSequence(objWICList(i), existingFilename) Then
                                        fileName = existingFilename
                                    Else
                                        fileName = objWICList(i).CreatedDate.GetValueOrDefault().ToString("yyyyMMdd") & "-" & CTRNewFileGeneratorBll.GetNewSequenceNumber(objWICList(i), objWICList(i).CreatedDate) & ".xml"
                                    End If

                                    If File.Exists(pathReport & fileName) Then
                                        File.Delete(pathReport & fileName)
                                    End If

                                    If IsExistInWICGeneratedFileSequence(objWICList(i), existingFilename) Then
                                        Dim sequenceEdit As String = existingFilename.Substring(existingFilename.IndexOf("-") + 1)
                                        CTRNewFileGeneratorBll.GenerateGRISPXML(pathReport & fileName, objWICList(i), objWICList(i).CreatedDate.GetValueOrDefault(), sequenceEdit)
                                    Else
                                        CTRNewFileGeneratorBll.GenerateGRISPXML(pathReport & fileName, objWICList(i), objWICList(i).CreatedDate.GetValueOrDefault(), CStr(CTRNewFileGeneratorBll.GetNewSequenceNumber(objWICList(i), objWICList(i).CreatedDate)))
                                    End If

                                    'setelah berhasil digenerate, masukkan ke table WICGeneratedFile
                                    If IsExistInWICGeneratedFileSequence(objWICList(i), existingFilename) Then
                                        'update data
                                        Dim objGeneratedUpdate As WICGeneratedFile = DataRepository.WICGeneratedFileProvider.GetPaged(WICGeneratedFileColumn.FK_WIC_Id.ToString() & " = " & objWICList(i).PK_WIC_Id, "", 0, 1, 0)(0)
                                        If objGeneratedUpdate IsNot Nothing Then
                                            objGeneratedUpdate.CTRXmlFileName = fileName '& ".xml"
                                            objGeneratedUpdate.BinaryFileData = CTRNewFileGeneratorBll.GetBinaryFromFile(pathReport & fileName)
                                            objGeneratedUpdate.LastUpdateBy = Commonly.SessionUserId
                                            objGeneratedUpdate.LastUpdateDate = Now

                                            DataRepository.WICGeneratedFileProvider.Save(objGeneratedUpdate)
                                        End If
                                    Else
                                        Using objGenerated As New WICGeneratedFile
                                            With objGenerated
                                                .FK_WIC_Id = CInt(objWICList(i).PK_WIC_Id)
                                                If objWICList(i).TipeTerlapor.GetValueOrDefault() = 0 Then
                                                    .CTRType = "P" 'ambil N(Non Personal) atau P (Personal)
                                                Else
                                                    .CTRType = "N" 'ambil N(Non Personal) atau P (Personal)
                                                End If
                                                .CTRXmlFileName = fileName '& ".xml"
                                                .CTRWICSequenceNumber = CTRNewFileGeneratorBll.GetNewSequenceNumber(objWICList(i), objWICList(i).CreatedDate)
                                                .ReffNumber = ""
                                                .CustomerType = "W"
                                                If objWICList(i).TipeTerlapor.GetValueOrDefault() = 0 Then
                                                    .WICName = objWICList(i).INDV_NamaLengkap
                                                Else
                                                    .WICName = objWICList(i).CORP_Nama
                                                End If
                                                If objWICList(i).TipeTerlapor.GetValueOrDefault() = 0 Then
                                                    .NPWP = objWICList(i).INDV_NPWP
                                                Else
                                                    .NPWP = objWICList(i).CORP_NPWP
                                                End If

                                                .IsReportedToPPATK = False
                                                .TransactionDate = objWICList(i).CreatedDate
                                                .IsNewFile = True
                                                .BinaryFileData = CTRNewFileGeneratorBll.GetBinaryFromFile(pathReport & fileName)
                                                .CreatedDate = DateTime.Now
                                                .CreatedBy = Commonly.SessionUserId
                                            End With

                                            DataRepository.WICGeneratedFileProvider.Save(objGenerated)
                                        End Using
                                    End If

                                    'masukkan ke list
                                    lstFile.Add(pathReport & fileName)
                                Next

                                'setelah proses semuanya maka lakukan zipping akan bisa didownload
                                If lstFile.Count > 0 Then
                                    Dim fileNameWIC As String = dirPath & "FolderExport\" & "WIC-" & dateDownload.ToString("yyyyMMdd") & ".zip"
                                    Using zip As New ZipFile()
                                        zip.CompressionLevel = CompressionLevel.Level9
                                        zip.AddFiles(lstFile, "")

                                        zip.Save(fileNameWIC)
                                    End Using
                                End If
                            End If

                            'download attachment
                            Response.Redirect("DownloadAttachment.aspx?IsFile=Y&FileName=" & dirPath & "FolderExport\" & "WIC-" & dateDownload.ToString("yyyyMMdd") & ".zip")
                        End Using
                    Else
                        'all data
                        Using objWICList As TList(Of WIC) = DataRepository.WICProvider.GetAll()
                            If objWICList.Count > 0 Then
                                For i As Integer = 0 To objWICList.Count - 1
                                    Dim fileName As String = ""
                                    'Dim objBll As New CTRNewFileGeneratorBll
                                    Dim pathReport As String = dirPath & "FolderExport\"
                                    Dim existingFilename As String = ""
                                    If IsExistInWICGeneratedFileSequence(objWICList(i), existingFilename) Then
                                        fileName = existingFilename
                                    Else
                                        fileName = objWICList(i).CreatedDate.GetValueOrDefault().ToString("yyyyMMdd") & "-" & CTRNewFileGeneratorBll.GetNewSequenceNumber(objWICList(i), objWICList(i).CreatedDate) & ".xml"
                                    End If

                                    'fileName = objWICList(i).CreatedDate.GetValueOrDefault().ToString("yyyyMMdd") & "-" & (objBll.GetNewSequenceNumber(i)).ToString & ".xml"
                                    If File.Exists(pathReport & fileName) Then
                                        File.Delete(pathReport & fileName)
                                    End If
                                    CTRNewFileGeneratorBll.GenerateGRISPXML(pathReport & fileName, objWICList(i), objWICList(i).CreatedDate.GetValueOrDefault(), CStr((CTRNewFileGeneratorBll.GetNewSequenceNumber(objWICList(i), objWICList(i).CreatedDate)).ToString))

                                    'setelah berhasil digenerate, masukkan ke table WICGeneratedFile
                                    If IsExistInWICGeneratedFileSequence(objWICList(i), existingFilename) Then
                                        'update data
                                        Dim objGeneratedUpdate As WICGeneratedFile = DataRepository.WICGeneratedFileProvider.GetPaged(WICGeneratedFileColumn.FK_WIC_Id.ToString() & " = " & objWICList(i).PK_WIC_Id, "", 0, 1, 0)(0)
                                        If objGeneratedUpdate IsNot Nothing Then
                                            objGeneratedUpdate.CTRXmlFileName = fileName '& ".xml"
                                            objGeneratedUpdate.BinaryFileData = CTRNewFileGeneratorBll.GetBinaryFromFile(pathReport & fileName)
                                            objGeneratedUpdate.LastUpdateBy = Commonly.SessionUserId
                                            objGeneratedUpdate.LastUpdateDate = Now

                                            DataRepository.WICGeneratedFileProvider.Save(objGeneratedUpdate)
                                        End If
                                    Else
                                        Using objGenerated As New WICGeneratedFile
                                            With objGenerated
                                                .FK_WIC_Id = CInt(objWICList(i).PK_WIC_Id)
                                                If objWICList(i).TipeTerlapor.GetValueOrDefault() = 0 Then
                                                    .CTRType = "P" 'ambil N(Non Personal) atau P (Personal)
                                                Else
                                                    .CTRType = "N" 'ambil N(Non Personal) atau P (Personal)
                                                End If
                                                .CTRXmlFileName = fileName
                                                .CTRWICSequenceNumber = CTRNewFileGeneratorBll.GetNewSequenceNumber(i)
                                                .ReffNumber = ""
                                                .CustomerType = "W"
                                                If objWICList(i).TipeTerlapor.GetValueOrDefault() = 0 Then
                                                    .WICName = objWICList(i).INDV_NamaLengkap
                                                Else
                                                    .WICName = objWICList(i).CORP_Nama
                                                End If
                                                If objWICList(i).TipeTerlapor.GetValueOrDefault() = 0 Then
                                                    .NPWP = objWICList(i).INDV_NPWP
                                                Else
                                                    .NPWP = objWICList(i).CORP_NPWP
                                                End If

                                                .IsReportedToPPATK = False
                                                .TransactionDate = objWICList(i).CreatedDate
                                                .IsNewFile = True
                                                .BinaryFileData = CTRNewFileGeneratorBll.GetBinaryFromFile(pathReport & fileName)
                                                .CreatedDate = DateTime.Now
                                                .CreatedBy = Commonly.SessionUserId
                                            End With

                                            DataRepository.WICGeneratedFileProvider.Save(objGenerated)
                                        End Using
                                    End If

                                    'masukkan ke list
                                    lstFile.Add(pathReport & fileName)
                                Next

                                'setelah proses semuanya maka lakukan zipping akan bisa didownload
                                If lstFile.Count > 0 Then
                                    Dim fileNameWIC As String = dirPath & "FolderExport\" & "WIC-" & dateDownload.ToString("yyyyMMdd") & ".zip"
                                    Using zip As New ZipFile()
                                        zip.CompressionLevel = CompressionLevel.Level9
                                        zip.AddFiles(lstFile, "")
                                        zip.Save(fileNameWIC)
                                    End Using
                                End If
                            End If

                            'download attachment
                            Response.Redirect("DownloadAttachment.aspx?IsFile=Y&FileName=" & dirPath & "FolderExport\" & "WIC-" & dateDownload.ToString("yyyyMMdd") & ".zip")
                        End Using
                    End If

                End If
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

#End Region

End Class