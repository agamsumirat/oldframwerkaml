Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper


Partial Class UserDelete
    Inherits Parent

    ''' <summary>
    ''' get user id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetPkUserID() As String
        Get
            Return Me.Request.Params("PkUserID")
        End Get
    End Property

    ''' <summary>
    ''' get id for focues
    ''' </summary>
    ''' <value></value>
    ''' <returns>element control id</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property GetFocusID() As String
        Get
            Return Me.TextUserid.ClientID
        End Get
    End Property

    ''' <summary>
    ''' cancel handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "UserView.aspx"

        Me.Response.Redirect("UserView.aspx", False)
    End Sub

#Region " Insert User dan Audit trail"
    Private Sub DeleteUserBySU()
        Try
            Using AccessUser As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                AccessUser.DeleteUser(Me.GetPkUserID)
            End Using
        Catch
            Throw
        End Try
    End Sub

    Private Sub InsertAuditTrail()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            'Using TransScope As New Transactions.TransactionScope
            Using AccessUser As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessUser, Data.IsolationLevel.ReadUncommitted)
                Using dt As Data.DataTable = AccessUser.GetData()
                    Sahassa.AML.AuditTrailAlert.AuditTrailChecking(14)
                    Dim RowData() As AMLDAL.AMLDataSet.UserRow = dt.Select("pkUserId='" & CInt(Me.GetPkUserID) & "'")
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserID", "Delete", "", RowData(0).UserID, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserName", "Delete", "", RowData(0).UserName, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserEmailAddress", "Delete", "", RowData(0).UserEmailAddress, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserMobilePhone", "Delete", "", RowData(0).UserMobilePhone, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserPassword", "Delete", "", RowData(0).UserPassword, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserPasswordSalt", "Delete", "", RowData(0).UserPasswordSalt, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserLastChangedPassword", "Delete", "", RowData(0).UserLastChangedPassword.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserIPAddress", "Delete", "", RowData(0).UserIPAddress, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserInUsed", "Delete", "", RowData(0).UserInUsed, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserCreatedDate", "Delete", RowData(0).UserCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), RowData(0).UserCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserIsDisabled", "Delete", "", RowData(0).UserIsDisabled, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserGroupId", "Delete", "", RowData(0).UserGroupId, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserLastLogin", "Delete", "", RowData(0).UserLastLogin.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "isCreatedBySU", "Delete", "", RowData(0).isCreatedBySU, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "TellerID", "Delete", "", RowData(0).TellerID, "Accepted")
                    End Using
                    oSQLTrans.Commit()
                End Using
            End Using
            'End Using
        Catch
            Throw
        End Try
    End Sub
#End Region

    ''' <summary>
    ''' update handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Page.Validate()

        If Page.IsValid Then
            Try
                Dim UserID As String = Me.TextUserid.Text

                'Periksa apakah UserID tsb sdh ada dalam tabel User atau belum
                Using AccessUser As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                    Dim counter As Int32 = AccessUser.CountMatchingUser(UserID)

                    'Counter > 0 berarti UserID tersebut masih ada dalam tabel User dan bisa didelete
                    If counter > 0 Then
                        'Periksa apakah UserID tsb dalam status pending approval atau tidak
                        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.User_ApprovalTableAdapter
                            counter = AccessPending.CountUserApprovalByUserID(UserID)

                            'Counter = 0 berarti UserID tersebut tidak dalam status pending approval
                            If counter = 0 Then
                                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                                    Me.InsertAuditTrail()
                                    Me.DeleteUserBySU()
                                    Me.LblSuccess.Text = "Success to Delete User."
                                    Me.LblSuccess.Visible = True
                                Else
                                    'Buat variabel untuk menampung nilai-nilai baru
                                    Dim UserName As String = Me.TextUserName.Text
                                    Dim Salt As String = ViewState("UserPasswordSalt_Old")
                                    Dim UserPassword As String = ViewState("UserPassword_Old")
                                    Dim EmailAddr As String = Me.TextboxEmailAddr.Text
                                    Dim Hp As String = Me.TextboxMobilePhone.Text
                                    Dim Group As Int32 = ViewState("UserGroupId_Old")
                                    Dim TellerID As String = Me.TextboxTellerID.Text

                                    'Buat variabel untuk menampung nilai-nilai lama
                                    Dim PkUserId_Old As Int64 = ViewState("PkUserId_Old")
                                    Dim UserID_Old As String = ViewState("UserID_Old")
                                    Dim UserName_Old As String = ViewState("UserName_Old")
                                    Dim Salt_Old As String = ViewState("UserPasswordSalt_Old")
                                    Dim UserPassword_Old As String = ViewState("UserPassword_Old")
                                    Dim EmailAddr_Old As String = ViewState("UserEmailAddress_Old")
                                    Dim Hp_Old As String = ViewState("UserMobilePhone_Old")
                                    Dim Group_Old As Int32 = ViewState("UserGroupId_Old")
                                    Dim UserLastChangedPassword_Old As DateTime = ViewState("UserLastChangedPassword_Old")
                                    Dim UserIPAddress_Old As String = ViewState("UserIPAddress_Old")
                                    Dim UserInUsed_Old As Boolean = ViewState("UserInUsed_Old")
                                    Dim UserIsDisabled_Old As Boolean = ViewState("UserIsDisabled_Old")
                                    Dim UserLastLogin_Old As DateTime = ViewState("UserLastLogin_Old")
                                    Dim UserWorkingUnitMappingStatus_Old As Boolean = ViewState("UserWorkingUnitMappingStatus_Old")
                                    Dim UserCreatedDate_Old As DateTime = ViewState("UserCreatedDate_Old")
                                    Dim isCreatedBySU_Old As Boolean = ViewState("isCreatedBySU_Old")
                                    Dim UserTellerID_Old As String = ViewState("UserTellerID_Old")

                                    'variabel untuk menampung nilai identity dari tabel NewsPendingApprovalID
                                    Dim UserPendingApprovalID As Int64

                                    'Using TransScope As New Transactions.TransactionScope
                                    Using AccessUserPending As New AMLDAL.AMLDataSetTableAdapters.User_PendingApprovalTableAdapter
                                        'Tambahkan ke dalam tabel User_PendingApproval dengan ModeID = 3 (Delete) 
                                        UserPendingApprovalID = AccessUserPending.InsertUser_PendingApproval(Sahassa.AML.Commonly.SessionUserId, Now, "User Delete", 3)
                                        'Tambahkan ke dalam tabel User_Approval dengan ModeID = 3 (Delete) 
                                        AccessPending.Insert(UserPendingApprovalID, 3, UserID, EmailAddr, Hp, UserPassword, Salt, Now, "", UserInUsed_Old, UserCreatedDate_Old, UserIsDisabled_Old, Group, UserLastLogin_Old, UserID_Old, EmailAddr_Old, Hp_Old, UserPassword_Old, Salt_Old, UserLastChangedPassword_Old, UserIPAddress_Old, UserInUsed_Old, UserCreatedDate_Old, UserIsDisabled_Old, Group_Old, UserLastLogin_Old, UserWorkingUnitMappingStatus_Old, UserWorkingUnitMappingStatus_Old, isCreatedBySU_Old, isCreatedBySU_Old, PkUserId_Old, UserName, PkUserId_Old, UserName_Old, TellerID, UserTellerID_Old)

                                        'TransScope.Complete()
                                    End Using
                                    'End Using

                                    Dim MessagePendingID As Integer = 8303 'MessagePendingID 8303 = User Delete 

                                    Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & UserID

                                    Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & UserID, False)
                                End If
                            Else
                                Throw New Exception("Cannot delete the following User: '" & UserID & "' because it is currently waiting for approval.")
                            End If
                        End Using
                    Else  'Counter = 0 berarti UserID tersebut tidak ada dalam tabel User
                        Throw New Exception("Cannot delete the following User: '" & UserID & "' because that User Name does not exist in the database anymore.")
                    End If
                End Using
            Catch ex As Exception
                Me.cvalPageError.IsValid = False
                Me.cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            End Try

        End If
    End Sub

    ''' <summary>
    ''' filledit data
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillEditData()
        Using AccessUser As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
            Using TableUser As AMLDAL.AMLDataSet.UserDataTable = AccessUser.GetDataByUserID(Me.GetPkUserID)
                If TableUser.Rows.Count > 0 Then
                    Dim TableRowUser As AMLDAL.AMLDataSet.UserRow = TableUser.Rows(0)

                    ViewState("PkUserId_Old") = TableRowUser.pkUserID

                    ViewState("UserID_Old") = TableRowUser.UserID
                    Me.TextUserid.Text = ViewState("UserID_Old")

                    ViewState("UserName_Old") = TableRowUser.UserName
                    Me.TextUserName.Text = ViewState("UserName_Old")

                    ViewState("UserEmailAddress_Old") = TableRowUser.UserEmailAddress
                    Me.TextboxEmailAddr.Text = ViewState("UserEmailAddress_Old")

                    ViewState("UserMobilePhone_Old") = TableRowUser.UserMobilePhone
                    Me.TextboxMobilePhone.Text = ViewState("UserMobilePhone_Old")

                    ViewState("UserPassword_Old") = TableRowUser.UserPassword
                    ViewState("UserPasswordSalt_Old") = TableRowUser.UserPasswordSalt
                    ViewState("UserLastChangedPassword_Old") = TableRowUser.UserLastChangedPassword
                    ViewState("UserIPAddress_Old") = TableRowUser.UserIPAddress
                    ViewState("UserInUsed_Old") = CType(TableRowUser.UserInUsed, Boolean)
                    ViewState("UserIsDisabled_Old") = CType(TableRowUser.UserIsDisabled, Boolean)

                    ViewState("UserGroupId_Old") = TableRowUser.UserGroupId
                    Using AccessGroup As New AMLDAL.AMLDataSetTableAdapters.GroupTableAdapter
                        Me.LabelGroupName.Text = AccessGroup.GetGroupNameByGroupID(ViewState("UserGroupId_Old"))
                    End Using

                    ViewState("UserLastLogin_Old") = TableRowUser.UserLastLogin
                    'ViewState("UserWorkingUnitMappingStatus_Old") = CType(TableRowUser.UserWorkingUnitMappingStatus, Boolean)
                    ViewState("UserCreatedDate_Old") = TableRowUser.UserCreatedDate
                    ViewState("isCreatedBySU_Old") = TableRowUser.isCreatedBySU

                    ViewState("UserTellerID_Old") = TableRowUser.TellerID
                    Me.TextboxTellerID.Text = ViewState("UserTellerID_Old")
                End If
            End Using
        End Using
    End Sub

    ''' <summary>
    ''' page load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.FillEditData()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class