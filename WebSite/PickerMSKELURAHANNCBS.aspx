<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PickerMSKELURAHANNCBS.aspx.vb" Inherits="PickerMSKELURAHANNCBS"
    Culture="auto" UICulture="auto" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>:: Cash Transaction Report System ::</title>
    <link href="Theme/aml.css" type="text/css" rel="stylesheet"/>
    <base target="_self">
    
    <script language="javascript" type="text/javascript">
        function SetDataForSent(ButtonId) {
            window.opener.SetDataRetrieve(ButtonId);
            window.close();
        }

    </script>

</head>
<body onblur="this.focus">
    <form id="form1" runat="server">
        <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
            style="border-top-style: none; border-right-style: none; border-left-style: none;
            border-bottom-style: none" width="100%">
            <tr>
                <td bgcolor="#ffffff" colspan="3">
                <ajax:AjaxPanel ID="AjaxPanel1" runat="server">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="validation"
                        HeaderText="There were uncompleteness on the page:" Width="85%" />
                        </ajax:AjaxPanel>
                </td>
            </tr>
            <tr>
                <td colspan="3" bgcolor="#ffffff" style="height: 5px">
                </td>
            </tr>
            <tr>
                <td colspan="3" bgcolor="#ffffff">
                    &nbsp;&nbsp;<img height="15" src="images/dot_title.gif" width="15">&nbsp;&nbsp;<asp:Label
                        ID="lblTitle" runat="server" CssClass="maintitle" Text="MASTER KELURAHAN - Picker"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="3" bgcolor="#ffffff" style="height: 5px">
                </td>
            </tr>
        </table>
        <table align="center" bordercolor="#dddddd" cellspacing="0" cellpadding="0" width="97%"
            bgcolor="#ffffff" border="1">
            <tr>
                <td background="Images/search-bar-background.gif" valign="middle" width="50%" align="left"
                    style="height: 6px">
                    <table cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td class="formtext" style="height: 14px">
                                &nbsp;<asp:Label ID="Label9" runat="server" Text="Searching Criteria" Font-Bold="True"></asp:Label>
                                &nbsp;</td>
                            <td style="width: 13px; height: 14px;">
                                &nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="SearchingBox">
                <td valign="top" bgcolor="#ffffff">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td valign="middle" width="100%" nowrap>
                                <ajax:AjaxPanel ID="AjaxPanel15" runat="server">
                                    <table style="width: 100%; height: 100%">
                                       <tr>
                                            <td nowrap bgcolor="#FFF7E6" height="20" width="20%" valign="middle">
                                                <asp:Label ID="lblIDKelurahanNCBS" runat="server" Text="ID Kelurahan Bank"></asp:Label></td>
                                            <td nowrap height="20" width="75%" valign="middle">
                                                <asp:TextBox ID="txtIDKelurahanNCBS" runat="server" CssClass="textBox" MaxLength="20"
                                                    Width="200px"></asp:TextBox></td>
                                        </tr>
<tr>
                                            <td nowrap bgcolor="#FFF7E6" height="20" width="20%" valign="middle">
                                                <asp:Label ID="lblNamaKelurahanNCBS" runat="server" Text="Nama Kelurahan Bank"></asp:Label></td>
                                            <td nowrap height="20" width="75%" valign="middle">
                                                <asp:TextBox ID="txtNamaKelurahanNCBS" runat="server" CssClass="textBox" MaxLength="20"
                                                    Width="200px"></asp:TextBox></td>
                                        </tr>


                                     <tr>
                                            <td colspan="2" height="10">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:ImageButton ID="ImgSearch" TabIndex="3" runat="server" SkinID="SearchButton"
                                                    ImageUrl="~/Images/button/search.gif"></asp:ImageButton>&nbsp;<asp:ImageButton ID="ImageClearSearch"
                                                        runat="server" ImageUrl="~/Images/button/clearsearch.gif" /></td>
                                        </tr>
                                        <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></table>
                                </ajax:AjaxPanel>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td style="height: 5px">
                </td>
            </tr>
        </table>
        <table align="center" cellspacing="0" cellpadding="0" width="97%" bgcolor="#ffffff"
            border="0">
            <tr id="TR1">
                <td valign="top" width="98%" bgcolor="#ffffff">
                    <table bordercolor="#ffffff" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td bgcolor="#ffffff">
                                <ajax:AjaxPanel ID="AjaxPanel4" runat="server">
                                    <asp:DataGrid ID="GridDataView" runat="server" AutoGenerateColumns="False" Font-Size="XX-Small"
                                        CellSpacing="1" CellPadding="4" SkinID="gridview" AllowPaging="True" Width="100%"
                                        GridLines="None" AllowSorting="True" ForeColor="#333333" Font-Bold="False" Font-Italic="False"
                                        Font-Overline="False" Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left">
                                        <Columns>
                                            <asp:TemplateColumn>
                                                <ItemTemplate>
                                                    <asp:CheckBox runat="server" ID="rbSelected" AutoPostBack="True" 
                                                        oncheckedchanged="rbSelected_CheckedChanged"/>
                                                </ItemTemplate>
                                                <HeaderStyle Width="2%" />
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="IDKELURAHANNCBS" Visible="False">
                                                <HeaderStyle Width="0%" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn HeaderText="No.">
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" Wrap="False" HorizontalAlign="Center" VerticalAlign="Top" />
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" Wrap="False" HorizontalAlign="Center" Width="3%" ForeColor="White"
                                                    VerticalAlign="Middle" />
                                            </asp:BoundColumn>
                                           <asp:BoundColumn DataField="IDKelurahanNCBS" HeaderText="ID Kelurahan Bank" SortExpression="IDKelurahanNCBS desc"
DataFormatString="{0:##,0}"></asp:BoundColumn>
<asp:BoundColumn DataField="NamaKelurahanNCBS" HeaderText="Nama Kelurahan Bank" SortExpression="NamaKelurahanNCBS asc"></asp:BoundColumn>

                                       
                                        </Columns>
                                          <PagerStyle Visible="False" HorizontalAlign="Center" ForeColor="White" BackColor="#666666">
                                        </PagerStyle>
                                        <EditItemStyle BackColor="#7C6F57" />
                                    </asp:DataGrid>
                                </ajax:AjaxPanel>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#ffffff">
                                <table id="Table3" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                    bgcolor="#ffffff" border="2">
                                    <tr class="regtext" align="center" bgcolor="#dddddd">
                                        <td valign="top" align="left" width="50%" bgcolor="#ffffff">
                                        <ajax:AjaxPanel ID="AjaxPanel2" runat="server">
                                          <asp:CheckBox ID="ChkAll" runat="server" Text="Check Current Page" 
                                                AutoPostBack="True" />
                                          </ajax:AjaxPanel> </br>
                                            Page&nbsp;<ajax:AjaxPanel ID="AjaxPanel6" runat="server"><asp:Label ID="PageCurrentPage"
                                                runat="server" CssClass="regtext">0</asp:Label>&nbsp;of&nbsp;
                                                <asp:Label ID="PageTotalPages" runat="server" CssClass="regtext">0</asp:Label></ajax:AjaxPanel></td>
                                        <td valign="top" align="right" width="50%" bgcolor="#ffffff">
                                            Total Records&nbsp;
                                            <ajax:AjaxPanel ID="AjaxPanel7" runat="server">
                                                <asp:Label ID="PageTotalRows" runat="server">0</asp:Label></ajax:AjaxPanel></td>
                                    </tr>
                                </table>
                                <table id="Table4" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                    bgcolor="#ffffff" border="2">
                                    <tr bgcolor="#ffffff">
                                        <td class="regtext" valign="middle" align="left" colspan="11" height="7">
                                            <hr color="#f40101" noshade size="1">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="regtext" valign="middle" align="left" width="63" bgcolor="#ffffff">
                                            Go to page</td>
                                        <td class="regtext" valign="middle" align="left" width="5" bgcolor="#ffffff">
                                            <font face="Verdana, Arial, Helvetica, sans-serif" size="1">
                                                <ajax:AjaxPanel ID="AjaxPanel8" runat="server">
                                                    <asp:TextBox ID="TextGoToPage" runat="server" Width="38px" CssClass="searcheditbox"></asp:TextBox></ajax:AjaxPanel></font></td>
                                        <td class="regtext" valign="middle" align="left" bgcolor="#ffffff">
                                            <ajax:AjaxPanel ID="AjaxPanel9" runat="server">
                                                <asp:ImageButton ID="ImageButtonGo" runat="server" ImageUrl="~/Images/button/go.gif">
                                                </asp:ImageButton></ajax:AjaxPanel></td>
                                        <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                                            <img height="5" src="images/first.gif" width="6"></td>
                                        <td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff">
                                            <ajax:AjaxPanel ID="AjaxPanel10" runat="server">
                                                <asp:LinkButton ID="LinkButtonFirst" runat="server" CssClass="regtext" CommandName="First"
                                                    OnCommand="PageNavigate">First</asp:LinkButton></ajax:AjaxPanel></td>
                                        <td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff">
                                            <img height="5" src="images/prev.gif" width="6"></td>
                                        <td class="regtext" valign="middle" align="right" width="14" bgcolor="#ffffff">
                                            <ajax:AjaxPanel ID="AjaxPanel11" runat="server">
                                                <asp:LinkButton ID="LinkButtonPrevious" runat="server" CssClass="regtext" CommandName="Prev"
                                                    OnCommand="PageNavigate">Previous</asp:LinkButton></ajax:AjaxPanel></td>
                                        <td class="regtext" valign="middle" align="right" width="60" bgcolor="#ffffff">
                                            <ajax:AjaxPanel ID="AjaxPanel12" runat="server">
                                                <a class="pageNav" href="#">
                                                    <asp:LinkButton ID="LinkButtonNext" runat="server" CssClass="regtext" CommandName="Next"
                                                        OnCommand="PageNavigate">Next</asp:LinkButton></a></ajax:AjaxPanel></td>
                                        <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                                            <img height="5" src="images/next.gif" width="6"></td>
                                        <td class="regtext" valign="middle" align="left" width="25" bgcolor="#ffffff">
                                            <ajax:AjaxPanel ID="AjaxPanel13" runat="server">
                                                <asp:LinkButton ID="LinkButtonLast" runat="server" CssClass="regtext" CommandName="Last"
                                                    OnCommand="PageNavigate">Last</asp:LinkButton></ajax:AjaxPanel></td>
                                        <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                                            <img height="5" src="images/last.gif" width="6"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="LemonChiffon"
                        border="2">
                        <tr>
                            <td style="width: 15px">
                                <img src="images/arrow.gif" width="15" height="15" /></td>
                            <td>
                                &nbsp;<asp:ImageButton ID="ImgSelected" TabIndex="3" runat="server"
                                    ImageUrl="~/Images/button/select.gif" CausesValidation="False"></asp:ImageButton>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>


