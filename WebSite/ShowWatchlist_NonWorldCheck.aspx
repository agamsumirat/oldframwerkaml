﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ShowWatchlist_NonWorldCheck.aspx.vb" Inherits="ShowWatchlist_NonWorldCheck" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Theme/aml.css" rel="stylesheet" />
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 30%;
            height: 17px;
        }
        .auto-style3 {
            width: 10px;
            height: 17px;
        }
        .auto-style4 {
            width: 70%;
            height: 17px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <%--<strong>Data</strong> --%>
        <table class="auto-style1">
            <tr class="formText">
                <td style="width: 30%">NAME</td>
                <td style="width: 10px">:</td>
                <td style="width: 70%">
                    <asp:Label ID="lblName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">DOB</td>
                <td style="width: 10px">:</td>
                <td style="width: 70%">
                    <asp:Label ID="lblDOB" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">YOB</td>
                <td style="width: 10px">:</td>
                <td style="width: 70%">
                    <asp:Label ID="lblYOB" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">NATIONALITY</td>
                <td style="width: 10px">:</td>
                <td style="width: 70%">
                    <asp:Label ID="lblNationality" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">BIRTH PLACE</td>
                <td style="width: 10px">:</td>
                <td style="width: 70%">
                    <asp:Label ID="lblBirthPlace" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">LIST TYPE</td>
                <td style="width: 10px">:</td>
                <td style="width: 70%">
                    <asp:Label ID="lblListType" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">CATEGORY TYPE</td>
                <td style="width: 10px">:</td>
                <td style="width: 70%">
                    <asp:Label ID="lblCategoryType" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td class="auto-style3">&nbsp;</td>
                <td class="auto-style4">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 30%">CUSTOM REMARK 1</td>
                <td style="width: 10px">:</td>
                <td style="width: 70%">
                    <asp:Label ID="lblCustomRemark_1" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">CUSTOM REMARK 2</td>
                <td style="width: 10px">:</td>
                <td style="width: 70%">
                    <asp:Label ID="lblCustomRemark_2" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">CUSTOM REMARK 3</td>
                <td style="width: 10px">:</td>
                <td style="width: 70%">
                    <asp:Label ID="lblCustomRemark_3" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">CUSTOM REMARK 4</td>
                <td style="width: 10px">:</td>
                <td style="width: 70%">
                    <asp:Label ID="lblCustomRemark_4" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">CUSTOM REMARK 5</td>
                <td style="width: 10px">:</td>
                <td style="width: 70%">
                    <asp:Label ID="lblCustomRemark_5" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">&nbsp;</td>
                <td style="width: 10px">&nbsp;</td>
                <td style="width: 70%">&nbsp;</td>
            </tr>
            <tr>
                <td style="width: 30%"><asp:imagebutton id="ImageSearch" runat="server" CausesValidation="True" ImageUrl="~/Images/button/ok.gif" OnClientClick="window.close();"></asp:imagebutton></td>
                <td style="width: 10px">&nbsp;</td>
                <td style="width: 70%">&nbsp;</td>
            </tr>
            </table>
   
    </div>
    </form>
</body>
</html>
