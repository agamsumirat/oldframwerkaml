#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports System.Collections.Generic
#End Region

Partial Class MsProvince_edit
    Inherits Parent

#Region "Function"

    Private Sub LoadData()
        Dim ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetPaged(MsProvinceColumn.IdProvince.ToString & "=" & parID, "", 0, 1, Nothing)(0)
        If ObjMsProvince Is Nothing Then Throw New Exception("Data Not Found")
        With ObjMsProvince
            SafeDefaultValue = "-"
            txtIdProvince.Text = Safe(.IdProvince)
            txtCode.Text = Safe(.Code)
            txtNama.Text = Safe(.Nama)
            txtDescription.Text = Safe(.Description)

            chkActivation.Checked = SafeBoolean(.activation)
            Dim L_objMappingMsProvinceNCBSPPATK As TList(Of MappingMsProvinceNCBSPPATK)
            L_objMappingMsProvinceNCBSPPATK = DataRepository.MappingMsProvinceNCBSPPATKProvider.GetPaged(MappingMsProvinceNCBSPPATKColumn.Pk_MsProvince_Id.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)


            Listmaping.AddRange(L_objMappingMsProvinceNCBSPPATK)

        End With
    End Sub

    Private Sub BindListMapping()
        LBMapping.DataSource = ListMappingDisplay
        LBMapping.DataBind()
    End Sub

    ''' <summary>
    ''' Validasi men-handle seluruh syarat2 data valid
    ''' data yang lolos dari validasi dipastikan lolos ke database 
    ''' karena menggunakan FillorNothing
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ValidasiControl()
        '======================== Validasi textbox :  ID canot Null ====================================================
        If ObjectAntiNull(txtIdProvince.Text) = False Then Throw New Exception("ID  must be filled  ")
        If Not IsNumeric(Me.txtIdProvince.Text) Then
            Throw New Exception("ID must be in number format")
        End If
        '======================== Validasi textbox :  Code canot Null ====================================================
        If ObjectAntiNull(txtCode.Text) = False Then Throw New Exception("Code  must be filled  ")
        '======================== Validasi textbox :  Nama canot Null ====================================================
        If ObjectAntiNull(txtNama.Text) = False Then Throw New Exception("Nama  must be filled  ")
        '======================== Validasi textbox :  Description canot Null ====================================================
        If ObjectAntiNull(txtDescription.Text) = False Then Throw New Exception("Description  must be filled  ")

        If DataRepository.MsProvinceProvider.GetPaged("IDProvince = '" & txtIdProvince.Text & "' AND IDProvince <> '" & parID & "'", "", 0, Integer.MaxValue, 0).Count > 0 Then
            Throw New Exception("ID already exist in the database")
        End If

        If DataRepository.MsProvince_ApprovalDetailProvider.GetPaged(MsProvince_ApprovalDetailColumn.IdProvince.ToString & "='" & txtIdProvince.Text & "' AND IDProvince <> '" & parID & "'", "", 0, 1, Nothing).Count > 0 Then
            Throw New Exception("Data exist in List Waiting Approval")
        End If
        'If LBMapping.Items.Count = 0 Then Throw New Exception("data tidak punya list Mapping")
    End Sub

#End Region

#Region "events..."

    Protected Sub imgBrowse_click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBrowse.Click
        Try
            If Not Get_DataFromPicker Is Nothing Then
                Listmaping.AddRange(Get_DataFromPicker)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgDeleteItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgDeleteItem.Click
        Try
            If LBMapping.SelectedIndex < 0 Then
                Throw New Exception("No data Selected")
            End If
            Listmaping.RemoveAt(LBMapping.SelectedIndex)

        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                'AuditTrailBLL.InsertAuditTrailUserAccess(Sahassa.CommonCTRWeb.Commonly.SessionCurrentPage)
                Listmaping = New TList(Of MappingMsProvinceNCBSPPATK)
                LoadData()
            Catch ex As Exception
                LogError(ex)
                CvalPageErr.IsValid = False
                CvalPageErr.ErrorMessage = ex.Message
            End Try
        End If

    End Sub

    Protected Sub ImgBtnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnSave.Click
        Try
            Page.Validate("handle")
            If Page.IsValid Then
                ValidasiControl()
                ' =========== Insert Header Approval
                Dim KeyHeaderApproval As Integer
                Using ObjMsProvince_Approval As New MsProvince_Approval
                    With ObjMsProvince_Approval
                        FillOrNothing(.Fk_MsMode_Id, 2, True, oInt)
                        FillOrNothing(.Fk_MsUser_Id, SessionPkUserId)
                        FillOrNothing(.RequestedDate, Date.Now)
                        .IsUpload = False
                    End With
                    DataRepository.MsProvince_ApprovalProvider.Save(ObjMsProvince_Approval)
                    KeyHeaderApproval = ObjMsProvince_Approval.Pk_MsProvince_Approval_Id
                End Using
                '============ Insert Detail Approval
                Using objMsProvince_ApprovalDetail As New MsProvince_ApprovalDetail()
                    With objMsProvince_ApprovalDetail
                        Dim ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(parID)
                        FillOrNothing(.IdProvince, ObjMsProvince.IdProvince)
                        FillOrNothing(.Nama, ObjMsProvince.Nama)
                        FillOrNothing(.Activation, ObjMsProvince.activation)
                        FillOrNothing(.CreatedDate, ObjMsProvince.CreatedDate)
                        FillOrNothing(.CreatedBy, ObjMsProvince.CreatedBy)
                        FillOrNothing(.Fk_MsProvince_Approval_Id, KeyHeaderApproval)

                        '==== Edit data =============
                        FillOrNothing(.IdProvince, txtIdProvince.Text, True, oInt)
                        FillOrNothing(.Code, txtCode.Text, True, Ovarchar)
                        FillOrNothing(.Nama, txtNama.Text, True, Ovarchar)
                        FillOrNothing(.Description, txtDescription.Text, True, Ovarchar)

                        FillOrNothing(.Activation, chkActivation.Checked, True, oBit)
                        FillOrNothing(.LastUpdatedBy, SessionUserId)
                        FillOrNothing(.LastUpdatedDate, Date.Now)

                    End With
                    DataRepository.MsProvince_ApprovalDetailProvider.Save(objMsProvince_ApprovalDetail)
                    Dim keyHeaderApprovalDetail As Integer = objMsProvince_ApprovalDetail.Pk_MsProvince_Approval_Detail_Id
                    '========= Insert mapping item 
                    Dim LobjMappingMsProvinceNCBSPPATK_Approval_Detail As New TList(Of MappingMsProvinceNCBSPPATK_Approval_Detail)
                    For Each objMappingMsProvinceNCBSPPATK As MappingMsProvinceNCBSPPATK In Listmaping
                        Dim Mapping As New MappingMsProvinceNCBSPPATK_Approval_Detail
                        With Mapping
                            FillOrNothing(.IDProvince, objMsProvince_ApprovalDetail.IdProvince)
                            FillOrNothing(.IDProvinceNCBS, objMappingMsProvinceNCBSPPATK.Pk_MsProvinceNCBS_Id)
                            FillOrNothing(.PK_MsProvince_Approval_Id, KeyHeaderApproval)
                            FillOrNothing(.nama, objMappingMsProvinceNCBSPPATK.Nama)
                            FillOrNothing(.PK_MappingMsProvinceNCBSPPATK_approval_detail_Id, keyHeaderApprovalDetail)
                            LobjMappingMsProvinceNCBSPPATK_Approval_Detail.Add(Mapping)
                        End With
                    Next
                    DataRepository.MappingMsProvinceNCBSPPATK_Approval_DetailProvider.Save(LobjMappingMsProvinceNCBSPPATK_Approval_Detail)
                    'session Maping item Clear
                    Listmaping.Clear()
                    ImgBtnSave.Visible = False
                    ImgBackAdd.Visible = False
                    LblConfirmation.Text = "Data has been added and waiting for approval"
                    MtvMsUser.ActiveViewIndex = 1
                End Using
            End If
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub



    Protected Sub ImgBackAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBackAdd.Click
        Response.Redirect("MsProvince_View.aspx")
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            BindListMapping()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBack.Click
        Response.Redirect("MsProvince_View.aspx")
    End Sub
#End Region

#Region "Property..."
    Public ReadOnly Property parID() As String
        Get
            Return Request.Item("ID")
        End Get
    End Property
    ''' <summary>
    ''' Mengambil Data dari Picker
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Get_DataFromPicker() As TList(Of MappingMsProvinceNCBSPPATK)
        Get
            Dim Temp As New TList(Of MappingMsProvinceNCBSPPATK)
            Dim L_MsProvinceNCBS As TList(Of MsProvinceNCBS)
            Try
                If Session("PickerMsProvinceNCBS.Data") Is Nothing Then
                    Throw New Exception
                End If
                L_MsProvinceNCBS = Session("PickerMsProvinceNCBS.Data")
                For Each i As MsProvinceNCBS In L_MsProvinceNCBS
                    Dim Tempmapping As New MappingMsProvinceNCBSPPATK
                    With Tempmapping
                        .Pk_MsProvinceNCBS_Id = i.IdProvinceNCBS
                        .Nama = i.Nama
                    End With
                    Temp.Add(Tempmapping)
                Next
            Catch ex As Exception
                L_MsProvinceNCBS = Nothing
            End Try
            Return Temp
        End Get
    End Property
    ''' <summary>
    ''' Menyimpan Item untuk mapping sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Listmaping() As TList(Of MappingMsProvinceNCBSPPATK)
        Get
            Return Session("Listmaping.data")
        End Get
        Set(ByVal value As TList(Of MappingMsProvinceNCBSPPATK))
            Session("Listmaping.data") = value
        End Set
    End Property
    ''' <summary>
    ''' List yang tampil di ListBox
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplay() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingMsProvinceNCBSPPATK In Listmaping.FindAllDistinct("Pk_MsProvinceNCBS_Id")
                Temp.Add(i.Pk_MsProvinceNCBS_Id.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property
#End Region

End Class


