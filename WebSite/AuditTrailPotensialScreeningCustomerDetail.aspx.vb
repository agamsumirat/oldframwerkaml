Option Explicit On
Option Strict On
Imports EkaDataNettier.Entities
Imports EkaDataNettier.Data

Imports AMLBLL

Partial Class AuditTrailPotensialScreeningCustomerDetail
    Inherits Parent

    Public ReadOnly Property PKAuditTrailPotensialScreeningCustomerid() As Integer
        Get
            Dim strTemp As String = Request.Params("PKAuditTrailPotensialScreeningCustomerid")
            Dim intResult As Integer
            If Integer.TryParse(strTemp, intResult) Then
                Return intResult
            Else
                CvalHandleErr.IsValid = False
                CvalHandleErr.ErrorMessage = "Parameter PKAuditTrailPotensialScreeningCustomerid is invalid."

            End If
        End Get
    End Property

#Region "SetGrid"
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return CType(IIf(Session("AuditTrailPotensialScreeningCustomerDetail.SelectedItem") Is Nothing, New ArrayList, Session("AuditTrailPotensialScreeningCustomerDetail.SelectedItem")), ArrayList)
        End Get
        Set(ByVal value As ArrayList)
            Session("AuditTrailPotensialScreeningCustomerDetail.SelectedItem") = value
        End Set
    End Property
    Private Property SetnGetSort() As String
        Get
            Return CType(IIf(Session("AuditTrailPotensialScreeningCustomerDetail.Sort") Is Nothing, "PK_AML_Screening_Customer_Result_Id  asc", Session("AuditTrailPotensialScreeningCustomerDetail.Sort")), String)
        End Get
        Set(ByVal Value As String)
            Session("AuditTrailPotensialScreeningCustomerDetail.Sort") = Value
        End Set
    End Property
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return CType(IIf(Session("AuditTrailPotensialScreeningCustomerDetail.CurrentPage") Is Nothing, 0, Session("AuditTrailPotensialScreeningCustomerDetail.CurrentPage")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("AuditTrailPotensialScreeningCustomerDetail.CurrentPage") = Value
        End Set
    End Property
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    Private Property SetnGetRowTotal() As Int32
        Get
            Return CType(IIf(Session("AuditTrailPotensialScreeningCustomerDetail.RowTotal") Is Nothing, 0, Session("AuditTrailPotensialScreeningCustomerDetail.RowTotal")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("AuditTrailPotensialScreeningCustomerDetail.RowTotal") = Value
        End Set
    End Property
    Public Property SetAndGetSearchingCriteria() As String
        Get
            Return CStr(IIf(Session("MsUserViewAppPageSearchCriteria") Is Nothing, "", Session("MsUserViewAppPageSearchCriteria")))
        End Get
        Set(ByVal value As String)
            Session("MsUserViewAppPageSearchCriteria") = value
        End Set
    End Property

    Public Property SetnGetBindTable() As VList(Of vw_AML_Screening_AuditTrailDetail)
        Get


            If Me.SetnGetCurrentPage > Me.GetPageTotal - 1 And Me.GetPageTotal - 1 > 0 Then
                Me.SetnGetCurrentPage = Me.GetPageTotal - 1
            End If

            Session("AuditTrailPotensialScreeningCustomerDetail.Table") = DataRepository.vw_AML_Screening_AuditTrailDetailProvider.GetPaged("FK_AML_Screening_Customer_Request_Detail_ID = '" & PKAuditTrailPotensialScreeningCustomerid & "'", SetnGetSort, SetnGetCurrentPage, Sahassa.AML.Commonly.GetDisplayedTotalRow, Me.SetnGetRowTotal)

            Return CType(Session("AuditTrailPotensialScreeningCustomerDetail.Table"), VList(Of vw_AML_Screening_AuditTrailDetail))
        End Get
        Set(ByVal value As VList(Of vw_AML_Screening_AuditTrailDetail))
            Session("AuditTrailPotensialScreeningCustomerDetail.Table") = value
        End Set
    End Property

    Public Property SetnGetBindTableAll() As VList(Of vw_AML_Screening_AuditTrailDetail)
        Get
            If Me.SetnGetCurrentPage > Me.GetPageTotal - 1 And Me.GetPageTotal - 1 > 0 Then
                Me.SetnGetCurrentPage = Me.GetPageTotal - 1
            End If

            Session("AuditTrailPotensialScreeningCustomerDetail.Table") = DataRepository.vw_AML_Screening_AuditTrailDetailProvider.GetPaged("FK_AML_Screening_Customer_Request_Detail_ID = '" & PKAuditTrailPotensialScreeningCustomerid & "'", SetnGetSort, SetnGetCurrentPage, Sahassa.AML.Commonly.GetDisplayedTotalRow, Me.SetnGetRowTotal)

            Return CType(Session("AuditTrailPotensialScreeningCustomerDetail.Table"), VList(Of vw_AML_Screening_AuditTrailDetail))
        End Get
        Set(ByVal value As VList(Of vw_AML_Screening_AuditTrailDetail))
            Session("AuditTrailPotensialScreeningCustomerDetail.Table") = value
        End Set
    End Property

    Private Sub GetAuditTrail_Potensial_Screening_Customer(ByVal PKAuditTrailPotensialScreeningCustomerid As Integer)
        Dim ObjAuditTrail_Potensial_Screening_Customer As EkaDataNettier.Entities.AML_Screening_Customer_Request_Detail = Nothing


        ObjAuditTrail_Potensial_Screening_Customer = DataRepository.AML_Screening_Customer_Request_DetailProvider.GetByPK_AML_Screening_Customer_Request_Detail_ID(PKAuditTrailPotensialScreeningCustomerid)



        If Not ObjAuditTrail_Potensial_Screening_Customer Is Nothing Then
            Dim objheader As EkaDataNettier.Entities.AML_Screening_Customer_Request = DataRepository.AML_Screening_Customer_RequestProvider.GetByPK_AML_Screening_Customer_Request_ID(CInt(ObjAuditTrail_Potensial_Screening_Customer.FK_AML_Screening_Customer_Request_ID.ToString))
            With objheader
                LblUserID.Text = .UserID

                Dim objListuser As SahassaNettier.Entities.TList(Of SahassaNettier.Entities.User) = SahassaNettier.Data.DataRepository.UserProvider.GetByUserID(.UserID)
                If objListuser.Count > 0 Then
                    LblUserName.Text = objListuser(0).UserName
                End If


                LblScreeningDate.Text = .CreatedDate.GetValueOrDefault(Sahassa.AML.Commonly.GetDefaultDate).ToString("dd-MM-yyyy").Replace("01-01-1900", "")
                LblNama.Text = ObjAuditTrail_Potensial_Screening_Customer.Nama
                LblNationality.Text = ObjAuditTrail_Potensial_Screening_Customer.Nationality
            End With
        End If

    End Sub


#End Region

    Private Sub BindGrid()

        Me.GridAuditTrailPotensialScreeningCustomer.DataSource = Me.SetnGetBindTable
        Me.GridAuditTrailPotensialScreeningCustomer.CurrentPageIndex = Me.SetnGetCurrentPage
        Me.GridAuditTrailPotensialScreeningCustomer.VirtualItemCount = Me.SetnGetRowTotal
        Me.GridAuditTrailPotensialScreeningCustomer.DataBind()
        GetAuditTrail_Potensial_Screening_Customer(PKAuditTrailPotensialScreeningCustomerid)
        If SetnGetRowTotal > 0 Then
            LabelNoRecordFound.Visible = False
        Else
            LabelNoRecordFound.Visible = True
        End If
    End Sub
    Private Sub ClearSession()
        Me.SetnGetSelectedItem = Nothing
        Me.SetnGetCurrentPage = Nothing
        Me.SetnGetRowTotal = Nothing
        Me.SetnGetSort = Nothing
        Me.SetnGetBindTable = Nothing
        Me.SetnGetBindTableAll = Nothing
    End Sub

    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In Me.GridAuditTrailPotensialScreeningCustomer.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = CType(gridRow.FindControl("CheckBoxExporttoExcel"), CheckBox)
                Dim PKAuditTrailPotensialScreeningCustomerID As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
                If ArrTarget.Contains(PKAuditTrailPotensialScreeningCustomerID) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(PKAuditTrailPotensialScreeningCustomerID)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(PKAuditTrailPotensialScreeningCustomerID)
                End If
                Me.SetnGetSelectedItem = ArrTarget
            End If
        Next
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            LblMessage.Text = ""
            LblMessage.Visible = False
            If Not Page.IsPostBack Then
                ClearSession()
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using
            End If
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
            CollectSelected()
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    Private Sub SetInfoNavigate()
        Me.PageCurrentPage.Text = (Me.SetnGetCurrentPage + 1).ToString
        Me.PageTotalPages.Text = Me.GetPageTotal.ToString
        Me.PageTotalRows.Text = Me.SetnGetRowTotal.ToString
        Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
        Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
        Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
        Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
            Me.SetInfoNavigate()
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        'For Each gridRow As DataGridItem In Me.GridAuditTrailPotensialScreeningCustomer.Items
        '    If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
        '        Dim pkid As String = gridRow.Cells(1).Text
        '        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        '        If Me.SetnGetSelectedItem.Contains(pkid) Then
        '            If Me.CheckBoxSelectAll.Checked Then
        '                If Not ArrTarget.Contains(pkid) Then
        '                    ArrTarget.Add(pkid)
        '                End If
        '            Else
        '                ArrTarget.Remove(pkid)
        '            End If
        '        Else
        '            If Me.CheckBoxSelectAll.Checked Then
        '                If Not ArrTarget.Contains(pkid) Then
        '                    ArrTarget.Add(pkid)
        '                End If
        '            End If
        '        End If
        '        Me.SetnGetSelectedItem = ArrTarget
        '    End If
        'Next
        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        For Each gridRow As DataGridItem In Me.GridAuditTrailPotensialScreeningCustomer.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(1).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItem.Contains(StrKey) = False Then
                        ArrTarget.Add(StrKey)
                    End If
                Else
                    If Me.SetnGetSelectedItem.Contains(StrKey) Then
                        ArrTarget.Remove(StrKey)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItem = ArrTarget
    End Sub

    Public Sub BindGridView()
        GridAuditTrailPotensialScreeningCustomer.DataSource = Me.SetnGetBindTable
        GridAuditTrailPotensialScreeningCustomer.VirtualItemCount = Me.SetnGetRowTotal
        GridAuditTrailPotensialScreeningCustomer.DataBind()
    End Sub

    Protected Sub GridAuditTrailPotensialScreeningCustomer_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridAuditTrailPotensialScreeningCustomer.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Header And e.Item.ItemType <> ListItemType.Footer Then
                Dim PK_vw_AML_Screening_AuditTrailDetail_ID As String = e.Item.Cells(1).Text
                Dim objcheckbox As CheckBox = CType(e.Item.FindControl("CheckBoxExporttoExcel"), CheckBox)
                objcheckbox.Checked = Me.SetnGetSelectedItem.Contains(PK_vw_AML_Screening_AuditTrailDetail_ID)

            End If
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub GridAuditTrailPotensialScreeningCustomer_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridAuditTrailPotensialScreeningCustomer.SortCommand
        Dim GridUser As DataGrid = CType(source, DataGrid)
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub BindSelectedAll()
        Try
            Me.GridAuditTrailPotensialScreeningCustomer.DataSource = SetnGetBindTableAll
            Me.GridAuditTrailPotensialScreeningCustomer.AllowPaging = False
            Me.GridAuditTrailPotensialScreeningCustomer.DataBind()

            For i As Integer = 0 To GridAuditTrailPotensialScreeningCustomer.Items.Count - 1
                For y As Integer = 0 To GridAuditTrailPotensialScreeningCustomer.Columns.Count - 1
                    GridAuditTrailPotensialScreeningCustomer.Items(i).Cells(y).Attributes.Add("class", "text")
                Next
            Next
            Me.GridAuditTrailPotensialScreeningCustomer.Columns(0).Visible = False
            Me.GridAuditTrailPotensialScreeningCustomer.Columns(1).Visible = False

        Catch
            Throw
        End Try
    End Sub

    Private Sub BindSelected()
        Dim Rows As New ArrayList

        CollectSelected()
        Dim AllList As VList(Of vw_AML_Screening_AuditTrailDetail) = DataRepository.vw_AML_Screening_AuditTrailDetailProvider.GetPaged("", "", 0, Integer.MaxValue, 0)

        For Each IdPk As Long In Me.SetnGetSelectedItem
            Dim oList As VList(Of vw_AML_Screening_AuditTrailDetail) = AllList.FindAll(vw_AML_Screening_AuditTrailDetailColumn.PK_AML_Screening_Customer_Result_Id.ToString, IdPk)
            If oList.Count > 0 Then
                Rows.Add(oList(0))
            End If
        Next
        Me.GridAuditTrailPotensialScreeningCustomer.DataSource = Rows
        Me.GridAuditTrailPotensialScreeningCustomer.AllowPaging = False
        Me.GridAuditTrailPotensialScreeningCustomer.DataBind()
        For i As Integer = 0 To GridAuditTrailPotensialScreeningCustomer.Items.Count - 1
            For y As Integer = 0 To GridAuditTrailPotensialScreeningCustomer.Columns.Count - 1
                GridAuditTrailPotensialScreeningCustomer.Items(i).Cells(y).Attributes.Add("class", "text")
            Next
        Next

        Me.GridAuditTrailPotensialScreeningCustomer.Columns(0).Visible = False
        Me.GridAuditTrailPotensialScreeningCustomer.Columns(1).Visible = False
    End Sub

    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub 'ClearControls

    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Me.CollectSelected()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelected()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=AuditTrailPotensialScreeningCustomerDetail.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridAuditTrailPotensialScreeningCustomer)
            GridAuditTrailPotensialScreeningCustomer.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub lnkExportAllData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExportAllData.Click
        Try
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            BindSelectedAll()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=AuditTrailPotensialScreeningCustomerDetailAll.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridAuditTrailPotensialScreeningCustomer)
            GridAuditTrailPotensialScreeningCustomer.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            Me.CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) And (CInt(Me.TextGoToPage.Text) > 0) Then
                If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                    Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                Else
                    Throw New Exception("Page number must be less than or equal to the total page count.")
                End If
            Else
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If
            CollectSelected()
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "AuditTrailPotensialScreeningCustomer.aspx"
            Me.Response.Redirect("AuditTrailPotensialScreeningCustomer.aspx", False)
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
End Class
