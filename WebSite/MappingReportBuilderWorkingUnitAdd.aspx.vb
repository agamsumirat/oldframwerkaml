Option Strict On
Option Explicit On
Imports amlbll
Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports SahassaNettier.Services
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Imports System.IO
Imports System.Collections.Generic

Partial Class MappingReportBuilderWorkingUnitAdd
    Inherits Parent

    Public Property SetnGetMode() As String
        Get
            If Not Session("MappingReportBuilderWorkingUnitAdd.Mode") Is Nothing Then
                Return CStr(Session("MappingReportBuilderWorkingUnitAdd.Mode"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("MappingReportBuilderWorkingUnitAdd.Mode") = value
        End Set
    End Property



    Public Property ObjTmappingreportBuilderWorkingUnitmap() As TList(Of mappingreportBuilderWorkingUnitmap)
        Get
            If Session("MappingReportBuilderWorkingUnitAdd.ObjTmappingreportBuilderWorkingUnitmap") Is Nothing Then

                Session("MappingReportBuilderWorkingUnitAdd.ObjTmappingreportBuilderWorkingUnitmap") = New TList(Of mappingreportBuilderWorkingUnitmap)

                Return CType(Session("MappingReportBuilderWorkingUnitAdd.ObjTmappingreportBuilderWorkingUnitmap"), TList(Of mappingreportBuilderWorkingUnitmap))
            Else
                Return CType(Session("MappingReportBuilderWorkingUnitAdd.ObjTmappingreportBuilderWorkingUnitmap"), TList(Of mappingreportBuilderWorkingUnitmap))
            End If
        End Get
        Set(value As TList(Of mappingreportBuilderWorkingUnitmap))
            Session("MappingReportBuilderWorkingUnitAdd.ObjTmappingreportBuilderWorkingUnitmap") = value
        End Set
    End Property

    Public Property ObjMappingReportBuilderWorkingUnit() As MappingReportBuilderWorkingUnit
        Get
            If Session("MappingReportBuilderWorkingUnitAdd.ObjMappingReportBuilderWorkingUnit") Is Nothing Then
                Using objnew As MappingReportBuilderWorkingUnit = New MappingReportBuilderWorkingUnit
                    Dim generator As New Random
                    Dim randomValue As Integer
                    randomValue = generator.Next(Integer.MinValue, -1)
                    objnew.PK_MappingReportBuilderWorkingUnit_ID = randomValue



                    Session("MappingReportBuilderWorkingUnitAdd.ObjMappingReportBuilderWorkingUnit") = objnew
                End Using

                Return CType(Session("MappingReportBuilderWorkingUnitAdd.ObjMappingReportBuilderWorkingUnit"), MappingReportBuilderWorkingUnit)
            Else
                Return CType(Session("MappingReportBuilderWorkingUnitAdd.ObjMappingReportBuilderWorkingUnit"), MappingReportBuilderWorkingUnit)
            End If
        End Get
        Set(value As MappingReportBuilderWorkingUnit)
            Session("MappingReportBuilderWorkingUnitAdd.ObjMappingReportBuilderWorkingUnit") = value
        End Set
    End Property



    Public ReadOnly Property ObjQueryBuilder() As TList(Of QueryBuilder)
        Get
            If Session("MappingReportBuilderWorkingUnitAdd.ObjQueryBuilder") Is Nothing Then
                Session("MappingReportBuilderWorkingUnitAdd.ObjQueryBuilder") = New TList(Of QueryBuilder)
            End If
            Return CType(Session("MappingReportBuilderWorkingUnitAdd.ObjQueryBuilder"), TList(Of QueryBuilder))
        End Get
    End Property

    Public ReadOnly Property ObjWorkingUnit() As TList(Of WorkingUnit)
        Get
            If Session("MappingReportBuilderWorkingUnitAdd.ObjWorkingUnit") Is Nothing Then
                Session("MappingReportBuilderWorkingUnitAdd.ObjWorkingUnit") = New TList(Of WorkingUnit)
            End If
            Return CType(Session("MappingReportBuilderWorkingUnitAdd.ObjWorkingUnit"), TList(Of WorkingUnit))
        End Get
    End Property

    Private Sub LoadModeQueryBuilder()
        Session("MappingReportBuilderWorkingUnitAdd.Mode") = Nothing
        CboReportBuilder.Items.Clear()
        Using ObjHIPORTJIVEBll As New AMLBLL.MappingReportBuilderWorkingUnitBLL
            CboReportBuilder.AppendDataBoundItems = True
            CboReportBuilder.DataSource = ObjHIPORTJIVEBll.GeModeDataReportBuilder
            CboReportBuilder.DataTextField = QueryBuilderColumn.QueryDescription.ToString
            CboReportBuilder.DataValueField = QueryBuilderColumn.PK_QueryBuilder_ID.ToString
            CboReportBuilder.DataBind()
            CboReportBuilder.Items.Insert(0, New ListItem("...", ""))
        End Using
    End Sub

    Private Sub LoadModeWorkingUnit()
        'Session("MappingReportBuilderWorkingUnitAdd.Mode") = Nothing

        'CboWorkingUnit.Items.Clear()
        'Using ObjHIPORTJIVEBll As New AMLBLL.MappingReportBuilderWorkingUnitBLL
        '    CboWorkingUnit.AppendDataBoundItems = True
        '    CboWorkingUnit.DataSource = ObjHIPORTJIVEBll.GeModeDataWorkingUnit
        '    CboWorkingUnit.DataTextField = WorkingUnitColumn.WorkingUnitName.ToString
        '    CboWorkingUnit.DataValueField = WorkingUnitColumn.WorkingUnitID.ToString
        '    CboWorkingUnit.DataBind()
        '    CboWorkingUnit.Items.Insert(0, New ListItem("...", ""))
        'End Using

    End Sub

    'Protected Sub ImageAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAdd.Click

    '    Try

    '        'If CboReportBuilder.SelectedValue <> "" And CboWorkingUnit.SelectedValue <> "" Then
    '        '    Dim listDONK As Integer = 0
    '        '    Dim nnn As Integer = 0
    '        '    Dim Objlist As TList(Of WorkingUnit) = ObjWorkingUnit

    '        '    If Objlist.Count > 0 Then
    '        '        Do While nnn < Objlist.Count
    '        '            If Objlist(nnn).WorkingUnitID = CDbl(CboWorkingUnit.SelectedValue) Then
    '        '                listDONK = 1
    '        '            End If
    '        '            nnn = nnn + 1
    '        '        Loop
    '        '    End If

    '        '    If listDONK = 0 Then

    '        '        Using objnewWorkingUnit As New WorkingUnit
    '        '            Using objNewNewWorkingUnit As TList(Of WorkingUnit) = DataRepository.WorkingUnitProvider.GetPaged("WorkingUnitID = '" & CboWorkingUnit.SelectedValue & "'", "", 0, Integer.MaxValue, 0)

    '        '                objnewWorkingUnit.WorkingUnitID = CInt(CboWorkingUnit.SelectedValue)
    '        '                objnewWorkingUnit.WorkingUnitName = objNewNewWorkingUnit(0).WorkingUnitName
    '        '                ObjWorkingUnit.Add(objnewWorkingUnit)

    '        '                gridView.DataSource = ObjWorkingUnit

    '        '                gridView.DataBind()
    '        '            End Using
    '        '        End Using
    '        '    Else
    '        '        Throw New SahassaException("Working Unit " & CboWorkingUnit.SelectedItem.Text & " sudah ada di list.")

    '        '    End If



    '        'Else
    '        '    Throw New SahassaException("Pilih ReportBuilder atau WorkingUnit terlebih dahulu.")
    '        'End If

    '    Catch ex As Exception
    '        Me.cvalPageError.IsValid = False
    '        Me.cvalPageError.ErrorMessage = ex.Message
    '        LogError(ex)
    '    End Try

    'End Sub

    Function IsDataValid() As Boolean
        Dim strErrorMessage As New StringBuilder
        If CboReportBuilder.SelectedValue = "" Then
            strErrorMessage.Append("Report Builder is Required. ")
        Else

            If Sahassa.AML.Commonly.SessionPkUserId <> 1 Then
                If AMLBLL.MappingReportBuilderWorkingUnitBLL.GetTlistMappingReportBuilderWorkingUnit_Approval(MappingReportBuilderWorkingUnit_ApprovalColumn.QueryName.ToString & "='" & CboReportBuilder.SelectedItem.ToString.Replace("'", "''") & "'", "", 0, Integer.MinValue, 0).Count > 0 Then
                    strErrorMessage.Append("Report Builder " & CboReportBuilder.SelectedItem.Text & " already exist in pending approval.")
                End If


            End If
            If AMLBLL.MappingReportBuilderWorkingUnitBLL.GetTlistMappingReportBuilderWorkingUnit(MappingReportBuilderWorkingUnitColumn.FK_Report_ID.ToString & "=" & CboReportBuilder.SelectedValue, "", 0, Integer.MinValue, 0).Count > 0 Then
                strErrorMessage.Append("Report Builder " & CboReportBuilder.SelectedItem.Text & " already exist.")
            End If
        End If


        If ObjTmappingreportBuilderWorkingUnitmap.Count = 0 Then
            strErrorMessage.Append("At least there is one Working Unit. ")
        End If
        If strErrorMessage.ToString.Trim.Length > 0 Then
            Throw New Exception(strErrorMessage.ToString)
        Else
            Return True
        End If

    End Function
    Sub clearData()
        CboReportBuilder.SelectedValue = ""
        ObjTmappingreportBuilderWorkingUnitmap = Nothing
        ObjMappingReportBuilderWorkingUnit = Nothing
        BindGrid()

    End Sub

    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Try

            If IsDataValid() Then

                ObjMappingReportBuilderWorkingUnit.FK_Report_ID = CInt(CboReportBuilder.SelectedValue)
                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                    AMLBLL.MappingReportBuilderWorkingUnitBLL.SaveAdd(ObjTmappingreportBuilderWorkingUnitmap, ObjMappingReportBuilderWorkingUnit)
                    LblSuccess.Text = "Data Saved."
                    LblSuccess.Visible = True
                Else
                    AMLBLL.MappingReportBuilderWorkingUnitBLL.SaveAddApproval(ObjTmappingreportBuilderWorkingUnitmap, ObjMappingReportBuilderWorkingUnit)
                    LblSuccess.Text = "Data Save into Pending Approval"
                    LblSuccess.Visible = True
                End If
                clearData()

            End If

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
        'Dim oTrans As SqlTransaction = Nothing
        'Try
        '    Dim FK_Report_ID As String
        '    FK_Report_ID = CboReportBuilder.Text


        '    If FK_Report_ID = "" Then
        '        Me.LblSuccess.Text = "pilih terlebih dahulu Repot Builder ."
        '        Me.LblSuccess.Visible = True
        '    Else
        '        If ObjWorkingUnit.Count = 0 Then
        '            Me.LblSuccess.Text = "Working Unit minimal dipilih 1 ."
        '            Me.LblSuccess.Visible = True
        '        Else

        '            AMLBLL.MappingReportBuilderWorkingUnitBLL.IsDataValidAddApproval(CStr(FK_Report_ID), CStr(CboWorkingUnit.SelectedValue))

        '            If Sahassa.AML.Commonly.SessionPkUserId = 1 Then

        '                Using Objvw_mappingreportBuilderWorkingUnit As VList(Of vw_MappingReportBuilderWorkingUnit) = DataRepository.vw_MappingReportBuilderWorkingUnitProvider.GetPaged("FK_Report_ID = '" & FK_Report_ID & "'", "", 0, Integer.MaxValue, 0)
        '                    If Objvw_mappingreportBuilderWorkingUnit.Count > 0 Then
        '                        'Sahassa.AML.Commonly.SessionIntendedPage = "MappingUserIdHIPORTJIVEView.aspx"
        '                        'Me.Response.Redirect("MappingUserIdHIPORTJIVEView.aspx", False)

        '                        Me.LblSuccess.Text = "Data " & Objvw_mappingreportBuilderWorkingUnit(0).QueryName & " Sudah Ada didalam User menu ."
        '                        Me.LblSuccess.Visible = True
        '                    Else
        '                        Using ObjMappingreportBuilderWorkingUnit As New MappingReportBuilderWorkingUnit
        '                            ObjMappingreportBuilderWorkingUnit.FK_Report_ID = CType(FK_Report_ID, Global.System.Nullable(Of Integer))

        '                            DataRepository.MappingReportBuilderWorkingUnitProvider.Save(ObjMappingreportBuilderWorkingUnit)
        '                            Using ObjQueryBuilder As QueryBuilder = DataRepository.QueryBuilderProvider.GetByPK_QueryBuilder_ID(CInt(FK_Report_ID))
        '                                AMLBLL.MappingReportBuilderWorkingUnitBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "MappingReportBuilderWorkingUnit", "QueryName", "Add", "", ObjQueryBuilder.QueryName, "Acc")
        '                            End Using
        '                            For Each FileList As WorkingUnit In ObjWorkingUnit
        '                                Dim ObjmappingreportBuilderWorkingUnit_map As New mappingreportBuilderWorkingUnitmap
        '                                With ObjmappingreportBuilderWorkingUnit_map
        '                                    Dim nama As String
        '                                    .FK_MappingReportBuilderWorkingUnit_ID = ObjMappingreportBuilderWorkingUnit.PK_MappingReportBuilderWorkingUnit_ID
        '                                    .FK_WorkingUnit_ID = FileList.WorkingUnitID
        '                                    nama = FileList.WorkingUnitName
        '                                    AMLBLL.MappingReportBuilderWorkingUnitBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "MappingReportBuilderWorkingUnit", "WorkingUnitName", "Add", "", nama, "Acc")
        '                                    DataRepository.mappingreportBuilderWorkingUnitmapProvider.Save(ObjmappingreportBuilderWorkingUnit_map)
        '                                End With
        '                            Next
        '                            Using ObjQueryBuilder As TList(Of QueryBuilder) = DataRepository.QueryBuilderProvider.GetPaged("PK_QueryBuilder_ID = '" & FK_Report_ID & "'", "", 0, Integer.MaxValue, 0)
        '                                Me.LblSuccess.Text = "Terimakasih data " & ObjQueryBuilder(0).QueryName & " sudah masuk kedalam User menu ."
        '                                Me.LblSuccess.Visible = True
        '                                LoadModeQueryBuilder()
        '                            End Using
        '                        End Using
        '                    End If
        '                End Using
        '            Else
        '                Dim queryname As String = ""
        '                Using ObjmappingreportBuilderWorkingUnit_approval As New MappingReportBuilderWorkingUnit_Approval
        '                    Using ObjmappingreportBuilderWorkingUnitList As TList(Of MappingReportBuilderWorkingUnit) = DataRepository.MappingReportBuilderWorkingUnitProvider.GetPaged("FK_Report_ID = '" & FK_Report_ID & "'", "", 0, Integer.MaxValue, 0)
        '                        If ObjmappingreportBuilderWorkingUnitList.Count > 0 Then
        '                            Sahassa.AML.Commonly.SessionIntendedPage = "MappingReportBuilderWorkingUnitView.aspx"
        '                            Me.Response.Redirect("MappingReportBuilderWorkingUnitView.aspx", False)

        '                        Else
        '                            Using objUser As TList(Of QueryBuilder) = DataRepository.QueryBuilderProvider.GetPaged("PK_QueryBuilder_ID = '" & FK_Report_ID & "'", "", 0, Integer.MaxValue, 0)
        '                                queryname = objUser(0).QueryName
        '                                ObjmappingreportBuilderWorkingUnit_approval.FK_MsUserID = Sahassa.AML.Commonly.SessionPkUserId
        '                                ObjmappingreportBuilderWorkingUnit_approval.QueryName = queryname
        '                                ObjmappingreportBuilderWorkingUnit_approval.FK_ModeID = CInt(Sahassa.AML.Commonly.TypeMode.Add)
        '                                ObjmappingreportBuilderWorkingUnit_approval.CreatedDate = Now
        '                                DataRepository.MappingReportBuilderWorkingUnit_ApprovalProvider.Save(ObjmappingreportBuilderWorkingUnit_approval)

        '                                Using ObjmappingreportBuilderWorkingUnit_approvalDetail As New MappingReportBuilderWorkingUnit_ApprovalDetail
        '                                    ObjmappingreportBuilderWorkingUnit_approvalDetail.FK_MappingReportBuilderWorkingUnit_Approval_ID = ObjmappingreportBuilderWorkingUnit_approval.PK_MappingReportBuilderWorkingUnit_Approval_ID
        '                                    ObjmappingreportBuilderWorkingUnit_approvalDetail.FK_Report_ID = CType(FK_Report_ID, Global.System.Nullable(Of Integer))
        '                                    DataRepository.MappingReportBuilderWorkingUnit_ApprovalDetailProvider.Save(ObjmappingreportBuilderWorkingUnit_approvalDetail)
        '                                    Using ObjQueryBuilder As QueryBuilder = DataRepository.QueryBuilderProvider.GetByPK_QueryBuilder_ID(CInt(FK_Report_ID))
        '                                        AMLBLL.MappingReportBuilderWorkingUnitBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "MappingReportBuilderWorkingUnit", "QueryName", "Add", "", ObjQueryBuilder.QueryName, "PendingApproval")
        '                                    End Using
        '                                    For Each FileList As WorkingUnit In ObjWorkingUnit
        '                                        Dim ObjmappingreportBuilderWorkingUnit_approvalDetailmap As New mappingreportBuilderWorkingUnit_approvalDetailMap
        '                                        With ObjmappingreportBuilderWorkingUnit_approvalDetailmap
        '                                            Dim nama As String
        '                                            .FK_MappingReportBuilderWorkingUnit_Approval_ID = CType(ObjmappingreportBuilderWorkingUnit_approval.PK_MappingReportBuilderWorkingUnit_Approval_ID, Global.System.Nullable(Of Integer))
        '                                            .FK_WorkingUnit_ID = FileList.WorkingUnitID
        '                                            .FK_MappingReportBuilderWorkingUnit_ApprovalDetail_ID = CType(ObjmappingreportBuilderWorkingUnit_approvalDetail.PK_MappingReportBuilderWorkingUnit_ApprovalDetail_ID, Global.System.Nullable(Of Integer))

        '                                            nama = FileList.WorkingUnitName
        '                                            AMLBLL.MappingReportBuilderWorkingUnitBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "MappingReportBuilderWorkingUnit", "WorkingUnitName", "Add", "", nama, "PendingApproval")
        '                                            DataRepository.mappingreportBuilderWorkingUnit_approvalDetailMapProvider.Save(ObjmappingreportBuilderWorkingUnit_approvalDetailmap)
        '                                        End With
        '                                    Next

        '                                End Using
        '                            End Using
        '                            'Sahassa.AML.Commonly.SessionIntendedPage = "MappingUserIdHIPORTJIVEView.aspx"
        '                            'Me.Response.Redirect("MappingUserIdHIPORTJIVEView.aspx", False)
        '                            Using ObjQueryBuilder As TList(Of QueryBuilder) = DataRepository.QueryBuilderProvider.GetPaged("PK_QueryBuilder_ID = '" & FK_Report_ID & "'", "", 0, Integer.MaxValue, 0)
        '                                Me.LblSuccess.Text = "Terimakasih data " & ObjQueryBuilder(0).QueryName & " sudah masuk kedalam Pending Approval ."
        '                                Me.LblSuccess.Visible = True
        '                                LoadModeQueryBuilder()
        '                            End Using
        '                        End If
        '                    End Using
        '                End Using
        '            End If
        '        End If
        '    End If

        'Catch ex As Exception
        '    If Not oTrans Is Nothing Then oTrans.Rollback()
        '    Me.cvalPageError.IsValid = False
        '    Me.cvalPageError.ErrorMessage = ex.Message
        '    LogError(ex)
        'Finally
        '    If Not oTrans Is Nothing Then
        '        oTrans.Dispose()
        '        oTrans = Nothing
        '    End If
        'End Try
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "MappingReportBuilderWorkingUnitView.aspx"
            Me.Response.Redirect("MappingReportBuilderWorkingUnitView.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Sub SelectWorkingUnit(objSelected As TList(Of WorkingUnit))
        For Each item As WorkingUnit In objSelected



            If ObjTmappingreportBuilderWorkingUnitmap.Find(mappingreportBuilderWorkingUnitmapColumn.FK_MappingReportBuilderWorkingUnit_ID, item.WorkingUnitID) Is Nothing Then
                Using objnewdata As mappingreportBuilderWorkingUnitmap = ObjTmappingreportBuilderWorkingUnitmap.AddNew
                    With objnewdata
                        .FK_MappingReportBuilderWorkingUnit_ID = ObjMappingReportBuilderWorkingUnit.PK_MappingReportBuilderWorkingUnit_ID
                        .FK_WorkingUnit_ID = item.WorkingUnitID
                    End With
                End Using
            End If
        Next

        BindGrid()

    End Sub
    Sub BindGrid()
        gridView.DataSource = ObjTmappingreportBuilderWorkingUnitmap
        gridView.DataBind()
    End Sub
    Sub ClearSession()
        ObjTmappingreportBuilderWorkingUnitmap = Nothing
        ObjMappingReportBuilderWorkingUnit = Nothing
        Session("MappingReportBuilderWorkingUnitAdd.ObjQueryBuilder") = Nothing
        Session("MappingReportBuilderWorkingUnitAdd.ObjWorkingUnit") = Nothing
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'Using Transcope As New Transactions.TransactionScope
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                '    Transcope.Complete()
            End Using

            AddHandler PickerWorkingUnit1.SelectWorkingUnit, AddressOf SelectWorkingUnit
            If Not Page.IsPostBack Then
                clearSession()
              
                LoadModeQueryBuilder()
                LoadModeWorkingUnit()
            End If
            'End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub gridview_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles gridView.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then

                Dim pk As String = e.Item.Cells(1).Text
                Dim delBtn As LinkButton = CType(e.Item.Cells(3).FindControl("lnkGridDelete"), LinkButton)

                Dim objdata As mappingreportBuilderWorkingUnitmap = CType(e.Item.DataItem, mappingreportBuilderWorkingUnitmap)
                Dim objlblworkingunit As Label = CType(e.Item.FindControl("LblWorkingUnit"), Label)

                Using objwp As WorkingUnit = AMLBLL.MappingReportBuilderWorkingUnitBLL.GetWorkingUnitByPk(objdata.FK_WorkingUnit_ID.GetValueOrDefault(0))
                    If Not objwp Is Nothing Then
                        objlblworkingunit.Text = objwp.WorkingUnitName
                    End If
                End Using


                delBtn.OnClientClick = DELETE_ATTACHMENT_CONFIRMATION(e.Item.Cells(2).Text.ToString)

            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

   

    Public Shared Function DELETE_ATTACHMENT_CONFIRMATION(ByVal str As String) As String
        Dim result As String = String.Empty

        If Not Sahassa.AML.Commonly.SessionLanguage Is Nothing Then
            Select Case Sahassa.AML.Commonly.SessionLanguage.ToLower()
                Case "en-us"
                    result = "javascript:return window.confirm('Are you sure want to delete " & str & "?');"
                Case "id-id"
                    result = "javascript:return window.confirm('Anda yakin ingin menghapus " & str & "?');"
                Case Else
                    result = "javascript:return window.confirm('Are you sure want to delete " & str & "?');"
            End Select
        Else
            result = "javascript:return window.confirm('Are you sure want to delete " & str & "?');"
        End If

        Return result
    End Function

    Protected Sub btnBrowse_Click(sender As Object, e As System.EventArgs) Handles btnBrowse.Click
        Try
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "idpopup2", "popUpDrag('" & CType(sender, LinkButton).ClientID & "','divBrowseUser');", True)
            PickerWorkingUnit1.initData()

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        Try

            If Not Page.ClientScript.IsClientScriptIncludeRegistered("idpopup1") Then
                Page.ClientScript.RegisterClientScriptInclude(Me.GetType(), "idpopup1", ResolveClientUrl("script/popupdrag.js"))
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub lnkGridDelete_Click(sender As Object, e As System.EventArgs)
        Try
            Dim objlinkdelete As LinkButton = CType(sender, LinkButton)
            Dim objgriditem As DataGridItem = CType(objlinkdelete.NamingContainer, DataGridItem)
            Dim objpk As Integer = CType(objgriditem.Cells(1).Text, Integer)
            Using objdel As mappingreportBuilderWorkingUnitmap = ObjTmappingreportBuilderWorkingUnitmap.Find(mappingreportBuilderWorkingUnitmapColumn.FK_WorkingUnit_ID, objpk)
                If Not objdel Is Nothing Then
                    ObjTmappingreportBuilderWorkingUnitmap.Remove(objdel)
                End If
            End Using
            BindGrid()



        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub gridView_PageIndexChanged(source As Object, e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles gridView.PageIndexChanged
        Try
            gridView.CurrentPageIndex = e.NewPageIndex
            BindGrid()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class


