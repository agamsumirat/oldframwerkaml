﻿Imports SahassaNettier.Entities
Imports SahassaNettier.Services
Imports AMLBLL

Partial Class VIPCodeApproval
    Inherits Parent
#Region " Property "
    ''' <summary>
    ''' selected item store
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("VIPCodeApprovalSelected") Is Nothing, New ArrayList, Session("VIPCodeApprovalSelected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("VIPCodeApprovalSelected") = value
        End Set
    End Property
    ''' <summary>
    ''' setnget search field
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetFieldSearch() As String
        Get
            Return IIf(Session("VIPCodeApprovalFieldSearch") Is Nothing, "", Session("VIPCodeApprovalFieldSearch"))
        End Get
        Set(ByVal Value As String)
            Session("VIPCodeApprovalFieldSearch") = Value
        End Set
    End Property
    ''' <summary>
    ''' search value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetValueSearch() As String
        Get
            Return IIf(Session("VIPCodeApprovalValueSearch") Is Nothing, "", Session("VIPCodeApprovalValueSearch"))
        End Get
        Set(ByVal Value As String)
            Session("VIPCodeApprovalValueSearch") = Value
        End Set
    End Property
    ''' <summary>
    ''' sort expresion
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("VIPCodeApprovalSort") Is Nothing, "PK_VIPCodeApproval_ID  asc", Session("VIPCodeApprovalSort"))
        End Get
        Set(ByVal Value As String)
            Session("VIPCodeApprovalSort") = Value
        End Set
    End Property
    ''' <summary>
    ''' current page index
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("VIPCodeApprovalCurrentPage") Is Nothing, 0, Session("VIPCodeApprovalCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("VIPCodeApprovalCurrentPage") = Value
        End Set
    End Property
    ''' <summary>
    ''' total pages
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    ''' <summary>
    ''' row total
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("VIPCodeApprovalRowTotal") Is Nothing, 0, Session("VIPCodeApprovalRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("VIPCodeApprovalRowTotal") = Value
        End Set
    End Property
    ''' <summary>
    ''' save bind table
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetBindTable() As VList(Of Vw_VIPCodeApproval)
        Get
            Session("VIPCodeApprovalData") = VIPCodeBLL.GetVw_VIPCodeApproval(SetnGetValueSearch, SetnGetSort, SetnGetCurrentPage, Sahassa.AML.Commonly.SessionPagingLimit, SetnGetRowTotal)
            Return CType(Session("VIPCodeApprovalData"), VList(Of Vw_VIPCodeApproval))
        End Get
        Set(ByVal value As VList(Of Vw_VIPCodeApproval))
            Session("VIPCodeApprovalData") = value
        End Set
    End Property
#End Region

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' fill search
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------

    Private Sub ClearThisPageSessions()
        Session("VIPCodeApprovalSelected") = Nothing
        Session("VIPCodeApprovalFieldSearch") = Nothing
        Session("VIPCodeApprovalValueSearch") = Nothing
        Session("VIPCodeApprovalSort") = Nothing
        Session("VIPCodeApprovalCurrentPage") = Nothing
        Session("VIPCodeApprovalRowTotal") = Nothing
        Session("VIPCodeApprovalData") = Nothing
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not Me.IsPostBack Then
                Me.ClearThisPageSessions()


                'Using AccessSTRReportToPPATK As New AMLDAL.RulesAdvancedTableAdapters.RulesAdvancedTableAdapter
                '    'Me.SetnGetBindTable = AccessSTRReportToPPATK.GetData
                'End Using
                Sahassa.AML.Commonly.SessionPagingLimit = 10
                FilterData()
                Me.BindGrid()
                'Me.GridMSUserView.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using

                Me.popUpEntryDate.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TxtEntryDateFrom.ClientID & "'), 'dd-mm-yyyy')")
                Me.popUpEntryDateUntil.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TxtEntryDateUntil.ClientID & "'), 'dd-mm-yyyy')")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' page navigate button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
            Me.CollectSelected()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' bind grid
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Sub BindGrid()
        Try
            'Dim Rows() As AMLDAL.RulesAdvanced.RulesAdvancedRow = Me.SetnGetBindTable.Select(Me.SetnGetFieldSearch.Replace("-=Search=-", Me.SetnGetValueSearch.Replace("'", "''")), Me.SetnGetSort)
            Me.gridVIPCodeApproval.DataSource = SetnGetBindTable()
            'Me.GridMSUserView.CurrentPageIndex = Me.SetnGetCurrentPage
            'Me.GridMSUserView.VirtualItemCount = Me.SetnGetRowTotal
            Me.gridVIPCodeApproval.DataBind()
        Catch
            Throw
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' set navigate info
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
            'Me.TextSearch.Text = Me.SetnGetValueSearch
        Catch
            Throw
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' change sort expression
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles gridVIPCodeApproval.SortCommand
        Dim GridUser As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' image button search
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------

    Sub FilterData()
        SetnGetValueSearch = ""
        Dim whereClause As New System.Collections.Generic.List(Of String)
        If TxtPreparer.Text.Trim.Length > 0 Then
            whereClause.Add("CreatedBy = '" & TxtPreparer.Text & "'")
        End If
        If TxtEntryDateFrom.Text.Trim.Length > 0 AndAlso TxtEntryDateUntil.Text.Trim.Length > 0 Then

            If Sahassa.AML.Commonly.IsDateValid("dd-MM-yyyy", TxtEntryDateFrom.Text) AndAlso Sahassa.AML.Commonly.IsDateValid("dd-MM-yyyy", TxtEntryDateUntil.Text) Then

                Dim tanggal As String = Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", TxtEntryDateFrom.Text).ToString("yyyy-MM-dd")
                Dim tanggalAkhir As String = Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", TxtEntryDateUntil.Text).ToString("yyyy-MM-dd")
                If DateDiff(DateInterval.Day, Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", TxtEntryDateFrom.Text), Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", TxtEntryDateUntil.Text)) > -1 Then

                    whereClause.Add(Vw_VIPCodeApprovalColumn.EntryDate.ToString & " between '" & tanggal & " 00:00:00' and '" & tanggalAkhir & " 23:59:59'")
                Else
                    Throw New Exception("Start Date must be greater than End Date.")
                End If
            End If
        End If

        whereClause.Add("FK_MsUserID <> " & Sahassa.AML.Commonly.SessionPkUserId & " AND FK_MsUserID IN (SELECT u.pkUserID FROM [User] u WHERE u.UserID IN(select UserId from UserWorkingUnitAssignment Where WorkingUnitId in (select WorkingUnitId from UserWorkingUnitAssignment where UserId = '" & Sahassa.AML.Commonly.SessionUserId & "')))")

        SetnGetValueSearch = String.Join(" and ", whereClause.ToArray)
    End Sub

    Private Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnSearch.Click
        Try
            FilterData()
            Me.SetnGetCurrentPage = 0
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub



    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' go button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	14/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            Me.CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be a positive number greater than zero.")
                End If
            Else
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    ''' <summary>
    ''' collect sub
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In Me.gridVIPCodeApproval.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = gridRow.FindControl("CheckBoxExporttoExcel")
                Dim groupID As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
                If ArrTarget.Contains(groupID) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(groupID)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(groupID)
                End If
                Me.SetnGetSelectedItem = ArrTarget
            End If
        Next
    End Sub

    ''' <summary>
    ''' prerender event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    ''' <summary>
    ''' clear all control except control
    ''' </summary>
    ''' <param name="control">excluded control</param>
    ''' <remarks></remarks>
    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub 'ClearControls

    ''' <summary>
    ''' bind selected item
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub BindSelected(Optional ByVal all As Boolean = False)
        Try
            If all Then
                Me.gridVIPCodeApproval.DataSource = VIPCodeBLL.GetVw_VIPCodeApproval(SetnGetValueSearch, SetnGetSort, SetnGetCurrentPage, Integer.MaxValue, SetnGetRowTotal)
            Else
                Dim listPK As New System.Collections.Generic.List(Of String)
                For Each IdPk As Int32 In Me.SetnGetSelectedItem
                    listPK.Add(IdPk.ToString)
                Next
                If listPK.Count < 1 Then
                    listPK.Add("0")
                End If
                Me.gridVIPCodeApproval.DataSource = VIPCodeBLL.GetVw_VIPCodeApproval("PK_VIPCodeApproval_ID in (" & String.Join(",", listPK.ToArray) & ")", SetnGetSort, 0, Sahassa.AML.Commonly.SessionPagingLimit, SetnGetRowTotal)

            End If


            Me.gridVIPCodeApproval.AllowPaging = False
            Me.gridVIPCodeApproval.DataBind()
            'Sembunyikan kolom ke 0,1,6 & 7 agar tidak ikut diekspor ke excel
            Me.gridVIPCodeApproval.Columns(0).Visible = False
            Me.gridVIPCodeApproval.Columns(6).Visible = False
            'Me.gridVIPCodeApproval.Columns(10).Visible = False
            'Me.gridVIPCodeApproval.Columns(11).Visible = False
        Catch
            Throw
        End Try
    End Sub

    ''' <summary>
    ''' export button 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Me.CollectSelected()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelected()
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=VIPCodeApproval.xls")
            Response.Charset = ""
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(gridVIPCodeApproval)
            gridVIPCodeApproval.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get item bound
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles gridVIPCodeApproval.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Dim chkBox As CheckBox = e.Item.FindControl("CheckBoxExporttoExcel")
                chkBox.Checked = Me.SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)

            End If
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                'If e.Item.Cells(4).Text <> "&nbsp;" And e.Item.Cells(4).Text <> "" Then
                '    e.Item.Cells(4).Text = CDate(e.Item.Cells(4).Text).ToString("dd-MM-yyyy")
                'End If
                'If e.Item.Cells(6).Text <> "&nbsp;" And e.Item.Cells(6).Text <> "" Then
                '    e.Item.Cells(6).Text = CDate(e.Item.Cells(6).Text).ToString("dd-MM-yyyy")
                'End If
                'If e.Item.Cells(10).Text <> "&nbsp;" And e.Item.Cells(10).Text <> "" Then
                '    e.Item.Cells(10).Text = CDate(e.Item.Cells(10).Text).ToString("dd-MM-yyyy")
                'End If
                'If e.Item.Cells(8).Text.ToLower = "true" Then
                '    Dim linkBtn As LinkButton = e.Item.FindControl("lnkActive")
                '    linkBtn.Enabled = False
                'Else
                '    Dim linkBtn As LinkButton = e.Item.FindControl("lnkInactive")
                '    linkBtn.Enabled = False
                'End If
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' select all
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        'For Each gridRow As DataGridItem In Me.gridVIPCodeApproval.Items
        '    If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
        '        Dim GroupID As String = gridRow.Cells(1).Text
        '        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        '        If Me.SetnGetSelectedItem.Contains(GroupID) Then
        '            If Me.CheckBoxSelectAll.Checked Then
        '                ArrTarget.Add(GroupID)
        '            Else
        '                ArrTarget.Remove(GroupID)
        '            End If
        '        Else
        '            If Me.CheckBoxSelectAll.Checked Then
        '                ArrTarget.Add(GroupID)
        '            End If
        '        End If
        '        Me.SetnGetSelectedItem = ArrTarget
        '    End If
        'Next
        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        For Each gridRow As DataGridItem In gridVIPCodeApproval.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(1).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItem.Contains(StrKey) = False Then
                        ArrTarget.Add(StrKey)
                    End If
                Else
                    If Me.SetnGetSelectedItem.Contains(StrKey) Then
                        ArrTarget.Remove(StrKey)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItem = ArrTarget
    End Sub



    Protected Sub gridVIPCodeApproval_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles gridVIPCodeApproval.ItemCommand
        Try
            Dim VIPCodeApprovalID As String = e.Item.Cells(1).Text
            If e.CommandName.ToLower = "detail" Then
                Sahassa.AML.Commonly.SessionIntendedPage = "VIPCodeApprovalDetail.aspx?VIPCodeApprovalID=" & VIPCodeApprovalID
                Me.Response.Redirect("VIPCodeApprovalDetail.aspx?VIPCodeApprovalID=" & VIPCodeApprovalID, False)
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Sub clearControl()
        TxtEntryDateFrom.Text = ""
        TxtEntryDateUntil.Text = ""
        TxtPreparer.Text = ""
    End Sub

    Protected Sub ImgBtnClearSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnClearSearch.Click
        clearControl()
        FilterData()
    End Sub

    Protected Sub lnkExportAllData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExportAllData.Click
        Try
            Me.CollectSelected()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelected(True)
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=VIPCodeApproval.xls")
            Response.Charset = ""
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(gridVIPCodeApproval)
            gridVIPCodeApproval.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class
