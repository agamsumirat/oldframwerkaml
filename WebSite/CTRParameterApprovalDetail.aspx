<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="CTRParameterApprovalDetail.aspx.vb" Inherits="CTRParameterApprovalDetail" title="CTR Parameter Setting - Approval Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
<table cellSpacing="4" cellPadding="0" width="100%">
		<tr>
			<td vAlign="bottom" align="left"><IMG height="15" src="images/dot_title.gif" width="15"></td>
			<td class="maintitle" vAlign="bottom" width="99%"><asp:label id="LabelTitle" runat="server"></asp:label></td>
		</tr>
	</table>
    <table style="width: 100%">
       
        <tr>
            <td style="height: 136px">
              </td>
            <td width="50%" valign="top" >            
                <asp:Panel ID="PanelNew" runat="server" Height="100%" Width="100%" GroupingText="New Value">
                    <table style="width: 100%">
                        <tr>
                            <td nowrap="nowrap" width="1%">
                                Transaction Ammount</td>
                            <td width="1">
                                :</td>
                            <td>
                                <asp:Label ID="LabelTransactionAmmountNew" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" width="1%">
                                CTR Email To</td>
                            <td>
                                :</td>
                            <td>
                                <asp:Label ID="LabelCTREmailToNew" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" width="1%">
                                CTR Email CC</td>
                            <td>
                                :</td>
                            <td>
                                <asp:Label ID="LabelCTREmailCCNew" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" width="1%">
                                CTR Email BCC</td>
                            <td>
                                :</td>
                            <td>
                                <asp:Label ID="LabelCTREmailBCCNew" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" width="1%">
                                CTR Email Subject</td>
                            <td>
                                :</td>
                            <td>
                                <asp:Label ID="LabelCTREmailSubjectNew" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" width="1%">
                                CTR Email Body</td>
                            <td>
                                :</td>
                            <td>
                                <asp:Label ID="LabelCTREmailBodyNew" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" width="1%">
                                CTR PIC</td>
                            <td>
                                :</td>
                            <td>
                                <asp:Label ID="LabelCTRPICNew" runat="server"></asp:Label></td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td width="50%" valign="top" >
              <asp:Panel ID="PanelOld" runat="server" Height="100%" Width="100%" GroupingText="Old Value">
                    <table style="width: 100%">
                        <tr>
                            <td nowrap="nowrap" width="1%">
                                Transaction Ammount</td>
                            <td width="1">
                                :</td>
                            <td>
                                <asp:Label ID="LabelTransactionAmmountOld" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" width="1%">
                                CTR Email To</td>
                            <td>
                                :</td>
                            <td>
                                <asp:Label ID="LabelCTREmailToOld" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" width="1%">
                                CTR Email CC</td>
                            <td>
                                :</td>
                            <td>
                                <asp:Label ID="LabelCTREmailCCOld" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" width="1%">
                                CTR Email BCC</td>
                            <td>
                                :</td>
                            <td>
                                <asp:Label ID="LabelCTREmailBCCOld" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" width="1%">
                                CTR Email Subject</td>
                            <td>
                                :</td>
                            <td>
                                <asp:Label ID="LabelCTREmailSubjectOld" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" width="1%">
                                CTR Email Body</td>
                            <td>
                                :</td>
                            <td>
                                <asp:Label ID="LabelCTREmailBodyOld" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" width="1%">
                                CTR PIC</td>
                            <td>
                                :</td>
                            <td>
                                <asp:Label ID="LabelCTRPICOld" runat="server"></asp:Label></td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        
        </tr>
       <TR class="formText" id="Button11" bgColor="#dddddd" height="30">
		    <TD style="width: 22px"><IMG height="15" src="images/arrow.gif" width="15"></TD>
		    <TD colSpan="7">
			    <TABLE cellSpacing="0" cellPadding="3" border="0">
				    <TR>
					    <TD><asp:imagebutton id="ImageAccept" runat="server" CausesValidation="True" SkinID="AcceptButton"></asp:imagebutton></TD>
					    <TD><asp:imagebutton id="ImageReject" runat="server" CausesValidation="False" SkinID="RejectButton"></asp:imagebutton></TD>
					    <td><asp:imagebutton id="ImageBack" runat="server" CausesValidation="False" SkinID="BackButton"></asp:imagebutton></td>
				    </TR>
			    </TABLE>
                <asp:CustomValidator ID="cvalPageErr" runat="server" Display="none"></asp:CustomValidator></TD>
	    </TR>
    </table>
	
</asp:Content>

