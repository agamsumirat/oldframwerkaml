﻿Imports Sahassanettier.Data
Imports Sahassanettier.Entities
Imports Sahassanettier.Services
Imports AMLBLL
Partial Class MsSystemParameterEdit
    Inherits Parent



    Public ReadOnly Property PKEdit() As Integer
        Get
            Dim strtemp As String = Request.Params("PKEdit")
            Dim intResult As Integer
            If Not Integer.TryParse(strtemp, intResult) Then
                Throw New Exception("PK edit must Integer")
            End If
            Return intResult
        End Get

    End Property

    Public Property ObjMsSystemParameter() As MsSystemParameter
        Get
            If Session("SystemParameterEdit.ObjMsSystemParameter") Is Nothing Then
                Session("SystemParameterEdit.ObjMsSystemParameter") = MsSystemParameterBll.GetMsSystemParameterByPk(Me.PKEdit)
                Return CType(Session("SystemParameterEdit.ObjMsSystemParameter"), MsSystemParameter)
            Else
                Return CType(Session("SystemParameterEdit.ObjMsSystemParameter"), MsSystemParameter)
            End If
        End Get
        Set(value As MsSystemParameter)
            Session("SystemParameterEdit.ObjMsSystemParameter") = value
        End Set
    End Property
    Sub ClearSession()
        ObjMsSystemParameter = Nothing
    End Sub

    Sub LoadData()
        If Not ObjMsSystemParameter Is Nothing Then
            lblMsSystemParameter_Name.Text = ObjMsSystemParameter.MsSystemParameter_Name
            txtMsSystemParameter_Value.Text = ObjMsSystemParameter.MsSystemParameter_Value

        End If
    End Sub



    Protected Sub Page_Load1(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                ClearSession()
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using

                LoadData()
            End If

        Catch ex As Exception
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Function IsDataValid() As Boolean
        Dim strErrorMessage As New StringBuilder

        If txtMsSystemParameter_Value.Text.Trim = "" Then
            strErrorMessage.Append("Value is Required.")
        End If

        If Sahassa.AML.Commonly.SessionPkUserId <> 1 Then
            'cek ada di approval belum kalau 
            If MsSystemParameterBll.IsInPendingApproval(Me.PKEdit) > 0 Then
                strErrorMessage.Append("Parameter " & lblMsSystemParameter_Name.Text & " already In Pending Approval.")
            End If
        End If
        If strErrorMessage.ToString.Trim.Length > 0 Then
            Throw New Exception(strErrorMessage.ToString)
        Else
            Return True
        End If

    End Function


    Protected Sub ImgBtnSave_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnSave.Click
        Try
            If IsDataValid() Then
                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                    ObjMsSystemParameter.MsSystemParameter_Value = txtMsSystemParameter_Value.Text.Trim
                    MsSystemParameterBll.SaveData(ObjMsSystemParameter)
                    LblConfirmation.Text = "Data System Parameter Saved"
                    MtvMsUser.ActiveViewIndex = 1
                Else
                    ObjMsSystemParameter.MsSystemParameter_Value = txtMsSystemParameter_Value.Text.Trim
                    MsSystemParameterBll.SaveDataIntoPending(ObjMsSystemParameter)
                    LblConfirmation.Text = "Data System Parameter Saved into Pending Approval."
                    MtvMsUser.ActiveViewIndex = 1
                End If
            End If
        Catch ex As Exception
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImgBack_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImgBack.Click
        Try
            Response.Redirect("SystemParameterView.aspx", False)
        Catch ex As Exception
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImgBackAdd_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImgBackAdd.Click
        Try
            Response.Redirect("SystemParameterView.aspx", False)
        Catch ex As Exception
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class
