Option Explicit On
Option Strict On
Imports SahassaNettier.Entities
Imports SahassaNettier.Data

Imports AMLBLL

Partial Class QuestionaireSTRApproval
    Inherits Parent

#Region "SetnGet Searching"
    Public Property SetnGetUserName() As String
        Get
            If Not Session("QuestionaireSTRApproval.SetnGetUserName") Is Nothing Then
                Return CStr(Session("QuestionaireSTRApproval.SetnGetUserName"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("QuestionaireSTRApproval.SetnGetDescriptionAlertSTR") = value
        End Set
    End Property
    Public Property SetnGetDescriptionAlertSTR() As String
        Get
            If Not Session("QuestionaireSTRApproval.SetnGetDescriptionAlertSTR") Is Nothing Then
                Return CStr(Session("QuestionaireSTRApproval.SetnGetDescriptionAlertSTR"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("QuestionaireSTRApproval.SetnGetDescriptionAlertSTR") = value
        End Set
    End Property
    Public Property SetnGetQuestionNo() As String
        Get
            If Not Session("QuestionaireSTRApproval.SetnGetQuestionNo") Is Nothing Then
                Return CStr(Session("QuestionaireSTRApproval.SetnGetQuestionNo"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("QuestionaireSTRApproval.SetnGetQuestionNo") = value
        End Set
    End Property
    Public Property SetnGetCreatedDate() As String
        Get
            If Not Session("QuestionaireSTRApproval.SetnGetCreatedDate") Is Nothing Then
                Return CStr(Session("QuestionaireSTRApproval.SetnGetCreatedDate"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("QuestionaireSTRApproval.SetnGetCreatedDate") = value
        End Set
    End Property
    Public Property SetnGetNama() As String
        Get
            If Not Session("QuestionaireSTRApproval.SetnGetNama") Is Nothing Then
                Return CStr(Session("QuestionaireSTRApproval.SetnGetNama"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("QuestionaireSTRApproval.SetnGetNama") = value
        End Set
    End Property
    Private Property SetnGetFieldSearch() As String
        Get
            Return CStr(IIf(Session("QuestionaireSTRApprovalFieldSearch") Is Nothing, "", Session("QuestionaireSTRApprovalFieldSearch")))
        End Get
        Set(ByVal Value As String)
            Session("QuestionaireSTRApprovalFieldSearch") = Value
        End Set
    End Property
    Private Property SetnGetValueSearch() As String
        Get
            Return CStr(IIf(Session("QuestionaireSTRApprovalValueSearch") Is Nothing, "", Session("QuestionaireSTRApprovalValueSearch")))
        End Get
        Set(ByVal Value As String)
            Session("QuestionaireSTRApprovalValueSearch") = Value
        End Set
    End Property
#End Region

#Region "SetGrid"
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return CType(IIf(Session("QuestionaireSTRApproval.SelectedItem") Is Nothing, New ArrayList, Session("QuestionaireSTRApproval.SelectedItem")), ArrayList)
        End Get
        Set(ByVal value As ArrayList)
            Session("QuestionaireSTRApproval.SelectedItem") = value
        End Set
    End Property
    Private Property SetnGetSort() As String
        Get
            Return CType(IIf(Session("QuestionaireSTRApproval.Sort") Is Nothing, "DescriptionAlertSTR asc", Session("QuestionaireSTRApproval.Sort")), String)
        End Get
        Set(ByVal Value As String)
            Session("QuestionaireSTRApproval.Sort") = Value
        End Set
    End Property
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return CType(IIf(Session("QuestionaireSTRApproval.CurrentPage") Is Nothing, 0, Session("QuestionaireSTRApproval.CurrentPage")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("QuestionaireSTRApproval.CurrentPage") = Value
        End Set
    End Property
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    Private Property SetnGetRowTotal() As Int32
        Get
            Return CType(IIf(Session("QuestionaireSTRApproval.RowTotal") Is Nothing, 0, Session("QuestionaireSTRApproval.RowTotal")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("QuestionaireSTRApproval.RowTotal") = Value
        End Set
    End Property
    Public Property SetAndGetSearchingCriteria() As String
        Get
            Return CStr(IIf(Session("MsUserViewAppPageSearchCriteria") Is Nothing, "", Session("MsUserViewAppPageSearchCriteria")))
        End Get
        Set(ByVal value As String)
            Session("MsUserViewAppPageSearchCriteria") = value
        End Set
    End Property

    Public Property SetnGetBindTable() As VList(Of vw_MsQuestionaireSTRApproval)
        Get
            Dim strWhereClause(-1) As String
            Dim strAllWhereClause As String = ""
            If Me.SetnGetCurrentPage > Me.GetPageTotal - 1 And Me.GetPageTotal - 1 > 0 Then
                Me.SetnGetCurrentPage = Me.GetPageTotal - 1
            End If

            If ComboSearch.Text = "UserName Like '%-=Search=-%'" Then

                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "UserName like '%" & TextSearch.Text.Trim.Replace("'", "''") & "%'"

            ElseIf ComboSearch.Text = "DescriptionAlertSTR Like '%-=Search=-%'" Then

                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "DescriptionAlertSTR like '%" & TextSearch.Text.Trim.Replace("'", "''") & "%'"

            ElseIf ComboSearch.Text = "QuestionNo Like '%-=Search=-%'" Then

                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "QuestionNo like '%" & TextSearch.Text.Trim.Replace("'", "''") & "%'"

            ElseIf ComboSearch.Text = "Nama Like '%-=Search=-%'" Then

                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "Nama like '%" & TextSearch.Text.Trim.Replace("'", "''") & "%'"

            ElseIf ComboSearch.Text = "CreatedDate Like '%-=Search=-%'" Then
                If Sahassa.AML.Commonly.IsDateValid("dd-MM-yyyy", TextSearch.Text) AndAlso Sahassa.AML.Commonly.IsDateValid("dd-MM-yyyy", TextSearch.Text) Then
                    Dim CreatedDate As String = Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", TextSearch.Text).ToString("yyyy-MM-dd")
                    ReDim Preserve strWhereClause(strWhereClause.Length)
                    strWhereClause(strWhereClause.Length - 1) = "CreatedDate >= '" & CreatedDate & " 00:01' AND CreatedDate <= '" & CreatedDate & " 23:59'"
                End If
            End If


            strAllWhereClause = String.Join(" and ", strWhereClause)
            'If strAllWhereClause.Trim.Length > 0 Then
            '    strAllWhereClause += " UserID IN (select UserId from UserWorkingUnitAssignment Where WorkingUnitId in (select WorkingUnitId from UserWorkingUnitAssignment where UserId = '" & Sahassa.AML.Commonly.SessionUserId & "'))"
            'Else
            '    strAllWhereClause += " UserID IN (select UserId from UserWorkingUnitAssignment Where WorkingUnitId in (select WorkingUnitId from UserWorkingUnitAssignment where UserId = '" & Sahassa.AML.Commonly.SessionUserId & "'))"
            'End If
            Session("QuestionaireSTRApproval.Table") = DataRepository.vw_MsQuestionaireSTRApprovalProvider.GetPaged(strAllWhereClause, SetnGetSort, SetnGetCurrentPage, Sahassa.AML.Commonly.GetDisplayedTotalRow, Me.SetnGetRowTotal)

            Return CType(Session("QuestionaireSTRApproval.Table"), VList(Of vw_MsQuestionaireSTRApproval))
        End Get
        Set(ByVal value As VList(Of vw_MsQuestionaireSTRApproval))
            Session("QuestionaireSTRApproval.Table") = value
        End Set
    End Property

    Public Property SetnGetBindTableAll() As VList(Of vw_MsQuestionaireSTRApproval)
        Get
            Dim strWhereClause(-1) As String
            Dim strAllWhereClause As String = ""
            If Me.SetnGetCurrentPage > Me.GetPageTotal - 1 And Me.GetPageTotal - 1 > 0 Then
                Me.SetnGetCurrentPage = Me.GetPageTotal - 1
            End If

            If ComboSearch.Text = "UserName Like '%-=Search=-%'" Then

                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "UserName like '%" & TextSearch.Text.Trim.Replace("'", "''") & "%'"

            ElseIf ComboSearch.Text = "DescriptionAlertSTR Like '%-=Search=-%'" Then

                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "DescriptionAlertSTR like '%" & TextSearch.Text.Trim.Replace("'", "''") & "%'"

            ElseIf ComboSearch.Text = "QuestionNo Like '%-=Search=-%'" Then

                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "QuestionNo like '%" & TextSearch.Text.Trim.Replace("'", "''") & "%'"

            ElseIf ComboSearch.Text = "Nama Like '%-=Search=-%'" Then

                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "Nama like '%" & TextSearch.Text.Trim.Replace("'", "''") & "%'"

            ElseIf ComboSearch.Text = "CreatedDate Like '%-=Search=-%'" Then
                If Sahassa.AML.Commonly.IsDateValid("dd-MM-yyyy", TextSearch.Text) AndAlso Sahassa.AML.Commonly.IsDateValid("dd-MM-yyyy", TextSearch.Text) Then
                    Dim CreatedDate As String = Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", TextSearch.Text).ToString("yyyy-MM-dd")
                    ReDim Preserve strWhereClause(strWhereClause.Length)
                    strWhereClause(strWhereClause.Length - 1) = "CreatedDate >= '" & CreatedDate & " 00:01' AND CreatedDate <= '" & CreatedDate & " 23:59'"
                End If
            End If



            strAllWhereClause = String.Join(" and ", strWhereClause)
            'If strAllWhereClause.Trim.Length > 0 Then
            '    strAllWhereClause += " and FK_MsUserID <> " & Sahassa.AML.Commonly.SessionPkUserId & " AND FK_MsUserID IN (SELECT u.pkUserID FROM [User] u WHERE u.UserID IN(select UserId from UserWorkingUnitAssignment Where WorkingUnitId in (select WorkingUnitId from UserWorkingUnitAssignment where UserId = '" & Sahassa.AML.Commonly.SessionUserId & "')))"
            'Else
            '    strAllWhereClause += " FK_MsUserID <> " & Sahassa.AML.Commonly.SessionPkUserId & " AND FK_MsUserID IN (SELECT u.pkUserID FROM [User] u WHERE u.UserID IN(select UserId from UserWorkingUnitAssignment Where WorkingUnitId in (select WorkingUnitId from UserWorkingUnitAssignment where UserId = '" & Sahassa.AML.Commonly.SessionUserId & "')))"
            'End If
            Session("QuestionaireSTRApproval.TableALL") = DataRepository.vw_MsQuestionaireSTRApprovalProvider.GetPaged(strAllWhereClause, SetnGetSort, 0, Integer.MaxValue, 0)

            Return CType(Session("QuestionaireSTRApproval.TableALL"), VList(Of vw_MsQuestionaireSTRApproval))
        End Get
        Set(ByVal value As VList(Of vw_MsQuestionaireSTRApproval))
            Session("QuestionaireSTRApproval.Table") = value
        End Set
    End Property

#End Region

    Private Sub FillSearch()
        Try
            Me.ComboSearch.Items.Add(New ListItem("...", ""))
            Me.ComboSearch.Items.Add(New ListItem("Preparer", "UserName Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Description Alert STR", "DescriptionAlertSTR Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Question No", "QuestionNo Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Mode", "Nama Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Created Date", "CreatedDate Like '%-=Search=-%'"))
        Catch
            Throw
        End Try
    End Sub

    Private Sub BindGrid()
        'SettingControlSearching()
        Me.GridView.DataSource = Me.SetnGetBindTable
        Me.GridView.CurrentPageIndex = Me.SetnGetCurrentPage
        Me.GridView.VirtualItemCount = Me.SetnGetRowTotal
        Me.GridView.DataBind()
    End Sub

    Public Sub BindGridView()
        GridView.DataSource = Me.SetnGetBindTable
        GridView.VirtualItemCount = Me.SetnGetRowTotal
        GridView.DataBind()
    End Sub

    Private Sub ClearThisPageSessions()
        SetnGetUserName = Nothing
        SetnGetDescriptionAlertSTR = Nothing
        SetnGetQuestionNo = Nothing
        SetnGetNama = Nothing
        SetnGetCreatedDate = Nothing

        SetnGetValueSearch = Nothing
        SetnGetSelectedItem = Nothing

        Me.SetnGetRowTotal = Nothing
        Me.SetnGetSort = Nothing
    End Sub

    Private Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Try
            Me.CollectSelected()
            Me.SetnGetFieldSearch = Me.ComboSearch.SelectedValue
            If Me.ComboSearch.SelectedIndex = 0 Then
                Me.TextSearch.Text = ""
                Me.SetnGetValueSearch = Me.TextSearch.Text
            Else
                Me.SetnGetValueSearch = Me.TextSearch.Text
            End If
            Me.SetnGetCurrentPage = 0
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not Me.IsPostBack Then
                Me.ClearThisPageSessions()

                Me.FillSearch()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    '        Transcope.Complete()
                End Using

                Me.popUp.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TextSearch.ClientID & "'), 'dd-mm-yyyy')")

                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub SetInfoNavigate()
        Me.PageCurrentPage.Text = (Me.SetnGetCurrentPage + 1).ToString
        Me.PageTotalPages.Text = Me.GetPageTotal.ToString
        Me.PageTotalRows.Text = Me.SetnGetRowTotal.ToString
        Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
        Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
        Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
        Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            Me.CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be a positive number greater than zero.")
                End If
            Else
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

#Region "export"
    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In Me.GridView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = CType(gridRow.FindControl("CheckBoxExporttoExcel"), CheckBox)
                Dim PKMsQuestionaireSTRID As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
                If ArrTarget.Contains(PKMsQuestionaireSTRID) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(PKMsQuestionaireSTRID)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(PKMsQuestionaireSTRID)
                End If
                Me.SetnGetSelectedItem = ArrTarget
            End If
        Next
    End Sub
    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        'For Each gridRow As DataGridItem In Me.GridView.Items
        '    If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
        '        Dim pkid As String = gridRow.Cells(1).Text
        '        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        '        If Me.SetnGetSelectedItem.Contains(pkid) Then
        '            If Me.CheckBoxSelectAll.Checked Then
        '                If Not ArrTarget.Contains(pkid) Then
        '                    ArrTarget.Add(pkid)
        '                End If
        '            Else
        '                ArrTarget.Remove(pkid)
        '            End If
        '        Else
        '            If Me.CheckBoxSelectAll.Checked Then
        '                If Not ArrTarget.Contains(pkid) Then
        '                    ArrTarget.Add(pkid)
        '                End If
        '            End If
        '        End If
        '        Me.SetnGetSelectedItem = ArrTarget
        '    End If
        'Next
        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        For Each gridRow As DataGridItem In Me.GridView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(1).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItem.Contains(StrKey) = False Then
                        ArrTarget.Add(StrKey)
                    End If
                Else
                    If Me.SetnGetSelectedItem.Contains(StrKey) Then
                        ArrTarget.Remove(StrKey)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItem = ArrTarget

    End Sub

    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub 'ClearControls

    Private Sub BindSelectedAll()
        Try
            Me.GridView.DataSource = SetnGetBindTableAll
            Me.GridView.AllowPaging = False
            Me.GridView.DataBind()

            For i As Integer = 0 To GridView.Items.Count - 1
                For y As Integer = 0 To GridView.Columns.Count - 1
                    GridView.Items(i).Cells(y).Attributes.Add("class", "text")
                Next
            Next
            Me.GridView.Columns(0).Visible = False
            Me.GridView.Columns(1).Visible = False
            Me.GridView.Columns(7).Visible = False
        Catch
            Throw
        End Try
    End Sub

    Private Sub BindSelected()
        Dim Rows As New ArrayList
        'Dim AllList As VList(Of vw_MsQuestionaireSTR) = DataRepository.vw_MsQuestionaireSTRProvider.GetPaged("", "", 0, Integer.MaxValue, 0)

        For Each IdPk As Long In Me.SetnGetSelectedItem
            'Dim oList As VList(Of vw_MsQuestionaireSTR) = AllList.FindAll(vw_MsQuestionaireSTRColumn.PK_MsQuestionaireSTR_ID, IdPk)
            Dim AllList As VList(Of vw_MsQuestionaireSTRApproval) = DataRepository.vw_MsQuestionaireSTRApprovalProvider.GetPaged("PK_MsQuestionaireSTR_ApprovalID = '" & IdPk & "'", "", 0, Integer.MaxValue, 0)
            If AllList.Count > 0 Then
                Rows.Add(AllList(0))
            End If
        Next
        Me.GridView.DataSource = Rows
        Me.GridView.AllowPaging = False
        Me.GridView.DataBind()
        For i As Integer = 0 To GridView.Items.Count - 1
            For y As Integer = 0 To GridView.Columns.Count - 1
                GridView.Items(i).Cells(y).Attributes.Add("class", "text")
            Next
        Next

        Me.GridView.Columns(0).Visible = False
        Me.GridView.Columns(1).Visible = False
        Me.GridView.Columns(7).Visible = False
    End Sub

    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Me.CollectSelected()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelected()
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=QuestionaireSTRApproval.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridView)
            GridView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub lnkExportAllData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExportAllData.Click
        Try
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            BindSelectedAll()
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=QuestionaireSTRApproval.xls")
            Response.Charset = ""
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridView)
            GridView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
#End Region

#Region "grid"

    Protected Sub GridView_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridView.ItemCommand
        Dim BActivationNeedApproval As Boolean = False
        Dim PKMsQuestionaireSTRApprovalID As Integer
        Dim strUserID As String 'uniq
        Try
            If e.CommandName.ToLower = "detail" Then
                PKMsQuestionaireSTRApprovalID = CInt(e.Item.Cells(1).Text)
                strUserID = e.Item.Cells(2).Text
                'Using ObjMsHelp As New SahassaBLL.MsHelpBLL
                'If ObjMsHelp.IsNotExistInApproval(strUserID) Then
                Response.Redirect("QuestionaireSTRApprovalDetail.aspx?PKMsQuestionaireSTRApprovalID=" & PKMsQuestionaireSTRApprovalID, False)
                ' End If
                'End Using
            End If

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridView.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Header And e.Item.ItemType <> ListItemType.Footer Then
                Dim PKMsQuestionaireSTRApprovalID As String = e.Item.Cells(1).Text
                Dim objcheckbox As CheckBox = CType(e.Item.FindControl("CheckBoxExporttoExcel"), CheckBox)
                objcheckbox.Checked = Me.SetnGetSelectedItem.Contains(PKMsQuestionaireSTRApprovalID)

                Dim DetailButton As LinkButton = CType(e.Item.Cells(7).FindControl("LnkDetail"), LinkButton)

            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridView_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridView.SortCommand
        Dim GridUser As DataGrid = CType(source, DataGrid)
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

#End Region
End Class
