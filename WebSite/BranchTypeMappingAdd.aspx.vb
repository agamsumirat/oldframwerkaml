Imports AMLDAL
Imports System.Data.SqlClient
Partial Class BranchTypeMappingAdd
    Inherits Parent
    
    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
            End Using
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
    Private Sub BindUnmapBranch()
        Try
            cbounMapBranch.Items.Clear()
            Using adapter As New AMLDAL.MappingBranchTableAdapters.JHDATATableAdapter
                Using otable As MappingBranch.JHDATADataTable = adapter.GetData
                    For Each orows As MappingBranch.JHDATARow In otable.Rows
                        cbounMapBranch.Items.Add(New ListItem(orows.JDBR & " - " & orows.JDNAME, orows.JDBR))
                    Next
                End Using
            End Using
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Sub
    Private Sub BindBranchType()
        Try
            cboBranchType.Items.Clear()
            Using adapter As New MappingBranchTableAdapters.BranchTypeTableAdapter
                cboBranchType.DataSource = adapter.GetData
                cboBranchType.DataTextField = "BranchTypeName"
                cboBranchType.DataValueField = "BranchTypeId"
                cboBranchType.DataBind()
            End Using
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            If Not IsPostBack Then
                BindUnmapBranch()
                BindBranchType()
            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
    Private Function IsDataValid() As Boolean
        Try
            'cek apakah data yang mau didelete tidak boleh ada di pending approval
            Using adapter As New AMLDAL.MappingBranchTableAdapters.BranchTypeMapping_PendingApprovalTableAdapter
                Dim jml As Integer = 0
                jml = adapter.CountByBranchTypeIDName(cbounMapBranch.SelectedItem.Text)
                If jml > 0 Then
                    cvalPageError.IsValid = False
                    cvalPageError.ErrorMessage = "Branch " & cbounMapBranch.SelectedItem.Text & " Already in waiting for approval."
                    Return False
                Else
                    Return True
                End If
            End Using

        Catch ex As Exception
            Return False
        End Try
    End Function

    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim otrans As SqlTransaction = Nothing
        Dim intPK As Long = 0
        Try
            If Page.IsValid AndAlso isDatavalid Then


                'insert header
                Using adapter As New MappingBranchTableAdapters.BranchTypeMapping_PendingApprovalTableAdapter
                    otrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(adapter)
                    intPK = adapter.InsertBranchTypeMappingPendingApproval(Sahassa.AML.Commonly.SessionUserId, Now, cbounMapBranch.SelectedItem.Text, "Add", Sahassa.AML.Commonly.TypeMode.Add)
                End Using

                'insert detail
                Using adapter As New MappingBranchTableAdapters.BranchTypeMappingApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(adapter, otrans)
                    adapter.Insert(intPK, 0, Convert.ToInt16(cboBranchType.SelectedValue), cbounMapBranch.SelectedValue, Nothing, Nothing, Nothing)
                End Using
                otrans.Commit()
                Dim MessagePendingID As Integer = 82591
                Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier= " & cbounMapBranch.SelectedItem.Text

                Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & cbounMapBranch.SelectedItem.Text, False)
            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Response.Redirect("BranchTypeMappingView.aspx", False)
    End Sub
End Class
