Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Partial Class GroupEdit
    Inherits Parent

    ''' <summary>
    ''' get user id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetGroupId() As String
        Get
            Return Me.Request.Params("GroupID")
        End Get
    End Property

    ''' <summary>
    ''' cancel handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "GroupView.aspx"

        Me.Response.Redirect("GroupView.aspx", False)
    End Sub

    ''' <summary>
    ''' insert by su
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub UpdateGroupBySU()
        Try
            Dim GroupDescription As String
            If Me.TextGroupDescription.Text.Length <= 255 Then
                GroupDescription = Me.TextGroupDescription.Text
            Else
                GroupDescription = Me.TextGroupDescription.Text.Substring(0, 255)
            End If

            Using AccessGroup As New AMLDAL.AMLDataSetTableAdapters.GroupTableAdapter
                AccessGroup.UpdateGroup(Me.GetGroupId, Me.TextGroupName.Text, GroupDescription, True)
            End Using
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' audit trail
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub InsertAuditTrail()
        Try
            Using AccessGroup As New AMLDAL.AMLDataSetTableAdapters.GroupTableAdapter
                Using dt As AMLDAL.AMLDataSet.GroupDataTable = AccessGroup.GetDataByGroupID(CInt(Me.GetGroupId))
                    Dim rowData As AMLDAL.AMLDataSet.GroupRow = dt.Rows(0)
                    Sahassa.AML.AuditTrailAlert.AuditTrailChecking(4)

                    Dim GroupDescription As String
                    If Me.TextGroupDescription.Text.Length <= 255 Then
                        GroupDescription = Me.TextGroupDescription.Text
                    Else
                        GroupDescription = Me.TextGroupDescription.Text.Substring(0, 255)
                    End If

                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Group", "GroupID", "Edit", rowData.GroupID, rowData.GroupID, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Group", "GroupName", "Edit", rowData.GroupName, Me.TextGroupName.Text, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Group", "GroupDescription", "Edit", GroupDescription, Me.TextGroupDescription.Text, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Group", "GroupCreatedDate", "Edit", rowData.GroupCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), rowData.GroupCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                    End Using
                End Using
            End Using
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' update handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageUpdate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageUpdate.Click
        Dim oSQLTrans As sqltransaction = Nothing
        Page.Validate()

        If Page.IsValid Then
            Try
                'Buat variabel untuk menampung nilai-nilai baru
                Dim GroupID As String = Me.LabelGroupID.Text
                Dim GroupName As String = Me.TextGroupName.Text


                'Jika tidak ada perubahan GroupName
                If GroupName = ViewState("GroupName_Old") Then
                    GoTo Add
                Else 'Jika ada perubahan GroupName
                    Dim counter As Int32
                    'Periksa apakah GroupName yg baru tsb sdh ada dalam tabel Group atau belum
                    Using AccessGroup As New AMLDAL.AMLDataSetTableAdapters.GroupTableAdapter
                        counter = AccessGroup.CountMatchingGroup(GroupName)
                    End Using

                    'Counter = 0 berarti GroupName tersebut belum pernah ada dalam tabel Group
                    If counter = 0 Then
Add:
                        'Periksa apakah GroupName tsb dalam status pending approval atau tidak
                        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.Group_ApprovalTableAdapter
                            oSQLTrans = BeginTransaction(AccessPending, IsolationLevel.ReadUncommitted)
                            counter = AccessPending.CountGroupApprovalByGroupName(GroupName)

                            'Counter = 0 berarti GroupName yg baru tsb tidak dalam status pending approval
                            If counter = 0 Then
                                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                                    Me.InsertAuditTrail()
                                    Me.UpdateGroupBySU()
                                    Me.LblSuccess.Text = "Success to Update Group."
                                    Me.LblSuccess.Visible = True
                                Else
                                    Dim GroupDescription As String
                                    If Me.TextGroupDescription.Text.Length <= 255 Then
                                        GroupDescription = Me.TextGroupDescription.Text
                                    Else
                                        GroupDescription = Me.TextGroupDescription.Text.Substring(0, 255)
                                    End If

                                    'Buat variabel untuk menampung nilai-nilai lama
                                    Dim GroupID_Old As Int32 = ViewState("GroupID_Old")
                                    Dim GroupName_Old As String = ViewState("GroupName_Old")
                                    Dim GroupDescription_Old As String = ViewState("GroupTypeDescription_Old")
                                    Dim GroupCreatedDate_Old As DateTime = ViewState("GroupCreatedDate_Old")
                                    Dim isCreatedBySU_Old As Boolean = ViewState("isCreatedBySU_Old")

                                    'variabel untuk menampung nilai identity dari tabel GroupPendingApprovalID
                                    Dim GroupPendingApprovalID As Int64

                                    'Using TransScope As New Transactions.TransactionScope()
                                    Using AccessGroupPendingApproval As New AMLDAL.AMLDataSetTableAdapters.Group_PendingApprovalTableAdapter
                                        SetTransaction(AccessGroupPendingApproval, oSQLTrans)
                                        'Tambahkan ke dalam tabel Group_PendingApproval dengan ModeID = 2 (Edit) 
                                        GroupPendingApprovalID = AccessGroupPendingApproval.InsertGroup_PendingApproval(Sahassa.AML.Commonly.SessionUserId, Now, "Group Edit", 2)

                                        'Tambahkan ke dalam tabel Group_Approval dengan ModeID = 2 (Edit) 
                                        AccessPending.Insert(GroupPendingApprovalID, 2, GroupID, GroupName, GroupDescription, Now, GroupID_Old, GroupName_Old, GroupDescription_Old, GroupCreatedDate_Old, isCreatedBySU_Old, isCreatedBySU_Old)

                                        'TransScope.Complete()
                                        oSQLTrans.Commit()
                                    End Using
                                    'End Using

                                    Dim MessagePendingID As Integer = 8202 'MessagePendingID 8202 = Group Edit 

                                    Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & GroupName_Old

                                    Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & GroupName_Old, False)
                                End If
                            Else  'Counter != 0 berarti GroupName tersebut dalam status pending approval
                                Throw New Exception("Cannot edit the following Group: '" & GroupName & "' because it is currently waiting for approval")
                            End If
                        End Using
                    Else 'Counter != 0 berarti GroupName tersebut sudah ada dalam tabel Group
                        Throw New Exception("Cannot change to the following Group Name: '" & GroupName & "' because that Group Name already exists in the database.")
                    End If
                End If
            Catch ex As Exception
                If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
                Me.cvalPageError.IsValid = False
                Me.cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            Finally
                If Not oSQLTrans Is Nothing Then
                    oSQLTrans.Dispose()
                    oSQLTrans = Nothing
                End If
            End Try
        End If
    End Sub

    ''' <summary>
    ''' filledit data
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillEditData()
        Try
            Using AccessGroup As New AMLDAL.AMLDataSetTableAdapters.GroupTableAdapter
                Using TableGroup As AMLDAL.AMLDataSet.GroupDataTable = AccessGroup.GetDataByGroupID(Me.GetGroupId)
                    If TableGroup.Rows.Count > 0 Then
                        Dim TableRowGroup As AMLDAL.AMLDataSet.GroupRow = TableGroup.Rows(0)

                        ViewState("GroupID_Old") = TableRowGroup.GroupID
                        Me.LabelGroupID.Text = ViewState("GroupID_Old")

                        ViewState("GroupName_Old") = TableRowGroup.GroupName
                        Me.TextGroupName.Text = ViewState("GroupName_Old")

                        ViewState("GroupTypeDescription_Old") = TableRowGroup.GroupDescription
                        Me.TextGroupDescription.Text = ViewState("GroupTypeDescription_Old")

                        ViewState("GroupCreatedDate_Old") = TableRowGroup.GroupCreatedDate
                        ViewState("isCreatedBySU_Old") = TableRowGroup.isCreatedBySU
                    End If
                End Using
            End Using
        Catch
            Throw
        End Try

    End Sub


    ''' <summary>
    ''' page load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.FillEditData()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    '    Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class