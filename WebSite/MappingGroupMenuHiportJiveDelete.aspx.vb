Option Strict On
Option Explicit On
Imports amlbll
Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports SahassaNettier.Services
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper


Imports System.IO
Imports System.Collections.Generic

Partial Class MappingGroupMenuHiportJiveDelete
    Inherits Parent

    Public ReadOnly Property PK_MappingGroupMenuHIPORTJIVE_ID() As Integer
        Get
            Dim strTemp As String = Request.Params("PK_MappingGroupMenuHIPORTJIVE_ID")
            Dim intResult As Integer
            If Integer.TryParse(strTemp, intResult) Then
                Return intResult
            Else
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = "Parameter PK_MappingGroupMenuHIPORTJIVE_ID is invalid."

            End If
        End Get
    End Property

    Public ReadOnly Property Objvw_MappingGroupMenuHIPORTJIVE() As VList(Of vw_MappingGroupMenuHiportJive)
        Get
            If Session("Objvw_MappingGroupMenuHIPORTJIVE.MappingGroupMenuHIPORTJIVEDelete") Is Nothing Then

                Session("Objvw_MappingGroupMenuHIPORTJIVE.MappingGroupMenuHIPORTJIVEDelete") = DataRepository.vw_MappingGroupMenuHiportJiveProvider.GetPaged("PK_MappingGroupMenuHIPORTJIVE_ID = '" & PK_MappingGroupMenuHIPORTJIVE_ID & "'", "", 0, Integer.MaxValue, 0)

            End If
            Return CType(Session("Objvw_MappingGroupMenuHIPORTJIVE.MappingGroupMenuHIPORTJIVEDelete"), VList(Of vw_MappingGroupMenuHiportJive))
        End Get
    End Property


    Private Sub loaddata()

        If Objvw_MappingGroupMenuHIPORTJIVE.Count > 0 Then
            TxtDelete.Text = Objvw_MappingGroupMenuHIPORTJIVE(0).GroupName

        Else
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = MappingGroupMenuHiportJiveBLL.DATA_NOTVALID
        End If
    End Sub

    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim oTrans As SqlTransaction = Nothing
        Try
            Using ObjList As MappingGroupMenuHiportJive = DataRepository.MappingGroupMenuHiportJiveProvider.GetByPK_MappingGroupMenuHiportJive_ID(PK_MappingGroupMenuHIPORTJIVE_ID)
                If ObjList Is Nothing Then
                    Throw New AMLBLL.SahassaException(MappingGroupMenuHiportJiveEnum.DATA_NOTVALID)
                End If
            End Using

            Dim groupID As Integer = 0
            Dim groupname As String = ""

            Using Objlist As TList(Of MappingGroupMenuHiportJive) = DataRepository.MappingGroupMenuHiportJiveProvider.GetPaged("PK_MappingGroupMenuHIPORTJIVE_ID = '" & PK_MappingGroupMenuHIPORTJIVE_ID & "'", "", 0, Integer.MaxValue, 0)
                If Objlist.Count > 0 Then

                    AMLBLL.MappingGroupMenuHiportJiveBLL.IsDataValidDeleteApproval(CStr(PK_MappingGroupMenuHIPORTJIVE_ID))

                    If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                        Using Objvw_MappingGroupMenuHIPORTJIVE As VList(Of vw_MappingGroupMenuHiportJive) = DataRepository.vw_MappingGroupMenuHiportJiveProvider.GetPaged("PK_MappingGroupMenuHIPORTJIVE_ID = '" & PK_MappingGroupMenuHIPORTJIVE_ID & "'", "", 0, Integer.MaxValue, 0)
                            Me.LblSuccess.Text = "Data deleted successfully"
                            Me.LblSuccess.Visible = True
                            AMLBLL.MappingGroupMenuHiportJiveBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "MappingGroupMenuHIPORTJIVE", "GroupName", "Delete", "", Objvw_MappingGroupMenuHIPORTJIVE(0).GroupName, "Acc")
                            Using ObjMappingGroupMenuHIPORTJIVE As TList(Of MappingGroupMenuHiportJive) = DataRepository.MappingGroupMenuHiportJiveProvider.GetPaged("PK_MappingGroupMenuHIPORTJIVE_ID = '" & PK_MappingGroupMenuHIPORTJIVE_ID & "'", "", 0, Integer.MaxValue, 0)
                                groupID = CInt(ObjMappingGroupMenuHIPORTJIVE(0).FK_Group_ID)

                                DataRepository.MappingGroupMenuHiportJiveProvider.Delete(ObjMappingGroupMenuHIPORTJIVE)
                            End Using
                        End Using
                    Else

                        Using ObjMappingGroupMenuHIPORTJIVE_Approval As New MappingGroupMenuHiportJive_Approval
                            Using ObjMappingGroupMenuHIPORTJIVEList As VList(Of vw_MappingGroupMenuHiportJive) = DataRepository.vw_MappingGroupMenuHiportJiveProvider.GetPaged("PK_MappingGroupMenuHIPORTJIVE_ID = '" & PK_MappingGroupMenuHIPORTJIVE_ID & "'", "", 0, Integer.MaxValue, 0)
                                If ObjMappingGroupMenuHIPORTJIVEList.Count > 0 Then

                                    groupname = ObjMappingGroupMenuHIPORTJIVEList(0).GroupName
                                    groupID = CInt(ObjMappingGroupMenuHIPORTJIVEList(0).FK_Group_ID)
                                    ObjMappingGroupMenuHIPORTJIVE_Approval.FK_MsUserID = Sahassa.AML.Commonly.SessionPkUserId
                                    ObjMappingGroupMenuHIPORTJIVE_Approval.GroupMenu = groupname
                                    ObjMappingGroupMenuHIPORTJIVE_Approval.FK_ModeID = CInt(Sahassa.AML.Commonly.TypeMode.Delete)
                                    ObjMappingGroupMenuHIPORTJIVE_Approval.CreatedDate = Now
                                    AMLBLL.MappingGroupMenuHiportJiveBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "MappingGroupMenuHIPORTJIVE", "GroupName", "Delete", "", groupname, "PendingApproval")
                                    DataRepository.MappingGroupMenuHiportJive_ApprovalProvider.Save(ObjMappingGroupMenuHIPORTJIVE_Approval)

                                    Using ObjMappingGroupMenuHIPORTJIVE_approvalDetail As New MappingGroupMenuHiportJive_ApprovalDetail
                                        ObjMappingGroupMenuHIPORTJIVE_approvalDetail.FK_MappingGroupMenuHiportJive_Approval_ID = ObjMappingGroupMenuHIPORTJIVE_Approval.PK_MappingGroupMenuHiportJive_Approval_ID
                                        ObjMappingGroupMenuHIPORTJIVE_approvalDetail.FK_Group_ID = groupID
                                        ObjMappingGroupMenuHIPORTJIVE_approvalDetail.PK_MappingGroupMenuHiportJive_ID = PK_MappingGroupMenuHIPORTJIVE_ID
                                        DataRepository.MappingGroupMenuHiportJive_ApprovalDetailProvider.Save(ObjMappingGroupMenuHIPORTJIVE_approvalDetail)
                                    End Using

                                    'Sahassa.AML.Commonly.SessionIntendedPage = "MappingGroupMenuHIPORTJIVEView.aspx"
                                    'Me.Response.Redirect("MappingGroupMenuHIPORTJIVEView.aspx", False)
                                    Using ObjGroup As TList(Of Group) = DataRepository.GroupProvider.GetPaged("GroupID = '" & groupID & "'", "", 0, Integer.MaxValue, 0)
                                        Me.LblSuccess.Text = "Data deleted successfully and waiting for Approval."
                                        Me.LblSuccess.Visible = True
                                    End Using
                                End If
                            End Using
                        End Using
                    End If
                    MultiView1.ActiveViewIndex = 1
                Else
                    Me.LblSuccess.Text = "Data already deleted"
                    Me.LblSuccess.Visible = True
                End If
            End Using
        Catch ex As Exception
            If Not oTrans Is Nothing Then oTrans.Rollback()
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not oTrans Is Nothing Then
                oTrans.Dispose()
                oTrans = Nothing
            End If
        End Try
    End Sub



    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "MappingGroupMenuHIPORTJIVEView.aspx"
            Me.Response.Redirect("MappingGroupMenuHIPORTJIVEView.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'Using Transcope As New Transactions.TransactionScope
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                '    Transcope.Complete()
            End Using
            Session("Objvw_MappingGroupMenuHIPORTJIVE.MappingGroupMenuHIPORTJIVEDelete") = Nothing

            If Not Page.IsPostBack Then
                loaddata()
                MultiView1.ActiveViewIndex = 0
            End If
            'End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub btnOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnOK.Click
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "MappingGroupMenuHIPORTJIVEView.aspx"
            Me.Response.Redirect("MappingGroupMenuHIPORTJIVEView.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class

