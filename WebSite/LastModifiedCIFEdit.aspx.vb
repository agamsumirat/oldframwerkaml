Imports SahassaNettier.Entities
Imports AMLBLL
Partial Class LastModifiedCIFEdit
    Inherits Parent



    Public Property ObjLastModifiedCIF() As LastModifiedCIF
        Get
            If Session("LastModifiedCIFEdit.ObjLastModifiedCIF") Is Nothing Then




                Using objTLastModifiedCIF As TList(Of LastModifiedCIF) = LastModifiedCIFBLL.GetTlistLastModifiedCIF(LastModifiedCIFColumn.CIF.ToString & "='" & Me.PKCIFNo & "'", "", 0, Integer.MaxValue, 0)
                    If objTLastModifiedCIF.Count > 0 Then
                        Session("LastModifiedCIFEdit.ObjLastModifiedCIF") = objTLastModifiedCIF(0)
                    End If

                End Using


                Return CType(Session("LastModifiedCIFEdit.ObjLastModifiedCIF"), LastModifiedCIF)
            Else
                Return CType(Session("LastModifiedCIFEdit.ObjLastModifiedCIF"), LastModifiedCIF)
            End If
        End Get
        Set(ByVal value As LastModifiedCIF)
            Session("LastModifiedCIFEdit.ObjLastModifiedCIF") = value
        End Set
    End Property



    Public ReadOnly Property dataupdatingid() As String
        Get
            Return Request.Params("dataupdatingid")
        End Get
    End Property
    Public ReadOnly Property PKCIFNo() As String
        Get
            Return Request.Params("CIFNo")
        End Get
    End Property

    Sub ClearSession()
        ObjLastModifiedCIF = Nothing
    End Sub

    Sub LoadData()
        If Not ObjLastModifiedCIF Is Nothing Then
            LblCIF.Text = ObjLastModifiedCIF.CIF
            LblName.Text = ObjLastModifiedCIF.CustomerName
            TxtlastModified.Text = ObjLastModifiedCIF.LastModified.GetValueOrDefault(Sahassa.AML.Commonly.GetDefaultDate).ToString("dd-MMM-yyyy").Replace("01-Jan-1900", "")
            Dim objds As Data.DataSet = EKABLL.CDDBLL.getdataupdatingandtglopen(Me.dataupdatingid)
            If Not objds Is Nothing Then
                If objds.Tables.Count > 0 Then
                    If objds.Tables(0).Rows.Count > 0 Then
                        LblDataupdatingReason.Text = objds.Tables(0).Rows(0).Item("DataUpdatingReason")
                        LblDateOpenCIF.Text = CDate(objds.Tables(0).Rows(0).Item("TglBukacif")).ToString("dd-MM-yyyy")
                    End If
                End If
            End If

            DataListMandatory.DataSource = EKABLL.CDDBLL.GetMandatoryError(ObjLastModifiedCIF.CIF)
            DataListMandatory.DataBind()

            GrdInvalid.DataSource = EKABLL.CDDBLL.GetInvalidError(ObjLastModifiedCIF.CIF)
            GrdInvalid.DataBind()
        End If
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                clearSession()
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Me.popUp.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TxtlastModified.ClientID & "'), 'dd-mmm-yyyy')")
                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    '        Transcope.Complete()
                End Using
                LoadData()
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Function IsDataValid() As Boolean
        Dim strErrorMessage As New StringBuilder
        If Not Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", TxtlastModified.Text) Then
            strErrorMessage.Append("Last Modified Must dd-MMM-yyyy.")
        End If
        If strErrorMessage.ToString.Trim.Length > 0 Then
            Throw New Exception(strErrorMessage.ToString)
        Else
            Return True
        End If

    End Function

    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Try

            If IsDataValid() Then
                ObjLastModifiedCIF.LastModified = Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", TxtlastModified.Text)
                ObjLastModifiedCIF.LastModifiedBy = Sahassa.AML.Commonly.SessionUserId
                ObjLastModifiedCIF.LastUpdateDate = Now
                If LastModifiedCIFBLL.SaveCIF(ObjLastModifiedCIF) Then
                    If Request.Params("sumber") <> "" Then
                        Response.Redirect("DataUpdatingView.aspx", False)
                    Else
                        Response.Redirect("LastModifiedCIFView.aspx", False)
                    End If

                End If

            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Try
            If Request.Params("sumber") <> "" Then
                Response.Redirect("DataUpdatingView.aspx", False)
            Else
                Response.Redirect("LastModifiedCIFView.aspx", False)
            End If

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub LblCif_Click(sender As Object, e As System.EventArgs) Handles LblCif.Click
        Try
            Response.Redirect("CustomerInformationDetail.aspx?CIFNo=" & Me.PKCIFNo)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


End Class
