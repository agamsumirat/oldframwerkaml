﻿Imports CDDLNPNettier.Data
Imports CDDLNPNettier.Entities
Imports AMLBLL
Imports System.Data

Partial Class CDDRiskFactQuestionDelete
    Inherits Parent

    Private ReadOnly Property GetPK_CDDRiskFactQuestion_ID() As Int32
        Get
            Return Request.QueryString("ID")
        End Get
    End Property

    Private Sub LoadData()
        Using objCDDQuestion As VList(Of vw_CDD_RiskFactQuestion) = DataRepository.vw_CDD_RiskFactQuestionProvider.GetPaged("PK_CDD_RiskFactQuestion_ID=" & GetPK_CDDRiskFactQuestion_ID, String.Empty, 0, 1, 0)
            If objCDDQuestion.Count > 0 Then
                Me.lblQuestion.Text = objCDDQuestion(0).Question.ToString
                Me.lblRiskFactName.Text = objCDDQuestion(0).RiskFactName
                Me.GridViewCDDQuestionDetail.DataSource = DataRepository.CDD_RiskFactQuestionDetailProvider.GetPaged("FK_CDD_RiskFactQuestion_ID = " & GetPK_CDDRiskFactQuestion_ID, String.Empty, 0, Integer.MaxValue, 0)
                Me.GridViewCDDQuestionDetail.DataBind()
            End If
        End Using
    End Sub

    Protected Sub CvalPageErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles cvalPageError.PreRender
        If cvalPageError.IsValid = False Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
        End If
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not Page.IsPostBack Then
                LoadData()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
            End If
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub

    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim objTransManager As New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)
        Dim objApproval As New CDD_RiskFactQuestion_Approval
        Dim objCDDRiskFactQuestion As New CDD_RiskFactQuestion_ApprovalDetail
        Dim objCDDRiskFactQuestionDetail As New CDD_RiskFactQuestionDetail_ApprovalDetail

        Try
            objTransManager.BeginTransaction()

            If DataRepository.CDD_RiskFactQuestion_ApprovalDetailProvider.GetTotalItems("PK_CDD_RiskFactQuestion_ID = " & Me.GetPK_CDDRiskFactQuestion_ID, 0) > 0 Then
                Throw New Exception("Question masih dalam pending approval")
            End If

            objApproval.RequestedBy = Sahassa.AML.Commonly.SessionPkUserId
            objApproval.RequestedDate = Date.Now
            objApproval.FK_MsMode_Id = 3
            objApproval.IsUpload = False
            DataRepository.CDD_RiskFactQuestion_ApprovalProvider.Save(objTransManager, objApproval)


            objCDDRiskFactQuestion.FK_CDD_RiskFactQuestion_Approval_Id = objApproval.PK_CDD_RiskFactQuestion_Approval_Id
            'objCDDRiskFactQuestion.Question = lblQuestion.Text.Trim
            'objCDDRiskFactQuestion.FK_Risk_Fact_ID = hfFK_Risk_Fact_ID.Value
            objCDDRiskFactQuestion.PK_CDD_RiskFactQuestion_ID = GetPK_CDDRiskFactQuestion_ID
            DataRepository.CDD_RiskFactQuestion_ApprovalDetailProvider.Save(objTransManager, objCDDRiskFactQuestion) 'Save Question

            objTransManager.Commit()

            Me.Response.Redirect("CDDLNPMessage.aspx?ID=10431", False)
        Catch ex As Exception
            objTransManager.Rollback()
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        Finally
            If Not objTransManager Is Nothing Then
                objTransManager.Dispose()
                objTransManager = Nothing
            End If
            If Not objCDDRiskFactQuestion Is Nothing Then
                objCDDRiskFactQuestion.Dispose()
                objCDDRiskFactQuestion = Nothing
            End If
            If Not objCDDRiskFactQuestionDetail Is Nothing Then
                objCDDRiskFactQuestionDetail.Dispose()
                objCDDRiskFactQuestionDetail = Nothing
            End If
        End Try
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Me.Response.Redirect("CDDRiskFactQuestionView.aspx")
    End Sub
End Class '369 - 102 / 147 - 103