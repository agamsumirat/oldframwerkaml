<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="CashTransactionReportPrint.aspx.vb" Inherits="CashTransactionReportPrint" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
    <rsweb:ReportViewer ID="reportViewer_content" runat="server" width="100%" height="800px" Font-Names="Verdana" Font-Size="8pt">
    <LocalReport ReportPath="CTR.rdlc" EnableExternalImages="True">
        <DataSources>
            <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="ReportCTRSTR_CFMAST" />
            <rsweb:ReportDataSource DataSourceId="ObjectDataSource2" Name="ReportCTRSTR_NPWP" />
            <rsweb:ReportDataSource DataSourceId="ObjectDataSource3" Name="ReportCTRSTR_CFADDR" />
            <rsweb:ReportDataSource DataSourceId="ObjectDataSource4" Name="ReportCTRSTR_CTRAccount" />
            <rsweb:ReportDataSource DataSourceId="ObjectDataSource5" Name="ReportCTRSTR_CTRTransaction" />
            <rsweb:ReportDataSource DataSourceId="ObjectDataSource6" Name="ReportCTRSTR_CTRAccountOwner" />
            <rsweb:ReportDataSource DataSourceId="ObjectDataSource7" Name="ReportCTRSTR_CTRJointAccount" />
            <rsweb:ReportDataSource DataSourceId="ObjectDataSource8" Name="ReportCTRSTR_KTP" />
            <rsweb:ReportDataSource DataSourceId="ObjectDataSource9" Name="ReportCTRSTR_PASPOR" />
            <rsweb:ReportDataSource DataSourceId="ObjectDataSource10" Name="ReportCTRSTR_KIMKITAS" />
            <rsweb:ReportDataSource DataSourceId="ObjectDataSource11" Name="ReportCTRSTR_KARTUPELAJAR" />
            <rsweb:ReportDataSource DataSourceId="ObjectDataSource12" Name="ReportCTRSTR_SIM" />
            <rsweb:ReportDataSource DataSourceId="ObjectDataSource13" Name="ReportCTRSTR_SIUP" />
            <rsweb:ReportDataSource DataSourceId="ObjectDataSource14" Name="ReportCTRSTR_CTRTotalKasMasukRp" />
            <rsweb:ReportDataSource DataSourceId="ObjectDataSource15" Name="ReportCTRSTR_CTRTotalKasKeluarRp" />
            <rsweb:ReportDataSource DataSourceId="ObjectDataSource16" Name="ReportCTRSTR_CTRTotalKasKeluarValutaAsing" />
            <rsweb:ReportDataSource DataSourceId="ObjectDataSource17" Name="ReportCTRSTR_CTRTotalKasMasukValutaAsing" />
            <rsweb:ReportDataSource DataSourceId="ObjectDataSource18" Name="ReportCTRSTR_CTRTotalKasMasuk" />
            <rsweb:ReportDataSource DataSourceId="ObjectDataSource19" Name="ReportCTRSTR_CTRTotalKasKeluar" />
            <rsweb:ReportDataSource DataSourceId="ObjectDataSource20" Name="ReportCTRSTR_CTRTransactionCurrencyKasMasuk_GetComma" />
            <rsweb:ReportDataSource DataSourceId="ObjectDataSource21" Name="ReportCTRSTR_CTRTransactionCurrencyKasKeluar_GetComma" />
        </DataSources>

        </LocalReport>
    </rsweb:ReportViewer>
    <asp:ObjectDataSource ID="ObjectDataSource21" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetData" TypeName="AMLDAL.ReportCTRSTRTableAdapters.CTRTransactionCurrencyKasKeluar_GetCommaTableAdapter">
        <SelectParameters>
            <asp:QueryStringParameter Name="FK_CTRID" QueryStringField="PK_CTRID" Type="Int64" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource20" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetData" TypeName="AMLDAL.ReportCTRSTRTableAdapters.CTRTransactionCurrencyKasMasuk_GetCommaTableAdapter">
        <SelectParameters>
            <asp:QueryStringParameter Name="FK_CTRID" QueryStringField="PK_CTRID" Type="Int64" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource19" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetData" TypeName="AMLDAL.ReportCTRSTRTableAdapters.CTRTotalKasKeluarTableAdapter">
        <SelectParameters>
            <asp:QueryStringParameter Name="FK_CTRID" QueryStringField="PK_CTRID" Type="Int64" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource18" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetData" TypeName="AMLDAL.ReportCTRSTRTableAdapters.CTRTotalKasMasukTableAdapter">
        <SelectParameters>
            <asp:QueryStringParameter Name="FK_CTRID" QueryStringField="PK_CTRID" Type="Int64" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource17" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetData" TypeName="AMLDAL.ReportCTRSTRTableAdapters.CTRTotalKasMasukValutaAsingTableAdapter">
        <SelectParameters>
            <asp:QueryStringParameter Name="FK_CTRID" QueryStringField="PK_CTRID" Type="Int64" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource16" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetData" TypeName="AMLDAL.ReportCTRSTRTableAdapters.CTRTotalKasKeluarValutaAsingTableAdapter">
        <SelectParameters>
            <asp:QueryStringParameter Name="FK_CTRID" QueryStringField="PK_CTRID" Type="Int64" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource15" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetData" TypeName="AMLDAL.ReportCTRSTRTableAdapters.CTRTotalKasKeluarRpTableAdapter">
        <SelectParameters>
            <asp:QueryStringParameter Name="FK_CTRID" QueryStringField="PK_CTRID" Type="Int64" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource14" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetData" TypeName="AMLDAL.ReportCTRSTRTableAdapters.CTRTotalKasMasukRpTableAdapter">
        <SelectParameters>
            <asp:QueryStringParameter Name="FK_CTRID" QueryStringField="PK_CTRID" Type="Int64" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource13" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetData" TypeName="AMLDAL.ReportCTRSTRTableAdapters.SIUPTableAdapter">
        <SelectParameters>
            <asp:QueryStringParameter Name="CFCIF" QueryStringField="CIFNo" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource12" runat="server" InsertMethod="Insert"
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="AMLDAL.ReportCTRSTRTableAdapters.SIMTableAdapter">
        <SelectParameters>
            <asp:QueryStringParameter Name="_CFCIF_" QueryStringField="CIFNo" Type="String" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="CFSSNO" Type="String" />
        </InsertParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource11" runat="server" InsertMethod="Insert"
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="AMLDAL.ReportCTRSTRTableAdapters.KARTUPELAJARTableAdapter">
        <SelectParameters>
            <asp:QueryStringParameter Name="_CFCIF_" QueryStringField="CIFNo" Type="String" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="CFSSNO" Type="String" />
        </InsertParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource10" runat="server" InsertMethod="Insert"
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="AMLDAL.ReportCTRSTRTableAdapters.KIMKITASTableAdapter">
        <SelectParameters>
            <asp:QueryStringParameter Name="_CFCIF_" QueryStringField="CIFNo" Type="String" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="CFSSNO" Type="String" />
        </InsertParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource9" runat="server" InsertMethod="Insert"
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="AMLDAL.ReportCTRSTRTableAdapters.PASPORTableAdapter">
        <SelectParameters>
            <asp:QueryStringParameter Name="_CFCIF_" QueryStringField="CIFNo" Type="String" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="CFSSNO" Type="String" />
        </InsertParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource8" runat="server" InsertMethod="Insert"
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="AMLDAL.ReportCTRSTRTableAdapters.KTPTableAdapter">
        <SelectParameters>
            <asp:QueryStringParameter Name="_CFCIF_" QueryStringField="CIFNo" Type="String" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="CFSSNO" Type="String" />
        </InsertParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource7" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetData" TypeName="AMLDAL.ReportCTRSTRTableAdapters.CTRJointAccountTableAdapter">
        <SelectParameters>
            <asp:QueryStringParameter Name="FK_CTRID" QueryStringField="PK_CTRID" Type="Int64" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource6" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetData" TypeName="AMLDAL.ReportCTRSTRTableAdapters.CTRAccountOwnerTableAdapter">
        <SelectParameters>
            <asp:QueryStringParameter Name="PK_CTRID" QueryStringField="PK_CTRID" Type="Int64" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource5" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetData" TypeName="AMLDAL.ReportCTRSTRTableAdapters.CTRTransactionTableAdapter">
        <SelectParameters>
            <asp:QueryStringParameter Name="FK_CTRID" QueryStringField="PK_CTRID" Type="Int64" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource4" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetData" TypeName="AMLDAL.ReportCTRSTRTableAdapters.CTRAccountTableAdapter">
        <SelectParameters>
            <asp:QueryStringParameter Name="FK_CTRID" QueryStringField="PK_CTRID" Type="Int64" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource3" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetData" TypeName="AMLDAL.ReportCTRSTRTableAdapters.CFADDRTableAdapter">
        <SelectParameters>
            <asp:QueryStringParameter Name="_CFCIF_" QueryStringField="CIFNo" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetData" TypeName="AMLDAL.ReportCTRSTRTableAdapters.NPWPTableAdapter">
        <SelectParameters>
            <asp:QueryStringParameter Name="_CFCIF_" QueryStringField="CIFNo" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetData" TypeName="AMLDAL.ReportCTRSTRTableAdapters.CFMASTTableAdapter">
        <SelectParameters>
            <asp:QueryStringParameter Name="_CFCIF_" QueryStringField="CIFNo" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
