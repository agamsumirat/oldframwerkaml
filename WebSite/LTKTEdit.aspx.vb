#Region "Imports..."

Imports Sahassanettier.Data
Imports Sahassanettier.Entities
Imports System.Data
Imports System.Collections.Generic
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports AMLBLL.DataType
#End Region

Partial Class LTKTEdit
    Inherits Parent

#Region "properties..."

    ReadOnly Property getLTKTPK() As Integer
        Get
            If Session("LTKTEdit.LTKTPK") = Nothing Then
                Session("LTKTEdit.LTKTPK") = (Request.Params("LTKT_ID"))
            End If
            Return Session("LTKTEdit.LTKTPK")
        End Get
    End Property

    Public Property SetnGetResumeKasMasukKasKeluar() As List(Of ResumeKasMasukKeluarLTKTAdd)
        Get
            If Session("LTKTEdit.ResumeKasMasukKasKeluar") Is Nothing Then
                Session("LTKTEdit.ResumeKasMasukKasKeluar") = New List(Of ResumeKasMasukKeluarLTKTAdd)
            End If
            Return Session("LTKTEdit.ResumeKasMasukKasKeluar")
        End Get
        Set(ByVal value As List(Of ResumeKasMasukKeluarLTKTAdd))
            Session("LTKTEdit.ResumeKasMasukKasKeluar") = value
        End Set
    End Property

    Public Property SetnGetgrvTRXKMDetilValutaAsing() As List(Of LTKTDetailCashInEdit)
        Get
            If Session("LTKTEdit.grvTRXKMDetilValutaAsingDATA") Is Nothing Then
                Session("LTKTEdit.grvTRXKMDetilValutaAsingDATA") = New List(Of LTKTDetailCashInEdit)
            End If
            Return Session("LTKTEdit.grvTRXKMDetilValutaAsingDATA")
        End Get
        Set(ByVal value As List(Of LTKTDetailCashInEdit))
            Session("LTKTEdit.grvTRXKMDetilValutaAsing") = value
        End Set
    End Property

    Public Property SetnGetgrvDetilKasKeluar() As List(Of LTKTDetailCashInEdit)
        Get
            If Session("LTKTEdit.grvDetilKasKeluarDATA") Is Nothing Then
                Session("LTKTEdit.grvDetilKasKeluarDATA") = New List(Of LTKTDetailCashInEdit)
            End If
            Return Session("LTKTEdit.grvDetilKasKeluarDATA")
        End Get
        Set(ByVal value As List(Of LTKTDetailCashInEdit))
            Session("LTKTEdit.grvDetilKasKeluarDATA") = value
        End Set
    End Property

    Public Property SetnGetRowEdit() As Integer
        Get
            If Session("LTKTEdit.RowEdit") Is Nothing Then
                Session("LTKTEdit.RowEdit") = -1
            End If
            Return Session("LTKTEdit.RowEdit")
        End Get
        Set(ByVal value As Integer)
            Session("LTKTEdit.RowEdit") = value
        End Set
    End Property


#End Region

#Region "Function..."
    Private Enum AddressLevel
        Kelurahan = 1
        Kecamatan
        KabupatenKota
        Propinsi
        Negara
    End Enum

    Private Sub FillNextLevelAlamatIndividuDomisili(enumAddress As AddressLevel)
        Select Case enumAddress
            Case AddressLevel.Kelurahan
                Using ObjMsKelurahan As MsKelurahan = DataRepository.MsKelurahanProvider.GetByIDKelurahan(hfTerlaporDOMKelurahan.Value)
                    If Not ObjMsKelurahan Is Nothing Then
                        hfTerlaporDOMKecamatan.Value = ObjMsKelurahan.IDKecamatan.GetValueOrDefault()
                        Using ObjMsKecamatan As MsKecamatan = DataRepository.MsKecamatanProvider.GetByIDKecamatan(hfTerlaporDOMKecamatan.Value)
                            If Not ObjMsKecamatan Is Nothing Then
                                txtTerlaporDOMKecamatan.Text = ObjMsKecamatan.NamaKecamatan
                                hfTerlaporDOMKotaKab.Value = ObjMsKecamatan.IDKotaKabupaten.GetValueOrDefault()
                                Using ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(hfTerlaporDOMKotaKab.Value)
                                    If Not ObjMsKotaKab Is Nothing Then
                                        txtTerlaporDOMKotaKab.Text = ObjMsKotaKab.NamaKotaKab
                                        hfTerlaporDOMProvinsi.Value = ObjMsKotaKab.IDPropinsi.GetValueOrDefault()
                                        Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfTerlaporDOMProvinsi.Value)
                                            If Not ObjMsProvince Is Nothing Then
                                                txtTerlaporDOMProvinsi.Text = ObjMsProvince.Nama
                                            End If
                                        End Using
                                    End If
                                End Using
                            End If
                        End Using

                    End If
                End Using
            Case AddressLevel.Kecamatan
                Using ObjMsKecamatan As MsKecamatan = DataRepository.MsKecamatanProvider.GetByIDKecamatan(hfTerlaporDOMKecamatan.Value)
                    If Not ObjMsKecamatan Is Nothing Then
                        txtTerlaporDOMKecamatan.Text = ObjMsKecamatan.NamaKecamatan
                        hfTerlaporDOMKotaKab.Value = ObjMsKecamatan.IDKotaKabupaten.GetValueOrDefault()
                        Using ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(hfTerlaporDOMKotaKab.Value)
                            If Not ObjMsKotaKab Is Nothing Then
                                txtTerlaporDOMKotaKab.Text = ObjMsKotaKab.NamaKotaKab
                                hfTerlaporDOMProvinsi.Value = ObjMsKotaKab.IDPropinsi.GetValueOrDefault()
                                Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfTerlaporDOMProvinsi.Value)
                                    If Not ObjMsProvince Is Nothing Then
                                        txtTerlaporDOMProvinsi.Text = ObjMsProvince.Nama
                                    End If
                                End Using
                            End If
                        End Using
                    End If
                End Using
            Case AddressLevel.KabupatenKota
                Using ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(hfTerlaporDOMKotaKab.Value)
                    If Not ObjMsKotaKab Is Nothing Then
                        txtTerlaporDOMKotaKab.Text = ObjMsKotaKab.NamaKotaKab
                        hfTerlaporDOMProvinsi.Value = ObjMsKotaKab.IDPropinsi.GetValueOrDefault()
                        Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfTerlaporDOMProvinsi.Value)
                            If Not ObjMsProvince Is Nothing Then
                                txtTerlaporDOMProvinsi.Text = ObjMsProvince.Nama
                            End If
                        End Using
                    End If
                End Using
        End Select
    End Sub

    Private Sub FillNextLevelAlamatIndividuIdentitas(enumAddress As AddressLevel)
        Select Case enumAddress
            Case AddressLevel.Kelurahan
                Using ObjMsKelurahan As MsKelurahan = DataRepository.MsKelurahanProvider.GetByIDKelurahan(hfTerlaporIDKelurahan.Value)
                    If Not ObjMsKelurahan Is Nothing Then
                        hfTerlaporIDKecamatan.Value = ObjMsKelurahan.IDKecamatan.GetValueOrDefault()
                        Using ObjMsKecamatan As MsKecamatan = DataRepository.MsKecamatanProvider.GetByIDKecamatan(hfTerlaporIDKecamatan.Value)
                            If Not ObjMsKecamatan Is Nothing Then
                                txtTerlaporIDKecamatan.Text = ObjMsKecamatan.NamaKecamatan
                                hfTerlaporIDKotaKab.Value = ObjMsKecamatan.IDKotaKabupaten.GetValueOrDefault()
                                Using ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(hfTerlaporIDKotaKab.Value)
                                    If Not ObjMsKotaKab Is Nothing Then
                                        txtTerlaporIDKotaKab.Text = ObjMsKotaKab.NamaKotaKab
                                        hfTerlaporIDProvinsi.Value = ObjMsKotaKab.IDPropinsi.GetValueOrDefault()
                                        Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfTerlaporIDProvinsi.Value)
                                            If Not ObjMsProvince Is Nothing Then
                                                txtTerlaporIDProvinsi.Text = ObjMsProvince.Nama
                                            End If
                                        End Using
                                    End If
                                End Using
                            End If
                        End Using

                    End If
                End Using
            Case AddressLevel.Kecamatan
                Using ObjMsKecamatan As MsKecamatan = DataRepository.MsKecamatanProvider.GetByIDKecamatan(hfTerlaporIDKecamatan.Value)
                    If Not ObjMsKecamatan Is Nothing Then
                        txtTerlaporIDKecamatan.Text = ObjMsKecamatan.NamaKecamatan
                        hfTerlaporIDKotaKab.Value = ObjMsKecamatan.IDKotaKabupaten.GetValueOrDefault()
                        Using ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(hfTerlaporIDKotaKab.Value)
                            If Not ObjMsKotaKab Is Nothing Then
                                txtTerlaporIDKotaKab.Text = ObjMsKotaKab.NamaKotaKab
                                hfTerlaporIDProvinsi.Value = ObjMsKotaKab.IDPropinsi.GetValueOrDefault()
                                Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfTerlaporIDProvinsi.Value)
                                    If Not ObjMsProvince Is Nothing Then
                                        txtTerlaporIDProvinsi.Text = ObjMsProvince.Nama
                                    End If
                                End Using
                            End If
                        End Using
                    End If
                End Using
            Case AddressLevel.KabupatenKota
                Using ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(hfTerlaporIDKotaKab.Value)
                    If Not ObjMsKotaKab Is Nothing Then
                        txtTerlaporIDKotaKab.Text = ObjMsKotaKab.NamaKotaKab
                        hfTerlaporIDProvinsi.Value = ObjMsKotaKab.IDPropinsi.GetValueOrDefault()
                        Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfTerlaporIDProvinsi.Value)
                            If Not ObjMsProvince Is Nothing Then
                                txtTerlaporIDProvinsi.Text = ObjMsProvince.Nama
                            End If
                        End Using
                    End If
                End Using
        End Select
    End Sub

    Private Sub FillNextLevelAlamatKorporasiDalamNegeri(enumAddress As AddressLevel)
        Select Case enumAddress
            Case AddressLevel.Kelurahan
                Using ObjMsKelurahan As MsKelurahan = DataRepository.MsKelurahanProvider.GetByIDKelurahan(hfTerlaporCORPDLKelurahan.Value)
                    If Not ObjMsKelurahan Is Nothing Then
                        hfTerlaporCORPDLKecamatan.Value = ObjMsKelurahan.IDKecamatan.GetValueOrDefault()
                        Using ObjMsKecamatan As MsKecamatan = DataRepository.MsKecamatanProvider.GetByIDKecamatan(hfTerlaporCORPDLKecamatan.Value)
                            If Not ObjMsKecamatan Is Nothing Then
                                txtTerlaporCORPDLKecamatan.Text = ObjMsKecamatan.NamaKecamatan
                                hfTerlaporCORPDLKotaKab.Value = ObjMsKecamatan.IDKotaKabupaten.GetValueOrDefault()
                                Using ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(hfTerlaporCORPDLKotaKab.Value)
                                    If Not ObjMsKotaKab Is Nothing Then
                                        txtTerlaporCORPDLKotaKab.Text = ObjMsKotaKab.NamaKotaKab
                                        hfTerlaporCORPDLProvinsi.Value = ObjMsKotaKab.IDPropinsi.GetValueOrDefault()
                                        Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfTerlaporCORPDLProvinsi.Value)
                                            If Not ObjMsProvince Is Nothing Then
                                                txtTerlaporCORPDLProvinsi.Text = ObjMsProvince.Nama
                                                If ObjMsProvince.Code <> "" Then
                                                    hfTerlaporCORPDLNegara.Value = ObjMsProvince.Code
                                                    Using ObjMsNegara As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(hfTerlaporCORPDLNegara.Value)
                                                        If Not ObjMsNegara Is Nothing Then
                                                            txtTerlaporCORPDLNegara.Text = ObjMsNegara.NamaNegara
                                                        End If
                                                    End Using
                                                End If
                                            End If
                                        End Using
                                    End If
                                End Using
                            End If
                        End Using

                    End If
                End Using
            Case AddressLevel.Kecamatan
                Using ObjMsKecamatan As MsKecamatan = DataRepository.MsKecamatanProvider.GetByIDKecamatan(hfTerlaporCORPDLKecamatan.Value)
                    If Not ObjMsKecamatan Is Nothing Then
                        txtTerlaporCORPDLKecamatan.Text = ObjMsKecamatan.NamaKecamatan
                        hfTerlaporCORPDLKotaKab.Value = ObjMsKecamatan.IDKotaKabupaten.GetValueOrDefault()
                        Using ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(hfTerlaporCORPDLKotaKab.Value)
                            If Not ObjMsKotaKab Is Nothing Then
                                txtTerlaporCORPDLKotaKab.Text = ObjMsKotaKab.NamaKotaKab
                                hfTerlaporCORPDLProvinsi.Value = ObjMsKotaKab.IDPropinsi.GetValueOrDefault()
                                Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfTerlaporCORPDLProvinsi.Value)
                                    If Not ObjMsProvince Is Nothing Then
                                        txtTerlaporCORPDLProvinsi.Text = ObjMsProvince.Nama
                                        If ObjMsProvince.Code <> "" Then
                                            hfTerlaporCORPDLNegara.Value = ObjMsProvince.Code
                                            Using ObjMsNegara As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(hfTerlaporCORPDLNegara.Value)
                                                If Not ObjMsNegara Is Nothing Then
                                                    txtTerlaporCORPDLNegara.Text = ObjMsNegara.NamaNegara
                                                End If
                                            End Using
                                        End If
                                    End If
                                End Using
                            End If
                        End Using
                    End If
                End Using
            Case AddressLevel.KabupatenKota
                Using ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(hfTerlaporCORPDLKotaKab.Value)
                    If Not ObjMsKotaKab Is Nothing Then
                        txtTerlaporCORPDLKotaKab.Text = ObjMsKotaKab.NamaKotaKab
                        hfTerlaporCORPDLProvinsi.Value = ObjMsKotaKab.IDPropinsi.GetValueOrDefault()
                        Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfTerlaporCORPDLProvinsi.Value)
                            If Not ObjMsProvince Is Nothing Then
                                txtTerlaporCORPDLProvinsi.Text = ObjMsProvince.Nama
                                If ObjMsProvince.Code <> "" Then
                                    hfTerlaporCORPDLNegara.Value = ObjMsProvince.Code
                                    Using ObjMsNegara As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(hfTerlaporCORPDLNegara.Value)
                                        If Not ObjMsNegara Is Nothing Then
                                            txtTerlaporCORPDLNegara.Text = ObjMsNegara.NamaNegara
                                        End If
                                    End Using
                                End If
                            End If
                        End Using
                    End If
                End Using
            Case AddressLevel.Propinsi
                Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfTerlaporCORPDLProvinsi.Value)
                    If Not ObjMsProvince Is Nothing Then
                        txtTerlaporCORPDLProvinsi.Text = ObjMsProvince.Nama
                        If ObjMsProvince.Code <> "" Then
                            hfTerlaporCORPDLNegara.Value = ObjMsProvince.Code
                            Using ObjMsNegara As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(hfTerlaporCORPDLNegara.Value)
                                If Not ObjMsNegara Is Nothing Then
                                    txtTerlaporCORPDLNegara.Text = ObjMsNegara.NamaNegara
                                End If
                            End Using
                        End If
                    End If
                End Using
        End Select
    End Sub

    Private Sub FillNextLevelAlamatKasMasukPJKTempatTerjadinyaTransaksi(enumAddress As AddressLevel)
        Select Case enumAddress
            Case AddressLevel.KabupatenKota
                Using ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(hfTRXKMKotaKab.Value)
                    If Not ObjMsKotaKab Is Nothing Then
                        txtTRXKMKotaKab.Text = ObjMsKotaKab.NamaKotaKab
                        hfTRXKMProvinsi.Value = ObjMsKotaKab.IDPropinsi.GetValueOrDefault()
                        Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfTRXKMProvinsi.Value)
                            If Not ObjMsProvince Is Nothing Then
                                txtTRXKMProvinsi.Text = ObjMsProvince.Nama
                            End If
                        End Using
                    End If
                End Using
        End Select
    End Sub

    Private Sub FillNextLevelAlamatKasMasukIndividuDomisili(enumAddress As AddressLevel)
        Select Case enumAddress
            Case AddressLevel.Kelurahan
                Using ObjMsKelurahan As MsKelurahan = DataRepository.MsKelurahanProvider.GetByIDKelurahan(hfTRXKMINDVDOMKelurahan.Value)
                    If Not ObjMsKelurahan Is Nothing Then
                        hfTRXKMINDVDOMKecamatan.Value = ObjMsKelurahan.IDKecamatan.GetValueOrDefault()
                        Using ObjMsKecamatan As MsKecamatan = DataRepository.MsKecamatanProvider.GetByIDKecamatan(hfTRXKMINDVDOMKecamatan.Value)
                            If Not ObjMsKecamatan Is Nothing Then
                                txtTRXKMINDVDOMKecamatan.Text = ObjMsKecamatan.NamaKecamatan
                                hfTRXKMINDVDOMKotaKab.Value = ObjMsKecamatan.IDKotaKabupaten.GetValueOrDefault()
                                Using ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(hfTRXKMINDVDOMKotaKab.Value)
                                    If Not ObjMsKotaKab Is Nothing Then
                                        txtTRXKMINDVDOMKotaKab.Text = ObjMsKotaKab.NamaKotaKab
                                        hfTRXKMINDVDOMProvinsi.Value = ObjMsKotaKab.IDPropinsi.GetValueOrDefault()
                                        Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfTRXKMINDVDOMProvinsi.Value)
                                            If Not ObjMsProvince Is Nothing Then
                                                txtTRXKMINDVDOMProvinsi.Text = ObjMsProvince.Nama
                                            End If
                                        End Using
                                    End If
                                End Using
                            End If
                        End Using

                    End If
                End Using
            Case AddressLevel.Kecamatan
                Using ObjMsKecamatan As MsKecamatan = DataRepository.MsKecamatanProvider.GetByIDKecamatan(hfTRXKMINDVDOMKecamatan.Value)
                    If Not ObjMsKecamatan Is Nothing Then
                        txtTRXKMINDVDOMKecamatan.Text = ObjMsKecamatan.NamaKecamatan
                        hfTRXKMINDVDOMKotaKab.Value = ObjMsKecamatan.IDKotaKabupaten.GetValueOrDefault()
                        Using ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(hfTRXKMINDVDOMKotaKab.Value)
                            If Not ObjMsKotaKab Is Nothing Then
                                txtTRXKMINDVDOMKotaKab.Text = ObjMsKotaKab.NamaKotaKab
                                hfTRXKMINDVDOMProvinsi.Value = ObjMsKotaKab.IDPropinsi.GetValueOrDefault()
                                Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfTRXKMINDVDOMProvinsi.Value)
                                    If Not ObjMsProvince Is Nothing Then
                                        txtTRXKMINDVDOMProvinsi.Text = ObjMsProvince.Nama
                                    End If
                                End Using
                            End If
                        End Using
                    End If
                End Using
            Case AddressLevel.KabupatenKota
                Using ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(hfTRXKMINDVDOMKotaKab.Value)
                    If Not ObjMsKotaKab Is Nothing Then
                        txtTRXKMINDVDOMKotaKab.Text = ObjMsKotaKab.NamaKotaKab
                        hfTRXKMINDVDOMProvinsi.Value = ObjMsKotaKab.IDPropinsi.GetValueOrDefault()
                        Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfTRXKMINDVDOMProvinsi.Value)
                            If Not ObjMsProvince Is Nothing Then
                                txtTRXKMINDVDOMProvinsi.Text = ObjMsProvince.Nama
                            End If
                        End Using
                    End If
                End Using
        End Select
    End Sub

    Private Sub FillNextLevelAlamatKasMasukIndividuIdentitas(enumAddress As AddressLevel)
        Select Case enumAddress
            Case AddressLevel.Kelurahan
                Using ObjMsKelurahan As MsKelurahan = DataRepository.MsKelurahanProvider.GetByIDKelurahan(hfTRXKMINDVIDKelurahan.Value)
                    If Not ObjMsKelurahan Is Nothing Then
                        hfTRXKMINDVIDKecamatan.Value = ObjMsKelurahan.IDKecamatan.GetValueOrDefault()
                        Using ObjMsKecamatan As MsKecamatan = DataRepository.MsKecamatanProvider.GetByIDKecamatan(hfTRXKMINDVIDKecamatan.Value)
                            If Not ObjMsKecamatan Is Nothing Then
                                txtTRXKMINDVIDKecamatan.Text = ObjMsKecamatan.NamaKecamatan
                                hfTRXKMINDVIDKotaKab.Value = ObjMsKecamatan.IDKotaKabupaten.GetValueOrDefault()
                                Using ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(hfTRXKMINDVIDKotaKab.Value)
                                    If Not ObjMsKotaKab Is Nothing Then
                                        txtTRXKMINDVIDKotaKab.Text = ObjMsKotaKab.NamaKotaKab
                                        hfTRXKMINDVIDProvinsi.Value = ObjMsKotaKab.IDPropinsi.GetValueOrDefault()
                                        Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfTRXKMINDVIDProvinsi.Value)
                                            If Not ObjMsProvince Is Nothing Then
                                                txtTRXKMINDVIDProvinsi.Text = ObjMsProvince.Nama
                                            End If
                                        End Using
                                    End If
                                End Using
                            End If
                        End Using

                    End If
                End Using
            Case AddressLevel.Kecamatan
                Using ObjMsKecamatan As MsKecamatan = DataRepository.MsKecamatanProvider.GetByIDKecamatan(hfTRXKMINDVIDKecamatan.Value)
                    If Not ObjMsKecamatan Is Nothing Then
                        txtTRXKMINDVIDKecamatan.Text = ObjMsKecamatan.NamaKecamatan
                        hfTRXKMINDVIDKotaKab.Value = ObjMsKecamatan.IDKotaKabupaten.GetValueOrDefault()
                        Using ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(hfTRXKMINDVIDKotaKab.Value)
                            If Not ObjMsKotaKab Is Nothing Then
                                txtTRXKMINDVIDKotaKab.Text = ObjMsKotaKab.NamaKotaKab
                                hfTRXKMINDVIDProvinsi.Value = ObjMsKotaKab.IDPropinsi.GetValueOrDefault()
                                Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfTRXKMINDVIDProvinsi.Value)
                                    If Not ObjMsProvince Is Nothing Then
                                        txtTRXKMINDVIDProvinsi.Text = ObjMsProvince.Nama
                                    End If
                                End Using
                            End If
                        End Using
                    End If
                End Using
            Case AddressLevel.KabupatenKota
                Using ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(hfTRXKMINDVIDKotaKab.Value)
                    If Not ObjMsKotaKab Is Nothing Then
                        txtTRXKMINDVIDKotaKab.Text = ObjMsKotaKab.NamaKotaKab
                        hfTRXKMINDVIDProvinsi.Value = ObjMsKotaKab.IDPropinsi.GetValueOrDefault()
                        Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfTRXKMINDVIDProvinsi.Value)
                            If Not ObjMsProvince Is Nothing Then
                                txtTRXKMINDVIDProvinsi.Text = ObjMsProvince.Nama
                            End If
                        End Using
                    End If
                End Using
        End Select
    End Sub

    Private Sub FillNextLevelAlamatKasMasukKorporasiDalamNegeri(enumAddress As AddressLevel)
        Select Case enumAddress
            Case AddressLevel.Kelurahan
                Using ObjMsKelurahan As MsKelurahan = DataRepository.MsKelurahanProvider.GetByIDKelurahan(hfTRXKMCORPDLKelurahan.Value)
                    If Not ObjMsKelurahan Is Nothing Then
                        hfTRXKMCORPDLKecamatan.Value = ObjMsKelurahan.IDKecamatan.GetValueOrDefault()
                        Using ObjMsKecamatan As MsKecamatan = DataRepository.MsKecamatanProvider.GetByIDKecamatan(hfTRXKMCORPDLKecamatan.Value)
                            If Not ObjMsKecamatan Is Nothing Then
                                txtTRXKMCORPDLKecamatan.Text = ObjMsKecamatan.NamaKecamatan
                                hfTRXKMCORPDLKotaKab.Value = ObjMsKecamatan.IDKotaKabupaten.GetValueOrDefault()
                                Using ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(hfTRXKMCORPDLKotaKab.Value)
                                    If Not ObjMsKotaKab Is Nothing Then
                                        txtTRXKMCORPDLKotaKab.Text = ObjMsKotaKab.NamaKotaKab
                                        hfTRXKMCORPDLProvinsi.Value = ObjMsKotaKab.IDPropinsi.GetValueOrDefault()
                                        Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfTRXKMCORPDLProvinsi.Value)
                                            If Not ObjMsProvince Is Nothing Then
                                                txtTRXKMCORPDLProvinsi.Text = ObjMsProvince.Nama
                                                If ObjMsProvince.Code <> "" Then
                                                    hfTRXKMCORPDLNegara.Value = ObjMsProvince.Code
                                                    Using ObjMsNegara As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(hfTRXKMCORPDLNegara.Value)
                                                        If Not ObjMsNegara Is Nothing Then
                                                            txtTRXKMCORPDLNegara.Text = ObjMsNegara.NamaNegara
                                                        End If
                                                    End Using
                                                End If
                                            End If
                                        End Using
                                    End If
                                End Using
                            End If
                        End Using

                    End If
                End Using
            Case AddressLevel.Kecamatan
                Using ObjMsKecamatan As MsKecamatan = DataRepository.MsKecamatanProvider.GetByIDKecamatan(hfTRXKMCORPDLKecamatan.Value)
                    If Not ObjMsKecamatan Is Nothing Then
                        txtTRXKMCORPDLKecamatan.Text = ObjMsKecamatan.NamaKecamatan
                        hfTRXKMCORPDLKotaKab.Value = ObjMsKecamatan.IDKotaKabupaten.GetValueOrDefault()
                        Using ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(hfTRXKMCORPDLKotaKab.Value)
                            If Not ObjMsKotaKab Is Nothing Then
                                txtTRXKMCORPDLKotaKab.Text = ObjMsKotaKab.NamaKotaKab
                                hfTRXKMCORPDLProvinsi.Value = ObjMsKotaKab.IDPropinsi.GetValueOrDefault()
                                Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfTRXKMCORPDLProvinsi.Value)
                                    If Not ObjMsProvince Is Nothing Then
                                        txtTRXKMCORPDLProvinsi.Text = ObjMsProvince.Nama
                                        If ObjMsProvince.Code <> "" Then
                                            hfTRXKMCORPDLNegara.Value = ObjMsProvince.Code
                                            Using ObjMsNegara As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(hfTRXKMCORPDLNegara.Value)
                                                If Not ObjMsNegara Is Nothing Then
                                                    txtTRXKMCORPDLNegara.Text = ObjMsNegara.NamaNegara
                                                End If
                                            End Using
                                        End If
                                    End If
                                End Using
                            End If
                        End Using
                    End If
                End Using
            Case AddressLevel.KabupatenKota
                Using ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(hfTRXKMCORPDLKotaKab.Value)
                    If Not ObjMsKotaKab Is Nothing Then
                        txtTRXKMCORPDLKotaKab.Text = ObjMsKotaKab.NamaKotaKab
                        hfTRXKMCORPDLProvinsi.Value = ObjMsKotaKab.IDPropinsi.GetValueOrDefault()
                        Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfTRXKMCORPDLProvinsi.Value)
                            If Not ObjMsProvince Is Nothing Then
                                txtTRXKMCORPDLProvinsi.Text = ObjMsProvince.Nama
                                If ObjMsProvince.Code <> "" Then
                                    hfTRXKMCORPDLNegara.Value = ObjMsProvince.Code
                                    Using ObjMsNegara As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(hfTRXKMCORPDLNegara.Value)
                                        If Not ObjMsNegara Is Nothing Then
                                            txtTRXKMCORPDLNegara.Text = ObjMsNegara.NamaNegara
                                        End If
                                    End Using
                                End If
                            End If
                        End Using
                    End If
                End Using
            Case AddressLevel.Propinsi
                Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfTRXKMCORPDLProvinsi.Value)
                    If Not ObjMsProvince Is Nothing Then
                        txtTRXKMCORPDLProvinsi.Text = ObjMsProvince.Nama
                        If ObjMsProvince.Code <> "" Then
                            hfTRXKMCORPDLNegara.Value = ObjMsProvince.Code
                            Using ObjMsNegara As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(hfTRXKMCORPDLNegara.Value)
                                If Not ObjMsNegara Is Nothing Then
                                    txtTRXKMCORPDLNegara.Text = ObjMsNegara.NamaNegara
                                End If
                            End Using
                        End If
                    End If
                End Using
        End Select
    End Sub

    Private Sub FillNextLevelAlamatKasKeluarPJKTempatTerjadinyaTransaksi(enumAddress As AddressLevel)
        Select Case enumAddress
            Case AddressLevel.KabupatenKota
                Using ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(hfTRXKKKotaKab.Value)
                    If Not ObjMsKotaKab Is Nothing Then
                        txtTRXKKKotaKab.Text = ObjMsKotaKab.NamaKotaKab
                        hfTRXKKProvinsi.Value = ObjMsKotaKab.IDPropinsi.GetValueOrDefault()
                        Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfTRXKKProvinsi.Value)
                            If Not ObjMsProvince Is Nothing Then
                                txtTRXKKProvinsi.Text = ObjMsProvince.Nama
                            End If
                        End Using
                    End If
                End Using
        End Select
    End Sub

    Private Sub FillNextLevelAlamatKasKeluarIndividuDomisili(enumAddress As AddressLevel)
        Select Case enumAddress
            Case AddressLevel.Kelurahan
                Using ObjMsKelurahan As MsKelurahan = DataRepository.MsKelurahanProvider.GetByIDKelurahan(hfTRXKKINDVDOMKelurahan.Value)
                    If Not ObjMsKelurahan Is Nothing Then
                        hfTRXKKINDVDOMKecamatan.Value = ObjMsKelurahan.IDKecamatan.GetValueOrDefault()
                        Using ObjMsKecamatan As MsKecamatan = DataRepository.MsKecamatanProvider.GetByIDKecamatan(hfTRXKKINDVDOMKecamatan.Value)
                            If Not ObjMsKecamatan Is Nothing Then
                                txtTRXKKINDVDOMKecamatan.Text = ObjMsKecamatan.NamaKecamatan
                                hfTRXKKINDVDOMKotaKab.Value = ObjMsKecamatan.IDKotaKabupaten.GetValueOrDefault()
                                Using ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(hfTRXKKINDVDOMKotaKab.Value)
                                    If Not ObjMsKotaKab Is Nothing Then
                                        txtTRXKKINDVDOMKotaKab.Text = ObjMsKotaKab.NamaKotaKab
                                        hfTRXKKINDVDOMProvinsi.Value = ObjMsKotaKab.IDPropinsi.GetValueOrDefault()
                                        Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfTRXKKINDVDOMProvinsi.Value)
                                            If Not ObjMsProvince Is Nothing Then
                                                txtTRXKKINDVDOMProvinsi.Text = ObjMsProvince.Nama
                                            End If
                                        End Using
                                    End If
                                End Using
                            End If
                        End Using

                    End If
                End Using
            Case AddressLevel.Kecamatan
                Using ObjMsKecamatan As MsKecamatan = DataRepository.MsKecamatanProvider.GetByIDKecamatan(hfTRXKKINDVDOMKecamatan.Value)
                    If Not ObjMsKecamatan Is Nothing Then
                        txtTRXKKINDVDOMKecamatan.Text = ObjMsKecamatan.NamaKecamatan
                        hfTRXKKINDVDOMKotaKab.Value = ObjMsKecamatan.IDKotaKabupaten.GetValueOrDefault()
                        Using ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(hfTRXKKINDVDOMKotaKab.Value)
                            If Not ObjMsKotaKab Is Nothing Then
                                txtTRXKKINDVDOMKotaKab.Text = ObjMsKotaKab.NamaKotaKab
                                hfTRXKKINDVDOMProvinsi.Value = ObjMsKotaKab.IDPropinsi.GetValueOrDefault()
                                Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfTRXKKINDVDOMProvinsi.Value)
                                    If Not ObjMsProvince Is Nothing Then
                                        txtTRXKKINDVDOMProvinsi.Text = ObjMsProvince.Nama
                                    End If
                                End Using
                            End If
                        End Using
                    End If
                End Using
            Case AddressLevel.KabupatenKota
                Using ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(hfTRXKKINDVDOMKotaKab.Value)
                    If Not ObjMsKotaKab Is Nothing Then
                        txtTRXKKINDVDOMKotaKab.Text = ObjMsKotaKab.NamaKotaKab
                        hfTRXKKINDVDOMProvinsi.Value = ObjMsKotaKab.IDPropinsi.GetValueOrDefault()
                        Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfTRXKKINDVDOMProvinsi.Value)
                            If Not ObjMsProvince Is Nothing Then
                                txtTRXKKINDVDOMProvinsi.Text = ObjMsProvince.Nama
                            End If
                        End Using
                    End If
                End Using
        End Select
    End Sub

    Private Sub FillNextLevelAlamatKasKeluarIndividuIdentitas(enumAddress As AddressLevel)
        Select Case enumAddress
            Case AddressLevel.Kelurahan
                Using ObjMsKelurahan As MsKelurahan = DataRepository.MsKelurahanProvider.GetByIDKelurahan(hfTRXKKINDVIDKelurahan.Value)
                    If Not ObjMsKelurahan Is Nothing Then
                        hfTRXKKINDVIDKecamatan.Value = ObjMsKelurahan.IDKecamatan.GetValueOrDefault()
                        Using ObjMsKecamatan As MsKecamatan = DataRepository.MsKecamatanProvider.GetByIDKecamatan(hfTRXKKINDVIDKecamatan.Value)
                            If Not ObjMsKecamatan Is Nothing Then
                                txtTRXKKINDVIDKecamatan.Text = ObjMsKecamatan.NamaKecamatan
                                hfTRXKKINDVIDKotaKab.Value = ObjMsKecamatan.IDKotaKabupaten.GetValueOrDefault()
                                Using ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(hfTRXKKINDVIDKotaKab.Value)
                                    If Not ObjMsKotaKab Is Nothing Then
                                        txtTRXKKINDVIDKotaKab.Text = ObjMsKotaKab.NamaKotaKab
                                        hfTRXKKINDVIDProvinsi.Value = ObjMsKotaKab.IDPropinsi.GetValueOrDefault()
                                        Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfTRXKKINDVIDProvinsi.Value)
                                            If Not ObjMsProvince Is Nothing Then
                                                txtTRXKKINDVIDProvinsi.Text = ObjMsProvince.Nama
                                            End If
                                        End Using
                                    End If
                                End Using
                            End If
                        End Using

                    End If
                End Using
            Case AddressLevel.Kecamatan
                Using ObjMsKecamatan As MsKecamatan = DataRepository.MsKecamatanProvider.GetByIDKecamatan(hfTRXKKINDVIDKecamatan.Value)
                    If Not ObjMsKecamatan Is Nothing Then
                        txtTRXKKINDVIDKecamatan.Text = ObjMsKecamatan.NamaKecamatan
                        hfTRXKKINDVIDKotaKab.Value = ObjMsKecamatan.IDKotaKabupaten.GetValueOrDefault()
                        Using ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(hfTRXKKINDVIDKotaKab.Value)
                            If Not ObjMsKotaKab Is Nothing Then
                                txtTRXKKINDVIDKotaKab.Text = ObjMsKotaKab.NamaKotaKab
                                hfTRXKKINDVIDProvinsi.Value = ObjMsKotaKab.IDPropinsi.GetValueOrDefault()
                                Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfTRXKKINDVIDProvinsi.Value)
                                    If Not ObjMsProvince Is Nothing Then
                                        txtTRXKKINDVIDProvinsi.Text = ObjMsProvince.Nama
                                    End If
                                End Using
                            End If
                        End Using
                    End If
                End Using
            Case AddressLevel.KabupatenKota
                Using ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(hfTRXKKINDVIDKotaKab.Value)
                    If Not ObjMsKotaKab Is Nothing Then
                        txtTRXKKINDVIDKotaKab.Text = ObjMsKotaKab.NamaKotaKab
                        hfTRXKKINDVIDProvinsi.Value = ObjMsKotaKab.IDPropinsi.GetValueOrDefault()
                        Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfTRXKKINDVIDProvinsi.Value)
                            If Not ObjMsProvince Is Nothing Then
                                txtTRXKKINDVIDProvinsi.Text = ObjMsProvince.Nama
                            End If
                        End Using
                    End If
                End Using
        End Select
    End Sub

    Private Sub FillNextLevelAlamatKasKeluarKorporasiDalamNegeri(enumAddress As AddressLevel)
        Select Case enumAddress
            Case AddressLevel.Kelurahan
                Using ObjMsKelurahan As MsKelurahan = DataRepository.MsKelurahanProvider.GetByIDKelurahan(hfTRXKKCORPDLKelurahan.Value)
                    If Not ObjMsKelurahan Is Nothing Then
                        hfTRXKKCORPDLKecamatan.Value = ObjMsKelurahan.IDKecamatan.GetValueOrDefault()
                        Using ObjMsKecamatan As MsKecamatan = DataRepository.MsKecamatanProvider.GetByIDKecamatan(hfTRXKKCORPDLKecamatan.Value)
                            If Not ObjMsKecamatan Is Nothing Then
                                txtTRXKKCORPDLKecamatan.Text = ObjMsKecamatan.NamaKecamatan
                                hfTRXKKCORPDLKotaKab.Value = ObjMsKecamatan.IDKotaKabupaten.GetValueOrDefault()
                                Using ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(hfTRXKKCORPDLKotaKab.Value)
                                    If Not ObjMsKotaKab Is Nothing Then
                                        txtTRXKKCORPDLKotaKab.Text = ObjMsKotaKab.NamaKotaKab
                                        hfTRXKKCORPDLProvinsi.Value = ObjMsKotaKab.IDPropinsi.GetValueOrDefault()
                                        Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfTRXKKCORPDLProvinsi.Value)
                                            If Not ObjMsProvince Is Nothing Then
                                                txtTRXKKCORPDLProvinsi.Text = ObjMsProvince.Nama
                                                If ObjMsProvince.Code <> "" Then
                                                    hfTRXKKCORPDLNegara.Value = ObjMsProvince.Code
                                                    Using ObjMsNegara As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(hfTRXKKCORPDLNegara.Value)
                                                        If Not ObjMsNegara Is Nothing Then
                                                            txtTRXKKCORPDLNegara.Text = ObjMsNegara.NamaNegara
                                                        End If
                                                    End Using
                                                End If
                                            End If
                                        End Using
                                    End If
                                End Using
                            End If
                        End Using

                    End If
                End Using
            Case AddressLevel.Kecamatan
                Using ObjMsKecamatan As MsKecamatan = DataRepository.MsKecamatanProvider.GetByIDKecamatan(hfTRXKKCORPDLKecamatan.Value)
                    If Not ObjMsKecamatan Is Nothing Then
                        txtTRXKKCORPDLKecamatan.Text = ObjMsKecamatan.NamaKecamatan
                        hfTRXKKCORPDLKotaKab.Value = ObjMsKecamatan.IDKotaKabupaten.GetValueOrDefault()
                        Using ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(hfTRXKKCORPDLKotaKab.Value)
                            If Not ObjMsKotaKab Is Nothing Then
                                txtTRXKKCORPDLKotaKab.Text = ObjMsKotaKab.NamaKotaKab
                                hfTRXKKCORPDLProvinsi.Value = ObjMsKotaKab.IDPropinsi.GetValueOrDefault()
                                Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfTRXKKCORPDLProvinsi.Value)
                                    If Not ObjMsProvince Is Nothing Then
                                        txtTRXKKCORPDLProvinsi.Text = ObjMsProvince.Nama
                                        If ObjMsProvince.Code <> "" Then
                                            hfTRXKKCORPDLNegara.Value = ObjMsProvince.Code
                                            Using ObjMsNegara As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(hfTRXKKCORPDLNegara.Value)
                                                If Not ObjMsNegara Is Nothing Then
                                                    txtTRXKKCORPDLNegara.Text = ObjMsNegara.NamaNegara
                                                End If
                                            End Using
                                        End If
                                    End If
                                End Using
                            End If
                        End Using
                    End If
                End Using
            Case AddressLevel.KabupatenKota
                Using ObjMsKotaKab As MsKotaKab = DataRepository.MsKotaKabProvider.GetByIDKotaKab(hfTRXKKCORPDLKotaKab.Value)
                    If Not ObjMsKotaKab Is Nothing Then
                        txtTRXKKCORPDLKotaKab.Text = ObjMsKotaKab.NamaKotaKab
                        hfTRXKKCORPDLProvinsi.Value = ObjMsKotaKab.IDPropinsi.GetValueOrDefault()
                        Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfTRXKKCORPDLProvinsi.Value)
                            If Not ObjMsProvince Is Nothing Then
                                txtTRXKKCORPDLProvinsi.Text = ObjMsProvince.Nama
                                If ObjMsProvince.Code <> "" Then
                                    hfTRXKKCORPDLNegara.Value = ObjMsProvince.Code
                                    Using ObjMsNegara As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(hfTRXKKCORPDLNegara.Value)
                                        If Not ObjMsNegara Is Nothing Then
                                            txtTRXKKCORPDLNegara.Text = ObjMsNegara.NamaNegara
                                        End If
                                    End Using
                                End If
                            End If
                        End Using
                    End If
                End Using
            Case AddressLevel.Propinsi
                Using ObjMsProvince As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(hfTRXKKCORPDLProvinsi.Value)
                    If Not ObjMsProvince Is Nothing Then
                        txtTRXKKCORPDLProvinsi.Text = ObjMsProvince.Nama
                        If ObjMsProvince.Code <> "" Then
                            hfTRXKKCORPDLNegara.Value = ObjMsProvince.Code
                            Using ObjMsNegara As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(hfTRXKKCORPDLNegara.Value)
                                If Not ObjMsNegara Is Nothing Then
                                    txtTRXKKCORPDLNegara.Text = ObjMsNegara.NamaNegara
                                End If
                            End Using
                        End If
                    End If
                End Using
        End Select
    End Sub

    Function isValidAddKas() As Boolean
        Try

            If (lblDetilKasKeluarJumlahRp.Text = "0" OrElse lblDetilKasKeluarJumlahRp.Text = "") AndAlso (lblTRXKMDetilValutaAsingJumlahRp.Text = "0" OrElse lblTRXKMDetilValutaAsingJumlahRp.Text = "") Then
                Throw New Exception("Kas Masuk or Kas Keluar must be filled")

            End If


            If lblTRXKMDetilValutaAsingJumlahRp.Text <> vbNullString OrElse lblTRXKMDetilValutaAsingJumlahRp.Text <> "" Then
                If txtTRXKMTanggalTrx.Text = vbNullString OrElse txtTRXKMTanggalTrx.Text = "" Then
                    Throw New Exception("Tanggal Transaksi (Kas Masuk) must be filled")

                End If

                If txtTRXKMTanggalTrx.Text > Now Then
                    Throw New Exception("Tanggal Transaksi (Kas Masuk) should be not greater than today")
                End If

                If txtTRXKMNamaKantor.Text = vbNullString OrElse txtTRXKMNamaKantor.Text = "" Then
                    Throw New Exception("Nama Kantor (Kas Masuk) must be filled")

                End If
                If txtTRXKMKotaKab.Text = vbNullString OrElse txtTRXKMKotaKab.Text = "" Then
                    Throw New Exception("Kota Kabupaten (Kas Masuk) must be filled")

                End If
                If txtTRXKMProvinsi.Text = vbNullString OrElse txtTRXKMProvinsi.Text = "" Then
                    Throw New Exception("Provinsi (Kas Masuk) must be filled")
                End If

                If rblTRXKMTipePelapor.SelectedValue = "" Then
                    Throw New Exception("Tipe pihak (Kas Masuk) terkait must be filled")
                End If

                'INDV
                If rblTRXKMTipePelapor.SelectedValue = "1" Then
                    If txtTRXKMINDVTanggalLahir.Text <> "" Then
                        If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", txtTRXKMINDVTanggalLahir.Text) = False Then
                            Throw New Exception("Tanggal Lahir (Kas Masuk) is not valid")

                        End If
                    End If

                    If rblTRXKMINDVKewarganegaraan.SelectedIndex = -1 Then
                        Throw New Exception("Kewarganegaraan (Kas Masuk) must be choosen")
                    End If

                    If txtTRXKMINDVNomorID.Text <> "" Then
                        If Not Regex.Match(txtTRXKMINDVNomorID.Text.Trim(), "^[A-Za-z0-9]*$").Success Then
                            Throw New Exception("Nomor Identitas (Kas Masuk) should not contains any special character")
                        End If
                    End If

                    If txtTRXKMINDVNPWP.Text <> "" Then
                        If ValidateBLL.isContainSpecialCharacter(txtTRXKMINDVNPWP.Text, " .-/,") Then
                            Throw New Exception("Nomor Pokok Wajib Pajak (NPWP) (Kas Masuk) should not contain any special character")
                        End If
                    End If

                    If txtTRXKMINDVPenghasilanRataRata.Text <> "" Then
                        If IsNumeric(txtTRXKMINDVPenghasilanRataRata.Text) = False Then
                            Throw New Exception("Penghasilan rata-rata/th (Rp) must be filled by number")
                        End If
                    End If


                End If

                'CORP
                If rblTRXKMTipePelapor.SelectedValue = "2" Then
                    If cboTRXKMCORPBentukBadanUsaha.SelectedIndex = 0 Then
                        Throw New Exception("Bentuk Badan Usaha (Kas Masuk) must be choosen")
                    End If
                    If txtTRXKMCORPBidangUsaha.Text = vbNullString OrElse txtTRXKMCORPBidangUsaha.Text = "" Then
                        Throw New Exception("Bidang Usaha (Kas Masuk) must be filled")
                    End If
                End If

            End If

            If lblDetilKasKeluarJumlahRp.Text <> vbNullString OrElse lblDetilKasKeluarJumlahRp.Text <> "" Then
                If txtTRXKKTanggalTransaksi.Text = vbNullString OrElse txtTRXKKTanggalTransaksi.Text = "" Then
                    Throw New Exception("Tanggal Transaksi (Kas Keluar) must be filled")

                End If

                If txtTRXKKTanggalTransaksi.Text > Now Then
                    Throw New Exception("Tanggal Transaksi (Kas Keluar) should be not greater than today")
                End If

                If txtTRXKKNamaKantor.Text = vbNullString OrElse txtTRXKKNamaKantor.Text = "" Then
                    Throw New Exception("Nama Kantor (Kas Keluar) must be filled")

                End If
                If txtTRXKKKotaKab.Text = vbNullString OrElse txtTRXKKKotaKab.Text = "" Then
                    Throw New Exception("Kota Kabupaten (Kas Keluar) must be filled")

                End If
                If txtTRXKKProvinsi.Text = vbNullString OrElse txtTRXKKProvinsi.Text = "" Then
                    Throw New Exception("Provinsi (Kas Keluar) must be filled")
                End If
                If rblTRXKKTipePelapor.SelectedValue = "" Then
                    Throw New Exception("Tipe pihak (Kas Keluar) terkait must be filled")
                End If
                'INDV
                If rblTRXKKTipePelapor.SelectedValue = "1" Then
                    If txtTRXKKINDVTglLahir.Text <> "" Then
                        If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", txtTRXKKINDVTglLahir.Text) = False Then
                            Throw New Exception("Tanggal Lahir (Kas Keluar) is not valid")

                        End If
                    End If

                    If rblTRXKKINDVKewarganegaraan.SelectedIndex = -1 Then
                        Throw New Exception("Kewarganegaraan (Kas Keluar) must be choosen")
                    End If

                    If txtTRXKKINDVNomorId.Text <> "" Then
                        If Not Regex.Match(txtTRXKKINDVNomorId.Text.Trim(), "^[A-Za-z0-9]*$").Success Then
                            Throw New Exception("Nomor Identitas (Kas Keluar) should not contains any special character")
                        End If
                    End If

                    If txtTRXKKINDVNPWP.Text <> "" Then
                        If ValidateBLL.isContainSpecialCharacter(txtTRXKKINDVNPWP.Text, " .-/,") Then
                            Throw New Exception("Nomor Pokok Wajib Pajak (NPWP) (Kas Keluar) should not contain any special character")
                        End If
                    End If

                    If txtTRXKKINDVPenghasilanRataRata.Text <> "" Then
                        If IsNumeric(txtTRXKKINDVPenghasilanRataRata.Text) = False Then
                            Throw New Exception("Penghasilan rata-rata/th (Rp) must be filled by number")
                        End If
                    End If


                End If

                'CORP
                If rblTRXKKTipePelapor.SelectedValue = "2" Then
                    If cboTRXKKCORPBentukBadanUsaha.SelectedIndex = 0 Then
                        Throw New Exception("Bentuk Badan Usaha (Kas Keluar) must be choosen")
                    End If
                    If txtTRXKKCORPBidangUsaha.Text = vbNullString OrElse txtTRXKKCORPBidangUsaha.Text = "" Then
                        Throw New Exception("Bidang Usaha (Kas Keluar) must be filled")
                    End If
                End If

            End If

            Return True


        Catch ex As Exception
            ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('IdTerlapor','Img1','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
            ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('Umum','searchimage2','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message.ToString
            Return False
        End Try
    End Function
    Sub clearKasMasukKasKeluar()
        'bersihin grid
        Session("LTKTEdit.grvTRXKMDetilValutaAsingDATA") = Nothing
        Session("LTKTEdit.grvDetilKasKeluarDATA") = Nothing
        grvDetilKasKeluar.DataSource = SetnGetgrvDetilKasKeluar
        grvDetilKasKeluar.DataBind()
        grvTRXKMDetilValutaAsing.DataSource = SetnGetgrvTRXKMDetilValutaAsing
        grvTRXKMDetilValutaAsing.DataBind()

        'bersihin sisa Kas Masuk
        txtTRXKMTanggalTrx.Text = ""
        txtTRXKMNamaKantor.Text = ""
        txtTRXKMKotaKab.Text = ""
        hfTRXKMKotaKab.Value = ""
        txtTRXKMProvinsi.Text = ""
        hfTRXKMProvinsi.Value = ""
        txtTRXKMDetilKasMasuk.Text = ""
        txtTRXKMDetilMataUang.Text = ""
        hfTRXKMDetilMataUang.Value = ""
        txtTRXKMDetailKursTrx.Text = ""
        txtTRXKMDetilJumlah.Text = ""
        lblTRXKMDetilValutaAsingJumlahRp.Text = ""
        txtTRXKMNoRekening.Text = ""
        txtTRXKMINDVGelar.Text = ""
        txtTRXKMINDVNamaLengkap.Text = ""
        txtTRXKMINDVTempatLahir.Text = ""
        txtTRXKMINDVTanggalLahir.Text = ""
        rblTRXKMINDVKewarganegaraan.SelectedIndex = -1
        cboTRXKMINDVNegara.SelectedIndex = -1
        txtTRXKMINDVDOMNamaJalan.Text = ""
        txtTRXKMINDVDOMRTRW.Text = ""
        txtTRXKMINDVDOMKelurahan.Text = ""
        hfTRXKMINDVDOMKelurahan.Value = ""
        txtTRXKMINDVDOMKecamatan.Text = ""
        hfTRXKMINDVDOMKecamatan.Value = ""
        txtTRXKMINDVDOMKotaKab.Text = ""
        hfTRXKMINDVDOMKotaKab.Value = ""
        txtTRXKMINDVDOMKodePos.Text = ""
        txtTRXKMINDVDOMProvinsi.Text = ""
        hfTRXKMINDVDOMProvinsi.Value = ""
        chkTRXKMINDVCopyDOM.Checked = False
        txtTRXKMINDVIDNamaJalan.Text = ""
        txtTRXKMINDVIDRTRW.Text = ""
        txtTRXKMINDVIDKelurahan.Text = ""
        hfTRXKMINDVIDKelurahan.Value = ""
        txtTRXKMINDVIDKecamatan.Text = ""
        hfTRXKMINDVIDKecamatan.Value = ""
        txtTRXKMINDVIDKotaKab.Text = ""
        hfTRXKMINDVIDKotaKab.Value = ""
        txtTRXKMINDVIDKodePos.Text = ""
        txtTRXKMINDVIDProvinsi.Text = ""
        hfTRXKMINDVIDProvinsi.Value = ""
        txtTRXKMINDVNANamaJalan.Text = ""
        txtTRXKMINDVNANegara.Text = ""
        hfTRXKMINDVNANegara.Value = ""
        txtTRXKMINDVNAProvinsi.Text = ""
        hfTRXKMINDVNAProvinsi.Value = ""
        txtTRXKMINDVNAKota.Text = ""
        hfTRXKMINDVNAKota.Value = ""
        txtTRXKMINDVNAKodePos.Text = ""
        cboTRXKMINDVJenisID.SelectedIndex = -1
        txtTRXKMINDVNomorID.Text = ""
        txtTRXKMINDVNPWP.Text = ""
        txtTRXKMINDVPekerjaan.Text = ""
        hfTRXKMINDVPekerjaan.Value = ""
        txtTRXKMINDVJabatan.Text = ""
        txtTRXKMINDVPenghasilanRataRata.Text = ""
        txtTRXKMINDVTempatKerja.Text = ""
        txtTRXKMINDVTujuanTrx.Text = ""
        txtTRXKMINDVSumberDana.Text = ""
        txtTRXKMINDVNamaBankLain.Text = ""
        txtTRXKMINDVNoRekeningTujuan.Text = ""

        cboTRXKMCORPBentukBadanUsaha.SelectedIndex = -1
        txtTRXKMCORPNama.Text = ""
        txtTRXKMCORPBidangUsaha.Text = ""
        hfTRXKMCORPBidangUsaha.Value = ""
        txtTRXKMCORPDLNamaJalan.Text = ""
        txtTRXKMCORPDLRTRW.Text = ""
        txtTRXKMCORPDLKelurahan.Text = ""
        hfTRXKMCORPDLKelurahan.Value = ""
        txtTRXKMCORPDLKecamatan.Text = ""
        hfTRXKMCORPDLKecamatan.Value = ""
        txtTRXKMCORPDLKotaKab.Text = ""
        hfTRXKMCORPDLKotaKab.Value = ""
        txtTRXKMCORPDLKodePos.Text = ""
        txtTRXKMCORPDLProvinsi.Text = ""
        hfTRXKMCORPDLProvinsi.Value = ""
        txtTRXKMCORPDLNegara.Text = ""
        hfTRXKMCORPDLNegara.Value = ""
        txtTRXKMCORPLNNamaJalan.Text = ""
        txtTRXKMCORPLNNegara.Text = ""
        hfTRXKMCORPLNNegara.Value = ""
        txtTRXKMCORPLNProvinsi.Text = ""
        hfTRXKMCORPLNProvinsi.Value = ""
        txtTRXKMCORPLNKota.Text = ""
        hfTRXKMCORPLNKota.Value = ""
        txtTRXKMCORPLNKodePos.Text = ""
        txtTRXKMCORPNPWP.Text = ""
        txtTRXKMCORPTujuanTrx.Text = ""
        txtTRXKMCORPSumberDana.Text = ""
        txtTRXKMCORPNamaBankLain.Text = ""
        txtTRXKMCORPNoRekeningTujuan.Text = ""


        'Bersihin sisa Kas Keluar
        txtTRXKKTanggalTransaksi.Text = ""
        txtTRXKKNamaKantor.Text = ""
        txtTRXKKKotaKab.Text = ""
        hfTRXKKKotaKab.Value = ""
        txtTRXKKProvinsi.Text = ""
        hfTRXKKProvinsi.Value = ""
        txtTRXKKDetailKasKeluar.Text = ""
        txtTRXKKDetilMataUang.Text = ""
        hfTRXKKDetilMataUang.Value = ""
        txtTRXKKDetilKursTrx.Text = ""
        txtTRXKKDetilJumlah.Text = ""
        lblDetilKasKeluarJumlahRp.Text = ""
        'txtTRXKKNoRekening.Text = ""
        txtTRXKKINDVGelar.Text = ""
        txtTRXKKINDVNamaLengkap.Text = ""
        txtTRXKKINDVTempatLahir.Text = ""
        txtTRXKKINDVTglLahir.Text = ""
        rblTRXKKINDVKewarganegaraan.SelectedIndex = -1
        cboTRXKKINDVNegara.SelectedIndex = -1
        txtTRXKKINDVDOMNamaJalan.Text = ""
        txtTRXKKINDVDOMRTRW.Text = ""
        txtTRXKKINDVDOMKelurahan.Text = ""
        hfTRXKKINDVDOMKelurahan.Value = ""
        txtTRXKKINDVDOMKecamatan.Text = ""
        hfTRXKKINDVDOMKecamatan.Value = ""
        txtTRXKKINDVDOMKotaKab.Text = ""
        hfTRXKKINDVDOMKotaKab.Value = ""
        txtTRXKKINDVDOMKodePos.Text = ""
        txtTRXKKINDVDOMProvinsi.Text = ""
        hfTRXKKINDVDOMProvinsi.Value = ""
        chkTRXKKINDVIDCopyDOM.Checked = False
        txtTRXKKINDVIDNamaJalan.Text = ""
        txtTRXKKINDVIDRTRW.Text = ""
        txtTRXKKINDVIDKelurahan.Text = ""
        hfTRXKKINDVIDKelurahan.Value = ""
        txtTRXKKINDVIDKecamatan.Text = ""
        hfTRXKKINDVIDKecamatan.Value = ""
        txtTRXKKINDVIDKotaKab.Text = ""
        hfTRXKKINDVIDKotaKab.Value = ""
        txtTRXKKINDVIDKodePos.Text = ""
        txtTRXKKINDVIDProvinsi.Text = ""
        hfTRXKKINDVIDProvinsi.Value = ""
        txtTRXKKINDVNANamaJalan.Text = ""
        txtTRXKKINDVNANegara.Text = ""
        hfTRXKKINDVNANegara.Value = ""
        txtTRXKKINDVNAProvinsi.Text = ""
        hfTRXKKINDVNAProvinsi.Value = ""
        txtTRXKKINDVNAKota.Text = ""
        hfTRXKKINDVNAKota.Value = ""
        txtTRXKKINDVNAKodePos.Text = ""
        cboTRXKKINDVJenisID.SelectedIndex = -1
        txtTRXKKINDVNomorId.Text = ""
        txtTRXKKINDVNPWP.Text = ""
        txtTRXKKINDVPekerjaan.Text = ""
        hfTRXKKINDVPekerjaan.Value = ""
        txtTRXKKINDVJabatan.Text = ""
        txtTRXKKINDVPenghasilanRataRata.Text = ""
        txtTRXKKINDVTempatKerja.Text = ""
        txtTRXKKINDVTujuanTrx.Text = ""
        txtTRXKKINDVSumberDana.Text = ""
        txtTRXKKINDVNamaBankLain.Text = ""
        txtTRXKKINDVNoRekTujuan.Text = ""

        cboTRXKKCORPBentukBadanUsaha.SelectedIndex = -1
        txtTRXKKCORPNama.Text = ""
        txtTRXKKCORPBidangUsaha.Text = ""
        hfTRXKKCORPBidangUsaha.Value = ""
        txtTRXKKCORPDLNamaJalan.Text = ""
        txtTRXKKCORPDLRTRW.Text = ""
        txtTRXKKCORPDLKelurahan.Text = ""
        hfTRXKKCORPDLKelurahan.Value = ""
        txtTRXKKCORPDLKecamatan.Text = ""
        hfTRXKKCORPDLKecamatan.Value = ""
        txtTRXKKCORPDLKotaKab.Text = ""
        hfTRXKKCORPDLKotaKab.Value = ""
        txtTRXKKCORPDLKodePos.Text = ""
        txtTRXKKCORPDLProvinsi.Text = ""
        hfTRXKKCORPDLProvinsi.Value = ""
        txtTRXKKCORPDLNegara.Text = ""
        hfTRXKKCORPDLNegara.Value = ""
        txtTRXKKCORPLNNamaJalan.Text = ""
        txtTRXKKCORPLNNegara.Text = ""
        hfTRXKKCORPLNNegara.Value = ""
        txtTRXKKCORPLNProvinsi.Text = ""
        hfTRXKKCORPLNProvinsi.Value = ""
        txtTRXKKCORPLNKota.Text = ""
        hfTRXKKCORPLNKota.Value = ""
        txtTRXKKCORPLNKodePos.Text = ""
        txtTRXKKCORPNPWP.Text = ""
        txtTRXKKCORPTujuanTrx.Text = ""
        txtTRXKKCORPSumberDana.Text = ""
        txtTRXKKCORPNamaBankLain.Text = ""
        txtTRXKKCORPNoRekeningTujuan.Text = ""
        txtTRXKKRekeningKK.Text = ""


    End Sub



    Function isvalidData() As Boolean
        Try
            If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", txtTglLaporan.Text) = False Then
                Throw New Exception("Format Tanggal Pelaporan not valid")

            End If

            If txtTglLaporan.Text > Now Then
                Throw New Exception("Tanggal Pelaporan should be not greater than today")
            End If

            If cboTipeLaporan.SelectedValue = 1 Then
                If txtNoLTKTKoreksi.Text = vbNullString OrElse txtNoLTKTKoreksi.Text = "" Then
                    Throw New Exception("No. LTKT yang dikoreksi must be filled")

                End If
            End If
            If cboTerlaporKepemilikan.SelectedIndex = 0 Then
                Throw New Exception("Kepemilikan must be choosen")

            End If

            'PK ID Pemilik Rekening = 13
            Dim IntIDPemilikRekening As Integer
            IntIDPemilikRekening = CType(LTKTBLL.GetSystemParameterValueById(13), Integer)
            If cboTerlaporKepemilikan.SelectedValue = IntIDPemilikRekening Then
                If txtTerlaporNoRekening.Text = vbNullString Or txtTerlaporNoRekening.Text = "" Then
                    Throw New Exception("Nomor Rekening harus diisi")
                End If
            End If

            If SetnGetRowEdit <> -1 Then
                Throw New Exception("Silahkan Save Atau Cancel Data Pihak terkait terlebih dahulu")
            End If


            If rblTerlaporTipePelapor.SelectedValue = 1 Then
                If txtTerlaporNamaLengkap.Text = vbNullString OrElse txtTerlaporNamaLengkap.Text = "" Then
                    Throw New Exception("Nama Lengkap must be filled")

                End If

                If txtTerlaporTempatLahir.Text = vbNullString OrElse txtTerlaporTempatLahir.Text = "" Then
                    Throw New Exception("Tempat Lahir must be filled")

                End If
                If txtTerlaporTglLahir.Text = vbNullString OrElse txtTerlaporTglLahir.Text = "" Then
                    Throw New Exception("Tanggal Lahir must be filled")

                End If
                If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", txtTerlaporTglLahir.Text) = False Then
                    Throw New Exception("Format Tanggal Lahir not valid")

                End If
                If rblTerlaporKewarganegaraan.SelectedIndex = -1 Then
                    Throw New Exception("Kewarganegaraan Terlapor  must be choosen")

                End If
                If rblTerlaporKewarganegaraan.SelectedIndex = 1 Then
                    If cboTerlaporNegara.SelectedIndex = 0 Then
                        Throw New Exception("Negara must be choosen")

                    End If
                    If txtTerlaporNAKodePos.Text = vbNullString OrElse txtTerlaporNAKodePos.Text = "" Then
                        Throw New Exception("Kode Pos must be filled (Alamat Sesuai Negara Asal)")

                    End If

                    If txtTerlaporNANegara.Text = vbNullString OrElse txtTerlaporNANegara.Text = "" Then
                        Throw New Exception("Negara must be filled (Alamat Sesuai Negara Asal)")

                    End If
                End If
                If txtTerlaporDOMKotaKab.Text = vbNullString OrElse txtTerlaporDOMKotaKab.Text = "" Then
                    Throw New Exception("Kota / Kabupaten must be filled (Alamat Domisili)")

                End If
                If txtTerlaporDOMProvinsi.Text = vbNullString OrElse txtTerlaporDOMProvinsi.Text = "" Then
                    Throw New Exception("Provinsi must be filled (Alamat Domisili)")

                End If
                If txtTerlaporIDKotaKab.Text = vbNullString OrElse txtTerlaporIDKotaKab.Text = "" Then
                    Throw New Exception("Kota Kabupaten must be filled (Alamat Sesuai Bukti Identitas)")

                End If
                If txtTerlaporIDProvinsi.Text = vbNullString OrElse txtTerlaporIDProvinsi.Text = "" Then
                    Throw New Exception("Kota Provinsi must be filled (Alamat Sesuai Bukti Identitas)")

                End If

                If cboTerlaporJenisDocID.SelectedIndex = 0 Then
                    Throw New Exception("Jenis Dokumen Identitas must be choosen")

                End If

                If txtTerlaporNomorID.Text = vbNullString OrElse txtTerlaporNomorID.Text = "" Then
                    Throw New Exception("Nomor Identitas must be filled")
                End If

                If txtTerlaporNomorID.Text <> "" Then
                    If Not Regex.Match(txtTerlaporNomorID.Text.Trim(), "^[a-zA-Z0-9]*$").Success Then
                        Throw New Exception("Nomor Identitas should not contains any special character")
                    End If
                End If

                If txtTerlaporNPWP.Text <> "" Then
                    If ValidateBLL.isContainSpecialCharacter(txtTerlaporNPWP.Text, " .-/,") Then
                        Throw New Exception("Nomor Pokok Wajib Pajak (NPWP) should not contains any special character")
                    End If
                End If

                If txtTerlaporPekerjaan.Text = vbNullString OrElse txtTerlaporPekerjaan.Text = "" Then
                    Throw New Exception("Pekerjaan must be filled")

                End If

                If txtTerlaporPenghasilanRataRata.Text <> "" Then
                    If IsNumeric(txtTerlaporPenghasilanRataRata.Text) = False Then
                        Throw New Exception("Penghasilan rata-rata/th must be filled by number")
                    End If
                End If
            End If

            'Corp
            If rblTerlaporTipePelapor.SelectedValue = 2 Then
                If cboTerlaporCORPBentukBadanUsaha.SelectedIndex = 0 Then
                    Throw New Exception("Bentuk Badan Usaha must be filled")

                End If
                If txtTerlaporCORPNama.Text = vbNullString OrElse txtTerlaporCORPNama.Text = "" Then
                    Throw New Exception("Nama Korporasi must be filled")

                End If
                If cboTerlaporCORPBentukBadanUsaha.SelectedItem.Text.ToLower.Contains("yayasan") = False Then
                    If txtTerlaporCORPBidangUsaha.Text = vbNullString OrElse txtTerlaporCORPBidangUsaha.Text = "" Then
                        Throw New Exception("Bidang Usaha Korporasi must be filled")

                    End If
                End If

                If rblTerlaporCORPTipeAlamat.SelectedIndex = -1 Then
                    Throw New Exception("Alamat Korporasi must be choosen")

                End If
                If rblTerlaporCORPTipeAlamat.SelectedIndex = 0 Then
                    If txtTerlaporCORPDLKotaKab.Text = vbNullString OrElse txtTerlaporCORPDLKotaKab.Text = "" Then
                        Throw New Exception("Kota / Kabupaten (Alamat Lengkap Korporasi) must be filled")

                    End If
                    If txtTerlaporCORPDLProvinsi.Text = vbNullString OrElse txtTerlaporCORPDLProvinsi.Text = "" Then
                        Throw New Exception("Provinsi (Alamat Lengkap Korporasi) must be filled")

                    End If
                    If txtTerlaporCORPDLNegara.Text = vbNullString OrElse txtTerlaporCORPDLNegara.Text = "" Then
                        Throw New Exception("Negara (Alamat Lengkap Korporasi) must be filled")

                    End If


                End If
                If rblTerlaporCORPTipeAlamat.SelectedIndex = 1 Then
                    If txtTerlaporCORPLNKodePos.Text = vbNullString OrElse txtTerlaporCORPLNKodePos.Text = "" Then
                        Throw New Exception("Kode Pos (Alamat Korporasi Luar Negeri) must be filled")

                    End If
                    If txtTerlaporCORPLNNegara.Text = vbNullString OrElse txtTerlaporCORPLNNegara.Text = "" Then
                        Throw New Exception("Negara (Alamat Korporasi Luar Negeri) must be filled")

                    End If
                End If

                If txtTerlaporCORPNPWP.Text <> "" Then
                    If ValidateBLL.isContainSpecialCharacter(txtTerlaporCORPNPWP.Text, " .-/,") Then
                        Throw New Exception("Nomor Pokok Wajib Pajak (NPWP) should not contains any special character")
                    End If
                End If
            End If

            Dim objResume As List(Of ResumeKasMasukKeluarLTKTAdd) = SetnGetResumeKasMasukKasKeluar
            If objResume.Count < 1 Then
                Throw New Exception("Transaction must be added at least once")

            End If

            For Each objSingleResume As ResumeKasMasukKeluarLTKTAdd In objResume
                If objSingleResume.Kas = "Kas Masuk" Then
                    If Me.cboDebitCredit.SelectedValue = "D" Then
                        Throw New Exception("Only Kas Keluar (Debet) are allowed")
                    End If
                Else
                    If Me.cboDebitCredit.SelectedValue = "C" Then
                        Throw New Exception("Only Kas Masuk (Kredit) are allowed")
                    End If
                End If
            Next



            Return True

        Catch ex As Exception
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message.ToString
            Return False
        End Try

    End Function





    Private Sub SetControlLoad()
        rblTerlaporTipePelapor.SelectedValue = 1
        divPerorangan.Visible = True
        divKorporasi.Visible = False

        rblTRXKMTipePelapor.SelectedValue = 1
        tblTRXKMTipePelapor.Visible = True
        tblTRXKMTipePelaporKorporasi.Visible = False

        rblTRXKKTipePelapor.SelectedValue = 1
        tblTRXKKTipePelaporPerorangan.Visible = True
        tblTRXKKTipePelaporKorporasi.Visible = False

        'bind MsKepemilikan
        Using objPemilik As TList(Of MsKepemilikan) = DataRepository.MsKepemilikanProvider.GetAll
            If objPemilik.Count > 0 Then
                cboTerlaporKepemilikan.Items.Clear()
                cboTerlaporKepemilikan.Items.Add("-Select-")
                For i As Integer = 0 To objPemilik.Count - 1
                    cboTerlaporKepemilikan.Items.Add(New ListItem(objPemilik(i).NamaKepemilikan, objPemilik(i).IDKepemilikan.ToString))
                Next
            End If
        End Using

        'bind MsNegara
        Using objNegara As TList(Of MsNegara) = DataRepository.MsNegaraProvider.GetAll
            If objNegara.Count > 0 Then
                cboTerlaporNegara.Items.Clear()
                cboTerlaporNegara.Items.Add("-Select-")

                cboTRXKMINDVNegara.Items.Clear()
                cboTRXKMINDVNegara.Items.Add("-Select-")

                cboTRXKKINDVNegara.Items.Clear()
                cboTRXKKINDVNegara.Items.Add("-Select-")
                For i As Integer = 0 To objNegara.Count - 1
                    cboTerlaporNegara.Items.Add(New ListItem(objNegara(i).NamaNegara, objNegara(i).IDNegara.ToString))
                    cboTRXKMINDVNegara.Items.Add(New ListItem(objNegara(i).NamaNegara, objNegara(i).IDNegara.ToString))
                    cboTRXKKINDVNegara.Items.Add(New ListItem(objNegara(i).NamaNegara, objNegara(i).IDNegara.ToString))
                Next
            End If
        End Using

        'Bind MsBentukBadanUsaha
        Using objBentukBadanUsaha As TList(Of MsBentukBidangUsaha) = DataRepository.MsBentukBidangUsahaProvider.GetAll
            If objBentukBadanUsaha.Count > 0 Then
                cboTerlaporCORPBentukBadanUsaha.Items.Clear()
                cboTerlaporCORPBentukBadanUsaha.Items.Add("-Select-")

                cboTRXKMCORPBentukBadanUsaha.Items.Clear()
                cboTRXKMCORPBentukBadanUsaha.Items.Add("-Select-")

                cboTRXKKCORPBentukBadanUsaha.Items.Clear()
                cboTRXKKCORPBentukBadanUsaha.Items.Add("-Select-")

                For i As Integer = 0 To objBentukBadanUsaha.Count - 1
                    cboTerlaporCORPBentukBadanUsaha.Items.Add(New ListItem(objBentukBadanUsaha(i).BentukBidangUsaha, objBentukBadanUsaha(i).IdBentukBidangUsaha.ToString))
                    cboTRXKMCORPBentukBadanUsaha.Items.Add(New ListItem(objBentukBadanUsaha(i).BentukBidangUsaha, objBentukBadanUsaha(i).IdBentukBidangUsaha.ToString))
                    cboTRXKKCORPBentukBadanUsaha.Items.Add(New ListItem(objBentukBadanUsaha(i).BentukBidangUsaha, objBentukBadanUsaha(i).IdBentukBidangUsaha.ToString))
                Next
            End If
        End Using

        'bind ID type
        Using objJenisID As TList(Of MsIDType) = DataRepository.MsIDTypeProvider.GetAll

            Using objHapusJenisID As MsIDType = objJenisID.Find(MsIDTypeColumn.MsIDType_Name, "kartu pelajar")
                If Not IsNothing(objHapusJenisID) Then
                    objJenisID.Remove(objHapusJenisID)
                End If
            End Using

            cboTerlaporJenisDocID.Items.Clear()
            cboTerlaporJenisDocID.Items.Add("-Select-")

            cboTRXKMINDVJenisID.Items.Clear()
            cboTRXKMINDVJenisID.Items.Add("-Select-")

            cboTRXKKINDVJenisID.Items.Clear()
            cboTRXKKINDVJenisID.Items.Add("-Select-")

            For i As Integer = 0 To objJenisID.Count - 1
                cboTerlaporJenisDocID.Items.Add(New ListItem(objJenisID(i).MsIDType_Name, objJenisID(i).Pk_MsIDType_Id.ToString))
                cboTRXKMINDVJenisID.Items.Add(New ListItem(objJenisID(i).MsIDType_Name, objJenisID(i).Pk_MsIDType_Id.ToString))
                cboTRXKKINDVJenisID.Items.Add(New ListItem(objJenisID(i).MsIDType_Name, objJenisID(i).Pk_MsIDType_Id.ToString))
            Next




        End Using

        'bind calendar
        Me.popUpTglLaporan.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtTglLaporan.ClientID & "'), 'dd-mmm-yyyy')")
        Me.popUpTglLaporan.Style.Add("display", "")
        Me.popTglLahirTerlapor.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtTerlaporTglLahir.ClientID & "'), 'dd-mmm-yyyy')")
        Me.popTglLahirTerlapor.Style.Add("display", "")
        Me.popTRXKMTanggalTrx.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtTRXKMTanggalTrx.ClientID & "'), 'dd-mmm-yyyy')")
        Me.popTRXKMTanggalTrx.Style.Add("display", "")
        Me.popTRXKMINDVTanggalLahir.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtTRXKMINDVTanggalLahir.ClientID & "'), 'dd-mmm-yyyy')")
        Me.popTRXKMINDVTanggalLahir.Style.Add("display", "")
        Me.popTRXKKTanggalTransaksi.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtTRXKKTanggalTransaksi.ClientID & "'), 'dd-mmm-yyyy')")
        Me.popTRXKKTanggalTransaksi.Style.Add("display", "")
        Me.popTRXKKINDVTglLahir.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtTRXKKINDVTglLahir.ClientID & "'), 'dd-mmm-yyyy')")
        Me.popTRXKKINDVTglLahir.Style.Add("display", "")

    End Sub


    Function JumlahKeseluruhanRp(ByVal objKas As List(Of LTKTDetailCashInEdit)) As Decimal
        Dim total As Decimal = 0
        For Each obj As LTKTDetailCashInEdit In objKas
            total = total + CDec(obj.JumlahRp)
        Next
        Return total
    End Function



    Function isvalidAddKassMasuk() As Boolean
        Try
            If txtTRXKMDetilMataUang.Text = vbNullString OrElse txtTRXKMDetilMataUang.Text = "" Then
                Throw New Exception("Mata Uang (Kas Masuk) must be filled")
                Return False
            End If
            If txtTRXKMDetilKasMasuk.Text <> vbNullString And txtTRXKMDetilKasMasuk.Text <> "" Then
                If IsNumeric(txtTRXKMDetilKasMasuk.Text) = False Then
                    Throw New Exception("Kas Masuk must be filled by number")
                    Return False
                End If
            End If
            If txtTRXKMDetailKursTrx.Text = vbNullString OrElse txtTRXKMDetailKursTrx.Text = "" Then
                Throw New Exception("Kurs Transaksi (Kas Masuk) must be filled")
                Return False
            End If
            If IsNumeric(txtTRXKMDetailKursTrx.Text) = False Then
                Throw New Exception("Kurs Transaksi (Kas Masuk) must be filled by number")
                Return False
            End If
            If txtTRXKMDetilJumlah.Text = vbNullString OrElse txtTRXKMDetilJumlah.Text = "" Then
                Throw New Exception("Jumlah (Kas Masuk) must be filled")
                Return False
            End If
            If IsNumeric(txtTRXKMDetilJumlah.Text) = False Then
                Throw New Exception("Jumlah (Kas Masuk) must be filled by number")
                Return False
            End If
            Return True

        Catch ex As Exception
            ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('IdTerlapor','Img1','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
            ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('Umum','searchimage2','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message.ToString
        End Try
    End Function

    Function isvalidAddKassKeluar() As Boolean
        Try
            If txtTRXKKDetilMataUang.Text = "" Then
                Throw New Exception("Mata Uang (Kas Keluar) must be filled")
                Return False
            End If

            If txtTRXKKDetailKasKeluar.Text <> "" Then
                If IsNumeric(txtTRXKKDetailKasKeluar.Text) = False Then
                    Throw New Exception("Kas Keluar must be filled by number")
                    Return False
                End If
            End If
            If txtTRXKKDetilKursTrx.Text = "" Then
                Throw New Exception("Kurs Transaksi (Kas Keluar) must be filled")
                Return False
            End If
            If IsNumeric(txtTRXKKDetilKursTrx.Text) = False Then
                Throw New Exception("Kurs Transaksi (Kas Keluar) must be filled by number")
                Return False
            End If
            If txtTRXKKDetilJumlah.Text = "" Then
                Throw New Exception("Jumlah (Kas Keluar) must be filled")
                Return False
            End If
            If IsNumeric(txtTRXKKDetilJumlah.Text) = False Then
                Throw New Exception("Jumlah (Kas Keluar) must be filled by number")
                Return False
            End If
            Return True

        Catch ex As Exception
            ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('IdTerlapor','Img1','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
            ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('Umum','searchimage2','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message.ToString
        End Try
    End Function



    Sub hideSave()
        ImageSave.Visible = False
        ImageCancel.Visible = False
    End Sub

    Sub clearSession()
        Session("LTKTEdit.grvTRXKMDetilValutaAsingDATA") = Nothing
        Session("LTKTEdit.grvDetilKasKeluarDATA") = Nothing
        Session("LTKTEdit.ResumeKasMasukKasKeluar") = Nothing
        Session("LTKTEdit.RowEdit") = Nothing
        Session("LTKTEdit.LTKTPK") = Nothing

    End Sub

    Sub loadResume()
        Dim objResume As List(Of ResumeKasMasukKeluarLTKTAdd) = SetnGetResumeKasMasukKasKeluar

        'load Kas Masuk ke objResume
        Using objListTransactionCashIn As TList(Of LTKTTransactionCashIn) = DataRepository.LTKTTransactionCashInProvider.GetPaged(LTKTTransactionCashInColumn.FK_LTKT_Id.ToString & " = " & getLTKTPK.ToString, "", 0, Integer.MaxValue, 0)
            For Each objSingleTransactinCashIn As LTKTTransactionCashIn In objListTransactionCashIn


                Dim objKas As ResumeKasMasukKeluarLTKTAdd = New ResumeKasMasukKeluarLTKTAdd
                'Insert untuk tampilan Grid
                objKas.TransactionDate = objSingleTransactinCashIn.TanggalTransaksi
                objKas.Branch = objSingleTransactinCashIn.NamaKantorPJK
                objKas.TransactionNominal = objSingleTransactinCashIn.Total.ToString
                objKas.AccountNumber = objSingleTransactinCashIn.NomorRekening
                objKas.Kas = "Kas Masuk"
                objKas.Type = "LTKT"

                'Insert Data Ke ObjectTransaction
                objKas.TransactionCashIn = New LTKTTransactionCashIn
                objKas.TransactionCashIn.PK_LTKTTransactionCashIn_Id = objSingleTransactinCashIn.PK_LTKTTransactionCashIn_Id
                objKas.TransactionCashIn.TanggalTransaksi = objSingleTransactinCashIn.TanggalTransaksi.GetValueOrDefault
                objKas.TransactionCashIn.NamaKantorPJK = objSingleTransactinCashIn.NamaKantorPJK
                objKas.TransactionCashIn.FK_MsKotaKab_Id = objSingleTransactinCashIn.FK_MsKotaKab_Id.GetValueOrDefault
                objKas.TransactionCashIn.FK_MsProvince_Id = objSingleTransactinCashIn.FK_MsProvince_Id.GetValueOrDefault
                objKas.TransactionCashIn.NomorRekening = objSingleTransactinCashIn.NomorRekening
                objKas.TransactionCashIn.TipeTerlapor = objSingleTransactinCashIn.TipeTerlapor

                'Individu
                objKas.TransactionCashIn.INDV_Gelar = objSingleTransactinCashIn.INDV_Gelar
                objKas.TransactionCashIn.INDV_NamaLengkap = objSingleTransactinCashIn.INDV_NamaLengkap
                objKas.TransactionCashIn.INDV_TempatLahir = objSingleTransactinCashIn.INDV_TempatLahir
                objKas.TransactionCashIn.INDV_TanggalLahir = objSingleTransactinCashIn.INDV_TanggalLahir
                objKas.TransactionCashIn.INDV_Kewarganegaraan = objSingleTransactinCashIn.INDV_Kewarganegaraan
                objKas.TransactionCashIn.INDV_FK_MsNegara_Id = objSingleTransactinCashIn.INDV_FK_MsNegara_Id.GetValueOrDefault
                objKas.TransactionCashIn.INDV_DOM_NamaJalan = objSingleTransactinCashIn.INDV_DOM_NamaJalan
                objKas.TransactionCashIn.INDV_DOM_RTRW = objSingleTransactinCashIn.INDV_DOM_RTRW
                objKas.TransactionCashIn.INDV_DOM_FK_MsKelurahan_Id = objSingleTransactinCashIn.INDV_DOM_FK_MsKelurahan_Id.GetValueOrDefault
                objKas.TransactionCashIn.INDV_DOM_FK_MsKecamatan_Id = objSingleTransactinCashIn.INDV_DOM_FK_MsKecamatan_Id.GetValueOrDefault
                objKas.TransactionCashIn.INDV_DOM_FK_MsKotaKab_Id = objSingleTransactinCashIn.INDV_DOM_FK_MsKotaKab_Id.GetValueOrDefault
                objKas.TransactionCashIn.INDV_DOM_KodePos = objSingleTransactinCashIn.INDV_DOM_KodePos
                objKas.TransactionCashIn.INDV_DOM_FK_MsProvince_Id = objSingleTransactinCashIn.INDV_DOM_FK_MsProvince_Id.GetValueOrDefault
                objKas.TransactionCashIn.INDV_DOM_FK_MsNegara_Id = objSingleTransactinCashIn.INDV_DOM_FK_MsNegara_Id.GetValueOrDefault


                objKas.TransactionCashIn.INDV_ID_NamaJalan = objSingleTransactinCashIn.INDV_ID_NamaJalan
                objKas.TransactionCashIn.INDV_ID_RTRW = objSingleTransactinCashIn.INDV_ID_RTRW
                objKas.TransactionCashIn.INDV_ID_FK_MsKelurahan_Id = objSingleTransactinCashIn.INDV_ID_FK_MsKelurahan_Id.GetValueOrDefault
                objKas.TransactionCashIn.INDV_ID_FK_MsKecamatan_Id = objSingleTransactinCashIn.INDV_ID_FK_MsKecamatan_Id.GetValueOrDefault
                objKas.TransactionCashIn.INDV_ID_FK_MsKotaKab_Id = objSingleTransactinCashIn.INDV_ID_FK_MsKotaKab_Id.GetValueOrDefault
                objKas.TransactionCashIn.INDV_ID_KodePos = objSingleTransactinCashIn.INDV_ID_KodePos
                objKas.TransactionCashIn.INDV_ID_FK_MsProvince_Id = objSingleTransactinCashIn.INDV_ID_FK_MsProvince_Id.GetValueOrDefault
                objKas.TransactionCashIn.INDV_ID_FK_MsNegara_Id = objSingleTransactinCashIn.INDV_ID_FK_MsNegara_Id.GetValueOrDefault

                objKas.TransactionCashIn.INDV_NA_NamaJalan = objSingleTransactinCashIn.INDV_NA_NamaJalan
                objKas.TransactionCashIn.INDV_NA_FK_MsNegara_Id = objSingleTransactinCashIn.INDV_NA_FK_MsNegara_Id.GetValueOrDefault
                objKas.TransactionCashIn.INDV_NA_FK_MsProvince_Id = objSingleTransactinCashIn.INDV_NA_FK_MsProvince_Id.GetValueOrDefault
                objKas.TransactionCashIn.INDV_NA_FK_MsKotaKab_Id = objSingleTransactinCashIn.INDV_NA_FK_MsKotaKab_Id.GetValueOrDefault
                objKas.TransactionCashIn.INDV_NA_KodePos = objSingleTransactinCashIn.INDV_NA_KodePos
                objKas.TransactionCashIn.INDV_NA_FK_MsNegara_Id = objSingleTransactinCashIn.INDV_NA_FK_MsNegara_Id

                objKas.TransactionCashIn.INDV_FK_MsIDType_Id = objSingleTransactinCashIn.INDV_FK_MsIDType_Id.GetValueOrDefault
                objKas.TransactionCashIn.INDV_NomorId = objSingleTransactinCashIn.INDV_NomorId
                objKas.TransactionCashIn.INDV_NPWP = objSingleTransactinCashIn.INDV_NPWP
                objKas.TransactionCashIn.INDV_FK_MsPekerjaan_Id = objSingleTransactinCashIn.INDV_FK_MsPekerjaan_Id.GetValueOrDefault
                objKas.TransactionCashIn.INDV_Jabatan = objSingleTransactinCashIn.INDV_Jabatan
                objKas.TransactionCashIn.INDV_PenghasilanRataRata = objSingleTransactinCashIn.INDV_PenghasilanRataRata
                objKas.TransactionCashIn.INDV_TempatBekerja = objSingleTransactinCashIn.INDV_TempatBekerja
                objKas.TransactionCashIn.INDV_TujuanTransaksi = objSingleTransactinCashIn.INDV_TujuanTransaksi
                objKas.TransactionCashIn.INDV_SumberDana = objSingleTransactinCashIn.INDV_SumberDana
                objKas.TransactionCashIn.INDV_NamaBankLain = objSingleTransactinCashIn.INDV_NamaBankLain
                objKas.TransactionCashIn.INDV_NomorRekeningTujuan = objSingleTransactinCashIn.INDV_NomorRekeningTujuan

                objKas.TransactionCashIn.CORP_FK_MsBentukBadanUsaha_Id = objSingleTransactinCashIn.CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault
                objKas.TransactionCashIn.CORP_Nama = objSingleTransactinCashIn.CORP_Nama
                objKas.TransactionCashIn.CORP_FK_MsBidangUsaha_Id = objSingleTransactinCashIn.CORP_FK_MsBidangUsaha_Id.GetValueOrDefault
                objKas.TransactionCashIn.CORP_TipeAlamat = objSingleTransactinCashIn.CORP_TipeAlamat.GetValueOrDefault
                objKas.TransactionCashIn.CORP_NamaJalan = objSingleTransactinCashIn.CORP_NamaJalan
                objKas.TransactionCashIn.CORP_RTRW = objSingleTransactinCashIn.CORP_RTRW
                objKas.TransactionCashIn.CORP_FK_MsKelurahan_Id = objSingleTransactinCashIn.CORP_FK_MsKelurahan_Id.GetValueOrDefault
                objKas.TransactionCashIn.CORP_FK_MsKecamatan_Id = objSingleTransactinCashIn.CORP_FK_MsKecamatan_Id.GetValueOrDefault
                objKas.TransactionCashIn.CORP_FK_MsKotaKab_Id = objSingleTransactinCashIn.CORP_FK_MsKotaKab_Id.GetValueOrDefault
                objKas.TransactionCashIn.CORP_KodePos = objSingleTransactinCashIn.CORP_KodePos
                objKas.TransactionCashIn.CORP_FK_MsProvince_Id = objSingleTransactinCashIn.CORP_FK_MsProvince_Id.GetValueOrDefault
                objKas.TransactionCashIn.CORP_FK_MsNegara_Id = objSingleTransactinCashIn.CORP_FK_MsNegara_Id.GetValueOrDefault

                objKas.TransactionCashIn.CORP_LN_NamaJalan = objSingleTransactinCashIn.CORP_LN_NamaJalan
                objKas.TransactionCashIn.CORP_LN_FK_MsNegara_Id = objSingleTransactinCashIn.CORP_LN_FK_MsNegara_Id.GetValueOrDefault
                objKas.TransactionCashIn.CORP_LN_FK_MsProvince_Id = objSingleTransactinCashIn.CORP_LN_FK_MsProvince_Id.GetValueOrDefault
                objKas.TransactionCashIn.CORP_LN_MsKotaKab_Id = objSingleTransactinCashIn.CORP_LN_MsKotaKab_Id.GetValueOrDefault
                objKas.TransactionCashIn.CORP_LN_KodePos = objSingleTransactinCashIn.CORP_LN_KodePos
                objKas.TransactionCashIn.CORP_LN_FK_MsNegara_Id = objSingleTransactinCashIn.CORP_LN_FK_MsNegara_Id

                objKas.TransactionCashIn.CORP_NPWP = objSingleTransactinCashIn.CORP_NPWP
                objKas.TransactionCashIn.CORP_TujuanTransaksi = objSingleTransactinCashIn.CORP_TujuanTransaksi
                objKas.TransactionCashIn.CORP_SumberDana = objSingleTransactinCashIn.CORP_SumberDana
                objKas.TransactionCashIn.CORP_NamaBankLain = objSingleTransactinCashIn.CORP_NamaBankLain
                objKas.TransactionCashIn.CORP_NomorRekeningTujuan = objSingleTransactinCashIn.CORP_NomorRekeningTujuan

                'Insert Data Ke Detail Transaction
                objKas.DetailTransactionCashIn = New List(Of LTKTDetailCashInTransaction)

                Dim objListDetailTransactionCashIn As TList(Of LTKTDetailCashInTransaction) = DataRepository.LTKTDetailCashInTransactionProvider.GetPaged(LTKTDetailCashInTransactionColumn.FK_LTKTTransactionCashIn_Id.ToString & " = '" & objSingleTransactinCashIn.PK_LTKTTransactionCashIn_Id.ToString & "'", "", 0, Integer.MaxValue, 0)
                For Each objSingleDetailTransactionCashIn As LTKTDetailCashInTransaction In objListDetailTransactionCashIn
                    Dim objCalonInsert As New LTKTDetailCashInTransaction
                    objCalonInsert.PK_LTKTDetailCashInTransaction_Id = objSingleDetailTransactionCashIn.PK_LTKTDetailCashInTransaction_Id
                    objCalonInsert.Asing_KursTransaksi = objSingleDetailTransactionCashIn.Asing_KursTransaksi
                    objCalonInsert.Asing_TotalKasMasukDalamRupiah = objSingleDetailTransactionCashIn.Asing_TotalKasMasukDalamRupiah
                    objCalonInsert.TotalKasMasuk = objSingleDetailTransactionCashIn.TotalKasMasuk
                    objCalonInsert.KasMasuk = objSingleDetailTransactionCashIn.KasMasuk
                    objCalonInsert.Asing_FK_MsCurrency_Id = objSingleDetailTransactionCashIn.Asing_FK_MsCurrency_Id

                    objKas.DetailTransactionCashIn.Add(objCalonInsert)
                Next

                objResume.Add(objKas)

            Next
        End Using

        Using objListTransactionCashOut As TList(Of LTKTTransactionCashOut) = DataRepository.LTKTTransactionCashOutProvider.GetPaged(LTKTTransactionCashOutColumn.FK_LTKT_Id.ToString & " = " & getLTKTPK.ToString, "", 0, Integer.MaxValue, 0)
            For Each objSingleTransactinCashOut As LTKTTransactionCashOut In objListTransactionCashOut


                Dim objKas As ResumeKasMasukKeluarLTKTAdd = New ResumeKasMasukKeluarLTKTAdd
                'Insert untuk tampilan Grid
                objKas.TransactionDate = objSingleTransactinCashOut.TanggalTransaksi
                objKas.Branch = objSingleTransactinCashOut.NamaKantorPJK
                objKas.TransactionNominal = objSingleTransactinCashOut.Total.ToString
                objKas.AccountNumber = objSingleTransactinCashOut.NomorRekening
                objKas.Kas = "Kas Keluar"
                objKas.Type = "LTKT"

                'Insert Data Ke ObjectTransaction
                objKas.TransactionCashOut = New LTKTTransactionCashOut
                objKas.TransactionCashOut.PK_LTKTTransactionCashOut_Id = objSingleTransactinCashOut.PK_LTKTTransactionCashOut_Id

                objKas.TransactionCashOut.TanggalTransaksi = objSingleTransactinCashOut.TanggalTransaksi.GetValueOrDefault
                objKas.TransactionCashOut.NamaKantorPJK = objSingleTransactinCashOut.NamaKantorPJK
                objKas.TransactionCashOut.FK_MsKotaKab_Id = objSingleTransactinCashOut.FK_MsKotaKab_Id.GetValueOrDefault
                objKas.TransactionCashOut.FK_MsProvince_Id = objSingleTransactinCashOut.FK_MsProvince_Id.GetValueOrDefault
                objKas.TransactionCashOut.NomorRekening = objSingleTransactinCashOut.NomorRekening
                objKas.TransactionCashOut.TipeTerlapor = objSingleTransactinCashOut.TipeTerlapor

                'Individu
                objKas.TransactionCashOut.INDV_Gelar = objSingleTransactinCashOut.INDV_Gelar
                objKas.TransactionCashOut.INDV_NamaLengkap = objSingleTransactinCashOut.INDV_NamaLengkap
                objKas.TransactionCashOut.INDV_TempatLahir = objSingleTransactinCashOut.INDV_TempatLahir
                objKas.TransactionCashOut.INDV_TanggalLahir = objSingleTransactinCashOut.INDV_TanggalLahir
                objKas.TransactionCashOut.INDV_Kewarganegaraan = objSingleTransactinCashOut.INDV_Kewarganegaraan
                objKas.TransactionCashOut.INDV_FK_MsNegara_Id = objSingleTransactinCashOut.INDV_FK_MsNegara_Id.GetValueOrDefault

                objKas.TransactionCashOut.INDV_DOM_NamaJalan = objSingleTransactinCashOut.INDV_DOM_NamaJalan
                objKas.TransactionCashOut.INDV_DOM_RTRW = objSingleTransactinCashOut.INDV_DOM_RTRW
                objKas.TransactionCashOut.INDV_DOM_FK_MsKelurahan_Id = objSingleTransactinCashOut.INDV_DOM_FK_MsKelurahan_Id.GetValueOrDefault
                objKas.TransactionCashOut.INDV_DOM_FK_MsKecamatan_Id = objSingleTransactinCashOut.INDV_DOM_FK_MsKecamatan_Id.GetValueOrDefault
                objKas.TransactionCashOut.INDV_DOM_FK_MsKotaKab_Id = objSingleTransactinCashOut.INDV_DOM_FK_MsKotaKab_Id.GetValueOrDefault
                objKas.TransactionCashOut.INDV_DOM_KodePos = objSingleTransactinCashOut.INDV_DOM_KodePos
                objKas.TransactionCashOut.INDV_DOM_FK_MsProvince_Id = objSingleTransactinCashOut.INDV_DOM_FK_MsProvince_Id.GetValueOrDefault
                objKas.TransactionCashOut.INDV_DOM_FK_MsNegara_Id = objSingleTransactinCashOut.INDV_DOM_FK_MsNegara_Id.GetValueOrDefault

                objKas.TransactionCashOut.INDV_ID_NamaJalan = objSingleTransactinCashOut.INDV_ID_NamaJalan
                objKas.TransactionCashOut.INDV_ID_RTRW = objSingleTransactinCashOut.INDV_ID_RTRW
                objKas.TransactionCashOut.INDV_ID_FK_MsKelurahan_Id = objSingleTransactinCashOut.INDV_ID_FK_MsKelurahan_Id.GetValueOrDefault
                objKas.TransactionCashOut.INDV_ID_FK_MsKecamatan_Id = objSingleTransactinCashOut.INDV_ID_FK_MsKecamatan_Id.GetValueOrDefault
                objKas.TransactionCashOut.INDV_ID_FK_MsKotaKab_Id = objSingleTransactinCashOut.INDV_ID_FK_MsKotaKab_Id.GetValueOrDefault
                objKas.TransactionCashOut.INDV_ID_FK_MsNegara_Id = objSingleTransactinCashOut.INDV_ID_FK_MsNegara_Id.GetValueOrDefault

                objKas.TransactionCashOut.INDV_ID_KodePos = objSingleTransactinCashOut.INDV_ID_KodePos
                objKas.TransactionCashOut.INDV_ID_FK_MsProvince_Id = objSingleTransactinCashOut.INDV_ID_FK_MsProvince_Id.GetValueOrDefault
                objKas.TransactionCashOut.INDV_NA_NamaJalan = objSingleTransactinCashOut.INDV_NA_NamaJalan
                objKas.TransactionCashOut.INDV_NA_FK_MsNegara_Id = objSingleTransactinCashOut.INDV_NA_FK_MsNegara_Id.GetValueOrDefault
                objKas.TransactionCashOut.INDV_NA_FK_MsProvince_Id = objSingleTransactinCashOut.INDV_NA_FK_MsProvince_Id.GetValueOrDefault
                objKas.TransactionCashOut.INDV_NA_FK_MsKotaKab_Id = objSingleTransactinCashOut.INDV_NA_FK_MsKotaKab_Id.GetValueOrDefault
                objKas.TransactionCashOut.INDV_NA_FK_MsNegara_Id = objSingleTransactinCashOut.INDV_NA_FK_MsNegara_Id.GetValueOrDefault

                objKas.TransactionCashOut.INDV_NA_KodePos = objSingleTransactinCashOut.INDV_NA_KodePos
                objKas.TransactionCashOut.INDV_FK_MsIDType_Id = objSingleTransactinCashOut.INDV_FK_MsIDType_Id.GetValueOrDefault
                objKas.TransactionCashOut.INDV_NomorId = objSingleTransactinCashOut.INDV_NomorId
                objKas.TransactionCashOut.INDV_NPWP = objSingleTransactinCashOut.INDV_NPWP
                objKas.TransactionCashOut.INDV_FK_MsPekerjaan_Id = objSingleTransactinCashOut.INDV_FK_MsPekerjaan_Id.GetValueOrDefault
                objKas.TransactionCashOut.INDV_Jabatan = objSingleTransactinCashOut.INDV_Jabatan
                objKas.TransactionCashOut.INDV_PenghasilanRataRata = objSingleTransactinCashOut.INDV_PenghasilanRataRata
                objKas.TransactionCashOut.INDV_TempatBekerja = objSingleTransactinCashOut.INDV_TempatBekerja
                objKas.TransactionCashOut.INDV_TujuanTransaksi = objSingleTransactinCashOut.INDV_TujuanTransaksi
                objKas.TransactionCashOut.INDV_SumberDana = objSingleTransactinCashOut.INDV_SumberDana
                objKas.TransactionCashOut.INDV_NamaBankLain = objSingleTransactinCashOut.INDV_NamaBankLain
                objKas.TransactionCashOut.INDV_NomorRekeningTujuan = objSingleTransactinCashOut.INDV_NomorRekeningTujuan

                objKas.TransactionCashOut.CORP_FK_MsBentukBadanUsaha_Id = objSingleTransactinCashOut.CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault
                objKas.TransactionCashOut.CORP_Nama = objSingleTransactinCashOut.CORP_Nama
                objKas.TransactionCashOut.CORP_FK_MsBidangUsaha_Id = objSingleTransactinCashOut.CORP_FK_MsBidangUsaha_Id.GetValueOrDefault
                objKas.TransactionCashOut.CORP_TipeAlamat = objSingleTransactinCashOut.CORP_TipeAlamat.GetValueOrDefault
                objKas.TransactionCashOut.CORP_NamaJalan = objSingleTransactinCashOut.CORP_NamaJalan
                objKas.TransactionCashOut.CORP_RTRW = objSingleTransactinCashOut.CORP_RTRW
                objKas.TransactionCashOut.CORP_FK_MsKelurahan_Id = objSingleTransactinCashOut.CORP_FK_MsKelurahan_Id.GetValueOrDefault
                objKas.TransactionCashOut.CORP_FK_MsKecamatan_Id = objSingleTransactinCashOut.CORP_FK_MsKecamatan_Id.GetValueOrDefault
                objKas.TransactionCashOut.CORP_FK_MsKotaKab_Id = objSingleTransactinCashOut.CORP_FK_MsKotaKab_Id.GetValueOrDefault
                objKas.TransactionCashOut.CORP_FK_MsNegara_Id = objSingleTransactinCashOut.CORP_FK_MsKotaKab_Id.GetValueOrDefault


                objKas.TransactionCashOut.CORP_KodePos = objSingleTransactinCashOut.CORP_KodePos
                objKas.TransactionCashOut.CORP_FK_MsProvince_Id = objSingleTransactinCashOut.CORP_FK_MsProvince_Id.GetValueOrDefault
                objKas.TransactionCashOut.CORP_FK_MsNegara_Id = objSingleTransactinCashOut.CORP_FK_MsNegara_Id.GetValueOrDefault
                objKas.TransactionCashOut.CORP_LN_NamaJalan = objSingleTransactinCashOut.CORP_LN_NamaJalan
                objKas.TransactionCashOut.CORP_LN_FK_MsNegara_Id = objSingleTransactinCashOut.CORP_LN_FK_MsNegara_Id.GetValueOrDefault
                objKas.TransactionCashOut.CORP_LN_FK_MsProvince_Id = objSingleTransactinCashOut.CORP_LN_FK_MsProvince_Id.GetValueOrDefault
                objKas.TransactionCashOut.CORP_LN_MsKotaKab_Id = objSingleTransactinCashOut.CORP_LN_MsKotaKab_Id.GetValueOrDefault
                objKas.TransactionCashOut.CORP_LN_FK_MsNegara_Id = objSingleTransactinCashOut.CORP_LN_FK_MsNegara_Id.GetValueOrDefault

                objKas.TransactionCashOut.CORP_LN_KodePos = objSingleTransactinCashOut.CORP_LN_KodePos
                objKas.TransactionCashOut.CORP_NPWP = objSingleTransactinCashOut.CORP_NPWP
                objKas.TransactionCashOut.CORP_TujuanTransaksi = objSingleTransactinCashOut.CORP_TujuanTransaksi
                objKas.TransactionCashOut.CORP_SumberDana = objSingleTransactinCashOut.CORP_SumberDana
                objKas.TransactionCashOut.CORP_NamaBankLain = objSingleTransactinCashOut.CORP_NamaBankLain
                objKas.TransactionCashOut.CORP_NomorRekeningTujuan = objSingleTransactinCashOut.CORP_NomorRekeningTujuan


                'Insert Data Ke Detail Transaction
                objKas.DetailTranscationCashOut = New List(Of LTKTDetailCashOutTransaction)

                Dim objListDetailTransactionCashOut As TList(Of LTKTDetailCashOutTransaction) = DataRepository.LTKTDetailCashOutTransactionProvider.GetPaged(LTKTDetailCashOutTransactionColumn.FK_LTKTTransactionCashOut_Id.ToString & " = '" & objSingleTransactinCashOut.PK_LTKTTransactionCashOut_Id.ToString & "'", "", 0, Integer.MaxValue, 0)
                For Each objSingleDetailTransactionCashOut As LTKTDetailCashOutTransaction In objListDetailTransactionCashOut
                    Dim objCalonInsert As New LTKTDetailCashOutTransaction
                    objCalonInsert.PK_LTKTDetailCashOutTransaction_Id = objSingleDetailTransactionCashOut.PK_LTKTDetailCashOutTransaction_Id
                    objCalonInsert.Asing_KursTransaksi = objSingleDetailTransactionCashOut.Asing_KursTransaksi
                    objCalonInsert.Asing_TotalKasKeluarDalamRupiah = objSingleDetailTransactionCashOut.Asing_TotalKasKeluarDalamRupiah
                    objCalonInsert.TotalKasKeluar = objSingleDetailTransactionCashOut.TotalKasKeluar
                    objCalonInsert.KasKeluar = objSingleDetailTransactionCashOut.KasKeluar
                    objCalonInsert.Asing_FK_MsCurrency_Id = objSingleDetailTransactionCashOut.Asing_FK_MsCurrency_Id

                    objKas.DetailTranscationCashOut.Add(objCalonInsert)
                Next

                objResume.Add(objKas)

            Next
        End Using
        SetnGetResumeKasMasukKasKeluar = objResume
        grvTransaksi.DataSource = objResume
        grvTransaksi.DataBind()

        Dim totalKasMasuk As Decimal = 0
        Dim totalKasKeluar As Decimal = 0
        For Each kas As ResumeKasMasukKeluarLTKTAdd In objResume
            If kas.Kas = "Kas Masuk" Then
                totalKasMasuk += CDec(kas.TransactionNominal)
            Else
                totalKasKeluar += CDec(kas.TransactionNominal)
            End If
        Next

        lblTotalKasMasuk.Text = ValidateBLL.FormatMoneyWithComma(totalKasMasuk)
        lblTotalKasKeluar.Text = ValidateBLL.FormatMoneyWithComma(totalKasKeluar)

    End Sub

    Sub LTKTTipeTerlaporChange()
        If rblTerlaporTipePelapor.SelectedValue = 1 Then
            divPerorangan.Visible = True
            divKorporasi.Visible = False
        Else
            divPerorangan.Visible = False
            divKorporasi.Visible = True
        End If
    End Sub

    Sub tipeLaporanCheck()
        If cboTipeLaporan.SelectedIndex = 0 Then
            tblLTKTKoreksi.Visible = False
        Else
            tblLTKTKoreksi.Visible = True
        End If
    End Sub



#End Region

#Region "events..."

    Protected Sub cboTipeLaporan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipeLaporan.SelectedIndexChanged
        Try
            tipeLaporanCheck()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub rblTerlaporTipePelapor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblTerlaporTipePelapor.SelectedIndexChanged
        Try
            LTKTTipeTerlaporChange()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                SetControlLoad()
                clearSession()
                loadLTKTToField()
                loadResume()
                If MultiView1.ActiveViewIndex = 0 Then
                    ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('IdTerlapor','Img1','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
                    ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('Transaksi','Img2','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
                End If
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Menu1_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs) Handles Menu1.MenuItemClick
        Try
            MultiView1.ActiveViewIndex = (Menu1.SelectedValue)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub rblTRXKMTipePelapor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblTRXKMTipePelapor.SelectedIndexChanged
        Try
            If rblTRXKMTipePelapor.SelectedValue = 1 Then
                tblTRXKMTipePelapor.Visible = True
                tblTRXKMTipePelaporKorporasi.Visible = False
            Else
                tblTRXKMTipePelapor.Visible = False
                tblTRXKMTipePelaporKorporasi.Visible = True
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub rblTRXKKTipePelapor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblTRXKKTipePelapor.SelectedIndexChanged
        Try
            If rblTRXKKTipePelapor.SelectedValue = 1 Then
                tblTRXKKTipePelaporPerorangan.Visible = True
                tblTRXKKTipePelaporKorporasi.Visible = False
            Else
                tblTRXKKTipePelaporPerorangan.Visible = False
                tblTRXKKTipePelaporKorporasi.Visible = True
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub txtTRXKMDetilKasMasuk_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTRXKMDetilKasMasuk.TextChanged
        Try
            'If IsNumeric(txtTRXKMDetilKasMasuk.Text) = False Then
            '    lblTRXKMDetilValutaAsingJumlahRp.Text = ValidateBLL.FormatMoneyWithComma(JumlahKeseluruhanRp(SetnGetgrvTRXKMDetilValutaAsing))
            '    Throw New Exception("Kas Masuk must be filled by number")
            'End If

            'lblTRXKMDetilValutaAsingJumlahRp.Text = ValidateBLL.FormatMoneyWithComma(CDec(txtTRXKMDetilKasMasuk.Text) + JumlahKeseluruhanRp(SetnGetgrvTRXKMDetilValutaAsing))

        Catch ex As Exception
            ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('IdTerlapor','Img1','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
            ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('Umum','searchimage2','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message.ToString
        End Try
    End Sub



    Protected Sub txtTRXKKDetailKasKeluar_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTRXKKDetailKasKeluar.TextChanged
        Try
            'If IsNumeric(txtTRXKKDetailKasKeluar.Text) = False Then
            '    lblDetilKasKeluarJumlahRp.Text = JumlahKeseluruhanRp(SetnGetgrvDetilKasKeluar).ToString
            '    Throw New Exception("Kas Keluar must be filled by number")
            'End If

            'lblDetilKasKeluarJumlahRp.Text = ValidateBLL.FormatMoneyWithComma(CDec(txtTRXKKDetailKasKeluar.Text) + JumlahKeseluruhanRp(SetnGetgrvDetilKasKeluar))

        Catch ex As Exception
            ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('IdTerlapor','Img1','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
            ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('Umum','searchimage2','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message.ToString
        End Try
    End Sub



    Protected Sub grvDetilKasKeluar_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grvDetilKasKeluar.RowDataBound
        Try
            If e.Row.RowType <> ListItemType.Footer And e.Row.RowType <> ListItemType.Header Then
                e.Row.Cells(0).Text = e.Row.RowIndex + 1
                If e.Row.Cells.Count > 2 Then
                    e.Row.Cells(4).Text = ValidateBLL.FormatMoneyWithComma(e.Row.Cells(4).Text)
                End If
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub BtnDeletegrvDetilKasKeluar_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim objgridviewrow As GridViewRow = CType(CType(sender, LinkButton).NamingContainer, GridViewRow)
            Dim objTemp As List(Of LTKTDetailCashInEdit) = SetnGetgrvDetilKasKeluar
            objTemp.RemoveAt(objgridviewrow.RowIndex)
            grvDetilKasKeluar.DataSource = objTemp
            grvDetilKasKeluar.DataBind()

            lblDetilKasKeluarJumlahRp.Text = ValidateBLL.FormatMoneyWithComma(JumlahKeseluruhanRp(SetnGetgrvDetilKasKeluar))

        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub BtngrvDetailValutaAsingDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim objgridviewrow As GridViewRow = CType(CType(sender, LinkButton).NamingContainer, GridViewRow)
            Dim objTemp As List(Of LTKTDetailCashInEdit) = SetnGetgrvTRXKMDetilValutaAsing
            objTemp.RemoveAt(objgridviewrow.RowIndex)
            grvTRXKMDetilValutaAsing.DataSource = objTemp
            grvTRXKMDetilValutaAsing.DataBind()
            Try
                'If IsNumeric(txtTRXKMDetilKasMasuk.Text) = False Then
                '    lblTRXKMDetilValutaAsingJumlahRp.Text = ValidateBLL.FormatMoneyWithComma(JumlahKeseluruhanRp(SetnGetgrvTRXKMDetilValutaAsing))
                '    Throw New Exception("Kas Masuk must be filled by number")
                'End If
                Dim total As Double = 0
                For Each item As LTKTDetailCashInEdit In objTemp
                    total += item.JumlahRp

                Next



                lblTRXKMDetilValutaAsingJumlahRp.Text = ValidateBLL.FormatMoneyWithComma(JumlahKeseluruhanRp(SetnGetgrvTRXKMDetilValutaAsing))

            Catch ex As Exception
                ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('IdTerlapor','Img1','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
                ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('Umum','searchimage2','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
                Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
                cvalPageErr.IsValid = False
                cvalPageErr.ErrorMessage = ex.Message.ToString
            End Try
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub grvTRXKMDetilValutaAsing_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grvTRXKMDetilValutaAsing.RowDataBound
        Try
            If e.Row.RowType <> ListItemType.Footer And e.Row.RowType <> ListItemType.Header Then
                e.Row.Cells(0).Text = e.Row.RowIndex + 1
                If e.Row.Cells.Count > 2 Then
                    e.Row.Cells(4).Text = ValidateBLL.FormatMoneyWithComma(e.Row.Cells(4).Text)
                End If
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Try
            If isvalidData() Then
                Dim defaultDate As New Date(1900, 1, 1)
                Dim objResume As List(Of ResumeKasMasukKeluarLTKTAdd) = SetnGetResumeKasMasukKasKeluar
                'Buat LTKT Approval
                Using objLTKTApproval As New LTKT_Approval
                    objLTKTApproval.RequestedBy = Sahassa.AML.Commonly.SessionPkUserId
                    objLTKTApproval.RequestedDate = Now()
                    objLTKTApproval.FK_MsMode_Id = 2
                    objLTKTApproval.IsUpload = 0

                    DataRepository.LTKT_ApprovalProvider.Save(objLTKTApproval)
                    'Ini dpake terus
                    'Dim objAuditTrailType As MsAuditTrailEntityType = DataRepository.MsAuditTrailEntityTypeProvider.GetByPk_MsAuditTrailEntityTypeId(18)
                    AuditTrailBLL.InsertAuditTrail(AuditTrailBLL.AuditTrailOperation.Edit, Date.Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Request for approval add new data LTKT", objLTKTApproval, Nothing)


                    'LTKT Detil Approval
                    Using objLTKTDetilApproval As New LTKT_ApprovalDetail
                        FillOrNothing(objLTKTDetilApproval.FK_LTKT_Approval_Id, objLTKTApproval.PK_LTKT_Approval_Id, True, oInt)
                        FillOrNothing(objLTKTDetilApproval.PK_LTKT_Id, getLTKTPK, True, OLong)
                        FillOrNothing(objLTKTDetilApproval.NamaPJKPelapor, txtUmumPJKPelapor.Text)
                        FillOrNothing(objLTKTDetilApproval.TanggalLaporan, txtTglLaporan.Text, True, oDate)
                        FillOrNothing(objLTKTDetilApproval.NamaPejabatPJKPelapor, txtPejabatPelapor.Text)
                        FillOrNothing(objLTKTDetilApproval.JenisLaporan, cboTipeLaporan.SelectedIndex.ToString)
                        FillOrNothing(objLTKTDetilApproval.NoLTKTKoreksi, txtNoLTKTKoreksi.Text)
                        FillOrNothing(objLTKTDetilApproval.InformasiLainnya, txtLTKTInformasiLainnya.Text)
                        FillOrNothing(objLTKTDetilApproval.FK_MsKepemilikan_ID, cboTerlaporKepemilikan.SelectedValue, True, oInt)
                        FillOrNothing(objLTKTDetilApproval.NoRekening, txtTerlaporNoRekening.Text)
                        FillOrNothing(objLTKTDetilApproval.CIFNo, txtCIFNo.Text)
                        FillOrNothing(objLTKTDetilApproval.TipeTerlapor, rblTerlaporTipePelapor.SelectedValue, True, oByte)
                        FillOrNothing(objLTKTDetilApproval.INDV_Gelar, txtTerlaporGelar.Text)
                        FillOrNothing(objLTKTDetilApproval.INDV_NamaLengkap, txtTerlaporNamaLengkap.Text)
                        FillOrNothing(objLTKTDetilApproval.INDV_TempatLahir, txtTerlaporTempatLahir.Text)
                        FillOrNothing(objLTKTDetilApproval.INDV_TanggalLahir, txtTerlaporTglLahir.Text, True, oDate)
                        FillOrNothing(objLTKTDetilApproval.INDV_Kewarganegaraan, rblTerlaporKewarganegaraan.SelectedValue.ToString)
                        FillOrNothing(objLTKTDetilApproval.INDV_FK_MsNegara_Id, cboTerlaporNegara.SelectedValue, True, oInt)
                        FillOrNothing(objLTKTDetilApproval.INDV_DOM_NamaJalan, txtTerlaporDOMNamaJalan.Text)
                        FillOrNothing(objLTKTDetilApproval.INDV_DOM_RTRW, txtTerlaporDOMRTRW.Text)
                        FillOrNothing(objLTKTDetilApproval.INDV_DOM_FK_MsKelurahan_Id, hfTerlaporDOMKelurahan.Value, True, OLong)
                        FillOrNothing(objLTKTDetilApproval.INDV_DOM_FK_MsKecamatan_Id, hfTerlaporDOMKecamatan.Value, True, oInt)
                        FillOrNothing(objLTKTDetilApproval.INDV_DOM_FK_MsKotaKab_Id, hfTerlaporDOMKotaKab.Value, True, oInt)
                        FillOrNothing(objLTKTDetilApproval.INDV_DOM_KodePos, txtTerlaporDOMKodePos.Text)
                        FillOrNothing(objLTKTDetilApproval.INDV_DOM_FK_MsProvince_Id, hfTerlaporDOMProvinsi.Value, True, oInt)
                        FillOrNothing(objLTKTDetilApproval.INDV_DOM_FK_MsNegara_Id, hfTerlaporDomNegara.Value, True, oInt)

                        FillOrNothing(objLTKTDetilApproval.INDV_ID_NamaJalan, txtTerlaporIDNamaJalan.Text)
                        FillOrNothing(objLTKTDetilApproval.INDV_ID_RTRW, txtTerlaporIDRTRW.Text)
                        FillOrNothing(objLTKTDetilApproval.INDV_ID_FK_MsKelurahan_Id, hfTerlaporIDKelurahan.Value, True, OLong)
                        FillOrNothing(objLTKTDetilApproval.INDV_ID_FK_MsKecamatan_Id, hfTerlaporIDKecamatan.Value, True, oInt)
                        FillOrNothing(objLTKTDetilApproval.INDV_ID_FK_MsKotaKab_Id, hfTerlaporIDKotaKab.Value, True, oInt)
                        FillOrNothing(objLTKTDetilApproval.INDV_ID_KodePos, txtTerlaporIDKodePos.Text)
                        FillOrNothing(objLTKTDetilApproval.INDV_ID_FK_MsProvince_Id, hfTerlaporIDProvinsi.Value, True, oInt)
                        FillOrNothing(objLTKTDetilApproval.INDV_ID_FK_MsNegara_Id, hfTerlaporIDNegara.Value, True, oInt)

                        FillOrNothing(objLTKTDetilApproval.INDV_NA_NamaJalan, txtTerlaporNANamaJalan.Text)

                        FillOrNothing(objLTKTDetilApproval.INDV_NA_FK_MsNegara_Id, hfTerlaporIDNANegara.Value, True, oInt)

                        

                        FillOrNothing(objLTKTDetilApproval.INDV_NA_FK_MsProvince_Id, hfTerlaporNAProvinsi.Value, True, oInt)
                        FillOrNothing(objLTKTDetilApproval.INDV_NA_FK_MsKotaKab_Id, hfTerlaporIDKota.Value, True, oInt)
                        FillOrNothing(objLTKTDetilApproval.INDV_NA_KodePos, txtTerlaporNAKodePos.Text)
                        FillOrNothing(objLTKTDetilApproval.INDV_FK_MsIDType_Id, cboTerlaporJenisDocID.SelectedValue, True, oInt)
                        FillOrNothing(objLTKTDetilApproval.INDV_NomorId, txtTerlaporNomorID.Text)
                        FillOrNothing(objLTKTDetilApproval.INDV_NPWP, txtTerlaporNPWP.Text)
                        FillOrNothing(objLTKTDetilApproval.INDV_FK_MsPekerjaan_Id, hfTerlaporPekerjaan.Value, True, oInt)
                        FillOrNothing(objLTKTDetilApproval.INDV_Jabatan, txtTerlaporJabatan.Text)
                        FillOrNothing(objLTKTDetilApproval.INDV_PenghasilanRataRata, txtTerlaporPenghasilanRataRata.Text)
                        FillOrNothing(objLTKTDetilApproval.INDV_TempatBekerja, txtTerlaporTempatKerja.Text)

                        'corp
                        If cboTerlaporCORPBentukBadanUsaha.SelectedIndex <> 0 Then
                            FillOrNothing(objLTKTDetilApproval.CORP_FK_MsBentukBadanUsaha_Id, cboTerlaporCORPBentukBadanUsaha.SelectedValue, True, oInt)
                        End If

                        FillOrNothing(objLTKTDetilApproval.CORP_Nama, txtTerlaporCORPNama.Text)
                        FillOrNothing(objLTKTDetilApproval.CORP_FK_MsBidangUsaha_Id, hfTerlaporCORPBidangUsaha.Value, True, oInt)

                        If rblTerlaporCORPTipeAlamat.SelectedIndex <> 0 Then
                            FillOrNothing(objLTKTDetilApproval.CORP_TipeAlamat, rblTerlaporCORPTipeAlamat.SelectedIndex, True, oByte)
                        End If

                        FillOrNothing(objLTKTDetilApproval.CORP_NamaJalan, txtTerlaporCORPDLNamaJalan.Text)
                        FillOrNothing(objLTKTDetilApproval.CORP_RTRW, txtTerlaporCORPDLRTRW.Text)
                        FillOrNothing(objLTKTDetilApproval.CORP_FK_MsKelurahan_Id, hfTerlaporCORPDLKelurahan.Value, True, OLong)
                        FillOrNothing(objLTKTDetilApproval.CORP_FK_MsKecamatan_Id, hfTerlaporCORPDLKecamatan.Value, True, oInt)
                        FillOrNothing(objLTKTDetilApproval.CORP_FK_MsKotaKab_Id, hfTerlaporCORPDLKotaKab.Value, True, oInt)
                        FillOrNothing(objLTKTDetilApproval.CORP_KodePos, txtTerlaporCORPDLKodePos.Text)
                        FillOrNothing(objLTKTDetilApproval.CORP_FK_MsProvince_Id, hfTerlaporCORPDLProvinsi.Value, True, oInt)

                        FillOrNothing(objLTKTDetilApproval.CORP_FK_MsNegara_Id, hfTerlaporCORPDLNegara.Value, True, oInt)



                        FillOrNothing(objLTKTDetilApproval.CORP_LN_NamaJalan, txtTerlaporCORPLNNamaJalan.Text)

                        FillOrNothing(objLTKTDetilApproval.CORP_LN_FK_MsNegara_Id, hfTerlaporCORPLNNegara.Value, True, oInt)

                        

                        FillOrNothing(objLTKTDetilApproval.CORP_LN_FK_MsProvince_Id, hfTerlaporCORPLNProvinsi.Value, True, oInt)
                        FillOrNothing(objLTKTDetilApproval.CORP_LN_MsKotaKab_Id, hfTerlaporCORPLNKota.Value, True, oInt)
                        FillOrNothing(objLTKTDetilApproval.CORP_LN_KodePos, txtTerlaporCORPLNKodePos.Text)
                        FillOrNothing(objLTKTDetilApproval.CORP_NPWP, txtTerlaporCORPNPWP.Text)
                        FillOrNothing(objLTKTDetilApproval.CreatedBy, Sahassa.AML.Commonly.SessionUserId)
                        FillOrNothing(objLTKTDetilApproval.CreatedDate, Now(), True, oDate)

                        DataRepository.LTKT_ApprovalDetailProvider.Save(objLTKTDetilApproval)
                        AuditTrailBLL.InsertAuditTrail(AuditTrailBLL.AuditTrailOperation.Edit, Date.Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Request for approval add new data LTKT Detail", objLTKTDetilApproval, Nothing)
                        For Each objSingleResume As ResumeKasMasukKeluarLTKTAdd In objResume


                            'transaction approval detail
                            If objSingleResume.Kas = "Kas Masuk" Then
                                If Me.cboDebitCredit.SelectedValue = "D" Then
                                    Throw New Exception("Only Kas Keluar (Debet) are allowed")
                                End If
                                Using objLTKTTransactionCashInApproval As New LTKTTransactionCashIn_ApprovalDetail
                                    FillOrNothing(objLTKTTransactionCashInApproval.FK_LTKT_ApprovalDetail_Id, objLTKTDetilApproval.PK_LTKT_ApprovalDetail_Id)
                                    FillOrNothing(objLTKTTransactionCashInApproval.PK_LTKTTransactionCashIn_Id, objSingleResume.TransactionCashIn.PK_LTKTTransactionCashIn_Id)
                                    FillOrNothing(objLTKTTransactionCashInApproval.FK_LTKT_Id, getLTKTPK)

                                    FillOrNothing(objLTKTTransactionCashInApproval.TanggalTransaksi, objSingleResume.TransactionCashIn.TanggalTransaksi)
                                    FillOrNothing(objLTKTTransactionCashInApproval.NamaKantorPJK, objSingleResume.TransactionCashIn.NamaKantorPJK)
                                    FillOrNothing(objLTKTTransactionCashInApproval.FK_MsKotaKab_Id, objSingleResume.TransactionCashIn.FK_MsKotaKab_Id)
                                    FillOrNothing(objLTKTTransactionCashInApproval.FK_MsProvince_Id, objSingleResume.TransactionCashIn.FK_MsProvince_Id)
                                    FillOrNothing(objLTKTTransactionCashInApproval.NomorRekening, objSingleResume.TransactionCashIn.NomorRekening)
                                    FillOrNothing(objLTKTTransactionCashInApproval.TipeTerlapor, objSingleResume.TransactionCashIn.TipeTerlapor)
                                    'Individu
                                    FillOrNothing(objLTKTTransactionCashInApproval.INDV_Gelar, objSingleResume.TransactionCashIn.INDV_Gelar)
                                    FillOrNothing(objLTKTTransactionCashInApproval.INDV_NamaLengkap, objSingleResume.TransactionCashIn.INDV_NamaLengkap)
                                    FillOrNothing(objLTKTTransactionCashInApproval.INDV_TempatLahir, objSingleResume.TransactionCashIn.INDV_TempatLahir)
                                    FillOrNothing(objLTKTTransactionCashInApproval.INDV_TanggalLahir, objSingleResume.TransactionCashIn.INDV_TanggalLahir)
                                    FillOrNothing(objLTKTTransactionCashInApproval.INDV_Kewarganegaraan, objSingleResume.TransactionCashIn.INDV_Kewarganegaraan)
                                    FillOrNothing(objLTKTTransactionCashInApproval.INDV_FK_MsNegara_Id, objSingleResume.TransactionCashIn.INDV_FK_MsNegara_Id)
                                    FillOrNothing(objLTKTTransactionCashInApproval.INDV_DOM_NamaJalan, objSingleResume.TransactionCashIn.INDV_DOM_NamaJalan)
                                    FillOrNothing(objLTKTTransactionCashInApproval.INDV_DOM_RTRW, objSingleResume.TransactionCashIn.INDV_DOM_RTRW)
                                    FillOrNothing(objLTKTTransactionCashInApproval.INDV_DOM_FK_MsKelurahan_Id, objSingleResume.TransactionCashIn.INDV_DOM_FK_MsKelurahan_Id)
                                    FillOrNothing(objLTKTTransactionCashInApproval.INDV_DOM_FK_MsKecamatan_Id, objSingleResume.TransactionCashIn.INDV_DOM_FK_MsKecamatan_Id)
                                    FillOrNothing(objLTKTTransactionCashInApproval.INDV_DOM_FK_MsKotaKab_Id, objSingleResume.TransactionCashIn.INDV_DOM_FK_MsKotaKab_Id)
                                    FillOrNothing(objLTKTTransactionCashInApproval.INDV_DOM_KodePos, objSingleResume.TransactionCashIn.INDV_DOM_KodePos)
                                    FillOrNothing(objLTKTTransactionCashInApproval.INDV_DOM_FK_MsProvince_Id, objSingleResume.TransactionCashIn.INDV_DOM_FK_MsProvince_Id)
                                    FillOrNothing(objLTKTTransactionCashInApproval.INDV_DOM_FK_MsNegara_Id, objSingleResume.TransactionCashIn.INDV_DOM_FK_MsNegara_Id)


                                    FillOrNothing(objLTKTTransactionCashInApproval.INDV_ID_NamaJalan, objSingleResume.TransactionCashIn.INDV_ID_NamaJalan)
                                    FillOrNothing(objLTKTTransactionCashInApproval.INDV_ID_RTRW, objSingleResume.TransactionCashIn.INDV_ID_RTRW)
                                    FillOrNothing(objLTKTTransactionCashInApproval.INDV_ID_FK_MsKelurahan_Id, objSingleResume.TransactionCashIn.INDV_ID_FK_MsKelurahan_Id)
                                    FillOrNothing(objLTKTTransactionCashInApproval.INDV_ID_FK_MsKecamatan_Id, objSingleResume.TransactionCashIn.INDV_ID_FK_MsKecamatan_Id)
                                    FillOrNothing(objLTKTTransactionCashInApproval.INDV_ID_FK_MsKotaKab_Id, objSingleResume.TransactionCashIn.INDV_ID_FK_MsKotaKab_Id)
                                    FillOrNothing(objLTKTTransactionCashInApproval.INDV_ID_KodePos, objSingleResume.TransactionCashIn.INDV_ID_KodePos)
                                    FillOrNothing(objLTKTTransactionCashInApproval.INDV_ID_FK_MsProvince_Id, objSingleResume.TransactionCashIn.INDV_ID_FK_MsProvince_Id)
                                    FillOrNothing(objLTKTTransactionCashInApproval.INDV_ID_FK_MsNegara_Id, objSingleResume.TransactionCashIn.INDV_ID_FK_MsNegara_Id)

                                    FillOrNothing(objLTKTTransactionCashInApproval.INDV_NA_NamaJalan, objSingleResume.TransactionCashIn.INDV_NA_NamaJalan)
                                    FillOrNothing(objLTKTTransactionCashInApproval.INDV_NA_FK_MsNegara_Id, objSingleResume.TransactionCashIn.INDV_NA_FK_MsNegara_Id)
                                    FillOrNothing(objLTKTTransactionCashInApproval.INDV_NA_FK_MsProvince_Id, objSingleResume.TransactionCashIn.INDV_NA_FK_MsProvince_Id)
                                    FillOrNothing(objLTKTTransactionCashInApproval.INDV_NA_FK_MsKotaKab_Id, objSingleResume.TransactionCashIn.INDV_NA_FK_MsKotaKab_Id)
                                    FillOrNothing(objLTKTTransactionCashInApproval.INDV_NA_KodePos, objSingleResume.TransactionCashIn.INDV_NA_KodePos)
                                    FillOrNothing(objLTKTTransactionCashInApproval.INDV_FK_MsIDType_Id, objSingleResume.TransactionCashIn.INDV_FK_MsIDType_Id)
                                    FillOrNothing(objLTKTTransactionCashInApproval.INDV_NomorId, objSingleResume.TransactionCashIn.INDV_NomorId)
                                    FillOrNothing(objLTKTTransactionCashInApproval.INDV_NPWP, objSingleResume.TransactionCashIn.INDV_NPWP)
                                    FillOrNothing(objLTKTTransactionCashInApproval.INDV_FK_MsPekerjaan_Id, objSingleResume.TransactionCashIn.INDV_FK_MsPekerjaan_Id)
                                    FillOrNothing(objLTKTTransactionCashInApproval.INDV_Jabatan, objSingleResume.TransactionCashIn.INDV_Jabatan)
                                    FillOrNothing(objLTKTTransactionCashInApproval.INDV_PenghasilanRataRata, objSingleResume.TransactionCashIn.INDV_PenghasilanRataRata)
                                    FillOrNothing(objLTKTTransactionCashInApproval.INDV_TempatBekerja, objSingleResume.TransactionCashIn.INDV_TempatBekerja)
                                    FillOrNothing(objLTKTTransactionCashInApproval.INDV_TujuanTransaksi, objSingleResume.TransactionCashIn.INDV_TujuanTransaksi)
                                    FillOrNothing(objLTKTTransactionCashInApproval.INDV_SumberDana, objSingleResume.TransactionCashIn.INDV_SumberDana)
                                    FillOrNothing(objLTKTTransactionCashInApproval.INDV_NamaBankLain, objSingleResume.TransactionCashIn.INDV_NamaBankLain)
                                    FillOrNothing(objLTKTTransactionCashInApproval.INDV_NomorRekeningTujuan, objSingleResume.TransactionCashIn.INDV_NomorRekeningTujuan)

                                    'corp
                                    FillOrNothing(objLTKTTransactionCashInApproval.CORP_FK_MsBentukBadanUsaha_Id, objSingleResume.TransactionCashIn.CORP_FK_MsBentukBadanUsaha_Id)
                                    FillOrNothing(objLTKTTransactionCashInApproval.CORP_Nama, objSingleResume.TransactionCashIn.CORP_Nama)
                                    FillOrNothing(objLTKTTransactionCashInApproval.CORP_FK_MsBidangUsaha_Id, objSingleResume.TransactionCashIn.CORP_FK_MsBidangUsaha_Id)
                                    FillOrNothing(objLTKTTransactionCashInApproval.CORP_TipeAlamat, objSingleResume.TransactionCashIn.CORP_TipeAlamat)
                                    FillOrNothing(objLTKTTransactionCashInApproval.CORP_NamaJalan, objSingleResume.TransactionCashIn.CORP_NamaJalan)
                                    FillOrNothing(objLTKTTransactionCashInApproval.CORP_RTRW, objSingleResume.TransactionCashIn.CORP_RTRW)
                                    FillOrNothing(objLTKTTransactionCashInApproval.CORP_FK_MsKelurahan_Id, objSingleResume.TransactionCashIn.CORP_FK_MsKelurahan_Id)
                                    FillOrNothing(objLTKTTransactionCashInApproval.CORP_FK_MsKecamatan_Id, objSingleResume.TransactionCashIn.CORP_FK_MsKecamatan_Id)
                                    FillOrNothing(objLTKTTransactionCashInApproval.CORP_FK_MsKotaKab_Id, objSingleResume.TransactionCashIn.CORP_FK_MsKotaKab_Id)
                                    FillOrNothing(objLTKTTransactionCashInApproval.CORP_KodePos, objSingleResume.TransactionCashIn.CORP_KodePos)
                                    FillOrNothing(objLTKTTransactionCashInApproval.CORP_FK_MsProvince_Id, objSingleResume.TransactionCashIn.CORP_FK_MsProvince_Id)
                                    FillOrNothing(objLTKTTransactionCashInApproval.CORP_FK_MsNegara_Id, objSingleResume.TransactionCashIn.CORP_FK_MsNegara_Id)
                                    FillOrNothing(objLTKTTransactionCashInApproval.CORP_LN_NamaJalan, objSingleResume.TransactionCashIn.CORP_LN_NamaJalan)
                                    FillOrNothing(objLTKTTransactionCashInApproval.CORP_LN_FK_MsNegara_Id, objSingleResume.TransactionCashIn.CORP_LN_FK_MsNegara_Id)
                                    FillOrNothing(objLTKTTransactionCashInApproval.CORP_LN_FK_MsProvince_Id, objSingleResume.TransactionCashIn.CORP_LN_FK_MsProvince_Id)
                                    FillOrNothing(objLTKTTransactionCashInApproval.CORP_LN_MsKotaKab_Id, objSingleResume.TransactionCashIn.CORP_LN_MsKotaKab_Id)
                                    FillOrNothing(objLTKTTransactionCashInApproval.CORP_LN_KodePos, objSingleResume.TransactionCashIn.CORP_LN_KodePos)
                                    FillOrNothing(objLTKTTransactionCashInApproval.CORP_NPWP, objSingleResume.TransactionCashIn.CORP_NPWP)
                                    FillOrNothing(objLTKTTransactionCashInApproval.CORP_TujuanTransaksi, objSingleResume.TransactionCashIn.CORP_TujuanTransaksi)
                                    FillOrNothing(objLTKTTransactionCashInApproval.CORP_SumberDana, objSingleResume.TransactionCashIn.CORP_SumberDana)
                                    FillOrNothing(objLTKTTransactionCashInApproval.CORP_NamaBankLain, objSingleResume.TransactionCashIn.CORP_NamaBankLain)
                                    FillOrNothing(objLTKTTransactionCashInApproval.CORP_NomorRekeningTujuan, objSingleResume.TransactionCashIn.CORP_NomorRekeningTujuan)

                                    FillOrNothing(objLTKTTransactionCashInApproval.Total, objSingleResume.TransactionNominal, True, oDecimal)
                                    FillOrNothing(objLTKTTransactionCashInApproval.CreatedBy, Sahassa.AML.Commonly.SessionUserId)
                                    FillOrNothing(objLTKTTransactionCashInApproval.CreatedDate, Now())
                                    DataRepository.LTKTTransactionCashIn_ApprovalDetailProvider.Save(objLTKTTransactionCashInApproval)
                                    AuditTrailBLL.InsertAuditTrail(AuditTrailBLL.AuditTrailOperation.Edit, Date.Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Request for approval add new data LTKT Transaction CashIn", objLTKTTransactionCashInApproval, Nothing)

                                    'Insert Detail Transaction Approval
                                    For Each objSingleDetail As LTKTDetailCashInTransaction In objSingleResume.DetailTransactionCashIn
                                        Using objLTKTDetailTransactionCashInApproval As New LTKTDetailCashInTransaction_ApprovalDetail
                                            FillOrNothing(objLTKTDetailTransactionCashInApproval.FK_LTKTTransactionCashIn_ApprovalDetail_Id, objLTKTTransactionCashInApproval.PK_LTKTTransactionCashIn_ApprovalDetail_Id)
                                            FillOrNothing(objLTKTDetailTransactionCashInApproval.PK_LTKTDetailCashInTransaction_Id, objSingleDetail.PK_LTKTDetailCashInTransaction_Id)
                                            FillOrNothing(objLTKTDetailTransactionCashInApproval.KasMasuk, objSingleDetail.KasMasuk)
                                            FillOrNothing(objLTKTDetailTransactionCashInApproval.Asing_FK_MsCurrency_Id, objSingleDetail.Asing_FK_MsCurrency_Id)
                                            FillOrNothing(objLTKTDetailTransactionCashInApproval.Asing_KursTransaksi, objSingleDetail.Asing_KursTransaksi)
                                            FillOrNothing(objLTKTDetailTransactionCashInApproval.Asing_TotalKasMasukDalamRupiah, objSingleDetail.Asing_TotalKasMasukDalamRupiah)
                                            FillOrNothing(objLTKTDetailTransactionCashInApproval.TotalKasMasuk, objSingleDetail.TotalKasMasuk)

                                            DataRepository.LTKTDetailCashInTransaction_ApprovalDetailProvider.Save(objLTKTDetailTransactionCashInApproval)
                                            AuditTrailBLL.InsertAuditTrail(AuditTrailBLL.AuditTrailOperation.Edit, Date.Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Request for approval add new data LTKT Transaction CashIn Detail ", objLTKTDetailTransactionCashInApproval, Nothing)
                                        End Using
                                    Next


                                End Using
                            Else
                                If Me.cboDebitCredit.SelectedValue = "C" Then
                                    Throw New Exception("Only Kas Masuk (Credit) are allowed")
                                End If
                                Using objLTKTTransactionCashOutApproval As New LTKTTransactionCashOut_ApprovalDetail
                                    FillOrNothing(objLTKTTransactionCashOutApproval.FK_LTKT_ApprovalDetail_Id, objLTKTDetilApproval.PK_LTKT_ApprovalDetail_Id)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.PK_LTKTTransactionCashOut_Id, objSingleResume.TransactionCashOut.PK_LTKTTransactionCashOut_Id)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.FK_LTKT_Id, getLTKTPK)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.TanggalTransaksi, objSingleResume.TransactionCashOut.TanggalTransaksi)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.NamaKantorPJK, objSingleResume.TransactionCashOut.NamaKantorPJK)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.FK_MsKotaKab_Id, objSingleResume.TransactionCashOut.FK_MsKotaKab_Id)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.FK_MsProvince_Id, objSingleResume.TransactionCashOut.FK_MsProvince_Id)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.NomorRekening, objSingleResume.TransactionCashOut.NomorRekening)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.TipeTerlapor, objSingleResume.TransactionCashOut.TipeTerlapor)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.INDV_Gelar, objSingleResume.TransactionCashOut.INDV_Gelar)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.INDV_NamaLengkap, objSingleResume.TransactionCashOut.INDV_NamaLengkap)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.INDV_TempatLahir, objSingleResume.TransactionCashOut.INDV_TempatLahir)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.INDV_TanggalLahir, objSingleResume.TransactionCashOut.INDV_TanggalLahir)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.INDV_Kewarganegaraan, objSingleResume.TransactionCashOut.INDV_Kewarganegaraan)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.INDV_FK_MsNegara_Id, objSingleResume.TransactionCashOut.INDV_FK_MsNegara_Id)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.INDV_DOM_NamaJalan, objSingleResume.TransactionCashOut.INDV_DOM_NamaJalan)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.INDV_DOM_RTRW, objSingleResume.TransactionCashOut.INDV_DOM_RTRW)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.INDV_DOM_FK_MsKelurahan_Id, objSingleResume.TransactionCashOut.INDV_DOM_FK_MsKelurahan_Id)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.INDV_DOM_FK_MsKecamatan_Id, objSingleResume.TransactionCashOut.INDV_DOM_FK_MsKecamatan_Id)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.INDV_DOM_FK_MsKotaKab_Id, objSingleResume.TransactionCashOut.INDV_DOM_FK_MsKotaKab_Id)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.INDV_DOM_KodePos, objSingleResume.TransactionCashOut.INDV_DOM_KodePos)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.INDV_DOM_FK_MsProvince_Id, objSingleResume.TransactionCashOut.INDV_DOM_FK_MsProvince_Id)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.INDV_DOM_FK_MsNegara_Id, objSingleResume.TransactionCashOut.INDV_DOM_FK_MsNegara_Id)

                                    FillOrNothing(objLTKTTransactionCashOutApproval.INDV_ID_NamaJalan, objSingleResume.TransactionCashOut.INDV_ID_NamaJalan)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.INDV_ID_RTRW, objSingleResume.TransactionCashOut.INDV_ID_RTRW)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.INDV_ID_FK_MsKelurahan_Id, objSingleResume.TransactionCashOut.INDV_ID_FK_MsKelurahan_Id)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.INDV_ID_FK_MsKecamatan_Id, objSingleResume.TransactionCashOut.INDV_ID_FK_MsKecamatan_Id)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.INDV_ID_FK_MsKotaKab_Id, objSingleResume.TransactionCashOut.INDV_ID_FK_MsKotaKab_Id)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.INDV_ID_KodePos, objSingleResume.TransactionCashOut.INDV_ID_KodePos)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.INDV_ID_FK_MsProvince_Id, objSingleResume.TransactionCashOut.INDV_ID_FK_MsProvince_Id)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.INDV_ID_FK_MsNegara_Id, objSingleResume.TransactionCashOut.INDV_ID_FK_MsNegara_Id)

                                    FillOrNothing(objLTKTTransactionCashOutApproval.INDV_NA_NamaJalan, objSingleResume.TransactionCashOut.INDV_NA_NamaJalan)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.INDV_NA_FK_MsNegara_Id, objSingleResume.TransactionCashOut.INDV_NA_FK_MsNegara_Id)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.INDV_NA_FK_MsProvince_Id, objSingleResume.TransactionCashOut.INDV_NA_FK_MsProvince_Id)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.INDV_NA_FK_MsKotaKab_Id, objSingleResume.TransactionCashOut.INDV_NA_FK_MsKotaKab_Id)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.INDV_NA_KodePos, objSingleResume.TransactionCashOut.INDV_NA_KodePos)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.INDV_FK_MsIDType_Id, objSingleResume.TransactionCashOut.INDV_FK_MsIDType_Id)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.INDV_NomorId, objSingleResume.TransactionCashOut.INDV_NomorId)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.INDV_NPWP, objSingleResume.TransactionCashOut.INDV_NPWP)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.INDV_FK_MsPekerjaan_Id, objSingleResume.TransactionCashOut.INDV_FK_MsPekerjaan_Id)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.INDV_Jabatan, objSingleResume.TransactionCashOut.INDV_Jabatan)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.INDV_PenghasilanRataRata, objSingleResume.TransactionCashOut.INDV_PenghasilanRataRata)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.INDV_TempatBekerja, objSingleResume.TransactionCashOut.INDV_TempatBekerja)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.INDV_TujuanTransaksi, objSingleResume.TransactionCashOut.INDV_TujuanTransaksi)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.INDV_SumberDana, objSingleResume.TransactionCashOut.INDV_SumberDana)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.INDV_NamaBankLain, objSingleResume.TransactionCashOut.INDV_NamaBankLain)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.INDV_NomorRekeningTujuan, objSingleResume.TransactionCashOut.INDV_NomorRekeningTujuan)

                                    'corp
                                    FillOrNothing(objLTKTTransactionCashOutApproval.CORP_FK_MsBentukBadanUsaha_Id, objSingleResume.TransactionCashOut.CORP_FK_MsBentukBadanUsaha_Id)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.CORP_Nama, objSingleResume.TransactionCashOut.CORP_Nama)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.CORP_FK_MsBidangUsaha_Id, objSingleResume.TransactionCashOut.CORP_FK_MsBidangUsaha_Id)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.CORP_TipeAlamat, objSingleResume.TransactionCashOut.CORP_TipeAlamat)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.CORP_NamaJalan, objSingleResume.TransactionCashOut.CORP_NamaJalan)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.CORP_RTRW, objSingleResume.TransactionCashOut.CORP_RTRW)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.CORP_FK_MsKelurahan_Id, objSingleResume.TransactionCashOut.CORP_FK_MsKelurahan_Id)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.CORP_FK_MsKecamatan_Id, objSingleResume.TransactionCashOut.CORP_FK_MsKecamatan_Id)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.CORP_FK_MsKotaKab_Id, objSingleResume.TransactionCashOut.CORP_FK_MsKotaKab_Id)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.CORP_KodePos, objSingleResume.TransactionCashOut.CORP_KodePos)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.CORP_FK_MsProvince_Id, objSingleResume.TransactionCashOut.CORP_FK_MsProvince_Id)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.CORP_FK_MsNegara_Id, objSingleResume.TransactionCashOut.CORP_FK_MsNegara_Id)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.CORP_LN_NamaJalan, objSingleResume.TransactionCashOut.CORP_LN_NamaJalan)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.CORP_LN_FK_MsNegara_Id, objSingleResume.TransactionCashOut.CORP_LN_FK_MsNegara_Id)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.CORP_LN_FK_MsProvince_Id, objSingleResume.TransactionCashOut.CORP_LN_FK_MsProvince_Id)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.CORP_LN_MsKotaKab_Id, objSingleResume.TransactionCashOut.CORP_LN_MsKotaKab_Id)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.CORP_LN_KodePos, objSingleResume.TransactionCashOut.CORP_LN_KodePos)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.CORP_NPWP, objSingleResume.TransactionCashOut.CORP_NPWP)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.CORP_TujuanTransaksi, objSingleResume.TransactionCashOut.CORP_TujuanTransaksi)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.CORP_SumberDana, objSingleResume.TransactionCashOut.CORP_SumberDana)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.CORP_NamaBankLain, objSingleResume.TransactionCashOut.CORP_NamaBankLain)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.CORP_NomorRekeningTujuan, objSingleResume.TransactionCashOut.CORP_NomorRekeningTujuan)


                                    FillOrNothing(objLTKTTransactionCashOutApproval.Total, objSingleResume.TransactionNominal, True, oDecimal)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.CreatedBy, Sahassa.AML.Commonly.SessionUserId)
                                    FillOrNothing(objLTKTTransactionCashOutApproval.CreatedDate, Now(), True, oDate)

                                    DataRepository.LTKTTransactionCashOut_ApprovalDetailProvider.Save(objLTKTTransactionCashOutApproval)
                                    AuditTrailBLL.InsertAuditTrail(AuditTrailBLL.AuditTrailOperation.Edit, Date.Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Request for approval add new data LTKT Transaction CashOut ", objLTKTTransactionCashOutApproval, Nothing)
                                    'Insert Detail Transaction Approval
                                    For Each objSingleDetail As LTKTDetailCashOutTransaction In objSingleResume.DetailTranscationCashOut
                                        Using objLTKTDetailTransactionCashOutApproval As New LTKTDetailCashOutTransaction_ApprovalDetail
                                            FillOrNothing(objLTKTDetailTransactionCashOutApproval.FK_LTKTTransactionCashOut_ApprovalDetail_Id, objLTKTTransactionCashOutApproval.PK_LTKTTransactionCashOut_ApprovalDetail_Id)
                                            FillOrNothing(objLTKTDetailTransactionCashOutApproval.PK_LTKTDetailCashOutTransaction_Id, objSingleDetail.PK_LTKTDetailCashOutTransaction_Id)
                                            FillOrNothing(objLTKTDetailTransactionCashOutApproval.KasKeluar, objSingleDetail.KasKeluar)
                                            FillOrNothing(objLTKTDetailTransactionCashOutApproval.Asing_FK_MsCurrency_Id, objSingleDetail.Asing_FK_MsCurrency_Id)
                                            FillOrNothing(objLTKTDetailTransactionCashOutApproval.Asing_KursTransaksi, objSingleDetail.Asing_KursTransaksi)
                                            FillOrNothing(objLTKTDetailTransactionCashOutApproval.Asing_TotalKasKeluarDalamRupiah, objSingleDetail.Asing_TotalKasKeluarDalamRupiah)
                                            FillOrNothing(objLTKTDetailTransactionCashOutApproval.TotalKasKeluar, objSingleDetail.TotalKasKeluar)

                                            DataRepository.LTKTDetailCashOutTransaction_ApprovalDetailProvider.Save(objLTKTDetailTransactionCashOutApproval)
                                            AuditTrailBLL.InsertAuditTrail(AuditTrailBLL.AuditTrailOperation.Edit, Date.Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Request for approval add new data LTKT Transaction CashOut Detail", objLTKTDetailTransactionCashOutApproval, Nothing)
                                        End Using
                                    Next

                                End Using
                            End If
                        Next
                    End Using
                End Using
                mtvPage.ActiveViewIndex = 1
                lblMsg.Text = "LTKT data has edited and wait for approval"
                hideSave()

                ''Kirim Email ke semua orang yang 1 group sama pembuat/pengedit/pendelete data

                'Dim ApproverUser As String = vbNullString
                'Dim EmailBodyData As String = vbNullString

                ''EmailBodyData = "Approval LTKT Edit (User Id : " & Sahassa.AML.Commonly.SessionNIK & "-" & Sahassa.AML.Commonly.SessionUserId & ")<br>Is need your approval"
                'EmailBodyData = EmailBLL.getEmailBodyTemplate("LTKT Edit", "LTKT")

                ''Using objMsUser As TList(Of User) = DataRepository.UserProvider.GetPaged(UserColumn.UserGroupId.ToString & " = " & Sahassa.AML.Commonly.SessionFkGroupApprovalId & " and MsUser_IsBankWide =  '" & Sahassa.AML.Commonly.SessionIsBankWide & "'", "", 0, Integer.MaxValue, 0)
                'Using objMsUser As TList(Of User) = DataRepository.UserProvider.GetPaged(UserColumn.UserGroupId.ToString & " = " & Sahassa.AML.Commonly.SessionGroupId & "'", "", 0, Integer.MaxValue, 0)
                '    If objMsUser.Count > 0 Then
                '        For Each singleObjMsUser As User In objMsUser
                '            If singleObjMsUser.pkUserID <> Sahassa.AML.Commonly.SessionPkUserId Then
                '                ApproverUser += singleObjMsUser.UserEmailAddress & ";"
                '            End If
                '        Next

                '        'Using CommonEmail As New Sahassa.AML.EmailSend
                '        '    'CommonEmail.Subject = "Approval LTKT Edit (User Id : " & Sahassa.AML.Commonly.SessionNIK & "-" & Sahassa.AML.Commonly.SessionUserId & ")"
                '        '    CommonEmail.Subject = "LTKT Edit"
                '        '    CommonEmail.Sender = "Cash Transaction Report Application"
                '        '    CommonEmail.Recipient = ApproverUser
                '        '    CommonEmail.Body = EmailBodyData
                '        '    CommonEmail.SendEmail()
                '        'End Using
                '    End If
                'End Using

            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message.ToString
        End Try
    End Sub

    Protected Sub chkCopyAlamatDOM_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkCopyAlamatDOM.CheckedChanged
        Try

            If chkCopyAlamatDOM.Checked = True Then
                txtTerlaporIDNamaJalan.Text = txtTerlaporDOMNamaJalan.Text
                txtTerlaporIDRTRW.Text = txtTerlaporDOMRTRW.Text
                txtTerlaporIDKelurahan.Text = txtTerlaporDOMKelurahan.Text
                txtTerlaporIDKecamatan.Text = txtTerlaporDOMKecamatan.Text
                txtTerlaporIDKotaKab.Text = txtTerlaporDOMKotaKab.Text
                txtTerlaporIDKodePos.Text = txtTerlaporDOMKodePos.Text
                txtTerlaporIDProvinsi.Text = txtTerlaporDOMProvinsi.Text
                txtTerlaporIDNegara.Text = txtTerlaporDOMNegara.Text


                hfTerlaporIDKelurahan.Value = hfTerlaporDOMKelurahan.Value
                hfTerlaporIDKecamatan.Value = hfTerlaporDOMKecamatan.Value
                hfTerlaporIDKotaKab.Value = hfTerlaporDOMKotaKab.Value
                hfTerlaporIDProvinsi.Value = hfTerlaporDOMProvinsi.Value
                hfTerlaporIDNegara.Value = hfTerlaporDomNegara.Value

                txtTerlaporIDNamaJalan.Enabled = False
                txtTerlaporIDRTRW.Enabled = False
                txtTerlaporIDKodePos.Enabled = False
            Else
                txtTerlaporIDNamaJalan.Text = ""
                txtTerlaporIDRTRW.Text = ""
                txtTerlaporIDKelurahan.Text = ""
                txtTerlaporIDKecamatan.Text = ""
                txtTerlaporIDKotaKab.Text = ""
                txtTerlaporIDKodePos.Text = ""
                txtTerlaporIDProvinsi.Text = ""
                txtTerlaporIDNegara.Text = ""

                hfTerlaporIDKelurahan.Value = ""
                hfTerlaporIDKecamatan.Value = ""
                hfTerlaporIDKotaKab.Value = ""
                hfTerlaporIDProvinsi.Value = ""
                hfTerlaporIDNegara.Value = ""

                txtTerlaporIDNamaJalan.Enabled = True
                txtTerlaporIDRTRW.Enabled = True
                txtTerlaporIDKodePos.Enabled = True
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub chkTRXKMINDVCopyDOM_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkTRXKMINDVCopyDOM.CheckedChanged
        Try

            If chkTRXKMINDVCopyDOM.Checked = True Then
                txtTRXKMINDVIDNamaJalan.Text = txtTRXKMINDVDOMNamaJalan.Text
                txtTRXKMINDVIDRTRW.Text = txtTRXKMINDVDOMRTRW.Text
                txtTRXKMINDVIDKelurahan.Text = txtTRXKMINDVDOMKelurahan.Text
                txtTRXKMINDVIDKecamatan.Text = txtTRXKMINDVDOMKecamatan.Text
                txtTRXKMINDVIDKotaKab.Text = txtTRXKMINDVDOMKotaKab.Text
                txtTRXKMINDVIDKodePos.Text = txtTRXKMINDVDOMKodePos.Text
                txtTRXKMINDVIDProvinsi.Text = txtTRXKMINDVDOMProvinsi.Text


                hfTRXKMINDVIDKelurahan.Value = hfTRXKMINDVDOMKelurahan.Value
                hfTRXKMINDVIDKecamatan.Value = hfTRXKMINDVDOMKecamatan.Value
                hfTRXKMINDVIDKotaKab.Value = hfTRXKMINDVDOMKotaKab.Value
                hfTRXKMINDVIDProvinsi.Value = hfTRXKMINDVDOMProvinsi.Value

                txtTRXKMINDVIDNamaJalan.Enabled = False
                txtTRXKMINDVIDRTRW.Enabled = False
                txtTRXKMINDVIDKodePos.Enabled = False
            Else
                txtTRXKMINDVIDNamaJalan.Text = ""
                txtTRXKMINDVIDRTRW.Text = ""
                txtTRXKMINDVIDKelurahan.Text = ""
                txtTRXKMINDVIDKecamatan.Text = ""
                txtTRXKMINDVIDKotaKab.Text = ""
                txtTRXKMINDVIDKodePos.Text = ""
                txtTRXKMINDVIDProvinsi.Text = ""

                hfTRXKMINDVIDKelurahan.Value = ""
                hfTRXKMINDVIDKecamatan.Value = ""
                hfTRXKMINDVIDKotaKab.Value = ""
                hfTRXKMINDVIDProvinsi.Value = ""

                txtTRXKMINDVIDNamaJalan.Enabled = True
                txtTRXKMINDVIDRTRW.Enabled = True
                txtTRXKMINDVIDKodePos.Enabled = True
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub chkTRXKKINDVIDCopyDOM_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkTRXKKINDVIDCopyDOM.CheckedChanged
        Try

            If chkTRXKKINDVIDCopyDOM.Checked = True Then
                txtTRXKKINDVIDNamaJalan.Text = txtTRXKKINDVDOMNamaJalan.Text
                txtTRXKKINDVIDRTRW.Text = txtTRXKKINDVDOMRTRW.Text
                txtTRXKKINDVIDKelurahan.Text = txtTRXKKINDVDOMKelurahan.Text
                txtTRXKKINDVIDKecamatan.Text = txtTRXKKINDVDOMKecamatan.Text
                txtTRXKKINDVIDKotaKab.Text = txtTRXKKINDVDOMKotaKab.Text
                txtTRXKKINDVIDKodePos.Text = txtTRXKKINDVDOMKodePos.Text
                txtTRXKKINDVIDProvinsi.Text = txtTRXKKINDVDOMProvinsi.Text
                txtTRXKKINDVIDNegara.Text = txtTRXKKINDVDOMNegara.Text

                hfTRXKKINDVIDKelurahan.Value = hfTRXKKINDVDOMKelurahan.Value
                hfTRXKKINDVIDKecamatan.Value = hfTRXKKINDVDOMKecamatan.Value
                hfTRXKKINDVIDKotaKab.Value = hfTRXKKINDVDOMKotaKab.Value
                hfTRXKKINDVIDProvinsi.Value = hfTRXKKINDVDOMProvinsi.Value
                hfTRXKKINDVIDNegara.Value = hfTRXKKINDVDOMNegara.Value

                txtTRXKKINDVIDNamaJalan.Enabled = False
                txtTRXKKINDVIDRTRW.Enabled = False
                txtTRXKKINDVIDKodePos.Enabled = False

            Else
                txtTRXKKINDVIDNamaJalan.Text = ""
                txtTRXKKINDVIDRTRW.Text = ""
                txtTRXKKINDVIDKelurahan.Text = ""
                txtTRXKKINDVIDKecamatan.Text = ""
                txtTRXKKINDVIDKotaKab.Text = ""
                txtTRXKKINDVIDKodePos.Text = ""
                txtTRXKKINDVIDProvinsi.Text = ""
                txtTRXKKINDVIDNegara.Text = ""

                hfTRXKKINDVIDKelurahan.Value = ""
                hfTRXKKINDVIDKecamatan.Value = ""
                hfTRXKKINDVIDKotaKab.Value = ""
                hfTRXKKINDVIDProvinsi.Value = ""
                hfTRXKKINDVIDNegara.Value = ""

                txtTRXKKINDVIDNamaJalan.Enabled = True
                txtTRXKKINDVIDRTRW.Enabled = True
                txtTRXKKINDVIDKodePos.Enabled = True

            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try

    End Sub



    Protected Sub grvTransaksi_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grvTransaksi.RowDataBound
        If e.Row.RowType <> ListItemType.Footer And e.Row.RowType <> ListItemType.Header Then
            e.Row.Cells(0).Text = e.Row.RowIndex + 1
            If e.Row.Cells.Count > 2 Then
                e.Row.Cells(6).Text = ValidateBLL.FormatMoneyWithComma(e.Row.Cells(6).Text)
            End If
        End If
    End Sub

    Protected Sub imgClearAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClearAll.Click
        clearKasMasukKasKeluar()
    End Sub

    Protected Sub DeleteResume_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim objgridviewrow As GridViewRow = CType(CType(sender, LinkButton).NamingContainer, GridViewRow)
            Dim objResume As List(Of ResumeKasMasukKeluarLTKTAdd) = SetnGetResumeKasMasukKasKeluar
            objResume.RemoveAt(objgridviewrow.RowIndex)
            grvTransaksi.DataSource = objResume
            grvTransaksi.DataBind()
        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message.ToString
        End Try

    End Sub


    Protected Sub imgCancelEdit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCancelEdit.Click
        imgAdd.Visible = True
        imgClearAll.Visible = True
        imgSaveEdit.Visible = False
        imgCancelEdit.Visible = False
        SetnGetRowEdit = -1
        clearKasMasukKasKeluar()
    End Sub



    Protected Sub imgOKMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgOKMsg.Click
        Response.Redirect("LTKTView.aspx")
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Response.Redirect("LTKTView.aspx")
    End Sub

    Protected Sub imgTerlaporDOMKelurahan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTerlaporDOMKelurahan.Click
        Try
            If Session("PickerKelurahan.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerKelurahan.Data")).Split(";")
                txtTerlaporDOMKelurahan.Text = strData(1)
                hfTerlaporDOMKelurahan.Value = strData(0)
                FillNextLevelAlamatIndividuDomisili(AddressLevel.Kelurahan)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTerlaporDOMKecamatan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTerlaporDOMKecamatan.Click
        Try
            If Session("PickerKecamatan.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerKecamatan.Data")).Split(";")
                txtTerlaporDOMKecamatan.Text = strData(1)
                hfTerlaporDOMKecamatan.Value = strData(0)
                FillNextLevelAlamatIndividuDomisili(AddressLevel.Kecamatan)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTerlaporIDProvinsi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTerlaporIDProvinsi.Click
        Try
            If Session("PickerProvinsi.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                txtTerlaporIDProvinsi.Text = strData(1)
                hfTerlaporIDProvinsi.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTerlaporIDKelurahan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTerlaporIDKelurahan.Click
        Try
            If Session("PickerKelurahan.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerKelurahan.Data")).Split(";")
                txtTerlaporIDKelurahan.Text = strData(1)
                hfTerlaporIDKelurahan.Value = strData(0)
                FillNextLevelAlamatIndividuIdentitas(AddressLevel.Kelurahan)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTerlaporCORPDLKelurahan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTerlaporCORPDLKelurahan.Click
        Try
            If Session("PickerKelurahan.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerKelurahan.Data")).Split(";")
                txtTerlaporCORPDLKelurahan.Text = strData(1)
                hfTerlaporCORPDLKelurahan.Value = strData(0)
                FillNextLevelAlamatKorporasiDalamNegeri(AddressLevel.Kelurahan)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKMINDVDOMKelurahan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKMINDVDOMKelurahan.Click
        Try
            If Session("PickerKelurahan.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerKelurahan.Data")).Split(";")
                txtTRXKMINDVDOMKelurahan.Text = strData(1)
                hfTRXKMINDVDOMKelurahan.Value = strData(0)
                FillNextLevelAlamatKasMasukIndividuDomisili(AddressLevel.Kelurahan)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKMINDVIDKelurahan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKMINDVIDKelurahan.Click
        Try
            If Session("PickerKelurahan.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerKelurahan.Data")).Split(";")
                txtTRXKMINDVIDKelurahan.Text = strData(1)
                hfTRXKMINDVIDKelurahan.Value = strData(0)
                FillNextLevelAlamatKasMasukIndividuIdentitas(AddressLevel.Kelurahan)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKMCORPDLKelurahan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKMCORPDLKelurahan.Click
        Try
            If Session("PickerKelurahan.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerKelurahan.Data")).Split(";")
                txtTRXKMCORPDLKelurahan.Text = strData(1)
                hfTRXKMCORPDLKelurahan.Value = strData(0)
                FillNextLevelAlamatKasMasukKorporasiDalamNegeri(AddressLevel.Kelurahan)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKKINDVDOMKelurahan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKKINDVDOMKelurahan.Click
        Try
            If Session("PickerKelurahan.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerKelurahan.Data")).Split(";")
                txtTRXKKINDVDOMKelurahan.Text = strData(1)
                hfTRXKKINDVDOMKelurahan.Value = strData(0)
                FillNextLevelAlamatKasKeluarIndividuDomisili(AddressLevel.Kelurahan)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKKINDVIDKelurahan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKKINDVIDKelurahan.Click
        Try
            If Session("PickerKelurahan.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerKelurahan.Data")).Split(";")
                txtTRXKKINDVIDKelurahan.Text = strData(1)
                hfTRXKKINDVIDKelurahan.Value = strData(0)
                FillNextLevelAlamatKasKeluarIndividuIdentitas(AddressLevel.Kelurahan)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKKCORPDLKelurahan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKKCORPDLKelurahan.Click
        Try
            If Session("PickerKelurahan.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerKelurahan.Data")).Split(";")
                txtTRXKKCORPDLKelurahan.Text = strData(1)
                hfTRXKKCORPDLKelurahan.Value = strData(0)
                FillNextLevelAlamatKasKeluarKorporasiDalamNegeri(AddressLevel.Kelurahan)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTerlaporIDKecamatan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTerlaporIDKecamatan.Click
        Try
            If Session("PickerKecamatan.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerKecamatan.Data")).Split(";")
                txtTerlaporIDKecamatan.Text = strData(1)
                hfTerlaporIDKecamatan.Value = strData(0)
                FillNextLevelAlamatIndividuIdentitas(AddressLevel.Kecamatan)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTerlaporCORPDLKecamatan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTerlaporCORPDLKecamatan.Click
        Try
            If Session("PickerKecamatan.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerKecamatan.Data")).Split(";")
                txtTerlaporCORPDLKecamatan.Text = strData(1)
                hfTerlaporCORPDLKecamatan.Value = strData(0)
                FillNextLevelAlamatKorporasiDalamNegeri(AddressLevel.Kecamatan)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKMINDVDOMKecamatan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKMINDVDOMKecamatan.Click
        Try
            If Session("PickerKecamatan.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerKecamatan.Data")).Split(";")
                txtTRXKMINDVDOMKecamatan.Text = strData(1)
                hfTRXKMINDVDOMKecamatan.Value = strData(0)
                FillNextLevelAlamatKasMasukIndividuDomisili(AddressLevel.Kecamatan)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKMINDVIDKecamatan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKMINDVIDKecamatan.Click
        Try
            If Session("PickerKecamatan.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerKecamatan.Data")).Split(";")
                txtTRXKMINDVIDKecamatan.Text = strData(1)
                hfTRXKMINDVIDKecamatan.Value = strData(0)
                FillNextLevelAlamatKasMasukIndividuIdentitas(AddressLevel.Kecamatan)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKMCORPDLKecamatan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKMCORPDLKecamatan.Click
        Try
            If Session("PickerKecamatan.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerKecamatan.Data")).Split(";")
                txtTRXKMCORPDLKecamatan.Text = strData(1)
                hfTRXKMCORPDLKecamatan.Value = strData(0)
                FillNextLevelAlamatKasMasukKorporasiDalamNegeri(AddressLevel.Kecamatan)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKKINDVDOMKecamatan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKKINDVDOMKecamatan.Click
        Try
            If Session("PickerKecamatan.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerKecamatan.Data")).Split(";")
                txtTRXKKINDVDOMKecamatan.Text = strData(1)
                hfTRXKKINDVDOMKecamatan.Value = strData(0)
                FillNextLevelAlamatKasKeluarIndividuDomisili(AddressLevel.Kecamatan)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKKINDVIDKecamatan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKKINDVIDKecamatan.Click
        Try
            If Session("PickerKecamatan.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerKecamatan.Data")).Split(";")
                txtTRXKKINDVIDKecamatan.Text = strData(1)
                hfTRXKKINDVIDKecamatan.Value = strData(0)
                FillNextLevelAlamatKasKeluarIndividuIdentitas(AddressLevel.Kecamatan)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKKCORPDLKecamatan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKKCORPDLKecamatan.Click
        Try
            If Session("PickerKecamatan.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerKecamatan.Data")).Split(";")
                txtTRXKKCORPDLKecamatan.Text = strData(1)
                hfTRXKKCORPDLKecamatan.Value = strData(0)
                FillNextLevelAlamatKasKeluarKorporasiDalamNegeri(AddressLevel.Kecamatan)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTerlaporDOMKotaKab_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTerlaporDOMKotaKab.Click
        Try
            If Session("PickerKotaKab.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
                txtTerlaporDOMKotaKab.Text = strData(1)
                hfTerlaporDOMKotaKab.Value = strData(0)
                FillNextLevelAlamatIndividuDomisili(AddressLevel.KabupatenKota)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTerlaporIDKotaKab_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTerlaporIDKotaKab.Click
        Try
            If Session("PickerKotaKab.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
                txtTerlaporIDKotaKab.Text = strData(1)
                hfTerlaporIDKotaKab.Value = strData(0)
                FillNextLevelAlamatIndividuIdentitas(AddressLevel.KabupatenKota)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTerlaporIDKota_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTerlaporIDKota.Click
        Try
            If Session("PickerKotaKab.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
                txtTerlaporNAKota.Text = strData(1)
                hfTerlaporIDKota.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTerlaporCORPDLKotaKab_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTerlaporCORPDLKotaKab.Click
        Try
            If Session("PickerKotaKab.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
                txtTerlaporCORPDLKotaKab.Text = strData(1)
                hfTerlaporCORPDLKotaKab.Value = strData(0)
                FillNextLevelAlamatKorporasiDalamNegeri(AddressLevel.KabupatenKota)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTerlaporCORPLNKota_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTerlaporCORPLNKota.Click
        Try
            If Session("PickerKotaKab.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
                txtTerlaporCORPLNKota.Text = strData(1)
                hfTerlaporCORPLNKota.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKMKotaKab_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKMKotaKab.Click
        Try
            If Session("PickerKotaKab.Data") <> "" Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
                txtTRXKMKotaKab.Text = strData(1)
                hfTRXKMKotaKab.Value = strData(0)
                FillNextLevelAlamatKasMasukPJKTempatTerjadinyaTransaksi(AddressLevel.KabupatenKota)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKMINDVDOMKotaKab_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKMINDVDOMKotaKab.Click
        Try
            If Session("PickerKotaKab.Data") <> "" Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
                txtTRXKMINDVDOMKotaKab.Text = strData(1)
                hfTRXKMINDVDOMKotaKab.Value = strData(0)
                FillNextLevelAlamatKasMasukIndividuDomisili(AddressLevel.KabupatenKota)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKMINDVIDKotaKab_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKMINDVIDKotaKab.Click
        Try
            If Session("PickerKotaKab.Data") <> "" Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
                txtTRXKMINDVIDKotaKab.Text = strData(1)
                hfTRXKMINDVIDKotaKab.Value = strData(0)
                FillNextLevelAlamatKasMasukIndividuIdentitas(AddressLevel.KabupatenKota)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKMINDVNAKota_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKMINDVNAKota.Click
        Try
            If Session("PickerKotaKab.Data") <> "" Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
                txtTRXKMINDVNAKota.Text = strData(1)
                hfTRXKMINDVNAKota.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKMCORPDLKotaKab_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKMCORPDLKotaKab.Click
        Try
            If Session("PickerKotaKab.Data") <> "" Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
                txtTRXKMCORPDLKotaKab.Text = strData(1)
                hfTRXKMCORPDLKotaKab.Value = strData(0)
                FillNextLevelAlamatKasMasukKorporasiDalamNegeri(AddressLevel.KabupatenKota)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKMCORPLNKota_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKMCORPLNKota.Click
        Try
            If Session("PickerKotaKab.Data") <> "" Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
                txtTRXKMCORPLNKota.Text = strData(1)
                hfTRXKMCORPLNKota.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKKKotaKab_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKKKotaKab.Click
        Try
            If Session("PickerKotaKab.Data") <> "" Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
                txtTRXKKKotaKab.Text = strData(1)
                hfTRXKKKotaKab.Value = strData(0)
                FillNextLevelAlamatKasKeluarPJKTempatTerjadinyaTransaksi(AddressLevel.KabupatenKota)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKKINDVDOMKotaKab_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKKINDVDOMKotaKab.Click
        Try
            If Session("PickerKotaKab.Data") <> "" Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
                txtTRXKKINDVDOMKotaKab.Text = strData(1)
                hfTRXKKINDVDOMKotaKab.Value = strData(0)
                FillNextLevelAlamatKasKeluarIndividuDomisili(AddressLevel.KabupatenKota)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKKINDVIDKotaKab_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKKINDVIDKotaKab.Click
        Try
            If Session("PickerKotaKab.Data") <> "" Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
                txtTRXKKINDVIDKotaKab.Text = strData(1)
                hfTRXKKINDVIDKotaKab.Value = strData(0)
                FillNextLevelAlamatKasKeluarIndividuIdentitas(AddressLevel.KabupatenKota)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKKINDVNAKota_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKKINDVNAKota.Click
        Try
            If Session("PickerKotaKab.Data") <> "" Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
                txtTRXKKINDVNAKota.Text = strData(1)
                hfTRXKKINDVNAKota.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKKCORPDLKotaKab_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKKCORPDLKotaKab.Click
        Try
            If Session("PickerKotaKab.Data") <> "" Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
                txtTRXKKCORPDLKotaKab.Text = strData(1)
                hfTRXKKCORPDLKotaKab.Value = strData(0)
                FillNextLevelAlamatKasKeluarKorporasiDalamNegeri(AddressLevel.KabupatenKota)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKKCORPLNKota_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKKCORPLNKota.Click
        Try
            If Session("PickerKotaKab.Data") <> "" Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
                txtTRXKKCORPLNKota.Text = strData(1)
                hfTRXKKCORPLNKota.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTerlaporDOMProvinsi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTerlaporDOMProvinsi.Click
        Try
            If Session("PickerProvinsi.Data") <> "" Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                txtTerlaporDOMProvinsi.Text = strData(1)
                hfTerlaporDOMProvinsi.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTerlaporNAProvinsi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTerlaporNAProvinsi.Click
        Try
            If Session("PickerProvinsi.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                txtTerlaporNAProvinsi.Text = strData(1)
                hfTerlaporNAProvinsi.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTerlaporCORPDLProvinsi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTerlaporCORPDLProvinsi.Click
        Try
            If Session("PickerProvinsi.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                txtTerlaporCORPDLProvinsi.Text = strData(1)
                hfTerlaporCORPDLProvinsi.Value = strData(0)
                FillNextLevelAlamatKorporasiDalamNegeri(AddressLevel.Propinsi)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTerlaporCORPLNProvinsi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTerlaporCORPLNProvinsi.Click
        Try
            If Session("PickerProvinsi.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                txtTerlaporCORPLNProvinsi.Text = strData(1)
                hfTerlaporCORPLNProvinsi.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKMProvinsi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKMProvinsi.Click
        Try
            If Session("PickerProvinsi.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                txtTRXKMProvinsi.Text = strData(1)
                hfTRXKMProvinsi.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKMINDVDOMProvinsi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKMINDVDOMProvinsi.Click
        Try
            If Session("PickerProvinsi.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                txtTRXKMINDVDOMProvinsi.Text = strData(1)
                hfTRXKMINDVDOMProvinsi.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKMINDVIDProvinsi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKMINDVIDProvinsi.Click
        Try
            If Session("PickerProvinsi.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                txtTRXKMINDVIDProvinsi.Text = strData(1)
                hfTRXKMINDVIDProvinsi.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKMINDVNAProvinsi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKMINDVNAProvinsi.Click
        Try
            If Session("PickerProvinsi.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                txtTRXKMINDVNAProvinsi.Text = strData(1)
                hfTRXKMINDVNAProvinsi.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKMCORPDLProvinsi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKMCORPDLProvinsi.Click
        Try
            If Session("PickerProvinsi.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                txtTRXKMCORPDLProvinsi.Text = strData(1)
                hfTRXKMCORPDLProvinsi.Value = strData(0)
                FillNextLevelAlamatKasMasukKorporasiDalamNegeri(AddressLevel.Propinsi)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKMCORPLNProvinsi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKMCORPLNProvinsi.Click
        Try
            If Session("PickerProvinsi.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                txtTRXKMCORPLNProvinsi.Text = strData(1)
                hfTRXKMCORPLNProvinsi.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKKProvinsi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKKProvinsi.Click
        Try
            If Session("PickerProvinsi.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                txtTRXKKProvinsi.Text = strData(1)
                hfTRXKKProvinsi.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKKINDVDOMProvinsi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKKINDVDOMProvinsi.Click
        Try
            If Session("PickerProvinsi.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                txtTRXKKINDVDOMProvinsi.Text = strData(1)
                hfTRXKKINDVDOMProvinsi.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKKINDVIDProvinsi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKKINDVIDPropinsi.Click
        Try
            If Session("PickerProvinsi.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                txtTRXKKINDVIDProvinsi.Text = strData(1)
                hfTRXKKINDVIDProvinsi.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKKINDVNAProvinsi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKKINDVNAProvinsi.Click
        Try
            If Session("PickerProvinsi.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                txtTRXKKINDVNAProvinsi.Text = strData(1)
                hfTRXKKINDVNAProvinsi.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKKCORPDLProvinsi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKKCORPDLProvinsi.Click
        Try
            If Session("PickerProvinsi.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                txtTRXKKCORPDLProvinsi.Text = strData(1)
                hfTRXKKCORPDLProvinsi.Value = strData(0)
                FillNextLevelAlamatKasKeluarKorporasiDalamNegeri(AddressLevel.Propinsi)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKKCORPLNProvinsi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKKCORPLNProvinsi.Click
        Try
            If Session("PickerProvinsi.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                txtTRXKKCORPLNProvinsi.Text = strData(1)
                hfTRXKKCORPLNProvinsi.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTerlaporIDNegara_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTerlaporIDNANegara.Click
        Try
            If Session("PickerNegara.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
                txtTerlaporNANegara.Text = strData(1)
                hfTerlaporIDNANegara.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTerlaporCORPDLNegara_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTerlaporCORPDLNegara.Click
        Try
            If Session("PickerNegara.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
                txtTerlaporCORPDLNegara.Text = strData(1)
                hfTerlaporCORPDLNegara.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTerlaporCORPLNNegara_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTerlaporCORPLNNegara.Click
        Try
            If Session("PickerNegara.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
                txtTerlaporCORPLNNegara.Text = strData(1)
                hfTerlaporCORPLNNegara.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKMINDVNANegara_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKMINDVNANegara.Click
        Try
            If Session("PickerNegara.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
                txtTRXKMINDVNANegara.Text = strData(1)
                hfTRXKMINDVNANegara.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKMCORPDLNegara_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKMCORPDLNegara.Click
        Try
            If Session("PickerNegara.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
                txtTRXKMCORPDLNegara.Text = strData(1)
                hfTRXKMCORPDLNegara.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKMCORPLNNegara_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKMCORPLNNegara.Click
        Try
            If Session("PickerNegara.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
                txtTRXKMCORPLNNegara.Text = strData(1)
                hfTRXKMCORPLNNegara.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKKINDVNANegara_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKKINDVNANegara.Click
        Try
            If Session("PickerNegara.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
                txtTRXKKINDVNANegara.Text = strData(1)
                hfTRXKKINDVNANegara.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKKCORPDLNegara_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKKCORPDLNegara.Click
        Try
            If Session("PickerNegara.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
                txtTRXKKCORPDLNegara.Text = strData(1)
                hfTRXKKCORPDLNegara.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKKCORPLNNegara_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKKCORPLNNegara.Click
        Try
            If Session("PickerNegara.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
                txtTRXKKCORPLNNegara.Text = strData(1)
                hfTRXKKCORPLNNegara.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTerlaporPekerjaan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTerlaporPekerjaan.Click
        Try
            If Session("PickerPekerjaan.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerPekerjaan.Data")).Split(";")
                txtTerlaporPekerjaan.Text = strData(1)
                hfTerlaporPekerjaan.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTerlaporCORPBidangUsaha_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTerlaporCORPBidangUsaha.Click
        Try
            If Session("PickerBidangUsaha.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerBidangUsaha.Data")).Split(";")
                txtTerlaporCORPBidangUsaha.Text = strData(1)
                hfTerlaporCORPBidangUsaha.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKMDetilMataUang_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKMDetilMataUang.Click
        Try
            If Session("PickerMataUang.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerMataUang.Data")).Split(";")
                txtTRXKMDetilMataUang.Text = strData(0) & " - " & strData(1)
                hfTRXKMDetilMataUang.Value = strData(0)
                Using objMatauang As TList(Of MsCurrencyRate) = DataRepository.MsCurrencyRateProvider.GetPaged("CurrencyCode = '" & strData(0).Trim & "'", "CurrencyRateDate desc", 0, 1, 0)
                    If objMatauang.Count > 0 Then
                        txtTRXKMDetailKursTrx.Text = ValidateBLL.FormatMoneyWithComma(objMatauang(0).CurrencyRate)
                    End If
                End Using
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKMINDVPekerjaan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKMINDVPekerjaan.Click
        Try
            If Session("PickerPekerjaan.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerPekerjaan.Data")).Split(";")
                txtTRXKMINDVPekerjaan.Text = strData(1)
                hfTRXKMINDVPekerjaan.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKMCORPBidangUsaha_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKMCORPBidangUsaha.Click
        Try
            If Session("PickerBidangUsaha.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerBidangUsaha.Data")).Split(";")
                txtTRXKMCORPBidangUsaha.Text = strData(1)
                hfTRXKMCORPBidangUsaha.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKKDetilMataUang_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKKDetilMataUang.Click
        Try
            If Session("PickerMataUang.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerMataUang.Data")).Split(";")
                txtTRXKKDetilMataUang.Text = strData(0) & " - " & strData(1)
                hfTRXKKDetilMataUang.Value = strData(0)
                Using objMatauang As TList(Of MsCurrencyRate) = DataRepository.MsCurrencyRateProvider.GetPaged("CurrencyCode = '" & strData(0).Trim & "'", "CurrencyRateDate desc", 0, 1, 0)
                    If objMatauang.Count > 0 Then
                        txtTRXKKDetilKursTrx.Text = ValidateBLL.FormatMoneyWithComma(objMatauang(0).CurrencyRate)
                    End If
                End Using
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKKINDVPekerjaan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKKINDVPekerjaan.Click
        Try
            If Session("PickerPekerjaan.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerPekerjaan.Data")).Split(";")
                txtTRXKKINDVPekerjaan.Text = strData(1)
                hfTRXKKINDVPekerjaan.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKKCORPBidangUsaha_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKKCORPBidangUsaha.Click
        Try
            If Session("PickerBidangUsaha.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerBidangUsaha.Data")).Split(";")
                txtTRXKKCORPBidangUsaha.Text = strData(1)
                hfTRXKKCORPBidangUsaha.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub rblTerlaporKewarganegaraan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblTerlaporKewarganegaraan.SelectedIndexChanged
        Try
            If rblTerlaporKewarganegaraan.SelectedIndex = 0 Then
                For Each item As ListItem In cboTerlaporNegara.Items
                    If item.Text.ToLower.Contains("indonesia") Then
                        cboTerlaporNegara.SelectedValue = item.Value
                    End If
                Next
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub rblTRXKMINDVKewarganegaraan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblTRXKMINDVKewarganegaraan.SelectedIndexChanged
        Try
            If rblTRXKMINDVKewarganegaraan.SelectedIndex = 0 Then
                For Each item As ListItem In cboTRXKMINDVNegara.Items
                    If item.Text.ToLower.Contains("indonesia") Then
                        cboTRXKMINDVNegara.SelectedValue = item.Value
                    End If
                Next
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try

    End Sub

    Protected Sub rblTRXKKINDVKewarganegaraan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblTRXKKINDVKewarganegaraan.SelectedIndexChanged
        Try
            If rblTRXKKINDVKewarganegaraan.SelectedIndex = 0 Then
                For Each item As ListItem In cboTRXKKINDVNegara.Items
                    If item.Text.ToLower.Contains("indonesia") Then
                        cboTRXKKINDVNegara.SelectedValue = item.Value
                    End If
                Next
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try

    End Sub
    Protected Sub imgTRXKMDetilAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKMDetilAdd.Click
        Try
            If txtTRXKMDetilKasMasuk.Text = "" Then 'valuta asing 


                If isvalidAddKassMasuk() Then
                    Dim objTemp As List(Of LTKTDetailCashInEdit)
                    objTemp = SetnGetgrvTRXKMDetilValutaAsing

                    Dim obj As New LTKTDetailCashInEdit
                    obj.MataUang = txtTRXKMDetilMataUang.Text
                    obj.KursTransaksi = txtTRXKMDetailKursTrx.Text
                    obj.Jumlah = txtTRXKMDetilJumlah.Text
                    obj.JumlahRp = (CDec(obj.KursTransaksi) * CDec(obj.Jumlah)).ToString

                    objTemp.Add(obj)

                    SetnGetgrvTRXKMDetilValutaAsing = objTemp

                    grvTRXKMDetilValutaAsing.DataSource = objTemp
                    grvTRXKMDetilValutaAsing.DataBind()

                    ' If txtTRXKMDetilKasMasuk.Text = vbNullString Then
                    lblTRXKMDetilValutaAsingJumlahRp.Text = ValidateBLL.FormatMoneyWithComma(JumlahKeseluruhanRp(SetnGetgrvTRXKMDetilValutaAsing))
                    'lse
                    ' lblTRXKMDetilValutaAsingJumlahRp.Text = ValidateBLL.FormatMoneyWithComma(CDec(txtTRXKMDetilKasMasuk.Text) + JumlahKeseluruhanRp(SetnGetgrvTRXKMDetilValutaAsing))
                    'End If


                End If
            Else
                Dim objTemp As List(Of LTKTDetailCashInEdit)
                objTemp = SetnGetgrvTRXKMDetilValutaAsing

                Dim obj As New LTKTDetailCashInEdit
                obj.MataUang = "IDR - Indonesian Rupiah"
                obj.KursTransaksi = 0
                obj.Jumlah = 0
                obj.JumlahRp = CDec(txtTRXKMDetilKasMasuk.Text)

                objTemp.Add(obj)

                SetnGetgrvTRXKMDetilValutaAsing = objTemp

                grvTRXKMDetilValutaAsing.DataSource = objTemp
                grvTRXKMDetilValutaAsing.DataBind()

                ' If txtTRXKMDetilKasMasuk.Text = vbNullString Then
                lblTRXKMDetilValutaAsingJumlahRp.Text = ValidateBLL.FormatMoneyWithComma(JumlahKeseluruhanRp(SetnGetgrvTRXKMDetilValutaAsing))
                'lse
                ' lblTRXKMDetilValutaAsingJumlahRp.Text = ValidateBLL.FormatMoneyWithComma(CDec(txtTRXKMDetilKasMasuk.Text) + JumlahKeseluruhanRp(SetnGetgrvTRXKMDetilValutaAsing))
                'End If

            End If

            txtTRXKMDetilMataUang.Text = vbNullString
            hfTRXKMDetilMataUang.Value = ""
            txtTRXKMDetailKursTrx.Text = vbNullString
            txtTRXKMDetilJumlah.Text = vbNullString
            txtTRXKMDetilKasMasuk.Text = ""
        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message.ToString
        End Try
    End Sub

    Protected Sub imgTRXKKDetilAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKKDetilAdd.Click
        Try

            If txtTRXKKDetailKasKeluar.Text = "" Then
                If isvalidAddKassKeluar() Then 'valuta asing
                    Dim objTemp As List(Of LTKTDetailCashInEdit)

                    objTemp = SetnGetgrvDetilKasKeluar

                    Dim obj As New LTKTDetailCashInEdit
                    obj.MataUang = txtTRXKKDetilMataUang.Text
                    obj.KursTransaksi = txtTRXKKDetilKursTrx.Text
                    obj.Jumlah = txtTRXKKDetilJumlah.Text
                    obj.JumlahRp = (CDec(obj.KursTransaksi) * CDec(obj.Jumlah)).ToString

                    objTemp.Add(obj)

                    SetnGetgrvDetilKasKeluar = objTemp

                    grvDetilKasKeluar.DataSource = objTemp
                    grvDetilKasKeluar.DataBind()

                    ' If txtTRXKKDetailKasKeluar.Text = vbNullString Then
                    lblDetilKasKeluarJumlahRp.Text = ValidateBLL.FormatMoneyWithComma(JumlahKeseluruhanRp(SetnGetgrvDetilKasKeluar))
                    'Else
                    '    lblDetilKasKeluarJumlahRp.Text = ValidateBLL.FormatMoneyWithComma(CDec(txtTRXKKDetailKasKeluar.Text) + JumlahKeseluruhanRp(SetnGetgrvDetilKasKeluar))
                    'End If
                End If
            Else
                Dim objTemp As List(Of LTKTDetailCashInEdit)

                objTemp = SetnGetgrvDetilKasKeluar

                Dim obj As New LTKTDetailCashInEdit
                obj.MataUang = "IDR - Indonesian Rupiah"
                obj.KursTransaksi = "0"
                obj.Jumlah = "0"
                obj.JumlahRp = txtTRXKKDetailKasKeluar.Text

                objTemp.Add(obj)

                SetnGetgrvDetilKasKeluar = objTemp
                grvDetilKasKeluar.DataSource = objTemp
                grvDetilKasKeluar.DataBind()

                'If txtTRXKKDetailKasKeluar.Text = vbNullString Then
                lblDetilKasKeluarJumlahRp.Text = FormatMoneyWithComma(JumlahKeseluruhanRp(SetnGetgrvDetilKasKeluar))
                'Else
                '    lblDetilKasKeluarJumlahRp.Text = ValidateBLL.FormatMoneyWithComma(CDec(txtTRXKKDetailKasKeluar.Text) + JumlahKeseluruhanRp(SetnGetgrvDetilKasKeluar))
                'End If
            End If

            txtTRXKKDetilMataUang.Text = vbNullString
            hfTRXKKDetilMataUang.Value = ""
            txtTRXKKDetilKursTrx.Text = vbNullString
            txtTRXKKDetilJumlah.Text = vbNullString
            txtTRXKKDetailKasKeluar.Text = ""
        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message.ToString
        End Try
    End Sub

    Protected Sub imgSaveEdit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgSaveEdit.Click
        Try
            If isValidAddKas() Then
                Dim objColKas As List(Of ResumeKasMasukKeluarLTKTAdd) = SetnGetResumeKasMasukKasKeluar
                If lblTRXKMDetilValutaAsingJumlahRp.Text <> vbNullString OrElse lblTRXKMDetilValutaAsingJumlahRp.Text <> "" Then
                    If Me.cboDebitCredit.SelectedValue = "D" Then
                        Throw New Exception("Only Kas Keluar (Debet) are allowed")
                    End If

                    'Insert untuk tampilan Grid
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionDate, txtTRXKMTanggalTrx.Text, True, oDate)
                    FillOrNothing(objColKas(SetnGetRowEdit).Branch, txtTRXKMNamaKantor.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionNominal, lblTRXKMDetilValutaAsingJumlahRp.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).AccountNumber, txtTRXKMNoRekening.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).Kas, "Kas Masuk")
                    FillOrNothing(objColKas(SetnGetRowEdit).Type, "LTKT")

                    'Insert Data Ke ObjectTransaction
                    objColKas(SetnGetRowEdit).TransactionCashIn = New LTKTTransactionCashIn
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.TanggalTransaksi, txtTRXKMTanggalTrx.Text, True, oDate)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.NamaKantorPJK, txtTRXKMNamaKantor.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.FK_MsKotaKab_Id, hfTRXKMKotaKab.Value, True, oInt)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.FK_MsProvince_Id, hfTRXKMProvinsi.Value, True, oInt)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.NomorRekening, txtTRXKMNoRekening.Text)


                    If rblTRXKMTipePelapor.SelectedValue = "" Then

                        objColKas(SetnGetRowEdit).TransactionCashIn.TipeTerlapor = Nothing
                    Else
                        objColKas(SetnGetRowEdit).TransactionCashIn.TipeTerlapor = rblTRXKMTipePelapor.SelectedValue

                    End If

                    'FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.TipeTerlapor, intTipeTerkait)

                    'Individu
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.INDV_Gelar, txtTRXKMINDVGelar.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.INDV_NamaLengkap, txtTRXKMINDVNamaLengkap.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.INDV_TempatLahir, txtTRXKMINDVTempatLahir.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.INDV_TanggalLahir, txtTRXKMINDVTanggalLahir.Text, True, oDate)

                    If rblTRXKMINDVKewarganegaraan.SelectedValue = "" Then
                        objColKas(SetnGetRowEdit).TransactionCashIn.INDV_Kewarganegaraan = Nothing
                    Else
                        objColKas(SetnGetRowEdit).TransactionCashIn.INDV_Kewarganegaraan = rblTRXKMINDVKewarganegaraan.SelectedValue
                    End If


                    If cboTRXKMINDVNegara.SelectedIndex <> 0 Then

                        FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.INDV_FK_MsNegara_Id, cboTRXKMINDVNegara.SelectedValue, True, oInt)

                    End If

                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.INDV_DOM_NamaJalan, txtTRXKMINDVDOMNamaJalan.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.INDV_DOM_RTRW, txtTRXKMINDVDOMRTRW.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.INDV_DOM_FK_MsKelurahan_Id, hfTRXKMINDVDOMKelurahan.Value, True, OLong)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.INDV_DOM_FK_MsKecamatan_Id, hfTRXKMINDVDOMKecamatan.Value, True, oInt)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.INDV_DOM_FK_MsKotaKab_Id, hfTRXKMINDVDOMKotaKab.Value, True, oInt)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.INDV_DOM_KodePos, txtTRXKMINDVDOMKodePos.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.INDV_DOM_FK_MsProvince_Id, hfTRXKMINDVDOMProvinsi.Value, True, oInt)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.INDV_DOM_FK_MsNegara_Id, hfTRXKMINDVDOMNegara.Value, True, oInt)


                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.INDV_ID_NamaJalan, txtTRXKMINDVIDNamaJalan.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.INDV_ID_RTRW, txtTRXKMINDVIDRTRW.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.INDV_ID_FK_MsKelurahan_Id, hfTRXKMINDVIDKelurahan.Value, True, OLong)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.INDV_ID_FK_MsKecamatan_Id, hfTRXKMINDVIDKecamatan.Value, True, oInt)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.INDV_ID_FK_MsKotaKab_Id, hfTRXKMINDVIDKotaKab.Value, True, oInt)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.INDV_ID_KodePos, txtTRXKMINDVIDKodePos.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.INDV_ID_FK_MsProvince_Id, hfTRXKMINDVIDProvinsi.Value, True, oInt)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.INDV_ID_FK_MsNegara_Id, hfTRXKMINDVIDNegara.Value, True, oInt)


                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.INDV_NA_NamaJalan, txtTRXKMINDVNANamaJalan.Text)


                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.INDV_NA_FK_MsNegara_Id, hfTRXKMINDVNANegara.Value, True, oInt)


                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.INDV_NA_FK_MsProvince_Id, hfTRXKMINDVNAProvinsi.Value, True, oInt)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.INDV_NA_FK_MsKotaKab_Id, hfTRXKMINDVNAKota.Value, True, oInt)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.INDV_NA_FK_MsNegara_Id, hfTRXKMINDVNANegara.Value, True, oInt)

                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.INDV_NA_KodePos, txtTRXKMINDVNAKodePos.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.INDV_FK_MsIDType_Id, cboTRXKMINDVJenisID.SelectedValue, True, oInt)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.INDV_NomorId, txtTRXKMINDVNomorID.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.INDV_NPWP, txtTRXKMINDVNPWP.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.INDV_FK_MsPekerjaan_Id, hfTRXKMINDVPekerjaan.Value, True, oInt)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.INDV_Jabatan, txtTRXKMINDVJabatan.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.INDV_PenghasilanRataRata, txtTRXKMINDVPenghasilanRataRata.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.INDV_TempatBekerja, txtTRXKMINDVTempatKerja.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.INDV_TujuanTransaksi, txtTRXKMINDVTujuanTrx.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.INDV_SumberDana, txtTRXKMINDVSumberDana.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.INDV_NamaBankLain, txtTRXKMINDVNamaBankLain.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.INDV_NomorRekeningTujuan, txtTRXKMINDVNoRekeningTujuan.Text)

                    If cboTRXKMCORPBentukBadanUsaha.SelectedIndex <> 0 Then
                        FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsBentukBadanUsaha_Id, cboTRXKMCORPBentukBadanUsaha.SelectedValue, True, oInt)
                    End If

                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.CORP_Nama, txtTRXKMCORPNama.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsBidangUsaha_Id, hfTRXKMCORPBidangUsaha.Value, True, oInt)

                    If rblTRXKMCORPTipeAlamat.SelectedIndex <> 0 Then
                        FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.CORP_TipeAlamat, rblTRXKMCORPTipeAlamat.SelectedIndex, True, oByte)
                    End If

                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.CORP_NamaJalan, txtTRXKMCORPDLNamaJalan.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.CORP_RTRW, txtTRXKMCORPDLRTRW.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsKelurahan_Id, hfTRXKMCORPDLKelurahan.Value, True, OLong)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsKecamatan_Id, hfTRXKMCORPDLKecamatan.Value, True, oInt)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsKotaKab_Id, hfTRXKMCORPDLKotaKab.Value, True, oInt)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.CORP_KodePos, txtTRXKMCORPDLKodePos.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsProvince_Id, hfTRXKMCORPDLProvinsi.Value, True, oInt)

                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsNegara_Id, hfTRXKMCORPDLNegara.Value, True, oInt)


                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.CORP_LN_NamaJalan, txtTRXKMCORPLNNamaJalan.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.CORP_LN_FK_MsNegara_Id, hfTRXKMCORPLNNegara.Value, True, oInt)

                    

                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.CORP_LN_FK_MsProvince_Id, hfTRXKMCORPLNProvinsi.Value, True, oInt)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.CORP_LN_MsKotaKab_Id, hfTRXKMCORPLNKota.Value, True, oInt)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.CORP_LN_KodePos, txtTRXKMCORPLNKodePos.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.CORP_NPWP, txtTRXKMCORPNPWP.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.CORP_TujuanTransaksi, txtTRXKMCORPTujuanTrx.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.CORP_SumberDana, txtTRXKMCORPSumberDana.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.CORP_NamaBankLain, txtTRXKMCORPNamaBankLain.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashIn.CORP_NomorRekeningTujuan, txtTRXKMCORPNoRekeningTujuan.Text)

                    'Insert Data Ke Detail Transaction
                    objColKas(SetnGetRowEdit).DetailTransactionCashIn = New List(Of LTKTDetailCashInTransaction)
                    Dim DetilCol As List(Of LTKTDetailCashInEdit) = SetnGetgrvTRXKMDetilValutaAsing

                    'hapus detil
                    If objColKas(SetnGetRowEdit).DetailTransactionCashIn.Count > 0 Then
                        For Each singleObj As LTKTDetailCashInTransaction In objColKas(SetnGetRowEdit).DetailTransactionCashIn
                            objColKas(SetnGetRowEdit).DetailTransactionCashIn.Remove(singleObj)
                        Next
                    End If

                    'add ulang detil
                    If DetilCol.Count > 0 Then

                        For Each singleObj As LTKTDetailCashInEdit In DetilCol
                            Dim objDetilKasMasuk As New LTKTDetailCashInTransaction

                            Dim matauangKM As String = singleObj.MataUang.Substring(0, singleObj.MataUang.IndexOf("-")).Trim
                            Dim objCurrency As TList(Of MsCurrency) = DataRepository.MsCurrencyProvider.GetPaged(MsCurrencyColumn.Code.ToString & " like '%" & matauangKM.ToString & "%'", "", 0, Integer.MaxValue, 0)
                            If objCurrency.Count > 0 Then
                                FillOrNothing(objDetilKasMasuk.Asing_FK_MsCurrency_Id, objCurrency(0).IdCurrency)
                            End If
                            FillOrNothing(objDetilKasMasuk.Asing_KursTransaksi, singleObj.KursTransaksi, True, oDecimal)
                            FillOrNothing(objDetilKasMasuk.Asing_TotalKasMasukDalamRupiah, singleObj.JumlahRp, True, oDecimal)
                            FillOrNothing(objDetilKasMasuk.TotalKasMasuk, lblTRXKMDetilValutaAsingJumlahRp.Text, True, oDecimal)

                            'Masukin ke detail
                            objColKas(SetnGetRowEdit).DetailTransactionCashIn.Add(objDetilKasMasuk)
                        Next

                        ''Masukin KasMasuknya aja
                        'If txtTRXKMDetilKasMasuk.Text <> "" Then
                        '    Dim objDetilKasMasuk As New LTKTDetailCashInTransaction
                        '    FillOrNothing(objDetilKasMasuk.KasMasuk, txtTRXKMDetilKasMasuk.Text, True, oDecimal)
                        '    FillOrNothing(objDetilKasMasuk.TotalKasMasuk, lblTRXKMDetilValutaAsingJumlahRp.Text, True, oDecimal)
                        '    objColKas(SetnGetRowEdit).DetailTransactionCashIn.Add(objDetilKasMasuk)
                        'End If

                        'Else
                        '    Dim objDetilKasMasuk As New LTKTDetailCashInTransaction
                        '    FillOrNothing(objDetilKasMasuk.KasMasuk, txtTRXKMDetilKasMasuk.Text, True, oDecimal)
                        '    FillOrNothing(objDetilKasMasuk.TotalKasMasuk, lblTRXKMDetilValutaAsingJumlahRp.Text, True, oDecimal)
                        '    objColKas(SetnGetRowEdit).DetailTransactionCashIn.Add(objDetilKasMasuk)
                    End If



                End If
                If lblDetilKasKeluarJumlahRp.Text <> vbNullString OrElse lblDetilKasKeluarJumlahRp.Text <> "" Then
                    If Me.cboDebitCredit.SelectedValue = "C" Then
                        Throw New Exception("Only Kas Masuk (Credit) are allowed")
                    End If

                    'Insert untuk tampilan Grid
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionDate, txtTRXKKTanggalTransaksi.Text, True, oDate)
                    FillOrNothing(objColKas(SetnGetRowEdit).Branch, txtTRXKKNamaKantor.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionNominal, lblDetilKasKeluarJumlahRp.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).AccountNumber, txtTRXKKRekeningKK.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).Kas, "Kas Keluar")
                    FillOrNothing(objColKas(SetnGetRowEdit).Type, "LTKT")

                    'Insert Data Ke ObjectTransction
                    objColKas(SetnGetRowEdit).TransactionCashOut = New LTKTTransactionCashOut
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.TanggalTransaksi, txtTRXKKTanggalTransaksi.Text, True, oDate)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.NamaKantorPJK, txtTRXKKNamaKantor.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.FK_MsKotaKab_Id, hfTRXKKKotaKab.Value, True, oInt)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.FK_MsProvince_Id, hfTRXKKProvinsi.Value, True, oInt)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.NomorRekening, txtTRXKKRekeningKK.Text)


                    If rblTRXKKTipePelapor.SelectedValue = "" Then

                        objColKas(SetnGetRowEdit).TransactionCashOut.TipeTerlapor = Nothing
                    Else
                        objColKas(SetnGetRowEdit).TransactionCashOut.TipeTerlapor = rblTRXKKTipePelapor.SelectedValue

                    End If


                    'Individu
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.INDV_Gelar, txtTRXKKINDVGelar.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.INDV_NamaLengkap, txtTRXKKINDVNamaLengkap.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.INDV_TempatLahir, txtTRXKKINDVTempatLahir.Text)

                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.INDV_TanggalLahir, txtTRXKKINDVTglLahir.Text)


                    If rblTRXKKINDVKewarganegaraan.SelectedValue = "" Then
                        objColKas(SetnGetRowEdit).TransactionCashOut.INDV_Kewarganegaraan = Nothing
                    Else
                        objColKas(SetnGetRowEdit).TransactionCashOut.INDV_Kewarganegaraan = rblTRXKKINDVKewarganegaraan.SelectedValue
                    End If



                    If cboTRXKKINDVNegara.SelectedIndex <> 0 Then
                        FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.INDV_ID_FK_MsNegara_Id, cboTRXKKINDVNegara.SelectedValue, True, oInt)
                        FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.INDV_FK_MsNegara_Id, cboTRXKKINDVNegara.SelectedValue, True, oInt)
                    End If

                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.INDV_DOM_NamaJalan, txtTRXKKINDVDOMNamaJalan.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.INDV_DOM_RTRW, txtTRXKKINDVDOMRTRW.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.INDV_DOM_FK_MsKelurahan_Id, hfTRXKKINDVDOMKelurahan.Value, True, OLong)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.INDV_DOM_FK_MsKecamatan_Id, hfTRXKKINDVDOMKecamatan.Value, True, oInt)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.INDV_DOM_FK_MsKotaKab_Id, hfTRXKKINDVDOMKotaKab.Value, True, oInt)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.INDV_DOM_KodePos, txtTRXKKINDVDOMKodePos.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.INDV_DOM_FK_MsProvince_Id, hfTRXKKINDVDOMProvinsi.Value, True, oInt)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.INDV_DOM_FK_MsNegara_Id, hfTRXKKINDVDOMNegara.Value, True, oInt)

                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.INDV_ID_NamaJalan, txtTRXKKINDVIDNamaJalan.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.INDV_ID_RTRW, txtTRXKKINDVIDRTRW.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.INDV_ID_FK_MsKelurahan_Id, hfTRXKKINDVIDKelurahan.Value, True, OLong)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.INDV_ID_FK_MsKecamatan_Id, hfTRXKKINDVIDKecamatan.Value, True, oInt)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.INDV_ID_FK_MsKotaKab_Id, hfTRXKKINDVIDKotaKab.Value, True, oInt)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.INDV_ID_KodePos, txtTRXKKINDVIDKodePos.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.INDV_ID_FK_MsProvince_Id, hfTRXKKINDVIDProvinsi.Value, True, oInt)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.INDV_ID_FK_MsNegara_Id, hfTRXKKINDVIDNegara.Value, True, oInt)

                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.INDV_NA_NamaJalan, txtTRXKKINDVNANamaJalan.Text)

                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.INDV_NA_FK_MsNegara_Id, hfTRXKKINDVNANegara.Value, True, oInt)

                    

                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.INDV_NA_FK_MsProvince_Id, hfTRXKKINDVNAProvinsi.Value, True, oInt)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.INDV_NA_FK_MsKotaKab_Id, hfTRXKKINDVNAKota.Value, True, oInt)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.INDV_NA_KodePos, txtTRXKKINDVNAKodePos.Text)
                    If cboTRXKKINDVJenisID.SelectedIndex <> 0 Then
                        FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.INDV_FK_MsIDType_Id, cboTRXKKINDVJenisID.SelectedValue, True, oInt)
                    End If

                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.INDV_NomorId, txtTRXKKINDVNomorId.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.INDV_NPWP, txtTRXKKINDVNPWP.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.INDV_FK_MsPekerjaan_Id, hfTRXKKINDVPekerjaan.Value, True, oInt)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.INDV_Jabatan, txtTRXKKINDVJabatan.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.INDV_PenghasilanRataRata, txtTRXKKINDVPenghasilanRataRata.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.INDV_TempatBekerja, txtTRXKKINDVTempatKerja.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.INDV_TujuanTransaksi, txtTRXKKINDVTujuanTrx.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.INDV_SumberDana, txtTRXKKINDVSumberDana.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.INDV_NamaBankLain, txtTRXKKINDVNamaBankLain.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.INDV_NomorRekeningTujuan, txtTRXKKINDVNoRekTujuan.Text)


                    If cboTRXKKCORPBentukBadanUsaha.SelectedIndex <> 0 Then
                        FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsBentukBadanUsaha_Id, cboTRXKKCORPBentukBadanUsaha.SelectedValue)
                    End If
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.CORP_Nama, txtTRXKKCORPNama.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsBidangUsaha_Id, hfTRXKKCORPBidangUsaha.Value, True, oInt)

                    If rblTRXKKCORPTipeAlamat.SelectedIndex <> 0 Then
                        FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.CORP_TipeAlamat, rblTRXKKCORPTipeAlamat.SelectedIndex, True, oByte)
                    End If

                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.CORP_NamaJalan, txtTRXKKCORPDLNamaJalan.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.CORP_RTRW, txtTRXKKCORPDLRTRW.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsKelurahan_Id, hfTRXKKCORPDLKelurahan.Value, True, OLong)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsKecamatan_Id, hfTRXKKCORPDLKecamatan.Value, True, oInt)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsKotaKab_Id, hfTRXKKCORPDLKotaKab.Value, True, oInt)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.CORP_KodePos, txtTRXKKCORPDLKodePos.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsProvince_Id, hfTRXKKCORPDLProvinsi.Value, True, oInt)

                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsNegara_Id, hfTRXKKCORPDLNegara.Value, True, oInt)

                    
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.CORP_LN_NamaJalan, txtTRXKKCORPLNNamaJalan.Text)

                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.CORP_LN_FK_MsNegara_Id, hfTRXKKCORPLNNegara.Value, True, oInt)


                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.CORP_LN_FK_MsProvince_Id, hfTRXKKCORPLNProvinsi.Value, True, oInt)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.CORP_LN_MsKotaKab_Id, hfTRXKKCORPLNKota.Value, True, oInt)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.CORP_LN_KodePos, txtTRXKKCORPLNKodePos.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.CORP_NPWP, txtTRXKKCORPNPWP.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.CORP_TujuanTransaksi, txtTRXKKCORPTujuanTrx.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.CORP_SumberDana, txtTRXKKCORPSumberDana.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.CORP_NamaBankLain, txtTRXKKCORPNamaBankLain.Text)
                    FillOrNothing(objColKas(SetnGetRowEdit).TransactionCashOut.CORP_NomorRekeningTujuan, txtTRXKKCORPNoRekeningTujuan.Text)


                    'Insert Date Ke DetailTransaction
                    objColKas(SetnGetRowEdit).DetailTranscationCashOut = New List(Of LTKTDetailCashOutTransaction)
                    Dim DetilCol As List(Of LTKTDetailCashInEdit) = SetnGetgrvDetilKasKeluar

                    'hapus detil
                    If objColKas(SetnGetRowEdit).DetailTranscationCashOut.Count > 0 Then
                        For Each singleObj As LTKTDetailCashOutTransaction In objColKas(SetnGetRowEdit).DetailTranscationCashOut
                            objColKas(SetnGetRowEdit).DetailTranscationCashOut.Remove(singleObj)
                        Next
                    End If

                    'insert ulang detil
                    If DetilCol.Count > 0 Then
                        For Each singleObj As LTKTDetailCashInEdit In DetilCol
                            Dim objDetilKasKeluar As New LTKTDetailCashOutTransaction

                            Dim matauangKK As String = singleObj.MataUang.Substring(0, singleObj.MataUang.IndexOf("-")).Trim
                            Dim objCurrency As TList(Of MsCurrency) = DataRepository.MsCurrencyProvider.GetPaged(MsCurrencyColumn.Code.ToString & " like '%" & matauangKK.ToString & "%'", "", 0, Integer.MaxValue, 0)
                            If objCurrency.Count > 0 Then
                                FillOrNothing(objDetilKasKeluar.Asing_FK_MsCurrency_Id, objCurrency(0).IdCurrency, True, oInt)
                            End If
                            FillOrNothing(objDetilKasKeluar.Asing_FK_MsCurrency_Id, objCurrency(0).IdCurrency, True, oInt)
                            FillOrNothing(objDetilKasKeluar.Asing_KursTransaksi, singleObj.KursTransaksi, True, oDecimal)
                            FillOrNothing(objDetilKasKeluar.Asing_TotalKasKeluarDalamRupiah, singleObj.JumlahRp, True, oDecimal)
                            FillOrNothing(objDetilKasKeluar.TotalKasKeluar, lblDetilKasKeluarJumlahRp.Text, True, oDecimal)

                            'Masukin ke detail
                            objColKas(SetnGetRowEdit).DetailTranscationCashOut.Add(objDetilKasKeluar)
                        Next

                        'Masukin KasKeluarnya aja
                        'If txtTRXKKDetailKasKeluar.Text <> "" Then
                        '    Dim objDetilKasKeluar As New LTKTDetailCashOutTransaction
                        '    FillOrNothing(objDetilKasKeluar.TotalKasKeluar, lblDetilKasKeluarJumlahRp.Text, True, oDecimal)
                        '    FillOrNothing(objDetilKasKeluar.KasKeluar, txtTRXKKDetailKasKeluar.Text, True, oDecimal)
                        '    objColKas(SetnGetRowEdit).DetailTranscationCashOut.Add(objDetilKasKeluar)
                        'End If

                        'Else
                        '    Dim objDetilKasKeluar As New LTKTDetailCashOutTransaction
                        '    FillOrNothing(objDetilKasKeluar.TotalKasKeluar, lblDetilKasKeluarJumlahRp.Text, True, oDecimal)
                        '    FillOrNothing(objDetilKasKeluar.KasKeluar, txtTRXKKDetailKasKeluar.Text, True, oDecimal)
                        '    objColKas(SetnGetRowEdit).DetailTranscationCashOut.Add(objDetilKasKeluar)
                    End If



                End If

                Dim totalKasMasuk As Decimal = 0
                Dim totalKasKeluar As Decimal = 0
                For Each kas As ResumeKasMasukKeluarLTKTAdd In objColKas
                    If kas.Kas = "Kas Masuk" Then
                        totalKasMasuk += CDec(kas.TransactionNominal)
                    Else
                        totalKasKeluar += CDec(kas.TransactionNominal)
                    End If
                Next

                lblTotalKasMasuk.Text = FormatMoneyWithComma(totalKasMasuk)
                lblTotalKasKeluar.Text = FormatMoneyWithComma(totalKasKeluar)

                grvTransaksi.DataSource = objColKas
                grvTransaksi.DataBind()

                clearKasMasukKasKeluar()
                imgAdd.Visible = True
                imgClearAll.Visible = False
                imgSaveEdit.Visible = False
                imgCancelEdit.Visible = True
                SetnGetRowEdit = -1
            End If


        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message.ToString
        End Try
    End Sub

    Protected Sub imgAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgAdd.Click
        Try
            If isValidAddKas() Then
                Dim defaultDate As New Date(1900, 1, 1)
                Dim objColKas As List(Of ResumeKasMasukKeluarLTKTAdd) = SetnGetResumeKasMasukKasKeluar
                If lblTRXKMDetilValutaAsingJumlahRp.Text <> vbNullString OrElse lblTRXKMDetilValutaAsingJumlahRp.Text <> "" Then
                    Dim objKas As ResumeKasMasukKeluarLTKTAdd = New ResumeKasMasukKeluarLTKTAdd
                    'Insert untuk tampilan Grid
                    FillOrNothing(objKas.TransactionDate, txtTRXKMTanggalTrx.Text, True, oDate)
                    FillOrNothing(objKas.Branch, txtTRXKMNamaKantor.Text)
                    FillOrNothing(objKas.TransactionNominal, lblTRXKMDetilValutaAsingJumlahRp.Text)
                    FillOrNothing(objKas.AccountNumber, txtTRXKMNoRekening.Text)
                    objKas.Kas = "Kas Masuk"
                    objKas.Type = "LTKT"
                    If Me.cboDebitCredit.SelectedValue = "D" Then
                        Throw New Exception("Only Kas Keluar (Debet) are allowed")
                    End If

                    'Insert Data Ke ObjectTransaction
                    objKas.TransactionCashIn = New LTKTTransactionCashIn
                    FillOrNothing(objKas.TransactionCashIn.TanggalTransaksi, txtTRXKMTanggalTrx.Text, True, oDate)
                    FillOrNothing(objKas.TransactionCashIn.NamaKantorPJK, txtTRXKMNamaKantor.Text)
                    FillOrNothing(objKas.TransactionCashIn.FK_MsKotaKab_Id, hfTRXKMKotaKab.Value, True, oInt)
                    FillOrNothing(objKas.TransactionCashIn.FK_MsProvince_Id, hfTRXKMProvinsi.Value, True, oInt)
                    FillOrNothing(objKas.TransactionCashIn.NomorRekening, txtTRXKMNoRekening.Text)



                    If rblTRXKMTipePelapor.SelectedValue = "" Then

                        objColKas(SetnGetRowEdit).TransactionCashIn.TipeTerlapor = Nothing
                    Else
                        objColKas(SetnGetRowEdit).TransactionCashIn.TipeTerlapor = rblTRXKMTipePelapor.SelectedValue

                    End If



                    'Individu
                    FillOrNothing(objKas.TransactionCashIn.INDV_Gelar, txtTRXKMINDVGelar.Text)
                    FillOrNothing(objKas.TransactionCashIn.INDV_NamaLengkap, txtTRXKMINDVNamaLengkap.Text)
                    FillOrNothing(objKas.TransactionCashIn.INDV_TempatLahir, txtTRXKMINDVTempatLahir.Text)
                    FillOrNothing(objKas.TransactionCashIn.INDV_TanggalLahir, txtTRXKMINDVTanggalLahir.Text, True, oDate)

                    If rblTRXKMINDVKewarganegaraan.SelectedValue = "" Then
                        objKas.TransactionCashIn.INDV_Kewarganegaraan = Nothing
                    Else
                        objKas.TransactionCashIn.INDV_Kewarganegaraan = rblTRXKMINDVKewarganegaraan.SelectedValue
                    End If



                    If cboTRXKMINDVNegara.SelectedIndex <> 0 Then
                        FillOrNothing(objKas.TransactionCashIn.INDV_FK_MsNegara_Id, cboTRXKMINDVNegara.SelectedValue, True, oInt)
                    End If
                    FillOrNothing(objKas.TransactionCashIn.INDV_DOM_NamaJalan, txtTRXKMINDVDOMNamaJalan.Text)
                    FillOrNothing(objKas.TransactionCashIn.INDV_DOM_RTRW, txtTRXKMINDVDOMRTRW.Text)
                    FillOrNothing(objKas.TransactionCashIn.INDV_DOM_FK_MsKelurahan_Id, hfTRXKMINDVDOMKelurahan.Value, True, OLong)
                    FillOrNothing(objKas.TransactionCashIn.INDV_DOM_FK_MsKecamatan_Id, hfTRXKMINDVDOMKecamatan.Value, True, oInt)
                    FillOrNothing(objKas.TransactionCashIn.INDV_DOM_FK_MsKotaKab_Id, hfTRXKMINDVDOMKotaKab.Value, True, oInt)
                    FillOrNothing(objKas.TransactionCashIn.INDV_DOM_KodePos, txtTRXKMINDVDOMKodePos.Text)
                    FillOrNothing(objKas.TransactionCashIn.INDV_DOM_FK_MsProvince_Id, hfTRXKMINDVDOMProvinsi.Value, True, oInt)
                    FillOrNothing(objKas.TransactionCashIn.INDV_DOM_FK_MsNegara_Id, hfTRXKMINDVDOMNegara.Value, True, oInt)

                    FillOrNothing(objKas.TransactionCashIn.INDV_ID_NamaJalan, txtTRXKMINDVIDNamaJalan.Text)
                    FillOrNothing(objKas.TransactionCashIn.INDV_ID_RTRW, txtTRXKMINDVIDRTRW.Text)
                    FillOrNothing(objKas.TransactionCashIn.INDV_ID_FK_MsKelurahan_Id, hfTRXKMINDVIDKelurahan.Value, True, OLong)
                    FillOrNothing(objKas.TransactionCashIn.INDV_ID_FK_MsKecamatan_Id, hfTRXKMINDVIDKecamatan.Value, True, oInt)
                    FillOrNothing(objKas.TransactionCashIn.INDV_ID_FK_MsKotaKab_Id, hfTRXKMINDVIDKotaKab.Value, True, oInt)
                    FillOrNothing(objKas.TransactionCashIn.INDV_ID_KodePos, txtTRXKMINDVIDKodePos.Text)
                    FillOrNothing(objKas.TransactionCashIn.INDV_ID_FK_MsProvince_Id, hfTRXKMINDVIDProvinsi.Value, True, oInt)
                    FillOrNothing(objKas.TransactionCashIn.INDV_ID_FK_MsNegara_Id, hfTRXKMINDVIDNegara.Value, True, oInt)

                    FillOrNothing(objKas.TransactionCashIn.INDV_NA_NamaJalan, txtTRXKMINDVNANamaJalan.Text)

                    FillOrNothing(objKas.TransactionCashIn.INDV_NA_FK_MsNegara_Id, hfTRXKMINDVNANegara.Value, True, oInt)

                    FillOrNothing(objKas.TransactionCashIn.INDV_NA_FK_MsProvince_Id, hfTRXKMINDVNAProvinsi.Value, True, oInt)
                    FillOrNothing(objKas.TransactionCashIn.INDV_NA_FK_MsKotaKab_Id, hfTRXKMINDVNAKota.Value, True, oInt)
                    FillOrNothing(objKas.TransactionCashIn.INDV_NA_KodePos, txtTRXKMINDVNAKodePos.Text)
                    If cboTRXKMINDVJenisID.SelectedIndex <> 0 Then
                        FillOrNothing(objKas.TransactionCashIn.INDV_FK_MsIDType_Id, cboTRXKMINDVJenisID.SelectedValue, True, oInt)
                    End If
                    FillOrNothing(objKas.TransactionCashIn.INDV_NomorId, txtTRXKMINDVNomorID.Text)
                    FillOrNothing(objKas.TransactionCashIn.INDV_NPWP, txtTRXKMINDVNPWP.Text)
                    FillOrNothing(objKas.TransactionCashIn.INDV_FK_MsPekerjaan_Id, hfTRXKMINDVPekerjaan.Value, True, oInt)
                    FillOrNothing(objKas.TransactionCashIn.INDV_Jabatan, txtTRXKMINDVJabatan.Text)
                    FillOrNothing(objKas.TransactionCashIn.INDV_PenghasilanRataRata, txtTRXKMINDVPenghasilanRataRata.Text)
                    FillOrNothing(objKas.TransactionCashIn.INDV_TempatBekerja, txtTRXKMINDVTempatKerja.Text)
                    FillOrNothing(objKas.TransactionCashIn.INDV_TujuanTransaksi, txtTRXKMINDVTujuanTrx.Text)
                    FillOrNothing(objKas.TransactionCashIn.INDV_SumberDana, txtTRXKMINDVSumberDana.Text)
                    FillOrNothing(objKas.TransactionCashIn.INDV_NamaBankLain, txtTRXKMINDVNamaBankLain.Text)
                    FillOrNothing(objKas.TransactionCashIn.INDV_NomorRekeningTujuan, txtTRXKMINDVNoRekeningTujuan.Text)
                    If cboTRXKMCORPBentukBadanUsaha.SelectedIndex <> 0 Then
                        FillOrNothing(objKas.TransactionCashIn.CORP_FK_MsBentukBadanUsaha_Id, cboTRXKMCORPBentukBadanUsaha.SelectedValue, True, oInt)
                    End If
                    FillOrNothing(objKas.TransactionCashIn.CORP_Nama, txtTRXKMCORPNama.Text)
                    FillOrNothing(objKas.TransactionCashIn.CORP_FK_MsBidangUsaha_Id, hfTRXKMCORPBidangUsaha.Value, True, oInt)
                    FillOrNothing(objKas.TransactionCashIn.CORP_TipeAlamat, rblTRXKMCORPTipeAlamat.SelectedIndex, True, oByte)
                    FillOrNothing(objKas.TransactionCashIn.CORP_NamaJalan, txtTRXKMCORPDLNamaJalan.Text)
                    FillOrNothing(objKas.TransactionCashIn.CORP_RTRW, txtTRXKMCORPDLRTRW.Text)
                    FillOrNothing(objKas.TransactionCashIn.CORP_FK_MsKelurahan_Id, hfTRXKMCORPDLKelurahan.Value, True, OLong)
                    FillOrNothing(objKas.TransactionCashIn.CORP_FK_MsKecamatan_Id, hfTRXKMCORPDLKecamatan.Value, True, oInt)
                    FillOrNothing(objKas.TransactionCashIn.CORP_FK_MsKotaKab_Id, hfTRXKMCORPDLKotaKab.Value, True, oInt)
                    FillOrNothing(objKas.TransactionCashIn.CORP_KodePos, txtTRXKMCORPDLKodePos.Text)
                    FillOrNothing(objKas.TransactionCashIn.CORP_FK_MsProvince_Id, hfTRXKMCORPDLProvinsi.Value, True, oInt)

                    FillOrNothing(objKas.TransactionCashIn.CORP_FK_MsNegara_Id, hfTRXKMCORPDLNegara.Value, True, oInt)



                    FillOrNothing(objKas.TransactionCashIn.CORP_LN_NamaJalan, txtTRXKMCORPLNNamaJalan.Text)
                    FillOrNothing(objKas.TransactionCashIn.CORP_LN_FK_MsNegara_Id, hfTRXKMCORPLNNegara.Value, True, oInt)


                    FillOrNothing(objKas.TransactionCashIn.CORP_LN_FK_MsProvince_Id, hfTRXKMCORPLNProvinsi.Value, True, oInt)
                    FillOrNothing(objKas.TransactionCashIn.CORP_LN_MsKotaKab_Id, hfTRXKMCORPLNKota.Value, True, oInt)
                    FillOrNothing(objKas.TransactionCashIn.CORP_LN_KodePos, txtTRXKMCORPLNKodePos.Text)
                    FillOrNothing(objKas.TransactionCashIn.CORP_NPWP, txtTRXKMCORPNPWP.Text)
                    FillOrNothing(objKas.TransactionCashIn.CORP_TujuanTransaksi, txtTRXKMCORPTujuanTrx.Text)
                    FillOrNothing(objKas.TransactionCashIn.CORP_SumberDana, txtTRXKMCORPSumberDana.Text)
                    FillOrNothing(objKas.TransactionCashIn.CORP_NamaBankLain, txtTRXKMCORPNamaBankLain.Text)
                    FillOrNothing(objKas.TransactionCashIn.CORP_NomorRekeningTujuan, txtTRXKMCORPNoRekeningTujuan.Text)

                    'Insert Data Ke Detail Transaction
                    objKas.DetailTransactionCashIn = New List(Of LTKTDetailCashInTransaction)
                    Dim DetilCol As List(Of LTKTDetailCashInEdit) = SetnGetgrvTRXKMDetilValutaAsing

                    If DetilCol.Count > 0 Then
                        For Each singleObj As LTKTDetailCashInEdit In DetilCol
                            Dim objDetilKasMasuk As New LTKTDetailCashInTransaction

                            Dim matauangKM As String = singleObj.MataUang.Substring(0, singleObj.MataUang.IndexOf("-")).Trim
                            Dim objCurrency As TList(Of MsCurrency) = DataRepository.MsCurrencyProvider.GetPaged(MsCurrencyColumn.Code.ToString & " like '%" & matauangKM.ToString & "%'", "", 0, Integer.MaxValue, 0)
                            If objCurrency.Count > 0 Then
                                FillOrNothing(objDetilKasMasuk.Asing_FK_MsCurrency_Id, objCurrency(0).IdCurrency, True, oInt)
                            End If

                            FillOrNothing(objDetilKasMasuk.Asing_KursTransaksi, singleObj.KursTransaksi, True, oDecimal)

                            Using objidr As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)
                                If Not objidr Is Nothing Then
                                    If objidr.MsSystemParameter_Value = objDetilKasMasuk.Asing_FK_MsCurrency_Id.GetValueOrDefault(0) Or objDetilKasMasuk.Asing_FK_MsCurrency_Id.GetValueOrDefault(0) Then
                                        FillOrNothing(objDetilKasMasuk.KasMasuk, singleObj.JumlahRp, True, oDecimal)
                                    Else
                                        FillOrNothing(objDetilKasMasuk.Asing_TotalKasMasukDalamRupiah, singleObj.JumlahRp, True, oDecimal)
                                    End If
                                End If
                            End Using

                            FillOrNothing(objDetilKasMasuk.TotalKasMasuk, lblTRXKMDetilValutaAsingJumlahRp.Text, True, oDecimal)

                            'Masukin ke detail
                            objKas.DetailTransactionCashIn.Add(objDetilKasMasuk)
                        Next


                        'Masukin KasMasuknya aja
                        If txtTRXKMDetilKasMasuk.Text <> "" Then
                            Dim objDetilKasMasuk As New LTKTDetailCashInTransaction
                            FillOrNothing(objDetilKasMasuk.KasMasuk, txtTRXKMDetilKasMasuk.Text, True, oDecimal)
                            FillOrNothing(objDetilKasMasuk.TotalKasMasuk, lblTRXKMDetilValutaAsingJumlahRp.Text, True, oDecimal)
                            objKas.DetailTransactionCashIn.Add(objDetilKasMasuk)
                        End If

                        'todo Tanyakan ?
                        'Else
                        '    Dim objDetilKasMasuk As New LTKTDetailCashInTransaction
                        '    objDetilKasMasuk.KasMasuk = txtTRXKMDetilKasMasuk.Text
                        '    objDetilKasMasuk.TotalKasMasuk = lblTRXKMDetilValutaAsingJumlahRp.Text
                        '    objKas.DetailTransactionCashIn.Add(objDetilKasMasuk)
                    End If

                    objColKas.Add(objKas)
                End If

                If lblDetilKasKeluarJumlahRp.Text <> vbNullString OrElse lblDetilKasKeluarJumlahRp.Text <> "" Then
                    Dim objKas As ResumeKasMasukKeluarLTKTAdd = New ResumeKasMasukKeluarLTKTAdd
                    'Insert untuk tampilan Grid
                    FillOrNothing(objKas.TransactionDate, txtTRXKKTanggalTransaksi.Text, True, oDate)
                    FillOrNothing(objKas.Branch, txtTRXKKNamaKantor.Text)
                    FillOrNothing(objKas.TransactionNominal, lblDetilKasKeluarJumlahRp.Text)
                    FillOrNothing(objKas.AccountNumber, txtTRXKKRekeningKK.Text)
                    objKas.Kas = "Kas Keluar"
                    objKas.Type = "LTKT"
                    If Me.cboDebitCredit.SelectedValue = "C" Then
                        Throw New Exception("Only Kas Masuk(Credit) are allowed")
                    End If

                    'Insert Data Ke ObjectTransction
                    objKas.TransactionCashOut = New LTKTTransactionCashOut
                    FillOrNothing(objKas.TransactionCashOut.TanggalTransaksi, txtTRXKKTanggalTransaksi.Text, True, oDate)
                    FillOrNothing(objKas.TransactionCashOut.NamaKantorPJK, txtTRXKKNamaKantor.Text)
                    FillOrNothing(objKas.TransactionCashOut.FK_MsKotaKab_Id, hfTRXKKKotaKab.Value, True, oInt)
                    FillOrNothing(objKas.TransactionCashOut.FK_MsProvince_Id, hfTRXKKProvinsi.Value, True, oInt)
                    FillOrNothing(objKas.TransactionCashOut.NomorRekening, txtTRXKKRekeningKK.Text)
                    If rblTRXKKTipePelapor.SelectedValue = "" Then

                        objKas.TransactionCashOut.TipeTerlapor = Nothing
                    Else
                        objKas.TransactionCashOut.TipeTerlapor = rblTRXKKTipePelapor.SelectedValue

                    End If




                    FillOrNothing(objKas.TransactionCashOut.INDV_Gelar, txtTRXKKINDVGelar.Text)
                    FillOrNothing(objKas.TransactionCashOut.INDV_NamaLengkap, txtTRXKKINDVNamaLengkap.Text)
                    FillOrNothing(objKas.TransactionCashOut.INDV_TempatLahir, txtTRXKKINDVTempatLahir.Text)
                    FillOrNothing(objKas.TransactionCashOut.INDV_TanggalLahir, txtTRXKKINDVTglLahir.Text, True, oDate)



                    If rblTRXKKINDVKewarganegaraan.SelectedValue = "" Then
                        objKas.TransactionCashOut.INDV_Kewarganegaraan = Nothing
                    Else
                        objKas.TransactionCashOut.INDV_Kewarganegaraan = rblTRXKKINDVKewarganegaraan.SelectedValue
                    End If



                    If cboTRXKKINDVNegara.SelectedIndex <> 0 Then
                        FillOrNothing(objKas.TransactionCashOut.INDV_FK_MsNegara_Id, cboTRXKKINDVNegara.SelectedValue, True, oInt)
                        FillOrNothing(objKas.TransactionCashOut.INDV_ID_FK_MsNegara_Id, cboTRXKKINDVNegara.SelectedValue, True, oInt)
                    End If
                    FillOrNothing(objKas.TransactionCashOut.INDV_DOM_NamaJalan, txtTRXKKINDVDOMNamaJalan.Text)
                    FillOrNothing(objKas.TransactionCashOut.INDV_DOM_RTRW, txtTRXKKINDVDOMRTRW.Text)
                    FillOrNothing(objKas.TransactionCashOut.INDV_DOM_FK_MsKelurahan_Id, hfTRXKKINDVDOMKelurahan.Value, True, OLong)
                    FillOrNothing(objKas.TransactionCashOut.INDV_DOM_FK_MsKecamatan_Id, hfTRXKKINDVDOMKecamatan.Value, True, oInt)
                    FillOrNothing(objKas.TransactionCashOut.INDV_DOM_FK_MsKotaKab_Id, hfTRXKKINDVDOMKotaKab.Value, True, oInt)
                    FillOrNothing(objKas.TransactionCashOut.INDV_DOM_KodePos, txtTRXKKINDVDOMKodePos.Text)
                    FillOrNothing(objKas.TransactionCashOut.INDV_DOM_FK_MsProvince_Id, hfTRXKKINDVDOMProvinsi.Value, True, oInt)
                    FillOrNothing(objKas.TransactionCashOut.INDV_DOM_FK_MsNegara_Id, hfTRXKKINDVDOMNegara.Value, True, oInt)

                    FillOrNothing(objKas.TransactionCashOut.INDV_ID_NamaJalan, txtTRXKKINDVIDNamaJalan.Text)
                    FillOrNothing(objKas.TransactionCashOut.INDV_ID_RTRW, txtTRXKKINDVIDRTRW.Text)
                    FillOrNothing(objKas.TransactionCashOut.INDV_ID_FK_MsKelurahan_Id, hfTRXKKINDVIDKelurahan.Value, True, OLong)
                    FillOrNothing(objKas.TransactionCashOut.INDV_ID_FK_MsKecamatan_Id, hfTRXKKINDVIDKecamatan.Value, True, oInt)
                    FillOrNothing(objKas.TransactionCashOut.INDV_ID_FK_MsKotaKab_Id, hfTRXKKINDVIDKotaKab.Value, True, oInt)
                    FillOrNothing(objKas.TransactionCashOut.INDV_ID_KodePos, txtTRXKKINDVIDKodePos.Text)
                    FillOrNothing(objKas.TransactionCashOut.INDV_ID_FK_MsProvince_Id, hfTRXKKINDVIDProvinsi.Value, True, oInt)
                    FillOrNothing(objKas.TransactionCashOut.INDV_ID_FK_MsNegara_Id, hfTRXKKINDVIDNegara.Value, True, oInt)


                    FillOrNothing(objKas.TransactionCashOut.INDV_NA_NamaJalan, txtTRXKKINDVNANamaJalan.Text)

                    FillOrNothing(objKas.TransactionCashOut.INDV_NA_FK_MsNegara_Id, hfTRXKKINDVNANegara.Value, True, oInt)


                    FillOrNothing(objKas.TransactionCashOut.INDV_NA_FK_MsProvince_Id, hfTRXKKINDVNAProvinsi.Value, True, oInt)
                    FillOrNothing(objKas.TransactionCashOut.INDV_NA_FK_MsKotaKab_Id, hfTRXKKINDVNAKota.Value, True, oInt)
                    FillOrNothing(objKas.TransactionCashOut.INDV_NA_KodePos, txtTRXKKINDVNAKodePos.Text)
                    If cboTRXKKINDVJenisID.SelectedIndex <> 0 Then
                        FillOrNothing(objKas.TransactionCashOut.INDV_FK_MsIDType_Id, cboTRXKKINDVJenisID.SelectedValue, True, oInt)
                    End If
                    FillOrNothing(objKas.TransactionCashOut.INDV_NomorId, txtTRXKKINDVNomorId.Text)
                    FillOrNothing(objKas.TransactionCashOut.INDV_NPWP, txtTRXKKINDVNPWP.Text)
                    FillOrNothing(objKas.TransactionCashOut.INDV_FK_MsPekerjaan_Id, hfTRXKKINDVPekerjaan.Value, True, oInt)
                    FillOrNothing(objKas.TransactionCashOut.INDV_Jabatan, txtTRXKKINDVJabatan.Text)
                    FillOrNothing(objKas.TransactionCashOut.INDV_PenghasilanRataRata, txtTRXKKINDVPenghasilanRataRata.Text)
                    FillOrNothing(objKas.TransactionCashOut.INDV_TempatBekerja, txtTRXKKINDVTempatKerja.Text)
                    FillOrNothing(objKas.TransactionCashOut.INDV_TujuanTransaksi, txtTRXKKINDVTujuanTrx.Text)
                    FillOrNothing(objKas.TransactionCashOut.INDV_SumberDana, txtTRXKKINDVSumberDana.Text)
                    FillOrNothing(objKas.TransactionCashOut.INDV_NamaBankLain, txtTRXKKINDVNamaBankLain.Text)
                    FillOrNothing(objKas.TransactionCashOut.INDV_NomorRekeningTujuan, txtTRXKKINDVNoRekTujuan.Text)
                    FillOrNothing(objKas.TransactionCashOut.CORP_NamaBankLain, txtTRXKKCORPNamaBankLain.Text)
                    FillOrNothing(objKas.TransactionCashOut.CORP_NomorRekeningTujuan, txtTRXKKCORPNoRekeningTujuan.Text)
                    If cboTRXKKCORPBentukBadanUsaha.SelectedIndex <> 0 Then
                        FillOrNothing(objKas.TransactionCashOut.CORP_FK_MsBentukBadanUsaha_Id, cboTRXKKCORPBentukBadanUsaha.SelectedValue, True, oInt)
                    End If
                    FillOrNothing(objKas.TransactionCashOut.CORP_Nama, txtTRXKKCORPNama.Text)
                    FillOrNothing(objKas.TransactionCashOut.CORP_FK_MsBidangUsaha_Id, hfTRXKKCORPBidangUsaha.Value, True, oInt)
                    FillOrNothing(objKas.TransactionCashOut.CORP_TipeAlamat, rblTRXKKCORPTipeAlamat.SelectedIndex, True, oByte)
                    FillOrNothing(objKas.TransactionCashOut.CORP_NamaJalan, txtTRXKKCORPDLNamaJalan.Text)
                    FillOrNothing(objKas.TransactionCashOut.CORP_RTRW, txtTRXKKCORPDLRTRW.Text)
                    FillOrNothing(objKas.TransactionCashOut.CORP_FK_MsKelurahan_Id, hfTRXKKCORPDLKelurahan.Value, True, OLong)
                    FillOrNothing(objKas.TransactionCashOut.CORP_FK_MsKecamatan_Id, hfTRXKKCORPDLKecamatan.Value, True, oInt)
                    FillOrNothing(objKas.TransactionCashOut.CORP_FK_MsKotaKab_Id, hfTRXKKCORPDLKotaKab.Value, True, oInt)
                    FillOrNothing(objKas.TransactionCashOut.CORP_KodePos, txtTRXKKCORPDLKodePos.Text)
                    FillOrNothing(objKas.TransactionCashOut.CORP_FK_MsProvince_Id, hfTRXKKCORPDLProvinsi.Value, True, oInt)

                    FillOrNothing(objKas.TransactionCashOut.CORP_FK_MsNegara_Id, hfTRXKKCORPDLNegara.Value, True, oInt)

                    FillOrNothing(objKas.TransactionCashOut.CORP_LN_NamaJalan, txtTRXKKCORPLNNamaJalan.Text)
                    FillOrNothing(objKas.TransactionCashOut.CORP_LN_FK_MsNegara_Id, hfTRXKKCORPLNNegara.Value, True, oInt)


                    FillOrNothing(objKas.TransactionCashOut.CORP_LN_FK_MsProvince_Id, hfTRXKKCORPLNProvinsi.Value, True, oInt)
                    FillOrNothing(objKas.TransactionCashOut.CORP_LN_MsKotaKab_Id, hfTRXKKCORPLNKota.Value)
                    FillOrNothing(objKas.TransactionCashOut.CORP_LN_KodePos, txtTRXKKCORPLNKodePos.Text)
                    FillOrNothing(objKas.TransactionCashOut.CORP_NPWP, txtTRXKKCORPNPWP.Text)
                    FillOrNothing(objKas.TransactionCashOut.CORP_TujuanTransaksi, txtTRXKKCORPTujuanTrx.Text)
                    FillOrNothing(objKas.TransactionCashOut.CORP_SumberDana, txtTRXKKCORPSumberDana.Text)


                    'Insert Date Ke DetailTransaction
                    objKas.DetailTranscationCashOut = New List(Of LTKTDetailCashOutTransaction)
                    Dim DetilCol As List(Of LTKTDetailCashInEdit) = SetnGetgrvDetilKasKeluar

                    If DetilCol.Count > 0 Then
                        For Each singleObj As LTKTDetailCashInEdit In DetilCol
                            Dim objDetilKasKeluar As New LTKTDetailCashOutTransaction

                            Dim matauangKK As String = singleObj.MataUang.Substring(0, singleObj.MataUang.IndexOf("-")).Trim
                            Dim objCurrency As TList(Of MsCurrency) = DataRepository.MsCurrencyProvider.GetPaged(MsCurrencyColumn.Code.ToString & " like '%" & matauangKK.ToString & "%'", "", 0, Integer.MaxValue, 0)
                            If objCurrency.Count > 0 Then
                                FillOrNothing(objDetilKasKeluar.Asing_FK_MsCurrency_Id, objCurrency(0).IdCurrency, True, oInt)
                            End If

                            FillOrNothing(objDetilKasKeluar.Asing_KursTransaksi, singleObj.KursTransaksi, True, oDecimal)



                            Using objidr As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)
                                If Not objidr Is Nothing Then
                                    If objidr.MsSystemParameter_Value = objDetilKasKeluar.Asing_FK_MsCurrency_Id.GetValueOrDefault(0) Or objDetilKasKeluar.Asing_FK_MsCurrency_Id.GetValueOrDefault(0) Then
                                        FillOrNothing(objDetilKasKeluar.KasKeluar, singleObj.JumlahRp, True, oDecimal)
                                    Else
                                        FillOrNothing(objDetilKasKeluar.Asing_TotalKasKeluarDalamRupiah, singleObj.JumlahRp, True, oDecimal)
                                    End If
                                End If
                            End Using
                            FillOrNothing(objDetilKasKeluar.TotalKasKeluar, lblDetilKasKeluarJumlahRp.Text, True, oDecimal)

                            'Masukin ke detail
                            objKas.DetailTranscationCashOut.Add(objDetilKasKeluar)
                        Next

                        'Masukin KasKeluarnya aja
                        If txtTRXKKDetailKasKeluar.Text <> "" Then
                            Dim objDetilKasKeluar As New LTKTDetailCashOutTransaction
                            FillOrNothing(objDetilKasKeluar.TotalKasKeluar, lblDetilKasKeluarJumlahRp.Text, True, oDecimal)
                            FillOrNothing(objDetilKasKeluar.KasKeluar, txtTRXKKDetailKasKeluar.Text, True, oDecimal)
                            objKas.DetailTranscationCashOut.Add(objDetilKasKeluar)
                        End If

                    Else
                        Dim objDetilKasKeluar As New LTKTDetailCashOutTransaction
                        FillOrNothing(objDetilKasKeluar.TotalKasKeluar, lblDetilKasKeluarJumlahRp.Text, True, oDecimal)
                        FillOrNothing(objDetilKasKeluar.KasKeluar, txtTRXKKDetailKasKeluar.Text, True, oDecimal)
                        objKas.DetailTranscationCashOut.Add(objDetilKasKeluar)
                    End If


                    objColKas.Add(objKas)
                End If

                Dim totalKasMasuk As Decimal = 0
                Dim totalKasKeluar As Decimal = 0
                For Each kas As ResumeKasMasukKeluarLTKTAdd In objColKas
                    If kas.Kas = "Kas Masuk" Then
                        totalKasMasuk += CDec(kas.TransactionNominal)
                    Else
                        totalKasKeluar += CDec(kas.TransactionNominal)
                    End If
                Next

                lblTotalKasMasuk.Text = FormatMoneyWithComma(totalKasMasuk)
                lblTotalKasKeluar.Text = FormatMoneyWithComma(totalKasKeluar)

                grvTransaksi.DataSource = objColKas
                grvTransaksi.DataBind()

                clearKasMasukKasKeluar()
            End If


        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message.ToString
        End Try
    End Sub



    Protected Sub EditResume_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            clearKasMasukKasKeluar()
            Dim objgridviewrow As GridViewRow = CType(CType(sender, LinkButton).NamingContainer, GridViewRow)
            SetnGetRowEdit = objgridviewrow.RowIndex
            Dim defaultcurrency As Integer
            Using objparam As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)
                defaultcurrency = objparam.MsSystemParameter_Value
            End Using
            'LoadData ke KasMasuk dan Keluar
            Dim objResume As List(Of ResumeKasMasukKeluarLTKTAdd) = SetnGetResumeKasMasukKasKeluar

            'Ambil data buat cari PK Negara, Provinsi, KotaKab, dkk (ini variable yang bakal dpke berkali2)
            Dim i As Integer = 0 'buat iterasi
            Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetAll
            Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
            Dim objKelurahan As TList(Of MsKelurahan) = DataRepository.MsKelurahanProvider.GetAll
            Dim objSKelurahan As MsKelurahan 'Penampung hasil search kelurahan
            Dim objKecamatan As TList(Of MsKecamatan) = DataRepository.MsKecamatanProvider.GetAll
            Dim objSkecamatan As MsKecamatan 'Penampung hasil search kecamatan
            Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetAll
            Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi
            Dim objMapNegara As TList(Of MappingMsNegaraNCBSPPATK) = DataRepository.MappingMsNegaraNCBSPPATKProvider.GetAll
            Dim objSMapNegara As MappingMsNegaraNCBSPPATK 'Penampung hasil search mappingnegara yang pke picker
            Dim objNegara As TList(Of MsNegaraNCBS) = DataRepository.MsNegaraNCBSProvider.GetAll
            Dim objSnegara As MsNegaraNCBS 'Penampung hasil search negara yang pke picker
            Dim objPekerjaan As TList(Of MsPekerjaan) = DataRepository.MsPekerjaanProvider.GetAll
            Dim objSPekerjaan As MsPekerjaan 'Penampung hasil search pekerjaan
            Dim objBidangUsaha As TList(Of MsBidangUsaha) = DataRepository.MsBidangUsahaProvider.GetAll
            Dim objSBidangUsaha As MsBidangUsaha 'Penampung hasil search bidang usaha

            If objResume(SetnGetRowEdit).Kas = "Kas Masuk" Then

                MultiView1.ActiveViewIndex = 0
                Menu1.Items.Item(0).Selected = True

                'LoadData ke DetailKasMasuk
                Dim detailkasMasuk As Decimal = 0
                Dim objDetailKasMasuk As List(Of LTKTDetailCashInEdit) = SetnGetgrvTRXKMDetilValutaAsing
                For Each obj As LTKTDetailCashInTransaction In objResume(SetnGetRowEdit).DetailTransactionCashIn
                    Dim singleDetailKasMasuk As New LTKTDetailCashInEdit

                    If obj.Asing_TotalKasMasukDalamRupiah.HasValue Then

                        If obj.Asing_TotalKasMasukDalamRupiah.Value > 0 Then

                            If obj.Asing_KursTransaksi.HasValue Then
                                If obj.Asing_KursTransaksi.Value > 0 Then
                                    Try
                                        singleDetailKasMasuk.Jumlah = (CDec(obj.Asing_TotalKasMasukDalamRupiah.GetValueOrDefault) / CDec(obj.Asing_KursTransaksi.GetValueOrDefault)).ToString
                                    Catch ex As Exception
                                        singleDetailKasMasuk.Jumlah = obj.Asing_TotalKasMasukDalamRupiah.GetValueOrDefault
                                    End Try
                                Else
                                    singleDetailKasMasuk.Jumlah = obj.Asing_TotalKasMasukDalamRupiah.GetValueOrDefault
                                End If
                            End If

                            singleDetailKasMasuk.JumlahRp = obj.Asing_TotalKasMasukDalamRupiah.GetValueOrDefault
                            singleDetailKasMasuk.KursTransaksi = obj.Asing_KursTransaksi.GetValueOrDefault

                            Dim ID As Decimal = 0
                            If obj.Asing_FK_MsCurrency_Id.HasValue Then
                                ID = obj.Asing_FK_MsCurrency_Id
                            Else 'Jika Nothing pake IDR
                                ID = defaultcurrency
                            End If

                            Dim objKurs As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(ID)
                            If Not IsNothing(objKurs) Then
                                singleDetailKasMasuk.MataUang = objKurs.Code & " - " & objKurs.Name
                            End If

                            objDetailKasMasuk.Add(singleDetailKasMasuk)
                        Else
                            If obj.KasMasuk.HasValue Then
                                singleDetailKasMasuk.Jumlah = 0
                                singleDetailKasMasuk.JumlahRp = obj.KasMasuk.GetValueOrDefault
                                singleDetailKasMasuk.KursTransaksi = 0
                                Dim objKurs As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(defaultcurrency) ' Indonesia IDR
                                If Not IsNothing(objKurs) Then
                                    singleDetailKasMasuk.MataUang = objKurs.Code & " - " & objKurs.Name
                                End If
                                detailkasMasuk += obj.KasMasuk.GetValueOrDefault
                                objDetailKasMasuk.Add(singleDetailKasMasuk)
                            End If
                        End If

                    ElseIf obj.KasMasuk.HasValue Then
                        singleDetailKasMasuk.Jumlah = 0
                        singleDetailKasMasuk.JumlahRp = obj.KasMasuk.GetValueOrDefault
                        singleDetailKasMasuk.KursTransaksi = 0
                        Dim objKurs As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(defaultcurrency) ' Indonesia IDR
                        If Not IsNothing(objKurs) Then
                            singleDetailKasMasuk.MataUang = objKurs.Code & " - " & objKurs.Name
                        End If
                        detailkasMasuk += obj.KasMasuk.GetValueOrDefault
                        objDetailKasMasuk.Add(singleDetailKasMasuk)
                    End If

                Next


                lblTRXKMDetilValutaAsingJumlahRp.Text = ValidateBLL.FormatMoneyWithComma(objResume(SetnGetRowEdit).TransactionNominal)

                'Gridview Transaksi Kas masuk Detail Valuta Asing
                grvTRXKMDetilValutaAsing.DataSource = objDetailKasMasuk
                grvTRXKMDetilValutaAsing.DataBind()



                'LoadData ke field Kas Masuk
                txtTRXKMTanggalTrx.Text = FormatDate(objResume(SetnGetRowEdit).TransactionCashIn.TanggalTransaksi)
                txtTRXKMNamaKantor.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.NamaKantorPJK)

                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashIn.FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTRXKMKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTRXKMKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashIn.FK_MsProvince_Id)
                If Not objSProvinsi Is Nothing Then
                    txtTRXKMProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTRXKMProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                txtTRXKMNoRekening.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.NomorRekening)
                SafeSelectedValue(rblTRXKMTipePelapor, objResume(SetnGetRowEdit).TransactionCashIn.TipeTerlapor)
                txtTRXKMINDVGelar.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_Gelar)
                txtTRXKMINDVNamaLengkap.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_NamaLengkap)
                txtTRXKMINDVTempatLahir.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_TempatLahir)
                txtTRXKMINDVTanggalLahir.Text = FormatDate(objResume(SetnGetRowEdit).TransactionCashIn.INDV_TanggalLahir)
                'If txtTRXKMINDVTanggalLahir.Text = Date.MinValue Then
                '    txtTRXKMINDVTanggalLahir.Text = ""
                'End If
                If objResume(SetnGetRowEdit).TransactionCashIn.INDV_Kewarganegaraan <> "-" And objResume(SetnGetRowEdit).TransactionCashIn.INDV_Kewarganegaraan.Trim <> "" Then
                    SafeSelectedValue(rblTRXKMINDVKewarganegaraan, objResume(SetnGetRowEdit).TransactionCashIn.INDV_Kewarganegaraan)
                End If
                If rblTRXKMINDVKewarganegaraan.SelectedValue <> "" Then
                    i = 0
                    For Each itemCboNegara As ListItem In cboTRXKMINDVNegara.Items
                        If objResume(SetnGetRowEdit).TransactionCashIn.INDV_FK_MsNegara_Id.ToString = itemCboNegara.Value.ToString Then
                            cboTRXKMINDVNegara.SelectedIndex = i
                            Exit For
                        End If
                        i = i + 1
                    Next
                End If
                txtTRXKMINDVDOMNamaJalan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_DOM_NamaJalan)

                txtTRXKMINDVDOMRTRW.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_DOM_RTRW)
                objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objResume(SetnGetRowEdit).TransactionCashIn.INDV_DOM_FK_MsKelurahan_Id)
                If Not IsNothing(objSKelurahan) Then
                    txtTRXKMINDVDOMKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
                    hfTRXKMINDVDOMKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
                End If
                objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objResume(SetnGetRowEdit).TransactionCashIn.INDV_DOM_FK_MsKecamatan_Id)
                If Not IsNothing(objSkecamatan) Then
                    txtTRXKMINDVDOMKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
                    hfTRXKMINDVDOMKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashIn.INDV_DOM_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTRXKMINDVDOMKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTRXKMINDVDOMKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtTRXKMINDVDOMKodePos.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_DOM_KodePos)
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashIn.INDV_DOM_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTRXKMINDVDOMProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTRXKMINDVDOMProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If

                Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objResume(SetnGetRowEdit).TransactionCashIn.INDV_DOM_FK_MsNegara_Id.GetValueOrDefault(0))
                    If Not objNegaraRef Is Nothing Then
                        txtTRXKMINDVDOMNegara.Text = Safe(objNegaraRef.NamaNegara)
                        hfTRXKMINDVDOMNegara.Value = Safe(objNegaraRef.IDNegara)
                    End If
                End Using

                'chkTRXKMINDVCopyDOM.Checked = False
                txtTRXKMINDVIDNamaJalan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_ID_NamaJalan)
                txtTRXKMINDVIDRTRW.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_ID_RTRW)
                objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objResume(SetnGetRowEdit).TransactionCashIn.INDV_ID_FK_MsKelurahan_Id)
                If Not IsNothing(objSKelurahan) Then
                    txtTRXKMINDVIDKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
                    hfTRXKMINDVIDKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
                End If
                objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objResume(SetnGetRowEdit).TransactionCashIn.INDV_ID_FK_MsKecamatan_Id)
                If Not IsNothing(objSkecamatan) Then
                    txtTRXKMINDVIDKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
                    hfTRXKMINDVIDKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashIn.INDV_ID_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTRXKMINDVIDKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTRXKMINDVIDKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtTRXKMINDVIDKodePos.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_ID_KodePos)
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashIn.INDV_ID_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTRXKMINDVIDProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTRXKMINDVIDProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If

                Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objResume(SetnGetRowEdit).TransactionCashIn.INDV_ID_FK_MsNegara_Id.GetValueOrDefault(0))
                    If Not objNegaraRef Is Nothing Then

                        txtTRXKMINDVIDNegara.Text = Safe(objNegaraRef.NamaNegara)
                        hfTRXKMINDVIDNegara.Value = Safe(objNegaraRef.IDNegara)
                    End If
                End Using

                txtTRXKMINDVNANamaJalan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_NA_NamaJalan)
                'objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objResume(SetnGetRowEdit).TransactionCashIn.INDV_NA_FK_MsNegara_Id)
                'If Not IsNothing(objSMapNegara) Then
                '    objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                '    If Not IsNothing(objSnegara) Then
                '        txtTRXKMINDVNANegara.Text = Safe(objSnegara.NamaNegara)
                '        hfTRXKMINDVNANegara.Value = Safe(objSnegara.IDNegaraNCBS)
                '    End If
                'End If

                Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objResume(SetnGetRowEdit).TransactionCashIn.INDV_NA_FK_MsNegara_Id.GetValueOrDefault(0))
                    If Not objNegaraRef Is Nothing Then

                        txtTRXKMINDVNANegara.Text = Safe(objNegaraRef.NamaNegara)
                        hfTRXKMINDVNANegara.Value = Safe(objNegaraRef.IDNegara)
                    End If
                End Using

                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashIn.INDV_NA_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTRXKMINDVNAProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTRXKMINDVNAProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashIn.INDV_NA_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTRXKMINDVNAKota.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTRXKMINDVNAKota.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtTRXKMINDVNAKodePos.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_NA_KodePos)
                If objResume(SetnGetRowEdit).TransactionCashIn.INDV_FK_MsIDType_Id.GetValueOrDefault <> Nothing Then
                    i = 0
                    For Each itemJenisID As ListItem In cboTRXKMINDVJenisID.Items
                        If itemJenisID.Value = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_FK_MsIDType_Id) Then
                            cboTRXKMINDVJenisID.SelectedIndex = i
                            Exit For
                        End If
                        i = i + 1
                    Next
                End If
                txtTRXKMINDVNomorID.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_NomorId)
                txtTRXKMINDVNPWP.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_NPWP)
                objSPekerjaan = objPekerjaan.Find(MsPekerjaanColumn.IDPekerjaan, objResume(SetnGetRowEdit).TransactionCashIn.INDV_FK_MsPekerjaan_Id.GetValueOrDefault)
                If Not IsNothing(objSPekerjaan) Then
                    txtTRXKMINDVPekerjaan.Text = Safe(objSPekerjaan.NamaPekerjaan)
                    hfTRXKMINDVPekerjaan.Value = Safe(objSPekerjaan.IDPekerjaan)
                End If
                txtTRXKMINDVJabatan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_Jabatan)
                txtTRXKMINDVPenghasilanRataRata.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_PenghasilanRataRata)
                txtTRXKMINDVTempatKerja.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_TempatBekerja)
                txtTRXKMINDVTujuanTrx.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_TujuanTransaksi)
                txtTRXKMINDVSumberDana.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_SumberDana)
                txtTRXKMINDVNamaBankLain.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_NamaBankLain)
                txtTRXKMINDVNoRekeningTujuan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_NomorRekeningTujuan)

                'CORP
                If objResume(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault <> Nothing Then
                    i = 0
                    For Each itemBentukBadangUsaha As ListItem In cboTRXKMCORPBentukBadanUsaha.Items
                        If itemBentukBadangUsaha.Value.ToString = objResume(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsBentukBadanUsaha_Id Then
                            cboTRXKMCORPBentukBadanUsaha.SelectedIndex = i
                            Exit For
                        End If
                        i = i + 1
                    Next
                End If

                txtTRXKMCORPNama.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_Nama)
                objSBidangUsaha = objBidangUsaha.Find(MsBidangUsahaColumn.IdBidangUsaha, objResume(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsBidangUsaha_Id)
                If Not IsNothing(objSBidangUsaha) Then
                    txtTRXKMCORPBidangUsaha.Text = Safe(objSBidangUsaha.NamaBidangUsaha)
                    hfTRXKMCORPBidangUsaha.Value = Safe(objSBidangUsaha.IdBidangUsaha)
                End If
                If objResume(SetnGetRowEdit).TransactionCashIn.CORP_TipeAlamat.GetValueOrDefault <> Nothing Then
                    SafeNumIndex(rblTRXKMCORPTipeAlamat, objResume(SetnGetRowEdit).TransactionCashIn.CORP_TipeAlamat)
                End If
                txtTRXKMCORPDLNamaJalan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_NamaJalan)
                txtTRXKMCORPDLRTRW.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_RTRW)
                objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objResume(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsKelurahan_Id)
                If Not IsNothing(objSKelurahan) Then
                    txtTRXKMCORPDLKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
                    hfTRXKMCORPDLKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
                End If
                objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objResume(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsKecamatan_Id)
                If Not IsNothing(objSkecamatan) Then
                    txtTRXKMCORPDLKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
                    hfTRXKMCORPDLKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTRXKMCORPDLKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTRXKMCORPDLKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtTRXKMCORPDLKodePos.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_KodePos)
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTRXKMCORPDLProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTRXKMCORPDLProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                'objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objResume(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsNegara_Id)
                'If Not IsNothing(objSMapNegara) Then
                '    objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                '    If Not IsNothing(objSnegara) Then
                '        txtTRXKMCORPDLNegara.Text = Safe(objSnegara.NamaNegara)
                '        hfTRXKMCORPDLNegara.Value = Safe(objSnegara.IDNegaraNCBS)
                '    End If
                'End If

                Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objResume(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsNegara_Id.GetValueOrDefault(0))
                    If Not objNegaraRef Is Nothing Then

                        txtTRXKMCORPDLNegara.Text = Safe(objNegaraRef.NamaNegara)
                        hfTRXKMCORPDLNegara.Value = Safe(objNegaraRef.IDNegara)
                    End If
                End Using


                txtTRXKMCORPLNNamaJalan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_LN_NamaJalan)
                'objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objResume(SetnGetRowEdit).TransactionCashIn.CORP_LN_FK_MsNegara_Id)
                'If Not IsNothing(objSMapNegara) Then
                '    objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                '    If Not IsNothing(objSnegara) Then
                '        txtTRXKMCORPLNNegara.Text = Safe(objSnegara.NamaNegara)
                '        hfTRXKMCORPLNNegara.Value = Safe(objSnegara.IDNegaraNCBS)
                '    End If
                'End If



                Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objResume(SetnGetRowEdit).TransactionCashIn.CORP_LN_FK_MsNegara_Id.GetValueOrDefault(0))
                    If Not objNegaraRef Is Nothing Then

                        txtTRXKMCORPLNNegara.Text = Safe(objNegaraRef.NamaNegara)
                        hfTRXKMCORPLNNegara.Value = Safe(objNegaraRef.IDNegara)
                    End If
                End Using
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashIn.CORP_LN_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTRXKMCORPLNProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTRXKMCORPLNProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashIn.CORP_LN_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTRXKMCORPLNKota.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTRXKMCORPLNKota.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtTRXKMCORPLNKodePos.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_LN_KodePos)
                txtTRXKMCORPNPWP.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_NPWP)
                txtTRXKMCORPTujuanTrx.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_TujuanTransaksi)
                txtTRXKMCORPSumberDana.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_SumberDana)
                txtTRXKMCORPNamaBankLain.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_NamaBankLain)
                txtTRXKMCORPNoRekeningTujuan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_NomorRekeningTujuan)

                imgAdd.Visible = False
                imgClearAll.Visible = False
                imgSaveEdit.Visible = True
                imgCancelEdit.Visible = True

            ElseIf objResume(SetnGetRowEdit).Kas = "Kas Keluar" Then

                MultiView1.ActiveViewIndex = 1
                Menu1.Items.Item(1).Selected = True

                'LoadData ke DetailKasKeluar
                Dim detailkaskeluar As Decimal = 0
                Dim objDetailKasKeluar As List(Of LTKTDetailCashInEdit) = SetnGetgrvDetilKasKeluar
                For Each obj As LTKTDetailCashOutTransaction In objResume(SetnGetRowEdit).DetailTranscationCashOut
                    Dim singleDetailKasKeluar As New LTKTDetailCashInEdit

                    If obj.Asing_TotalKasKeluarDalamRupiah.HasValue Then

                        If obj.Asing_TotalKasKeluarDalamRupiah.Value > 0 Then

                            If obj.Asing_KursTransaksi.HasValue Then
                                If obj.Asing_KursTransaksi.Value > 0 Then
                                    Try
                                        singleDetailKasKeluar.Jumlah = (CDec(obj.Asing_TotalKasKeluarDalamRupiah.GetValueOrDefault) / CDec(obj.Asing_KursTransaksi.GetValueOrDefault)).ToString
                                    Catch ex As Exception
                                        singleDetailKasKeluar.Jumlah = obj.Asing_TotalKasKeluarDalamRupiah.GetValueOrDefault
                                    End Try

                                Else
                                    singleDetailKasKeluar.Jumlah = obj.Asing_TotalKasKeluarDalamRupiah.GetValueOrDefault
                                End If
                            End If

                            singleDetailKasKeluar.JumlahRp = obj.Asing_TotalKasKeluarDalamRupiah.GetValueOrDefault
                            singleDetailKasKeluar.KursTransaksi = obj.Asing_KursTransaksi.GetValueOrDefault

                            Dim ID As Decimal = 0
                            If obj.Asing_FK_MsCurrency_Id.HasValue Then
                                ID = obj.Asing_FK_MsCurrency_Id
                            Else 'Jika Nothing pake IDR
                                ID = defaultcurrency
                            End If

                            Dim objKurs As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(ID)
                            If Not IsNothing(objKurs) Then
                                singleDetailKasKeluar.MataUang = objKurs.Code & " - " & objKurs.Name
                            End If

                            objDetailKasKeluar.Add(singleDetailKasKeluar)
                        Else
                            If obj.KasKeluar.HasValue Then
                                singleDetailKasKeluar.Jumlah = 0
                                singleDetailKasKeluar.JumlahRp = obj.KasKeluar.GetValueOrDefault
                                singleDetailKasKeluar.KursTransaksi = 0
                                Dim objKurs As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(defaultcurrency) ' Indonesia IDR
                                If Not IsNothing(objKurs) Then
                                    singleDetailKasKeluar.MataUang = objKurs.Code & " - " & objKurs.Name
                                End If
                                detailkaskeluar += obj.KasKeluar.GetValueOrDefault
                                objDetailKasKeluar.Add(singleDetailKasKeluar)
                            End If
                        End If

                    ElseIf obj.KasKeluar.HasValue Then
                        singleDetailKasKeluar.Jumlah = 0
                        singleDetailKasKeluar.JumlahRp = obj.KasKeluar.GetValueOrDefault
                        singleDetailKasKeluar.KursTransaksi = 0
                        Dim objKurs As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(defaultcurrency) ' Indonesia IDR
                        If Not IsNothing(objKurs) Then
                            singleDetailKasKeluar.MataUang = objKurs.Code & " - " & objKurs.Name
                        End If
                        detailkaskeluar += obj.KasKeluar.GetValueOrDefault
                        objDetailKasKeluar.Add(singleDetailKasKeluar)
                    End If

                Next
                lblDetilKasKeluarJumlahRp.Text = ValidateBLL.FormatMoneyWithComma(objResume(SetnGetRowEdit).TransactionNominal)

                'Detail Kas Keluar GridView
                grvDetilKasKeluar.DataSource = objDetailKasKeluar
                grvDetilKasKeluar.DataBind()


                'LoadData ke field Kas Keluar
                txtTRXKKTanggalTransaksi.Text = FormatDate(objResume(SetnGetRowEdit).TransactionCashOut.TanggalTransaksi)
                txtTRXKKNamaKantor.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.NamaKantorPJK)

                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashOut.FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTRXKKKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTRXKKKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashOut.FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTRXKKProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTRXKKProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                txtTRXKKRekeningKK.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.NomorRekening)
                SafeSelectedValue(rblTRXKKTipePelapor, objResume(SetnGetRowEdit).TransactionCashOut.TipeTerlapor)
                txtTRXKKINDVGelar.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_Gelar)
                txtTRXKKINDVNamaLengkap.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_NamaLengkap)
                txtTRXKKINDVTempatLahir.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_TempatLahir)
                txtTRXKKINDVTglLahir.Text = FormatDate(objResume(SetnGetRowEdit).TransactionCashOut.INDV_TanggalLahir)
                'If txtTRXKKINDVTglLahir.Text = Date.MinValue Then
                '    txtTRXKKINDVTglLahir.Text = ""
                'End If
                If objResume(SetnGetRowEdit).TransactionCashOut.INDV_Kewarganegaraan <> "-" And objResume(SetnGetRowEdit).TransactionCashOut.INDV_Kewarganegaraan.Trim <> "" Then
                    SafeSelectedValue(rblTRXKKINDVKewarganegaraan, objResume(SetnGetRowEdit).TransactionCashOut.INDV_Kewarganegaraan)
                End If
                If rblTRXKKINDVKewarganegaraan.SelectedIndex <> -1 Then
                    i = 0
                    For Each itemCboNegara As ListItem In cboTRXKKINDVNegara.Items
                        If objResume(SetnGetRowEdit).TransactionCashOut.INDV_FK_MsNegara_Id.ToString = itemCboNegara.Value.ToString Then
                            cboTRXKKINDVNegara.SelectedIndex = i
                            Exit For
                        End If
                        i = i + 1
                    Next
                End If
                If objResume(SetnGetRowEdit).TransactionCashOut.CORP_TipeAlamat.GetValueOrDefault <> Nothing Then
                    rblTRXKKCORPTipeAlamat.SelectedIndex = CInt(objResume(SetnGetRowEdit).TransactionCashOut.CORP_TipeAlamat)
                End If
                txtTRXKKINDVDOMNamaJalan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_DOM_NamaJalan)
                txtTRXKKINDVDOMRTRW.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_DOM_RTRW)
                objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objResume(SetnGetRowEdit).TransactionCashOut.INDV_DOM_FK_MsKelurahan_Id)
                If Not IsNothing(objSKelurahan) Then
                    txtTRXKKINDVDOMKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
                    hfTRXKKINDVDOMKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
                End If
                objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objResume(SetnGetRowEdit).TransactionCashOut.INDV_DOM_FK_MsKecamatan_Id)
                If Not IsNothing(objSkecamatan) Then
                    txtTRXKKINDVDOMKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
                    hfTRXKKINDVDOMKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashOut.INDV_DOM_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTRXKKINDVDOMKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTRXKKINDVDOMKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtTRXKKINDVDOMKodePos.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_DOM_KodePos)
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashOut.INDV_DOM_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTRXKKINDVDOMProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTRXKKINDVDOMProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objResume(SetnGetRowEdit).TransactionCashOut.INDV_DOM_FK_MsNegara_Id.GetValueOrDefault(0))
                    If Not objNegaraRef Is Nothing Then

                        txtTRXKKINDVDOMNegara.Text = Safe(objNegaraRef.NamaNegara)
                        hfTRXKKINDVDOMNegara.Value = Safe(objNegaraRef.IDNegara)
                    End If
                End Using

                'chkTRXKKINDVCopyDOM.Checked = False
                txtTRXKKINDVIDNamaJalan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_ID_NamaJalan)
                txtTRXKKINDVIDRTRW.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_ID_RTRW)
                objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objResume(SetnGetRowEdit).TransactionCashOut.INDV_ID_FK_MsKelurahan_Id)
                If Not IsNothing(objSKelurahan) Then
                    txtTRXKKINDVIDKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
                    hfTRXKKINDVIDKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
                End If
                objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objResume(SetnGetRowEdit).TransactionCashOut.INDV_ID_FK_MsKecamatan_Id)
                If Not IsNothing(objSkecamatan) Then
                    txtTRXKKINDVIDKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
                    hfTRXKKINDVIDKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashOut.INDV_ID_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTRXKKINDVIDKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTRXKKINDVIDKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtTRXKKINDVIDKodePos.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_ID_KodePos)
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashOut.INDV_ID_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTRXKKINDVIDProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTRXKKINDVIDProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objResume(SetnGetRowEdit).TransactionCashOut.INDV_ID_FK_MsNegara_Id.GetValueOrDefault(0))
                    If Not objNegaraRef Is Nothing Then

                        txtTRXKKINDVIDNegara.Text = Safe(objNegaraRef.NamaNegara)
                        hfTRXKKINDVIDNegara.Value = Safe(objNegaraRef.IDNegara)
                    End If
                End Using
                txtTRXKKINDVNANamaJalan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_NA_NamaJalan)
                'objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objResume(SetnGetRowEdit).TransactionCashOut.INDV_NA_FK_MsNegara_Id)
                'If Not IsNothing(objSMapNegara) Then
                '    objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                '    If Not IsNothing(objSnegara) Then
                '        txtTRXKKINDVNANegara.Text = Safe(objSnegara.NamaNegara)
                '        hfTRXKKINDVNANegara.Value = Safe(objSnegara.IDNegaraNCBS)
                '    End If
                'End If

                Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objResume(SetnGetRowEdit).TransactionCashOut.INDV_NA_FK_MsNegara_Id.GetValueOrDefault(0))
                    If Not objNegaraRef Is Nothing Then

                        txtTRXKKINDVNANegara.Text = Safe(objNegaraRef.NamaNegara)
                        hfTRXKKINDVNANegara.Value = Safe(objNegaraRef.IDNegara)
                    End If
                End Using

                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashOut.INDV_NA_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTRXKKINDVNAProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTRXKKINDVNAProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashOut.INDV_NA_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTRXKKINDVNAKota.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTRXKKINDVNAKota.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtTRXKKINDVNAKodePos.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_NA_KodePos)
                If objResume(SetnGetRowEdit).TransactionCashOut.INDV_FK_MsIDType_Id.GetValueOrDefault <> Nothing Then
                    i = 0
                    For Each itemJenisID As ListItem In cboTRXKKINDVJenisID.Items
                        If itemJenisID.Value = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_FK_MsIDType_Id) Then
                            cboTRXKKINDVJenisID.SelectedIndex = i
                            Exit For
                        End If
                        i = i + 1
                    Next
                End If
                txtTRXKKINDVNomorId.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_NomorId)
                txtTRXKKINDVNPWP.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_NPWP)
                objSPekerjaan = objPekerjaan.Find(MsPekerjaanColumn.IDPekerjaan, objResume(SetnGetRowEdit).TransactionCashOut.INDV_FK_MsPekerjaan_Id.GetValueOrDefault)
                If Not IsNothing(objSPekerjaan) Then
                    txtTRXKKINDVPekerjaan.Text = Safe(objSPekerjaan.NamaPekerjaan)
                    hfTRXKKINDVPekerjaan.Value = Safe(objSPekerjaan.IDPekerjaan)
                End If
                txtTRXKKINDVJabatan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_Jabatan)
                txtTRXKKINDVPenghasilanRataRata.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_PenghasilanRataRata)
                txtTRXKKINDVTempatKerja.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_TempatBekerja)
                txtTRXKKINDVTujuanTrx.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_TujuanTransaksi)
                txtTRXKKINDVSumberDana.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_SumberDana)
                txtTRXKKINDVNamaBankLain.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_NamaBankLain)
                txtTRXKKINDVNoRekTujuan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_NomorRekeningTujuan)

                'CORP
                If objResume(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault <> Nothing Then
                    i = 0
                    For Each itemBentukBadangUsaha As ListItem In cboTRXKKCORPBentukBadanUsaha.Items
                        If itemBentukBadangUsaha.Value.ToString = objResume(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsBentukBadanUsaha_Id Then
                            cboTRXKKCORPBentukBadanUsaha.SelectedIndex = i
                            Exit For
                        End If
                        i = i + 1
                    Next
                End If

                txtTRXKKCORPNama.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_Nama)
                objSBidangUsaha = objBidangUsaha.Find(MsBidangUsahaColumn.IdBidangUsaha, objResume(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsBidangUsaha_Id)
                If Not IsNothing(objSBidangUsaha) Then
                    txtTRXKKCORPBidangUsaha.Text = Safe(objSBidangUsaha.NamaBidangUsaha)
                    hfTRXKKCORPBidangUsaha.Value = Safe(objSBidangUsaha.IdBidangUsaha)
                End If
                If objResume(SetnGetRowEdit).TransactionCashOut.CORP_TipeAlamat.GetValueOrDefault <> Nothing Then
                    SafeNumIndex(rblTRXKKCORPTipeAlamat, objResume(SetnGetRowEdit).TransactionCashOut.CORP_TipeAlamat)
                End If
                txtTRXKKCORPDLNamaJalan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_NamaJalan)
                txtTRXKKCORPDLRTRW.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_RTRW)
                objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objResume(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsKelurahan_Id)
                If Not IsNothing(objSKelurahan) Then
                    txtTRXKKCORPDLKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
                    hfTRXKKCORPDLKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
                End If
                objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objResume(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsKecamatan_Id)
                If Not IsNothing(objSkecamatan) Then
                    txtTRXKKCORPDLKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
                    hfTRXKKCORPDLKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTRXKKCORPDLKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTRXKKCORPDLKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtTRXKKCORPDLKodePos.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_KodePos)
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTRXKKCORPDLProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTRXKKCORPDLProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                'objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objResume(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsNegara_Id)
                'If Not IsNothing(objSMapNegara) Then
                '    objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                '    If Not IsNothing(objSnegara) Then
                '        txtTRXKKCORPDLNegara.Text = Safe(objSnegara.NamaNegara)
                '        hfTRXKKCORPDLNegara.Value = Safe(objSnegara.IDNegaraNCBS)
                '    End If
                'End If


                Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objResume(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsNegara_Id.GetValueOrDefault(0))
                    If Not objNegaraRef Is Nothing Then

                        txtTRXKKCORPDLNegara.Text = Safe(objNegaraRef.NamaNegara)
                        hfTRXKKCORPDLNegara.Value = Safe(objNegaraRef.IDNegara)
                    End If
                End Using


                txtTRXKKCORPLNNamaJalan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_LN_NamaJalan)
                'objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objResume(SetnGetRowEdit).TransactionCashOut.CORP_LN_FK_MsNegara_Id)
                'If Not IsNothing(objSMapNegara) Then
                '    objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                '    If Not IsNothing(objSnegara) Then
                '        txtTRXKKCORPLNNegara.Text = Safe(objSnegara.NamaNegara)
                '        hfTRXKKCORPLNNegara.Value = Safe(objSnegara.IDNegaraNCBS)
                '    End If
                'End If


                Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objResume(SetnGetRowEdit).TransactionCashOut.CORP_LN_FK_MsNegara_Id.GetValueOrDefault(0))
                    If Not objNegaraRef Is Nothing Then

                        txtTRXKKCORPLNNegara.Text = Safe(objNegaraRef.NamaNegara)
                        hfTRXKKCORPLNNegara.Value = Safe(objNegaraRef.IDNegara)
                    End If
                End Using


                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashOut.CORP_LN_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTRXKKCORPLNProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTRXKKCORPLNProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashOut.CORP_LN_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTRXKKCORPLNKota.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTRXKKCORPLNKota.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtTRXKKCORPLNKodePos.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_LN_KodePos)
                txtTRXKKCORPNPWP.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_NPWP)
                txtTRXKKCORPTujuanTrx.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_TujuanTransaksi)
                txtTRXKKCORPSumberDana.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_SumberDana)
                txtTRXKKCORPNamaBankLain.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_NamaBankLain)
                txtTRXKKCORPNoRekeningTujuan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_NomorRekeningTujuan)

                imgAdd.Visible = False
                imgClearAll.Visible = False
                imgSaveEdit.Visible = True
                imgCancelEdit.Visible = True
            End If

            ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('IdTerlapor','Img1','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
            ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('Umum','searchimage2','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)

        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Sub loadLTKTToField()
        Using objLTKT As LTKT = DataRepository.LTKTProvider.GetByPK_LTKT_Id(getLTKTPK)

            If Not objLTKT Is Nothing Then
                'Ambil data buat cari PK Negara, Provinsi, KotaKab, dkk (ini variable yang bakal dpke berkali2)
                Dim i As Integer = 0 'buat iterasi
                Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetAll
                Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
                Dim objKelurahan As TList(Of MsKelurahan) = DataRepository.MsKelurahanProvider.GetAll
                Dim objSKelurahan As MsKelurahan 'Penampung hasil search kelurahan
                Dim objKecamatan As TList(Of MsKecamatan) = DataRepository.MsKecamatanProvider.GetAll
                Dim objSkecamatan As MsKecamatan 'Penampung hasil search kecamatan
                Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetAll
                Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi
                Dim objMapNegara As TList(Of MappingMsNegaraNCBSPPATK) = DataRepository.MappingMsNegaraNCBSPPATKProvider.GetAll
                Dim objSMapNegara As MappingMsNegaraNCBSPPATK 'Penampung hasil search mappingnegara yang pke picker
                Dim objNegara As TList(Of MsNegaraNCBS) = DataRepository.MsNegaraNCBSProvider.GetAll
                Dim objSnegara As MsNegaraNCBS 'Penampung hasil search negara yang pke picker
                Dim objPekerjaan As TList(Of MsPekerjaan) = DataRepository.MsPekerjaanProvider.GetAll
                Dim objSPekerjaan As MsPekerjaan 'Penampung hasil search pekerjaan
                Dim objBidangUsaha As TList(Of MsBidangUsaha) = DataRepository.MsBidangUsahaProvider.GetAll
                Dim objSBidangUsaha As MsBidangUsaha 'Penampung hasil search bidang usaha

                txtUmumPJKPelapor.Text = Safe(objLTKT.NamaPJKPelapor)
                txtTglLaporan.Text = FormatDate(objLTKT.TanggalLaporan)
                txtPejabatPelapor.Text = Safe(objLTKT.NamaPejabatPJKPelapor)
                If objLTKT.NoLTKTKoreksi <> "" Then
                    cboTipeLaporan.SelectedIndex = 1
                    txtNoLTKTKoreksi.Text = Safe(objLTKT.NoLTKTKoreksi)
                    tipeLaporanCheck()
                End If
                Me.cboDebitCredit.SelectedValue = Safe(objLTKT.DebetCredit)

                i = 0
                For Each listKepemilikan As ListItem In cboTerlaporKepemilikan.Items
                    If objLTKT.FK_MsKepemilikan_Id.ToString = listKepemilikan.Value Then
                        cboTerlaporKepemilikan.SelectedIndex = i
                        Exit For
                    End If
                    i = i + 1
                Next
                txtLTKTInformasiLainnya.Text = Safe(objLTKT.InformasiLainnya)
                txtTerlaporNoRekening.Text = Safe(objLTKT.NoRekening)
                SafeSelectedValue(rblTerlaporTipePelapor, objLTKT.TipeTerlapor)
                LTKTTipeTerlaporChange()
                txtTerlaporGelar.Text = Safe(objLTKT.INDV_Gelar)
                txtTerlaporNamaLengkap.Text = Safe(objLTKT.INDV_NamaLengkap)
                txtTerlaporTempatLahir.Text = Safe(objLTKT.INDV_TempatLahir)
                txtTerlaporTglLahir.Text = FormatDate(objLTKT.INDV_TanggalLahir)
                SafeSelectedValue(rblTerlaporKewarganegaraan, objLTKT.INDV_Kewarganegaraan)
                i = 0
                For Each listNegara As ListItem In cboTerlaporNegara.Items
                    If objLTKT.INDV_FK_MsNegara_Id.GetValueOrDefault.ToString = listNegara.Value Then
                        cboTerlaporNegara.SelectedIndex = i
                        Exit For
                    End If
                    i = i + 1
                Next
                txtTerlaporDOMNamaJalan.Text = Safe(objLTKT.INDV_DOM_NamaJalan)
                txtTerlaporDOMRTRW.Text = Safe(objLTKT.INDV_DOM_RTRW)
                objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objLTKT.INDV_DOM_FK_MsKelurahan_Id)
                If Not IsNothing(objSKelurahan) Then
                    LblTerlaporDOMKelurahan.Text = "Data Core Bank : " & LTKTBLL.GetKelurahanDataCore(objLTKT.CIFNo)
                    txtTerlaporDOMKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
                    hfTerlaporDOMKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
                End If
                objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objLTKT.INDV_DOM_FK_MsKecamatan_Id)
                If Not IsNothing(objSkecamatan) Then
                    LblTerlaporDOMKecamatan.Text = "Data Core Bank :" & LTKTBLL.GetKecamatanDataCore(objLTKT.CIFNo)
                    txtTerlaporDOMKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
                    hfTerlaporDOMKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objLTKT.INDV_DOM_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    LblTerlaporDOMKotaKab.Text = "Data Core Bank :" & LTKTBLL.GetKotaKabDataCore(objLTKT.CIFNo)
                    txtTerlaporDOMKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTerlaporDOMKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtTerlaporDOMKodePos.Text = Safe(objLTKT.INDV_DOM_KodePos)
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objLTKT.INDV_DOM_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTerlaporDOMProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTerlaporDOMProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If


                'objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objLTKT.INDV_DOM_FK_MsNegara_Id)
                'If Not IsNothing(objSMapNegara) Then
                '    objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                '    If Not IsNothing(objSnegara) Then
                '        LblTerlaporDOMNegara.Text = "Data Core Bank :" & LTKTBLL.getNegaraDataCore(objLTKT.CIFNo)
                '        txtTerlaporDOMNegara.Text = Safe(objSnegara.NamaNegara)
                '        hfTerlaporDomNegara.Value = Safe(objSnegara.IDNegaraNCBS)
                '    End If
                'End If
                Using objNegaraDom As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objLTKT.INDV_DOM_FK_MsNegara_Id.GetValueOrDefault(0))
                    If Not objNegaraDom Is Nothing Then
                        LblTerlaporDOMNegara.Text = "Data Core Bank :" & LTKTBLL.getNegaraDataCore(objLTKT.CIFNo)
                        txtTerlaporDOMNegara.Text = Safe(objNegaraDom.NamaNegara)
                        hfTerlaporDomNegara.Value = Safe(objNegaraDom.IDNegara)
                    End If
                End Using


                txtTerlaporIDNamaJalan.Text = Safe(objLTKT.INDV_ID_NamaJalan)
                txtTerlaporIDRTRW.Text = Safe(objLTKT.INDV_ID_RTRW)
                objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objLTKT.INDV_ID_FK_MsKelurahan_Id)
                If Not IsNothing(objSKelurahan) Then
                    LblTerlaporIDKelurahan.Text = "Data Core Bank : " & LTKTBLL.GetKelurahanDataCore(objLTKT.CIFNo)
                    txtTerlaporIDKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
                    hfTerlaporIDKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
                End If
                objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objLTKT.INDV_ID_FK_MsKecamatan_Id)
                If Not IsNothing(objSkecamatan) Then
                    LblTerlaporIDKecamatan.Text = "Data Core Bank :" & LTKTBLL.GetKecamatanDataCore(objLTKT.CIFNo)
                    txtTerlaporIDKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
                    hfTerlaporIDKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objLTKT.INDV_ID_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    LblTerlaporIDKotaKab.Text = "Data Core Bank :" & LTKTBLL.GetKotaKabDataCore(objLTKT.CIFNo)
                    txtTerlaporIDKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTerlaporIDKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtTerlaporIDKodePos.Text = Safe(objLTKT.INDV_ID_KodePos)
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objLTKT.INDV_ID_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTerlaporIDProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTerlaporIDProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If



                'objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objLTKT.INDV_ID_FK_MsNegara_Id)
                'If Not IsNothing(objSMapNegara) Then
                '    objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                '    If Not IsNothing(objSnegara) Then
                '        LblTerlaporIDNegara.Text = "Data Core Bank :" & LTKTBLL.getNegaraDataCore(objLTKT.CIFNo)
                '        txtTerlaporIDNegara.Text = Safe(objSnegara.NamaNegara)
                '        hfTerlaporIDNegara.Value = Safe(objSnegara.IDNegaraNCBS)
                '    End If
                'End If
                Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objLTKT.INDV_ID_FK_MsNegara_Id.GetValueOrDefault(0))
                    If Not objNegaraRef Is Nothing Then
                        LblTerlaporIDNegara.Text = "Data Core Bank :" & LTKTBLL.getNegaraDataCore(objLTKT.CIFNo)
                        txtTerlaporIDNegara.Text = Safe(objNegaraRef.NamaNegara)
                        hfTerlaporIDNegara.Value = Safe(objNegaraRef.IDNegara)
                    End If
                End Using



                txtTerlaporNANamaJalan.Text = Safe(objLTKT.INDV_NA_NamaJalan)




                'objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objLTKT.INDV_NA_FK_MsNegara_Id)
                'If Not IsNothing(objSMapNegara) Then
                '    objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                '    If Not IsNothing(objSnegara) Then
                '        LblTerlaporIDNANegara.Text = "Data Core Bank :" & LTKTBLL.getNegaraDataCore(objLTKT.CIFNo)
                '        txtTerlaporNANegara.Text = Safe(objSnegara.NamaNegara)
                '        hfTerlaporIDNANegara.Value = Safe(objSnegara.IDNegaraNCBS)
                '    End If
                'End If
                Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objLTKT.INDV_NA_FK_MsNegara_Id.GetValueOrDefault(0))
                    If Not objNegaraRef Is Nothing Then
                        LblTerlaporIDNANegara.Text = "Data Core Bank :" & LTKTBLL.getNegaraDataCore(objLTKT.CIFNo)
                        txtTerlaporNANegara.Text = Safe(objNegaraRef.NamaNegara)
                        hfTerlaporIDNANegara.Value = Safe(objNegaraRef.IDNegara)
                    End If
                End Using

                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objLTKT.INDV_NA_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTerlaporNAProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTerlaporNAProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objLTKT.INDV_NA_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTerlaporNAKota.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTerlaporIDKota.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtTerlaporNAKodePos.Text = Safe(objLTKT.INDV_NA_KodePos)
                i = 0
                For Each jenisDoc As ListItem In cboTerlaporJenisDocID.Items
                    If objLTKT.INDV_FK_MsIDType_Id.GetValueOrDefault.ToString = jenisDoc.Value Then
                        cboTerlaporJenisDocID.SelectedIndex = i
                        Exit For
                    End If
                    i = i + 1
                Next
                txtTerlaporNomorID.Text = Safe(objLTKT.INDV_NomorId)
                txtTerlaporNPWP.Text = Safe(objLTKT.INDV_NPWP)
                objSPekerjaan = objPekerjaan.Find(MsPekerjaanColumn.IDPekerjaan, objLTKT.INDV_FK_MsPekerjaan_Id)
                If Not IsNothing(objSPekerjaan) Then
                    LblTerlaporPekerjaan.Text = "Data Core :" + LTKTBLL.GetPekerjaanDataCore(objLTKT.CIFNo)
                    txtTerlaporPekerjaan.Text = Safe(objSPekerjaan.NamaPekerjaan)
                    hfTerlaporPekerjaan.Value = Safe(objSPekerjaan.IDPekerjaan)
                End If
                txtTerlaporJabatan.Text = Safe(objLTKT.INDV_Jabatan)
                txtTerlaporPenghasilanRataRata.Text = Safe(objLTKT.INDV_PenghasilanRataRata)
                txtTerlaporTempatKerja.Text = Safe(objLTKT.INDV_TempatBekerja)
                i = 0
                For Each listBentukBadanUsaha As ListItem In cboTerlaporCORPBentukBadanUsaha.Items
                    If objLTKT.CORP_FK_MsBentukBadanUsaha_Id.ToString = listBentukBadanUsaha.Value Then
                        cboTerlaporCORPBentukBadanUsaha.SelectedIndex = i
                        Exit For
                    End If
                    i = i + 1
                Next
                txtTerlaporCORPNama.Text = Safe(objLTKT.CORP_Nama)
                objSBidangUsaha = objBidangUsaha.Find(MsBidangUsahaColumn.IdBidangUsaha, objLTKT.CORP_FK_MsBidangUsaha_Id)
                If Not IsNothing(objSBidangUsaha) Then
                    lblTerlaporCORPBidangUsaha.Text = "Data Core :" + LTKTBLL.getBidangUsahaByCif(objLTKT.CIFNo)
                    txtTerlaporCORPBidangUsaha.Text = Safe(objSBidangUsaha.NamaBidangUsaha)
                    hfTerlaporCORPBidangUsaha.Value = Safe(objSBidangUsaha.IdBidangUsaha)
                End If

                SafeNumIndex(rblTerlaporCORPTipeAlamat, objLTKT.CORP_TipeAlamat)

                txtTerlaporCORPDLNamaJalan.Text = Safe(objLTKT.CORP_NamaJalan)
                txtTerlaporCORPDLRTRW.Text = Safe(objLTKT.CORP_RTRW)

                objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objLTKT.CORP_FK_MsKelurahan_Id)
                If Not IsNothing(objSKelurahan) Then
                    txtTerlaporCORPDLKelurahanCore.Text = "Data Core :" + LTKTBLL.GetKelurahanDataCore(objLTKT.CIFNo)
                    txtTerlaporCORPDLKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
                    hfTerlaporCORPDLKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
                End If
                objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objLTKT.CORP_FK_MsKecamatan_Id)
                If Not IsNothing(objSkecamatan) Then
                    TxtTerlaporCORPDLKecamatanCore.Text = "Data Core :" + LTKTBLL.GetKecamatanDataCore(objLTKT.CIFNo)
                    txtTerlaporCORPDLKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
                    hfTerlaporCORPDLKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objLTKT.CORP_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTerlaporCORPDLKotaKabCore.Text = "Data Core :" + LTKTBLL.GetKotaKabDataCore(objLTKT.CIFNo)
                    txtTerlaporCORPDLKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTerlaporCORPDLKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtTerlaporCORPDLKodePos.Text = Safe(objLTKT.CORP_KodePos)
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objLTKT.CORP_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTerlaporCORPDLProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTerlaporCORPDLProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                'objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objLTKT.CORP_FK_MsNegara_Id)
                'If Not IsNothing(objSMapNegara) Then
                '    objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                '    If Not IsNothing(objSnegara) Then
                '        txtTerlaporCORPDLNegaraCore.Text = "Data Core :" + LTKTBLL.getNegaraDataCore(objLTKT.CIFNo)
                '        txtTerlaporCORPDLNegara.Text = Safe(objSnegara.NamaNegara)
                '        hfTerlaporCORPDLNegara.Value = Safe(objSnegara.IDNegaraNCBS)
                '    End If
                'End If

                Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objLTKT.CORP_FK_MsNegara_Id.GetValueOrDefault(0))
                    If Not objNegaraRef Is Nothing Then
                        txtTerlaporCORPDLNegaraCore.Text = "Data Core Bank :" & LTKTBLL.getNegaraDataCore(objLTKT.CIFNo)
                        txtTerlaporCORPDLNegara.Text = Safe(objNegaraRef.NamaNegara)
                        hfTerlaporCORPDLNegara.Value = Safe(objNegaraRef.IDNegara)
                    End If
                End Using

                txtTerlaporCORPLNNamaJalan.Text = Safe(objLTKT.CORP_LN_NamaJalan)
                'objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objLTKT.CORP_LN_FK_MsNegara_Id)
                'If Not IsNothing(objSMapNegara) Then
                '    objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                '    If Not IsNothing(objSnegara) Then
                '        TxtTerlaporCORPLNNegaraCore.Text = "Data Core :" + LTKTBLL.getNegaraDataCore(objLTKT.CIFNo)
                '        txtTerlaporCORPLNNegara.Text = Safe(objSnegara.NamaNegara)
                '        hfTerlaporCORPLNNegara.Value = Safe(objSnegara.IDNegaraNCBS)
                '    End If
                'End If

                Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objLTKT.CORP_LN_FK_MsNegara_Id.GetValueOrDefault(0))
                    If Not objNegaraRef Is Nothing Then
                        TxtTerlaporCORPLNNegaraCore.Text = "Data Core Bank :" & LTKTBLL.getNegaraDataCore(objLTKT.CIFNo)
                        txtTerlaporCORPLNNegara.Text = Safe(objNegaraRef.NamaNegara)
                        hfTerlaporCORPLNNegara.Value = Safe(objNegaraRef.IDNegara)
                    End If
                End Using
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objLTKT.CORP_LN_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTerlaporCORPLNProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTerlaporCORPLNProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objLTKT.CORP_LN_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTerlaporCORPLNKota.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTerlaporCORPLNKota.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtTerlaporCORPLNKodePos.Text = Safe(objLTKT.CORP_LN_KodePos)
                txtTerlaporCORPNPWP.Text = Safe(objLTKT.CORP_NPWP)
                txtCIFNo.Text = Safe(objLTKT.CIFNo)
            End If


        End Using
    End Sub


#End Region

    Protected Sub imgTRXKMINDVDOMNegara_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKMINDVDOMNegara.Click
        Try
            If Session("PickerNegara.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
                txtTRXKMINDVDOMNegara.Text = strData(1)
                hfTRXKMINDVDOMNegara.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTerlaporIDNegara_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTerlaporIDNegara.Click
        Try
            If Session("PickerNegara.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
                txtTerlaporIDNegara.Text = strData(1)
                hfTerlaporIDNegara.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTerlaporDOMNegara_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTerlaporDOMNegara.Click
        Try
            If Session("PickerNegara.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
                txtTerlaporDOMNegara.Text = strData(1)
                hfTerlaporDomNegara.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub imgTRXKMINDVIDNegara_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKMINDVIDNegara.Click
        Try
            If Session("PickerNegara.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
                txtTRXKMINDVIDNegara.Text = strData(1)
                hfTRXKMINDVIDNegara.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgTRXKKINDVDOMNegara_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKKINDVDOMNegara.Click
        Try
            If Session("PickerNegara.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
                txtTRXKKINDVDOMNegara.Text = strData(1)
                hfTRXKKINDVDOMNegara.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

   
    Protected Sub imgTRXKKINDVIDNegara_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTRXKKINDVIDNegara.Click
        Try
            If Session("PickerNegara.data") <> "" Then
                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
                txtTRXKKINDVIDNegara.Text = strData(1)
                hfTRXKKINDVIDNegara.Value = strData(0)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
End Class

#Region "Class LTKTDetailCashIn"
Public Class LTKTDetailCashInEdit

    Private _mataUang As String
    Public Property MataUang() As String
        Get
            Return _mataUang
        End Get
        Set(ByVal value As String)
            _mataUang = value
        End Set
    End Property

    Private _kursTransaksi As String
    Public Property KursTransaksi() As String
        Get
            Return _kursTransaksi
        End Get
        Set(ByVal value As String)
            _kursTransaksi = value
        End Set
    End Property

    Private _jumlah As String
    Public Property Jumlah() As String
        Get
            Return _jumlah
        End Get
        Set(ByVal value As String)
            _jumlah = value
        End Set
    End Property

    Private _jumlahRp As String
    Public Property JumlahRp() As String
        Get
            Return _jumlahRp
        End Get
        Set(ByVal value As String)
            _jumlahRp = value
        End Set
    End Property

End Class
#End Region

#Region "Class ResumeKasMasukKeluarLTKTAdd"

Public Class ResumeKasMasukKeluarLTKTAdd

    Private _kas As String
    Public Property Kas() As String
        Get
            Return _kas
        End Get
        Set(ByVal value As String)
            _kas = value
        End Set
    End Property

    Private _transactionDate As String
    Public Property TransactionDate() As String
        Get
            Return _transactionDate
        End Get
        Set(ByVal value As String)
            _transactionDate = value
        End Set
    End Property

    Private _branch As String
    Public Property Branch() As String
        Get
            Return _branch
        End Get
        Set(ByVal value As String)
            _branch = value
        End Set
    End Property

    Private _accountNumber As String
    Public Property AccountNumber() As String
        Get
            Return _accountNumber
        End Get
        Set(ByVal value As String)
            _accountNumber = value
        End Set
    End Property

    Private _Type As String
    Public Property Type() As String
        Get
            Return _Type
        End Get
        Set(ByVal value As String)
            _Type = value
        End Set
    End Property

    Private _transactionNominal As String
    Public Property TransactionNominal() As String
        Get
            Return _transactionNominal
        End Get
        Set(ByVal value As String)
            _transactionNominal = value
        End Set
    End Property

    Private _transactionCashIn As LTKTTransactionCashIn
    Public Property TransactionCashIn() As LTKTTransactionCashIn
        Get
            Return _transactionCashIn
        End Get
        Set(ByVal value As LTKTTransactionCashIn)
            _transactionCashIn = value
        End Set
    End Property

    Private _transactionCashOut As LTKTTransactionCashOut
    Public Property TransactionCashOut() As LTKTTransactionCashOut
        Get
            Return _transactionCashOut
        End Get
        Set(ByVal value As LTKTTransactionCashOut)
            _transactionCashOut = value
        End Set
    End Property

    Private _detailTransactionCashIn As List(Of LTKTDetailCashInTransaction)
    Public Property DetailTransactionCashIn() As List(Of LTKTDetailCashInTransaction)
        Get
            Return _detailTransactionCashIn
        End Get
        Set(ByVal value As List(Of LTKTDetailCashInTransaction))
            _detailTransactionCashIn = value
        End Set
    End Property

    Private _detailTransactionCashOut As List(Of LTKTDetailCashOutTransaction)
    Public Property DetailTranscationCashOut() As List(Of LTKTDetailCashOutTransaction)
        Get
            Return _detailTransactionCashOut
        End Get
        Set(ByVal value As List(Of LTKTDetailCashOutTransaction))
            _detailTransactionCashOut = value
        End Set
    End Property



End Class

#End Region



