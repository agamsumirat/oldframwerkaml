#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports System.Collections.Generic
Imports System.IO
Imports Sahassa.AML.Commonly
Imports AMLBLL.ValidateBLL
Imports AMLBLL.MsProvinceBll
Imports AMLBLL.DataType
Imports System.Drawing
#End Region
Partial Class MsProvince_UploadApprovalDetail_View
    Inherits Parent

#Region "Member"
    Private ReadOnly BindGridFromExcel As Boolean
#End Region

#Region "Property"

    Public ReadOnly Property SetnGetUserID() As String
        Get
            Return SessionPkUserId
        End Get

    End Property

    Private Property GetTotalInsert() As Double
        Get
            Return Session("MsProvince.GetTotalInsert")
        End Get
        Set(ByVal value As Double)
            Session("MsProvince.GetTotalInsert") = value
        End Set
    End Property

    Private Property GetTotalUpdate() As Double
        Get
            Return Session("MsProvince.GetTotalUpdate")
        End Get
        Set(ByVal value As Double)
            Session("MsProvince.GetTotalUpdate") = value
        End Set
    End Property

    Public ReadOnly Property parID() As String
        Get
            Return Request.Item("ID")
        End Get
    End Property

    Public Property SetIdProvince() As String
        Get
            If Not Session("MsProvince_ApprovalDetail.SetIdProvince") Is Nothing Then
                Return CStr(Session("MsProvince_ApprovalDetail.SetIdProvince"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("MsProvince_ApprovalDetail.SetIdProvince") = value
        End Set
    End Property

    Public Property SetNama() As String
        Get
            If Not Session("MsProvince_ApprovalDetail.SetNama") Is Nothing Then
                Return CStr(Session("MsProvince_ApprovalDetail.SetNama"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("MsProvince_ApprovalDetail.SetNama") = value
        End Set
    End Property

    Private Property SetnGetSort() As String
        Get
            Return CType(IIf(Session("MsProvince_ApprovalDetail.Sort") Is Nothing, " IdProvince  asc", Session("MsProvince_ApprovalDetail.Sort")), String)
        End Get
        Set(ByVal Value As String)
            Session("MsProvince_ApprovalDetail.Sort") = Value
        End Set
    End Property

    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return CType(IIf(Session("MsProvince_ApprovalDetail.SelectedItem") Is Nothing, New ArrayList, Session("MsProvince_ApprovalDetail.SelectedItem")), ArrayList)
        End Get
        Set(ByVal value As ArrayList)
            Session("MsProvince_ApprovalDetail.SelectedItem") = value
        End Set
    End Property

    Private Property SetnGetRowTotal() As Int32
        Get
            Return CType(IIf(Session("MsProvince_ApprovalDetail.RowTotal") Is Nothing, 0, Session("MsProvince_ApprovalDetail.RowTotal")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("MsProvince_ApprovalDetail.RowTotal") = Value
        End Set
    End Property

    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return iTotalPages(SetnGetRowTotal)
        End Get
    End Property

    Private Property SetnGetCurrentPage() As Int32
        Get
            Return CType(IIf(Session("MsProvince_ApprovalDetail.CurrentPage") Is Nothing, 0, Session("MsProvince_ApprovalDetail.CurrentPage")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("MsProvince_ApprovalDetail.CurrentPage") = Value
        End Set
    End Property

    Public ReadOnly Property SetnGetBindTable() As VList(Of Vw_MsProvince_UploadApprovalDetail)
        Get
            Dim strWhereClause(-1) As String
            Dim strAllWhereClause As String
            If SetnGetCurrentPage > GetPageTotal - 1 And GetPageTotal - 1 > 0 Then
                SetnGetCurrentPage = GetPageTotal - 1
            End If

            ReDim Preserve strWhereClause(strWhereClause.Length)
            strWhereClause(strWhereClause.Length - 1) = "FK_MsProvince_Approval_id=" & parID

            If SetIdProvince.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = Vw_MsProvince_UploadApprovalDetailColumn.IdProvince.ToString & " like '%" & SetIdProvince.Trim.Replace("'", "''") & "%'"
            End If
            If SetNama.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = Vw_MsProvince_UploadApprovalDetailColumn.Nama.ToString & " like '%" & SetNama.Trim.Replace("'", "''") & "%'"
            End If

            strAllWhereClause = String.Join(" and ", strWhereClause)
            Return DataRepository.Vw_MsProvince_UploadApprovalDetailProvider.GetPaged(strAllWhereClause, SetnGetSort, SetnGetCurrentPage, GetDisplayedTotalRow, SetnGetRowTotal)

        End Get

    End Property


#End Region

#Region "Function"

    Function ConvertTo_MappingMsProvinceNCBSPPATKl(ByVal SourceObject As TList(Of MappingMsProvinceNCBSPPATK_Approval_Detail)) As TList(Of MappingMsProvinceNCBSPPATK)
        Dim temp As New TList(Of MappingMsProvinceNCBSPPATK)
        For Each i As MappingMsProvinceNCBSPPATK_Approval_Detail In SourceObject
            Dim DestObject As New MappingMsProvinceNCBSPPATK
            With DestObject
                FillOrNothing(.Pk_MsProvinceNCBS_Id, i.IDProvinceNCBS, True, oInt)
                FillOrNothing(.Pk_MsProvince_Id, i.IDProvince, True, oInt)
                FillOrNothing(.Nama, i.Nama)
            End With

            temp.Add(DestObject)
            DestObject.Dispose()
        Next
        Return temp
    End Function

    Function ConvertTo_MsProvinceNCBS(ByVal SourceObject As TList(Of MsProvinceNCBS_approvalDetail)) As TList(Of MsProvinceNCBS)
        Dim temp As New TList(Of MsProvinceNCBS)
        For Each i As MsProvinceNCBS_approvalDetail In SourceObject
            Dim DestObject As MsProvinceNCBS = DataRepository.MsProvinceNCBSProvider.GetByIdProvinceNCBS(i.Pk_MsProvinceNCBS_Id)
            If DestObject Is Nothing Then
                DestObject = New MsProvinceNCBS
            Else
            End If

            With DestObject
                FillOrNothing(.IdProvinceNCBS, i.Pk_MsProvinceNCBS_Id, True, oInt)
                FillOrNothing(.Nama, i.MsProvinceNCBS_Name, True, oString)
                FillOrNothing(.Activation, i.MsProvinceNCBS_IsActive, True, oBit)
                FillOrNothing(.CreatedBy, i.MsProvinceNCBS_CreatedBy, True, oString)
                FillOrNothing(.CreatedDate, i.MsProvinceNCBS_CreatedDate, True, oDate)
                FillOrNothing(.ApprovedDate, Now, True, oDate)
                FillOrNothing(.ApprovedBy, SessionUserId, True, oString)
            End With

            temp.Add(DestObject)
            DestObject.Dispose()
        Next

        Return temp
    End Function
    Function ConvertTo_MsProvince(ByVal SourceObject As MsProvince_ApprovalDetail) As MsProvince
        Dim temp As New MsProvince
        Dim DestObject As MsProvince = DataRepository.MsProvinceProvider.GetByIdProvince(SourceObject.IdProvince)
        If DestObject Is Nothing Then
            GetTotalInsert += 1
            DestObject = New MsProvince
        Else
            GetTotalUpdate += 1
        End If

        With DestObject
            FillOrNothing(.IdProvince, SourceObject.IdProvince, True, oInt)
            FillOrNothing(.Nama, SourceObject.Nama, True, oString)
            FillOrNothing(.Activation, SourceObject.Activation, True, oBit)
            FillOrNothing(.CreatedBy, SourceObject.CreatedBy, True, oString)
            FillOrNothing(.CreatedDate, SourceObject.CreatedDate, True, oDate)
            FillOrNothing(.ApprovedDate, Now, True, oDate)
            FillOrNothing(.ApprovedBy, SessionUserId, True, oString)
            .Code = SourceObject.Code
            .Description = SourceObject.Description
        End With

        temp = DestObject
        DestObject.Dispose()

        Return temp
    End Function

    Private Sub ShowButonApproveReject(ByVal Show As Boolean)
        imgReject.Visible = Show
        imgApprove.Visible = Show
    End Sub

    Private Sub BindSelected()
        Dim Rows As New ArrayList

        For Each IdDIN As Int64 In SetnGetSelectedItem
            Using rowData As VList(Of Vw_MsProvince_UploadApprovalDetail) = DataRepository.Vw_MsProvince_UploadApprovalDetailProvider.GetPaged(" IdProvince = " & IdDIN & "", "", 0, Integer.MaxValue, 0)
                If rowData.Count > 0 Then
                    Rows.Add(rowData(0))
                End If
            End Using
        Next

        GridMsUserView.DataSource = Rows
        GridMsUserView.AllowPaging = False
        GridMsUserView.DataBind()
        For i As Integer = 0 To GridMsUserView.Items.Count - 1
            For y As Integer = 0 To GridMsUserView.Columns.Count - 1
                GridMsUserView.Items(i).Cells(y).Attributes.Add("class", "text")
            Next
        Next

        HideButtonGrid()

    End Sub

    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In GridMsUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = CType(gridRow.FindControl("CheckBoxExporttoExcel"), CheckBox)
                Dim PKMsUserId As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = SetnGetSelectedItem
                If ArrTarget.Contains(PKMsUserId) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(PKMsUserId)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(PKMsUserId)
                End If
                SetnGetSelectedItem = ArrTarget
            End If
        Next

    End Sub

    Private Sub SettingControlSearching()
        txtNama.Text = SetNama

    End Sub

    Private Sub SettingPropertySearching()
        SetNama = txtNama.Text.Trim
    End Sub

    Private Sub ClearThisPageSessions()
        Clearproperty()
        LblMessage.Visible = False
        LblMessage.Text = ""
        SetnGetRowTotal = Nothing
        SetnGetSort = Nothing
        SetnGetCurrentPage = 0
    End Sub

    Private Sub Clearproperty()
        SetIdProvince = Nothing
        SetNama = Nothing
    End Sub

    Private Sub popDateControl()

    End Sub

    Private Sub BindComboBox()

    End Sub

    Private Sub bindgrid()
        SettingControlSearching()
        GridMsUserView.DataSource = SetnGetBindTable
        GridMsUserView.CurrentPageIndex = SetnGetCurrentPage
        GridMsUserView.VirtualItemCount = SetnGetRowTotal
        GridMsUserView.DataBind()
        If SetnGetRowTotal > 0 Then
            LabelNoRecordFound.Visible = False
            ShowButonApproveReject(True)
        Else
            LabelNoRecordFound.Visible = True
            ShowButonApproveReject(False)
        End If
    End Sub

#End Region

#Region "events..."

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Response.Redirect("MsProvince_UploadApproval_View.aspx")
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            popDateControl()
            BindComboBox()
            LblMessage.Text = ""
            LblMessage.Visible = False
            MultiViewUtama.ActiveViewIndex = 0
            If Not Page.IsPostBack Then
                ClearThisPageSessions()
                'AuditTrailBLL.InsertAuditTrailUserAccess(SessionCurrentPage)
            End If
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : SetnGetCurrentPage = 0
                Case "Prev" : SetnGetCurrentPage -= 1
                Case "Next" : SetnGetCurrentPage += 1
                Case "Last" : SetnGetCurrentPage = GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
            CollectSelected()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBtnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnSearch.Click

        Try
            SettingPropertySearching()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub
    Private Sub SetInfoNavigate()
        PageCurrentPage.Text = (SetnGetCurrentPage + 1).ToString
        PageTotalPages.Text = GetPageTotal.ToString
        PageTotalRows.Text = SetnGetRowTotal.ToString
        LinkButtonNext.Enabled = (Not SetnGetCurrentPage + 1 = GetPageTotal) AndAlso GetPageTotal <> 0
        LinkButtonLast.Enabled = (Not SetnGetCurrentPage + 1 = GetPageTotal) AndAlso GetPageTotal <> 0
        First.Enabled = Not SetnGetCurrentPage = 0
        LinkButtonPrevious.Enabled = Not SetnGetCurrentPage = 0
    End Sub

    Private Sub SetCheckedAll()
        Dim i As Int16 = 0
        Dim TotalRow As Int16 = 0
        For Each gridRow As DataGridItem In GridMsUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = CType(gridRow.FindControl("CheckBoxExporttoExcel"), CheckBox)
                If chkBox.Checked Then
                    i = CType(i + 1, Int16)
                End If
                TotalRow = CType(TotalRow + 1, Int16)
            End If
        Next
        If TotalRow = 0 Then
            CheckBoxSelectAll.Checked = False
        Else
            If i = TotalRow Then
                CheckBoxSelectAll.Checked = True
            Else
                CheckBoxSelectAll.Checked = False
            End If
        End If
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            bindgrid()
            SetInfoNavigate()
            SetCheckedAll()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImgBtnClearSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnClearSearch.Click
        Try
            Clearproperty()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        'For Each gridRow As DataGridItem In GridMsUserView.Items
        '    If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
        '        Dim pkid As String = gridRow.Cells(1).Text
        '        Dim ArrTarget As ArrayList = SetnGetSelectedItem
        '        If SetnGetSelectedItem.Contains(pkid) Then
        '            If CheckBoxSelectAll.Checked Then
        '                If Not ArrTarget.Contains(pkid) Then
        '                    ArrTarget.Add(pkid)
        '                End If
        '            Else
        '                ArrTarget.Remove(pkid)
        '            End If
        '        Else
        '            If CheckBoxSelectAll.Checked Then
        '                If Not ArrTarget.Contains(pkid) Then
        '                    ArrTarget.Add(pkid)
        '                End If
        '            End If
        '        End If
        '        SetnGetSelectedItem = ArrTarget
        '    End If
        'Next
        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        For Each gridRow As DataGridItem In Me.GridMsUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(1).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItem.Contains(StrKey) = False Then
                        ArrTarget.Add(StrKey)
                    End If
                Else
                    If Me.SetnGetSelectedItem.Contains(StrKey) Then
                        ArrTarget.Remove(StrKey)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItem = ArrTarget
    End Sub


    Protected Sub GridMsUserView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridMsUserView.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Dim chkBox As CheckBox = CType(e.Item.FindControl("CheckBoxExporttoExcel"), CheckBox)
                chkBox.Checked = SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)
                If BindGridFromExcel = True Then
                    e.Item.Cells(2).Text = CStr((e.Item.ItemIndex + 1))
                Else
                    e.Item.Cells(2).Text = CStr((e.Item.ItemIndex + 1) + (SetnGetCurrentPage * GetDisplayedTotalRow))
                End If

                If e.Item.Cells(GridMsUserView.Columns.Count - 5).Text = "C" Then
                    e.Item.Cells(GridMsUserView.Columns.Count - 2).Enabled = False
                End If
            End If
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub GridMsUserView_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridMsUserView.SortCommand
        Dim GridUser As DataGrid = CType(source, DataGrid)
        Try
            SetnGetSort = ChangeSortCommand(e.SortExpression)
            GridUser.Columns(IndexSort(GridUser, e.SortExpression)).SortExpression = SetnGetSort
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub HideButtonGrid()
        With GridMsUserView
            .Columns(.Columns.Count - 1).Visible = False
        End With
    End Sub

    Private Sub BindSelectedAll()
        Try
            GridMsUserView.DataSource = DataRepository.Vw_MsProvince_UploadApprovalDetailProvider.GetAll
            GridMsUserView.AllowPaging = False
            GridMsUserView.DataBind()

            For i As Integer = 0 To GridMsUserView.Items.Count - 1
                For y As Integer = 0 To GridMsUserView.Columns.Count - 1
                    GridMsUserView.Items(i).Cells(y).Attributes.Add("class", "text")
                Next
            Next
            HideButtonGrid()
        Catch
            Throw
        End Try


    End Sub

    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                    'Skip
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub

    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click

        Try
            CollectSelected()
            Const strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            BindSelected()
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=MsProvinceUploadApproval.xls")
            Response.Charset = ""
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            EnableViewState = False
            Using stringWrite As System.IO.StringWriter = New System.IO.StringWriter()
                Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
                ClearControls(GridMsUserView)
                GridMsUserView.RenderControl(htmlWrite)
                Response.Write(strStyle)
                Response.Write(stringWrite.ToString())
            End Using
            Response.End()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            Me.CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) AndAlso (CInt(Me.TextGoToPage.Text) > 0) Then
                If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                    Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                Else
                    'Throw New Exception("SahassaCommonly.CommonlyEnum.EXECPTION_PAGENUMBER_GREATER_TOTALPAGENUMBER")
                    Throw New Exception("Page number must be less than or equal to the total page count.")
                End If
            Else
                'Throw New Exception("SahassaCommonly.CommonlyEnum.EXECPTION_PAGENUMBER_NUMERICPOSITIF")
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If
            CollectSelected()
        Catch ex As Exception
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub CvalHandleErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles CvalHandleErr.PreRender
        If CvalHandleErr.IsValid = False Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
        End If
    End Sub


    Protected Sub lnkExportAllData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExportAllData.Click
        Try
            Const strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            BindSelectedAll()
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=MsProvinceUploadApprovalAll.xls")
            Response.Charset = ""
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            EnableViewState = False
            Using stringWrite As System.IO.StringWriter = New System.IO.StringWriter()
                Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
                ClearControls(GridMsUserView)
                GridMsUserView.RenderControl(htmlWrite)
                Response.Write(strStyle)
                Response.Write(stringWrite.ToString())
            End Using
            Response.End()
        Catch ex As Exception
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub


#End Region

#Region "Essential..."

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgReject.Click
        Try
            Dim Key_ApprovalID As String = "0"
            For Each Idx As Vw_MsProvince_UploadApprovalDetail In SetnGetBindTable
                '----------------------------------------------------------------------------------------------------------------------------------------------------------------
                Dim OMsProvince_ApprovalDetail As MsProvince_ApprovalDetail
                Dim L_MsProvinceNCBS_ApprovalDetail As New TList(Of MsProvinceNCBS_approvalDetail)
                Dim L_MappingMsProvinceNCBSPPATK_Approval_Detail As New TList(Of MappingMsProvinceNCBSPPATK_Approval_Detail)
                'Get MsProvince Approval detail
                OMsProvince_ApprovalDetail = DataRepository.MsProvince_ApprovalDetailProvider.GetPaged( _
                    MsProvince_ApprovalDetailColumn.IdProvince.ToString & "=" & CStr(Idx.IdProvince), _
                    "", 0, Integer.MaxValue, Nothing)(0)
                'Get Key Approval
                Key_ApprovalID = OMsProvince_ApprovalDetail.Fk_MsProvince_Approval_Id().ToString()
                'Get Key MsProvince
                Dim MsProvinceID As String = OMsProvince_ApprovalDetail.IdProvince

                'Get List Mapping
                L_MappingMsProvinceNCBSPPATK_Approval_Detail = DataRepository.MappingMsProvinceNCBSPPATK_Approval_DetailProvider.GetPaged( _
                    MappingMsProvinceNCBSPPATK_Approval_DetailColumn.IDProvince.ToString & "=" & MsProvinceID & _
                    " and " & MappingMsProvinceNCBSPPATK_Approval_DetailColumn.PK_MsProvince_Approval_Id.ToString & "=" & Key_ApprovalID _
                    , "", 0, Integer.MaxValue, Nothing)

                For Each IdxA As MappingMsProvinceNCBSPPATK_Approval_Detail In L_MappingMsProvinceNCBSPPATK_Approval_Detail
                    Dim OMsProvinceNCBS_ApprovalDetail As MsProvinceNCBS_approvalDetail
                    Dim Ltemp As TList(Of MsProvinceNCBS_approvalDetail) = DataRepository.MsProvinceNCBS_approvalDetailProvider.GetPaged( _
                        MsProvinceNCBS_approvalDetailColumn.Pk_MsProvinceNCBS_Id.ToString & "=" & IdxA.IDProvinceNCBS.Value & " and " _
                        & MsProvinceNCBS_approvalDetailColumn.PK_MsProvince_Approval_Id.ToString & "=" & Key_ApprovalID _
                        , "", 0, Integer.MaxValue, Nothing)

                    If Ltemp.Count > 0 Then
                        OMsProvinceNCBS_ApprovalDetail = Ltemp(0)
                        L_MsProvinceNCBS_ApprovalDetail.Add(OMsProvinceNCBS_ApprovalDetail)
                    End If
                Next

                'Delete Approval Detail
                DataRepository.MsProvince_ApprovalDetailProvider.Delete(OMsProvince_ApprovalDetail)
                DataRepository.MsProvinceNCBS_approvalDetailProvider.Delete(L_MsProvinceNCBS_ApprovalDetail)
                DataRepository.MappingMsProvinceNCBSPPATK_Approval_DetailProvider.Delete(L_MappingMsProvinceNCBSPPATK_Approval_Detail)
            Next

            Using app As MsProvince_Approval = DataRepository.MsProvince_ApprovalProvider.GetByPk_MsProvince_Approval_Id(CInt(Key_ApprovalID))
                If Not IsNothing(app) Then
                    DataRepository.MsProvince_ApprovalProvider.Delete(app)
                End If
            End Using




            Using app As MsProvince_Approval = DataRepository.MsProvince_ApprovalProvider.GetByPk_MsProvince_Approval_Id(CInt(Key_ApprovalID))
                If Not IsNothing(app) Then
                    Using objMsUser As User = DataRepository.UserProvider.GetByPkUserID(app.Fk_MsUser_Id)
                        If Not IsNothing(objMsUser) Then
                            Session("MsProvince_ApprovalDetail_View.ApproverUser") = objMsUser.UserEmailAddress & ";"
                        End If
                    End Using
                End If
            End Using

            ShowButonApproveReject(False)
            Me.LblMessage.Text = "Rejected Success "
            Me.LblMessage.Visible = True
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub btnConfirmed_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgApprove.Click
        Try
            Dim Key_ApprovalID As String = ""
            For Each Idx As Vw_MsProvince_UploadApprovalDetail In SetnGetBindTable
                '================================ Deklaration ===================================================
                Dim OMsProvince As MsProvince
                Dim L_MsProvinceNCBS As New TList(Of MsProvinceNCBS)
                Dim L_MappingMsProvinceNCBSPPATK As New TList(Of MappingMsProvinceNCBSPPATK)
                '----------------------------------------------------------------------------------------------------------------------------------------------------------------
                Dim OMsProvince_ApprovalDetail As MsProvince_ApprovalDetail
                Dim L_MsProvinceNCBS_ApprovalDetail As New TList(Of MsProvinceNCBS_approvalDetail)
                Dim L_MappingMsProvinceNCBSPPATK_Approval_Detail As New TList(Of MappingMsProvinceNCBSPPATK_Approval_Detail)
                'Get MsProvince Approval detail
                OMsProvince_ApprovalDetail = DataRepository.MsProvince_ApprovalDetailProvider.GetPaged( _
                    MsProvince_ApprovalDetailColumn.IdProvince.ToString & "=" & CStr(Idx.IdProvince), _
                    "", 0, Integer.MaxValue, Nothing)(0)
                'Get Key Approval
                Key_ApprovalID = OMsProvince_ApprovalDetail.Fk_MsProvince_Approval_Id().ToString()
                'Get Key MsProvince
                Dim MsProvinceID As String = OMsProvince_ApprovalDetail.IdProvince

                'Get List Mapping
                L_MappingMsProvinceNCBSPPATK_Approval_Detail = DataRepository.MappingMsProvinceNCBSPPATK_Approval_DetailProvider.GetPaged( _
                    MappingMsProvinceNCBSPPATK_Approval_DetailColumn.IDProvince.ToString & "=" & MsProvinceID & _
                    " and " & MappingMsProvinceNCBSPPATK_Approval_DetailColumn.PK_MsProvince_Approval_Id.ToString & "=" & Key_ApprovalID _
                    , "", 0, Integer.MaxValue, Nothing)

                For Each IdxA As MappingMsProvinceNCBSPPATK_Approval_Detail In L_MappingMsProvinceNCBSPPATK_Approval_Detail
                    Dim OMsProvinceNCBS_ApprovalDetail As MsProvinceNCBS_approvalDetail
                    Dim Ltemp As TList(Of MsProvinceNCBS_approvalDetail) = DataRepository.MsProvinceNCBS_approvalDetailProvider.GetPaged( _
                        MsProvinceNCBS_approvalDetailColumn.Pk_MsProvinceNCBS_Id.ToString & "=" & IdxA.IDProvinceNCBS.Value & " and " _
                        & MsProvinceNCBS_approvalDetailColumn.PK_MsProvince_Approval_Id.ToString & "=" & Key_ApprovalID _
                        , "", 0, Integer.MaxValue, Nothing)

                    If Ltemp.Count > 0 Then
                        OMsProvinceNCBS_ApprovalDetail = Ltemp(0)
                        L_MsProvinceNCBS_ApprovalDetail.Add(OMsProvinceNCBS_ApprovalDetail)
                    End If
                Next

                'Convert approval ke object sebenarnya 
                OMsProvince = ConvertTo_MsProvince(OMsProvince_ApprovalDetail)
                L_MsProvinceNCBS = ConvertTo_MsProvinceNCBS(L_MsProvinceNCBS_ApprovalDetail)
                L_MappingMsProvinceNCBSPPATK = ConvertTo_MappingMsProvinceNCBSPPATKl(L_MappingMsProvinceNCBSPPATK_Approval_Detail)
                '======================= Save MsProvince =========================================================================
                DataRepository.MsProvinceProvider.Save(OMsProvince)
                DataRepository.MsProvinceNCBSProvider.Save(L_MsProvinceNCBS)
                Dim L_mappingDelete As TList(Of MappingMsProvinceNCBSPPATK) = DataRepository.MappingMsProvinceNCBSPPATKProvider.GetPaged( _
                        MappingMsProvinceNCBSPPATKColumn.Pk_MsProvince_Id.ToString & "= " & OMsProvince.IdProvince, "", 0, Integer.MaxValue, Nothing)
                DataRepository.MappingMsProvinceNCBSPPATKProvider.Delete(L_mappingDelete)
                DataRepository.MappingMsProvinceNCBSPPATKProvider.Save(L_MappingMsProvinceNCBSPPATK)
                'Delete Approval Detail
                DataRepository.MsProvince_ApprovalDetailProvider.Delete(OMsProvince_ApprovalDetail)
                DataRepository.MsProvinceNCBS_approvalDetailProvider.Delete(L_MsProvinceNCBS_ApprovalDetail)
                DataRepository.MappingMsProvinceNCBSPPATK_Approval_DetailProvider.Delete(L_MappingMsProvinceNCBSPPATK_Approval_Detail)
            Next

            Using app As MsProvince_Approval = DataRepository.MsProvince_ApprovalProvider.GetByPk_MsProvince_Approval_Id(CInt(Key_ApprovalID))
                If Not IsNothing(app) Then
                    DataRepository.MsProvince_ApprovalProvider.Delete(app)
                End If
            End Using

            Dim ket As String = ""
            If GetTotalInsert > 0 Then
                ket = "(Insert :" & GetTotalInsert & "; Update:" & GetTotalUpdate & "  )"
            End If
            Me.LblMessage.Text = "Confirmed Success " & ket
            Me.LblMessage.Visible = True
            ShowButonApproveReject(False)
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

#End Region
End Class


