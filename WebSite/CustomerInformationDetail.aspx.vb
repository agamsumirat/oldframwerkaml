Imports SahassaNettier.Entities
Imports SahassaNettier.Data
Imports AMLBLL
Imports AMLBLL.CDDLNPBLL
Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Collections.Generic
Imports System.Reflection

Partial Class CustomerInformationDetail
    Inherits Parent

    ''' <summary>
    ''' CIF No
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property CIFNo() As String
        Get
            Return Me.Request.Params.Get("CIFNo")
        End Get
    End Property


    Sub ClearSession()
        SearchFilterSafetyBoxCIF = Nothing
        Session("CustomerInformationDetail.SearchFilterCreditCard") = Nothing
        Session("CustomerInformationDetail.SearchFilterLoan") = Nothing
        Session("CustomerInformationDetail.SearchFilterAsset") = Nothing
        Session("CustomerInformationDetail.SearchFilterJIVE") = Nothing
        Session("CustomerInformationDetail.SearchFilterStakeholder") = Nothing
        Session("CustomerInformationDetail.SearchFilterBeneficial") = Nothing
        Session("CustomerInformationDetail.SearchFilterCardBillingAddress") = Nothing
    End Sub

    Public Property SearchFilterJIVE() As String
        Get
            Return IIf(Session("CustomerInformationDetail.SearchFilterJIVE") Is Nothing, "", Session("CustomerInformationDetail.SearchFilterJIVE"))
        End Get
        Set(ByVal value As String)
            Session("CustomerInformationDetail.SearchFilterJIVE") = value
        End Set
    End Property

    Public Property SearchFilterHIPOR() As String
        Get
            Return IIf(Session("CustomerInformationDetail.SearchFilterHIPOR") Is Nothing, "", Session("CustomerInformationDetail.SearchFilterHIPOR"))
        End Get
        Set(ByVal value As String)
            Session("CustomerInformationDetail.SearchFilterHIPOR") = value
        End Set
    End Property

    Public Property SearchFilterURSCustody() As String
        Get
            Return IIf(Session("CustomerInformationDetail.SearchFilterURSCustody") Is Nothing, "", Session("CustomerInformationDetail.SearchFilterURSCustody"))
        End Get
        Set(ByVal value As String)
            Session("CustomerInformationDetail.SearchFilterURSCustody") = value
        End Set
    End Property

    Public Property SearchFilterCreditCard() As String
        Get
            Return Session("CustomerInformationDetail.SearchFilterCreditCard")
        End Get
        Set(ByVal value As String)
            Session("CustomerInformationDetail.SearchFilterCreditCard") = value
        End Set
    End Property
    Public Property SearchFilterLoan() As String
        Get
            Return Session("CustomerInformationDetail.SearchFilterLoan")
        End Get
        Set(ByVal value As String)
            Session("CustomerInformationDetail.SearchFilterLoan") = value
        End Set
    End Property

    Public Property SearchFilterStakeholder() As String
        Get
            Return Session("CustomerInformationDetail.SearchFilterStakeholder")
        End Get
        Set(ByVal value As String)
            Session("CustomerInformationDetail.SearchFilterStakeholder") = value
        End Set
    End Property

    Public Property SearchFilterBeneficial() As String
        Get
            Return Session("CustomerInformationDetail.SearchFilterBeneficial")
        End Get
        Set(ByVal value As String)
            Session("CustomerInformationDetail.SearchFilterBeneficial") = value
        End Set
    End Property

    Public Property SearchFilterSafetyBoxCIF() As String
        Get
            Return Session("CustomerInformationDetail.SearchFilterSafetyBoxCIF")
        End Get
        Set(ByVal value As String)
            Session("CustomerInformationDetail.SearchFilterSafetyBoxCIF") = value
        End Set
    End Property

    Public Property SearchFilterLiabilities() As String
        Get
            Return Session("CustomerInformationDetail.SearchFilterAsset")
        End Get
        Set(ByVal value As String)
            Session("CustomerInformationDetail.SearchFilterAsset") = value
        End Set
    End Property

    Public Property SearchFilterCardBillingAddress() As String
        Get
            Return Session("CustomerInformationDetail.SearchFilterCardBillingAddress")
        End Get
        Set(ByVal value As String)
            Session("CustomerInformationDetail.SearchFilterCardBillingAddress") = value
        End Set
    End Property



    Protected Function GenerateAccountLink(ByVal strAccountNo As String) As String
        If Convert.IsDBNull(strAccountNo) Then
            Return "Account Data Not Valid"
        Else
            If strAccountNo = "0" Then
                Return String.Format("", "")
            Else
                Return String.Format("<a href=""{0}"">{1}</a> ", ResolveUrl("~\AccountInformationDetail.aspx?AccountNo=" & strAccountNo), strAccountNo)
            End If

        End If
    End Function


    Private Function IsValidDataAccess(ByVal StrCIFNo As String) As Boolean
        Dim StrTables As String
        Dim StrFilter As String
        If Not StrCIFNo Is Nothing Then
            StrTables = "CustomerInformation_WebTempTable INNER JOIN CFMAST ON CustomerInformation_WebTempTable.CIFNo=CFMAST.CFCIF#"
            StrFilter = "CustomerInformation_WebTempTable.CIFNo='" & StrCIFNo.Replace("'", "''") & "' AND (CFMAST.CFINSC IN (SELECT InsiderCodeId FROM GroupInsiderCodeMapping WHERE GroupId= (SELECT UserGroupId FROM [User] WHERE UserId = '" & Sahassa.AML.Commonly.SessionUserId & "')) OR CFMAST.CFINSC='' OR CFMAST.CFINSC IS NULL)"
            If Sahassa.AML.Commonly.GetTableCount(StrTables, StrFilter) > 0 Then
                Return True
            End If
        End If

        Return False
    End Function

    Sub LoadFilter()
        CboFilterLiabilities.Items.Add(New ListItem("...", ""))
        CboFilterLiabilities.Items.Add(New ListItem("CIF", "CIFNO like '%-=Search=-%'"))
        CboFilterLiabilities.Items.Add(New ListItem("AccountOwner", "AccountOwnerName like '%-=Search=-%'"))
        CboFilterLiabilities.Items.Add(New ListItem("AccountNo", "AccountNo = '-=Search=-'"))
        CboFilterLiabilities.Items.Add(New ListItem("Name", "AccountName like '%-=Search=-%'"))
        CboFilterLiabilities.Items.Add(New ListItem("CIFRelation", "CIFRelation like '%-=Search=-%'"))
        CboFilterLiabilities.Items.Add(New ListItem("AccountType", "AccountTypeDescription like '%-=Search=-%'"))
        CboFilterLiabilities.Items.Add(New ListItem("OpeningDate", "OpeningDate >='-=Search=- 00:00' AND OpeningDate <= '-=Search=- 23:59'"))
        CboFilterLiabilities.Items.Add(New ListItem("Status", "AccountStatusDescription like '%-=Search=-%'"))
        CboFilterLiabilities.Items.Add(New ListItem("Joint Account", "IsJointAccount like '%-=Search=-%'"))
        CboFilterLiabilities.Items.Add(New ListItem("JointAccountOwnership", "JointAccountOwnership like '%-=Search=-%'"))
        CboFilterLiabilities.Items.Add(New ListItem("CurrencyType", "CurrencyType like '%-=Search=-%'"))
        CboFilterLiabilities.Items.Add(New ListItem("Saldo", "Saldo like '%-=Search=-%'"))



        CboFilterLoan.Items.Add(New ListItem("...", ""))
        CboFilterLoan.Items.Add(New ListItem("CIF", "CIFNO like '%-=Search=-%'"))
        CboFilterLoan.Items.Add(New ListItem("AccountOwner", "AccountOwnerName like '%-=Search=-%'"))
        CboFilterLoan.Items.Add(New ListItem("AccountNo", "AccountNo = '-=Search=-'"))
        CboFilterLoan.Items.Add(New ListItem("Name", "AccountName like '%-=Search=-%'"))
        CboFilterLoan.Items.Add(New ListItem("CIFRelation", "CIFRelation like '%-=Search=-%'"))
        CboFilterLoan.Items.Add(New ListItem("AccountType", "AccountTypeDescription like '%-=Search=-%'"))
        CboFilterLoan.Items.Add(New ListItem("OpeningDate", "OpeningDate >='-=Search=- 00:00' AND OpeningDate <= '-=Search=- 23:59'"))
        CboFilterLoan.Items.Add(New ListItem("Closing_Date", "Closing_Date >='-=Search=- 00:00' AND Closing_Date <= '-=Search=- 23:59'"))
        CboFilterLoan.Items.Add(New ListItem("Maturity_Date", "Maturity_Date >='-=Search=- 00:00' AND Maturity_Date <= '-=Search=- 23:59'"))
        CboFilterLoan.Items.Add(New ListItem("Status", "AccountStatusDescription like '%-=Search=-%'"))
        CboFilterLoan.Items.Add(New ListItem("Joint Account", "IsJointAccount like '%-=Search=-%'"))
        CboFilterLoan.Items.Add(New ListItem("JointAccountOwnership", "JointAccountOwnership like '%-=Search=-%'"))
        CboFilterLoan.Items.Add(New ListItem("CurrencyType", "CurrencyType like '%-=Search=-%'"))
        CboFilterLoan.Items.Add(New ListItem("OutStanding", "Saldo like '%-=Search=-%'"))
        CboFilterLoan.Items.Add(New ListItem("Plafon", "Plafon like '%-=Search=-%'"))




        CboFilterCreditCard.Items.Add(New ListItem("...", ""))
        CboFilterCreditCard.Items.Add(New ListItem("CIF", "CIFNO like '%-=Search=-%'"))
        CboFilterCreditCard.Items.Add(New ListItem("AccountOwner", "AccountOwnerName like '%-=Search=-%'"))
        CboFilterCreditCard.Items.Add(New ListItem("AccountNo", "AccountNo = '-=Search=-'"))
        CboFilterCreditCard.Items.Add(New ListItem("Name", "AccountName like '%-=Search=-%'"))
        CboFilterCreditCard.Items.Add(New ListItem("CIFRelation", "CIFRelation like '%-=Search=-%'"))
        CboFilterCreditCard.Items.Add(New ListItem("AccountType", "AccountTypeDescription like '%-=Search=-%'"))
        CboFilterCreditCard.Items.Add(New ListItem("OpeningDate", "OpeningDate >='-=Search=- 00:00' AND OpeningDate <= '-=Search=- 23:59'"))
        CboFilterCreditCard.Items.Add(New ListItem("Status", "AccountStatusDescription like '%-=Search=-%'"))
        CboFilterCreditCard.Items.Add(New ListItem("Joint Account", "IsJointAccount like '%-=Search=-%'"))
        CboFilterCreditCard.Items.Add(New ListItem("JointAccountOwnership", "JointAccountOwnership like '%-=Search=-%'"))
        CboFilterCreditCard.Items.Add(New ListItem("CurrencyType", "CurrencyType like '%-=Search=-%'"))
        CboFilterCreditCard.Items.Add(New ListItem("Product", "Product like '%-=Search=-%'"))
        CboFilterCreditCard.Items.Add(New ListItem("OutStanding", "OutStanding like '%-=Search=-%'"))

        CboFilterSDB.Items.Add(New ListItem("CIFNo", "CIFNo  like '%-=Search=-%'"))
        CboFilterSDB.Items.Add(New ListItem("AccountOwner", "AccountOwnerName  like '%-=Search=-%'"))
        CboFilterSDB.Items.Add(New ListItem("AccountNo", "Acctno ='-=Search=-'"))
        CboFilterSDB.Items.Add(New ListItem("Name", "Acctno ='-=Search=-'"))
        CboFilterSDB.Items.Add(New ListItem("Box No", "Boxno like '%-=Search=-%'"))
        CboFilterSDB.Items.Add(New ListItem("Date Open", "tglopen >='-=Search=- 00:00' AND tglopen <= '-=Search=- 23:59'"))
        CboFilterSDB.Items.Add(New ListItem("Date Expired", "TglExpired >='-=Search=- 00:00' AND TglExpired <= '-=Search=- 23:59'"))
        CboFilterSDB.Items.Add(New ListItem("Date Closed", "TglClosed >='-=Search=- 00:00' AND TglClosed <= '-=Search=- 23:59'"))


        CbFilBa.Items.Add(New ListItem("Cif_No", "Cif_No  like '%-=Search=-%'"))
        CbFilBa.Items.Add(New ListItem("Account_Owner", "Account_Owner  like '%-=Search=-%'"))
        CbFilBa.Items.Add(New ListItem("Account_No", "Account_No  like '%-=Search=-%'"))
        CbFilBa.Items.Add(New ListItem("Name", "Name  like '%-=Search=-%'"))
        CbFilBa.Items.Add(New ListItem("Cif_Relation", "Cif_Relation  like '%-=Search=-%'"))
        CbFilBa.Items.Add(New ListItem("Account_Type", "Account_Type  like '%-=Search=-%'"))
        CbFilBa.Items.Add(New ListItem("Opening_Date", "Opening_Date  like '%-=Search=-%'"))
        CbFilBa.Items.Add(New ListItem("Status", "Status  like '%-=Search=-%'"))
        CbFilBa.Items.Add(New ListItem("Joint_Account", "Joint_Account  like '%-=Search=-%'"))
        CbFilBa.Items.Add(New ListItem("Account_OwnerShip_Type", "Account_OwnerShip_Type  like '%-=Search=-%'"))
        CbFilBa.Items.Add(New ListItem("Product_Description", "Product_Description  like '%-=Search=-%'"))


        CboFilterJIVE.Items.Add(New ListItem("...", ""))
        CboFilterJIVE.Items.Add(New ListItem("CIF", "CIFNO like '%-=Search=-%'"))
        CboFilterJIVE.Items.Add(New ListItem("AccountOwner", "AccountOwnerName like '%-=Search=-%'"))
        CboFilterJIVE.Items.Add(New ListItem("AccountNo", "AccountNo = '-=Search=-'"))
        CboFilterJIVE.Items.Add(New ListItem("Name", "AccountName like '%-=Search=-%'"))
        CboFilterJIVE.Items.Add(New ListItem("CIFRelation", "CIFRelation like '%-=Search=-%'"))
        CboFilterJIVE.Items.Add(New ListItem("AccountType", "AccountTypeDescription like '%-=Search=-%'"))
        CboFilterJIVE.Items.Add(New ListItem("OpeningDate", "OpeningDate >='-=Search=- 00:00' AND OpeningDate <= '-=Search=- 23:59'"))
        CboFilterJIVE.Items.Add(New ListItem("Status", "AccountStatusDescription like '%-=Search=-%'"))
        CboFilterJIVE.Items.Add(New ListItem("Joint Account", "IsJointAccount like '%-=Search=-%'"))
        CboFilterJIVE.Items.Add(New ListItem("JointAccountOwnership", "JointAccountOwnership like '%-=Search=-%'"))
        CboFilterJIVE.Items.Add(New ListItem("CurrencyType", "CurrencyType like '%-=Search=-%'"))
        CboFilterJIVE.Items.Add(New ListItem("Saldo", "Saldo like '%-=Search=-%'"))

        CboFilterHipor.Items.Add(New ListItem("...", ""))
        CboFilterHipor.Items.Add(New ListItem("NAV TXN DATE", "NAV_TXN_DATE >='-=Search=- 00:00' AND NAV_TXN_DATE <= '-=Search=- 23:59'"))
        CboFilterHipor.Items.Add(New ListItem("FUND_ID", "FUND_ID like '%-=Search=-%'"))
        CboFilterHipor.Items.Add(New ListItem("Description", "Description like '%-=Search=-%'"))
        CboFilterHipor.Items.Add(New ListItem("CIF Account", "CIF like '%-=Search=-%'"))
        CboFilterHipor.Items.Add(New ListItem("NAV", "NAV like '%-=Search=-%'"))
        CboFilterHipor.Items.Add(New ListItem("AccountNo", "AccountNo like '%-=Search=-%'"))

        CboFilterURSCustody.Items.Add(New ListItem("...", ""))
        CboFilterURSCustody.Items.Add(New ListItem("Tanggal Holding", "TanggalHolding = '-=Search=-'"))
        CboFilterURSCustody.Items.Add(New ListItem("Nama Investor", "NamaInvestor like '%-=Search=-%'"))
        CboFilterURSCustody.Items.Add(New ListItem("CIF", "CIF like '%-=Search=-%'"))
        CboFilterURSCustody.Items.Add(New ListItem("Fund ID", "Fund_ID like '%-=Search=-%'"))
        CboFilterURSCustody.Items.Add(New ListItem("Unit", "Unit = -=Search=-"))
        CboFilterURSCustody.Items.Add(New ListItem("NAV Per Unit", "NAVPerUnit = -=Search=-"))
        CboFilterURSCustody.Items.Add(New ListItem("Amount", "Amount = -=Search=-"))

        CbFilStakeholder.Items.Add(New ListItem("...", ""))
        CbFilStakeholder.Items.Add(New ListItem("Bussiness Date", "BUSINESS_DATE = '-=Search=-'"))
        CbFilStakeholder.Items.Add(New ListItem("Customer No", "CUST_NO like '%-=Search=-%'"))
        CbFilStakeholder.Items.Add(New ListItem("Stakeholder Seq No", "STAKEHOLDER_SEQ_NO = '-=Search=-'"))
        CbFilStakeholder.Items.Add(New ListItem("Stakeholder Customer No", "STAKEHOLDER_CUST_NO like '%-=Search=-%'"))
        CbFilStakeholder.Items.Add(New ListItem("Stakeholder Name", "STAKEHOLDER_NAME like '%-=Search=-%'"))
        CbFilStakeholder.Items.Add(New ListItem("ID Number", "ID_NUMBER like '%-=Search=-%'"))
        CbFilStakeholder.Items.Add(New ListItem("ID Type", "ID_TYPE like '%-=Search=-%'"))
        CbFilStakeholder.Items.Add(New ListItem("Alternate ID Number", "ALTERNATE_ID_NUMBER like '%-=Search=-%'"))
        CbFilStakeholder.Items.Add(New ListItem("Alternate ID Type", "ALTERNATE_ID_TYPE like '%-=Search=-%'"))
        CbFilStakeholder.Items.Add(New ListItem("Last Maintenance Date", "LAST_MAINTENANCE_DATE = '-=Search=-'"))
        CbFilStakeholder.Items.Add(New ListItem("DOB", "DOB = '-=Search=-'"))
        CbFilStakeholder.Items.Add(New ListItem("Designation", "DESIGNATION like '%-=Search=-%'"))
        CbFilStakeholder.Items.Add(New ListItem("Ownership", "OWNERSHIP = '-=Search=-'"))
        CbFilStakeholder.Items.Add(New ListItem("Gender", "GENDER like '%-=Search=-%'"))
        CbFilStakeholder.Items.Add(New ListItem("Nationality", "NATIONALITY like '%-=Search=-%'"))

        CbFilBeneficial.Items.Add(New ListItem("...", ""))
        CbFilBeneficial.Items.Add(New ListItem("Bussiness Date", "BUSINESS_DATE = '-=Search=-'"))
        CbFilBeneficial.Items.Add(New ListItem("Customer No", "CUST_NO like '%-=Search=-%'"))
        CbFilBeneficial.Items.Add(New ListItem("Beneficial Customer No", "BENEFICIAL_CUST_NO like '%-=Search=-%'"))
        CbFilBeneficial.Items.Add(New ListItem("Beneficiary Name", "BENEFICIARY_NAME like '%-=Search=-%'"))
        CbFilBeneficial.Items.Add(New ListItem("Business Type", "BUSINESS_TYPE like '%-=Search=-%'"))
        CbFilBeneficial.Items.Add(New ListItem("Job Title", "JOB_TYTLE like '%-=Search=-%'"))
        CbFilBeneficial.Items.Add(New ListItem("Address 1", "COMPANY_ADDRESS1 like '%-=Search=-%'"))
        CbFilBeneficial.Items.Add(New ListItem("Address 2", "COMPANY_ADDRESS2 like '%-=Search=-%'"))
        CbFilBeneficial.Items.Add(New ListItem("Address 3", "COMPANY_ADDRESS3 like '%-=Search=-%'"))
        CbFilBeneficial.Items.Add(New ListItem("Address 4", "COMPANY_ADDRESS4 like '%-=Search=-%'"))
        CbFilBeneficial.Items.Add(New ListItem("Address 5", "COMPANY_ADDRESS5 like '%-=Search=-%'"))
        CbFilBeneficial.Items.Add(New ListItem("Address 6", "COMPANY_ADDRESS6 like '%-=Search=-%'"))
        CbFilBeneficial.Items.Add(New ListItem("Company Name", "COMPANY_NAME like '%-=Search=-%'"))
        CbFilBeneficial.Items.Add(New ListItem("DOB", "DOB = '-=Search=-'"))
        CbFilBeneficial.Items.Add(New ListItem("Nationality", "NATIONALITY like '%-=Search=-%'"))

        'CbFilCardBillingAddress.Items.Add(New ListItem("...", ""))
        'CbFilCardBillingAddress.Items.Add(New ListItem("Name", "NAMA like '%-=Search=-%'"))
        'CbFilCardBillingAddress.Items.Add(New ListItem("ID Type", "JENIS_ID like '%-=Search=-%'"))
        'CbFilCardBillingAddress.Items.Add(New ListItem("ID Number", "NO_ID like '%-=Search=-%'"))
        'CbFilCardBillingAddress.Items.Add(New ListItem("ID Based Address Line 1", "ALAMAT_1_SESUAI_ID like '%-=Search=-%'"))
        'CbFilCardBillingAddress.Items.Add(New ListItem("ID Based Address Line 2", "ALAMAT_2_SESUAI_ID like '%-=Search=-%'"))
        'CbFilCardBillingAddress.Items.Add(New ListItem("ID Based Address Line 3", "ALAMAT_3_SESUAI_ID like '%-=Search=-%'"))
        'CbFilCardBillingAddress.Items.Add(New ListItem("ID Based Address Line 4", "ALAMAT_4_SESUAI_ID like '%-=Search=-%'"))
        'CbFilCardBillingAddress.Items.Add(New ListItem("ID Based Postal Code", "KODE_POS_SESUAI_ID like '%-=Search=-%'"))
        'CbFilCardBillingAddress.Items.Add(New ListItem("ID Based City", "KOTA_SESUAI_ID like '%-=Search=-%'"))
        'CbFilCardBillingAddress.Items.Add(New ListItem("ID Based State", "KODE_PROVINSI_SESUAI_ID like '%-=Search=-%'"))
        'CbFilCardBillingAddress.Items.Add(New ListItem("ID Based Country", "KODE_NEGARA_SESUAI_ID like '%-=Search=-%'"))
        'CbFilCardBillingAddress.Items.Add(New ListItem("Current Address Line 1", "ALAMAT_1_TERKINI like '%-=Search=-%'"))
        'CbFilCardBillingAddress.Items.Add(New ListItem("Current Address Line 2", "ALAMAT_2_TERKINI like '%-=Search=-%'"))
        'CbFilCardBillingAddress.Items.Add(New ListItem("Current Address Line 3", "ALAMAT_3_TERKINI like '%-=Search=-%'"))
        'CbFilCardBillingAddress.Items.Add(New ListItem("Current Address Line 4", "ALAMAT_4_TERKINI like '%-=Search=-%'"))
        'CbFilCardBillingAddress.Items.Add(New ListItem("Current Posal Code", "KODE_POS_TERKINI like '%-=Search=-%'"))
        'CbFilCardBillingAddress.Items.Add(New ListItem("Current City", "KOTA_TERKINI like '%-=Search=-%'"))
        'CbFilCardBillingAddress.Items.Add(New ListItem("Current State", "KODE_PROVINSI_TERKINI like '%-=Search=-%'"))
        'CbFilCardBillingAddress.Items.Add(New ListItem("Current Country", "KODE_NEGARA_TERKINI like '%-=Search=-%'"))
        'CbFilCardBillingAddress.Items.Add(New ListItem("Birth Place", "TEMPAT_LAHIR like '%-=Search=-%'"))
        'CbFilCardBillingAddress.Items.Add(New ListItem("DOB", "TANGGAL_LAHIR = '-=Search=-'"))
        'CbFilCardBillingAddress.Items.Add(New ListItem("Occupation", "PEKERJAAN like '%-=Search=-%'"))
        'CbFilCardBillingAddress.Items.Add(New ListItem("Workplace", "TEMPAT_BEKERJA like '%-=Search=-%'"))
        'CbFilCardBillingAddress.Items.Add(New ListItem("Business Field", "BIDANG_USAHA like '%-=Search=-%'"))
        'CbFilCardBillingAddress.Items.Add(New ListItem("Position", "JABATAN like '%-=Search=-%'"))
        'CbFilCardBillingAddress.Items.Add(New ListItem("Workplace Address Line 1", "ALAMAT_1_TEMPAT_BEKERJA like '%-=Search=-%'"))
        'CbFilCardBillingAddress.Items.Add(New ListItem("Workplace Address Line 2", "ALAMAT_2_TEMPAT_BEKERJA like '%-=Search=-%'"))
        'CbFilCardBillingAddress.Items.Add(New ListItem("Workplace Address Line 3", "ALAMAT_3_TEMPAT_BEKERJA like '%-=Search=-%'"))
        'CbFilCardBillingAddress.Items.Add(New ListItem("Workplace Address Line 4", "ALAMAT_4_TEMPAT_BEKERJA like '%-=Search=-%'"))
        'CbFilCardBillingAddress.Items.Add(New ListItem("Workplace Postal Code", "KODE_POS_TEMPAT_BEKERJA like '%-=Search=-%'"))
        'CbFilCardBillingAddress.Items.Add(New ListItem("Workplace City", "KOTA_TEMPAT_BEKERJA like '%-=Search=-%'"))
        'CbFilCardBillingAddress.Items.Add(New ListItem("Workplace State", "KODE_PROVINSI_TEMPAT_BEKERJA like '%-=Search=-%'"))
        'CbFilCardBillingAddress.Items.Add(New ListItem("Workplace Country", "KODE_NEGARA_TEMPAT_BEKERJA like '%-=Search=-%'"))
        'CbFilCardBillingAddress.Items.Add(New ListItem("Workplace Phone Number", "NOMOR_TELEPON_TEMPAT_BEKERJA like '%-=Search=-%'"))
        'CbFilCardBillingAddress.Items.Add(New ListItem("Employee Extension Phone", "EKSTENSI_TELEPON_KARYAWAN like '%-=Search=-%'"))
        'CbFilCardBillingAddress.Items.Add(New ListItem("Average Incoming", "PENDAPATAN_RATA_RATA like '%-=Search=-%'"))
        'CbFilCardBillingAddress.Items.Add(New ListItem("Sex", "JENIS_KELAMIN like '%-=Search=-%'"))
        'CbFilCardBillingAddress.Items.Add(New ListItem("Marriage Status", "STATUS_PERKAWINAN like '%-=Search=-%'"))

    End Sub


    ''' <summary>
    ''' Page Load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsValidDataAccess(Me.CIFNo) Then
                If Not Page.IsPostBack Then

                    Dim isAllowedViewCC As Boolean = False

                    Session("LinkAnalysisGraphCommand") = Nothing
                    ClearSession()
                    Me.LoadData()
                    Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                        Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                        AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                    End Using
                    LoadFilter()

                    isAllowedViewCC = EKABLL.CustomerInformationDetailBLL.IsAllowedAccessCCTab(Sahassa.AML.Commonly.SessionGroupName)

                    If Not isAllowedViewCC Then
                        For index As Integer = 13 To 17
                            Menu1.Items.Remove(Menu1.FindItem(index.ToString()))
                        Next
                    End If

                    Me.CboFilterCreditCard.Attributes.Add("onchange", "javascript:Check2Calendar(this,'" & Me.txtfiltercreditcard.ClientID & "', '" & Me.popupcreditcard.ClientID & "',7);")
                    Me.CboFilterLiabilities.Attributes.Add("onchange", "javascript:Check2Calendar(this,'" & Me.txtFilterliabilities.ClientID & "', '" & Me.popupliabilities.ClientID & "',7);")

                    Me.CboFilterLoan.Attributes.Add("onchange", "javascript:Check3Calendar(this,'" & Me.txtfilterloan.ClientID & "', '" & Me.popUploan.ClientID & "',7,8,9);")

                    Me.CboFilterJIVE.Attributes.Add("onchange", "javascript:Check2Calendar(this,'" & Me.txtFilterJIVE.ClientID & "', '" & Me.popupJIVE.ClientID & "',7);")
                    Me.CboFilterHipor.Attributes.Add("onchange", "javascript:Check2Calendar(this,'" & Me.txtFilterHipor.ClientID & "', '" & Me.popUpHipor.ClientID & "',1);")
                    Me.CboFilterURSCustody.Attributes.Add("onchange", "javascript:Check2Calendar(this,'" & Me.txtFilterURSCustody.ClientID & "', '" & Me.popupUSRCustody.ClientID & "',1);")
                    Me.CboFilterSDB.Attributes.Add("onchange", "javascript:Check3Calendar(this,'" & Me.txtSDB.ClientID & "', '" & Me.popupsdb.ClientID & "',5,6,7);")
                    Me.CbFilBa.Attributes.Add("onchange", "javascript:Check3Calendar(this,'" & Me.txtBa.ClientID & "', '" & Me.popupsdb.ClientID & "',5,6,7);")

                    Me.CbFilStakeholder.Attributes.Add("onchange", "javascript:Check3Calendar(this,'" & Me.TxtFilStakeholder.ClientID & "', '" & Me.popStakeholder.ClientID & "',1,10,11);")
                    Me.CbFilBeneficial.Attributes.Add("onchange", "javascript:Check3Calendar(this,'" & Me.TxtFilBeneficial.ClientID & "', '" & Me.popBeneficial.ClientID & "',1,14);")

                    'Me.CbFilCardBillingAddress.Attributes.Add("onchange", "javascript:Check3Calendar(this,'" & Me.TxtFilCardBillingAddress.ClientID & "', '" & Me.popCardBillingAddress.ClientID & "',1,10,11);")

                    Me.popupsdb.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtBa.ClientID & "'), 'dd-mmm-yyyy')")
                    Me.popupcreditcard.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtfiltercreditcard.ClientID & "'), 'dd-mmm-yyyy')")
                    Me.popupliabilities.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtFilterliabilities.ClientID & "'), 'dd-mmm-yyyy')")
                    Me.popUploan.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtfilterloan.ClientID & "'), 'dd-mmm-yyyy')")

                    Me.popupsdb.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtSDB.ClientID & "'), 'dd-mmm-yyyy')")

                    Me.popupJIVE.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtFilterJIVE.ClientID & "'), 'dd-mmm-yyyy')")
                    Me.popUpHipor.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtFilterHipor.ClientID & "'), 'dd-mmm-yyyy')")
                    Me.popupUSRCustody.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtFilterURSCustody.ClientID & "'), 'dd-mmm-yyyy')")
                    Me.popupsdb.Style.Add("display", "none")

                    Me.popStakeholder.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TxtFilStakeholder.ClientID & "'), 'dd-mmm-yyyy')")
                    Me.popBeneficial.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TxtFilBeneficial.ClientID & "'), 'dd-mmm-yyyy')")

                    'Me.popCardBillingAddress.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TxtFilCardBillingAddress.ClientID & "'), 'dd-mmm-yyyy')")

                    Me.popupcreditcard.Style.Add("display", "none")
                    Me.popupliabilities.Style.Add("display", "none")
                    Me.popUploan.Style.Add("display", "none")
                    Me.popupJIVE.Style.Add("display", "none")
                    Me.popUpHipor.Style.Add("display", "none")
                    Me.popupUSRCustody.Style.Add("display", "none")
                    Me.popStakeholder.Style.Add("display", "none")
                    Me.popBeneficial.Style.Add("display", "none")
                    'Me.popCardBillingAddress.Style.Add("display", "none")

                    'tambahan untuk filter link analysis
                    If Not IsNothing(Session("LinkAnalisys")) Then
                        If Session("LinkAnalisys") Then
                            MultiView1.ActiveViewIndex = 9
                            Menu1.TabIndex = 9
                            Menu1.Items.Item(8).Selected = True
                        End If
                        Session("LinkAnalisys") = Nothing

                        txtFromAmount.Text = Session("LinkAnalysisFromAmount")
                        txtToAmount.Text = Session("LinkAnalysisToAmount")
                    Else
                        Session("LinkAnalysisFromAmount") = Nothing
                        Session("LinkAnalysisToAmount") = Nothing
                    End If

                End If

                'for CDD
                Using objFileSecurity As TList(Of FileSecurity) = DataRepository.FileSecurityProvider.GetPaged("FileSecurityFileName LIKE '%CDDLNP%' and FileSecurityGroupId in (SELECT g.GroupID FROM [Group] g where g.GroupName = '" & Sahassa.AML.Commonly.SessionGroupName & "')", "", 0, Integer.MaxValue, 0)
                    If objFileSecurity.Count > 0 Then
                        Menu1.Items.Item(9).Enabled = True
                    Else
                        Menu1.Items.Item(9).Enabled = False
                    End If
                End Using


            Else
                Me.Menu1.Visible = False
                Me.MultiView1.Visible = False
                Throw New Exception("Customer not found or you do not have access to this customer data.")
            End If

            'If Not Me.IsPostBack Then
            '    Me.LoadData()

            '    Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

            '    Using Transcope As New Transactions.TransactionScope
            '        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
            '            Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
            '            AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

            '            Transcope.Complete()
            '        End Using
            '    End Using
            'End If

        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Sub BindGridIncome(ByVal strcif As String)
        GrdIncome.DataSource = AMLBLL.AccountInformationBLL.GetCIFIncomeByCIFNo(strcif)
        GrdIncome.DataBind()
    End Sub
    ''' <summary>
    ''' Load Data
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub LoadData()
        Try
            If Me.CIFNo <> "" Then
                ' Display detail Customer
                LblAMLRisk.Text = AMLBLL.AccountInformationBLL.GetAMLRiskByCIFNo(Me.CIFNo)
                If LblAMLRisk.Text.ToLower.Contains("high") Then
                    LblAMLRisk.ForeColor = Drawing.Color.Red
                    LblAMLRisk.Font.Bold = True
                ElseIf LblAMLRisk.Text.ToLower.Contains("medium") Then
                    LblAMLRisk.ForeColor = Drawing.Color.Yellow
                    LblAMLRisk.Font.Bold = True
                End If

                BindGridIncome(Me.CIFNo)
                LbllastModifiedDate.Text = AMLBLL.AccountInformationBLL.getLastModifiedCIF(Me.CIFNo)
                Using AccessDetailCustomer As New AMLDAL.CustomerInformationTableAdapters.SelectCDD_CIF_MasterFilterTableAdapter

                    Using dtDetailCustomer As Data.DataTable = AccessDetailCustomer.GetData(Me.CIFNo)
                        If dtDetailCustomer.Rows.Count > 0 Then
                            Dim RowDetailCustomer As AMLDAL.CustomerInformation.SelectCDD_CIF_MasterFilterRow = dtDetailCustomer.Rows(0)
                            If Not RowDetailCustomer Is Nothing Then
                                Me.LblCIFNo.Text = RowDetailCustomer.CIFNo
                                If Not RowDetailCustomer.IsCustomerNameNull Then
                                    Me.LblName.Text = RowDetailCustomer.CustomerName
                                End If
                                If Not RowDetailCustomer.IsDateOfBirthNull Then
                                    Me.LblDOB.Text = RowDetailCustomer.DateOfBirth.ToString("dd-MMM-yyyy")
                                End If
                                If Not RowDetailCustomer.IsAccountOwnerNull Then
                                    Me.LblAccountOwner.Text = RowDetailCustomer.AccountOwner
                                End If
                                If Not RowDetailCustomer.IsOpeningDateNull Then
                                    Me.LblOpeningDate.Text = RowDetailCustomer.OpeningDate.ToString("dd-MMM-yyyy")
                                End If
                                If Not RowDetailCustomer.IsCustomerTypeNull Then
                                    Me.LblCustomerType.Text = RowDetailCustomer.CustomerType
                                End If
                                If Not RowDetailCustomer.IsCustomerSubClassNull Then
                                    Me.LblCustomerSubType.Text = RowDetailCustomer.CustomerSubClass
                                End If
                                If Not RowDetailCustomer.IsCountryNull Then
                                    Me.LblCountry.Text = RowDetailCustomer.Country
                                End If
                                If Not RowDetailCustomer.IsCitizenshipNull Then
                                    Me.LblCitizen.Text = RowDetailCustomer.Citizenship
                                End If
                                If Not RowDetailCustomer.IsBusinessTypeNull Then
                                    Me.LblBusinessType.Text = RowDetailCustomer.BusinessType
                                End If
                                If Not RowDetailCustomer.IsIndustryCodeNull Then
                                    Me.LblIndustryCode.Text = RowDetailCustomer.IndustryCode
                                End If


                                'General
                                Me.TdGeneralNotAvailable.Visible = False
                                If Not RowDetailCustomer.IsBirthPlaceNull Then
                                    Me.LblBirthPlace.Text = RowDetailCustomer.BirthPlace
                                End If
                                If Not RowDetailCustomer.IsMaidenMotherNameNull Then
                                    Me.LblMaidenMotherName.Text = RowDetailCustomer.MaidenMotherName
                                End If
                                If Not RowDetailCustomer.IsGenderNull Then
                                    Me.LblGender.Text = RowDetailCustomer.Gender
                                End If

                                If Not RowDetailCustomer.IsMaritalStatusNull Then
                                    Me.LblMartialStatus.Text = RowDetailCustomer.MaritalStatus
                                End If

                                Me.LoadSBU(RowDetailCustomer.CIFNo)


                                Me.LblSourceOfFund.Text = EKABLL.CDDBLL.GetSourceOffund(RowDetailCustomer.CIFNo)


                                'If Not RowDetailCustomer.IsIncomeNull Then
                                '    Me.LblIncome.Text = RowDetailCustomer.Income
                                'End If

                                ''source of Fund
                                'Using dtSourceOfFund As New AMLDAL.CustomerInformationTableAdapters.SelectSourceOfFundTableAdapter
                                '    Using dataSourceOfFund As AMLDAL.CustomerInformation.SelectSourceOfFundDataTable = dtSourceOfFund.GetData(LblCIFNo.Text)
                                '        If dataSourceOfFund.Rows.Count > 0 Then
                                '            LblSourceOfFundDataNotAvailable.Visible = False
                                '            DgSourceOfFund.Visible = True
                                '            DgSourceOfFund.DataSource = dataSourceOfFund
                                '            DgSourceOfFund.DataBind()
                                '        Else
                                '            DgSourceOfFund.Visible = False
                                '            LblSourceOfFundDataNotAvailable.Visible = True
                                '        End If
                                '    End Using
                                'End Using

                            End If
                        End If
                    End Using
                End Using

            End If
        Catch
            Throw
        End Try
    End Sub

    Private Function LoadSBU(ByVal StrCIFNo As String) As Boolean
        Dim reader As Data.SqlClient.SqlDataReader
        'Dim i As Integer
        Dim str As String
        str = " SELECT CFMAST.CFOFID, CFMAST.SSONAM, CFMAST.CFSBUC, CFMAST.CFCCRD, CFMAST.CFSBUT, CFMAST.CFSBUTName,  " _
                   & " CFMAST.CFBNKN, CASE CFMAST.CFCSTS WHEN  '1'  THEN 'Active' " _
                   & "    WHEN  '2'  THEN 'Inactive' " _
                   & "    WHEN	'4'  THEN 'Deleted' " _
                   & "    WHEN	'9'  THEN 'Merged ' END CIFStatus," _
                   & " CFMAST.CFBRNN + ' - ' + ao1.AccountOwnerName CreationBranch, " _
                   & " CFMAST.CFOFFR + ' - ' + ao2.AccountOwnerName ControllingBranch " _
                   & "   FROM CFMAST " _
                   & "   INNER JOIN AccountOwner ao1 ON CAST(CFMAST.CFBRNN AS VARCHAR) = ao1.AccountOwnerId " _
                   & "   INNER JOIN AccountOwner ao2 ON CAST(CFMAST.CFOFFR AS VARCHAR) = ao2.AccountOwnerId " _
                   & " where CFMAST.CFCIF# = @CIFNO "
        Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommand(str)
            objCommand.Parameters.Add(New SqlParameter("@CIFNO", StrCIFNo))
            reader = DataRepository.Provider.ExecuteReader(objCommand)
            If reader.HasRows Then
                If reader.Read Then
                    If reader.IsDBNull(0) = False Then
                        Me.lblRM.Text = reader.GetString(0)
                    End If
                    If reader.IsDBNull(1) = False Then
                        Me.lblRM.Text = Me.lblRM.Text + " - " + reader.GetString(1)
                    End If

                    If reader.IsDBNull(2) = False Then
                        Me.lblSbu.Text = reader.GetString(2)
                    End If

                    If reader.IsDBNull(3) = False Then
                        Me.lblSbu.Text = Me.lblSbu.Text + " - " + reader.GetString(3)
                    End If

                    If reader.IsDBNull(4) = False Then
                        Me.lblSubSBU.Text = reader.GetString(4)
                    End If

                    If reader.IsDBNull(5) = False Then
                        Me.lblSubSBU.Text = Me.lblSubSBU.Text + " - " + reader.GetString(5)
                    End If

                    If reader.IsDBNull(6) = False Then
                        Me.lblCIFCreationBank.Text = reader.GetString(6)
                    End If

                    If reader.IsDBNull(7) = False Then
                        Me.lblCifStatus.Text = reader.GetString(7)
                    End If



                    If reader.IsDBNull(8) = False Then
                        Me.lblCIFCreationBranch.Text = reader.GetString(8)
                    End If

                    If reader.IsDBNull(9) = False Then
                        Me.lblCIFControllingBranch.Text = reader.GetString(9)
                    End If
                End If
            End If
        End Using
        Return True
    End Function


    ''' <summary>
    ''' Address
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ShowAddress()
        ' Display Address 
        Try
            Using AccessAddress As New AMLDAL.CustomerInformationTableAdapters.SelectCDD_CIF_AddressByCIFTableAdapter
                Using dtAddress As Data.DataTable = AccessAddress.GetData(Me.CIFNo)
                    If dtAddress.Rows.Count > 0 Then
                        Me.GridAddress.DataSource = dtAddress
                        Me.GridAddress.DataBind()
                        Me.AddressNotFound.Visible = False
                        Me.AddresFound.Visible = True
                    Else
                        Me.AddressNotFound.Visible = True
                        Me.AddresFound.Visible = False
                    End If
                End Using
            End Using
        Catch
            Throw
        End Try
    End Sub

    ''' <summary>
    ''' Id Number
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ShowIdNumber()
        Try

            ' display ID Number
            Using AccessIDNumber As New AMLDAL.CustomerInformationTableAdapters.SelectCDD_CIF_IdNumberTableAdapter
                Using dtIDNumber As Data.DataTable = AccessIDNumber.GetData(Me.CIFNo)
                    If dtIDNumber.Rows.Count > 0 Then
                        Me.GridIdNumber.DataSource = dtIDNumber
                        Me.GridIdNumber.DataBind()
                        Me.Tr2.Visible = True
                        Me.TrIdNumberFound.Visible = False
                    Else
                        Me.Tr2.Visible = False
                        Me.TrIdNumberFound.Visible = True
                    End If
                End Using
            End Using
        Catch
            Throw
        End Try
    End Sub

    ''' <summary>
    '''  Watch / Negative List
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ShowWatchList()
        Try
            ' Display Watch
            Using AccessWatch As New AMLDAL.CustomerInformationTableAdapters.SelectWatchNegativeListInformationTableAdapter
                Using dtWatch As AMLDAL.CustomerInformation.SelectWatchNegativeListInformationDataTable = AccessWatch.GetData(Me.CIFNo)
                    If dtWatch.Rows.Count > 0 Then
                        Me.DetailData.Visible = True
                        Me.Tr1.Visible = True
                        Me.NotData.Visible = False
                        Me.GridWatch.DataSource = dtWatch
                        Me.GridWatch.DataBind()
                    Else
                        Me.NotData.Visible = True
                        Me.DetailData.Visible = False
                        Me.Tr1.Visible = False
                    End If

                End Using
            End Using
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' TabPhone
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ShowPhone()
        Try
            Using AccessCustomerInformation As New AMLDAL.CustomerInformationTableAdapters.SelectCustomerInformationPhoneDetailTableAdapter
                Using dtPhone As Data.DataTable = AccessCustomerInformation.GetData(Me.CIFNo)
                    If dtPhone.Rows.Count > 0 Then
                        Me.TdPhoneNotAvailable.Visible = False
                        Me.TdPhodeAvailable.Visible = True
                        Me.GridPhone.DataSource = dtPhone
                        Me.GridPhone.DataBind()
                    Else
                        Me.TdPhoneNotAvailable.Visible = True
                        Me.TdPhodeAvailable.Visible = False
                    End If
                End Using
            End Using
        Catch
            Throw
        End Try
    End Sub

    ''' <summary>
    ''' Email
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ShowEmail()
        Try
            Using AccessCustomerInformation As New AMLDAL.CustomerInformationTableAdapters.SelectCustomerInformationEmailDetailTableAdapter
                Using dtEmail As Data.DataTable = AccessCustomerInformation.GetData(Me.CIFNo)
                    If dtEmail.Rows.Count > 0 Then
                        Me.GridEmail.DataSource = dtEmail
                        Me.GridEmail.DataBind()
                        Me.TdElecAddAvailable.Visible = True
                        Me.TdElecAddNotAvailable.Visible = False
                    Else
                        Me.TdElecAddAvailable.Visible = False
                        Me.TdElecAddNotAvailable.Visible = True
                    End If
                End Using
            End Using
        Catch
            Throw
        End Try
    End Sub

    ''' <summary>
    ''' Account Information
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ShowAccountInformation()
        Try
            SearchFilterLiabilities = CboFilterLiabilities.SelectedValue
            SearchFilterCreditCard = CboFilterCreditCard.SelectedValue
            SearchFilterLoan = CboFilterLoan.SelectedValue

            Using objtemp As Data.DataView = AMLBLL.AccountInformationBLL.GetAccountSafetyboxByCIF(Me.CIFNo).DefaultView
                objtemp.RowFilter = SearchFilterLiabilities
                GrdSafedeposit.DataSource = objtemp
                GrdSafedeposit.DataBind()
            End Using

            Using objtemp As Data.DataView = AMLBLL.AccountInformationBLL.GetAccountBA(Me.CIFNo).DefaultView
                objtemp.RowFilter = SearchFilterLiabilities
                GdViewBA.DataSource = objtemp
                GdViewBA.DataBind()
            End Using

            Using objtemp As Data.DataView = AMLBLL.AccountInformationBLL.GetAccountAssetByCIF(Me.CIFNo).DefaultView
                objtemp.RowFilter = SearchFilterLiabilities
                GrdLiabilities.DataSource = objtemp
                GrdLiabilities.DataBind()
            End Using

            Using objtemp As Data.DataView = AMLBLL.AccountInformationBLL.GetAccountLoanByCIF(Me.CIFNo).DefaultView
                objtemp.RowFilter = SearchFilterLoan
                GrdAccountLoan.DataSource = objtemp
                GrdAccountLoan.DataBind()
            End Using
            Using objtemp As Data.DataView = AMLBLL.AccountInformationBLL.GetAccountCreditCardByCIF(Me.CIFNo).DefaultView
                objtemp.RowFilter = SearchFilterCreditCard
                GrdAccountCreditCard.DataSource = objtemp
                GrdAccountCreditCard.DataBind()
            End Using

            'Jive Grid
            Using objMappingGroupMenuHiportJive As VList(Of vw_MappingGroupMenuHiportJive) = DataRepository.vw_MappingGroupMenuHiportJiveProvider.GetPaged("GroupName = '" & Sahassa.AML.Commonly.SessionGroupName & "'", "", 0, Integer.MaxValue, 0)
                If objMappingGroupMenuHiportJive.Count > 0 Then
                    Using objtemp As Data.DataView = AMLBLL.AccountInformationBLL.GetAccountJIVEByCIF(Me.CIFNo).DefaultView
                        objtemp.RowFilter = SearchFilterJIVE
                        GrdJIVE.DataSource = objtemp
                        GrdJIVE.DataBind()
                    End Using
                Else
                    Using objtemp As Data.DataView = AMLBLL.AccountInformationBLL.GetAccountJIVEByCIF(Me.CIFNo).DefaultView
                        Dim OfficerCodeFilter As String = "---"
                        Using objMappingUserIdHiportJive As TList(Of MappingUserIdHiportJive) = MappingUserIdHiportJiveBLL.GetMappingUserIdHIPORTJIVE(Sahassa.AML.Commonly.SessionPkUserId)
                            If objMappingUserIdHiportJive.Count > 0 Then
                                OfficerCodeFilter = objMappingUserIdHiportJive(0).Kode_AO
                            End If
                        End Using

                        If SearchFilterJIVE.Trim.Length = 0 Then
                            SearchFilterJIVE = "OfficerCode = '" & OfficerCodeFilter & "'"
                        End If
                        objtemp.RowFilter = SearchFilterJIVE
                        GrdJIVE.DataSource = objtemp
                        GrdJIVE.DataBind()
                    End Using
                End If
            End Using

            'Hipor Grid
            Using objMappingGroupMenuHiportJive As VList(Of vw_MappingGroupMenuHiportJive) = DataRepository.vw_MappingGroupMenuHiportJiveProvider.GetPaged("GroupName = '" & Sahassa.AML.Commonly.SessionGroupName & "'", "", 0, Integer.MaxValue, 0)
                If objMappingGroupMenuHiportJive.Count > 0 Then
                    Using objtemp As Data.DataView = AMLBLL.AccountInformationBLL.GetAccountHiporByCIF(Me.CIFNo).DefaultView
                        objtemp.RowFilter = SearchFilterJIVE
                        GrdHipor.DataSource = objtemp
                        GrdHipor.DataBind()
                    End Using
                End If
            End Using


            Using objtemp As Data.DataView = AMLBLL.AccountInformationBLL.GetAccountURSCustodyByCIF(Me.CIFNo).DefaultView
                objtemp.RowFilter = SearchFilterLiabilities
                GrdURSCustody.DataSource = objtemp
                GrdURSCustody.DataBind()
            End Using


            LblTotalBancassurance.Text = AMLBLL.AccountInformationBLL.GetTotalBaByCIF(Me.CIFNo, "")
            LblTotalAsset.Text = AMLBLL.AccountInformationBLL.GetTotalAssetByCIF(Me.CIFNo)
            LblTotalAsset1.Text = AMLBLL.AccountInformationBLL.GetTotalAssetByCIF(Me.CIFNo)
            LblLiabilitis.Text = AMLBLL.AccountInformationBLL.GetTotalLiabilitiesByCIF(Me.CIFNo, "")
            LblTotalLoan.Text = AMLBLL.AccountInformationBLL.GetTotalloanByCIF(Me.CIFNo, "")
            LblTotalLoan1.Text = AMLBLL.AccountInformationBLL.GetTotalloanByCIF(Me.CIFNo, "")
            LblTotalCreditCard.Text = AMLBLL.AccountInformationBLL.GetTotalCreditCardByCIF(Me.CIFNo, "")
            LblTotalCreditCard1.Text = AMLBLL.AccountInformationBLL.GetTotalCreditCardByCIF(Me.CIFNo, "")
            LblTotalLiabilities.Text = AMLBLL.AccountInformationBLL.GetTotalLiabilitiesByCIF(Me.CIFNo, "")
            LblTotalLiabilities1.Text = AMLBLL.AccountInformationBLL.GetTotalLiabilitiesByCIF(Me.CIFNo, "")

            LblTotalJIVE.Text = AMLBLL.AccountInformationBLL.GetTotalJIVEByCIF(Me.CIFNo, "")
            LblTotalJive1.Text = AMLBLL.AccountInformationBLL.GetTotalJIVEByCIF(Me.CIFNo, "")

            LblTotalHipor.Text = AMLBLL.AccountInformationBLL.GetTotalHIPORByCIF(Me.CIFNo, "")
            LblTotalHipor1.Text = AMLBLL.AccountInformationBLL.GetTotalHIPORByCIF(Me.CIFNo, "")

            LblTotalURSCustody.Text = AMLBLL.AccountInformationBLL.GetTotalURSCUSTODYByCIF(Me.CIFNo, "")
            LblTotalURSCustody1.Text = AMLBLL.AccountInformationBLL.GetTotalURSCUSTODYByCIF(Me.CIFNo, "")


            'Display for JIVE Grid
            'Jive grid tidak muncul di design mode karena ada control yang didalam <table></table> 
            'sehinggal visual studio menganggap format tag salah dan tidak merender di design view
            If GrdJIVE.Rows.Count < 1 Then
                JIVEPanel.Visible = False
                RowTotalJIVE.Visible = False
            End If

            If GrdHipor.Rows.Count < 1 Then
                HiporPanel.Visible = False
                RowTotalHipor.Visible = False
            End If


            If GrdURSCustody.Rows.Count < 1 Then
                PanelURSCustody.Visible = False
                RowTotalHipor.Visible = False
            End If


            'Using AccessCustomerInformation As New AMLDAL.CustomerInformationTableAdapters.SelectCustomerInformationAccountInfoDetailTableAdapter

            '    Dim dtAccountInformation As Data.DataRow() = AccessCustomerInformation.GetData(Me.CIFNo).Select("", Me.SetnGetSort)

            '    If dtAccountInformation.Length > 0 Then
            '        Me.GridAccountInformation.DataSource = dtAccountInformation
            '        Me.GridAccountInformation.DataBind()
            '        Me.TdAccountInfoAvailable.Visible = True
            '        Me.TdAccountInfoNotAvailable.Visible = False
            '    Else
            '        Me.TdAccountInfoAvailable.Visible = False
            '        Me.TdAccountInfoNotAvailable.Visible = True
            '    End If

            'End Using
        Catch
            Throw
        End Try
    End Sub

    ''' <summary>
    ''' Risk Rating
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ShowRiskRating()
        Try
            ' Get Detail Score
            Using AccessCustomerInformation As New AMLDAL.CustomerInformationTableAdapters.SelectRiskInformationDetailScoreTableAdapter
                Using dtRiskInformation As Data.DataTable = AccessCustomerInformation.GetData(Me.CIFNo)
                    If dtRiskInformation.Rows.Count > 0 Then
                        Me.GridRiskRating.DataSource = dtRiskInformation
                        Me.GridRiskRating.DataBind()
                        Dim StrTemp As String = ""
                        Dim IntRiskTotalScore As Integer = 0
                        Dim StrRiskRatingName As String = ""
                        For Each Row As AMLDAL.CustomerInformation.SelectRiskInformationDetailScoreRow In dtRiskInformation.Rows
                            IntRiskTotalScore += Convert.ToInt32(Row.RiskScore)
                        Next
                        Me.TdRiskInformationAvailable.Visible = True
                        Me.TdRiskInformationNotAvailable.Visible = False

                        Using RiskRatingAdapter As New AMLDAL.AMLDataSetTableAdapters.RiskRatingTableAdapter
                            Using RiskRatingTable As New AMLDAL.AMLDataSet.RiskRatingDataTable
                                RiskRatingAdapter.Fill(RiskRatingTable)
                                If Not RiskRatingTable Is Nothing Then
                                    If RiskRatingTable.Rows.Count > 0 Then
                                        Dim RiskRatingTableRow As AMLDAL.AMLDataSet.RiskRatingRow
                                        For Each RiskRatingTableRow In RiskRatingTable.Rows
                                            If IntRiskTotalScore >= RiskRatingTableRow.RiskScoreFrom And IntRiskTotalScore <= RiskRatingTableRow.RiskScoreTo Then
                                                'If Not RiskRatingTableRow.IsRiskRatingNameNull Then
                                                StrRiskRatingName = RiskRatingTableRow.RiskRatingName
                                                'End If
                                                Exit For
                                            End If
                                        Next
                                    End If
                                End If
                            End Using
                        End Using
                        'Get Total Risk Score 
                        Me.RiskRatingNameLabel.Text = StrRiskRatingName
                        Me.LblTotalRating.Text = IntRiskTotalScore
                    Else
                        Me.TdRiskInformationAvailable.Visible = False
                        Me.TdRiskInformationNotAvailable.Visible = True
                    End If
                End Using
            End Using
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' LoadDetailAllVerification
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub LoadDetailAllVerification(ByVal VerificationListId As Int64)
        Try
            ' Display Address
            Me.ListAddress.Items.Clear()
            Using AccessAddress As New AMLDAL.CustomerInformationTableAdapters.SelectAddressPotentialCustomerMatchTableAdapter
                Using dtAddress As Data.DataTable = AccessAddress.GetData(VerificationListId)
                    For Each RowAddress As AMLDAL.CustomerInformation.SelectAddressPotentialCustomerMatchRow In dtAddress.Rows
                        Me.ListAddress.Items.Add(RowAddress.Address)
                    Next
                End Using
            End Using

            ' display alias
            Me.ListAlias.Items.Clear()
            Using AccessAlias As New AMLDAL.CustomerInformationTableAdapters.SelectVerificationList_AliasTableAdapter
                Using dtAlias As Data.DataTable = AccessAlias.GetData(VerificationListId)
                    For Each RowAlias As AMLDAL.CustomerInformation.SelectVerificationList_AliasRow In dtAlias.Rows
                        Me.ListAlias.Items.Add(RowAlias.Name)
                    Next
                End Using
            End Using

            ' display ID Number
            Me.ListIdNumber.Items.Clear()
            Using AccessIdNumber As New AMLDAL.CustomerInformationTableAdapters.SelectIDNumberPotentialCustomerMatchTableAdapter
                Using dtIdNumber As Data.DataTable = AccessIdNumber.GetData(VerificationListId)
                    For Each RowIdNumber As AMLDAL.CustomerInformation.SelectIDNumberPotentialCustomerMatchRow In dtIdNumber.Rows
                        Me.ListIdNumber.Items.Add(RowIdNumber.IDNumber)
                    Next
                End Using
            End Using

            ' display Verification Master
            Me.LblDateOfData.Text = ""
            Me.TxtCustomRemark1.Text = ""
            Me.TxtCustomRemark2.Text = ""
            Me.TxtCustomRemark3.Text = ""
            Me.TxtCustomRemark4.Text = ""
            Me.TxtCustomRemark5.Text = ""

            Using AccessVerificationMaster As New AMLDAL.CustomerInformationTableAdapters.SelectCustomerVerificationMatchDetailTableAdapter
                Using dtVerification As Data.DataTable = AccessVerificationMaster.GetData(CInt(VerificationListId))
                    If dtVerification.Rows.Count > 0 Then
                        Dim RowVerification As AMLDAL.CustomerInformation.SelectCustomerVerificationMatchDetailRow = dtVerification.Rows(0)
                        Me.LblDateOfData.Text = IIf(RowVerification.DateOfData <> "", CDate(RowVerification.DateOfData).ToString("dd MMMM yyyy"), "")
                        Me.TxtCustomRemark1.Text = RowVerification.CustomRemark1
                        Me.TxtCustomRemark2.Text = RowVerification.CustomRemark2
                        Me.TxtCustomRemark3.Text = RowVerification.CustomRemark3
                        Me.TxtCustomRemark4.Text = RowVerification.CustomRemark4
                        Me.TxtCustomRemark5.Text = RowVerification.CustomRemark5
                    End If
                End Using
            End Using
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' detail
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub GridWatch_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridWatch.EditCommand
        Try
            Me.LoadDetailAllVerification(CInt(e.Item.Cells(0).Text))
        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' Back To Previous Page
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click
        Me.ListIdNumber.Items.Clear()
        Me.ListAlias.Items.Clear()
        Me.ListAddress.Items.Clear()

        Sahassa.AML.Commonly.SessionIntendedPage = "CustomerInformation.aspx"

        Me.Response.Redirect("CustomerInformation.aspx", False)
    End Sub


    Protected Sub Menu1_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs) Handles Menu1.MenuItemClick
        MultiView1.ActiveViewIndex = Int32.Parse(e.Item.Value)
    End Sub

    ''' <summary>
    ''' Show General
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub TabGeneral_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabGeneral.Activate
        Try
            ' Do Nothing
        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    ''' <summary>
    ''' Show Address
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub TabAddress_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabAddress.Activate
        Try
            Me.ShowAddress()
        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    ''' <summary>
    ''' Show Phone
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub TabPhone_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabPhone.Activate
        Try
            Me.ShowPhone()
        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    ''' <summary>
    ''' ID Number
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub TabIDNumber_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabIDNumber.Activate
        Try
            Me.ShowIdNumber()
        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    ''' <summary>
    ''' Show Email
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub TabElectronicAddress_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabElectronicAddress.Activate
        Try
            Me.ShowEmail()
        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    ''' <summary>
    ''' Show Account Information
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub TabAccountInformation_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabAccountInformation.Activate
        Try
            Me.ShowAccountInformation()
        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    ''' <summary>
    ''' Show Watch / Negative List
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub TabVerificationList_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabVerificationList.Activate
        Try
            Me.ShowWatchList()
        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
        End Try
    End Sub
    ''' <summary>
    ''' Show Risk Rating
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub TabRiskInformation_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabRiskInformation.Activate
        Try
            Me.ShowRiskRating()
        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub TabLinkAnalysis_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabLinkAnalysis.Activate
        Session("LinkAnalysisCIFNo") = Me.CIFNo
        filterLinkAnalysis()
    End Sub

    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("CustomerInformationDetail.AccountInformationDetailSort") Is Nothing, "AccountNo  ASC", Session("CustomerInformationDetail.AccountInformationDetailSort"))
        End Get
        Set(ByVal Value As String)
            Session("CustomerInformationDetail.AccountInformationDetailSort") = Value
        End Set
    End Property

    'Protected Sub GridAccountInformation_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GrdAccountAsset.SortCommand
    '    Dim GridUser As DataGrid = source
    '    Try
    '        Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
    '        GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort

    '        ShowAccountInformation()
    '    Catch ex As Exception
    '        Me.CValPageError.IsValid = False
    '        Me.CValPageError.ErrorMessage = ex.Message
    '        LogError(ex)
    '    End Try
    'End Sub

    Protected Sub TabEmployeeHistory_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabEmployeeHistory.Activate
        Try
            GrdEmployeeHistory.DataSource = AMLBLL.AccountInformationBLL.GetCIFEmployeeHistory(Me.CIFNo)
            GrdEmployeeHistory.DataBind()
        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try

    End Sub



    Protected Sub ImgSearchLiabilities_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgSearchLiabilities.Click
        Try
            If CboFilterLiabilities.SelectedValue <> "" Then
                If CboFilterLiabilities.SelectedIndex <> 7 Then
                    SearchFilterLiabilities = CboFilterLiabilities.SelectedValue.Replace("-=Search=-", txtFilterliabilities.Text.Trim)
                Else
                    If Not Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", txtFilterliabilities.Text.Trim) Then
                        Throw New Exception("Opening Date Must dd-MMM-yyyy")
                    End If

                    Me.popupliabilities.Style.Add("display", "")


                    Dim searchdate As Date = Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", txtFilterliabilities.Text.Trim)
                    SearchFilterLiabilities = CboFilterLiabilities.SelectedValue.Replace("-=Search=-", searchdate.ToString("yyyy-MM-dd"))
                End If

            Else
                SearchFilterLiabilities = CboFilterLiabilities.SelectedValue
            End If

            LblTotalLiabilities.Text = AMLBLL.AccountInformationBLL.GetTotalLiabilitiesByCIF(Me.CIFNo, SearchFilterLiabilities)
            LblTotalLiabilities1.Text = AMLBLL.AccountInformationBLL.GetTotalLiabilitiesByCIF(Me.CIFNo, SearchFilterLiabilities)


            Using objtemp As Data.DataView = AMLBLL.AccountInformationBLL.GetAccountAssetByCIF(Me.CIFNo).DefaultView
                objtemp.RowFilter = SearchFilterLiabilities
                GrdLiabilities.DataSource = objtemp
                GrdLiabilities.DataBind()
            End Using

        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImgSearchLoan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgSearchLoan.Click
        Try
            If CboFilterLoan.SelectedValue <> "" Then
                If CboFilterLoan.SelectedIndex = 7 Then
                    If Not Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", txtfilterloan.Text.Trim) Then
                        Throw New Exception("Opening Date Must dd-MMM-yyyy")
                    End If

                    Me.popUploan.Style.Add("display", "")

                    Dim searchdate As Date = Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", txtfilterloan.Text.Trim)
                    SearchFilterLoan = CboFilterLoan.SelectedValue.Replace("-=Search=-", searchdate.ToString("yyyy-MM-dd"))
                ElseIf CboFilterLoan.SelectedIndex = 8 Then
                    If Not Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", txtfilterloan.Text.Trim) Then
                        Throw New Exception("Closing Date Must dd-MMM-yyyy")
                    End If

                    Me.popUploan.Style.Add("display", "")

                    Dim searchdate As Date = Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", txtfilterloan.Text.Trim)
                    SearchFilterLoan = CboFilterLoan.SelectedValue.Replace("-=Search=-", searchdate.ToString("yyyy-MM-dd"))
                ElseIf CboFilterLoan.SelectedIndex = 9 Then
                    If Not Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", txtfilterloan.Text.Trim) Then
                        Throw New Exception("Maturity Date Must dd-MMM-yyyy")
                    End If

                    Me.popUploan.Style.Add("display", "")

                    Dim searchdate As Date = Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", txtfilterloan.Text.Trim)
                    SearchFilterLoan = CboFilterLoan.SelectedValue.Replace("-=Search=-", searchdate.ToString("yyyy-MM-dd"))
                Else
                    SearchFilterLoan = CboFilterLoan.SelectedValue.Replace("-=Search=-", txtfilterloan.Text.Trim)
                End If
            Else
                SearchFilterLoan = CboFilterLoan.SelectedValue
            End If


            Using objtemp As Data.DataView = AMLBLL.AccountInformationBLL.GetAccountLoanByCIF(Me.CIFNo).DefaultView
                objtemp.RowFilter = SearchFilterLoan
                GrdAccountLoan.DataSource = objtemp
                GrdAccountLoan.DataBind()
            End Using
            LblTotalLoan.Text = AMLBLL.AccountInformationBLL.GetTotalloanByCIF(Me.CIFNo, SearchFilterLoan)
            LblTotalLoan1.Text = AMLBLL.AccountInformationBLL.GetTotalloanByCIF(Me.CIFNo, SearchFilterLoan)



        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImgFilStakeholder_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgFilStakeholder.Click
        Try
            If CbFilStakeholder.SelectedValue <> "" Then
                If CbFilStakeholder.SelectedIndex = 1 Then
                    If Not Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", TxtFilStakeholder.Text.Trim) Then
                        Throw New Exception("Bussiness Date Must dd-MMM-yyyy")
                    End If

                    Me.popStakeholder.Style.Add("display", "")

                    Dim searchdate As Date = Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", TxtFilStakeholder.Text.Trim)
                    SearchFilterStakeholder = CbFilStakeholder.SelectedValue.Replace("-=Search=-", searchdate.ToString("yyyy-MM-dd"))

                ElseIf CbFilStakeholder.SelectedIndex = 3 Then

                    If TxtFilStakeholder.Text = "" Then
                        Throw New Exception("Please Fill Stackeholder seq no")
                    ElseIf Not IsNumeric(TxtFilStakeholder.Text) Then
                        Throw New Exception("Please Fill Stackeholder seq no numeric")
                    Else
                        SearchFilterStakeholder = CbFilStakeholder.SelectedValue.Replace("-=Search=-", TxtFilStakeholder.Text.Trim)
                    End If
                ElseIf CbFilStakeholder.SelectedIndex = 10 Then
                    If Not Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", TxtFilStakeholder.Text.Trim) Then
                        Throw New Exception("Last Maintenance Date Must dd-MMM-yyyy")
                    End If

                    Me.popStakeholder.Style.Add("display", "")

                    Dim searchdate As Date = Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", TxtFilStakeholder.Text.Trim)
                    SearchFilterStakeholder = CbFilStakeholder.SelectedValue.Replace("-=Search=-", searchdate.ToString("yyyy-MM-dd"))
                ElseIf CbFilStakeholder.SelectedIndex = 11 Then
                    If Not Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", TxtFilStakeholder.Text.Trim) Then
                        Throw New Exception("DOB Date Must dd-MMM-yyyy")
                    End If

                    Me.popStakeholder.Style.Add("display", "")

                    Dim searchdate As Date = Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", TxtFilStakeholder.Text.Trim)
                    SearchFilterStakeholder = CbFilStakeholder.SelectedValue.Replace("-=Search=-", searchdate.ToString("yyyy-MM-dd"))
                ElseIf CbFilStakeholder.SelectedIndex = 13 Then

                    If TxtFilStakeholder.Text = "" Then
                        Throw New Exception("Please Fill ownership")
                    ElseIf Not IsNumeric(TxtFilStakeholder.Text) Then
                        Throw New Exception("Please Fill ownership with numeric")
                    Else
                        SearchFilterStakeholder = CbFilStakeholder.SelectedValue.Replace("-=Search=-", TxtFilStakeholder.Text.Trim)
                    End If


                Else
                    SearchFilterStakeholder = CbFilStakeholder.SelectedValue.Replace("-=Search=-", TxtFilStakeholder.Text.Trim)
                End If
            Else
                SearchFilterStakeholder = CbFilStakeholder.SelectedValue
            End If

            If CbFilStakeholder.SelectedIndex = 1 Or CbFilStakeholder.SelectedIndex = 10 Or CbFilStakeholder.SelectedIndex = 11 Then
                Me.popStakeholder.Style.Add("display", "")
            Else
                Me.popStakeholder.Style.Add("display", "none")
            End If

            Using objtemp As Data.DataView = LoadStakeHolder.DefaultView
                objtemp.RowFilter = SearchFilterStakeholder
                GridStakeholderDetail.DataSource = objtemp
                GridStakeholderDetail.DataBind()
            End Using



        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImgFilBeneficial_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgFilBeneficial.Click
        Try
            If CbFilBeneficial.SelectedValue <> "" Then
                If CbFilBeneficial.SelectedIndex = 1 Then
                    If Not Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", TxtFilBeneficial.Text.Trim) Then
                        Throw New Exception("Bussiness Date Must dd-MMM-yyyy")
                    End If

                    Me.popBeneficial.Style.Add("display", "")

                    Dim searchdate As Date = Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", TxtFilBeneficial.Text.Trim)
                    SearchFilterBeneficial = CbFilBeneficial.SelectedValue.Replace("-=Search=-", searchdate.ToString("yyyy-MM-dd"))
                ElseIf CbFilBeneficial.SelectedIndex = 14 Then
                    If Not Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", TxtFilBeneficial.Text.Trim) Then
                        Throw New Exception("DOB Must dd-MMM-yyyy")
                    End If

                    Me.popBeneficial.Style.Add("display", "")

                    Dim searchdate As Date = Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", TxtFilBeneficial.Text.Trim)
                    SearchFilterBeneficial = CbFilBeneficial.SelectedValue.Replace("-=Search=-", searchdate.ToString("yyyy-MM-dd"))
                Else
                    SearchFilterBeneficial = CbFilBeneficial.SelectedValue.Replace("-=Search=-", TxtFilBeneficial.Text.Trim)
                End If
            Else
                SearchFilterBeneficial = CbFilBeneficial.SelectedValue
            End If

            If CbFilBeneficial.SelectedIndex = 1 Or CbFilBeneficial.SelectedIndex = 14 Then
                Me.popBeneficial.Style.Add("display", "")
            Else
                Me.popBeneficial.Style.Add("display", "none")
            End If

            Using objtemp As Data.DataView = LoadBeneficialOwner.DefaultView
                objtemp.RowFilter = SearchFilterBeneficial
                GridBeneficialOwner.DataSource = objtemp
                GridBeneficialOwner.DataBind()
            End Using



        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub CheckDateFormat(textToValidateParam As String, errorMessageToShowParam As String)

        If Not Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", textToValidateParam) Then
            Throw New Exception(errorMessageToShowParam)
        End If

    End Sub

    Private Sub CheckNumericFormat(textToValidateParam As String, errorMessageToShowParam As String)

        If String.IsNullOrEmpty(textToValidateParam) OrElse Not IsNumeric(textToValidateParam) Then
            Throw New Exception(errorMessageToShowParam)
        End If

    End Sub

    'Protected Sub ImgFilCardBillingAddress_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgFilCardBillingAddress.Click
    '    Try
    '        Dim searchDate As Date

    '        If CbFilCardBillingAddress.SelectedValue <> "" Then

    '            Select Case CbFilCardBillingAddress.SelectedIndex
    '                Case 21

    '                    CheckDateFormat(TxtFilCardBillingAddress.Text.Trim(), "Date of Birth format must dd-MMM-yyyy")

    '                    'If Not Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", TxtFilCardBillingAddress.Text.Trim) Then
    '                    '    Throw New Exception("Date of Birth format must dd-MMM-yyyy")
    '                    'End If

    '                    Me.popStakeholder.Style.Add("display", "")

    '                    searchDate = Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", TxtFilStakeholder.Text.Trim)
    '                    SearchFilterStakeholder = CbFilStakeholder.SelectedValue.Replace("-=Search=-", searchdate.ToString("yyyy-MM-dd"))

    '                Case 35, 36, 37

    '                    CheckNumericFormat(TxtFilStakeholder.Text, "Please fill search data with numeric")

    '                    SearchFilterStakeholder = CbFilStakeholder.SelectedValue.Replace("-=Search=-", TxtFilStakeholder.Text.Trim)

    '                Case Else
    '                     SearchFilterStakeholder = CbFilStakeholder.SelectedValue.Replace("-=Search=-", TxtFilStakeholder.Text.Trim)
    '            End Select

    '            'If CbFilCardBillingAddress.SelectedIndex = 21 Then

    '            '    CheckDateFormat(TxtFilCardBillingAddress.Text.Trim(), "Date of Birth format must dd-MMM-yyyy")

    '            '    'If Not Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", TxtFilCardBillingAddress.Text.Trim) Then
    '            '    '    Throw New Exception("Date of Birth format must dd-MMM-yyyy")
    '            '    'End If

    '            '    Me.popStakeholder.Style.Add("display", "")

    '            '    searchDate = Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", TxtFilStakeholder.Text.Trim)
    '            '    SearchFilterStakeholder = CbFilStakeholder.SelectedValue.Replace("-=Search=-", searchdate.ToString("yyyy-MM-dd"))

    '            'ElseIf CbFilStakeholder.SelectedIndex = 35 Then

    '            '    CheckNumericFormat(TxtFilStakeholder.Text, "Please fill search data with numeric")

    '            '    SearchFilterStakeholder = CbFilStakeholder.SelectedValue.Replace("-=Search=-", TxtFilStakeholder.Text.Trim)

    '            '    'If TxtFilStakeholder.Text = "" Then
    '            '    '    Throw New Exception("Please Fill Stackeholder seq no")
    '            '    'ElseIf Not isnumeric(TxtFilStakeholder.Text) Then
    '            '    '    Throw New Exception("Please Fill Stackeholder seq no numeric")
    '            '    'Else
    '            '    '    SearchFilterStakeholder = CbFilStakeholder.SelectedValue.Replace("-=Search=-", TxtFilStakeholder.Text.Trim)
    '            '    'End If
    '            'ElseIf CbFilStakeholder.SelectedIndex = 10 Then
    '            '    If Not Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", TxtFilStakeholder.Text.Trim) Then
    '            '        Throw New Exception("Last Maintenance Date Must dd-MMM-yyyy")
    '            '    End If

    '            '    Me.popStakeholder.Style.Add("display", "")

    '            '    searchDate = Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", TxtFilStakeholder.Text.Trim)
    '            '    SearchFilterStakeholder = CbFilStakeholder.SelectedValue.Replace("-=Search=-", searchdate.ToString("yyyy-MM-dd"))
    '            'ElseIf CbFilStakeholder.SelectedIndex = 11 Then
    '            '    If Not Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", TxtFilStakeholder.Text.Trim) Then
    '            '        Throw New Exception("DOB Date Must dd-MMM-yyyy")
    '            '    End If

    '            '    Me.popStakeholder.Style.Add("display", "")

    '            '    searchDate = Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", TxtFilStakeholder.Text.Trim)
    '            '    SearchFilterStakeholder = CbFilStakeholder.SelectedValue.Replace("-=Search=-", searchdate.ToString("yyyy-MM-dd"))
    '            'ElseIf CbFilStakeholder.SelectedIndex = 13 Then

    '            '    If TxtFilStakeholder.Text = "" Then
    '            '        Throw New Exception("Please Fill ownership")
    '            '    ElseIf Not isnumeric(TxtFilStakeholder.Text) Then
    '            '        Throw New Exception("Please Fill ownership with numeric")
    '            '    Else
    '            '        SearchFilterStakeholder = CbFilStakeholder.SelectedValue.Replace("-=Search=-", TxtFilStakeholder.Text.Trim)
    '            '    End If


    '            'Else
    '            '    SearchFilterStakeholder = CbFilStakeholder.SelectedValue.Replace("-=Search=-", TxtFilStakeholder.Text.Trim)
    '            'End If
    '        Else
    '            SearchFilterStakeholder = CbFilStakeholder.SelectedValue
    '        End If

    '        'If CbFilStakeholder.SelectedIndex = 1 Or CbFilStakeholder.SelectedIndex = 10 Or CbFilStakeholder.SelectedIndex = 11 Then
    '        '    Me.popStakeholder.Style.Add("display", "")
    '        'Else
    '        '    Me.popStakeholder.Style.Add("display", "none")
    '        'End If

    '        Using objtemp As Data.DataView = LoadStakeHolder.DefaultView
    '            objtemp.RowFilter = SearchFilterStakeholder
    '            GridStakeholderDetail.DataSource = objtemp
    '            GridStakeholderDetail.DataBind()
    '        End Using



    '    Catch ex As Exception
    '        Me.CValPageError.IsValid = False
    '        Me.CValPageError.ErrorMessage = ex.Message
    '        LogError(ex)
    '    End Try
    'End Sub


    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Try
            If CboFilterCreditCard.SelectedValue <> "" Then


                If CboFilterCreditCard.SelectedIndex <> 7 Then
                    SearchFilterCreditCard = CboFilterCreditCard.SelectedValue.Replace("-=Search=-", txtfiltercreditcard.Text.Trim)
                Else
                    If Not Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", txtfiltercreditcard.Text.Trim) Then
                        Throw New Exception("Opening Date Must dd-MMM-yyyy")
                    End If

                    Me.popupcreditcard.Style.Add("display", "")


                    Dim searchdate As Date = Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", txtfiltercreditcard.Text.Trim)
                    SearchFilterCreditCard = CboFilterCreditCard.SelectedValue.Replace("-=Search=-", searchdate.ToString("yyyy-MM-dd"))
                End If

            Else
                SearchFilterCreditCard = CboFilterCreditCard.SelectedValue
            End If


            Using objtemp As Data.DataView = AMLBLL.AccountInformationBLL.GetAccountCreditCardByCIF(Me.CIFNo).DefaultView
                objtemp.RowFilter = SearchFilterCreditCard
                GrdAccountCreditCard.DataSource = objtemp
                GrdAccountCreditCard.DataBind()
            End Using
            LblTotalCreditCard.Text = AMLBLL.AccountInformationBLL.GetTotalCreditCardByCIF(Me.CIFNo, SearchFilterCreditCard)
            LblTotalCreditCard1.Text = AMLBLL.AccountInformationBLL.GetTotalCreditCardByCIF(Me.CIFNo, SearchFilterCreditCard)
        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GrdSafedeposit_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GrdSafedeposit.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                If e.Row.Cells(4).Text = "01-Jan-1900" Then
                    e.Row.Cells(4).Text = ""
                End If
                If e.Row.Cells(5).Text = "01-Jan-1900" Then
                    e.Row.Cells(5).Text = ""
                End If
                If e.Row.Cells(6).Text = "01-Jan-1900" Then
                    e.Row.Cells(6).Text = ""
                End If
            End If
        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton2.Click
        Try
            If CboFilterSDB.SelectedValue <> "" Then
                If CboFilterSDB.SelectedIndex = 0 Or CboFilterSDB.SelectedIndex = 1 Or CboFilterSDB.SelectedIndex = 2 Or CboFilterSDB.SelectedIndex = 3 Or CboFilterSDB.SelectedIndex = 4 Then
                    SearchFilterSafetyBoxCIF = CboFilterSDB.SelectedValue.Replace("-=Search=-", txtSDB.Text.Trim)
                Else
                    If Not Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", txtSDB.Text.Trim) Then
                        Throw New Exception("Opening Date Must dd-MMM-yyyy")
                    End If

                    Me.popUploan.Style.Add("display", "")


                    Dim searchdate As Date = Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", txtSDB.Text.Trim)
                    SearchFilterSafetyBoxCIF = CboFilterSDB.SelectedValue.Replace("-=Search=-", searchdate.ToString("yyyy-MM-dd"))
                End If
            Else
                SearchFilterSafetyBoxCIF = CboFilterSDB.SelectedValue
            End If




            Using objtemp As Data.DataView = AMLBLL.AccountInformationBLL.GetAccountSafetyboxByCIF(Me.CIFNo).DefaultView
                objtemp.RowFilter = SearchFilterSafetyBoxCIF
                GrdSafedeposit.DataSource = objtemp
                GrdSafedeposit.DataBind()
            End Using





        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Function IsValidSearch() As Boolean
        Try
            If txtFromAmount.Text.Trim.Length <> 0 And txtToAmount.Text.Trim.Length <> 0 Then
                If Not Double.TryParse(txtFromAmount.Text, 0) Then
                    Throw New Exception("Amount (From) must be filled by number")
                End If
                If Not Double.TryParse(txtToAmount.Text, 0) Then
                    Throw New Exception("Amount (To) must be filled by number")
                End If
                If CDbl(txtFromAmount.Text) > CDbl(txtToAmount.Text) Then
                    Throw New Exception("Amount (To) must be greater than number")
                End If

                Return True
            End If
            Return False
        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
            LogError(ex)
            Return False
        End Try
    End Function

    Sub filterLinkAnalysis()
        If IsValidSearch() Then
            Session("LinkAnalysisFromAmount") = txtFromAmount.Text
            Session("LinkAnalysisToAmount") = txtToAmount.Text
        End If
    End Sub

    Protected Sub imgBtnGOLinkAnalysis_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnGOLinkAnalysis.Click
        filterLinkAnalysis()
        Session("LinkAnalisys") = True
        Response.Redirect("CustomerInformationDetail.aspx?CIFNo=" & CIFNo)
    End Sub

    Protected Sub imgBtnClearSearchLinkAnalysis_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnClearSearchLinkAnalysis.Click
        Session("LinkAnalysisFromAmount") = Nothing
        Session("LinkAnalysisToAmount") = Nothing
        Session("LinkAnalisys") = True
        Response.Redirect("CustomerInformationDetail.aspx?CIFNo=" & CIFNo)
    End Sub

    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton3.Click
        Try
            If CboFilterJIVE.SelectedValue <> "" Then
                If CboFilterJIVE.SelectedIndex <> 7 Then
                    SearchFilterJIVE = CboFilterJIVE.SelectedValue.Replace("-=Search=-", txtFilterJIVE.Text.Trim)
                Else
                    If Not Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", txtFilterJIVE.Text.Trim) Then
                        Throw New Exception("Opening Date Must dd-MMM-yyyy")
                    End If

                    Me.popupJIVE.Style.Add("display", "")


                    Dim searchdate As Date = Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", txtFilterJIVE.Text.Trim)
                    SearchFilterJIVE = CboFilterJIVE.SelectedValue.Replace("-=Search=-", searchdate.ToString("yyyy-MM-dd"))
                End If

            Else
                SearchFilterJIVE = CboFilterJIVE.SelectedValue
            End If

            LblTotalJIVE.Text = AMLBLL.AccountInformationBLL.GetTotalJIVEByCIF(Me.CIFNo, SearchFilterJIVE)
            LblTotalJive1.Text = AMLBLL.AccountInformationBLL.GetTotalJIVEByCIF(Me.CIFNo, SearchFilterJIVE)


            Using objtemp As Data.DataView = AMLBLL.AccountInformationBLL.GetAccountJIVEByCIF(Me.CIFNo).DefaultView
                objtemp.RowFilter = SearchFilterJIVE
                GrdJIVE.DataSource = objtemp
                GrdJIVE.DataBind()
            End Using

        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageButton4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton4.Click
        Try
            If CboFilterHipor.SelectedValue <> "" Then
                If CboFilterHipor.SelectedIndex <> 1 Then
                    SearchFilterHIPOR = CboFilterHipor.SelectedValue.Replace("-=Search=-", txtFilterHipor.Text.Trim)
                Else
                    If Not Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", txtFilterHipor.Text.Trim) Then
                        Throw New Exception("NAV TXN DATE Must dd-MMM-yyyy")
                    End If

                    Me.popUpHipor.Style.Add("display", "")


                    Dim searchdate As Date = Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", txtFilterHipor.Text.Trim)
                    SearchFilterHIPOR = CboFilterHipor.SelectedValue.Replace("-=Search=-", searchdate.ToString("yyyy-MM-dd"))
                End If

            Else
                SearchFilterHIPOR = CboFilterHipor.SelectedValue
            End If

            LblTotalHipor.Text = AMLBLL.AccountInformationBLL.GetTotalHIPORByCIF(Me.CIFNo, SearchFilterHIPOR)
            LblTotalHipor1.Text = AMLBLL.AccountInformationBLL.GetTotalHIPORByCIF(Me.CIFNo, SearchFilterHIPOR)


            Using objtemp As Data.DataView = AMLBLL.AccountInformationBLL.GetAccountHiporByCIF(Me.CIFNo).DefaultView
                objtemp.RowFilter = SearchFilterHIPOR
                GrdHipor.DataSource = objtemp
                GrdHipor.DataBind()
            End Using

        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageButton5_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton5.Click
        Try
            If CboFilterURSCustody.SelectedValue <> "" Then
                If CboFilterURSCustody.SelectedIndex <> 1 Then
                    SearchFilterURSCustody = CboFilterURSCustody.SelectedValue.Replace("-=Search=-", txtFilterURSCustody.Text.Trim.Replace(",", ""))
                Else
                    If Not Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", txtFilterURSCustody.Text.Trim) Then
                        Throw New Exception("Tanggal Holding Format Must dd-MMM-yyyy")
                    End If

                    Me.popupUSRCustody.Style.Add("display", "")


                    Dim searchdate As Date = Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", txtFilterURSCustody.Text.Trim)
                    SearchFilterURSCustody = CboFilterURSCustody.SelectedValue.Replace("-=Search=-", searchdate.ToString("yyyy-MM-dd"))
                End If

            Else
                SearchFilterURSCustody = CboFilterURSCustody.SelectedValue
            End If

            LblTotalURSCustody.Text = AMLBLL.AccountInformationBLL.GetTotalURSCUSTODYByCIF(Me.CIFNo, SearchFilterURSCustody)
            LblTotalURSCustody1.Text = AMLBLL.AccountInformationBLL.GetTotalURSCUSTODYByCIF(Me.CIFNo, SearchFilterURSCustody)


            Using objtemp As Data.DataView = AMLBLL.AccountInformationBLL.GetAccountURSCustodyByCIF(Me.CIFNo).DefaultView
                objtemp.RowFilter = SearchFilterURSCustody
                GrdURSCustody.DataSource = objtemp
                GrdURSCustody.DataBind()
            End Using

        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    'for CDD

    Private Function SetnGetBindTable() As CDDLNPNettier.Entities.VList(Of CDDLNPNettier.Entities.vw_CDDLNP_View)
        Return CDDLNPNettier.Data.DataRepository.vw_CDDLNP_ViewProvider.GetPaged(SearchFilterCDD, "", 0, Integer.MaxValue, 0)
    End Function
    Private Function SearchFilterCDD() As String
        Dim strWhereClause(-1) As String

        Try
            ReDim Preserve strWhereClause(strWhereClause.Length)
            strWhereClause(strWhereClause.Length - 1) = CDDLNPNettier.Entities.vw_CDDLNP_ViewColumn.CIF.ToString & " = '" & Me.CIFNo.Trim.Replace("'", "''") & "'"

            ReDim Preserve strWhereClause(strWhereClause.Length)
            strWhereClause(strWhereClause.Length - 1) = "FK_RefParent_ID IS NULL  AND CreatedBy IN (" & GetRMUser() & ")"

            Return String.Join(" AND ", strWhereClause)
        Catch
            Throw
        End Try
    End Function
    Private Function GetRMUser() As String
        Dim strUser As String = Sahassa.AML.Commonly.SessionUserId
        Dim strWhereClause As String = " List_TeamHead LIKE '%" & strUser & ";%' OR List_GroupHead LIKE '%" & strUser & ";%' OR List_HeadOff LIKE '%" & strUser & ";%'"
        Dim strChild As String = "'" & Sahassa.AML.Commonly.SessionUserId & "',"

        'Get RM under this user
        For Each objWorkflow As CDDLNPNettier.Entities.CDDLNP_WorkflowUpload In CDDLNPNettier.Data.DataRepository.CDDLNP_WorkflowUploadProvider.GetPaged(strWhereClause, String.Empty, 0, Int32.MaxValue, 0)
            For Each user As String In objWorkflow.List_RM.Split(";")
                strChild &= "'" & user & "',"
            Next
        Next

        Return strChild.Remove(strChild.LastIndexOf(","))
    End Function
    Protected Sub TabCDDLNP_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabCDDLNP.Activate
        Try
            TableCDDDetail.Visible = False
            Me.ShowCDDLNP()
        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
        End Try
    End Sub
    Private Sub ShowCDDLNP()
        Try
            ' Display Watch
            Using Objvw_CDDLNP_View As CDDLNPNettier.Entities.VList(Of CDDLNPNettier.Entities.vw_CDDLNP_View) = CDDLNPNettier.Data.DataRepository.vw_CDDLNP_ViewProvider.GetPaged(SearchFilterCDD, "", 0, Integer.MaxValue, 0)
                If Objvw_CDDLNP_View.Count > 0 Then
                    'Me.DetailData.Visible = True
                    'Me.Tr1.Visible = True
                    'Me.NotData.Visible = False
                    Me.lblNoRecord.Visible = False
                    Me.GridViewCDDLNP.DataSource = SetnGetBindTable()
                    Me.GridViewCDDLNP.DataBind()
                Else
                    'Me.NotData.Visible = True
                    'Me.DetailData.Visible = False
                    Me.lblNoRecord.Visible = True
                End If
            End Using
        Catch
            Throw
        End Try
    End Sub

    Protected Sub GridViewCDDLNP_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridViewCDDLNP.ItemDataBound
        Try
            'Dim ArrTarget As ArrayList = Nothing
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                'ArrTarget.Add(e.Item.Cells(1).Text)
                e.Item.Cells(2).Text = CStr((e.Item.ItemIndex + 1))
                'Dim chkBox As CheckBox = e.Item.FindControl("CheckBoxExporttoExcel")
                'chkBox.Checked = Me.SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)
                'If BindGridFromExcel = True Then

                'Else
                '    e.Item.Cells(2).Text = CStr((e.Item.ItemIndex + 1) + (Me.SetnGetCurrentPage * Sahassa.AML.Commonly.GetDisplayedTotalRow))
                'End If

                'Parent BO
                If e.Item.Cells(12).Text = "&nbsp;" Then
                    e.Item.Cells(12).Text = "N/A"
                End If

                'Approval next PIC
                Select Case e.Item.Cells(16).Text
                    Case 1, 3 'Jika Draft dan Request change maka PIC adalah CDD Initiator
                        e.Item.Cells(17).Text = "RM - " & e.Item.Cells(14).Text

                    Case 4, 5 'Jika Approve dan Declined maka PIC adalah "-"
                        e.Item.Cells(17).Text = "-"

                    Case 2
                        Dim strUser As String = e.Item.Cells(14).Text
                        Dim strWhereClause As String = "List_RM LIKE '%" & strUser & ";%'"
                        Using objWorkflow As CDDLNPNettier.Entities.TList(Of CDDLNPNettier.Entities.CDDLNP_WorkflowUpload) = CDDLNPNettier.Data.DataRepository.CDDLNP_WorkflowUploadProvider.GetPaged(strWhereClause, String.Empty, 0, 1, 0)
                            If objWorkflow.Count > 0 Then
                                Select Case CInt(e.Item.Cells(17).Text) + 1
                                    Case 2
                                        e.Item.Cells(17).Text = "Team Head - " & objWorkflow(0).List_TeamHead.TrimEnd(";")
                                    Case 3
                                        e.Item.Cells(17).Text = "Group Head - " & objWorkflow(0).List_GroupHead.TrimEnd(";")
                                    Case 4
                                        e.Item.Cells(17).Text = "Head Off - " & objWorkflow(0).List_HeadOff.TrimEnd(";")

                                End Select

                            End If
                        End Using
                End Select

                'PEP
                If e.Item.Cells(11).Text = "True" Then
                    e.Item.Cells(11).Text = "Yes"
                Else
                    e.Item.Cells(11).Text = "No"
                End If

                'Dim EditButton As LinkButton = CType(e.Item.Cells(GridViewCDDLNP.Columns.Count - 2).FindControl("LnkEdit"), LinkButton)
                'Dim DelButton As LinkButton = CType(e.Item.Cells(GridViewCDDLNP.Columns.Count - 1).FindControl("LnkDelete"), LinkButton)

                ''Approval
                'Select Case e.Item.Cells(16).Text
                '    Case 2, 4, 5
                '        EditButton.Enabled = False
                '        DelButton.Enabled = False

                '    Case 1, 3, "&nbsp;"
                '        EditButton.Enabled = True
                '        DelButton.Enabled = True
                '        Dim StrDelMessage As String = "javascript:return confirm('You are about to delete CDD for Customer: " & e.Item.Cells(7).Text & ". This action cannot be undone. Are you sure want to proceed?')"
                '        DelButton.Attributes.Add("onclick", StrDelMessage)

                'End Select

                'ElseIf e.Item.ItemType = ListItemType.Header Then
                '    PKColor = Nothing
                '    'e.Item.Cells(18).Text = "Action"
                '    'e.Item.Cells(18).ForeColor = Drawing.Color.White
                '    'e.Item.Cells(18).ColumnSpan = 4
                '    'e.Item.Cells(19).Visible = False
                '    'e.Item.Cells(20).Visible = False
                '    'e.Item.Cells(21).Visible = False
                'Else
                '    PKColor = ArrTarget
            End If
        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub GridViewCDDLNP_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridViewCDDLNP.SortCommand
        Dim GridUser As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub GridViewCDDLNP_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridViewCDDLNP.ItemCommand
        Try

            Select Case e.CommandName.ToLower
                Case "detail"

                    Session("CDDLNP.BackPage") = Sahassa.AML.Commonly.SessionCurrentPage
                    TableCDDDetail.Visible = True
                    Session("CustomerInformationDetail.CDDPKId") = e.Item.Cells(1).Text

                    For Each gridRow As DataGridItem In Me.GridViewCDDLNP.Items
                        gridRow.Cells(2).BackColor = Drawing.Color.White
                        gridRow.Cells(4).BackColor = Drawing.Color.White
                        gridRow.Cells(6).BackColor = Drawing.Color.White
                        gridRow.Cells(7).BackColor = Drawing.Color.White
                        gridRow.Cells(8).BackColor = Drawing.Color.White
                        gridRow.Cells(9).BackColor = Drawing.Color.White
                        gridRow.Cells(10).BackColor = Drawing.Color.White
                        gridRow.Cells(11).BackColor = Drawing.Color.White
                        gridRow.Cells(12).BackColor = Drawing.Color.White
                        gridRow.Cells(13).BackColor = Drawing.Color.White
                        gridRow.Cells(14).BackColor = Drawing.Color.White
                        gridRow.Cells(15).BackColor = Drawing.Color.White
                        gridRow.Cells(17).BackColor = Drawing.Color.White
                        gridRow.Cells(18).BackColor = Drawing.Color.White
                        gridRow.Cells(19).BackColor = Drawing.Color.White
                        gridRow.Cells(20).BackColor = Drawing.Color.White
                    Next
                    ''Dim PkId As String = gridRow.Cells(1).Text
                    ''If PkId = e.Item.Cells(1).Text Then
                    e.Item.Cells(2).BackColor = Drawing.Color.RoyalBlue
                    e.Item.Cells(4).BackColor = Drawing.Color.RoyalBlue
                    e.Item.Cells(6).BackColor = Drawing.Color.RoyalBlue
                    e.Item.Cells(7).BackColor = Drawing.Color.RoyalBlue
                    e.Item.Cells(8).BackColor = Drawing.Color.RoyalBlue
                    e.Item.Cells(9).BackColor = Drawing.Color.RoyalBlue
                    e.Item.Cells(10).BackColor = Drawing.Color.RoyalBlue
                    e.Item.Cells(11).BackColor = Drawing.Color.RoyalBlue
                    e.Item.Cells(12).BackColor = Drawing.Color.RoyalBlue
                    e.Item.Cells(13).BackColor = Drawing.Color.RoyalBlue
                    e.Item.Cells(14).BackColor = Drawing.Color.RoyalBlue
                    e.Item.Cells(15).BackColor = Drawing.Color.RoyalBlue
                    e.Item.Cells(17).BackColor = Drawing.Color.RoyalBlue
                    e.Item.Cells(18).BackColor = Drawing.Color.RoyalBlue
                    e.Item.Cells(19).BackColor = Drawing.Color.RoyalBlue
                    e.Item.Cells(20).BackColor = Drawing.Color.RoyalBlue
                    ''End If

                    lblCDDTypeNasabah.Text = Nothing
                    hfCDDTypeNasabah.Value = Nothing
                    lblCustomerTypeNasabah.Text = Nothing
                    hfCustomerTypeNasabah.Value = Nothing
                    lblNameNasabah.Text = Nothing
                    lblRatingPersonalNasabah.Text = Nothing
                    lblRatingGabunganNasabah.Text = Nothing
                    lblIsPEPNasabah.Text = Nothing
                    SetnGetScreeningNasabahID = Nothing

                    lblCDDTypeBO.Text = Nothing
                    hfCDDTypeBO.Value = Nothing
                    lblCustomerTypeBO.Text = Nothing
                    hfCustomerTypeBO.Value = Nothing
                    lblNameBO.Text = Nothing
                    lblRatingPersonalBO.Text = Nothing
                    lblRatingGabunganBO.Text = Nothing
                    lblIsPEPBO.Text = Nothing
                    SetnGetScreeningBOID = Nothing
                    SetnGetFK_ParentRef_ID = Nothing
                    ' Informasi Nasabah
                    Using objCDDLNP As CDDLNPNettier.Entities.VList(Of CDDLNPNettier.Entities.vw_CDDLNP_View) = CDDLNPNettier.Data.DataRepository.vw_CDDLNP_ViewProvider.GetPaged("PK_CDDLNP_ID = " & e.Item.Cells(1).Text, String.Empty, 0, Int32.MaxValue, 0)
                        If objCDDLNP.Count > 0 Then
                            Session("CustomerInformationDetail.CDDPKId") = e.Item.Cells(1).Text
                            lblCDDTypeNasabah.Text = objCDDLNP(0).CDD_Type
                            hfCDDTypeNasabah.Value = objCDDLNP(0).FK_CDD_Type_ID
                            lblCustomerTypeNasabah.Text = objCDDLNP(0).CDD_NasabahType_ID
                            hfCustomerTypeNasabah.Value = objCDDLNP(0).FK_CDD_NasabahType_ID
                            lblNameNasabah.Text = objCDDLNP(0).Nama
                            lblRatingPersonalNasabah.Text = objCDDLNP(0).AMLRiskRatingPersonal
                            lblRatingGabunganNasabah.Text = objCDDLNP(0).AMLRiskRatingGabungan
                            lblIsPEPNasabah.Text = GetYesNo(objCDDLNP(0).IsPEP)
                            SetnGetScreeningNasabahID = objCDDLNP(0).FK_AuditTrail_Potensial_Screening_Customer_ID.GetValueOrDefault(0)

                            Using objAudit As SahassaNettier.Entities.AuditTrail_Potensial_Screening_Customer = SahassaNettier.Data.DataRepository.AuditTrail_Potensial_Screening_CustomerProvider.GetByPK_AuditTrail_Potensial_Screening_Customer_ID(SetnGetScreeningNasabahID)
                                If objAudit Is Nothing Then
                                    lnkSuspectNasabah.Visible = False
                                    lblScreeningNasabah.Visible = True
                                Else
                                    lnkSuspectNasabah.Text = objAudit.SuspectMatch.ToString & " suspect(s)"
                                    lnkSuspectNasabah.Visible = True
                                    lblScreeningNasabah.Visible = False
                                End If
                            End Using

                            For Each objAttachment As CDDLNPNettier.Entities.CDDLNP_Attachment In CDDLNPNettier.Data.DataRepository.CDDLNP_AttachmentProvider.GetPaged("FK_CDDLNP_ID = " & e.Item.Cells(1).Text, String.Empty, 0, Int32.MaxValue, 0)
                                RowAttachmentNasabah.Controls.Add(New LiteralControl(Me.GenerateLink(objAttachment.PK_CDDLNP_AttachmentID, objAttachment.FileName) & "</BR>"))
                            Next

                            'strCreatedBy = objCDDLNP(0).CreatedBy
                            'SetnGetPEPNasabah = objCDDLNP(0).IsPEP
                            lnkCDDNasabah.Visible = True
                            lblCDDNasabah.Visible = False
                        Else
                            lnkCDDNasabah.Visible = False
                            lblCDDNasabah.Visible = True
                        End If
                    End Using
                    'Response.Redirect("CDDLNPApprovalDetail.aspx?ID=" & e.Item.Cells(1).Text)

                    ' Informasi Beneficial Owner
                    Using objCDDLNP As CDDLNPNettier.Entities.VList(Of CDDLNPNettier.Entities.vw_CDDLNP_View) = CDDLNPNettier.Data.DataRepository.vw_CDDLNP_ViewProvider.GetPaged("FK_RefParent_ID = " & e.Item.Cells(1).Text, String.Empty, 0, Int32.MaxValue, 0)
                        If objCDDLNP.Count > 0 Then
                            lblCDDTypeBO.Text = objCDDLNP(0).CDD_Type
                            hfCDDTypeBO.Value = objCDDLNP(0).FK_CDD_Type_ID
                            lblCustomerTypeBO.Text = objCDDLNP(0).CDD_NasabahType_ID
                            hfCustomerTypeBO.Value = objCDDLNP(0).FK_CDD_NasabahType_ID
                            lblNameBO.Text = objCDDLNP(0).Nama
                            lblRatingPersonalBO.Text = objCDDLNP(0).AMLRiskRatingPersonal
                            lblRatingGabunganBO.Text = objCDDLNP(0).AMLRiskRatingGabungan
                            lblIsPEPBO.Text = GetYesNo(objCDDLNP(0).IsPEP)
                            SetnGetScreeningBOID = objCDDLNP(0).FK_AuditTrail_Potensial_Screening_Customer_ID.GetValueOrDefault(0)
                            SetnGetFK_ParentRef_ID = objCDDLNP(0).PK_CDDLNP_ID

                            Using objAudit As SahassaNettier.Entities.AuditTrail_Potensial_Screening_Customer = SahassaNettier.Data.DataRepository.AuditTrail_Potensial_Screening_CustomerProvider.GetByPK_AuditTrail_Potensial_Screening_Customer_ID(SetnGetScreeningBOID)
                                If objAudit Is Nothing Then
                                    lnkSuspectBO.Visible = False
                                    lblScreeningBO.Visible = True
                                Else
                                    lnkSuspectBO.Text = objAudit.SuspectMatch.ToString & " suspect(s)"
                                    lnkSuspectBO.Visible = True
                                    lblScreeningBO.Visible = False
                                End If
                            End Using

                            For Each objAttachment As CDDLNPNettier.Entities.CDDLNP_Attachment In CDDLNPNettier.Data.DataRepository.CDDLNP_AttachmentProvider.GetPaged("FK_CDDLNP_ID = " & Me.SetnGetFK_ParentRef_ID, String.Empty, 0, Int32.MaxValue, 0)
                                RowAttachmentBO.Controls.Add(New LiteralControl(Me.GenerateLink(objAttachment.PK_CDDLNP_AttachmentID, objAttachment.FileName) & "</BR>"))
                            Next

                            'strCreatedBy = objCDDLNP(0).CreatedBy
                            'SetnGetPEPBO = objCDDLNP(0).IsPEP
                            lnkCDDBO.Visible = True
                            lblCDDBO.Visible = False
                        Else
                            lnkCDDBO.Visible = False
                            lblCDDBO.Visible = True
                        End If
                    End Using
                Case "print"
                    Me.Controls.Add(New LiteralControl("<script>window.open('CDDLNPPrint.aspx?CID=" & e.Item.Cells(1).Text & "&TID=" & e.Item.Cells(3).Text & "&NTID=" & e.Item.Cells(5).Text & "','mustunique','scrollbars=1,directories=0,height=600,width=800,location=0,menubar=0,resizable=1,status=0,toolbar=0');</script> "))

                Case "edit"
                    Session("CDDLNPEdit.ReferenceID") = 0
                    Response.Redirect("CDDLNPEdit.aspx?CID=" & e.Item.Cells(1).Text & "&TID=" & e.Item.Cells(3).Text & "&NTID=" & e.Item.Cells(5).Text)

                Case "delete"
                    Using objCDDLNP As New CDDLNPBLL
                        objCDDLNP.CDDLNPDelete(e.Item.Cells(1).Text)
                        ClientScript.RegisterStartupScript(Page.GetType(), "DeleteSuccess" & e.Item.Cells(1).Text, "alert('CDD is deleted.');", False)
                    End Using
                    'Response.Redirect("CDDLNPDelete.aspx?CID=" & e.Item.Cells(1).Text & "&TID=" & e.Item.Cells(3).Text & "&NTID=" & e.Item.Cells(5).Text)

            End Select
        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
        End Try
    End Sub
    Private Function GetYesNo(ByVal value As Boolean) As String
        Select Case value
            Case True : Return "Yes"
            Case False : Return "No"
            Case Else : Return "No"
        End Select
    End Function
    Private Property SetnGetScreeningNasabahID() As Int32
        Get
            Return IIf(Session("CDDLNPApprovalDetail.ScreeningNasabahID") Is Nothing, 0, Session("CDDLNPApprovalDetail.ScreeningNasabahID"))
        End Get
        Set(ByVal Value As Int32)
            Session("CDDLNPApprovalDetail.ScreeningNasabahID") = Value
        End Set
    End Property
    Private Property SetnGetScreeningBOID() As Int32
        Get
            Return IIf(Session("CDDLNPApprovalDetail.ScreeningBOID") Is Nothing, 0, Session("CDDLNPApprovalDetail.ScreeningBOID"))
        End Get
        Set(ByVal Value As Int32)
            Session("CDDLNPApprovalDetail.ScreeningBOID") = Value
        End Set
    End Property
    Private Property SetnGetFK_ParentRef_ID() As Int32
        Get
            Return IIf(Session("CDDLNPApprovalDetail.ParentID") Is Nothing, 0, Session("CDDLNPApprovalDetail.ParentID"))
        End Get
        Set(ByVal Value As Int32)
            Session("CDDLNPApprovalDetail.ParentID") = Value
        End Set
    End Property
    Protected Function GenerateLink(ByVal PK_CDDLNP_AttachmentID As Integer, ByVal Filename As String) As String
        Return String.Format("<a href=""{0}"">{1}</a> ", ResolveUrl("~\CDDLNPAttachmentDownload.aspx?PK_CDDLNP_AttachmentID=" & PK_CDDLNP_AttachmentID), Filename)
    End Function
    Protected Sub lnkSuspectNasabah_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSuspectNasabah.Click
        Try
            SessionCDDLNPAuditTrailId = SetnGetScreeningNasabahID
            Me.Controls.Add(New LiteralControl("<script>popupWindow = window.open('PopUpPotentialCustomerVerificationResult.aspx','mustunique','scrollbars=1,directories=0,height=600,width=800,location=0,menubar=0,resizable=1,status=0,toolbar=0');popupWindow.focus();</script> "))

        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
        End Try
    End Sub
    Protected Sub lnkSuspectBO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSuspectBO.Click
        Try
            SessionCDDLNPAuditTrailId = SetnGetScreeningBOID
            Me.Controls.Add(New LiteralControl("<script>popupWindow = window.open('PopUpPotentialCustomerVerificationResult.aspx','mustunique','scrollbars=1,directories=0,height=600,width=800,location=0,menubar=0,resizable=1,status=0,toolbar=0');popupWindow.focus();</script> "))

        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
        End Try
    End Sub
    Protected Sub lnkCDDNasabah_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCDDNasabah.Click
        Try
            Me.Controls.Add(New LiteralControl("<script>popupWindow = window.open('CDDLNPPrint.aspx?CID=" & Me.Session("CustomerInformationDetail.CDDPKId") & "&TID=" & hfCDDTypeNasabah.Value & "&NTID=" & hfCustomerTypeNasabah.Value & "','mustunique','scrollbars=1,directories=0,height=600,width=800,location=0,menubar=0,resizable=1,status=0,toolbar=0');popupWindow.focus();</script> "))
        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub lnkCDDBO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCDDBO.Click
        Try
            Me.Controls.Add(New LiteralControl("<script>popupWindow = window.open('CDDLNPPrint.aspx?CID=" & Me.SetnGetFK_ParentRef_ID & "&TID=" & hfCDDTypeBO.Value & "&NTID=" & hfCustomerTypeBO.Value & "','mustunique','scrollbars=1,directories=0,height=600,width=800,location=0,menubar=0,resizable=1,status=0,toolbar=0');popupWindow.focus();</script> "))
        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
        End Try
    End Sub
    Private Property PKColor() As ArrayList
        Get
            Return IIf(Session("CustomerInformationDetail.PKColor") Is Nothing, New ArrayList, Session("CustomerInformationDetail.PKColor"))
        End Get
        Set(ByVal value As ArrayList)
            Session("CustomerInformationDetail.PKColor") = value
        End Set
    End Property



    Protected Sub txtSDB_TextChanged(sender As Object, e As System.EventArgs) Handles txtSDB.TextChanged

    End Sub

    Protected Sub ImageButton6_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnBA.Click
        Try
            If CbFilBa.SelectedValue <> "" Then
                If CbFilBa.SelectedIndex = 0 Or CbFilBa.SelectedIndex = 1 Or CbFilBa.SelectedIndex = 2 Or CbFilBa.SelectedIndex = 3 Or CbFilBa.SelectedIndex = 4 Then
                    SearchFilterSafetyBoxCIF = CbFilBa.SelectedValue.Replace("-=Search=-", txtBa.Text.Trim)
                Else
                    If Not Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", txtBa.Text.Trim) Then
                        Throw New Exception("Opening Date Must dd-MMM-yyyy")
                    End If

                    Me.popUploan.Style.Add("display", "")


                    Dim searchdate As Date = Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", txtBa.Text.Trim)
                    SearchFilterSafetyBoxCIF = CbFilBa.SelectedValue.Replace("-=Search=-", searchdate.ToString("yyyy-MM-dd"))
                End If
            Else
                SearchFilterSafetyBoxCIF = CbFilBa.SelectedValue
            End If


            Using objtemp As Data.DataView = AMLBLL.AccountInformationBLL.GetAccountBA(Me.CIFNo).DefaultView
                objtemp.RowFilter = SearchFilterSafetyBoxCIF
                GdViewBA.DataSource = objtemp
                GdViewBA.DataBind()
            End Using



        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Function LoadStakeHolder() As Data.DataTable
        Dim dt As Data.DataTable = New Data.DataTable
        Try
            Dim reader As Data.SqlClient.SqlDataReader
            Dim str As String
            str = "SELECT DISTINCT     BUSINESS_DATE, " & vbCrLf _
                & "       CUST_NO, " & vbCrLf _
                & "       STAKEHOLDER_SEQ_NO, " & vbCrLf _
                & "       STAKEHOLDER_CUST_NO, " & vbCrLf _
                & "       STAKEHOLDER_NAME, " & vbCrLf _
                & "       ID_NUMBER, " & vbCrLf _
                & "       ID_TYPE, " & vbCrLf _
                & "       ALTERNATE_ID_NUMBER, " & vbCrLf _
                & "       ALTERNATE_ID_TYPE, " & vbCrLf _
                & "       LAST_MAINTENANCE_DATE, " & vbCrLf _
                & "       DOB, " & vbCrLf _
                & "       CASE  " & vbCrLf _
                & "            WHEN CUSTOMER_STAKEHOLDER.DESIGNATION IS NULL THEN 'Beneficiary' " & vbCrLf _
                & "            WHEN CUSTOMER_STAKEHOLDER.DESIGNATION = '' THEN 'Other' " & vbCrLf _
                & "            ELSE CUSTOMER_STAKEHOLDER.DESIGNATION + ' - ' + arct.RelatedCustomerType " & vbCrLf _
                & "       END       AS DESIGNATION, " & vbCrLf _
                & "       OWNERSHIP, " & vbCrLf _
                & "       GENDER, " & vbCrLf _
                & "       NATIONALITY " & vbCrLf _
                & "FROM   CUSTOMER_STAKEHOLDER " & vbCrLf _
                & "       LEFT JOIN AML_RelatedCustomer_Type AS arct " & vbCrLf _
                & "            ON  CUSTOMER_STAKEHOLDER.DESIGNATION = arct.Designation Where CUST_NO = " + Me.CIFNo

            Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommand(str)
                reader = DataRepository.Provider.ExecuteReader(objCommand)
                dt.Load(reader)
            End Using

            Return dt

        Catch ex As Exception
            Console.WriteLine(ex.Message)
            Return dt
        End Try

    End Function

    Private Function LoadBeneficialOwner() As Data.DataTable
        Dim dt As Data.DataTable = New Data.DataTable
        Try
            Dim reader As Data.SqlClient.SqlDataReader

            Dim sql As String
            sql = "SELECT BUSINESS_DATE, " & vbCrLf _
    & "       CUST_NO, " & vbCrLf _
    & "       BENEFICIAL_CUST_NO, " & vbCrLf _
    & "       BENEFICIARY_NAME, " & vbCrLf _
    & "         BUSINESS_TYPE +' - '+ isnull(ltrim(rtrim(c.CFSINA)),'') AS BUSINESS_TYPE, " & vbCrLf _
    & "         JOB_TYTLE +' - '+  isnull(ltrim(rtrim(c2.CFCCRD)),'') AS JOB_TYTLE, " & vbCrLf _
    & "       COMPANY_ADDRESS1, " & vbCrLf _
    & "       COMPANY_ADDRESS2, " & vbCrLf _
    & "       COMPANY_ADDRESS3, " & vbCrLf _
    & "       COMPANY_ADDRESS4, " & vbCrLf _
    & "       COMPANY_ADDRESS5, " & vbCrLf _
    & "       COMPANY_ADDRESS6, " & vbCrLf _
    & "       COMPANY_NAME, " & vbCrLf _
    & "       DOB, " & vbCrLf _
    & "       NATIONALITY " & vbCrLf _
    & "FROM   CUSTOMER_BENEFICIAL_OWNER " & vbCrLf _
    & "left JOIN CFSICC c ON CUSTOMER_BENEFICIAL_OWNER.BUSINESS_TYPE=c.CFBUST " & vbCrLf _
    & "LEFT JOIN CFPARG c2 ON c2.CFEJCD=CUSTOMER_BENEFICIAL_OWNER.JOB_TYTLE " & vbCrLf _
    & "WHERE  CUST_NO = '" & Me.CIFNo & "'"
            Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommand(sql)
                reader = DataRepository.Provider.ExecuteReader(objCommand)
                dt.Load(reader)
            End Using

            Return dt

        Catch ex As Exception
            Console.WriteLine(ex.Message)
            Return dt
        End Try

    End Function

    Protected Sub TabStakeHolder_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabStakeHolder.Activate
        Try
            GridStakeholderDetail.DataSource = LoadStakeHolder()
            GridStakeholderDetail.DataBind()

            GridBeneficialOwner.DataSource = LoadBeneficialOwner()
            GridBeneficialOwner.DataBind()

        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    'Private Function LoadCardBillingAddress() As Data.DataTable

    '    Dim dt As Data.DataTable = New Data.DataTable

    '    Try
    '        Dim reader As SqlDataReader
    '        Dim builder As New StringBuilder()

    '        builder.AppendLine("SELECT a.IPNAME AS [NAMA],")
    '        builder.AppendLine("CASE")
    '        builder.AppendLine("WHEN a.IPITP1 <> '' THEN")
    '        builder.AppendLine("a.IPITP1")
    '        builder.AppendLine("ELSE")
    '        builder.AppendLine("a.IPITP2")
    '        builder.AppendLine("END AS [JENIS_ID],")
    '        builder.AppendLine("CASE")
    '        builder.AppendLine("WHEN ISNULL(a.IPOLIC, '') <> '' THEN")
    '        builder.AppendLine("a.IPOLIC")
    '        builder.AppendLine("ELSE")
    '        builder.AppendLine("a.IPNWIC")
    '        builder.AppendLine("END AS [NO_ID],")
    '        builder.AppendLine("b.AXADR1 AS [ALAMAT_1_SESUAI_ID],")
    '        builder.AppendLine("b.AXADR2 AS [ALAMAT_2_SESUAI_ID],")
    '        builder.AppendLine("b.AXADR3 AS [ALAMAT_3_SESUAI_ID],")
    '        builder.AppendLine("b.AXADR4 AS [ALAMAT_4_SESUAI_ID],")
    '        builder.AppendLine("b.AXPNR AS [KODE_POS_SESUAI_ID],")
    '        builder.AppendLine("b.AXCITY AS [KOTA_SESUAI_ID],")
    '        builder.AppendLine("b.AXLSTE AS [KODE_PROVINSI_SESUAI_ID],")
    '        builder.AppendLine("b.AXCNCD AS [KODE_NEGARA_SESUAI_ID],")
    '        builder.AppendLine("a.IPADR1 AS [ALAMAT_1_TERKINI],")
    '        builder.AppendLine("a.IPADR2 AS [ALAMAT_2_TERKINI],")
    '        builder.AppendLine("a.IPADR3 AS [ALAMAT_3_TERKINI],")
    '        builder.AppendLine("a.IPADR4 AS [ALAMAT_4_TERKINI],")
    '        builder.AppendLine("a.IPPNR AS [KODE_POS_TERKINI],")
    '        builder.AppendLine("a.IPCITY AS [KOTA_TERKINI],")
    '        builder.AppendLine("a.IPLSTE AS [KODE_PROVINSI_TERKINI],")
    '        builder.AppendLine("a.IPCNCD AS [KODE_NEGARA_TERKINI],")
    '        builder.AppendLine("a.IPFORT AS [TEMPAT_LAHIR],")
    '        builder.AppendLine("a.IPFDAT AS [TANGGAL_LAHIR],")
    '        builder.AppendLine("c.IPOCCP AS [PEKERJAAN],")
    '        builder.AppendLine("c.IPARBG AS [TEMPAT_BEKERJA],")
    '        builder.AppendLine("c.IPINDS AS [BIDANG_USAHA],")
    '        builder.AppendLine("c.IPPSN1 AS [JABATAN],")
    '        builder.AppendLine("c.IPEAD1 AS [ALAMAT_1_TEMPAT_BEKERJA],")
    '        builder.AppendLine("c.IPEAD2 AS [ALAMAT_2_TEMPAT_BEKERJA],")
    '        builder.AppendLine("c.IPEAD3 AS [ALAMAT_3_TEMPAT_BEKERJA],")
    '        builder.AppendLine("c.IPEAD4 AS [ALAMAT_4_TEMPAT_BEKERJA],")
    '        builder.AppendLine("c.IPEPNR AS [KODE_POS_TEMPAT_BEKERJA],")
    '        builder.AppendLine("c.IPACTY AS [KOTA_TEMPAT_BEKERJA],")
    '        builder.AppendLine("c.IPESTE AS [KODE_PROVINSI_TEMPAT_BEKERJA],")
    '        builder.AppendLine("c.IPECNC AS [KODE_NEGARA_TEMPAT_BEKERJA],")
    '        builder.AppendLine("c.IPTEL AS [NOMOR_TELEPON_TEMPAT_BEKERJA],")
    '        builder.AppendLine("c.IPEXT1 AS [EKSTENSI_TELEPON_KARYAWAN],")
    '        builder.AppendLine("c.IPINCO AS [PENDAPATAN_RATA_RATA],")
    '        builder.AppendLine("a.IPSEX AS [JENIS_KELAMIN],")
    '        builder.AppendLine("a.IPCIVS AS [STATUS_PERKAWINAN]")
    '        builder.AppendLine("FROM WRKINPPF AS a,")
    '        builder.AppendLine("ADDRESPF AS b,")
    '        builder.AppendLine("CUSEMPPF AS c;")

    '        Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommand(builder.ToString())
    '            reader = DataRepository.Provider.ExecuteReader(objCommand)
    '            dt.Load(reader)
    '        End Using

    '        Return dt

    '    Catch ex As Exception
    '        Console.WriteLine(ex.Message)
    '        Return dt
    '    End Try

    'End Function

    Private Sub LoadCardBillingAddress()

        Dim dt As New Data.DataTable()

        Try
            Dim reader As SqlDataReader

            Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommand("SELECT * FROM CustomerDetailCreditCardData WHERE CIFNO = " & CIFNo)
                reader = DataRepository.Provider.ExecuteReader(objCommand)

                If Not reader.HasRows Then Exit Sub

                dt.Load(reader)
            End Using

            ProcessData(dt)

        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Private Sub HideNotFoundErrorMessage()

        'TdNoName.Visible = False
        'TdNoNumber.Visible = False
        tdNoIDNumber.Visible = False
        'TdNoID.Visible = False
        TdNoIDAddress.Visible = False
        'TdNoCurrentAddress.Visible = False
        TdNoPOB.Visible = False
        'TdNoDOB.Visible = False
        'TdNoNation.Visible = False
        'TdNoOcc.Visible = False
        TdNoEmp.Visible = False
        'TdNoBF.Visible = False
        'TdNoPosition.Visible = False
        'TdNoWorkAddress.Visible = False
        TdNoWorkPhone.Visible = False
        'TdNoAvg.Visible = False
        'TdNoSex.Visible = False
        'TdNoMarriage.Visible = False
        'TdNoNPWP.Visible = False
        'TdNoBO.Visible = False
        'TdNoFOC.Visible = False
        'TdNoStakeholder.Visible = False

    End Sub

    Private Function ConvertToDataTable(listParam As List(Of CreditCardDataFormatTable)) As Data.DataTable

        Dim dt As New Data.DataTable()
        Dim fields() As FieldInfo = GetType(CreditCardDataFormatTable).GetFields()
        Dim row As Data.DataRow

        For Each field As FieldInfo In fields
            dt.Columns.Add(field.Name, field.FieldType)
        Next

        For Each data As CreditCardDataFormatTable In listParam
            row = dt.NewRow()

            For Each field As FieldInfo In fields
                row(field.Name) = field.GetValue(data)
            Next

            dt.Rows.Add(row)
        Next

        Return dt

    End Function

    Private Sub LoadCardBillingAddressToTable(dataTableParam As Data.Datatable)

        'GridCardBillingAddressDetail.DataSource = dataTableParam
        'GridCardBillingAddressDetail.DataBind()

        'GridCreditCardName.DataSource = dataTableParam
        'GridCreditCardName.DataBind()

        'GridCreditCardNumber.DataSource = dataTableParam
        'GridCreditCardNumber.DataBind()

        GridCreditCardCategory.DataSource = dataTableParam
        GridCreditCardCategory.DataBind()

        'GridCreditCardID.DataSource = dataTableParam
        'GridCreditCardID.DataBind()

        'GridCreditCardIDBasedAddress.DataSource = dataTableParam
        'GridCreditCardIDBasedAddress.DataBind()

        GridCreditCardAddress.DataSource = dataTableParam
        GridCreditCardAddress.DataBind()

        GridCreditCardPlaceOfBirth.DataSource = dataTableParam
        GridCreditCardPlaceOfBirth.DataBind()

        'GridCreditCardDOB.DataSource = dataTableParam
        'GridCreditCardDOB.DataBind()

        'GridCreditCardNationality.DataSource = dataTableParam
        'GridCreditCardNationality.DataBind()

        'GridCreditCardOccupation.DataSource = dataTableParam
        'GridCreditCardOccupation.DataBind()

        GridCreditCardEmployer.DataSource = dataTableParam
        GridCreditCardEmployer.DataBind()

        'GridCreditCardBusinessField.DataSource = dataTableParam
        'GridCreditCardBusinessField.DataBind()

        'GridCreditCardPosition.DataSource = dataTableParam
        'GridCreditCardPosition.DataBind()

        'GridCreditCardWorkplaceAddress.DataSource = dataTableParam
        'GridCreditCardWorkplaceAddress.DataBind()

        GridCreditCardWorkplacePhoneNumber.DataSource = dataTableParam
        GridCreditCardWorkplacePhoneNumber.DataBind()

        'GridCreditCardAverageIncoming.DataSource = dataTableParam
        'GridCreditCardAverageIncoming.DataBind()

        'GridCreditCardSex.DataSource = dataTableParam
        'GridCreditCardSex.DataBind()

        'GridCreditCardMarriageStatus.DataSource = dataTableParam
        'GridCreditCardMarriageStatus.DataBind()

        'GridCreditCardNPWP.DataSource = dataTableParam
        'GridCreditCardNPWP.DataBind()

        'GridCreditCardBeneficialOwner.DataSource = dataTableParam
        'GridCreditCardBeneficialOwner.DataBind()

        'GridCreditCardFormOfCompany.DataSource = dataTableParam
        'GridCreditCardFormOfCompany.DataBind()

        'GridCreditCardStakeholder.DataSource = dataTableParam
        'GridCreditCardStakeholder.DataBind()

    End Sub

    Private Sub ProcessData(dataTableParam As Data.DataTable)

        Dim builder As New StringBuilder()
        Dim lstData As New List(Of CreditCardDataFormatTable)
        Dim newData As CreditCardDataFormatTable
        Dim newDataTable As New Data.DataTable()
        Dim firstData As String
        Dim secondData As String

        HideNotFoundErrorMessage()

        For Each row As Data.DataRow In dataTableParam.Rows

            newData = New CreditCardDataFormatTable()

            newData.NO_KARTU_KREDIT = row("NO_KARTU_KREDIT").ToString()
            newData.ACCOUNT_NO = row("ACCOUNT_NO").ToString()
            newData.CIFNO = row("CIFNO").ToString()
            newData.NAMA = row("NAMA").ToString()

            'builder.Append(row("JENIS_ID_1").ToString())

            'If Not String.IsNullOrEmpty(row("JENIS_ID_2").ToString().Trim()) Then
            '    builder.Append(" / ")
            '    builder.Append(row("JENIS_ID_2").ToString())
            'End If

            newData.JENIS_ID_1 = row("JENIS_ID_1").ToString()
            newData.JENIS_ID_2 = row("JENIS_ID_2").ToString()

            '===================================================================

            'Bersihkan StringBuilder dikarenakan tidak ada method .Clear() (minimal .NET versi 4 keatas)

            'Referensi:
            'https://stackoverflow.com/questions/2866747/what-is-the-appropriate-way-to-set-a-vb-stringbuilder-to-an-empty-string
            'http://burnignorance.com/asp-net-developer-tips/a-better-way-to-clear-a-stringbuilder/
            'https://docs.microsoft.com/en-us/dotnet/api/system.text.stringbuilder.clear?redirectedfrom=MSDN&view=netframework-4.7.2#System_Text_StringBuilder_Clear
            'builder.Length = 0

            'builder.Append(row("NO_ID_1").ToString())

            'If Not String.IsNullOrEmpty(row("NO_ID_2").ToString().Trim()) Then
            '    builder.Append(" / ")
            '    builder.Append(row("NO_ID_2").ToString())
            'End If

            'firstData = row("NO_ID_1").ToString()
            'secondData = row("NO_ID_2").ToString()

            'newData.NO_ID_1 = firstData
            'newData.NO_ID_2 = secondData

            newData.NO_ID_1 = row("NO_ID_1").ToString()
            newData.NO_ID_2 = row("NO_ID_2").ToString()

            'If String.IsNullOrEmpty(firstData) AndAlso String.IsNullOrEmpty(secondData) Then
            '    TdNoID.Visible = True
            'End If

            '===================================================================

            builder.Length = 0

            builder.AppendLine(row("ALAMAT_1_SESUAI_ID").ToString())
            builder.AppendLine(row("ALAMAT_2_SESUAI_ID").ToString())
            builder.AppendLine(row("ALAMAT_3_SESUAI_ID").ToString())
            builder.AppendLine(row("ALAMAT_4_SESUAI_ID").ToString())
            builder.AppendLine(row("KOTA_SESUAI_ID").ToString())
            builder.AppendLine(row("KODE_PROVINSI_SESUAI_ID").ToString())
            builder.Append(row("KODE_NEGARA_SESUAI_ID").ToString())
            builder.Append(" ")
            builder.Append(row("KODE_POS_SESUAI_ID").ToString())

            newData.ALAMAT_SESUAI_ID = builder.ToString()
            'newData.KODE_POS_SESUAI_ID = row("KODE_POS_SESUAI_ID").ToString()

            'If String.IsNullOrEmpty(builder.ToString()) Then
            '    TdNoIDAddress.Visible = True
            'End If

            '===================================================================

            builder.Length = 0

            builder.AppendLine(row("ALAMAT_1_TERKINI").ToString())
            builder.AppendLine(row("ALAMAT_2_TERKINI").ToString())
            builder.AppendLine(row("ALAMAT_3_TERKINI").ToString())
            builder.AppendLine(row("ALAMAT_4_TERKINI").ToString())
            builder.AppendLine(row("KOTA_TERKINI").ToString())
            builder.AppendLine(row("KODE_PROVINSI_TERKINI").ToString())
            builder.Append(row("KODE_NEGARA_TERKINI").ToString())
            builder.Append(" ")
            builder.Append(row("KODE_POS_TERKINI").ToString())

            newData.ALAMAT_TERKINI = builder.ToString()
            'newData.ALAMAT_TERKINI = row("KODE_POS_TERKINI").ToString()

            'If String.IsNullOrEmpty(builder.ToString()) Then
            '    TdNoCurrentAddress.Visible = True
            'End If

            '===================================================================

            newData.TEMPAT_LAHIR = row("TEMPAT_LAHIR").ToString()

            If String.IsNullOrEmpty(builder.ToString()) Then
                TdNoPOB.Visible = True
            End If

            'Referensi : https://stackoverflow.com/questions/6747220/string-to-datetime-conversion-as-per-specified-format
            'newData.TANGGAL_LAHIR = DateTime.ParseExact(row("TANGGAL_LAHIR").ToString(), "yymmdd", CultureInfo.InvariantCulture).ToString("dd-mm-yyyy")

            'If String.IsNullOrEmpty(newData.TANGGAL_LAHIR) Then
            '    TdNoDOB.Visible = True
            'End If

            'newData.KEWARGANEGARAAN = row("KEWARGANEGARAAN").ToString()

            'If String.IsNullOrEmpty(newData.KEWARGANEGARAAN) Then
            '    TdNoNation.Visible = True
            'End If

            'newData.PEKERJAAN = row("PEKERJAAN").ToString()

            'If String.IsNullOrEmpty(newData.PEKERJAAN) Then
            '    TdNoOcc.Visible = True
            'End If

            newData.TEMPAT_BEKERJA = row("TEMPAT_BEKERJA").ToString()

            If String.IsNullOrEmpty(newData.TEMPAT_BEKERJA) Then
                TdNoEmp.Visible = True
            End If

            newData.BIDANG_USAHA = row("BIDANG_USAHA").ToString()

            'If String.IsNullOrEmpty(newData.BIDANG_USAHA) Then
            '    TdNoBF.Visible = True
            'End If

            'newData.JABATAN = row("JABATAN").ToString()

            'If String.IsNullOrEmpty(newData.JABATAN) Then
            '    TdNoPosition.Visible = True
            'End If

            '===================================================================

            builder.Length = 0

            builder.AppendLine(row("ALAMAT_1_TEMPAT_BEKERJA").ToString())
            builder.AppendLine(row("ALAMAT_2_TEMPAT_BEKERJA").ToString())
            builder.AppendLine(row("ALAMAT_3_TEMPAT_BEKERJA").ToString())
            builder.AppendLine(row("ALAMAT_4_TEMPAT_BEKERJA").ToString())
            builder.AppendLine(row("KOTA_TEMPAT_BEKERJA").ToString())
            builder.AppendLine(row("KODE_PROVINSI_TEMPAT_BEKERJA").ToString())
            builder.Append(row("KODE_NEGARA_TEMPAT_BEKERJA").ToString())
            'builder.Append(" ")
            'builder.Append(row("KODE_POS_TEMPAT_BEKERJA").ToString())

            newData.ALAMAT_TEMPAT_BEKERJA = builder.ToString()
            'newData.KODE_POS_TEMPAT_BEKERJA = row("KODE_POS_TEMPAT_BEKERJA").ToString()

            'If String.IsNullOrEmpty(builder.ToString()) Then
            '    TdNoWorkAddress.Visible = True
            'End If

            '====================================================================

            'builder.Length = 0

            'builder.Append(row("NOMOR_TELEPON_TEMPAT_BEKERJA").ToString())

            'If Not String.IsNullOrEmpty(row("EKSTENSI_TELEPON_KARYAWAN").ToString().Trim()) Then
            '    builder.Append(" EXT : ")
            '    builder.Append(row("EKSTENSI_TELEPON_KARYAWAN").ToString())
            'End If

            firstData = row("NOMOR_TELEPON_TEMPAT_BEKERJA").ToString()
            secondData = row("EKSTENSI_TELEPON_KARYAWAN").ToString()

            newData.NOMOR_TELEPON_TEMPAT_BEKERJA = firstData
            newData.EKSTENSI_TELEPON_KARYAWAN = secondData

            If String.IsNullOrEmpty(firstData) AndAlso String.IsNullOrEmpty(secondData) Then
                TdNoWorkPhone.Visible = True
            End If

            '====================================================================

            newData.PENDAPATAN_RATA_RATA = row("PENDAPATAN_RATA_RATA").ToString()
            newData.JABATAN = row("JABATAN").ToString()

            'If String.IsNullOrEmpty(newData.PENDAPATAN_RATA_RATA) Then
            '    TdNoAvg.Visible = True
            'End If

            newData.JENIS_KELAMIN = row("JENIS_KELAMIN").ToString()

            'If String.IsNullOrEmpty(newData.JENIS_KELAMIN) Then
            '    TdNoSex.Visible = True
            'End If

            newData.STATUS_PERKAWINAN = row("STATUS_PERKAWINAN").ToString()

            'If String.IsNullOrEmpty(newData.STATUS_PERKAWINAN) Then
            '    TdNoMarriage.Visible = True
            'End If

            'newData.NPWP = row("NPWP").ToString()

            'If String.IsNullOrEmpty(newData.NPWP) Then
            '    TdNoNPWP.Visible = True
            'End If

            'newData.BENEFICIAL_OWNER = row("BENEFICIAL_OWNER").ToString()

            'If String.IsNullOrEmpty(newData.BENEFICIAL_OWNER) Then
            '    TdNoBO.Visible = True
            'End If

            'newData.BENTUK_BADAN = row("BENTUK_BADAN").ToString()

            'If String.IsNullOrEmpty(newData.BENTUK_BADAN) Then
            '    TdNoFOC.Visible = True
            'End If

            'newData.STAKEHOLDER = row("STAKEHOLDER").ToString()

            'If String.IsNullOrEmpty(newData.STAKEHOLDER) Then
            '    TdNoStakeholder.Visible = True
            'End If

            lstData.Add(newData)
        Next

        newDataTable = ConvertToDataTable(lstData)

        LoadCardBillingAddressToTable(newDataTable)
        
    End Sub

    'Private Sub LoadCardBillingAddressIntoTable(dataTableParam As Data.DataTable)

    '    Dim row As Data.DataRow = dataTableParam.Rows(0)
    '    Dim builder As New StringBuilder()

    '    builder.Append(row("JENIS_ID_1").ToString()) 

    '    If Not String.IsNullOrEmpty(row("JENIS_ID_2").ToString().Trim()) Then
    '        builder.Append(" / ")
    '        builder.Append(row("JENIS_ID_2").ToString())
    '    End If

    '    TdIDType.InnerText = builder.ToString()

    '    '===================================================================

    '    'Bersihkan StringBuilder dikarenakan tidak ada method .Clear() (minimal .NET versi 4 keatas)
    '    '
    '    'Referensi :
    '    'https://stackoverflow.com/questions/2866747/what-is-the-appropriate-way-to-set-a-vb-stringbuilder-to-an-empty-string
    '    'http://burnignorance.com/asp-net-developer-tips/a-better-way-to-clear-a-stringbuilder/
    '    'https://docs.microsoft.com/en-us/dotnet/api/system.text.stringbuilder.clear?redirectedfrom=MSDN&view=netframework-4.7.2#System_Text_StringBuilder_Clear
    '    builder.Length = 0

    '    builder.Append(row("NO_ID_1").ToString())

    '    If Not String.IsNullOrEmpty(row("NO_ID_2").ToString().Trim()) Then
    '        builder.Append(" / ")
    '        builder.Append(row("NO_ID_2").ToString())
    '    End If

    '    TdIDNo.InnerText = builder.ToString()

    '    '===================================================================

    '    builder.Length = 0

    '    builder.AppendLine(row("ALAMAT_1_SESUAI_ID").ToString())
    '    builder.AppendLine(row("ALAMAT_2_SESUAI_ID").ToString())
    '    builder.AppendLine(row("ALAMAT_3_SESUAI_ID").ToString())
    '    builder.AppendLine(row("ALAMAT_4_SESUAI_ID").ToString())
    '    builder.AppendLine(row("KOTA_SESUAI_ID").ToString())
    '    builder.AppendLine(row("KODE_PROVINSI_SESUAI_ID").ToString())
    '    builder.Append(row("KODE_NEGARA_SESUAI_ID").ToString())
    '    builder.Append(" ")
    '    builder.Append(row("KODE_POS_SESUAI_ID").ToString())

    '    TdIDBasedAddress.InnerText = builder.ToString()

    '    '===================================================================

    '    builder.Length = 0

    '    builder.AppendLine(row("ALAMAT_1_TERKINI").ToString())
    '    builder.AppendLine(row("ALAMAT_2_TERKINI").ToString())
    '    builder.AppendLine(row("ALAMAT_3_TERKINI").ToString())
    '    builder.AppendLine(row("ALAMAT_4_TERKINI").ToString())
    '    builder.AppendLine(row("KOTA_TERKINI").ToString())
    '    builder.AppendLine(row("KODE_PROVINSI_TERKINI").ToString())
    '    builder.Append(row("KODE_NEGARA_TERKINI").ToString())
    '    builder.Append(" ")
    '    builder.Append(row("KODE_POS_TERKINI").ToString())

    '    TdCurrentAddress.InnerText = builder.ToString()

    '    '===================================================================

    '    TdBirthPlace.InnerText = row("TEMPAT_LAHIR").ToString()

    '    'Referensi : https://stackoverflow.com/questions/6747220/string-to-datetime-conversion-as-per-specified-format
    '    TdDOB.InnerText = DateTime.ParseExact(row("TANGGAL_LAHIR").ToString(), "yymmdd", CultureInfo.InvariantCulture).ToString("dd-mm-yyyy")
    '    TdNationality.InnerText = row("KEWARGANEGARAAN").ToString()
    '    TdOccupation.InnerText = row("PEKERJAAN").ToString()
    '    TdNationality.InnerText = row("KEWARGANEGARAAN").ToString()
    '    TdWorkplace.InnerText = row("TEMPAT_BEKERJA").ToString()
    '    TdBusinessField.InnerText = row("BIDANG_USAHA").ToString()
    '    TdPosition.InnerText = row("JABATAN").ToString()

    '    '===================================================================

    '    builder.Length = 0

    '    builder.AppendLine(row("ALAMAT_1_TEMPAT_BEKERJA").ToString())
    '    builder.AppendLine(row("ALAMAT_2_TEMPAT_BEKERJA").ToString())
    '    builder.AppendLine(row("ALAMAT_3_TEMPAT_BEKERJA").ToString())
    '    builder.AppendLine(row("ALAMAT_4_TEMPAT_BEKERJA").ToString())
    '    builder.AppendLine(row("KOTA_TEMPAT_BEKERJA").ToString())
    '    builder.AppendLine(row("KODE_PROVINSI_TEMPAT_BEKERJA").ToString())
    '    builder.Append(row("KODE_NEGARA_TEMPAT_BEKERJA").ToString())
    '    builder.Append(" ")
    '    builder.Append(row("KODE_POS_TEMPAT_BEKERJA").ToString())

    '    TdWorkplaceAddress.InnerText = builder.ToString()

    '    '====================================================================

    '    builder.Length = 0

    '    builder.Append(row("NOMOR_TELEPON_TEMPAT_BEKERJA").ToString())

    '    If Not String.IsNullOrEmpty(row("EKSTENSI_TELEPON_KARYAWAN").ToString().Trim()) Then
    '        builder.Append(" EXT : ")
    '        builder.Append(row("EKSTENSI_TELEPON_KARYAWAN").ToString())
    '    End If

    '    TdWorkplacePhoneNumber.InnerText = builder.ToString()

    '    '====================================================================

    '    TdAverageIncome.InnerText = row("PENDAPATAN_RATA_RATA").ToString()
    '    TdSex.InnerText = row("JENIS_KELAMIN").ToString()
    '    TdMarriageStatus.InnerText = row("STATUS_PERKAWINAN").ToString()
    '    TdNPWP.InnerText = row("NPWP").ToString()
    '    TdBeneficialOwner.InnerText = row("BENEFICIAL_OWNER").ToString()
    '    TdFormBusinessEntity.InnerText = row("BENTUK_BADAN").ToString()
    '    TdStakeholder.InnerText = row("STAKEHOLDER").ToString()

    'End Sub

    'Protected Sub TabCardBillingAddress_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabCardBillingAddress.Activate
    '    Try
    '        'GridCardBillingAddressDetail.DataSource = LoadCardBillingAddress()
    '        'GridCardBillingAddressDetail.DataBind()

    '        LoadCardBillingAddress()

    '    Catch ex As Exception
    '        Me.CValPageError.IsValid = False
    '        Me.CValPageError.ErrorMessage = ex.Message
    '    End Try
    'End Sub


    '========ACTIVATE TAB TO LOAD DATA========

    'Protected Sub TabCardName_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabCardName.Activate
    '    Try

    '        LoadCardBillingAddress()

    '    Catch ex As Exception
    '        Me.CValPageError.IsValid = False
    '        Me.CValPageError.ErrorMessage = ex.Message
    '    End Try
    'End Sub

    'Protected Sub TabCardNumber_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabCardNumber.Activate
    '    Try

    '        LoadCardBillingAddress()

    '    Catch ex As Exception
    '        Me.CValPageError.IsValid = False
    '        Me.CValPageError.ErrorMessage = ex.Message
    '    End Try
    'End Sub

    Protected Sub TabCardIDNumber_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabCardIDNumber.Activate
        Try

            LoadCardBillingAddress()

        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    'Protected Sub TabCardID_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabCardID.Activate
    '    Try

    '        LoadCardBillingAddress()

    '    Catch ex As Exception
    '        Me.CValPageError.IsValid = False
    '        Me.CValPageError.ErrorMessage = ex.Message
    '    End Try
    'End Sub

    Protected Sub TabCardAddress_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabCardAddress.Activate
        Try

            LoadCardBillingAddress()

        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    'Protected Sub TabCardCurrentAddress_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabCardCurrentAddress.Activate
    '    Try

    '        LoadCardBillingAddress()

    '    Catch ex As Exception
    '        Me.CValPageError.IsValid = False
    '        Me.CValPageError.ErrorMessage = ex.Message
    '    End Try
    'End Sub

    Protected Sub TabCardGeneral_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabCardGeneral.Activate
        Try

            LoadCardBillingAddress()

        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    'Protected Sub TabCardDOB_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabCardDOB.Activate
    '    Try

    '        LoadCardBillingAddress()

    '    Catch ex As Exception
    '        Me.CValPageError.IsValid = False
    '        Me.CValPageError.ErrorMessage = ex.Message
    '    End Try
    'End Sub

    'Protected Sub TabCardNation_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabCardNation.Activate
    '    Try

    '        LoadCardBillingAddress()

    '    Catch ex As Exception
    '        Me.CValPageError.IsValid = False
    '        Me.CValPageError.ErrorMessage = ex.Message
    '    End Try
    'End Sub

    'Protected Sub TabCardOcc_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabCardOcc.Activate
    '    Try

    '        LoadCardBillingAddress()

    '    Catch ex As Exception
    '        Me.CValPageError.IsValid = False
    '        Me.CValPageError.ErrorMessage = ex.Message
    '    End Try
    'End Sub

    Protected Sub TabCardWorkplace_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabCardWorkplace.Activate
        Try

            LoadCardBillingAddress()

        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    'Protected Sub TabCardBF_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabCardBF.Activate
    '    Try

    '        LoadCardBillingAddress()

    '    Catch ex As Exception
    '        Me.CValPageError.IsValid = False
    '        Me.CValPageError.ErrorMessage = ex.Message
    '    End Try
    'End Sub

    'Protected Sub TabCardPosition_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabCardPosition.Activate
    '    Try

    '        LoadCardBillingAddress()

    '    Catch ex As Exception
    '        Me.CValPageError.IsValid = False
    '        Me.CValPageError.ErrorMessage = ex.Message
    '    End Try
    'End Sub

    'Protected Sub TabCardWorkAddress_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabCardWorkAddress.Activate
    '    Try

    '        LoadCardBillingAddress()

    '    Catch ex As Exception
    '        Me.CValPageError.IsValid = False
    '        Me.CValPageError.ErrorMessage = ex.Message
    '    End Try
    'End Sub

    Protected Sub TabCardWorkPhone_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabCardWorkPhone.Activate
        Try

            LoadCardBillingAddress()

        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    'Protected Sub TabCardAvg_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabCardAvg.Activate
    '    Try

    '        LoadCardBillingAddress()

    '    Catch ex As Exception
    '        Me.CValPageError.IsValid = False
    '        Me.CValPageError.ErrorMessage = ex.Message
    '    End Try
    'End Sub

    'Protected Sub TabCardSex_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabCardSex.Activate
    '    Try

    '        LoadCardBillingAddress()

    '    Catch ex As Exception
    '        Me.CValPageError.IsValid = False
    '        Me.CValPageError.ErrorMessage = ex.Message
    '    End Try
    'End Sub

    'Protected Sub TabCardMarriage_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabCardMarriage.Activate
    '    Try

    '        LoadCardBillingAddress()

    '    Catch ex As Exception
    '        Me.CValPageError.IsValid = False
    '        Me.CValPageError.ErrorMessage = ex.Message
    '    End Try
    'End Sub

    'Protected Sub TabCardNPWP_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabCardNPWP.Activate
    '    Try

    '        LoadCardBillingAddress()

    '    Catch ex As Exception
    '        Me.CValPageError.IsValid = False
    '        Me.CValPageError.ErrorMessage = ex.Message
    '    End Try
    'End Sub

    'Protected Sub TabCardBO_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabCardBO.Activate
    '    Try

    '        LoadCardBillingAddress()

    '    Catch ex As Exception
    '        Me.CValPageError.IsValid = False
    '        Me.CValPageError.ErrorMessage = ex.Message
    '    End Try
    'End Sub

    'Protected Sub TabCardFOC_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabCardFOC.Activate
    '    Try

    '        LoadCardBillingAddress()

    '    Catch ex As Exception
    '        Me.CValPageError.IsValid = False
    '        Me.CValPageError.ErrorMessage = ex.Message
    '    End Try
    'End Sub

    'Protected Sub TabCardStakeholder_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabCardStakeholder.Activate
    '    Try

    '        LoadCardBillingAddress()

    '    Catch ex As Exception
    '        Me.CValPageError.IsValid = False
    '        Me.CValPageError.ErrorMessage = ex.Message
    '    End Try
    'End Sub
    
    Private Function LoadRisk() As Data.DataTable
        Dim dt As Data.DataTable = New Data.DataTable
        Try
            Dim reader As Data.SqlClient.SqlDataReader
            Dim str As String
            str = "SELECT RiskFactFinalResult.totalrisk, " & vbCrLf _
            & "       RiskFactFinalResult.amlrisk, " & vbCrLf _
            & "       RiskFactDetail.PK_RiskFactDetail_ID, " & vbCrLf _
            & "       RiskFactDetail.RiskFactDetail, " & vbCrLf _
            & "       RiskFactDetail.RiskFactDetailValue, " & vbCrLf _
            & "       RiskFactDetail.RiskFactScore " & vbCrLf _
            & "FROM   RiskFactFinalResult " & vbCrLf _
            & "       INNER JOIN RiskFactDetail " & vbCrLf _
            & "            ON  RiskFactFinalResult.cifno = RiskFactDetail.CIFNo " & vbCrLf _
            & "WHERE  RiskFactDetail.CIFNo = '" + Me.CIFNo + "'"
            Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommand(str)
                reader = DataRepository.Provider.ExecuteReader(objCommand)
                dt.Load(reader)
            End Using

            For Each item As Data.DataRow In dt.Rows
                lblCurrentScore.Text = item("totalrisk") & " - " & item("amlrisk")
                If item("amlrisk") = "High" Then
                    lblCurrentScore.ForeColor = Drawing.Color.Red
                ElseIf item("amlrisk") = "Medium" Then
                    lblCurrentScore.ForeColor = Drawing.Color.Yellow
                Else
                    lblCurrentScore.ForeColor = Drawing.Color.Green
                End If
                Exit For
            Next

            Return dt
        Catch ex As Exception
            Console.WriteLine(ex.Message)
            Return dt
        End Try
    End Function

    Private Function LoadRiskHistory() As Data.DataTable
        Dim dt As Data.DataTable = New Data.DataTable
        Try
            Dim reader As Data.SqlClient.SqlDataReader
            Dim str As String
            str = "SELECT TOP 5  RiskFactFinalHistory.PK_RiskFactFinalHistory_ID, " & vbCrLf _
            & "       RiskFactFinalHistory.RiskValue, RiskFactFinalHistory.CIFNo," & vbCrLf _
            & "       RiskFactFinalHistory.AMLRisk, " & vbCrLf _
            & "       RiskFactDetailHistory.RiskFactDetail, " & vbCrLf _
            & "       RiskFactDetailHistory.RiskFactDetailValue, " & vbCrLf _
            & "       RiskFactDetailHistory.RiskFactScore, " & vbCrLf _
            & "       RiskFactDetailHistory.CreatedDate " & vbCrLf _
            & "FROM   RiskFactFinalHistory " & vbCrLf _
            & "       INNER JOIN RiskFactDetailHistory " & vbCrLf _
            & "            ON  RiskFactFinalHistory.PK_RiskFactFinalHistory_ID =  " & vbCrLf _
            & "                RiskFactDetailHistory.PK_RiskFactFinalHistory_ID WHERE RiskFactFinalHistory.CIFNo = '" + Me.CIFNo + "' ORDER BY RiskFactFinalHistory.CreatedDate Desc"
            Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommand(str)
                reader = DataRepository.Provider.ExecuteReader(objCommand)
                dt.Load(reader)
            End Using

            Return dt
        Catch ex As Exception
            Console.WriteLine(ex.Message)
            Return dt
        End Try
    End Function
    Protected Sub TabRiskRatingHistory_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabRiskRatingHistory.Activate
        Try

            GrdRiskCurrentRatingDetail.DataSource = LoadRisk()
            GrdRiskCurrentRatingDetail.DataBind()

            Dim count As Long = 0
            Dim pk_data As Long = 0

            Dim dataSource1 As Data.DataTable = LoadRiskHistory().Clone

            Dim dataSource2 As Data.DataTable = LoadRiskHistory().Clone

            Dim dataSource3 As Data.DataTable = LoadRiskHistory().Clone

            Dim dataSource4 As Data.DataTable = LoadRiskHistory().Clone

            Dim dataSource5 As Data.DataTable = LoadRiskHistory().Clone

            For Each item As Data.DataRow In LoadRiskHistory().Rows
                If pk_data = 0 Or pk_data <> item("PK_RiskFactFinalHistory_ID") Then
                    pk_data = item("PK_RiskFactFinalHistory_ID")
                    count += 1
                End If
                If count = 1 Then
                    trHeader1.Visible = True
                    trDetail1.Visible = True

                    lblPrevious1.Text = item("RiskValue") & " - " & item("AMLRisk")

                    dataSource1.ImportRow(item)

                    GrdRiskPreviousRatingHistory1.DataSource = dataSource1
                    GrdRiskPreviousRatingHistory1.DataBind()
                ElseIf count = 2 Then
                    trHeader2.Visible = True
                    trDetail2.Visible = True

                    lblPrevious2.Text = item("RiskValue") & " - " & item("AMLRisk")

                    dataSource2.ImportRow(item)

                    GrdRiskPreviousRatingHistory2.DataSource = dataSource2
                    GrdRiskPreviousRatingHistory2.DataBind()
                ElseIf count = 3 Then
                    trHeader3.Visible = True
                    trDetail3.Visible = True

                    lblPrevious3.Text = item("RiskValue") & " - " & item("AMLRisk")

                    dataSource3.ImportRow(item)

                    GrdRiskPreviousRatingHistory3.DataSource = dataSource3
                    GrdRiskPreviousRatingHistory3.DataBind()
                ElseIf count = 4 Then
                    trHeader4.Visible = True
                    trDetail4.Visible = True

                    lblPrevious4.Text = item("RiskValue") & " - " & item("AMLRisk")

                    dataSource4.ImportRow(item)

                    GrdRiskPreviousRatingHistory4.DataSource = dataSource4
                    GrdRiskPreviousRatingHistory4.DataBind()
                ElseIf count = 5 Then
                    trHeader5.Visible = True
                    trDetail5.Visible = True

                    lblPrevious5.Text = item("RiskValue") & " - " & item("AMLRisk")

                    dataSource5.ImportRow(item)

                    GrdRiskPreviousRatingHistory5.DataSource = dataSource5
                    GrdRiskPreviousRatingHistory5.DataBind()

                End If

            Next





        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try

    End Sub

    Private Class CreditCardDataFormatTable

        Public NO_KARTU_KREDIT As String
        Public ACCOUNT_NO As String
        Public CIFNO As String
        Public NAMA As String
        Public JENIS_ID_1 As String
        Public JENIS_ID_2 As String
        Public NO_ID_1 As String
        Public NO_ID_2 As String
        Public ALAMAT_SESUAI_ID As String
        'Public KODE_POS_SESUAI_ID As String
        Public ALAMAT_TERKINI As String
        'Public KODE_POS_TERKINI As String
        Public TEMPAT_LAHIR As String
        Public TANGGAL_LAHIR As String
        Public PEKERJAAN As String
        Public KEWARGANEGARAAN As String
        Public TEMPAT_BEKERJA As String
        Public BIDANG_USAHA As String
        Public JABATAN As String
        Public ALAMAT_TEMPAT_BEKERJA As String
        'Public KODE_POS_TEMPAT_BEKERJA As String
        Public NOMOR_TELEPON_TEMPAT_BEKERJA As String
        Public EKSTENSI_TELEPON_KARYAWAN As String
        Public PENDAPATAN_RATA_RATA As String
        Public JENIS_KELAMIN As String
        Public STATUS_PERKAWINAN As String
        Public NPWP As String
        Public BENEFICIAL_OWNER As String
        Public BENTUK_BADAN As String
        Public STAKEHOLDER As String

    End Class

End Class


