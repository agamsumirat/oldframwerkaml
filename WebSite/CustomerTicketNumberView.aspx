<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="CustomerTicketNumberView.aspx.vb" Inherits="CustomerTicketNumberView" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">

    <script src="Script/popcalendar.js"></script>

    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td>
                <img src="Images/blank.gif" width="5" height="1" /></td>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td width="99%" bgcolor="#FFFFFF">
                            <img src="Images/blank.gif" width="1" height="1" /></td>
                        <td bgcolor="#FFFFFF">
                            <img src="Images/blank.gif" width="1" height="1" /></td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="Images/blank.gif" width="5" height="1" /></td>
        </tr>
        <tr>
            <td>
            </td>
            <td id="tdcontent" height="99%" valign="top" bgcolor="#FFFFFF">
                <div >
                    <table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td>
                                <img src="Images/blank.gif" width="10" height="100%" /></td>
                            <td class="divcontentinside" bgcolor="#FFFFFF">
                                <ajax:AjaxPanel ID="AjaxPanel1" runat="server">
                                    <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
                                        style="border-top-style: none; border-right-style: none; border-left-style: none;
                                        border-bottom-style: none" width="100%">
                                        <tr>
                                            <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                                                border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                <img src="Images/dot_title.gif" width="17" height="17" />
                                                <strong>
                                                    <asp:Label ID="Label1" runat="server" Text="Customer Ticket Number"></asp:Label>
                                                </strong>
                                                <hr />
                                            </td>
                                        </tr>
                                    </table>
                                    <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                        bgcolor="#dddddd" border="2">
                                        <tr>
                                            <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                style="height: 6px">
                                                <table cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td class="formtext">
                                                            <asp:Label ID="Label6" runat="server" Text="Search Criteria" Font-Bold="True"></asp:Label>
                                                            &nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr id="SearchCriteria">
                                            <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                    <tr >
                                                        <td nowrap style="background-color: #FFF7E6; width: 15%">
                                                            <asp:Label ID="Label10" runat="server" Text="CIF Number"></asp:Label>
                                                        </td>
                                                        <td nowrap style="background-color: #ffffff; width: 35%">
                                                            <asp:TextBox ID="TxtCIFNumber" runat="server" CssClass="searcheditbox" 
                                                                MaxLength="50" Width="125px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr >
                                                        <td nowrap style="background-color: #FFF7E6; width: 15%">
                                                            <asp:Label ID="Label11" runat="server" Text="Account Number"></asp:Label>
                                                        </td>
                                                        <td nowrap style="background-color: #ffffff; width: 35%">
                                                            <asp:TextBox ID="TxtAccountNumber" runat="server" CssClass="searcheditbox" 
                                                                MaxLength="50" Width="125px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap style="background-color: #FFF7E6; width: 15%">
                                                            <asp:Label ID="Label7" runat="server" Text="Name"></asp:Label>
                                                        </td>
                                                        <td nowrap style="background-color: #ffffff; width: 35%">
                                                            <asp:TextBox ID="TxtName" runat="server" CssClass="searcheditbox" 
                                                                MaxLength="50" Width="250px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap style="width: 15%; background-color: #FFF7E6;" valign="top">
                                                            <asp:Label ID="LblTransactionDate" runat="server" Text="Transaction Date"></asp:Label></td>
                                                        <td nowrap style="width: 75%;" valign="top">
                                                            <asp:TextBox ID="TxtTransactionDate" runat="server" CssClass="searcheditbox"
                                                                Width="100px"></asp:TextBox>
                                                            <input id="popUpTransactionDate" title="Click to show calendar"
                                                                    style="border-right: #ffffff 0px solid; border-top: #ffffff 0px solid; font-size: 11px;
                                                                    border-left: #ffffff 0px solid; border-bottom: #ffffff 0px solid; height: 17px;
                                                                    background-image: url(Script/Calendar/cal.gif); width: 16px;" type="button" 
                                                                    runat="server" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap style="width: 15%; background-color: #FFF7E6;" valign="top">
                                                            <asp:Label ID="LblAccountOwner" runat="server" Text="Account Owner ID"></asp:Label>
                                                        </td>
                                                        <td nowrap style="width: 75%;" valign="top">
                                                            <asp:TextBox ID="TxtAccountOwnerID" runat="server" CssClass="searcheditbox" 
                                                                MaxLength="50" Width="125px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap style="width: 15%; background-color: #FFF7E6;" valign="top">
                                                            <asp:Label ID="LblAccountOwner0" runat="server" Text="Account Owner Name"></asp:Label>
                                                        </td>
                                                        <td nowrap style="width: 75%;" valign="top">
                                                            <asp:TextBox ID="TxtAccountOwnerName" runat="server" CssClass="searcheditbox" 
                                                                MaxLength="50" Width="125px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap style="background-color: #FFF7E6; width: 15%">
                                                            <asp:Label ID="Label2" runat="server" Text="Debit/Credit"></asp:Label></td>
                                                        <td nowrap style="background-color: #ffffff; width: 35%">
                                                            <asp:TextBox ID="TxtDebitOrCredit" runat="server" CssClass="searcheditbox" Width="125px"
                                                                MaxLength="50"></asp:TextBox></td>
                                                    </tr>
                                                    <tr >
                                                        <td nowrap style="background-color: #FFF7E6; width: 15%">
                                                            <asp:Label ID="Label9" runat="server" Text="Currency Type"></asp:Label></td>
                                                        <td nowrap style="background-color: #ffffff; width: 35%">
                                                            <asp:TextBox ID="txtCurrencyType" runat="server" CssClass="searcheditbox" Width="125px"
                                                                MaxLength="50"></asp:TextBox></td>
                                                    </tr>
                                                    <tr >
                                                        <td nowrap style="background-color: #FFF7E6; width: 15%">
                                                            <asp:Label ID="Label12" runat="server" Text="Amount"></asp:Label>
                                                        </td>
                                                        <td nowrap style="background-color: #ffffff; width: 35%">
                                                            <asp:TextBox ID="TxtAmount" runat="server" CssClass="searcheditbox" 
                                                                MaxLength="50" Width="125px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr >
                                                        <td nowrap style="background-color: #FFF7E6; width: 15%">
                                                            <asp:Label ID="Label13" runat="server" Text="Transaction Ticket Number"></asp:Label>
                                                        </td>
                                                        <td nowrap style="background-color: #ffffff; width: 35%">
                                                            <asp:TextBox ID="TxtTransactionTicketNumber" runat="server" 
                                                                CssClass="searcheditbox" MaxLength="50" Width="125px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr >
                                                        <td nowrap style="background-color: #FFF7E6; width: 15%">
                                                            <asp:Label ID="Label14" runat="server" Text="Aging"></asp:Label>
                                                        </td>
                                                        <td nowrap style="background-color: #ffffff; width: 35%">
                                                            <asp:TextBox ID="TxtAging" runat="server" CssClass="searcheditbox" 
                                                                MaxLength="50" Width="125px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap style="background-color: #FFF7E6; width: 15%">
                                                        </td>
                                                        <td nowrap style="background-color: #ffffff; width: 35%">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" style="height: 10px">
                                                            &nbsp;<asp:ImageButton ID="ImageButtonSearch" TabIndex="3" runat="server" SkinID="SearchButton"
                                                                CausesValidation="False" ImageUrl="~/Images/Button/Search.gif"></asp:ImageButton>&nbsp;<asp:ImageButton
                                                                    ID="BtnClearSearch" runat="server" CausesValidation="False" 
                                                                ImageUrl="~/Images/Button/clearSearch.gif" TabIndex="3" 
                                                                style="height: 17px" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>

                                     <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%" bgcolor="#dddddd" border="2" id="TblData">
                                        <tr id="TR1">
                                            <td valign="top" width="98%" bgcolor="#ffffff" >                                            
                                                <table bordercolor="#ffffff" cellspacing="1" cellpadding="0" width="100%" bgcolor="#dddddd"
                                                    border="2">
                                                    <tr>
                                                        <td bgcolor="#ffffff" background="images/testbg.gif">
                                                            <asp:DataGrid ID="GridDataView" runat="server" Font-Size="XX-Small"
                                                                CellPadding="4" AllowPaging="True" Width="100%" GridLines="Vertical"
                                                                AllowSorting="True" ForeColor="Black" Font-Bold="False" Font-Italic="False"
                                                                Font-Overline="False" Font-Strikeout="False" Font-Underline="False" 
                                                                HorizontalAlign="Left" AutoGenerateColumns="False" BackColor="White" 
                                                                BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px">
                                                                <AlternatingItemStyle BackColor="White" />
                                                                <Columns>
                                                                    <asp:TemplateColumn>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="CheckBoxExporttoExcel" runat="server" />
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="2%" />
                                                                    </asp:TemplateColumn>
                                                                    <asp:BoundColumn DataField="TransactionDetailCTRGrips_ID" Visible="False">
                                                                        <HeaderStyle Width="0%" />
                                                                    </asp:BoundColumn>
                                                                    <asp:TemplateColumn HeaderText="No.">
                                                                        <HeaderStyle ForeColor="White" />
                                                                    </asp:TemplateColumn>
                                                                    <asp:BoundColumn DataField="CIFNo" HeaderText="CIF Number" 
                                                                        SortExpression="CIFNo desc">
                                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                            Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" 
                                                                            VerticalAlign="Middle" Wrap="False" />
                                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                            Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" 
                                                                            VerticalAlign="Middle" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="AccountNo" HeaderText="Account Number" 
                                                                        SortExpression="AccountNo desc"></asp:BoundColumn>
                                                                    <asp:BoundColumn HeaderText="Customer Name" DataField="CustomerName" 
                                                                        SortExpression="CustomerName desc"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="TransactionDate" HeaderText="Transaction Date" 
                                                                        SortExpression="TransactionDate desc" DataFormatString="{0:yyyy-MM-dd}">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="AccountOwnerId" HeaderText="Account Owner ID" 
                                                                        SortExpression="AccountOwnerId desc">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="AccountOwnerName" HeaderText="Account Owner Name" 
                                                                        SortExpression="AccountOwnerName desc"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="DebetCredit" HeaderText="Debet/Credit" 
                                                                        SortExpression="DebetCredit desc"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="CurrencyType" HeaderText="Currency Type" 
                                                                        SortExpression="CurrencyType desc"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="TransactionAmountOriginal" HeaderText="Amount" 
                                                                        SortExpression="TransactionAmountOriginal desc"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="UserId" HeaderText="User ID" 
                                                                        SortExpression="UserId desc"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="TransactionTicketNo" 
                                                                        HeaderText="TransactionTicketNo" SortExpression="TransactionTicketNo desc">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Aging" HeaderText="Aging" 
                                                                        SortExpression="Aging desc"></asp:BoundColumn>
                                                                    <asp:ButtonColumn CommandName="Edit" Text="Lengkapi"></asp:ButtonColumn>
                                                                </Columns>
                                                                <FooterStyle BackColor="#CCCC99" />
                                                                <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                                                <ItemStyle BackColor="#F7F7DE" />
                                                                <PagerStyle Visible="False" HorizontalAlign="Right" ForeColor="Black" 
                                                                    BackColor="#F7F7DE" Mode="NumericPages">
                                                                </PagerStyle>
                                                                <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                            </asp:DataGrid>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: #ffffff">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                    <tr>
                                                        <td>
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td nowrap style="height: 20px">
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td style="width: 81px">
                                                                                    <asp:CheckBox ID="CheckBoxSelectAll" runat="server" AutoPostBack="True" Text="Select All" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:LinkButton ID="LnkExportToExcel" runat="server" ajaxCall="none">Export to Excel</asp:LinkButton>
                                                                                </td>
                                                                                <td style="width: 120px">
                                                                                    &nbsp;</td>
                                                                                <td>
                                                                                    &nbsp;</td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td style="height: 20px">
                                                                        &nbsp;&nbsp;
                                                                    </td>
                                                                    <td align="right" nowrap style="height: 20px">
                                                                        &nbsp;&nbsp;
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>   
                                                <table width="100%">                                    
                                                <tr>
                                                    <td bgcolor="#ffffff">
                                                        <table id="Table3" bgcolor="#ffffff" border="2" bordercolor="#ffffff" cellpadding="2"
                                                            cellspacing="1" width="100%">
                                                            <tr align="center" bgcolor="#dddddd" class="regtext">
                                                                <td align="left" bgcolor="#ffffff" style="height: 19px" valign="top" width="50%">
                                                                    Page&nbsp;<asp:Label ID="PageCurrentPage" runat="server" CssClass="regtext">0</asp:Label>
                                                                    &nbsp;of&nbsp;
                                                                    <asp:Label ID="PageTotalPages" runat="server" CssClass="regtext">0</asp:Label>
                                                                </td>
                                                                <td align="right" bgcolor="#ffffff" style="height: 19px" valign="top" width="50%">
                                                                    Total Records&nbsp;
                                                                    <asp:Label ID="PageTotalRows" runat="server">0</asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table id="Table4" bgcolor="#ffffff" border="2" bordercolor="#ffffff" cellpadding="2"
                                                            cellspacing="1" width="100%">
                                                            <tr bgcolor="#ffffff">
                                                                <td align="left" class="regtext" colspan="11" height="7" valign="middle">
                                                                    <hr></hr>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="63">
                                                                    Go to page</td>
                                                                <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="5">
                                                                    <font face="Verdana, Arial, Helvetica, sans-serif" size="1">
                                                                        <asp:TextBox ID="TextGoToPage" runat="server" CssClass="searcheditbox" Width="38px"></asp:TextBox>
                                                                    </font>
                                                                </td>
                                                                <td align="left" bgcolor="#ffffff" class="regtext" valign="middle">
                                                                    <asp:ImageButton ID="ImageButtonGo" runat="server" ImageUrl="~/Images/Button/Go.gif"
                                                                        SkinID="GoButton" />
                                                                </td>
                                                                <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="6">
                                                                    <img height="5" src="images/first.gif" width="6"> </img>
                                                                </td>
                                                                <td align="right" bgcolor="#ffffff" class="regtext" valign="middle" width="6">
                                                                    <asp:LinkButton ID="LinkButtonFirst" runat="server" CommandName="First" CssClass="regtext"
                                                                        OnCommand="PageNavigate">First</asp:LinkButton>
                                                                </td>
                                                                <td align="right" bgcolor="#ffffff" class="regtext" valign="middle" width="6">
                                                                    <img height="5" src="images/prev.gif" width="6"></img></td>
                                                                <td align="right" bgcolor="#ffffff" class="regtext" valign="middle" width="14">
                                                                    <asp:LinkButton ID="LinkButtonPrevious" runat="server" CommandName="Prev" CssClass="regtext"
                                                                        OnCommand="PageNavigate">Previous</asp:LinkButton>
                                                                </td>
                                                                <td align="right" bgcolor="#ffffff" class="regtext" valign="middle" width="60">
                                                                    <a class="pageNav" href="#">
                                                                        <asp:LinkButton ID="LinkButtonNext" runat="server" CommandName="Next" CssClass="regtext"
                                                                            OnCommand="PageNavigate">Next</asp:LinkButton>
                                                                    </a>
                                                                </td>
                                                                <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="6">
                                                                    <img height="5" src="images/next.gif" width="6"></img></td>
                                                                <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="25">
                                                                    <asp:LinkButton ID="LinkButtonLast" runat="server" CommandName="Last" CssClass="regtext"
                                                                        OnCommand="PageNavigate">Last</asp:LinkButton>
                                                                </td>
                                                                <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="6">
                                                                    <img height="5" src="images/last.gif" width="6"></img></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                </table>   
                                            </td>
                                        </tr>
                                    </table>
                                </ajax:AjaxPanel>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td background="Images/button-bground.gif" align="left" valign="middle">
                            <img src="Images/blank.gif" width="5" height="1" /></td>
                        <td background="Images/button-bground.gif" align="left" valign="middle">
                            &nbsp;</td>
                        <td width="99%" background="Images/button-bground.gif">
                            <img src="Images/blank.gif" width="1" height="1" /></td>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:CustomValidator ID="cvalPageErr" runat="server" Display="None"></asp:CustomValidator>
</asp:Content>
