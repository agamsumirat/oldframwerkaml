Imports Sahassa.AML.Commonly
Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports System.Data
Imports AMLBLL
Imports System.Data.SqlClient

Partial Class IFTIFileUploadReferenceNumber
    Inherits Parent


    Public Property ObjTblReferenceNo() As Data.DataTable
        Get
            If Session("IFTIFileUploadReferenceNumber.ObjTblReferenceNo") Is Nothing Then
                Using objDt As New DataTable

                    Dim auto As DataColumn = New DataColumn("No", GetType(Integer))                    
                    objDt.Columns.Add(auto)
                    objDt.Columns.Add(New Data.DataColumn("Localid", GetType(String)))
                    objDt.Columns.Add(New Data.DataColumn("NoLTKL", GetType(String)))
                    objDt.Columns.Add(New Data.DataColumn("TransactionDate", GetType(DateTime)))
                    objDt.Columns.Add(New Data.DataColumn("ReportDate", GetType(DateTime)))
                    Session("IFTIFileUploadReferenceNumber.ObjTblReferenceNo") = objDt
                End Using
            End If
            Return CType(Session("IFTIFileUploadReferenceNumber.ObjTblReferenceNo"), Data.DataTable)
        End Get
        Set(ByVal value As Data.DataTable)
            Session("IFTIFileUploadReferenceNumber.ObjTblReferenceNo") = value
        End Set
    End Property
    Public Property ObjListOfGeneratedIFTI() As ListOfGeneratedIFTI
        Get
            If Session("IFTIFileUploadReferenceNumber.ObjListOfGeneratedIFTI") Is Nothing Then
                Session("IFTIFileUploadReferenceNumber.ObjListOfGeneratedIFTI") = ListOfGeneratedIFTIBLL.GetListOfGeneratedIFTIByPk(Me.PKListOfTransactionIFTI)
                Return CType(Session("IFTIFileUploadReferenceNumber.ObjListOfGeneratedIFTI"), ListOfGeneratedIFTI)
            Else
                Return CType(Session("IFTIFileUploadReferenceNumber.ObjListOfGeneratedIFTI"), ListOfGeneratedIFTI)
            End If
        End Get
        Set(ByVal value As ListOfGeneratedIFTI)
            Session("IFTIFileUploadReferenceNumber.ObjListOfGeneratedIFTI") = value
        End Set
    End Property

    Public ReadOnly Property PKListOfTransactionIFTI() As Long
        Get
            Dim temp As String
            Dim retvalue As Long
            temp = Request.Params("PkListOfGeneratedIFTIID")
            If Not Long.TryParse(temp, retvalue) Then
                Throw New Exception("PkListOfGeneratedIFTIID is not Vaid")
            End If
            Return retvalue
        End Get
    End Property

    Function IsDataValid() As Boolean
        Dim strErrorMessage As New StringBuilder
        If Not Me.FileUploadFileReferenceNumber.HasFile Then
            strErrorMessage.Append("Please choose file to be uploaded.")
        End If

        If strErrorMessage.ToString.Trim.Length > 0 Then
            Throw New Exception(strErrorMessage.ToString)
        Else
            Return True
        End If

    End Function


    Private Function ConvertHTMLTablesToDataSet(ByVal HTML As String) As DataSet
        ' Declarations  
        Dim ds As New DataSet
        Dim dt As DataTable
        Dim dr As DataRow
        Dim dc As DataColumn
        Dim TableExpression As String = "<table[^>]*>(.*?)</table>"
        Dim HeaderExpression As String = "<th[^>]*>(.*?)</th>"
        Dim RowExpression As String = "<tr[^>]*>(.*?)</tr>"
        Dim ColumnExpression As String = "<td[^>]*>(.*?)</td>"
        Dim HeadersExist As Boolean = False
        Dim iCurrentColumn As Integer = 0
        Dim iCurrentRow As Integer = 0

        ' Get a match for all the tables in the HTML  
        Dim Tables As MatchCollection = Regex.Matches(HTML, TableExpression, RegexOptions.Multiline Or RegexOptions.Singleline Or RegexOptions.IgnoreCase)

        ' Loop through each table element  
        For Each Table As Match In Tables

            ' Reset the current row counter and the header flag  
            iCurrentRow = 0
            HeadersExist = False

            ' Add a new table to the DataSet  
            dt = New DataTable

            ' Create the relevant amount of columns for this table (use the headers if they exist, otherwise use default names)  
            If Table.Value.Contains("<th") Then
                ' Set the HeadersExist flag  
                HeadersExist = True

                ' Get a match for all the rows in the table  
                Dim Headers As MatchCollection = Regex.Matches(Table.Value, HeaderExpression, RegexOptions.Multiline Or RegexOptions.Singleline Or RegexOptions.IgnoreCase)

                ' Loop through each header element  
                For Each Header As Match In Headers
                    dt.Columns.Add(Header.Groups(1).ToString)
                Next
            Else
                For iColumns As Integer = 1 To Regex.Matches(Regex.Matches(Regex.Matches(Table.Value, TableExpression, RegexOptions.Multiline Or RegexOptions.Singleline Or RegexOptions.IgnoreCase).Item(0).ToString, RowExpression, RegexOptions.Multiline Or RegexOptions.Singleline Or RegexOptions.IgnoreCase).Item(0).ToString, ColumnExpression, RegexOptions.Multiline Or RegexOptions.Singleline Or RegexOptions.IgnoreCase).Count
                    dt.Columns.Add("Column " & iColumns)
                Next
            End If

            ' Get a match for all the rows in the table  
            Dim Rows As MatchCollection = Regex.Matches(Table.Value, RowExpression, RegexOptions.Multiline Or RegexOptions.Singleline Or RegexOptions.IgnoreCase)

            ' Loop through each row element  
            For Each Row As Match In Rows

                ' Only loop through the row if it isn't a header row  
                If Not (iCurrentRow = 0 And HeadersExist = True) Then

                    ' Create a new row and reset the current column counter  
                    dr = dt.NewRow
                    iCurrentColumn = 0

                    ' Get a match for all the columns in the row  
                    Dim Columns As MatchCollection = Regex.Matches(Row.Value, ColumnExpression, RegexOptions.Multiline Or RegexOptions.Singleline Or RegexOptions.IgnoreCase)

                    ' Loop through each column element  
                    For Each Column As Match In Columns

                        ' Add the value to the DataRow  
                        dr(iCurrentColumn) = Column.Groups(1).ToString

                        ' Increase the current column  
                        iCurrentColumn += 1
                    Next

                    ' Add the DataRow to the DataTable  
                    dt.Rows.Add(dr)

                End If

                ' Increase the current row counter  
                iCurrentRow += 1
            Next

            ' Add the DataTable to the DataSet  
            ds.Tables.Add(dt)

        Next

        Return (ds)

    End Function
    Private Function GetIFTIUploadedConfirmationNumber() As Integer
        Dim myStream As System.IO.Stream
        Dim ArrConfirmationNumber As String() = Nothing
        Dim ArrReturn As New ArrayList
        Dim Counter As Integer
        Dim StrConfirmationNumber As String = ""
        Dim StrConfirmationNumberDetail As String
        Dim IntStartCounter As Integer
        Dim ds As DataSet

        Try
            ' Initialize the stream to read the uploaded file.
            myStream = Me.FileUploadFileReferenceNumber.FileContent
            Using sr As IO.StreamReader = New IO.StreamReader(myStream)
                StrConfirmationNumber = sr.ReadToEnd
                sr.Close()
            End Using

            ds = ConvertHTMLTablesToDataSet(StrConfirmationNumber)


            For Each Item As DataRow In ds.Tables(0).Rows

                Dim objNewRow As DataRow = ObjTblReferenceNo.NewRow
                objNewRow.Item("No") = Item(0)
                objNewRow.Item("Localid") = Item(1)
                objNewRow.Item("NoLTKL") = Item(2)
                objNewRow.Item("TransactionDate") = ObjListOfGeneratedIFTI.TransactionDate
                objNewRow.Item("ReportDate") = Item(3)

                ObjTblReferenceNo.Rows.Add(objNewRow)


            Next


            
            Return ObjTblReferenceNo.Rows.Count
        Catch
            Throw
        End Try
    End Function
    Function ClearUploadIFTIGripsReferenceNo() As Boolean
        Using objCommand As SqlCommand = GetSQLCommandStoreProcedure("usp_ClearUploadIFTIGripsReferenceNo")
            DataRepository.Provider.ExecuteNonQuery(objCommand)
        End Using

        Return True
    End Function

    Protected Sub ImgUploadBasedonRefNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgUploadBasedonRefNo.Click

        Try
            Me.ObjTblReferenceNo = Nothing

            ClearUploadIFTIGripsReferenceNo()
            If Not Me.FileUploadFileReferenceNumber.HasFile Then
                Throw New Exception("Please choose file to be uploaded.")
            End If

            Me.GetIFTIUploadedConfirmationNumber()

            If ObjTblReferenceNo.Rows.Count > 0 Then
                Me.LblSucces.Text = "Update IFTI PPATK Confirmation Number has done successfully."
                Me.LblSucces.Visible = True


                'If ObjTblReferenceNo.Rows.Count = Convert.ToInt32(Me.LblTotalValid.Text) Then



                'Else
                '    Throw New Exception("Count Of Confirmation Number :" & ObjTblReferenceNo.Rows.Count & " not same with count of IFTI for Valid Transaction  '" & Me.LblTotalValid.Text & "'")
                'End If

                Using objsqlbulk As New Data.SqlClient.SqlBulkCopy(System.Configuration.ConfigurationManager.ConnectionStrings("netTiersConnectionString").ConnectionString)
                    objsqlbulk.BatchSize = 1000
                    objsqlbulk.BulkCopyTimeout = System.Configuration.ConfigurationManager.AppSettings("SQLCommandTimeout")
                    Dim mapping2 As Data.SqlClient.SqlBulkCopyColumnMapping = New Data.SqlClient.SqlBulkCopyColumnMapping("No", "No")
                    Dim mapping0 As Data.SqlClient.SqlBulkCopyColumnMapping = New Data.SqlClient.SqlBulkCopyColumnMapping("Localid", "Localid")
                    Dim mapping3 As Data.SqlClient.SqlBulkCopyColumnMapping = New Data.SqlClient.SqlBulkCopyColumnMapping("NoLTKL", "NoLTKL")
                    Dim mapping1 As Data.SqlClient.SqlBulkCopyColumnMapping = New Data.SqlClient.SqlBulkCopyColumnMapping("TransactionDate", "TransactionDate")
                    Dim mapping4 As Data.SqlClient.SqlBulkCopyColumnMapping = New Data.SqlClient.SqlBulkCopyColumnMapping("ReportDate", "ReportDate")
                    objsqlbulk.ColumnMappings.Add(mapping2)
                    objsqlbulk.ColumnMappings.Add(mapping0)
                    objsqlbulk.ColumnMappings.Add(mapping3)
                    objsqlbulk.ColumnMappings.Add(mapping1)
                    objsqlbulk.ColumnMappings.Add(mapping4)
                    objsqlbulk.DestinationTableName = "UploadIFTIGripsReferenceNo"
                    objsqlbulk.WriteToServer(ObjTblReferenceNo)
                End Using



                Using objCommand As System.Data.SqlClient.SqlCommand = GetSQLCommandStoreProcedure("usp_UpdateIFTIsRefNo")
                    objCommand.Parameters.Add(New SqlParameter("@PKListofGeneratedID", ObjListOfGeneratedIFTI.PK_ListOfGeneratedIFTI_ID.ToString))
                    DataRepository.Provider.ExecuteNonQuery(objCommand)

                End Using




            End If


        Catch ex As Exception
            Me.LblSucces.Visible = False
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Shared Function GetSQLCommandStoreProcedure(ByVal strsql As String) As System.Data.SqlClient.SqlCommand
        Using cmd As New System.Data.SqlClient.SqlCommand
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = strsql
            cmd.CommandTimeout = System.Configuration.ConfigurationManager.AppSettings("SQLCommandTimeout")
            Return cmd
        End Using


    End Function

    Sub ClearSession()
        ObjListOfGeneratedIFTI = Nothing
        ObjTblReferenceNo = Nothing
    End Sub

    Sub LoadData()
        If Not ObjListOfGeneratedIFTI Is Nothing Then

            LblLastConfirmBy.Text = ObjListOfGeneratedIFTI.LastConfirmedBy

            Using objMsCTRReportTypeFile As MsCTRReportTypeFile = DataRepository.MsCTRReportTypeFileProvider.GetByPk_MsCTRReportTypeFile_Id(ObjListOfGeneratedIFTI.Fk_MsCTRReportTypeFile_Id)
                If Not objMsCTRReportTypeFile Is Nothing Then
                    LblReportType.Text = objMsCTRReportTypeFile.MsCTRReportTypeFile_Name
                End If
            End Using


            Using objMsStatusUploadPPATK As MsStatusUploadPPATK = DataRepository.MsStatusUploadPPATKProvider.GetByPk_MsStatusUploadPPATK_Id(ObjListOfGeneratedIFTI.Fk_MsStatusUploadPPATK_Id)
                If Not objMsStatusUploadPPATK Is Nothing Then
                    LblStatusUploadPPATK.Text = objMsStatusUploadPPATK.MsStatusUploadPPATK_Name
                End If
            End Using



            LblTotalInvalid.Text = ObjListOfGeneratedIFTI.TotalInvalid.GetValueOrDefault(0)
            LblTransactionType.Text = ObjListOfGeneratedIFTI.SwiftType
            LblTotalTransaction.text = ObjListOfGeneratedIFTI.TotalTransaksi
            LblTotalValid.Text = ObjListOfGeneratedIFTI.TotalValid
            LblTransactionDate.Text = ObjListOfGeneratedIFTI.TransactionDate.GetValueOrDefault(Sahassa.AML.Commonly.GetDefaultDate).ToString("dd-MMM-yyyy").Replace("01-Jan-1900", "")

            Dim intIFTItype As Integer
            Using objIFTI_Type As TList(Of IFTI_Type) = DataRepository.IFTI_TypeProvider.GetPaged(IFTI_TypeColumn.IFTIType.ToString & "='" & ObjListOfGeneratedIFTI.SwiftType & "'", "", 0, Integer.MaxValue, 0)
                If objIFTI_Type.Count > 0 Then
                    intIFTItype = objIFTI_Type(0).PK_IFTI_Type_ID
                End If
            End Using



        End If


    End Sub
    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                ClearSession()
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using

                LoadData()
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImgBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBack.Click
        Response.Redirect("ListofGeneratedIFTIFile.aspx")
    End Sub
End Class
