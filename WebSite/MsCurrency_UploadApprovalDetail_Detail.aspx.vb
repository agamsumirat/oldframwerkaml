#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports Sahassa.AML
Imports System.Collections.Generic
#End Region

Partial Class MsCurrency_UploadApprovalDetail_Detail
    Inherits Parent

#Region "Function"

    Sub HideControl()
        ImageCancel.Visible = False
    End Sub

    Sub ChangeMultiView(ByVal index As Integer)
        MtvMsUser.ActiveViewIndex = index
    End Sub

#End Region

#Region "events..."

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Dim LngId As Long = 0
        Using ObjMsCurrency_ApprovalDetail As TList(Of MsCurrency_ApprovalDetail) = DataRepository.MsCurrency_ApprovalDetailProvider.GetPaged("PK_MsCurrency_Approval_detail_id = " & parID, "", 0, Integer.MaxValue, 0)
            If ObjMsCurrency_ApprovalDetail.Count > 0 Then
                LngId = ObjMsCurrency_ApprovalDetail(0).PK_MsCurrency_Approval_id
            End If
        End Using
        Response.Redirect("MsCurrency_UploadApprovalDetail_View.aspx?ID=" & LngId)
    End Sub






    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
                ListmapingNew = New TList(Of MappingMsCurrencyNCBSPPATK_Approval_Detail)
                ListmapingOld = New TList(Of MappingMsCurrencyNCBSPPATK)
                LoadData()
            Catch ex As Exception
                LogError(ex)
                CvalPageErr.IsValid = False
                CvalPageErr.ErrorMessage = ex.Message
            End Try
        End If

    End Sub

    Private Sub LoadData()
        Dim PkObject As String
        'Load new data
        Using ObjMsCurrency_ApprovalDetail As MsCurrency_ApprovalDetail = DataRepository.MsCurrency_ApprovalDetailProvider.GetPaged(MsCurrency_ApprovalDetailColumn.PK_MsCurrency_Approval_detail_id.ToString & _
            "=" & _
            parID, "", 0, 1, Nothing)(0)
            With ObjMsCurrency_ApprovalDetail
                SafeDefaultValue = "-"
                txtIdCurrencynew.Text = Safe(.IdCurrency)
                txtCodenew.Text = Safe(.Code)
                txtNamenew.Text = Safe(.Name)

                'other info
                Dim Omsuser As TList(Of User)
                Omsuser = DataRepository.UserProvider.GetByUserID(.CreatedBy)
                If Omsuser.Count > 0 Then
                    lblCreatedByNew.Text = Omsuser(0).UserName
                Else
                    lblCreatedByNew.Text = SafeDefaultValue
                End If
                lblCreatedDateNew.Text = FormatDate(.CreatedDate)
                Omsuser = DataRepository.UserProvider.GetByUserID(.LastUpdatedBy)
                If Omsuser.Count > 0 Then
                    lblUpdatedbyNew.Text = Omsuser(0).UserName
                Else
                    lblUpdatedbyNew.Text = SafeDefaultValue
                End If
                lblUpdatedDateNew.Text = FormatDate(.LastUpdatedDate)
                'txtActivationNew.Text = SafeActiveInactive(.Activation)
                Dim L_objMappingMsCurrencyNCBSPPATK_Approval_Detail As TList(Of MappingMsCurrencyNCBSPPATK_Approval_Detail)
                L_objMappingMsCurrencyNCBSPPATK_Approval_Detail = DataRepository.MappingMsCurrencyNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsCurrencyNCBSPPATK_Approval_DetailColumn.Pk_MsCurrency_Id.ToString & _
                 "=" & _
                 ObjMsCurrency_ApprovalDetail.IdCurrency, "", 0, Integer.MaxValue, Nothing)

                'Add mapping
                ListmapingNew.AddRange(L_objMappingMsCurrencyNCBSPPATK_Approval_Detail)

            End With
            PkObject = ObjMsCurrency_ApprovalDetail.IdCurrency
        End Using



        'Load Old Data
        Using objMsCurrency As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(PkObject)
            If objMsCurrency Is Nothing Then
                lamaOld.Visible = False
                lama.Visible = False
                Return
            End If

            With objMsCurrency
                SafeDefaultValue = "-"
                txtIdCurrencyOld.Text = Safe(.IdCurrency)
                txtCodeOld.Text = Safe(.Code)
                txtNameOld.Text = Safe(.Name)


                'other info
                Dim Omsuser As TList(Of User)
                Omsuser = DataRepository.UserProvider.GetByUserID(.CreatedBy)
                If Omsuser.Count > 0 Then
                    lblCreatedByOld.Text = Omsuser(0).UserName
                Else
                    lblCreatedByOld.Text = SafeDefaultValue
                End If
                lblCreatedDateNew.Text = FormatDate(.CreatedDate)
                Omsuser = DataRepository.UserProvider.GetByUserID(.LastUpdatedBy)
                If Omsuser.Count > 0 Then
                    lblUpdatedbyOld.Text = Omsuser(0).UserName
                Else
                    lblUpdatedbyOld.Text = SafeDefaultValue
                End If
                lblUpdatedDateOld.Text = FormatDate(.LastUpdatedDate)
                Dim l_objMappingMsCurrencyNCBSPPATK As TList(Of MappingMsCurrencyNCBSPPATK)
                'txtNameOld.Text = Safe(.Name)
                l_objMappingMsCurrencyNCBSPPATK = DataRepository.MappingMsCurrencyNCBSPPATKProvider.GetPaged(MappingMsCurrencyNCBSPPATKColumn.IdCurrency.ToString & _
                   "=" & _
                   PkObject, "", 0, Integer.MaxValue, Nothing)

                ListmapingOld.AddRange(l_objMappingMsCurrencyNCBSPPATK)
            End With
        End Using
    End Sub






    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            BindListMapping()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub BindListMapping()
        'bind new value
        LBMappingNew.DataSource = ListMappingDisplayNew '(0).PK_MappingMsCurrencyNCBSPPATK_Id
        LBMappingNew.DataBind()
        'Bind OldValue
        LBmappingOld.DataSource = ListMappingDisplayOld
        LBmappingOld.DataBind()
    End Sub

#End Region

#Region "Property..."

    Public ReadOnly Property parID() As String
        Get
            Return Request.Item("ID")
        End Get
    End Property
    ''' <summary>
    ''' Menyimpan Item untuk mapping New sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ListmapingNew() As TList(Of MappingMsCurrencyNCBSPPATK_Approval_Detail)
        Get
            Return Session("ListmapingNew.data")
        End Get
        Set(ByVal value As TList(Of MappingMsCurrencyNCBSPPATK_Approval_Detail))
            Session("ListmapingNew.data") = value
        End Set
    End Property

    ''' <summary>
    ''' Menyimpan Item untuk mapping Old sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ListmapingOld() As TList(Of MappingMsCurrencyNCBSPPATK)
        Get
            Return Session("ListmapingOld.data")
        End Get
        Set(ByVal value As TList(Of MappingMsCurrencyNCBSPPATK))
            Session("ListmapingOld.data") = value
        End Set
    End Property

    ''' <summary>
    ''' List yang tampil di ListBox New
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplayNew() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingMsCurrencyNCBSPPATK_Approval_Detail In ListmapingNew.FindAllDistinct("Pk_MsCurrencyPPATK_Id")
                Temp.Add(i.Pk_MsCurrencyPPATK_Id.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property

    ''' <summary>
    ''' List yang tampil di ListBox Old
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplayOld() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingMsCurrencyNCBSPPATK In ListmapingOld.FindAllDistinct("IdCurrencyNCBS")
                Temp.Add(i.IdCurrencyNCBS.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property
#End Region



End Class




