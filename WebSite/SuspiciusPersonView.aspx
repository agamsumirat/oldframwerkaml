<%@ Page Language="VB" MasterPageFile="~/masterpage.master" AutoEventWireup="false" CodeFile="SuspiciusPersonView.aspx.vb" Inherits="SuspiciusPersonView" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
    <table id="title"  border="2" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr>

            <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Custom Alert - View&nbsp;
                    <hr />
                </strong>
            </td>
        </tr>
    </table>
    <TABLE borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2">
        <TR>
            <TD align="left" bgColor="#eeeeee"><IMG height="15" src="images/arrow.gif" width="15"></TD>
				<TD vAlign="top" width="98%" bgColor="#ffffff"><TABLE cellSpacing="4" cellPadding="0" width="100%" border="0">
						<TR>
							<TD class="Regtext" noWrap>Search By :
							</TD>
							<TD vAlign="middle" noWrap><ajax:ajaxpanel id="AjaxPanel1" runat="server">
                                <asp:dropdownlist id="ComboSearch" tabIndex="1" runat="server" Width="120px" CssClass="searcheditcbo"></asp:dropdownlist>&nbsp; 
                                <asp:textbox id="TextSearch" tabIndex="2" runat="server" CssClass="searcheditbox"></asp:textbox>
                                <input id="popUp" runat="server" name="popUpCalc" style="border-right: #ffffff 0px solid;
                                    border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
                                    border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
                                    height: 17px" title="Click to show calendar" type="button" /></ajax:ajaxpanel></TD>
							<TD vAlign="middle" width="99%"><ajax:ajaxpanel id="AjaxPanel2" runat="server">
									<asp:imagebutton id="ImageButtonSearch" tabIndex="3" runat="server" SkinID="SearchButton"></asp:imagebutton></ajax:ajaxpanel>
								</TD>
						</TR>
					</TABLE><ajax:ajaxpanel id="AjaxPanel3" runat="server">
                    <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></ajax:ajaxpanel></TD>
			</TR>
	</TABLE>
	
<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2">
  <tr> 
    <td bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel4" runat="server"> 
      <asp:datagrid id="GridMSUserView" runat="server" AutoGenerateColumns="False"
						Font-Size="XX-Small" BackColor="White" CellPadding="4" BorderWidth="1px" BorderStyle="None"
						AllowPaging="True" width="100%" GridLines="Vertical" AllowSorting="True" BorderColor="#DEDFDE"
						ForeColor="Black"> 
        <FooterStyle BackColor="#CCCC99"></FooterStyle>
        <SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#CE5D5A"></SelectedItemStyle>
        <AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
        <ItemStyle BackColor="#F7F7DE"></ItemStyle>
        <HeaderStyle Font-Size="XX-Small" Font-Bold="True" ForeColor="White" BackColor="#6B696B"></HeaderStyle>
        <Columns>
        <asp:TemplateColumn>
          <ItemTemplate> 
            <asp:CheckBox ID="CheckBoxExporttoExcel" runat="server" />
          </ItemTemplate>
        </asp:TemplateColumn>
        <asp:BoundColumn DataField="PK_SuspiciusPerson_ID" Visible="False" SortExpression="PK_SuspiciusPerson_ID  desc"></asp:BoundColumn>
            <asp:BoundColumn DataField="CIFNo" HeaderText="CIFNo" SortExpression="CIFNo  desc"></asp:BoundColumn>
            <asp:BoundColumn DataField="Nama" HeaderText="Name" SortExpression="Nama  desc"></asp:BoundColumn>
        <asp:BoundColumn DataField="TanggalLahir" HeaderText="Birth Date" SortExpression="TanggalLahir  desc" DataFormatString="{0:dd-MMM-yyyy}"> 
          <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" Wrap="False" /> </asp:BoundColumn>
        <asp:BoundColumn DataField="tempatlahir" HeaderText="Birth Place" SortExpression="tempatlahir  desc"> 
        </asp:BoundColumn>
            <asp:BoundColumn DataField="NoIdentitas" HeaderText="Identity No" SortExpression="NoIdentitas  desc">
            </asp:BoundColumn>
            <asp:BoundColumn DataField="Alamat" HeaderText="Address" SortExpression="Alamat  desc">
            </asp:BoundColumn>
            <asp:BoundColumn DataField="KecamatanKeluarahan" HeaderText="Kecamatan /Kelurahan"
                SortExpression="KecamatanKeluarahan  desc"></asp:BoundColumn>
            <asp:BoundColumn DataField="NoTelp" HeaderText="Phone no" SortExpression="NoTelp  desc">
            </asp:BoundColumn>
            <asp:BoundColumn DataField="Pekerjaan" HeaderText="Position" SortExpression="Pekerjaan  desc">
            </asp:BoundColumn>
            <asp:BoundColumn DataField="AlamatTempatKerja" HeaderText="Company" SortExpression="AlamatTempatKerja  desc">
            </asp:BoundColumn>
            <asp:BoundColumn DataField="NPWP" HeaderText="NPWP" SortExpression="NPWP  desc"></asp:BoundColumn>
            <asp:BoundColumn DataField="Description" HeaderText="Description" SortExpression="Description  desc">
            </asp:BoundColumn>
            <asp:BoundColumn DataField="UseridCreator" HeaderText="Created By" SortExpression="UseridCreator  desc">
            </asp:BoundColumn>
            <asp:BoundColumn DataField="FK_CaseManagement_ID" HeaderText="Case ID" SortExpression="FK_CaseManagement_ID  desc">
            </asp:BoundColumn>
            <asp:TemplateColumn>
                <EditItemTemplate>
                    <asp:LinkButton runat="server" CommandName="Update" Text=""></asp:LinkButton>
                    <asp:LinkButton runat="server" CausesValidation="false" CommandName="Cancel" Text=""></asp:LinkButton>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="lnkEdit" runat="server" CausesValidation="false" CommandName="Edit" Text="Edit"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn>
                <ItemTemplate>
                    <asp:LinkButton ID="lnkdelete" runat="server" CausesValidation="false" CommandName="Delete" Text="Delete"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Create Case">
                <ItemTemplate>
                    <asp:LinkButton ID="lnkCreateCase" runat="server" OnClick="lnkCreateCase_Click">Create Case</asp:LinkButton>
                </ItemTemplate>
                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" ForeColor="White" />
            </asp:TemplateColumn>
        </Columns>
        <PagerStyle Visible="False" HorizontalAlign="Right" ForeColor="Black" BackColor="#F7F7DE" Mode="NumericPages"></PagerStyle>
      </asp:datagrid>
      </ajax:ajaxpanel> </td>
  </tr>
  <tr> 
    <td style="background-color:#ffffff"><table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td nowrap><ajax:ajaxpanel id="AjaxPanel5" runat="server">&nbsp;<asp:CheckBox ID="CheckBoxSelectAll" runat="server" Text="Select All" AutoPostBack="True" />
			  &nbsp; </ajax:ajaxpanel> </td>
			 <td width="99%">&nbsp;&nbsp;<asp:LinkButton ID="LinkButtonExportExcel" runat="server">Export 
		        to Excel</asp:LinkButton>&nbsp;&nbsp;<asp:LinkButton id="LnkBtnExportAllToExcel" runat="server">Export All to Excel</asp:LinkButton></td>
			<td align="right" nowrap><asp:LinkButton ID="LinkButtonAddNew" runat="server">Add 
	        New</asp:LinkButton>&nbsp;&nbsp;</td>
		</tr>
      </table></td>
  </tr>
  <tr> 
    <td bgColor="#ffffff"> <TABLE id="Table3" borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#ffffff"
					border="2">
        <TR class="regtext" align="center" bgColor="#dddddd"> 
          <TD vAlign="top" align="left" width="50%" bgColor="#ffffff">Page&nbsp;<ajax:ajaxpanel id="AjaxPanel6" runat="server"> 
            <asp:label id="PageCurrentPage" runat="server" CssClass="regtext">0</asp:label>
            &nbsp;of&nbsp; 
            <asp:label id="PageTotalPages" runat="server" CssClass="regtext">0</asp:label>
            </ajax:ajaxpanel></TD>
          <TD vAlign="top" align="right" width="50%" bgColor="#ffffff">Total Records&nbsp; 
            <ajax:ajaxpanel id="AjaxPanel7" runat="server"> 
            <asp:label id="PageTotalRows" runat="server">0</asp:label>
            </ajax:ajaxpanel></TD>
        </TR>
      </TABLE>
      <TABLE id="Table4" borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#ffffff"
					border="2">
        <TR bgColor="#ffffff"> 
          <TD class="regtext" vAlign="middle" align="left" colSpan="11" height="7"> 
            <HR color="#f40101" noShade SIZE="1"> </TD>
        </TR>
        <TR> 
          <TD class="regtext" vAlign="middle" align="left" width="63" bgColor="#ffffff">Go 
            to page</TD>
          <TD class="regtext" vAlign="middle" align="left" width="5" bgColor="#ffffff"><FONT face="Verdana, Arial, Helvetica, sans-serif" size="1"> 
            <ajax:ajaxpanel id="AjaxPanel8" runat="server"> 
            <asp:textbox id="TextGoToPage" runat="server" Width="38px" CssClass="searcheditbox"></asp:textbox>
            </ajax:ajaxpanel> </FONT></TD>
          <TD class="regtext" vAlign="middle" align="left" bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel9" runat="server"> 
            <asp:imagebutton id="ImageButtonGo" runat="server" SkinID="GoButton"></asp:imagebutton>
            </ajax:ajaxpanel> </TD>
          <TD class="regtext" vAlign="middle" align="left" width="6" bgColor="#ffffff"><IMG height="5" src="images/first.gif" width="6"> 
          </TD>
          <TD class="regtext" vAlign="middle" align="right" width="6" bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel10" runat="server"> 
            <asp:linkbutton id="LinkButtonFirst" runat="server" CssClass="regtext" CommandName="First" OnCommand="PageNavigate">First</asp:linkbutton>
            </ajax:ajaxpanel> </TD>
          <TD class="regtext" vAlign="middle" align="right" width="6" bgColor="#ffffff"><IMG height="5" src="images/prev.gif" width="6"></TD>
          <TD class="regtext" vAlign="middle" align="right" width="14" bgColor="#ffffff"> 
            <ajax:ajaxpanel id="AjaxPanel11" runat="server"> 
            <asp:linkbutton id="LinkButtonPrevious" runat="server" CssClass="regtext" CommandName="Prev" OnCommand="PageNavigate">Previous</asp:linkbutton>
            </ajax:ajaxpanel> </TD>
          <TD class="regtext" vAlign="middle" align="right" width="60" bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel12" runat="server"><A class="pageNav" href="#"> 
            <asp:linkbutton id="LinkButtonNext" runat="server" CssClass="regtext" CommandName="Next" OnCommand="PageNavigate">Next</asp:linkbutton>
            </A></ajax:ajaxpanel></TD>
          <TD class="regtext" vAlign="middle" align="left" width="6" bgColor="#ffffff"><IMG height="5" src="images/next.gif" width="6"></TD>
          <TD class="regtext" vAlign="middle" align="left" width="25" bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel13" runat="server"> 
            <asp:linkbutton id="LinkButtonLast" runat="server" CssClass="regtext" CommandName="Last" OnCommand="PageNavigate">Last</asp:linkbutton>
            </ajax:ajaxpanel> </TD>
          <TD class="regtext" vAlign="middle" align="left" width="6" bgColor="#ffffff"><IMG height="5" src="images/last.gif" width="6"></TD>
        </TR>
      </TABLE></td>
  </tr>
</table>
</asp:Content>
