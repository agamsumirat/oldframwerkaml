Option Explicit On
Option Strict On
Imports AMLBLL
Imports SahassaNettier.Entities
Imports SahassaNettier.Data
Imports SahassaNettier.Services
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports Sahassa.AML.Commonly

Partial Class TransactionFrequencyEdit
    Inherits Parent

    Public Property ObjTransactionFrequency() As TList(Of TransactionFrequency)
        Get
            If Session("TransactionFrequencyadd.ObjTransactionFrequency") Is Nothing Then
                Using objreturn As TList(Of TransactionFrequency) = DataRepository.TransactionFrequencyProvider.GetPaged("Tier<>0", "Tier", 0, Integer.MaxValue, 0)
                    Session("TransactionFrequencyadd.ObjTransactionFrequency") = objreturn
                End Using
            End If
            Return CType(Session("TransactionFrequencyadd.ObjTransactionFrequency"), TList(Of TransactionFrequency))
        End Get
        Set(ByVal value As TList(Of TransactionFrequency))
            Session("TransactionFrequencyadd.ObjTransactionFrequency") = value
        End Set
    End Property
    Public Property ObjEditTransactionFrequency() As TransactionFrequency
        Get
            Return CType(Session("ObjEditTransactionFrequency.ObjTransactionFrequency"), TransactionFrequency)
        End Get
        Set(ByVal value As TransactionFrequency)
            Session("ObjEditTransactionFrequency.ObjTransactionFrequency") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                'Add Event Calender
                ClearSession()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using

                BindAmount()
            End If
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message

        End Try
    End Sub

    Sub ClearSession()
        Session("TransactionFrequencyadd.ConfigApproval") = Nothing
        Session("TransactionFrequencyadd.objTransactionFrequency") = Nothing
        Session("objeditTransactionFrequency.objTransactionFrequency") = Nothing
        Session("objappTransactionFrequency.objTransactionFrequency") = Nothing

    End Sub


    Public Sub IsDataValidEdit(ByVal StrTier As String, ByVal StrStartRange As String, ByVal StrEndRange As String)
        'Required Field
        If StrTier = "" Then
            Throw New Exception("Tier value is required")
        End If
        If Not Integer.TryParse(StrTier, 0) Then
            Throw New Exception("Tier must be numeric")
        End If
        If Not Double.TryParse(StrStartRange, 0) Then
            Throw New Exception("Start range must be numeric")
        End If
        If StrStartRange = "" Then
            Throw New Exception("Start range value is required")
        End If
        If Not Double.TryParse(StrEndRange, 0) Then
            Throw New Exception("End range must be numeric")
        End If
        If StrEndRange = "" Then
            Throw New Exception("End range value is required")
        End If

        'Start Range < End Range
        If CDbl(StrStartRange) >= CDbl(StrEndRange) Then
            Throw New Exception("Start Range value must be lower than End Range value")
        End If

        'Value
        ObjTransactionFrequency.Sort("Tier")
        Dim objtierbefore As TransactionFrequency
        Dim objtierafter As TransactionFrequency
        For Each Item As TransactionFrequency In ObjTransactionFrequency
            If Item.Tier.GetValueOrDefault(0) <> ObjEditTransactionFrequency.Tier.GetValueOrDefault(0) Then


                If Item.Tier.GetValueOrDefault(0) = CInt(StrTier) Then
                    Throw New Exception("Tier " & StrTier & " already exist")
                End If

                If Item.Tier.GetValueOrDefault(0) < CInt(StrTier) Then
                    objtierbefore = Item
                End If

                If objtierafter Is Nothing Then
                    If Item.Tier.GetValueOrDefault(0) > CInt(StrTier) Then
                        objtierafter = Item
                    End If
                End If
            End If
        Next

        If Not objtierbefore Is Nothing Then
            If CDbl(StrStartRange) <= objtierbefore.EndRange.GetValueOrDefault(0) Then
                Throw New Exception("Start Range For Tier " & StrTier & " : " & StrStartRange & " must be greater than tier " & objtierbefore.Tier.GetValueOrDefault(0) & " End Range :" & objtierbefore.EndRange.GetValueOrDefault(0))
            End If
            objtierbefore = Nothing
        End If

        If Not objtierafter Is Nothing Then
            If CDbl(StrEndRange) >= objtierafter.StartRange.GetValueOrDefault(0) Then
                Throw New Exception("End Range For Tier " & StrTier & " : " & StrEndRange & " must be lower than tier " & objtierafter.Tier.GetValueOrDefault(0) & " Start Range : " & objtierafter.StartRange.GetValueOrDefault(0))
            End If
            objtierafter = Nothing
        End If
    End Sub

    Public Sub IsDataValid(ByVal StrTier As String, ByVal StrStartRange As String, ByVal StrEndRange As String)
        'Required Field
        If StrTier = "" Then
            Throw New Exception("Tier value is required")
        End If
        If Not Integer.TryParse(StrTier, 0) Then
            Throw New Exception("Tier must be numeric")
        End If
        If Not Double.TryParse(StrStartRange, 0) Then
            Throw New Exception("Start range must be numeric")
        End If
        If StrStartRange = "" Then
            Throw New Exception("Start range value is required")
        End If
        If Not Double.TryParse(StrEndRange, 0) Then
            Throw New Exception("End range must be numeric")
        End If
        If StrEndRange = "" Then
            Throw New Exception("End range value is required")
        End If

        'Start Range < End Range
        If CDbl(StrStartRange) >= CDbl(StrEndRange) Then
            Throw New Exception("Start Range value must be lower than End Range value")
        End If

        'Value
        ObjTransactionFrequency.Sort("Tier")
        Dim objtierbefore As TransactionFrequency
        Dim objtierafter As TransactionFrequency
        For Each Item As TransactionFrequency In ObjTransactionFrequency
            If Item.Tier.GetValueOrDefault(0) = CInt(StrTier) Then
                Throw New Exception("Tier " & StrTier & " already exist")
            End If

            If Item.Tier.GetValueOrDefault(0) < CInt(StrTier) Then
                objtierbefore = Item
            End If

            If objtierafter Is Nothing Then
                If Item.Tier.GetValueOrDefault(0) > CInt(StrTier) Then
                    objtierafter = Item
                End If
            End If
        Next

        If Not objtierbefore Is Nothing Then
            If CDbl(StrStartRange) <= objtierbefore.EndRange.GetValueOrDefault(0) Then
                Throw New Exception("Start Range For Tier " & StrTier & " : " & StrStartRange & " must be greater than tier " & objtierbefore.Tier.GetValueOrDefault(0) & " End Range :" & objtierbefore.EndRange.GetValueOrDefault(0))
            End If
            objtierbefore = Nothing
        End If

        If Not objtierafter Is Nothing Then
            If CDbl(StrEndRange) >= objtierafter.StartRange.GetValueOrDefault(0) Then
                Throw New Exception("End Range For Tier " & StrTier & " : " & StrEndRange & " must be lower than tier " & objtierafter.Tier.GetValueOrDefault(0) & " Start Range : " & objtierafter.StartRange.GetValueOrDefault(0))
            End If
            objtierafter = Nothing
        End If
    End Sub

    Function CekExcludeEdit(ByVal objcek As TransactionFrequency) As Boolean
        If Not ObjEditTransactionFrequency Is Nothing Then
            If objcek.Tier.GetValueOrDefault(0) = ObjEditTransactionFrequency.Tier.GetValueOrDefault(0) Then
                Return False
            Else
                Return True
            End If
        Else
            Return True
        End If
    End Function

    Private Sub SaveEdit()
        Try
            Using ObjTransactionFrequencyBll As New TransactionFrequencyBLL
                If ObjTransactionFrequencyBll.SaveDirectly(Me.ObjTransactionFrequency) Then
                    LblConfirmation.Text = "Success to Edit Transaction Frequency."
                    MtvTransactionFrequency.ActiveViewIndex = 1
                End If
            End Using
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub SaveToApproval()
        Try
            Using ObjTransactionFrequencyBll As New TransactionFrequencyBLL
                If ObjTransactionFrequencyBll.SaveToApproval(ObjTransactionFrequency) Then
                    LblConfirmation.Text = "Transaction Frequency parameter has been changed and it is currently waiting for approval."
                    MtvTransactionFrequency.ActiveViewIndex = 1
                End If
            End Using
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBtnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnSave.Click
        Try
            'cek apakah sudah ada di pending approval
            Dim IntCount As Integer = 0
            DataRepository.TransactionFrequency_ApprovalProvider.GetPaged("", "", 0, 1, IntCount)

            If IntCount = 0 Then
                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                    SaveEdit()
                Else
                    SaveToApproval()
                End If
            Else
                Throw New Exception("Cannot edit Transaction Frequency because it is currently waiting for approval.")
            End If
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBackAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBackAdd.Click
        Try
            Response.Redirect("Default.aspx", False)
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBack.Click
        Try
            Response.Redirect("Default.aspx", False)
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub ClearControl()
        TxtTier.Text = ""
        TxtStartRange.Text = ""
        TxtEndRange.Text = ""
        txtToleransi.Text = ""
    End Sub

    Protected Sub ImgBtnAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAdd.Click
        Try
            ClearControl()
            MtvTransactionFrequency.ActiveViewIndex = 0
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    'Protected Sub CvalHandleErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles CvalHandleErr.PreRender
    '    If CvalHandleErr.IsValid = False Then
    '        Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
    '    End If
    'End Sub

    'Protected Sub CvalPageErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles CvalPageErr.PreRender
    '    If CvalPageErr.IsValid = False Then
    '        Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
    '    End If
    'End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            If MtvTransactionFrequency.ActiveViewIndex = 0 Then
                ImgBtnSave.Visible = True
                ImgBackAdd.Visible = True
            Else
                ImgBtnSave.Visible = False
                ImgBackAdd.Visible = False
            End If
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Function BindAmount() As Boolean
        Me.GrdVwTransactionFrequency.DataSource = Me.ObjTransactionFrequency
        Me.GrdVwTransactionFrequency.DataBind()
    End Function

    Protected Sub GrdVwTransactionFrequency_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GrdVwTransactionFrequency.RowDataBound
        Dim PKMsAction As String = Nothing
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim objData As TransactionFrequency = CType(e.Row.DataItem, TransactionFrequency)
                e.Row.Cells(0).Text = objData.Tier.ToString
                e.Row.Cells(1).Text = objData.StartRange.ToString
                e.Row.Cells(2).Text = objData.EndRange.ToString

                Dim lnkButtonRemove As LinkButton = CType(e.Row.FindControl("lnkButtonRemove"), LinkButton)
                lnkButtonRemove.CommandName = "Remove"
                lnkButtonRemove.CommandArgument = objData.Tier.ToString

                Dim LinkBtnEdit As LinkButton = CType(e.Row.FindControl("LinkBtnEdit"), LinkButton)
                LinkBtnEdit.CommandName = "EditData"
                LinkBtnEdit.CommandArgument = objData.Tier.ToString
            End If
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImageAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAdd.Click
        Try
            If ObjEditTransactionFrequency Is Nothing Then
                Me.IsDataValid(Me.TxtTier.Text, Me.TxtStartRange.Text, Me.TxtEndRange.Text)

                Dim ObjTransAmount As New TransactionFrequency
                With ObjTransAmount
                    .Tier = CType(TxtTier.Text.Trim, Global.System.Nullable(Of Integer))
                    .StartRange = CType(TxtStartRange.Text.Trim, Global.System.Nullable(Of Decimal))
                    .EndRange = CType(TxtEndRange.Text.Trim, Global.System.Nullable(Of Decimal))
                    .Toleransi = CType(txtToleransi.Text, Global.System.Nullable(Of Double))
                End With

                ObjTransactionFrequency.Add(ObjTransAmount)

                BindAmount()
                ClearControl()
            Else
                Me.IsDataValidEdit(Me.TxtTier.Text, Me.TxtStartRange.Text, Me.TxtEndRange.Text)

                ObjEditTransactionFrequency.Tier = CType(TxtTier.Text.Trim, Global.System.Nullable(Of Integer))
                ObjEditTransactionFrequency.StartRange = CType(TxtStartRange.Text.Trim, Global.System.Nullable(Of Decimal))
                ObjEditTransactionFrequency.EndRange = CType(TxtEndRange.Text.Trim, Global.System.Nullable(Of Decimal))
                ObjEditTransactionFrequency.Toleransi = CType(txtToleransi.Text, Global.System.Nullable(Of Double))
                BindAmount()
                ClearControl()
                ObjEditTransactionFrequency = Nothing
                ImgBtnCancel.Visible = False
            End If

        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub GrdVwTransactionFrequency_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdVwTransactionFrequency.RowCommand
        If e.CommandName = "Remove" Then

            Dim idx As Integer = CInt(e.CommandArgument)
            Using ObjDeleted As TransactionFrequency = ObjTransactionFrequency.Find(TransactionFrequencyColumn.Tier, idx)
                If Not ObjDeleted Is Nothing Then
                    If Not ObjEditTransactionFrequency Is Nothing Then
                        If ObjDeleted.Tier.GetValueOrDefault(0) = ObjEditTransactionFrequency.Tier.GetValueOrDefault(0) Then
                            ImgBtnCancel_Click(ImgBtnCancel, Nothing)
                        End If
                    End If

                    ObjTransactionFrequency.Remove(ObjDeleted)
                End If
                BindAmount()
            End Using
        ElseIf e.CommandName = "EditData" Then
            Dim idx As Integer = CInt(e.CommandArgument)

            Using ObjEdit As TransactionFrequency = ObjTransactionFrequency.Find(TransactionFrequencyColumn.Tier, idx)
                If Not ObjEdit Is Nothing Then
                    ObjEditTransactionFrequency = ObjEdit
                    Loadeditdata()
                End If
            End Using
        End If
    End Sub

    Sub Loadeditdata()
        TxtTier.Text = CStr(ObjEditTransactionFrequency.Tier)
        TxtStartRange.Text = CStr(ObjEditTransactionFrequency.StartRange)
        TxtEndRange.Text = CStr(ObjEditTransactionFrequency.EndRange)
        txtToleransi.Text = CStr(ObjEditTransactionFrequency.Toleransi)
        ImgBtnCancel.Visible = True
    End Sub

    Protected Sub ImgBtnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnCancel.Click
        Try
            ClearControl()

            ObjEditTransactionFrequency = Nothing
            ImgBtnCancel.Visible = False
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message

        End Try
    End Sub
End Class