<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="LTKTApproval.aspx.vb" Inherits="LTKTApproval"   %>


<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
<script src="Script/popcalendar.js"></script>
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td>
                <img src="Images/blank.gif" width="5" height="1" /></td>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td></td><td width="99%" bgcolor="#FFFFFF">
                            <img src="Images/blank.gif" width="1" height="1" /></td>
                        <td bgcolor="#FFFFFF">
                            <img src="Images/blank.gif" width="1" height="1" /></td>
                           
                    </tr>
                </table>
            </td>
            <td>
                <img src="Images/blank.gif" width="5" height="1" /></td>
        </tr>
        <tr>
            <td>
            </td>
            <td id="tdcontent" height="99%" valign="top" bgcolor="#FFFFFF">
                <div id="divcontent" >
                    <table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td>
                                <img src="Images/blank.gif" width="20" height="100%" /></td>
                            <td class="divcontentinside" bgcolor="#FFFFFF">
                                <ajax:AjaxPanel ID="a" runat="server">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" HeaderText="There were uncompleteness on the page:"
                                        Width="95%" CssClass="validation">
                                    </asp:ValidationSummary>
                                </ajax:AjaxPanel>
                                <ajax:AjaxPanel ID="AjaxPanel5" runat="server" Width="100%">
                                    <asp:MultiView ID="MultiVwTransactionCodeView" runat="server" ActiveViewIndex="0">
                                        <asp:View ID="VwMasterData" runat="server">
                                            <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
                                                style="border-top-style: none; border-right-style: none; border-left-style: none;
                                                border-bottom-style: none" width="100%">
                                                <tr>
                                                    <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                                                        border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                        <img src="Images/dot_title.gif" width="17" height="17"><b><asp:Label ID="Label1"
                                                            runat="server" Text="LTKT - Approval"></asp:Label></b><hr />
                                                    </td>
                                                </tr>
                                            </table>
                                            <table border="0" cellpadding="0" cellspacing="0" style="background-color: White;
                                                border-color: White;" width="97%">
                                                <tr>
                                                    <td>
                                                        <table align="left" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                            bgcolor="#dddddd" border="2">
                                                            <tr>
                                                                <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                                    style="height: 6px">
                                                                    <table cellpadding="0" cellspacing="0" border="0">
                                                                        <tr>
                                                                            <td class="formtext">
                                                                                <asp:Label ID="Label9" runat="server" Text="Searching Criteria" Font-Bold="True"
                                                                                    ></asp:Label>&nbsp;</td>
                                                                            <td>
                                                                                <a href="#" onclick="javascript:ShowHidePanel('SearchBox','searchimage','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                                    title="click to minimize or maximize">
                                                                                    <img id="searchimage" src="Images/search-bar-minimize.gif" border="0" height="12px"
                                                                                        width="12px"></a></td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr id="SearchBox">
                                                                <td valign="top" bgcolor="#ffffff">
                                                                    <ajax:AjaxPanel ID="AjaxPanel1" runat="server" Width="100%">
                                                                        <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                            border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                            
                                                                            <tr>
                                                                                <td nowrap style="width: 15%; background-color: #FFF7E6;" valign="top">
                                                                                    <asp:Label ID="lblNIKStaff" runat="server" Text="Request By"></asp:Label></td>
                                                                                <td nowrap style="width: 75%;" valign="top">
                                                                                    <asp:TextBox ID="txtNIKStaff" TabIndex="1" runat="server" CssClass="searcheditbox"
                                                                                        Width="284px"></asp:TextBox></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td nowrap style="width: 15%; background-color: #FFF7E6;" valign="top">
                                                                                    <asp:Label ID="LblAction" runat="server" Text="Action"></asp:Label></td>
                                                                                <td nowrap style="width: 75%;" valign="top">
                                                                                    <asp:DropDownList ID="CboAction" runat="server" CssClass="comboBox">
                                                                                    </asp:DropDownList></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td nowrap style="width: 15%; background-color: #FFF7E6;" valign="top">
                                                                                    <asp:Label ID="LblRequestedDateSearch" runat="server" Text="Request Date"></asp:Label></td>
                                                                                <td nowrap style="width: 75%; " valign="top">
                                                                                    <asp:TextBox ID="txtStartDate" TabIndex="2" runat="server" CssClass="searcheditbox"
                                                                                        Width="100px"></asp:TextBox><input id="popUpStartDate"
                                                                                            title="Click to show calendar" style="border-right: #ffffff 0px solid; border-top: #ffffff 0px solid;
                                                                                            font-size: 11px; border-left: #ffffff 0px solid; border-bottom: #ffffff 0px solid;
                                                                                            height: 17px; background-image: url(Script/Calendar/cal.gif); width: 16px;" type="button"
                                                                                            onclick="popUpCalendar(this, frmBasicCTRWeb.txtStartDate, 'dd-mmm-yyyy')" runat="server">&nbsp;&nbsp;
                                                                                    <asp:Label ID="Label5" runat="server" Text="Until"></asp:Label>&nbsp;&nbsp;
                                                                                    <asp:TextBox ID="txtEndDate" TabIndex="2" runat="server" CssClass="searcheditbox"
                                                                                        Width="100px"></asp:TextBox><input id="popUpEndDate"
                                                                                            title="Click to show calendar" style="border-right: #ffffff 0px solid; border-top: #ffffff 0px solid;
                                                                                            font-size: 11px; border-left: #ffffff 0px solid; border-bottom: #ffffff 0px solid;
                                                                                            height: 17px; background-image: url(Script/Calendar/cal.gif); width: 16px;" type="button"
                                                                                            onclick="popUpCalendar(this, frmBasicCTRWeb.txtEndDate, 'dd-mmm-yyyy')" runat="server"></td>
                                                  
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="4">
                                                                                    <ajax:AjaxPanel ID="AjaxPanel3" runat="server">
                                                                                        <asp:ImageButton ID="BtnSearch" TabIndex="3" runat="server" ImageUrl="~/Images/button/Search.gif"
                                                                                            CausesValidation="False"></asp:ImageButton>&nbsp;&nbsp;<asp:ImageButton
                                                                                                ID="ImageClearSearch" runat="server" ImageUrl="~/Images/button/ClearSearch.gif"
                                                                                                /><asp:CustomValidator ID="cvalPageError"
                                                                                                    runat="server" Display="None"></asp:CustomValidator></ajax:AjaxPanel></td>
                                                                            </tr>
                                                                        </table>
                                                                    </ajax:AjaxPanel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table align="left" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                            bgcolor="#dddddd" border="2">
                                                            <tr>
                                                                <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                                    style="height: 6px">
                                                                    <table cellpadding="0" cellspacing="0" border="0">
                                                                        <tr>
                                                                            <td class="formtext" style="height: 14px">
                                                                                <asp:Label ID="Label8" runat="server" Text="Searching Result" Font-Bold="True"></asp:Label>&nbsp;</td>
                                                                            <td style="height: 14px">
                                                                                <a href="#" onclick="javascript:ShowHidePanel('TrGridData','ImgGriddData','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                                    title="click to minimize or maximize">
                                                                                    <img id="ImgGriddData" src="Images/search-bar-minimize.gif" border="0" height="12px"
                                                                                        width="12px"></a></td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr id="TrGridData">
                                                                <td valign="top" width="98%" bgcolor="#ffffff">
                                                                    <table cellspacing="4" cellpadding="0" width="100%" border="0">
                                                                        <tr>
                                                                            <td valign="middle" width="100%" nowrap>
                                                                                <table style="width: 100%; height: 100%">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <ajax:AjaxPanel ID="AjaxPanel4" runat="server" Width="100%">
                                                                                                <asp:DataGrid ID="GridLTKTApprovalList" runat="server" SkinID="gridview" AutoGenerateColumns="False"
                                                                                                    Font-Size="XX-Small" CellPadding="4" AllowPaging="True" Width="100%" GridLines="Vertical"
                                                                                                    AllowSorting="True" ForeColor="Black" Font-Bold="False" Font-Italic="False"
                                                                                                    Font-Overline="False" Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left"
                                                                                                    BorderWidth="1px" BackColor="White" BorderColor="#DEDFDE" 
                                                                                                    BorderStyle="None">
                                                                                                    <AlternatingItemStyle BackColor="White" />
                                                                                                    <Columns>
                                                                                                        <asp:BoundColumn DataField="PK_LTKT_Approval_Id" Visible="False">
                                                                                                            <HeaderStyle Width="0%" />
                                                                                                        </asp:BoundColumn>
                                                                                                        <asp:BoundColumn HeaderText="No.">
                                                                                                            <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                                                Font-Underline="False" ForeColor="White" Width="2%" HorizontalAlign="Center"
                                                                                                                VerticalAlign="Top" Wrap="False" />
                                                                                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                                                Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Top" Wrap="False" />
                                                                                                        </asp:BoundColumn>
                                                                                                        <asp:BoundColumn DataField="UserName" HeaderText="Requested By" 
                                                                                                            SortExpression="UserName desc">
                                                                                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                                                Font-Underline="False" Wrap="False" HorizontalAlign="Left" VerticalAlign="Top" />
                                                                                                            <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                                                Font-Underline="False" Wrap="False" HorizontalAlign="Left" Width="20%" VerticalAlign="Top" />
                                                                                                        </asp:BoundColumn>
                                                                                                        <asp:BoundColumn DataField="Nama" HeaderText="Action" 
                                                                                                            SortExpression="Nama desc">
                                                                                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                                                Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Top" Wrap="False" />
                                                                                                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                                                Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Top" Width="20%" Wrap="False" />
                                                                                                        </asp:BoundColumn>
                                                                                                        <asp:BoundColumn DataField="RequestedDate" HeaderText="Requested Date" SortExpression="RequestedDate desc">
                                                                                                            <HeaderStyle Width="20%" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                                                                                                Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Top"
                                                                                                                Wrap="False" />
                                                                                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                                                Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Top" Wrap="False" />
                                                                                                        </asp:BoundColumn>
                                                                                                        <asp:BoundColumn DataField="RequestedBy" Visible="False" 
                                                                                                            SortExpression="RequestedBy desc"></asp:BoundColumn>
                                                                                                        <asp:TemplateColumn>
                                                                                                            <ItemTemplate>
                                                                                                                <asp:LinkButton ID="lnkDetail" runat="server" CommandName="detail">Detail</asp:LinkButton>
                                                                                                            </ItemTemplate>
                                                                                                        </asp:TemplateColumn>
                                                                                                    </Columns>
                                                                                                    <PagerStyle Visible="False" HorizontalAlign="Right" ForeColor="Black" 
                                                                                                        BackColor="#F7F7DE" Mode="NumericPages">
                                                                                                    </PagerStyle>
                                                                                                    <ItemStyle BorderColor="#CC9966" BorderStyle="Solid" BackColor="#F7F7DE" />
                                                                                                    <FooterStyle BackColor="#CCCC99" />
                                                                                                    <HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#6B696B" />
                                                                                                    <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                                                                </asp:DataGrid></ajax:AjaxPanel></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="background-color: #ffffff; height: 5px;">
                                                                                            &nbsp;</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td bgcolor="#ffffff">
                                                                                            <table id="Table3" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                                                                bgcolor="#ffffff" border="2">
                                                                                                <tr class="regtext" align="center" bgcolor="#dddddd">
                                                                                                    <td valign="top" align="left" width="50%" bgcolor="#ffffff">
                                                                                                        Page&nbsp;<ajax:AjaxPanel ID="AjaxPanel6" runat="server"><asp:Label
                                                                                                            ID="PageCurrentPage" runat="server" CssClass="regtext" Text="0"></asp:Label>&nbsp;of&nbsp;<asp:Label
                                                                                                                ID="PageTotalPages" runat="server" CssClass="regtext" Text="0"></asp:Label></ajax:AjaxPanel></td>
                                                                                                    <td valign="top" align="right" width="50%" bgcolor="#ffffff">
                                                                                                        Total Records&nbsp;<ajax:AjaxPanel ID="AjaxPanel7" runat="server"><asp:Label
                                                                                                            ID="PageTotalRows" runat="server" Text="0"></asp:Label></ajax:AjaxPanel></td>
                                                                                                </tr>
                                                                                            </table>
                                                                                            <table id="Table4" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                                                                bgcolor="#ffffff" border="2">
                                                                                                <tr bgcolor="#ffffff">
                                                                                                    <td class="regtext" valign="middle" align="left" colspan="11" height="7">
                                                                                                        <hr>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="regtext" valign="middle" align="left" width="63" bgcolor="#ffffff" style="height: 86px">
                                                                                                        Go to page</td>
                                                                                                    <td class="regtext" valign="middle" align="left" width="5" bgcolor="#ffffff" style="height: 86px">
                                                                                                        <font face="Verdana, Arial, Helvetica, sans-serif" size="1">
                                                                                                            <ajax:AjaxPanel ID="AjaxPanel8" runat="server">
                                                                                                                <asp:TextBox ID="TextGoToPage" runat="server" Width="38px" CssClass="searcheditbox"
                                                                                                                    ></asp:TextBox></ajax:AjaxPanel></font></td>
                                                                                                    <td class="regtext" valign="middle" align="left" bgcolor="#ffffff" style="height: 86px">
                                                                                                        <ajax:AjaxPanel ID="AjaxPanel9" runat="server">
                                                                                                            <asp:ImageButton ID="ImageButtonGo" runat="server" SkinID="GoButton" ImageUrl="~/Images/button/go.gif"
                                                                                                                ></asp:ImageButton></ajax:AjaxPanel></td>
                                                                                                    <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff" style="height: 86px">
                                                                                                        <img height="5" src="images/first.gif" width="6"></td>
                                                                                                    <td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff" style="height: 86px">
                                                                                                        <ajax:AjaxPanel ID="AjaxPanel10" runat="server">
                                                                                                            <asp:LinkButton ID="LinkButtonFirst" runat="server" CssClass="regtext" CommandName="First"
                                                                                                                OnCommand="PageNavigate" Text="First"></asp:LinkButton></ajax:AjaxPanel></td>
                                                                                                    <td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff" style="height: 86px">
                                                                                                        <img height="5" src="images/prev.gif" width="6"></td>
                                                                                                    <td class="regtext" valign="middle" align="right" width="14" bgcolor="#ffffff" style="height: 86px">
                                                                                                        <ajax:AjaxPanel ID="AjaxPanel11" runat="server">
                                                                                                            <asp:LinkButton ID="LinkButtonPrevious" runat="server" CssClass="regtext" CommandName="Prev"
                                                                                                                OnCommand="PageNavigate" Text="Previous"></asp:LinkButton></ajax:AjaxPanel></td>
                                                                                                    <td class="regtext" valign="middle" align="right" width="60" bgcolor="#ffffff" style="height: 86px">
                                                                                                        <ajax:AjaxPanel ID="AjaxPanel12" runat="server">
                                                                                                            <a class="pageNav" href="#">
                                                                                                                <asp:LinkButton ID="LinkButtonNext" runat="server" CssClass="regtext" CommandName="Next"
                                                                                                                    OnCommand="PageNavigate" Text="Next"></asp:LinkButton></a></ajax:AjaxPanel></td>
                                                                                                    <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff" style="height: 86px">
                                                                                                        <img height="5" src="images/next.gif" width="6"></td>
                                                                                                    <td class="regtext" valign="middle" align="left" width="25" bgcolor="#ffffff" style="height: 86px">
                                                                                                        <ajax:AjaxPanel ID="AjaxPanel13" runat="server">
                                                                                                            <asp:LinkButton ID="LinkButtonLast" runat="server" CssClass="regtext" CommandName="Last"
                                                                                                                OnCommand="PageNavigate" Text="Last"></asp:LinkButton></ajax:AjaxPanel></td>
                                                                                                    <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff" style="height: 86px">
                                                                                                        <img height="5" src="images/last.gif" width="6"></td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:View>
                                        &nbsp;
                                    </asp:MultiView></ajax:AjaxPanel></td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td background="Images/button-bground.gif" align="left" valign="middle">
                            <img src="Images/blank.gif" width="5" height="1" /></td>
                        <td background="Images/button-bground.gif" align="left" valign="middle">
                            <img src="images/arrow.gif" width="15" height="15" />&nbsp;&nbsp;</td>
                            <td background="Images/button-bground.gif" align="left" valign="middle">
                            </td>
                            <td background="Images/button-bground.gif" align="left" valign="middle">
                            </td>
                        <td background="Images/button-bground.gif" align="left" valign="middle">
                            <ajax:AjaxPanel ID="AjaxPanel22" runat="server"></ajax:AjaxPanel></td>
                        <td width="99%" background="Images/button-bground.gif">
                            <img src="Images/blank.gif" width="1" height="1" /></td>
                        <td>
                            </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
