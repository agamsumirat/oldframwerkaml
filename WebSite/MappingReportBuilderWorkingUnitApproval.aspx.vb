Option Strict On
Option Explicit On
Imports amlbll
Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports SahassaNettier.Services
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper


Imports System.IO
Imports System.Collections.Generic

Partial Class MappingReportBuilderWorkingUnitApproval
    Inherits Parent

    Public Property PK_MappingreportBuilderWorkingUnit_approval_ID() As String
        Get
            Return CType(Session("MappingReportBuilderWorkingUnitApproval.PK_MappingreportBuilderWorkingUnit_approval_ID"), String)
        End Get
        Set(ByVal value As String)
            Session("MappingReportBuilderWorkingUnitApproval.PK_MappingreportBuilderWorkingUnit_approval_ID") = value
        End Set
    End Property

    Public ReadOnly Property ObjWorkingUnit() As TList(Of WorkingUnit)
        Get
            If Session("MappingReportBuilderWorkingUnitApproval.ObjWorkingUnitdata") Is Nothing Then
                Session("MappingReportBuilderWorkingUnitApproval.ObjWorkingUnitdata") = New TList(Of WorkingUnit)
            End If
            Return CType(Session("MappingReportBuilderWorkingUnitApproval.ObjWorkingUnitdata"), TList(Of WorkingUnit))
        End Get
    End Property

    Public Property SetnGetQueryName() As String
        Get
            If Not Session("MappingReportBuilderWorkingUnitApproval.SetnGetQueryName") Is Nothing Then
                Return CStr(Session("MappingReportBuilderWorkingUnitApproval.SetnGetQueryName"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("MappingReportBuilderWorkingUnitApproval.SetnGetQueryName") = value
        End Set
    End Property

    Public Property SetnGetWorkingUnitName() As String
        Get
            If Not Session("MappingReportBuilderWorkingUnitApproval.SetnGetWorkingUnitName") Is Nothing Then
                Return CStr(Session("MappingReportBuilderWorkingUnitApproval.SetnGetWorkingUnitName"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("MappingReportBuilderWorkingUnitApproval.SetnGetWorkingUnitName") = value
        End Set
    End Property


    Public Property SetnGetMode() As String
        Get
            If Not Session("MappingReportBuilderWorkingUnitApproval.SetnGetMode") Is Nothing Then
                Return CStr(Session("MappingReportBuilderWorkingUnitApproval.SetnGetMode"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("MappingReportBuilderWorkingUnitApproval.SetnGetMode") = value
        End Set
    End Property

    Public Property SetnGetPreparer() As String
        Get
            If Not Session("MappingReportBuilderWorkingUnitApproval.SetnGetPreparer") Is Nothing Then
                Return CStr(Session("MappingReportBuilderWorkingUnitApproval.SetnGetPreparer"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("MappingReportBuilderWorkingUnitApproval.SetnGetPreparer") = value
        End Set
    End Property

    Public Property SetnGetEntryDateFrom() As String
        Get
            If Not Session("MappingReportBuilderWorkingUnitApproval.CreatedDate") Is Nothing Then
                Return CStr(Session("MappingReportBuilderWorkingUnitApproval.CreatedDate"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("MappingReportBuilderWorkingUnitApproval.CreatedDate") = value
        End Set
    End Property

    Public Property SetnGetEntryDateUntil() As String
        Get
            If Not Session("MappingReportBuilderWorkingUnitApproval.CreatedDateUntil") Is Nothing Then
                Return CStr(Session("MappingReportBuilderWorkingUnitApproval.CreatedDateUntil"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("MappingReportBuilderWorkingUnitApproval.CreatedDateUntil") = value
        End Set
    End Property

    Private Property SetnGetSort() As String
        Get
            Return CType(IIf(Session("MappingReportBuilderWorkingUnitApproval.Sort") Is Nothing, "QueryName  asc", Session("MappingReportBuilderWorkingUnitApproval.Sort")), String)
        End Get
        Set(ByVal Value As String)
            Session("MappingReportBuilderWorkingUnitApproval.Sort") = Value
        End Set
    End Property

    Private Property SetnGetRowTotal() As Int32
        Get
            Return CType(IIf(Session("MappingReportBuilderWorkingUnitApproval.RowTotal") Is Nothing, 0, Session("MappingReportBuilderWorkingUnitApproval.RowTotal")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("MappingReportBuilderWorkingUnitApproval.RowTotal") = Value
        End Set
    End Property
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property

    Private Property SetnGetCurrentPage() As Int32
        Get
            Return CType(IIf(Session("MappingreportBuilderWorkingUnit_approvalView.CurrentPage") Is Nothing, 0, Session("MappingreportBuilderWorkingUnit_approvalView.CurrentPage")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("MappingreportBuilderWorkingUnit_approvalView.CurrentPage") = Value
        End Set
    End Property

    Public Shared Function FORMAT_DATE_BEGIN_GREATER_THAN_DATE_END(ByVal strTanggal As String) As String
        If Not Sahassa.AML.Commonly.SessionLanguage Is Nothing Then
            If Sahassa.AML.Commonly.SessionLanguage.ToLower = "en-us" Then
                Return "Start Date must Earlier than End Date for " & strTanggal
            ElseIf Sahassa.AML.Commonly.SessionLanguage.ToLower = "id-id" Then
                Return "Tanggal Mulai harus lebih Muda dari pada Tanggal Akhir untuk " & strTanggal
            Else
                Return "Start Date must Earlier than End Date for " & strTanggal
            End If

        Else
            Return "Start Date must Earlier than End Date for " & strTanggal
        End If
    End Function

    Function Getvw_mappingreportBuilderWorkingUnitapproval(ByVal strWhereClause As String, ByVal strOrderBy As String, ByVal intStart As Integer, ByVal intPageLength As Integer, ByRef intCount As Integer) As VList(Of vw_MappingReportBuilderWorkingUnitApproval)
        Dim Objvw_mappingreportBuilderWorkingUnitapproval As VList(Of vw_MappingReportBuilderWorkingUnitApproval) = Nothing
        Objvw_mappingreportBuilderWorkingUnitapproval = DataRepository.vw_MappingReportBuilderWorkingUnitApprovalProvider.GetPaged(strWhereClause, strOrderBy, intStart, intPageLength, intCount)
        Return Objvw_mappingreportBuilderWorkingUnitapproval
    End Function

    Public Property SetnGetBindTable() As VList(Of vw_MappingReportBuilderWorkingUnitApproval)
        Get
            Dim strWhereClause(-1) As String
            Dim strAllWhereClause As String = ""
            If Me.SetnGetCurrentPage > Me.GetPageTotal - 1 And Me.GetPageTotal - 1 > 0 Then
                Me.SetnGetCurrentPage = Me.GetPageTotal - 1
            End If
            If SetnGetQueryName.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "QueryName like '%" & SetnGetQueryName.Trim.Replace("'", "''") & "%'"
            End If
         
            If SetnGetMode.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "modename like '%" & SetnGetMode.Trim.Replace("'", "''") & "%'"
            End If
            If SetnGetPreparer.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "Preparer like '%" & SetnGetPreparer.Trim.Replace("'", "''") & "%'"
            End If

            If SetnGetEntryDateFrom.Length > 0 AndAlso SetnGetEntryDateUntil.Length > 0 Then

                If Sahassa.AML.Commonly.IsDateValid("dd-MM-yyyy", SetnGetEntryDateFrom) AndAlso Sahassa.AML.Commonly.IsDateValid("dd-MM-yyyy", SetnGetEntryDateFrom) Then

                    Dim tanggal As String = Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", SetnGetEntryDateFrom).ToString("yyyy-MM-dd")
                    Dim tanggalAkhir As String = Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", SetnGetEntryDateUntil).ToString("yyyy-MM-dd")
                    If DateDiff(DateInterval.Day, Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", SetnGetEntryDateFrom), Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", SetnGetEntryDateUntil)) > -1 Then

                        ReDim Preserve strWhereClause(strWhereClause.Length)

                        strWhereClause(strWhereClause.Length - 1) = vw_MappingReportBuilderWorkingUnitApprovalColumn.CreatedDate.ToString & " between '" & tanggal & " 00:00:00' and '" & tanggalAkhir & " 23:59:59'"
                    Else
                        Throw New SahassaException(FORMAT_DATE_BEGIN_GREATER_THAN_DATE_END("CreatedDate"))
                    End If
                End If
            End If

            strAllWhereClause = String.Join(" and ", strWhereClause)
            If strAllWhereClause.Trim.Length > 0 Then
                strAllWhereClause += " and FK_MsUserID <> " & Sahassa.AML.Commonly.SessionPkUserId & " AND FK_MsUserID IN (SELECT u.pkUserID FROM [User] u WHERE u.UserID IN(select UserId from UserWorkingUnitAssignment Where WorkingUnitId in (select WorkingUnitId from UserWorkingUnitAssignment where UserId = '" & Sahassa.AML.Commonly.SessionUserId & "')))"
            Else
                strAllWhereClause += " FK_MsUserID <> " & Sahassa.AML.Commonly.SessionPkUserId & " AND FK_MsUserID IN (SELECT u.pkUserID FROM [User] u WHERE u.UserID IN(select UserId from UserWorkingUnitAssignment Where WorkingUnitId in (select WorkingUnitId from UserWorkingUnitAssignment where UserId = '" & Sahassa.AML.Commonly.SessionUserId & "')))"
            End If
            Session("MappingReportBuilderWorkingUnitApproval.Table") = Getvw_mappingreportBuilderWorkingUnitapproval(strAllWhereClause, SetnGetSort, SetnGetCurrentPage, Sahassa.AML.Commonly.GetDisplayedTotalRow, Me.SetnGetRowTotal)


            Return CType(Session("MappingReportBuilderWorkingUnitApproval.Table"), VList(Of vw_MappingReportBuilderWorkingUnitApproval))


        End Get
        Set(ByVal value As VList(Of vw_MappingReportBuilderWorkingUnitApproval))
            Session("MappingReportBuilderWorkingUnitApproval.Table") = value
        End Set
    End Property



    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select            
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub ClearThisPageSessions()
        'Session("MappingreportBuilderWorkingUnit_approvalView.PK_MappingUserIdSTRPPATK_ApprovalDetail_ID") = Nothing
        Session("MappingReportBuilderWorkingUnitApproval.SetnGetUserName") = Nothing
        Session("MappingReportBuilderWorkingUnitApproval.ObjWorkingUnitdata") = Nothing
        Session("MappingReportBuilderWorkingUnitApproval.SetnGetQueryName") = Nothing
        Session("MappingReportBuilderWorkingUnitApproval.SetnGetWorkingUnitName") = Nothing
        Session("MappingReportBuilderWorkingUnitApproval.SetnGetMode") = Nothing
        Session("MappingReportBuilderWorkingUnitApproval.Table") = Nothing
        Session("MappingReportBuilderWorkingUnitApproval.SetnGetPreparer") = Nothing
        Session("MappingReportBuilderWorkingUnitApproval.CreatedDate") = Nothing
        Session("MappingReportBuilderWorkingUnitApproval.CreatedDateUntil") = Nothing

        SetnGetCurrentPage = Nothing
        Me.SetnGetRowTotal = Nothing
        Me.SetnGetSort = Nothing


    End Sub


    Private Sub bindgrid()
        SettingControlSearching()
        Me.GridRBWU.DataSource = Me.SetnGetBindTable
        Me.GridRBWU.CurrentPageIndex = Me.SetnGetCurrentPage
        Me.GridRBWU.VirtualItemCount = Me.SetnGetRowTotal
        Me.GridRBWU.DataBind()

        'If SetnGetRowTotal > 0 Then
        '    LabelNoRecordFound.Visible = False
        'Else
        '    LabelNoRecordFound.Visible = True
        'End If
    End Sub

    Private Sub SettingPropertySearching()

        SetnGetQueryName = TxtQueryName.Text.Trim
        SetnGetMode = TxtNama.Text.Trim

        SetnGetPreparer = TxtPreparer.Text.Trim
        Me.SetnGetEntryDateFrom = TxtEntryDateFrom.Text.Trim
        Me.SetnGetEntryDateUntil = TxtEntryDateUntil.Text.Trim

    End Sub

    Private Sub SettingControlSearching()

        TxtQueryName.Text = SetnGetQueryName
        TxtNama.Text = SetnGetMode

        TxtPreparer.Text = SetnGetPreparer
        TxtEntryDateFrom.Text = SetnGetEntryDateFrom
        TxtEntryDateUntil.Text = SetnGetEntryDateUntil

    End Sub



    Private Sub SetInfoNavigate()

        Me.PageCurrentPage.Text = (Me.SetnGetCurrentPage + 1).ToString
        Me.PageTotalPages.Text = Me.GetPageTotal.ToString
        Me.PageTotalRows.Text = Me.SetnGetRowTotal.ToString
        Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
        Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
        Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
        Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0

    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

        Try
            Me.bindgrid()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try

    End Sub

    Protected Sub ImageButtonSearchCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearchCancel.Click
        Try

            SetnGetQueryName = Nothing
            SetnGetMode = Nothing
            SetnGetWorkingUnitName = Nothing
            SetnGetPreparer = Nothing
            Me.SetnGetEntryDateFrom = Nothing
            Me.SetnGetEntryDateUntil = Nothing

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Try
            SettingPropertySearching()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then

                ClearThisPageSessions()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Me.popUpEntryDate.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TxtEntryDateFrom.ClientID & "'), 'dd-mm-yyyy')")
                Me.popUpEntryDateUntil.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TxtEntryDateUntil.ClientID & "'), 'dd-mm-yyyy')")

                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using

                bindgrid()

            End If
            GridRBWU.PageSize = CInt(Sahassa.AML.Commonly.GetDisplayedTotalRow)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub GridRBWU_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridRBWU.EditCommand
        Dim PK_MappingreportBuilderWorkingUnit_approval_ID As Integer 'primary key
        'Dim StrUserID As String  'unik
        Try
            PK_MappingreportBuilderWorkingUnit_approval_ID = CType(e.Item.Cells(1).Text, Integer)
            'StrUserID = e.Item.Cells(2).Text

            Response.Redirect("MappingreportBuilderWorkingUnitapprovalDetail.aspx?PK_MappingReportBuilderWorkingUnit_Approval_ID=" & PK_MappingreportBuilderWorkingUnit_approval_ID, False)
            'Using ObjMsBranchBll As New SahassaBLL.MsHelpBLL
            '    If ObjMsBranchBll.IsNotExistInApproval(StrUserID) Then
            '        Response.Redirect("MsHelpEdit.aspx?PkMsHelpID=" & PkMsHelpID, False)
            '    End If
            'End Using

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub GridRBWU_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridRBWU.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Session("MappingReportBuilderWorkingUnitApproval.ObjWorkingUnitdata") = Nothing
                Session("MappingreportBuilderWorkingUnit_approvalDelete.ObjWorkingUnitdata") = Nothing

                Dim EditButton As LinkButton = CType(e.Item.Cells(7).FindControl("LnkEdit"), LinkButton)

                Dim ObjGridReport As DataGrid = CType(e.Item.FindControl("GridView"), DataGrid)
                Dim PKAuditProjectID As Integer = CInt(e.Item.Cells(1).Text)
                Dim ObjPkAuditProjectID() As Object = {PKAuditProjectID}

                Dim iii As Integer = 0

                Dim ObjMappingreportBuilderWorkingUnit_approvalmap As TList(Of mappingreportBuilderWorkingUnit_approvalDetailMap)
                ObjMappingreportBuilderWorkingUnit_approvalmap = DataRepository.mappingreportBuilderWorkingUnit_approvalDetailMapProvider.GetPaged(mappingreportBuilderWorkingUnit_approvalDetailMapColumn.FK_MappingReportBuilderWorkingUnit_Approval_ID.ToString & " = " & PKAuditProjectID, "", 0, Integer.MaxValue, 0)

                Do While iii < ObjMappingreportBuilderWorkingUnit_approvalmap.Count And iii < 10
                    Using ObjWorkingUnitlist As New WorkingUnit
                        Using objWorkingUnit As WorkingUnit = DataRepository.WorkingUnitProvider.GetByWorkingUnitID(CInt(ObjMappingreportBuilderWorkingUnit_approvalmap(iii).FK_WorkingUnit_ID))
                            ObjWorkingUnitlist.WorkingUnitName = objWorkingUnit.WorkingUnitName
                            ObjWorkingUnitlist.WorkingUnitID = objWorkingUnit.WorkingUnitID
                        End Using
                        ObjWorkingUnit.Add(ObjWorkingUnitlist)
                    End Using
                    iii = iii + 1
                Loop


                ObjGridReport.DataSource = ObjWorkingUnit
                'ObjGridReport.DataKeyField = MappingreportBuilderWorkingUnit_approvalmapColumn.PK_MappingreportBuilderWorkingUnit_approval_Map_ID.ToString
                ObjGridReport.DataBind()

                If Not ObjMappingreportBuilderWorkingUnit_approvalmap Is Nothing Then
                    ObjMappingreportBuilderWorkingUnit_approvalmap.Dispose()
                    ObjMappingreportBuilderWorkingUnit_approvalmap = Nothing
                End If



                e.Item.Cells(0).Text = CStr((e.Item.ItemIndex + 1))
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try            
            If IsNumeric(Me.TextGoToPage.Text) AndAlso (CInt(Me.TextGoToPage.Text) > 0) Then
                If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                    Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                Else
                    Throw New Exception(Sahassa.AML.CommonlyEnum.EXECPTION_PAGENUMBER_GREATER_TOTALPAGENUMBER)
                End If
            Else
                Throw New Exception(Sahassa.AML.CommonlyEnum.EXECPTION_PAGENUMBER_NUMERICPOSITIF)
            End If

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridRBWU_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridRBWU.SortCommand
        Dim GridUser As DataGrid = CType(source, DataGrid)
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class




