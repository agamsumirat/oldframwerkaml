#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports Sahassa.AML
Imports System.Collections.Generic
#End Region

Partial Class MsNegara_APPROVAL_Detail
	Inherits Parent

#Region "Function"

	Sub HideControl()
		imgReject.Visible = False
		imgApprove.Visible = False
		ImageCancel.Visible = False
	End Sub

	Sub ChangeMultiView(index As Integer)
		MtvMsUser.ActiveViewIndex = index
	End Sub

#End Region

#Region "events..."

	Protected Sub ImageCancel_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
		Response.Redirect("MsNegara_Approval_View.aspx")
	End Sub

	Protected Sub imgReject_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles imgReject.Click
		Try
            Rejected()
            ChangeMultiView(1)
            LblConfirmation.Text = "Data has been rejected"
            HideControl()
		Catch ex As Exception
			LogError(ex)
			CvalPageErr.IsValid = False
			CvalPageErr.ErrorMessage = ex.Message
		End Try
	End Sub

	Protected Sub imgApprove_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles imgApprove.Click
		Try
			Using ObjMsNegara_Approval As MsNegara_Approval = DataRepository.MsNegara_ApprovalProvider.GetByPK_MsNegara_Approval_Id(parID)
				If ObjMsNegara_Approval.FK_MsMode_Id.GetValueOrDefault = 1 Then
					Inserdata(ObjMsNegara_Approval)
				ElseIf ObjMsNegara_Approval.FK_MsMode_Id.GetValueOrDefault = 2 Then
					UpdateData(ObjMsNegara_Approval)
				ElseIf ObjMsNegara_Approval.FK_MsMode_Id.GetValueOrDefault = 3 Then
					DeleteData(ObjMsNegara_Approval)
				End If
			End Using
			ChangeMultiView(1)
			LblConfirmation.Text = "Data approved successful"
            HideControl()
		Catch ex As Exception
			LogError(ex)
			CvalPageErr.IsValid = False
			CvalPageErr.ErrorMessage = ex.Message
		End Try

	End Sub

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Not Page.IsPostBack Then
			Try
                'AuditTrailBLL.InsertAuditTrailUserAccess(SessionCurrentPage)
				ListmapingNew = New TList(Of MappingMsNegaraNCBSPPATK_Approval_Detail)
				ListmapingOld = New TList(Of MappingMsNegaraNCBSPPATK)
				LoadData()
			Catch ex As Exception
				LogError(ex)
				CvalPageErr.IsValid = False
				CvalPageErr.ErrorMessage = ex.Message
			End Try
		End If

	End Sub

	Private Sub LoadData()
		Dim PkObject As String
		'Load new data
		Using ObjMsNegara_ApprovalDetail As MsNegara_ApprovalDetail = DataRepository.MsNegara_ApprovalDetailProvider.GetPaged(MsNegara_ApprovalDetailColumn.FK_MsNegara_Approval_Id.ToString & _
			"=" & _
			parID, "", 0, 1, Nothing)(0)
            Dim CekMode As MsNegara_Approval = DataRepository.MsNegara_ApprovalProvider.GetByPK_MsNegara_Approval_Id(ObjMsNegara_ApprovalDetail.FK_MsNegara_Approval_Id)
            Dim nama As String = CekMode.FK_MsMode_Id
            If nama = 1 Then
                Baru.Visible = True
                lama.Visible = False
                baruNew.Visible = True
                lamaOld.Visible = False
            ElseIf nama = 2 Then
                Baru.Visible = True
                lama.Visible = True
                baruNew.Visible = True
                lamaOld.Visible = True
            ElseIf nama = 3 Then
                Baru.Visible = True
                lama.Visible = False
                baruNew.Visible = True
                lamaOld.Visible = False
            ElseIf nama = 4 Then
                Baru.Visible = True
                lama.Visible = False
                baruNew.Visible = True
                lamaOld.Visible = False
            End If
			With ObjMsNegara_ApprovalDetail
				SafeDefaultValue = "-"
			   txtIDNegaraNew.Text = Safe(.IDNegara)
txtNamaNegaraNew.Text = Safe(.NamaNegara)


			       'other info
                Dim Omsuser As TList(Of User)
                Omsuser = DataRepository.UserProvider.GetByUserID(.CreatedBy)
                If Omsuser.Count > 0 Then
                    lblCreatedByNew.Text = Omsuser(0).UserName
                Else
                    lblCreatedByNew.Text = SafeDefaultValue
                End If
                lblCreatedDateNew.Text = FormatDate(.CreatedDate)
                Omsuser = DataRepository.UserProvider.GetByUserID(.LastUpdatedBy)
                If Omsuser.Count > 0 Then
                    lblUpdatedbyNew.Text = Omsuser(0).UserName
                Else
                    lblUpdatedbyNew.Text = SafeDefaultValue
                End If
			  lblUpdatedDateNew.Text = FormatDate(.LastUpdatedDate)
               txtActivationNew.Text = SafeActiveInactive(.activation)
				Dim L_objMappingMsNegaraNCBSPPATK_Approval_Detail As TList(Of MappingMsNegaraNCBSPPATK_Approval_Detail)
				L_objMappingMsNegaraNCBSPPATK_Approval_Detail = DataRepository.MappingMsNegaraNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsNegaraNCBSPPATK_Approval_DetailColumn.PK_MsNegara_Approval_Id.ToString & _
					"=" & _
					parID, "", 0, Integer.MaxValue, Nothing)
                'Add mapping
                ListmapingNew.AddRange(L_oBJMappingMsNegaraNCBSPPATK_Approval_Detail)
			
			End With
			PkObject = ObjMsNegara_ApprovalDetail.IDNegara
		End Using

		'Load Old Data
		Using ObjMsNegara As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(PkObject)
			If ObjMsNegara Is Nothing Then Return
			With ObjMsNegara
				SafeDefaultValue = "-"
			    txtIDNegaraOld.Text = Safe(.IDNegara)
txtNamaNegaraOld.Text = Safe(.NamaNegara)


			 'other info
                Dim Omsuser As TList(Of User)
                Omsuser = DataRepository.UserProvider.GetByUserID(.CreatedBy)
                If Omsuser.Count > 0 Then
                    lblCreatedByOld.Text = Omsuser(0).UserName
                Else
                    lblCreatedByOld.Text = SafeDefaultValue
                End If
                lblCreatedDateNew.Text = FormatDate(.CreatedDate)
                Omsuser = DataRepository.UserProvider.GetByUserID(.LastUpdatedBy)
                If Omsuser.Count > 0 Then
                    lblUpdatedbyOld.Text = Omsuser(0).UserName
                Else
                    lblUpdatedbyOld.Text = SafeDefaultValue
                End If
			  lblUpdatedDateOld.Text = FormatDate(.LastUpdatedDate)
				Dim L_objMappingMsNegaraNCBSPPATK As TList(Of MappingMsNegaraNCBSPPATK)
                 txtActivationoLD.Text = SafeActiveInactive(.activation)
              L_ObjMappingMsNegaraNCBSPPATK = DataRepository.MappingMsNegaraNCBSPPATKProvider.GetPaged(MappingMsNegaraNCBSPPATKColumn.IDNegara.ToString & _
                 "=" & _
                 PkObject, "", 0, Integer.MaxValue, Nothing)
                ListmapingOld.AddRange(L_ObjMappingMsNegaraNCBSPPATK)
            End With
        End Using
    End Sub



	Protected Sub imgBtnConfirmation_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnConfirmation.Click
		Try
			Response.Redirect("MsNegara_approval_view.aspx")
		Catch ex As Exception
			LogError(ex)
			CvalPageErr.IsValid = False
			CvalPageErr.ErrorMessage = ex.Message
		End Try
	End Sub


	Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
		Try
			BindListMapping()
		Catch ex As Exception
			LogError(ex)
			CvalPageErr.IsValid = False
			CvalPageErr.ErrorMessage = ex.Message
		End Try
	End Sub

    Private Sub BindListMapping()
        'bind new value
        LBMappingNew.DataSource = ListMappingDisplayNew
        LBMappingNew.DataBind()
        'Bind OldValue
        LBmappingOld.DataSource = ListMappingDisplayOld
        LBmappingOld.DataBind()
    End Sub

#End Region

#Region "Property..."

	Public ReadOnly Property parID As String
		Get
			Return Request.Item("ID")
		End Get
	End Property
	''' <summary>
	''' Menyimpan Item untuk mapping New sementara
	''' </summary>
	''' <value></value>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Property ListmapingNew As TList(Of MappingMsNegaraNCBSPPATK_Approval_Detail)
		Get
			Return Session("ListmapingNew.data")
		End Get
		Set(value As TList(Of MappingMsNegaraNCBSPPATK_Approval_Detail))
			Session("ListmapingNew.data") = value
		End Set
	End Property

	''' <summary>
	''' Menyimpan Item untuk mapping Old sementara
	''' </summary>
	''' <value></value>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Property ListmapingOld As TList(Of MappingMsNegaraNCBSPPATK)
		Get
			Return Session("ListmapingOld.data")
		End Get
		Set(value As TList(Of MappingMsNegaraNCBSPPATK))
			Session("ListmapingOld.data") = value
		End Set
	End Property

    ''' <summary>
    ''' List yang tampil di ListBox
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplayNew() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingMsNegaraNCBSPPATK_Approval_Detail In ListmapingNew.FindAllDistinct("IDNegaraNCBS")
                Temp.Add(i.IDNegaraNCBS.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property

    ''' <summary>
    ''' List yang tampil di ListBox
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplayOld() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingMsNegaraNCBSPPATK In ListmapingOld.FindAllDistinct("IDNegaraNCBS")
                Temp.Add(i.IDNegaraNCBS.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property

#End Region

#Region "Action Approval..."

	Sub Rejected()
		Dim ObjMsNegara_Approval As MsNegara_Approval = DataRepository.MsNegara_ApprovalProvider.GetByPK_MsNegara_Approval_Id(parID)
		Dim ObjMsNegara_ApprovalDetail As MsNegara_ApprovalDetail = DataRepository.MsNegara_ApprovalDetailProvider.GetPaged(MsNegara_ApprovalDetailColumn.FK_MsNegara_Approval_Id.ToString & "=" & ObjMsNegara_Approval.PK_MsNegara_Approval_Id, "", 0, 1, Nothing)(0)
		Dim L_ObjMappingMsNegaraNCBSPPATK_Approval_Detail As TList(Of MappingMsNegaraNCBSPPATK_Approval_Detail) = DataRepository.MappingMsNegaraNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsNegaraNCBSPPATK_Approval_DetailColumn.PK_MsNegara_Approval_Id.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)
		DeleteApproval(ObjMsNegara_Approval, ObjMsNegara_ApprovalDetail, L_ObjMappingMsNegaraNCBSPPATK_Approval_Detail)
	End Sub

	Sub DeleteApproval(ByRef objMsNegara_Approval As MsNegara_Approval, ByRef ObjMsNegara_ApprovalDetail As MsNegara_ApprovalDetail, ByRef L_ObjMappingMsNegaraNCBSPPATK_Approval_Detail As TList(Of MappingMsNegaraNCBSPPATK_Approval_Detail))
		DataRepository.MsNegara_ApprovalProvider.Delete(objMsNegara_Approval)
		DataRepository.MsNegara_ApprovalDetailProvider.Delete(ObjMsNegara_ApprovalDetail)
		DataRepository.MappingMsNegaraNCBSPPATK_Approval_DetailProvider.Delete(L_ObjMappingMsNegaraNCBSPPATK_Approval_Detail)
	End Sub

	''' <summary>
	''' Delete Data dari  asli dari approval
	''' </summary>
	''' <remarks></remarks>
	Private Sub DeleteData(objMsNegara_Approval As MsNegara_Approval)
		'   '================= Delete Header ===========================================================
		Dim LObjMsNegara_ApprovalDetail As TList(Of MsNegara_ApprovalDetail) = DataRepository.MsNegara_ApprovalDetailProvider.GetPaged(MsNegara_ApprovalDetailColumn.FK_MsNegara_Approval_Id.ToString & "=" & objMsNegara_Approval.PK_MsNegara_Approval_Id, "", 0, 1, Nothing)
		If LObjMsNegara_ApprovalDetail.Count = 0 Then Throw New Exception("Data Not Found")
		Dim ObjMsNegara_ApprovalDetail As MsNegara_ApprovalDetail = LObjMsNegara_ApprovalDetail(0)
		Using ObjNewMsNegara As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(ObjMsNegara_ApprovalDetail.IDNegara)
			DataRepository.MsNegaraProvider.Delete(ObjNewMsNegara)
		End Using

		'======== Delete MAPPING =========================================
		Dim L_ObjMappingMsNegaraNCBSPPATK_Approval_Detail As TList(Of MappingMsNegaraNCBSPPATK_Approval_Detail) = DataRepository.MappingMsNegaraNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsNegaraNCBSPPATK_Approval_DetailColumn.PK_MsNegara_Approval_Id.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)


		For Each c As MappingMsNegaraNCBSPPATK_Approval_Detail In L_ObjMappingMsNegaraNCBSPPATK_Approval_Detail
			Using ObjNewMappingMsNegaraNCBSPPATK As MappingMsNegaraNCBSPPATK = DataRepository.MappingMsNegaraNCBSPPATKProvider.GetByPK_MappingMsNegaraNCBSPPATK_Id(c.PK_MappingMsNegaraNCBSPPATK_Id)
				DataRepository.MappingMsNegaraNCBSPPATKProvider.Delete(ObjNewMappingMsNegaraNCBSPPATK)
			End Using
		Next

		'======== Delete Approval data ======================================
		DeleteApproval(objMsNegara_Approval, ObjMsNegara_ApprovalDetail, L_ObjMappingMsNegaraNCBSPPATK_Approval_Detail)
	End Sub

	''' <summary>
	''' Update Data dari approval ke table asli 
	''' </summary>
	''' <remarks></remarks>
	Private Sub UpdateData(objMsNegara_Approval As MsNegara_Approval)
        'Dim objAuditTrailType As MsAuditTrailEntityType = DataRepository.MsAuditTrailEntityTypeProvider.GetByPk_MsAuditTrailEntityTypeId(18)
		'   '================= Update Header ===========================================================
		Dim LObjMsNegara_ApprovalDetail As TList(Of MsNegara_ApprovalDetail) = DataRepository.MsNegara_ApprovalDetailProvider.GetPaged(MsNegara_ApprovalDetailColumn.FK_MsNegara_Approval_Id.ToString & "=" & objMsNegara_Approval.PK_MsNegara_Approval_Id, "", 0, 1, Nothing)
		If LObjMsNegara_ApprovalDetail.Count = 0 Then Throw New Exception("Data Not Found")
		Dim ObjMsNegara_ApprovalDetail As MsNegara_ApprovalDetail = LObjMsNegara_ApprovalDetail(0)
		Dim ObjMsNegaraNew As MsNegara
		Using ObjMsNegaraOld As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(ObjMsNegara_ApprovalDetail.IDNegara)
			If ObjMsNegaraOld Is Nothing Then Throw New Exception("Data Not Found")
			ObjMsNegaraNew = ObjMsNegaraOld.Clone
			With ObjMsNegaraNew
				FillOrNothing(.NamaNegara, ObjMsNegara_ApprovalDetail.NamaNegara)
				FillOrNothing(.CreatedBy, ObjMsNegara_ApprovalDetail.CreatedBy)
				FillOrNothing(.CreatedDate, ObjMsNegara_ApprovalDetail.CreatedDate)
				FillOrNothing(.ApprovedBy, SessionUserId, True, oString)
				FillOrNothing(.ApprovedDate, Date.Now)
			End With
			DataRepository.MsNegaraProvider.Save(ObjMsNegaraNew)
			'Insert auditrail
            'AuditTrailBLL.InsertAuditTrail(AuditTrailOperation.Add, objAuditTrailType, Now(), Sahassa.AML.Commonly.SessionNIK, Sahassa.AML.Commonly.SessionStaffName, "MsBentukBidangUsaha has Approved to Update data", ObjMsBentukBidangUsahaNew, ObjMsBentukBidangUsahaOld)
			'======== Update MAPPING =========================================
			'delete mapping item
			Using L_ObjMappingMsNegaraNCBSPPATK As TList(Of MappingMsNegaraNCBSPPATK) = DataRepository.MappingMsNegaraNCBSPPATKProvider.GetPaged(MappingMsNegaraNCBSPPATKColumn.IDNegara.ToString & _
				"=" & _
				ObjMsNegaraNew.IDNegara, "", 0, Integer.MaxValue, Nothing)
				DataRepository.MappingMsNegaraNCBSPPATKProvider.Delete(L_ObjMappingMsNegaraNCBSPPATK)
			End Using
			'Insert mapping item
			Dim L_ObjMappingMsNegaraNCBSPPATK_Approval_Detail As TList(Of MappingMsNegaraNCBSPPATK_Approval_Detail) = DataRepository.MappingMsNegaraNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsNegaraNCBSPPATK_Approval_DetailColumn.PK_MsNegara_Approval_Id.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)

			Dim L_ObjNewMappingMsNegaraNCBSPPATK As New TList(Of MappingMsNegaraNCBSPPATK)
			For Each c As MappingMsNegaraNCBSPPATK_Approval_Detail In L_ObjMappingMsNegaraNCBSPPATK_Approval_Detail
				Dim ObjNewMappingMsNegaraNCBSPPATK As New MappingMsNegaraNCBSPPATK '= DataRepository.MappingMsNegaraNCBSPPATKProvider.GetByPK_MappingMsNegaraNCBSPPATK_Id(c.PK_MappingMsNegaraNCBSPPATK_Id.GetValueOrDefault)
				With ObjNewMappingMsNegaraNCBSPPATK
					FillOrNothing(.IDNegara, ObjMsNegaraNew.IDNegara)
					FillOrNothing(.Nama, c.Nama)
					FillOrNothing(.IDNegaraNCBS, c.IDNegaraNCBS)
					L_ObjNewMappingMsNegaraNCBSPPATK.Add(ObjNewMappingMsNegaraNCBSPPATK)
				End With
			Next
			DataRepository.MappingMsNegaraNCBSPPATKProvider.Save(L_ObjNewMappingMsNegaraNCBSPPATK)
			'======== Delete Approval data ======================================
			DeleteApproval(objMsNegara_Approval, ObjMsNegara_ApprovalDetail, L_ObjMappingMsNegaraNCBSPPATK_Approval_Detail)
		End Using
	End Sub
	''' <summary>
	''' Insert Data dari approval ke table asli 
	''' </summary>
	''' <remarks></remarks>
	Private Sub Inserdata(objMsNegara_Approval As MsNegara_Approval)
        'Dim objAuditTrailType As MsAuditTrailEntityType = DataRepository.MsAuditTrailEntityTypeProvider.GetByPk_MsAuditTrailEntityTypeId(18)
		'   '================= Save Header ===========================================================
		Dim LObjMsNegara_ApprovalDetail As TList(Of MsNegara_ApprovalDetail) = DataRepository.MsNegara_ApprovalDetailProvider.GetPaged(MsNegara_ApprovalDetailColumn.FK_MsNegara_Approval_Id.ToString & "=" & objMsNegara_Approval.PK_MsNegara_Approval_Id, "", 0, 1, Nothing)
		'jika data tidak ada di database
		If LObjMsNegara_ApprovalDetail.Count = 0 Then Throw New Exception("Data Not Found")
		'binding approval ke table asli
		Dim ObjMsNegara_ApprovalDetail As MsNegara_ApprovalDetail = LObjMsNegara_ApprovalDetail(0)
		Using ObjNewMsNegara As New MsNegara()
			With ObjNewMsNegara
				FillOrNothing(.IDNegara, ObjMsNegara_ApprovalDetail.IDNegara)
				FillOrNothing(.NamaNegara, ObjMsNegara_ApprovalDetail.NamaNegara)
				FillOrNothing(.CreatedBy, ObjMsNegara_ApprovalDetail.CreatedBy)
				FillOrNothing(.CreatedDate, ObjMsNegara_ApprovalDetail.CreatedDate)
				FillOrNothing(.ApprovedBy, SessionUserId, True, oString)
				FillOrNothing(.ApprovedDate, Date.Now)
                .activation = True
			End With
			DataRepository.MsNegaraProvider.Save(ObjNewMsNegara)
			'Insert auditrail
            'AuditTrailBLL.InsertAuditTrail(AuditTrailOperation.Add, objAuditTrailType, Now(), Sahassa.AML.Commonly.SessionNIK, Sahassa.AML.Commonly.SessionStaffName, "MsBentukBidangUsaha has Approved to Insert data", ObjNewMsBentukBidangUsaha, Nothing)
			'======== Insert MAPPING =========================================
			Dim L_ObjMappingMsNegaraNCBSPPATK_Approval_Detail As TList(Of MappingMsNegaraNCBSPPATK_Approval_Detail) = DataRepository.MappingMsNegaraNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsNegaraNCBSPPATK_Approval_DetailColumn.PK_MsNegara_Approval_Id.ToString & _
				"=" & _
				parID, "", 0, Integer.MaxValue, Nothing)
			Dim L_ObjNewMappingMsNegaraNCBSPPATK As New TList(Of MappingMsNegaraNCBSPPATK)()
			For Each c As MappingMsNegaraNCBSPPATK_Approval_Detail In L_ObjMappingMsNegaraNCBSPPATK_Approval_Detail
				Dim ObjNewMappingMsNegaraNCBSPPATK As New MappingMsNegaraNCBSPPATK()
				With ObjNewMappingMsNegaraNCBSPPATK
					FillOrNothing(.IDNegara, ObjNewMsNegara.IDNegara)
 					FillOrNothing(.Nama, c.Nama)
					FillOrNothing(.IDNegaraNCBS, c.IDNegaraNCBS)
					L_ObjNewMappingMsNegaraNCBSPPATK.Add(ObjNewMappingMsNegaraNCBSPPATK)
				End With
			Next
			DataRepository.MappingMsNegaraNCBSPPATKProvider.Save(L_ObjNewMappingMsNegaraNCBSPPATK)
			'======== Delete Approval data ======================================
			DeleteApproval(objMsNegara_Approval, ObjMsNegara_ApprovalDetail, L_ObjMappingMsNegaraNCBSPPATK_Approval_Detail)
		End Using
	End Sub

#End Region

End Class


