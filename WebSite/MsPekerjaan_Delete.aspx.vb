#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports Sahassa.AML
#End Region

Partial Class MsPekerjaan_Delete
    Inherits Parent

#Region "Function"

    Sub ChangeMultiView(ByVal index As Integer)
        MtvMsUser.ActiveViewIndex = index
    End Sub

    Private Sub BindListMapping()
        LBMapping.DataSource = Listmaping
        LBMapping.DataTextField = "Nama"
        LBMapping.DataValueField = "IDPekerjaanNCBS"
        LBMapping.DataBind()
    End Sub


#End Region

#Region "events..."

    Public ReadOnly Property parID() As String
        Get
            Return Request.Item("ID")
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                'AuditTrailBLL.InsertAuditTrailUserAccess(SessionCurrentPage)
                Listmaping = New TList(Of MappingMsPekerjaanNCBSPPATK)
                LoadData()
            Catch ex As Exception
                LogError(ex)
                CvalPageErr.IsValid = False
                CvalPageErr.ErrorMessage = ex.Message
            End Try
        End If

    End Sub

    Private Sub LoadData()
        Dim ObjMsPekerjaan As MsPekerjaan = DataRepository.MsPekerjaanProvider.GetPaged(MsPekerjaanColumn.IDPekerjaan.ToString & "=" & parID, "", 0, 1, Nothing)(0)
        If ObjMsPekerjaan Is Nothing Then Throw New Exception("Data Not Found")
        With ObjMsPekerjaan
            SafeDefaultValue = "-"
            lblIDPekerjaan.Text = .IDPekerjaan
            lblNamaPekerjaan.Text = Safe(.NamaPekerjaan)

            'other info
            Dim Omsuser As TList(Of User)
            Omsuser = DataRepository.UserProvider.GetByUserID(.CreatedBy)
            If Omsuser.Count > 0 Then
                lblCreatedBy.Text = Omsuser(0).UserName
            End If
            lblCreatedDate.Text = FormatDate(.CreatedDate)
            Omsuser = DataRepository.UserProvider.GetByUserID(.LastUpdatedBy)
            If Omsuser.Count > 0 Then
                lblUpdatedby.Text = Omsuser(0).UserName
            End If
            lblUpdatedDate.Text = FormatDate(.LastUpdatedDate)
            lblActivation.Text = SafeActiveInactive(.Activation)
            Dim L_objMappingMsPekerjaanNCBSPPATK As TList(Of MappingMsPekerjaanNCBSPPATK)
            L_objMappingMsPekerjaanNCBSPPATK = DataRepository.MappingMsPekerjaanNCBSPPATKProvider.GetPaged(MappingMsPekerjaanNCBSPPATKColumn.IdPekerjaan.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)

            Listmaping.AddRange(L_objMappingMsPekerjaanNCBSPPATK)
        End With
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            BindListMapping()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

#End Region

#Region "Property..."

    ''' <summary>
    ''' Menyimpan Item untuk mapping sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Listmaping() As TList(Of MappingMsPekerjaanNCBSPPATK)
        Get
            Return Session("Listmaping.data")
        End Get
        Set(ByVal value As TList(Of MappingMsPekerjaanNCBSPPATK))
            Session("Listmaping.data") = value
        End Set
    End Property

#End Region

#Region "events..."
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Response.Redirect("MsPekerjaan_View.aspx")
    End Sub

    Protected Sub imgOk_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgOk.Click
        Try
            Page.Validate("handle")
            If Page.IsValid Then

                ' =========== Insert Header Approval
                Dim KeyHeaderApproval As Integer
                Using ObjMsPekerjaan_Approval As New MsPekerjaan_Approval
                    With ObjMsPekerjaan_Approval
                        .FK_MsMode_Id = 3
                        FillOrNothing(.RequestedBy, SessionPkUserId)
                        FillOrNothing(.RequestedDate, Date.Now)
                        .IsUpload = False
                    End With
                    DataRepository.MsPekerjaan_ApprovalProvider.Save(ObjMsPekerjaan_Approval)
                    KeyHeaderApproval = ObjMsPekerjaan_Approval.PK_MsPekerjaan_Approval_Id
                End Using
                '============ Insert Detail Approval
                Using objMsPekerjaan_ApprovalDetail As New MsPekerjaan_ApprovalDetail()
                    With objMsPekerjaan_ApprovalDetail
                        Dim ObjMsPekerjaan As MsPekerjaan = DataRepository.MsPekerjaanProvider.GetByIDPekerjaan(parID)
                        FillOrNothing(.IDPekerjaan, lblIDPekerjaan.Text, True, oInt)
                        FillOrNothing(.NamaPekerjaan, lblNamaPekerjaan.Text, True, Ovarchar)

                        FillOrNothing(.IDPekerjaan, ObjMsPekerjaan.IDPekerjaan)
                        FillOrNothing(.Activation, ObjMsPekerjaan.Activation)
                        FillOrNothing(.CreatedDate, ObjMsPekerjaan.CreatedDate)
                        FillOrNothing(.CreatedBy, ObjMsPekerjaan.CreatedBy)
                        FillOrNothing(.FK_MsPekerjaan_Approval_Id, KeyHeaderApproval)
                    End With
                    DataRepository.MsPekerjaan_ApprovalDetailProvider.Save(objMsPekerjaan_ApprovalDetail)
                    Dim keyHeaderApprovalDetail As Integer = objMsPekerjaan_ApprovalDetail.PK_MsPekerjaan_ApprovalDetail_Id

                    '========= Insert mapping item 
                    Dim LobjMappingMsPekerjaanNCBSPPATK_Approval_Detail As New TList(Of MappingMsPekerjaanNCBSPPATK_Approval_Detail)

                    Dim L_ObjMappingMsPekerjaanNCBSPPATK As TList(Of MappingMsPekerjaanNCBSPPATK) = DataRepository.MappingMsPekerjaanNCBSPPATKProvider.GetPaged(MappingMsPekerjaanNCBSPPATKColumn.IdPekerjaan.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)

                    For Each objMappingMsPekerjaanNCBSPPATK As MappingMsPekerjaanNCBSPPATK In L_ObjMappingMsPekerjaanNCBSPPATK
                        Dim objMappingMsPekerjaanNCBSPPATK_Approval_Detail As New MappingMsPekerjaanNCBSPPATK_Approval_Detail
                        With objMappingMsPekerjaanNCBSPPATK_Approval_Detail
                            FillOrNothing(.IdPekerjaan, objMappingMsPekerjaanNCBSPPATK.PK_MappingMsPekerjaanNCBSPPATK_Id)
                            FillOrNothing(.IdPekerjaanNCBS, objMappingMsPekerjaanNCBSPPATK.IdPekerjaanNCBS)
                            FillOrNothing(.PK_MsPekerjaan_approval_id, KeyHeaderApproval)
                            FillOrNothing(.PK_MappingMsPekerjaanNCBSPPATK_Id, objMappingMsPekerjaanNCBSPPATK.PK_MappingMsPekerjaanNCBSPPATK_Id)
                            .Nama = objMappingMsPekerjaanNCBSPPATK.Nama
                            LobjMappingMsPekerjaanNCBSPPATK_Approval_Detail.Add(objMappingMsPekerjaanNCBSPPATK_Approval_Detail)
                        End With
                        DataRepository.MappingMsPekerjaanNCBSPPATK_Approval_DetailProvider.Save(LobjMappingMsPekerjaanNCBSPPATK_Approval_Detail)
                    Next
                    'session Maping item Clear
                    Listmaping.Clear()
                    Me.imgOk.Visible = False
                    Me.ImageCancel.Visible = False
                    LblConfirmation.Text = "Data has been Delete and waiting for approval"
                    MtvMsUser.ActiveViewIndex = 1
                End Using
            End If
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImgBtnAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAdd.Click
        Response.Redirect("MsPekerjaan_View.aspx")
    End Sub

#End Region

End Class



