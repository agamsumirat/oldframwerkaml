﻿Imports SahassaNettier.Entities
Imports SahassaNettier.Data
Partial Class UploadHiportAccountApproval
    Inherits Parent
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("UploadHiportAccountApproval.Selected") Is Nothing, New ArrayList, Session("UploadHiportAccountApproval.Selected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("UploadHiportAccountApproval.Selected") = value
        End Set
    End Property
    Private Property SetnGetValueSearch() As String
        Get
            Return IIf(Session("UploadHiportAccountApproval.ValueSearch") Is Nothing, "", Session("UploadHiportAccountApproval.ValueSearch"))
        End Get
        Set(ByVal Value As String)
            Session("UploadHiportAccountApproval.ValueSearch") = Value
        End Set
    End Property

    Private Property SetnGetFieldSearch() As String
        Get
            Return IIf(Session("UploadHiportAccountApproval.FieldSearch") Is Nothing, "", Session("UploadHiportAccountApproval.FieldSearch"))
        End Get
        Set(ByVal Value As String)
            Session("UploadHiportAccountApproval.FieldSearch") = Value
        End Set
    End Property

    Private Property SetnGetCurrentPage() As Int32
        Get
            Return CType(IIf(Session("UploadHiportAccountApproval.CurrentPage") Is Nothing, 0, Session("UploadHiportAccountApproval.CurrentPage")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("UploadHiportAccountApproval.CurrentPage") = Value
        End Set
    End Property
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    Private Property SetnGetRowTotal() As Int32
        Get
            Return CType(IIf(Session("UploadHiportAccountApproval.RowTotal") Is Nothing, 0, Session("UploadHiportAccountApproval.RowTotal")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("UploadHiportAccountApproval.RowTotal") = Value
        End Set
    End Property

    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("UploadHiportAccountApproval.Sort") Is Nothing, "PK_HiportApproval_ID  asc", Session("UploadHiportAccountApproval.Sort"))
        End Get
        Set(ByVal Value As String)
            Session("UploadHiportAccountApproval.Sort") = Value
        End Set
    End Property

    Sub ClearSession()
        SetnGetSelectedItem = Nothing
        SetnGetValueSearch = Nothing
        SetnGetFieldSearch = Nothing
        SetnGetCurrentPage = Nothing
        SetnGetRowTotal = Nothing
        SetnGetSort = Nothing
    End Sub
    Private Sub FillSearch()
        Me.ComboSearch.Items.Add(New ListItem("...", ""))
        Me.ComboSearch.Items.Add(New ListItem("Created By", "UserID Like '%-=Search=-%'"))
        Me.ComboSearch.Items.Add(New ListItem("Entry Date", "EntryDate >= '-=Search=- 00:00' AND EntryDate <= '-=Search=- 23:59'"))
        
    End Sub
    Public Property SetnGetBindTable() As TList(Of HiportApproval)
        Get
            Dim strWhereClause(-1) As String
            Dim strAllWhereClause As String = ""
            If Me.SetnGetCurrentPage > Me.GetPageTotal - 1 And Me.GetPageTotal - 1 > 0 Then
                Me.SetnGetCurrentPage = Me.GetPageTotal - 1
            End If

            If ComboSearch.SelectedValue <> "" Then
                If ComboSearch.SelectedItem.Text = "Created By" Then
                    ReDim Preserve strWhereClause(strWhereClause.Length)
                    strWhereClause(strWhereClause.Length - 1) = Me.ComboSearch.SelectedValue.Replace("%-=Search=-%", TextSearch.Text)
                End If

                If ComboSearch.SelectedItem.Text = "Entry Date" Then
                    If TextSearch.Text <> "" Then
                        If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", TextSearch.Text.Trim) Then
                            ReDim Preserve strWhereClause(strWhereClause.Length)
                            strWhereClause(strWhereClause.Length - 1) = Me.ComboSearch.SelectedValue.Replace("-=Search=-", Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", TextSearch.Text.Trim).ToString("yyyy-MM-dd"))

                        Else

                            Throw New Exception("Entry Date Must dd-MMM-yyyy")
                        End If
                    Else
                        Throw New Exception("Please Fill Entry Date.")
                    End If
                End If


            End If


            strAllWhereClause = String.Join(" and ", strWhereClause)
            If strAllWhereClause.Trim.Length > 0 Then
                strAllWhereClause += " and UserID <> '" & Sahassa.AML.Commonly.SessionUserId & "' AND UserID IN (SELECT u.UserID FROM [User] u WHERE u.UserID IN (select UserId from UserWorkingUnitAssignment Where WorkingUnitId in (select WorkingUnitId from UserWorkingUnitAssignment where UserId = '" & Sahassa.AML.Commonly.SessionUserId & "')))"
            Else
                strAllWhereClause += " UserID <> '" & Sahassa.AML.Commonly.SessionUserId & "' AND UserID IN (SELECT u.UserID FROM [User] u WHERE u.UserID IN (select UserId from UserWorkingUnitAssignment Where WorkingUnitId in (select WorkingUnitId from UserWorkingUnitAssignment where UserId = '" & Sahassa.AML.Commonly.SessionUserId & "')))"
            End If
            Session("UploadHiportAccountApproval.Table") = DataRepository.HiportApprovalProvider.GetPaged(strAllWhereClause, SetnGetSort, SetnGetCurrentPage, Sahassa.AML.Commonly.GetDisplayedTotalRow, Me.SetnGetRowTotal)

            Return CType(Session("UploadHiportAccountApproval.Table"), TList(Of HiportApproval))
        End Get
        Set(ByVal value As TList(Of HiportApproval))
            Session("UploadHiportAccountApproval.Table") = value
        End Set
    End Property
    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
            Me.CollectSelected()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = gridRow.FindControl("CheckBoxExporttoExcel")
                Dim UserId As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
                If ArrTarget.Contains(UserId) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(UserId)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(UserId)
                End If
                Me.SetnGetSelectedItem = ArrTarget
            End If
        Next
    End Sub
    Protected Sub Page_Load1(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                clearSession()
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using
                FillSearch()
                Me.ComboSearch.Attributes.Add("onchange", "javascript:Check(this, 2, '" & Me.TextSearch.ClientID & "', '" & Me.popUp.ClientID & "');")


                Me.popUp.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TextSearch.ClientID & "'), 'dd-mmm-yyyy')")
                Me.popUp.Style.Add("display", "none")
            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Sub BindGrid()

        Me.GridMSUserView.DataSource = SetnGetBindTable

        Me.GridMSUserView.CurrentPageIndex = Me.SetnGetCurrentPage
        Me.GridMSUserView.VirtualItemCount = Me.SetnGetRowTotal
        Me.GridMSUserView.DataBind()
    End Sub
    Sub SetinfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.ComboSearch.SelectedValue = Me.SetnGetFieldSearch
            Me.TextSearch.Text = Me.SetnGetValueSearch
        Catch
            Throw
        End Try
    End Sub



    Protected Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
            Me.SetInfoNavigate()
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message

        End Try
    End Sub

    Protected Sub GridMSUserView_EditCommand(source As Object, e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridMSUserView.EditCommand
        Try
            Dim pk As Integer = e.Item.Cells(1).Text

            Response.Redirect("UploadHiportAccountApprovalDetail.aspx?PK_HiportApproval_ID=" & pk, False)

        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Private Sub BindSelected()
        Dim Rows As New ArrayList
        Dim AllList As TList(Of HiportApproval) = DataRepository.HiportApprovalProvider.GetPaged("", "", 0, Integer.MaxValue, 0)

        For Each IdPk As Long In Me.SetnGetSelectedItem
            Dim oList As TList(Of HiportApproval) = AllList.FindAll(HiportApprovalColumn.PK_HiportApproval_ID.ToString, IdPk)
            If oList.Count > 0 Then
                Rows.Add(oList(0))
            End If
        Next
        Me.GridMSUserView.DataSource = Rows
        Me.GridMSUserView.AllowPaging = False
        Me.GridMSUserView.DataBind()
        For i As Integer = 0 To GridMSUserView.Items.Count - 1
            For y As Integer = 0 To GridMSUserView.Columns.Count - 1
                GridMSUserView.Items(i).Cells(y).Attributes.Add("class", "text")
            Next
        Next
        'Sembunyikan kolom ke 0, 1, 5 & 6 agar tidak ikut diekspor ke excel
        Me.GridMSUserView.Columns(0).Visible = False
        Me.GridMSUserView.Columns(1).Visible = False
        Me.GridMSUserView.Columns(4).Visible = False
    End Sub

    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub 'ClearControls

    Protected Sub LinkButtonExportExcel_Click(sender As Object, e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Me.CollectSelected()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelected()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=SystemParameterApproval.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridMSUserView)
            GridMSUserView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
End Class
