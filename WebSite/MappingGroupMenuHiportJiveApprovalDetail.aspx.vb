Option Strict On
Option Explicit On
Imports amlbll
Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports SahassaNettier.Services
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper


Imports System.IO
Imports System.Collections.Generic

Partial Class MappingGroupMenuHiportJiveApprovalDetail
    Inherits Parent

    Public ReadOnly Property PK_MappingGroupMenuHIPORTJIVE_ApprovalDetail_ID() As Integer
        Get
            Dim strTemp As String = Request.Params("PK_MappingGroupMenuHIPORTJIVE_ApprovalDetail_ID")
            Dim intResult As Integer
            If Integer.TryParse(strTemp, intResult) Then
                Return intResult
            Else
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = "Parameter PK_MappingGroupMenuHIPORTJIVE_ApprovalDetail_ID is invalid."

            End If
        End Get
    End Property

    Public ReadOnly Property ObjMappingGroupMenuHIPORTJIVE_ApprovalDetail() As TList(Of MappingGroupMenuHiportJive_ApprovalDetail)
        Get
            If Session("MappingGroupMenuHIPORTJIVEApprovalDetail.ObjMappingGroupMenuHIPORTJIVE_ApprovalDetail") Is Nothing Then

                Session("MappingGroupMenuHIPORTJIVEApprovalDetail.ObjMappingGroupMenuHIPORTJIVE_ApprovalDetail") = DataRepository.MappingGroupMenuHIPORTJIVE_ApprovalDetailProvider.GetPaged("PK_MappingGroupMenuHIPORTJIVE_ApprovalDetail_ID = '" & PK_MappingGroupMenuHIPORTJIVE_ApprovalDetail_ID & "'", "", 0, Integer.MaxValue, 0)

            End If
            Return CType(Session("MappingGroupMenuHIPORTJIVEApprovalDetail.ObjMappingGroupMenuHIPORTJIVE_ApprovalDetail"), TList(Of MappingGroupMenuHIPORTJIVE_ApprovalDetail))
        End Get
    End Property


    Sub LoadDataAdd()
        Try
            PanelOld.Visible = False
            PanelNew.Visible = True

            LblAction.Text = "Add"

            If ObjMappingGroupMenuHIPORTJIVE_ApprovalDetail.Count > 0 Then
                Using objGroupNew As Group = DataRepository.GroupProvider.GetByGroupID(CInt(ObjMappingGroupMenuHIPORTJIVE_ApprovalDetail(0).FK_Group_ID))
                    LblFK_Group_ID_New.Text = objGroupNew.GroupName
                End Using
            Else
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = MappingGroupMenuHIPORTJIVEBLL.DATA_NOTVALID
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub loaddataEdit()
        Try
            PanelOld.Visible = True
            PanelNew.Visible = True

            LblAction.Text = "Edit"

            If ObjMappingGroupMenuHIPORTJIVE_ApprovalDetail.Count > 0 Then
                Using objGroupOld As Group = DataRepository.GroupProvider.GetByGroupID(CInt(ObjMappingGroupMenuHIPORTJIVE_ApprovalDetail(0).FK_Group_ID_Old))
                    LblFK_Group_ID_Old.Text = objGroupOld.GroupName
                End Using

                Using objGroupNew As Group = DataRepository.GroupProvider.GetByGroupID(CInt(ObjMappingGroupMenuHIPORTJIVE_ApprovalDetail(0).FK_Group_ID))
                    LblFK_Group_ID_New.Text = objGroupNew.GroupName
                End Using
            Else
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = MappingGroupMenuHIPORTJIVEBLL.DATA_NOTVALID
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try

    End Sub

    Sub LoadDataDelete()
        Try
            PanelOld.Visible = False
            PanelNew.Visible = True

            LblAction.Text = "Delete"

            If ObjMappingGroupMenuHIPORTJIVE_ApprovalDetail.Count > 0 Then
                Using objGroupNew As Group = DataRepository.GroupProvider.GetByGroupID(CInt(ObjMappingGroupMenuHIPORTJIVE_ApprovalDetail(0).FK_Group_ID))
                    LblFK_Group_ID_New.Text = objGroupNew.GroupName
                End Using
            Else
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = MappingGroupMenuHIPORTJIVEBLL.DATA_NOTVALID
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Sub LoadData()
        Try
            If ObjMappingGroupMenuHIPORTJIVE_ApprovalDetail.Count > 0 Then
                Dim ListModeID As Integer = 0
                Using objMappingGroupMenuHIPORTJIVE_Approval As MappingGroupMenuHIPORTJIVE_Approval = DataRepository.MappingGroupMenuHIPORTJIVE_ApprovalProvider.GetByPK_MappingGroupMenuHIPORTJIVE_Approval_ID(CInt(ObjMappingGroupMenuHIPORTJIVE_ApprovalDetail(0).FK_MappingGroupMenuHIPORTJIVE_Approval_ID))
                    ListModeID = CInt(objMappingGroupMenuHIPORTJIVE_Approval.FK_ModeID)
                    If ListModeID = 1 Then
                        LoadDataAdd()
                    ElseIf ListModeID = 2 Then
                        loaddataEdit()
                    ElseIf ListModeID = 3 Then
                        LoadDataDelete()
                    End If
                End Using

            Else
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = MappingGroupMenuHIPORTJIVEBLL.DATA_NOTVALID
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "MappingGroupMenuHIPORTJIVEApproval.aspx"
            Me.Response.Redirect("MappingGroupMenuHIPORTJIVEApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not IsPostBack Then
                Session("MappingGroupMenuHIPORTJIVEApprovalDetail.ObjMappingGroupMenuHIPORTJIVE_ApprovalDetail") = Nothing
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                    LabelTitle.Text = "Mapping Group Menu Hiport Jive  - Approval Detail"
                    LoadData()

                End Using
            End If

        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImageAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click
        Try
            Using ObjMappingGroupMenuHiportJive_ApprovalDetail As MappingGroupMenuHiportJive_ApprovalDetail = DataRepository.MappingGroupMenuHiportJive_ApprovalDetailProvider.GetByPK_MappingGroupMenuHiportJive_ApprovalDetail_ID(PK_MappingGroupMenuHIPORTJIVE_ApprovalDetail_ID)
                If ObjMappingGroupMenuHiportJive_ApprovalDetail Is Nothing Then
                    Throw New AMLBLL.SahassaException(MappingGroupMenuHiportJiveEnum.DATA_NOTVALID)
                End If
            End Using

            If LblAction.Text = "Add" Then
                AMLBLL.MappingGroupMenuHiportJiveBLL.AcceptAddHIPORTJIVE(PK_MappingGroupMenuHIPORTJIVE_ApprovalDetail_ID)
            ElseIf LblAction.Text = "Edit" Then
                Using ObjList As MappingGroupMenuHiportJive = DataRepository.MappingGroupMenuHiportJiveProvider.GetByPK_MappingGroupMenuHiportJive_ID(ObjMappingGroupMenuHIPORTJIVE_ApprovalDetail(0).PK_MappingGroupMenuHiportJive_ID)
                    If ObjList Is Nothing Then
                        Throw New AMLBLL.SahassaException(MappingGroupMenuHiportJiveEnum.DATA_NOTVALID)
                    End If
                End Using
                AMLBLL.MappingGroupMenuHiportJiveBLL.AcceptEditHIPORTJIVE(PK_MappingGroupMenuHIPORTJIVE_ApprovalDetail_ID)
            ElseIf LblAction.Text = "Delete" Then
                Using ObjList As MappingGroupMenuHiportJive = DataRepository.MappingGroupMenuHiportJiveProvider.GetByPK_MappingGroupMenuHiportJive_ID(ObjMappingGroupMenuHIPORTJIVE_ApprovalDetail(0).PK_MappingGroupMenuHiportJive_ID)
                    If ObjList Is Nothing Then
                        Throw New AMLBLL.SahassaException(MappingGroupMenuHiportJiveEnum.DATA_NOTVALID)
                    End If
                End Using
                AMLBLL.MappingGroupMenuHiportJiveBLL.AcceptDeleteHIPORTJIVE(PK_MappingGroupMenuHIPORTJIVE_ApprovalDetail_ID)
            End If

            Sahassa.AML.Commonly.SessionIntendedPage = "MappingGroupMenuHIPORTJIVEApproval.aspx"

            Me.Response.Redirect("MappingGroupMenuHIPORTJIVEApproval.aspx", False)

        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImageReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click
        Try
            Using ObjMappingGroupMenuHiportJive_ApprovalDetail As MappingGroupMenuHiportJive_ApprovalDetail = DataRepository.MappingGroupMenuHiportJive_ApprovalDetailProvider.GetByPK_MappingGroupMenuHiportJive_ApprovalDetail_ID(PK_MappingGroupMenuHIPORTJIVE_ApprovalDetail_ID)
                If ObjMappingGroupMenuHiportJive_ApprovalDetail Is Nothing Then
                    Throw New AMLBLL.SahassaException(MappingGroupMenuHiportJiveEnum.DATA_NOTVALID)
                End If
            End Using

            If LblAction.Text = "Add" Then
                AMLBLL.MappingGroupMenuHiportJiveBLL.RejectAddHIPORTJIVE(PK_MappingGroupMenuHIPORTJIVE_ApprovalDetail_ID)
            ElseIf LblAction.Text = "Edit" Then
                AMLBLL.MappingGroupMenuHiportJiveBLL.RejectEditHIPORTJIVE(PK_MappingGroupMenuHIPORTJIVE_ApprovalDetail_ID)
            ElseIf LblAction.Text = "Delete" Then
                AMLBLL.MappingGroupMenuHiportJiveBLL.RejectDeleteHIPORTJIVE(PK_MappingGroupMenuHIPORTJIVE_ApprovalDetail_ID)
            End If

            Sahassa.AML.Commonly.SessionIntendedPage = "MappingGroupMenuHIPORTJIVEApproval.aspx"

            Me.Response.Redirect("MappingGroupMenuHIPORTJIVEApproval.aspx", False)


        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub


End Class

