<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="IFTIFileGenerate.aspx.vb" Inherits="IFTIFileGenerate" title="Untitled Page" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">

   <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td>
            
                <img src="Images/blank.gif" width="5" height="1" />
            </td>
            <td>                               
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td width="99%" bgcolor="#FFFFFF">
                            <img src="Images/blank.gif" width="1" height="1" />
                        </td>
                        <td bgcolor="#FFFFFF">
                            <img src="Images/blank.gif" width="1" height="1" />
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="Images/blank.gif" width="5" height="1" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td id="tdcontent" height="99%" valign="top" bgcolor="#FFFFFF">
                <div id="divcontent" class="divcontent">
                    <table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td>
                                <img src="Images/blank.gif" width="20" height="100%" />
                            </td>
                            <td class="divcontentinside" bgcolor="#FFFFFF">
                                <ajax:AjaxPanel ID="a" runat="server">
                                    
                                    <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator>
                                </ajax:AjaxPanel>
                                <ajax:AjaxPanel ID="AjaxPanel5" runat="server" Width="100%">
                                    <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
                                        style="border-top-style: none; border-right-style: none; border-left-style: none;
                                        border-bottom-style: none" width="100%">
                                        <tr>
                                            <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                                                border-right-style: none; border-left-style: none; border-bottom-style: none;
                                                font-family: Tahoma">
                                                <img src="Images/dot_title.gif" width="17" height="17">&nbsp;<b><asp:Label ID="Label1"
                                                    runat="server" Text="IFTI Report File - List of Transactions"></asp:Label></b><hr />
                                            </td>
                                        </tr>
                                    </table>
                                    <table border="0" cellpadding="0" cellspacing="0" style="background-color: White;
                                        border-color: White;" width="100%">
                                        <tr>
                                            <td>
                                                <table align="left" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                    bgcolor="#dddddd" border="2">
                                                    <tr>
                                                        <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                            style="height: 6px">
                                                            <table cellpadding="0" cellspacing="0" border="0">
                                                                <tr>
                                                                    <td class="formtext">
                                                                        <asp:Label ID="Label9" runat="server" Text="IFTI File Information" Font-Bold="True"></asp:Label>&nbsp;
                                                                    </td>
                                                                    <td>
                                                                        <a href="#" onclick="javascript:ShowHidePanel('TrCTRFileInfo','ImgCTRFileInfo','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                            title="click to minimize or maximize">
                                                                            <img id="ImgCTRFileInfo" src="Images/search-bar-minimize.gif" border="0" height="12px"
                                                                                width="12px"></a>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr id="TrCTRFileInfo">
                                                        <td valign="top" bgcolor="#ffffff">
                                                            <ajax:AjaxPanel ID="AjaxPanel1" runat="server" Width="100%">
                                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                    <tr class="formText">
                                                                        <td width="15%" bgcolor="#FFF7E6">
                                                                            <asp:Label ID="Label3" runat="server" Text="Transaction Date"></asp:Label>
                                                                        </td>
                                                                        <td width="1%" bgcolor="#ffffff">
                                                                            :
                                                                        </td>
                                                                        <td width="30%" bgcolor="#ffffff">
                                                                            <ajax:AjaxPanel ID="AjaxPanel3" runat="server">
                                                                                <asp:Label ID="LblTransactionDate" runat="server"></asp:Label></ajax:AjaxPanel>
                                                                        </td>
                                                                        <td width="3%" bgcolor="#ffffff">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td width="15%" bgcolor="#FFF7E6">
                                                                            <asp:Label ID="Label5" runat="server" Text="Total Valid"></asp:Label>
                                                                        </td>
                                                                        <td width="1%" bgcolor="#ffffff">
                                                                            :
                                                                        </td>
                                                                        <td width="30%" bgcolor="#ffffff">
                                                                            <ajax:AjaxPanel ID="AjaxPanel15" runat="server">
                                                                                <asp:Label ID="LblTotalValid" runat="server"></asp:Label></ajax:AjaxPanel>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="formText">
                                                                        <td width="15%" bgcolor="#FFF7E6">
                                                                            <asp:Label ID="lblTranstype" runat="server" Text="IFTI Type"></asp:Label>
                                                                        </td>
                                                                        <td width="1%" bgcolor="#ffffff">
                                                                            :
                                                                        </td>
                                                                        <td width="30%" bgcolor="#ffffff">
                                                                            <ajax:AjaxPanel ID="AjaxPanel17" runat="server">
                                                                                <asp:Label ID="LblTransactionType" runat="server"></asp:Label></ajax:AjaxPanel>
                                                                        </td>
                                                                        <td width="3%" bgcolor="#ffffff">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td width="15%" bgcolor="#FFF7E6">
                                                                            <asp:Label ID="Label11" runat="server" Text="Total Invalid"></asp:Label>
                                                                        </td>
                                                                        <td width="1%" bgcolor="#ffffff">
                                                                            :
                                                                        </td>
                                                                        <td width="30%" bgcolor="#ffffff">
                                                                            <ajax:AjaxPanel ID="AjaxPanel19" runat="server">
                                                                                <asp:Label ID="LblTotalInvalid" runat="server"></asp:Label></ajax:AjaxPanel>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="formText">
                                                                        <td width="15%" bgcolor="#FFF7E6">
                                                                            <asp:Label ID="Label10" runat="server" Text="Total Transaction"></asp:Label>
                                                                        </td>
                                                                        <td width="1%" bgcolor="#ffffff">
                                                                            :
                                                                        </td>
                                                                        <td width="30%" bgcolor="#ffffff">
                                                                            <ajax:AjaxPanel ID="ajax1" runat="server">
                                                                                <asp:Label ID="LblTotalTransaction" runat="server" Text="Total Transaction"></asp:Label></ajax:AjaxPanel>
                                                                        </td>
                                                                        <td width="3%" bgcolor="#ffffff">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td width="15%" bgcolor="#fff7e6">
                                                                            <asp:Label ID="Label4" runat="server" Text="Status Upload to PPATK"></asp:Label></td>
                                                                        <td width="1%" bgcolor="#ffffff">
                                                                            :</td>
                                                                        <td width="30%" bgcolor="#ffffff">
                                                                            &nbsp;
                                                                            <ajax:AjaxPanel ID="AjaxPanel2" runat="server">
                                                                                <asp:Label ID="LblStatusUploadPPATK" runat="server"></asp:Label></ajax:AjaxPanel></td>
                                                                    </tr>
                                                                    <tr class="formText">
                                                                        <td bgcolor="#fff7e6" width="15%">
                                                                            <asp:Label ID="Label6" runat="server" Text="Report Type"></asp:Label></td>
                                                                        <td bgcolor="#ffffff" width="1%">
                                                                            :</td>
                                                                        <td bgcolor="#ffffff" width="30%">
                                                                            <ajax:AjaxPanel ID="AjaxPanel14" runat="server">
                                                                                <asp:Label ID="LblReportType" runat="server"></asp:Label></ajax:AjaxPanel></td>
                                                                        <td bgcolor="#ffffff" width="3%">
                                                                        </td>
                                                                        <td bgcolor="#fff7e6" width="15%">
                                                                            <asp:Label ID="Label7" runat="server" Text="Last Confirm By"></asp:Label></td>
                                                                        <td bgcolor="#ffffff" width="1%">
                                                                            :</td>
                                                                        <td bgcolor="#ffffff" width="30%">
                                                                            <ajax:AjaxPanel ID="AjaxPanel16" runat="server">
                                                                                <asp:Label ID="LblLastConfirmBy" runat="server"></asp:Label></ajax:AjaxPanel></td>
                                                                    </tr>
                                                                    <tr class="formText">
                                                                        <td align="right" colspan="7">
                                                                            <asp:ImageButton ajaxcall="none" ID="ImgGenerate" runat="server" ImageUrl="~/Images/button/generate.gif" /></td>
                                                                    </tr>
                                                                </table>
                                                            </ajax:AjaxPanel>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 13px">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table align="left" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                    bgcolor="#dddddd" border="2">
                                                    <tr id="TrGridData">
                                                        <td valign="top" width="100%" bgcolor="#ffffff">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                            
                                </ajax:AjaxPanel>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td background="Images/button-bground.gif" align="left" valign="middle">
                            <img src="Images/blank.gif" width="5" height="1" />
                        </td>
                        <td background="Images/button-bground.gif" align="left" valign="middle">
                            <img src="images/arrow.gif" width="15" height="15" />
                        </td>
                        <td background="Images/button-bground.gif" align="left" valign="middle">
                            &nbsp;<ajax:AjaxPanel ID="AjaxPanel22" runat="server"><asp:ImageButton ID="ImgBack"
                                runat="server" ImageUrl="~/Images/button/back.gif"></asp:ImageButton></ajax:AjaxPanel>
                        </td>
                        <td width="99%" background="Images/button-bground.gif">
                            <img src="Images/blank.gif" width="1" height="1" />
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
            </td>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>

