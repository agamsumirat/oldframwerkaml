#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Sahassanettier.Entities
Imports SahassaNettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports System.Collections.Generic
#End Region

Partial Class MsBentukBidangUsaha_edit
    Inherits Parent

#Region "Function"

    Private Sub LoadData()
        Dim ObjMsBentukBidangUsaha As MsBentukBidangUsaha = DataRepository.MsBentukBidangUsahaProvider.GetPaged(MsBentukBidangUsahaColumn.IdBentukBidangUsaha.ToString & "=" & parID, "", 0, 1, Nothing)(0)
        If ObjMsBentukBidangUsaha Is Nothing Then Throw New Exception("Data Not Found")
        With ObjMsBentukBidangUsaha
            SafeDefaultValue = "-"
            txtIdBentukBidangUsaha.Text = Safe(.IdBentukBidangUsaha)
            txtBentukBidangUsaha.Text = Safe(.BentukBidangUsaha)

            chkActivation.Checked = SafeBoolean(.Activation)
            Dim L_objMappingMsBentukBidangUsahaNCBSPPATK As TList(Of MappingMsBentukBidangUsahaNCBSPPATK)
            L_objMappingMsBentukBidangUsahaNCBSPPATK = DataRepository.MappingMsBentukBidangUsahaNCBSPPATKProvider.GetPaged(MappingMsBentukBidangUsahaNCBSPPATKColumn.IdBentukBidangUsaha.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)


            Listmaping.AddRange(L_objMappingMsBentukBidangUsahaNCBSPPATK)

        End With
    End Sub

    Private Sub BindListMapping()
        LBMapping.DataSource = ListMappingDisplay
        LBMapping.DataBind()
    End Sub

    ''' <summary>
    ''' Validasi men-handle seluruh syarat2 data valid
    ''' data yang lolos dari validasi dipastikan lolos ke database 
    ''' karena menggunakan FillorNothing
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ValidasiControl()
        '======================== Validasi textbox :  Id canot Null ====================================================
        If ObjectAntiNull(txtIdBentukBidangUsaha.Text) = False Then Throw New Exception("Id  must be filled  ")
        If Not IsNumeric(Me.txtIdBentukBidangUsaha.Text) Then
            Throw New Exception("ID must be in number format")
        End If
        '======================== Validasi textbox :  Nama canot Null ====================================================
        If ObjectAntiNull(txtBentukBidangUsaha.Text) = False Then Throw New Exception("Nama  must be filled  ")

        If DataRepository.MsBentukBidangUsahaProvider.GetPaged("IDBentukBidangUsaha = '" & txtIdBentukBidangUsaha.Text & "' AND IDBentukBidangUsaha <> '" & parID & "'", "", 0, Integer.MaxValue, 0).Count > 0 Then
            Throw New Exception("ID already exist in the database")
        End If

        If DataRepository.MsBentukBidangUsaha_ApprovalDetailProvider.GetPaged(MsBentukBidangUsaha_ApprovalDetailColumn.IdBentukBidangUsaha.ToString & "='" & txtIdBentukBidangUsaha.Text & "' AND IDBentukBidangUsaha <> '" & parID & "'", "", 0, 1, Nothing).Count > 0 Then
            Throw New Exception("Data exist in List Waiting Approval")
        End If
        'If LBMapping.Items.Count = 0 Then Throw New Exception("data tidak punya list Mapping")
    End Sub

#End Region

#Region "events..."

    Protected Sub imgBrowse_click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBrowse.Click
        Try
            If Not Get_DataFromPicker Is Nothing Then
                Listmaping.AddRange(Get_DataFromPicker)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgDeleteItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgDeleteItem.Click
        Try
            If LBMapping.SelectedIndex < 0 Then
                Throw New Exception("No data Selected")
            End If
            Listmaping.RemoveAt(LBMapping.SelectedIndex)

        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
                Listmaping = New TList(Of MappingMsBentukBidangUsahaNCBSPPATK)
                LoadData()
            Catch ex As Exception
                LogError(ex)
                CvalPageErr.IsValid = False
                CvalPageErr.ErrorMessage = ex.Message
            End Try
        End If

    End Sub

    Protected Sub ImgBtnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnSave.Click
        Try
            Page.Validate("handle")
            If Page.IsValid Then
                ValidasiControl()
                ' =========== Insert Header Approval
                Dim KeyHeaderApproval As Integer
                Using ObjMsBentukBidangUsaha_Approval As New MsBentukBidangUsaha_Approval
                    With ObjMsBentukBidangUsaha_Approval
                        FillOrNothing(.FK_MsMode_Id, 2, True, oInt)
                        FillOrNothing(.RequestedBy, SessionPkUserId)
                        FillOrNothing(.RequestedDate, Date.Now)
                        .IsUpload = False
                    End With
                    DataRepository.MsBentukBidangUsaha_ApprovalProvider.Save(ObjMsBentukBidangUsaha_Approval)
                    KeyHeaderApproval = ObjMsBentukBidangUsaha_Approval.PK_MsBentukBidangUsaha_Approval_Id
                End Using
                '============ Insert Detail Approval
                Using objMsBentukBidangUsaha_ApprovalDetail As New MsBentukBidangUsaha_ApprovalDetail()
                    With objMsBentukBidangUsaha_ApprovalDetail
                        Dim ObjMsBentukBidangUsaha As MsBentukBidangUsaha = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(parID)
                        FillOrNothing(.IdBentukBidangUsaha, ObjMsBentukBidangUsaha.IdBentukBidangUsaha)
                        FillOrNothing(.BentukBidangUsaha, ObjMsBentukBidangUsaha.BentukBidangUsaha)
                        FillOrNothing(.Activation, ObjMsBentukBidangUsaha.Activation)
                        FillOrNothing(.CreatedDate, ObjMsBentukBidangUsaha.CreatedDate)
                        FillOrNothing(.CreatedBy, ObjMsBentukBidangUsaha.CreatedBy)
                        FillOrNothing(.FK_MsBentukBidangUsaha_Approval_Id, KeyHeaderApproval)

                        '==== Edit data =============
                        FillOrNothing(.IdBentukBidangUsaha, txtIdBentukBidangUsaha.Text, True, oInt)
                        FillOrNothing(.BentukBidangUsaha, txtBentukBidangUsaha.Text, True, Ovarchar)

                        FillOrNothing(.Activation, chkActivation.Checked, True, oBit)
                        FillOrNothing(.LastUpdatedBy, SessionUserId)
                        FillOrNothing(.LastUpdatedDate, Date.Now)

                    End With
                    DataRepository.MsBentukBidangUsaha_ApprovalDetailProvider.Save(objMsBentukBidangUsaha_ApprovalDetail)
                    Dim keyHeaderApprovalDetail As Integer = objMsBentukBidangUsaha_ApprovalDetail.PK_MsBentukBidangUsaha_ApprovalDetail_Id
                    '========= Insert mapping item 
                    Dim LobjMappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail As New TList(Of MappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail)
                    For Each objMappingMsBentukBidangUsahaNCBSPPATK As MappingMsBentukBidangUsahaNCBSPPATK In Listmaping
                        Dim Mapping As New MappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail
                        With Mapping
                            FillOrNothing(.idBentukBidangUsaha, objMsBentukBidangUsaha_ApprovalDetail.IdBentukBidangUsaha)
                            FillOrNothing(.IdBentukBidangUsahaNCBS, objMappingMsBentukBidangUsahaNCBSPPATK.IdBentukBidangUsahaNCBS)
                            FillOrNothing(.PK_MsBentukBidangUsaha_Approval_Id, KeyHeaderApproval)
                            FillOrNothing(.Nama, objMappingMsBentukBidangUsahaNCBSPPATK.Nama)
                            FillOrNothing(.PK_MappingMsBentukBidangUsahaNCBSPPATK_approval_detail_Id, keyHeaderApprovalDetail)
                            LobjMappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail.Add(Mapping)
                        End With
                    Next
                    DataRepository.MappingMsBentukBidangUsahaNCBSPPATK_Approval_DetailProvider.Save(LobjMappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail)
                    'session Maping item Clear
                    Listmaping.Clear()
                    ImgBtnSave.Visible = False
                    ImgBackAdd.Visible = False
                    LblConfirmation.Text = "Data has been added and waiting for approval"
                    MtvMsUser.ActiveViewIndex = 1
                End Using
            End If
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub



    Protected Sub ImgBackAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBackAdd.Click
        Response.Redirect("MsBentukBidangUsaha_View.aspx")
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            BindListMapping()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBack.Click
        Response.Redirect("MsBentukBidangUsaha_View.aspx")
    End Sub
#End Region

#Region "Property..."
    Public ReadOnly Property parID() As String
        Get
            Return Request.Item("ID")
        End Get
    End Property
    ''' <summary>
    ''' Mengambil Data dari Picker
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Get_DataFromPicker() As TList(Of MappingMsBentukBidangUsahaNCBSPPATK)
        Get
            Dim Temp As New TList(Of MappingMsBentukBidangUsahaNCBSPPATK)
            Dim L_MsBentukBidangUsahaNCBS As TList(Of MsBentukBidangUsahaNCBS)
            Try
                If Session("PickerMsBentukBidangUsahaNCBS.Data") Is Nothing Then
                    Throw New Exception
                End If
                L_MsBentukBidangUsahaNCBS = Session("PickerMsBentukBidangUsahaNCBS.Data")
                For Each i As MsBentukBidangUsahaNCBS In L_MsBentukBidangUsahaNCBS
                    Dim Tempmapping As New MappingMsBentukBidangUsahaNCBSPPATK
                    With Tempmapping
                        .IdBentukBidangUsahaNCBS = i.IdBentukBidangusahaNCBS
                        .Nama = i.BentukBidangUsahaNCBS
                    End With
                    Temp.Add(Tempmapping)
                Next
            Catch ex As Exception
                L_MsBentukBidangUsahaNCBS = Nothing
            End Try
            Return Temp
        End Get
    End Property
    ''' <summary>
    ''' Menyimpan Item untuk mapping sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Listmaping() As TList(Of MappingMsBentukBidangUsahaNCBSPPATK)
        Get
            Return Session("Listmaping.data")
        End Get
        Set(ByVal value As TList(Of MappingMsBentukBidangUsahaNCBSPPATK))
            Session("Listmaping.data") = value
        End Set
    End Property
    ''' <summary>
    ''' List yang tampil di ListBox
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplay() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingMsBentukBidangUsahaNCBSPPATK In Listmaping.FindAllDistinct("IdBentukBidangusahaNCBS")
                Temp.Add(i.IdBentukBidangUsahaNCBS.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property
#End Region

End Class



