﻿Imports SahassaNettier.Entities
Imports AMLBLL
Imports SahassaNettier.Data
Partial Class DataUpdatingAlertApprovalDetail
    Inherits Parent

    Public ReadOnly Property objPkPendingID() As Long
        Get
            Dim strtemp As String = Request.Params("PKApprovalID")
            Dim lngresult As Long
            If Long.TryParse(strtemp, lngresult) Then
                Return lngresult
            Else
                Throw New SahassaException("PKPendingID is not Valid.")
            End If
        End Get
    End Property


    Public Property ObjDataUpdatingParameterAlert_Approval() As DataUpdatingParameterAlert_Approval
        Get
            If Session("DataUpdatingAlertApprovalDetail.ObjDataUpdatingParameterAlert_Approval") Is Nothing Then
                Session("DataUpdatingAlertApprovalDetail.ObjDataUpdatingParameterAlert_Approval") = DataUpdatingAlertBLL.GetDataUpdatingParameterAlert_ApprovalByPk(Me.objPkPendingID)
                Return CType(Session("DataUpdatingAlertApprovalDetail.ObjDataUpdatingParameterAlert_Approval"), DataUpdatingParameterAlert_Approval)
            Else
                Return CType(Session("DataUpdatingAlertApprovalDetail.ObjDataUpdatingParameterAlert_Approval"), DataUpdatingParameterAlert_Approval)
            End If
        End Get
        Set(value As DataUpdatingParameterAlert_Approval)
            Session("DataUpdatingAlertApprovalDetail.ObjDataUpdatingParameterAlert_Approval") = value
        End Set
    End Property


    Public Property ObjTDataUpdatingParameterAlert_ApprovalDetail() As TList(Of DataUpdatingParameterAlert_ApprovalDetail)
        Get
            If Session("StatementGeneratorAdd.ObjTDataUpdatingParameterAlert_ApprovalDetail") Is Nothing Then
                Session("StatementGeneratorAdd.ObjTDataUpdatingParameterAlert_ApprovalDetail") = DataUpdatingAlertBLL.GetTlistDataUpdatingParameterAlert_ApprovalDetail(DataUpdatingParameterAlert_ApprovalDetailColumn.FK_DataUpdatingParameterAlert_ApprovalID.ToString & "=" & Me.objPkPendingID, "", 0, Integer.MaxValue, 0)
                Return CType(Session("StatementGeneratorAdd.ObjTDataUpdatingParameterAlert_ApprovalDetail"), TList(Of DataUpdatingParameterAlert_ApprovalDetail))
            Else
                Return CType(Session("StatementGeneratorAdd.ObjTDataUpdatingParameterAlert_ApprovalDetail"), TList(Of DataUpdatingParameterAlert_ApprovalDetail))
            End If
        End Get
        Set(value As TList(Of DataUpdatingParameterAlert_ApprovalDetail))
            Session("StatementGeneratorAdd.ObjTDataUpdatingParameterAlert_ApprovalDetail") = value
        End Set
    End Property

    Sub ClearSession()
        ObjDataUpdatingParameterAlert_Approval = Nothing
        ObjTDataUpdatingParameterAlert_ApprovalDetail = Nothing
    End Sub
    Sub LoadDataApproval()
        If Not ObjDataUpdatingParameterAlert_Approval Is Nothing Then

            Using objUser As User = DataRepository.UserProvider.GetBypkUserID(ObjDataUpdatingParameterAlert_Approval.FK_MsUserID.GetValueOrDefault(0))
                If Not objUser Is Nothing Then
                    LblRequestBy.Text = objUser.UserName
                End If
            End Using


            LblRequestDate.Text = ObjDataUpdatingParameterAlert_Approval.CreatedDate.GetValueOrDefault(Sahassa.AML.Commonly.GetDefaultDate).ToString("dd-MMM-yyyy").Replace("01-Jan-0001", "")
            If ObjDataUpdatingParameterAlert_Approval.FK_ModeID.GetValueOrDefault(0) = 1 Then
                LblMode.Text = "Add"
            ElseIf ObjDataUpdatingParameterAlert_Approval.FK_ModeID.GetValueOrDefault(0) = 2 Then
                LblMode.Text = "Edit"
            ElseIf ObjDataUpdatingParameterAlert_Approval.FK_ModeID.GetValueOrDefault(0) = 3 Then
                LblMode.Text = "Delete"
            End If
            If ObjTDataUpdatingParameterAlert_ApprovalDetail.Count > 0 Then

                Using objHigh As DataUpdatingParameterAlert_ApprovalDetail = ObjTDataUpdatingParameterAlert_ApprovalDetail.Find(DataUpdatingParameterAlert_ApprovalDetailColumn.PK_DataUpdatingParameterAlert_ID, 1)
                    If Not objHigh Is Nothing Then
                        ChkHighRiskCustomer.Checked = objHigh.Activation_old.GetValueOrDefault(False)
                        LblYearHighRiskcustomer.Text = objHigh.YearOnOpeningCIF_old.GetValueOrDefault(0)

                        ChkHighRiskCustomerNew.Checked = objHigh.Activation.GetValueOrDefault(False)
                        LblYearHighRiskcustomerNew.Text = objHigh.YearOnOpeningCIF.GetValueOrDefault(0)
                    End If
                End Using

                Using objmedium As DataUpdatingParameterAlert_ApprovalDetail = ObjTDataUpdatingParameterAlert_ApprovalDetail.Find(DataUpdatingParameterAlert_ApprovalDetailColumn.PK_DataUpdatingParameterAlert_ID, 3)
                    If Not objmedium Is Nothing Then
                        ChkMediumRiskCustomer.Checked = objmedium.Activation_old.GetValueOrDefault(False)
                        LblYearMediumRiskCustomer.Text = objmedium.YearOnOpeningCIF_old.GetValueOrDefault(0)

                        ChkMediumRiskCustomerNew.Checked = objmedium.Activation.GetValueOrDefault(False)
                        LblYearMediumRiskCustomerNew.Text = objmedium.YearOnOpeningCIF.GetValueOrDefault(0)
                    End If
                End Using

                Using objNonHigh As DataUpdatingParameterAlert_ApprovalDetail = ObjTDataUpdatingParameterAlert_ApprovalDetail.Find(DataUpdatingParameterAlert_ApprovalDetailColumn.PK_DataUpdatingParameterAlert_ID, 2)
                    If Not objNonHigh Is Nothing Then
                        ChkNonHighRiskCustomer.Checked = objNonHigh.Activation_old.GetValueOrDefault(False)
                        LblYearNonHighRiskCustomer.Text = objNonHigh.YearOnOpeningCIF_old.GetValueOrDefault(0)

                        ChkNonHighRiskCustomerNew.Checked = objNonHigh.Activation.GetValueOrDefault(False)
                        LblYearNonHighRiskCustomerNew.Text = objNonHigh.YearOnOpeningCIF.GetValueOrDefault(0)
                    End If
                End Using

            End If
        End If
    End Sub

    Protected Sub Page_Load1(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                ClearSession()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using

                LoadDataApproval()
            End If
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBtnAccept_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAccept.Click
        Try

            If DataUpdatingAlertBLL.AcceptEdit(Me.objPkPendingID) Then
                Response.Redirect("DataUpdatingAlertApproval.aspx", False)
            End If



        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImgBtnReject_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnReject.Click
        Try

            If DataUpdatingAlertBLL.RejectEdit(Me.objPkPendingID) Then
                Response.Redirect("DataUpdatingAlertApproval.aspx", False)
            End If



        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImgBackAdd_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImgBackAdd.Click
        Try

            Response.Redirect("DataUpdatingAlertApproval.aspx", False)

        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub
End Class
