
Partial Class CaseManagementWorkflowApproval
    Inherits Parent

#Region " Property "
    ''' <summary>
    ''' selected item store
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("CaseManagementWorkflowApprovalSelected") Is Nothing, New ArrayList, Session("CaseManagementWorkflowApprovalSelected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("CaseManagementWorkflowApprovalSelected") = value
        End Set
    End Property
    ''' <summary>
    ''' setnget search field
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetFieldSearch() As String
        Get
            Return IIf(Session("CaseManagementWorkflowApprovalFieldSearch") Is Nothing, "", Session("CaseManagementWorkflowApprovalFieldSearch"))
        End Get
        Set(ByVal Value As String)
            Session("CaseManagementWorkflowApprovalFieldSearch") = Value
        End Set
    End Property
    ''' <summary>
    ''' search value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetValueSearch() As String
        Get
            Return IIf(Session("CaseManagementWorkflowApprovalValueSearch") Is Nothing, "", Session("CaseManagementWorkflowApprovalValueSearch"))
        End Get
        Set(ByVal Value As String)
            Session("CaseManagementWorkflowApprovalValueSearch") = Value
        End Set
    End Property
    ''' <summary>
    ''' sort expresion
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("CaseManagementWorkflowApprovalSort") Is Nothing, "AccountOwnerName  asc", Session("CaseManagementWorkflowApprovalSort"))
        End Get
        Set(ByVal Value As String)
            Session("CaseManagementWorkflowApprovalSort") = Value
        End Set
    End Property
    ''' <summary>
    ''' current page index
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("CaseManagementWorkflowApprovalCurrentPage") Is Nothing, 0, Session("CaseManagementWorkflowApprovalCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("CaseManagementWorkflowApprovalCurrentPage") = Value
        End Set
    End Property
    ''' <summary>
    ''' total pages
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    ''' <summary>
    ''' row total
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("CaseManagementWorkflowApprovalRowTotal") Is Nothing, 0, Session("CaseManagementWorkflowApprovalRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("CaseManagementWorkflowApprovalRowTotal") = Value
        End Set
    End Property
    ''' <summary>
    ''' save bind table
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetBindTable() As Data.DataTable
        Get
            Return IIf(Session("CaseManagementWorkflowApprovalData") Is Nothing, New AMLDAL.CaseManagementWorkflow.SelectCMW_Approval_ViewDataTable, Session("CaseManagementWorkflowApprovalData"))
        End Get
        Set(ByVal value As Data.DataTable)
            Session("CaseManagementWorkflowApprovalData") = value
        End Set
    End Property
#End Region

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' fill search
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub FillSearch()
        Try
            Me.ComboSearch.Items.Add(New ListItem("...", ""))
            Me.ComboSearch.Items.Add(New ListItem("Account Owner", "AccountOwnerName Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Mode", "Mode Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("VIPCode", "VIPCode Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Segment", "Segment Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("InsiderCode", "InsiderCode Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("SBU", "SBU Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("SUBSBU", "SUBSBU Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("RM", "RM Like '%-=Search=-%'"))

        Catch
            Throw
        End Try
    End Sub

    Private Sub ClearThisPageSessions()
        Session("CaseManagementWorkflowApprovalSelected") = Nothing
        Session("CaseManagementWorkflowApprovalFieldSearch") = Nothing
        Session("CaseManagementWorkflowApprovalValueSearch") = Nothing
        Session("CaseManagementWorkflowApprovalSort") = Nothing
        Session("CaseManagementWorkflowApprovalCurrentPage") = Nothing
        Session("CaseManagementWorkflowApprovalRowTotal") = Nothing
        Session("CaseManagementWorkflowApprovalData") = Nothing
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not Me.IsPostBack Then
                Me.ClearThisPageSessions()
                Me.ComboSearch.Attributes.Add("onchange", "javascript:CheckNoPopUp('" & Me.TextSearch.ClientID & "');")
                Using AccessCaseManagement As New AMLDAL.CaseManagementWorkflowTableAdapters.SelectCMW_Approval_ViewTableAdapter
                    Me.SetnGetBindTable = AccessCaseManagement.GetData(Sahassa.AML.Commonly.SessionUserId)
                End Using
                Me.FillSearch()
                Me.GridCaseManagementWorkflowApproval.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' page navigate button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select            
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' bind grid
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Sub BindGrid()
        Try
            Dim Rows() As AMLDAL.CaseManagementWorkflow.SelectCMW_Approval_ViewRow = Me.SetnGetBindTable.Select(Me.SetnGetFieldSearch.Replace("-=Search=-", Me.SetnGetValueSearch.Replace("'", "''")), Me.SetnGetSort)
            Me.GridCaseManagementWorkflowApproval.DataSource = Rows
            Me.SetnGetRowTotal = Rows.Length
            Me.GridCaseManagementWorkflowApproval.CurrentPageIndex = Me.SetnGetCurrentPage
            Me.GridCaseManagementWorkflowApproval.VirtualItemCount = Me.SetnGetRowTotal
            Me.GridCaseManagementWorkflowApproval.DataBind()
        Catch
            Throw
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' set navigate info
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.ComboSearch.SelectedValue = Me.SetnGetFieldSearch
            Me.TextSearch.Text = Me.SetnGetValueSearch
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' Detail
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub GridCaseManagementWorkflowApproval_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridCaseManagementWorkflowApproval.EditCommand
        Try
            Using oAccessCountApproval As New AMLDAL.CaseManagementWorkflowTableAdapters.CountAccountOwnerCaseManagementApprovalTableAdapter
                Dim IntCountApproval As Integer = oAccessCountApproval.Fill(e.Item.Cells(0).Text)
                If IntCountApproval > 0 Then
                    Me.Response.Redirect("CaseManagementWorkflowApprovalDetail.aspx?AccountOwnerID=" & e.Item.Cells(0).Text.Trim & "&Mode=" & e.Item.Cells(9).Text.Trim & "&AccountOwnerName=" & e.Item.Cells(1).Text.Trim & "&CaseAlertDescription=" & e.Item.Cells(2).Text.Trim & "&VIPCode=" & e.Item.Cells(3).Text.Trim & "&InsiderCode=" & e.Item.Cells(4).Text.Trim & "&Segment=" & e.Item.Cells(5).Text.Trim & "&SBU=" & e.Item.Cells(6).Text.Trim & "&SUBSBU=" & e.Item.Cells(7).Text.Trim & "&RM=" & e.Item.Cells(8).Text.Trim, False)
                Else
                    Throw New Exception("Case Management Workflow for Account Owner '" & e.Item.Cells(1).Text & "' is not available.")
                End If
            End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' change sort expression
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridCaseManagementWorkflowApproval_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridCaseManagementWorkflowApproval.SortCommand
        Dim GridCaseManagementWorkflowApproval As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridCaseManagementWorkflowApproval.Columns(Sahassa.AML.Commonly.IndexSort(GridCaseManagementWorkflowApproval, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' image button search
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Try            
            Me.SetnGetFieldSearch = Me.ComboSearch.SelectedValue
            If Me.ComboSearch.SelectedIndex = 0 Then
                Me.TextSearch.Text = ""
            End If
            Me.SetnGetValueSearch = Me.TextSearch.Text
            Me.SetnGetCurrentPage = 0
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' go button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	14/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try            
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be a positive number greater than zero.")
                End If
            Else
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' prerender event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

End Class
