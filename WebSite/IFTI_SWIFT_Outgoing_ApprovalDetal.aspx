﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="IFTI_SWIFT_Outgoing_ApprovalDetal.aspx.vb" Inherits="IFTI_SWIFT_Outgoing_ApprovalDetal" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
    <div id="divcontent" class="divcontent">
    <table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
          <tr>
              <td bgcolor="White">
                <ajax:AjaxPanel ID="AjaxPanel1" runat="server" Width="100%">
                    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                        <asp:View ID="ViewSWIFTOutAppDet" runat="server">
                                <table id="TblSearchSWIFTOutgoing" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
                                        style="border-top-style: none; border-right-style: none; border-left-style: none;
                                        border-bottom-style: none" width="100%">
                                        <tr>
                                            <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                                                border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                <img src="Images/dot_title.gif" width="17" height="17" />
                                                <strong>
                                                    <asp:Label ID="Label5" runat="server" Text="IFTI Swift Outgoing - Approval Detail "></asp:Label>
                                                </strong>
                                                <hr />
                                             </td>                                      
                                        </tr>
                                        <tr>
                                            <td>
                                                <ajax:AjaxPanel ID="AjaxPanel2" runat="server" Width="885px" meta:resourcekey="AjaxPanel1Resource1">
																	<table style="height: 100%">
																		<tr>
																			<td colspan="3" style="height: 7px">
																			</td>
																		</tr>
																		<tr>
																			<td nowrap style="height: 26px; width: 107px;">
																				<asp:Label ID="LabelMsUser_StaffName" runat="server" Text="User"></asp:Label>
																			</td>
																			<td style="width: 19px;">
																				:
																			</td>
																			<td style="height: 26px; width: 765px;">
																				<asp:TextBox ID="txtMsUser_StaffName" runat="server" CssClass="searcheditbox" TabIndex="2"
																					Width="308px" Enabled="False"></asp:TextBox>
																			</td>
																		</tr>
																		<tr>
																			<td nowrap style="height: 26px; width: 107px;">
																				<asp:Label ID="LabelRequestedDate" runat="server" Text="Requested Date"></asp:Label>
																			</td>
																			<td style="width: 19px;">
																				:
																			</td>
																			<td style="height: 26px; width: 765px;">
																				<asp:TextBox ID="txtRequestedDate1" runat="server" CssClass="textBox" MaxLength="1000"
																					TabIndex="2" Width="308px" ToolTip="RequestedDate" Enabled="False"></asp:TextBox>
																				&nbsp;&nbsp;</td>
																		</tr>																		
																		<tr>
																			<td colspan="3">
																			</td>
																		</tr>																		
																	</table>																	
											    </ajax:AjaxPanel>
                                            </td>
                                        </tr>
                                </table>
                                <table id="UMUM" align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2"
                                    width="100%" bgcolor="#dddddd" border="2">
                                    <tr>
                                        <td background="Images/search-bar-background.gif" valign="middle" align="left" style="height: 6px;
                                            width: 100%;" width="100%">
                                            <table cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td class="formtext">
                                                        <asp:Label ID="Label25" runat="server" Text="A. Umum" Font-Bold="True"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <a href="#" onclick="javascript:ShowHidePanel('SWIFTOut_Umum','Img13','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                            title="click to minimize or maximize">
                                                            <img id="Img13" src="Images/search-bar-minimize.gif" border="0" height="12px" width="12px" /></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr id="SWIFTOut_Umum">
                                        <td valign="top" bgcolor="#ffffff" style="height: 125px; width: 100%;" width="100%">
                                            <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                <tr>
                                                    <td align="center" style="width: 5%; height: 22px; background-color: silver">
                                                        <asp:Label ID="Label6322" runat="server" Font-Bold="True" Text="Field"></asp:Label></td>
                                                    <td align="center" style="width: 5%; background-color: silver">
                                                        <asp:Label ID="Label8543" runat="server" Font-Bold="True" Text="Old Data"></asp:Label></td>
                                                    <td style="width: 20%; background-color: silver">
                                                        <asp:Label ID="Label1343" runat="server" Font-Bold="True" Text="New Data"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 22px;">
                                                        <asp:Label ID="Label27" runat="server" Text="a. No. LTDLN "></asp:Label><asp:Label
                                                            ID="Label28" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                    <td style="width: 5%;">
                                                        <asp:TextBox ID="SWIFTOutUmum_LTDNOld" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                    <td style="width: 20%">
                                                        <asp:TextBox ID="SWIFTOutUmum_LTDNNew" runat="server" CssClass="searcheditbox" MaxLength="50"
                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 22px;">
                                                        <asp:Label ID="Label50" runat="server" Text="b. No. LTDLN Koreksi " Width="147px"></asp:Label></td>
                                                    <td style="width: 5%">
                                                        <asp:TextBox ID="SWIFTOutUmum_LtdnKoreksiOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                    <td style="width: 20%">
                                                        <asp:TextBox ID="SWIFTOutUmum_LtdnKoreksiNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 22px;">
                                                        <asp:Label ID="Label66" runat="server" Text="c. Tanggal Laporan "></asp:Label><asp:Label
                                                            ID="Label67" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                    <td style="width: 5%">
                                                        <asp:TextBox ID="SWIFTOutUmum_TanggalLaporanOld" runat="server" CssClass="searcheditbox"
                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 20%">
                                                        <asp:TextBox ID="SWIFTOutUmum_TanggalLaporanNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 22px;">
                                                        <asp:Label ID="Label68" runat="server" Text="d. Nama PJK Bank Pelapor " Width="164px"></asp:Label>
                                                        <asp:Label ID="Label72" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                    <td style="width: 5%">
                                                        <asp:TextBox ID="SWIFTOutUmum_NamaPJKBankOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 20%">
                                                        <asp:TextBox ID="SWIFTOutUmum_NamaPJKBankNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 22px;">
                                                        <asp:Label ID="Label73" runat="server" Text="e. Nama Pejabat PJK Bank Pelapor" Width="203px"></asp:Label>
                                                        <asp:Label ID="Label74" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                    <td style="width: 5%;">
                                                        <asp:TextBox ID="SWIFTOutUmum_NamaPejabatPJKBankOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                    <td style="width: 20%">
                                                        <asp:TextBox ID="SWIFTOutUmum_NamaPejabatPJKBankNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 22px;">
                                                        <asp:Label ID="Label75" runat="server" Text="f. Jenis Laporan"></asp:Label>
                                                        <asp:Label ID="Label76" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                    <td style="width: 5%" align="left">
                                                        <asp:RadioButtonList ID="RbSWIFTOutUmum_JenisLaporanOld" runat="server" RepeatColumns="2"
                                                            RepeatDirection="Horizontal" Enabled="False">
                                                            <asp:ListItem Value="1">Baru</asp:ListItem>
                                                            <asp:ListItem Value="2">Recall</asp:ListItem>
                                                            <asp:ListItem Value="3">Koreksi</asp:ListItem>
                                                            <asp:ListItem Value="4">Reject</asp:ListItem>
                                                        </asp:RadioButtonList></td>
                                                    <td align="left" style="width: 20%">
                                                        <asp:RadioButtonList ID="RbSWIFTOutUmum_JenisLaporanNew" runat="server" RepeatColumns="2"
                                                            RepeatDirection="Horizontal" Enabled="False">
                                                            <asp:ListItem Value="1">Baru</asp:ListItem>
                                                            <asp:ListItem Value="2">Recall</asp:ListItem>
                                                            <asp:ListItem Value="3">Koreksi</asp:ListItem>
                                                            <asp:ListItem Value="4">Reject</asp:ListItem>
                                                        </asp:RadioButtonList></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; height: 22px; background-color: #fff7e6;">
                                                    </td>
                                                    <td style="width: 5%">
                                                    </td>
                                                    <td style="width: 20%">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <table id="PENGIRIM" align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2"
                                    width="100%" bgcolor="White" border="2">
                                    <tr>
                                        <td bgcolor="White">
                                            <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                <tr>
                                                    <td align="center" style="width: 5%; height: 22px; background-color: silver">
                                                        <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="Field"></asp:Label></td>
                                                    <td align="center" style="width: 5%; background-color: silver">
                                                        <asp:Label ID="Label3" runat="server" Font-Bold="True" Text="Old Data"></asp:Label></td>
                                                    <td style="width: 20%; background-color: silver">
                                                        <asp:Label ID="Label6e" runat="server" Font-Bold="True" Text="New Data"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6" bgcolor="White">
                                                        <asp:Label ID="Label51" runat="server" Text="Tipe PJK Bank" Width="72px"></asp:Label>
                                                    </td>
                                                    <td align="left" style="border: thin dashed silver; width: 5%;" bgcolor="White">
                                                        <ajax:AjaxPanel ID="AjaxPanel7" runat="server" Width="100%">
                                                            <asp:DropDownList ID="cboNonSwiftOutPenerima_TipePJKBankOld" runat="server" AutoPostBack="True"
                                                                Enabled="False">
                                                                <asp:ListItem Value="0">Penyelenggara Pengirim Akhir</asp:ListItem>
                                                                <asp:ListItem Value="1">Penyelenggara Penerus</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </ajax:AjaxPanel>
                                                    </td>
                                                    <td align="left" style="width: 20%; border-right: silver thin dashed; border-top: silver thin dashed;
                                                        border-left: silver thin dashed; border-bottom: silver thin dashed;" bgcolor="White">
                                                        <ajax:AjaxPanel ID="AjaxPanel11" runat="server" Width="100%">
                                                            <asp:DropDownList ID="cboNonSwiftOutPenerima_TipePJKBankNew" runat="server" AutoPostBack="True"
                                                                Enabled="False">
                                                                <asp:ListItem Value="0">Penyelenggara Pengirim Akhir</asp:ListItem>
                                                                <asp:ListItem Value="1">Penyelenggara Penerus</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </ajax:AjaxPanel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:MultiView ID="PengirimPJK" runat="server" ActiveViewIndex="0">
                                                <asp:View ID="viewB1" runat="server">
                                                    <table width="100%">
                                                        <tr>
                                                            <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                                style="height: 6px" bgcolor="White">
                                                                <table>
                                                                    <tr>
                                                                        <td class="formtext">
                                                                            <asp:Label ID="Label10" runat="server" Text="B.1 Identitas Pengirim/Pengirim Asal"
                                                                                Font-Bold="True"></asp:Label>
                                                                            <a href="#" onclick="javascript:ShowHidePanel('BSWIFTOutIdentitasPengirim','Img8','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                                title="click to minimize or maximize">
                                                                                <img id="Img14" src="Images/search-bar-minimize.gif" border="0" height="12px" width="12px" />
                                                                            </a>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <tr id="B1SWIFTOutIdentitasPengirim">
                                                        <td valign="top" bgcolor="White" style="height: 125px" width="100%">
                                                            <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                            </table>
                                                            <table width="100%" bgcolor="White">
                                                                <tr>
                                                                    <td align="center" style="width: 5%; height: 22px; background-color: silver">
                                                                        <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Field"></asp:Label></td>
                                                                    <td align="center" style="width: 5%; background-color: silver">
                                                                        <asp:Label ID="Label39" runat="server" Font-Bold="True" Text="Old Data"></asp:Label></td>
                                                                    <td style="width: 20%; background-color: silver">
                                                                        <asp:Label ID="Label45" runat="server" Font-Bold="True" Text="New Data"></asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 5%; background-color: #fff7e6; height: 27px;">
                                                                        <asp:Label ID="Label190" runat="server" Text="No. Rek " Width="48px" Height="16px"
                                                                            Enabled="false"></asp:Label>
                                                                        <asp:Label ID="Label191" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                    <td style="width: 5%; height: 27px;" align="left">
                                                                        <asp:TextBox ID="TxtB1SWIFTOutPengirim_rekeningOld" runat="server" CssClass="searcheditbox"
                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                    <td style="height: 27px; width: 20%;" align="left">
                                                                        <asp:TextBox ID="TxtB1SWIFTOutPengirim_rekeningNew" runat="server" CssClass="searcheditbox"
                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trSwiftOutPengirimTipePengirim" runat="server">
                                                                    <td style="width: 5%; background-color: #fff7e6; height: 21px;">
                                                                        <asp:Label ID="Label192" runat="server" Text="       "></asp:Label>
                                                                        <asp:Label ID="Label193" runat="server" Text="       Tipe Pengirim"></asp:Label>
                                                                        <asp:Label ID="Label194" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 5%; height: 21px;" align="left">
                                                                        <ajax:AjaxPanel ID="AjaxPanel5" runat="server" Height="16px">
                                                                            <asp:RadioButtonList ID="RbB1PengirimNasabah_TipePengirimOld" runat="server" RepeatDirection="Horizontal"
                                                                                AutoPostBack="True" Width="195px" Enabled="False">
                                                                                <asp:ListItem Value="1">Nasabah</asp:ListItem>
                                                                                <asp:ListItem Value="2">Non Nasabah</asp:ListItem>
                                                                            </asp:RadioButtonList></ajax:AjaxPanel></td>
                                                                    <td style="height: 21px; width: 20%;" align="left">
                                                                        <ajax:AjaxPanel ID="AjaxPanel3" runat="server" Height="16px">
                                                                            <asp:RadioButtonList ID="RbB1PengirimNasabah_TipePengirimNew" runat="server" RepeatDirection="Horizontal"
                                                                                AutoPostBack="True" Width="195px" Enabled="False">
                                                                                <asp:ListItem Value="1">Nasabah</asp:ListItem>
                                                                                <asp:ListItem Value="2">Non Nasabah</asp:ListItem>
                                                                            </asp:RadioButtonList></ajax:AjaxPanel>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trSwiftOutPengirimTipeNasabah" runat="server" visible="False">
                                                                    <td style="width: 5%; height: 34px; background-color: #fff7e6">
                                                                        <asp:Label ID="Label9256" runat="server" Text="Tipe Nasabah "></asp:Label>
                                                                        <asp:Label ID="Label9257" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 5%; height: 34px" align="left">
                                                                        <ajax:AjaxPanel ID="AjaxPanel779" runat="server">
                                                                            <asp:RadioButtonList ID="RbB1_IdenPengirimNas_TipeNasabahOld" runat="server" AutoPostBack="True"
                                                                                RepeatDirection="Horizontal" Enabled="False">
                                                                                <asp:ListItem Value="1">Perorangan</asp:ListItem>
                                                                                <asp:ListItem Value="2">Korporasi</asp:ListItem>
                                                                            </asp:RadioButtonList>
                                                                        </ajax:AjaxPanel>
                                                                    </td>
                                                                    <td style="height: 34px; width: 20%;" align="left">
                                                                        <ajax:AjaxPanel ID="AjaxPanel4" runat="server">
                                                                            <asp:RadioButtonList ID="RbB1_IdenPengirimNas_TipeNasabahNew" runat="server" AutoPostBack="True"
                                                                                RepeatDirection="Horizontal" Enabled="False">
                                                                                <asp:ListItem Value="1">Perorangan</asp:ListItem>
                                                                                <asp:ListItem Value="2">Korporasi</asp:ListItem>
                                                                            </asp:RadioButtonList>
                                                                        </ajax:AjaxPanel>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:MultiView ID="MultiViewB1SWIFTOutIdenPengirim" runat="server">
                                                                <asp:View ID="ViewB1SWIFTOutIdenPengirimNasabah" runat="server">
                                                                    <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                                        bgcolor="#dddddd" border="2">
                                                                        <tr>
                                                                            <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left">
                                                                                <table cellpadding="0" cellspacing="0" border="0">
                                                                                    <tr>
                                                                                        <td class="formtext">
                                                                                            <asp:Label ID="Label11" runat="server" Text="Nasabah" Font-Bold="True"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <a href="#" onclick="javascript:ShowHidePanel('BSWIFTOutIdentitasPengirim1nasabah','Imgs9','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                                                title="click to minimize or maximize">
                                                                                                <img id="Imgs9" src="Images/search-bar-minimize.gif" border="0" height="12px" width="12px" /></a>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="BSWIFTOutIdentitasPengirim1nasabah">
                                                                            <td valign="top" bgcolor="#ffffff">
                                                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                                </table>
                                                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                                    <tr>
                                                                                        <td style="width: 100%">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 100%">
                                                                                            <asp:MultiView ID="MultiViewB1PengirimNasabah" runat="server" ActiveViewIndex="0">
                                                                                                <asp:View ID="ViewB1PengirimNasabah_IND" runat="server">
                                                                                                    <table style="width: 100%;">
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Label30" runat="server" Text="Perorangan " Font-Bold="True"></asp:Label>
                                                                                                            </td>
                                                                                                            <td style="height: 15px; width: 20%;">
                                                                                                            </td>
                                                                                                           <%-- <td style="height: 15px; width: 20%;">
                                                                                                            </td>--%>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="center" style="width: 5%; background-color: silver">
                                                                                                                <asp:Label ID="Label46" runat="server" Font-Bold="True" Text="Field"></asp:Label></td>
                                                                                                            <td align="center" style="width: 20%; background-color: silver">
                                                                                                                <asp:Label ID="Label48" runat="server" Font-Bold="True" Text="Old Data"></asp:Label></td>
                                                                                                            <%--<td style="width: 20%; background-color: silver">
                                                                                                                <asp:Label ID="Label49" runat="server" Font-Bold="True" Text="New Data"></asp:Label></td>--%>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Label29" runat="server" Text="a. Nama Lengkap"></asp:Label>
                                                                                                                <asp:Label ID="Label263" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_NamaLengkapOld" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                           <%-- <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_NamaLengkapNew" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                            </td>--%>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Label32" runat="server" Text="b. Tanggal lahir (tgl/bln/thn)" Width="135px"></asp:Label>
                                                                                                                <asp:Label ID="Label264" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_tanggalLahirOld" runat="server" CssClass="searcheditbox"
                                                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <%--<td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_tanggalLahirNew" runat="server" CssClass="searcheditbox"
                                                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                &nbsp;</td>--%>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Label33" runat="server" Text="c. Kewarganegaraan (Pilih salah satu) "
                                                                                                                    Width="223px"></asp:Label></td>
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:RadioButtonList ID="RbB1_IdenPengirimNas_Ind_kewarganegaraanOld" runat="server"
                                                                                                                    RepeatDirection="Horizontal" Enabled="False">
                                                                                                                    <asp:ListItem Value="1">WNI</asp:ListItem>
                                                                                                                    <asp:ListItem Value="2">WNA</asp:ListItem>
                                                                                                                </asp:RadioButtonList>
                                                                                                            </td>
                                                                                                            <%--<td style="width: 20%">
                                                                                                                <asp:RadioButtonList ID="RbB1_IdenPengirimNas_Ind_kewarganegaraanNew" runat="server"
                                                                                                                    RepeatDirection="Horizontal" Enabled="False">
                                                                                                                    <asp:ListItem Value="1">WNI</asp:ListItem>
                                                                                                                    <asp:ListItem Value="2">WNA</asp:ListItem>
                                                                                                                </asp:RadioButtonList>
                                                                                                            </td>--%>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Label34" runat="server" Text="d. Negara "></asp:Label>
                                                                                                                <br />
                                                                                                                <asp:Label ID="Label40" runat="server" Font-Italic="True" Text="(Wajib diisi apabila kewarganegaraan WNA)"
                                                                                                                    Width="258px"></asp:Label>
                                                                                                            </td>
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_negaraOld" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                <asp:HiddenField ID="HfB1IdenPengirimNas_Ind_negaraOld" runat="server" />
                                                                                                            </td>
                                                                                                           <%-- <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_negaraNew" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                <asp:HiddenField ID="HfB1IdenPengirimNas_Ind_negaraNew" runat="server" />
                                                                                                            </td>--%>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Label47" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_negaralainOld" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                            <%--<td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_negaralainNew" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                            </td>--%>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Label35" runat="server" Text="e. Pekerjaan"></asp:Label>
                                                                                                                <asp:Label ID="Label266" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_pekerjaanOld" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                <asp:HiddenField ID="HfB1IdenPengirimNas_Ind_pekerjaanOld" runat="server" />
                                                                                                            </td>
                                                                                                           <%-- <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_pekerjaanNew" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                <asp:HiddenField ID="HfB1IdenPengirimNas_Ind_pekerjaanNew" runat="server" />
                                                                                                            </td>--%>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labelu45" runat="server" Text="Pekerjaan Lainnya"></asp:Label></td>
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_PekerjaanlainOld" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                            <%--<td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_PekerjaanlainNew" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                            </td>--%>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labely37" runat="server" Text="f. Alamat Domisili"></asp:Label></td>
                                                                                                            <td style="width: 20%">
                                                                                                            </td>
                                                                                                            <%--<td style="width: 20%">
                                                                                                            </td>--%>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6; height: 20px;">
                                                                                                                <asp:Label ID="Labely38" runat="server" Text="Alamat"></asp:Label>
                                                                                                            </td>
                                                                                                            <td style="width: 20%; height: 20px;">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_AlamatOld" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                                                           <%-- <td style="width: 20%; height: 20px;">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_AlamatNew" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                                                            </td>--%>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; height: 18px; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labelo39" runat="server" Text="Kota / Kabupaten "></asp:Label></td>
                                                                                                            <td style="height: 18px; width: 20%;">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_KotaOld" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                <asp:HiddenField ID="HfB1IdenPengirimNas_Ind_Kota_domOld" runat="server" />
                                                                                                            </td>
                                                                                                           <%-- <td style="height: 18px; width: 20%;">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_KotaNew" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                <asp:HiddenField ID="HfB1IdenPengirimNas_Ind_Kota_domNew" runat="server" />
                                                                                                            </td>--%>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; height: 18px; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labelk57" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                                                            <td style="width: 20%; height: 18px">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_kotalainOld" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                            <%--<td style="width: 20%; height: 18px">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_kotalainNew" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                            </td>--%>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labelj41" runat="server" Text="Provinsi"></asp:Label></td>
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_provinsiOld" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                <asp:HiddenField ID="HfB1IdenPengirimNas_Ind_provinsi_domOld" runat="server" />
                                                                                                            </td>
                                                                                                           <%-- <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_provinsiNew" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                <asp:HiddenField ID="HfB1IdenPengirimNas_Ind_provinsi_domNew" runat="server" />
                                                                                                            </td>--%>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labeli186" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_provinsiLainOld" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                            <%--<td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_provinsiLainNew" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                            </td>--%>
                                                                                                        </tr>
                                                                                                      
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labelj43" runat="server" Text="Alamat"></asp:Label>
                                                                                                                <asp:Label ID="Labelj267" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_alamatIdentitasOld" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                                                            <%--<td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_alamatIdentitasNew" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                                                            </td>--%>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labelj44" runat="server" Text="Kota / Kabupaten"></asp:Label>
                                                                                                                <asp:Label ID="Labelj268" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_kotaIdentitasOld" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                <asp:HiddenField ID="HfB1IdenPengirimNas_Ind_kotaIdentitasOld" runat="server" />
                                                                                                            </td>
                                                                                                           <%-- <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_kotaIdentitasNew" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                <asp:HiddenField ID="HfB1IdenPengirimNas_Ind_kotaIdentitasNew" runat="server" />
                                                                                                            </td>--%>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; height: 18px; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labeli83" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                                                            <td style="width: 20%; height: 18px">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_KotaLainIdenOld" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                           <%-- <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_KotaLainIdenNew" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                            </td>--%>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                                                                                <asp:Label ID="Labeol46" runat="server" Text="Provinsi"></asp:Label>
                                                                                                                <asp:Label ID="Labeli269" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                            <td style="height: 18px; width: 20%;">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_ProvinsiIdenOld" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                <asp:HiddenField ID="HfB1IdenPengirimNas_Ind_ProvinsiIdenOld" runat="server" />
                                                                                                            </td>
                                                                                                            <%--<td style="height: 18px; width: 20%;">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_ProvinsiIdenNew" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                <asp:HiddenField ID="HfB1IdenPengirimNas_Ind_ProvinsiIdenNew" runat="server" />
                                                                                                            </td>--%>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; height: 18px; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labeli62" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                                                            <td style="width: 20%; height: 18px">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_ProvinsilainIdenOld" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                            <%--<td style="width: 20%; height: 18px">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_ProvinsilainIdenNew" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                            </td>--%>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labeli48" runat="server" Text="h. Jenis Dokument Identitas"></asp:Label>
                                                                                                                <asp:Label ID="Labeli270" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:DropDownList ID="CboB1PengirimNasInd_jenisidentitasOld" runat="server" Enabled="False">
                                                                                                                </asp:DropDownList></td>
                                                                                                           <%-- <td style="width: 20%">
                                                                                                                <asp:DropDownList ID="CboB1PengirimNasInd_jenisidentitasNew" runat="server" Enabled="False">
                                                                                                                </asp:DropDownList>
                                                                                                            </td>--%>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                                                                                <asp:Label ID="LabeliI49" runat="server" Text="Nomor Identitas"></asp:Label>
                                                                                                                <asp:Label ID="LabeliI271" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                            <td style="height: 18px; width: 20%;">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_noIdentitasOld" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                            <%--<td style="width: 20%; height: 18px;">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_noIdentitasNew" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                            </td>--%>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6; height: 15px;">
                                                                                                            </td>
                                                                                                            <td style="height: 15px; width: 20%;">
                                                                                                            </td>
                                                                                                            <%--<td style="width: 20%; height: 15px;">
                                                                                                            </td>--%>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </asp:View>
                                                                                                <asp:View ID="ViewB1PengirimNasabah_Korp" runat="server">
                                                                                                    <table style="height: 31px" width="100%">
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; height: 15px; background-color: #fff7e6">
                                                                                                                <asp:Label ID="LabelIu51" runat="server" Text="Korporasi" Font-Bold="True"></asp:Label>
                                                                                                            </td>
                                                                                                            <td style="height: 15px; color: #000000; width: 20%;">
                                                                                                            </td>
                                                                                                            
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="center" style="width: 5%; height: 22px; background-color: silver">
                                                                                                                <asp:Label ID="Label52" runat="server" Font-Bold="True" Text="Field"></asp:Label></td>
                                                                                                            <td align="center" style="width: 20%; background-color: silver">
                                                                                                                <asp:Label ID="Label53" runat="server" Font-Bold="True" Text="Old Data"></asp:Label></td>
                                                                                                            
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labeu9306" runat="server" Text="a. Bentuk Badan Usaha "></asp:Label>
                                                                                                            </td>
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Corp_BentukBadanUsahaOld" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                            </td>
                                                                                                           
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labelb9307" runat="server" Text="Bentuk Badan Usaha Lainnya " Width="214px"></asp:Label>
                                                                                                            </td>
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Corp_BentukBadanUsahaLainOld" runat="server"
                                                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                            </td>
                                                                                                            
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Label5i5" runat="server" Text="b. Nama Korporasi" Width="88px"></asp:Label>
                                                                                                                <asp:Label ID="Labeli2" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                            </td>
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Corp_NamaKorpOld" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                            </td>
                                                                                                            
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labelr4" runat="server" Text="c. Bidang Usaha Korporasi"></asp:Label>
                                                                                                                <asp:Label ID="Labelo273" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                            </td>
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Corp_BidangUsahaKorpOld" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                <asp:HiddenField ID="HfB1IdenPengirimNas_Corp_BidangUsahaKorpOld" runat="server" />
                                                                                                            </td>
                                                                                                            
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Label6" runat="server" Text="Bidang Usaha Korporasi Lainnya " Width="214px"></asp:Label>
                                                                                                            </td>
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Corp_BidangUsahaLainnyaKorpOld" runat="server"
                                                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                            </td>
                                                                                                            
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labelk58" runat="server" Text="d. Alamat Lengkap Korporasi"></asp:Label></td>
                                                                                                            <td style="width: 20%">
                                                                                                            </td>
                                                                                                            
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labelk59" runat="server" Text="Alamat"></asp:Label>
                                                                                                                <asp:Label ID="Labelk274" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Corp_alamatkorpOld" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="342px" Enabled="False"></asp:TextBox></td>
                                                                                                            
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labelk61" runat="server" Text="Kota / Kabupaten "></asp:Label>
                                                                                                                <asp:Label ID="Labelk275" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Corp_kotakorpOld" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                <asp:HiddenField ID="HfB1IdenPengirimNas_Corp_kotakorpOld" runat="server" />
                                                                                                            </td>
                                                                                                            
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labeli69" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Corp_kotakorplainOld" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                            
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labelu63" runat="server" Text="Provinsi"></asp:Label>
                                                                                                                <asp:Label ID="Label2u76" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Corp_provKorpOld" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                <asp:HiddenField ID="HfB1IdenPengirimNas_Corp_provKorpOld" runat="server" />
                                                                                                            </td>
                                                                                                            
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labelu78" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Corp_provKorpLainOld" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                            
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labelu64" runat="server" Text="e. Alamat Korporasi di Luar Negeri"></asp:Label></td>
                                                                                                            <td style="width: 20%">
                                                                                                            </td>
                                                                                                            
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labelu9258" runat="server" Text="Alamat"></asp:Label></td>
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Corp_AlamatLuarOld" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                           
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labelu60" runat="server" Text="f. Alamat Cabang Pengirim Asal"></asp:Label></td>
                                                                                                            <td style="width: 20%">
                                                                                                            </td>
                                                                                                            
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labelu9259" runat="server" Text="Alamat"></asp:Label></td>
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Corp_AlamatAsalOld" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                            
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </asp:View>
                                                                                            </asp:MultiView>
                                                                                         </td>
                                                                                         <td style="width: 100%">
                                                                                            <asp:MultiView ID="MultiViewB1PengirimNasabah_new" runat="server" ActiveViewIndex="0">
                                                                                                <asp:View ID="ViewB1PengirimNasabah_IND_new" runat="server">
                                                                                                    <table style="width: 100%; height: 49px">
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; height: 15px; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Label31_new" runat="server" Text="Perorangan " Font-Bold="True"></asp:Label>
                                                                                                            </td>
                                                                                                            <%--<td style="height: 15px; width: 5%;">
                                                                                                            </td>--%>
                                                                                                            <td style="height: 15px; width: 20%;">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="center" style="width: 5%; height: 22px; background-color: silver">
                                                                                                                <asp:Label ID="Label61_new" runat="server" Font-Bold="True" Text="Field"></asp:Label></td>
                                                                                                            <%--<td align="center" style="width: 5%; background-color: silver">
                                                                                                                <asp:Label ID="Label89" runat="server" Font-Bold="True" Text="Old Data"></asp:Label></td>--%>
                                                                                                            <td style="width: 20%; background-color: silver">
                                                                                                                <asp:Label ID="Label90" runat="server" Font-Bold="True" Text="New Data"></asp:Label></td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Label91_new" runat="server" Text="a. Nama Lengkap"></asp:Label>
                                                                                                                <asp:Label ID="Label92_new" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                            <%--<td style="width: 5%">
                                                                                                                <asp:TextBox ID="TextBox1" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>--%>
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_NamaLengkapNew" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Label93_new" runat="server" Text="b. Tanggal lahir (tgl/bln/thn)" Width="135px"></asp:Label>
                                                                                                                <asp:Label ID="Label94_new" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                            <%--<td style="width: 5%">
                                                                                                                <asp:TextBox ID="TextBox3" runat="server" CssClass="searcheditbox"
                                                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                            </td>--%>
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_tanggalLahirNew" runat="server" CssClass="searcheditbox"
                                                                                                                    TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                &nbsp;</td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Label95_new" runat="server" Text="c. Kewarganegaraan (Pilih salah satu) "
                                                                                                                    Width="223px"></asp:Label></td>
                                                                                                           <%-- <td style="width: 5%">
                                                                                                                <asp:RadioButtonList ID="RadioButtonList1" runat="server"
                                                                                                                    RepeatDirection="Horizontal" Enabled="False">
                                                                                                                    <asp:ListItem Value="1">WNI</asp:ListItem>
                                                                                                                    <asp:ListItem Value="2">WNA</asp:ListItem>
                                                                                                                </asp:RadioButtonList>
                                                                                                            </td>--%>
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:RadioButtonList ID="RbB1_IdenPengirimNas_Ind_kewarganegaraanNew" runat="server"
                                                                                                                    RepeatDirection="Horizontal" Enabled="False">
                                                                                                                    <asp:ListItem Value="1">WNI</asp:ListItem>
                                                                                                                    <asp:ListItem Value="2">WNA</asp:ListItem>
                                                                                                                </asp:RadioButtonList>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Label96_new" runat="server" Text="d. Negara "></asp:Label>
                                                                                                                <br />
                                                                                                                <asp:Label ID="Label97_new" runat="server" Font-Italic="True" Text="(Wajib diisi apabila kewarganegaraan WNA)"
                                                                                                                    Width="258px"></asp:Label>
                                                                                                            </td>
                                                                                                           <%-- <td style="width: 5%">
                                                                                                                <asp:TextBox ID="TextBox5" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                <asp:HiddenField ID="HiddenField1" runat="server" />
                                                                                                            </td>--%>
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_negaraNew" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                <asp:HiddenField ID="HfB1IdenPengirimNas_Ind_negaraNew" runat="server" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Label98_new" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                                                            <%--<td style="width: 5%">
                                                                                                                <asp:TextBox ID="TextBox7" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>--%>
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_negaralainNew" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Label99_new" runat="server" Text="e. Pekerjaan"></asp:Label>
                                                                                                                <asp:Label ID="Label100_new" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                           <%-- <td style="width: 5%">
                                                                                                                <asp:TextBox ID="TextBox9" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                <asp:HiddenField ID="HiddenField3" runat="server" />
                                                                                                            </td>--%>
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_pekerjaanNew" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                <asp:HiddenField ID="HfB1IdenPengirimNas_Ind_pekerjaanNew" runat="server" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Label101_new" runat="server" Text="Pekerjaan Lainnya"></asp:Label></td>
                                                                                                            <%--<td style="width: 5%">
                                                                                                                <asp:TextBox ID="TextBox11" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>--%>
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_PekerjaanlainNew" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Label102_new" runat="server" Text="f. Alamat Domisili"></asp:Label></td>
                                                                                                            <%--<td style="width: 5%">
                                                                                                            </td>--%>
                                                                                                            <td style="width: 20%">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6; height: 20px;">
                                                                                                                <asp:Label ID="Label103_new" runat="server" Text="Alamat"></asp:Label>
                                                                                                            </td>
                                                                                                            <%--<td style="width: 5%; height: 20px;">
                                                                                                                <asp:TextBox ID="TextBox13" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>--%>
                                                                                                            <td style="width: 20%; height: 20px;">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_AlamatNew" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; height: 18px; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Label104_new" runat="server" Text="Kota / Kabupaten "></asp:Label></td>
                                                                                                            <%--<td style="height: 18px; width: 5%;">
                                                                                                                <asp:TextBox ID="TextBox15" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                <asp:HiddenField ID="HiddenField5" runat="server" />
                                                                                                            </td>--%>
                                                                                                            <td style="height: 18px; width: 20%;">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_KotaNew" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                <asp:HiddenField ID="HfB1IdenPengirimNas_Ind_Kota_domNew" runat="server" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; height: 18px; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Label105_new" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                                                            <%--<td style="width: 5%; height: 18px">
                                                                                                                <asp:TextBox ID="TextBox17" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>--%>
                                                                                                            <td style="width: 20%; height: 18px">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_kotalainNew" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Label106_new" runat="server" Text="Provinsi"></asp:Label></td>
                                                                                                           <%-- <td style="width: 5%">
                                                                                                                <asp:TextBox ID="TextBox19" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                <asp:HiddenField ID="HiddenField7" runat="server" />
                                                                                                            </td>--%>
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_provinsiNew" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                <asp:HiddenField ID="HfB1IdenPengirimNas_Ind_provinsi_domNew" runat="server" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labeli186_new" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                                                            <%--<td style="width: 5%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_provinsiLainOld" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>--%>
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_provinsiLainNew" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                       
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labelj43_new" runat="server" Text="Alamat"></asp:Label>
                                                                                                                <asp:Label ID="Labelj26_new7" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                            <%--<td style="width: 5%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_alamatIdentitasOld" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>--%>
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_alamatIdentitasNew" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labelj44_new" runat="server" Text="Kota / Kabupaten"></asp:Label>
                                                                                                                <asp:Label ID="Labelj268_new" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                            <%--<td style="width: 5%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_kotaIdentitasOld" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                <asp:HiddenField ID="HfB1IdenPengirimNas_Ind_kotaIdentitasOld" runat="server" />
                                                                                                            </td>--%>
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_kotaIdentitasNew" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                <asp:HiddenField ID="HfB1IdenPengirimNas_Ind_kotaIdentitasNew" runat="server" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; height: 18px; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labeli83_new" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                                                           <%-- <td style="width: 5%; height: 18px">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_KotaLainIdenOld" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>--%>
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_KotaLainIdenNew" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                                                                                <asp:Label ID="Labeol46_new" runat="server" Text="Provinsi"></asp:Label>
                                                                                                                <asp:Label ID="Labeli269_new" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                            <%--<td style="height: 18px; width: 5%;">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_ProvinsiIdenOld" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                <asp:HiddenField ID="HfB1IdenPengirimNas_Ind_ProvinsiIdenOld" runat="server" />
                                                                                                            </td>--%>
                                                                                                            <td style="height: 18px; width: 20%;">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_ProvinsiIdenNew" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                <asp:HiddenField ID="HfB1IdenPengirimNas_Ind_ProvinsiIdenNew" runat="server" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; height: 18px; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labeli62_new" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                                                            <%--<td style="width: 5%; height: 18px">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_ProvinsilainIdenOld" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>--%>
                                                                                                            <td style="width: 20%; height: 18px">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_ProvinsilainIdenNew" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labeli48_new" runat="server" Text="h. Jenis Dokument Identitas"></asp:Label>
                                                                                                                <asp:Label ID="Labeli270_new" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                           <%-- <td style="width: 5%">
                                                                                                                <asp:DropDownList ID="CboB1PengirimNasInd_jenisidentitasOld" runat="server" Enabled="False">
                                                                                                                </asp:DropDownList></td>--%>
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:DropDownList ID="CboB1PengirimNasInd_jenisidentitasNew" runat="server" Enabled="False">
                                                                                                                </asp:DropDownList>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                                                                                <asp:Label ID="LabeliI49_new" runat="server" Text="Nomor Identitas"></asp:Label>
                                                                                                                <asp:Label ID="LabeliI271_new" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                           <%-- <td style="height: 18px; width: 5%;">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_noIdentitasOld" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>--%>
                                                                                                            <td style="width: 20%; height: 18px;">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Ind_noIdentitasNew" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6; height: 15px;">
                                                                                                            </td>
                                                                                                           <%-- <td style="height: 15px; width: 5%;">
                                                                                                            </td>--%>
                                                                                                            <td style="width: 20%; height: 15px;">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </asp:View>
                                                                                                <asp:View ID="ViewB1PengirimNasabah_Korp_new" runat="server">
                                                                                                    <table style="height: 31px" width="100%">
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; height: 15px; background-color: #fff7e6">
                                                                                                                <asp:Label ID="LabelIu51_new" runat="server" Text="Korporasi" Font-Bold="True"></asp:Label>
                                                                                                            </td>
                                                                                                            
                                                                                                            <td style="height: 15px; width: 20%; color: #000000;">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="center" style="width: 5%; height: 22px; background-color: silver">
                                                                                                                <asp:Label ID="Label52_new" runat="server" Font-Bold="True" Text="Field"></asp:Label></td>
                                                                                                            
                                                                                                            <td style="width: 20%; background-color: silver">
                                                                                                                <asp:Label ID="Label54_new" runat="server" Font-Bold="True" Text="New Data"></asp:Label></td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labeu9306_new" runat="server" Text="a. Bentuk Badan Usaha "></asp:Label>
                                                                                                            </td>
                                                                                                            
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Corp_BentukBadanUsahaNew" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labelb9307_new" runat="server" Text="Bentuk Badan Usaha Lainnya " Width="214px"></asp:Label>
                                                                                                            </td>
                                                                                                           
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Corp_BentukBadanUsahaLainNew" runat="server"
                                                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Label5i5_new" runat="server" Text="b. Nama Korporasi" Width="88px"></asp:Label>
                                                                                                                <asp:Label ID="Labeli2_new" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                            </td>
                                                                                                           
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Corp_NamaKorpNew" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labelr4_new" runat="server" Text="c. Bidang Usaha Korporasi"></asp:Label>
                                                                                                                <asp:Label ID="Labelo273_new" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                            </td>
                                                                                                            
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Corp_BidangUsahaKorpNew" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                <asp:HiddenField ID="HfB1IdenPengirimNas_Corp_BidangUsahaKorpNew" runat="server" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Label6_new" runat="server" Text="Bidang Usaha Korporasi Lainnya " Width="214px"></asp:Label>
                                                                                                            </td>
                                                                                                           
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Corp_BidangUsahaLainnyaKorpNew" runat="server"
                                                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labelk58_new" runat="server" Text="d. Alamat Lengkap Korporasi"></asp:Label></td>
                                                                                                            
                                                                                                            <td style="width: 20%">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labelk59_new" runat="server" Text="Alamat"></asp:Label>
                                                                                                                <asp:Label ID="Labelk274_new" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                            
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Corp_alamatkorpNew" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="342px" Enabled="False"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labelk61_new" runat="server" Text="Kota / Kabupaten "></asp:Label>
                                                                                                                <asp:Label ID="Labelk275_new" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                           
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Corp_kotakorpNew" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                <asp:HiddenField ID="HfB1IdenPengirimNas_Corp_kotakorpNew" runat="server" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labeli69_new" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                                                            
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Corp_kotakorplainNew" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labelu63_new" runat="server" Text="Provinsi"></asp:Label>
                                                                                                                <asp:Label ID="Label2u76_new" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                           
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Corp_provKorpNew" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                <asp:HiddenField ID="HfB1IdenPengirimNas_Corp_provKorpNew" runat="server" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labelu78_new" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                                                           
                                                                                                            <td style="width: 20%; height: 15px">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Corp_provKorpLainNew" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labelu64_new" runat="server" Text="e. Alamat Korporasi di Luar Negeri"></asp:Label></td>
                                                                                                            
                                                                                                            <td style="width: 20%">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labelu9258_new" runat="server" Text="Alamat"></asp:Label></td>
                                                                                                           
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Corp_AlamatLuarNew" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labelu60_new" runat="server" Text="f. Alamat Cabang Pengirim Asal"></asp:Label></td>
                                                                                                            
                                                                                                            <td style="width: 20%">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                                <asp:Label ID="Labelu9259_new" runat="server" Text="Alamat"></asp:Label></td>
                                                                                                            
                                                                                                            <td style="width: 20%">
                                                                                                                <asp:TextBox ID="TxtB1IdenPengirimNas_Corp_AlamatAsalNew" runat="server" CssClass="searcheditbox"
                                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </asp:View>
                                                                                            </asp:MultiView>
                                                                                         </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </asp:View>
                                                                <asp:View ID="ViewB1SWIFTOutIdenPengirimNonNasabah" runat="server">
                                                                    <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                                        bgcolor="#dddddd" border="2">
                                                                        <tr>
                                                                            <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                                                style="height: 7px">
                                                                                <table cellpadding="0" cellspacing="0" border="0">
                                                                                    <tr>
                                                                                        <td class="formtext" style="height: 14px">
                                                                                            <asp:Label ID="Labelb4" runat="server" Text="Non Nasabah" Font-Bold="True"></asp:Label>
                                                                                        </td>
                                                                                        <td style="height: 14px">
                                                                                            <a href="#" onclick="javascript:ShowHidePanel('B1identitasNonSwiftOutPengirimNonNasabah','Img3','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                                                title="click to minimize or maximize">
                                                                                                <img id="Img3" src="Images/search-bar-minimize.gif" border="0" height="12" width="12" />
                                                                                            </a>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="B1identitasNonSwiftOutPengirimNonNasabah">
                                                                            <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                                </table>
                                                                                <table style="height: 49px; width: 100%;">
                                                                                    <tr>
                                                                                        <td align="center" style="width: 5%; height: 22px; background-color: silver">
                                                                                            <asp:Label ID="Label55" runat="server" Font-Bold="True" Text="Field"></asp:Label></td>
                                                                                        <td align="center" style="width: 5%; background-color: silver">
                                                                                            <asp:Label ID="Label56" runat="server" Font-Bold="True" Text="Old Data"></asp:Label></td>
                                                                                        <td style="width: 20%; background-color: silver">
                                                                                            <asp:Label ID="Label57" runat="server" Font-Bold="True" Text="New Data"></asp:Label></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="background-color: #fff7e6; width: 5%;">
                                                                                        </td>
                                                                                        <td style="background-color: #fff7e6; width: 5%;">
                                                                                            <asp:RadioButtonList ID="RbB1PengirimNonNasabah_100JutaOld" runat="server" Enabled="false"
                                                                                                RepeatDirection="Horizontal">
                                                                                                <asp:ListItem Value="0">< 100 Juta (Kurs yang digunakan</asp:ListItem>
                                                                                                <asp:ListItem Value="1">> 100 Juta (Kurs yang digunakan</asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                        </td>
                                                                                        <td style="background-color: #fff7e6;" width="20%">
                                                                                            <asp:RadioButtonList ID="RbB1PengirimNonNasabah_100JutaNew" runat="server" RepeatDirection="Horizontal"
                                                                                                Enabled="false">
                                                                                                <asp:ListItem Value="0">< 100 Juta (Kurs yang digunakan</asp:ListItem>
                                                                                                <asp:ListItem Value="1">> 100 Juta (Kurs yang digunakan</asp:ListItem>
                                                                                            </asp:RadioButtonList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 5%; background-color: #fff7e6">
                                                                                            <asp:Label ID="Labely81" runat="server" Text="a. Nama Lengkap"></asp:Label>
                                                                                            <asp:Label ID="Labely85" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                        <td style="width: 5%">
                                                                                            <asp:TextBox ID="TxtB1PengirimNonNasabah_namaOld" runat="server" CssClass="searcheditbox"
                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                        <td style="width: 20%">
                                                                                            <asp:TextBox ID="TxtB1PengirimNonNasabah_namaNew" runat="server" CssClass="searcheditbox"
                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 5%; background-color: #fff7e6">
                                                                                            <asp:Label ID="Labele82" runat="server" Text="b. Tanggal lahir (tgl/bln/thn)" Width="162px"
                                                                                                Height="16px"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 5%">
                                                                                            <asp:TextBox ID="TxtB1PengirimNonNasabah_TanggalLahirOld" runat="server" CssClass="searcheditbox"
                                                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                        </td>
                                                                                        <td style="width: 20%">
                                                                                            <asp:TextBox ID="TxtB1PengirimNonNasabah_TanggalLahirNew" runat="server" CssClass="searcheditbox"
                                                                                                TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            &nbsp;</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 5%; background-color: #fff7e6">
                                                                                            <asp:Label ID="Labeil95" runat="server" Text="c. Alamat Sesuai Bukti Identitas"></asp:Label></td>
                                                                                        <td style="width: 5%">
                                                                                        </td>
                                                                                        <td style="width: 20%">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 5%; background-color: #fff7e6">
                                                                                            <asp:Label ID="Labelr96" runat="server" Text="Alamat"></asp:Label>
                                                                                            <asp:Label ID="Label9324" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 5%">
                                                                                            <asp:TextBox ID="TxtB1PengirimNonNasabah_alamatidenOld" runat="server" CssClass="searcheditbox"
                                                                                                MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                                        <td style="width: 20%">
                                                                                            <asp:TextBox ID="TxtB1PengirimNonNasabah_alamatidenNew" runat="server" CssClass="searcheditbox"
                                                                                                MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 5%; background-color: #fff7e6">
                                                                                            <asp:Label ID="Labelr97" runat="server" Text="Kota / Kabupaten"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 5%" align="left">
                                                                                            <asp:TextBox ID="TxtB1PengirimNonNasabah_kotaIdenOld" runat="server" CssClass="searcheditbox"
                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            <asp:HiddenField ID="HfB1PengirimNonNasabah_kotaIdenOld" runat="server" />
                                                                                        </td>
                                                                                        <td style="width: 20%">
                                                                                            <asp:TextBox ID="TxtB1PengirimNonNasabah_kotaIdenNew" runat="server" CssClass="searcheditbox"
                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            <asp:HiddenField ID="HfB1PengirimNonNasabah_kotaIdenNew" runat="server" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 5%; background-color: #fff7e6">
                                                                                            <asp:Label ID="Labeli88" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                                        <td style="width: 5%">
                                                                                            <asp:TextBox ID="TxtB1PengirimNonNasabah_KotaLainIdenOld" runat="server" CssClass="searcheditbox"
                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                        <td style="width: 20%">
                                                                                            <asp:TextBox ID="TxtB1PengirimNonNasabah_KotaLainIdenNew" runat="server" CssClass="searcheditbox"
                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 5%; background-color: #fff7e6;">
                                                                                            <asp:Label ID="Labeli98" runat="server" Text="Provinsi"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 5%" align="left">
                                                                                            <asp:TextBox ID="TxtB1PengirimNonNasabah_ProvIdenOld" runat="server" CssClass="searcheditbox"
                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            <asp:HiddenField ID="HfB1PengirimNonNasabah_ProvIdenOld" runat="server" />
                                                                                        </td>
                                                                                        <td style="width: 20%" align="left">
                                                                                            <asp:TextBox ID="TxtB1PengirimNonNasabah_ProvIdenNew" runat="server" CssClass="searcheditbox"
                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            <asp:HiddenField ID="HfB1PengirimNonNasabah_ProvIdenNew" runat="server" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 5%; background-color: #fff7e6">
                                                                                            <asp:Label ID="Labelo99" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                                        <td style="width: 5%;">
                                                                                            <asp:TextBox ID="TxtB1PengirimNonNasabah_ProvLainIdenOld" runat="server" CssClass="searcheditbox"
                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                        <td style="width: 20%;">
                                                                                            <asp:TextBox ID="TxtB1PengirimNonNasabah_ProvLainIdenNew" runat="server" CssClass="searcheditbox"
                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 5%; background-color: #fff7e6">
                                                                                            <asp:Label ID="Label84" runat="server" Text="d. No Telepon"></asp:Label></td>
                                                                                        <td style="width: 5%;">
                                                                                            <asp:TextBox ID="TxtB1PengirimNonNasabah_NoTeleponOld" runat="server" CssClass="searcheditbox"
                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                        <td style="width: 20%;">
                                                                                            <asp:TextBox ID="TxtB1PengirimNonNasabah_NoTeleponNew" runat="server" CssClass="searcheditbox"
                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 5%; background-color: #fff7e6; height: 24px;">
                                                                                            <asp:Label ID="Label1o00" runat="server" Text="e. Jenis Dokument Identitas"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 5%; height: 24px;">
                                                                                            <asp:DropDownList ID="CboB1PengirimNonNasabah_JenisDokumenOld" runat="server" Enabled="False">
                                                                                            </asp:DropDownList></td>
                                                                                        <td style="width: 20%; height: 24px;">
                                                                                            <asp:DropDownList ID="CboB1PengirimNonNasabah_JenisDokumenNew" runat="server" Enabled="False">
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 5%; background-color: #fff7e6;">
                                                                                            <asp:Label ID="Labelo101" runat="server" Text="Nomor Identitas"></asp:Label>
                                                                                        </td>
                                                                                        <td style="width: 5%;">
                                                                                            <asp:TextBox ID="TxtB1PengirimNonNasabah_NomorIdenOld" runat="server" CssClass="searcheditbox"
                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                        <td style="width: 20%;">
                                                                                            <asp:TextBox ID="TxtB1PengirimNonNasabah_NomorIdenNew" runat="server" CssClass="searcheditbox"
                                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 5%; background-color: #fff7e6;">
                                                                                        </td>
                                                                                        <td style="width: 5%;">
                                                                                        </td>
                                                                                        <td style="width: 20%;">
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </asp:View>
                                                            </asp:MultiView>
                                                        </td>
                                                    </tr>
                                                </asp:View>
                                                <asp:View ID="viewB2" runat="server">
                                                    <table width="100%">
                                                        <tr>
                                                            <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                                style="height: 6px" bgcolor="White">
                                                                <table cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td class="formtext">
                                                                            <asp:Label ID="Label7" runat="server" Text="B.2 Identitas Pengirim" Font-Bold="True"></asp:Label>
                                                                            <a href="#" onclick="javascript:ShowHidePanel('BSWIFTOutIdentitasPengirim','Img8','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                                title="click to minimize or maximize">
                                                                                <img id="Img15" src="Images/search-bar-minimize.gif" border="0" height="12px" width="12px" />
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <tr id="B2SWIFTOutIdentitasPengirim">
                                                        <td valign="top" bgcolor="White" style="height: 125px" width="100%">
                                                            <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                            </table>
                                                            <table width="100%">
                                                                <tr>
                                                                    <td style="width: 5%; background-color: #fff7e6; height: 27px;">
                                                                        <asp:Label ID="Label8" runat="server" Text="No. Rek " Width="52px"></asp:Label>
                                                                        <asp:Label ID="Label9" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                    <td style="width: 5%; height: 27px;" align="left">
                                                                        <asp:TextBox ID="TxtB2SWIFTOutPengirim_rekeningOld" runat="server" CssClass="searcheditbox"
                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                    <td style="width: 20%;height: 27px;" align="left">
                                                                        <asp:TextBox ID="TxtB2SWIFTOutPengirim_rekeningNew" runat="server" CssClass="searcheditbox"
                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr id="tr3" runat="server" visible="False">
                                                                    <td style="width: 5%; height: 34px; background-color: #fff7e6">
                                                                        <asp:Label ID="Label18" runat="server" Text="Tipe Nasabah "></asp:Label>
                                                                        <asp:Label ID="Label22" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 5%; height: 34px" align="left">
                                                                        <ajax:AjaxPanel ID="AjaxPanel13" runat="server">
                                                                            <asp:RadioButtonList ID="RbB2_IdenPengirimNas_TipeNasabahOld" runat="server" AutoPostBack="True"
                                                                                RepeatDirection="Horizontal" Enabled="False">
                                                                                <asp:ListItem Value="1">Perorangan</asp:ListItem>
                                                                                <asp:ListItem Value="2">Korporasi</asp:ListItem>
                                                                            </asp:RadioButtonList>
                                                                        </ajax:AjaxPanel>
                                                                    </td>
                                                                    <td style="width: 20%;height: 34px" align="left">
                                                                        <ajax:AjaxPanel ID="AjaxPanel14" runat="server">
                                                                            <asp:RadioButtonList ID="RbB2_IdenPengirimNas_TipeNasabahNew" runat="server" AutoPostBack="True"
                                                                                RepeatDirection="Horizontal" Enabled="False">
                                                                                <asp:ListItem Value="1">Perorangan</asp:ListItem>
                                                                                <asp:ListItem Value="2">Korporasi</asp:ListItem>
                                                                            </asp:RadioButtonList>
                                                                        </ajax:AjaxPanel>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                                bgcolor="#dddddd" border="2">
                                                                <tr>
                                                                    <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                                        style="height: 6px">
                                                                        <table cellpadding="0" cellspacing="0" border="0">
                                                                            <tr>
                                                                                <td class="formtext">
                                                                                    <asp:Label ID="Label23" runat="server" Text="Nasabah" Font-Bold="True"></asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <a href="#" onclick="javascript:ShowHidePanel('BSWIFTOutIdentitasPengirim1nasabah','Imgs9','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                                        title="click to minimize or maximize">
                                                                                        <img id="Img2" src="Images/search-bar-minimize.gif" border="0" height="12px" width="12px" /></a>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr id="Tr4">
                                                                    <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                                        <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                            border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                        </table>
                                                                        <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                            border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                            <tr>
                                                                                <td style="width: 100%">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 100%">
                                                                                    <asp:MultiView ID="MultiViewB2PengirimNasabah" runat="server" ActiveViewIndex="0">
                                                                                        <asp:View ID="ViewB2PengirimNasabah_IND" runat="server">
                                                                                            <table style="width: 100%; height: 49px">
                                                                                                <tr>
                                                                                                    <td style="width: 5%; height: 15px; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label24" runat="server" Text="Perorangan " Font-Bold="True"></asp:Label>
                                                                                                    </td>
                                                                                                    <td style="height: 15px; width: 20%;">
                                                                                                    </td>
                                                                                                    <%--<td style="height: 15px; width: 20%;">
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label16" runat="server" Text="a. Nama Bank"></asp:Label>
                                                                                                        <asp:Label ID="Label17" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_NamaBankOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                    <%--<td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_NamaBankNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label36" runat="server" Text="b. Nama Lengkap"></asp:Label>
                                                                                                        <asp:Label ID="Label37" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_NamaLengkapOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                    <%--<td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_NamaLengkapNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label38" runat="server" Text="c. Tanggal lahir (tgl/bln/thn)" Width="183px"></asp:Label>
                                                                                                    </td>
                                                                                                   <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_tanggalLahirOld" runat="server" CssClass="searcheditbox"
                                                                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>
                                                                                                   <%-- <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_tanggalLahirNew" runat="server" CssClass="searcheditbox"
                                                                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        &nbsp;</td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label41" runat="server" Text="d. Kewarganegaraan (Pilih salah satu) "
                                                                                                            Width="223px"></asp:Label></td>
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:RadioButtonList ID="RbB2_IdenPengirimNas_Ind_kewarganegaraanOld" runat="server"
                                                                                                            RepeatDirection="Horizontal" Enabled="False">
                                                                                                            <asp:ListItem Value="1">WNI</asp:ListItem>
                                                                                                            <asp:ListItem Value="2">WNA</asp:ListItem>
                                                                                                        </asp:RadioButtonList>
                                                                                                    </td>
                                                                                                    <%--<td style="width: 20%">
                                                                                                        <asp:RadioButtonList ID="RbB2_IdenPengirimNas_Ind_kewarganegaraanNew" runat="server"
                                                                                                            RepeatDirection="Horizontal" Enabled="False">
                                                                                                            <asp:ListItem Value="1">WNI</asp:ListItem>
                                                                                                            <asp:ListItem Value="2">WNA</asp:ListItem>
                                                                                                        </asp:RadioButtonList>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label42" runat="server" Text="e. Negara "></asp:Label>
                                                                                                        <br />
                                                                                                        <asp:Label ID="Label44" runat="server" Font-Italic="True" Text="(Wajib diisi apabila kewarganegaraan WNA)"
                                                                                                            Width="258px"></asp:Label>
                                                                                                    </td>
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_negaraOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="HfB2IdenPengirimNas_Ind_negaraOld" runat="server" />
                                                                                                    </td>
                                                                                                   <%-- <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_negaraNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="HfB2IdenPengirimNas_Ind_negaraNew" runat="server" />
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelb47" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_negaralainOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                    <%--<td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_negaralainNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelb35" runat="server" Text="f. Pekerjaan"></asp:Label>
                                                                                                    </td>
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_pekerjaanOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="HfB2IdenPengirimNas_Ind_pekerjaanOld" runat="server" />
                                                                                                    </td>
                                                                                                   <%-- <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_pekerjaanNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="HfB2IdenPengirimNas_Ind_pekerjaanNew" runat="server" />
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelub45" runat="server" Text="Pekerjaan Lainnya"></asp:Label></td>
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_PekerjaanlainOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                   <%-- <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_PekerjaanlainNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelyb37" runat="server" Text="g. Alamat Domisili"></asp:Label></td>
                                                                                                    <td style="width: 20%">
                                                                                                    </td>
                                                                                                   
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6; height: 20px;">
                                                                                                        <asp:Label ID="Labelyb38" runat="server" Text="Alamat"></asp:Label>
                                                                                                    </td>
                                                                                                    <td style="width: 20%; height: 20px;">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_AlamatOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                                                   <%-- <td style="width: 20%; height: 20px;">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_AlamatNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; height: 18px; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelob39" runat="server" Text="Kota / Kabupaten "></asp:Label></td>
                                                                                                    <td style="height: 18px; width: 20%;">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_KotaOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="HfB2IdenPengirimNas_Ind_Kota_domOld" runat="server" />
                                                                                                    </td>
                                                                                                   <%-- <td style="height: 18px; width: 20%;">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_KotaNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="HfB2IdenPengirimNas_Ind_Kota_domNew" runat="server" />
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; height: 18px; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelkb57" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                                                    <td style="width: 20%; height: 18px">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_kotalainOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                    <%--<td style="width: 20%; height: 18px">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_kotalainNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labeljb41" runat="server" Text="Provinsi"></asp:Label></td>
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_provinsiOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="HfB2IdenPengirimNas_Ind_provinsi_domOld" runat="server" />
                                                                                                    </td>
                                                                                                   <%-- <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_provinsiNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="HfB2IdenPengirimNas_Ind_provinsi_domNew" runat="server" />
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelib186" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_provinsiLainOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                    <%--<td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_provinsiLainNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                               
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labeljb43" runat="server" Text="Alamat"></asp:Label>
                                                                                                    </td>
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_alamatIdentitasOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                                                   <%-- <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_alamatIdentitasNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labeljb44" runat="server" Text="Kota / Kabupaten"></asp:Label>
                                                                                                    </td>
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_kotaIdentitasOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="HfB2IdenPengirimNas_Ind_kotaIdentitasOld" runat="server" />
                                                                                                    </td>
                                                                                                    <%--<td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_kotaIdentitasNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="HfB2IdenPengirimNas_Ind_kotaIdentitasNew" runat="server" />
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; height: 18px; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelib83" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                                                    <td style="width: 20%; height: 18px">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_KotaLainIdenOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                    <%--<td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_KotaLainIdenNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                                                                        <asp:Label ID="Labeolb46" runat="server" Text="Provinsi"></asp:Label>
                                                                                                    </td>
                                                                                                    <td style="height: 18px; width: 20%;">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_ProvinsiIdenOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="HfB2IdenPengirimNas_Ind_ProvinsiIdenOld" runat="server" />
                                                                                                    </td>
                                                                                                    <%--<td style="height: 18px; width: 20%;">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_ProvinsiIdenNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="HfB2IdenPengirimNas_Ind_ProvinsiIdenNew" runat="server" />
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; height: 18px; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelib62" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                                                    <td style="width: 20%; height: 18px">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_ProvinsilainIdenOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                  <%--  <td style="width: 20%; height: 18px">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_ProvinsilainIdenNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelib48" runat="server" Text="i. Jenis Dokument Identitas"></asp:Label>
                                                                                                    </td>
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:DropDownList ID="CboB2PengirimNasInd_jenisidentitasOld" runat="server" Enabled="False">
                                                                                                        </asp:DropDownList></td>
                                                                                                    <%--<td style="width: 20%">
                                                                                                        <asp:DropDownList ID="CboB2PengirimNasInd_jenisidentitasNew" runat="server" Enabled="False">
                                                                                                        </asp:DropDownList>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                                                                        <asp:Label ID="LabeliIb49" runat="server" Text="Nomor Identitas"></asp:Label>
                                                                                                    </td>
                                                                                                    <td style="height: 18px; width: 20%;">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_noIdentitasOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                   <%-- <td style="width: 20%; height: 18px;">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_noIdentitasNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6; height: 15px;">
                                                                                                    </td>
                                                                                                    <td style="height: 15px; width: 20%;">
                                                                                                    </td>
                                                                                                    <%--<td style="width: 20%; height: 15px;">
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </asp:View>
                                                                                        <asp:View ID="ViewB2PengirimNasabah_Korp" runat="server">
                                                                                            <table style="height: 31px" width="100%">
                                                                                                <tr>
                                                                                                    <td style="width: 5%; height: 15px; background-color: #fff7e6">
                                                                                                        <asp:Label ID="LabelIub51" runat="server" Text="Korporasi" Font-Bold="True"></asp:Label>
                                                                                                    </td>
                                                                                                    <td style="height: 15px; color: #000000; width: 20%;">
                                                                                                    </td>
                                                                                                   <%-- <td style="height: 15px; width: 20%; color: #000000;">
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label20t" runat="server" Text="a. Nama Bank "></asp:Label>
                                                                                                        <asp:Label ID="Labelib187" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                    </td>
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Corp_NamaBankOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <%--<td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Corp_NamaBankNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labeub9306" runat="server" Text="b. Bentuk Badan Usaha "></asp:Label>
                                                                                                    </td>
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Corp_BentukBadanUsahaOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <%--<td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Corp_BentukBadanUsahaNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelbb9307" runat="server" Text="Bentuk Badan Usaha Lainnya " Width="214px"></asp:Label>
                                                                                                    </td>
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Corp_BentukBadanUsahaLainOld" runat="server"
                                                                                                            CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>
                                                                                                   <%-- <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Corp_BentukBadanUsahaLainNew" runat="server"
                                                                                                            CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelb5i5" runat="server" Text="c. Nama Korporasi" Width="94px"></asp:Label>
                                                                                                        <asp:Label ID="Labelib2" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                    </td>
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Corp_NamaKorpOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <%--<td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Corp_NamaKorpNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="LabelRbB24" runat="server" Text="d. Bidang Usaha Korporasi"></asp:Label>
                                                                                                    </td>
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Corp_BidangUsahaKorpOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="HfB2IdenPengirimNas_Corp_BidangUsahaKorpOld" runat="server" />
                                                                                                    </td>
                                                                                                   <%-- <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Corp_BidangUsahaKorpNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="HfB2IdenPengirimNas_Corp_BidangUsahaKorpNew" runat="server" />
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label13" runat="server" Text="BIdang Usaha Lainnya " Width="214px"></asp:Label>
                                                                                                    </td>
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Corp_BidangUsahaLainnyaKorpOld" runat="server"
                                                                                                            CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>
                                                                                                   <%-- <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Corp_BidangUsahaLainnyaKorpNew" runat="server"
                                                                                                            CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelbk58" runat="server" Text="e. Alamat Lengkap Korporasi"></asp:Label></td>
                                                                                                    <td style="width: 20%">
                                                                                                    </td>
                                                                                                   <%-- <td style="width: 20%">
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelkb59" runat="server" Text="Alamat"></asp:Label>
                                                                                                    </td>
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Corp_alamatkorpOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="342px" Enabled="False"></asp:TextBox></td>
                                                                                                    <%--<td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Corp_alamatkorpNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="342px" Enabled="False"></asp:TextBox>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelbk61" runat="server" Text="Kota / Kabupaten "></asp:Label>
                                                                                                    </td>
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Corp_kotakorpOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="HfB2IdenPengirimNas_Corp_kotakorpOld" runat="server" />
                                                                                                    </td>
                                                                                                    <%--<td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Corp_kotakorpNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="HfB2IdenPengirimNas_Corp_kotakorpNew" runat="server" />
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelbi69" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Corp_kotakorplainOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                    <%--<td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Corp_kotakorplainNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelub63" runat="server" Text="Provinsi"></asp:Label>
                                                                                                    </td>
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Corp_provKorpOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="HfB2IdenPengirimNas_Corp_provKorpOld" runat="server" />
                                                                                                    </td>
                                                                                                   <%-- <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Corp_provKorpNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="HfB2IdenPengirimNas_Corp_provKorpNew" runat="server" />
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelbu78" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Corp_provKorpLainOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                    <%--<td style="width: 20%; height: 15px">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Corp_provKorpLainNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelbu64" runat="server" Text="f. Alamat Korporasi di Luar Negeri"></asp:Label></td>
                                                                                                    <td style="width: 20%">
                                                                                                    </td>
                                                                                                    <%--<td style="width: 20%">
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelbu9258" runat="server" Text="Alamat"></asp:Label></td>
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Corp_AlamatLuarOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                    <%--<td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Corp_AlamatLuarNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelbu60" runat="server" Text="g. Alamat Cabang Pengirim Asal"></asp:Label></td>
                                                                                                    <td style="width: 20%">
                                                                                                    </td>
                                                                                                    <%--<td style="width: 20%">
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelbu9259" runat="server" Text="Alamat"></asp:Label></td>
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Corp_AlamatAsalOld" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                    <%--<td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Corp_AlamatAsalNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>--%>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </asp:View>
                                                                                    </asp:MultiView>
                                                                                </td>
                                                                                <td style="width: 100%">
                                                                                    <asp:MultiView ID="MultiViewB2PengirimNasabah_new" runat="server" ActiveViewIndex="0">
                                                                                        <asp:View ID="ViewB2PengirimNasabah_IND_new" runat="server">
                                                                                            <table style="width: 100%; height: 49px">
                                                                                                <tr>
                                                                                                    <td style="width: 5%; height: 15px; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label107_new" runat="server" Text="Perorangan " Font-Bold="True"></asp:Label>
                                                                                                    </td>
                                                                                                   <%-- <td style="height: 15px; width: 5%;">
                                                                                                    </td>--%>
                                                                                                    <td style="height: 15px; width: 20%;">
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label108_new" runat="server" Text="a. Nama Bank"></asp:Label>
                                                                                                        <asp:Label ID="Label109_new" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                    <%--<td style="width: 5%">
                                                                                                        <asp:TextBox ID="TextBox21" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>--%>
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_NamaBankNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label110_new" runat="server" Text="b. Nama Lengkap"></asp:Label>
                                                                                                        <asp:Label ID="Label111_new" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                    <%--<td style="width: 5%">
                                                                                                        <asp:TextBox ID="TextBox23" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>--%>
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_NamaLengkapNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label112_new" runat="server" Text="c. Tanggal lahir (tgl/bln/thn)" Width="183px"></asp:Label>
                                                                                                    </td>
                                                                                                    
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_tanggalLahirNew" runat="server" CssClass="searcheditbox"
                                                                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        &nbsp;</td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label113_new" runat="server" Text="d. Kewarganegaraan (Pilih salah satu) "
                                                                                                            Width="223px"></asp:Label></td>
                                                                                                    
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:RadioButtonList ID="RbB2_IdenPengirimNas_Ind_kewarganegaraanNew" runat="server"
                                                                                                            RepeatDirection="Horizontal" Enabled="False">
                                                                                                            <asp:ListItem Value="1">WNI</asp:ListItem>
                                                                                                            <asp:ListItem Value="2">WNA</asp:ListItem>
                                                                                                        </asp:RadioButtonList>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label114_new" runat="server" Text="e. Negara "></asp:Label>
                                                                                                        <br />
                                                                                                        <asp:Label ID="Label115_new" runat="server" Font-Italic="True" Text="(Wajib diisi apabila kewarganegaraan WNA)"
                                                                                                            Width="258px"></asp:Label>
                                                                                                    </td>
                                                                                                    
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_negaraNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="HfB2IdenPengirimNas_Ind_negaraNew" runat="server" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label116_new" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                                                    
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_negaralainNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label117_new" runat="server" Text="f. Pekerjaan"></asp:Label>
                                                                                                    </td>
                                                                                                   
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_pekerjaanNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="HfB2IdenPengirimNas_Ind_pekerjaanNew" runat="server" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label118_new" runat="server" Text="Pekerjaan Lainnya"></asp:Label></td>
                                                                                                    
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_PekerjaanlainNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label119_new" runat="server" Text="g. Alamat Domisili"></asp:Label></td>
                                                                                                   
                                                                                                    <td style="width: 20%">
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6; height: 20px;">
                                                                                                        <asp:Label ID="Label120_new" runat="server" Text="Alamat"></asp:Label>
                                                                                                    </td>
                                                                                                    
                                                                                                    <td style="width: 20%; height: 20px;">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_AlamatNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; height: 18px; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label121_new" runat="server" Text="Kota / Kabupaten "></asp:Label></td>
                                                                                                   
                                                                                                    <td style="height: 18px; width: 20%;">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_KotaNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="HfB2IdenPengirimNas_Ind_Kota_domNew" runat="server" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; height: 18px; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label122_new" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                                                    
                                                                                                    <td style="width: 20%; height: 18px">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_kotalainNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label123_new" runat="server" Text="Provinsi"></asp:Label></td>
                                                                                                   
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_provinsiNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="HfB2IdenPengirimNas_Ind_provinsi_domNew" runat="server" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelib186_new" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                                                    
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_provinsiLainNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                               
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labeljb43_new" runat="server" Text="Alamat"></asp:Label>
                                                                                                    </td>
                                                                                                   
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_alamatIdentitasNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labeljb44_new" runat="server" Text="Kota / Kabupaten"></asp:Label>
                                                                                                    </td>
                                                                                                  
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_kotaIdentitasNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="HfB2IdenPengirimNas_Ind_kotaIdentitasNew" runat="server" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; height: 18px; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelib83_new" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                                                   
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_KotaLainIdenNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                                                                        <asp:Label ID="Labeolb46_new" runat="server" Text="Provinsi"></asp:Label>
                                                                                                    </td>
                                                                                                    
                                                                                                    <td style="height: 18px; width: 20%;">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_ProvinsiIdenNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="HfB2IdenPengirimNas_Ind_ProvinsiIdenNew" runat="server" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; height: 18px; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelib62_new" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                                                  
                                                                                                    <td style="width: 20%; height: 18px">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_ProvinsilainIdenNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelib48_new" runat="server" Text="i. Jenis Dokument Identitas"></asp:Label>
                                                                                                    </td>
                                                                                                    
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:DropDownList ID="CboB2PengirimNasInd_jenisidentitasNew" runat="server" Enabled="False">
                                                                                                        </asp:DropDownList>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                                                                        <asp:Label ID="LabeliIb49_new" runat="server" Text="Nomor Identitas"></asp:Label>
                                                                                                    </td>
                                                                                                    
                                                                                                    <td style="width: 20%; height: 18px;">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Ind_noIdentitasNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6; height: 15px;">
                                                                                                    </td>
                                                                                                   
                                                                                                    <td style="width: 20%; height: 15px;">
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </asp:View>
                                                                                        <asp:View ID="ViewB2PengirimNasabah_Korp_new" runat="server">
                                                                                            <table style="height: 31px" width="100%">
                                                                                                <tr>
                                                                                                    <td style="width: 5%; height: 15px; background-color: #fff7e6">
                                                                                                        <asp:Label ID="LabelIub51_new" runat="server" Text="Korporasi" Font-Bold="True"></asp:Label>
                                                                                                    </td>
                                                                                                    <%--<td style="height: 15px; color: #000000; width: 5%;">
                                                                                                    </td>--%>
                                                                                                    <td style="height: 15px; width: 20%; color: #000000;">
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label20t_new" runat="server" Text="a. Nama Bank "></asp:Label>
                                                                                                        <asp:Label ID="Labelib187_new" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                    </td>
                                                                                                    
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Corp_NamaBankNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labeub9306_new" runat="server" Text="b. Bentuk Badan Usaha "></asp:Label>
                                                                                                    </td>
                                                                                                   
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Corp_BentukBadanUsahaNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelbb9307_new" runat="server" Text="Bentuk Badan Usaha Lainnya " Width="214px"></asp:Label>
                                                                                                    </td>
                                                                                                   
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Corp_BentukBadanUsahaLainNew" runat="server"
                                                                                                            CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelb5i5_new" runat="server" Text="c. Nama Korporasi" Width="94px"></asp:Label>
                                                                                                        <asp:Label ID="Labelib2_new" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                    </td>
                                                                                                    
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Corp_NamaKorpNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="LabelRbB24_new" runat="server" Text="d. Bidang Usaha Korporasi"></asp:Label>
                                                                                                    </td>
                                                                                                   
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Corp_BidangUsahaKorpNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="HfB2IdenPengirimNas_Corp_BidangUsahaKorpNew" runat="server" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Label13_new" runat="server" Text="BIdang Usaha Lainnya " Width="214px"></asp:Label>
                                                                                                    </td>
                                                                                                    
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Corp_BidangUsahaLainnyaKorpNew" runat="server"
                                                                                                            CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelbk58_new" runat="server" Text="e. Alamat Lengkap Korporasi"></asp:Label></td>
                                                                                                    
                                                                                                    <td style="width: 20%">
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelkb59_new" runat="server" Text="Alamat"></asp:Label>
                                                                                                    </td>
                                                                                                   
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Corp_alamatkorpNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="342px" Enabled="False"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelbk61_new" runat="server" Text="Kota / Kabupaten "></asp:Label>
                                                                                                    </td>
                                                                                                    
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Corp_kotakorpNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="HfB2IdenPengirimNas_Corp_kotakorpNew" runat="server" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelbi69_new" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                                                   
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Corp_kotakorplainNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelub63_new" runat="server" Text="Provinsi"></asp:Label>
                                                                                                    </td>
                                                                                                    
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Corp_provKorpNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                        <asp:HiddenField ID="HfB2IdenPengirimNas_Corp_provKorpNew" runat="server" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelbu78_new" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                                                    
                                                                                                    <td style="width: 20%; height: 15px">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Corp_provKorpLainNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelbu64_new" runat="server" Text="f. Alamat Korporasi di Luar Negeri"></asp:Label></td>
                                                                                                    
                                                                                                    <td style="width: 20%">
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelbu9258_new" runat="server" Text="Alamat"></asp:Label></td>
                                                                                                    
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Corp_AlamatLuarNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelbu60_new" runat="server" Text="g. Alamat Cabang Pengirim Asal"></asp:Label></td>
                                                                                                    
                                                                                                    <td style="width: 20%">
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                                        <asp:Label ID="Labelbu9259_new" runat="server" Text="Alamat"></asp:Label></td>
                                                                                                   
                                                                                                    <td style="width: 20%">
                                                                                                        <asp:TextBox ID="TxtB2IdenPengirimNas_Corp_AlamatAsalNew" runat="server" CssClass="searcheditbox"
                                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </asp:View>
                                                                                    </asp:MultiView>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </asp:View>
                                            </asp:MultiView>
                                        </td>
                                    </tr>
                                </table>
                                <table id="BENEFICIARY OWNER" align="center" bordercolor="#ffffff" cellspacing="1"
                                    cellpadding="2" width="100%" bgcolor="#dddddd" border="2">
                                    <tr>
                                        <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                            style="height: 6px">
                                            <table cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td class="formtext">
                                                        <asp:Label ID="Labelr7" runat="server" Text="C. Beneficiary Owner " Font-Bold="True"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <a href="#" onclick="javascript:ShowHidePanel('CBeneficiaryOwner','Img5','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                            title="click to minimize or maximize">
                                                            <img id="Img5" src="Images/search-bar-minimize.gif" border="0" height="12px" width="12px" /></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr id="CBeneficiaryOwner">
                                        <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                            <table style="width: 100%; height: 60px">
                                                <tr>
                                                    <td align="center" style="width: 5%; height: 22px; background-color: silver">
                                                        <asp:Label ID="Label8t" runat="server" Font-Bold="True" Text="Field"></asp:Label></td>
                                                    <td align="center" style="width: 5%; background-color: silver">
                                                        <asp:Label ID="Label13u" runat="server" Font-Bold="True" Text="Old Data"></asp:Label></td>
                                                    <td style="width: 20%; background-color: silver">
                                                        <asp:Label ID="Label16t" runat="server" Font-Bold="True" Text="New Data"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%" bgcolor="#FFF7E6">
                                                        <asp:Label ID="Label12" runat="server" Text="Keterlibatan Dengan Pemilik Dana ( Beneficial Owner )"
                                                            Width="318px"></asp:Label>
                                                    </td>
                                                    <td style="border: thin dashed silver; width: 5%;" align="left">
                                                        <ajax:AjaxPanel ID="AjaxPanel17" runat="server" Width="100%">
                                                            <asp:DropDownList ID="KeterlibatanBenefOld" runat="server" AutoPostBack="True" Enabled="False">
                                                                <asp:ListItem Value="1">Ya</asp:ListItem>
                                                                <asp:ListItem Value="2">Tidak</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </ajax:AjaxPanel>
                                                    </td>
                                                    <td style="width: 20%; border-right: silver thin dashed; border-top: silver thin dashed;
                                                        border-left: silver thin dashed; border-bottom: silver thin dashed;" align="left">
                                                        <ajax:AjaxPanel ID="AjaxPanel18" runat="server" Width="100%">
                                                            <asp:DropDownList ID="KeterlibatanBenefNew" runat="server" AutoPostBack="True" Enabled="False">
                                                                <asp:ListItem Value="1">Ya</asp:ListItem>
                                                                <asp:ListItem Value="2">Tidak</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </ajax:AjaxPanel>
                                                    </td>
                                                </tr>
                                                <tr id="row1" runat="server">
                                                    <td style="width: 5%; background-color: #fff7e6">
                                                        <asp:Label ID="Labelt162" runat="server" Text="Hubungan dengan Pemilik Dana (Beneficial Owner)"
                                                            Width="298px"></asp:Label>
                                                    </td>
                                                    <td style="width: 5%">
                                                        <asp:TextBox ID="BenfOwnerHubunganPemilikDanaOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="343px" Enabled="False"></asp:TextBox></td>
                                                    <td style="width: 20%">
                                                        <asp:TextBox ID="BenfOwnerHubunganPemilikDanaNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="343px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr id="row2" runat="server">
                                                    <td style="width: 5%; background-color: #fff7e6;">
                                                        <asp:Label ID="Label9t255" runat="server" Text="       Tipe Pengirim " Width="91px"></asp:Label></td>
                                                    <td style="width: 5%;">
                                                        <ajax:AjaxPanel ID="AjaxPanely6" runat="server">
                                                            <asp:RadioButtonList ID="Rb_BOwnerNas_TipePengirimOld" runat="server" RepeatDirection="Horizontal"
                                                                AutoPostBack="True" Width="189px" Enabled="False">
                                                                <asp:ListItem Value="1">Nasabah</asp:ListItem>
                                                                <asp:ListItem Value="2">Non Nasabah</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </ajax:AjaxPanel>
                                                    </td>
                                                    <td style="width: 20%">
                                                        <ajax:AjaxPanel ID="AjaxPanel8" runat="server">
                                                            <asp:RadioButtonList ID="Rb_BOwnerNas_TipePengirimNew" runat="server" RepeatDirection="Horizontal"
                                                                AutoPostBack="True" Width="189px" Enabled="False">
                                                                <asp:ListItem Value="1">Nasabah</asp:ListItem>
                                                                <asp:ListItem Value="2">Non Nasabah</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </ajax:AjaxPanel>
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:MultiView ID="MultiViewSwiftOutBOwner" runat="server">
                                                <asp:View ID="ViewSwiftOutBOwnerNasabah" runat="server">
                                                    <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                        bgcolor="#dddddd" border="2" id="TableSwiftOutBOwnerNas">
                                                        <tr>
                                                            <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                                style="height: 6px">
                                                                <table cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td class="formtext">
                                                                            <asp:Label ID="Label4" runat="server" Text="Nasabah" Font-Bold="True"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <a href="#" onclick="javascript:ShowHidePanel('BOwner1nasabah','Img6','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                                title="click to minimize or maximize">
                                                                                <img id="Img6" src="Images/search-bar-minimize.gif" border="0" height="12" width="12" /></a>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr id="BOwner1nasabah">
                                                            <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                </table>
                                                                <table style="width: 100%; height: 49px">
                                                                    <tr>
                                                                        <td align="center" style="width: 5%; height: 22px; background-color: silver">
                                                                            <asp:Label ID="Label17r" runat="server" Font-Bold="True" Text="Field"></asp:Label></td>
                                                                        <td align="center" style="width: 5%; background-color: silver">
                                                                            <asp:Label ID="Label18e" runat="server" Font-Bold="True" Text="Old Data"></asp:Label></td>
                                                                        <td style="width: 20%; background-color: silver">
                                                                            <asp:Label ID="Label20" runat="server" Font-Bold="True" Text="New Data"></asp:Label></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 5%; background-color: #fff7e6">
                                                                            <asp:Label ID="Labelf1163" runat="server" Text="a. No. Rek "></asp:Label>
                                                                        </td>
                                                                        <td style="width: 5%; height: 15px;">
                                                                            <asp:TextBox ID="BenfOwnerNasabah_rekeningOld" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                        <td style="width: 20%">
                                                                            <asp:TextBox ID="BenfOwnerNasabah_rekeningNew" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 5%; background-color: #fff7e6">
                                                                            <asp:Label ID="Labele146" runat="server" Text="b. Nama Lengkap"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 5%; height: 15px;">
                                                                            <asp:TextBox ID="BenfOwnerNasabah_NamaOld" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                        <td style="width: 20%">
                                                                            <asp:TextBox ID="BenfOwnerNasabah_NamaNew" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 5%; background-color: #fff7e6">
                                                                            <asp:Label ID="Label148" runat="server" Text="c. Tanggal lahir (tgl/bln/thn)" Width="144px"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 5%; height: 15px;">
                                                                            <asp:TextBox ID="BenfOwnerNasabah_tanggalLahirOld" runat="server" CssClass="searcheditbox"
                                                                                TabIndex="2" Width="122px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                        <td style="width: 20%">
                                                                            <asp:TextBox ID="BenfOwnerNasabah_tanggalLahirNew" runat="server" CssClass="searcheditbox"
                                                                                TabIndex="2" Width="122px" Enabled="False"></asp:TextBox>
                                                                            &nbsp;</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 5%; background-color: #fff7e6">
                                                                            <asp:Label ID="Labeld150" runat="server" Text="d. Alamat Sesuai Bukti Identitas"></asp:Label></td>
                                                                        <td style="width: 5%; height: 15px;">
                                                                        </td>
                                                                        <td style="width: 20%">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 5%; background-color: #fff7e6">
                                                                            <asp:Label ID="Labelw151" runat="server" Text="Alamat"></asp:Label></td>
                                                                        <td style="width: 5%; height: 15px;">
                                                                            <asp:TextBox ID="BenfOwnerNasabah_AlamatIdenOld" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                        <td style="width: 20%">
                                                                            <asp:TextBox ID="BenfOwnerNasabah_AlamatIdenNew" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 5%; background-color: #fff7e6">
                                                                            <asp:Label ID="Labelt152" runat="server" Text="Kota / Kabupaten"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 5%; height: 15px;">
                                                                            <asp:TextBox ID="BenfOwnerNasabah_KotaIdenOld" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            <asp:HiddenField ID="hfBenfOwnerNasabah_KotaIdenOld" runat="server" />
                                                                        </td>
                                                                        <td style="width: 20%">
                                                                            <asp:TextBox ID="BenfOwnerNasabah_KotaIdenNew" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            <asp:HiddenField ID="hfBenfOwnerNasabah_KotaIdenNew" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 5%; background-color: #fff7e6">
                                                                            <asp:Label ID="Labelt154" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                        <td style="width: 5%; height: 15px;">
                                                                            <asp:TextBox ID="BenfOwnerNasabah_kotaLainIdenOld" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                        <td style="width: 20%">
                                                                            <asp:TextBox ID="BenfOwnerNasabah_kotaLainIdenNew" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 5%; background-color: #fff7e6;">
                                                                            <asp:Label ID="Labelt155" runat="server" Text="Provinsi"></asp:Label></td>
                                                                        <td style="height: 15px; width: 5%;">
                                                                            <asp:TextBox ID="BenfOwnerNasabah_ProvinsiIdenOld" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            <asp:HiddenField ID="hfBenfOwnerNasabah_ProvinsiIdenOld" runat="server" />
                                                                        </td>
                                                                        <td style="width: 20%">
                                                                            <asp:TextBox ID="BenfOwnerNasabah_ProvinsiIdenNew" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                            <asp:HiddenField ID="hfBenfOwnerNasabah_ProvinsiIdenNew" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 5%; background-color: #fff7e6">
                                                                            <asp:Label ID="Labelt156" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                        <td style="width: 5%; height: 15px">
                                                                            <asp:TextBox ID="BenfOwnerNasabah_ProvinsiLainIdenOld" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                        <td style="width: 20%;">
                                                                            <asp:TextBox ID="BenfOwnerNasabah_ProvinsiLainIdenNew" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 5%; background-color: #fff7e6">
                                                                            <asp:Label ID="Labelt158" runat="server" Text="e. Jenis Dokument Identitas"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 5%; height: 15px;">
                                                                            <asp:DropDownList ID="CBOBenfOwnerNasabah_JenisDokumenOld" runat="server" Enabled="False">
                                                                            </asp:DropDownList></td>
                                                                        <td style="width: 20%">
                                                                            <asp:DropDownList ID="CBOBenfOwnerNasabah_JenisDokumenNew" runat="server" Enabled="False">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 5%; background-color: #fff7e6;">
                                                                            <asp:Label ID="Labelt160" runat="server" Text="Nomor Identitas"></asp:Label></td>
                                                                        <td style="height: 15px; width: 5%;">
                                                                            <asp:TextBox ID="BenfOwnerNasabah_NomorIdenOld" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                        <td style="width: 20%;">
                                                                            <asp:TextBox ID="BenfOwnerNasabah_NomorIdenMew" runat="server" CssClass="searcheditbox"
                                                                                MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 5%; background-color: #fff7e6;">
                                                                        </td>
                                                                        <td style="height: 15px; width: 5%;">
                                                                        </td>
                                                                        <td style="width: 20%;">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:View>
                                                <asp:View ID="ViewSwiftOutBOwnerNonNasabah" runat="server">
                                                    <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                        border-right-style: none; border-left-style: none; border-bottom-style: none"
                                                        id="TableSwiftOutBOwnerNonNas">
                                                        <tr>
                                                            <td>
                                                                <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                                                    bgcolor="#dddddd" border="2">
                                                                    <tr>
                                                                        <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                                            style="height: 6px">
                                                                            <table cellpadding="0" cellspacing="0" border="0">
                                                                                <tr>
                                                                                    <td class="formtext" style="height: 14px">
                                                                                        <asp:Label ID="Labelt9" runat="server" Text="Non Nasabah" Font-Bold="True"></asp:Label>
                                                                                    </td>
                                                                                    <td style="height: 14px">
                                                                                        <a href="#" onclick="javascript:ShowHidePanel('BOwnerNonNasabah','Img7','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                                            title="click to minimize or maximize">
                                                                                            <img id="Img7" src="Images/search-bar-minimize.gif" border="0" height="12" width="12" /></a>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="BOwnerNonNasabah">
                                                                        <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                                            <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                                border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                            </table>
                                                                            <table style="width: 100%; height: 49px">
                                                                                <tr>
                                                                                    <td align="center" style="width: 5%; height: 22px; background-color: silver">
                                                                                        <asp:Label ID="Label22q" runat="server" Font-Bold="True" Text="Field"></asp:Label></td>
                                                                                    <td align="center" style="width: 5%; background-color: silver">
                                                                                        <asp:Label ID="Label23q" runat="server" Font-Bold="True" Text="Old Data"></asp:Label></td>
                                                                                    <td style="width: 20%; background-color: silver">
                                                                                        <asp:Label ID="Label24q" runat="server" Font-Bold="True" Text="New Data"></asp:Label></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                        <asp:Label ID="Labelu157" runat="server" Text="a. Nama Lengkap"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 5%">
                                                                                        <asp:TextBox ID="BenfOwnerNonNasabah_NamaOld" runat="server" CssClass="searcheditbox"
                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                    <td style="width: 20%">
                                                                                        <asp:TextBox ID="BenfOwnerNonNasabah_NamaNew" runat="server" CssClass="searcheditbox"
                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                        <asp:Label ID="Label1t66" runat="server" Text="b. Tanggal lahir (tgl/bln/thn)" Width="136px"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 5%">
                                                                                        <asp:TextBox ID="BenfOwnerNonNasabah_TanggalLahirOld" runat="server" CssClass="searcheditbox"
                                                                                            TabIndex="2" Width="122px" Enabled="False"></asp:TextBox>
                                                                                    </td>
                                                                                    <td style="width: 20%">
                                                                                        <asp:TextBox ID="BenfOwnerNonNasabah_TanggalLahirNew" runat="server" CssClass="searcheditbox"
                                                                                            TabIndex="2" Width="122px" Enabled="False"></asp:TextBox>
                                                                                        &nbsp;</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                        <asp:Label ID="Labelt168" runat="server" Text="c. Alamat Sesuai Bukti Identitas"></asp:Label></td>
                                                                                    <td style="width: 5%">
                                                                                    </td>
                                                                                    <td style="width: 20%">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                        <asp:Label ID="Labele169" runat="server" Text="Alamat"></asp:Label></td>
                                                                                    <td style="width: 5%">
                                                                                        <asp:TextBox ID="BenfOwnerNonNasabah_AlamatIdenOld" runat="server" CssClass="searcheditbox"
                                                                                            MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                                    <td style="width: 20%">
                                                                                        <asp:TextBox ID="BenfOwnerNonNasabah_AlamatIdenNew" runat="server" CssClass="searcheditbox"
                                                                                            MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                        <asp:Label ID="Label1t70" runat="server" Text="Kota / Kabupaten"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 5%">
                                                                                        <asp:TextBox ID="BenfOwnerNonNasabah_KotaIdenOld" runat="server" CssClass="searcheditbox"
                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                        <asp:HiddenField ID="hfBenfOwnerNonNasabah_KotaIdenOld" runat="server" />
                                                                                    </td>
                                                                                    <td style="width: 20%">
                                                                                        <asp:TextBox ID="BenfOwnerNonNasabah_KotaIdenNew" runat="server" CssClass="searcheditbox"
                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                        <asp:HiddenField ID="hfBenfOwnerNonNasabah_KotaIdenNew" runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                        <asp:Label ID="Labelo172" runat="server" Text="Kota / Kabupaten Lainnya"></asp:Label></td>
                                                                                    <td style="width: 5%">
                                                                                        <asp:TextBox ID="BenfOwnerNonNasabah_KotaLainIdenOld" runat="server" CssClass="searcheditbox"
                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                    <td style="width: 20%">
                                                                                        <asp:TextBox ID="BenfOwnerNonNasabah_KotaLainIdenNew" runat="server" CssClass="searcheditbox"
                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 5%; background-color: #fff7e6;">
                                                                                        <asp:Label ID="Labelo173" runat="server" Text="Provinsi"></asp:Label></td>
                                                                                    <td style="width: 5%;">
                                                                                        <asp:TextBox ID="BenfOwnerNonNasabah_ProvinsiIdenOld" runat="server" CssClass="searcheditbox"
                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                        <asp:HiddenField ID="hfBenfOwnerNonNasabah_ProvinsiIdenOld" runat="server" />
                                                                                    </td>
                                                                                    <td style="width: 20%">
                                                                                        <asp:TextBox ID="BenfOwnerNonNasabah_ProvinsiIdennew" runat="server" CssClass="searcheditbox"
                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                        <asp:HiddenField ID="hfBenfOwnerNonNasabah_ProvinsiIdenNew" runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                        <asp:Label ID="Labelo174" runat="server" Text="Provinsi Lainnya"></asp:Label></td>
                                                                                    <td style="width: 5%;">
                                                                                        <asp:TextBox ID="BenfOwnerNonNasabah_ProvinsiLainIdenOld" runat="server" CssClass="searcheditbox"
                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                    <td style="width: 20%;">
                                                                                        <asp:TextBox ID="BenfOwnerNonNasabah_ProvinsiLainIdenNew" runat="server" CssClass="searcheditbox"
                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 5%; background-color: #fff7e6">
                                                                                        <asp:Label ID="Labelo176" runat="server" Text="d. Jenis Dokument Identitas"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 5%">
                                                                                        <asp:DropDownList ID="CBOBenfOwnerNonNasabah_JenisDokumenOld" runat="server" Enabled="False">
                                                                                        </asp:DropDownList></td>
                                                                                    <td style="width: 20%">
                                                                                        <asp:DropDownList ID="CBOBenfOwnerNonNasabah_JenisDokumenNew" runat="server" Enabled="False">
                                                                                        </asp:DropDownList>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 5%; background-color: #fff7e6;">
                                                                                        <asp:Label ID="Labelo178" runat="server" Text="Nomor Identitas"></asp:Label></td>
                                                                                    <td style="width: 5%;">
                                                                                        <asp:TextBox ID="BenfOwnerNonNasabah_NomorIdentitasOld" runat="server" CssClass="searcheditbox"
                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                    <td style="width: 20%;">
                                                                                        <asp:TextBox ID="BenfOwnerNonNasabah_NomorIdentitasNew" runat="server" CssClass="searcheditbox"
                                                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 5%; background-color: #fff7e6;">
                                                                                        <asp:Label ID="Label9269" runat="server" Text="e. Nilai Transaksi dalam Rupiah "></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 5%;">
                                                                                        <asp:TextBox ID="BenfOwnerNonNasabah_NilaiTransaksidalamRUpiahOld" runat="server"
                                                                                            CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                    </td>
                                                                                    <td style="width: 20%;">
                                                                                        <asp:TextBox ID="BenfOwnerNonNasabah_NilaiTransaksidalamRUpiahNew" runat="server"
                                                                                            CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:View>
                                            </asp:MultiView>
                                        </td>
                                    </tr>
                                </table>
                                <table id="PENERIMA" align="center" bgcolor="#dddddd" border="2" bordercolor="#ffffff"
                                    cellpadding="2" cellspacing="1" width="100%">
                                    <tr>
                                        <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                            style="height: 6px">
                                            <table cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td class="formtext">
                                                        <asp:Label ID="Labelo10" runat="server" Text="D. Identitas Penerima" Font-Bold="True"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <a href="#" onclick="javascript:ShowHidePanel('DIdentitasPenerima','Img8','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                            title="click to minimize or maximize">
                                                            <img id="Img4" src="Images/search-bar-minimize.gif" border="0" height="12px" width="12px" />
                                                        </a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr id="DIdentitasPenerima">
                                        <td valign="top" bgcolor="#ffffff" style="height: 100%">
                                            <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                border-right-style: none; border-left-style: none; border-bottom-style: none">
                                            </table>
                                            <table style="width: 100%; height: 60px">
                                                <tr>
                                                    <td colspan="3">
                                                        <ajax:AjaxPanel ID="AjaxPanel780" runat="server" meta:resourcekey="AjaxPanel4Resource1"
                                                            Width="100%">
                                                            <asp:DataGrid ID="GV" runat="server" AllowCustomPaging="True" AllowPaging="True"
                                                                AllowSorting="True" AutoGenerateColumns="False" BackColor="#DEBA84" BorderColor="#DEBA84"
                                                                BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2" Font-Bold="False"
                                                                Font-Italic="False" Font-Overline="False" Font-Size="XX-Small" Font-Strikeout="False"
                                                                Font-Underline="False" HorizontalAlign="Left" meta:resourcekey="GridMsUserViewResource1"
                                                                SkinID="gridview" Width="100%">
                                                                <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                                                                <SelectedItemStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                                                                <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" Mode="NumericPages" Visible="False" />
                                                                <AlternatingItemStyle BackColor="White" />
                                                                <ItemStyle BackColor="#FFF7E7" ForeColor="#8C4510" HorizontalAlign="Left" VerticalAlign="Top" />
                                                                <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" HorizontalAlign="Left"
                                                                    VerticalAlign="Middle" Width="200px" Wrap="False" />
                                                                <Columns>
                                                                    <asp:BoundColumn HeaderText="No">
                                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                            Font-Underline="False" ForeColor="White" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Beneficiary_Nasabah_INDV_NoRekening" HeaderText="No. Rekening ( Perorangan )"
                                                                        SortExpression="Beneficiary_Nasabah_INDV_NoRekening asc"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Beneficiary_Nasabah_INDV_NamaBank" HeaderText="Nama Bank ( Perorangan )"
                                                                        SortExpression="Beneficiary_Nasabah_INDV_NamaBank asc"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Beneficiary_Nasabah_INDV_NamaLengkap" HeaderText="Nama Lengkap ( Perorangan )"
                                                                        SortExpression="Beneficiary_Nasabah_INDV_NamaLengkap asc"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Beneficiary_Nasabah_INDV_TanggalLahir" HeaderText="Tanggal Lahir ( Perorangan )"
                                                                        SortExpression="Beneficiary_Nasabah_INDV_TanggalLahir asc"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Beneficiary_Nasabah_CORP_NoRekening" HeaderText="No. Rekening ( Korporasi )"
                                                                        SortExpression="Beneficiary_Nasabah_CORP_NoRekening asc"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Beneficiary_Nasabah_CORP_NamaKorporasi" HeaderText="Nama Korporasi ( Korporasi )"
                                                                        SortExpression="Beneficiary_Nasabah_CORP_NamaKorporasi asc"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Beneficiary_Nasabah_CORP_AlamatLengkap" HeaderText="Alamat Lengkap ( Korporasi )"
                                                                        SortExpression="Beneficiary_Nasabah_CORP_AlamatLengkap asc"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Beneficiary_Nasabah_CORP_NoTelp" HeaderText="No. Telp ( Korporasi )"
                                                                        SortExpression="Beneficiary_Nasabah_CORP_NoTelp asc"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Beneficiary_NonNasabah_NoRekening" HeaderText="No. Rekening ( Non Nasabah )"
                                                                        SortExpression="Beneficiary_NonNasabah_NoRekening asc"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Beneficiary_NonNasabah_KodeRahasia" HeaderText="Kode Rahasia ( Non Nasabah )"
                                                                        SortExpression="Beneficiary_NonNasabah_KodeRahasia asc"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Beneficiary_NonNasabah_NamaBank" HeaderText="Nama Bank ( Non Nasabah )"
                                                                        SortExpression="Beneficiary_NonNasabah_NamaBank asc"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Beneficiary_NonNasabah_NamaLengkap" HeaderText="Nama Lengakap ( Non Nasabah )"
                                                                        SortExpression="Beneficiary_NonNasabah_NamaLengkap asc"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Beneficiary_NonNasabah_TanggalLahir" HeaderText="Tanggal lahir ( Non Nasabah )"
                                                                        SortExpression="Beneficiary_NonNasabah_TanggalLahir asc"></asp:BoundColumn>
                                                                    <asp:EditCommandColumn EditText="Detail"></asp:EditCommandColumn>
                                                                    <asp:BoundColumn DataField="PK_IFTI_Approval_Beneficiary_ID" Visible="False"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="FK_IFTI_Beneficiary_ID" Visible="False"></asp:BoundColumn>
                                                                </Columns>
                                                            </asp:DataGrid>
                                                        </ajax:AjaxPanel>
                                                        <asp:Label ID="LabelNoRecordFound" runat="server" CssClass="text" meta:resourcekey="LabelNoRecordFoundResource1"
                                                            Text="No record match with your criteria" Visible="False">
                                                        </asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:MultiView ID="DataPenerima" runat="server">
                                                <asp:View ID="dataTambahan" runat="server">
                                                    <table>
                                                        <tr id="Tr1" runat="server">
                                                            <td align="center" style="width: 5%; height: 22px; background-color: silver">
                                                                <asp:Label ID="Label26" runat="server" Font-Bold="True" Text="Field"></asp:Label></td>
                                                            <td align="center" style="width: 5%; background-color: silver">
                                                                <asp:Label ID="Label29y" runat="server" Font-Bold="True" Text="Old Data"></asp:Label></td>
                                                            <td style="width: 20%; background-color: silver">
                                                                <asp:Label ID="Label30t" runat="server" Font-Bold="True" Text="New Data"></asp:Label></td>
                                                        </tr>
                                                        <tr id="RekeningNasabah" runat="server">
                                                            <td style="width: 5%; background-color: #fff7e6; height: 34px;">
                                                                <asp:Label ID="Labelo190" runat="server" Text="No. Rek " Width="40px"></asp:Label>
                                                                <asp:Label ID="Labelo191" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                            <td style="width: 5%; height: 34px;">
                                                                <asp:TextBox ID="txtPenerima_rekeningOld" runat="server" CssClass="searcheditbox"
                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="false"></asp:TextBox></td>
                                                            <td style="width: 20%; height: 34px;">
                                                                <asp:TextBox ID="txtPenerima_rekeningNew" runat="server" CssClass="searcheditbox"
                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="false"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr id="trSwiftOutPenerimaTipePengirim" runat="server">
                                                            <td style="width: 5%; background-color: #fff7e6; height: 34px;">
                                                                <asp:Label ID="Labelo192" runat="server" Text="       "></asp:Label>
                                                                <asp:Label ID="Labelo193" runat="server" Text="       Tipe Penerima "></asp:Label>
                                                                <asp:Label ID="Labelo194" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                            </td>
                                                            <td style="width: 5%; height: 34px;">
                                                                <ajax:AjaxPanel ID="AjaxPaneli5" runat="server" Width="93px">
                                                                    <asp:RadioButtonList ID="RBPenerimaNasabah_TipePengirimOld" runat="server" RepeatDirection="Horizontal"
                                                                        AutoPostBack="True" Width="201px" Enabled="false">
                                                                        <asp:ListItem Value="1">Nasabah</asp:ListItem>
                                                                        <asp:ListItem Value="2">Non Nasabah</asp:ListItem>
                                                                    </asp:RadioButtonList></ajax:AjaxPanel>
                                                            </td>
                                                            <td style="width: 20%; height: 34px;">
                                                                <ajax:AjaxPanel ID="AjaxPanel9" runat="server" Width="93px">
                                                                    <asp:RadioButtonList ID="RBPenerimaNasabah_TipePengirimNew" runat="server" RepeatDirection="Horizontal"
                                                                        AutoPostBack="True" Width="201px" Enabled="false">
                                                                        <asp:ListItem Value="1">Nasabah</asp:ListItem>
                                                                        <asp:ListItem Value="2">Non Nasabah</asp:ListItem>
                                                                    </asp:RadioButtonList></ajax:AjaxPanel>
                                                            </td>
                                                        </tr>
                                                        <tr id="trSwiftOutPenerimaTipeNasabah" runat="server" visible="False">
                                                            <td style="width: 5%; height: 34px; background-color: #fff7e6">
                                                                <asp:Label ID="Labelo9256" runat="server" Text="Tipe Nasabah "></asp:Label>
                                                                <asp:Label ID="Labelo9257" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                            </td>
                                                            <td style="width: 5%; height: 34px">
                                                                <ajax:AjaxPanel ID="AjaxPanelo9" runat="server">
                                                                    <asp:RadioButtonList ID="Rb_IdenPenerimaNas_TipeNasabahOld" runat="server" AutoPostBack="True"
                                                                        RepeatDirection="Horizontal">
                                                                        <asp:ListItem Value="1">Perorangan</asp:ListItem>
                                                                        <asp:ListItem Value="2">Korporasi</asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                                </ajax:AjaxPanel>
                                                            </td>
                                                            <td style="width: 20%; height: 34px">
                                                                <ajax:AjaxPanel ID="AjaxPanel10" runat="server">
                                                                    <asp:RadioButtonList ID="Rb_IdenPenerimaNas_TipeNasabahNew" runat="server" AutoPostBack="True"
                                                                        RepeatDirection="Horizontal">
                                                                        <asp:ListItem Value="1">Perorangan</asp:ListItem>
                                                                        <asp:ListItem Value="2">Korporasi</asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                                </ajax:AjaxPanel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3">
                                                                <asp:MultiView ID="MultiViewSwiftOutIdenPenerima" runat="server">
                                                                    <asp:View ID="ViewSwiftOutIdenPenerimaNasabah" runat="server">
                                                                        <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                                            bgcolor="#dddddd" border="2">
                                                                            <tr>
                                                                                <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                                                    style="height: 6px">
                                                                                    <table cellpadding="0" cellspacing="0" border="0">
                                                                                        <tr>
                                                                                            <td class="formtext">
                                                                                                <asp:Label ID="Label11e" runat="server" Text="Nasabah" Font-Bold="True"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <a href="#" onclick="javascript:ShowHidePanel('DIdentitasPenerima1nasabah','Img9','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                                                    title="click to minimize or maximize">
                                                                                                    <img id="Img9" src="Images/search-bar-minimize.gif" border="0" height="12px" width="12px" /></a>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr id="DIdentitasPenerima1nasabah">
                                                                                <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                                                    <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                                        border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                                    </table>
                                                                                    <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                                        border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                                        <tr>
                                                                                            <td style="width: 100%" colspan ="2">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 50%">
                                                                                                <asp:MultiView ID="MultiViewPenerimaNasabah_OLD" runat="server" ActiveViewIndex="0">
                                                                                                    <asp:View ID="ViewPenerimaNasabah_IND_OLD" runat="server">
                                                                                                        <table style="width: 100%; height: 49px">
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #fff7e6">
                                                                                                                    <asp:Label ID="Label1o95o" runat="server" Text="Perorangan " Font-Bold="True"></asp:Label>
                                                                                                                </td>
                                                                                                                <td style="width: 5%;">
                                                                                                                </td>
                                                                                                               
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td align="center" style="width: 5%; height: 22px; background-color: silver">
                                                                                                                    <asp:Label ID="Label31o" runat="server" Font-Bold="True" Text="Field"></asp:Label></td>
                                                                                                                <td align="center" style="width: 5%; background-color: silver">
                                                                                                                    <asp:Label ID="Label32yo" runat="server" Font-Bold="True" Text="Old Data"></asp:Label></td>
                                                                                                                
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #fff7e6">
                                                                                                                    <asp:Label ID="Label19o7o" runat="server" Text="a. Nama Lengkap"></asp:Label>
                                                                                                                    <asp:Label ID="Label12o5o" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                                <td style="width: 5%">
                                                                                                                    <asp:TextBox ID="txtPenerimaNasabah_IND_namaOld" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #fff7e6">
                                                                                                                    <asp:Label ID="Label19o8o" runat="server" Text="b. Tanggal lahir (tgl/bln/thn)" Width="136px"></asp:Label></td>
                                                                                                                <td style="width: 5%">
                                                                                                                    <asp:TextBox ID="txtPenerimaNasabah_IND_TanggalLahirOld" runat="server" CssClass="searcheditbox"
                                                                                                                        TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                </td>
                                                                                                                
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #fff7e6">
                                                                                                                    <asp:Label ID="Labelo200o" runat="server" Text="c. Kewarganegaraan (Pilih salah satu) "
                                                                                                                        Width="184px"></asp:Label></td>
                                                                                                                <td style="width: 5%">
                                                                                                                    <asp:RadioButtonList ID="RbPenerimaNasabah_IND_WarganegaraOld" runat="server" RepeatDirection="Horizontal"
                                                                                                                        Enabled="False">
                                                                                                                        <asp:ListItem Value="1">WNI</asp:ListItem>
                                                                                                                        <asp:ListItem Value="2">WNA</asp:ListItem>
                                                                                                                    </asp:RadioButtonList></td>
                                                                                                                
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #fff7e6">
                                                                                                                    <asp:Label ID="Label201o" runat="server" Text="d. Negara "></asp:Label>
                                                                                                                    <asp:Label ID="Label147o" runat="server" ForeColor="Red" Text="*"></asp:Label><br />
                                                                                                                    <asp:Label ID="Label202o" runat="server" Font-Italic="True" Text="(Wajib diisi apabila kewarganegaraan WNA)"
                                                                                                                        Width="216px"></asp:Label></td>
                                                                                                                <td style="width: 5%">
                                                                                                                    <asp:TextBox ID="txtPenerimaNasabah_IND_negaraOld" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                    <asp:HiddenField ID="hfPenerimaNasabah_IND_negaraOld" runat="server" />
                                                                                                                </td>
                                                                                                                
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #fff7e6">
                                                                                                                    <asp:Label ID="Label203o" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                                                                <td style="width: 5%">
                                                                                                                    <asp:TextBox ID="txtPenerimaNasabah_IND_negaraLainOld" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #fff7e6;">
                                                                                                                    <asp:Label ID="Label213o" runat="server" Text="e. Alamat Sesuai Bukti Identitas"></asp:Label></td>
                                                                                                                <td style="width: 5%;">
                                                                                                                </td>
                                                                                                                 
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #fff7e6">
                                                                                                                    <asp:Label ID="Label214o" runat="server" Text="Alamat"></asp:Label>
                                                                                                                    <asp:Label ID="Label9325o" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                                </td>
                                                                                                                <td style="width: 5%">
                                                                                                                    <asp:TextBox ID="txtPenerimaNasabah_IND_alamatIdenOld" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                                                                
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #fff7e6">
                                                                                                                    <asp:Label ID="Label205o" runat="server" Text="Negara Bagian/ Kota"></asp:Label></td>
                                                                                                                <td style="width: 5%">
                                                                                                                    <asp:TextBox ID="txtPenerimaNasabah_IND_negaraBagianOld" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                </td>
                                                                                                                
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #fff7e6">
                                                                                                                    <asp:Label ID="Label204o" runat="server" Text="Negara"></asp:Label>
                                                                                                                    <asp:Label ID="Label9326o" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                                </td>
                                                                                                                <td style="width: 5%;">
                                                                                                                    <asp:TextBox ID="txtPenerimaNasabah_IND_negaraIdenOld" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                    <asp:HiddenField ID="hfPenerimaNasabah_IND_negaraIdenOld" runat="server" />
                                                                                                                </td>
                                                                                                               
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #fff7e6;">
                                                                                                                    <asp:Label ID="Label206o" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                                                                <td style="width: 5%;">
                                                                                                                    <asp:TextBox ID="txtPenerimaNasabah_IND_negaraLainIdenOld" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                </td>
                                                                                                                
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #fff7e6">
                                                                                                                    <asp:Label ID="Label220o" runat="server" Text="f. Jenis Dokument Identitas"></asp:Label></td>
                                                                                                                <td style="width: 5%">
                                                                                                                    <asp:DropDownList ID="CBOPenerimaNasabah_IND_JenisIdenOld" runat="server" Enabled="False">
                                                                                                                    </asp:DropDownList></td>
                                                                                                                
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #fff7e6;">
                                                                                                                    <asp:Label ID="Label221o" runat="server" Text="Nomor Identitas"></asp:Label></td>
                                                                                                                <td style="width: 5%;">
                                                                                                                    <asp:TextBox ID="txtPenerimaNasabah_IND_NomorIdenOld" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #fff7e6;">
                                                                                                                    <asp:Label ID="Labelew9270o" runat="server" Text="g. Nilai Transaksi Keuangan (dalam Rupiah)"></asp:Label>
                                                                                                                </td>
                                                                                                                <td style="width: 5%;">
                                                                                                                    <asp:TextBox ID="txtPenerimaNasabah_IND_NilaiTransaksiKeuanganOld" runat="server"
                                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                </td>
                                                                                                                
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #fff7e6;">
                                                                                                                </td>
                                                                                                                <td style="width: 5%;">
                                                                                                                </td>
                                                                                                               
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </asp:View>
                                                                                                    <asp:View ID="ViewPenerimaNasabah_Korp_OLD" runat="server">
                                                                                                        <table style="width: 100%; height: 31px">
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #FFF7E6;">
                                                                                                                    <asp:Label ID="Labelko223o" runat="server" Text="Korporasi" Font-Bold="True"></asp:Label>
                                                                                                                </td>
                                                                                                                <td style="height: 18px; width: 5%;">
                                                                                                                </td>
                                                                                                                 
                                                                                                            </tr>
                                                                                                            <tr style="color: #000000">
                                                                                                                <td align="center" style="width: 5%; height: 22px; background-color: silver">
                                                                                                                    <asp:Label ID="Label34yo" runat="server" Font-Bold="True" Text="Field"></asp:Label></td>
                                                                                                                <td align="center" style="width: 5%; background-color: silver">
                                                                                                                    <asp:Label ID="Label35yo" runat="server" Font-Bold="True" Text="Old Data"></asp:Label></td>
                                                                                                                 
                                                                                                            </tr>
                                                                                                            <tr style="color: #000000">
                                                                                                                <td style="width: 5%; background-color: #FFF7E6;">
                                                                                                                    <asp:Label ID="Labela225o" runat="server" Text="a. Bentuk Badan Usaha "></asp:Label></td>
                                                                                                                <td style="width: 5%; height: 18px;">
                                                                                                                    <asp:TextBox ID="txtPenerimaNasabah_Korp_BentukBadanUsahaOld" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                               
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #FFF7E6;">
                                                                                                                    <asp:Label ID="Labelb226o" runat="server" Text="Bentuk Badan Usaha Lainnya " Width="144px"></asp:Label></td>
                                                                                                                <td style="width: 5%; height: 18px;">
                                                                                                                    <asp:TextBox ID="txtPenerimaNasabah_Korp_BentukBadanUsahaLainnyaOld" runat="server"
                                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #FFF7E6;">
                                                                                                                    <asp:Label ID="Labelb227o" runat="server" Text="b. Nama Korporasi" Width="88px"></asp:Label>
                                                                                                                    <asp:Label ID="Labelb149o" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                                <td style="width: 5%; height: 18px;">
                                                                                                                    <asp:TextBox ID="txtPenerimaNasabah_Korp_namaKorpOld" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #FFF7E6;">
                                                                                                                    <asp:Label ID="Labelb228o" runat="server" Text="c. Bidang Usaha Korporasi"></asp:Label></td>
                                                                                                                <td style="width: 5%; height: 18px;">
                                                                                                                    <asp:TextBox ID="txtPenerimaNasabah_Korp_BidangUsahaKorpOld" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                    <asp:HiddenField ID="hfPenerimaNasabah_Korp_BidangUsahaKorpOld" runat="server" />
                                                                                                                </td>
                                                                                                                
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #FFF7E6;">
                                                                                                                    <asp:Label ID="Label14o" runat="server" Text="Bidang Usaha Lainnya " Width="112px"></asp:Label></td>
                                                                                                                <td style="width: 5%; height: 18px;">
                                                                                                                    <asp:TextBox ID="txtPenerimaNasabah_Korp_BidangUsahaLainKorpOld" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                                                
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #FFF7E6;">
                                                                                                                    <asp:Label ID="Labelb229o" runat="server" Text="d. Alamat Lengkap Korporasi"></asp:Label></td>
                                                                                                                <td style="width: 5%; height: 18px;">
                                                                                                                </td>
                                                                                                                 
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #FFF7E6;">
                                                                                                                    <asp:Label ID="Labelb199o" runat="server" Text="Alamat"></asp:Label>
                                                                                                                    <asp:Label ID="Labelb153o" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                                <td style="width: 5%; height: 18px;">
                                                                                                                    <asp:TextBox ID="txtPenerimaNasabah_Korp_AlamatKorpOld" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                                                               
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #FFF7E6;">
                                                                                                                    <asp:Label ID="Labelh207o" runat="server" Text="Negara Bagian/ Kota"></asp:Label></td>
                                                                                                                <td style="width: 5%; height: 18px;">
                                                                                                                    <asp:TextBox ID="txtPenerimaNasabah_Korp_KotaOld" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                </td>
                                                                                                                
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #FFF7E6;">
                                                                                                                    <asp:Label ID="Labelh208o" runat="server" Text="Negara"></asp:Label>
                                                                                                                    <asp:Label ID="Labelh159o" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                                <td style="width: 5%; height: 18px">
                                                                                                                    <asp:TextBox ID="txtPenerimaNasabah_Korp_NegaraKorpOld" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                </td>
                                                                                                                
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #FFF7E6;">
                                                                                                                    <asp:Label ID="Labelh209o" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                                                                <td style="height: 18px; width: 5%;">
                                                                                                                    <asp:TextBox ID="txtPenerimaNasabah_Korp_NegaraLainnyaKorpOld" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                </td>
                                                                                                                
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #FFF7E6;">
                                                                                                                    <asp:Label ID="Labelse9271o" runat="server" Text="e. Nilai Transaksi Keuangan (dalam Rupiah )"></asp:Label>
                                                                                                                </td>
                                                                                                                <td style="height: 18px; width: 5%;">
                                                                                                                    <asp:TextBox ID="txtPenerimaNasabah_Korp_NilaiTransaksiKeuanganOld" runat="server"
                                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                </td>
                                                                                                                
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </asp:View>
                                                                                                </asp:MultiView>
                                                                                            sss</td>
                                                                                            <td style="width: 50%">
                                                                                            
                                                                                            
                                                                                            
                                                                                            <asp:MultiView ID="MultiViewPenerimaNasabah_New" runat="server" ActiveViewIndex="0">
                                                                                                    <asp:View ID="ViewPenerimaNasabah_IND_New" runat="server">
                                                                                                        <table style="width: 100%; height: 49px">
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #fff7e6">
                                                                                                                    <asp:Label ID="Label58" runat="server" Text="Perorangan " Font-Bold="True"></asp:Label>
                                                                                                                </td>
                                                                                                                <td style="width: 5%;">
                                                                                                                </td>
                                                                                                                 
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td align="center" style="width: 5%; height: 22px; background-color: silver">
                                                                                                                    <asp:Label ID="Label59" runat="server" Font-Bold="True" Text="Field"></asp:Label></td>
                                                                                                                <td align="center" style="width: 5%; background-color: silver">
                                                                                                                    <asp:Label ID="Label60" runat="server" Font-Bold="True" Text="New Data"></asp:Label></td>
                                                                                                                 
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #fff7e6">
                                                                                                                    <asp:Label ID="Label62" runat="server" Text="a. Nama Lengkap"></asp:Label>
                                                                                                                    <asp:Label ID="Label63" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                                <td style="width: 20%; height: 15px;">
                                                                                                                    <asp:TextBox ID="txtPenerimaNasabah_IND_namaNew" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #fff7e6">
                                                                                                                    <asp:Label ID="Label64" runat="server" Text="b. Tanggal lahir (tgl/bln/thn)" Width="136px"></asp:Label></td>
                                                                                                                <td style="width: 20%; height: 15px;">
                                                                                                                    <asp:TextBox ID="txtPenerimaNasabah_IND_TanggalLahirNew" runat="server" CssClass="searcheditbox"
                                                                                                                        TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                    &nbsp;</td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #fff7e6">
                                                                                                                    <asp:Label ID="Label65" runat="server" Text="c. Kewarganegaraan (Pilih salah satu) "
                                                                                                                        Width="184px"></asp:Label></td>
                                                                                                                <td style="width: 20%; height: 15px;">
                                                                                                                    <asp:RadioButtonList ID="RbPenerimaNasabah_IND_WarganegaraNew" runat="server" RepeatDirection="Horizontal"
                                                                                                                        Enabled="False">
                                                                                                                        <asp:ListItem Value="1">WNI</asp:ListItem>
                                                                                                                        <asp:ListItem Value="2">WNA</asp:ListItem>
                                                                                                                    </asp:RadioButtonList>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #fff7e6">
                                                                                                                    <asp:Label ID="Label69" runat="server" Text="d. Negara "></asp:Label>
                                                                                                                    <asp:Label ID="Label70" runat="server" ForeColor="Red" Text="*"></asp:Label><br />
                                                                                                                    <asp:Label ID="Label71" runat="server" Font-Italic="True" Text="(Wajib diisi apabila kewarganegaraan WNA)"
                                                                                                                        Width="216px"></asp:Label></td>
                                                                                                               <td style="width: 20%; height: 15px;">
                                                                                                                    <asp:TextBox ID="txtPenerimaNasabah_IND_negaraNew" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                    <asp:HiddenField ID="hfPenerimaNasabah_IND_negaraNew" runat="server" />
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #fff7e6">
                                                                                                                    <asp:Label ID="Label77" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                                                                <td style="width: 20%; height: 15px;">
                                                                                                                    <asp:TextBox ID="txtPenerimaNasabah_IND_negaraLainNew" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #fff7e6;">
                                                                                                                    <asp:Label ID="Label78" runat="server" Text="e. Alamat Sesuai Bukti Identitas"></asp:Label></td>
                                                                                                                <td style="width: 5%;">
                                                                                                                </td>
                                                                                                                 
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #fff7e6">
                                                                                                                    <asp:Label ID="Label79" runat="server" Text="Alamat"></asp:Label>
                                                                                                                    <asp:Label ID="Label80" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                                </td>
                                                                                                               <td style="width: 20%; height: 15px;">
                                                                                                                    <asp:TextBox ID="txtPenerimaNasabah_IND_alamatIdenNew" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #fff7e6">
                                                                                                                    <asp:Label ID="Label81" runat="server" Text="Negara Bagian/ Kota"></asp:Label></td>
                                                                                                                <td style="width: 20%; height: 15px;">
                                                                                                                    <asp:TextBox ID="txtPenerimaNasabah_IND_negaraBagianNew" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #fff7e6">
                                                                                                                    <asp:Label ID="Label82" runat="server" Text="Negara"></asp:Label>
                                                                                                                    <asp:Label ID="Label83" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                                                                                </td>
                                                                                                                 <td style="width: 20%; height: 15px;">
                                                                                                                    <asp:TextBox ID="txtPenerimaNasabah_IND_negaraIdenNew" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                    <asp:HiddenField ID="hfPenerimaNasabah_IND_negaraIdenNew" runat="server" />
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #fff7e6;">
                                                                                                                    <asp:Label ID="Label85" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                                                                <td style="width: 20%; height: 15px">
                                                                                                                    <asp:TextBox ID="txtPenerimaNasabah_IND_negaraLainIdenNew" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #fff7e6">
                                                                                                                    <asp:Label ID="Label86" runat="server" Text="f. Jenis Dokument Identitas"></asp:Label></td>
                                                                                                                <td style="width: 20%; height: 15px;">
                                                                                                                    <asp:DropDownList ID="CBOPenerimaNasabah_IND_JenisIdenNew" runat="server" Enabled="False">
                                                                                                                    </asp:DropDownList>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #fff7e6;">
                                                                                                                    <asp:Label ID="Label87" runat="server" Text="Nomor Identitas"></asp:Label></td>
                                                                                                                <td style="width: 20%; height: 15px;">
                                                                                                                    <asp:TextBox ID="txtPenerimaNasabah_IND_NomorIdenNew" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #fff7e6;">
                                                                                                                    <asp:Label ID="Label88" runat="server" Text="g. Nilai Transaksi Keuangan (dalam Rupiah)"></asp:Label>
                                                                                                                </td>
                                                                                                               <td style="width: 20%; height: 15px;">
                                                                                                                    <asp:TextBox ID="txtPenerimaNasabah_IND_NilaiTransaksiKeuanganNew" runat="server"
                                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #fff7e6;">
                                                                                                                </td>
                                                                                                                <td style="width: 5%;">
                                                                                                                </td>
                                                                                                               
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </asp:View>
                                                                                                    <asp:View ID="ViewPenerimaNasabah_Korp_New" runat="server">
                                                                                                        <table style="width: 100%; height: 31px">
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #FFF7E6;">
                                                                                                                    <asp:Label ID="Labelko223" runat="server" Text="Korporasi" Font-Bold="True"></asp:Label>
                                                                                                                </td>
                                                                                                                <td style="height: 18px; width: 5%;">
                                                                                                                </td>
                                                                                                                 
                                                                                                            </tr>
                                                                                                            <tr style="color: #000000">
                                                                                                                <td align="center" style="width: 5%; height: 22px; background-color: silver">
                                                                                                                    <asp:Label ID="Label34y" runat="server" Font-Bold="True" Text="Field"></asp:Label></td>
                                                                                                                <td align="center" style="width: 5%; background-color: silver">
                                                                                                                    <asp:Label ID="Label35y" runat="server" Font-Bold="True" Text="New Data"></asp:Label></td>
                                                                                                                
                                                                                                            </tr>
                                                                                                            <tr style="color: #000000">
                                                                                                                <td style="width: 5%; background-color: #FFF7E6;">
                                                                                                                    <asp:Label ID="Labela225" runat="server" Text="a. Bentuk Badan Usaha "></asp:Label></td>
                                                                                                                 <td style="width: 20%">
                                                                                                                    <asp:TextBox ID="txtPenerimaNasabah_Korp_BentukBadanUsahaNew" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #FFF7E6;">
                                                                                                                    <asp:Label ID="Labelb226" runat="server" Text="Bentuk Badan Usaha Lainnya " Width="144px"></asp:Label></td>
                                                                                                                <td style="width: 20%">
                                                                                                                    <asp:TextBox ID="txtPenerimaNasabah_Korp_BentukBadanUsahaLainnyaNew" runat="server"
                                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #FFF7E6;">
                                                                                                                    <asp:Label ID="Labelb227" runat="server" Text="b. Nama Korporasi" Width="88px"></asp:Label>
                                                                                                                    <asp:Label ID="Labelb149" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                                <td style="width: 20%">
                                                                                                                    <asp:TextBox ID="txtPenerimaNasabah_Korp_namaKorpNew" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #FFF7E6;">
                                                                                                                    <asp:Label ID="Labelb228" runat="server" Text="c. Bidang Usaha Korporasi"></asp:Label></td>
                                                                                                                <td style="width: 20%">
                                                                                                                    <asp:TextBox ID="txtPenerimaNasabah_Korp_BidangUsahaKorpNew" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                    <asp:HiddenField ID="hfPenerimaNasabah_Korp_BidangUsahaKorpNew" runat="server" />
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #FFF7E6;">
                                                                                                                    <asp:Label ID="Label14" runat="server" Text="Bidang Usaha Lainnya " Width="112px"></asp:Label></td>
                                                                                                                <td style="width: 20%;">
                                                                                                                    <asp:TextBox ID="txtPenerimaNasabah_Korp_BidangUsahaLainKorpNew" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #FFF7E6;">
                                                                                                                    <asp:Label ID="Labelb229" runat="server" Text="d. Alamat Lengkap Korporasi"></asp:Label></td>
                                                                                                                <td style="width: 5%; height: 18px;">
                                                                                                                </td>
                                                                                                               
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #FFF7E6;">
                                                                                                                    <asp:Label ID="Labelb199" runat="server" Text="Alamat"></asp:Label>
                                                                                                                    <asp:Label ID="Labelb153" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                                 <td style="width: 20%">
                                                                                                                    <asp:TextBox ID="txtPenerimaNasabah_Korp_AlamatKorpNew" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #FFF7E6;">
                                                                                                                    <asp:Label ID="Labelh207" runat="server" Text="Negara Bagian/ Kota"></asp:Label></td>
                                                                                                                <td style="width: 20%">
                                                                                                                    <asp:TextBox ID="txtPenerimaNasabah_Korp_KotaNew" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #FFF7E6;">
                                                                                                                    <asp:Label ID="Labelh208" runat="server" Text="Negara"></asp:Label>
                                                                                                                    <asp:Label ID="Labelh159" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                                                <td style="width: 20%">
                                                                                                                    <asp:TextBox ID="txtPenerimaNasabah_Korp_NegaraKorpNew" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #FFF7E6;">
                                                                                                                    <asp:Label ID="Labelh209" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                                                               <td style="width: 20%;">
                                                                                                                    <asp:TextBox ID="txtPenerimaNasabah_Korp_NegaraLainnyaKorpNew" runat="server" CssClass="searcheditbox"
                                                                                                                        MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 5%; background-color: #FFF7E6;">
                                                                                                                    <asp:Label ID="Labelse9271" runat="server" Text="e. Nilai Transaksi Keuangan (dalam Rupiah )"></asp:Label>
                                                                                                                </td>
                                                                                                                <td style="width: 20%">
                                                                                                                    <asp:TextBox ID="txtPenerimaNasabah_Korp_NilaiTransaksiKeuanganNew" runat="server"
                                                                                                                        CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </asp:View>
                                                                                                </asp:MultiView>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </asp:View>
                                                                    <asp:View ID="ViewSwiftOutIdenPenerimaNonNasabah" runat="server">
                                                                        <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                                            bgcolor="#dddddd" border="2">
                                                                            <tr>
                                                                                <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                                                    style="height: 6px">
                                                                                    <table cellpadding="0" cellspacing="0" border="0">
                                                                                        <tr>
                                                                                            <td class="formtext">
                                                                                                <asp:Label ID="Labelh12" runat="server" Text="Non Nasabah" Font-Bold="True"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <a href="#" onclick="javascript:ShowHidePanel('DIdentitasPenerimaNonNasabah','Img10','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                                                    title="click to minimize or maximize">
                                                                                                    <img id="Img10" src="Images/search-bar-minimize.gif" border="0" height="12" width="12" /></a>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr id="DIdentitasPenerimaNonNasabah">
                                                                                <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                                                                    <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                                        border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                                    </table>
                                                                                    <table style="width: 100%; height: 49px">
                                                                                        <tr>
                                                                                            <td align="center" style="width: 5%; height: 22px; background-color: silver">
                                                                                                <asp:Label ID="Label37y" runat="server" Font-Bold="True" Text="Field"></asp:Label></td>
                                                                                            <td align="center" style="width: 5%; background-color: silver">
                                                                                                <asp:Label ID="Label38y" runat="server" Font-Bold="True" Text="Old Data"></asp:Label></td>
                                                                                            <td style="width: 20%; background-color: silver">
                                                                                                <asp:Label ID="Label39y" runat="server" Font-Bold="True" Text="New Data"></asp:Label></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                <asp:Label ID="Labelh163" runat="server" Text="a. No. Rek "></asp:Label>
                                                                                            </td>
                                                                                            <td style="width: 5%">
                                                                                                <asp:TextBox ID="txtPenerimaNonNasabah_rekeningOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                            <td style="width: 20%">
                                                                                                <asp:TextBox ID="txtPenerimaNonNasabah_rekeningNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                <asp:Label ID="Labelh9272" runat="server" Text="b. Nama Bank"></asp:Label>
                                                                                            </td>
                                                                                            <td style="width: 5%">
                                                                                                <asp:TextBox ID="txtPenerimaNonNasabah_namabankOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                            <td style="width: 20%">
                                                                                                <asp:TextBox ID="txtPenerimaNonNasabah_namabankNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                <asp:Label ID="Labelh1146" runat="server" Text="c. Nama Lengkap"></asp:Label>
                                                                                                <asp:Label ID="Labelh1147" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                            <td style="width: 5%">
                                                                                                <asp:TextBox ID="txtPenerimaNonNasabah_NamaOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                            <td style="width: 20%">
                                                                                                <asp:TextBox ID="txtPenerimaNonNasabah_NamaNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                <asp:Label ID="Labelh210" runat="server" Text="d. Alamat Sesuai Bukti Identitas"
                                                                                                    Height="14px" Width="152px"></asp:Label>
                                                                                            </td>
                                                                                            <td style="width: 5%">
                                                                                                <asp:TextBox ID="txtPenerimaNonNasabah_Alamat_IdenOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                                            <td style="width: 20%">
                                                                                                <asp:TextBox ID="txtPenerimaNonNasabah_Alamat_IdenNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                <asp:Label ID="Labelalam1150" runat="server" Text="Alamat"></asp:Label></td>
                                                                                            <td style="width: 5%">
                                                                                                <asp:TextBox ID="txtPenerimaNonNasabah_AlamatOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox></td>
                                                                                            <td style="width: 20%">
                                                                                                <asp:TextBox ID="txtPenerimaNonNasabah_AlamatNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="341px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                <asp:Label ID="Labelneg211" runat="server" Text="Negara"></asp:Label>
                                                                                                <asp:Label ID="Labelneg165" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
                                                                                            <td style="width: 5%">
                                                                                                <asp:TextBox ID="txtPenerimaNonNasabah_NegaraOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hfPenerimaNonNasabah_NegaraOld" runat="server" />
                                                                                            </td>
                                                                                            <td style="width: 20%">
                                                                                                <asp:TextBox ID="txtPenerimaNonNasabah_NegaraNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                                <asp:HiddenField ID="hfPenerimaNonNasabah_NegaraNew" runat="server" />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                <asp:Label ID="Labellain212" runat="server" Text="Negara Lainnya"></asp:Label></td>
                                                                                            <td style="width: 5%">
                                                                                                <asp:TextBox ID="txtPenerimaNonNasabah_negaraLainOld" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                                                            <td style="width: 20%">
                                                                                                <asp:TextBox ID="txtPenerimaNonNasabah_negaraLainNew" runat="server" CssClass="searcheditbox"
                                                                                                    MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 5%; background-color: #fff7e6">
                                                                                                <asp:Label ID="Labelnilai74" runat="server" Text="e. Nilai Transaksi Keuangan (dalam Rupiah) "></asp:Label>
                                                                                            </td>
                                                                                            <td style="width: 5%">
                                                                                                <asp:TextBox ID="txtPenerimaNonNasabah_nilaiTransaksiKeuanganOld" runat="server"
                                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                            <td style="width: 20%">
                                                                                                <asp:TextBox ID="txtPenerimaNonNasabah_nilaiTransaksiKeuanganNew" runat="server"
                                                                                                    CssClass="searcheditbox" MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </asp:View>
                                                                </asp:MultiView>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:View>
                                            </asp:MultiView>
                                        </td>
                                    </tr>
                                </table>
                                <table id="TRANSAKSI" align="center" bgcolor="#dddddd" border="2" bordercolor="#ffffff"
                                    cellpadding="2" cellspacing="1" width="100%">
                                    <tr>
                                        <td align="left" background="Images/search-bar-background.gif" style="height: 6px;
                                            width: 100%;" valign="middle">
                                            <table border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td class="formtext">
                                                        <asp:Label ID="Label9275" runat="server" Font-Bold="True" Text="E. Transaksi"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <a href="#" onclick="javascript:ShowHidePanel('ESwiftIntransaksi','Imgrr25','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                            title="click to minimize or maximize">
                                                            <img id="Imgrr25" src="Images/search-bar-minimize.gif" border="0" height="12px" width="12px" />
                                                        </a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:Label ID="Label9276" runat="server" Font-Italic="True" Text="(Diisi oleh PJK Bank  yang bertindak sebagai Penyelenggara Pengirim Asal atau sebagai Penyelenggara Penerus)"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr id="ESwiftIntransaksi">
                                        <td bgcolor="#ffffff" style="height: 125px; width: 100%;" valign="top">
                                            <table border="0" cellpadding="2" cellspacing="1" style="border-top-style: none;
                                                border-right-style: none; border-left-style: none; border-bottom-style: none"
                                                width="100%">
                                            </table>
                                            <table style="width: 100%; height: 49px">
                                                <tr>
                                                    <td align="center" style="width: 5%; height: 22px; background-color: silver">
                                                        <asp:Label ID="Label40y" runat="server" Font-Bold="True" Text="Field"></asp:Label></td>
                                                    <td align="center" style="width: 5%; background-color: silver">
                                                        <asp:Label ID="Label41y" runat="server" Font-Bold="True" Text="Old Data"></asp:Label></td>
                                                    <td style="width: 20%; background-color: silver">
                                                        <asp:Label ID="Label42y" runat="server" Font-Bold="True" Text="New Data"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; height: 18px; background-color: #fff7e6">
                                                        <asp:Label ID="Label9277" runat="server" Text="a. Tanggal Transaksi (tgl/bln/thn) "></asp:Label>
                                                        <asp:Label ID="Label9278" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                    </td>
                                                    <td style="width: 5%">
                                                        <asp:TextBox ID="Transaksi_SwiftIntanggalOld" runat="server" CssClass="searcheditbox"
                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                        &nbsp;</td>
                                                    <td style="width: 20%">
                                                        <asp:TextBox ID="Transaksi_SwiftIntanggalNew" runat="server" CssClass="searcheditbox"
                                                            TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="Label9279" runat="server" Text="b. Waktu Transaksi diproses (Time Indication) "></asp:Label>
                                                    </td>
                                                    <td style="width: 5%">
                                                        <asp:TextBox ID="Transaksi_SwiftInwaktutransaksiOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 20%">
                                                        <asp:TextBox ID="Transaksi_SwiftInwaktutransaksiNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="Label9280" runat="server" Text="c. Referensi Pengirim " Width="104px"></asp:Label>
                                                        <asp:Label ID="Label9323" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                    </td>
                                                    <td style="width: 5%">
                                                        <asp:TextBox ID="Transaksi_SwiftInSenderOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 20%">
                                                        <asp:TextBox ID="Transaksi_SwiftInSenderNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="Label9284" runat="server" Text="d. Kode Operasi Bank"></asp:Label>
                                                        <asp:Label ID="Label9285" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                    </td>
                                                    <td style="width: 5%">
                                                        <asp:TextBox ID="Transaksi_SwiftInBankOperationCodeOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 20%">
                                                        <asp:TextBox ID="Transaksi_SwiftInBankOperationCodeNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="Label9286" runat="server" Text="e. Kode instuksi "></asp:Label>
                                                    </td>
                                                    <td style="width: 5%">
                                                        <asp:TextBox ID="Transaksi_SwiftInInstructionCodeOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 20%">
                                                        <asp:TextBox ID="Transaksi_SwiftInInstructionCodeNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="Label9287" runat="server" Text="f. Kantor Cabang Penyelenggara Pengirim Asal"></asp:Label>
                                                    </td>
                                                    <td style="width: 5%">
                                                        <asp:TextBox ID="Transaksi_SwiftInkantorCabangPengirimOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 20%">
                                                        <asp:TextBox ID="Transaksi_SwiftInkantorCabangPengirimNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="Label9289" runat="server" Text="g. Kode Tipe Transaksi "></asp:Label>
                                                    </td>
                                                    <td style="width: 5%">
                                                        <asp:TextBox ID="Transaksi_SwiftInkodeTipeTransaksiOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 20%">
                                                        <asp:TextBox ID="Transaksi_SwiftInkodeTipeTransaksiNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="Label9290" runat="server" Text="h. Tanggal Transaksi/Mata Uang/Nilai Transaksi"></asp:Label>
                                                    </td>
                                                    <td style="width: 5%">
                                                    </td>
                                                    <td style="width: 20%">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="Label9291" runat="server" Text="Tanggal Transaksi (Value Date)"></asp:Label>
                                                        <asp:Label ID="Label9292" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                    </td>
                                                    <td style="width: 5%">
                                                        <asp:TextBox ID="Transaksi_SwiftInValueTanggalTransaksiOld" runat="server" CssClass="searcheditbox"
                                                            TabIndex="2" Width="122px" Enabled="False"></asp:TextBox>
                                                        &nbsp;</td>
                                                    <td style="width: 20%">
                                                        <asp:TextBox ID="Transaksi_SwiftInValueTanggalTransaksiNew" runat="server" CssClass="searcheditbox"
                                                            TabIndex="2" Width="122px" Enabled="False"></asp:TextBox>
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; height: 18px; background-color: #fff7e6">
                                                        <asp:Label ID="Label9293" runat="server" Text="Nilai Transaksi (Amount of The EFT)"></asp:Label>
                                                        <asp:Label ID="Label9315" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                    </td>
                                                    <td style="width: 5%">
                                                        <asp:TextBox ID="TtxNilaitransold" runat="server" CssClass="searcheditbox" TabIndex="2"
                                                            Width="122px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 20%">
                                                        <asp:TextBox ID="TtxNilaitransNew" runat="server" CssClass="searcheditbox" TabIndex="2"
                                                            Width="122px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; height: 18px; background-color: #fff7e6">
                                                        <asp:Label ID="Label9294" runat="server" Text="Mata Uang Transaksi (currency of the EFT)"></asp:Label>
                                                        <asp:Label ID="Label9295" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                    </td>
                                                    <td style="width: 5%">
                                                        <asp:TextBox ID="Transaksi_SwiftInMataUangTransaksiOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 20%">
                                                        <asp:TextBox ID="Transaksi_SwiftInMataUangTransaksiNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; height: 18px; background-color: #fff7e6">
                                                        <asp:Label ID="Label15" runat="server" Text="Mata Uang Lainnya"></asp:Label>
                                                    </td>
                                                    <td style="width: 5%">
                                                        <asp:TextBox ID="Transaksi_SwiftInMataUangTransaksiLainnyaOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 20%">
                                                        <asp:TextBox ID="Transaksi_SwiftInMataUangTransaksiLainnyaNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="Label9296" runat="server" Text="Nilai Transaksi Keuangan yang dikirim dalam Rupiah"></asp:Label>
                                                    </td>
                                                    <td style="width: 5%">
                                                        <asp:TextBox ID="Transaksi_SwiftInAmountdalamRupiahOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 20%">
                                                        <asp:TextBox ID="Transaksi_SwiftInAmountdalamRupiahNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="Label9297" runat="server" Text="i. Mata Uang/Nilai Transaksi Keuangan yang diinstruksikan"></asp:Label>
                                                    </td>
                                                    <td style="width: 5%">
                                                    </td>
                                                    <td style="width: 20%">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="Label9298" runat="server" Text="Mata uang yang diinstruksikan "></asp:Label>
                                                    </td>
                                                    <td style="width: 5%">
                                                        <asp:TextBox ID="Transaksi_SwiftIncurrencyOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 20%">
                                                        <asp:TextBox ID="Transaksi_SwiftIncurrencyNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="Label19" runat="server" Text="Mata uang Lainnya "></asp:Label>
                                                    </td>
                                                    <td style="width: 5%">
                                                        <asp:TextBox ID="Transaksi_SwiftIncurrencyLainnyaOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 20%">
                                                        <asp:TextBox ID="Transaksi_SwiftIncurrencyLainnyaNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="Label9299" runat="server" Text="Nilai Transaksi Keuangan yang diinstruksikan "></asp:Label>
                                                    </td>
                                                    <td style="width: 5%">
                                                        <asp:TextBox ID="Transaksi_SwiftIninstructedAmountOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 20%">
                                                        <asp:TextBox ID="Transaksi_SwiftIninstructedAmountNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; height: 18px; background-color: #fff7e6">
                                                        <asp:Label ID="Label9300" runat="server" Text="j. Nilai Tukar (Exchange Rate) "></asp:Label>
                                                    </td>
                                                    <td style="width: 5%">
                                                        <asp:TextBox ID="Transaksi_SwiftInnilaiTukarOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 20%">
                                                        <asp:TextBox ID="Transaksi_SwiftInnilaiTukarNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="Label9301" runat="server" Text="k. Institusi Pengirim"></asp:Label>
                                                    </td>
                                                    <td style="width: 5%">
                                                        <asp:TextBox ID="Transaksi_SwiftInsendingInstitutionOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 20%">
                                                        <asp:TextBox ID="Transaksi_SwiftInsendingInstitutionNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="Label9304" runat="server" Text="l. Tujuan Transaksi "></asp:Label>
                                                    </td>
                                                    <td style="width: 5%">
                                                        <asp:TextBox ID="Transaksi_SwiftInTujuanTransaksiOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 20%">
                                                        <asp:TextBox ID="Transaksi_SwiftInTujuanTransaksiNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                        <asp:Label ID="Label9305" runat="server" Text="m. Sumber Penggunaan Dana "></asp:Label>
                                                        <asp:Label ID="Label9327" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                    </td>
                                                    <td style="width: 5%">
                                                        <asp:TextBox ID="Transaksi_SwiftInSumberPenggunaanDanaOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="296px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 20%">
                                                        <asp:TextBox ID="Transaksi_SwiftInSumberPenggunaanDanaNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="296px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 18px;">
                                                    </td>
                                                    <td style="width: 5%">
                                                    </td>
                                                    <td style="width: 20%">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <table id="INFORMASI LAINNYA" align="center" bordercolor="#ffffff" cellspacing="1"
                                    cellpadding="2" width="100%" bgcolor="#dddddd" border="2">
                                    <tr>
                                        <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                            style="height: 6px">
                                            <table cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td class="formtext">
                                                        <asp:Label ID="Label21" runat="server" Text="F. Informasi Lainnya" Font-Bold="True"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <a href="#" onclick="javascript:ShowHidePanel('FSWIFTOutInformasiLainnya','Imgw12','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                            title="click to minimize or maximize">
                                                            <img id="Imgw12" src="Images/search-bar-minimize.gif" border="0" height="12px" width="12px" />
                                                        </a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:Label ID="Label230" runat="server" Font-Italic="True" Text="(Diisi oleh PJK Bank  yang bertindak sebagai Penyelenggara Pengirim Asal atau sebagai Penyelenggara Penerus)"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr id="FSWIFTOutInformasiLainnya">
                                        <td valign="top" bgcolor="#ffffff" style="height: 125px">
                                            <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                border-right-style: none; border-left-style: none; border-bottom-style: none">
                                            </table>
                                            <table style="width: 100%; height: 49px">
                                                <tr>
                                                    <td align="center" style="width: 5%; height: 22px; background-color: silver">
                                                        <asp:Label ID="Label43" runat="server" Font-Bold="True" Text="Field"></asp:Label></td>
                                                    <td align="center" style="width: 5%; background-color: silver">
                                                        <asp:Label ID="Label44y" runat="server" Font-Bold="True" Text="Old Data"></asp:Label></td>
                                                    <td style="width: 20%; background-color: silver">
                                                        <asp:Label ID="Label45y" runat="server" Font-Bold="True" Text="New Data"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; height: 15px; background-color: #fff7e6">
                                                        <asp:Label ID="Label233" runat="server" Text="a. Informasi Korespondensi Pengirim"></asp:Label>
                                                    </td>
                                                    <td style="width: 5%;">
                                                        <asp:TextBox ID="InformasiLainnya_SWIFTOutSenderOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                    <td style="height: 15px; width: 20%; color: #000000;">
                                                        <asp:TextBox ID="InformasiLainnya_SWIFTOutSenderNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 15px;">
                                                        <asp:Label ID="Label249" runat="server" Text="b. Informasi Korespondensi Penerima  "></asp:Label></td>
                                                    <td style="width: 5%;">
                                                        <asp:TextBox ID="InformasiLainnya_SWIFTOutreceiverOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                    <td style="width: 20%; height: 15px; color: #000000;">
                                                        <asp:TextBox ID="InformasiLainnya_SWIFTOutreceiverNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 15px;">
                                                        <asp:Label ID="Label253" runat="server" Text="c. Informasi Institusi Pengganti Ketiga"
                                                            Width="218px"></asp:Label></td>
                                                    <td style="width: 5%">
                                                        <asp:TextBox ID="InformasiLainnya_SWIFTOutthirdReimbursementOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                    <td style="width: 20%; color: #000000; height: 15px;">
                                                        <asp:TextBox ID="InformasiLainnya_SWIFTOutthirdReimbursementNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 15px;">
                                                        <asp:Label ID="Label254" runat="server" Text="d. Informasi Institusi Perantara Pihak Ketiga "
                                                            Width="262px"></asp:Label></td>
                                                    <td style="width: 5%">
                                                        <asp:TextBox ID="InformasiLainnya_SWIFTOutintermediaryOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                    <td style="width: 20%; color: #000000; height: 15px;">
                                                        <asp:TextBox ID="InformasiLainnya_SWIFTOutintermediaryNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 15px;">
                                                        <asp:Label ID="Label256" runat="server" Text="e. Informasi Remittance"></asp:Label>
                                                    </td>
                                                    <td style="width: 5%;">
                                                        <asp:TextBox ID="InformasiLainnya_SWIFTOutRemittanceOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 20%; color: #000000; height: 15px;">
                                                        <asp:TextBox ID="InformasiLainnya_SWIFTOutRemittanceNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 15px;">
                                                        <asp:Label ID="Label257" runat="server" Text="f. Informasi Pengirim ke Penerima"></asp:Label></td>
                                                    <td style="width: 5%">
                                                        <asp:TextBox ID="InformasiLainnya_SWIFTOutSenderToReceiverOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 20%; color: #000000; height: 15px;">
                                                        <asp:TextBox ID="InformasiLainnya_SWIFTOutSenderToReceiverNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 15px;">
                                                        <asp:Label ID="Label258" runat="server" Text="g. Laporan Pemerintah"></asp:Label></td>
                                                    <td style="width: 5%;">
                                                        <asp:TextBox ID="InformasiLainnya_SWIFTOutRegulatoryReportOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                    <td style="width: 20%; height: 15px; color: #000000;">
                                                        <asp:TextBox ID="InformasiLainnya_SWIFTOutRegulatoryReportNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 15px;">
                                                        <asp:Label ID="Label259" runat="server" Text="h. Isi Amplop"></asp:Label></td>
                                                    <td style="width: 5%">
                                                        <asp:TextBox ID="InformasiLainnya_SWIFTOutEnvelopeContentsOld" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox></td>
                                                    <td style="width: 20%; color: #000000; height: 15px;">
                                                        <asp:TextBox ID="InformasiLainnya_SWIFTOutEnvelopeContentsNew" runat="server" CssClass="searcheditbox"
                                                            MaxLength="50" TabIndex="2" Width="125px" Enabled="False"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 5%; background-color: #fff7e6; height: 15px;">
                                                    </td>
                                                    <td style="width: 5%;">
                                                    </td>
                                                    <td style="width: 20%; height: 15px; color: #000000;">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <table id="FUNCTION BUTTON" width="100%">
                                    <tr>
                                        <td style="width: 387px">
                                        </td>
                                        <td style="width: 122px">
                                            <asp:Button ID="BtnSave" runat="server" Text="Accept" />
                                        </td>
                                        <td style="width: 107px">
                                            <asp:Button ID="BtnReject" runat="server" Text="Reject" />
                                        </td>
                                        <td>
                                            <ajax:AjaxPanel ID="AjaxPanel111" runat="server" Width="100%">
                                                <asp:Button ID="BtnCancel" runat="server" Text="<<Back" />
                                            </ajax:AjaxPanel>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </asp:View>
                            <asp:View ID="VwConfirmation" runat="server">
                                <table style="width: 100%" bordercolor="#ffffff" cellspacing="1" cellpadding="2"
                                    width="100%" bgcolor="#dddddd" border="0">
                                    <tr bgcolor="#ffffff">
                                        <td colspan="2" align="center" style="height: 17px">
                                            <asp:Label ID="LblConfirmation" runat="server" meta:resourcekey="LblConfirmationResource1"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr bgcolor="#ffffff">
                                        <td align="center" colspan="2">
                                            <asp:ImageButton ID="ImgBtnAdd" runat="server" ImageUrl="~/images/button/Ok.gif"
                                                CausesValidation="False" meta:resourcekey="ImgBtnAddResource1" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:View>
                        </asp:MultiView>
                    </ajax:AjaxPanel>
                </td>
            </tr>
        </table>
        <ajax:AjaxPanel ID="AjaxPanel6" runat="server" meta:resourcekey="AjaxPanel14Resource1">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left" background="Images/button-bground.gif" valign="middle">
                        <img height="1" src="Images/blank.gif" width="5" />
                    </td>
                    <td align="left" background="Images/button-bground.gif" valign="middle">
                        <img height="15" src="images/arrow.gif" width="15" />&nbsp;
                    </td>
                    <td background="Images/button-bground.gif">
                        &nbsp;
                    </td>
                    <td background="Images/button-bground.gif">
                        &nbsp;
                    </td>
                    <td background="Images/button-bground.gif">
                        &nbsp;
                    </td>
                    <td background="Images/button-bground.gif" width="99%">
                        <img height="1" src="Images/blank.gif" width="1" />
                        <asp:CustomValidator ID="CvalHandleErr" runat="server" Display="None" meta:resourcekey="CvalHandleErrResource1"
                            ValidationGroup="handle"></asp:CustomValidator>
                        <asp:CustomValidator ID="CvalPageErr" runat="server" Display="None" meta:resourcekey="CvalPageErrResource1"></asp:CustomValidator>
                    </td>
                    <td>
                        
                    </td>
                </tr>
            </table>
            <asp:CustomValidator ID="CustomValidator1" runat="server" Display="None" meta:resourcekey="CustomValidator1Resource1"
                ValidationGroup="handle"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </asp:CustomValidator>
            <asp:CustomValidator ID="CustomValidator2" runat="server" Display="None" meta:resourcekey="CustomValidator2Resource1"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </asp:CustomValidator>
        </ajax:AjaxPanel>
    </div>
</asp:Content>
