<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="AuditTrailParameterApprovalDetail.aspx.vb" Inherits="AuditTrailParameterApprovalDetail" title="AuditTrail Parameter Approval Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CpContent" Runat="Server">
	<table cellSpacing="4" cellPadding="0" width="100%">
		<tr>
			<td vAlign="bottom" align="left"><IMG height="15" src="images/dot_title.gif" width="15"></td>
			<td class="maintitle" vAlign="bottom" width="99%"><asp:label id="LabelTitle" runat="server"></asp:label></td>
		</tr>
	</table>
    <TABLE id="TableDetail" borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%"
	    bgColor="#dddddd" border="2" runat="server">
	     <TR class="formText" id="ATPAdd1">
		    <TD bgColor="#ffffff" height="24" style="width: 22px">&nbsp;</TD>
		    <TD bgColor="#ffffff" colspan="1">
                Maximun Audit Trail Record</TD>
             <td bgcolor="#ffffff" colspan="1">
                 :</td>
		    <TD width="80%" bgColor="#ffffff"><asp:label id="LabelMaximunAuditTrailRecord" runat="server"></asp:label></TD>
	    </TR>
        <tr class="formText" id="ATPAdd2">
            <td bgcolor="#ffffff" height="24" style="width: 22px">
            </td>
            <td bgcolor="#ffffff" colspan="1">
                Type Alert</td>
            <td bgcolor="#ffffff" colspan="1">
                :</td>
            <td bgcolor="#ffffff" width="80%">
                <asp:Label ID="LabelTypeAlert" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText" id="ATPAdd3">
            <td bgcolor="#ffffff" height="24" style="width: 22px">
            </td>
            <td bgcolor="#ffffff" colspan="1">
                Operator Email Address</td>
            <td bgcolor="#ffffff" colspan="1">
            </td>
            <td bgcolor="#ffffff" width="80%">
                <asp:Label ID="LabelOperatorEmailAddress" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText" id="ATPAdd4">
            <td bgcolor="#ffffff" height="24" style="width: 22px">
            </td>
            <td bgcolor="#ffffff" colspan="1">
                Number of record before reach out</td>
            <td bgcolor="#ffffff" colspan="1">
            </td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelNumberofRecordBeforeReachOut" runat="server"></asp:Label></td>
        </tr>
	    <tr class="formText" id="ATPEdi1">
		    <td height="24" colspan="4" bgcolor="#ffffcc">&nbsp;Old Values</td>
		    <td colspan="4" bgcolor="#ffffcc">&nbsp;New Values</td>
	    </tr>
	    <tr class="formText" id="ATPEdi2">
		    <td bgcolor="#ffffff" style="width: 22px;">&nbsp;</td>
		    <td width="24%" bgcolor="#ffffff">
                Maximun Audit Trail Record</td>
		    <td width="1%" bgcolor="#ffffff">:</td>
		    <td width="20%" bgcolor="#ffffff"><asp:label id="LabelMaximunAuditTrailRecordOld" runat="server"></asp:label></td>
            <td bgcolor="#ffffff" style="width: 22px;">
            </td>
		    <td width="24%" bgcolor="#ffffff">
                Maximun Audit Trail Record</td>
		    <td width="5%" bgcolor="#ffffff" style="width: 3px;">:</td>
		    <td bgcolor="#ffffff"><asp:label id="LabelMaximunAuditTrailRecordNew" runat="server"></asp:label></td>
	    </tr>
	    <tr class="formText" id="ATPEdi3">
		    <td bgcolor="#ffffff" style="width: 22px">&nbsp;</td>
		    <td bgcolor="#ffffff">
                Type Alert</td>
		    <td bgcolor="#ffffff" style="width: 44px">:</td>
		    <td bgcolor="#ffffff"><asp:label id="LabelTypeAlertOld" runat="server"></asp:label></td>
            <td bgcolor="#ffffff">
            </td>
		    <td bgcolor="#ffffff">
                Type Alert</td>
		    <td bgcolor="#ffffff">:</td>
		    <td bgcolor="#ffffff"><asp:label id="LabelTypeAlertNew" runat="server"></asp:label></td>
	    </tr>
        <tr class="formText" id="ATPEdi4">
            <td bgcolor="#ffffff" style="width: 22px">
            </td>
            <td bgcolor="#ffffff">
                Operator Email Address</td>
            <td bgcolor="#ffffff" style="width: 44px">
                :</td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelOperatorEmailAddressOld" runat="server"></asp:Label></td>
            <td bgcolor="#ffffff">
            </td>
            <td bgcolor="#ffffff">
                Operator Email Address</td>
            <td bgcolor="#ffffff">
                :</td>
            <td bgcolor="#ffffff">
                <asp:Label ID="LabelOperatorEmailAddressNew" runat="server"></asp:Label></td>
        </tr>
	    <tr class="formText" id="ATPEdi5">
		    <td bgcolor="#ffffff" style="width: 22px">&nbsp;</td>
		    <td bgcolor="#ffffff">
                Number of record before reach out</td>
		    <td bgcolor="#ffffff" style="width: 44px">:</td>
		    <td bgcolor="#ffffff">
                <asp:Label ID="LabelNumberofRecordBeforeReachOutOld" runat="server"></asp:Label></td>
            <td bgcolor="#ffffff">
            </td>
		    <td bgcolor="#ffffff">
                Number of record before reach out</td>
		    <td bgcolor="#ffffff">:</td>
		    <td bgcolor="#ffffff">
                <asp:Label ID="LabelNumberofRecordBeforeReachOutNew" runat="server"></asp:Label></td>
	    </tr>
	    <TR class="formText" id="Button11" bgColor="#dddddd" height="30">
		    <TD style="width: 22px"><IMG height="15" src="images/arrow.gif" width="15"></TD>
		    <TD colSpan="7">
			    <TABLE cellSpacing="0" cellPadding="3" border="0">
				    <TR>
					    <TD style="width: 35px"><asp:imagebutton id="ImageAccept" runat="server" CausesValidation="True" SkinID="AcceptButton"></asp:imagebutton></TD>
					    <TD><asp:imagebutton id="ImageReject" runat="server" CausesValidation="False" SkinID="RejectButton"></asp:imagebutton></TD>
					    <td><asp:imagebutton id="ImageBack" runat="server" CausesValidation="False" SkinID="BackButton"></asp:imagebutton></td>
				    </TR>
			    </TABLE>
                <asp:CustomValidator ID="cvalPageErr" runat="server" Display="Dynamic"></asp:CustomValidator></TD>
	    </TR>
    </TABLE>
</asp:Content>