
Partial Class PopupBranchList
    Inherits Parent

#Region "Set Session"

    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("BranchListPageSelected") Is Nothing, New ArrayList, Session("BranchListPageSelected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("BranchListPageSelected") = value
        End Set
    End Property
    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("BranchListPageSort") Is Nothing, "RegionMaster.BranchCode asc", Session("BranchListPageSort"))
        End Get
        Set(ByVal Value As String)
            Session("BranchListPageSort") = Value
        End Set
    End Property
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("BranchListCurrentPage") Is Nothing, 0, Session("BranchListCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("BranchListCurrentPage") = Value
        End Set
    End Property
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("BranchListRowTotal") Is Nothing, 0, Session("BranchListRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("BranchListRowTotal") = Value
        End Set
    End Property
    Private Property SetAndGetSearchingCriteria() As String
        Get
            Return IIf(Session("BranchListSearchCriteria") Is Nothing, "", Session("BranchListSearchCriteria"))
        End Get
        Set(ByVal Value As String)
            Session("BranchListSearchCriteria") = Value
        End Set
    End Property

    Private Function SetAllSearchingCriteria() As String
        Dim StrSearch As String = ""
        Try
            If TextSearch.Text <> "" Then
                If ComboSearch.SelectedValue = "BranchCode" Then
                    If StrSearch = "" Then
                        StrSearch = StrSearch & " BranchCode = " & Me.TextSearch.Text
                    End If
                End If
                If ComboSearch.SelectedValue = "BranchName" Then
                    If StrSearch = "" Then
                        StrSearch = StrSearch & " BranchName = '" & Me.TextSearch.Text & "'"
                    End If
                End If
                If ComboSearch.SelectedValue = "RegionName" Then
                    If StrSearch = "" Then
                        StrSearch = StrSearch & " RegionName = '" & Me.TextSearch.Text & "'"
                    End If
                End If
            End If
            Return StrSearch
        Catch
            Throw
            Return ""
        End Try
    End Function

    Private Function SetSearchingCriteriaForFirstTime() As String
        Dim StrSearch As String = ""
        Try
            If Session("RegionSelected") <> "" Then
                If StrSearch = "" Then
                    StrSearch = StrSearch & " RegionName = '" & CStr(Session("RegionSelected")) & "'"
                End If
            End If
            Return StrSearch
        Catch
            Throw
            Return ""
        End Try
    End Function

    Private Property SetnGetBindTable() As Data.DataTable
        Get
            Return IIf(Session("BranchListViewData") Is Nothing, Sahassa.AML.Commonly.GetDatasetPaging(Me.TablesRelated, Me.Pk, Me.SetnGetSort, Me.SetnGetCurrentPage + 1, 10, Me.Fields, Me.SetAndGetSearchingCriteria, ""), Session("BranchListViewData"))
        End Get
        Set(ByVal value As Data.DataTable)
            Session("BranchListViewData") = value
        End Set
    End Property

    Private Sub ClearThisPageSessions()
        Session("BranchListPageSelected") = Nothing
        Session("BranchListPageSort") = Nothing
        Session("BranchListCurrentPage") = Nothing
        Session("BranchListRowTotal") = Nothing
        Session("BranchListSearchCriteria") = Nothing
        Session("BranchListViewData") = Nothing
    End Sub

    Private Sub FillSearchField()
        ComboSearch.Items.Add(New ListItem("Branch Code", "BranchCode"))
        ComboSearch.Items.Add(New ListItem("Branch Name", "BranchName"))
        ComboSearch.Items.Add(New ListItem("Region Name", "RegionName"))
    End Sub
#End Region

#Region "Set Property For Table Bind Grid"
    Private ReadOnly Property TablesRelated() As String
        Get
            Return "RegionMaster "
        End Get
    End Property
    Private ReadOnly Property Pk() As String
        Get
            Return "RegionMaster.BranchCode"
        End Get
    End Property
    Private ReadOnly Property Fields() As String
        Get
            Return "RegionMaster.BranchCode, RegionMaster.BranchName, RegionMaster.RegionName "
        End Get
    End Property
#End Region

#Region "Bind Grid View"
    Public Sub BindGrid()
        Me.SetnGetBindTable = Sahassa.AML.Commonly.GetDatasetPaging(Me.TablesRelated, Me.Pk, Me.SetnGetSort, Me.SetnGetCurrentPage + 1, Sahassa.AML.Commonly.GetDisplayedTotalRow, Me.Fields, Me.SetAndGetSearchingCriteria, "")
        Me.GridView.DataSource = Me.SetnGetBindTable
        Me.SetnGetRowTotal = Sahassa.AML.Commonly.GetTableCount(Me.TablesRelated, Me.SetAndGetSearchingCriteria)
        Me.GridView.CurrentPageIndex = Me.SetnGetCurrentPage
        Me.GridView.VirtualItemCount = Me.SetnGetRowTotal
        Me.GridView.DataBind()
    End Sub
#End Region

#Region "Paging Event"
    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
        Catch
            Throw
        End Try
    End Sub
    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
            CollectSelected()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            'LogError(ex)
        End Try
    End Sub
    Protected Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            Me.CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be a positive number greater than zero.")
                End If
            Else
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            'LogError(ex)
        End Try
    End Sub
#End Region

#Region "Sorting Event"

    Protected Sub GridView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridView.ItemDataBound
        'bikin indexing
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Dim chkBox As CheckBox = e.Item.FindControl("CheckBoxExporttoExcel")
                chkBox.Checked = Me.SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)
                'e.Item.Cells(1).Text = e.Item.ItemIndex + 1
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            'LogError(ex)
        End Try
    End Sub

    Protected Sub GridView_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridView.SortCommand
        Dim GridRETBaselViewPage As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridRETBaselViewPage.Columns(Sahassa.AML.Commonly.IndexSort(GridView, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            'LogError(ex)
        End Try
    End Sub
#End Region

#Region "Excel Export Event"
    Private Sub SetCheckedAll()
        Dim i As Int16 = 0
        Dim totalrow As Int16 = 0
        For Each gridRow As DataGridItem In Me.GridView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = CType(gridRow.FindControl("CheckBoxExporttoExcel"), CheckBox)
                If chkBox.Checked Then
                    i = CType(i + 1, Int16)
                End If
                totalrow = CType(totalrow + 1, Int16)
            End If
        Next

        If totalrow = 0 Then
            Me.CheckBoxSelectAll.Checked = False
        Else
            If i = totalrow Then
                Me.CheckBoxSelectAll.Checked = True
            Else
                Me.CheckBoxSelectAll.Checked = False
            End If
        End If
    End Sub

    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In Me.GridView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = gridRow.FindControl("CheckBoxExporttoExcel")
                Dim RETBaselPkId As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
                If ArrTarget.Contains(RETBaselPkId) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(RETBaselPkId)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(RETBaselPkId)
                End If
                Me.SetnGetSelectedItem = ArrTarget
            End If
        Next
    End Sub

    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub 'ClearControls

    Private Sub BindSelected()
        Dim Rows As New ArrayList
        Dim oTableTrans As New Data.DataTable
        Dim oRowTrans As Data.DataRow
        oTableTrans.Columns.Add("BranchCode", GetType(Int64))
        oTableTrans.Columns.Add("BranchName", GetType(String))
        oTableTrans.Columns.Add("RegionName", GetType(String))
        For Each IdPk As Int64 In Me.SetnGetSelectedItem
            Dim rowData() As Data.DataRow = Me.SetnGetBindTable.Select("BranchCode = " & IdPk & "")
            If rowData.Length > 0 Then
                oRowTrans = oTableTrans.NewRow
                oRowTrans(0) = rowData(0)("BranchCode")
                oRowTrans(1) = rowData(0)("BranchName")
                oRowTrans(2) = rowData(0)("RegionName")
                oTableTrans.Rows.Add(oRowTrans)
            End If
        Next
        Me.GridView.DataSource = oTableTrans
        Me.GridView.AllowPaging = False
        Me.GridView.DataBind()

        'Sembunyikan kolom agar tidak ikut diekspor ke excel
        Me.GridView.Columns(0).Visible = False
        Me.GridView.Columns(3).Visible = False
        Me.GridView.Columns(5).Visible = False
        Me.GridView.Columns(6).Visible = False
    End Sub

    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Me.CollectSelected()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelected()
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=TransactionTypeAuxiliaryMapping.xls")
            Response.Charset = ""
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridView)
            GridView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            'LogError(ex)
        End Try
    End Sub

    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        'For Each gridRow As DataGridItem In Me.GridView.Items
        '    If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
        '        Dim RETBaselViewId As String = gridRow.Cells(1).Text
        '        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        '        If Me.SetnGetSelectedItem.Contains(RETBaselViewId) Then
        '            If Not Me.CheckBoxSelectAll.Checked Then
        '                '    ArrTarget.Add(RETBaselViewId)
        '                'Else
        '                ArrTarget.Remove(RETBaselViewId)
        '            End If
        '        Else
        '            If Me.CheckBoxSelectAll.Checked Then
        '                ArrTarget.Add(RETBaselViewId)
        '            End If
        '        End If
        '        Me.SetnGetSelectedItem = ArrTarget
        '    End If
        'Next
        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        For Each gridRow As DataGridItem In Me.GridView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(1).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItem.Contains(StrKey) = False Then
                        ArrTarget.Add(StrKey)
                    End If
                Else
                    If Me.SetnGetSelectedItem.Contains(StrKey) Then
                        ArrTarget.Remove(StrKey)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItem = ArrTarget
    End Sub
#End Region

    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                ClearThisPageSessions()
                Me.GridView.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow
                Me.SetAndGetSearchingCriteria = Me.SetSearchingCriteriaForFirstTime
                Me.FillSearchField()

                Dim clientID As String = Request.QueryString("ClientId")

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using
                'End Using

            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgBtnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnCancel.Click
        Session("RegionSelected") = ""
        Dim scriptStr As String = "<script>window.close();</script>"
        ClientScript.RegisterClientScriptBlock(Me.GetType, "closing", scriptStr)
    End Sub

    Protected Sub imgBtnSelect_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnSelect.Click
        CollectSelected()
        Dim sb As New StringBuilder
        If Me.SetnGetSelectedItem.Count > 0 Then
            For i As Integer = 0 To Me.SetnGetSelectedItem.Count - 1
                sb.Append(Me.SetnGetSelectedItem(i).ToString() & ",")
            Next
        Else
            Dim scriptErr As String = "<script>alert('Please select an item')</script>"
            ClientScript.RegisterClientScriptBlock(Me.GetType, "closing", scriptErr)
            Return
        End If
        Dim strTemp As String = sb.ToString().Trim
        Dim strFinal As String = strTemp.Substring(0, strTemp.Length - 1)
        If Session("BranchListPopUp") IsNot Nothing Then
            Session("BranchListPopUp") = strFinal
        Else
            Session.Add("BranchListPopUp", strFinal)
        End If

        Me.SetnGetSelectedItem = Nothing
        'Session("RegionSelected") = ""
        ''close PopUp
        'Dim scriptStr As String = "<script>window.close();</script>"
        'ClientScript.RegisterClientScriptBlock(Me.GetType, "closing", scriptStr)
        'Dim scriptSTR As String = "javascript:GetRowValues('" & strFinal & "'); window.close();"
        'ClientScript.RegisterClientScriptBlock(Me.GetType, "closing", scriptSTR)
        Dim scriptSTR As String = "<script>window.opener.document.getElementById('ctl00_cpContent_textBranch').value = '" & strFinal & "'; window.close();</script>"
        ClientScript.RegisterClientScriptBlock(Me.GetType, "closing", scriptSTR)
    End Sub

    Protected Sub ImageSearchButton_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSearchButton.Click
        Try
            Me.SetAndGetSearchingCriteria = Me.SetAllSearchingCriteria
            Me.SetnGetCurrentPage = 0
            'Me.BindGrid()
            'Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            'LogError(ex)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
            Me.SetInfoNavigate()
            Me.SetCheckedAll()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            'LogError(ex)
        End Try
    End Sub
End Class
