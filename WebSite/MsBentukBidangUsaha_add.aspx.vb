#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports System.Collections.Generic
#End Region

Partial Class MsBentukBidangUsaha_add
    Inherits Parent



#Region "Function"

    ''' <summary>
    ''' Validasi men-handle seluruh syarat2 data valid
    ''' data yang lolos dari validasi dipastikan lolos ke database 
    ''' karena menggunakan FillorNothing
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ValidasiControl()

        '======================== Validasi textbox :  Id canot Null ====================================================
        If ObjectAntiNull(txtIdBentukBidangUsaha.Text) = False Then Throw New Exception("Id  must be filled  ")
        If Not IsNumeric(Me.txtIdBentukBidangUsaha.Text) Then
            Throw New Exception("ID must be in number format")
        End If
        '======================== Validasi textbox :  Nama canot Null ====================================================
        If ObjectAntiNull(txtBentukBidangUsaha.Text) = False Then Throw New Exception("Nama  must be filled  ")


        If Not DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(txtIdBentukBidangUsaha.Text) Is Nothing Then
            Throw New Exception("ID already exist in the database")
        End If

        If DataRepository.MsBentukBidangUsaha_ApprovalDetailProvider.GetPaged(MsBentukBidangUsaha_ApprovalDetailColumn.IdBentukBidangUsaha.ToString & "=" & txtIdBentukBidangUsaha.Text, "", 0, 1, Nothing).Count > 0 Then
            Throw New Exception("Data exist in List Waiting Approval")
        End If


        'If LBMapping.Items.Count = 0 Then Throw New Exception("data doesnt have list Mapping")
    End Sub

#End Region

#Region "events..."
    Protected Sub ImgBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBack.Click
        Response.Redirect("MsBentukBidangUsaha_View.aspx")
    End Sub
    Protected Sub imgBrowse_click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBrowse.Click
        Try
            If Not Get_DataFromPicker Is Nothing Then
                Listmaping.AddRange(Get_DataFromPicker)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgDeleteItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgDeleteItem.Click
        Try
            If LBMapping.SelectedIndex < 0 Then
                Throw New Exception("No data Selected")
            End If
            Listmaping.RemoveAt(LBMapping.SelectedIndex)

        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    '    Transcope.Complete()
                End Using
                Listmaping = New TList(Of MappingMsBentukBidangUsahaNCBSPPATK)
            Catch ex As Exception
                LogError(ex)
                CvalPageErr.IsValid = False
                CvalPageErr.ErrorMessage = ex.Message
            End Try
        End If

    End Sub



    Protected Sub ImgBtnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnSave.Click
        Try
            Page.Validate("handle")
            If Page.IsValid Then
                ValidasiControl()
                'create audittrailtype
                'Dim objAuditTrailType As MsAuditTrailEntityType = DataRepository.MsAuditTrailEntityTypeProvider.GetByPk_MsAuditTrailEntityTypeId(18)
                ' =========== Insert Header Approval
                Dim KeyHeaderApproval As Integer
                Using ObjMsBentukBidangUsaha_Approval As New MsBentukBidangUsaha_Approval
                    With ObjMsBentukBidangUsaha_Approval
                        FillOrNothing(.FK_MsMode_Id, 1, True, oInt)
                        FillOrNothing(.RequestedBy, SessionPkUserId)
                        FillOrNothing(.RequestedDate, Date.Now)
                        .IsUpload = False
                    End With
                    DataRepository.MsBentukBidangUsaha_ApprovalProvider.Save(ObjMsBentukBidangUsaha_Approval)
                    KeyHeaderApproval = ObjMsBentukBidangUsaha_Approval.PK_MsBentukBidangUsaha_Approval_Id
                End Using
                '============ Insert Detail Approval
                Using objMsBentukBidangUsaha_ApprovalDetail As New MsBentukBidangUsaha_ApprovalDetail()
                    With objMsBentukBidangUsaha_ApprovalDetail
                        FillOrNothing(.IdBentukBidangUsaha, txtIdBentukBidangUsaha.Text, True, oInt)
                        FillOrNothing(.BentukBidangUsaha, txtBentukBidangUsaha.Text, True, Ovarchar)

                        FillOrNothing(.Activation, 1, True, oBit)
                        FillOrNothing(.CreatedDate, Date.Now)
                        FillOrNothing(.CreatedBy, SessionUserId)
                        FillOrNothing(.FK_MsBentukBidangUsaha_Approval_Id, KeyHeaderApproval)
                    End With
                    DataRepository.MsBentukBidangUsaha_ApprovalDetailProvider.Save(objMsBentukBidangUsaha_ApprovalDetail)
                    Dim keyHeaderApprovalDetail As Integer = objMsBentukBidangUsaha_ApprovalDetail.PK_MsBentukBidangUsaha_ApprovalDetail_Id
                    'Insert auditrail
                    InsertAuditTrail()
                    '========= Insert mapping item 
                    Dim LobjMappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail As New TList(Of MappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail)
                    For Each objMappingMsBentukBidangUsahaNCBSPPATK As MappingMsBentukBidangUsahaNCBSPPATK In Listmaping
                        Dim Mapping As New MappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail
                        With Mapping
                            FillOrNothing(.idBentukBidangUsaha, objMsBentukBidangUsaha_ApprovalDetail.IdBentukBidangUsaha)
                            FillOrNothing(.IdBentukBidangUsahaNCBS, objMappingMsBentukBidangUsahaNCBSPPATK.IdBentukBidangUsahaNCBS)
                            FillOrNothing(.PK_MsBentukBidangUsaha_Approval_Id, KeyHeaderApproval)
                            FillOrNothing(.PK_MappingMsBentukBidangUsahaNCBSPPATK_approval_detail_Id, keyHeaderApprovalDetail)
                            FillOrNothing(.Nama, objMappingMsBentukBidangUsahaNCBSPPATK.Nama)
                            LobjMappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail.Add(Mapping)
                        End With
                    Next
                    DataRepository.MappingMsBentukBidangUsahaNCBSPPATK_Approval_DetailProvider.Save(LobjMappingMsBentukBidangUsahaNCBSPPATK_Approval_Detail)
                    'session Maping item Clear
                    Listmaping.Clear()
                    LblConfirmation.Text = "MsBentukBidangUsaha data is waiting approval for Insert"
                    MtvMsUser.ActiveViewIndex = 1
                    ImgBtnSave.Visible = False
                    ImgBackAdd.Visible = False
                End Using
            End If
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImgBtnAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAdd.Click
        Try
            ClearAllControl(VwAdd)
            MtvMsUser.ActiveViewIndex = 0
            ImgBtnSave.Visible = True
            ImgBackAdd.Visible = True
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBackAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBackAdd.Click
        Response.Redirect("MsBentukBidangUsaha_View.aspx")
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            BindListMapping()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub BindListMapping()
        LBMapping.DataSource = ListMappingDisplay
        LBMapping.DataBind()
    End Sub

#End Region

#Region "Property..."
    ''' <summary>
    ''' Mengambil Data dari Picker
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Get_DataFromPicker() As TList(Of MappingMsBentukBidangUsahaNCBSPPATK)
        Get
            Dim Temp As New TList(Of MappingMsBentukBidangUsahaNCBSPPATK)
            Dim L_MsBentukBidangUsahaNCBS As TList(Of MsBentukBidangUsahaNCBS)
            Try
                If Session("PickerMsBentukBidangUsahaNCBS.Data") Is Nothing Then
                    Throw New Exception
                End If
                L_MsBentukBidangUsahaNCBS = Session("PickerMsBentukBidangUsahaNCBS.Data")
                For Each i As MsBentukBidangUsahaNCBS In L_MsBentukBidangUsahaNCBS
                    Dim Tempmapping As New MappingMsBentukBidangUsahaNCBSPPATK
                    With Tempmapping
                        .IdBentukBidangUsahaNCBS = i.IdBentukBidangusahaNCBS
                        .Nama = i.BentukBidangUsahaNCBS
                    End With
                    Temp.Add(Tempmapping)
                Next
            Catch ex As Exception
                L_MsBentukBidangUsahaNCBS = Nothing
            End Try
            Return Temp
        End Get
    End Property
    ''' <summary>
    ''' Menyimpan Item untuk mapping sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Listmaping() As TList(Of MappingMsBentukBidangUsahaNCBSPPATK)
        Get
            Return Session("Listmaping.data")
        End Get
        Set(ByVal value As TList(Of MappingMsBentukBidangUsahaNCBSPPATK))
            Session("Listmaping.data") = value
        End Set
    End Property
    ''' <summary>
    ''' List yang tampil di ListBox
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplay() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingMsBentukBidangUsahaNCBSPPATK In Listmaping.FindAllDistinct("IdBentukBidangusahaNCBS")
                Temp.Add(i.IdBentukBidangUsahaNCBS.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property
#End Region

    Private Sub InsertAuditTrail()
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(3)
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "MsBentukBidangUsaha", "ID", "Add", "", txtIdBentukBidangUsaha.Text, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "MsBentukBidangUsaha", "Nama", "Add", "", txtBentukBidangUsaha.Text, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "MsBentukBidangUsaha", "Mapping Item", "Add", "", LBMapping.Text, "Accepted")
            End Using
        Catch
            Throw
        End Try
    End Sub
End Class


