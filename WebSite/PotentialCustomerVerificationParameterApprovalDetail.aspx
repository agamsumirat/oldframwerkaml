<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="PotentialCustomerVerificationParameterApprovalDetail.aspx.vb" Inherits="PotentialCustomerVerificationParameterApprovalDetail" title="Potential Customer Verification Parameter Approval Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
	<table cellSpacing="4" cellPadding="0" width="100%">
		<tr>
			<td vAlign="bottom" align="left"><IMG height="15" src="images/dot_title.gif" width="15"></td>
			<td class="maintitle" vAlign="bottom" width="99%"><asp:label id="LabelTitle" runat="server"></asp:label></td>
		</tr>
	</table>
    <TABLE id="TableDetail" borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%"
	    bgColor="#dddddd" border="2" runat="server">
	       <TR class="formText" id="UserAdd1">
		    <TD bgColor="#ffffff" height="24" style="width: 22px">&nbsp;</TD>
		    <TD bgColor="#ffffff" colspan="1">
                Minimum Result Percentage</TD>
             <td bgcolor="#ffffff" colspan="1">
                 :</td>
		    <TD width="80%" bgColor="#ffffff"><asp:label id="LabelMinimumResultPercentageAdd" runat="server"></asp:label></TD>
	    </TR>
	    <tr class="formText" id="UserEdi1">
		    <td height="24" colspan="4" bgcolor="#ffffcc">&nbsp;Old Values</td>
		    <td colspan="4" bgcolor="#ffffcc">&nbsp;New Values</td>
	    </tr>
	    <tr class="formText" id="UserEdi2">
		    <td bgcolor="#ffffff" style="width: 2%; height: 32px">&nbsp;</td>
		    <td width="30%" bgcolor="#ffffff" style="height: 32px; width: 20%;">
                Minimum Result Percentage</td>
		    <td width="5%" bgcolor="#ffffff" style="height: 32px; width: 1%;">:</td>
		    <td bgcolor="#ffffff" style="height: 32px"><asp:label id="LabelMinimumResultPercentageOld" runat="server"></asp:label></td>
		    <td bgcolor="#ffffff" style="width: 2%; height: 32px">&nbsp;</td>
		    <td width="30%" bgcolor="#ffffff" style="height: 32px; width: 20%;">
                Minimum Result Percentage</td>
		    <td bgcolor="#ffffff" style="height: 32px; width: 1%;">:</td>
		    <td bgcolor="#ffffff" style="height: 32px"><asp:label id="LabelMinimumResultPercentageNew" runat="server"></asp:label></td>
	    </tr>
	    <TR class="formText" id="Button11" bgColor="#dddddd" height="30">
		    <TD style="width: 22px"><IMG height="15" src="images/arrow.gif" width="15"></TD>
		    <TD colSpan="7">
			    <TABLE cellSpacing="0" cellPadding="3" border="0">
				    <TR>
					    <TD style="width: 35px"><asp:imagebutton id="ImageAccept" runat="server" CausesValidation="True" SkinID="AcceptButton"></asp:imagebutton></TD>
					    <TD><asp:imagebutton id="ImageReject" runat="server" CausesValidation="False" SkinID="RejectButton"></asp:imagebutton></TD>
					    <td><asp:imagebutton id="ImageBack" runat="server" CausesValidation="False" SkinID="BackButton"></asp:imagebutton></td>
				    </TR>
			    </TABLE>
                <asp:CustomValidator ID="cvalPageErr" runat="server" Display="Dynamic"></asp:CustomValidator></TD>
	    </TR>
    </TABLE>
</asp:Content>