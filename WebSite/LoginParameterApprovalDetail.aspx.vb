Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper
Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports System


Partial Class LoginParameterApprovalDetail
    Inherits parent

#Region " Property "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get confirm type
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property ParamType() As Sahassa.AML.Commonly.TypeConfirm
        Get
            Return Me.Request.Params("TypeConfirm")
        End Get
    End Property

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get param pk RiskRating management
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    14/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property LoginParameterApprovalID() As Int64
        Get
            Return Me.Request.Params("ApprovalID")
        End Get
    End Property

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get param Parameters_PendingApprovalID
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    15/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property ParametersPendingApprovalID() As Int64
        Get
            Return Me.Request.Params("PendingApprovalID")
        End Get
    End Property

    ''' <summary>
    ''' get user id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetUserID() As String
        Get
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.Parameters_PendingApprovalTableAdapter
                Return AccessPending.SelectParameters_PendingApprovalUserID(Me.LoginParameterApprovalID)
            End Using
        End Get
    End Property
#End Region

#Region " Load "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' hide all
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    '''     [Stabbian]  01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub HideAll()
        Dim StrId As String = ""
        Select Case Me.ParamType
            Case Sahassa.AML.Commonly.TypeConfirm.LoginParameterAdd
                StrId = "UserAdd"
            Case Sahassa.AML.Commonly.TypeConfirm.LoginParameterEdit
                StrId = "UserEdi"
            Case Else
                Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
        End Select
        For Each ObjRow As HtmlControls.HtmlTableRow In Me.TableDetail.Rows
            ObjRow.Visible = ObjRow.ID = "Button11" Or ObjRow.ID.Substring(0, 7) = StrId
        Next
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load LoadLoginParameterAdd 
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    22/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub LoadLoginParameterAdd()
        Me.LabelTitle.Text = "Activity: Add Login Parameter"
        Using AccessPending As New LoginParameter_Approval
            Using ObjTable As TList(Of LoginParameter_Approval) = DataRepository.LoginParameter_ApprovalProvider.GetPaged("ApprovalID = " & LoginParameterApprovalID, "", 0, Integer.MaxValue, 0)
                If ObjTable.Count > 0 Then
                    Dim rowData As LoginParameter_Approval = DataRepository.LoginParameter_ApprovalProvider.GetByApprovalID(Me.LoginParameterApprovalID)
                    Me.LabelPasswordExpiredPeriodAdd.Text = rowData.PasswordExpiredPeriod
                    Me.LabelMinimumPasswordLengthAdd.Text = rowData.MinimumPasswordLength
                    Me.LabelPasswordRecycleCountAdd.Text = rowData.PasswordRecycleCount
                    Me.LabelLockUnusedAdd.Text = rowData.LockUserAfterNDays
                    Me.LabelAccountLockoutAdd.Text = rowData.AccountLockout

                    Select Case rowData.PasswordChar.ToString
                        Case True
                            Me.LabelPassworCharAdd.Text = "Enable"
                        Case False
                            Me.LabelPassworCharAdd.Text = "Disable"
                    End Select

                    Select Case rowData.PasswordCombination.ToString
                        Case True
                            Me.LabelPassCombinationAdd.Text = "Enable"
                        Case False
                            Me.LabelPassCombinationAdd.Text = "Disable"
                    End Select

                    Select Case rowData.ChangePasswordLogin.ToString
                        Case True
                            Me.LabelChangePassLoginAdd.Text = "Enable"
                        Case False
                            Me.LabelChangePassLoginAdd.Text = "Disable"
                    End Select
                End If
            End Using
        End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load LoginParameter edit
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    14/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub LoadLoginParameterEdit()
        Me.LabelTitle.Text = "Activity: Edit Login Parameter"
        Using AccessPending As New LoginParameter_Approval
            Using ObjTable As TList(Of LoginParameter_Approval) = DataRepository.LoginParameter_ApprovalProvider.GetPaged("ApprovalID = " & LoginParameterApprovalID, "", 0, Integer.MaxValue, 0)
                If ObjTable.Count > 0 Then
                    Dim rowData As LoginParameter_Approval = DataRepository.LoginParameter_ApprovalProvider.GetByApprovalID(Me.LoginParameterApprovalID)
                    Me.LabelPasswordExpiredPeriodNew.Text = rowData.PasswordExpiredPeriod
                    Me.LabelMinimumPasswordLengthNew.Text = rowData.MinimumPasswordLength
                    Me.LabelPasswordRecycleCountNew.Text = rowData.PasswordRecycleCount
                    Me.LabelLockUnusedNew.Text = rowData.LockUserAfterNDays
                    Me.LabelAccountLockoutNew.Text = rowData.AccountLockout

                    Select Case rowData.PasswordChar.ToString
                        Case True
                            Me.LabelPasswordCharNew.Text = "Enable"
                        Case False
                            Me.LabelPasswordCharNew.Text = "Disable"
                    End Select

                    Select Case rowData.PasswordCombination.ToString
                        Case True
                            Me.LabelPassCombinationNew.Text = "Enable"
                        Case False
                            Me.LabelPassCombinationNew.Text = "Disable"
                    End Select

                    Select Case rowData.ChangePasswordLogin.ToString
                        Case True
                            Me.LabelChangePassNew.Text = "Enable"
                        Case False
                            Me.LabelChangePassNew.Text = "Disable"
                    End Select

                    Me.LabelPasswordExpiredPeriodOld.Text = rowData.PasswordExpiredPeriod_Old
                    Me.LabelMinimumPasswordLengthOld.Text = rowData.MinimumPasswordLength_Old
                    Me.LabelPasswordRecycleCountOld.Text = rowData.PasswordRecycleCount_Old
                    Me.LabelLockUnusedOld.Text = rowData.LockUserAfterNDays_Old
                    Me.LabelAccountLockoutOld.Text = rowData.AccountLockout_Old

                    Select Case rowData.PasswordChar_Old.ToString
                        Case True
                            Me.LabelPasswordCharOld.Text = "Enable"
                        Case False
                            Me.LabelPasswordCharOld.Text = "Disable"
                    End Select

                    Select Case rowData.PasswordCombination_Old.ToString
                        Case True
                            Me.LabelPassCombinationOld.Text = "Enable"
                        Case False
                            Me.LabelPassCombinationOld.Text = "Disable"
                    End Select

                    Select Case rowData.ChangePasswordLogin_Old.ToString
                        Case True
                            Me.LabelChangePassOld.Text = "Enable"
                        Case False
                            Me.LabelChangePassOld.Text = "Disable"
                    End Select
                End If
            End Using
        End Using
    End Sub
#End Region

#Region "delete All Approval"
    Public Sub DeleteAllApproval(ByRef oSQLTrans As SqlTransaction)
        Try
            'Using TransScope As New Transactions.TransactionScope
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.LoginParameter_ApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, oSQLTrans)
                Using AccessParametersApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_ApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessParametersApproval, oSQLTrans)
                    Using AccessParametersPendingApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_PendingApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessParametersPendingApproval, oSQLTrans)
                        'hapus item tsb dalam tabel Parameters_Approval
                        AccessParametersApproval.DeleteParametersApproval(Me.ParametersPendingApprovalID)

                        'hapus item tsb dalam tabel LoginParameter_Approval
                        AccessPending.DeleteLoginParameterApproval(ParametersPendingApprovalID)

                        'hapus item tsb dalam tabel Parameters_PendingApproval
                        AccessParametersPendingApproval.DeleteParametersPendingApproval(Me.ParametersPendingApprovalID)
                    End Using
                End Using
                oSQLTrans.Commit()
            End Using
            'End Using
        Catch
            Throw
        End Try
    End Sub
#End Region

#Region "Insert , Check Audit Trail"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="mode"></param>
    ''' <param name="action"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function InsertAuditTrail(ByVal mode As String, ByVal action As String, ByRef oSQLTrans As SqlTransaction) As Boolean
        Try
            'Using TranScope As New Transactions.TransactionScope
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.LoginParameter_ApprovalTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessPending, IsolationLevel.ReadUncommitted)
                Dim ObjTable As Data.DataTable = AccessPending.GetLoginParameterApprovalData(Me.LoginParameterApprovalID)
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.LoginParameter_ApprovalRow = ObjTable.Rows(0)
                    'catat aktifitas dalam tabel Audit Trail
                    Sahassa.AML.AuditTrailAlert.AuditTrailChecking(7)
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                        If mode.ToLower = "add" Then
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - LoginParameter", "PasswordExpiredPeriod", "Add", "", rowData.PasswordExpiredPeriod, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - LoginParameter", "MinimumPasswordLength", "Add", "", rowData.MinimumPasswordLength, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - LoginParameter", "PasswordRecycleCount", "Add", "", rowData.PasswordRecycleCount, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - LoginParameter", "PasswordChar", "Add", "", rowData.PasswordChar, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - LoginParameter", "PasswordCombination", "Add", "", rowData.PasswordCombination, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - LoginParameter", "ChangePasswordLogin", "Add", "", rowData.ChangePasswordLogin, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - LoginParameter", "LockUserAfterNDays", "Add", "", rowData.LockUserAfterNDays, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - LoginParameter", "AccountLockout", "Add", "", rowData.AccountLockout, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - LoginParameter", "CreatedDate", "Add", "", rowData.CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - LoginParameter", "LastUpdateDate", "Add", "", rowData.LastUpdateDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                        Else
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - LoginParameter", "PasswordExpiredPeriod", "Edit", rowData.PasswordExpiredPeriod_Old, rowData.PasswordExpiredPeriod, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - LoginParameter", "MinimumPasswordLength", "Edit", rowData.MinimumPasswordLength_Old, rowData.MinimumPasswordLength, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - LoginParameter", "PasswordRecycleCount", "Edit", rowData.PasswordRecycleCount_Old, rowData.PasswordRecycleCount, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - LoginParameter", "PasswordChar", "Edit", rowData.PasswordChar_Old, rowData.PasswordChar, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - LoginParameter", "PasswordCombination", "Edit", rowData.PasswordCombination_Old, rowData.PasswordCombination, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - LoginParameter", "ChangePasswordLogin", "Edit", rowData.ChangePasswordLogin_Old, rowData.ChangePasswordLogin, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - LoginParameter", "LockUserAfterNDays", "Edit", rowData.LockUserAfterNDays_Old, rowData.LockUserAfterNDays, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - LoginParameter", "AccountLockout", "Edit", rowData.AccountLockout_Old, rowData.AccountLockout, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - LoginParameter", "CreatedDate", "Edit", rowData.CreatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), rowData.CreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Parameters - LoginParameter", "LastUpdateDate", "Edit", rowData.LastUpdateDate_Old.ToString("dd-MMMM-yyyy HH:mm"), rowData.LastUpdateDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                        End If
                    End Using
                End If
            End Using
            'End Using
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
#End Region

#Region " Accept "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' AcceptLoginParameter add accept
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    21/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptLoginParameterAdd()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            Using AccessLoginParameter As New LoginParameter
                Using AccessPending As New LoginParameter_Approval
                    Dim ObjTable As TList(Of LoginParameter_Approval) = DataRepository.LoginParameter_ApprovalProvider.GetPaged("ApprovalID = " & LoginParameterApprovalID, "", 0, Integer.MaxValue, 0)
                    If ObjTable.Count > 0 Then
                        Dim rowData As LoginParameter_Approval = DataRepository.LoginParameter_ApprovalProvider.GetByApprovalID(Me.LoginParameterApprovalID)
                        'tambahkan item tersebut dalam tabel LoginParameter
                        With AccessLoginParameter
                            .PasswordExpiredPeriod = rowData.PasswordExpiredPeriod
                            .MinimumPasswordLength = rowData.MinimumPasswordLength
                            .PasswordRecycleCount = rowData.PasswordRecycleCount
                            .PasswordChar = rowData.PasswordChar
                            .PasswordCombination = rowData.PasswordCombination
                            .ChangePasswordLogin = rowData.ChangePasswordLogin
                            .LockUserAfterNDays = rowData.LockUserAfterNDays
                            .AccountLockout = rowData.AccountLockout
                            .CreatedDate = rowData.LastUpdateDate
                            .LastUpdateDate = rowData.CreatedDate
                        End With
                        DataRepository.LoginParameterProvider.Insert(AccessLoginParameter)
                        If Me.InsertAuditTrail("Add", "Accept", oSQLTrans) Then
                            Me.DeleteAllApproval(oSQLTrans)
                        Else
                            Throw New Exception("Failed to Insert Audit Trail.")
                        End If
                    End If
                End Using
            End Using
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' LoginParameter edit accept
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptLoginParameterEdit()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            Using AccessLoginParameter As LoginParameter = DataRepository.LoginParameterProvider.GetPaged("PK_LoginParameter_ID = 1", "", 0, Integer.MaxValue, 0)(0)
                Using AccessPending As New LoginParameter_Approval
                    Dim ObjTable As TList(Of LoginParameter_Approval) = DataRepository.LoginParameter_ApprovalProvider.GetPaged("ApprovalID = " & LoginParameterApprovalID, "", 0, Integer.MaxValue, 0)
                    If ObjTable.Count > 0 Then
                        Dim rowData As LoginParameter_Approval = DataRepository.LoginParameter_ApprovalProvider.GetByApprovalID(Me.LoginParameterApprovalID)
                        'update item tersebut dalam tabel LoginParameter
                        With AccessLoginParameter
                            .PasswordExpiredPeriod = rowData.PasswordExpiredPeriod
                            .MinimumPasswordLength = rowData.MinimumPasswordLength
                            .PasswordRecycleCount = rowData.PasswordRecycleCount
                            .PasswordChar = rowData.PasswordChar
                            .PasswordCombination = rowData.PasswordCombination
                            .ChangePasswordLogin = rowData.ChangePasswordLogin
                            .LockUserAfterNDays = rowData.LockUserAfterNDays
                            .AccountLockout = rowData.AccountLockout
                            .CreatedDate = rowData.LastUpdateDate
                            .LastUpdateDate = rowData.CreatedDate
                        End With
                        DataRepository.LoginParameterProvider.Update(AccessLoginParameter)
                        If Me.InsertAuditTrail("Edit", "Accept", oSQLTrans) Then
                            Me.DeleteAllApproval(oSQLTrans)
                        Else
                            Throw New Exception("Failed to Insert Audit Trail.")
                        End If
                    End If
                End Using
            End Using
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub

#End Region

#Region " Reject "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' reject LoginParameter add
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    21/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectLoginParameterAdd()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            If Me.InsertAuditTrail("Add", "Reject", oSQLTrans) Then
                Me.DeleteAllApproval(oSQLTrans)
            Else
                Throw New Exception("Failed to Insert Audit Trail.")
            End If
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' LoginParameter reject edit
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    14/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectLoginParameterEdit()
        Dim oSQLTRans As SqlTransaction = Nothing
        Try
            If Me.InsertAuditTrail("Edit", "Reject", oSQLtrans) Then
                Me.DeleteAllApproval(oSQLTrans)
            Else
                Throw New Exception("Failed to Insert Audit Trail.")
            End If
        Catch ex As Exception
            If Not oSQLTRans Is Nothing Then oSQLTRans.Rollback()
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        Finally
            If Not oSQLTRans Is Nothing Then
                oSQLTRans.Dispose()
                oSQLTRans = Nothing
            End If
        End Try
    End Sub

#End Region

    ''' <summary>
    ''' page load event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.ImageAccept.Attributes.Add("onclick", "javascript: if(confirm('Do you want to accept this activity ?')== false) return false;")
                Me.ImageReject.Attributes.Add("onclick", "javascript: if(confirm('Do you want to reject this activity ?')== false) return false;")

                Me.HideAll()
                Select Case Me.ParamType
                    Case Sahassa.AML.Commonly.TypeConfirm.LoginParameterAdd
                        Me.LoadLoginParameterAdd()
                    Case Sahassa.AML.Commonly.TypeConfirm.LoginParameterEdit
                        Me.LoadLoginParameterEdit()
                    Case Else
                        Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
                End Select

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' accept button handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click
        Try
            Select Case Me.ParamType
                Case Sahassa.AML.Commonly.TypeConfirm.LoginParameterAdd
                    Me.AcceptLoginParameterAdd()
                Case Sahassa.AML.Commonly.TypeConfirm.LoginParameterEdit
                    Me.AcceptLoginParameterEdit()
                Case Else
                    Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
            End Select

            Sahassa.AML.Commonly.SessionIntendedPage = "LoginParameterManagementApproval.aspx"

            Me.Response.Redirect("LoginParameterManagementApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' reject button handlert
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click
        Try
            Select Case Me.ParamType
                Case Sahassa.AML.Commonly.TypeConfirm.LoginParameterAdd
                    Me.RejectLoginParameterAdd()
                Case Sahassa.AML.Commonly.TypeConfirm.LoginParameterEdit
                    Me.RejectLoginParameterEdit()
                Case Else
                    Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
            End Select

            Sahassa.AML.Commonly.SessionIntendedPage = "LoginParameterManagementApproval.aspx"

            Me.Response.Redirect("LoginParameterManagementApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' back button handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "LoginParameterManagementApproval.aspx"

        Me.Response.Redirect("LoginParameterManagementApproval.aspx", False)
    End Sub
End Class