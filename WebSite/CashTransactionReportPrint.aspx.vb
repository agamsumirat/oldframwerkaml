
Partial Class CashTransactionReportPrint
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.reportViewer_content.ShowPrintButton = True
        Me.reportViewer_content.LocalReport.Refresh()
        Me.reportViewer_content.DataBind()
    End Sub
End Class
