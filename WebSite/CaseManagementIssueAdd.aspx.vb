
Partial Class CaseManagementIssueAdd
    Inherits Parent

    Public ReadOnly Property GetFocusID() As String
        Get
            Return Me.TextIssueTitle.ClientID
        End Get
    End Property
    Private ReadOnly Property PKCaseManagementID() As Integer
        Get
            Dim temp As String = ""
            temp = Request.Params("PKCaseManagementID")
            If temp = "" AndAlso Not IsNumeric(temp) Then
                Throw New Exception("PKCaseManagementID Is not Valid")
            Else
                Return temp
            End If

        End Get

    End Property
    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Try
            If Page.IsValid Then
                Using adapter As New AMLDAL.CaseManagementTableAdapters.MapCaseManagementIssueTableAdapter
                    'adapter.Insert(Me.PKCaseManagementID, TextIssueTitle.Text.Trim, TextIssueDescription.Text.Trim, CboIssueStatus.SelectedValue, Sahassa.AML.Commonly.SessionPkUserId, Now)
                    adapter.InsertMapCaseManagementIssue(Me.PKCaseManagementID, TextIssueTitle.Text.Trim, TextIssueDescription.Text.Trim, CboIssueStatus.SelectedValue, Sahassa.AML.Commonly.SessionPkUserId, Now)
                End Using
                Using CaseManagementAdapter As New AMLDAL.CaseManagementTableAdapters.SelectCaseManagementTableAdapter
                    Select Case CboIssueStatus.SelectedValue
                        Case "1" ' open
                            CaseManagementAdapter.UpdateCaseManagementHasOpenIssueByPK(True, Me.PKCaseManagementID)
                        Case "2" ' closed
                            CaseManagementAdapter.UpdateCaseManagementHasOpenIssueByPK(False, Me.PKCaseManagementID)
                    End Select
                End Using
                Response.Redirect("CaseManagementViewDetail.aspx?PK_CaseManagementID=" & Me.PKCaseManagementID & "&SelectedIndex=2", False)
            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
        
    End Sub

    Protected Sub FillIssueStatus()
        Try
            Using adapter As New AMLDAL.CaseManagementTableAdapters.MsIssueStatusTableAdapter

                CboIssueStatus.DataSource = adapter.GetData()
                CboIssueStatus.DataTextField = "IssueStatus"
                CboIssueStatus.DataValueField = "PK_IssueStatusId"
                CboIssueStatus.DataBind()
            End Using
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)


            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)


            End Using
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Not IsPostBack Then
            fillIssueStatus()
        End If
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Response.Redirect("CaseManagementViewDetail.aspx?PK_CaseManagementID=" & Me.PKCaseManagementID & "&SelectedIndex=2", False)
    End Sub
End Class
