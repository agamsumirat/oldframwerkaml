<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="PrintSTR.aspx.vb" Inherits="PrintSTR" title="Print STR" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
    &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp;<rsweb:ReportViewer ID="ReportViewer1" runat="server"
        Font-Names="Verdana" Font-Size="8pt" Height="400px" Width="100%">
        <LocalReport ReportPath="G:\AML\APP\development\WebSite\ReportSTR.rdlc">
            <DataSources>
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="ReportCTRSTR_CFMAST" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource2" Name="ReportCTRSTR_CFADDR" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource3" Name="ReportCTRSTR_CFConn" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource4" Name="ReportCTRSTR_NPWP" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource5" Name="ReportCTRSTR_KTP" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource6" Name="ReportCTRSTR_PASPOR" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource7" Name="ReportCTRSTR_SIM" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource8" Name="ReportCTRSTR_KIMKITAS" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource9" Name="ReportCTRSTR_KARTUPELAJAR" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource10" Name="ReportCTRSTR_SIUP" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource11" Name="ReportCTRSTR_MapCaseManagementTransaction" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource12" Name="ReportCTRSTR_Rekening" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource13" Name="ReportCTRSTR_AccountOwner" />
            </DataSources>
        </LocalReport>
    </rsweb:ReportViewer>
    <asp:ObjectDataSource ID="ObjectDataSource13" runat="server" SelectMethod="GetData"
        TypeName="amldal.ReportCTRSTRTableAdapters.AccountOwnerTableAdapter" OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:QueryStringParameter Name="PK_CaseManagementID" QueryStringField="PKCaseManagementID"
                Type="Int64" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource12" runat="server" SelectMethod="GetData"
        TypeName="amldal.ReportCTRSTRTableAdapters.RekeningTableAdapter" OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:QueryStringParameter Name="FK_CaseManagementID" QueryStringField="PKCaseManagementID"
                Type="Int64" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource11" runat="server" SelectMethod="GetData"
        TypeName="amldal.ReportCTRSTRTableAdapters.MapCaseManagementTransactionTableAdapter" DeleteMethod="Delete" InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" UpdateMethod="Update">
        <DeleteParameters>
            <asp:Parameter Name="Original_PK_MapCaseManagementTransactionID" Type="Int64" />
            <asp:Parameter Name="Original_FK_CaseManagementID" Type="Int64" />
            <asp:Parameter Name="Original_TransactionDetailId" Type="Int64" />
            <asp:Parameter Name="Original_TransactionChannelType" Type="Byte" />
            <asp:Parameter Name="Original_CountryCode" Type="String" />
            <asp:Parameter Name="Original_BankCode" Type="String" />
            <asp:Parameter Name="Original_BankName" Type="String" />
            <asp:Parameter Name="Original_CreatedDate" Type="DateTime" />
            <asp:Parameter Name="Original_CIFNo" Type="String" />
            <asp:Parameter Name="Original_AccountNo" Type="Decimal" />
            <asp:Parameter Name="Original_AccountName" Type="String" />
            <asp:Parameter Name="Original_AccountType" Type="String" />
            <asp:Parameter Name="Original_TransactionCode" Type="Decimal" />
            <asp:Parameter Name="Original_TransactionAmount" Type="Decimal" />
            <asp:Parameter Name="Original_CurrencyType" Type="String" />
            <asp:Parameter Name="Original_DebitORCredit" Type="String" />
            <asp:Parameter Name="Original_TransactionDate" Type="DateTime" />
            <asp:Parameter Name="Original_TransactionDate7" Type="Decimal" />
            <asp:Parameter Name="Original_TransactionDate6" Type="Decimal" />
            <asp:Parameter Name="Original_EffectiveDate7" Type="Decimal" />
            <asp:Parameter Name="Original_EffectiveDate6" Type="Decimal" />
            <asp:Parameter Name="Original_TimeEntered" Type="Decimal" />
            <asp:Parameter Name="Original_JobName" Type="String" />
            <asp:Parameter Name="Original_UserId" Type="String" />
            <asp:Parameter Name="Original_MemoRemark" Type="String" />
            <asp:Parameter Name="Original_EFTNumber" Type="Decimal" />
            <asp:Parameter Name="Original_EFTTypeCode" Type="String" />
            <asp:Parameter Name="Original_BOPCode" Type="String" />
            <asp:Parameter Name="Original_AuxiliaryTransactionCode" Type="String" />
            <asp:Parameter Name="Original_TransactionRemark" Type="String" />
            <asp:Parameter Name="Original_TransactionExchangeRate" Type="Decimal" />
            <asp:Parameter Name="Original_TransactionLocalEquivalent" Type="Decimal" />
            <asp:Parameter Name="Original_TransactionTreasuryRate" Type="Decimal" />
            <asp:Parameter Name="Original_OriginalTransactionAmount" Type="Decimal" />
            <asp:Parameter Name="Original_OriginalTransactionCurrencyType" Type="String" />
            <asp:Parameter Name="Original_TransactionTicketNo" Type="String" />
            <asp:Parameter Name="Original_TransactionReferenceNo" Type="String" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="FK_CaseManagementID" Type="Int64" />
            <asp:Parameter Name="TransactionDetailId" Type="Int64" />
            <asp:Parameter Name="TransactionChannelType" Type="Byte" />
            <asp:Parameter Name="CountryCode" Type="String" />
            <asp:Parameter Name="BankCode" Type="String" />
            <asp:Parameter Name="BankName" Type="String" />
            <asp:Parameter Name="CreatedDate" Type="DateTime" />
            <asp:Parameter Name="CIFNo" Type="String" />
            <asp:Parameter Name="AccountNo" Type="Decimal" />
            <asp:Parameter Name="AccountName" Type="String" />
            <asp:Parameter Name="AccountType" Type="String" />
            <asp:Parameter Name="TransactionCode" Type="Decimal" />
            <asp:Parameter Name="TransactionAmount" Type="Decimal" />
            <asp:Parameter Name="CurrencyType" Type="String" />
            <asp:Parameter Name="DebitORCredit" Type="String" />
            <asp:Parameter Name="TransactionDate" Type="DateTime" />
            <asp:Parameter Name="TransactionDate7" Type="Decimal" />
            <asp:Parameter Name="TransactionDate6" Type="Decimal" />
            <asp:Parameter Name="EffectiveDate7" Type="Decimal" />
            <asp:Parameter Name="EffectiveDate6" Type="Decimal" />
            <asp:Parameter Name="TimeEntered" Type="Decimal" />
            <asp:Parameter Name="JobName" Type="String" />
            <asp:Parameter Name="UserId" Type="String" />
            <asp:Parameter Name="MemoRemark" Type="String" />
            <asp:Parameter Name="EFTNumber" Type="Decimal" />
            <asp:Parameter Name="EFTTypeCode" Type="String" />
            <asp:Parameter Name="BOPCode" Type="String" />
            <asp:Parameter Name="AuxiliaryTransactionCode" Type="String" />
            <asp:Parameter Name="TransactionRemark" Type="String" />
            <asp:Parameter Name="TransactionExchangeRate" Type="Decimal" />
            <asp:Parameter Name="TransactionLocalEquivalent" Type="Decimal" />
            <asp:Parameter Name="TransactionTreasuryRate" Type="Decimal" />
            <asp:Parameter Name="OriginalTransactionAmount" Type="Decimal" />
            <asp:Parameter Name="OriginalTransactionCurrencyType" Type="String" />
            <asp:Parameter Name="TransactionTicketNo" Type="String" />
            <asp:Parameter Name="TransactionReferenceNo" Type="String" />
            <asp:Parameter Name="Original_PK_MapCaseManagementTransactionID" Type="Int64" />
            <asp:Parameter Name="Original_FK_CaseManagementID" Type="Int64" />
            <asp:Parameter Name="Original_TransactionDetailId" Type="Int64" />
            <asp:Parameter Name="Original_TransactionChannelType" Type="Byte" />
            <asp:Parameter Name="Original_CountryCode" Type="String" />
            <asp:Parameter Name="Original_BankCode" Type="String" />
            <asp:Parameter Name="Original_BankName" Type="String" />
            <asp:Parameter Name="Original_CreatedDate" Type="DateTime" />
            <asp:Parameter Name="Original_CIFNo" Type="String" />
            <asp:Parameter Name="Original_AccountNo" Type="Decimal" />
            <asp:Parameter Name="Original_AccountName" Type="String" />
            <asp:Parameter Name="Original_AccountType" Type="String" />
            <asp:Parameter Name="Original_TransactionCode" Type="Decimal" />
            <asp:Parameter Name="Original_TransactionAmount" Type="Decimal" />
            <asp:Parameter Name="Original_CurrencyType" Type="String" />
            <asp:Parameter Name="Original_DebitORCredit" Type="String" />
            <asp:Parameter Name="Original_TransactionDate" Type="DateTime" />
            <asp:Parameter Name="Original_TransactionDate7" Type="Decimal" />
            <asp:Parameter Name="Original_TransactionDate6" Type="Decimal" />
            <asp:Parameter Name="Original_EffectiveDate7" Type="Decimal" />
            <asp:Parameter Name="Original_EffectiveDate6" Type="Decimal" />
            <asp:Parameter Name="Original_TimeEntered" Type="Decimal" />
            <asp:Parameter Name="Original_JobName" Type="String" />
            <asp:Parameter Name="Original_UserId" Type="String" />
            <asp:Parameter Name="Original_MemoRemark" Type="String" />
            <asp:Parameter Name="Original_EFTNumber" Type="Decimal" />
            <asp:Parameter Name="Original_EFTTypeCode" Type="String" />
            <asp:Parameter Name="Original_BOPCode" Type="String" />
            <asp:Parameter Name="Original_AuxiliaryTransactionCode" Type="String" />
            <asp:Parameter Name="Original_TransactionRemark" Type="String" />
            <asp:Parameter Name="Original_TransactionExchangeRate" Type="Decimal" />
            <asp:Parameter Name="Original_TransactionLocalEquivalent" Type="Decimal" />
            <asp:Parameter Name="Original_TransactionTreasuryRate" Type="Decimal" />
            <asp:Parameter Name="Original_OriginalTransactionAmount" Type="Decimal" />
            <asp:Parameter Name="Original_OriginalTransactionCurrencyType" Type="String" />
            <asp:Parameter Name="Original_TransactionTicketNo" Type="String" />
            <asp:Parameter Name="Original_TransactionReferenceNo" Type="String" />
            <asp:Parameter Name="PK_MapCaseManagementTransactionID" Type="Int64" />
        </UpdateParameters>
        <SelectParameters>
            <asp:QueryStringParameter Name="casemanagementid" QueryStringField="PKCaseManagementID"
                Type="Int64" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="FK_CaseManagementID" Type="Int64" />
            <asp:Parameter Name="TransactionDetailId" Type="Int64" />
            <asp:Parameter Name="TransactionChannelType" Type="Byte" />
            <asp:Parameter Name="CountryCode" Type="String" />
            <asp:Parameter Name="BankCode" Type="String" />
            <asp:Parameter Name="BankName" Type="String" />
            <asp:Parameter Name="CreatedDate" Type="DateTime" />
            <asp:Parameter Name="CIFNo" Type="String" />
            <asp:Parameter Name="AccountNo" Type="Decimal" />
            <asp:Parameter Name="AccountName" Type="String" />
            <asp:Parameter Name="AccountType" Type="String" />
            <asp:Parameter Name="TransactionCode" Type="Decimal" />
            <asp:Parameter Name="TransactionAmount" Type="Decimal" />
            <asp:Parameter Name="CurrencyType" Type="String" />
            <asp:Parameter Name="DebitORCredit" Type="String" />
            <asp:Parameter Name="TransactionDate" Type="DateTime" />
            <asp:Parameter Name="TransactionDate7" Type="Decimal" />
            <asp:Parameter Name="TransactionDate6" Type="Decimal" />
            <asp:Parameter Name="EffectiveDate7" Type="Decimal" />
            <asp:Parameter Name="EffectiveDate6" Type="Decimal" />
            <asp:Parameter Name="TimeEntered" Type="Decimal" />
            <asp:Parameter Name="JobName" Type="String" />
            <asp:Parameter Name="UserId" Type="String" />
            <asp:Parameter Name="MemoRemark" Type="String" />
            <asp:Parameter Name="EFTNumber" Type="Decimal" />
            <asp:Parameter Name="EFTTypeCode" Type="String" />
            <asp:Parameter Name="BOPCode" Type="String" />
            <asp:Parameter Name="AuxiliaryTransactionCode" Type="String" />
            <asp:Parameter Name="TransactionRemark" Type="String" />
            <asp:Parameter Name="TransactionExchangeRate" Type="Decimal" />
            <asp:Parameter Name="TransactionLocalEquivalent" Type="Decimal" />
            <asp:Parameter Name="TransactionTreasuryRate" Type="Decimal" />
            <asp:Parameter Name="OriginalTransactionAmount" Type="Decimal" />
            <asp:Parameter Name="OriginalTransactionCurrencyType" Type="String" />
            <asp:Parameter Name="TransactionTicketNo" Type="String" />
            <asp:Parameter Name="TransactionReferenceNo" Type="String" />
        </InsertParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource10" runat="server" SelectMethod="GetData"
        TypeName="amldal.ReportCTRSTRTableAdapters.SIUPTableAdapter" OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:QueryStringParameter Name="CFCIF" QueryStringField="cifno" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource9" runat="server" SelectMethod="GetData"
        TypeName="amldal.ReportCTRSTRTableAdapters.KARTUPELAJARTableAdapter" InsertMethod="Insert" OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:QueryStringParameter Name="_CFCIF_" QueryStringField="cifno" Type="String" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="CFSSNO" Type="String" />
        </InsertParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource8" runat="server" SelectMethod="GetData"
        TypeName="amldal.ReportCTRSTRTableAdapters.KIMKITASTableAdapter" InsertMethod="Insert" OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:QueryStringParameter Name="_CFCIF_" QueryStringField="cifno" Type="String" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="CFSSNO" Type="String" />
        </InsertParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource7" runat="server" SelectMethod="GetData"
        TypeName="amldal.ReportCTRSTRTableAdapters.SIMTableAdapter" InsertMethod="Insert" OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:QueryStringParameter Name="_CFCIF_" QueryStringField="cifno" Type="String" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="CFSSNO" Type="String" />
        </InsertParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource6" runat="server" SelectMethod="GetData"
        TypeName="amldal.ReportCTRSTRTableAdapters.PASPORTableAdapter" InsertMethod="Insert" OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:QueryStringParameter Name="_CFCIF_" QueryStringField="cifno" Type="String" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="CFSSNO" Type="String" />
        </InsertParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource5" runat="server" SelectMethod="GetData"
        TypeName="amldal.ReportCTRSTRTableAdapters.KTPTableAdapter" InsertMethod="Insert" OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:QueryStringParameter Name="_CFCIF_" QueryStringField="cifno" Type="String" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="CFSSNO" Type="String" />
        </InsertParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource4" runat="server" SelectMethod="GetData"
        TypeName="amldal.ReportCTRSTRTableAdapters.NPWPTableAdapter" InsertMethod="Insert" OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:QueryStringParameter Name="_CFCIF_" QueryStringField="cifno" Type="String" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="CFSSNO" Type="String" />
        </InsertParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource3" runat="server" SelectMethod="GetData"
        TypeName="amldal.ReportCTRSTRTableAdapters.CFConnTableAdapter" OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:QueryStringParameter Name="cifno" QueryStringField="cifno" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" SelectMethod="GetData"
        TypeName="amldal.ReportCTRSTRTableAdapters.CFADDRTableAdapter" OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:QueryStringParameter Name="_CFCIF_" QueryStringField="cifno" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetData"
        TypeName="AMLDAL.ReportCTRSTRTableAdapters.CFMASTTableAdapter" OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:QueryStringParameter Name="_CFCIF_" QueryStringField="cifno" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ImageButton ID="ImageBack" runat="server" SkinID="BackButton" />
</asp:Content>

