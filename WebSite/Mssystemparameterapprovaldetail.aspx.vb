﻿#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports Sahassa.AML
Imports System.Collections.Generic
#End Region

Partial Class Mssystemparameterapprovaldetail
    Inherits Parent


    Public ReadOnly Property GetID() As Integer
        Get
            Return Request.Params("ID")
        End Get
    End Property

    Public Property objMsSystemParameter_Approval_Detail() As TList(Of MsSystemParameter_Approval_Detail)
        Get
            If Session("systemparameterapprovaldetail.objMsSystemParameter_Approval_Detail") Is Nothing Then
                Session("systemparameterapprovaldetail.objMsSystemParameter_Approval_Detail") = MsSystemParameterBll.GetTlistMsSystemParameter_Approval_Detail(MsSystemParameter_Approval_DetailColumn.Fk_MsSystemParameter_Approval_Id.ToString & "=" & Me.GetID, "", 0, Integer.MaxValue, 0)
                Return CType(Session("systemparameterapprovaldetail.objMsSystemParameter_Approval_Detail"), TList(Of MsSystemParameter_Approval_Detail))
            Else
                Return CType(Session("systemparameterapprovaldetail.objMsSystemParameter_Approval_Detail"), TList(Of MsSystemParameter_Approval_Detail))
            End If
        End Get
        Set(value As TList(Of MsSystemParameter_Approval_Detail))
            Session("systemparameterapprovaldetail.objMsSystemParameter_Approval_Detail") = value
        End Set
    End Property

    Public Property ObjMsSystemParameter_Approval() As MsSystemParameter_Approval
        Get
            If Session("systemparameterapprovaldetail.ObjMsSystemParameter_Approval") Is Nothing Then
                Session("systemparameterapprovaldetail.ObjMsSystemParameter_Approval") = MsSystemParameterBll.GetMsSystemParameter_ApprovalByPk(Me.GetID)
                Return CType(Session("systemparameterapprovaldetail.ObjMsSystemParameter_Approval"), MsSystemParameter_Approval)
            Else
                Return CType(Session("systemparameterapprovaldetail.ObjMsSystemParameter_Approval"), MsSystemParameter_Approval)
            End If
        End Get
        Set(value As MsSystemParameter_Approval)
            Session("systemparameterapprovaldetail.ObjMsSystemParameter_Approval") = value
        End Set
    End Property

    Sub ClearSession()
        Me.objMsSystemParameter_Approval_Detail = Nothing

    End Sub

    Protected Sub Page_Load1(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                ClearSession()
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
                LoadData()

            End If
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub


    Sub LoadData()
        If Not ObjMsSystemParameter_Approval Is Nothing Then
            Using objuser As User = DataRepository.UserProvider.GetBypkUserID(ObjMsSystemParameter_Approval.MsSystemParameter_RequestedBy_Fk_MsUser_Id)
                If Not objuser Is Nothing Then
                    lblRequestedby.Text = objuser.UserName
                End If
            End Using
            lblRequestedDate.Text = ObjMsSystemParameter_Approval.MsSystemParameter_Approval_RequestedDate.ToString("dd-MMM-yyyy")

        End If
        If objMsSystemParameter_Approval_Detail.Count > 0 Then
            txtMsSystemParameter_NameOld.Text = objMsSystemParameter_Approval_Detail(0).MsSystemParameter_Name
            txtMsSystemParameter_Namenew.Text = objMsSystemParameter_Approval_Detail(0).MsSystemParameter_Name
            txtMsSystemParameter_Valuenew.ForeColor = Drawing.Color.Red
            txtMsSystemParameter_ValueOld.Text = objMsSystemParameter_Approval_Detail(0).MsSystemParameter_Value_Old
            txtMsSystemParameter_Valuenew.Text = objMsSystemParameter_Approval_Detail(0).MsSystemParameter_Value
        End If

    End Sub

    Protected Sub imgReject_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles imgReject.Click
        Try
            MsSystemParameterBll.Reject(Me.GetID)
            Response.Redirect("SystemParameterapproval.aspx", False)
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgApprove_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles imgApprove.Click
        Try
            MsSystemParameterBll.Accept(Me.GetID)
            Response.Redirect("SystemParameterapproval.aspx", False)
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImageCancel_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Try
            Response.Redirect("SystemParameterapproval.aspx", False)
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
End Class


