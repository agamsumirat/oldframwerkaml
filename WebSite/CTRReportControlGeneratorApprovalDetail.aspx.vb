﻿Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Partial Class CTRReportControlGeneratorApprovalDetail
    Inherits Parent


    Public ReadOnly Property PK_ListOfGeneratedFileCTR_ID() As Integer
        Get
            Dim strtemp As String = Request.Params("PK_ListOfGeneratedFileCTR_ID")
            Dim intresult As Integer
            If Not Integer.TryParse(strtemp, intresult) Then
                Throw New Exception("PK_ListOfGeneratedFileCTR_ID is invalid")
            Else
                Return intresult
            End If

        End Get

        
    End Property


    Public Property Objvw_CtrReportControlGeneratorApproval() As vw_CtrReportControlGeneratorApproval
        Get
            If Session("StatementGeneratorAdd.Objvw_CtrReportControlGeneratorApproval") Is Nothing Then
                Session("StatementGeneratorAdd.Objvw_CtrReportControlGeneratorApproval") = AMLBLL.ReportControlGeneratorBLL.Getvw_CtrReportControlGeneratorApprovalByPk(Me.PK_ListOfGeneratedFileCTR_ID)
                Return CType(Session("StatementGeneratorAdd.Objvw_CtrReportControlGeneratorApproval"), vw_CtrReportControlGeneratorApproval)
            Else
                Return CType(Session("StatementGeneratorAdd.Objvw_CtrReportControlGeneratorApproval"), vw_CtrReportControlGeneratorApproval)
            End If
        End Get
        Set(value As vw_CtrReportControlGeneratorApproval)
            Session("StatementGeneratorAdd.Objvw_CtrReportControlGeneratorApproval") = value
        End Set
    End Property

    Public Property ObjListOfGeneratedFileCTR() As ListOfGeneratedFileCTR
        Get
            If Session("CTRReportControlGeneratorApprovalDetail.ObjListOfGeneratedFileCTR") Is Nothing Then
                Session("CTRReportControlGeneratorApprovalDetail.ObjListOfGeneratedFileCTR") = AMLBLL.ReportControlGeneratorBLL.GetListOfGeneratedFileCTRByPk(Me.PK_ListOfGeneratedFileCTR_ID)
                Return CType(Session("CTRReportControlGeneratorApprovalDetail.ObjListOfGeneratedFileCTR"), ListOfGeneratedFileCTR)
            Else
                Return CType(Session("CTRReportControlGeneratorApprovalDetail.ObjListOfGeneratedFileCTR"), ListOfGeneratedFileCTR)
            End If
        End Get
        Set(value As ListOfGeneratedFileCTR)
            Session("CTRReportControlGeneratorApprovalDetail.ObjListOfGeneratedFileCTR") = value
        End Set
    End Property

    Sub ClearSession()
        ObjListOfGeneratedFileCTR = Nothing
        Objvw_CtrReportControlGeneratorApproval = Nothing
    End Sub
    Sub LoadData()
        If Not ObjListOfGeneratedFileCTR Is Nothing Then

            LblRequestDate.Text = ObjListOfGeneratedFileCTR.CreatedDate.GetValueOrDefault(Sahassa.AML.Commonly.GetDefaultDate).ToString("dd-MMM-yyyy")
            lblCTRDate.Text = ObjListOfGeneratedFileCTR.TransactionDate.GetValueOrDefault(Sahassa.AML.Commonly.GetDefaultDate).ToString("dd-MMM-yyyy")
            lblReportType.Text = Objvw_CtrReportControlGeneratorApproval.ReportTypeDesc
            LblReportFormat.Text = Objvw_CtrReportControlGeneratorApproval.ReportFormatName
            LblTotalCIF.Text = ObjListOfGeneratedFileCTR.TotalCIF
            LblTotalWIC.Text = ObjListOfGeneratedFileCTR.TotalWIC
            LnkFile.Text = ObjListOfGeneratedFileCTR.FileName
            LblUserid.Text = ObjListOfGeneratedFileCTR.UserIDProposed
        End If
        LabelTitle.Text = "CTR CONTROL GENERATOR APPROVAL DETAIL"


    End Sub


    Protected Sub Page_Load1(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                ClearSession()
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)


                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                End Using
                LoadData()
            End If
        Catch ex As Exception
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImgAccept_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImgAccept.Click
        Try
            AMLBLL.ReportControlGeneratorBLL.AcceptGenerate(Me.PK_ListOfGeneratedFileCTR_ID)

            Page.ClientScript.RegisterStartupScript(Me.GetType, Guid.NewGuid.ToString, "window.alert('CTR Control Generator Approved'); window.location.href='CTRReportControlGeneratorApproval.aspx';", True)

        Catch ex As Exception
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImgReject_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImgReject.Click
        Try
            AMLBLL.ReportControlGeneratorBLL.RejectGenerate(Me.PK_ListOfGeneratedFileCTR_ID)

            Page.ClientScript.RegisterStartupScript(Me.GetType, Guid.NewGuid.ToString, "window.alert('CTR Control Generator Rejected'); window.location.href='CTRReportControlGeneratorApproval.aspx';", True)




        Catch ex As Exception
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImgBack_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImgBack.Click
        Try
            Response.Redirect("CTRReportControlGeneratorApproval.aspx", False)

        Catch ex As Exception
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub LnkFile_Click(sender As Object, e As System.EventArgs) Handles LnkFile.Click
        Try
            Response.AddHeader("content-disposition", "attachment; filename=" & ObjListOfGeneratedFileCTR.FileName)
            Response.Charset = ""
            Me.EnableViewState = False
            Response.ContentType = ""
            Response.BinaryWrite(ObjListOfGeneratedFileCTR.IsiFile)
            Response.End()
        Catch ex As Exception
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class
