﻿Imports SahassaNettier.Entities
Imports SahassaNettier.Data
Imports AMLBLL

Partial Class SegmentActivation
    Inherits Parent


    ReadOnly Property GetPk_Segment As Long
        Get
            If IsNumeric(Request.Params("SegmentID")) Then
                If Not IsNothing(Session("SegmentActivation.PK")) Then
                    Return CLng(Session("SegmentActivation.PK"))
                Else
                    Session("SegmentActivation.PK") = Request.Params("SegmentID")
                    Return CLng(Session("SegmentActivation.PK"))
                End If
            End If
            Return 0
        End Get
    End Property

    Sub clearSession()
        Session("SegmentActivation.PK") = Nothing
    End Sub


    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "SegmentView.aspx"

            Me.Response.Redirect("SegmentView.aspx", False)
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub ImageDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageDelete.Click
        Try

            If Sahassa.AML.Commonly.SessionGroupName.ToLower = "superuser" Then
                If SegmentBLL.Activation(GetPk_Segment) Then
                    lblMessage.Text = lblActiveOrInactive.Text & " Data Success"
                    mtvSegmentAdd.ActiveViewIndex = 1
                End If
            Else
                If SegmentBLL.ActivationApproval(GetPk_Segment) Then
                    lblMessage.Text = lblActiveOrInactive.Text & " Data has been inserted to Approval"
                    mtvSegmentAdd.ActiveViewIndex = 1
                End If
            End If


        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Sub LoadData()
        Using objSegment As Segment = SegmentBLL.getSegmentByPk(GetPk_Segment)
            If Not IsNothing(objSegment) Then
                txtDescription.Text = objSegment.SegmentDescription.ToString
                txtSegmentCode.Text = objSegment.SegmentCode.ToString
                If CBool(objSegment.Activation) Then
                    lblActiveOrInactive.Text = "Deactivated"
                Else
                    lblActiveOrInactive.Text = "Activated"
                End If
            Else
                ImageDelete.Visible = False
            End If
        End Using
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                mtvSegmentAdd.ActiveViewIndex = 0
                clearSession()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
                LoadData()
            End If
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnOK.Click
        Try
            Response.Redirect("SegmentView.aspx")
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class


