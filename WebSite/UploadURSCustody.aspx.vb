Imports System.Data

Partial Class UploadURSCustody
    Inherits Parent
    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try

            If Not Page.IsPostBack Then
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)


                End Using
            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImgSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgSearch.Click


        Try
            If FileUpload1.HasFile Then
                If System.IO.Path.GetExtension(FileUpload1.FileName).ToLower = ".txt" Then

                    Dim currentrow(7) As String
                    Dim TanggalHolding As String
                    Dim NamaInvestor As String
                    Dim CIF As String
                    Dim FUND_ID As String
                    Dim Unit As String
                    Dim NAVPerUnit As String
                    Dim Amount As String

                    Dim objDt As New System.Data.DataTable


                    Using cmd As System.Data.SqlClient.SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("usp_DeleteValidateAccountURSCustody")
                        Dim customerIDParam As New System.Data.SqlClient.SqlParameter("@UserID", Sahassa.AML.Commonly.SessionUserId)
                        cmd.Parameters.Add(customerIDParam)
                        SahassaNettier.Data.DataRepository.Provider.ExecuteNonQuery(cmd)
                    End Using


                    Using NAVDate As New DataColumn()
                        NAVDate.ColumnName = "TanggalHolding"
                        NAVDate.DataType = GetType(String)
                        objDt.Columns.Add(NAVDate)
                    End Using

                    Using objFundid As New DataColumn()
                        objFundid.ColumnName = "NamaInvestor"
                        objFundid.DataType = GetType(String)
                        objDt.Columns.Add(objFundid)
                    End Using


                    Using objFundid As New DataColumn()
                        objFundid.ColumnName = "CIF"
                        objFundid.DataType = GetType(String)
                        objDt.Columns.Add(objFundid)
                    End Using
                    Using objFundid As New DataColumn()
                        objFundid.ColumnName = "Fund_ID"
                        objFundid.DataType = GetType(String)
                        objDt.Columns.Add(objFundid)
                    End Using


                    Using objFundid As New DataColumn()
                        objFundid.ColumnName = "NamaFund"
                        objFundid.DataType = GetType(String)
                        objDt.Columns.Add(objFundid)
                    End Using

                    Using objFundid As New DataColumn()
                        objFundid.ColumnName = "Unit"
                        objFundid.DataType = GetType(String)
                        objDt.Columns.Add(objFundid)
                    End Using
                    Using objFundid As New DataColumn()
                        objFundid.ColumnName = "NAVPerUnit"
                        objFundid.DataType = GetType(String)
                        objDt.Columns.Add(objFundid)
                    End Using

                    Using objFundid As New DataColumn()
                        objFundid.ColumnName = "Amount"
                        objFundid.DataType = GetType(String)
                        objDt.Columns.Add(objFundid)
                    End Using


                    TxtValidasi.Text = ""

                    Using MyReader As New Microsoft.VisualBasic.FileIO.TextFieldParser(FileUpload1.FileContent)
                        MyReader.TextFieldType = FileIO.FieldType.Delimited
                        MyReader.SetDelimiters("|")
                        Dim intBaris As Integer = 0
                        While Not MyReader.EndOfData
                            intBaris += 1

                            If intBaris > 2 Then
                                currentrow = MyReader.ReadFields()

                                If currentrow.Length = 8 Then
                                    Dim objnewrow As DataRow = objDt.NewRow
                                    objnewrow("TanggalHolding") = currentrow(0)
                                    objnewrow("NamaInvestor") = currentrow(1)
                                    objnewrow("CIF") = currentrow(2)
                                    objnewrow("Fund_ID") = currentrow(3)
                                    objnewrow("NamaFund") = currentrow(4)
                                    objnewrow("Unit") = currentrow(5)
                                    objnewrow("NAVPerUnit") = currentrow(6)
                                    objnewrow("Amount") = currentrow(7)
                                    objDt.Rows.Add(objnewrow)
                                Else
                                    ImageSave.Visible = False
                                    Throw New Exception("Row format is not valid. (Line " & intBaris & ")")
                                End If

                            Else
                                MyReader.ReadFields()
                            End If

                        End While

                        If objDt.Rows.Count > 0 Then
                            AMLBLL.USRAccountCustodyBLL.ProcessTextUSRCustody(objDt)
                            BindGrid()
                            TxtValidasi.Text = AMLBLL.USRAccountCustodyBLL.GetInValidAccount
                            If TxtValidasi.Text.Trim.Length > 0 Then
                                PanelValidasi.Visible = True
                                ImageSave.Visible = False
                            Else
                                PanelValidasi.Visible = False
                                ImageSave.Visible = True
                            End If
                        Else
                            ImageSave.Visible = False
                            Throw New Exception("There is no data in file URS Custody.")
                        End If
                        
                    End Using


                Else
                    ImageSave.Visible = False
                    Throw New Exception("File URS Custody Must Txt.")
                End If

            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
    Sub BindGrid()

        GridValidData.DataSource = AMLBLL.USRAccountCustodyBLL.GetValidAccount
        GridValidData.PageSize = Sahassa.AML.Commonly.SessionPagingLimit

        GridValidData.DataBind()
        If GridValidData.Rows.Count > 0 Then
            PanelValidData.Visible = True
        Else
            PanelValidData.Visible = False
        End If
    End Sub


    Protected Sub ImgBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBack.Click
        Try
            Response.Redirect("UploadURSCustody.aspx", False)

        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub GridValidData_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridValidData.PageIndexChanging
        Try

            GridValidData.PageIndex = e.NewPageIndex
            BindGrid()

        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Try
            If Sahassa.AML.Commonly.SessionPkUserId = "1" Then
                AMLBLL.USRAccountCustodyBLL.Save()
                LblMessage.Text = "URS Custody Data Saved"
            Else
                AMLBLL.USRAccountCustodyBLL.SaveApproval()
                LblMessage.Text = "URS Custody Data Saved into Pending Approval"
            End If
            MultiView1.ActiveViewIndex = 1


        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
End Class
