Imports SahassaNettier.Entities
Partial Class SuspiciusPersonView
    Inherits Parent
#Region " Property "
    ''' <summary>
    ''' selected item store
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("MsSuspiciusPersonViewSelected") Is Nothing, New ArrayList, Session("MsSuspiciusPersonViewSelected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("MsSuspiciusPersonViewSelected") = value
        End Set
    End Property
    ''' <summary>
    ''' setnget search field
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetFieldSearch() As String
        Get
            Return IIf(Session("MsSuspiciusPersonViewFieldSearch") Is Nothing, "", Session("MsSuspiciusPersonViewFieldSearch"))
        End Get
        Set(ByVal Value As String)
            Session("MsSuspiciusPersonViewFieldSearch") = Value
        End Set
    End Property
    ''' <summary>
    ''' search value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetValueSearch() As String
        Get
            Return IIf(Session("MsSuspiciusPersonViewValueSearch") Is Nothing, "", Session("MsSuspiciusPersonViewValueSearch"))
        End Get
        Set(ByVal Value As String)
            Session("MsSuspiciusPersonViewValueSearch") = Value
        End Set
    End Property
    ''' <summary>
    ''' sort expresion
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("MsSuspiciusPersonViewSort") Is Nothing, "Nama asc", Session("MsSuspiciusPersonViewSort"))
        End Get
        Set(ByVal Value As String)
            Session("MsSuspiciusPersonViewSort") = Value
        End Set
    End Property
    ''' <summary>
    ''' current page index
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("MsSuspiciusPersonViewCurrentPage") Is Nothing, 0, Session("MsSuspiciusPersonViewCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("MsSuspiciusPersonViewCurrentPage") = Value
        End Set
    End Property
    ''' <summary>
    ''' total pages
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    ''' <summary>
    ''' row total
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("MsSuspiciusPersonViewRowTotal") Is Nothing, 0, Session("MsSuspiciusPersonViewRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("MsSuspiciusPersonViewRowTotal") = Value
        End Set
    End Property
    ''' <summary>
    ''' save bind table
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetBindTable(ByVal whereClause As String, ByVal orderBy As String, ByVal start As Integer, ByVal pageLength As Integer) As VList(Of vw_SuspiciusPerson)
        Get
            Return IIf(Session("MsSuspiciusPersonViewData") Is Nothing, AMLBLL.SuspiciusPersonBLL.GetVw_SuspiciusPerson(whereClause, orderBy, start, pageLength, Me.SetnGetRowTotal), Session("MsSuspiciusPersonViewData"))
        End Get
        Set(ByVal value As VList(Of vw_SuspiciusPerson))
            Session("MsSuspiciusPersonViewData") = value
        End Set
    End Property
#End Region

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' fill search
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub FillSearch()
        Try
            Me.ComboSearch.Items.Add(New ListItem("...", ""))
            Me.ComboSearch.Items.Add(New ListItem("CIFNo", "CiFNO Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Name", "Nama Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Birth Date", "TanggalLahir = '-=Search=-'"))
            Me.ComboSearch.Items.Add(New ListItem("Birth Place", "TempatLahir Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Identity No", "NoIdentitas Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Address", "Alamat Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Kecamatan /Kelurahan", "KecamatanKeluarahan Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Phone No", "NoTelp Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Position", "Pekerjaan Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Company", "AlamatTempatKerja Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("NPWP", "NPWP Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Description", "Description Like '%-=Search=-%'"))

            Me.ComboSearch.Items.Add(New ListItem("Created By", "UseridCreator Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Case ID", "FK_CaseManagement_ID Like '%-=Search=-%'"))
        Catch
            Throw
        End Try
    End Sub

    Private Sub ClearThisPageSessions()
        Session("MsSuspiciusPersonViewSelected") = Nothing
        Session("MsSuspiciusPersonViewFieldSearch") = Nothing
        Session("MsSuspiciusPersonViewValueSearch") = Nothing
        Session("MsSuspiciusPersonViewSort") = Nothing
        Session("MsSuspiciusPersonViewCurrentPage") = Nothing
        Session("MsSuspiciusPersonViewRowTotal") = Nothing
        Session("MsSuspiciusPersonViewData") = Nothing
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not Me.IsPostBack Then
                Me.ClearThisPageSessions()
                Me.ComboSearch.Attributes.Add("onchange", "javascript:Check(this, 3, '" & Me.TextSearch.ClientID & "', '" & Me.popUp.ClientID & "');")

                Me.popUp.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TextSearch.ClientID & "'), 'dd-mmm-yyyy')")
                Me.popUp.Style.Add("display", "none")
                'Using AccessGroup As New AMLDAL.AMLDataSetTableAdapters.GroupTableAdapter

                'End Using
                Me.FillSearch()
                Me.GridMSUserView.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' page navigate button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
            Me.CollectSelected()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' bind grid
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Sub BindGrid()
        Try

            If ComboSearch.SelectedIndex <> 3 Then


                Dim strFilter As String = Me.SetnGetFieldSearch.Replace("-=Search=-", Me.SetnGetValueSearch.Replace("'", "''"))
                If strFilter.Length > 0 Then
                    strFilter = strFilter + " and UseridCreator IN (select UserId from UserWorkingUnitAssignment Where WorkingUnitId in (select WorkingUnitId from UserWorkingUnitAssignment where UserId ='" & Sahassa.AML.Commonly.SessionUserId & "' )) "
                Else
                    strFilter = strFilter + " UseridCreator IN (select UserId from UserWorkingUnitAssignment Where WorkingUnitId in (select WorkingUnitId from UserWorkingUnitAssignment where UserId ='" & Sahassa.AML.Commonly.SessionUserId & "' )) "
                End If


                Me.GridMSUserView.DataSource = Me.SetnGetBindTable(strFilter, Me.SetnGetSort, Me.SetnGetCurrentPage, Sahassa.AML.Commonly.GetDisplayedTotalRow)
            Else
                Dim datebirth As DateTime = Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", Me.SetnGetValueSearch.Replace("'", "''"))

                Dim strFilter As String = Me.SetnGetFieldSearch.Replace("-=Search=-", datebirth)
                If strFilter.Length > 0 Then
                    strFilter = strFilter + " and UseridCreator IN (select UserId from UserWorkingUnitAssignment Where WorkingUnitId in (select WorkingUnitId from UserWorkingUnitAssignment where UserId ='" & Sahassa.AML.Commonly.SessionUserId & "' )) "
                Else
                    strFilter = strFilter + " UseridCreator IN (select UserId from UserWorkingUnitAssignment Where WorkingUnitId in (select WorkingUnitId from UserWorkingUnitAssignment where UserId ='" & Sahassa.AML.Commonly.SessionUserId & "' )) "
                End If

                Me.GridMSUserView.DataSource = Me.SetnGetBindTable(strFilter, Me.SetnGetSort, Me.SetnGetCurrentPage, Sahassa.AML.Commonly.GetDisplayedTotalRow)
            End If


            'Me.GridMSUserView.CurrentPageIndex = Me.SetnGetCurrentPage
            Me.GridMSUserView.VirtualItemCount = Me.SetnGetRowTotal
            Me.GridMSUserView.DataBind()
        Catch
            Throw
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' set navigate info
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.ComboSearch.SelectedValue = Me.SetnGetFieldSearch
            Me.TextSearch.Text = Me.SetnGetValueSearch
        Catch
            Throw
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' change sort expression
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridMSUserView.SortCommand
        Dim GridUser As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' image button search
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Try
            CollectSelected()
            Me.SetnGetFieldSearch = Me.ComboSearch.SelectedValue
            If Me.ComboSearch.SelectedIndex = 0 Then
                Me.TextSearch.Text = ""
            ElseIf Me.ComboSearch.SelectedIndex = 3 Then
                If Not Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", TextSearch.Text.Trim) Then
                    Throw New Exception("Birth Date must dd-MMM-yyyy")
                End If
            End If
            Me.SetnGetValueSearch = Me.TextSearch.Text
            Me.SetnGetCurrentPage = 0
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' cek if groupID already edited
    ''' </summary>
    ''' <param name="strgroupID"></param>
    ''' <returns></returns>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Function IsPendingApproval(ByVal strgroupID As String) As Boolean


        Dim count As Int32 = AMLBLL.SuspiciusPersonBLL.CountPendingApproval(strgroupID)
        If count > 0 Then
            Return True
        Else
            Return False
        End If

    End Function

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' grid edit command handler
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridMSUserView.EditCommand
        Dim strgroupID As String = e.Item.Cells(1).Text

        Try
       
            If Me.IsPendingApproval(strgroupID) = False Then
                Sahassa.AML.Commonly.SessionIntendedPage = "SuspiciusPersonEdit.aspx?PKSuspiciusPersonID=" & strgroupID

                Me.Response.Redirect("SuspiciusPersonEdit.aspx?PKSuspiciusPersonID=" & strgroupID, False)
            Else
                Throw New Exception("Cannot edit the following Suspicius Person: '" & e.Item.Cells(2).Text & "' because it is currently waiting for approval.")
            End If

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    ''' <summary>
    ''' get row user
    ''' </summary>
    ''' <param name="strgroupID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    'Private Function GetRowGroup(ByVal strgroupID As String) As VList(Of vw_SuspiciusPerson)

    '    'Using AccessGroup As New AMLDAL.AMLDataSetTableAdapters.GroupTableAdapter

    '    Return AMLBLL.SuspiciusPersonBLL.GetVw_SuspiciusPerson

    '    'End Using
    'End Function

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' cek if groupID is being used by a user
    ''' </summary>
    ''' <param name="strgroupID"></param>
    ''' <returns></returns>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Function IsBeingUsedByUser(ByVal strgroupID As String) As Boolean
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.GroupTableAdapter
            Dim count As Int32 = AccessPending.CountGroupUsedByUser(strgroupID)
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        End Using
    End Function

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' grid delete event handler
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridMSUserView.DeleteCommand
        Dim strgroupID As String = e.Item.Cells(1).Text

        Try
            
            If Me.IsPendingApproval(strgroupID) = False Then
                Dim strCaseid As String = e.Item.Cells(15).Text
                If strCaseid.Replace("&nbsp;", "") = "" Then
                    Sahassa.AML.Commonly.SessionIntendedPage = "SuspiciusPersonDelete.aspx?PKSuspiciusPersonID=" & strgroupID

                    Me.Response.Redirect("SuspiciusPersonDelete.aspx?PKSuspiciusPersonID=" & strgroupID, False)
                Else
                    Throw New Exception("Cannot delete the following SuspiciusPerson: '" & e.Item.Cells(2).Text & "' because it is already have case id.")
                End If

              
            ElseIf Me.IsPendingApproval(strgroupID) = True Then
                Throw New Exception("Cannot delete the following SuspiciusPerson: '" & e.Item.Cells(2).Text & "' because it is currently waiting for approval.")

            End If

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' go button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	14/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be a positive number greater than zero.")
                End If
            Else
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If
            Me.CollectSelected()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    ''' <summary>
    ''' collect sub
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = gridRow.FindControl("CheckBoxExporttoExcel")
                Dim groupID As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
                If ArrTarget.Contains(groupID) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(groupID)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(groupID)
                End If
                Me.SetnGetSelectedItem = ArrTarget
            End If
        Next
    End Sub

    ''' <summary>
    ''' prerender event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    ''' <summary>
    ''' clear all control except control
    ''' </summary>
    ''' <param name="control">excluded control</param>
    ''' <remarks></remarks>
    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub 'ClearControls

    ''' <summary>
    ''' bind selected item
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub BindSelected()
        Try
            'Dim Rows As New ArrayList
            Dim strfilter As String = "0,"
            Dim rowData As VList(Of vw_SuspiciusPerson)
            For Each IdPk As Int32 In Me.SetnGetSelectedItem
                strfilter += IdPk & ","
            Next
            'If strfilter.Length > 0 Then
            strfilter = strfilter.Substring(0, strfilter.Length - 1)
            rowData = Me.SetnGetBindTable(vw_SuspiciusPersonColumn.PK_SuspiciusPerson_ID.ToString & " in (" & strfilter & ")", "", 0, Integer.MaxValue)

            Me.GridMSUserView.DataSource = rowData
            Me.GridMSUserView.AllowPaging = False
            Me.GridMSUserView.DataBind()

            'Sembunyikan kolom ke 0,1,4 & 5 agar tidak ikut diekspor ke excel
            Me.GridMSUserView.Columns(0).Visible = False
            Me.GridMSUserView.Columns(1).Visible = False
            Me.GridMSUserView.Columns(16).Visible = False
            Me.GridMSUserView.Columns(17).Visible = False
            Me.GridMSUserView.Columns(18).Visible = False

            'End If

        Catch
            Throw
        End Try
    End Sub

    ''' <summary>
    ''' export button 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Me.CollectSelected()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelected()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=CustomAlertView.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridMSUserView)
            GridMSUserView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get item bound
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridMSUserView.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Dim chkBox As CheckBox = e.Item.FindControl("CheckBoxExporttoExcel")
                chkBox.Checked = Me.SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)
                Dim struserid As String = e.Item.Cells(14).Text
                If struserid.ToLower = Sahassa.AML.Commonly.SessionUserId.ToLower Then
                    Dim strCaseid As String = e.Item.Cells(15).Text
                    If strCaseid.Replace("&nbsp;", "") <> "" Then
                        Dim objlnkCreateCase As LinkButton = CType(e.Item.FindControl("lnkCreateCase"), LinkButton)
                        objlnkCreateCase.Visible = False
                    End If
                Else
                    Dim objedit As LinkButton = CType(e.Item.FindControl("lnkEdit"), LinkButton)
                    Dim objDelete As LinkButton = CType(e.Item.FindControl("lnkdelete"), LinkButton)
                    Dim objlnkCreateCase As LinkButton = CType(e.Item.FindControl("lnkCreateCase"), LinkButton)
                    objedit.Visible = False
                    objDelete.Visible = False
                    objlnkCreateCase.Visible = False
                End If
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' select all
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(1).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItem.Contains(StrKey) = False Then
                        ArrTarget.Add(StrKey)
                    End If
                Else
                    If Me.SetnGetSelectedItem.Contains(StrKey) Then
                        ArrTarget.Remove(StrKey)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItem = ArrTarget
    End Sub

    Protected Sub LinkButtonAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonAddNew.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "SuspiciusPersonAdd.aspx"

        Me.Response.Redirect("SuspiciusPersonAdd.aspx", False)
    End Sub

    Protected Sub lnkCreateCase_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try


            Dim objdatagriditem As DataGridItem = CType(sender, LinkButton).Parent.Parent

            Dim pksuspicius As Integer = GridMSUserView.Items(objdatagriditem.ItemIndex).Cells(1).Text
            Response.Redirect("CreateCaseSuspiciusPerson.aspx?PKSuspiciusPersonId=" & pksuspicius, False)

        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Private Sub BindExportAll()
        Try
            Dim strFilter As String
            If ComboSearch.SelectedIndex <> 3 Then
                strFilter = Me.SetnGetFieldSearch.Replace("-=Search=-", Me.SetnGetValueSearch.Replace("'", "''"))
            Else
                Dim datebirth As DateTime = Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", Me.SetnGetValueSearch.Replace("'", "''"))
                strFilter = Me.SetnGetFieldSearch.Replace("-=Search=-", datebirth)
            End If

            If strFilter.Length > 0 Then
                strFilter = strFilter + " and UseridCreator IN (select UserId from UserWorkingUnitAssignment Where WorkingUnitId in (select WorkingUnitId from UserWorkingUnitAssignment where UserId ='" & Sahassa.AML.Commonly.SessionUserId & "' )) "
            Else
                strFilter = strFilter + " UseridCreator IN (select UserId from UserWorkingUnitAssignment Where WorkingUnitId in (select WorkingUnitId from UserWorkingUnitAssignment where UserId ='" & Sahassa.AML.Commonly.SessionUserId & "' )) "
            End If

            Me.GridMSUserView.DataSource = Me.SetnGetBindTable(strFilter, Me.SetnGetSort, 0, Int32.MaxValue)
            Me.GridMSUserView.AllowPaging = False
            Me.GridMSUserView.DataBind()

            'Sembunyikan kolom ke 0,1,4 & 5 agar tidak ikut diekspor ke excel
            Me.GridMSUserView.Columns(0).Visible = False
            Me.GridMSUserView.Columns(1).Visible = False
            Me.GridMSUserView.Columns(16).Visible = False
            Me.GridMSUserView.Columns(17).Visible = False
            Me.GridMSUserView.Columns(18).Visible = False
        Catch
            Throw
        End Try
    End Sub

    Protected Sub LnkBtnExportAllToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LnkBtnExportAllToExcel.Click
        Try
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindExportAll()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=CustomAlertView.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridMSUserView)
            GridMSUserView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class
