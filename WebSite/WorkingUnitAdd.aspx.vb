Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Partial Class WorkingUnitAdd
    Inherits Parent

    ''' <summary>
    ''' cancel handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "WorkingUnitView.aspx"

        Me.Response.Redirect("WorkingUnitView.aspx", False)
    End Sub

    ''' <summary>
    ''' fill group
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillGroupLevelType()
        Using AccessGroup As New AMLDAL.AMLDataSetTableAdapters.LevelTypeTableAdapter
            Me.DropDownLevelTypeName.DataSource = AccessGroup.GetData
            Me.DropDownLevelTypeName.DataTextField = "LevelTypeName"
            Me.DropDownLevelTypeName.DataValueField = "LevelTypeID"
            Me.DropDownLevelTypeName.DataBind()
        End Using
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillGroupWorkingUnitParent()
        Using AccessGroup As New AMLDAL.AMLDataSetTableAdapters.WorkingUnitTableAdapter
            Me.DropDownWorkingUnitParent.DataSource = AccessGroup.GetData
            Me.DropDownWorkingUnitParent.DataTextField = "WorkingUnitName"
            Me.DropDownWorkingUnitParent.DataValueField = "WorkingUnitID"
            Me.DropDownWorkingUnitParent.DataBind()
        End Using
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub InsertWorkingUnitBySU()
        Try
            Dim WorkingUnitDescription As String
            If Me.TextWorkingUnitDescription.Text.Length <= 255 Then
                WorkingUnitDescription = Me.TextWorkingUnitDescription.Text
            Else
                WorkingUnitDescription = Me.TextWorkingUnitDescription.Text.Substring(0, 255)
            End If

            Using AccessWorkingUnit As New AMLDAL.AMLDataSetTableAdapters.WorkingUnitTableAdapter
                AccessWorkingUnit.Insert(Me.TextWorkingUnitName.Text, CInt(Me.DropDownLevelTypeName.SelectedValue), CInt(Me.DropDownWorkingUnitParent.SelectedValue), WorkingUnitDescription, Now)
            End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub InsertAuditTrail()
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(5)

            Dim WorkingUnitDescription As String
            If Me.TextWorkingUnitDescription.Text.Length <= 255 Then
                WorkingUnitDescription = Me.TextWorkingUnitDescription.Text
            Else
                WorkingUnitDescription = Me.TextWorkingUnitDescription.Text.Substring(0, 255)
            End If

            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "WorkingUnitName", "Add", "", Me.TextWorkingUnitName.Text, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "LevelTypeID", "Add", "", Me.DropDownLevelTypeName.SelectedItem.Text, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "WorkingUnitParentID", "Add", "", Me.DropDownWorkingUnitParent.SelectedItem.Text, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "WorkingUnitDesc", "Add", "", WorkingUnitDescription, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "WorkingUnit", "WorkingUnitCreatedDate", "Add", "", Now.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
            End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    ''' <summary>
    ''' Save
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim oSQLTrans As SqlTransaction = Nothing
        Page.Validate()

        If Page.IsValid Then
            Try
                Dim WorkingUnitName As String = Trim(Me.TextWorkingUnitName.Text)

                'Periksa apakah Working Unit Name tersebut sudah ada dalam tabel WorkingUnit atau belum
                Using AccessWorkingUnit As New AMLDAL.AMLDataSetTableAdapters.WorkingUnitTableAdapter
                    Dim counter As Int32 = AccessWorkingUnit.CountMatchingWorkingUnit(WorkingUnitName)

                    'Counter = 0 berarti Working Unit Name tersebut belum pernah ada dalam Working Unit 
                    If counter = 0 Then
                        'Periksa apakah Working Unit Name tersebut sudah ada dalam tabel WorkingUnit_Approval atau belum
                        Using AccessWorkingUnitApproval As New AMLDAL.AMLDataSetTableAdapters.WorkingUnit_ApprovalTableAdapter
                            oSQLTrans = BeginTransaction(AccessWorkingUnitApproval, IsolationLevel.ReadUncommitted)
                            counter = AccessWorkingUnitApproval.CountWorkingUnitApprovalByWorkingUnitName(WorkingUnitName)

                            'Counter = 0 berarti WorkingUnit tersebut statusnya tidak dalam pending approval dan boleh ditambahkan dlm tabel WorkingUnit_Approval
                            If counter = 0 Then
                                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                                    Me.InsertAuditTrail()
                                    Me.InsertWorkingUnitBySU()
                                    Me.LblSuccess.Text = "Success to Insert Working Unit."
                                    Me.LblSuccess.Visible = True
                                Else
                                    Dim LevelTypeID As Int32 = Me.DropDownLevelTypeName.SelectedValue
                                    Dim WorkingUnitParentID As Int32 = Me.DropDownWorkingUnitParent.SelectedValue

                                    Dim WorkingUnitDescription As String
                                    If Me.TextWorkingUnitDescription.Text.Length <= 255 Then
                                        WorkingUnitDescription = Me.TextWorkingUnitDescription.Text
                                    Else
                                        WorkingUnitDescription = Me.TextWorkingUnitDescription.Text.Substring(0, 255)
                                    End If

                                    Dim WorkingUnitPendingApprovalID As Int64

                                    'Using TransScope As New Transactions.TransactionScope
                                    Using AccessWorkingUnitPendingApproval As New AMLDAL.AMLDataSetTableAdapters.WorkingUnit_PendingApprovalTableAdapter
                                        SetTransaction(AccessWorkingUnitPendingApproval, oSQLTrans)
                                        'Tambahkan ke dalam tabel News_PendingApproval dengan ModeID = 1 (Add) 
                                        WorkingUnitPendingApprovalID = AccessWorkingUnitPendingApproval.InsertWorkingUnit_PendingApproval(Sahassa.AML.Commonly.SessionUserId, Now, "WorkingUnit Add", 1)

                                        AccessWorkingUnitApproval.Insert(WorkingUnitPendingApprovalID, 1, Nothing, WorkingUnitName, LevelTypeID, WorkingUnitParentID, WorkingUnitDescription, Now, Nothing, Nothing, Nothing, Nothing, Nothing, Sahassa.AML.Commonly.GetDefaultDate)

                                        oSQLTrans.Commit()
                                        'TransScope.Complete()

                                        Dim MessagePendingID As Integer = 8501 'MessagePendingID 8501 = WorkingUnit Add 

                                        Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & WorkingUnitName

                                        Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & WorkingUnitName, False)
                                    End Using
                                    'End Using
                                End If
                            Else
                                Throw New Exception("Cannot add the following Working Unit: '" & WorkingUnitName & "' because it is currently waiting for approval.")
                            End If
                        End Using
                    Else
                        Throw New Exception("Cannot add the following Working Unit: '" & WorkingUnitName & "' because that Working Unit Name already exists in the database.")
                    End If
                End Using
            Catch ex As Exception
                If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
                Me.cvalPageError.IsValid = False
                Me.cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            Finally
                If Not oSQLTrans Is Nothing Then
                    oSQLTrans.Dispose()
                    oSQLTrans = Nothing
                End If
            End Try

        End If
    End Sub

    ''' <summary>
    ''' page load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.FillGroupLevelType()
                Me.FillGroupWorkingUnitParent()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    '    Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

End Class