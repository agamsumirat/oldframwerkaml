Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Partial Class VerificationListCategoryManagementApprovalDetail
    Inherits Parent

#Region " Property "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get confirm type
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property ParamType() As Sahassa.AML.Commonly.TypeConfirm
        Get
            Return Me.Request.Params("TypeConfirm")
        End Get
    End Property

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get param pk user management
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property ParamPkCategoryManagement() As Int64
        Get
            Return Me.Request.Params("CategoryID")
        End Get
    End Property

    ''' <summary>
    ''' get user id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetUserID() As String
        Get
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.VerificationListCategory_PendingApprovalTableAdapter
                Return AccessPending.SelectVerificationListCategory_PendingApprovalUserID(Me.ParamPkCategoryManagement)
            End Using
        End Get
    End Property
#End Region

#Region " Load "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' hide all
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub HideAll()
        Dim StrId As String = ""
        Select Case Me.ParamType
            Case Sahassa.AML.Commonly.TypeConfirm.VerificationListCategoryAdd
                StrId = "UserAdd"
            Case Sahassa.AML.Commonly.TypeConfirm.VerificationListCategoryEdit
                StrId = "UserEdi"
            Case Sahassa.AML.Commonly.TypeConfirm.VerificationListCategoryDelete
                StrId = "UserDel"
            Case Else
                Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
        End Select
        For Each ObjRow As HtmlControls.HtmlTableRow In Me.TableDetail.Rows
            ObjRow.Visible = ObjRow.ID = "Button11" Or ObjRow.ID.Substring(0, 7) = StrId
        Next
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load Category add
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub LoadCategoryAdd()
        Me.LabelTitle.Text = "Activity: Add Verification List Category"
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.VerificationListCategory_ApprovalTableAdapter
            Using ObjTable As Data.DataTable = AccessPending.GetVerificationListCategoryData(Me.ParamPkCategoryManagement)
                'Bila ObjTable.Rows.Count > 0 berarti VerificationListCategory tsb masih ada dlm tabel VerificationListCategory_Approval
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.VerificationListCategory_ApprovalRow = ObjTable.Rows(0)
                    Me.LabelCategoryNameAdd.Text = rowData.CategoryName
                    Me.TextCategoryDescriptionAdd.Text = rowData.CategoryDescription
                Else 'Bila ObjTable.Rows.Count = 0 berarti VerificationListCategory tsb sudah tidak lagi berada dlm tabel VerificationListCategory_Approval
                    Throw New Exception("Cannot load data from the following Verification List Category: " & Me.LabelCategoryNameAdd.Text & " because that Verification List Category is no longer in the approval table.")
                End If
            End Using
        End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load Category edit
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub LoadCategoryEdit()
        Me.LabelTitle.Text = "Activity: Edit Verification List Category"
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.VerificationListCategory_ApprovalTableAdapter
            Using ObjTable As Data.DataTable = AccessPending.GetVerificationListCategoryData(Me.ParamPkCategoryManagement)
                'Bila ObjTable.Rows.Count > 0 berarti VerificationListCategory tsb masih ada dlm tabel VerificationListCategory_Approval
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.VerificationListCategory_ApprovalRow = ObjTable.Rows(0)
                    Me.LabelCategoryIDEditNewCategoryID.Text = rowData.CategoryID
                    Me.LabelCategoryEditNewCategoryName.Text = rowData.CategoryName
                    Me.TextCategoryEditNewCategoryDescription.Text = rowData.CategoryDescription
                    Me.LabelCategoryIDEditOldCategoryID.Text = rowData.CategoryID_Old
                    Me.LabelCategoryNameEditOldCategoryName.Text = rowData.CategoryName_Old
                    Me.TextCategoryDescriptionEditOldCategoryDescription.Text = rowData.CategoryDescription_Old
                Else 'Bila ObjTable.Rows.Count = 0 berarti VerificationListCategory tsb sudah tidak lagi berada dlm tabel VerificationListCategory_Approval
                    Throw New Exception("Cannot load data from the following Verification List Category: " & Me.LabelCategoryNameEditOldCategoryName.Text & " because that Verification List Category is no longer in the approval table.")
                End If
            End Using
        End Using
    End Sub
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load Category delete
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub LoadCategoryDelete()
        Me.LabelTitle.Text = "Activity: Delete Verification List Category"
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.VerificationListCategory_ApprovalTableAdapter
            Using ObjTable As Data.DataTable = AccessPending.GetVerificationListCategoryData(Me.ParamPkCategoryManagement)
                'Bila ObjTable.Rows.Count > 0 berarti VerificationListCategory tsb masih ada dlm tabel VerificationListCategory_Approval
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.VerificationListCategory_ApprovalRow = ObjTable.Rows(0)
                    Me.LabelCategoryDeleteCategoryID.Text = rowData.CategoryID
                    Me.LabelCategoryDeleteCategoryName.Text = rowData.CategoryName
                    Me.TextCategoryDeleteCategoryDescription.Text = rowData.CategoryDescription
                Else 'Bila ObjTable.Rows.Count = 0 berarti VerificationListCategory tsb sudah tidak lagi berada dlm tabel VerificationListCategory_Approval
                    Throw New Exception("Cannot load data from the following Verification List Category: " & Me.LabelCategoryDeleteCategoryName.Text & " because that Verification List Category is no longer in the approval table.")
                End If
            End Using
        End Using
    End Sub
#End Region
#Region " Accept "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' Category add accept
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptCategoryAdd()
        Dim oSQLTrans As SqlTransaction = Nothing
        'Using TranScope As New Transactions.TransactionScope
        Using AccessCategory As New AMLDAL.AMLDataSetTableAdapters.VerificationListCategoryTableAdapter
            oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessCategory, Data.IsolationLevel.ReadUncommitted)
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.VerificationListCategory_ApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, oSQLTrans)
                Dim ObjTable As Data.DataTable = AccessPending.GetVerificationListCategoryData(Me.ParamPkCategoryManagement)

                'Bila ObjTable.Rows.Count > 0 berarti VerificationListCategory tsb masih ada dlm tabel VerificationListCategory_Approval
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.VerificationListCategory_ApprovalRow = ObjTable.Rows(0)

                    'Periksa apakah CategoryName yg baru tsb sudah ada dlm tabel VerificationListCategory atau belum. 
                    Dim counter As Int32 = AccessCategory.CountMatchingCategory(rowData.CategoryName)

                    'Bila counter = 0 berarti CategoryName tsb belum ada dlm tabel VerificationListCategory, maka boleh ditambahkan
                    If counter = 0 Then
                        'tambahkan item tersebut dalam tabel VerificationListCategory 
                        AccessCategory.Insert(rowData.CategoryName, rowData.CategoryDescription, rowData.CategoryCreatedDate)

                        'catat aktifitas dalam tabel Audit Trail
                        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationListCategory", "CategoryName", "Add", "", rowData.CategoryName, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationListCategory", "CategoryDescription", "Add", "", rowData.CategoryDescription, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationListCategory", "CategoryCreatedDate", "Add", "", rowData.CategoryCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")

                            'hapus item tersebut dalam tabel VerificationListCategory_Approval
                            AccessPending.DeleteVerificationListCategoryApproval(rowData.Category_PendingApprovalID)

                            'hapus item tersebut dalam tabel VerificationListCategory_PendingApproval
                            Using AccessPendingGroup As New AMLDAL.AMLDataSetTableAdapters.VerificationListCategory_PendingApprovalTableAdapter
                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPendingGroup, oSQLTrans)
                                AccessPendingGroup.DeleteVerificationListCategoryPendingApproval(rowData.Category_PendingApprovalID)
                            End Using

                            oSQLTrans.Commit()
                        End Using
                    Else 'Bila counter != 0 berarti CategoryName tsb sudah ada dlm tabel VerificationListCategory, maka AcceptCategoryAdd gagal
                        Throw New Exception("Cannot add the following Verification List Category: " & rowData.CategoryName & " because that Category Name already exists in the database.")
                    End If
                Else 'Bila ObjTable.Rows.Count = 0 berarti VerificationListCategory tsb sudah tidak lagi berada dlm tabel VerificationListCategory_Approval
                    Throw New Exception("Cannot add the following Verification List Category: " & Me.LabelCategoryNameAdd.Text & " because that Verification List Category is no longer in the approval table.")
                End If
            End Using
        End Using
        'End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' Category edit accept
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptCategoryEdit()
        Dim oSQLTrans As SqlTransaction = Nothing
        'Using TranScope As New Transactions.TransactionScope
        Using AccessCategory As New AMLDAL.AMLDataSetTableAdapters.VerificationListCategoryTableAdapter
            oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessCategory, Data.IsolationLevel.ReadUncommitted)
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.VerificationListCategory_ApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, oSQLTrans)
                Dim ObjTable As Data.DataTable = AccessPending.GetVerificationListCategoryData(Me.ParamPkCategoryManagement)

                'Bila ObjTable.Rows.Count > 0 berarti VerificationListCategory tsb masih ada dlm tabel VerificationListCategory_Approval
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.VerificationListCategory_ApprovalRow = ObjTable.Rows(0)

                    'Periksa apakah CategoryName yg baru tsb sudah ada dlm tabel Group atau belum. 
                    Dim counter As Int32 = AccessCategory.CountMatchingCategory(rowData.CategoryName)

                    'Bila tidak ada perubahan CategoryName
                    If rowData.CategoryName = rowData.CategoryName_Old Then
                        GoTo Edit
                    Else 'Bila ada perubahan CategoryName
                        'Counter = 0 berarti CategoryName tersebut tidak ada dalam tabel VerificationListCategory
                        If counter = 0 Then
Edit:
                            'update item tersebut dalam tabel VerificationListCategory 
                            AccessCategory.UpdateVerificationListCategory(rowData.CategoryID, rowData.CategoryName, rowData.CategoryDescription)

                            'catat aktifitas dalam tabel Audit Trail
                            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationListCategory", "CategoryName", "Edit", rowData.CategoryName_Old, rowData.CategoryName, "Accepted")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationListCategory", "CategoryDescription", "Edit", rowData.CategoryDescription_Old, rowData.CategoryDescription, "Accepted")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationListCategory", "CategoryID", "Edit", rowData.CategoryID_Old, rowData.CategoryID, "Accepted")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationListCategory", "CategoryCreatedDate", "Edit", rowData.CategoryCreatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), rowData.CategoryCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")

                                'hapus item tersebut dalam tabel Verification List Category_Approval
                                AccessPending.DeleteVerificationListCategoryApproval(rowData.Category_PendingApprovalID)

                                'hapus item tersebut dalam tabel Verification List Category_PendingApproval
                                Using AccessPendingGroup As New AMLDAL.AMLDataSetTableAdapters.VerificationListCategory_PendingApprovalTableAdapter
                                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPendingGroup, oSQLTrans)
                                    AccessPendingGroup.DeleteVerificationListCategoryPendingApproval(rowData.Category_PendingApprovalID)
                                End Using
                                oSQLTrans.Commit()
                            End Using
                        Else 'Bila counter != 0 berarti CategoryName tsb telah ada dlm tabel Group, maka AcceptCategoryEdit gagal
                            Throw New Exception("Cannot change to the following Category Name: " & rowData.CategoryName & " because that Category Name already exists in the database.")
                        End If
                    End If
                Else 'Bila ObjTable.Rows.Count = 0 berarti VerificationListCategory tsb sudah tidak lagi berada dlm tabel VerificationListCategory_Approval
                    Throw New Exception("Cannot edit the following Verification List Category: " & Me.LabelCategoryNameEditOldCategoryName.Text & " because that Verification List Category is no longer in the approval table.")
                End If
            End Using
        End Using
        'End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' Category delete accept
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptCategoryDelete()
        Dim oSQLTrans As SqlTransaction = Nothing
        'Using TranScope As New Transactions.TransactionScope
        Using AccessCategory As New AMLDAL.AMLDataSetTableAdapters.VerificationListCategoryTableAdapter
            oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessCategory, Data.IsolationLevel.ReadUncommitted)
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.VerificationListCategory_ApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, oSQLTrans)
                Dim ObjTable As Data.DataTable = AccessPending.GetVerificationListCategoryData(Me.ParamPkCategoryManagement)

                'Bila ObjTable.Rows.Count > 0 berarti VerificationListCategory tsb masih ada dlm tabel VerificationListCategory_Approval
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.VerificationListCategory_ApprovalRow = ObjTable.Rows(0)

                    'Periksa apakah VerificationListCategory yg hendak didelete tsb ada dlm tabelnya atau tidak. 
                    Dim counter As Int32 = AccessCategory.CountMatchingCategory(rowData.CategoryName)

                    'Bila counter = 0 berarti VerificationListCategory tsb tidak ada dlm tabel VerificationListCategory, maka AcceptCategoryDelete gagal
                    If counter = 0 Then
                        Throw New Exception("Cannot delete the following Verification List Category: " & rowData.CategoryName & " because that Verification List Category does not exist in the database anymore.")
                    Else 'Bila counter != 0, maka VerificationListCategory tsb bisa didelete
                        'hapus item tersebut dari tabel VerificationListCategory 
                        AccessCategory.DeleteVerificationListCategory(rowData.CategoryID)

                        'catat aktifitas dalam tabel Audit Trail
                        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationListCategory", "CategoryName", "Delete", rowData.CategoryName_Old, rowData.CategoryName, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationListCategory", "CategoryDescription", "Delete", rowData.CategoryDescription_Old, rowData.CategoryDescription, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationListCategory", "CategoryID", "Delete", rowData.CategoryID_Old, rowData.CategoryID, "Accepted")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationListCategory", "CategoryCreatedDate", "Delete", rowData.CategoryCreatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), rowData.CategoryCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")

                            'hapus item tersebut dalam tabel VerificationListCategory_Approval
                            AccessPending.DeleteVerificationListCategoryApproval(rowData.Category_PendingApprovalID)

                            'hapus item tersebut dalam tabel VerificationListCategory_PendingApproval
                            Using AccessPendingGroup As New AMLDAL.AMLDataSetTableAdapters.VerificationListCategory_PendingApprovalTableAdapter
                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPendingGroup, oSQLTrans)
                                AccessPendingGroup.DeleteVerificationListCategoryPendingApproval(rowData.Category_PendingApprovalID)
                            End Using

                            oSQLTrans.Commit()
                        End Using
                    End If
                Else 'Bila ObjTable.Rows.Count = 0 berarti VerificationListCategory tsb sudah tidak lagi berada dlm tabel VerificationListCategory_Approval
                    Throw New Exception("Cannot delete the following Verification List Category: " & Me.LabelCategoryDeleteCategoryName.Text & " because that Verification List Category is no longer in the approval table.")
                End If
            End Using
        End Using
        'End Using
    End Sub

#End Region
#Region " Reject "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' reject Category add
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectCategoryAdd()
        Dim oSQLTrans As SqlTransaction = Nothing
        'Using TranScope As New Transactions.TransactionScope
        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
            oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAudit, Data.IsolationLevel.ReadUncommitted)
            Using AccessCategory As New AMLDAL.AMLDataSetTableAdapters.VerificationListCategory_ApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessCategory, oSQLTrans)
                Using AccessCategoryPending As New AMLDAL.AMLDataSetTableAdapters.VerificationListCategory_PendingApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessCategoryPending, oSQLTrans)
                    Using TableCategory As Data.DataTable = AccessCategory.GetVerificationListCategoryData(Me.ParamPkCategoryManagement)
                        'Bila TableCategory.Rows.Count > 0 berarti VerificationListCategory tsb masih ada dlm tabel VerificationListCategory_Approval
                        If TableCategory.Rows.Count > 0 Then
                            Dim ObjRow As AMLDAL.AMLDataSet.VerificationListCategory_ApprovalRow = TableCategory.Rows(0)

                            'catat aktifitas dalam tabel Audit Trail
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationListCategory", "CategoryCreatedDate", "Add", "", ObjRow.CategoryCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationListCategory", "CategoryName", "Add", "", ObjRow.CategoryName, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationListCategory", "CategoryDescription", "Add", "", ObjRow.CategoryDescription, "Rejected")

                            'hapus item tersebut dalam tabel VerificationListCategory_Approval
                            AccessCategory.DeleteVerificationListCategoryApproval(Me.ParamPkCategoryManagement)

                            'hapus item tersebut dalam tabel VerificationListCategory_PendingApproval
                            AccessCategoryPending.DeleteVerificationListCategoryPendingApproval(Me.ParamPkCategoryManagement)
                            oSQLTrans.Commit()
                        Else 'Bila TableCategory.Rows.Count = 0 berarti VerificationListCategory tsb sudah tidak lagi berada dlm tabel VerificationListCategory_Approval
                            Throw New Exception("Operation failed. The following Verification List Category: " & Me.LabelCategoryNameAdd.Text & " is no longer in the approval table.")
                        End If
                    End Using
                End Using
            End Using
        End Using
        'End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' reject Category edit
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectCategoryEdit()
        Dim oSQLTrans As SqlTransaction = Nothing

        'Using TranScope As New Transactions.TransactionScope
        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
            oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAudit, Data.IsolationLevel.ReadUncommitted)
            Using AccessCategory As New AMLDAL.AMLDataSetTableAdapters.VerificationListCategory_ApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessCategory, oSQLTrans)
                Using AccessCategoryPending As New AMLDAL.AMLDataSetTableAdapters.VerificationListCategory_PendingApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessCategoryPending, oSQLTrans)
                    Using TableCategory As Data.DataTable = AccessCategory.GetVerificationListCategoryData(Me.ParamPkCategoryManagement)
                        'Bila TableCategory.Rows.Count > 0 berarti VerificationListCategory tsb masih ada dlm tabel Group_Approval
                        If TableCategory.Rows.Count > 0 Then
                            Dim ObjRow As AMLDAL.AMLDataSet.VerificationListCategory_ApprovalRow = TableCategory.Rows(0)

                            'catat aktifitas dalam tabel Audit Trail
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationListCategory", "CategoryName", "Edit", ObjRow.CategoryName_Old, ObjRow.CategoryName, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationListCategory", "CategoryDescription", "Edit", ObjRow.CategoryDescription_Old, ObjRow.CategoryDescription, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationListCategory", "CategoryID", "Edit", ObjRow.CategoryID_Old, ObjRow.CategoryID, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationListCategory", "CategoryCreatedDate", "Edit", ObjRow.CategoryCreatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), ObjRow.CategoryCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")

                            'hapus item tersebut dalam tabel VerificationListCategory_Approval
                            AccessCategory.DeleteVerificationListCategoryApproval(Me.ParamPkCategoryManagement)

                            'hapus item tersebut dalam tabel VerificationListCategory_PendingApproval
                            AccessCategoryPending.DeleteVerificationListCategoryPendingApproval(Me.ParamPkCategoryManagement)
                            oSQLTrans.Commit()
                        Else 'Bila TableGroup.Rows.Count = 0 berarti Group tsb sudah tidak lagi berada dlm tabel VerificationListCategory_PendingApproval
                            Throw New Exception("Operation failed. The following Verification List Category: " & Me.LabelCategoryNameEditOldCategoryName.Text & " is no longer in the approval table.")
                        End If
                    End Using
                End Using
            End Using
        End Using
        'End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' reject Category delete
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectCategoryDelete()
        Dim oSQLTrans As SqlTransaction = Nothing
        'Using TranScope As New Transactions.TransactionScope
        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
            oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAudit, Data.IsolationLevel.ReadUncommitted)
            Using AccessCategory As New AMLDAL.AMLDataSetTableAdapters.VerificationListCategory_ApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessCategory, oSQLTrans)
                Using AccessCategoryPending As New AMLDAL.AMLDataSetTableAdapters.VerificationListCategory_PendingApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessCategoryPending, oSQLTrans)
                    Using TableCategory As Data.DataTable = AccessCategory.GetVerificationListCategoryData(Me.ParamPkCategoryManagement)
                        'Bila TableCategory.Rows.Count > 0 berarti VerificationListCategory tsb masih ada dlm tabel VerificationListCategory_Approval
                        If TableCategory.Rows.Count > 0 Then
                            Dim ObjRow As AMLDAL.AMLDataSet.VerificationListCategory_ApprovalRow = TableCategory.Rows(0)

                            'catat aktifitas dalam tabel Audit Trail
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationListCategory", "CategoryName", "Delete", ObjRow.CategoryName_Old, ObjRow.CategoryName, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationListCategory", "CategoryDescription", "Delete", ObjRow.CategoryDescription_Old, ObjRow.CategoryDescription, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationListCategory", "CategoryID", "Delete", ObjRow.CategoryID_Old, ObjRow.CategoryID, "Rejected")
                            AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "VerificationListCategory", "CategoryCreatedDate", "Delete", ObjRow.CategoryCreatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), ObjRow.CategoryCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")

                            'hapus item tersebut dalam tabel VerificationListCategory_Approval
                            AccessCategory.DeleteVerificationListCategoryApproval(Me.ParamPkCategoryManagement)

                            'hapus item tersebut dalam tabel VerificationListCategory_PendingApproval
                            AccessCategoryPending.DeleteVerificationListCategoryPendingApproval(Me.ParamPkCategoryManagement)
                            oSQLTrans.Commit()
                        Else 'Bila TableCategory.Rows.Count = 0 berarti VerificationListCategory tsb sudah tidak lagi berada dlm tabel VerificationListCategory_Approval
                            Throw New Exception("Operation failed. The following Verification List Category: " & Me.LabelCategoryDeleteCategoryName.Text & " is no longer in the approval table.")
                        End If
                    End Using
                End Using
            End Using
        End Using
        'End Using
    End Sub
#End Region

    ''' <summary>
    ''' page load event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.ImageAccept.Attributes.Add("onclick", "javascript: if(confirm('Do you want to accept this activity ?')== false) return false;")
                Me.ImageReject.Attributes.Add("onclick", "javascript: if(confirm('Do you want to reject this activity ?')== false) return false;")
                Me.HideAll()
                Select Case Me.ParamType
                    Case Sahassa.AML.Commonly.TypeConfirm.VerificationListCategoryAdd
                        Me.LoadCategoryAdd()
                    Case Sahassa.AML.Commonly.TypeConfirm.VerificationListCategoryEdit
                        Me.LoadCategoryEdit()
                    Case Sahassa.AML.Commonly.TypeConfirm.VerificationListCategoryDelete
                        Me.LoadCategoryDelete()
                    Case Else
                        Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
                End Select

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' accept button handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click
        Try
            Select Case Me.ParamType
                Case Sahassa.AML.Commonly.TypeConfirm.VerificationListCategoryAdd
                    Me.AcceptCategoryAdd()
                Case Sahassa.AML.Commonly.TypeConfirm.VerificationListCategoryEdit
                    Me.AcceptCategoryEdit()
                Case Sahassa.AML.Commonly.TypeConfirm.VerificationListCategoryDelete
                    Me.AcceptCategoryDelete()
                Case Else
                    Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
            End Select

            Sahassa.AML.Commonly.SessionIntendedPage = "VerificationListCategoryManagementApproval.aspx"

            Me.Response.Redirect("VerificationListCategoryManagementApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' reject button handlert
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click
        Try
            Select Case Me.ParamType
                Case Sahassa.AML.Commonly.TypeConfirm.VerificationListCategoryAdd
                    Me.RejectCategoryAdd()
                Case Sahassa.AML.Commonly.TypeConfirm.VerificationListCategoryEdit
                    Me.RejectCategoryEdit()
                Case Sahassa.AML.Commonly.TypeConfirm.VerificationListCategoryDelete
                    Me.RejectCategoryDelete()
                Case Else
                    Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
            End Select

            Sahassa.AML.Commonly.SessionIntendedPage = "VerificationListCategoryManagementApproval.aspx"

            Me.Response.Redirect("VerificationListCategoryManagementApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' back button handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "VerificationListCategoryManagementApproval.aspx"

        Me.Response.Redirect("VerificationListCategoryManagementApproval.aspx", False)
    End Sub
End Class