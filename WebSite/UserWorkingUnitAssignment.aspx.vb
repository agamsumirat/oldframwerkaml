Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Public Class UserWorkingUnitAssignment
    Inherits Parent

    Public ArrWorkingUnit_Old As New ArrayList
    Public ArrIdUserWorking_PendingApproval As New ArrayList

#Region " Property "
    ''' <summary>
    ''' get focus id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property GetFocusID() As String
        Get
            Return Me.ButtonAdd.ClientID
        End Get
    End Property

    Public ReadOnly Property GetUserId() As String
        Get
            Return Me.Request.Params("UserID")
        End Get
    End Property

    Public Property SetAndGetWorkingUnitOld() As ArrayList
        Get
            Return ViewState("WorkingUnit_Old")
        End Get
        Set(ByVal value As ArrayList)
            ViewState("WorkingUnit_Old") = value
        End Set
    End Property
#End Region

#Region "Insert , Check Audit Trail"
    Private Function InsertAuditTrail(ByVal mode As String) As Boolean
        Try
            Using AccessUserWorkingUnitAssignment As New AMLDAL.AMLDataSetTableAdapters.SelectUserWorkingUnitAssignmentTableAdapter
                Using AccessAuditTrail As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                    If mode.ToLower = "add" Then
                        Dim countAuditTrail As Integer = 4 * Me.DropDownListWorkingUnit.Items.Count
                        Sahassa.AML.AuditTrailAlert.AuditTrailChecking(countAuditTrail)
                        For Each item As ListItem In Me.DropDownListWorkingUnit.Items
                            AccessAuditTrail.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - UserWorkingUnitAssignment", "UserID", "Add", "", Me.LabelUserID.Text, "")
                            AccessAuditTrail.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - UserWorkingUnitAssignment", "WorkingUnitID", "Add", "", item.Text, "")
                            AccessAuditTrail.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - UserWorkingUnitAssignment", "CreatedDate", "Add", "", Now.ToString("dd-MMMM-yyyy HH:mm"), "")
                            AccessAuditTrail.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - UserWorkingUnitAssignment", "LasUpdateDate", "Add", "", Now.ToString("dd-MMMM-yyyy HH:mm"), "")
                        Next
                    Else
                        Using dt As AMLDAL.AMLDataSet.SelectUserWorkingUnitAssignmentDataTable = AccessUserWorkingUnitAssignment.GetData(Me.LabelUserID.Text)
                            If dt.Rows.Count > 0 Then
                                Dim countAuditTrail As Integer = 4 * (dt.Rows.Count + Me.ListBoxWorkingUnits.Items.Count)
                                Sahassa.AML.AuditTrailAlert.AuditTrailChecking(countAuditTrail)
                                ' new value
                                For Each item As ListItem In ListBoxWorkingUnits.Items
                                    AccessAuditTrail.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - UserWorkingUnitAssignment", "UserID", mode, Me.LabelUserID.Text, Me.LabelUserID.Text, "")
                                    AccessAuditTrail.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - UserWorkingUnitAssignment", "WorkingUnitID", mode, "", item.Text, "")
                                    AccessAuditTrail.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - UserWorkingUnitAssignment", "CreatedDate", mode, "", Now.ToString("dd-MMMM-yyyy HH:mm"), "")
                                    AccessAuditTrail.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - UserWorkingUnitAssignment", "LasUpdateDate", mode, "", Now.ToString("dd-MMMM-yyyy HH:mm"), "")
                                Next
                                ' old
                                Using AccessWorkingUnit As New AMLDAL.AMLDataSetTableAdapters.WorkingUnitTableAdapter
                                    For Each row As AMLDAL.AMLDataSet.SelectUserWorkingUnitAssignmentRow In dt.Rows
                                        Using dtWorkingUnit As AMLDAL.AMLDataSet.WorkingUnitDataTable = AccessWorkingUnit.GetDataByWorkingUnitID(row.WorkingUnitID)
                                            Dim RowWU As AMLDAL.AMLDataSet.WorkingUnitRow = dtWorkingUnit.Rows(0)
                                            AccessAuditTrail.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - UserWorkingUnitAssignment", "UserID", mode, Me.LabelUserID.Text, Me.LabelUserID.Text, "")
                                            AccessAuditTrail.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - UserWorkingUnitAssignment", "WorkingUnitID", mode, "", RowWU.WorkingUnitName, "")
                                            AccessAuditTrail.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - UserWorkingUnitAssignment", "CreatedDate", mode, "", Now.ToString("dd-MMMM-yyyy HH:mm"), "")
                                            AccessAuditTrail.Insert(Now, Me.GetUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - UserWorkingUnitAssignment", "LasUpdateDate", mode, "", Now.ToString("dd-MMMM-yyyy HH:mm"), "")
                                        End Using
                                    Next
                                End Using
                            End If
                        End Using
                    End If
                End Using
            End Using
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
#End Region

    ''' <summary>
    ''' fill group
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillGroupWorkingUnit()
        Using AccessGroup As New AMLDAL.AMLDataSetTableAdapters.WorkingUnitTableAdapter
            'Bind DropDownListCC webcontrol
            Me.DropDownListWorkingUnit.DataSource = AccessGroup.GetData
            Me.DropDownListWorkingUnit.DataTextField = "WorkingUnitName"
            Me.DropDownListWorkingUnit.DataValueField = "WorkingUnitID"
            Me.DropDownListWorkingUnit.DataBind()
        End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get data login parameter
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	03/07/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GetData()
        Try
            Me.LabelUserID.Text = Me.GetUserId
            Using AccessCekStatusUserId As New AMLDAL.AMLDataSetTableAdapters.CekUserWorkingUnitAssignmentTableAdapter
                Dim BoolStatusUserId As Boolean = AccessCekStatusUserId.Cek(Me.LabelUserID.Text)
                If BoolStatusUserId Then
                    Using AccessGetUserWorkingAssignment As New AMLDAL.AMLDataSetTableAdapters.SelectUserWorkingUnitAssignmentTableAdapter
                        Using DtUserWorkingAssignment As AMLDAL.AMLDataSet.SelectUserWorkingUnitAssignmentDataTable = AccessGetUserWorkingAssignment.GetData(Me.LabelUserID.Text)
                            For Each Row As AMLDAL.AMLDataSet.SelectUserWorkingUnitAssignmentRow In DtUserWorkingAssignment.Rows
                                Dim item As New ListItem(Row.WorkingUnitName, Row.WorkingUnitID)
                                Me.ListBoxWorkingUnits.Items.Add(item)
                                ViewState("CreateDate") = CType(Row.CreatedDate, Date)
                                ViewState("LastUpdateDate") = Today
                            Next
                        End Using
                    End Using
                Else
                    ViewState("CreateDate") = Today
                    ViewState("LastUpdateDate") = Today
                End If
            End Using
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load page handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not Me.CekIsApproval(Me.GetUserId) Then
                If Not Me.IsPostBack Then
                    Me.FillGroupWorkingUnit()
                    Me.GetData()

                    Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                    'Using Transcope As New Transactions.TransactionScope
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                        Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                        AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                        'Transcope.Complete()
                    End Using
                    'End Using
                End If
            Else
                Throw New Exception("Cannot edit the following User ID: '" & Me.LabelUserID.Text & "' because it is currently waiting for approval")

            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' cancel event handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	09/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "UserWorkingUnitAssignmentView.aspx"

        Me.Response.Redirect("UserWorkingUnitAssignmentView.aspx", False)
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' save handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>    
    ''' </remarks>
    ''' <history>
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageSave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            If Not Me.CekIsApproval(Me.LabelUserID.Text) Then
                Page.Validate()
                If Page.IsValid Then
                    If Sahassa.AML.Commonly.SessionPkUserId <> 1 Then
                        'Using TranScope As New Transactions.TransactionScope
                        '1. insert ke dalam table approval user working assigment
                        Using AccessInsertUserAssignmentWorkingUnitApproval As New AMLDAL.AMLDataSetTableAdapters.InsertUserWorkingUnitAssignmentApprovalTableAdapter
                            oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessInsertUserAssignmentWorkingUnitApproval, Data.IsolationLevel.ReadUncommitted)
                            Dim IntCountNewUserWorkingUnits As Integer = Me.ListBoxWorkingUnits.Items.Count
                            'Dim IntCountOldUserWorkingUnits As Integer = Me.SetAndGetWorkingUnitOld.Count
                            Dim IntUserWorkingApprovlId As Int64
                            Dim IntPendingApprovalId As Integer
                            Dim TmpArrNewWorkingUnit, TmpArrDeleteWorkingUnit As New ArrayList
                            Dim FlagExist As Boolean = 0

                            If Me.ListBoxWorkingUnits.Items.Count = 0 Then
                                Using AccessGetUserWorkingAssignment As New AMLDAL.AMLDataSetTableAdapters.SelectUserWorkingUnitAssignmentTableAdapter
                                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessGetUserWorkingAssignment, oSQLTrans)
                                    Using DtUserWorkingAssignment As AMLDAL.AMLDataSet.SelectUserWorkingUnitAssignmentDataTable = AccessGetUserWorkingAssignment.GetData(Me.LabelUserID.Text)
                                        For Each Row As AMLDAL.AMLDataSet.SelectUserWorkingUnitAssignmentRow In DtUserWorkingAssignment.Rows

                                            Dim dtInsert As AMLDAL.AMLDataSet.InsertUserWorkingUnitAssignmentApprovalDataTable = AccessInsertUserAssignmentWorkingUnitApproval.InsertUserWorkingUnitAssignmentApproval(Me.GetUserId, Integer.Parse(Row.WorkingUnitID), ViewState("CreateDate"), Today, Me.GetUserId, 0, ViewState("CreateDate"), ViewState("LastUpdateDate"), Sahassa.AML.Commonly.SessionUserId)
                                            IntUserWorkingApprovlId = CInt(dtInsert.Rows(0).Item(0))

                                            ' insert parameter_pending approval
                                            Using AccessInsertParameter_PendingApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_PendingApprovalTableAdapter
                                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessInsertParameter_PendingApproval, oSQLTrans)
                                                IntPendingApprovalId = AccessInsertParameter_PendingApproval.InsertParameters_PendingApproval(Sahassa.AML.Commonly.SessionUserId, ViewState("CreateDate"), 3)
                                            End Using

                                            ' Insert Parameter Approval
                                            Using AccessParameterApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_ApprovalTableAdapter
                                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessParameterApproval, oSQLTrans)
                                                AccessParameterApproval.InsertParameters_Approval(IntPendingApprovalId, 3, 8, IntUserWorkingApprovlId)
                                            End Using

                                        Next
                                    End Using
                                End Using
                            Else
                                ' new 
                                For j As Integer = 0 To Me.ListBoxWorkingUnits.Items.Count - 1
                                    Dim dtInsert As AMLDAL.AMLDataSet.InsertUserWorkingUnitAssignmentApprovalDataTable = AccessInsertUserAssignmentWorkingUnitApproval.InsertUserWorkingUnitAssignmentApproval(Me.GetUserId, Integer.Parse(Me.ListBoxWorkingUnits.Items(j).Value), ViewState("CreateDate"), Today, Me.GetUserId, 0, ViewState("CreateDate"), ViewState("LastUpdateDate"), Sahassa.AML.Commonly.SessionUserId)
                                    IntUserWorkingApprovlId = CInt(dtInsert.Rows(0).Item(0))

                                    ' insert parameter_pending approval
                                    Using AccessInsertParameter_PendingApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_PendingApprovalTableAdapter
                                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessInsertParameter_PendingApproval, oSQLTrans)
                                        IntPendingApprovalId = AccessInsertParameter_PendingApproval.InsertParameters_PendingApproval(Sahassa.AML.Commonly.SessionUserId, ViewState("CreateDate"), 2)
                                    End Using

                                    ' Insert Parameter Approval
                                    Using AccessParameterApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_ApprovalTableAdapter
                                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessParameterApproval, oSQLTrans)
                                        AccessParameterApproval.InsertParameters_Approval(IntPendingApprovalId, 2, 8, IntUserWorkingApprovlId)
                                    End Using
                                Next
                            End If
                        End Using
                        oSQLTrans.Commit()

                        Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=8862&Identifier=" & Me.LabelUserID.Text

                        Me.Response.Redirect("MessagePending.aspx?MessagePendingID=8862&Identifier=" & Me.LabelUserID.Text, False)
                        'End Using

                    Else
                        Using AccessUWUA As New AMLDAL.AMLDataSetTableAdapters.SelectUserWorkingUnitAssignmentTableAdapter
                            Using dt As AMLDAL.AMLDataSet.SelectUserWorkingUnitAssignmentDataTable = AccessUWUA.GetData(Me.LabelUserID.Text)
                                If dt.Rows.Count > 0 Then
                                    Me.InsertAuditTrail("Edit")
                                    Using DeleteUWUA As New AMLDAL.AMLDataSetTableAdapters.UserWorkingUnitAssignmentTableAdapter
                                        DeleteUWUA.DeleteUserWorkingUnitByUserID(Me.LabelUserID.Text)
                                    End Using
                                    Using InsertUWUA As New AMLDAL.AMLDataSetTableAdapters.UserWorkingUnitAssignmentTableAdapter
                                        For Each item As ListItem In ListBoxWorkingUnits.Items
                                            InsertUWUA.Insert(Me.LabelUserID.Text, item.Value, Now, Now)
                                        Next
                                    End Using
                                    'change status
                                    Using AccessChangeStatus As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                                        If Me.ListBoxWorkingUnits.Items.Count <> 0 Then
                                            AccessChangeStatus.UpdateStatusUser(Me.LabelUserID.Text, CBool(Boolean.TrueString))
                                        Else
                                            AccessChangeStatus.UpdateStatusUser(Me.LabelUserID.Text, CBool(Boolean.FalseString))
                                        End If
                                    End Using

                                    Me.LblSucces.Visible = True
                                    Me.LblSucces.Text = "Success to Update User Working Unit Assignment."
                                Else ' add
                                    Me.InsertAuditTrail("Add")
                                    Using InsertUWUA As New AMLDAL.AMLDataSetTableAdapters.UserWorkingUnitAssignmentTableAdapter
                                        For Each item As ListItem In ListBoxWorkingUnits.Items
                                            InsertUWUA.Insert(Me.LabelUserID.Text, item.Value, Now, Now)
                                        Next
                                        Me.LblSucces.Visible = True
                                        Me.LblSucces.Text = "Success to Insert User Working Unit Assignment."
                                    End Using
                                    Using AccessChangeStatus As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                                        If Me.ListBoxWorkingUnits.Items.Count <> 0 Then
                                            AccessChangeStatus.UpdateStatusUser(Me.LabelUserID.Text, CBool(Boolean.TrueString))
                                        Else
                                            AccessChangeStatus.UpdateStatusUser(Me.LabelUserID.Text, CBool(Boolean.FalseString))
                                        End If
                                    End Using
                                End If
                            End Using
                        End Using

                    End If
                End If
            Else
                Throw New Exception("Cannot edit the following User ID: '" & Me.LabelUserID.Text & "' because it is currently waiting for approval")
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    ''' <summary>
    ''' Add Working Unit
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ButtonAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        Try
            Dim item As ListItem = Me.DropDownListWorkingUnit.SelectedItem

            Dim add As Boolean = True

            'Cari apakah Working Unit yg dipilih sdh pernah ditambahkan pd webcontrol ListBoxWorkingUnits atau belum
            For Each item_ As ListItem In Me.ListBoxWorkingUnits.Items
                If item.Equals(item_) = True Then
                    add = False
                End If
            Next

            'Bila Working Unit tsb belum pernah dipilih maka tambahkan pd webcontrol ListBoxWorkingUnits
            If add = True Then
                Me.ListBoxWorkingUnits.Items.Add(item)
            Else 'Bila Working Unit tsb sudah pernah ditambahkan maka tampilkan pesan error
                Throw New Exception("The following Working Unit: " & item.Text & " has been added before, please choose another Working Unit.")
            End If

        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    ''' <summary>
    ''' Remove Working Unit
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ButtonRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonRemove.Click
        Try
            Dim item As New ListItemCollection

            'Cari Working Unit2 yang dipilih user untuk dihapus, lalu tambahkan Working Unit2 yang tidak dihapus ke dalam ListItemCollection 
            For Each item_ As ListItem In Me.ListBoxWorkingUnits.Items
                If Not item_.Selected Then
                    item.Add(item_)
                End If
            Next

            'Kosongkan isi dari webcontrol ListBoxWorkingUnits
            Me.ListBoxWorkingUnits.Items.Clear()

            'Tambahkan Working Unit2 yang tidak dihapus kembali ke dalam webcontrol ListBoxWorkingUnits
            For Each item_ As ListItem In item
                Me.ListBoxWorkingUnits.Items.Add(item_)
            Next

        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    ''' <summary>
    ''' cek is approval
    ''' </summary>
    ''' <param name="strUserId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function CekIsApproval(ByVal strUserId As String) As Boolean
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.CountUserWorkingUnitAssignmentApprovalByUserIdTableAdapter
            Dim count As Int32 = AccessPending.GetData(strUserId).Rows(0)("jml")
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        End Using
    End Function
End Class