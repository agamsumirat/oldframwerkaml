﻿Partial Class CaseManagementViewByCIF
    Inherits Parent
#Region " Property "
    Private Property SearchCustomerName() As String
        Get
            If Not Session("CaseManagementViewByCIFCustomerName") Is Nothing Then
                Return Session("CaseManagementViewByCIFCustomerName")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("CaseManagementViewByCIFCustomerName") = value
        End Set
    End Property
    Private Property SearchCIFNo() As String
        Get
            If Not Session("CaseManagementViewByCIFSearchCIFNo") Is Nothing Then
                Return Session("CaseManagementViewByCIFSearchCIFNo")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("CaseManagementViewByCIFSearchCIFNo") = value
        End Set
    End Property
    Private Property SearchTotal() As String
        Get
            If Not Session("CaseManagementViewByCIFSearchTotal") Is Nothing Then
                Return Session("CaseManagementViewByCIFSearchTotal")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("CaseManagementViewByCIFSearchTotal") = value
        End Set
    End Property

    Private Property SearchAging() As String
        Get
            If Not Session("CaseManagementViewByCIFAging") Is Nothing Then
                Return Session("CaseManagementViewByCIFAging")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("CaseManagementViewByCIFAging") = value
        End Set
    End Property

    Private Property Searchamlrisk() As String
        Get
            If Not Session("CaseManagementViewByCIFAMLRISK") Is Nothing Then
                Return Session("CaseManagementViewByCIFAMLRISK")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Session("CaseManagementViewByCIFAMLRISK") = value
        End Set
    End Property

    ''' <summary>
    ''' selected item store
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("CaseManagementViewByCIFSelected") Is Nothing, New ArrayList, Session("CaseManagementViewByCIFSelected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("CaseManagementViewByCIFSelected") = value
        End Set
    End Property


    ''' <summary>
    ''' sort expresion
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("CaseManagementViewByCIFSort") Is Nothing, " aging desc", Session("CaseManagementViewByCIFSort"))
        End Get
        Set(ByVal Value As String)
            Session("CaseManagementViewByCIFSort") = Value
        End Set
    End Property
    ''' <summary>
    ''' current page index
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("CaseManagementViewByCIFCurrentPage") Is Nothing, 0, Session("CaseManagementViewByCIFCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("CaseManagementViewByCIFCurrentPage") = Value
        End Set
    End Property
    ''' <summary>
    ''' total pages
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    ''' <summary>
    ''' row total
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("CaseManagementViewByCIFRowTotal") Is Nothing, 0, Session("CaseManagementViewByCIFRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("CaseManagementViewByCIFRowTotal") = Value
        End Set
    End Property
    Private ReadOnly Property Tables() As String
        Get
            Dim StrQuery As New StringBuilder

            StrQuery.Append(" CaseByCIF" & Sahassa.AML.Commonly.SessionPkUserId)
            'StrQuery.Append(" CaseManagement INNER JOIN CaseStatus ON CaseManagement.FK_CaseStatusID = CaseStatus.CaseStatusId INNER JOIN AccountOwner ON CaseManagement.FK_AccountOwnerID = AccountOwner.AccountOwnerId LEFT OUTER JOIN CaseManagementProposedAction ON CaseManagement.FK_LastProposedStatusID = CaseManagementProposedAction.PK_CaseManagementProposedActionID ")
            'StrQuery.Append(" INNER JOIN CaseManagementWorkflow ON CaseManagement.FK_AccountOwnerId=CaseManagementWorkflow.AccountOwnerId ")
            'StrQuery.Append(" INNER JOIN CaseManagementWorkflowDetail ON CaseManagementWorkflow.PK_CMW_ID=CaseManagementWorkflowDetail.FK_CMW_ID ")
            'StrQuery.Append(" INNER JOIN MapCaseManagementWorkflowHistory mcmwh ON mcmwh.FK_CaseManagementId=casemanagement.PK_CaseManagementID ")
            Return StrQuery.ToString()
        End Get
    End Property
    Private ReadOnly Property PK() As String
        Get
            Return "CaseByCIF" & Sahassa.AML.Commonly.SessionPkUserId & ".CIFNo"
        End Get
    End Property
    Private ReadOnly Property Fields() As String

        Get


            Dim strField As New StringBuilder
            strField.Append("CIFNo,CustomerName,total,Aging,amlrisk")
            'strField.Append("CaseManagement.CIFNo,CaseManagement.CustomerName,(SELECT COUNT(1) FROM CaseManagement cm WHERE cm.CIFNo=CaseManagement.CIFNo and  dbo.ufn_GetSerialNoLastWorkflow(cm.PK_CaseManagementID)<=dbo.ufn_GetSerialNoLastWorkflowByUserID(cm.PK_CaseManagementID," & Sahassa.AML.Commonly.SessionPkUserId & ") and cm.FK_CaseStatusID in (1,2)) AS Total, ")
            'strField.Append("(SELECT MAX(ab.Aging)FROM CaseManagement ab WHERE ab.CIFNo= CaseManagement.CIFNo) AS Aging")

            Return strField.ToString
        End Get
    End Property

    Private ReadOnly Property GroupBy() As String
        Get
            Dim strField As New StringBuilder
            strField.Append("")
            'strField.Append("CaseStatus.CaseStatusDescription, CaseManagement.WorkflowStep, CaseManagement.CreatedDate, CaseManagement.LastUpdated,")

            ''update PIC
            ''strField.Append("CaseManagement.FK_AccountOwnerID, AccountOwner.AccountOwnerName, CAST(CaseManagement.FK_AccountOwnerID AS VARCHAR) + ' - ' + AccountOwner.AccountOwnerName, CaseManagement.PIC, CaseManagement.HasOpenIssue,")
            'strField.Append("CaseManagement.FK_AccountOwnerID, AccountOwner.AccountOwnerName, CAST(CaseManagement.FK_AccountOwnerID AS VARCHAR) + ' - ' + AccountOwner.AccountOwnerName, mcmwh.userid, CaseManagement.HasOpenIssue,")

            'strField.Append("CaseManagement.Aging, CaseManagement.IsReportedtoRegulator, CaseManagement.PPATKConfirmationno,")
            'strField.Append("CaseManagementProposedAction.ProposedAction")
            Return strField.ToString
        End Get
    End Property


    Function IsValidSearchAging(ByVal straging As String) As Boolean

        Try
            If straging.Split(" and ").Length = 3 Then
                If IsNumeric(straging.Split(" and ")(0)) AndAlso IsNumeric(straging.Split(" and ")(2)) Then
                    Dim filterfrom As Integer = straging.Split(" and ")(0)
                    Dim filterto As Integer = straging.Split(" and ")(2)

                    If filterfrom < filterto Then
                        Return True
                    Else
                        Throw New Exception("Filter Aging To must Greater than Filter Aging From")
                    End If

                End If
            Else
                Return False
            End If
        Catch ex As Exception

        End Try
        Return True
    End Function

    Private ReadOnly Property SetnGetBindTableAll() As Data.DataTable
        Get
            ' Return IIf(Session("CaseManagementViewByCIFData") Is Nothing, New AMLDAL.CaseManagement.CaseManagementDataTable, Session("CaseManagementViewByCIFData"))

            Dim strWhereClause(-1) As String
            Dim strAllWhereClause As String = ""
            If Me.SetnGetCurrentPage > Me.GetPageTotal - 1 And Me.GetPageTotal - 1 > 0 Then
                Me.SetnGetCurrentPage = Me.GetPageTotal - 1
            End If
            If SearchCustomerName.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "CustomerName like'%" & SearchCustomerName.Replace("'", "''") & "%' "
            End If
            If SearchCIFNo.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "CIFNo like'%" & SearchCIFNo.Replace("'", "''") & "%' "
            End If
            If SearchTotal.Trim.Length > 0 Then
                If IsNumeric(SearchTotal) Then
                    ReDim Preserve strWhereClause(strWhereClause.Length)
                    strWhereClause(strWhereClause.Length - 1) = "total = " & SearchTotal.Replace("'", "''").Trim & " "

                    'Session("Total") = "Having Count(1) = " & SearchTotal.Replace("'", "''") & " "
                Else
                    Throw New Exception("Total must be numeric.")
                End If
            End If
            If SearchAging.Trim.Length > 0 Then
                If IsValidSearchAging(SearchAging) Then
                    ReDim Preserve strWhereClause(strWhereClause.Length)
                    strWhereClause(strWhereClause.Length - 1) = "Aging between " & SearchAging.Replace("'", "''") & " "
                Else
                    Throw New Exception("Aging must be numeric.")
                End If
            End If

            If Searchamlrisk.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "amlrisk like'%" & Searchamlrisk.Replace("'", "''") & "%' "
            End If


            strAllWhereClause = String.Join(" and ", strWhereClause)

            If strAllWhereClause.Trim = "" Then
                strAllWhereClause += " total>0"
            Else
                strAllWhereClause += " and total>0"
            End If

            Session("strAllWhereClause") = strAllWhereClause
            Me.SetnGetRowTotal = Sahassa.AML.Commonly.GetTableCountDistinctByColumn(Me.Tables, strAllWhereClause, Me.PK)
            Dim pageSize As Long = Integer.MaxValue
            'If Sahassa.AML.Commonly.SessionPagingLimit = "" Then
            '    pageSize = 10
            'Else
            '    pageSize = Sahassa.AML.Commonly.SessionPagingLimit
            'End If
            'Return Sahassa.AML.Commonly.GetDatasetPaging(Me.Tables, Me.PK, Me.SetnGetSort, Me.SetnGetCurrentPage + 1, pageSize, Me.Fields, strAllWhereClause, Me.GroupBy)
            Dim strsql As String = ""
            strsql = "select " & Me.Fields & " from " & Me.Tables & " where " & strAllWhereClause & " order by " & Me.SetnGetSort


            Return SahassaNettier.Data.DataRepository.Provider.ExecuteDataSet(Sahassa.AML.Commonly.GetSQLCommand(strsql)).Tables(0)
        End Get

    End Property

    ''' <summary>
    ''' save bind table
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property SetnGetBindTable() As Data.DataTable
        Get
            ' Return IIf(Session("CaseManagementViewByCIFData") Is Nothing, New AMLDAL.CaseManagement.CaseManagementDataTable, Session("CaseManagementViewByCIFData"))

            Dim strWhereClause(-1) As String
            Dim strAllWhereClause As String = ""
            If Me.SetnGetCurrentPage > Me.GetPageTotal - 1 And Me.GetPageTotal - 1 > 0 Then
                Me.SetnGetCurrentPage = Me.GetPageTotal - 1
            End If
            If SearchCustomerName.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "CustomerName like'%" & SearchCustomerName.Replace("'", "''") & "%' "
            End If
            If SearchCIFNo.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "CIFNo like'%" & SearchCIFNo.Replace("'", "''") & "%' "
            End If
            If SearchTotal.Trim.Length > 0 Then
                If IsNumeric(SearchTotal) Then
                    ReDim Preserve strWhereClause(strWhereClause.Length)
                    strWhereClause(strWhereClause.Length - 1) = "total = " & SearchTotal.Replace("'", "''").Trim & " "

                    'Session("Total") = "Having Count(1) = " & SearchTotal.Replace("'", "''") & " "
                Else
                    Throw New Exception("Total must be numeric.")
                End If
            End If
            If SearchAging.Trim.Length > 0 Then
                If IsValidSearchAging(SearchAging) Then
                    ReDim Preserve strWhereClause(strWhereClause.Length)
                    strWhereClause(strWhereClause.Length - 1) = "Aging between " & SearchAging.Replace("'", "''") & " "
                Else
                    Throw New Exception("Aging must be numeric.")
                End If
            End If


            If Searchamlrisk.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "amlrisk like'%" & Searchamlrisk.Replace("'", "''") & "%' "
            End If
            strAllWhereClause = String.Join(" and ", strWhereClause)

            If strAllWhereClause.Trim = "" Then
                strAllWhereClause += " total>0"
            Else
                strAllWhereClause += " and total>0"
            End If

            Me.SetnGetRowTotal = Sahassa.AML.Commonly.GetTableCountDistinctByColumn(Me.Tables, strAllWhereClause, Me.PK)
            Dim pageSize As Long
            If Sahassa.AML.Commonly.SessionPagingLimit = "" Then
                pageSize = 10
            Else
                pageSize = Sahassa.AML.Commonly.SessionPagingLimit
            End If
            Session("strAllWhereClause") = strAllWhereClause
            Return Sahassa.AML.Commonly.GetDatasetPaging(Me.Tables, Me.PK, Me.SetnGetSort, Me.SetnGetCurrentPage + 1, pageSize, Me.Fields, strAllWhereClause, Me.GroupBy + Session("Total"))

        End Get

    End Property
#End Region




    Private Sub ClearThisPageSessions()

        Session("CaseManagementViewByCIFSearchTotal") = Nothing
        Session("CaseManagementSearchSameCaseCount") = Nothing
        Session("CaseManagementViewByCIFSelected") = Nothing
        Session("CaseManagementViewByCIFFieldSearch") = Nothing
        Session("CaseManagementViewByCIFValueSearch") = Nothing
        Session("CaseManagementViewByCIFSort") = Nothing
        Session("CaseManagementViewByCIFCurrentPage") = Nothing
        Session("CaseManagementViewByCIFRowTotal") = Nothing
        Session("CaseManagementViewByCIFData") = Nothing
        Session("CaseManagementViewByCIFRMCode") = Nothing
        Session("CaseManagementViewByCIFCustomerName") = Nothing
        Searchamlrisk = Nothing
        Me.SearchAging = Nothing
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' [Ariwibawa] 05/06/2006 Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not Me.IsPostBack Then

                If Request.Params("clear") = "" Then
                    Me.ClearThisPageSessions()
                End If

                Me.GridMSUserView.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                Using objCommand As System.Data.SqlClient.SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("usp_GenerateSTRByCIF")
                    objCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PkuserID", Sahassa.AML.Commonly.SessionPkUserId))
                    SahassaNettier.Data.DataRepository.Provider.ExecuteNonQuery(objCommand)
                End Using


                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                    If Request.Params("msg") <> "" Then
                        Page.ClientScript.RegisterStartupScript(Me.GetType, Guid.NewGuid.ToString, "  window.alert(' " & Request.Params("msg") & ".')", True)
                    End If

                End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' page navigate button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' [Ariwibawa] 05/06/2006 Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
            Me.CollectSelected()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' bind grid
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' [Ariwibawa] 05/06/2006 Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Sub BindGrid()
        Try
            SettingControlSearch()
            Me.GridMSUserView.DataSource = Me.SetnGetBindTable
            'Me.GridMSUserView.CurrentPageIndex = Me.SetnGetCurrentPage
            Me.GridMSUserView.VirtualItemCount = Me.SetnGetRowTotal
            Me.GridMSUserView.DataBind()
        Catch
            Throw
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' set navigate info
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' [Ariwibawa] 05/06/2006 Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0


        Catch
            Throw
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' change sort expression
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' [Ariwibawa] 05/06/2006 Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridMSUserView.SortCommand
        Dim GridUser As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' image button search
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' [Ariwibawa] 05/06/2006 Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Try
            Me.CollectSelected()
            Me.SettingPropertySearch()
            Me.SetnGetCurrentPage = 0
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' grid edit command handler
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' [Ariwibawa] 05/06/2006 Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridMSUserView.EditCommand
        Dim strCIFNo As String = e.Item.Cells(2).Text
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "CaseManagementViewByCIFDetail.aspx?CIFNo=" & strCIFNo
            Me.Response.Redirect("CaseManagementViewByCIFDetail.aspx?CIFNo=" & strCIFNo, False)
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub


    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' go button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' [Ariwibawa] 14/06/2006 Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            Me.CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be a positive number greater than zero.")
                End If
            Else
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    ''' <summary>
    ''' collect sub
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = gridRow.FindControl("CheckBoxExporttoExcel")
                Dim PKID As String = gridRow.Cells(2).Text
                Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
                If ArrTarget.Contains(PKID) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(PKID)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(PKID)
                End If
                Me.SetnGetSelectedItem = ArrTarget
            End If
        Next
    End Sub

    ''' <summary>
    ''' prerender event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try

            Me.BindGrid()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    ''' <summary>
    ''' clear all control except control
    ''' </summary>
    ''' <param name="control">excluded control</param>
    ''' <remarks></remarks>
    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub 'ClearControls

    ''' <summary>
    ''' bind selected item
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub BindSelected()
        Try
            Dim listPK As New System.Collections.Generic.List(Of String)
            listPK.Add("'0'")
            For Each IdPk As String In Me.SetnGetSelectedItem
                listPK.Add("'" & IdPk.ToString & "'")
            Next

            Me.GridMSUserView.DataSource = Sahassa.AML.Commonly.GetDatasetPaging(Me.Tables, Me.PK, Me.SetnGetSort, 0, Int32.MaxValue, Me.Fields, Session("strAllWhereClause") + " and CIFNo IN (" & String.Join(",", listPK.ToArray) & ")", Me.GroupBy + Session("Total"))
            Me.GridMSUserView.AllowPaging = False
            Me.GridMSUserView.DataBind()

            'Sembunyikan kolom ke 0,1,4 & 5 agar tidak ikut diekspor ke excel
            Me.GridMSUserView.Columns(0).Visible = False
            Me.GridMSUserView.Columns(5).Visible = False
        Catch ex As Exception
            Throw ex
        End Try

    End Sub
    ''' <summary>
    ''' export button
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Me.CollectSelected()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelected()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=CaseManagementViewByCIF.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridMSUserView)
            GridMSUserView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get item bound
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' [Ariwibawa] 05/06/2006 Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridMSUserView.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Dim chkBox As CheckBox = e.Item.FindControl("CheckBoxExporttoExcel")
                chkBox.Checked = Me.SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)

                Dim objdt As Data.DataRowView = CType(e.Item.DataItem, Data.DataRowView)
                If objdt.Item("total").ToString = "0" Then
                    Dim objlinkbutton As LinkButton = CType(e.Item.FindControl("linkdetail"), LinkButton)
                    objlinkbutton.Enabled = False
                End If
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' select all
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(1).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItem.Contains(StrKey) = False Then
                        ArrTarget.Add(StrKey)
                    End If
                Else
                    If Me.SetnGetSelectedItem.Contains(StrKey) Then
                        ArrTarget.Remove(StrKey)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItem = ArrTarget
    End Sub


    ''' <summary>
    ''' Setting Property Searching
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub SettingPropertySearch()
        Me.SearchCustomerName = txtCustomerName.Text.Trim
        Me.SearchCIFNo = txtCIFNo.Text.Trim
        Me.SearchTotal = txtTotal.Text.Trim
        If txtAgingFrom.Text.Trim.Length > 0 And txtAgingTo.Text.Trim.Length > 0 Then
            Me.SearchAging = txtAgingFrom.Text.Trim & " and " & txtAgingTo.Text.Trim
        Else
            Me.SearchAging = ""
        End If

        Me.Searchamlrisk = TxtAMLRisk.Text.Trim
    End Sub
    Private Sub SettingControlSearch()
        txtCustomerName.Text = Me.SearchCustomerName
        txtCIFNo.Text = Me.SearchCIFNo
        txtTotal.Text = Me.SearchTotal
        TxtAMLRisk.Text = Me.Searchamlrisk
        Try
            txtAgingFrom.Text = Me.SearchAging.Split(" and ")(0)
            txtAgingTo.Text = Me.SearchAging.Split(" and ")(2)
        Catch ex As Exception

        End Try

        Session("Total") = Nothing
    End Sub

    Protected Sub ImageClearSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageClearSearch.Click
        txtCustomerName.Text = ""
        txtCIFNo.Text = ""
        txtTotal.Text = ""
        txtAgingFrom.Text = ""
        txtAgingTo.Text = ""
        TxtAMLRisk.Text = ""
        Session("Total") = Nothing
        Me.SettingPropertySearch()
    End Sub


    Sub BindExportAll()
        SettingControlSearch()
        GridMSUserView.PageSize = Integer.MaxValue
        Me.GridMSUserView.DataSource = Me.SetnGetBindTableAll
        Me.GridMSUserView.DataBind()
        Me.GridMSUserView.Columns(0).Visible = False
        Me.GridMSUserView.Columns(5).Visible = False
    End Sub

    Protected Sub LnkBtnExportAllToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LnkBtnExportAllToExcel.Click

        Try

            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindExportAll()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=CaseManagementALL.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridMSUserView)
            GridMSUserView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
End Class '1033