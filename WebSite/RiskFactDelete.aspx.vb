Imports System.Data.SqlClient
Partial Class RiskFactDelete
    Inherits Parent

    Private ReadOnly Property RiskFactID() As Integer
        Get
            Dim temp As String
            temp = Request.Params("RiskFactID")
            If Not IsNumeric(temp) OrElse temp = "" Then
                Throw New Exception("Risk Fact ID not valid")
            Else
                Return temp
            End If
        End Get
    End Property
    Public ReadOnly Property oRowRiskFact() As AMLDAL.RiskFact.RiskFactRow
        Get
            Using adapter As New AMLDAL.RiskFactTableAdapters.RiskFactTableAdapter
                If adapter.GetRiskFactByRiskFactID(Me.RiskFactID).Rows.Count > 0 Then
                    Return adapter.GetRiskFactByRiskFactID(Me.RiskFactID).Rows(0)
                Else
                    Return Nothing
                End If

            End Using
        End Get
    End Property
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "RiskFactView.aspx"

        Me.Response.Redirect("RiskFactView.aspx", False)

    End Sub
#Region "Delete RiskFact Rules"
    Private Sub DeleteRiskFactBySU()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try

            'Using oTransScope As New Transactions.TransactionScope
            Using adapter As New AMLDAL.RiskFactTableAdapters.RiskFactTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(adapter, Data.IsolationLevel.ReadUncommitted)
                adapter.DeleteRiskFactByPK(Me.RiskFactID)

                InsertAuditTrailDelete(oSQLTrans)
                oSQLTrans.Commit()
                Response.Redirect("RiskFactView.aspx", False)
            End Using

        Catch tex As Threading.ThreadAbortException
            ' ignore
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Rollback()
            End If
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub
    Private Sub InsertAuditTrailDelete(ByRef oSQLTrans As SqlTransaction)
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(5)

            If Not Me.oRowRiskFact Is Nothing Then
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Risk Fact", "Risk Fact Name", "Delete", "", Me.oRowRiskFact.RiskFactName, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Risk Fact", "Description", "Delete", "", Me.oRowRiskFact.RiskFactDescription, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Risk Fact", "Enabled", "Delete", "", Me.oRowRiskFact.Enable, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Risk Fact", "Risk Score", "Delete", "", Me.oRowRiskFact.RiskScore, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Risk Fact", "Expression", "Delete", "", Me.oRowRiskFact.RiskFactExpression, "Accepted")


                End Using
            End If
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' menyimpan data risk fact ke pending approval
    ''' </summary>
    ''' <remarks>
    ''' Step
    ''' 1.insert header
    ''' 2.insert detail
    ''' </remarks>
    Private Sub InsertDataRiskFactToPendingApproval()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try

            Dim intHeader As Integer

            Using adapter As New AMLDAL.RiskFactTableAdapters.RiskFactPendingApprovalTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(adapter, Data.IsolationLevel.ReadUncommitted)
                intHeader = adapter.InsertRiskFactPendingApproval(oRowRiskFact.RiskFactName, Sahassa.AML.Commonly.SessionUserId, 3, "Risk Fact Delete", Now)


                Using adapter1 As New AMLDAL.RiskFactTableAdapters.RiskFactApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(adapter1, oSQLTrans)
                    adapter1.Insert(intHeader, oRowRiskFact.Pk_RiskFact_ID, oRowRiskFact.RiskFactName, oRowRiskFact.RiskFactDescription, oRowRiskFact.RiskFactExpression, oRowRiskFact.RiskScore, oRowRiskFact.Enable, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
                End Using
                oSQLTrans.Commit()

            End Using
            Dim MessagePendingID As Integer = 81503 'MessagePendingID 81503 = Risk Fact Delete

            Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & oRowRiskFact.RiskFactName
            Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & oRowRiskFact.RiskFactName, False)
        Catch tex As Threading.ThreadAbortException
            'ignore
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Rollback()
            End If
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub
#End Region
    ''' <summary>
    ''' 1.cek data riskfact Masih ada ngak di table
    ''' 2.cek data riskfactngak boleh ada di table approval
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function DataValidToDelete() As Boolean
        Try
            'Step 1
            If oRowRiskFact Is Nothing Then
                Try
                    Throw New Exception("Cannot delete Risk Fact Name because that Risk Fact name does not exist in the database anymore. ")
                Catch ex As Exception
                    cvalPageError.IsValid = False
                    cvalPageError.ErrorMessage = ex.Message
                    Return False
                End Try
            End If
            'Step 2
            Using adapter As New AMLDAL.RiskFactTableAdapters.RiskFactPendingApprovalTableAdapter
                If adapter.CountRiskFactPendingApprovalByUniqueKey(oRowRiskFact.RiskFactName) > 0 Then
                    Try
                        Throw New Exception("Cannot delete the following Risk Fact : " & oRowRiskFact.RiskFactName & " Name because it is currently waiting for approval.")
                    Catch ex As Exception
                        cvalPageError.IsValid = False
                        cvalPageError.ErrorMessage = ex.Message
                        Return False
                    End Try
                End If
            End Using

            Return True
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub LoadData()
        Try
            If Not oRowRiskFact Is Nothing Then
                Me.LabelRiskFactName.Text = oRowRiskFact.RiskFactName
                Me.LabelRiskFactDescription.Text = oRowRiskFact.RiskFactDescription
                Me.LabelEnabled.Text = oRowRiskFact.Enable.ToString
                Me.LabelRiskScore.Text = oRowRiskFact.RiskScore
                Me.LabelExpression.Text = oRowRiskFact.RiskFactExpression
            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' Delete 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click

        Try
            If Page.IsValid Then
                If DataValidToDelete Then
                    If Sahassa.AML.Commonly.SessionUserId.ToLower = "superuser" Then
                        DeleteRiskFactBySU()
                    Else
                        InsertDataRiskFactToPendingApproval()
                    End If
                End If
            End If
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                LoadData()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)


                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
            End If
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class
