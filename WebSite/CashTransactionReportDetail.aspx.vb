Imports System.Data.SqlClient
Partial Class CashTransactionReportDetail
    Inherits Parent

#Region " Property "
    Private ReadOnly Property PK_CTRID() As String
        Get
            Dim StrPK_CTRID As String
            StrPK_CTRID = Request.Params("PK_CTRID")
            If StrPK_CTRID = "" Then
                Throw New Exception("PK_CTRID is not valid")
            Else
                Return StrPK_CTRID
            End If
        End Get
    End Property

    Private ReadOnly Property GetAccountOwner() As String
        Get
            Return Request.Params("AccountOwner")
        End Get
    End Property

    Private ReadOnly Property GetCIFNo() As String
        Get
            Return Request.Params("CIFNo")
        End Get
    End Property

    Private ReadOnly Property GetCustomerName() As String
        Get
            Return Request.Params("CustomerName")
        End Get
    End Property

    Private ReadOnly Property GetTotalTransactionValue() As String
        Get
            Return Request.Params("TotalTransactionValue")
        End Get
    End Property

    Private ReadOnly Property GetCTRDescription() As String
        Get
            Return Request.Params("CTRDescription")
        End Get
    End Property


    Private Property SetnGetSelectedItemCreateCase() As ArrayList
        Get
            Return IIf(Session("SetnGetSelectedItemCreateCaseSelected") Is Nothing, New ArrayList, Session("SetnGetSelectedItemCreateCaseSelected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("SetnGetSelectedItemCreateCaseSelected") = value
        End Set
    End Property

    Private Property SetnGetSelectedItemFilter() As ArrayList
        Get
            Return IIf(Session("CashTransactionReportDetailSelected") Is Nothing, New ArrayList, Session("CashTransactionReportDetailSelected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("CashTransactionReportDetailSelected") = value
        End Set
    End Property

    Private Property SetnGetFieldSearch() As String
        Get
            Return IIf(Session("CashTransactionReportDetailFieldSearch") Is Nothing, "", Session("CashTransactionReportDetailFieldSearch"))
        End Get
        Set(ByVal Value As String)
            Session("CashTransactionReportDetailFieldSearch") = Value
        End Set
    End Property

    Private Property SetnGetValueSearch() As String
        Get
            Return IIf(Session("CashTransactionReportDetailValueSearch") Is Nothing, "", Session("CashTransactionReportDetailValueSearch"))
        End Get
        Set(ByVal Value As String)
            Session("CashTransactionReportDetailValueSearch") = Value
        End Set
    End Property

    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("CashTransactionReportDetailSort") Is Nothing, "CTRTransaction.PK_CTRTransactionID  asc", Session("CashTransactionReportDetailSort"))
        End Get
        Set(ByVal Value As String)
            Session("CashTransactionReportDetailSort") = Value
        End Set
    End Property

    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("CashTransactionReportDetailCurrentPage") Is Nothing, 0, Session("CashTransactionReportDetailCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("CashTransactionReportDetailCurrentPage") = Value
        End Set
    End Property

    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property

    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("CashTransactionReportDetailRowTotal") Is Nothing, 0, Session("CashTransactionReportDetailRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("CashTransactionReportDetailRowTotal") = Value
        End Set
    End Property

    Private Property SetnGetBindTable() As Data.DataTable
        Get
            Return IIf(Session("CashTransactionReportDetailData") Is Nothing, Sahassa.AML.Commonly.GetDatasetPaging(Me.Tables, Me.PK, Me.SetnGetSort, Me.SetnGetCurrentPage + 1, Me.PageSize, Me.Fields, Me.SetnGetValueSearch, ""), Session("CashTransactionReportDetailData"))
        End Get
        Set(ByVal value As Data.DataTable)
            Session("CashTransactionReportDetailData") = value
        End Set
    End Property

    Private ReadOnly Property Tables() As String
        Get
            Dim StrQuery As New StringBuilder
            StrQuery.Append("CTRTransaction ")
            StrQuery.Append("INNER JOIN TransactionChannelType ON CTRTransaction.TransactionChannelType = TransactionChannelType.PK_TransactionChannelType ")
            StrQuery.Append("LEFT JOIN JHCOUN ON CTRTransaction.CountryCode=JHCOUN.JHCCOC ")
            StrQuery.Append("INNER JOIN vw_AuxTransactionCode ON CTRTransaction.AuxiliaryTransactionCode=vw_AuxTransactionCode.TransactionCode ")
            StrQuery.Append("INNER JOIN AccountOwner ON CTRTransaction.AccountOwnerId=AccountOwner.AccountOwnerId ")

            Return StrQuery.ToString()
        End Get
    End Property

    Private ReadOnly Property PK() As String
        Get
            Return "CTRTransaction.PK_CTRTransactionID"
        End Get
    End Property

    Private ReadOnly Property Fields() As String
        Get
            Dim StrQuery As New StringBuilder
            StrQuery.Append("CTRTransaction.PK_CTRTransactionID, CTRTransaction.TransactionDate, ")
            StrQuery.Append("CAST(CTRTransaction.AccountOwnerId AS VARCHAR)  + ' - ' + AccountOwner.AccountOwnerName AS AccountOwner, ")
            StrQuery.Append("TransactionChannelType.TransactionChannelTypeName, CTRTransaction.CountryCode + ' - ' + ISNULL(JHCOUN.JHCNAM,'') AS Country, ")
            StrQuery.Append("CTRTransaction.BankCode, CTRTransaction.BankName, ")
            StrQuery.Append("CTRTransaction.CIFNo, CTRTransaction.AccountNo, CTRTransaction.AccountName, ")
            StrQuery.Append("CTRTransaction.AuxiliaryTransactionCode + ' - ' + vw_AuxTransactionCode.TransactionDescription AS AuxiliaryTransactionCode, ")
            StrQuery.Append("CTRTransaction.TransactionAmount, CTRTransaction.CurrencyType, ")
            StrQuery.Append("CTRTransaction.DebitORCredit, CTRTransaction.MemoRemark, ")
            StrQuery.Append("CTRTransaction.TransactionRemark, CTRTransaction.TransactionExchangeRate, ")
            StrQuery.Append("CTRTransaction.TransactionLocalEquivalent, CTRTransaction.TransactionTicketNo, ")
            StrQuery.Append("CTRTransaction.UserId, ")
            StrQuery.Append("CASE WHEN (SELECT TOP 1 CFACC# FROM CFACCT WHERE CFACC#=CTRTransaction.AccountNo AND NOT CFACC# IS NULL) IS NULL THEN 'N' ELSE 'Y' END AS IsJointAccount")
            Return StrQuery.ToString()
        End Get
    End Property

    Public Property PageSize() As Int16
        Get
            Return IIf(Session("PageSize") IsNot Nothing, Session("PageSize"), 10)
        End Get
        Set(ByVal value As Int16)
            Session("PageSize") = value
        End Set
    End Property
#End Region

    Private Sub FillSearch()
        Try
            Me.ComboSearch.Items.Add(New ListItem("...", ""))
            Me.ComboSearch.Items.Add(New ListItem("Transaction Date", "TransactionDate"))
            Me.ComboSearch.Items.Add(New ListItem("Account Owner", "CAST(CTRTransaction.AccountOwnerId AS VARCHAR)  + ' - ' + AccountOwner.AccountOwnerName"))
            Me.ComboSearch.Items.Add(New ListItem("TransactionChannelTypeName", "TransactionChannelType.TransactionChannelTypeName"))
            Me.ComboSearch.Items.Add(New ListItem("Country", "CTRTransaction.CountryCode + ' - ' + ISNULL(JHCOUN.JHCNAM,'')"))
            Me.ComboSearch.Items.Add(New ListItem("Bank Code", "BankCode"))
            Me.ComboSearch.Items.Add(New ListItem("Bank Name", "BankName"))
            Me.ComboSearch.Items.Add(New ListItem("CIFNo", "CIFNo"))
            Me.ComboSearch.Items.Add(New ListItem("Account No", "AccountNo"))
            Me.ComboSearch.Items.Add(New ListItem("Account Name", "AccountName"))
            Me.ComboSearch.Items.Add(New ListItem("AuxiliaryTransactionCode", "CTRTransaction.AuxiliaryTransactionCode + ' - ' + vw_AuxTransactionCode.TransactionDescription"))
            Me.ComboSearch.Items.Add(New ListItem("Transaction Amount", "TransactionAmount"))
            Me.ComboSearch.Items.Add(New ListItem("CurrencyType", "CurrencyType"))
            Me.ComboSearch.Items.Add(New ListItem("Debit / Credit", "DebitORCredit"))
            Me.ComboSearch.Items.Add(New ListItem("Memo Remark", "MemoRemark"))
            Me.ComboSearch.Items.Add(New ListItem("Transaction Remark", "TransactionRemark"))
            Me.ComboSearch.Items.Add(New ListItem("Transaction Exchange Rate", "TransactionExchangeRate"))
            Me.ComboSearch.Items.Add(New ListItem("Transaction Local Equivalent", "TransactionLocalEquivalent"))
            Me.ComboSearch.Items.Add(New ListItem("Transaction Ticket No", "TransactionTicketNo"))
            Me.ComboSearch.Items.Add(New ListItem("UserId", "UserId"))
            Me.ComboSearch.Items.Add(New ListItem("IsJointAccount", "CASE WHEN (SELECT TOP 1 CFACC# FROM CFACCT WHERE CFACC#=CTRTransaction.AccountNo AND NOT CFACC# IS NULL) IS NULL THEN 'N' ELSE 'Y' END"))
            'Me.ComboSearch.Items.Add(New ListItem("Transaction Date", "TransactionDate Like '%-=Search=-%'"))
            'Me.ComboSearch.Items.Add(New ListItem("Transaction Code", "TransactionCode Like '%-=Search=-%'"))
            'Me.ComboSearch.Items.Add(New ListItem("Country Name", "CountryCode like '%-=Search=-%'"))
            'Me.ComboSearch.Items.Add(New ListItem("Bank Name", "BankName Like '%-=Search=-%'"))
            'Me.ComboSearch.Items.Add(New ListItem("CIFNo", "CIFNo like '%-=Search=-%'"))
            'Me.ComboSearch.Items.Add(New ListItem("Account No", "AccountNo like '%-=Search=-%'"))
            'Me.ComboSearch.Items.Add(New ListItem("Account Name", "AccountName LIKE '%-=Search=-%'"))
            'Me.ComboSearch.Items.Add(New ListItem("Transaction Amount", "TransactionAmount Like '%-=Search=-%'"))
            'Me.ComboSearch.Items.Add(New ListItem("Transaction Remark", "TransactionRemark Like '%-=Search=-%'"))
            'Me.ComboSearch.Items.Add(New ListItem("Memo Remark", "MemoRemark like '%-=Search=-%'"))
            'Me.ComboSearch.Items.Add(New ListItem("Transaction Ticket No", "TransactionTicketNo Like '%-=Search=-%'"))
        Catch
            Throw
        End Try
    End Sub

    Private Sub ClearThisPageSessions()
        SetnGetSelectedItemCreateCase = Nothing
        Session("CashTransactionReportDetailSelected") = Nothing
        Session("CashTransactionReportDetailFieldSearch") = Nothing
        Session("CashTransactionReportDetailValueSearch") = Nothing
        Session("CashTransactionReportDetailSort") = Nothing
        Session("CashTransactionReportDetailCurrentPage") = Nothing
        Session("CashTransactionReportDetailRowTotal") = Nothing
        Session("CashTransactionReportDetailData") = Nothing
    End Sub

    Private Function LoadData() As Boolean
        If Not Me.PK_CTRID Is Nothing Then
            Me.LabelCaseNumber.Text = Me.PK_CTRID
        End If
        If Not Me.GetAccountOwner Is Nothing Then
            Me.LabelAccountOwner.Text = Me.GetAccountOwner
        End If
        If Not Me.GetCIFNo Is Nothing Then
            Me.LabelCIFNo.Text = Me.GetCIFNo
        End If
        If Not Me.GetCustomerName Is Nothing Then
            Me.LabelCustomerName.Text = Me.GetCustomerName
        End If
        If Not Me.GetTotalTransactionValue Is Nothing Then
            Me.LabelTotalTransactionValue.Text = Me.GetTotalTransactionValue
        End If
        If Not Me.GetCTRDescription Is Nothing Then
            Me.LabelDescription.Text = Me.GetCTRDescription
        End If
        Return True
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim Trans As SqlTransaction = Nothing
        Try
            If Page.IsPostBack = False Then
                Me.ClearThisPageSessions()
                Me.ComboSearch.Attributes.Add("onchange", "javascript:Check(this, 1, '" & Me.TextSearch.ClientID & "', '" & Me.popUp.ClientID & "');")
                Me.popUp.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TextSearch.ClientID & "'), 'dd-mmm-yyyy')")
                Me.popUp.Style.Add("display", "none")
                Me.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow
                Me.FillSearch()
                Me.GridMSUserView.PageSize = Me.PageSize
                Me.LoadData()
                Me.SetnGetValueSearch = "FK_CTRID=" & Me.PK_CTRID

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Trans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAudit)
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                    Trans.Commit()
                End Using
            End If
        Catch ex As Exception
            If Not Trans Is Nothing Then Trans.Rollback()
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not Trans Is Nothing Then
                Trans.Dispose()
            End If
        End Try
    End Sub

    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknown Commandname " & e.CommandName)
            End Select
            Me.CollectSelected()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Public Sub BindGrid()
        Try
            Me.SetnGetBindTable = Sahassa.AML.Commonly.GetDatasetPaging(Me.Tables, Me.PK, Me.SetnGetSort, Me.SetnGetCurrentPage + 1, Me.PageSize, Me.Fields, Me.SetnGetValueSearch, "")
            Me.GridMSUserView.DataSource = Me.SetnGetBindTable
            Dim TotalRow As Int64

            TotalRow = Sahassa.AML.Commonly.GetTableCount(Me.Tables, Me.SetnGetValueSearch)
            Me.SetnGetRowTotal = TotalRow

            Me.GridMSUserView.VirtualItemCount = Me.SetnGetRowTotal
            Me.GridMSUserView.DataBind()
        Catch
            Throw
        End Try
    End Sub

    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.ComboSearch.SelectedValue = Me.SetnGetFieldSearch
            'Me.TextSearch.Text = Me.SetnGetValueSearch
        Catch
            Throw
        End Try
    End Sub

    Private Sub GridA_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridMSUserView.SortCommand
        Dim GridUser As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Try
            Me.CollectSelected()
            Me.SetnGetFieldSearch = Me.ComboSearch.SelectedValue
            If Me.ComboSearch.SelectedIndex = 0 Then
                Me.TextSearch.Text = ""
                Me.SetnGetValueSearch = "FK_CTRID=" & Me.PK_CTRID
            ElseIf Me.ComboSearch.SelectedIndex = 1 Then
                Dim DateSearch As DateTime
                Try
                    DateSearch = DateTime.Parse(Me.TextSearch.Text, New System.Globalization.CultureInfo("id-ID"))
                Catch ex As ArgumentOutOfRangeException
                    Throw New Exception("Input character cannot be convert to datetime.")
                Catch ex As FormatException
                    Throw New Exception("Unknown format date")
                End Try
                Me.SetnGetValueSearch = "FK_CTRID=" & Me.PK_CTRID & " AND " & Me.ComboSearch.SelectedValue & " = '" & DateSearch.ToString("yyyy-MM-dd") & "' "
            Else
                Me.SetnGetValueSearch = "FK_CTRID=" & Me.PK_CTRID & " AND " & Me.ComboSearch.SelectedValue & " like '%" & Me.TextSearch.Text.Replace("'", "''") & "%' "
            End If
            Me.SetnGetCurrentPage = 0
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            Me.CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be a positive number greater than zero.")
                End If
            Else
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

#Region "Excel"
    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = gridRow.FindControl("CheckBoxExporttoExcel")
                Dim CIFNo As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = Me.SetnGetSelectedItemFilter
                If ArrTarget.Contains(CIFNo) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(CIFNo)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(CIFNo)
                End If
                Me.SetnGetSelectedItemFilter = ArrTarget
            End If
        Next
    End Sub

    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub 'ClearControls

    Private Sub BindSelected()
        Try
            Dim listPK As New System.Collections.Generic.List(Of String)
            listPK.Add("'0'")
            For Each IdPk As String In Me.SetnGetSelectedItemFilter
                listPK.Add("'" & IdPk.ToString & "'")
            Next

            Me.GridMSUserView.DataSource = Sahassa.AML.Commonly.GetDatasetPaging(Me.Tables, Me.PK, Me.SetnGetSort, 0, Int32.MaxValue, Me.Fields, "PK_CTRTransactionID IN (" & String.Join(",", listPK.ToArray) & ")", "")
            Me.GridMSUserView.AllowPaging = False
            Me.GridMSUserView.DataBind()

            'Sembunyikan kolom ke 0,1,4 & 5 agar tidak ikut diekspor ke excel
            Me.GridMSUserView.Columns(0).Visible = False
            Me.GridMSUserView.Columns(1).Visible = False
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Me.CollectSelected()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelected()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=CashTransactionReportDetail.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridMSUserView)
            GridMSUserView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
#End Region

    Private Sub GridMSUserView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridMSUserView.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Dim chkBox As CheckBox = e.Item.FindControl("CheckBoxExporttoExcel")
                chkBox.Checked = Me.SetnGetSelectedItemFilter.Contains(e.Item.Cells(1).Text)
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItemFilter
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(1).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItemFilter.Contains(StrKey) = False Then
                        ArrTarget.Add(StrKey)
                    End If
                Else
                    If Me.SetnGetSelectedItemFilter.Contains(StrKey) Then
                        ArrTarget.Remove(StrKey)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItemFilter = ArrTarget
    End Sub

    Protected Sub LnkCreateCase_Click(sender As Object, e As System.EventArgs) Handles LnkCreateCase.Click
        Try
            For Each gridRow As DataGridItem In Me.GridMSUserView.Items
                If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                    Dim chkBox As CheckBox = gridRow.FindControl("CheckBoxExporttoExcel")
                    Dim CIFNo As String = gridRow.Cells(1).Text
                    Dim ArrTarget As ArrayList = Me.SetnGetSelectedItemCreateCase
                    If ArrTarget.Contains(CIFNo) Then
                        If Not chkBox.Checked Then
                            ArrTarget.Remove(CIFNo)
                        End If
                    Else
                        If chkBox.Checked Then ArrTarget.Add(CIFNo)
                    End If
                    Me.SetnGetSelectedItemCreateCase = ArrTarget
                End If
            Next
            If SetnGetSelectedItemCreateCase.Count > 0 Then
                Response.Redirect("CreateSTRFromCTR.aspx")
            Else
                Throw New Exception("Please Select At least one Transaction before create Case.")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class
