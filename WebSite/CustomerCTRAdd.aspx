<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="CustomerCTRAdd.aspx.vb" Inherits="CustomerCTRAdd" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    <script src="Script/popcalendar.js"></script>
    <script language="javascript" type="text/javascript">


        function charIndex(str, cSearch) {
            var i = 0;
            str = new String(str);
            for (i = 0; i < str.length; i++) {
                if (str.charAt(i) == cSearch) {
                    return i;
                }
            }
            return -1;
        }

        function replaceAll(str, cSearch) {
            while (charIndex(str, cSearch) != -1) {
                str = str.replace(cSearch, "")
            }
            return str;
        }

        function CommaFormatted(obj, amount) {

            var delimiter = "."; // replace comma if desired
            var coma = ".";
            var belakangKoma = 0;  //penampung angka koma
            var i = 0; //buat iterasi
            var sign = ""; //buat tanda          
            var hasil = [];
            // penyesuaian tanda koma
            if (delimiter == ".") {
                coma = ",";
            }

            amount = replaceAll(amount, delimiter);
            amount = new String(amount);
            var index = charIndex(amount, coma);

            if (index != -1) {
                belakangKoma = amount.substr(index + 1, amount.length - index);
                if (isNaN(belakangKoma) == true) {
                    alert("Format angka koma salah"); //Handling kalau setelah koma ada tanda lain
                    belakangKoma = 0;
                }
                amount = amount.substr(0, index);
            }

            if (amount < 0) {
                var z;
                sign = "-";
                amount = Math.abs(amount);
            }

            while (amount.length > 3) {
                var nn = amount.substr(amount.length - 3);
                hasil.unshift(nn);
                amount = amount.substr(0, amount.length - 3);
            }

            if (amount.length > 0) {
                hasil.unshift(amount);
            }
            hasil = hasil.join(delimiter);

            if (belakangKoma != 0) {
                hasil = hasil + coma + belakangKoma;
            }

            if (sign != "") {
                hasil = sign + hasil;
            }

            obj.value = hasil;


        }
        function hidePanel(objhide, objpanel, imgmin, imgmax) {
            document.getElementById(objhide).style.display = 'none';
            document.getElementById(objpanel).src = imgmax;
        }
        // JScript File

        function popWin2() {
            var height = '600px';
            var width = '550px';
            var left = (screen.availWidth - width) / 2;
            var top = (screen.availHeight - height) / 2;
            var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

            window.showModalDialog("PickerKelurahan.aspx", "#1", winSetting);

        }


        function popWinKecamatan() {
            var height = '600px';
            var width = '550px';
            var left = (screen.availWidth - width) / 2;
            var top = (screen.availHeight - height) / 2;
            var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

            window.showModalDialog("PickerKecamatan.aspx", "#2", winSetting);
        }
        function popWinNegara() {
            var height = '600px';
            var width = '550px';
            var left = (screen.availWidth - width) / 2;
            var top = (screen.availHeight - height) / 2;
            var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

            window.showModalDialog("PickerNegara.aspx", "#3", winSetting);
        }
        function popWinMataUang() {
            var height = '600px';
            var width = '550px';
            var left = (screen.availWidth - width) / 2;
            var top = (screen.availHeight - height) / 2;
            var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

            window.showModalDialog("PickerMataUang.aspx", "#4", winSetting);

        }
        function popWinProvinsi() {
            var height = '600px';
            var width = '550px';
            var left = (screen.availWidth - width) / 2;
            var top = (screen.availHeight - height) / 2;
            var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

            window.showModalDialog("PickerProvinsi.aspx", "#5", winSetting);
        }
        function popWinPekerjaan() {
            var height = '600px';
            var width = '550px';
            var left = (screen.availWidth - width) / 2;
            var top = (screen.availHeight - height) / 2;
            var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

            window.showModalDialog("PickerPekerjaan.aspx", "#6", winSetting);
        }
        function popWinKotaKab() {
            var height = '600px';
            var width = '550px';
            var left = (screen.availWidth - width) / 2;
            var top = (screen.availHeight - height) / 2;
            var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

            window.showModalDialog("PickerKotaKab.aspx", "#7", winSetting);
        }
        function popWinBidangUsaha() {
            var height = '600px';
            var width = '550px';
            var left = (screen.availWidth - width) / 2;
            var top = (screen.availHeight - height) / 2;
            var winSetting = "dialogWidth=" + width + ";dialogHeight=" + height + ";dialogleft=" + left + ";dialogtop=" + top + ";scrollbars=yes;resizable=yes;location=no;menubar=no;toolbar=no";

            window.showModalDialog("PickerBidangUsaha.aspx", "#8", winSetting);
        }
    </script>
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td>
                <img src="Images/blank.gif" width="5" height="1" />
            </td>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td>
                        </td>
                        <td width="99%" bgcolor="#FFFFFF">
                            <img src="Images/blank.gif" width="1" height="1" />
                        </td>
                        <td bgcolor="#FFFFFF">
                            <img src="Images/blank.gif" width="1" height="1" />
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <img src="Images/blank.gif" width="5" height="1" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td id="tdcontent" height="99%" valign="top" bgcolor="#FFFFFF">
                <div id="divcontent">
                    <table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td bgcolor="#ffffff" class="divcontentinside" colspan="2">
                                <img src="Images/blank.gif" width="20" height="100%" /><ajax:AjaxPanel ID="a" runat="server">
                                </ajax:AjaxPanel>
                                <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" border="0"
                                    style="border-top-style: none; border-right-style: none; border-left-style: none;
                                    border-bottom-style: none">
                                    <tr>
                                        <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                                            border-right-style: none; border-left-style: none; border-bottom-style: none">
                                            <img src="Images/dot_title.gif" width="17" height="17">
                                            <strong>
                                                <asp:Label ID="Label1" runat="server" Text="Customer CTR - Add New"></asp:Label></strong>
                                            <hr />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                                            border-right-style: none; border-left-style: none; border-bottom-style: none">
                                            <ajax:AjaxPanel ID="AjaxPanel1" runat="server">
                                                <asp:Label ID="LblSucces" CssClass="validationok" Width="94%" runat="server" Visible="False"></asp:Label>
                                            </ajax:AjaxPanel>
                                        </td>
                                    </tr>
                                </table>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                                <ajax:AjaxPanel ID="AjaxMultiView" runat="server" Width="100%">
                                    <asp:MultiView ID="mtvPage" runat="server" ActiveViewIndex="0">
                                        <asp:View ID="vwTransaksi" runat="server">
                                            <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
                                                border="0">
                                                <tbody>
                                                    <tr>
                                                        <td style="height: 6px" valign="middle" align="left" width="100%" background="Images/search-bar-background.gif">
                                                            <table cellspacing="0" cellpadding="0" border="0">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="formtext">
                                                                            <asp:Label ID="Label2" runat="server" Text="IDENTITAS " Font-Bold="True"></asp:Label>&nbsp;
                                                                        </td>
                                                                        <td>
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr id="Id">
                                                        <td bgcolor="#ffffff" colspan="2">
                                                            <ajax:AjaxPanel ID="ajxPnl" runat="server" Width="100%">
                                                                <table width="100%">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td class="formText">
                                                                                Tipe Customer
                                                                            </td>
                                                                            <td style="width: 5px">
                                                                                :
                                                                            </td>
                                                                            <td class="formtext">
                                                                                <asp:RadioButtonList ID="rblTipePelapor" runat="server" AutoPostBack="True" RepeatDirection="Horizontal">
                                                                                    <asp:ListItem Value="1">Perorangan</asp:ListItem>
                                                                                    <asp:ListItem Value="2">Korporasi</asp:ListItem>
                                                                                </asp:RadioButtonList>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="formtext" colspan="3">
                                                                                <div id="divPerorangan" runat="server" hidden="false">
                                                                                    <table>
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td class="formtext" colspan="3">
                                                                                                    <strong>Perorangan</strong>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formText" style="width: 162px">
                                                                                                    Gelar
                                                                                                </td>
                                                                                                <td style="width: 3px">
                                                                                                    :
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    <asp:TextBox ID="txtGelar" runat="server" CssClass="textBox"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formText" style="width: 162px">
                                                                                                    Nama Lengkap <span style="color: #cc0000">*</span>
                                                                                                </td>
                                                                                                <td style="width: 3px">
                                                                                                    :
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    <asp:TextBox ID="txtNamaLengkap" runat="server" CssClass="textBox" Width="370px"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formText" style="width: 162px">
                                                                                                    Nama Alias
                                                                                                </td>
                                                                                                <td style="width: 3px">
                                                                                                    :
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    <asp:TextBox ID="txtNamaAlias" runat="server" CssClass="textBox" Width="370px"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formText" style="width: 162px">
                                                                                                    Tempat Lahir <span style="color: #cc0000">*</span>
                                                                                                </td>
                                                                                                <td style="width: 3px">
                                                                                                    :
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    <asp:TextBox ID="txtTempatLahir" runat="server" CssClass="textBox" Width="240px"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formText" style="width: 162px">
                                                                                                    Tanggal Lahir <span style="color: #cc0000">*</span>
                                                                                                </td>
                                                                                                <td style="width: 3px">
                                                                                                    :
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    <br />
                                                                                                    <asp:TextBox ID="txtTglLahir" runat="server" CssClass="textBox" Width="96px" MaxLength="50"></asp:TextBox><input
                                                                                                        style="border-right: #ffffff 0px solid; border-top: #ffffff 0px solid; font-size: 11px;
                                                                                                        background-image: url(Script/Calendar/cal.gif); border-left: #ffffff 0px solid;
                                                                                                        width: 16px; border-bottom: #ffffff 0px solid; height: 17px" id="popTglLahir"
                                                                                                        title="Click to show calendar" type="button" runat="server" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formText" style="width: 162px">
                                                                                                    Kewarganegaraan (Pilih salah satu) <span style="color: #cc0000">*</span>
                                                                                                </td>
                                                                                                <td style="width: 3px">
                                                                                                    :
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    <asp:RadioButtonList ID="rblKewarganegaraan" runat="server" AutoPostBack="True" RepeatDirection="Horizontal">
                                                                                                        <asp:ListItem Value="1">WNI</asp:ListItem>
                                                                                                        <asp:ListItem Value="2">WNA</asp:ListItem>
                                                                                                    </asp:RadioButtonList>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formText" style="width: 162px">
                                                                                                    Negara <span style="color: #cc0000">*</span>
                                                                                                </td>
                                                                                                <td style="width: 3px">
                                                                                                    :
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    <asp:DropDownList ID="cboNegara" runat="server" CssClass="combobox">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formText" style="width: 162px">
                                                                                                    Alamat Domisili
                                                                                                </td>
                                                                                                <td style="width: 3px">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext" colspan="3">
                                                                                                    <table>
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td class="formtext">
                                                                                                                    &nbsp; &nbsp;&nbsp;
                                                                                                                </td>
                                                                                                                <td class="formText">
                                                                                                                    Nama Jalan
                                                                                                                </td>
                                                                                                                <td style="width: 5px">
                                                                                                                    :
                                                                                                                </td>
                                                                                                                <td class="formtext">
                                                                                                                    <asp:TextBox ID="txtDOMNamaJalan" runat="server" CssClass="textBox" Width="370px"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtext">
                                                                                                                </td>
                                                                                                                <td class="formText">
                                                                                                                    RT
                                                                                                                </td>
                                                                                                                <td style="width: 5px">
                                                                                                                    :
                                                                                                                </td>
                                                                                                                <td class="formtext">
                                                                                                                    <asp:TextBox ID="txtDOMRT" runat="server" CssClass="textBox"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtext">
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                                <td class="formText">
                                                                                                                    RW
                                                                                                                </td>
                                                                                                                <td style="width: 5px">
                                                                                                                    :
                                                                                                                </td>
                                                                                                                <td class="formtext">
                                                                                                                    <asp:TextBox ID="txtDOMRW" runat="server" CssClass="textBox"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtext">
                                                                                                                </td>
                                                                                                                <td class="formText">
                                                                                                                    Kelurahan
                                                                                                                </td>
                                                                                                                <td style="width: 5px">
                                                                                                                    :
                                                                                                                </td>
                                                                                                                <td class="formtext" valign="top">
                                                                                                                    <table style="width: 127px">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:TextBox ID="txtDOMKelurahan" runat="server" CssClass="textbox" Width="167px"
                                                                                                                                        Enabled="False" ajaxcall="none"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    <asp:ImageButton ID="imgDOMKelurahan" runat="server" OnClientClick="javascript:popWin2();"
                                                                                                                                        ImageUrl="~/Images/button/browse.gif" CausesValidation="False"></asp:ImageButton>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                    <asp:HiddenField ID="hfDOMKelurahan" runat="server"></asp:HiddenField>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtext">
                                                                                                                </td>
                                                                                                                <td class="formText">
                                                                                                                    Kecamatan
                                                                                                                </td>
                                                                                                                <td style="width: 5px">
                                                                                                                    :
                                                                                                                </td>
                                                                                                                <td class="formtext">
                                                                                                                    &nbsp;<table style="width: 127px">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:TextBox ID="txtDOMKecamatan" runat="server" CssClass="textBox" Width="167px"
                                                                                                                                        Enabled="False"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    <asp:ImageButton ID="imgDOMKecamatan" runat="server" ImageUrl="~/Images/button/browse.gif"
                                                                                                                                        CausesValidation="False" OnClientClick="javascript:popWinKecamatan();"></asp:ImageButton>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                    <asp:HiddenField ID="hfDOMKecamatan" runat="server"></asp:HiddenField>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtext">
                                                                                                                </td>
                                                                                                                <td class="formText">
                                                                                                                    Kota / Kabupaten<span style="color: #cc0000">*</span>
                                                                                                                </td>
                                                                                                                <td style="width: 5px">
                                                                                                                    :
                                                                                                                </td>
                                                                                                                <td class="formtext">
                                                                                                                    <table style="width: 127px">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:TextBox ID="txtDOMKotaKab" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    <asp:ImageButton ID="imgDOMKotaKab" runat="server" ImageUrl="~/Images/button/browse.gif"
                                                                                                                                        CausesValidation="False" OnClientClick="javascript:popWinKotaKab();"></asp:ImageButton>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                    <asp:HiddenField ID="hfDOMKotaKab" runat="server"></asp:HiddenField>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtext">
                                                                                                                </td>
                                                                                                                <td class="formText">
                                                                                                                    Provinsi<span style="color: #cc0000">*</span>
                                                                                                                </td>
                                                                                                                <td style="width: 5px">
                                                                                                                    :
                                                                                                                </td>
                                                                                                                <td class="formtext">
                                                                                                                    <table style="width: 127px">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:TextBox ID="txtDOMProvinsi" runat="server" CssClass="textBox" Width="167px"
                                                                                                                                        Enabled="False"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    <asp:ImageButton ID="imgDOMProvinsi" runat="server" ImageUrl="~/Images/button/browse.gif"
                                                                                                                                        CausesValidation="False" OnClientClick="javascript:popWinProvinsi();"></asp:ImageButton>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                    <asp:HiddenField ID="hfDOMProvinsi" runat="server"></asp:HiddenField>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtext">
                                                                                                                </td>
                                                                                                                <td class="formText">
                                                                                                                    Negara<span style="color: #cc0000">*</span>
                                                                                                                </td>
                                                                                                                <td style="width: 5px">
                                                                                                                    :
                                                                                                                </td>
                                                                                                                <td class="formtext">
                                                                                                                    <table style="width: 127px">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:TextBox ID="txtDOMNegara" runat="server" CssClass="textBox" Enabled="False"
                                                                                                                                        Width="167px"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    <asp:ImageButton ID="imgDOMNegara" runat="server" CausesValidation="False" ImageUrl="~/Images/button/browse.gif"
                                                                                                                                        OnClientClick="javascript:popWinNegara();" />
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                    <asp:HiddenField ID="hfDomNegara" runat="server" />
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtext">
                                                                                                                </td>
                                                                                                                <td class="formText">
                                                                                                                    Kode Pos
                                                                                                                </td>
                                                                                                                <td style="width: 5px">
                                                                                                                    :
                                                                                                                </td>
                                                                                                                <td class="formtext">
                                                                                                                    <asp:TextBox ID="txtDOMKodePos" runat="server" CssClass="textBox"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formText" style="width: 162px">
                                                                                                    Alamat Sesuai Bukti Identitas
                                                                                                </td>
                                                                                                <td style="width: 3px">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    <asp:CheckBox ID="chkCopyAlamatDOM" runat="server" Text="Sama dengan Alamat Domisili"
                                                                                                        AutoPostBack="True"></asp:CheckBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext" colspan="3">
                                                                                                    <table>
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td class="formText">
                                                                                                                    &nbsp; &nbsp;&nbsp;
                                                                                                                </td>
                                                                                                                <td class="formText">
                                                                                                                    Nama Jalan
                                                                                                                </td>
                                                                                                                <td style="width: 5px">
                                                                                                                    :
                                                                                                                </td>
                                                                                                                <td class="formtext">
                                                                                                                    <asp:TextBox ID="txtIDNamaJalan" runat="server" CssClass="textBox" Width="370px"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formText">
                                                                                                                </td>
                                                                                                                <td class="formText">
                                                                                                                    RT
                                                                                                                </td>
                                                                                                                <td style="width: 5px">
                                                                                                                    :
                                                                                                                </td>
                                                                                                                <td class="formtext">
                                                                                                                    <asp:TextBox ID="txtIDRT" runat="server" CssClass="textBox"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formText">
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                                <td class="formText">
                                                                                                                    RW
                                                                                                                </td>
                                                                                                                <td style="width: 5px">
                                                                                                                    :
                                                                                                                </td>
                                                                                                                <td class="formtext">
                                                                                                                    <asp:TextBox ID="txtIDRW" runat="server" CssClass="textBox"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formText">
                                                                                                                </td>
                                                                                                                <td class="formText">
                                                                                                                    Kelurahan
                                                                                                                </td>
                                                                                                                <td style="width: 5px">
                                                                                                                    :
                                                                                                                </td>
                                                                                                                <td class="formtext" valign="top">
                                                                                                                    <table style="width: 127px">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:TextBox ID="txtIDKelurahan" runat="server" CssClass="textBox" Width="167px"
                                                                                                                                        Enabled="False"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    <asp:ImageButton ID="imgIDKelurahan" runat="server" ImageUrl="~/Images/button/browse.gif"
                                                                                                                                        CausesValidation="False" OnClientClick="javascript:popWin2();"></asp:ImageButton>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                    <asp:HiddenField ID="hfIDKelurahan" runat="server"></asp:HiddenField>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formText">
                                                                                                                </td>
                                                                                                                <td class="formText">
                                                                                                                    Kecamatan
                                                                                                                </td>
                                                                                                                <td style="width: 5px">
                                                                                                                    :
                                                                                                                </td>
                                                                                                                <td class="formtext">
                                                                                                                    &nbsp;<table style="width: 127px">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:TextBox ID="txtIDKecamatan" runat="server" CssClass="textBox" Width="167px"
                                                                                                                                        Enabled="False"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    <asp:ImageButton ID="imgIDKecamatan" runat="server" ImageUrl="~/Images/button/browse.gif"
                                                                                                                                        CausesValidation="False" OnClientClick="javascript:popWinKecamatan();"></asp:ImageButton>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                    <asp:HiddenField ID="hfIDKecamatan" runat="server"></asp:HiddenField>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formText">
                                                                                                                </td>
                                                                                                                <td class="formText">
                                                                                                                    Kota / Kabupaten<span style="color: #cc0000">*</span>
                                                                                                                </td>
                                                                                                                <td style="width: 5px">
                                                                                                                    :
                                                                                                                </td>
                                                                                                                <td class="formtext">
                                                                                                                    <table style="width: 127px">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:TextBox ID="txtIDKotaKab" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    <asp:ImageButton ID="imgIDKotaKab" runat="server" ImageUrl="~/Images/button/browse.gif"
                                                                                                                                        CausesValidation="False" OnClientClick="javascript:popWinKotaKab();"></asp:ImageButton>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                    <asp:HiddenField ID="hfIDKotaKab" runat="server"></asp:HiddenField>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formText">
                                                                                                                </td>
                                                                                                                <td class="formText">
                                                                                                                    Provinsi<span style="color: #cc0000">*</span>
                                                                                                                </td>
                                                                                                                <td style="width: 5px">
                                                                                                                    :
                                                                                                                </td>
                                                                                                                <td class="formtext">
                                                                                                                    <table style="width: 127px">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td style="height: 22px">
                                                                                                                                    <asp:TextBox ID="txtIDProvinsi" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                                <td style="height: 22px">
                                                                                                                                    <asp:ImageButton ID="imgIDProvinsi" runat="server" ImageUrl="~/Images/button/browse.gif"
                                                                                                                                        CausesValidation="False" OnClientClick="javascript:popWinProvinsi();"></asp:ImageButton>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                    <asp:HiddenField ID="hfIDProvinsi" runat="server"></asp:HiddenField>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtext">
                                                                                                                </td>
                                                                                                                <td class="formText">
                                                                                                                    Negara<span style="color: #cc0000">*</span>
                                                                                                                </td>
                                                                                                                <td style="width: 5px">
                                                                                                                    :
                                                                                                                </td>
                                                                                                                <td class="formtext">
                                                                                                                    <table style="width: 127px">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:TextBox ID="txtIDNegara" runat="server" CssClass="textBox" Enabled="False" Width="167px"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    <asp:ImageButton ID="imgIDNegara" runat="server" CausesValidation="False" ImageUrl="~/Images/button/browse.gif"
                                                                                                                                        OnClientClick="javascript:popWinNegara();" />
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                    <asp:HiddenField ID="hfIDNegara" runat="server" />
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formText">
                                                                                                                </td>
                                                                                                                <td class="formText">
                                                                                                                    Kode Pos
                                                                                                                </td>
                                                                                                                <td style="width: 5px">
                                                                                                                    :
                                                                                                                </td>
                                                                                                                <td class="formtext">
                                                                                                                    <asp:TextBox ID="txtIDKodePos" runat="server" CssClass="textBox"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formText" style="width: 162px">
                                                                                                    Alamat Sesuai Negara Asal
                                                                                                </td>
                                                                                                <td style="width: 3px">
                                                                                                </td>
                                                                                                <td>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext" colspan="3">
                                                                                                    <table id="tblnegaraasal" runat="Server" visible="false">
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td class="formText">
                                                                                                                </td>
                                                                                                                <td class="formText">
                                                                                                                    Nama Jalan
                                                                                                                </td>
                                                                                                                <td style="width: 5px">
                                                                                                                    :
                                                                                                                </td>
                                                                                                                <td class="formtext">
                                                                                                                    <asp:TextBox ID="txtNANamaJalan" runat="server" CssClass="textBox" Width="370px"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td class="formtext">
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formText">
                                                                                                                    &nbsp; &nbsp;&nbsp;
                                                                                                                </td>
                                                                                                                <td class="formText">
                                                                                                                    Negara<span style="color: #cc0000">*</span>
                                                                                                                </td>
                                                                                                                <td style="width: 5px">
                                                                                                                    :
                                                                                                                </td>
                                                                                                                <td class="formtext">
                                                                                                                    <table style="width: 127px">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:TextBox ID="txtNANegara" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    <asp:ImageButton ID="imgIDNANegara" runat="server" ImageUrl="~/Images/button/browse.gif"
                                                                                                                                        CausesValidation="False" OnClientClick="javascript:popWinNegara();"></asp:ImageButton>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                    <asp:HiddenField ID="hfNANegara" runat="server"></asp:HiddenField>
                                                                                                                </td>
                                                                                                                <td class="formtext" style="font-style: italic">
                                                                                                                    Required jika Kewarganegaraan WNA
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formText">
                                                                                                                </td>
                                                                                                                <td class="formText">
                                                                                                                    Provinsi
                                                                                                                </td>
                                                                                                                <td style="width: 5px">
                                                                                                                    :
                                                                                                                </td>
                                                                                                                <td class="formtext" valign="top">
                                                                                                                    <table style="width: 127px">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:TextBox ID="txtNAProvinsi" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    <asp:ImageButton ID="imgNAProvinsi" runat="server" ImageUrl="~/Images/button/browse.gif"
                                                                                                                                        CausesValidation="False" OnClientClick="javascript:popWinProvinsi();"></asp:ImageButton>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                    <asp:HiddenField ID="hfNAProvinsi" runat="server"></asp:HiddenField>
                                                                                                                </td>
                                                                                                                <td class="formtext" valign="top">
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formText">
                                                                                                                </td>
                                                                                                                <td class="formText">
                                                                                                                    Kota
                                                                                                                </td>
                                                                                                                <td style="width: 5px">
                                                                                                                    :
                                                                                                                </td>
                                                                                                                <td class="formtext">
                                                                                                                    <table style="width: 127px">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:TextBox ID="txtNAKota" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    <asp:ImageButton ID="imgIDKota" runat="server" ImageUrl="~/Images/button/browse.gif"
                                                                                                                                        CausesValidation="False" OnClientClick="javascript:popWinKotaKab();"></asp:ImageButton>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                    <asp:HiddenField ID="hfIDKota" runat="server"></asp:HiddenField>
                                                                                                                </td>
                                                                                                                <td class="formtext">
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formText">
                                                                                                                </td>
                                                                                                                <td class="formText">
                                                                                                                    Kode Pos<span style="color: #cc0000">*</span>
                                                                                                                </td>
                                                                                                                <td style="width: 5px">
                                                                                                                    :
                                                                                                                </td>
                                                                                                                <td class="formtext">
                                                                                                                    <asp:TextBox ID="txtNAKodePos" runat="server" CssClass="textBox"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td class="formtext" style="font-style: italic">
                                                                                                                    Required jika Kewarganegaraan WNA
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formText" style="width: 162px">
                                                                                                    Jenis Dokumen Identitas<span style="color: #cc0000">*</span>
                                                                                                </td>
                                                                                                <td style="width: 1px">
                                                                                                    :
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    <ajax:AjaxPanel ID="ajPnlDokId" runat="server">
                                                                                                        <asp:DropDownList runat="server" ID="cboJenisDocID" CssClass="combobox">
                                                                                                        </asp:DropDownList>
                                                                                                    </ajax:AjaxPanel>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td id="tdNomorId" class="formtext" colspan="3" runat="server">
                                                                                                    <table>
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td class="formText" style="width: 197px">
                                                                                                                    Nomor Identitas <span style="color: #cc0000">*</span>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    :
                                                                                                                </td>
                                                                                                                <td style="width: 260px" class="formtext">
                                                                                                                    <asp:TextBox ID="txtNomorID" runat="server" CssClass="textBox" Width="257px"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formText" style="width: 162px">
                                                                                                    Nomor Pokok Wajib Pajak (NPWP)
                                                                                                </td>
                                                                                                <td style="width: 1px">
                                                                                                    :
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    <asp:TextBox ID="txtNPWP" runat="server" CssClass="textBox" Width="257px"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="height: 15px; width: 162px;" class="formText">
                                                                                                    Pekerjaan
                                                                                                </td>
                                                                                                <td style="width: 1px; height: 15px">
                                                                                                </td>
                                                                                                <td style="height: 15px" class="formtext">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext" colspan="3">
                                                                                                    <table>
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td class="formtext">
                                                                                                                    &nbsp; &nbsp;&nbsp;
                                                                                                                </td>
                                                                                                                <td class="formText">
                                                                                                                    Pekerjaan<span style="color: #cc0000">*</span>
                                                                                                                </td>
                                                                                                                <td style="width: 5px">
                                                                                                                    :
                                                                                                                </td>
                                                                                                                <td style="width: 260px" class="formtext">
                                                                                                                    <table>
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:TextBox ID="txtPekerjaan" runat="server" CssClass="textBox" Width="257px" Enabled="False"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    <asp:ImageButton ID="imgPekerjaan" runat="server" ImageUrl="~/Images/button/browse.gif"
                                                                                                                                        CausesValidation="False" OnClientClick="javascript:popWinPekerjaan();"></asp:ImageButton>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                    <asp:HiddenField ID="hfPekerjaan" runat="server"></asp:HiddenField>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtext">
                                                                                                                </td>
                                                                                                                <td class="formText">
                                                                                                                    Jabatan
                                                                                                                </td>
                                                                                                                <td style="width: 5px">
                                                                                                                    :
                                                                                                                </td>
                                                                                                                <td style="width: 260px" class="formtext">
                                                                                                                    <asp:TextBox ID="txtJabatan" runat="server" CssClass="textBox" Width="257px"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtext">
                                                                                                                </td>
                                                                                                                <td class="formText">
                                                                                                                    Penghasilan rata-rata/th (Rp)
                                                                                                                </td>
                                                                                                                <td style="width: 5px">
                                                                                                                    :
                                                                                                                </td>
                                                                                                                <td style="width: 260px" class="formtext">
                                                                                                                    <asp:TextBox ID="txtPenghasilanRataRata" runat="server" CssClass="textBox" Width="257px"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtext">
                                                                                                                </td>
                                                                                                                <td class="formText">
                                                                                                                    Tempat kerja
                                                                                                                </td>
                                                                                                                <td style="width: 5px">
                                                                                                                    :
                                                                                                                </td>
                                                                                                                <td style="width: 260px" class="formtext">
                                                                                                                    <asp:TextBox ID="txtTempatKerja" runat="server" CssClass="textBox" Width="257px"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                                <div id="divKorporasi" runat="server" hidden="true">
                                                                                    <table>
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td class="formtext" colspan="3">
                                                                                                    <strong>Korporasi</strong>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formText">
                                                                                                    Bentuk Badan Usaha<span style="color: #cc0000">*</span>
                                                                                                </td>
                                                                                                <td style="width: 5px">
                                                                                                    :
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    <asp:DropDownList ID="cboCORPBentukBadanUsaha" runat="server" CssClass="combobox">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formText">
                                                                                                    Nama Korporasi <span style="color: #cc0000">*</span>
                                                                                                </td>
                                                                                                <td style="width: 5px">
                                                                                                    :
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    <asp:TextBox ID="txtCORPNama" runat="server" CssClass="textBox" Width="370px"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formText">
                                                                                                    Bidang Usaha Korporasi<span style="color: #cc0000">*</span>
                                                                                                </td>
                                                                                                <td style="width: 5px">
                                                                                                    :
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    <table style="width: 127px">
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <asp:TextBox ID="txtCORPBidangUsaha" runat="server" CssClass="textBox" Width="167px"
                                                                                                                        Enabled="False"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <asp:ImageButton ID="imgCORPBidangUsaha" runat="server" ImageUrl="~/Images/button/browse.gif"
                                                                                                                        CausesValidation="False" OnClientClick="javascript:popWinBidangUsaha();"></asp:ImageButton>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                    <asp:HiddenField ID="hfCORPBidangUsaha" runat="server"></asp:HiddenField>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formText">
                                                                                                    Alamat Korporasi<span style="color: #cc0000">*</span>
                                                                                                </td>
                                                                                                <td style="width: 5px">
                                                                                                    :
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    <asp:RadioButtonList ID="rblCORPTipeAlamat" runat="server" AutoPostBack="True" RepeatDirection="Horizontal">
                                                                                                        <asp:ListItem>Dalam Negeri</asp:ListItem>
                                                                                                        <asp:ListItem>Luar Negeri</asp:ListItem>
                                                                                                    </asp:RadioButtonList>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formText">
                                                                                                    Alamat Lengkap Korporasi
                                                                                                </td>
                                                                                                <td style="width: 5px">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext" colspan="3">
                                                                                                    <table>
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td class="formtext">
                                                                                                                    &nbsp; &nbsp;&nbsp;
                                                                                                                </td>
                                                                                                                <td class="formText">
                                                                                                                    Nama Jalan
                                                                                                                </td>
                                                                                                                <td style="width: 5px">
                                                                                                                    :
                                                                                                                </td>
                                                                                                                <td class="formtext">
                                                                                                                    <asp:TextBox ID="txtCORPDLNamaJalan" runat="server" CssClass="textBox" Width="370px"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtext">
                                                                                                                </td>
                                                                                                                <td class="formText">
                                                                                                                    RT
                                                                                                                </td>
                                                                                                                <td style="width: 5px">
                                                                                                                    :
                                                                                                                </td>
                                                                                                                <td class="formtext">
                                                                                                                    <asp:TextBox ID="txtCORPDLRT" runat="server" CssClass="textBox"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtext">
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                                <td class="formText">
                                                                                                                    RW
                                                                                                                </td>
                                                                                                                <td style="width: 5px">
                                                                                                                    :
                                                                                                                </td>
                                                                                                                <td class="formtext">
                                                                                                                    <asp:TextBox ID="txtCORPDLRW" runat="server" CssClass="textBox"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtext">
                                                                                                                </td>
                                                                                                                <td class="formText">
                                                                                                                    Kelurahan
                                                                                                                </td>
                                                                                                                <td style="width: 5px">
                                                                                                                    :
                                                                                                                </td>
                                                                                                                <td class="formtext" valign="top">
                                                                                                                    <table style="width: 127px">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:TextBox ID="txtCORPDLKelurahan" runat="server" CssClass="textBox" Width="167px"
                                                                                                                                        Enabled="False"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    <asp:ImageButton ID="imgCORPDLKelurahan" runat="server" ImageUrl="~/Images/button/browse.gif"
                                                                                                                                        CausesValidation="False" OnClientClick="javascript:popWin2();"></asp:ImageButton>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                    <asp:HiddenField ID="hfCORPDLKelurahan" runat="server"></asp:HiddenField>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtext">
                                                                                                                </td>
                                                                                                                <td class="formText">
                                                                                                                    Kecamatan
                                                                                                                </td>
                                                                                                                <td style="width: 5px">
                                                                                                                    :
                                                                                                                </td>
                                                                                                                <td class="formtext">
                                                                                                                    &nbsp;<table style="width: 127px">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:TextBox ID="txtCORPDLKecamatan" runat="server" CssClass="textBox" Width="167px"
                                                                                                                                        Enabled="False"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    <asp:ImageButton ID="imgCORPDLKecamatan" runat="server" ImageUrl="~/Images/button/browse.gif"
                                                                                                                                        CausesValidation="False" OnClientClick="javascript:popWinKecamatan();"></asp:ImageButton>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                    <asp:HiddenField ID="hfCORPDLKecamatan" runat="server"></asp:HiddenField>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtext">
                                                                                                                </td>
                                                                                                                <td class="formText">
                                                                                                                    Kota / Kabupaten<span style="color: #cc0000">*</span>
                                                                                                                </td>
                                                                                                                <td style="width: 5px">
                                                                                                                    :
                                                                                                                </td>
                                                                                                                <td class="formtext">
                                                                                                                    <table style="width: 127px">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:TextBox ID="txtCORPDLKotaKab" runat="server" CssClass="textBox" Width="167px"
                                                                                                                                        Enabled="False"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    <asp:ImageButton ID="imgCORPDLKotaKab" runat="server" ImageUrl="~/Images/button/browse.gif"
                                                                                                                                        CausesValidation="False" OnClientClick="javascript:popWinKotaKab();"></asp:ImageButton>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                    <asp:HiddenField ID="hfCORPDLKotaKab" runat="server"></asp:HiddenField>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtext">
                                                                                                                </td>
                                                                                                                <td class="formText">
                                                                                                                    Kode Pos
                                                                                                                </td>
                                                                                                                <td style="width: 5px">
                                                                                                                    :
                                                                                                                </td>
                                                                                                                <td class="formtext">
                                                                                                                    <asp:TextBox ID="txtCORPDLKodePos" runat="server" CssClass="textBox"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtext">
                                                                                                                </td>
                                                                                                                <td class="formText">
                                                                                                                    Provinsi<span style="color: #cc0000">*</span>
                                                                                                                </td>
                                                                                                                <td style="width: 5px">
                                                                                                                    :
                                                                                                                </td>
                                                                                                                <td class="formtext">
                                                                                                                    <table style="width: 127px">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:TextBox ID="txtCORPDLProvinsi" runat="server" CssClass="textBox" Width="167px"
                                                                                                                                        Enabled="False"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    <asp:ImageButton ID="imgCORPDLProvinsi" runat="server" ImageUrl="~/Images/button/browse.gif"
                                                                                                                                        CausesValidation="False" OnClientClick="javascript:popWinProvinsi();"></asp:ImageButton>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                    <asp:HiddenField ID="hfCORPDLProvinsi" runat="server"></asp:HiddenField>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtext">
                                                                                                                </td>
                                                                                                                <td class="formText">
                                                                                                                    Negara<span style="color: #cc0000">*</span>
                                                                                                                </td>
                                                                                                                <td style="width: 5px">
                                                                                                                    :
                                                                                                                </td>
                                                                                                                <td class="formtext">
                                                                                                                    <table style="width: 127px">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:TextBox ID="txtCORPDLNegara" runat="server" CssClass="textBox" Width="167px"
                                                                                                                                        Enabled="False"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    <asp:ImageButton ID="imgCORPDLNegara" runat="server" ImageUrl="~/Images/button/browse.gif"
                                                                                                                                        CausesValidation="False" OnClientClick="javascript:popWinNegara();"></asp:ImageButton>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                    <asp:HiddenField ID="hfCORPDLNegara" runat="server"></asp:HiddenField>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formText">
                                                                                                    Alamat Korporasi Luar Negeri
                                                                                                </td>
                                                                                                <td style="width: 5px">
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formtext" colspan="3">
                                                                                                    <table>
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td class="formtext">
                                                                                                                    &nbsp; &nbsp;&nbsp;
                                                                                                                </td>
                                                                                                                <td style="width: 69px" class="formText">
                                                                                                                    Nama Jalan
                                                                                                                </td>
                                                                                                                <td style="width: 5px">
                                                                                                                    :
                                                                                                                </td>
                                                                                                                <td class="formtext">
                                                                                                                    <asp:TextBox ID="txtCORPLNNamaJalan" runat="server" CssClass="textBox" Width="370px"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtext">
                                                                                                                </td>
                                                                                                                <td style="width: 69px" class="formText">
                                                                                                                    Negara<span style="color: #cc0000">*</span>
                                                                                                                </td>
                                                                                                                <td style="width: 5px">
                                                                                                                    :
                                                                                                                </td>
                                                                                                                <td class="formtext">
                                                                                                                    <table style="width: 127px">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:TextBox ID="txtCORPLNNegara" runat="server" CssClass="textBox" Width="167px"
                                                                                                                                        Enabled="False"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    <asp:ImageButton ID="imgCORPLNNegara" runat="server" ImageUrl="~/Images/button/browse.gif"
                                                                                                                                        CausesValidation="False" OnClientClick="javascript:popWinNegara();"></asp:ImageButton>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                    <asp:HiddenField ID="hfCORPLNNegara" runat="server"></asp:HiddenField>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtext">
                                                                                                                </td>
                                                                                                                <td style="width: 69px" class="formText">
                                                                                                                    Provinsi
                                                                                                                </td>
                                                                                                                <td style="width: 5px">
                                                                                                                    :
                                                                                                                </td>
                                                                                                                <td class="formtext" valign="top">
                                                                                                                    <table style="width: 127px">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td style="height: 22px">
                                                                                                                                    <asp:TextBox ID="txtCORPLNProvinsi" runat="server" CssClass="textBox" Width="167px"
                                                                                                                                        Enabled="False"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                                <td style="height: 22px">
                                                                                                                                    <asp:ImageButton ID="imgCORPLNProvinsi" runat="server" ImageUrl="~/Images/button/browse.gif"
                                                                                                                                        CausesValidation="False" OnClientClick="javascript:popWinProvinsi();"></asp:ImageButton>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                    <asp:HiddenField ID="hfCORPLNProvinsi" runat="server"></asp:HiddenField>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtext">
                                                                                                                </td>
                                                                                                                <td style="width: 69px" class="formText">
                                                                                                                    Kota
                                                                                                                </td>
                                                                                                                <td style="width: 5px">
                                                                                                                    :
                                                                                                                </td>
                                                                                                                <td class="formtext">
                                                                                                                    <table style="width: 127px">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:TextBox ID="txtCORPLNKota" runat="server" CssClass="textBox" Width="167px" Enabled="False"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    <asp:ImageButton ID="imgCORPLNKota" runat="server" ImageUrl="~/Images/button/browse.gif"
                                                                                                                                        CausesValidation="False" OnClientClick="javascript:popWinKotaKab();"></asp:ImageButton>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                    <asp:HiddenField ID="hfCORPLNKota" runat="server"></asp:HiddenField>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="formtext">
                                                                                                                </td>
                                                                                                                <td style="width: 69px" class="formText">
                                                                                                                    Kode Pos<span style="color: #cc0000">*</span>
                                                                                                                </td>
                                                                                                                <td style="width: 5px">
                                                                                                                    :
                                                                                                                </td>
                                                                                                                <td class="formtext">
                                                                                                                    <asp:TextBox ID="txtCORPLNKodePos" runat="server" CssClass="textBox"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="formText">
                                                                                                    Nomor Pokok Wajib Pajak (NPWP)
                                                                                                </td>
                                                                                                <td style="width: 5px">
                                                                                                    :
                                                                                                </td>
                                                                                                <td class="formtext">
                                                                                                    <asp:TextBox ID="txtCORPNPWP" runat="server" CssClass="textBox" Width="257px"></asp:TextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </ajax:AjaxPanel>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 6px" valign="middle" align="left" width="100%" background="Images/search-bar-background.gif">
                                                            <table cellspacing="0" cellpadding="0" border="0">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="formtext">
                                                                            <asp:Label ID="Label3" runat="server" Text="TRANSAKSI" Font-Bold="True"></asp:Label>&nbsp;
                                                                        </td>
                                                                        <td>
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr id="Transaksi">
                                                        <td bgcolor="#ffffff" colspan="2">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <table width="100%" id="TblEditTransaksi" runat="server">
                                                                            <tr>
                                                                                <td width="25%">
                                                                                    Tanggal Transaksi<span style="color: #cc0000">*</span>
                                                                                </td>
                                                                                <td width="1px">
                                                                                    :
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="TxtTanggalTransaksi" runat="server" CssClass="searcheditbox"></asp:TextBox><input
                                                                                        id="popUpTanggalTransaksi" title="Click to show calendar" style="border-right: #ffffff 0px solid;
                                                                                        border-top: #ffffff 0px solid; font-size: 11px; border-left: #ffffff 0px solid;
                                                                                        border-bottom: #ffffff 0px solid; height: 17px; background-image: url(Script/Calendar/cal.gif);
                                                                                        width: 16px;" type="button" runat="server" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td width="25%">
                                                                                    Tipe Transaksi<span style="color: #cc0000">*</span>
                                                                                </td>
                                                                                <td width="1px">
                                                                                    :
                                                                                </td>
                                                                                <td>
                                                                                    <asp:RadioButtonList ID="RblTipeTransaksi" runat="server" RepeatDirection="Horizontal">
                                                                                        <asp:ListItem Value="D">Tarik</asp:ListItem>
                                                                                        <asp:ListItem Value="C">Setor</asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td width="25%">
                                                                                    Nomor Tiket Transaksi<span style="color: #cc0000">*</span>
                                                                                </td>
                                                                                <td width="1px">
                                                                                    :
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="TxtNomorTiketTransaksi" runat="server" CssClass="searcheditbox"
                                                                                        MaxLength="50" Width="246px"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td width="25%">
                                                                                    Tujuan Transaksi<span style="color: #cc0000">*</span>
                                                                                </td>
                                                                                <td width="1px">
                                                                                    :
                                                                                </td>
                                                                                <td>
                                                                                    <asp:DropDownList ID="DdlTujuanTransaksi" runat="server" CssClass="searcheditcbo"
                                                                                        AutoPostBack="True">
                                                                                    </asp:DropDownList>
                                                                                    <asp:TextBox ID="TxtOtherTujuanTransaksi" runat="server" CssClass="searcheditbox"
                                                                                        MaxLength="50" Visible="False" Width="246px"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td width="25%">
                                                                                    Relasi dengan Pemilik Rekening<span style="color: #cc0000">*</span>
                                                                                </td>
                                                                                <td width="1px">
                                                                                    :
                                                                                </td>
                                                                                <td>
                                                                                    <asp:DropDownList ID="DdlRelasiDenganPemilikRekening" runat="server" CssClass="searcheditcbo"
                                                                                        AutoPostBack="True">
                                                                                    </asp:DropDownList>
                                                                                    <asp:TextBox ID="TxtOtherRelasiDenganPemilikRekening" runat="server" CssClass="searcheditbox"
                                                                                        MaxLength="50" Visible="False" Width="246px"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td width="25%">
                                                                                    &nbsp;
                                                                                </td>
                                                                                <td width="1px">
                                                                                    &nbsp;
                                                                                </td>
                                                                                <td>
                                                                                    &nbsp;
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td width="25%" colspan="3">
                                                                                    <asp:ImageButton ID="BtnSave" runat="server" ImageUrl="~/Images/button/save.gif" />
                                                                                    <asp:ImageButton ID="BtnCancel" runat="server" ImageUrl="~/Images/button/cancel.gif" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr id="TrNewTrx" runat="server">
                                                                    <td>
                                                                        <asp:ImageButton ID="BtnNewTrx" runat="server" ImageUrl="~/Images/button/newdata.gif" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td bgcolor="#ffffff" colspan="2">
                                                                        <asp:DataGrid ID="GridViewTransaction" runat="server" AllowPaging="True" AllowSorting="True"
                                                                            AutoGenerateColumns="False" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None"
                                                                            BorderWidth="1px" CellPadding="4" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                                            Font-Size="XX-Small" Font-Strikeout="False" Font-Underline="False" ForeColor="Black"
                                                                            GridLines="Vertical" HorizontalAlign="Left" Width="100%">
                                                                            <AlternatingItemStyle BackColor="White" />
                                                                            <Columns>
                                                                                <asp:BoundColumn DataField="PK_CustomerCTRTransaction_ID" Visible="False">
                                                                                    <HeaderStyle Width="0%" />
                                                                                </asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="FK_TujuanTransaksi_ID" Visible="False"></asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="FK_RelasiDenganPemilikRekening_ID" Visible="False"></asp:BoundColumn>
                                                                                <asp:TemplateColumn HeaderText="No.">
                                                                                    <HeaderStyle ForeColor="White" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:BoundColumn DataField="TransactionDate" DataFormatString="{0:yyyy-MM-dd}" HeaderText="Transaction Date"
                                                                                    SortExpression="TransactionDate  desc">
                                                                                    <HeaderStyle ForeColor="White" />
                                                                                </asp:BoundColumn>
                                                                                <asp:TemplateColumn HeaderText="Transaction Type" SortExpression="TransactionType  desc">
                                                                                    <EditItemTemplate>
                                                                                        <asp:TextBox runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TransactionType") %>'></asp:TextBox>
                                                                                    </EditItemTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LblTransactiontype" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TransactionType") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                    <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                        Font-Underline="False" ForeColor="White" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:BoundColumn DataField="TransactionTicketNumber" HeaderText="TransactionTicketNumber"
                                                                                    SortExpression="TransactionTicketNumber  desc">
                                                                                    <HeaderStyle ForeColor="White" />
                                                                                </asp:BoundColumn>
                                                                                <asp:TemplateColumn HeaderText="Tujuan Transaksi">
                                                                                    <HeaderStyle ForeColor="White" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LblTujuanTransaksi" runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                    <EditItemTemplate>
                                                                                        <asp:TextBox runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.NamaTujuanTransaksi") %>'></asp:TextBox>
                                                                                    </EditItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderText="Relasi Dengan Pemilik Rekening">
                                                                                    <HeaderStyle ForeColor="White" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="LblRelasiDenganPemilikRekening" runat="server"></asp:Label>
                                                                                    </ItemTemplate>
                                                                                    <EditItemTemplate>
                                                                                        <asp:TextBox runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.NamaRelasiDenganPemilikRekening") %>'></asp:TextBox>
                                                                                    </EditItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:BoundColumn DataField="CreatedDate" DataFormatString="{0:yyyy-MM-dd}" HeaderText="Input Date"
                                                                                    SortExpression="CreatedDate  desc">
                                                                                    <HeaderStyle ForeColor="White" />
                                                                                </asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="LastUpdateDate" HeaderText="Update Date" DataFormatString="{0:yyyy-MM-dd}"
                                                                                    SortExpression="LastUpdateDate  desc">
                                                                                    <HeaderStyle ForeColor="White" />
                                                                                </asp:BoundColumn>
                                                                                <asp:ButtonColumn CommandName="Edit" Text="Edit"></asp:ButtonColumn>
                                                                                <asp:TemplateColumn>
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="delbutton" runat="server" CausesValidation="false" CommandName="Delete"
                                                                                            OnClick="delbutton_Click" Text="Delete"></asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                            </Columns>
                                                                            <FooterStyle BackColor="#CCCC99" />
                                                                            <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                                                            <ItemStyle BackColor="#F7F7DE" />
                                                                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages" />
                                                                            <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                                        </asp:DataGrid>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </asp:View>
                                        <asp:View ID="vwMessage" runat="server">
                                            <table width="100%" style="horiz-align: center;">
                                                <tr>
                                                    <td class="formtext" align="center">
                                                        <asp:Label runat="server" ID="lblMsg"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="formtext" align="center">
                                                        <asp:ImageButton ID="imgOKMsg" runat="server" ImageUrl="~/Images/button/Ok.gif" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:View>
                                    </asp:MultiView>
                                </ajax:AjaxPanel>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <ajax:AjaxPanel ID="AjaxPanel5" runat="server">
                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td background="Images/button-bground.gif" align="left" valign="middle">
                                <img src="Images/blank.gif" width="5" height="1" />
                            </td>
                            <td background="Images/button-bground.gif" align="left" valign="middle">
                                <img src="images/arrow.gif" width="15" height="15" />&nbsp;
                            </td>
                            <td background="Images/button-bground.gif" style="width: 5px">
                                &nbsp;
                            </td>
                            <td background="Images/button-bground.gif">
                                <asp:ImageButton ID="ImageSave" runat="server" ImageUrl="~/Images/Button/Save.gif">
                                </asp:ImageButton>&nbsp;
                            </td>
                            <td background="Images/button-bground.gif" style="width: 62px">
                                <asp:ImageButton ID="ImageCancel" runat="server" CausesValidation="False" ImageUrl="~/Images/Button/Cancel.gif">
                                </asp:ImageButton>
                            </td>
                            <td width="99%" background="Images/button-bground.gif">
                                <img src="Images/blank.gif" width="1" height="1" />
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </ajax:AjaxPanel>
            </td>
        </tr>
    </table>
    <asp:CustomValidator ID="cvalPageErr" runat="server" Display="None"></asp:CustomValidator>
</asp:Content>
