<%@ Page Language="VB" MasterPageFile="~/masterpage.master" AutoEventWireup="false" CodeFile="CaseManagementIssueFollowUpAdd.aspx.vb" Inherits="CaseManagementIssueFollowUpAdd" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
  <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
        height="72" style="border-top-style: none; border-right-style: none; border-left-style: none;
        border-bottom-style: none" width="100%">
        <tr>
            <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Case Management Issue - Action
                    <hr />
                </strong>
                <asp:Label ID="LblSuccess" runat="server" CssClass="validationok" Visible="False"
                    Width="100%"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" colspan="4" style="border-top-style: none; border-right-style: none;
                border-left-style: none; height: 24px; border-bottom-style: none">
                <span style="color: #000000">* Required</span></td>
        </tr>
    </table>
    <table bgcolor="#dddddd" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
        height="72" style="color: #000000; border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
                <br />
            </td>
            <td bgcolor="#ffffff" style="color: #000000; height: 24px" width="20%">
                Issue Title</td>
            <td bgcolor="#ffffff" style="height: 24px" width="5">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px" width="80%" valign="top">
                <asp:Label ID="LabelIssueTitle" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" rowspan="6" style="width: 24px; height: 24px">
                <br />
                <br />
                <br />
                <br />
                &nbsp;</td>
            <td bgcolor="#ffffff" rowspan="6" style="height: 24px">
                Description<br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" rowspan="6" style="height: 24px">
                :<br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" rowspan="6" style="height: 24px" valign="top">
                <asp:Label ID="LabelDescription" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText">
        </tr>
        <tr class="formText">
        </tr>
        <tr class="formText">
        </tr>
        <tr class="formText">
        </tr>
        <tr class="formText">
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" rowspan="1" style="width: 24px; height: 24px">
            </td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px">
                Issue Status</td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px">
                :</td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px">
                <asp:Label ID="LabelStatus" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" rowspan="1" style="width: 24px; height: 24px">
            </td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px">
            </td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px">
            </td>
            <td bgcolor="#ffffff" rowspan="1" style="height: 24px">
            </td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" rowspan="1" style="width: 24px; height: 24px">
            </td>
            <td bgcolor="#ffffff" colspan="3" rowspan="1" style="height: 24px">
                <asp:Panel ID="Panel1" runat="server" GroupingText="Action" Height="100%" Width="100%">
                    &nbsp;<asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="validation"
                        ValidationGroup="AddAction" />
                    &nbsp;
                    <table style="width: 100%">
                        <tr>
                            <td valign="top">
                                Action :</td>
                        </tr>
                        <tr>
                            <td valign="top">
                                &nbsp;
                                <ajax:AjaxPanel ID="AjaxPanel1" runat="server">
                                    <asp:TextBox ID="TextAction" runat="server" Rows="5" TextMode="MultiLine" Width="446px"></asp:TextBox></ajax:AjaxPanel>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextAction"
                                    Display="Dynamic" ErrorMessage="Please Fill Action" ValidationGroup="AddAction">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                &nbsp;<ajax:AjaxPanel ID="AjaxPanel2" runat="server"><asp:ImageButton ID="ImageButtonAddAction" runat="server" SkinID="AddButton" /></ajax:AjaxPanel></td>
                        </tr>
                        <tr>
                            <td>
                                <strong>List Of Action</strong></td>
                        </tr>
                        <tr>
                            <td>
                                <ajax:AjaxPanel ID="AjaxPanel3" runat="server" Width="100%"><asp:GridView ID="GridViewAction" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                    EmptyDataText="No Record Action" ForeColor="#333333" GridLines="None" DataSourceID="SqlDataSource1" DataKeyNames="PK_MapCaseManagementIssueFollowUp" >
                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"  />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Date" SortExpression="CreatedDate">
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("CreatedDate", "{0:dd MMM yyyy HH:mm}") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action" SortExpression="Action">
                                            <EditItemTemplate>
                                                <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="validation"
                                                    ValidationGroup="EditAction" />
                                                <asp:TextBox ID="textActionGrid" runat="server" 
                                                    Text='<%# Bind("Action") %>' Rows="5" TextMode="MultiLine" Width="446px"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="textActionGrid"
                                                    Display="Dynamic" ErrorMessage="Please Fill Action" ValidationGroup="EditAction">*</asp:RequiredFieldValidator>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="Label2" runat="server" Text='<%# Bind("Action") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="UserID" SortExpression="UserID">
                                            <EditItemTemplate>
                                                <asp:Label ID="Label4" runat="server" Text='<%# Bind("UserID") %>'></asp:Label>                                                
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="LabelUserID" runat="server" Text='<%# Bind("UserID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
<%--                                        <asp:TemplateField ShowHeader="False">
                                            <EditItemTemplate>
                                                <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update"
                                                    Text="Update" ValidationGroup="EditAction"></asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel"
                                                    Text="Cancel"></asp:LinkButton>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="LinkEdit" runat="server" CausesValidation="False" CommandName="Edit"
                                                    Text="Edit"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ShowHeader="False">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="LinkDelete" runat="server" CausesValidation="False" CommandName="Delete"
                                                    Text="Delete"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                    </Columns>
                                    <RowStyle BackColor="#EFF3FB" />
                                    <EditRowStyle BackColor="#2461BF" />
                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <AlternatingRowStyle BackColor="White" />
                                </asp:GridView>
                                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:AMLConnectionString %>"
                                        DeleteCommand="DELETE FROM MapCaseManagementIssueFollowUpAction WHERE (PK_MapCaseManagementIssueFollowUp = @PK_MapCaseManagementIssueFollowUp)"
                                        InsertCommand="INSERT INTO MapCaseManagementIssueFollowUpAction(FK_MapCaseManagementIssueID, Action, CreatedDate, FK_UserID) VALUES (@FK_MapCaseManagementIssueID, @Action, @CreatedDate, @FK_UserID)"
                                        SelectCommand="SELECT [User].pkUserID,[User].UserID, MapCaseManagementIssueFollowUpAction.PK_MapCaseManagementIssueFollowUp, MapCaseManagementIssueFollowUpAction.FK_MapCaseManagementIssueID, MapCaseManagementIssueFollowUpAction.Action, MapCaseManagementIssueFollowUpAction.CreatedDate FROM MapCaseManagementIssueFollowUpAction INNER JOIN [User] ON MapCaseManagementIssueFollowUpAction.FK_UserID = [User].pkUserID WHERE (MapCaseManagementIssueFollowUpAction.FK_MapCaseManagementIssueID = @FK_MapCaseManagementIssueID)"
                                        UpdateCommand="UPDATE MapCaseManagementIssueFollowUpAction SET Action = @Action, CreatedDate = @CreatedDate WHERE (PK_MapCaseManagementIssueFollowUp = @PK_MapCaseManagementIssueFollowUp)">
                                        <DeleteParameters>
                                            <asp:Parameter Name="PK_MapCaseManagementIssueFollowUp" />
                                        </DeleteParameters>
                                        <UpdateParameters>
                                            <asp:Parameter Name="Action" />
                                            <asp:Parameter Name="CreatedDate" />
                                            <asp:Parameter Name="PK_MapCaseManagementIssueFollowUp" />
                                        </UpdateParameters>
                                        <SelectParameters>
                                            <asp:QueryStringParameter Name="FK_MapCaseManagementIssueID" QueryStringField="PK_MapCaseManagementIssueID" />
                                        </SelectParameters>
                                        <InsertParameters>
                                            <asp:Parameter Name="FK_MapCaseManagementIssueID" />
                                            <asp:Parameter Name="Action" />
                                            <asp:Parameter Name="CreatedDate" />
                                            <asp:Parameter Name="FK_UserID" />
                                        </InsertParameters>
                                    </asp:SqlDataSource>
                                </ajax:AjaxPanel>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr bgcolor="#dddddd" class="formText" height="30">
            <td style="width: 24px">
                <img height="15" src="images/arrow.gif" width="15" /></td>
            <td colspan="3" style="height: 9px">
                <table border="0" cellpadding="3" cellspacing="0">
                    <tr>
                        <td>
                            <asp:ImageButton ID="ImageBack" runat="server" CausesValidation="True" SkinID="BackButton" /></td>
                        <td>
                            </td>
                    </tr>
                </table>
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
        </tr>
    </table>
</asp:Content>

