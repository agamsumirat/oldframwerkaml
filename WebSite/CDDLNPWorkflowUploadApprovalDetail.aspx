<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="CDDLNPWorkflowUploadApprovalDetail.aspx.vb" Inherits="CDDLNPWorkflowUploadApprovalDetail" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">

    <script src="Script/popcalendar.js"></script>

    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td style="width: 3px">
            </td>
            <td id="tdcontent" height="99%" valign="top" bgcolor="#FFFFFF">
                <div>
                    <table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td class="divcontentinside" bgcolor="#FFFFFF">
                                <ajax:AjaxPanel ID="AjaxPanel1" runat="server">
                                    <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
                                        style="border-top-style: none; border-right-style: none; border-left-style: none;
                                        border-bottom-style: none" width="100%">
                                        <tr>
                                            <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                                                border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                <img src="Images/dot_title.gif" width="17" height="17">
                                                <strong>
                                                    <asp:Label ID="Label1" runat="server" Text="EDD Workflow Upload - View"></asp:Label>
                                                </strong>
                                                <hr />
                                            </td>
                                        </tr>
                                    </table>
                                    <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                        bgcolor="#dddddd" border="2">
                                        <tr id="SearchCriteria">
                                            <td valign="top" bgcolor="#ffffff">
                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                    <tr>
                                                        <td nowrap style="background-color: #FFF7E6; width: 15%">
                                                            Requested By</td>
                                                        <td nowrap style="background-color: #ffffff; width: 35%">
                                                            <asp:Label ID="lblRequestedBy" runat="server"></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap="nowrap" style="width: 15%; background-color: #fff7e6">
                                                            Requested Date</td>
                                                        <td nowrap="nowrap" style="width: 35%; background-color: #ffffff">
                                                            <asp:Label ID="lblRequestedDate" runat="server"></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap="nowrap" style="width: 15%; background-color: #fff7e6">
                                                            Mode</td>
                                                        <td nowrap="nowrap" style="width: 35%; background-color: #ffffff">
                                                            <asp:Label ID="lblMode" runat="server">Upload</asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" height="10">
                                                            &nbsp;&nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                        bgcolor="#dddddd" border="2">
                                        <tr id="TR1">
                                            <td valign="top" width="98%" bgcolor="#ffffff">
                                                <table bordercolor="#ffffff" cellspacing="1" cellpadding="0" width="100%" bgcolor="#dddddd"
                                                    border="2">
                                                    <tr>
                                                        <td bgcolor="#ffffff">
                                                            <asp:DataGrid ID="GridViewCDDLNP" runat="server" AutoGenerateColumns="False" Font-Size="XX-Small"
                                                                BackColor="White" CellPadding="4" BorderWidth="1px" BorderStyle="None" AllowPaging="True"
                                                                Width="100%" GridLines="Vertical" AllowSorting="True" BorderColor="#DEDFDE" ForeColor="Black">
                                                                <FooterStyle BackColor="#CCCC99"></FooterStyle>
                                                                <SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#CE5D5A"></SelectedItemStyle>
                                                                <AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
                                                                <ItemStyle BackColor="#F7F7DE"></ItemStyle>
                                                                <HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#6B696B"></HeaderStyle>
                                                                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Visible="false" />
                                                                <Columns>
                                                                    <asp:TemplateColumn Visible="false">
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="CheckBoxExporttoExcel" runat="server" />
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="2%" />
                                                                    </asp:TemplateColumn>
                                                                    <asp:BoundColumn DataField="PK_CDDLNP_WorkflowUpload_ApprovalDetail_id" Visible="false" />
                                                                    <asp:BoundColumn HeaderText="No" ItemStyle-Width="5%" HeaderStyle-ForeColor="white" />
                                                                    <asp:BoundColumn DataField="AccountOwnerId" HeaderText="Account Owner ID" ItemStyle-Width="20%"
                                                                        SortExpression="AccountOwnerId  asc" />
                                                                    <asp:BoundColumn DataField="List_RM" HeaderText="RM" ItemStyle-Width="20%" SortExpression="List_RM  asc" />
                                                                    <asp:BoundColumn DataField="List_TeamHead" HeaderText="Team Head" ItemStyle-Width="20%"
                                                                        SortExpression="List_TeamHead  asc" />
                                                                    <asp:BoundColumn DataField="List_GroupHead" HeaderText="Group Head" ItemStyle-Width="20%"
                                                                        SortExpression="List_GroupHead  asc" />
                                                                    <asp:BoundColumn DataField="List_HeadOff" HeaderText="Head Off" ItemStyle-Width="10%"
                                                                        SortExpression="List_HeadOff  asc" />
                                                                </Columns>
                                                            </asp:DataGrid>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#ffffff">
                                                <table id="Table3" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                    bgcolor="#ffffff" border="2">
                                                </table>
                                                <table id="Table4" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                    bgcolor="#ffffff" border="2">
                                                    <tr bgcolor="#ffffff">
                                                        <td class="regtext" valign="middle" align="left" colspan="11" height="7">
                                                            <hr>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="middle" align="left" width="50%" bgcolor="#ffffff" style="height: 20px">
                                                            Page&nbsp;<asp:Label ID="PageCurrentPage" runat="server" CssClass="regtext">0</asp:Label>&nbsp;of&nbsp;
                                                            <asp:Label ID="PageTotalPages" runat="server" CssClass="regtext">0</asp:Label>
                                                            (<asp:Label ID="PageTotalRows" runat="server">0</asp:Label>
                                                            Records)</td>
                                                        <td valign="middle" align="right" width="50%" bgcolor="#ffffff" style="height: 20px">
                                                            &nbsp;
                                                        </td>
                                                        <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff" style="height: 20px">
                                                            <img height="5" src="images/first.gif" width="6">
                                                        </td>
                                                        <td class="regtext" valign="middle" align="right" width="50" bgcolor="#ffffff" style="height: 20px">
                                                            <asp:LinkButton ID="LinkButtonFirst" runat="server" CssClass="regtext" CommandName="First"
                                                                OnCommand="PageNavigate">First</asp:LinkButton>
                                                        </td>
                                                        <td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff" style="height: 20px">
                                                            <img height="5" src="images/prev.gif" width="6"></td>
                                                        <td class="regtext" valign="middle" align="right" width="50" bgcolor="#ffffff" style="height: 20px">
                                                            <asp:LinkButton ID="LinkButtonPrevious" runat="server" CssClass="regtext" CommandName="Prev"
                                                                OnCommand="PageNavigate">Previous</asp:LinkButton>
                                                        </td>
                                                        <td class="regtext" valign="middle" align="right" width="50" bgcolor="#ffffff" style="height: 20px">
                                                            <asp:LinkButton ID="LinkButtonNext" runat="server" CssClass="regtext" CommandName="Next"
                                                                OnCommand="PageNavigate">Next</asp:LinkButton></td>
                                                        <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff" style="height: 20px">
                                                            <img height="5" src="images/next.gif" width="6"></td>
                                                        <td class="regtext" valign="middle" align="left" width="50" bgcolor="#ffffff" style="height: 20px">
                                                            <asp:LinkButton ID="LinkButtonLast" runat="server" CssClass="regtext" CommandName="Last"
                                                                OnCommand="PageNavigate">Last</asp:LinkButton>
                                                        </td>
                                                        <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff" style="height: 20px">
                                                            <img height="5" src="images/last.gif" width="6"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr runat="server" id="rowCDDLNPReassignment">
                                            <td bgcolor="#ffffff" style="height: 19px">
                                                CDDLNP Reassignment<br />
                                                <asp:DataGrid ID="GridReassignment" runat="server" AllowPaging="True" AllowSorting="True"
                                                    AutoGenerateColumns="False" BorderColor="#003300" BorderStyle="Solid" CellPadding="4"
                                                    Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" ForeColor="White" GridLines="None" Width="100%">
                                                    <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                                                    <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" Wrap="False" />
                                                    <Columns>
                                                        <asp:BoundColumn DataField="Old_RM" HeaderText="OldUser" SortExpression="Old_RM  asc">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="New_RM" HeaderText="NewUser" SortExpression="New_RM  asc">
                                                        </asp:BoundColumn>
                                                    </Columns>
                                                    <EditItemStyle BackColor="#7C6F57" />
                                                    <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                                                    <SelectedItemStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                                                    <AlternatingItemStyle BackColor="White" />
                                                    <ItemStyle BackColor="#E3EAEB" />
                                                </asp:DataGrid></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="3" border="0">
                                                    <tr>
                                                        <td>
                                                            <ajax:AjaxPanel ID="AjaxPanel5" runat="server">
                                                                <asp:ImageButton ID="ImageAccept" runat="server" CausesValidation="True" ImageUrl="~/Images/Button/Accept.gif" /></ajax:AjaxPanel></td>
                                                        <td>
                                                            <ajax:AjaxPanel ID="AjaxPanel7" runat="server">
                                                                <asp:ImageButton ID="ImageReject" runat="server" CausesValidation="True" ImageUrl="~/Images/Button/Reject.gif">
                                                                </asp:ImageButton></ajax:AjaxPanel></td>
                                                        <td>
                                                            <ajax:AjaxPanel ID="AjaxPanel8" runat="server">
                                                                <asp:ImageButton ID="ImageCancel" runat="server" ImageUrl="~/Images/Button/Cancel.gif">
                                                                </asp:ImageButton></ajax:AjaxPanel></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </ajax:AjaxPanel>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    <asp:CustomValidator ID="cvalPageErr" runat="server" Display="None"></asp:CustomValidator>
</asp:Content>
