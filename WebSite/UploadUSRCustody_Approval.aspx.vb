Imports CDDLNPNettier.Entities
Imports CDDLNPNettier.Data
Partial Class UploadUSRCustody_Approval
    Inherits Parent
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("UploadUSRCustody_Approval.Selected") Is Nothing, New ArrayList, Session("UploadUSRCustody_Approval.Selected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("UploadUSRCustody_Approval.Selected") = value
        End Set
    End Property
    Private Property SetnGetValueSearch() As String
        Get
            Return IIf(Session("UploadUSRCustody_Approval.ValueSearch") Is Nothing, "", Session("UploadUSRCustody_Approval.ValueSearch"))
        End Get
        Set(ByVal Value As String)
            Session("UploadUSRCustody_Approval.ValueSearch") = Value
        End Set
    End Property

    Private Property SetnGetFieldSearch() As String
        Get
            Return IIf(Session("UploadUSRCustody_Approval.FieldSearch") Is Nothing, "", Session("UploadUSRCustody_Approval.FieldSearch"))
        End Get
        Set(ByVal Value As String)
            Session("UploadUSRCustody_Approval.FieldSearch") = Value
        End Set
    End Property

    Private Property SetnGetCurrentPage() As Int32
        Get
            Return CType(IIf(Session("UploadUSRCustody_Approval.CurrentPage") Is Nothing, 0, Session("UploadUSRCustody_Approval.CurrentPage")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("UploadUSRCustody_Approval.CurrentPage") = Value
        End Set
    End Property
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    Private Property SetnGetRowTotal() As Int32
        Get
            Return CType(IIf(Session("UploadUSRCustody_Approval.RowTotal") Is Nothing, 0, Session("UploadUSRCustody_Approval.RowTotal")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("UploadUSRCustody_Approval.RowTotal") = Value
        End Set
    End Property

    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("UploadUSRCustody_Approval.Sort") Is Nothing, "PK_AccountUSRCustody_Approval_ID  asc", Session("UploadUSRCustody_Approval.Sort"))
        End Get
        Set(ByVal Value As String)
            Session("UploadUSRCustody_Approval.Sort") = Value
        End Set
    End Property

    Sub ClearSession()
        SetnGetSelectedItem = Nothing
        SetnGetValueSearch = Nothing
        SetnGetFieldSearch = Nothing
        SetnGetCurrentPage = Nothing
        SetnGetRowTotal = Nothing
        SetnGetSort = Nothing
    End Sub
    Private Sub FillSearch()
        Me.ComboSearch.Items.Add(New ListItem("...", ""))
        Me.ComboSearch.Items.Add(New ListItem("Created By", "UserID Like '%-=Search=-%'"))
        Me.ComboSearch.Items.Add(New ListItem("Entry Date", "EntryDate >= '-=Search=- 00:00' AND EntryDate <= '-=Search=- 23:59'"))

    End Sub
    Public Property SetnGetBindTable() As TList(Of AccountUSRCustody_Approval)
        Get
            Dim strWhereClause(-1) As String
            Dim strAllWhereClause As String = ""
            If Me.SetnGetCurrentPage > Me.GetPageTotal - 1 And Me.GetPageTotal - 1 > 0 Then
                Me.SetnGetCurrentPage = Me.GetPageTotal - 1
            End If

            If ComboSearch.SelectedValue <> "" Then
                If ComboSearch.SelectedItem.Text = "Created By" Then
                    ReDim Preserve strWhereClause(strWhereClause.Length)
                    strWhereClause(strWhereClause.Length - 1) = Me.ComboSearch.SelectedValue.Replace("%-=Search=-%", TextSearch.Text)
                End If

                If ComboSearch.SelectedItem.Text = "Entry Date" Then
                    If TextSearch.Text <> "" Then
                        If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", TextSearch.Text.Trim) Then
                            ReDim Preserve strWhereClause(strWhereClause.Length)
                            strWhereClause(strWhereClause.Length - 1) = Me.ComboSearch.SelectedValue.Replace("-=Search=-", Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", TextSearch.Text.Trim).ToString("yyyy-MM-dd"))

                        Else

                            Throw New Exception("Entry Date Must dd-MMM-yyyy")
                        End If
                    Else
                        Throw New Exception("Please Fill Entry Date.")
                    End If
                End If


            End If


            strAllWhereClause = String.Join(" and ", strWhereClause)
            If strAllWhereClause.Trim.Length > 0 Then
                strAllWhereClause += " and UserID <> '" & Sahassa.AML.Commonly.SessionUserId & "' AND UserID IN (SELECT u.UserID FROM [User] u WHERE u.UserID IN (select UserId from UserWorkingUnitAssignment Where WorkingUnitId in (select WorkingUnitId from UserWorkingUnitAssignment where UserId = '" & Sahassa.AML.Commonly.SessionUserId & "')))"
            Else
                strAllWhereClause += " UserID <> '" & Sahassa.AML.Commonly.SessionUserId & "' AND UserID IN (SELECT u.UserID FROM [User] u WHERE u.UserID IN (select UserId from UserWorkingUnitAssignment Where WorkingUnitId in (select WorkingUnitId from UserWorkingUnitAssignment where UserId = '" & Sahassa.AML.Commonly.SessionUserId & "')))"
            End If
            Session("UploadUSRCustody_Approval.Table") = DataRepository.vw_AccountUSRCustody_ApprovalProvider.GetPaged(strAllWhereClause, SetnGetSort, SetnGetCurrentPage, Sahassa.AML.Commonly.GetDisplayedTotalRow, Me.SetnGetRowTotal)

            Return CType(Session("UploadUSRCustody_Approval.Table"), TList(Of AccountUSRCustody_Approval))
        End Get
        Set(ByVal value As TList(Of AccountUSRCustody_Approval))
            Session("UploadUSRCustody_Approval.Table") = value
        End Set
    End Property
    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
            Me.CollectSelected()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = gridRow.FindControl("CheckBoxExporttoExcel")
                Dim UserId As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
                If ArrTarget.Contains(UserId) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(UserId)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(UserId)
                End If
                Me.SetnGetSelectedItem = ArrTarget
            End If
        Next
    End Sub
    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                ClearSession()
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using
                FillSearch()
                Me.ComboSearch.Attributes.Add("onchange", "javascript:Check(this, 2, '" & Me.TextSearch.ClientID & "', '" & Me.popUp.ClientID & "');")


                Me.popUp.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TextSearch.ClientID & "'), 'dd-mmm-yyyy')")
                Me.popUp.Style.Add("display", "none")
            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Sub BindGrid()

        Me.GridMSUserView.DataSource = SetnGetBindTable

        Me.GridMSUserView.CurrentPageIndex = Me.SetnGetCurrentPage
        Me.GridMSUserView.VirtualItemCount = Me.SetnGetRowTotal
        Me.GridMSUserView.DataBind()
    End Sub
    Sub SetinfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.ComboSearch.SelectedValue = Me.SetnGetFieldSearch
            Me.TextSearch.Text = Me.SetnGetValueSearch
        Catch
            Throw
        End Try
    End Sub



    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
            Me.SetinfoNavigate()
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message

        End Try
    End Sub

    Protected Sub GridMSUserView_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridMSUserView.EditCommand
        Try
            Dim pk As Integer = e.Item.Cells(1).Text

            Response.Redirect("UploadHiportAccountApprovalDetail.aspx?PK_AccountUSRCustody_Approval_ID=" & pk, False)

        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub
End Class
