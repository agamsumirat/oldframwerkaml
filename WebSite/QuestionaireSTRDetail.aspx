<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="QuestionaireSTRDetail.aspx.vb" Inherits="QuestionaireSTRDetail"  %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
 <ajax:AjaxPanel runat="server" ID="Ajax1">
        <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
            height="72" style="border-top-style: none; border-right-style: none; border-left-style: none;
            border-bottom-style: none" width="100%">
            <tr>
                <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                    border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                    <strong>Questionaire STR - Detail
                        <hr />
                    </strong>
                    <asp:Label ID="LblSuccess" runat="server" CssClass="validationok" Visible="False"
                        Width="100%"></asp:Label></td>
            </tr>
            <tr class="formText">
                <td bgcolor="#ffffff" colspan="4" style="border-top-style: none; border-right-style: none;
                    border-left-style: none; height: 24px; border-bottom-style: none">
                    <span style="color: #ff0000">* Required</span></td>
            </tr>
        </table>
        <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
            border="2" height="72" style="border-top-style: none; border-right-style: none;
            border-left-style: none; border-bottom-style: none">
            <tr class="formText">
                <td bgcolor="#ffffff" style="width: 24px; height: 24px">
                </td>
                <td width="20%" bgcolor="#ffffff" style="height: 24px">
                    Description Alert STR</td>
                <td width="5" bgcolor="#ffffff" style="height: 24px">
                    :</td>
                <td width="80%" bgcolor="#ffffff" style="height: 24px">
                    &nbsp;<asp:Label ID="LblDescriptionAlertSTR" runat="server"></asp:Label><strong><span
                        style="color: #ff0000"></span></strong></td>
            </tr>
            <tr class="formText">
                <td bgcolor="#ffffff" style="width: 24px; height: 24px">
                </td>
                <td width="20%" bgcolor="#ffffff" style="height: 24px">
                    Question Type</td>
                <td width="5" bgcolor="#ffffff" style="height: 24px">
                    :</td>
                <td width="80%" bgcolor="#ffffff" style="height: 24px">
                    &nbsp;
                    <asp:Label ID="LblQuestiontype" runat="server"></asp:Label><strong><span style="color: #ff0000"></span></strong></td>
            </tr>
            <tr class="formText">
                <td bgcolor="#ffffff" style="width: 24px; height: 24px">
                </td>
                <td width="20%" bgcolor="#ffffff" style="height: 24px">
                    Question No</td>
                <td width="5" bgcolor="#ffffff" style="height: 24px">
                    :</td>
                <td width="80%" bgcolor="#ffffff" style="height: 24px">
                    &nbsp;
                    <asp:Label ID="LblQuestionNo" runat="server"></asp:Label><strong><span style="color: #ff0000"></span></strong></td>
            </tr>
            <tr class="formText">
                <td bgcolor="#ffffff" style="width: 24px; height: 24px">
                </td>
                <td width="20%" bgcolor="#ffffff" style="height: 24px">
                    Question</td>
                <td width="5" bgcolor="#ffffff" style="height: 24px">
                    :</td>
                <td width="80%" bgcolor="#ffffff" style="height: 24px">
                    &nbsp;
                    <asp:TextBox ID="TxtQuestion" runat="server" TextMode="MultiLine" Height="112px"
                        Width="481px" ReadOnly="True"></asp:TextBox><strong><span style="color: #ff0000"></span></strong></td>
            </tr>
             <tr id="Tr1" runat="server" class="formText">
                <td bgcolor="#ffffff" style="width: 24px; height: 24px">
                </td>
                <td bgcolor="#ffffff" style="height: 24px" width="20%">
                    Activation</td>
                <td bgcolor="#ffffff" style="height: 24px" width="5">
                    :</td>
                <td bgcolor="#ffffff" style="height: 24px" width="80%">
                    <asp:Label ID="LblActivationValue" runat="server"></asp:Label></td>
            </tr>
            <tr class="formText" id="cde" runat="server">
                <td bgcolor="#ffffff" style="width: 24px; height: 24px">
                </td>
                <td bgcolor="#ffffff" style="height: 24px" width="20%">
                </td>
                <td bgcolor="#ffffff" style="height: 24px" width="5">
                </td>
                <td bgcolor="#ffffff" style="height: 24px" width="80%">
                    <asp:DataGrid ID="gridView" runat="server" AllowPaging="True" AllowSorting="True"
                        AutoGenerateColumns="False" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None"
                        BorderWidth="1px" CellPadding="4" Font-Size="XX-Small" ForeColor="Black" GridLines="Vertical"
                        Width="100%">
                        <FooterStyle BackColor="#CCCC99" />
                        <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                        <AlternatingItemStyle BackColor="White" />
                        <ItemStyle BackColor="#F7F7DE" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="No." Visible="False"></asp:TemplateColumn>
                            <asp:BoundColumn DataField="MultipleChoice" HeaderText="Choice" SortExpression="MultipleChoice Desc">
                            </asp:BoundColumn>
                        </Columns>
                        <HeaderStyle BackColor="#6B696B" Font-Bold="True" Font-Size="XX-Small" ForeColor="White" />
                        <PagerStyle Mode="NumericPages" />
                    </asp:DataGrid></td>
            </tr>
           
            
            <tr bgcolor="#ffffff">
                <td colspan="4" valign="top">
                    <table bgcolor="#dddddd" border="0" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
                        width="100%">
                        <tr>
                            <td align="left" background="Images/search-bar-background.gif" style="height: 6px"
                                valign="middle" width="100%">
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="formtext" style="height: 13px; width: 153px;">
                                            <asp:Label ID="Label11" runat="server" Font-Bold="True" Text="Additional Information"></asp:Label>&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="TrAdditionalInfo">
                            <td bgcolor="#ffffff" colspan="2" style="width: 603px">
                                <table id="Table2" bordercolor="#ffffff" cellpadding="2" cellspacing="1" width="96%">
                                    <tr class="formText">
                                        <td nowrap="nowrap" style="width: 20px; height: 17px;">
                                        </td>
                                        <td bgcolor="#fff7e6" width="20%" style="height: 17px">
                                            <asp:Label ID="Label12" runat="server" Text="Created By/Date"></asp:Label></td>
                                        <td bgcolor="#ffffff" style="width: 5px; height: 17px;">
                                            :</td>
                                        <td bgcolor="#ffffff" width="80%" style="height: 17px">
                                            &nbsp;<asp:Label ID="LblCreatedBy" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr class="formText">
                                        <td nowrap="nowrap" style="width: 20px; height: 30px;">
                                        </td>
                                        <td bgcolor="#fff7e6" valign="top" width="20%" style="height: 30px">
                                            <asp:Label ID="Label14" runat="server" Text="Last Updated By/Date"></asp:Label></td>
                                        <td bgcolor="#ffffff" style="width: 5px; height: 30px;" valign="top">
                                            :</td>
                                        <td bgcolor="#ffffff" width="80%" style="height: 30px">
                                            &nbsp;<asp:Label ID="LblUpdatedBy" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr class="formText">
                                        <td nowrap="nowrap" style="width: 20px">
                                        </td>
                                        <td bgcolor="#fff7e6" width="20%">
                                            <asp:Label ID="Label16" runat="server" Text="Approved By/Date"></asp:Label></td>
                                        <td bgcolor="#ffffff" style="width: 5px">
                                            :</td>
                                        <td bgcolor="#ffffff" width="80%">
                                            &nbsp;<asp:Label ID="LblApprovedBy" runat="server"></asp:Label></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="formText" bgcolor="#dddddd" height="30">
                <td style="width: 24px">
                    <img height="15" src="images/arrow.gif" width="15"></td>
                <td colspan="3" style="height: 9px">
                    <table cellspacing="0" cellpadding="3" border="0">
                        <tr>
                            <td style="height: 36px">
                                </td>
                            <td style="height: 36px; width: 35px;">
                                <asp:ImageButton ID="ImageCancel" runat="server" CausesValidation="False" SkinID="BackButton">
                                </asp:ImageButton></td>
                        </tr>
                    </table>
                    <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
            </tr>
        </table>
    </ajax:AjaxPanel>
</asp:Content>
