Imports system.Data.SqlClient
Imports sahassa.AML.TableAdapterHelper
Partial Class AdvancedRulesDelete
    Inherits Parent

    Private ReadOnly Property AdvancedRulesID() As Integer
        Get
            Dim temp As String
            temp = Request.Params("AdvancedRulesID")
            If Not IsNumeric(temp) OrElse temp = "" Then
                Throw New Exception("Advanced Rules not valid")
            Else
                Return temp
            End If
        End Get
    End Property
    Public ReadOnly Property oRowAdvancedRules() As AMLDAL.RulesAdvanced.RulesAdvancedRow
        Get
            Using adapter As New AMLDAL.RulesAdvancedTableAdapters.RulesAdvancedTableAdapter
                If adapter.GetDataByRulesAdvancedID(Me.AdvancedRulesID).Rows.Count > 0 Then
                    Return adapter.GetDataByRulesAdvancedID(Me.AdvancedRulesID).Rows(0)
                Else
                    Return Nothing
                End If

            End Using
        End Get
    End Property
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "AdvancedRulesView.aspx"

        Me.Response.Redirect("AdvancedRulesView.aspx", False)

    End Sub
#Region "Delete Advanced Rules"
    Private Sub DeleteAdvancedRulesBySU()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try

            Using adapter As New AMLDAL.RulesAdvancedTableAdapters.RulesAdvancedTableAdapter
                oSQLTrans = BeginTransaction(adapter)
                adapter.DeleteRulesAdvancedByPK(Me.AdvancedRulesID)
            End Using
            InsertAuditTrailDelete(oSQLTrans)
            oSQLTrans.Commit()
            Response.Redirect("AdvancedRulesView.aspx", False)
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Rollback()
            End If
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub
    Private Sub InsertAuditTrailDelete(ByRef oSQLTrans As SqlTransaction)
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(5)

            If Not Me.oRowAdvancedRules Is Nothing Then
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                    SetTransaction(AccessAudit, oSQLTrans)
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Advanced Rule Name", "Delete", "", Me.oRowAdvancedRules.RulesAdvancedName, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Description", "Delete", "", Me.oRowAdvancedRules.RulesAdvancedDescription, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Enabled", "Delete", "", Me.oRowAdvancedRules.Enabled, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Case Grouping By", "Delete", "", Me.oRowAdvancedRules.CaseGroupingBy, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Advanced Rules", "Expression", "Delete", "", Me.oRowAdvancedRules.Expression, "Accepted")


                End Using
            End If
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' menyimpan data advanced rules ke pending approval
    ''' </summary>
    ''' <remarks>
    ''' Step
    ''' 1.insert header
    ''' 2.insert detail
    ''' </remarks>
    Private Sub InsertDataAdvancedRulesToPendingApproval()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            Dim intHeader As Integer
            Using adapter As New AMLDAL.RulesAdvancedTableAdapters.RulesAdvancedPendingApprovalTableAdapter
                oSQLTrans = BeginTransaction(adapter)
                intHeader = adapter.InsertRulesAdvancedPendingApproval(oRowAdvancedRules.RulesAdvancedName, Sahassa.AML.Commonly.SessionUserId, 3, "Advanced Rules Delete", Now)

            End Using
            Using adapter As New AMLDAL.RulesAdvancedTableAdapters.RulesAdvancedApprovalTableAdapter
                SetTransaction(adapter, oSQLTrans)
                adapter.Insert(intHeader, oRowAdvancedRules.RulesAdvancedId, oRowAdvancedRules.RulesAdvancedName, oRowAdvancedRules.RulesAdvancedDescription, oRowAdvancedRules.Enabled, oRowAdvancedRules.CaseGroupingBy, oRowAdvancedRules.Expression, oRowAdvancedRules.CreatedDate, oRowAdvancedRules.IsCreatedBySU, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
            End Using
            oSQLTrans.Commit()
            Dim MessagePendingID As Integer = 81403 'MessagePendingID 81403 = Advanced Rules Delete

            Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & oRowAdvancedRules.RulesAdvancedName
            Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & oRowAdvancedRules.RulesAdvancedName, False)
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub
#End Region
    ''' <summary>
    ''' 1.cek data rulesAdvanced Masih ada ngak di table
    ''' 2.cek data rulesAdvanced ngak boleh ada di table approval
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function DataValidToDelete() As Boolean
        Try
            'Step 1
            If oRowAdvancedRules Is Nothing Then
                Try
                    Throw New Exception("Cannot delete Advanced Rules Name because that advanced rules name does not exist in the database anymore. ")
                Catch ex As Exception
                    cvalPageError.IsValid = False
                    cvalPageError.ErrorMessage = ex.Message
                    Return False
                End Try
            End If
            'Step 2
            Using adapter As New AMLDAL.RulesAdvancedTableAdapters.RulesAdvancedPendingApprovalTableAdapter
                If adapter.GetCountRulesAdvancePendingByUniqueKey(oRowAdvancedRules.RulesAdvancedName) > 0 Then
                    Try
                        Throw New Exception("Cannot delete the following Advanced Rules : " & oRowAdvancedRules.RulesAdvancedName & " Name because it is currently waiting for approval.")
                    Catch ex As Exception
                        cvalPageError.IsValid = False
                        cvalPageError.ErrorMessage = ex.Message
                        Return False
                    End Try
                End If
            End Using
            
            Return True
        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Sub LoadData()
        Try
            If Not oRowAdvancedRules Is Nothing Then
                Me.LabelAdvancedRulesName.Text = oRowAdvancedRules.RulesAdvancedName
                Me.LabelAdvancedRulesDescription.Text = oRowAdvancedRules.RulesAdvancedDescription
                Me.LabelEnabled.Text = oRowAdvancedRules.Enabled.ToString
                Me.LabelAdvancedRulesCaseGroupingBy.Text = oRowAdvancedRules.CaseGroupingBy
                Me.LabelExpression.Text = oRowAdvancedRules.Expression
            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' Delete 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click

        Try
            If Page.IsValid Then
                If DataValidToDelete Then
                    If Sahassa.AML.Commonly.SessionUserId.ToLower = "superuser" Then
                        DeleteAdvancedRulesBySU()
                    Else
                        InsertDataAdvancedRulesToPendingApproval()
                    End If
                End If
            End If
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                LoadData()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

              
                End Using
            End If
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class
