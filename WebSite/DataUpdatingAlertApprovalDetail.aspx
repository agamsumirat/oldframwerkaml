﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="DataUpdatingAlertApprovalDetail.aspx.vb" Inherits="DataUpdatingAlertApprovalDetail" %>

    <%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
    <asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
        <span defaultbutton="ImgBackAdd">
            <script src="Script/popcalendar.js"></script>
            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr>
                    <td>
                    </td>
                    <td id="tdcontent" height="99%" valign="top" bgcolor="#FFFFFF">
                        <div id="divcontent" class="divcontent">
                            <table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
                                <tr>
                                    <td class="divcontentinside" bgcolor="#FFFFFF">
                                        <ajax:AjaxPanel ID="AjaxPanel5" runat="server" Width="100%">
                                            <table style="width: 100%" bgcolor="#dddddd">
                                                <tr bgcolor="#ffffff">
                                                    <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                                                        border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                                                        <strong>
                                                            <img src="Images/dot_title.gif" width="17" height="17">
                                                            <asp:Label ID="Label1" runat="server" Text="Data Updating Alert Approval Detail"></asp:Label>
                                                            <hr />
                                                        </strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    </td>
                                                </tr>
                                                <tr bgcolor="#ffffff">
                                                    <td style="width: 10%; height: 18px">
                                                        <asp:Label ID="Label2" runat="server" Text="Request By"></asp:Label>
                                                    </td>
                                                    <td style="width: 1px">
                                                        :
                                                    </td>
                                                    <td style="width: 90%">
                                                        <asp:Label ID="LblRequestBy" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr bgcolor="#ffffff">
                                                    <td style="width: 10%; height: 18px">
                                                        <asp:Label ID="Label3" runat="server" Text="Request Date"></asp:Label>
                                                    </td>
                                                    <td style="width: 1px">
                                                        :
                                                    </td>
                                                    <td style="width: 90%">
                                                        <asp:Label ID="LblRequestDate" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr bgcolor="#ffffff">
                                                    <td style="width: 10%; height: 18px">
                                                        <asp:Label ID="LblMode" runat="server" Text="Mode"></asp:Label>
                                                    </td>
                                                    <td style="width: 1px">
                                                        :
                                                    </td>
                                                    <td style="width: 90%">
                                                        <asp:Label ID="LblAction" runat="server" Text="Mode"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr bgcolor="#ffffff">
                                                    <td colspan="3">
                                                        <table width="100%">
                                                            <tr>
                                                                <td style="vertical-align: top;">
                                                                    <asp:Panel ID="PanelOld" runat="server" Width="100%">
                                                                        <table style="width: 100%">
                                                                            <tr>
                                                                                <td colspan="4" valign="top">
                                                                                    <asp:Label ID="LblOldValue" runat="server" Text="Old Value" Font-Bold="True" Font-Size="Medium"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="4" valign="top">
                                                                                    <table width="100%" cellpadding="1" cellspacing="2">
                                                                                        <tr>
                                                                                            <td width="29%">
                                                                                                <asp:CheckBox Text="High Risk Customer" runat="server" ID="ChkHighRiskCustomer" 
                                                                                                    Enabled="False" />
                                                                                            </td>
                                                                                            <td width="1">
                                                                                                :
                                                                                            </td>
                                                                                            <td width="79%">
                                                                                                <asp:Label Text="" runat="server" ID="LblYearHighRiskcustomer" /> Year On Opening CIF
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td width="29%">
                                                                                                <asp:CheckBox Text="Medium Risk Customer" runat="server" ID="ChkMediumRiskCustomer" 
                                                                                                    Enabled="False" /></td>
                                                                                            <td width="1">
                                                                                                :</td>
                                                                                            <td width="79%">
                                                                                                <asp:Label runat="server" ID="LblYearMediumRiskCustomer" />
                                                                                                Year On Opening CIF</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td width="29%">
                                                                                                <span defaultbutton="ImgBackAdd">
                                                                                                <asp:CheckBox ID="ChkNonHighRiskCustomer" runat="server" 
                                                                                                    Text="Non High Risk Customer" Enabled="False" />
                                                                                                </span>
                                                                                            </td>
                                                                                            <td width="1">
                                                                                                :
                                                                                            </td>
                                                                                            <td width="79%">
                                                                                                <asp:Label Text="" runat="server" ID="LblYearNonHighRiskCustomer" /> Year On Opening CIF
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </asp:Panel>
                                                                </td>
                                                                <td style="vertical-align: top;">
                                                                    <asp:Panel ID="PanelNew" runat="server" Width="100%">
                                                                        <table style="width: 100%">
                                                                            <tr>
                                                                                <td colspan="4" valign="top">
                                                                                    <asp:Label ID="LblNewValue" runat="server" Text="New Value" Font-Bold="True" Font-Size="Medium"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="4" valign="top">
                                                                                    &nbsp;
                                                                                <span defaultbutton="ImgBackAdd">
                                                                                    <table cellpadding="1" cellspacing="2" width="100%">
                                                                                        <tr>
                                                                                            <td width="29%">
                                                                                                <asp:CheckBox ID="ChkHighRiskCustomerNew" runat="server" 
                                                                                                    Text="High Risk Customer" Enabled="False" />
                                                                                            </td>
                                                                                            <td width="1px">
                                                                                                :
                                                                                            </td>
                                                                                            <td width="79%">
                                                                                                <asp:Label ID="LblYearHighRiskcustomerNew" runat="server" Text="" />
                                                                                                Year On Opening CIF
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td width="29%">
                                                                                                <asp:CheckBox ID="ChkMediumRiskCustomerNew" runat="server" 
                                                                                                    Text="Medium Risk Customer" Enabled="False" /></td>
                                                                                            <td width="1">
                                                                                            </td>
                                                                                            <td width="79%">
                                                                                                <asp:Label runat="server" ID="LblYearMediumRiskCustomerNew" />
                                                                                                Year On Opening CIF</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td width="29%">
                                                                                                <span defaultbutton="ImgBackAdd">
                                                                                                <asp:CheckBox ID="ChkNonHighRiskCustomerNew" runat="server" 
                                                                                                    Text="Non High Risk Customer" Enabled="False" />
                                                                                                </span>
                                                                                            </td>
                                                                                            <td width="1px">
                                                                                                :
                                                                                            </td>
                                                                                            <td width="79%">
                                                                                                <asp:Label ID="LblYearNonHighRiskCustomerNew" runat="server" Text="" />
                                                                                                Year On Opening CIF
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                    </span>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </asp:Panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ajax:AjaxPanel>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <ajax:AjaxPanel ID="AjaxPanel1" runat="server">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="left" background="Images/button-bground.gif" valign="middle">
                                        <img height="1" src="Images/blank.gif" width="5" />
                                    </td>
                                    <td align="left" background="Images/button-bground.gif" valign="middle">
                                        <img height="15" src="images/arrow.gif" width="15" />&nbsp;
                                    </td>
                                    <td background="Images/button-bground.gif">
                                        &nbsp;<asp:ImageButton ID="ImgBtnAccept" runat="server" ImageUrl="~/images/button/accept.gif" />
                                    </td>
                                    <td background="Images/button-bground.gif">
                                        &nbsp;<asp:ImageButton ID="ImgBtnReject" runat="server" ImageUrl="~/images/button/reject.gif" />
                                    </td>
                                    <td background="Images/button-bground.gif">
                                        &nbsp;<asp:ImageButton ID="ImgBackAdd" runat="server" ImageUrl="~/images/button/back.gif"
                                            CausesValidation="False" />
                                    </td>
                                    <td background="Images/button-bground.gif" width="99%">
                                        <img height="1" src="Images/blank.gif" width="1" />
                                    </td>
                                </tr>
                            </table>
                            <asp:CustomValidator ID="CvalHandleErr" runat="server" Display="None" ValidationGroup="handle"></asp:CustomValidator><asp:CustomValidator
                                ID="CvalPageErr" runat="server" Display="None"></asp:CustomValidator></ajax:AjaxPanel>
                    </td>
                </tr>
            </table>
    </asp:Content>
