Imports System.Data.SqlClient

Partial Class UserAdd
    Inherits Parent

    ''' <summary>
    ''' get id for focues
    ''' </summary>
    ''' <value></value>
    ''' <returns>element control id</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property GetFocusID() As String
        Get
            Return Me.TextUserid.ClientID
        End Get
    End Property
#Region " Insert User dan Audit trail"
    Private Sub InsertUserBySU()
        Try
            Dim UserID As String = Trim(Me.TextUserid.Text)
            Dim UserName As String = Me.TextUserName.Text
            Dim Salt As String = Guid.NewGuid.ToString
            Dim UserPassword As String = Sahassa.AML.Commonly.Encrypt(Me.TextPassword.Text, Salt)
            Dim EmailAddr As String = Me.TextboxEmailAddr.Text
            Dim Hp As String = Me.TextboxMobilePhone.Text
            Dim GroupID As Int32 = Me.DropdownlistGroup.SelectedValue
            Dim TellerID As String = Me.TextboxTellerID.Text
            Using AccessUser As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                AccessUser.Insert(UserID, EmailAddr, Hp, UserPassword, Salt, Now, "", False, Now, False, GroupID, New DateTime(1753, 1, 1, 12, 0, 0), False, True, UserName, TellerID)
            End Using
            Using AccessHistoryPassword As New AMLDAL.AMLDataSetTableAdapters.HistoryPasswordTableAdapter
                AccessHistoryPassword.Insert(UserID, UserPassword, Now)
            End Using
        Catch
            Throw
        End Try
    End Sub

    Private Sub InsertAuditTrail()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            Dim UserID As String = Trim(Me.TextUserid.Text)
            Dim UserName As String = Me.TextUserName.Text
            Dim Salt As String = Guid.NewGuid.ToString
            Dim UserPassword As String = Sahassa.AML.Commonly.Encrypt(Me.TextPassword.Text, Salt)
            Dim EmailAddr As String = Me.TextboxEmailAddr.Text
            Dim Hp As String = Me.TextboxMobilePhone.Text
            Dim GroupID As Int32 = Me.DropdownlistGroup.SelectedValue
            Dim TellerID As String = Me.TextboxTellerID.Text
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(15)
            'Using TransScope As New Transactions.TransactionScope
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAudit, Data.IsolationLevel.ReadUncommitted)
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserID", "Add", "", UserID, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserName", "Add", "", UserName, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserEmailAddress", "Add", "", EmailAddr, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserMobilePhone", "Add", "", Hp, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserPassword", "Add", "", UserPassword, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserPasswordSalt", "Add", "", Salt, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserLastChangedPassword", "Add", "", "", "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserIPAddress", "Add", "", "", "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserInUsed", "Add", "", "False", "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserCreatedDate", "Add", "", Now.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserIsDisabled", "Add", "", "False", "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserGroupId", "Add", "", GroupID, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserLastLogin", "Add", "", "", "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserWorkingUnitMappingStatus", "Add", "", "False", "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "isCreatedBySU", "Add", "", "True", "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "TellerID", "Add", TellerID, "True", "Accepted")
            End Using
            oSQLTrans.Commit()
            'End Using
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub
#End Region
    ''' <summary>
    ''' save button handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Page.Validate()

        If Page.IsValid Then
            Try
                If Me.TextPassword.Text.Length < ViewState("MinimumPasswordLength") Then
                    Throw New Exception("Password must be at least " & ViewState("MinimumPasswordLength") & " characters long.")
                End If

                Dim UserID As String = Trim(Me.TextUserid.Text)

                'Periksa apakah UserID tersebut sudah ada dalam tabel User atau belum
                Using AccessUser As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                    Dim counter As Int32 = AccessUser.CountMatchingUser(UserID)

                    'Counter = 0 berarti UserID tersebut tidak ada dalam tabel User
                    If counter = 0 Then
                        'Periksa apakah UserID tersebut sudah ada dalam tabel User_Approval atau belum
                        Using AccessUserApproval As New AMLDAL.AMLDataSetTableAdapters.User_ApprovalTableAdapter
                            counter = AccessUserApproval.CountUserApprovalByUserID(UserID)

                            'Counter = 0 berarti UserID tersebut statusnya tidak dalam pending approval dan boleh ditambahkan dlm tabel User_Approval
                            If counter = 0 Then
                                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                                    Me.InsertAuditTrail()
                                    Me.InsertUserBySU()
                                    Me.LblSuccess.Text = "Success to Insert User."
                                    Me.LblSuccess.Visible = True
                                Else
                                    Dim UserName As String = Me.TextUserName.Text
                                    Dim Salt As String = Guid.NewGuid.ToString
                                    Dim UserPassword As String = Sahassa.AML.Commonly.Encrypt(Me.TextPassword.Text, Salt)
                                    Dim EmailAddr As String = Me.TextboxEmailAddr.Text
                                    Dim Hp As String = Me.TextboxMobilePhone.Text
                                    Dim GroupID As Int32 = Me.DropdownlistGroup.SelectedValue
                                    Dim TellerID As String = Me.TextboxTellerID.Text

                                    Dim UserPendingApprovalID As Int64

                                    'Using TransScope As New Transactions.TransactionScope
                                    Using AccessUserPendingApproval As New AMLDAL.AMLDataSetTableAdapters.User_PendingApprovalTableAdapter
                                        'Tambahkan ke dalam tabel User_PendingApproval dengan ModeID = 1 (Add) 
                                        UserPendingApprovalID = AccessUserPendingApproval.InsertUser_PendingApproval(Sahassa.AML.Commonly.SessionUserId, Now, "User Add", 1)
                                    End Using

                                    'Tambahkan ke dalam tabel User_Approval dengan ModeID = 1 (Add) 
                                    AccessUserApproval.Insert(UserPendingApprovalID, 1, UserID, EmailAddr, Hp, UserPassword, Salt, New DateTime(1900, 1, 1, 12, 0, 0), "", False, Now, False, GroupID, New DateTime(1900, 1, 1, 12, 0, 0), Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, False, Nothing, False, Nothing, Nothing, False, False, False, False, Nothing, UserName, Nothing, Nothing, TellerID, Nothing)
                                    'TransScope.Complete()
                                    'End Using

                                    Dim MessagePendingID As Integer = 8301 'MessagePendingID 8301 = User Add 

                                    Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & UserID

                                    Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & UserID, False)
                                End If
                            Else
                                Throw New Exception("Cannot add the following User: '" & UserID & "' because it is currently waiting for approval.")
                            End If
                        End Using
                    Else
                        Throw New Exception("Cannot add the following User: '" & UserID & "' because that User already exists in the database.")
                    End If
                End Using
            Catch ex As Exception
                Me.cvalPageError.IsValid = False
                Me.cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            End Try
        End If
    End Sub

    ''' <summary>
    ''' fill group
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillGroup()
        Using AccessGroup As New AMLDAL.AMLDataSetTableAdapters.GroupTableAdapter
            Me.DropdownlistGroup.DataSource = AccessGroup.GetData
            Me.DropdownlistGroup.DataTextField = "GroupName"
            Me.DropdownlistGroup.DataValueField = "GroupID"
            Me.DropdownlistGroup.DataBind()
        End Using

        Try
            'Hapus SuperUser Group dari webcontrol DropDownListGroup
            For Each item As ListItem In Me.DropdownlistGroup.Items
                If item.Text = "SuperUser" Then
                    Me.DropdownlistGroup.Items.Remove(item)
                End If
            Next
        Catch ex As Exception

        End Try
    End Sub

    ''' <summary>
    ''' page load event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.FillGroup()

                Using AccessLoginParameter As New AMLDAL.AMLDataSetTableAdapters.LoginParameterTableAdapter
                    Dim objTable As Data.DataTable = AccessLoginParameter.GetData
                    If objTable.Rows.Count > 0 Then
                        Dim objRow As AMLDAL.AMLDataSet.LoginParameterRow = objTable.Rows(0)
                        ViewState("MinimumPasswordLength") = objRow.MinimumPasswordLength
                    Else
                        ViewState("MinimumPasswordLength") = 6
                    End If

                    Me.TextPassword.MaxLength = CInt(ViewState("MinimumPasswordLength")) * 2
                    Me.TextBoxRetype.MaxLength = CInt(ViewState("MinimumPasswordLength")) * 2
                End Using

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' cancel event handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "UserView.aspx"

        Me.Response.Redirect("UserView.aspx", False)
    End Sub
End Class