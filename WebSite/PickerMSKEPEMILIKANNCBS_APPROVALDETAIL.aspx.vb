Imports Sahassanettier.Data
Imports Sahassanettier.Entities
Imports Sahassanettier.Services
Imports System.Collections.Generic

Partial Class PickerMSKEPEMILIKANNCBS_APPROVALDETAIL
	Inherits Parent

#Region "Set Session"


	Private Property SetnGetSort() As String
		Get
			Return IIf(Session("PickerMSKEPEMILIKANNCBS_APPROVALDETAILSort") Is Nothing, "IDKEPEMILIKANNCBS asc", Session("PickerMSKEPEMILIKANNCBS_APPROVALDETAILSort"))
		End Get
		Set(ByVal Value As String)
			Session("PickerMSKEPEMILIKANNCBS_APPROVALDETAILSort") = Value
		End Set
	End Property
	Private Property SetnGetCurrentPage() As Int32
		Get
			Return IIf(Session("PickerMSKEPEMILIKANNCBS_APPROVALDETAILCurrentPage") Is Nothing, 0, Session("PickerMSKEPEMILIKANNCBS_APPROVALDETAILCurrentPage"))
		End Get
		Set(ByVal Value As Int32)
			Session("PickerMSKEPEMILIKANNCBS_APPROVALDETAILCurrentPage") = Value
		End Set
	End Property
	Private ReadOnly Property GetPageTotal() As Int32
		Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("PickerMSKEPEMILIKANNCBS_APPROVALDETAILRowTotal") Is Nothing, 0, Session("PickerMSKEPEMILIKANNCBS_APPROVALDETAILRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("PickerMSKEPEMILIKANNCBS_APPROVALDETAILRowTotal") = Value
        End Set
    End Property
    Private Property SetAndGetSearchingCriteria() As String
        Get
            Return IIf(Session("PickerMSKEPEMILIKANNCBS_APPROVALDETAILSearchCriteria") Is Nothing, "", Session("PickerMSKEPEMILIKANNCBS_APPROVALDETAILSearchCriteria"))
        End Get
        Set(ByVal Value As String)
            Session("PickerMSKEPEMILIKANNCBS_APPROVALDETAILSearchCriteria") = Value
        End Set
    End Property
    Private Function SearchFilter() As String
        Dim StrSearch As String = ""
        Try
            StrSearch = " Activation = 1 "
            '// Filter
            If Me.txtNamaKepemilikanNCBS.Text <> "" Then
                If StrSearch = "" Then
                    StrSearch = StrSearch & " NamaKepemilikanNCBS like '%" & Me.txtNamaKepemilikanNCBS.Text.Replace("'", "''") & "%' "
                Else
                    StrSearch = StrSearch & " And NamaKepemilikanNCBS like '%" & Me.txtNamaKepemilikanNCBS.Text.Replace("'", "''") & "%' "
                End If
            End If



            Return StrSearch
        Catch
            Throw
            Return ""
        End Try
    End Function
    Private Function SetnGetBindTable() As TList(Of MsKepemilikanNCBS_ApprovalDetail)
        Return DataRepository.MsKepemilikanNCBS_ApprovalDetailProvider.GetPaged(SearchFilter, SetnGetSort, SetnGetCurrentPage, 10, SetnGetRowTotal)
    End Function
#End Region
    Private Property StrMSKEPEMILIKANNCBS_APPROVALDETAILFilter() As String
        Get
            Return IIf(Session("PickerMSKEPEMILIKANNCBS_APPROVALDETAILStrMSKEPEMILIKANNCBS_APPROVALDETAILFilter") Is Nothing, "", Session("PickerMSKEPEMILIKANNCBS_APPROVALDETAILStrMSKEPEMILIKANNCBS_APPROVALDETAILFilter"))
        End Get
        Set(ByVal Value As String)
            Session("PickerMSKEPEMILIKANNCBS_APPROVALDETAILStrMSKEPEMILIKANNCBS_APPROVALDETAILFilter") = Value
        End Set
    End Property

#Region " Searching Box"
    Protected Sub ImageClearSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageClearSearch.Click
        Try
            '//Clear Control
            Me.txtNamaKepemilikanNCBS.Text = ""


        Catch
            Throw
        End Try
    End Sub
    Protected Sub ImgSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgSearch.Click
        Try
            Me.SetnGetCurrentPage = 0
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
#End Region
#Region "Sorting Event"
    Protected Sub GridDataView_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridDataView.SortCommand
        Dim GridPickerProductTier2Sort As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridPickerProductTier2Sort.Columns(Sahassa.AML.Commonly.IndexSort(GridDataView, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
#End Region
#Region "Paging Event"
    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
        Catch
            Throw
        End Try
    End Sub
    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Protected Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be less than or equal to the total page count.")
                End If
            Else
                Throw New Exception("Page number must be in number format.")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
#End Region
    Private Sub ClearThisPageSessions()
        Session("PickerMSKEPEMILIKANNCBS_APPROVALDETAILSort") = Nothing
        Session("PickerMSKEPEMILIKANNCBS_APPROVALDETAILCurrentPage") = Nothing
        Session("PickerMSKEPEMILIKANNCBS_APPROVALDETAILRowTotal") = Nothing
        Session("PickerMSKEPEMILIKANNCBS_APPROVALDETAILSearchCriteria") = Nothing
        Session("PickerProductTier2ViewData") = Nothing
        Session("PickerProductTier2Selected") = Nothing
        Session("PickerMSKEPEMILIKANNCBS_APPROVALDETAIL.Data") = Nothing
    End Sub

    Public Sub BindGrid()
        Me.GridDataView.DataSource = Me.SetnGetBindTable
        Me.GridDataView.VirtualItemCount = Me.SetnGetRowTotal
        Me.GridDataView.DataBind()
    End Sub
    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            If Not Me.IsPostBack Then
                Me.ClearThisPageSessions()

                Me.GridDataView.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                'Using AccessAudit As New CTRWebBLL.AuditTrailBLL
                '	AccessAudit.InsertAuditTrailUserAccess(Sahassa.AML.Commonly.SessionNIK, Sahassa.AML.Commonly.SessionStaffName, "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded")
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.CollectSelected()
            Me.BindGrid()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("PickerProductTier2Selected") Is Nothing, New ArrayList, Session("PickerProductTier2Selected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("PickerProductTier2Selected") = value
        End Set
    End Property

    Protected Sub GridDataView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridDataView.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Dim chkBox As CheckBox = e.Item.FindControl("rbSelected")
                chkBox.Checked = Me.SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)
                e.Item.Cells(2).Text = CStr((e.Item.ItemIndex + 1) + (Me.SetnGetCurrentPage * Sahassa.AML.Commonly.GetDisplayedTotalRow))
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

	Private Sub CollectSelected()
		For Each gridRow As DataGridItem In Me.GridDataView.Items
			If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
				Dim chkBox As CheckBox = gridRow.FindControl("rbSelected")
				Dim PkId As String = gridRow.Cells(1).Text
				Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
				If ArrTarget.Contains(PkId) Then
					If Not chkBox.Checked Then
						ArrTarget.Remove(PkId)
					End If
				Else
					If chkBox.Checked Then ArrTarget.Add(PkId)
				End If
				Me.SetnGetSelectedItem = ArrTarget
			End If
		Next
	End Sub

	Protected Sub ImgSelected_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgSelected.Click
		Dim ObjMSKEPEMILIKANNCBS_APPROVALDETAIL As New TList(Of MSKEPEMILIKANNCBS_APPROVALDETAIL)
		Dim intSelected As Integer = 0

		For i As Integer = 0 To GridDataView.Items.Count - 1
			Dim rbSelected As CheckBox = CType(GridDataView.Items(i).FindControl("rbSelected"), CheckBox)
			If rbSelected.Checked Then
				Dim Key As String = GridDataView.Items(i).Cells(1).Text
				Dim obj As MSKEPEMILIKANNCBS_APPROVALDETAIL = DataRepository.MSKEPEMILIKANNCBS_APPROVALDETAILProvider.GetByIDKEPEMILIKANNCBS(Key)
				ObjMSKEPEMILIKANNCBS_APPROVALDETAIL.Add(obj)
				intSelected = 1
				' Exit For
			End If
		Next

		Dim sbScript As New StringBuilder
		If intSelected = 0 Then
			sbScript.Append("<script>alert('Please select data');</script>")
			ClientScript.RegisterClientScriptBlock(Page.GetType, "alterting", sbScript.ToString())
		Else
			If Session("PickerMSKEPEMILIKANNCBS_APPROVALDETAIL.Data") Is Nothing Then
				Session.Add("PickerMSKEPEMILIKANNCBS_APPROVALDETAIL.Data", ObjMSKEPEMILIKANNCBS_APPROVALDETAIL)
			Else
				Session("PickerMSKEPEMILIKANNCBS_APPROVALDETAIL.Data") = ObjMSKEPEMILIKANNCBS_APPROVALDETAIL
			End If
			ClientScript.RegisterClientScriptBlock(Page.GetType, "closing", "javascript:window.close()", True)
		End If

	End Sub

	Private Property ListMSKEPEMILIKANNCBS_APPROVALDETAILId() As System.Collections.Generic.List(Of Integer)
		Get
			Return IIf(Session("PickerMSKEPEMILIKANNCBS_APPROVALDETAIL_List_MSKEPEMILIKANNCBS_APPROVALDETAIL_Id") Is Nothing, New System.Collections.Generic.List(Of Integer), Session("PickerMSKEPEMILIKANNCBS_APPROVALDETAIL_List_MSKEPEMILIKANNCBS_APPROVALDETAIL_Id"))
		End Get
		Set(ByVal Value As System.Collections.Generic.List(Of Integer))
			Session("PickerMSKEPEMILIKANNCBS_APPROVALDETAIL_List_MSKEPEMILIKANNCBS_APPROVALDETAIL_Id") = Value
		End Set
	End Property


End Class


