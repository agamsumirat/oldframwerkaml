<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="CDDLNPHeaderApproval.aspx.vb" Inherits="CDDLNPHeaderApproval" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">

    <script src="Script/popcalendar.js"></script>

    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td style="width: 3px">
            </td>
            <td id="tdcontent" height="99%" valign="top" bgcolor="#FFFFFF">
                <div>
                    <table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td class="divcontentinside" bgcolor="#FFFFFF">
                                <ajax:AjaxPanel ID="AjaxPanel1" runat="server">
                                    <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
                                        style="border-top-style: none; border-right-style: none; border-left-style: none;
                                        border-bottom-style: none" width="100%">
                                        <tr>
                                            <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                                                border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                <img src="Images/dot_title.gif" width="17" height="17">
                                                <strong>
                                                    <asp:Label ID="Label1" runat="server" Text="EDD Workflow Parameter - Approval"></asp:Label>
                                                </strong>
                                                <hr />
                                            </td>
                                        </tr>
                                    </table>
                                    <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                        bgcolor="#dddddd" border="2">
                                        <tr>
                                            <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                style="height: 6px">
                                                <table cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td class="formtext">
                                                            <asp:Label ID="Label6" runat="server" Text="Search Criteria" Font-Bold="True"></asp:Label>
                                                            &nbsp;</td>
                                                        <td>
                                                            <a href="#" onclick="javascript:ShowHidePanel('SearchCriteria','searchimage4','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                title="click to minimize or maximize">
                                                                <img id="searchimage4" src="Images/search-bar-minimize.gif" border="0" height="12px"
                                                                    width="12px"></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr id="SearchCriteria">
                                            <td valign="top" bgcolor="#ffffff">
                                                <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                    <tr>
                                                        <td nowrap style="background-color: #FFF7E6; width: 15%">
                                                            Requested By</td>
                                                        <td nowrap style="width: 75%; height: 22px;" valign="top">
                                                            <asp:TextBox ID="txtRequestedBy" runat="server" CssClass="searcheditbox" TabIndex="2"
                                                                Width="308px"></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap style="background-color: #FFF7E6; width: 15%">
                                                            Requested Date</td>
                                                        <td nowrap style="width: 75%; height: 22px;" valign="top">
                                                            <asp:TextBox ID="txtRequestedDateFrom" runat="server" CssClass="textBox" MaxLength="1000"
                                                                TabIndex="2" ToolTip="RequestedDate" Width="120px"></asp:TextBox>
                                                            <input id="PopRequestedDateFrom" runat="server" style="border-right: #ffffff 0px solid;
                                                                border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
                                                                border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
                                                                height: 17px" title="Click to show calendar" type="button" />
                                                            s/d
                                                            <asp:TextBox ID="txtRequestedDateTo" runat="server" CssClass="textBox" MaxLength="1000"
                                                                TabIndex="2" ToolTip="RequestedDate" Width="120px"></asp:TextBox>
                                                            <input id="PopRequestedDateTo" runat="server" style="border-right: #ffffff 0px solid;
                                                                border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
                                                                border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
                                                                height: 17px" title="Click to show calendar" type="button" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap="nowrap" style="width: 15%; background-color: #fff7e6">
                                                            Mode</td>
                                                        <td nowrap="nowrap" style="width: 75%; height: 22px" valign="top">
                                                            <asp:DropDownList ID="ddlMode" runat="server" CssClass="comboBox">
                                                            </asp:DropDownList></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" height="10">
                                                            &nbsp;<asp:ImageButton ID="ImageButtonSearch" TabIndex="3" runat="server" SkinID="SearchButton"
                                                                CausesValidation="False" ImageUrl="~/Images/Button/Search.gif"></asp:ImageButton>&nbsp;<asp:ImageButton
                                                                    ID="ImageClear" runat="server" CausesValidation="False" ImageUrl="~/Images/Button/clearSearch.gif"
                                                                    TabIndex="3" /></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <table align="center" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="99%"
                                        bgcolor="#dddddd" border="2">
                                        <tr id="TR1">
                                            <td valign="top" width="98%" bgcolor="#ffffff">
                                                <table bordercolor="#ffffff" cellspacing="1" cellpadding="0" width="100%" bgcolor="#dddddd"
                                                    border="2">
                                                    <tr>
                                                        <td bgcolor="#ffffff">
                                                            &nbsp;<asp:DataGrid ID="GridViewCDDLNP" runat="server" AllowCustomPaging="True" AllowPaging="True"
                                                                AllowSorting="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#DEDFDE"
                                                                BorderStyle="None" BorderWidth="1px" CellPadding="4" Font-Size="XX-Small" ForeColor="Black"
                                                                GridLines="Vertical" Width="100%">
                                                                <FooterStyle BackColor="#CCCC99" />
                                                                <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                                <AlternatingItemStyle BackColor="White" />
                                                                <ItemStyle BackColor="#F7F7DE" />
                                                                <HeaderStyle BackColor="#6B696B" Font-Bold="True" Font-Size="XX-Small" ForeColor="White" />
                                                                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages"
                                                                    Visible="False" />
                                                                <Columns>
                                                                    <asp:TemplateColumn>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="CheckBoxExporttoExcel" runat="server" meta:resourcekey="CheckBoxExporttoExcelResource1" />
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="2%" />
                                                                    </asp:TemplateColumn>
                                                                    <asp:BoundColumn DataField="PK_CDD_Bagian_Approval_Id" Visible="False">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn HeaderText="No" Visible="false">
                                                                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                            Font-Underline="False" ForeColor="White" />
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="UserName" HeaderText="Requested By" SortExpression="UserName  asc" />
                                                                    <asp:BoundColumn DataField="RequestedDate" DataFormatString="{0:dd-MMM-yyyy}" HeaderText="Requested Date"
                                                                        SortExpression="RequestedDate desc" />
                                                                    <asp:BoundColumn DataField="Mode" HeaderText="Mode" SortExpression="Mode  asc" />
                                                                    <asp:TemplateColumn>
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="LnkDetail" runat="server" CausesValidation="false" CommandName="Detail"
                                                                                Text="Detail"></asp:LinkButton>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="5%" />
                                                                    </asp:TemplateColumn>
                                                                </Columns>
                                                            </asp:DataGrid>
                                                            <asp:Label ID="LabelNoRecordFound" runat="server" CssClass="text" Text="No record match with your criteria"
                                                                Visible="False"></asp:Label></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: #ffffff">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                    <tr>
                                                        <td nowrap style="height: 20px">
                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                <tr>
                                                                    <td nowrap style="height: 20px;">
                                                                        <asp:CheckBox ID="CheckBoxSelectAll" runat="server" Text="Select All" AutoPostBack="True" /></td>
                                                                    <td width="100%" style="height: 20px">
                                                                        &nbsp;<asp:LinkButton ID="lnkExportData" runat="server" ajaxcall="none" Text="Export Selected"></asp:LinkButton>
                                                                        &nbsp;<asp:LinkButton ID="lnkExportAllData" runat="server" ajaxcall="none" Text="Export All"></asp:LinkButton></td>
                                                                    <td align="right" nowrap style="height: 20px">
                                                                        &nbsp;
                                                                        &nbsp;&nbsp;</td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#ffffff">
                                                <table id="Table3" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                    bgcolor="#ffffff" border="2">
                                                    <tr class="regtext" align="center" bgcolor="#dddddd">
                                                        <td valign="top" align="left" width="50%" bgcolor="#ffffff" style="height: 19px">
                                                            Page&nbsp;<asp:Label ID="PageCurrentPage" runat="server" CssClass="regtext">0</asp:Label>&nbsp;of&nbsp;
                                                            <asp:Label ID="PageTotalPages" runat="server" CssClass="regtext">0</asp:Label></td>
                                                        <td valign="top" align="right" width="50%" bgcolor="#ffffff" style="height: 19px">
                                                            Total Records&nbsp;
                                                            <asp:Label ID="PageTotalRows" runat="server">0</asp:Label></td>
                                                    </tr>
                                                </table>
                                                <table id="Table4" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                    bgcolor="#ffffff" border="2">
                                                    <tr bgcolor="#ffffff">
                                                        <td class="regtext" valign="middle" align="left" colspan="11" height="7">
                                                            <hr>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="regtext" valign="middle" align="left" width="63" bgcolor="#ffffff" style="height: 45px">
                                                            Go to page</td>
                                                        <td class="regtext" valign="middle" align="left" width="5" bgcolor="#ffffff" style="height: 45px">
                                                            <font face="Verdana, Arial, Helvetica, sans-serif" size="1">
                                                                <asp:TextBox ID="TextGoToPage" runat="server" Width="38px" CssClass="searcheditbox"></asp:TextBox>
                                                            </font>
                                                        </td>
                                                        <td class="regtext" valign="middle" align="left" bgcolor="#ffffff" style="height: 45px">
                                                            <asp:ImageButton ID="ImageButtonGo" runat="server" SkinID="GoButton" ImageUrl="~/Images/Button/Go.gif">
                                                            </asp:ImageButton>
                                                        </td>
                                                        <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff" style="height: 45px">
                                                            <img height="5" src="images/first.gif" width="6">
                                                        </td>
                                                        <td class="regtext" valign="middle" align="right" width="50" bgcolor="#ffffff" style="height: 45px">
                                                            <asp:LinkButton ID="LinkButtonFirst" runat="server" CssClass="regtext" CommandName="First"
                                                                OnCommand="PageNavigate">First</asp:LinkButton>
                                                        </td>
                                                        <td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff" style="height: 45px">
                                                            <img height="5" src="images/prev.gif" width="6"></td>
                                                        <td class="regtext" valign="middle" align="right" width="50" bgcolor="#ffffff" style="height: 45px">
                                                            <asp:LinkButton ID="LinkButtonPrevious" runat="server" CssClass="regtext" CommandName="Prev"
                                                                OnCommand="PageNavigate">Previous</asp:LinkButton>
                                                        </td>
                                                        <td class="regtext" valign="middle" align="right" width="50" bgcolor="#ffffff" style="height: 45px">
                                                            <asp:LinkButton ID="LinkButtonNext" runat="server" CssClass="regtext" CommandName="Next"
                                                                OnCommand="PageNavigate">Next</asp:LinkButton></td>
                                                        <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff" style="height: 45px">
                                                            <img height="5" src="images/next.gif" width="6"></td>
                                                        <td class="regtext" valign="middle" align="left" width="50" bgcolor="#ffffff" style="height: 45px">
                                                            <asp:LinkButton ID="LinkButtonLast" runat="server" CssClass="regtext" CommandName="Last"
                                                                OnCommand="PageNavigate">Last</asp:LinkButton>
                                                        </td>
                                                        <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff" style="height: 45px">
                                                            <img height="5" src="images/last.gif" width="6"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </ajax:AjaxPanel>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    <asp:CustomValidator ID="cvalPageErr" runat="server" Display="None"></asp:CustomValidator>
</asp:Content>
