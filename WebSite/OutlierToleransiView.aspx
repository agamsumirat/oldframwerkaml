﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="OutlierToleransiView.aspx.vb" Inherits="OutlierToleransiView" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
        height="72" style="border-top-style: none; border-right-style: none; border-left-style: none;
        border-bottom-style: none" width="100%">
        <tr>
            <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Outlier Toleransi - View </strong>
            </td>
        </tr>
    </table>
    <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
        border="2">
        <tr>
            <td align="left" bgcolor="#eeeeee">
                <img height="15" src="images/arrow.gif" width="15">
            </td>
            <td valign="top" width="98%" bgcolor="#ffffff">
                <table cellspacing="4" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="Regtext" nowrap>
                            Search By :
                        </td>
                        <td valign="middle" nowrap>
                            <ajax:AjaxPanel ID="AjaxPanel1" runat="server">
                                <asp:DropDownList ID="ComboSearch" TabIndex="1" runat="server" Width="120px" CssClass="searcheditcbo">
                                </asp:DropDownList>
                                &nbsp;
                                <asp:TextBox ID="TextSearch" TabIndex="2" runat="server" CssClass="searcheditbox"></asp:TextBox></ajax:AjaxPanel>
                        </td>
                        <td valign="middle" width="99%">
                            <ajax:AjaxPanel ID="AjaxPanel2" runat="server">
                                <asp:ImageButton ID="ImageButtonSearch" TabIndex="3" runat="server" SkinID="SearchButton"
                                    ImageUrl="~/Images/button/search.gif"></asp:ImageButton>
                                &nbsp;</ajax:AjaxPanel>
                        </td>
                    </tr>
                </table>
                <ajax:AjaxPanel ID="AjaxPanel3" runat="server">
                    <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></ajax:AjaxPanel>
            </td>
        </tr>
    </table>
    <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
        border="2">
        <tr>
            <td bgcolor="#ffffff" valign="top">
                <ajax:AjaxPanel ID="AjaxPanel4" runat="server" Width="99%">
                    <asp:DataGrid ID="GridOutlierToleransi" runat="server" 
                        AutoGenerateColumns="False" Font-Size="XX-Small"
                        BackColor="White" CellPadding="4" BorderWidth="1px" BorderStyle="None" AllowPaging="True"
                        Width="100%" GridLines="Vertical" AllowSorting="True" 
                        BorderColor="#DEDFDE" ForeColor="Black">
                        <AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
                        <Columns>
                            <asp:TemplateColumn>
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBoxExporttoExcel" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="PK_OutlierToleransi_ID" 
                                SortExpression="PK_OutlierToleransi_ID" Visible="False">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="AccountOwnerName" HeaderText="Account Owner Name" 
                                SortExpression="AccountOwnerName desc">
                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" Wrap="False" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Segment" HeaderText="Segment" 
                                SortExpression="Segment desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="ToleransiDebet" HeaderText="Toleransi Debet" 
                                SortExpression="ToleransiDebet desc"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ToleransiKredit" HeaderText="Toleransi Kredit" 
                                SortExpression="ToleransiKredit desc"></asp:BoundColumn>
                            <asp:BoundColumn DataField="CreatedDate" HeaderText="Created Date" SortExpression="CreatedDate desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CreatedBy" HeaderText="CreatedBy" SortExpression="CreatedBy desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="LastUpdateDate" HeaderText="Last Update Date" SortExpression="LastUpdateDate desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="LastUpdateBy" HeaderText="Last Update By" SortExpression="LastUpdateBy desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Activation" HeaderText="Activation" SortExpression="Activation">
                            </asp:BoundColumn>
                            <asp:ButtonColumn CommandName="Edit" Text="Edit"></asp:ButtonColumn>
                            <asp:TemplateColumn>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkDelete" runat="server" CommandName="delete" ForeColor="Black">Delete</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkActive" runat="server" CommandName="activation" ForeColor="Black">Active</asp:LinkButton>
                                    /<asp:LinkButton ID="lnkInactive" runat="server" CommandName="activation" ForeColor="Black">InActive</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <FooterStyle BackColor="#CCCC99" />
                        <HeaderStyle BackColor="#6B696B" Font-Bold="True" Font-Size="XX-Small" ForeColor="White" />
                        <ItemStyle BackColor="#F7F7DE"></ItemStyle>
                        <PagerStyle Visible="False" HorizontalAlign="Right" ForeColor="Black" BackColor="#F7F7DE"
                            Mode="NumericPages"></PagerStyle>
                        <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                    </asp:DataGrid>
                </ajax:AjaxPanel>
            </td>
        </tr>
        <tr>
            <td style="background-color: #ffffff">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td nowrap>
                            <ajax:AjaxPanel ID="AjaxPanel5" runat="server">
                                &nbsp;<asp:CheckBox ID="CheckBoxSelectAll" runat="server" Text="Select All" AutoPostBack="True" />
                                &nbsp;
                            </ajax:AjaxPanel>
                        </td>
                        <td width="99%">
                            &nbsp;&nbsp;<asp:LinkButton ID="LinkButtonExportExcel" runat="server">Export 
		        to Excel</asp:LinkButton>
                        &nbsp;<asp:LinkButton ID="LinkExportAllToExcel" runat="server">Export All To Excel</asp:LinkButton>
                        </td>
                        <td align="right" nowrap>
                            &nbsp;<asp:LinkButton ID="LinkButtonAddNew" runat="server">Add 
	        New</asp:LinkButton>&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor="#ffffff">
                <table id="Table3" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                    bgcolor="#ffffff" border="2">
                    <tr class="regtext" align="center" bgcolor="#dddddd">
                        <td valign="top" align="left" width="50%" bgcolor="#ffffff">
                            Page&nbsp;<ajax:AjaxPanel ID="AjaxPanel6" runat="server">
                                <asp:Label ID="PageCurrentPage" runat="server" CssClass="regtext">0</asp:Label>
                                &nbsp;of&nbsp;
                                <asp:Label ID="PageTotalPages" runat="server" CssClass="regtext">0</asp:Label>
                            </ajax:AjaxPanel>
                        </td>
                        <td valign="top" align="right" width="50%" bgcolor="#ffffff">
                            Total Records&nbsp;
                            <ajax:AjaxPanel ID="AjaxPanel7" runat="server">
                                <asp:Label ID="PageTotalRows" runat="server">0</asp:Label>
                            </ajax:AjaxPanel>
                        </td>
                    </tr>
                </table>
                <table id="Table4" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                    bgcolor="#ffffff" border="2">
                    <tr bgcolor="#ffffff">
                        <td class="regtext" valign="middle" align="left" colspan="11" height="7">
                            <hr color="#f40101" noshade size="1">
                        </td>
                    </tr>
                    <tr>
                        <td class="regtext" valign="middle" align="left" width="63" bgcolor="#ffffff">
                            Go to page
                        </td>
                        <td class="regtext" valign="middle" align="left" width="5" bgcolor="#ffffff">
                            <font face="Verdana, Arial, Helvetica, sans-serif" size="1">
                                <ajax:AjaxPanel ID="AjaxPanel8" runat="server">
                                    <asp:TextBox ID="TextGoToPage" runat="server" Width="38px" CssClass="searcheditbox"></asp:TextBox>
                                </ajax:AjaxPanel>
                            </font>
                        </td>
                        <td class="regtext" valign="middle" align="left" bgcolor="#ffffff">
                            <ajax:AjaxPanel ID="AjaxPanel9" runat="server">
                                <asp:ImageButton ID="ImageButtonGo" runat="server" SkinID="GoButton"></asp:ImageButton>
                            </ajax:AjaxPanel>
                        </td>
                        <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                            <img height="5" src="images/first.gif" width="6">
                        </td>
                        <td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff">
                            <ajax:AjaxPanel ID="AjaxPanel10" runat="server">
                                <asp:LinkButton ID="LinkButtonFirst" runat="server" CssClass="regtext" CommandName="First"
                                    OnCommand="PageNavigate">First</asp:LinkButton>
                            </ajax:AjaxPanel>
                        </td>
                        <td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff">
                            <img height="5" src="images/prev.gif" width="6">
                        </td>
                        <td class="regtext" valign="middle" align="right" width="14" bgcolor="#ffffff">
                            <ajax:AjaxPanel ID="AjaxPanel11" runat="server">
                                <asp:LinkButton ID="LinkButtonPrevious" runat="server" CssClass="regtext" CommandName="Prev"
                                    OnCommand="PageNavigate">Previous</asp:LinkButton>
                            </ajax:AjaxPanel>
                        </td>
                        <td class="regtext" valign="middle" align="right" width="60" bgcolor="#ffffff">
                            <ajax:AjaxPanel ID="AjaxPanel12" runat="server">
                                <a class="pageNav" href="#">
                                    <asp:LinkButton ID="LinkButtonNext" runat="server" CssClass="regtext" CommandName="Next"
                                        OnCommand="PageNavigate">Next</asp:LinkButton>
                                </a>
                            </ajax:AjaxPanel>
                        </td>
                        <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                            <img height="5" src="images/next.gif" width="6">
                        </td>
                        <td class="regtext" valign="middle" align="left" width="25" bgcolor="#ffffff">
                            <ajax:AjaxPanel ID="AjaxPanel13" runat="server">
                                <asp:LinkButton ID="LinkButtonLast" runat="server" CssClass="regtext" CommandName="Last"
                                    OnCommand="PageNavigate">Last</asp:LinkButton>
                            </ajax:AjaxPanel>
                        </td>
                        <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                            <img height="5" src="images/last.gif" width="6">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>


