Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Partial Class AuditTrailMaintenanceApprovalDetail
    Inherits Parent

#Region "Property"
    Public ReadOnly Property pk_PendingApproval_id() As Integer
        Get
            Return Me.Request.Params.Item("PendingApprovalID")
        End Get
    End Property

    Public ReadOnly Property pk_Approval_id() As Integer
        Get
            Return Me.Request.Params.Item("ApprovalID")
        End Get
    End Property
#End Region

    '''' <summary>
    '''' Delete All Approval 
    '''' </summary>
    '''' <remarks></remarks>
    'Private Sub DeleteAllApproval(ByRef oSQLTrans As SqlTransaction)
    '    Try
    '        Using AccessAuditTrailPendingApproval As New AMLDAL.AMLDataSetTableAdapters.AuditTrailMaintenance_PendingApprovalTableAdapter
    '            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAuditTrailPendingApproval, oSQLTrans)
    '            AccessAuditTrailPendingApproval.DeleteAuditTrailMaintenance_PendingApproval(Me.pk_PendingApproval_id)
    '            Using AccessAuditTrailApproval As New AMLDAL.AMLDataSetTableAdapters.AuditTrailMaintenance_ApprovalTableAdapter
    '                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAuditTrailApproval, oSQLTrans)
    '                AccessAuditTrailApproval.DeleteAuditTrailMaintenance_Approval(Me.pk_Approval_id)
    '            End Using
    '        End Using
    '    Catch
    '        Throw
    '    End Try
    'End Sub
    ''' <summary>
    ''' Insert Audit Trail
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub InsertAuditTrail(ByVal Action As String, ByRef oSQLTrans As SqlTransaction)
        Try
            Using AccessAuditTrailPendingApproval As New AMLDAL.AMLDataSetTableAdapters.AuditTrailMaintenance_PendingApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAuditTrailPendingApproval, oSQLTrans)
                Using dt As Data.DataTable = AccessAuditTrailPendingApproval.GetData
                    If dt.Rows.Count > 0 Then
                        Dim row As AMLDAL.AMLDataSet.AuditTrailMaintenance_PendingApprovalRow = dt.Rows(0)
                        Sahassa.AML.AuditTrailAlert.AuditTrailChecking(1)
                        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                            AccessAudit.Insert(Now, row.UserID, Sahassa.AML.Commonly.SessionUserId, "AuditTrail Maintenance", "Purge", "Delete", "", "", Action)
                        End Using
                    Else
                        Throw New Exception("Data not exist anymore.")
                    End If
                End Using
            End Using
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' Accept Purge
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            Using AccessQueryAuditTrail As New AMLDAL.AMLDataSetTableAdapters.QueriesSHSMenu
                'oTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(Adapter, Data.IsolationLevel.ReadUncommitted)
                If Session("TypePurge") = "Date" Then
                    AccessQueryAuditTrail.DeleteAuditTrailByDate(CDate(Session("StartDate")).ToString("yyyy-MM-dd 00:00"), CDate(Session("EndDate")).ToString("yyyy-MM-dd 23:59"))
                Else
                    AccessQueryAuditTrail.DeleteAuditTrailByCount(CInt(Session("Count")))
                End If
                Using AccessAuditTrailPendingApproval As New AMLDAL.AMLDataSetTableAdapters.AuditTrailMaintenance_PendingApprovalTableAdapter
                    oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAuditTrailPendingApproval)
                    AccessAuditTrailPendingApproval.DeleteAuditTrailMaintenance_PendingApproval(Me.pk_PendingApproval_id)
                    Using AccessAuditTrailApproval As New AMLDAL.AMLDataSetTableAdapters.AuditTrailMaintenance_ApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAuditTrailApproval, oSQLTrans)
                        AccessAuditTrailApproval.DeleteAuditTrailMaintenance_Approval(Me.pk_Approval_id)
                    End Using
                    Me.InsertAuditTrail("Accept", oSQLTrans)
                End Using
                'DeleteAllApproval(oTrans)
                'InsertAuditTrail("Accept", oTrans)


                Sahassa.AML.Commonly.SessionIntendedPage = "AuditTrailMaintenanceApproval.aspx"
            End Using
            oSQLTrans.Commit()
            Me.Response.Redirect("AuditTrailMaintenanceApproval.aspx", False)
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try


    End Sub

    Public Sub HideAll(ByVal TypePurge As String)
        Try
            Dim StrId As String = ""
            Select Case TypePurge
                Case "Date"
                    StrId = "PurgeD"
                Case "Count"
                    StrId = "PurgeC"
                Case Else
                    Throw New Exception("Type not supported type:" & TypePurge.ToString)
            End Select
            For Each ObjRow As HtmlControls.HtmlTableRow In Me.TableDetail.Rows
                ObjRow.Visible = ObjRow.ID.Substring(0, 6) = "Netral" Or ObjRow.ID.Substring(0, 6) = StrId
            Next
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' Load Data
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.ImageAccept.Attributes.Add("onclick", "javascript: if(confirm('Do you want to accept this activity ?')== false) return false;")
                Me.ImageReject.Attributes.Add("onclick", "javascript: if(confirm('Do you want to reject this activity ?')== false) return false;")
                Using AccessAuditTrailPendingApproval As New AMLDAL.AMLDataSetTableAdapters.AuditTrailMaintenance_PendingApprovalTableAdapter
                    Using dt As Data.DataTable = AccessAuditTrailPendingApproval.GetData
                        If dt.Rows.Count > 0 Then
                            Dim RowPending As AMLDAL.AMLDataSet.AuditTrailMaintenance_PendingApprovalRow = dt.Select("pk_audittrail_maintenance_pending_id=" & pk_PendingApproval_id)(0)
                            HideAll(RowPending.TypePurge)
                            Using AccessAuditTrailApproval As New AMLDAL.AMLDataSetTableAdapters.AuditTrailMaintenance_ApprovalTableAdapter
                                Using dtApproval As Data.DataTable = AccessAuditTrailApproval.GetData
                                    Dim RowApproval As AMLDAL.AMLDataSet.AuditTrailMaintenance_ApprovalRow = dtApproval.Select("pk_AuditTrail_maintenance_id=" & pk_Approval_id)(0)
                                    If RowPending.TypePurge = "Date" Then
                                        Me.LabelStartDate.Text = RowApproval.StartDate.ToString("dd-MMMM-yyyy")
                                        Me.LabelEndDate.Text = RowApproval.EndDate.ToString("dd-MMMM-yyyy")
                                        Me.LabelPurgeType.Text = "Date"
                                    Else
                                        Me.LabelCount.Text = RowApproval.Count
                                        Me.LabelPurgeType.Text = "Count"
                                    End If
                                    Me.LabelCreatedBy.Text = RowPending.UserID
                                    Me.LabelCreatedDate.Text = RowPending.CreatedDate.ToString("dd-MMMM-yyyy")
                                    Session("TypePurge") = RowPending.TypePurge
                                    Session("StartDate") = RowApproval.StartDate.ToString("MM/dd/yyyy 00:00")
                                    Session("EndDate") = RowApproval.EndDate.ToString("MM/dd/yyyy 23:59")
                                    Session("Count") = RowApproval.Count
                                End Using
                            End Using
                        End If
                    End Using
                End Using

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    ''' <summary>
    ''' Reject Purge
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click
        Dim oSQLTrans As SqlTransaction = Nothing

        Try

            Using AccessAuditTrailPendingApproval As New AMLDAL.AMLDataSetTableAdapters.AuditTrailMaintenance_PendingApprovalTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAuditTrailPendingApproval)
                AccessAuditTrailPendingApproval.DeleteAuditTrailMaintenance_PendingApproval(Me.pk_PendingApproval_id)
                Using AccessAuditTrailApproval As New AMLDAL.AMLDataSetTableAdapters.AuditTrailMaintenance_ApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAuditTrailApproval, oSQLTrans)
                    AccessAuditTrailApproval.DeleteAuditTrailMaintenance_Approval(Me.pk_Approval_id)
                End Using
                Me.InsertAuditTrail("Reject", oSQLTrans)
            End Using
            oSQLTrans.Commit()
            'Me.DeleteAllApproval(oSQLTrans)
            'Me.InsertAuditTrail("Reject", oSQLTrans)
            Sahassa.AML.Commonly.SessionIntendedPage = "AuditTrailMaintenanceApproval.aspx"
            Me.Response.Redirect("AuditTrailMaintenanceApproval.aspx", False)
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If

        End Try
    End Sub
    ''' <summary>
    ''' Back
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "AuditTrailMaintenanceApproval.aspx"

        Me.Response.Redirect("AuditTrailMaintenanceApproval.aspx", False)
    End Sub
End Class
