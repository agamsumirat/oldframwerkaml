
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper
Imports System.Text.RegularExpressions

Partial Class MenuAdd
    Inherits Parent

    Public js As String = ""
    Public jsplain As String = ""
    Public sothink As String = ""
    Public ObjMenu As Sahassa.AML.AMLMenu
    Public BufferLevelPattern, BufferLevelMenuItem, BufferLevelMenuItemWithLink As StringBuilder

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     Untuk mengisi ComboGroup
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub FillMsGroup()
        Try
            Using AccessGroup As New AMLDAL.AMLDataSetTableAdapters.GroupTableAdapter
                Using dtGroup As AMLDAL.AMLDataSet.GroupDataTable = AccessGroup.GetData
                    Me.cbomenugroup.DataSource = dtGroup.Select() 'SqlDataRepository.MsGroupProvider.GetPaged("pk_MsGroup_id <> 1", "pk_msgroup_id", 0, Int16.MaxValue, 100)
                    Me.cbomenugroup.DataTextField = "GroupName"
                    Me.cbomenugroup.DataValueField = "GroupID"
                    Me.cbomenugroup.DataBind()
                End Using
            End Using
        Catch
            Throw
        End Try
    End Sub

    Public Function Split(ByVal FileName As String) As String
        Dim OriFilename As String = FileName
        Dim RemoveExt As String = OriFilename.Substring(0, OriFilename.LastIndexOf("."))
        Dim r As Regex = New Regex("[A-Z]")
        Dim GroupColl As MatchCollection = r.Matches(RemoveExt)
        Dim last, curr As Integer
        curr = 0
        last = 0
        Dim jadi As String = ""
        For Each i As Match In GroupColl
            curr = i.Index
            If curr <> last Then
                jadi = jadi + RemoveExt.Substring(last, curr - last) & " "
            End If
            last = curr
        Next
        jadi += RemoveExt.Substring(last, RemoveExt.Length - last)
        Return jadi
    End Function
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     Untuk menguisi ComboHyperLink
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub FillHyperlink()
        Try
            Me.cbomenuhyperlink.Items.Clear()
            Using AccessFileSecurity As New AMLDAL.AMLDataSetTableAdapters.FileSecurityTableAdapter
                Using dtFileSecurity As AMLDAL.AMLDataSet.FileSecurityDataTable = AccessFileSecurity.GetData
                    'Dim FileSecurityFileName As Int32
                    'Dim ModuleFileSecurityFileName As String

                    For Each RowLink As AMLDAL.AMLDataSet.FileSecurityRow In dtFileSecurity.Select("FileSecurityGroupID =" & Me.cbomenugroup.SelectedValue, "FileSecurityFileName")
                        Me.cbomenuhyperlink.Items.Add(New ListItem(Split(RowLink.FileSecurityFileName), RowLink.FileSecurityFileName))
                    Next
                    'Me.cbomenuhyperlink.DataSource = dtFileSecurity.Select("FileSecurityGroupID =" & Me.cbomenugroup.SelectedValue, "FileSecurityFileName") 'DataRepository.FileSecurityProvider.GetPaged("fk_MsGroup_id=" & cbomenugroup.SelectedValue & "", "FileName", 0, 1000, 1)
                    'Me.cbomenuhyperlink.DataTextField = "FileSecurityFileName"
                    'Me.cbomenuhyperlink.DataValueField = "FileSecurityFileName"
                    'Me.cbomenuhyperlink.DataBind()
                    Me.cbomenuhyperlink.Items.Insert(0, "No link/link removed.")
                End Using
            End Using
            'Me.cbomenuhyperlink.DataSource = DataRepository.FileSecurityProvider.GetPaged("fk_MsGroup_id=" & cbomenugroup.SelectedValue & "", "FileName", 0, 1000, 1)
            'Me.cbomenuhyperlink.DataTextField = "FileName"
            'Me.cbomenuhyperlink.DataValueField = "FileName"
            'Me.cbomenuhyperlink.DataBind()
            'Me.cbomenuhyperlink.Items.Insert(0, "No link/link removed.")
        Catch
            Throw
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     Load Page
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            ObjMenu = New Sahassa.AML.AMLMenu
            If Not Page.IsPostBack Then
                'load group
                FillMsGroup()

                If cbomenugroup.Items.Count > 0 Then
                    cbomenugroup.SelectedIndex = 0
                    Init_Data(Integer.Parse(cbomenugroup.SelectedValue))
                End If

                'register button
                ImageSave.Attributes.Add("onclick", "javascript:menuFixTreeStructure();javascript:menuSaveToServer();")

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try

    End Sub
    ''' <summary>
    ''' Initial Data
    ''' </summary>
    ''' <param name="intGroupID"></param>
    ''' <remarks></remarks>
    Private Sub Init_Data(ByVal intGroupID As Integer)

        'load  hyperlink
        Try
            FillHyperlink()
            ObjMenu.GenerateMenu(intGroupID, "")

            sothink = ObjMenu.GetResultInSothinkFormat()
            js = ObjMenu.GetResultInArray()
            jsplain = ObjMenu.GetResultInArrayPlain()

            Dim i As Integer = 0
            Dim stmp As String() = Nothing

            BufferLevelPattern = New StringBuilder
            stmp = ObjMenu.GetLevelPattern()
            BufferLevelPattern.Append("[")
            For i = 0 To stmp.Length - 1
                If Not (stmp(i) Is Nothing) Then
                    BufferLevelPattern.Append("'" & stmp(i).Replace("'", "\'").Replace(vbLf, "").Replace(vbCr, "") & "'")
                    BufferLevelPattern.Append(",")
                End If
            Next i
            BufferLevelPattern.Remove(BufferLevelPattern.Length - 1, 1)
            BufferLevelPattern.Append("]")

            BufferLevelMenuItem = New StringBuilder
            stmp = ObjMenu.GetLevelMenuITem()
            BufferLevelMenuItem.Append("[")
            For i = 0 To stmp.Length - 1
                If Not (stmp(i) Is Nothing) Then
                    BufferLevelMenuItem.Append("'" & stmp(i).Replace("'", "\'").Replace(vbLf, "").Replace(vbCr, "") & "'")
                    BufferLevelMenuItem.Append(",")
                End If
            Next i
            BufferLevelMenuItem.Remove(BufferLevelMenuItem.Length - 1, 1)
            BufferLevelMenuItem.Append("]")

            BufferLevelMenuItemWithLink = New StringBuilder
            stmp = ObjMenu.GetLevelMenuItemWithLink()
            BufferLevelMenuItemWithLink.Append("[")
            For i = 0 To stmp.Length - 1
                If Not (stmp(i) Is Nothing) Then
                    BufferLevelMenuItemWithLink.Append("'" & stmp(i).Replace("'", "\'").Replace(vbLf, "").Replace(vbCr, "") & "'")
                    BufferLevelMenuItemWithLink.Append(",")
                End If
            Next i
            BufferLevelMenuItemWithLink.Remove(BufferLevelMenuItemWithLink.Length - 1, 1)
            BufferLevelMenuItemWithLink.Append("]")

        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try

    End Sub 'Init_Data 

    ''' <summary>
    ''' Select Change
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub cbomenugroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbomenugroup.SelectedIndexChanged
        Init_Data(Integer.Parse(cbomenugroup.SelectedValue))
        LabelMessage.Visible = False
    End Sub
    ''' <summary>
    ''' custom validator
    ''' untuk cek approval
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="args"></param>
    ''' <remarks></remarks>
    Protected Sub ValidCustomComboGroup_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles ValidCustomComboGroup.ServerValidate
        Try
            Using AccessMenuApproval As New AMLDAL.AMLDataSetTableAdapters.SHSMenu_ApprovalTableAdapter
                Using dtMenuApproval As AMLDAL.AMLDataSet.SHSMenu_ApprovalDataTable = AccessMenuApproval.GetCountMenuApprovalByGroupID(Me.cbomenugroup.SelectedValue)
                    args.IsValid = IIf(dtMenuApproval.Rows(0)("jml") = 0, True, False)
                    ValidCustomComboGroup.ErrorMessage = "Group Menu " & cbomenugroup.SelectedItem.Text.Trim & " already added and waiting for approval" & "<br>"
                End Using
            End Using
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage += ex.Message & "<br>"
        End Try
    End Sub
    ''' <summary>
    ''' Generate menu
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub GenerateMenu()
        Dim ObjAMLMenu As New Sahassa.AML.AMLMenu
        Dim ObjCommonly As New Sahassa.AML.Commonly
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            Dim strMenuItems As String() = Request.Form.GetValues("lstMenuSubmit")
            ' Using TransScope As New Transactions.TransactionScope
            Using AccessSHS_Menu As New AMLDAL.AMLDataSetTableAdapters.SHS_MenuTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessSHS_Menu, Data.IsolationLevel.ReadUncommitted)
                Using dtAccessSHS_Menu As AMLDAL.AMLDataSet.SHS_MenuDataTable = AccessSHS_Menu.GetDataSHSMenuByGroupID(Me.cbomenugroup.SelectedValue)
                    Dim CountApproval As Integer = dtAccessSHS_Menu.Rows.Count

                    ' jika super user
                    If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                        'If CountApproval = 0 Then
                        'ObjAMLMenu.SaveMenu(Integer.Parse(cbomenugroup.SelectedValue), "Add", strMenuItems)
                        'Else
                        ObjAMLMenu.SaveMenu(Integer.Parse(cbomenugroup.SelectedValue), "Edit", strMenuItems)
                        'End If

                        Init_Data(Integer.Parse(cbomenugroup.SelectedValue))
                        ObjAMLMenu.UpdateWebConfig(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile.ToString())
                        ObjAMLMenu.GenerateAllMenu(Server.MapPath("./script"))

                        LabelMessage.Text = "All system security and menu settings have been updated successfully.<br>During this moment, all users may have authorization request problem issue, therefore all users should re-login."
                        LabelMessage.Visible = True

                        Using AccessUpdateUser As New AMLDAL.AMLDataSetTableAdapters.QueriesSHSMenu
                            'Sahassa.AML.TableAdapterHelper.SetTransaction(AccessUpdateUser, oSQLTrans)
                            If Sahassa.AML.Commonly.SessionUserId IsNot Nothing Then
                                AccessUpdateUser.UpdateUser_forSHSMenu(CInt(Sahassa.AML.Commonly.SessionPkUserId), "", Boolean.FalseString)
                            End If
                        End Using

                        Sahassa.AML.Commonly.SessionIntendedPage = "Login.aspx"

                        Me.Response.Redirect("Login.aspx", False)
                    Else
                        ' selain super user        
                        If CountApproval = 0 Then
                            ObjAMLMenu.SaveMenu(Integer.Parse(cbomenugroup.SelectedValue), "Add", strMenuItems)
                            Using AccessMenuPendingApproval As New AMLDAL.AMLDataSetTableAdapters.InsertMenuPendingApprovalTableAdapter
                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessMenuPendingApproval, oSQLTrans)
                                Dim IdMenuPendingApproval As Integer = AccessMenuPendingApproval.Insert1(Sahassa.AML.Commonly.SessionUserId, Now, "Menu", "", Sahassa.AML.Commonly.TypeMode.Add)
                                ObjAMLMenu.SaveMenuToAddApproval(IdMenuPendingApproval, Integer.Parse(Me.cbomenugroup.SelectedValue), Sahassa.AML.Commonly.TypeMode.Add, strMenuItems)
                            End Using

                            Session("Link") = "MenuAdd.aspx"

                            Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=8721&Identifier=" & cbomenugroup.SelectedItem.Text.Trim

                            Response.Redirect("MessagePending.aspx?MessagePendingID=8721&Identifier=" & cbomenugroup.SelectedItem.Text.Trim, False)
                        Else
                            Using AccessMenuPendingApproval As New AMLDAL.AMLDataSetTableAdapters.InsertMenuPendingApprovalTableAdapter
                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessMenuPendingApproval, oSQLTrans)
                                Dim IdMenuPendingApproval As Integer = AccessMenuPendingApproval.Insert1(Sahassa.AML.Commonly.SessionUserId, Now, "Menu", "", Sahassa.AML.Commonly.TypeMode.Edit)
                                ObjAMLMenu.SaveMenuToUpdateApprovalOld(IdMenuPendingApproval, Integer.Parse(Me.cbomenugroup.SelectedValue), Sahassa.AML.Commonly.TypeMode.Edit, strMenuItems)
                                ObjAMLMenu.SaveMenuUpdate(Me.cbomenugroup.SelectedValue, strMenuItems)
                                ObjAMLMenu.SaveMenuToUpdateApprovalNew(IdMenuPendingApproval, Integer.Parse(Me.cbomenugroup.SelectedValue), Sahassa.AML.Commonly.TypeMode.Edit, strMenuItems)
                            End Using

                            Session("Link") = "MenuAdd.aspx"

                            Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=8722&Identifier=" & cbomenugroup.SelectedItem.Text.Trim

                            Response.Redirect("MessagePending.aspx?MessagePendingID=8722&Identifier=" & cbomenugroup.SelectedItem.Text.Trim, False)
                        End If
                    End If
                End Using
                oSQLTrans.Commit()
            End Using
        Catch
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub

    ''' <summary>
    ''' Save Menu
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
     
        Try
            If Page.IsValid Then
                If Me.cbomenugroup.SelectedValue = 1 Then
                    If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                        Me.GenerateMenu()
                    Else
                        Throw New Exception("Only SuperUser have a permission to change menu 'SuperUser'.")
                    End If
                Else
                    Me.GenerateMenu()
                End If
            Else
                Init_Data(Integer.Parse(cbomenugroup.SelectedValue))
            End If
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class
