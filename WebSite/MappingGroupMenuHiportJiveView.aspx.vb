Option Strict On
Option Explicit On
Imports amlbll
Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports SahassaNettier.Services
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper


Imports System.IO
Imports System.Collections.Generic

Partial Class MappingGroupMenuHiportJiveView
    Inherits Parent

    Public Property PK_MappingGroupMenuHIPORTJIVE_ID() As String
        Get
            Return CType(Session("MappingGroupMenuHiportJiveView.PK_MappingGroupMenuHIPORTJIVE_ID"), String)
        End Get
        Set(ByVal value As String)
            Session("MappingGroupMenuHiportJiveView.PK_MappingGroupMenuHIPORTJIVE_ID") = value
        End Set
    End Property

    Public Property SetnGetGroupName() As String
        Get
            If Not Session("MappingGroupMenuHiportJiveView.SetnGetGroupName") Is Nothing Then
                Return CStr(Session("MappingGroupMenuHiportJiveView.SetnGetGroupName"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("MappingGroupMenuHIPORTJIVEView.SetnGetGroupName") = value
        End Set
    End Property

    Private Property SetnGetSort() As String
        Get
            Return CType(IIf(Session("MappingGroupMenuHIPORTJIVEView.Sort") Is Nothing, "GroupName  asc", Session("MappingGroupMenuHIPORTJIVEView.Sort")), String)
        End Get
        Set(ByVal Value As String)
            Session("MappingGroupMenuHIPORTJIVEView.Sort") = Value
        End Set
    End Property

    Private Property SetnGetRowTotal() As Int32
        Get
            Return CType(IIf(Session("MappingGroupMenuHIPORTJIVEView.RowTotal") Is Nothing, 0, Session("MappingGroupMenuHIPORTJIVEView.RowTotal")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("MappingGroupMenuHIPORTJIVEView.RowTotal") = Value
        End Set
    End Property
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property

    Private Property SetnGetCurrentPage() As Int32
        Get
            Return CType(IIf(Session("MappingGroupMenuHIPORTJIVEView.CurrentPage") Is Nothing, 0, Session("MappingGroupMenuHIPORTJIVEView.CurrentPage")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("MappingGroupMenuHIPORTJIVEView.CurrentPage") = Value
        End Set
    End Property

    Function Getvw_MappingGroupMenuHiportJive(ByVal strWhereClause As String, ByVal strOrderBy As String, ByVal intStart As Integer, ByVal intPageLength As Integer, ByRef intCount As Integer) As VList(Of vw_MappingGroupMenuHiportJive)
        Dim Objvw_MappingGroupMenuHiportJive As VList(Of vw_MappingGroupMenuHiportJive) = Nothing
        Objvw_MappingGroupMenuHiportJive = DataRepository.vw_MappingGroupMenuHiportJiveProvider.GetPaged(strWhereClause, strOrderBy, intStart, intPageLength, intCount)
        Return Objvw_MappingGroupMenuHiportJive
    End Function

    Public Property SetnGetBindTable() As VList(Of vw_MappingGroupMenuHiportJive)
        Get
            Dim strWhereClause(-1) As String
            Dim strAllWhereClause As String = ""
            If Me.SetnGetCurrentPage > Me.GetPageTotal - 1 And Me.GetPageTotal - 1 > 0 Then
                Me.SetnGetCurrentPage = Me.GetPageTotal - 1
            End If
            If SetnGetGroupName.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = "GroupName like '%" & SetnGetGroupName.Trim.Replace("'", "''") & "%'"
            End If

            strAllWhereClause = String.Join(" and ", strWhereClause)
            'If strAllWhereClause.Trim.Length > 0 Then
            '    strAllWhereClause += " AND FK_MsUserID IN (SELECT u.pkUserID FROM [User] u WHERE u.UserID IN(select UserId from UserWorkingUnitAssignment Where WorkingUnitId in (select WorkingUnitId from UserWorkingUnitAssignment where UserId = '" & Sahassa.AML.Commonly.SessionUserId & "')))"
            'Else
            '    strAllWhereClause += " FK_MsUserID IN (SELECT u.pkUserID FROM [User] u WHERE u.UserID IN(select UserId from UserWorkingUnitAssignment Where WorkingUnitId in (select WorkingUnitId from UserWorkingUnitAssignment where UserId = '" & Sahassa.AML.Commonly.SessionUserId & "')))"
            'End If
            Session("MappingGroupMenuHIPORTJIVEView.Table") = Getvw_MappingGroupMenuHiportJive(strAllWhereClause, SetnGetSort, SetnGetCurrentPage, Sahassa.AML.Commonly.GetDisplayedTotalRow, Me.SetnGetRowTotal)


            Return CType(Session("MappingGroupMenuHIPORTJIVEView.Table"), VList(Of vw_MappingGroupMenuHiportJive))


        End Get
        Set(ByVal value As VList(Of vw_MappingGroupMenuHiportJive))
            Session("MappingGroupMenuHIPORTJIVEView.Table") = value
        End Set
    End Property



    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select            
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub ClearThisPageSessions()
        'Session("MappingGroupMenuHIPORTJIVEView.PK_MappingGroupMenuSTRPPATK_ApprovalDetail_ID") = Nothing
        Session("MappingGroupMenuHIPORTJIVEView.SetnGetGroupName") = Nothing

        SetnGetCurrentPage = Nothing
        Me.SetnGetRowTotal = Nothing
        Me.SetnGetSort = Nothing

    End Sub


    Private Sub bindgrid()
        SettingControlSearching()
        Me.GridGMHiportJive.DataSource = Me.SetnGetBindTable
        Me.GridGMHiportJive.CurrentPageIndex = Me.SetnGetCurrentPage
        Me.GridGMHiportJive.VirtualItemCount = Me.SetnGetRowTotal
        Me.GridGMHiportJive.DataBind()

        'If SetnGetRowTotal > 0 Then
        '    LabelNoRecordFound.Visible = False
        'Else
        '    LabelNoRecordFound.Visible = True
        'End If
    End Sub

    Private Sub SettingPropertySearching()

        SetnGetGroupName = TxtGroupName.Text.Trim

    End Sub

    Private Sub SettingControlSearching()

        TxtGroupName.Text = SetnGetGroupName

    End Sub



    Private Sub SetInfoNavigate()

        Me.PageCurrentPage.Text = (Me.SetnGetCurrentPage + 1).ToString
        Me.PageTotalPages.Text = Me.GetPageTotal.ToString
        Me.PageTotalRows.Text = Me.SetnGetRowTotal.ToString
        Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
        Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
        Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
        Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0

    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

        Try
            Me.bindgrid()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try

    End Sub

    Protected Sub ImageButtonSearchCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearchCancel.Click
        Try

            SetnGetGroupName = Nothing

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Try
            SettingPropertySearching()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then

                ClearThisPageSessions()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using

                bindgrid()

            End If
            GridGMHIPORTJIVE.PageSize = CInt(Sahassa.AML.Commonly.GetDisplayedTotalRow)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub LinkButtonAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonAddNew.Click

        Try
            Response.Redirect("MappingGroupMenuHIPORTJIVEAdd.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try

    End Sub

    Protected Sub GridGMHIPORTJIVE_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridGMHIPORTJIVE.EditCommand
        Dim PK_MappingGroupMenuHIPORTJIVE_ID As Integer 'primary key
        'Dim StrUserID As String  'unik
        Try
            PK_MappingGroupMenuHIPORTJIVE_ID = CType(e.Item.Cells(1).Text, Integer)
            'StrUserID = e.Item.Cells(2).Text

            Response.Redirect("MappingGroupMenuHIPORTJIVEEdit.aspx?PK_MappingGroupMenuHIPORTJIVE_ID=" & PK_MappingGroupMenuHIPORTJIVE_ID, False)
            'Using ObjMsBranchBll As New SahassaBLL.MsHelpBLL
            '    If ObjMsBranchBll.IsNotExistInApproval(StrUserID) Then
            '        Response.Redirect("MsHelpEdit.aspx?PkMsHelpID=" & PkMsHelpID, False)
            '    End If
            'End Using

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridGMHIPORTJIVE_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridGMHIPORTJIVE.DeleteCommand
        Dim PK_MappingGroupMenuHIPORTJIVE_ID As Integer 'primary key
        'Dim StrUnikID As String  'unik
        Try
            PK_MappingGroupMenuHIPORTJIVE_ID = CType(e.Item.Cells(1).Text, Integer)
            'StrUnikID = e.Item.Cells(2).Text

            Response.Redirect("MappingGroupMenuHIPORTJIVEDelete.aspx?PK_MappingGroupMenuHIPORTJIVE_ID=" & PK_MappingGroupMenuHIPORTJIVE_ID, False)
            'Using ObjMsBranchBll As New SahassaBLL.MsHelpBLL
            '    If ObjMsBranchBll.IsNotExistInApproval(StrUnikID) Then
            '        Response.Redirect("MsHelpDelete.aspx?PkMsHelpID=" & PkMsHelpID, False)
            '    End If
            'End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub



    Protected Sub GridGMHIPORTJIVE_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridGMHIPORTJIVE.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then

                Dim EditButton As LinkButton = CType(e.Item.Cells(3).FindControl("LnkEdit"), LinkButton)
                Dim DelButton As LinkButton = CType(e.Item.Cells(4).FindControl("LnkDelete"), LinkButton)

                e.Item.Cells(0).Text = CStr((e.Item.ItemIndex + 1))
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try            
            If IsNumeric(Me.TextGoToPage.Text) AndAlso (CInt(Me.TextGoToPage.Text) > 0) Then
                If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                    Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                Else
                    Throw New Exception(Sahassa.AML.CommonlyEnum.EXECPTION_PAGENUMBER_GREATER_TOTALPAGENUMBER)
                End If
            Else
                Throw New Exception(Sahassa.AML.CommonlyEnum.EXECPTION_PAGENUMBER_NUMERICPOSITIF)
            End If

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridGMHiportJive_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridGMHiportJive.SortCommand
        Dim GridUser As DataGrid = CType(source, DataGrid)
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class

