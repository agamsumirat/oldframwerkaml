﻿Imports SahassaNettier.Entities
Imports SahassaNettier.Data
Imports AMLBLL

Partial Class SegmentAdd
    Inherits Parent

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "SegmentView.aspx"

            Me.Response.Redirect("SegmentView.aspx", False)
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Function isValidData() As Boolean
        Try
            If txtSegmentCode.Text.Trim.Length = 0 Then
                Throw New Exception("Segment Code must be filled")
            End If
            If Not SegmentBLL.IsUniqueSegmentCode(txtSegmentCode.Text) Then
                Throw New Exception("Sement Code " & txtSegmentCode.Text & " existed")
            End If
            Return True
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
            Return False
        End Try
    End Function

    Sub ClearControl()
        txtDescription.Text = ""
        txtSegmentCode.Text = ""
    End Sub

    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        If isValidData() Then
            Try

                Using objUser As User = AMLBLL.UserBLL.GetUserByPkUserID(Sahassa.AML.Commonly.SessionPkUserId)
                    If Not IsNothing(objUser) Then
                        Using objSegment As New Segment
                            objSegment.SegmentCode = txtSegmentCode.Text
                            objSegment.SegmentDescription = txtDescription.Text
                            objSegment.Activation = True
                            objSegment.CreatedDate = Now
                            objSegment.CreatedBy = objUser.UserName
                            objSegment.LastUpdateDate = Now
                            objSegment.LastUpdateBy = objUser.UserName



                            If Sahassa.AML.Commonly.SessionGroupName.ToLower = "superuser" Then
                                If SegmentBLL.SaveAdd(objSegment) Then
                                    lblMessage.Text = "Insert Data Success Click Ok to Add new data"
                                    mtvSegmentAdd.ActiveViewIndex = 1
                                End If
                            Else
                                If SegmentBLL.SaveAddApproval(objSegment) Then
                                    lblMessage.Text = "Data has been inserted to Approval Click Ok to Add new data"
                                    mtvSegmentAdd.ActiveViewIndex = 1
                                End If
                            End If
                        End Using
                    End If
                End Using

            Catch ex As Exception
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            End Try
        End If
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                mtvSegmentAdd.ActiveViewIndex = 0

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
            End If
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnOK.Click
        Try
            mtvSegmentAdd.ActiveViewIndex = 0
            ClearControl()
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBack.Click
        Try
            Response.Redirect("SegmentView.aspx")
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class


