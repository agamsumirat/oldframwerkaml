﻿Imports CDDLNPNettier.Data
Imports CDDLNPNettier.Entities
Imports AMLBLL
Imports AMLBLL.CDDLNPBLL
Imports System.Data

Partial Class CDDLNPAdd
    Inherits Parent

    Private ReadOnly Property GetFK_CDD_Type_ID() As Int32
        Get
            Return Request.QueryString("TID")
        End Get
    End Property

    Private ReadOnly Property GetFK_CDD_NasabahType_ID() As Int32
        Get
            Return Request.QueryString("NTID")
        End Get
    End Property

    Public Property ScreeningCustomerPK As Int64
        Get
            Return Session("ScreeningCustomerPK")
        End Get
        Set
            Session("ScreeningCustomerPK") = Value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not Page.IsPostBack Then
                'Audit Trail Access Page
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using

                Select Case GetFK_CDD_Type_ID
                    Case 1, 3 : ImageHistory.Visible = False
                    Case 2, 4 : ImageHistory.Visible = True
                End Select

                'Reset Session
                SessionCDDLNPAuditTrailId = 0
                SessionCDDLNP_Attachment = New TList(Of CDDLNP_Attachment)

                ScreeningCustomerPK = -1
            End If

            'Set CDD Risk Fact Question Detail
            SessionCDD_RiskFactQuestionDetail = DataRepository.CDD_RiskFactQuestionDetailProvider.GetAll

            'Set CDD Customer Type
            Using objCustomerType As CDD_NasabahType = DataRepository.CDD_NasabahTypeProvider.GetByPK_CDD_NasabahType_ID(Me.GetFK_CDD_NasabahType_ID)
                If objCustomerType IsNot Nothing Then
                    SessionCustomerType = objCustomerType.CDD_NasabahType_ID
                End If
            End Using

            'Set CDD Type
            Using objCDDType As CDD_Type = DataRepository.CDD_TypeProvider.GetByPK_CDD_Type_ID(Me.GetFK_CDD_Type_ID)
                If objCDDType IsNot Nothing Then

                    If SessionReferenceID = 0 Then
                        lblHeader.Text = "EDD " & objCDDType.CDD_Type
                    Else
                        lblHeader.Text = "EDD for Beneficial Owner"
                        ClientScript.RegisterStartupScript(Page.GetType(), "BO", "alert('Mohon mengisi form BO atau pemilik sumber dana berikut');", True)
                    End If

                    SessionDefaultScore = objCDDType.CDD_DefaultScore
                End If
            End Using

            'Buat form
            Using objCDDLNP As New CDDLNPBLL
                objCDDLNP.ConstructFormAdd(Me.TableCategory, Me.GetFK_CDD_Type_ID, Me.GetFK_CDD_NasabahType_ID)
            End Using
            'ClientScript.RegisterStartupScript(Page.GetType(), "gotopage" & Guid.NewGuid.ToString, "javascript:var scroll = $(window).scrollTop();$('html').scrollTop(scroll);", True)

        Catch ex As Exception
            cvalPageErr.ErrorMessage = ex.Message
            cvalPageErr.IsValid = False
        End Try
    End Sub

    Protected Sub ImageSavePropose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSavePropose.Click
        Try
            Dim isExist As Boolean = False
            For Each objWorkflow As CDDLNP_WorkflowUpload In DataRepository.CDDLNP_WorkflowUploadProvider.GetPaged("List_RM LIKE '%" & Sahassa.AML.Commonly.SessionUserId & "%'", String.Empty, 0, Int32.MaxValue, 0)
                For Each objUserId As String In objWorkflow.List_RM.Split(";")
                    If String.Compare(Sahassa.AML.Commonly.SessionUserId, objUserId, StringComparison.OrdinalIgnoreCase) = 0 Then
                        isExist = True
                    End If
                Next
            Next

            If Not isExist Then
                Throw New Exception("Your user ID has not been assigned for workflow approval. Please contact your administrator.")
            End If

            Using objCDDLNP As New CDDLNPBLL
                If objCDDLNP.IsRequired(Me.TableCategory) Then
                    If objCDDLNP.IsNeedFillBO(Me.TableCategory) Then
                        SessionCDDLNPIsCompleted = False
                        objCDDLNP.CDDLNPAdd(Me.TableCategory, Me.GetFK_CDD_Type_ID, Me.GetFK_CDD_NasabahType_ID, SessionReferenceID)
                        Me.Response.Redirect("CDDLNPAdd.aspx?TID=" & Me.GetFK_CDD_Type_ID & "&NTID=2")

                    Else
                        SessionCDDLNPIsCompleted = True 'Jika CDD sudah complete maka langsung diajukan untuk approval
                        objCDDLNP.CDDLNPAdd(Me.TableCategory, Me.GetFK_CDD_Type_ID, Me.GetFK_CDD_NasabahType_ID, SessionReferenceID)
                        objCDDLNP.UpdateAMLRating(SessionReferenceID)
                        CDDLNPEmailSend.CDDLNPEmailSend(SessionReferenceID)
                        SessionReferenceID = 0
                        ClientScript.RegisterStartupScript(Page.GetType(), "Success", "alert('EDD is submitted for approval.');window.location='" & objCDDLNP.GetCDDLNPUrl(Me.GetFK_CDD_Type_ID) & "';", True)
                    End If
                End If

            End Using
        Catch ex As Exception
            cvalPageErr.ErrorMessage = ex.Message
            cvalPageErr.IsValid = False
        End Try
    End Sub

    Protected Sub ImageSaveDraft_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSaveDraft.Click
        Try
            Using objCDDLNP As New CDDLNPBLL
                SessionCDDLNPIsCompleted = False
                objCDDLNP.CDDLNPAdd(Me.TableCategory, Me.GetFK_CDD_Type_ID, Me.GetFK_CDD_NasabahType_ID, SessionReferenceID)
                SessionReferenceID = 0
                ClientScript.RegisterStartupScript(Page.GetType(), "Success", "alert('EDD is saved as draft.');window.location='" & objCDDLNP.GetCDDLNPUrl(Me.GetFK_CDD_Type_ID) & "';", True)
            End Using
        Catch ex As Exception
            cvalPageErr.ErrorMessage = ex.Message
            cvalPageErr.IsValid = False
        End Try
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Try
            Dim strRedirectUrl As String = String.Empty
            Using objCDDLNP As New CDDLNPBLL
                'If Me.GetReferenceID > 0 Then
                '    Session("CDDLNPEdit.ReferenceID") = 0
                '    Me.Response.Redirect("CDDLNPEdit.aspx?CID=" & Me.GetReferenceID & "&TID=" & Me.GetFK_CDD_Type_ID & "&NTID=" & Me.GetFK_CDD_NasabahType_ID)
                'Else
                If SessionReferenceID = 0 Then
                    strRedirectUrl = GetCDDLNPMenuUrl(Me.GetFK_CDD_Type_ID)
                Else
                    strRedirectUrl = objCDDLNP.GetCDDLNPUrl(Me.GetFK_CDD_Type_ID)
                End If
                SessionReferenceID = 0
                'Me.Response.Redirect(objCDDLNP.GetCDDLNPUrl(Me.GetFK_CDD_Type_ID))
                'End If
            End Using
            Me.Response.Redirect(strRedirectUrl)
        Catch ex As Exception
            cvalPageErr.ErrorMessage = ex.Message
            cvalPageErr.IsValid = False
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Dim l As LinkButton = CType(TableCategory.FindControl("lnkSuspect"), LinkButton)
            Dim totalSuspect As Long = EKABLL.CDDBLL.GetTotalSuspectScreening(ScreeningCustomerPK)
            
            If totalSuspect < 0 Then
                l.Visible = False
            Else
                l.Visible = True
                l.Text = totalSuspect.ToString() & " suspect(s)"
                SessionCDDLNPAuditTrailCount = totalSuspect.ToString()
            End If

            'Using objAudit As SahassaNettier.Entities.AuditTrail_Potensial_Screening_Customer = SahassaNettier.Data.DataRepository.AuditTrail_Potensial_Screening_CustomerProvider.GetByPK_AuditTrail_Potensial_Screening_Customer_ID(SessionCDDLNPAuditTrailId)
            '    If objAudit Is Nothing Then
            '        l.Visible = False
            '    Else
            '        l.Visible = True
            '        l.Text = objAudit.SuspectMatch.ToString & " suspect(s)"
            '        SessionCDDLNPAuditTrailCount = objAudit.SuspectMatch.ToString
            '    End If
            'End Using

            Using objCDDLNP As New CDDLNPBLL
                Dim lblAMLResult As Label = CType(TableCategory.FindControl("lblAMLResult"), Label)
                SessionRiskRating = objCDDLNP.CalculateRating(Me.TableCategory)
                lblAMLResult.ForeColor = Drawing.Color.FromName(objCDDLNP.GetRatingColor(SessionRiskRating))
                lblAMLResult.Text = SessionRiskRating

                objCDDLNP.HideUncheck(Me.TableCategory)
            End Using
        Catch ex As Exception
            cvalPageErr.ErrorMessage = ex.Message
            cvalPageErr.IsValid = False
        End Try
    End Sub

    Protected Sub CvalPageErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles cvalPageErr.PreRender
        If cvalPageErr.IsValid = False Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
        End If
    End Sub

    Protected Sub ImageHistory_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageHistory.Click
        Try
            Dim strCIF As String = String.Empty
            Using objCDDLNP As New CDDLNPBLL
                strCIF = objCDDLNP.getCIF(Me.TableCategory)
            End Using

            If DataRepository.CDDLNPProvider.GetTotalItems("CIF = '" & strCIF & "' AND CreatedBy = '" & Sahassa.AML.Commonly.SessionUserId & "'", 0) = 0 Then
                Throw New Exception("No history was found for CIF : '" & strCIF & "'. Please fill below CIF correctly to view history.")
            End If
            
            Me.Controls.Add(New LiteralControl("<script>popupWindow = window.open('CDDLNPHistory.aspx?ID=" & strCIF & "','mustunique','scrollbars=1,directories=0,height=600,width=800,location=0,menubar=0,resizable=1,status=0,toolbar=0');popupWindow.focus();</script> "))
        Catch ex As Exception
            cvalPageErr.ErrorMessage = ex.Message
            cvalPageErr.IsValid = False
        End Try
    End Sub

    Private Function GetCDDLNPMenuUrl(ByVal intCDDType As Int32) As String
        Select Case intCDDType
            Case 1, 2 : Return "CDDLNP_PBGCustomerMenu.aspx"
            Case 3, 4 : Return "CDDLNP_NonPBGCustomerMenu.aspx"
            Case Else : Return "Default.aspx"
        End Select

    End Function
End Class