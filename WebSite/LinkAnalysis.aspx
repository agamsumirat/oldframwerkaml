<%
    Dim strCommand As String
    strCommand = Session("LinkAnalysisGraphCommand")
    Dim dot As New WINGRAPHVIZLib.DOT
    Try
        Response.Clear()
        If Not strCommand Is Nothing Then
            Response.ContentType = "image/svg+xml"
            'Generate TextImage(SVG)

            Dim strXML As String
            strXML = dot.ToSvg(strCommand)
            Response.Write(strXML)
            Response.End()
        End If
    Catch tex As Threading.ThreadAbortException
        ' ignore cause by Response.End
    Catch ex As Exception
        Throw ex
    Finally
        dot = Nothing
    End Try
%>