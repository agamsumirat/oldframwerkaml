Imports System.IO
Imports System
Imports System.Data
Imports System.Data.SqlClient

Partial Class CustomTransactionAnalysis
    Inherits Parent

    Dim ProductDescriptionCount As Integer = 0

    Private ReadOnly Property GetPk_Account_Id() As Integer
        Get
            Return Me.Page.Request.Params.Get("Pk_Account")
        End Get
    End Property

    'Public Sub FillTransactionType()
    '    Try
    '        Me.ListTransactionType.Items.Clear()
    '        Me.ListTransactionType.Items.Add("---")
    '        Dim flag As Boolean = 0
    '        Using AccessTransactionType As New AMLDAL.AuxTransactionCodeTableAdapters.AuxTransactionCode_SelectCommandTableAdapter
    '            For Each RowTransactionType As AMLDAL.AuxTransactionCode.AuxTransactionCode_SelectCommandRow In AccessTransactionType.GetData
    '                Me.ListTransactionType.Items.Add( _
    '                RowTransactionType.AuxTransactionCode & "-" & _
    '                RowTransactionType.AuxTransactionCodeDescription)
    '                flag = 1
    '            Next
    '            If flag = 0 Then
    '                Me.ListTransactionType.Items.Clear()
    '            End If
    '        End Using
    '    Catch
    '        Throw
    '    End Try
    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                Me.ClearThisPageSessions()
                'Me.FillTransactionType()
                'Tambahkan event handler OnClick yang akan menampilkan kalender javascript
                Me.popUpFromDate.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TextDateFrom.ClientID & "'), 'dd-mmm-yyyy')")
                Me.PopCalcToDate.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TextDateTo.ClientID & "'), 'dd-mmm-yyyy')")
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
            End If
        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub ClearThisPageSessions()
        Session("CustomTransaction_CIFNo") = Nothing
        Session("CustomTransaction_Name") = Nothing
        Session("CustomTransaction_AccountNo") = Nothing
        Session("CustomTransaction_TransactionType") = Nothing
        Session("CustomTransaction_TransactionAmountFrom") = Nothing
        Session("CustomTransaction_TransactionAmountTo") = Nothing
        Session("CustomTransaction_TransactionDateFrom") = Nothing
        Session("CustomTransaction_TransactionDateTo") = Nothing
        Session("CustomTransaction_AccountType") = Nothing
        Session("CustomTransaction_CurrencyType") = Nothing
        Session("CustomTransaction_ProductType") = Nothing
    End Sub

    Protected Sub ImageSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSearch.Click
        Try
            Dim strSearch As String = ""

            'ambil TransactionDate
            If IsDate(Me.TextDateFrom.Text) And IsDate(Me.TextDateTo.Text) Then
                Dim NewsStartDate As DateTime = CDate(Me.TextDateFrom.Text)
                Dim NewsEndDate As DateTime = CDate(Me.TextDateTo.Text)

                If strSearch <> "" Then strSearch = strSearch & " And "
                strSearch = strSearch & " TransactionDate between '" & Me.TextDateFrom.Text & "' and '" & Me.TextDateTo.Text & "' "

                If DateDiff(DateInterval.Day, NewsStartDate, NewsEndDate) < 0 Then
                    Throw New Exception("Start Date must be less than or equal to End Date.")
                End If

                'ambil CIFNO
                If TextCIF.Text <> "" Then
                    If strSearch <> "" Then strSearch = strSearch & " And "
                    strSearch = strSearch & " CIFNo='" & Me.TextCIF.Text.Replace("'", "''") & "'"
                End If

                'ambil Name
                If TextName.Text <> "" Then
                    If strSearch <> "" Then strSearch = strSearch & " And "
                    strSearch = strSearch & " AccountName LIKE '" & Me.TextName.Text.Replace("'", "''") & "%' "
                End If

                'ambil AccountNo
                If TextAccountNo.Text <> "" Then
                    Session("CustomTransaction_AccountNo") = TextAccountNo.Text
                    If strSearch <> "" Then strSearch = strSearch & " And "
                    strSearch = strSearch & " AccountNo='" & Me.TextAccountNo.Text.Replace("'", "''") & "'"
                End If

                ''ambil TransactionCode
                'Dim splitTransactionType = Split(ListTransactionType.SelectedValue, "-", -1)
                'If splitTransactionType(0) <> "" Then
                '    If strSearch <> "" Then strSearch = strSearch & " And "
                '    strSearch = strSearch & " AuxiliaryTransactionCode='" & splitTransactionType(0).Replace("'", "''") & "' "
                'End If

                'ambil TransactionAmount
                If TextAmountFrom.Text <> "" And TextAmountTo.Text <> "" Then
                    If IsNumeric(Me.TextAmountFrom.Text) And IsNumeric(Me.TextAmountTo.Text) Then
                        If strSearch <> "" Then strSearch = strSearch & " And "
                        strSearch = strSearch & " TransactionAmount between '" & TextAmountFrom.Text & "' and '" & TextAmountTo.Text & "' "
                    End If
                End If

                'untuk inner join dengan table CFMAST
                If strSearch <> "" Then strSearch = strSearch & " and "
                strSearch = strSearch & "(CFMAST.CFINSC IN (SELECT InsiderCodeId FROM GroupInsiderCodeMapping WHERE GroupId= (select UserGroupId from [User] where UserId = '" & Sahassa.AML.Commonly.SessionUserId & "')) OR CFMAST.CFINSC='' OR CFMAST.CFINSC IS NULL) "
                'strSearch = strSearch & " CFMAST.CFINSC IN (SELECT InsiderCodeId FROM GroupInsiderCodeMapping WHERE GroupId= 2 OR CFMAST.CFINSC='' OR CFMAST.CFINSC IS NULL"

                Session("CustomTransactionSearchCriteria") = strSearch
                Sahassa.AML.Commonly.SessionIntendedPage = "CustomTransactionAnalysisDetail.aspx"
                Response.Redirect("CustomTransactionAnalysisDetail.aspx", False)

            Else
                Throw New Exception("Date is required")
            End If
        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
        End Try
    End Sub

End Class