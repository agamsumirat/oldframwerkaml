Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports System.Data
Imports System.Data.SqlClient
Imports iTextSharp
Imports iTextSharp.text.pdf
Imports iTextSharp.text.html
Imports iTextSharp.text.html.simpleparser
Partial Class ReportOverBookingTunai
    Inherits Parent


    Private Const ViewName = "ReportOverBookingTunai"
    Private excludefield() As String = {"UserIDAML"}
    Public Property Tables() As String
        Get
            If Session("ReportOverBookingTunai.Tables") Is Nothing Then
                Return ""
            Else
                Return CType(Session("ReportOverBookingTunai.Tables"), String)
            End If
        End Get
        Set(ByVal value As String)
            Session("ReportOverBookingTunai.Tables") = value
        End Set
    End Property


    Public Property Field() As String
        Get
            If Session("ReportOverBookingTunai.Field") Is Nothing Then
                Return ""
            Else
                Return CType(Session("ReportOverBookingTunai.Field"), String)
            End If
        End Get
        Set(ByVal value As String)
            Session("ReportOverBookingTunai.Field") = value
        End Set
    End Property

    Public Property Whereclause() As String
        Get
            If Session("ReportOverBookingTunai.Whereclause") Is Nothing Then
                Return ""
            Else
                Return CType(Session("ReportOverBookingTunai.Whereclause"), String)
            End If
        End Get
        Set(ByVal value As String)
            Session("ReportOverBookingTunai.Whereclause") = value
        End Set
    End Property


    Private Property Sort() As String
        Get
            Return IIf(Session("ReportOverBookingTunai.Sort") Is Nothing, "TimeEntered  asc", Session("ReportOverBookingTunai.Sort"))
        End Get
        Set(ByVal value As String)
            Session("ReportOverBookingTunai.Sort") = value
        End Set
    End Property

    Private Property CurrentPage() As Integer
        Get
            Return IIf(Session("ReportOverBookingTunai.CurrentPage") Is Nothing, 0, Session("ReportOverBookingTunai.CurrentPage"))
        End Get
        Set(ByVal value As Integer)
            Session("ReportOverBookingTunai.CurrentPage") = value
        End Set
    End Property

    Private Property RowTotal() As Integer
        Get
            Return IIf(Session("ReportOverBookingTunai.RowTotal") Is Nothing, 0, Session("ReportOverBookingTunai.RowTotal"))
        End Get
        Set(ByVal value As Integer)
            Session("ReportOverBookingTunai.RowTotal") = value
        End Set
    End Property
    Private ReadOnly Property PageTotal() As Integer
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.RowTotal)
        End Get
    End Property


    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First"
                    Me.CurrentPage = 0
                Case "Next"
                    Me.CurrentPage = Me.CurrentPage + 1
                Case "Prev"
                    Me.CurrentPage = Me.CurrentPage - 1
                Case "Last"
                    Me.CurrentPage = Me.PageTotal - 1
                Case Else
                    Throw New Exception("Unknown Command Name: " & e.CommandName)
            End Select
            BindGrid(Me.Field, Me.Whereclause)
        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Public Sub WebInfoPageNavigate()
        Try
            Me.label_CurrentPage.Text = Me.CurrentPage + 1
            Me.label_TotalPages.Text = Me.PageTotal
            Me.label_TotalRows.Text = Me.RowTotal

            Me.linkButton_First.Enabled = Not Me.CurrentPage = 0
            Me.linkButton_Next.Enabled = Not Me.CurrentPage + 1 = Me.PageTotal AndAlso Me.PageTotal <> 0
            Me.linkButton_Previous.Enabled = Not Me.CurrentPage = 0
            Me.linkButton_Last.Enabled = Not Me.CurrentPage + 1 = Me.PageTotal AndAlso Me.PageTotal <> 0
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public ReadOnly Property sqlquery() As String
        Get
            Return CType(Session("ReportOverBookingTunai.sqlquery"), String)

        End Get
    End Property





    Public ReadOnly Property SelectedDataRow() As DataRowView
        Get
            Return CType(Session("ReportOverBookingTunai.SelectedDataRow"), DataRowView)
        End Get

    End Property




    Public ReadOnly Property ObjDataFilter() As DataTable
        Get
            If Session("ReportOverBookingTunai.ObjDataFilter") Is Nothing Then
                Dim objData As New DataTable
                Dim ObjCol As New DataColumn("PK", GetType(Integer))
                ObjCol.AutoIncrement = True
                ObjCol.AutoIncrementSeed = 1
                ObjCol.AutoIncrementStep = 1
                objData.Columns.Add(ObjCol)
                objData.PrimaryKey = New DataColumn() {ObjCol}
                objData.Columns.Add("AndOr", GetType(String))
                objData.Columns.Add("ColumnName", GetType(String))
                objData.Columns.Add("Operator", GetType(String))
                objData.Columns.Add("nilai", GetType(String))
                Session("ReportOverBookingTunai.ObjDataFilter") = objData
                Return CType(Session("ReportOverBookingTunai.ObjDataFilter"), DataTable)
            Else
                Return CType(Session("ReportOverBookingTunai.ObjDataFilter"), DataTable)
            End If
        End Get
    End Property


    'Sub LoadData()
    '    Dim ObjQueryTableList As TList(Of QueryBuilder) = DataRepository.QueryBuilderProvider.GetPaged("", QueryBuilderColumn.QueryName.ToString & " asc", 0, Integer.MaxValue, 0)


    '    Dim objVw_WorkingUnitQueryBuilder As VList(Of vw_WorkingUnitQueryBuilder) = DataRepository.vw_WorkingUnitQueryBuilderProvider.GetPaged("UserID = '" & Sahassa.AML.Commonly.SessionUserId & "'", "", 0, Integer.MaxValue, 0)
    '    If objVw_WorkingUnitQueryBuilder.Count > 0 Then
    '        CboViewList.DataSource = objVw_WorkingUnitQueryBuilder
    '        CboViewList.DataTextField = vw_WorkingUnitQueryBuilderColumn.QueryDescription.ToString
    '        CboViewList.DataValueField = vw_WorkingUnitQueryBuilderColumn.QueryName.ToString
    '        CboViewList.AppendDataBoundItems = True
    '    End If

    '    CboViewList.Items.Add(New ListItem("[LIST TABLE]", ""))
    '    CboViewList.DataBind()
    'End Sub

    Sub ClearSession()
        Session("ReportOverBookingTunai.ObjDataFilter") = Nothing
        Session("ReportOverBookingTunai.SelectedDataRow") = Nothing
        Session("ReportOverBookingTunai.sqlquery") = Nothing
        Session("ReportOverBookingTunai.sort") = Nothing
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then

                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using

                ClearSession()
                LoadField()
                Me.popUp1.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TxtFieldAwal.ClientID & "'), 'dd-mm-yyyy')")
                Me.popUp2.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TxtFieldAkhir.ClientID & "'), 'dd-mm-yyyy')")

                Me.popUpStartDate.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TxtStartDate.ClientID & "'), 'dd-mm-yyyy')")
                Me.PopUpEndDate.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TxtEndDate.ClientID & "'), 'dd-mm-yyyy')")

            End If
        Catch ex As Exception
            CValPageError.IsValid = False
            CValPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Sub LoadField()
        Dim ObjDs As Data.DataSet
        Try
            CboFilter.Items.Clear()
            Dim objParam As Object() = {ViewName}
            ObjDs = DataRepository.Provider.ExecuteDataSet("usp_GetQueryFieldByTableName", objParam)
            FieldList.DataSource = ObjDs.Tables(0).DefaultView
            FieldList.DataBind()



            CboFilter.Visible = True
            CboFilter.DataSource = ObjDs.Tables(0).DefaultView
            CboFilter.DataTextField = "Column_name"
            CboFilter.DataTextField = "Column_name"
            CboFilter.AppendDataBoundItems = True
            CboFilter.Items.Add(New System.Web.UI.WebControls.ListItem("[FIELD LIST]", ""))
            CboFilter.DataBind()
            For Each Itemdata As String In excludefield
                Dim item As ListItem = CboFilter.Items.FindByValue("[" & Itemdata & "]")
                If Not item Is Nothing Then
                    CboFilter.Items.Remove(item)
                End If
                
            Next

        Catch
            Throw
        Finally
            If Not ObjDs Is Nothing Then
                ObjDs.Dispose()
                ObjDs = Nothing
            End If
        End Try

    End Sub

    'Protected Sub CboViewList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CboViewList.SelectedIndexChanged
    '    Try
    '        If CboViewList.SelectedValue <> "" Then
    '            Me.Tables = Nothing
    '            Me.Field = Nothing
    '            Me.Sort = Nothing
    '            Me.Whereclause = Nothing


    '            ChkSelectAll.Checked = False

    '            LoadField()
    '            Session("ReportOverBookingTunai.ObjDataFilter") = Nothing
    '            GridViewList.DataSource = Me.ObjDataFilter
    '            GridViewList.DataBind()
    '            If ObjDataFilter.Rows.Count > 0 Then
    '                PanelAndOr.Visible = True
    '            Else
    '                PanelAndOr.Visible = False
    '            End If
    '            cboOperator.Visible = False
    '            PanelAwal.Visible = False
    '            PanelAkhir.Visible = False
    '            ImageAddFilter.Visible = False
    '        Else
    '            CValPageError.IsValid = False
    '            CValPageError.ErrorMessage = "Please Select Table"
    '        End If
    '    Catch ex As Exception
    '        CValPageError.IsValid = False
    '        CValPageError.ErrorMessage = ex.Message
    '        LogError(ex)
    '    End Try
    'End Sub
    Function formatstring(ByVal strData As String) As String
        If strData.Length > 0 Then
            If Left(strData, 1) = "'" Then
                strData = Right(strData, strData.Length - 1)
            End If
            If Right(strData, 1) = "'" Then
                strData = Left(strData, strData.Length - 1)
            End If
            strData = strData.Replace("'", "''")
        End If
        Return strData
    End Function

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click

        Try
            Dim StrField As String = ""
            'FieldList.FindControl("ChkField")
            For Each items As DataListItem In FieldList.Items
                Dim ObjCheckField As CheckBox = CType(items.FindControl("ChkField"), CheckBox)
                If ObjCheckField.Checked Then
                    StrField += ObjCheckField.Text & ","
                End If
            Next
            If StrField.Length > 0 Then StrField = StrField.Substring(0, StrField.Length - 1)
            If StrField.Length = 0 Then
                CValPageError.IsValid = False
                CValPageError.ErrorMessage = "Please Check at least 1 Field to query."
                Exit Sub
            End If
            Dim strWhere As String = ""
            Dim ObjDs As Data.DataSet
            Dim objParam As Object() = {Me.ViewName}
            ObjDs = DataRepository.Provider.ExecuteDataSet("usp_GetQueryFieldByTableName", objParam)
            Dim objView As DataView = ObjDs.Tables(0).DefaultView

            Dim strType As String = ""
            For Each objrow As DataRow In ObjDataFilter.Rows
                strWhere &= " " & objrow("andor") & " (" & objrow("ColumnName")
                objView.RowFilter = " column_name='" & objrow("ColumnName") & "'"
                strType = objView.Item(0).Item(7)
                Select Case objrow("Operator").ToString.ToLower
                    Case "Equals".ToLower
                        Select Case strType.ToLower
                            Case "bigint", "int", "smallint", "tinyint", "decimal", "numeric", "money", "float"
                                strWhere &= " = " & objrow("nilai") & " )"
                            Case "datetime"
                                strWhere &= " between '" & Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", objrow("nilai")).ToString("yyyy-MM-dd") & " 00:00:00' and '" & Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", objrow("nilai")).ToString("yyyy-MM-dd") & " 23:59:59' )"
                            Case "varchar", "nvarchar", "char"
                                strWhere &= " ='" & formatstring(objrow("nilai").ToString) & "' )"
                            Case Else
                                strWhere &= " = " & objrow("nilai") & " )"
                        End Select
                    Case "Does Not Equal".ToLower
                        Select Case strType.ToLower
                            Case "bigint", "int", "smallint", "tinyint", "decimal", "numeric", "money", "float"
                                strWhere &= " <> " & objrow("nilai") & " )"
                            Case "datetime"
                                strWhere &= " not between  '" & Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", objrow("nilai")).ToString("yyyy-MM-dd") & " 00:00:00' and '" & Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", objrow("nilai")).ToString("yyyy-MM-dd") & " 23:59:59' )"
                            Case "varchar", "nvarchar", "char"
                                strWhere &= "<>'" & formatstring(objrow("nilai").ToString) & "' )"
                            Case Else
                                strWhere &= " <> " & objrow("nilai") & " )"
                        End Select
                    Case "Is Greater Than".ToLower
                        Select Case strType.ToLower
                            Case "bigint", "int", "smallint", "tinyint", "decimal", "numeric", "money", "float"
                                strWhere &= " > " & objrow("nilai") & " )"
                            Case "datetime"
                                strWhere &= " >'" & Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", objrow("nilai")).ToString("yyyy-MM-dd") & " 00:00:00'" & " )"
                            Case Else
                                strWhere &= " > " & objrow("nilai") & " )"
                        End Select
                    Case "Is Less Than".ToLower
                        Select Case strType.ToLower
                            Case "bigint", "int", "smallint", "tinyint", "decimal", "numeric", "money", "float"
                                strWhere &= " < " & objrow("nilai") & " )"
                            Case "datetime"
                                strWhere &= " <'" & Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", objrow("nilai")).ToString("yyyy-MM-dd") & " 00:00:00'" & " )"
                            Case Else
                                strWhere &= " < " & objrow("nilai") & " )"
                        End Select

                    Case "Is Equals or Greater".ToLower
                        Select Case strType.ToLower
                            Case "bigint", "int", "smallint", "tinyint", "decimal", "numeric", "money", "float"
                                strWhere &= " >= " & objrow("nilai") & " )"
                            Case "datetime"
                                strWhere &= " >= '" & Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", objrow("nilai")).ToString("yyyy-MM-dd") & " 00:00:00'" & " )"

                            Case Else
                                strWhere &= " >= " & objrow("nilai") & " )"
                        End Select
                    Case "Is Equals or Less".ToLower
                        Select Case strType.ToLower
                            Case "bigint", "int", "smallint", "tinyint", "decimal", "numeric", "money", "float"
                                strWhere &= " <= " & objrow("nilai") & " )"

                            Case "datetime"
                                strWhere &= " <= '" & Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", objrow("nilai")).ToString("yyyy-MM-dd") & " 23:59:59'" & " )"
                            Case Else
                                strWhere &= " <= " & objrow("nilai") & " )"
                        End Select
                    Case "Is Between".ToLower
                        Select Case strType.ToLower
                            Case "bigint", "int", "smallint", "tinyint", "decimal", "numeric", "money", "float"
                                strWhere &= " between " & objrow("nilai") & " )"

                            Case "datetime"

                                'Dim arrtemp() As String = objrow("nilai").ToString.ToLower.Split("and")
                                Dim indexand As Integer = objrow("nilai").ToString.ToLower.IndexOf("and")

                                Dim date1 As Date = Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", objrow("nilai").ToString.ToLower.Substring(0, indexand - 1).Trim)
                                Dim date2 As Date = Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", objrow("nilai").ToString.ToLower.Substring(indexand + 3).Trim)

                                strWhere &= " between '" & date1.ToString("yyyy-MM-dd") & " 00:00:00' and '" & date2.ToString("yyyy-MM-dd") & " 23:59:59'" & " )"
                            Case Else
                                strWhere &= " between " & objrow("nilai") & " )"
                        End Select
                    Case "Is Not Between".ToLower
                        Select Case strType.ToLower
                            Case "bigint", "int", "smallint", "tinyint", "decimal", "numeric", "money", "float"
                                strWhere &= " not between " & objrow("nilai") & " )"

                            Case "datetime"
                                Dim indexand As Integer = objrow("nilai").ToString.ToLower.IndexOf("and")

                                Dim date1 As Date = Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", objrow("nilai").ToString.ToLower.Substring(0, indexand - 1).Trim)
                                Dim date2 As Date = Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", objrow("nilai").ToString.ToLower.Substring(indexand + 3).Trim)

                                strWhere &= " not between '" & date1.ToString("yyyy-MM-dd") & " 00:00:00' and '" & date2.ToString("yyyy-MM-dd") & " 23:59:59'" & " )"
                            Case Else
                                strWhere &= " not between " & objrow("nilai") & " )"
                        End Select
                    Case "Is Null".ToLower
                        Select Case strType.ToLower
                            Case "bigint", "int", "smallint", "tinyint", "decimal", "numeric", "money", "float"
                                strWhere &= " is null " & " )"
                            Case "bit"
                                strWhere &= " is null " & " )"
                            Case "varchar", "nvarchar", "char"
                                strWhere &= " is null " & " )"
                            Case "datetime"
                                strWhere &= " is null" & " )"
                            Case Else
                                strWhere &= " is null " & " )"
                        End Select
                    Case "Is Not Null".ToLower
                        Select Case strType.ToLower
                            Case "bigint", "int", "smallint", "tinyint", "decimal", "numeric", "money", "float"
                                strWhere &= " is not null " & " )"
                            Case "bit"
                                strWhere &= " is not null " & " )"
                            Case "varchar", "nvarchar", "char"
                                strWhere &= " is not null " & " )"
                            Case "datetime"
                                strWhere &= " is not null " & " )"
                            Case Else
                                strWhere &= " is not null " & " )"
                        End Select
                    Case "Is True".ToLower
                        Select Case strType.ToLower
                            Case "bit"
                                strWhere &= " =1" & " )"
                            Case Else
                                strWhere &= " =1" & " )"
                        End Select
                    Case "Is False".ToLower
                        Select Case strType.ToLower
                            Case "bit"
                                strWhere &= " =0" & " )"
                            Case Else
                                strWhere &= " =0" & " )"
                        End Select
                    Case "Contains".ToLower
                        Select Case strType.ToLower
                            Case "varchar", "nvarchar", "char"
                                strWhere &= " like '%" & objrow("nilai") & "%'" & " )"
                            Case Else
                                strWhere &= " like '%" & objrow("nilai") & "%'" & " )"
                        End Select
                    Case "Start With".ToLower
                        Select Case strType.ToLower
                            Case "varchar", "nvarchar", "char"
                                strWhere &= " like '" & objrow("nilai") & "%'" & " )"
                            Case Else
                                strWhere &= " like '" & objrow("nilai") & "%'" & " )"
                        End Select
                    Case "Ends With".ToLower
                        Select Case strType.ToLower
                            Case "varchar", "nvarchar", "char"
                                strWhere &= " like '%" & objrow("nilai") & "'" & " )"
                            Case Else
                                strWhere &= " like '%" & objrow("nilai") & "'" & " )"
                        End Select
                End Select
            Next

            Me.CurrentPage = 0


            Dim strAllwhere As String = ""
            If strWhere.Trim.Length > 0 Then
                strAllwhere = " where useridaml='" & Sahassa.AML.Commonly.SessionUserId & "' and " & strWhere
                strWhere += " and useridaml='" & Sahassa.AML.Commonly.SessionUserId & "'"
            Else
                strAllwhere = " where useridaml='" & Sahassa.AML.Commonly.SessionUserId & "'"
                strWhere += " useridaml='" & Sahassa.AML.Commonly.SessionUserId & "'"
            End If




            Session("ReportOverBookingTunai.sqlquery") = "Select " & StrField & " from [" & Me.ViewName & "] " & strAllwhere
            BindGrid(StrField, strWhere)
            'Dim ds As DataSet = DataRepository.Provider.ExecuteDataSet(CommandType.Text, Session("ReportOverBookingTunai.sqlquery"))

        Catch ex As Exception
            CValPageError.IsValid = False
            CValPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Sub BindGrid(ByVal strField, ByVal strwhere)
        Me.Tables = "[" & Me.ViewName & "]"
        Me.Field = strField
        Me.Whereclause = strwhere

        If Me.Field.Trim.Length > 0 Then
            Dim dt As DataTable = GetDatasetPaging(Me.Tables, Me.Field, Me.Whereclause, Me.Sort, Me.CurrentPage, Sahassa.AML.Commonly.GetDisplayedTotalRow, Me.RowTotal)

            GridViewQuery.DataSource = dt.DefaultView
            GridViewQuery.PageIndex = Me.CurrentPage
            GridViewQuery.DataBind()
            If GridViewQuery.Rows.Count > 0 Then
                PanelNavigasi.Visible = True
            Else
                PanelNavigasi.Visible = False
            End If

            WebInfoPageNavigate()
        End If


    End Sub

    Protected Sub ImageAddFilter_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAddFilter.Click
        Try
            If CboFilter.SelectedValue = "" Then
                Exit Sub
            End If
            Dim ObjRow As DataRow = ObjDataFilter.NewRow
            If ObjDataFilter.Rows.Count > 0 Then
                ObjRow("AndOr") = CboAndOR.SelectedValue
            Else
                ObjRow("AndOr") = ""
            End If
            ObjRow("ColumnName") = CboFilter.SelectedValue
            ObjRow("Operator") = cboOperator.SelectedValue
            Select Case cboOperator.SelectedValue.ToLower
                Case "Equals".ToLower, "does not equal".ToLower, "Is Greater Than".ToLower, "Is Less Than".ToLower, "Is Equals or Greater".ToLower, "Is Equals or Less".ToLower, "Contains".ToLower, "Start With".ToLower, "Ends With".ToLower
                    If SelectedDataRow.Item(7).ToString.ToLower.Contains("datetime") Then
                        If Sahassa.AML.Commonly.IsDateValid("dd-MM-yyyy", TxtFieldAwal.Text.Trim) Then
                            ObjRow("nilai") = TxtFieldAwal.Text.Trim
                        Else
                            CValPageError.IsValid = False
                            CValPageError.ErrorMessage = "Date is not in valid format."
                            Exit Sub
                        End If

                    Else
                        ObjRow("nilai") = TxtFieldAwal.Text.Trim

                    End If
                Case "Is Between".ToLower, "Is Not Between".ToLower
                    If SelectedDataRow.Item(7).ToString.ToLower.Contains("datetime") Then
                        If Not Sahassa.AML.Commonly.IsDateValid("dd-MM-yyyy", TxtFieldAwal.Text.Trim) Then
                            CValPageError.IsValid = False
                            CValPageError.ErrorMessage = "Begin Date is not in valid format."
                            Exit Sub
                        End If

                        If Not Sahassa.AML.Commonly.IsDateValid("dd-MM-yyyy", TxtFieldAkhir.Text.Trim) Then
                            CValPageError.IsValid = False
                            CValPageError.ErrorMessage = "End Date is not in valid format."
                            Exit Sub
                        End If
                        ObjRow("nilai") = TxtFieldAwal.Text.Trim & " and " & TxtFieldAkhir.Text.Trim

                    Else
                        ObjRow("nilai") = TxtFieldAwal.Text.Trim & " and " & TxtFieldAkhir.Text.Trim

                    End If
                Case "Is Null".ToLower, "Is Not Null".ToLower, "Is True".ToLower
                    ObjRow("nilai") = ""
            End Select

            ObjDataFilter.Rows.Add(ObjRow)
            If ObjDataFilter.Rows.Count > 0 Then
                PanelAndOr.Visible = True
            Else
                PanelAndOr.Visible = False
            End If
            GridViewList.DataSource = ObjDataFilter
            GridViewList.DataBind()
        Catch ex As Exception
            CValPageError.IsValid = False
            CValPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub CboFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CboFilter.SelectedIndexChanged
        Dim objds As Data.DataSet
        Try
            Dim objParam As Object() = {Me.ViewName}
            objds = DataRepository.Provider.ExecuteDataSet("usp_GetQueryFieldByTableName", objParam)
            Dim ObjView As Data.DataView = objds.Tables(0).DefaultView
            ObjView.RowFilter = "Column_Name='" & CboFilter.SelectedValue & "'"

            If ObjView.Count > 0 Then
                Session("ReportOverBookingTunai.SelectedDataRow") = ObjView.Item(0)
                cboOperator.Visible = True
                Dim strDataType As String = ObjView.Item(0).Item(7)

                Select Case strDataType.ToLower
                    Case "bigint", "int", "smallint", "tinyint", "decimal", "numeric", "money", "float"
                        cboOperator.Items.Clear()
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("[OPERATOR]", "[OPERATOR]"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Equals", "Equals"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Does Not Equal", "Does Not Equal"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is Greater Than", "Is Greater Than"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is Less Than", "Is Less Than"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is Equals or Greater", "Is Equals or Greater"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is Equals or Less", "Is Equals or Less"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is Between", "Is Between"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is Not Between", "Is Not Between"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is Null", "Is Null"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is Not Null", "Is Not Null"))


                    Case "bit"
                        cboOperator.Items.Clear()
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("[OPERATOR]", "[OPERATOR]"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is True", "Is True"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is False", "Is False"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is Null", "Is Null"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is Not Null", "Is Not Null"))

                    Case "datetime"
                        cboOperator.Items.Clear()
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("[OPERATOR]", "[OPERATOR]"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Equals", "Equals"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Does Not Equal", "Does Not Equal"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is Between", "Is Between"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is Not Between", "Is Not Between"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is Equals or Greater", "Is Equals or Greater"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is Equals or Less", "Is Equals or Less"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is Greater Than", "Is Greater Than"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is Less Than", "Is Less Than"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is Null", "Is Null"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is Not Null", "Is Not Null"))

                    Case "varchar", "nvarchar", "char"
                        cboOperator.Items.Clear()
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("[OPERATOR]", "[OPERATOR]"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Equals", "Equals"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Does Not Equal", "Does Not Equal"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Contains", "Contains"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Start With", "Start With"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Ends With", "Ends With"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is Null", "Is Null"))
                        cboOperator.Items.Add(New System.Web.UI.WebControls.ListItem("Is Not Null", "Is Not Null"))

                    Case Else
                        cboOperator.Items.Clear()
                        cboOperator.Items.Add(New ListItem("[OPERATOR]", "[OPERATOR]"))
                        cboOperator.Items.Add(New ListItem("Equals", "Equals"))
                        cboOperator.Items.Add(New ListItem("Does Not Equal", "Does Not Equal"))
                        cboOperator.Items.Add(New ListItem("Contains", "Contains"))
                        cboOperator.Items.Add(New ListItem("Start With", "Start With"))
                        cboOperator.Items.Add(New ListItem("Ends With", "Ends With"))
                        cboOperator.Items.Add(New ListItem("Is Null", "Is Null"))
                        cboOperator.Items.Add(New ListItem("Is Not Null", "Is Not Null"))

                End Select

            End If
            cboOperator_SelectedIndexChanged(Nothing, Nothing)

        Catch ex As Exception

            CValPageError.IsValid = False
            CValPageError.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not objds Is Nothing Then
                objds.Dispose()
                objds = Nothing
            End If

        End Try
    End Sub

    Protected Sub cboOperator_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboOperator.SelectedIndexChanged

        Try
            If Not SelectedDataRow Is Nothing Then

                Dim strDataType As String = SelectedDataRow.Item(7)
                If Not strDataType.ToLower.Contains("datetime") Then
                    popUp1.Visible = False
                    popUp2.Visible = False
                Else
                    popUp1.Visible = True
                    popUp2.Visible = True
                End If

                TxtFieldAwal.Text = ""
                TxtFieldAkhir.Text = ""
                Select Case cboOperator.SelectedValue.ToLower
                    Case "[OPERATOR]".ToLower
                        PanelAwal.Visible = False
                        PanelAnd.Visible = False
                        PanelAkhir.Visible = False
                        ImageAddFilter.Visible = False
                    Case "Equals".ToLower, "does not equal".ToLower, "Is Greater Than".ToLower, "Is Less Than".ToLower, "Is Equals or Greater".ToLower, "Is Equals or Less".ToLower, "Contains".ToLower, "Start With".ToLower, "Ends With".ToLower
                        PanelAwal.Visible = True
                        PanelAnd.Visible = False
                        PanelAkhir.Visible = False
                        If strDataType.ToLower.Contains("datetime") Then
                            popUp1.Visible = True
                            popUp2.Visible = False
                        End If
                        ImageAddFilter.Visible = True
                    Case "Is Between".ToLower, "Is Not Between".ToLower
                        PanelAwal.Visible = True
                        PanelAnd.Visible = True
                        PanelAkhir.Visible = True
                        If strDataType.ToLower.Contains("datetime") Then
                            popUp1.Visible = True
                            popUp2.Visible = True
                        End If
                        ImageAddFilter.Visible = True
                    Case "Is Null".ToLower, "Is Not Null".ToLower, "Is True".ToLower
                        PanelAwal.Visible = False
                        PanelAnd.Visible = False
                        PanelAkhir.Visible = False
                        ImageAddFilter.Visible = True
                End Select
            End If
        Catch ex As Exception
            CValPageError.IsValid = False
            CValPageError.ErrorMessage = ex.Message
            LogError(ex)
        Finally

        End Try
    End Sub

    Protected Sub GridViewList_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GridViewList.RowDeleting
        Try
            Dim Pk As Integer = GridViewList.DataKeys(e.RowIndex).Value.ToString

            Dim ObjRow As DataRow = ObjDataFilter.Rows.Find(Pk)
            ObjDataFilter.Rows.Remove(ObjRow)
            If ObjDataFilter.Rows.Count = 0 Then
                PanelAndOr.Visible = False
            ElseIf ObjDataFilter.Rows.Count > 0 Then
                ObjRow = ObjDataFilter.Rows(0)
                ObjRow("AndOr") = ""
            End If
            GridViewList.DataSource = ObjDataFilter
            GridViewList.DataBind()

        Catch ex As Exception
            CValPageError.IsValid = False
            CValPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridViewQuery_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridViewQuery.RowCreated
        Try
            'If e.Row.RowType = DataControlRowType.Header Or e.Row.RowType = DataControlRowType.DataRow Then
            If e.Row.Cells.Count >= 14 Then
                e.Row.Cells(14).Visible = False


            End If

            'End If
        Catch ex As Exception
            CValPageError.IsValid = False
            CValPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

   

    Protected Sub GridViewQuery_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles GridViewQuery.Sorting
        Try



            Dim strsort As String
            If e.SortDirection = SortDirection.Ascending Then
                strsort = "  asc"
            Else
                strsort = " desc"
            End If
            If Me.Sort.ToLower.Length > 5 Then
                If Left(Me.Sort.ToLower, Me.Sort.Length - 5).Trim = "[" & e.SortExpression.ToLower & "]" Then
                    Me.Sort = Sahassa.AML.Commonly.ChangeSortCommand(Me.Sort)
                Else
                    Me.Sort = "[" & e.SortExpression & "]" & strsort
                End If
            Else
                Me.Sort = "[" & e.SortExpression & "]" & strsort
            End If





            ' Dim ds As DataSet = DataRepository.Provider.ExecuteDataSet(CommandType.Text, Session("ReportOverBookingTunai.sqlquery") & Me.Sort)
            BindGrid(Me.Field, Me.Whereclause)

        Catch ex As Exception
            CValPageError.IsValid = False
            CValPageError.ErrorMessage = ex.Message
            LogError(ex)
        Finally
        End Try

    End Sub

    Protected Sub ChkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ChkSelectAll.CheckedChanged
        Try
            If ChkSelectAll.Checked Then
                For Each items As DataListItem In FieldList.Items
                    Dim ObjCheckField As CheckBox = CType(items.FindControl("ChkField"), CheckBox)
                    ObjCheckField.Checked = True
                Next
            Else
                For Each items As DataListItem In FieldList.Items
                    Dim ObjCheckField As CheckBox = CType(items.FindControl("ChkField"), CheckBox)
                    ObjCheckField.Checked = False
                Next
            End If
        Catch ex As Exception
            CValPageError.IsValid = False
            CValPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try

    End Sub

    Sub ExportExcel()
        Dim strsql As String
        If Me.Sort.Trim.Length > 0 Then
            strsql = Session("ReportOverBookingTunai.sqlquery") & " order by " & Me.Sort
        Else
            strsql = Session("ReportOverBookingTunai.sqlquery")
        End If


        Dim ds As DataSet = DataRepository.Provider.ExecuteDataSet(CommandType.Text, strsql)
        GridViewQuery.DataSource = ds.Tables(0).DefaultView
        Me.GridViewQuery.AllowPaging = False
        Me.GridViewQuery.AllowSorting = False
        GridViewQuery.DataBind()
        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            For y As Integer = 0 To GridViewQuery.Columns.Count - 1
                GridViewQuery.Rows(i).Cells(y).Attributes.Add("class", "text")
            Next
        Next


        ds.Dispose()
        ds = Nothing




        Dim str_Style As String = "<style>.text{ mso-number-format:\@; }</style>"
        Response.Clear()

        Response.AddHeader("content-disposition", "attachment;filename=QueryBuilderdinamic.xls")
        Response.Charset = ""
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/vnd.xls"

        Me.EnableViewState = False

        Dim stringWriter_Temp As System.IO.StringWriter = New System.IO.StringWriter
        Dim htmlTextWriter_Temp As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWriter_Temp)

        '  Me.ClearControl(Me.GridViewQuery)

        Me.GridViewQuery.RenderControl(htmlTextWriter_Temp)
        Response.Write(str_Style)
        Response.Write(stringWriter_Temp.ToString)
        Response.End()
    End Sub


    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

        ' Verifies that the control is rendered 

    End Sub

    Private Sub ClearControl(ByVal control_C As Control)
        For i As Integer = control_C.Controls.Count - 1 To 0 Step -1
            ClearControl(control_C.Controls(i))
        Next i

        If Not TypeOf control_C Is TableCell Then
            If Not (control_C.GetType.GetProperty("SelectedItem") Is Nothing) Then
                Dim literalControl_L As New LiteralControl
                control_C.Parent.Controls.Add(literalControl_L)
                literalControl_L.Text = CStr(control_C.GetType.GetProperty("SelectedItem").GetValue(control_C, Nothing))
                control_C.Parent.Controls.Remove(control_C)
            Else 'Property("SelectedItem")
                If Not (control_C.GetType.GetProperty("Text") Is Nothing) Then
                    Dim literalControl_L As New LiteralControl
                    control_C.Parent.Controls.Add(literalControl_L)
                    literalControl_L.Text = CStr(control_C.GetType.GetProperty("Text").GetValue(control_C, Nothing))
                    control_C.Parent.Controls.Remove(control_C)
                End If
            End If 'Property("Text")
        End If
    End Sub


    Protected Sub GridViewQuery_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridViewQuery.PageIndexChanging
        Try
            GridViewQuery.PageIndex = e.NewPageIndex
            BindGrid(Me.Field, Me.Whereclause)
            '     Dim ds As DataSet = DataRepository.Provider.ExecuteDataSet(CommandType.Text, Session("ReportOverBookingTunai.sqlquery") & Me.Sort)


        Catch ex As Exception
            CValPageError.IsValid = False
            CValPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Protected Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imageButton_Go.Click
        Try            
            If IsNumeric(Me.textBox_GoToPage.Text) Then
                If (CInt(Me.textBox_GoToPage.Text) > 0) Then
                    If (CInt(Me.textBox_GoToPage.Text) <= CInt(Me.label_TotalPages.Text)) Then
                        Me.CurrentPage = Integer.Parse(Me.textBox_GoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less or equal to the total page count.")
                    End If 'Me.textBox_GoToPage.Text <= Me.label_TotalPages.Text

                Else
                    Throw New Exception("Page number must be a positive number greater than zero.")
                End If 'textBox_GoToPage > 0

            Else
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If 'IsNumeric(Me.textBox_GoToPage.Text)
            BindGrid(Me.Field, Me.Whereclause)
        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Public Shared Function ReaderToTable(ByVal reader As SqlDataReader) As DataTable

        Dim newTable As New DataTable()
        Dim col As DataColumn
        Dim row As DataRow
        Dim i As Integer

        For i = 0 To reader.FieldCount - 1

            col = New DataColumn()
            col.ColumnName = reader.GetName(i)
            col.DataType = reader.GetFieldType(i)

            newTable.Columns.Add(col)
        Next

        While reader.Read

            row = newTable.NewRow()
            For i = 0 To reader.FieldCount - 1
                row(i) = reader.Item(i)
            Next

            newTable.Rows.Add(row)
        End While

        Return newTable
    End Function


    Public Shared Function GetDatasetPaging(ByVal Tables As String, ByVal Field As String, ByVal strWhereClause As String, ByVal Sort As String, ByVal PageIndex As Int64, ByVal PageSize As Int64, ByRef intcount As Integer) As DataTable
        Dim SqlConn As SqlConnection = Nothing
        Dim SqlDa As SqlDataAdapter = Nothing
        Dim Ds As DataSet = Nothing
        Dim reader As Data.SqlClient.SqlDataReader
        Try
            Dim StrConn As String = System.Configuration.ConfigurationManager.ConnectionStrings("netTiersConnectionString").ToString
            Ds = New DataSet
            SqlConn = New SqlConnection(StrConn)
            SqlConn.Open()
            Dim SqlCmd As New SqlCommand()
            SqlCmd.CommandTimeout = System.Configuration.ConfigurationManager.AppSettings("SQLCommandTimeout").ToString
            SqlCmd.CommandType = CommandType.StoredProcedure
            SqlCmd.CommandText = "usp_newCustomPaging"
            SqlCmd.Connection = SqlConn

            SqlCmd.Parameters.Add("@Tables", SqlDbType.VarChar, Integer.MaxValue)
            SqlCmd.Parameters("@Tables").Value = Tables

            SqlCmd.Parameters.Add("@fields", SqlDbType.VarChar, Integer.MaxValue)
            SqlCmd.Parameters("@fields").Value = Field

            SqlCmd.Parameters.Add("@WhereClause", SqlDbType.VarChar, 8000)
            SqlCmd.Parameters("@WhereClause").Value = strWhereClause


            SqlCmd.Parameters.Add("@OrderBy", SqlDbType.VarChar, 2000)
            SqlCmd.Parameters("@OrderBy").Value = Sort

            SqlCmd.Parameters.Add("@Pageindex", SqlDbType.Int)
            SqlCmd.Parameters("@Pageindex").Value = PageIndex

            SqlCmd.Parameters.Add("@PageSize", SqlDbType.Int)
            SqlCmd.Parameters("@PageSize").Value = PageSize

            reader = SqlCmd.ExecuteReader
            Dim objDT As DataTable = ReaderToTable(reader)
            If reader.NextResult Then
                reader.Read()
                intcount = reader(0)
            End If
            ' SqlDa = New SqlDataAdapter(SqlCmd)
            'SqlDa.Fill(Ds)

            Return objDT
        Catch tex As Threading.ThreadAbortException
            Throw tex
        Catch ex As Exception
            Throw ex
        Finally
            If Not SqlConn Is Nothing Then
                SqlConn.Close()
                SqlConn.Dispose()
                SqlConn = Nothing
            End If
            If Not SqlDa Is Nothing Then
                SqlDa.Dispose()
                SqlDa = Nothing
            End If
            If Not Ds Is Nothing Then
                Ds.Dispose()
                Ds = Nothing
            End If
        End Try
    End Function

    Protected Sub GridViewQuery_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridViewQuery.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then




                CType(e.Row.Cells(0).FindControl("lblno"), Label).Text = e.Row.RowIndex + 1
                Dim objDataRowView As DataRowView = CType(e.Row.DataItem, DataRowView)
                For i As Integer = 0 To objDataRowView.DataView.Table.Columns.Count - 1

                    Dim objcol As DataColumn = objDataRowView.DataView.Table.Columns(i)
                    If objcol.DataType.Name.ToLower = "datetime" Then
                        If Not (e.Row.Cells(i + 1).Text = "&nbsp;" Or e.Row.Cells(i + 1).Text = "") Then
                            e.Row.Cells(i + 1).Text = CType(e.Row.Cells(i + 1).Text, Date).ToString("dd-MM-yyyy")
                            e.Row.Cells(i + 1).Attributes.Add("class", "text")
                        End If
                    ElseIf objcol.DataType.Name.ToLower = "boolean" Then
                        For Each ocontrol As Control In e.Row.Cells(i + 1).Controls
                            If ocontrol.GetType.Name.ToLower = "checkbox" Then
                                ocontrol.Visible = False
                                e.Row.Cells(i + 1).Text = CType(ocontrol, CheckBox).Checked.ToString
                                e.Row.Cells(i + 1).Attributes.Add("class", "text")
                            End If
                        Next
                    ElseIf objcol.DataType.Name.ToLower = "string" Then
                        e.Row.Cells(i + 1).Text = Server.HtmlDecode(e.Row.Cells(i + 1).Text)
                    ElseIf objcol.DataType.Name.ToLower = "decimal" Then
                        e.Row.HorizontalAlign = HorizontalAlign.Right
                        Try
                            '    e.Row.Cells(i + 1).Text = FormatNumber(e.Row.Cells(i + 1).Text)
                        Catch ex As Exception

                        End Try
                    End If

                Next







            End If
        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub CboPageSize_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CboPageSize.SelectedIndexChanged
        Try
            Sahassa.AML.Commonly.SessionPagingLimit = CboPageSize.SelectedValue
            Me.CurrentPage = 0
            BindGrid(Me.Field, Me.Whereclause)
        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Sub ExportWord()
        Try

            Dim strsql As String
            If Me.Sort.Trim.Length > 0 Then
                strsql = Session("ReportOverBookingTunai.sqlquery") & " order by " & Me.Sort
            Else
                strsql = Session("ReportOverBookingTunai.sqlquery")
            End If


            Dim ds As DataSet = DataRepository.Provider.ExecuteDataSet(CommandType.Text, strsql)
            GridViewQuery.DataSource = ds.Tables(0).DefaultView
            Me.GridViewQuery.AllowPaging = False
            Me.GridViewQuery.AllowSorting = False
            GridViewQuery.DataBind()
            ds.Dispose()
            ds = Nothing




            Dim str_Style As String = "<style>.text{ mso-number-format:\@; }</style>"
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = System.Text.Encoding.UTF7.ToString
            Response.AddHeader("content-disposition", "attachment;filename=QueryBuilderdinamic.doc")

            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.word"

            Me.EnableViewState = False

            Dim stringWriter_Temp As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlTextWriter_Temp As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWriter_Temp)

            '  Me.ClearControl(Me.GridViewQuery)

            Me.GridViewQuery.RenderControl(htmlTextWriter_Temp)
            Response.Write(str_Style)
            Response.Write(stringWriter_Temp.ToString)
            Response.End()


        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try

    End Sub
    Sub ExportCSV()
        Try

            Dim strsql As String
            If Me.Sort.Trim.Length > 0 Then
                strsql = Session("ReportOverBookingTunai.sqlquery") & " order by " & Me.Sort
            Else
                strsql = Session("ReportOverBookingTunai.sqlquery")
            End If


            Dim ds As DataSet = DataRepository.Provider.ExecuteDataSet(CommandType.Text, strsql)
            GridViewQuery.DataSource = ds.Tables(0).DefaultView
            Me.GridViewQuery.AllowPaging = False
            Me.GridViewQuery.AllowSorting = False
            GridViewQuery.DataBind()



            Dim tempfile As String = System.IO.Path.GetTempFileName
            Dim tempfileexcel As String = IO.Path.ChangeExtension(tempfile, ".cvs")
            Dim stringWriter_Temp = New IO.StreamWriter(tempfileexcel)




            For k As Integer = 0 To ds.Tables(0).Columns.Count - 1

                'add separator 

                stringWriter_Temp.write(ds.Tables(0).Columns(k).ColumnName + ","c)

            Next

            'append new line 

            stringWriter_Temp.Write(vbCr & vbLf)

            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1

                For k As Integer = 0 To ds.Tables(0).Columns.Count - 1

                    'add separator 

                    stringWriter_Temp.Write(ds.Tables(0).Rows(i).Item(k).ToString + ","c)

                Next

                'append new line 

                stringWriter_Temp.Write(vbCr & vbLf)

            Next
            stringWriter_Temp.close()

            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=QuerybuilderDinamic.csv")
            Response.Charset = ""
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Me.EnableViewState = False
            'Response.ContentType = "application/ms-excel"
            Response.ContentType = "text/csv"


            Response.BinaryWrite(System.IO.File.ReadAllBytes(tempfileexcel))

            Response.End()

            ds.Dispose()
            ds = Nothing
        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try

    End Sub

    Sub ExportPDf()

        Try


            Dim strsql As String
            If Me.Sort.Trim.Length > 0 Then
                strsql = Session("ReportOverBookingTunai.sqlquery") & " order by " & Me.Sort
            Else
                strsql = Session("ReportOverBookingTunai.sqlquery")
            End If


            Dim ds As DataSet = DataRepository.Provider.ExecuteDataSet(CommandType.Text, strsql)
            GridViewQuery.DataSource = ds.Tables(0).DefaultView
            Me.GridViewQuery.AllowPaging = False
            Me.GridViewQuery.AllowSorting = False
            GridViewQuery.DataBind()
            ds.Dispose()
            ds = Nothing

            If Not GridViewQuery.HeaderRow Is Nothing Then
                GridViewQuery.HeaderRow.ForeColor = Drawing.Color.Black
            End If



            'Dim str_Style As String = "<style>.text{ mso-number-format:\@; }</style>"
            Response.Clear()


            Response.AddHeader("content-disposition", "attachment;filename=QueryBuilderdinamic.pdf")

            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/pdf"

            Me.EnableViewState = False

            Dim stringWriter_Temp As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlTextWriter_Temp As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWriter_Temp)
            Me.GridViewQuery.RenderControl(htmlTextWriter_Temp)

            Dim pdfDoc As New text.Document(text.PageSize.A4.Rotate, 10.0F, 10.0F, 10.0F, 0.0F)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            'Dim sr As New IO.StringReader(stringWriter_Temp.ToString())
            Dim sr As New IO.StringReader(stringWriter_Temp.ToString().Replace("font-size: medium", "font-size: 14px").Replace("font-size: smaller", "font-size: 5px").Replace("font-size: larger", "font-size: 12px").Replace("font-size: xx-small", "font-size: 5px").Replace("font-size: xx-small", "font-size: 5px").Replace("font-size: x-small", "font-size: 7px").Replace("font-size: small", "font-size: 10px").Replace("font-size: large", "font-size: 16px").Replace("font-size: x-large", "font-size: 26px").Replace("font-size: xx-large", "font-size: 40px"))
            htmlparser.Parse(sr)

            pdfDoc.Close()

            Response.Write(pdfDoc)

            '  Me.ClearControl(Me.GridViewQuery)


            'Response.Write(stringWriter_Temp.ToString)
            Response.End()


        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try

    End Sub


    Protected Sub ImageExportWord_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageExportWord.Click

        Try

            If CboType.SelectedValue = 0 Then
                ExportExcel()
            ElseIf CboType.SelectedValue = 1 Then
                ExportWord()
            ElseIf CboType.SelectedValue = 2 Then
                ExportPDf()
            ElseIf CboType.SelectedValue = 3 Then
                ExportCSV()
            End If

        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try

    End Sub

    Function IsDataValid() As Boolean
     

    End Function

    Function IsSearchingValid() As Boolean
        'todo:   cek tanggal harus di isi
        'todo:  cek tanggal valid ngak
        'todo:  cek norek harus di isi

        Dim strErrorMessage As New StringBuilder

     

        If Not Sahassa.AML.Commonly.IsDateValid("dd-MM-yyyy", TxtStartDate.Text.Trim) Then
            strErrorMessage.Append("Transaction Date Start Must dd-MM-yyyy  </br>")
        End If

        If Not Sahassa.AML.Commonly.IsDateValid("dd-MM-yyyy", TxtStartDate.Text.Trim) Then
            strErrorMessage.Append("Transaction Date End Must dd-MM-yyyy  </br>")
        End If
        If Sahassa.AML.Commonly.IsDateValid("dd-MM-yyyy", TxtStartDate.Text.Trim) AndAlso Sahassa.AML.Commonly.IsDateValid("dd-MM-yyyy", TxtStartDate.Text.Trim) Then
            If DateDiff(DateInterval.Day, Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", TxtStartDate.Text.Trim), Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", TxtEndDate.Text.Trim)) < 0 Then
                strErrorMessage.Append("Transaction Date End  Must greater than Transaction Date End  </br>")
            End If
        End If

        If TxtAccountNo.Text.Trim = "" Then
            strErrorMessage.Append("Please Fill Account No </BR>")
        End If
        If strErrorMessage.ToString.Trim.Length > 0 Then
            Throw New Exception(strErrorMessage.ToString)
        Else
            Return True
        End If
    End Function

    Sub FilterReport()
        'todo:generate table report
        Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommandStoreProcedure("usp_GenerateReportOverBookingTunai")
            objCommand.Parameters.Add(New SqlParameter("@UserIDAML", Sahassa.AML.Commonly.SessionUserId))
            objCommand.Parameters.Add(New SqlParameter("@StartTanggalTransaksi", Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", TxtStartDate.Text.Trim).ToString("yyyy-MM-dd")))
            objCommand.Parameters.Add(New SqlParameter("@EndTanggalTransaksi", Sahassa.AML.Commonly.ConvertToDate("dd-MM-yyyy", TxtEndDate.Text.Trim).ToString("yyyy-MM-dd")))
            objCommand.Parameters.Add(New SqlParameter("@AccountNo", TxtAccountNo.Text.Trim))
            DataRepository.Provider.ExecuteNonQuery(objCommand)
        End Using

    End Sub

    Sub showTampilanFilter()
        'show tampilan
        PanelData.Visible = True
    End Sub

    Protected Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Try
            If IsSearchingValid Then
                FilterReport()
                showTampilanFilter()
            End If
        Catch ex As Exception
            Me.CValPageError.IsValid = False
            Me.CValPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub FieldList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles FieldList.ItemDataBound
        Try

            Dim ObjCheckField As CheckBox = e.Item.FindControl("ChkField")



            For Each Itemdata As String In excludefield
                If "[" & Itemdata.ToLower & "]" = ObjCheckField.Text.ToLower Then
                    ObjCheckField.Visible = False
                End If
            Next


        Catch ex As Exception

        End Try
    End Sub
End Class


