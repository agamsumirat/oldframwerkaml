<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="UserWorkingUnitAssignmentApprovalDetail.aspx.vb" Inherits="UserWorkingUnitAssignmentApprovalDetail" title="User Working Unit Assignment Approval Detail" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cpContent" Runat="Server">
	<table cellSpacing="4" cellPadding="0" width="100%">
		<tr>
			<td vAlign="bottom" align="left"><IMG height="15" src="images/dot_title.gif" width="15"></td>
			<td class="maintitle" vAlign="bottom" width="99%"><asp:label id="LabelTitle" runat="server"></asp:label></td>
		</tr>
	</table>
    <TABLE id="TableDetail" borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%"
	    bgColor="#dddddd" border="2" >
	    
	    <TR class="formText" >
		    <TD bgColor="#ffffff" style="height: 23px; width: 2%;">&nbsp;</TD>
            <td bgcolor="#ffffff" style="width: 8%; height: 23px">
                User ID &nbsp;</td>
            <td bgcolor="#ffffff" style="width: 1%; height: 23px">
                :</td>
		    <TD bgColor="#ffffff" style="height: 23px;" colspan="3">
                &nbsp;<asp:label id="LabelUserID" runat="server"></asp:label></TD>
	    </TR>
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 2%; height: 23px">
            </td>
            <td bgcolor="#ffffff" style="width: 5%; height: 23px">
                Created By</td>
            <td bgcolor="#ffffff" style="width: 1px; height: 23px">
                :</td>
            <td bgcolor="#ffffff" colspan="3" style="height: 23px">
                &nbsp;<asp:Label ID="LabelCreatedBy" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 2%; height: 23px">
            </td>
            <td bgcolor="#ffffff" style="width: 5%; height: 23px">
                Entry Date</td>
            <td bgcolor="#ffffff" style="width: 1px; height: 23px">
                :</td>
            <td bgcolor="#ffffff" colspan="3" style="height: 23px">
                &nbsp;<asp:Label ID="LabelEntryDate" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText" style="background-color:#ffffcc">
            <td bgcolor="#ffffcc" height="24" style="width: 10px; ">
            </td>
            <td bgcolor="#ffffcc" colspan="4" height="24"  width="30%">
                <strong >
                Old Values</strong></td>
           
            <td bgcolor="#ffffcc" valign="middle" >
                <strong >
                New Values</strong></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" height="24" style="width: 22px">
            </td>
            <td bgcolor="#ffffff" colspan="4" height="24">
                <asp:ListBox ID="ListOldValue" runat="server" SelectionMode="Multiple" Width="272px" Height="100px" CssClass="combobox">
                </asp:ListBox></td>
          
            <td bgcolor="#ffffff"  valign="top">
                <asp:ListBox ID="ListNewValue" runat="server" SelectionMode="Multiple" Width="272px" Height="100px" CssClass="combobox">
                </asp:ListBox></td>
        </tr>
	    <TR class="formText"  bgColor="#dddddd" height="30">
		    <TD style="width: 10px"><IMG height="15" src="images/arrow.gif" width="15"></TD>
            <td colspan="9">
			    <TABLE cellSpacing="0" cellPadding="3" border="0">
				    <TR>
					    <TD><asp:imagebutton id="ImageAccept" runat="server" CausesValidation="False" SkinID="AcceptButton"></asp:imagebutton></TD>
					    <TD><asp:imagebutton id="ImageReject" runat="server" CausesValidation="False" SkinID="RejectButton"></asp:imagebutton></TD>
					    <td><asp:imagebutton id="ImageBack" runat="server" CausesValidation="False" SkinID="BackButton"></asp:imagebutton></td>
				    </TR>
			    </TABLE>
                <asp:CustomValidator ID="cvalPageErr" runat="server" Display="Dynamic" Visible="False"></asp:CustomValidator></td>
	    </TR>
    </TABLE>
</asp:Content>