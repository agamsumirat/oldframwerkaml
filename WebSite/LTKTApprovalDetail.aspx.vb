#Region "Imports..."

Imports Sahassanettier.Data
Imports Sahassanettier.Entities
Imports System.Data
Imports System.Collections.Generic
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports AMLBLL.DataType
#End Region



Partial Class LTKTApprovalDetail
    Inherits Parent

#Region "properties..."

    ReadOnly Property getAPPROVALPK() As Integer
        Get
            If Session("LTKTApprovalDetail.APPROVALPK") = Nothing Then
                Session("LTKTApprovalDetail.APPROVALPK") = CInt(Request.Params("Approval_ID"))
            End If
            Return Session("LTKTApprovalDetail.APPROVALPK")
        End Get
    End Property

    Public Property SetnGetResumeKasMasukKasKeluar() As List(Of ResumeKasMasukKeluarLTKT)
        Get
            If Session("LTKTApprovalDetail.ResumeKasMasukKasKeluar") Is Nothing Then
                Session("LTKTApprovalDetail.ResumeKasMasukKasKeluar") = New List(Of ResumeKasMasukKeluarLTKT)
            End If
            Return Session("LTKTApprovalDetail.ResumeKasMasukKasKeluar")
        End Get
        Set(ByVal value As List(Of ResumeKasMasukKeluarLTKT))
            Session("LTKTApprovalDetail.ResumeKasMasukKasKeluar") = value
        End Set
    End Property

    Public Property SetnGetResumeKasMasukKasKeluarOLD() As List(Of ResumeKasMasukKeluarLTKT)
        Get
            If Session("LTKTApprovalDetail.ResumeKasMasukKasKeluarOLD") Is Nothing Then
                Session("LTKTApprovalDetail.ResumeKasMasukKasKeluarOLD") = New List(Of ResumeKasMasukKeluarLTKT)
            End If
            Return Session("LTKTApprovalDetail.ResumeKasMasukKasKeluarOLD")
        End Get
        Set(ByVal value As List(Of ResumeKasMasukKeluarLTKT))
            Session("LTKTApprovalDetail.ResumeKasMasukKasKeluarOLD") = value
        End Set
    End Property

    Public Property SetnGetgrvTRXKMDetilValutaAsing() As List(Of LTKTDetailCashInApprovalDetail)
        Get
            If Session("LTKTApprovalDetail.grvTRXKMDetilValutaAsingDATA") Is Nothing Then
                Session("LTKTApprovalDetail.grvTRXKMDetilValutaAsingDATA") = New List(Of LTKTDetailCashInApprovalDetail)
            End If
            Return Session("LTKTApprovalDetail.grvTRXKMDetilValutaAsingDATA")
        End Get
        Set(ByVal value As List(Of LTKTDetailCashInApprovalDetail))
            Session("LTKTApprovalDetail.grvTRXKMDetilValutaAsing") = value
        End Set
    End Property

    Public Property SetnGetgrvTRXKMDetilValutaAsingOLD() As List(Of LTKTDetailCashInApprovalDetail)
        Get
            If Session("LTKTApprovalDetail.grvTRXKMDetilValutaAsingDATAOLD") Is Nothing Then
                Session("LTKTApprovalDetail.grvTRXKMDetilValutaAsingDATAOLD") = New List(Of LTKTDetailCashInApprovalDetail)
            End If
            Return Session("LTKTApprovalDetail.grvTRXKMDetilValutaAsingDATAOLD")
        End Get
        Set(ByVal value As List(Of LTKTDetailCashInApprovalDetail))
            Session("LTKTApprovalDetail.grvTRXKMDetilValutaAsingDATAOLD") = value
        End Set
    End Property

    Public Property SetnGetgrvDetilKasKeluar() As List(Of LTKTDetailCashInApprovalDetail)
        Get
            If Session("LTKTApprovalDetail.grvDetilKasKeluarDATA") Is Nothing Then
                Session("LTKTApprovalDetail.grvDetilKasKeluarDATA") = New List(Of LTKTDetailCashInApprovalDetail)
            End If
            Return Session("LTKTApprovalDetail.grvDetilKasKeluarDATA")
        End Get
        Set(ByVal value As List(Of LTKTDetailCashInApprovalDetail))
            Session("LTKTApprovalDetail.grvDetilKasKeluarDATA") = value
        End Set
    End Property

    Public Property SetnGetgrvDetilKasKeluarOLD() As List(Of LTKTDetailCashInApprovalDetail)
        Get
            If Session("LTKTApprovalDetail.grvDetilKasKeluarDATAOLD") Is Nothing Then
                Session("LTKTApprovalDetail.grvDetilKasKeluarDATAOLD") = New List(Of LTKTDetailCashInApprovalDetail)
            End If
            Return Session("LTKTApprovalDetail.grvDetilKasKeluarDATAOLD")
        End Get
        Set(ByVal value As List(Of LTKTDetailCashInApprovalDetail))
            Session("LTKTApprovalDetail.grvDetilKasKeluarDATAOLD") = value
        End Set
    End Property

    Public Property SetnGetRowEdit() As Integer
        Get
            If Session("LTKTApprovalDetail.RowEdit") Is Nothing Then
                Session("LTKTApprovalDetail.RowEdit") = -1
            End If
            Return Session("LTKTApprovalDetail.RowEdit")
        End Get
        Set(ByVal value As Integer)
            Session("LTKTApprovalDetail.RowEdit") = value
        End Set
    End Property

    Public Property SetnGetRowEditOLD() As Integer
        Get
            If Session("LTKTApprovalDetail.RowEditOLD") Is Nothing Then
                Session("LTKTApprovalDetail.RowEditOLD") = -1
            End If
            Return Session("LTKTApprovalDetail.RowEditOLD")
        End Get
        Set(ByVal value As Integer)
            Session("LTKTApprovalDetail.RowEditOLD") = value
        End Set
    End Property

#End Region

#Region "Function..."

    Sub LTKTTipeTerlaporChangeOld()
        Try
            If rblTerlaporTipePelapor.SelectedValue = 1 Then
                old_divPerorangan.Visible = True
                old_divKorporasi.Visible = False
            Else
                old_divPerorangan.Visible = False
                old_divKorporasi.Visible = True
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Sub LTKTTipeTerlaporChange()
        Try
            If rblTerlaporTipePelapor.SelectedValue = 1 Then
                divPerorangan.Visible = True
                divKorporasi.Visible = False
            Else
                divPerorangan.Visible = False
                divKorporasi.Visible = True
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Function JumlahKeseluruhanRp(ByVal objKas As List(Of LTKTDetailCashInApprovalDetail)) As Decimal
        Dim total As Decimal = 0
        For Each obj As LTKTDetailCashInApprovalDetail In objKas
            total = total + CDec(obj.JumlahRp)
        Next
        Return total
    End Function


    Sub deleteApproval()
        Using objApproval As LTKT_Approval = DataRepository.LTKT_ApprovalProvider.GetByPK_LTKT_Approval_Id(getAPPROVALPK)
            Using objApprovalDetail As TList(Of LTKT_ApprovalDetail) = DataRepository.LTKT_ApprovalDetailProvider.GetPaged(LTKT_ApprovalDetailColumn.FK_LTKT_Approval_Id.ToString & "=" & objApproval.PK_LTKT_Approval_Id.ToString, "", 0, Integer.MaxValue, 0)
                Using objTransactionCashIn As TList(Of LTKTTransactionCashIn_ApprovalDetail) = DataRepository.LTKTTransactionCashIn_ApprovalDetailProvider.GetPaged(LTKTTransactionCashIn_ApprovalDetailColumn.FK_LTKT_ApprovalDetail_Id.ToString & " = " & objApprovalDetail(0).PK_LTKT_ApprovalDetail_Id.ToString, "", 0, Integer.MaxValue, 0)
                    If objTransactionCashIn.Count > 0 Then
                        For Each singleTransactionCashIn As LTKTTransactionCashIn_ApprovalDetail In objTransactionCashIn
                            Using objDetailCashIn As TList(Of LTKTDetailCashInTransaction_ApprovalDetail) = DataRepository.LTKTDetailCashInTransaction_ApprovalDetailProvider.GetPaged(LTKTDetailCashInTransaction_ApprovalDetailColumn.FK_LTKTTransactionCashIn_ApprovalDetail_Id.ToString & " = " & singleTransactionCashIn.PK_LTKTTransactionCashIn_ApprovalDetail_Id.ToString, "", 0, Integer.MaxValue, 0)
                                For Each singleObjDetailCashIn As LTKTDetailCashInTransaction_ApprovalDetail In objDetailCashIn
                                    DataRepository.LTKTDetailCashInTransaction_ApprovalDetailProvider.Delete(singleObjDetailCashIn)
                                Next
                            End Using
                            DataRepository.LTKTTransactionCashIn_ApprovalDetailProvider.Delete(singleTransactionCashIn)
                        Next
                    End If
                End Using
                Using objTransactionCashOut As TList(Of LTKTTransactionCashOut_ApprovalDetail) = DataRepository.LTKTTransactionCashOut_ApprovalDetailProvider.GetPaged(LTKTTransactionCashOut_ApprovalDetailColumn.FK_LTKT_ApprovalDetail_Id.ToString & " = " & objApprovalDetail(0).PK_LTKT_ApprovalDetail_Id.ToString, "", 0, Integer.MaxValue, 0)
                    If objTransactionCashOut.Count > 0 Then
                        For Each singleTransactionCashOut As LTKTTransactionCashOut_ApprovalDetail In objTransactionCashOut
                            Using objDetailCashOut As TList(Of LTKTDetailCashOutTransaction_ApprovalDetail) = DataRepository.LTKTDetailCashOutTransaction_ApprovalDetailProvider.GetPaged(LTKTDetailCashOutTransaction_ApprovalDetailColumn.FK_LTKTTransactionCashOut_ApprovalDetail_Id.ToString & " = " & singleTransactionCashOut.PK_LTKTTransactionCashOut_ApprovalDetail_Id.ToString, "", 0, Integer.MaxValue, 0)
                                For Each singleObjDetailCashOut As LTKTDetailCashOutTransaction_ApprovalDetail In objDetailCashOut
                                    DataRepository.LTKTDetailCashOutTransaction_ApprovalDetailProvider.Delete(singleObjDetailCashOut)
                                Next
                            End Using
                            DataRepository.LTKTTransactionCashOut_ApprovalDetailProvider.Delete(singleTransactionCashOut)
                        Next
                    End If
                End Using
                DataRepository.LTKT_ApprovalDetailProvider.Delete(objApprovalDetail(0))
            End Using
            DataRepository.LTKT_ApprovalProvider.Delete(objApproval)
        End Using
    End Sub

    Sub deleteApprovaRejected()
        'ini dipake terus buat audit
        'Dim objAuditTrailType As MsAuditTrailEntityType = DataRepository.MsAuditTrailEntityTypeProvider.GetByPk_MsAuditTrailEntityTypeId(18)
        Using objApproval As LTKT_Approval = DataRepository.LTKT_ApprovalProvider.GetByPK_LTKT_Approval_Id(getAPPROVALPK)
            Using objApprovalDetail As TList(Of LTKT_ApprovalDetail) = DataRepository.LTKT_ApprovalDetailProvider.GetPaged(LTKT_ApprovalDetailColumn.FK_LTKT_Approval_Id.ToString & "=" & objApproval.PK_LTKT_Approval_Id.ToString, "", 0, Integer.MaxValue, 0)
                Using objTransactionCashIn As TList(Of LTKTTransactionCashIn_ApprovalDetail) = DataRepository.LTKTTransactionCashIn_ApprovalDetailProvider.GetPaged(LTKTTransactionCashIn_ApprovalDetailColumn.FK_LTKT_ApprovalDetail_Id.ToString & " = " & objApprovalDetail(0).PK_LTKT_ApprovalDetail_Id.ToString, "", 0, Integer.MaxValue, 0)
                    If objTransactionCashIn.Count > 0 Then
                        For Each singleTransactionCashIn As LTKTTransactionCashIn_ApprovalDetail In objTransactionCashIn
                            Using objDetailCashIn As TList(Of LTKTDetailCashInTransaction_ApprovalDetail) = DataRepository.LTKTDetailCashInTransaction_ApprovalDetailProvider.GetPaged(LTKTDetailCashInTransaction_ApprovalDetailColumn.FK_LTKTTransactionCashIn_ApprovalDetail_Id.ToString & " = " & singleTransactionCashIn.PK_LTKTTransactionCashIn_ApprovalDetail_Id.ToString, "", 0, Integer.MaxValue, 0)
                                For Each singleObjDetailCashIn As LTKTDetailCashInTransaction_ApprovalDetail In objDetailCashIn
                                    If objApproval.FK_MsMode_Id.GetValueOrDefault = 1 Then
                                        AuditTrailBLL.InsertAuditTrail(AuditTrailBLL.AuditTrailOperation.Add, Date.Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Rejected add new data LTKT Transaction Detail CashIn", singleObjDetailCashIn, Nothing)
                                    ElseIf objApproval.FK_MsMode_Id.GetValueOrDefault = 2 Then
                                        AuditTrailBLL.InsertAuditTrail(AuditTrailBLL.AuditTrailOperation.Edit, Date.Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Rejected edit data LTKT Transaction Detail CashIn", singleObjDetailCashIn, Nothing)
                                    ElseIf objApproval.FK_MsMode_Id.GetValueOrDefault = 3 Then
                                        AuditTrailBLL.InsertAuditTrail(AuditTrailBLL.AuditTrailOperation.Delete, Date.Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Rejected delete data LTKT Transaction Detail CashIn", Nothing, singleObjDetailCashIn)
                                    End If
                                    DataRepository.LTKTDetailCashInTransaction_ApprovalDetailProvider.Delete(singleObjDetailCashIn)
                                Next
                            End Using
                            If objApproval.FK_MsMode_Id.GetValueOrDefault = 1 Then
                                AuditTrailBLL.InsertAuditTrail(AuditTrailBLL.AuditTrailOperation.Add, Date.Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Rejected add new data LTKT Transaction CashIn", singleTransactionCashIn, Nothing)
                            ElseIf objApproval.FK_MsMode_Id.GetValueOrDefault = 2 Then
                                AuditTrailBLL.InsertAuditTrail(AuditTrailBLL.AuditTrailOperation.Edit, Date.Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Rejected edit data LTKT Transaction CashIn", singleTransactionCashIn, Nothing)
                            ElseIf objApproval.FK_MsMode_Id.GetValueOrDefault = 3 Then
                                AuditTrailBLL.InsertAuditTrail(AuditTrailBLL.AuditTrailOperation.Delete, Date.Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Rejected delete data LTKT Transaction CashIn", Nothing, singleTransactionCashIn)
                            End If
                            DataRepository.LTKTTransactionCashIn_ApprovalDetailProvider.Delete(singleTransactionCashIn)
                        Next
                    End If
                End Using
                Using objTransactionCashOut As TList(Of LTKTTransactionCashOut_ApprovalDetail) = DataRepository.LTKTTransactionCashOut_ApprovalDetailProvider.GetPaged(LTKTTransactionCashOut_ApprovalDetailColumn.FK_LTKT_ApprovalDetail_Id.ToString & " = " & objApprovalDetail(0).PK_LTKT_ApprovalDetail_Id.ToString, "", 0, Integer.MaxValue, 0)
                    If objTransactionCashOut.Count > 0 Then
                        For Each singleTransactionCashOut As LTKTTransactionCashOut_ApprovalDetail In objTransactionCashOut
                            Using objDetailCashOut As TList(Of LTKTDetailCashOutTransaction_ApprovalDetail) = DataRepository.LTKTDetailCashOutTransaction_ApprovalDetailProvider.GetPaged(LTKTDetailCashOutTransaction_ApprovalDetailColumn.FK_LTKTTransactionCashOut_ApprovalDetail_Id.ToString & " = " & singleTransactionCashOut.PK_LTKTTransactionCashOut_ApprovalDetail_Id.ToString, "", 0, Integer.MaxValue, 0)
                                For Each singleObjDetailCashOut As LTKTDetailCashOutTransaction_ApprovalDetail In objDetailCashOut
                                    If objApproval.FK_MsMode_Id.GetValueOrDefault = 1 Then
                                        AuditTrailBLL.InsertAuditTrail(AuditTrailBLL.AuditTrailOperation.Add, Date.Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Rejected add new data LTKT Transaction Detail CashOut", singleObjDetailCashOut, Nothing)
                                    ElseIf objApproval.FK_MsMode_Id.GetValueOrDefault = 2 Then
                                        AuditTrailBLL.InsertAuditTrail(AuditTrailBLL.AuditTrailOperation.Edit, Date.Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Rejected edit data LTKT Transaction Detail CashOut", singleObjDetailCashOut, Nothing)
                                    ElseIf objApproval.FK_MsMode_Id.GetValueOrDefault = 3 Then
                                        AuditTrailBLL.InsertAuditTrail(AuditTrailBLL.AuditTrailOperation.Delete, Date.Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Rejected delete data LTKT Transaction Detail CashOut", Nothing, singleObjDetailCashOut)
                                    End If
                                    DataRepository.LTKTDetailCashOutTransaction_ApprovalDetailProvider.Delete(singleObjDetailCashOut)
                                Next
                            End Using
                            If objApproval.FK_MsMode_Id.GetValueOrDefault = 1 Then
                                AuditTrailBLL.InsertAuditTrail(AuditTrailBLL.AuditTrailOperation.Add, Date.Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Rejected add new data LTKT Transaction CashOut", singleTransactionCashOut, Nothing)
                            ElseIf objApproval.FK_MsMode_Id.GetValueOrDefault = 2 Then
                                AuditTrailBLL.InsertAuditTrail(AuditTrailBLL.AuditTrailOperation.Edit, Date.Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Rejected edit data LTKT Transaction CashOut", singleTransactionCashOut, Nothing)
                            ElseIf objApproval.FK_MsMode_Id.GetValueOrDefault = 3 Then
                                AuditTrailBLL.InsertAuditTrail(AuditTrailBLL.AuditTrailOperation.Delete, Date.Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Rejected delete data LTKT Transaction CashOut", Nothing, singleTransactionCashOut)
                            End If
                            DataRepository.LTKTTransactionCashOut_ApprovalDetailProvider.Delete(singleTransactionCashOut)
                        Next
                    End If
                End Using
                If objApproval.FK_MsMode_Id.GetValueOrDefault = 1 Then
                    AuditTrailBLL.InsertAuditTrail(AuditTrailBLL.AuditTrailOperation.Add, Date.Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Rejected add new data LTKT", objApprovalDetail(0), Nothing)
                ElseIf objApproval.FK_MsMode_Id.GetValueOrDefault = 2 Then
                    AuditTrailBLL.InsertAuditTrail(AuditTrailBLL.AuditTrailOperation.Edit, Date.Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Rejected edit data LTKT", objApprovalDetail(0), Nothing)
                ElseIf objApproval.FK_MsMode_Id.GetValueOrDefault = 3 Then
                    AuditTrailBLL.InsertAuditTrail(AuditTrailBLL.AuditTrailOperation.Delete, Date.Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Rejected delete data LTKT", Nothing, objApprovalDetail(0))
                End If
                DataRepository.LTKT_ApprovalDetailProvider.Delete(objApprovalDetail(0))
            End Using
            DataRepository.LTKT_ApprovalProvider.Delete(objApproval)
        End Using



    End Sub

    Sub hideControl()
        imgReject.Visible = False
        imgApprove.Visible = False
        ImageCancel.Visible = False
    End Sub

    Sub clearKasMasukKasKeluar()
        'bersihin grid
        Session("LTKTApprovalDetail.grvTRXKMDetilValutaAsingDATA") = Nothing
        Session("LTKTApprovalDetail.grvDetilKasKeluarDATA") = Nothing
        grvDetilKasKeluar.DataSource = SetnGetgrvDetilKasKeluar
        grvDetilKasKeluar.DataBind()
        grvTRXKMDetilValutaAsing.DataSource = SetnGetgrvTRXKMDetilValutaAsing
        grvTRXKMDetilValutaAsing.DataBind()

        'bersihin sisa Kas Masuk
        txtTRXKMTanggalTrx.Text = ""
        txtTRXKMNamaKantor.Text = ""
        txtTRXKMKotaKab.Text = ""
        hfTRXKMKotaKab.Value = ""
        txtTRXKMProvinsi.Text = ""
        hfTRXKMProvinsi.Value = ""
        txtTRXKMDetilKasMasuk.Text = ""
        txtTRXKMDetilMataUang.Text = ""
        hfTRXKMDetilMataUang.Value = ""
        txtTRXKMDetailKursTrx.Text = ""
        txtTRXKMDetilJumlah.Text = ""
        lblTRXKMDetilValutaAsingJumlahRp.Text = ""
        txtTRXKMNoRekening.Text = ""
        txtTRXKMINDVGelar.Text = ""
        txtTRXKMINDVNamaLengkap.Text = ""
        txtTRXKMINDVTempatLahir.Text = ""
        txtTRXKMINDVTanggalLahir.Text = ""
        rblTRXKMINDVKewarganegaraan.SelectedIndex = -1
        cboTRXKMINDVNegara.SelectedIndex = 0
        txtTRXKMINDVDOMNamaJalan.Text = ""
        txtTRXKMINDVDOMRTRW.Text = ""
        txtTRXKMINDVDOMKelurahan.Text = ""
        hfTRXKMINDVDOMKelurahan.Value = ""
        txtTRXKMINDVDOMKecamatan.Text = ""
        hfTRXKMINDVDOMKecamatan.Value = ""
        txtTRXKMINDVDOMKotaKab.Text = ""
        hfTRXKMINDVDOMKotaKab.Value = ""
        txtTRXKMINDVDOMKodePos.Text = ""
        txtTRXKMINDVDOMProvinsi.Text = ""
        hfTRXKMINDVDOMProvinsi.Value = ""
        'chkTRXKMINDVCopyDOM.Checked = False
        txtTRXKMINDVIDNamaJalan.Text = ""
        txtTRXKMINDVIDRTRW.Text = ""
        txtTRXKMINDVIDKelurahan.Text = ""
        hfTRXKMINDVIDKelurahan.Value = ""
        txtTRXKMINDVIDKecamatan.Text = ""
        hfTRXKMINDVIDKecamatan.Value = ""
        txtTRXKMINDVIDKotaKab.Text = ""
        hfTRXKMINDVIDKotaKab.Value = ""
        txtTRXKMINDVIDKodePos.Text = ""
        txtTRXKMINDVIDProvinsi.Text = ""
        hfTRXKMINDVIDProvinsi.Value = ""
        txtTRXKMINDVNANamaJalan.Text = ""
        txtTRXKMINDVNANegara.Text = ""
        hfTRXKMINDVNANegara.Value = ""
        txtTRXKMINDVNAProvinsi.Text = ""
        hfTRXKMINDVNAProvinsi.Value = ""
        txtTRXKMINDVNAKota.Text = ""
        hfTRXKMINDVNAKota.Value = ""
        txtTRXKMINDVNAKodePos.Text = ""
        cboTRXKMINDVJenisID.SelectedIndex = 0
        txtTRXKMINDVNomorID.Text = ""
        txtTRXKMINDVNPWP.Text = ""
        txtTRXKMINDVPekerjaan.Text = ""
        hfTRXKMINDVPekerjaan.Value = ""
        txtTRXKMINDVJabatan.Text = ""
        txtTRXKMINDVPenghasilanRataRata.Text = ""
        txtTRXKMINDVTempatKerja.Text = ""
        txtTRXKMINDVTujuanTrx.Text = ""
        txtTRXKMINDVSumberDana.Text = ""
        txtTRXKMINDVNamaBankLain.Text = ""
        txtTRXKMINDVNoRekeningTujuan.Text = ""

        cboTRXKMCORPBentukBadanUsaha.SelectedIndex = 0
        txtTRXKMCORPNama.Text = ""
        txtTRXKMCORPBidangUsaha.Text = ""
        hfTRXKMCORPBidangUsaha.Value = ""
        txtTRXKMCORPDLNamaJalan.Text = ""
        txtTRXKMCORPDLRTRW.Text = ""
        txtTRXKMCORPDLKelurahan.Text = ""
        hfTRXKMCORPDLKelurahan.Value = ""
        txtTRXKMCORPDLKecamatan.Text = ""
        hfTRXKMCORPDLKecamatan.Value = ""
        txtTRXKMCORPDLKotaKab.Text = ""
        hfTRXKMCORPDLKotaKab.Value = ""
        txtTRXKMCORPDLKodePos.Text = ""
        txtTRXKMCORPDLProvinsi.Text = ""
        hfTRXKMCORPDLProvinsi.Value = ""
        txtTRXKMCORPDLNegara.Text = ""
        hfTRXKMCORPDLNegara.Value = ""
        txtTRXKMCORPLNNamaJalan.Text = ""
        txtTRXKMCORPLNNegara.Text = ""
        hfTRXKMCORPLNNegara.Value = ""
        txtTRXKMCORPLNProvinsi.Text = ""
        hfTRXKMCORPLNProvinsi.Value = ""
        txtTRXKMCORPLNKota.Text = ""
        hfTRXKMCORPLNKota.Value = ""
        txtTRXKMCORPLNKodePos.Text = ""
        txtTRXKMCORPNPWP.Text = ""
        txtTRXKMCORPTujuanTrx.Text = ""
        txtTRXKMCORPSumberDana.Text = ""
        txtTRXKMCORPNamaBankLain.Text = ""
        txtTRXKMCORPNoRekeningTujuan.Text = ""


        'Bersihin sisa Kas Keluar
        txtTRXKKTanggalTransaksi.Text = ""
        txtTRXKKNamaKantor.Text = ""
        txtTRXKKKotaKab.Text = ""
        hfTRXKKKotaKab.Value = ""
        txtTRXKKProvinsi.Text = ""
        hfTRXKKProvinsi.Value = ""
        txtTRXKKDetailKasKeluar.Text = ""
        txtTRXKKDetilMataUang.Text = ""
        hfTRXKKDetilMataUang.Value = ""
        txtTRXKKDetilKursTrx.Text = ""
        txtTRXKKDetilJumlah.Text = ""
        lblDetilKasKeluarJumlahRp.Text = ""
        'txtTRXKKNoRekening.Text = ""
        txtTRXKKINDVGelar.Text = ""
        txtTRXKKINDVNamaLengkap.Text = ""
        txtTRXKKINDVTempatLahir.Text = ""
        txtTRXKKINDVTglLahir.Text = ""
        rblTRXKKINDVKewarganegaraan.SelectedIndex = -1
        cboTRXKKINDVNegara.SelectedIndex = 0
        txtTRXKKINDVDOMNamaJalan.Text = ""
        txtTRXKKINDVDOMRTRW.Text = ""
        txtTRXKKINDVDOMKelurahan.Text = ""
        hfTRXKKINDVDOMKelurahan.Value = ""
        txtTRXKKINDVDOMKecamatan.Text = ""
        hfTRXKKINDVDOMKecamatan.Value = ""
        txtTRXKKINDVDOMKotaKab.Text = ""
        hfTRXKKINDVDOMKotaKab.Value = ""
        txtTRXKKINDVDOMKodePos.Text = ""
        txtTRXKKINDVDOMProvinsi.Text = ""
        hfTRXKKINDVDOMProvinsi.Value = ""
        'chkTRXKKINDVIDCopyDOM.Checked = False
        txtTRXKKINDVIDNamaJalan.Text = ""
        txtTRXKKINDVIDRTRW.Text = ""
        txtTRXKKINDVIDKelurahan.Text = ""
        hfTRXKKINDVIDKelurahan.Value = ""
        txtTRXKKINDVIDKecamatan.Text = ""
        hfTRXKKINDVIDKecamatan.Value = ""
        txtTRXKKINDVIDKotaKab.Text = ""
        hfTRXKKINDVIDKotaKab.Value = ""
        txtTRXKKINDVIDKodePos.Text = ""
        txtTRXKKINDVIDProvinsi.Text = ""
        hfTRXKKINDVIDProvinsi.Value = ""
        txtTRXKKINDVNANamaJalan.Text = ""
        txtTRXKKINDVNANegara.Text = ""
        hfTRXKKINDVNANegara.Value = ""
        txtTRXKKINDVNAProvinsi.Text = ""
        hfTRXKKINDVNAProvinsi.Value = ""
        txtTRXKKINDVNAKota.Text = ""
        hfTRXKKINDVNAKota.Value = ""
        txtTRXKKINDVNAKodePos.Text = ""
        cboTRXKKINDVJenisID.SelectedIndex = 0
        txtTRXKKINDVNomorId.Text = ""
        txtTRXKKINDVNPWP.Text = ""
        txtTRXKKINDVPekerjaan.Text = ""
        hfTRXKKINDVPekerjaan.Value = ""
        txtTRXKKINDVJabatan.Text = ""
        txtTRXKKINDVPenghasilanRataRata.Text = ""
        txtTRXKKINDVTempatKerja.Text = ""
        txtTRXKKINDVTujuanTrx.Text = ""
        txtTRXKKINDVSumberDana.Text = ""
        txtTRXKKINDVNamaBankLain.Text = ""
        txtTRXKKINDVNoRekTujuan.Text = ""

        cboTRXKKCORPBentukBadanUsaha.SelectedIndex = 0
        txtTRXKKCORPNama.Text = ""
        txtTRXKKCORPBidangUsaha.Text = ""
        hfTRXKKCORPBidangUsaha.Value = ""
        txtTRXKKCORPDLNamaJalan.Text = ""
        txtTRXKKCORPDLRTRW.Text = ""
        txtTRXKKCORPDLKelurahan.Text = ""
        hfTRXKKCORPDLKelurahan.Value = ""
        txtTRXKKCORPDLKecamatan.Text = ""
        hfTRXKKCORPDLKecamatan.Value = ""
        txtTRXKKCORPDLKotaKab.Text = ""
        hfTRXKKCORPDLKotaKab.Value = ""
        txtTRXKKCORPDLKodePos.Text = ""
        txtTRXKKCORPDLProvinsi.Text = ""
        hfTRXKKCORPDLProvinsi.Value = ""
        txtTRXKKCORPDLNegara.Text = ""
        hfTRXKKCORPDLNegara.Value = ""
        txtTRXKKCORPLNNamaJalan.Text = ""
        txtTRXKKCORPLNNegara.Text = ""
        hfTRXKKCORPLNNegara.Value = ""
        txtTRXKKCORPLNProvinsi.Text = ""
        hfTRXKKCORPLNProvinsi.Value = ""
        txtTRXKKCORPLNKota.Text = ""
        hfTRXKKCORPLNKota.Value = ""
        txtTRXKKCORPLNKodePos.Text = ""
        txtTRXKKCORPNPWP.Text = ""
        txtTRXKKCORPTujuanTrx.Text = ""
        txtTRXKKCORPSumberDana.Text = ""
        txtTRXKKCORPNamaBankLain.Text = ""
        txtTRXKKCORPNoRekeningTujuan.Text = ""
        txtTRXKKRekeningKK.Text = ""




    End Sub
    Sub clearKasMasukKasKeluarOld()
        'bersihin grid
        Session("LTKTApprovalDetail.grvTRXKMDetilValutaAsingDATAOLD") = Nothing
        Session("LTKTApprovalDetail.grvDetilKasKeluarDATAOLD") = Nothing
        old_grvDetilKasKeluar.DataSource = SetnGetgrvDetilKasKeluar
        old_grvDetilKasKeluar.DataBind()
        old_grvTRXKMDetilValutaAsing.DataSource = SetnGetgrvTRXKMDetilValutaAsing
        old_grvTRXKMDetilValutaAsing.DataBind()

        'bersihin sisa Kas Masuk
        old_txtTRXKMTanggalTrx.Text = ""
        old_txtTRXKMNamaKantor.Text = ""
        old_txtTRXKMKotaKab.Text = ""
        old_hfTRXKMKotaKab.Value = ""
        old_txtTRXKMDetilKasMasuk.Text = ""
        old_txtTRXKMDetilMataUang.Text = ""
        old_hfTRXKMDetilMataUang.Value = ""
        old_txtTRXKMDetailKursTrx.Text = ""
        old_txtTRXKMDetilJumlah.Text = ""
        old_lblTRXKMDetilValutaAsingJumlahRp.Text = ""
        old_txtTRXKMNoRekening.Text = ""
        old_txtTRXKMINDVGelar.Text = ""
        old_txtTRXKMINDVNamaLengkap.Text = ""
        old_txtTRXKMINDVTempatLahir.Text = ""
        old_txtTRXKMINDVTanggalLahir.Text = ""
        old_rblTRXKMINDVKewarganegaraan.SelectedIndex = -1
        old_cboTRXKMINDVNegara.SelectedIndex = 0
        old_txtTRXKMINDVDOMNamaJalan.Text = ""
        old_txtTRXKMINDVDOMRTRW.Text = ""
        old_txtTRXKMINDVDOMKelurahan.Text = ""
        old_hfTRXKMINDVDOMKelurahan.Value = ""
        old_txtTRXKMINDVDOMKecamatan.Text = ""
        old_hfTRXKMINDVDOMKecamatan.Value = ""
        old_txtTRXKMINDVDOMKotaKab.Text = ""
        old_hfTRXKMINDVDOMKotaKab.Value = ""
        old_txtTRXKMINDVDOMKodePos.Text = ""
        old_txtTRXKMINDVDOMProvinsi.Text = ""
        old_hfTRXKMINDVDOMProvinsi.Value = ""
        'chkTRXKMINDVCopyDOM.Checked = False
        old_txtTRXKMINDVIDNamaJalan.Text = ""
        old_txtTRXKMINDVIDRTRW.Text = ""
        old_txtTRXKMINDVIDKelurahan.Text = ""
        old_hfTRXKMINDVIDKelurahan.Value = ""
        old_txtTRXKMINDVIDKecamatan.Text = ""
        old_hfTRXKMINDVIDKecamatan.Value = ""
        old_txtTRXKMINDVIDKotaKab.Text = ""
        old_hfTRXKMINDVIDKotaKab.Value = ""
        old_txtTRXKMINDVIDKodePos.Text = ""
        old_txtTRXKMINDVIDProvinsi.Text = ""
        old_hfTRXKMINDVIDProvinsi.Value = ""
        old_txtTRXKMINDVNANamaJalan.Text = ""
        old_txtTRXKMINDVNANegara.Text = ""
        old_hfTRXKMINDVNANegara.Value = ""
        old_txtTRXKMINDVNAProvinsi.Text = ""
        old_hfTRXKMINDVNAProvinsi.Value = ""
        old_txtTRXKMINDVNAKota.Text = ""
        old_hfTRXKMINDVNAKota.Value = ""
        old_txtTRXKMINDVNAKodePos.Text = ""
        old_cboTRXKMINDVJenisID.SelectedIndex = 0
        old_txtTRXKMINDVNomorID.Text = ""
        old_txtTRXKMINDVNPWP.Text = ""
        old_txtTRXKMINDVPekerjaan.Text = ""
        old_hfTRXKMINDVPekerjaan.Value = ""
        old_txtTRXKMINDVJabatan.Text = ""
        old_txtTRXKMINDVPenghasilanRataRata.Text = ""
        old_txtTRXKMINDVTempatKerja.Text = ""
        old_txtTRXKMINDVTujuanTrx.Text = ""
        old_txtTRXKMINDVSumberDana.Text = ""
        old_txtTRXKMINDVNamaBankLain.Text = ""
        old_txtTRXKMINDVNoRekeningTujuan.Text = ""

        old_cboTRXKMCORPBentukBadanUsaha.SelectedIndex = 0
        old_txtTRXKMCORPNama.Text = ""
        old_txtTRXKMCORPBidangUsaha.Text = ""
        old_hfTRXKMCORPBidangUsaha.Value = ""
        old_txtTRXKMCORPDLNamaJalan.Text = ""
        old_txtTRXKMCORPDLRTRW.Text = ""
        old_txtTRXKMCORPDLKelurahan.Text = ""
        old_hfTRXKMCORPDLKelurahan.Value = ""
        old_txtTRXKMCORPDLKecamatan.Text = ""
        old_hfTRXKMCORPDLKecamatan.Value = ""
        old_txtTRXKMCORPDLKotaKab.Text = ""
        old_hfTRXKMCORPDLKotaKab.Value = ""
        old_txtTRXKMCORPDLKodePos.Text = ""
        old_txtTRXKMCORPDLProvinsi.Text = ""
        old_hfTRXKMCORPDLProvinsi.Value = ""
        old_txtTRXKMCORPDLNegara.Text = ""
        old_hfTRXKMCORPDLNegara.Value = ""
        old_txtTRXKMCORPLNNamaJalan.Text = ""
        old_txtTRXKMCORPLNNegara.Text = ""
        old_hfTRXKMCORPLNNegara.Value = ""
        old_txtTRXKMCORPLNProvinsi.Text = ""
        old_hfTRXKMCORPLNProvinsi.Value = ""
        old_txtTRXKMCORPLNKota.Text = ""
        old_hfTRXKMCORPLNKota.Value = ""
        old_txtTRXKMCORPLNKodePos.Text = ""
        old_txtTRXKMCORPNPWP.Text = ""
        old_txtTRXKMCORPTujuanTrx.Text = ""
        old_txtTRXKMCORPSumberDana.Text = ""
        old_txtTRXKMCORPNamaBankLain.Text = ""
        old_txtTRXKMCORPNoRekeningTujuan.Text = ""


        'Bersihin sisa Kas Keluar
        old_txtTRXKKTanggalTransaksi.Text = ""
        old_txtTRXKKNamaKantor.Text = ""
        old_txtTRXKKKotaKab.Text = ""
        old_hfTRXKKKotaKab.Value = ""
        old_txtTRXKKDetailKasKeluar.Text = ""
        old_txtTRXKKDetilMataUang.Text = ""
        old_hfTRXKKDetilMataUang.Value = ""
        old_txtTRXKKDetilKursTrx.Text = ""
        old_txtTRXKKDetilJumlah.Text = ""
        old_lblDetilKasKeluarJumlahRp.Text = ""
        'txtTRXKKNoRekening.Text = ""
        old_txtTRXKKINDVGelar.Text = ""
        old_txtTRXKKINDVNamaLengkap.Text = ""
        old_txtTRXKKINDVTempatLahir.Text = ""
        old_txtTRXKKINDVTglLahir.Text = ""
        old_rblTRXKKINDVKewarganegaraan.SelectedIndex = -1
        old_cboTRXKKINDVNegara.SelectedIndex = 0
        old_txtTRXKKINDVDOMNamaJalan.Text = ""
        old_txtTRXKKINDVDOMRTRW.Text = ""
        old_txtTRXKKINDVDOMKelurahan.Text = ""
        old_hfTRXKKINDVDOMKelurahan.Value = ""
        old_txtTRXKKINDVDOMKecamatan.Text = ""
        old_hfTRXKKINDVDOMKecamatan.Value = ""
        old_txtTRXKKINDVDOMKotaKab.Text = ""
        old_hfTRXKKINDVDOMKotaKab.Value = ""
        old_txtTRXKKINDVDOMKodePos.Text = ""
        old_txtTRXKKINDVDOMProvinsi.Text = ""
        old_hfTRXKKINDVDOMProvinsi.Value = ""
        'chkTRXKKINDVIDCopyDOM.Checked = False
        old_txtTRXKKINDVIDNamaJalan.Text = ""
        old_txtTRXKKINDVIDRTRW.Text = ""
        old_txtTRXKKINDVIDKelurahan.Text = ""
        old_hfTRXKKINDVIDKelurahan.Value = ""
        old_txtTRXKKINDVIDKecamatan.Text = ""
        old_hfTRXKKINDVIDKecamatan.Value = ""
        old_txtTRXKKINDVIDKotaKab.Text = ""
        old_hfTRXKKINDVIDKotaKab.Value = ""
        old_txtTRXKKINDVIDKodePos.Text = ""
        old_txtTRXKKINDVIDProvinsi.Text = ""
        old_hfTRXKKINDVIDProvinsi.Value = ""
        old_txtTRXKKINDVNANamaJalan.Text = ""
        old_txtTRXKKINDVNANegara.Text = ""
        old_hfTRXKKINDVNANegara.Value = ""
        old_txtTRXKKINDVNAProvinsi.Text = ""
        old_hfTRXKKINDVNAProvinsi.Value = ""
        old_txtTRXKKINDVNAKota.Text = ""
        old_hfTRXKKINDVNAKota.Value = ""
        old_txtTRXKKINDVNAKodePos.Text = ""
        old_cboTRXKKINDVJenisID.SelectedIndex = 0
        old_txtTRXKKINDVNomorId.Text = ""
        old_txtTRXKKINDVNPWP.Text = ""
        old_txtTRXKKINDVPekerjaan.Text = ""
        old_hfTRXKKINDVPekerjaan.Value = ""
        old_txtTRXKKINDVJabatan.Text = ""
        old_txtTRXKKINDVPenghasilanRataRata.Text = ""
        old_txtTRXKKINDVTempatKerja.Text = ""
        old_txtTRXKKINDVTujuanTrx.Text = ""
        old_txtTRXKKINDVSumberDana.Text = ""
        old_txtTRXKKINDVNamaBankLain.Text = ""
        old_txtTRXKKINDVNoRekTujuan.Text = ""

        old_cboTRXKKCORPBentukBadanUsaha.SelectedIndex = 0
        old_txtTRXKKCORPNama.Text = ""
        old_txtTRXKKCORPBidangUsaha.Text = ""
        old_hfTRXKKCORPBidangUsaha.Value = ""
        old_txtTRXKKCORPDLNamaJalan.Text = ""
        old_txtTRXKKCORPDLRTRW.Text = ""
        old_txtTRXKKCORPDLKelurahan.Text = ""
        old_hfTRXKKCORPDLKelurahan.Value = ""
        old_txtTRXKKCORPDLKecamatan.Text = ""
        old_hfTRXKKCORPDLKecamatan.Value = ""
        old_txtTRXKKCORPDLKotaKab.Text = ""
        old_hfTRXKKCORPDLKotaKab.Value = ""
        old_txtTRXKKCORPDLKodePos.Text = ""
        old_txtTRXKKCORPDLProvinsi.Text = ""
        old_hfTRXKKCORPDLProvinsi.Value = ""
        old_txtTRXKKCORPDLNegara.Text = ""
        old_hfTRXKKCORPDLNegara.Value = ""
        old_txtTRXKKCORPLNNamaJalan.Text = ""
        old_txtTRXKKCORPLNNegara.Text = ""
        old_hfTRXKKCORPLNNegara.Value = ""
        old_txtTRXKKCORPLNProvinsi.Text = ""
        old_hfTRXKKCORPLNProvinsi.Value = ""
        old_txtTRXKKCORPLNKota.Text = ""
        old_hfTRXKKCORPLNKota.Value = ""
        old_txtTRXKKCORPLNKodePos.Text = ""
        old_txtTRXKKCORPNPWP.Text = ""
        old_txtTRXKKCORPTujuanTrx.Text = ""
        old_txtTRXKKCORPSumberDana.Text = ""
        old_txtTRXKKCORPNamaBankLain.Text = ""
        old_txtTRXKKCORPNoRekeningTujuan.Text = ""
        old_txtTRXKKRekeningKK.Text = ""


    End Sub

    Sub compareLTKT()
        If OldPanel.Visible = True And NewPanel.Visible = True Then
            Dim index As Integer = 0
            Dim oldValue As ControlCollection = OldPanel.Controls

            For Each item As Control In NewPanel.Controls
                If TypeOf (item) Is Label Then
                    If CType(item, Label).Text <> CType(oldValue(index), Label).Text Then
                        CType(item, Label).ForeColor = Drawing.Color.Red
                        CType(oldValue(index), Label).ForeColor = Drawing.Color.Red
                    End If
                End If
                If TypeOf (item) Is MagicAjax.UI.Controls.AjaxPanel Then
                    Dim index2 As Integer = 0
                    Dim oldValue2 As ControlCollection = oldValue(index).Controls
                    For Each item2 As Control In CType(item, MagicAjax.UI.Controls.AjaxPanel).Controls
                        If TypeOf (item2) Is Label Then
                            If CType(item2, Label).Text <> CType(oldValue2(index2), Label).Text Then
                                CType(item2, Label).ForeColor = Drawing.Color.Red
                                CType(oldValue2(index2), Label).ForeColor = Drawing.Color.Red
                            End If
                        End If

                        If TypeOf (item2) Is System.Web.UI.HtmlControls.HtmlGenericControl Then
                            Dim index3 As Integer = 0
                            Dim oldvalue3 As ControlCollection = oldValue2(index2).Controls
                            For Each item3 As Control In CType(item2, System.Web.UI.HtmlControls.HtmlGenericControl).Controls
                                If TypeOf (item3) Is Label Then
                                    If CType(item3, Label).Text <> CType(oldvalue3(index3), Label).Text Then
                                        CType(item3, Label).ForeColor = Drawing.Color.Red
                                        CType(oldvalue3(index3), Label).ForeColor = Drawing.Color.Red
                                    End If
                                End If
                                index3 = index3 + 1
                            Next
                        End If


                        index2 = index2 + 1
                    Next
                End If

                index = index + 1
            Next
        End If
    End Sub

    Sub clearSession()
        Session("LTKTApprovalDetail.grvTRXKMDetilValutaAsingDATA") = Nothing
        Session("LTKTApprovalDetail.grvDetilKasKeluarDATA") = Nothing
        Session("LTKTApprovalDetail.ResumeKasMasukKasKeluar") = Nothing
        Session("LTKTApprovalDetail.RowEdit") = Nothing
        Session("LTKTApprovalDetail.grvTRXKMDetilValutaAsingDATAOLD") = Nothing
        Session("LTKTApprovalDetail.grvDetilKasKeluarDATAOLD") = Nothing
        Session("LTKTApprovalDetail.ResumeKasMasukKasKeluarOLD") = Nothing
        Session("LTKTApprovalDetail.RowEditOLD") = Nothing
        Session("LTKTApprovalDetail.APPROVALPK") = Nothing
    End Sub

    Sub loadApprovalInformation()
        Using objApproval As LTKT_Approval = DataRepository.LTKT_ApprovalProvider.GetByPK_LTKT_Approval_Id(getAPPROVALPK)
            If Not IsNothing(objApproval) Then
                Using objUser As User = DataRepository.UserProvider.GetBypkUserID(objApproval.RequestedBy)
                    LblRequestedByApprovalDetail.Text = Safe(objUser.UserName)
                End Using

                LblRequestedDateApprovalDetail.Text = Safe(objApproval.RequestedDate.ToString)
                Using objMode As Mode = DataRepository.ModeProvider.GetByMsModeID(objApproval.FK_MsMode_Id)
                    LblActionApprovalDetail.Text = Safe(objMode.Nama)
                End Using
            End If
        End Using
    End Sub


    Sub loadResumeOld()
        Dim objResume As List(Of ResumeKasMasukKeluarLTKT) = SetnGetResumeKasMasukKasKeluarOLD


        Dim objApprovalDetail As TList(Of LTKT_ApprovalDetail) = DataRepository.LTKT_ApprovalDetailProvider.GetPaged(LTKT_ApprovalDetailColumn.FK_LTKT_Approval_Id.ToString & " = " & getAPPROVALPK.ToString, "", 0, Integer.MaxValue, 0)

        If objApprovalDetail.Count > 0 Then
            'load Kas Masuk ke objResume
            Using objListTransactionCashIn As TList(Of LTKTTransactionCashIn) = DataRepository.LTKTTransactionCashInProvider.GetPaged(LTKTTransactionCashInColumn.FK_LTKT_Id.ToString & " = " & objApprovalDetail(0).PK_LTKT_Id.ToString, "", 0, Integer.MaxValue, 0)
                For Each objSingleTransactinCashIn As LTKTTransactionCashIn In objListTransactionCashIn


                    Dim objKas As ResumeKasMasukKeluarLTKT = New ResumeKasMasukKeluarLTKT
                    'Insert untuk tampilan Grid
                    objKas.TransactionDate = objSingleTransactinCashIn.TanggalTransaksi
                    objKas.Branch = objSingleTransactinCashIn.NamaKantorPJK
                    objKas.TransactionNominal = objSingleTransactinCashIn.Total.ToString
                    objKas.AccountNumber = objSingleTransactinCashIn.NomorRekening
                    objKas.Kas = "Kas Masuk"
                    objKas.Type = "LTKT"

                    'Insert Data Ke ObjectTransaction
                    objKas.TransactionCashIn = New LTKTTransactionCashIn
                    objKas.TransactionCashIn.PK_LTKTTransactionCashIn_Id = objSingleTransactinCashIn.PK_LTKTTransactionCashIn_Id
                    objKas.TransactionCashIn.TanggalTransaksi = objSingleTransactinCashIn.TanggalTransaksi.GetValueOrDefault
                    objKas.TransactionCashIn.NamaKantorPJK = objSingleTransactinCashIn.NamaKantorPJK
                    objKas.TransactionCashIn.FK_MsKotaKab_Id = objSingleTransactinCashIn.FK_MsKotaKab_Id.GetValueOrDefault
                    objKas.TransactionCashIn.FK_MsProvince_Id = objSingleTransactinCashIn.FK_MsProvince_Id.GetValueOrDefault
                    objKas.TransactionCashIn.NomorRekening = objSingleTransactinCashIn.NomorRekening
                    objKas.TransactionCashIn.TipeTerlapor = objSingleTransactinCashIn.TipeTerlapor
                    objKas.TransactionCashIn.INDV_Gelar = objSingleTransactinCashIn.INDV_Gelar
                    objKas.TransactionCashIn.INDV_NamaLengkap = objSingleTransactinCashIn.INDV_NamaLengkap
                    objKas.TransactionCashIn.INDV_TempatLahir = objSingleTransactinCashIn.INDV_TempatLahir
                    objKas.TransactionCashIn.INDV_TanggalLahir = objSingleTransactinCashIn.INDV_TanggalLahir
                    objKas.TransactionCashIn.INDV_Kewarganegaraan = objSingleTransactinCashIn.INDV_Kewarganegaraan
                    objKas.TransactionCashIn.INDV_FK_MsNegara_Id = objSingleTransactinCashIn.INDV_FK_MsNegara_Id.GetValueOrDefault
                    objKas.TransactionCashIn.INDV_DOM_NamaJalan = objSingleTransactinCashIn.INDV_DOM_NamaJalan
                    objKas.TransactionCashIn.INDV_DOM_RTRW = objSingleTransactinCashIn.INDV_DOM_RTRW
                    objKas.TransactionCashIn.INDV_DOM_FK_MsKelurahan_Id = objSingleTransactinCashIn.INDV_DOM_FK_MsKelurahan_Id.GetValueOrDefault
                    objKas.TransactionCashIn.INDV_DOM_FK_MsKecamatan_Id = objSingleTransactinCashIn.INDV_DOM_FK_MsKecamatan_Id.GetValueOrDefault
                    objKas.TransactionCashIn.INDV_DOM_FK_MsKotaKab_Id = objSingleTransactinCashIn.INDV_DOM_FK_MsKotaKab_Id.GetValueOrDefault
                    objKas.TransactionCashIn.INDV_DOM_KodePos = objSingleTransactinCashIn.INDV_DOM_KodePos
                    objKas.TransactionCashIn.INDV_DOM_FK_MsProvince_Id = objSingleTransactinCashIn.INDV_DOM_FK_MsProvince_Id.GetValueOrDefault
                    objKas.TransactionCashIn.INDV_DOM_FK_MsNegara_Id = objSingleTransactinCashIn.INDV_DOM_FK_MsNegara_Id.GetValueOrDefault

                    objKas.TransactionCashIn.INDV_ID_NamaJalan = objSingleTransactinCashIn.INDV_ID_NamaJalan
                    objKas.TransactionCashIn.INDV_ID_RTRW = objSingleTransactinCashIn.INDV_ID_RTRW
                    objKas.TransactionCashIn.INDV_ID_FK_MsKelurahan_Id = objSingleTransactinCashIn.INDV_ID_FK_MsKelurahan_Id.GetValueOrDefault
                    objKas.TransactionCashIn.INDV_ID_FK_MsKecamatan_Id = objSingleTransactinCashIn.INDV_ID_FK_MsKecamatan_Id.GetValueOrDefault
                    objKas.TransactionCashIn.INDV_ID_FK_MsKotaKab_Id = objSingleTransactinCashIn.INDV_ID_FK_MsKotaKab_Id.GetValueOrDefault
                    objKas.TransactionCashIn.INDV_ID_KodePos = objSingleTransactinCashIn.INDV_ID_KodePos
                    objKas.TransactionCashIn.INDV_ID_FK_MsProvince_Id = objSingleTransactinCashIn.INDV_ID_FK_MsProvince_Id.GetValueOrDefault
                    objKas.TransactionCashIn.INDV_ID_FK_MsNegara_Id = objSingleTransactinCashIn.INDV_ID_FK_MsNegara_Id.GetValueOrDefault

                    objKas.TransactionCashIn.INDV_NA_NamaJalan = objSingleTransactinCashIn.INDV_NA_NamaJalan
                    objKas.TransactionCashIn.INDV_NA_FK_MsNegara_Id = objSingleTransactinCashIn.INDV_NA_FK_MsNegara_Id.GetValueOrDefault
                    objKas.TransactionCashIn.INDV_NA_FK_MsProvince_Id = objSingleTransactinCashIn.INDV_NA_FK_MsProvince_Id.GetValueOrDefault
                    objKas.TransactionCashIn.INDV_NA_FK_MsKotaKab_Id = objSingleTransactinCashIn.INDV_NA_FK_MsKotaKab_Id.GetValueOrDefault
                    objKas.TransactionCashIn.INDV_NA_KodePos = objSingleTransactinCashIn.INDV_NA_KodePos
                    objKas.TransactionCashIn.INDV_FK_MsIDType_Id = objSingleTransactinCashIn.INDV_FK_MsIDType_Id.GetValueOrDefault
                    objKas.TransactionCashIn.INDV_NomorId = objSingleTransactinCashIn.INDV_NomorId
                    objKas.TransactionCashIn.INDV_NPWP = objSingleTransactinCashIn.INDV_NPWP
                    objKas.TransactionCashIn.INDV_FK_MsPekerjaan_Id = objSingleTransactinCashIn.INDV_FK_MsPekerjaan_Id.GetValueOrDefault
                    objKas.TransactionCashIn.INDV_Jabatan = objSingleTransactinCashIn.INDV_Jabatan
                    objKas.TransactionCashIn.INDV_PenghasilanRataRata = objSingleTransactinCashIn.INDV_PenghasilanRataRata
                    objKas.TransactionCashIn.INDV_TempatBekerja = objSingleTransactinCashIn.INDV_TempatBekerja
                    objKas.TransactionCashIn.INDV_TujuanTransaksi = objSingleTransactinCashIn.INDV_TujuanTransaksi
                    objKas.TransactionCashIn.INDV_SumberDana = objSingleTransactinCashIn.INDV_SumberDana
                    objKas.TransactionCashIn.INDV_NamaBankLain = objSingleTransactinCashIn.INDV_NamaBankLain
                    objKas.TransactionCashIn.INDV_NomorRekeningTujuan = objSingleTransactinCashIn.INDV_NomorRekeningTujuan
                    objKas.TransactionCashIn.CORP_FK_MsBentukBadanUsaha_Id = objSingleTransactinCashIn.CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault
                    objKas.TransactionCashIn.CORP_Nama = objSingleTransactinCashIn.CORP_Nama
                    objKas.TransactionCashIn.CORP_FK_MsBidangUsaha_Id = objSingleTransactinCashIn.CORP_FK_MsBidangUsaha_Id.GetValueOrDefault
                    objKas.TransactionCashIn.CORP_TipeAlamat = objSingleTransactinCashIn.CORP_TipeAlamat.GetValueOrDefault
                    objKas.TransactionCashIn.CORP_NamaJalan = objSingleTransactinCashIn.CORP_NamaJalan
                    objKas.TransactionCashIn.CORP_RTRW = objSingleTransactinCashIn.CORP_RTRW
                    objKas.TransactionCashIn.CORP_FK_MsKelurahan_Id = objSingleTransactinCashIn.CORP_FK_MsKelurahan_Id.GetValueOrDefault
                    objKas.TransactionCashIn.CORP_FK_MsKecamatan_Id = objSingleTransactinCashIn.CORP_FK_MsKecamatan_Id.GetValueOrDefault
                    objKas.TransactionCashIn.CORP_FK_MsKotaKab_Id = objSingleTransactinCashIn.CORP_FK_MsKotaKab_Id.GetValueOrDefault
                    objKas.TransactionCashIn.CORP_KodePos = objSingleTransactinCashIn.CORP_KodePos
                    objKas.TransactionCashIn.CORP_FK_MsProvince_Id = objSingleTransactinCashIn.CORP_FK_MsProvince_Id.GetValueOrDefault
                    objKas.TransactionCashIn.CORP_FK_MsNegara_Id = objSingleTransactinCashIn.CORP_FK_MsNegara_Id.GetValueOrDefault
                    objKas.TransactionCashIn.CORP_LN_NamaJalan = objSingleTransactinCashIn.CORP_LN_NamaJalan
                    objKas.TransactionCashIn.CORP_LN_FK_MsNegara_Id = objSingleTransactinCashIn.CORP_LN_FK_MsNegara_Id.GetValueOrDefault
                    objKas.TransactionCashIn.CORP_LN_FK_MsProvince_Id = objSingleTransactinCashIn.CORP_LN_FK_MsProvince_Id.GetValueOrDefault
                    objKas.TransactionCashIn.CORP_LN_MsKotaKab_Id = objSingleTransactinCashIn.CORP_LN_MsKotaKab_Id.GetValueOrDefault
                    objKas.TransactionCashIn.CORP_LN_KodePos = objSingleTransactinCashIn.CORP_LN_KodePos
                    objKas.TransactionCashIn.CORP_NPWP = objSingleTransactinCashIn.CORP_NPWP
                    objKas.TransactionCashIn.CORP_TujuanTransaksi = objSingleTransactinCashIn.CORP_TujuanTransaksi
                    objKas.TransactionCashIn.CORP_SumberDana = objSingleTransactinCashIn.CORP_SumberDana
                    objKas.TransactionCashIn.CORP_NamaBankLain = objSingleTransactinCashIn.CORP_NamaBankLain
                    objKas.TransactionCashIn.CORP_NomorRekeningTujuan = objSingleTransactinCashIn.CORP_NomorRekeningTujuan

                    'Insert Data Ke Detail Transaction
                    objKas.DetailTransactionCashIn = New List(Of LTKTDetailCashInTransaction)

                    Dim objListDetailTransactionCashIn As TList(Of LTKTDetailCashInTransaction) = DataRepository.LTKTDetailCashInTransactionProvider.GetPaged(LTKTDetailCashInTransactionColumn.FK_LTKTTransactionCashIn_Id.ToString & " = '" & objSingleTransactinCashIn.PK_LTKTTransactionCashIn_Id.ToString & "'", "", 0, Integer.MaxValue, 0)
                    For Each objSingleDetailTransactionCashIn As LTKTDetailCashInTransaction In objListDetailTransactionCashIn
                        Dim objCalonInsert As New LTKTDetailCashInTransaction
                        objCalonInsert.PK_LTKTDetailCashInTransaction_Id = objSingleDetailTransactionCashIn.PK_LTKTDetailCashInTransaction_Id
                        objCalonInsert.Asing_KursTransaksi = objSingleDetailTransactionCashIn.Asing_KursTransaksi
                        objCalonInsert.Asing_TotalKasMasukDalamRupiah = objSingleDetailTransactionCashIn.Asing_TotalKasMasukDalamRupiah
                        objCalonInsert.TotalKasMasuk = objSingleDetailTransactionCashIn.TotalKasMasuk
                        objCalonInsert.KasMasuk = objSingleDetailTransactionCashIn.KasMasuk
                        objCalonInsert.Asing_FK_MsCurrency_Id = objSingleDetailTransactionCashIn.Asing_FK_MsCurrency_Id

                        objKas.DetailTransactionCashIn.Add(objCalonInsert)
                    Next

                    objResume.Add(objKas)

                Next
            End Using

            Using objListTransactionCashOut As TList(Of LTKTTransactionCashOut) = DataRepository.LTKTTransactionCashOutProvider.GetPaged(LTKTTransactionCashOutColumn.FK_LTKT_Id.ToString & " = " & objApprovalDetail(0).PK_LTKT_Id.ToString, "", 0, Integer.MaxValue, 0)
                For Each objSingleTransactinCashOut As LTKTTransactionCashOut In objListTransactionCashOut


                    Dim objKas As ResumeKasMasukKeluarLTKT = New ResumeKasMasukKeluarLTKT
                    'Insert untuk tampilan Grid
                    objKas.TransactionDate = objSingleTransactinCashOut.TanggalTransaksi
                    objKas.Branch = objSingleTransactinCashOut.NamaKantorPJK
                    objKas.TransactionNominal = objSingleTransactinCashOut.Total.ToString
                    objKas.AccountNumber = objSingleTransactinCashOut.NomorRekening
                    objKas.Kas = "Kas Keluar"
                    objKas.Type = "LTKT"

                    'Insert Data Ke ObjectTransaction
                    objKas.TransactionCashOut = New LTKTTransactionCashOut
                    objKas.TransactionCashOut.PK_LTKTTransactionCashOut_Id = objSingleTransactinCashOut.PK_LTKTTransactionCashOut_Id

                    objKas.TransactionCashOut.TanggalTransaksi = objSingleTransactinCashOut.TanggalTransaksi.GetValueOrDefault
                    objKas.TransactionCashOut.NamaKantorPJK = objSingleTransactinCashOut.NamaKantorPJK
                    objKas.TransactionCashOut.FK_MsKotaKab_Id = objSingleTransactinCashOut.FK_MsKotaKab_Id.GetValueOrDefault
                    objKas.TransactionCashOut.FK_MsProvince_Id = objSingleTransactinCashOut.FK_MsProvince_Id.GetValueOrDefault
                    objKas.TransactionCashOut.NomorRekening = objSingleTransactinCashOut.NomorRekening
                    objKas.TransactionCashOut.TipeTerlapor = objSingleTransactinCashOut.TipeTerlapor
                    objKas.TransactionCashOut.INDV_Gelar = objSingleTransactinCashOut.INDV_Gelar
                    objKas.TransactionCashOut.INDV_NamaLengkap = objSingleTransactinCashOut.INDV_NamaLengkap
                    objKas.TransactionCashOut.INDV_TempatLahir = objSingleTransactinCashOut.INDV_TempatLahir
                    objKas.TransactionCashOut.INDV_TanggalLahir = objSingleTransactinCashOut.INDV_TanggalLahir
                    objKas.TransactionCashOut.INDV_Kewarganegaraan = objSingleTransactinCashOut.INDV_Kewarganegaraan
                    objKas.TransactionCashOut.INDV_FK_MsNegara_Id = objSingleTransactinCashOut.INDV_FK_MsNegara_Id.GetValueOrDefault
                    objKas.TransactionCashOut.INDV_DOM_NamaJalan = objSingleTransactinCashOut.INDV_DOM_NamaJalan
                    objKas.TransactionCashOut.INDV_DOM_RTRW = objSingleTransactinCashOut.INDV_DOM_RTRW
                    objKas.TransactionCashOut.INDV_DOM_FK_MsKelurahan_Id = objSingleTransactinCashOut.INDV_DOM_FK_MsKelurahan_Id.GetValueOrDefault
                    objKas.TransactionCashOut.INDV_DOM_FK_MsKecamatan_Id = objSingleTransactinCashOut.INDV_DOM_FK_MsKecamatan_Id.GetValueOrDefault
                    objKas.TransactionCashOut.INDV_DOM_FK_MsKotaKab_Id = objSingleTransactinCashOut.INDV_DOM_FK_MsKotaKab_Id.GetValueOrDefault
                    objKas.TransactionCashOut.INDV_DOM_KodePos = objSingleTransactinCashOut.INDV_DOM_KodePos
                    objKas.TransactionCashOut.INDV_DOM_FK_MsProvince_Id = objSingleTransactinCashOut.INDV_DOM_FK_MsProvince_Id.GetValueOrDefault
                    objKas.TransactionCashOut.INDV_DOM_FK_MsNegara_Id = objSingleTransactinCashOut.INDV_DOM_FK_MsNegara_Id.GetValueOrDefault

                    objKas.TransactionCashOut.INDV_ID_NamaJalan = objSingleTransactinCashOut.INDV_ID_NamaJalan
                    objKas.TransactionCashOut.INDV_ID_RTRW = objSingleTransactinCashOut.INDV_ID_RTRW
                    objKas.TransactionCashOut.INDV_ID_FK_MsKelurahan_Id = objSingleTransactinCashOut.INDV_ID_FK_MsKelurahan_Id.GetValueOrDefault
                    objKas.TransactionCashOut.INDV_ID_FK_MsKecamatan_Id = objSingleTransactinCashOut.INDV_ID_FK_MsKecamatan_Id.GetValueOrDefault
                    objKas.TransactionCashOut.INDV_ID_FK_MsKotaKab_Id = objSingleTransactinCashOut.INDV_ID_FK_MsKotaKab_Id.GetValueOrDefault
                    objKas.TransactionCashOut.INDV_ID_KodePos = objSingleTransactinCashOut.INDV_ID_KodePos
                    objKas.TransactionCashOut.INDV_ID_FK_MsProvince_Id = objSingleTransactinCashOut.INDV_ID_FK_MsProvince_Id.GetValueOrDefault
                    objKas.TransactionCashOut.INDV_ID_FK_MsNegara_Id = objSingleTransactinCashOut.INDV_ID_FK_MsNegara_Id.GetValueOrDefault

                    objKas.TransactionCashOut.INDV_NA_NamaJalan = objSingleTransactinCashOut.INDV_NA_NamaJalan
                    objKas.TransactionCashOut.INDV_NA_FK_MsNegara_Id = objSingleTransactinCashOut.INDV_NA_FK_MsNegara_Id.GetValueOrDefault
                    objKas.TransactionCashOut.INDV_NA_FK_MsProvince_Id = objSingleTransactinCashOut.INDV_NA_FK_MsProvince_Id.GetValueOrDefault
                    objKas.TransactionCashOut.INDV_NA_FK_MsKotaKab_Id = objSingleTransactinCashOut.INDV_NA_FK_MsKotaKab_Id.GetValueOrDefault
                    objKas.TransactionCashOut.INDV_NA_KodePos = objSingleTransactinCashOut.INDV_NA_KodePos
                    objKas.TransactionCashOut.INDV_FK_MsIDType_Id = objSingleTransactinCashOut.INDV_FK_MsIDType_Id.GetValueOrDefault
                    objKas.TransactionCashOut.INDV_NomorId = objSingleTransactinCashOut.INDV_NomorId
                    objKas.TransactionCashOut.INDV_NPWP = objSingleTransactinCashOut.INDV_NPWP
                    objKas.TransactionCashOut.INDV_FK_MsPekerjaan_Id = objSingleTransactinCashOut.INDV_FK_MsPekerjaan_Id.GetValueOrDefault
                    objKas.TransactionCashOut.INDV_Jabatan = objSingleTransactinCashOut.INDV_Jabatan
                    objKas.TransactionCashOut.INDV_PenghasilanRataRata = objSingleTransactinCashOut.INDV_PenghasilanRataRata
                    objKas.TransactionCashOut.INDV_TempatBekerja = objSingleTransactinCashOut.INDV_TempatBekerja
                    objKas.TransactionCashOut.INDV_TujuanTransaksi = objSingleTransactinCashOut.INDV_TujuanTransaksi
                    objKas.TransactionCashOut.INDV_SumberDana = objSingleTransactinCashOut.INDV_SumberDana
                    objKas.TransactionCashOut.INDV_NamaBankLain = objSingleTransactinCashOut.INDV_NamaBankLain
                    objKas.TransactionCashOut.INDV_NomorRekeningTujuan = objSingleTransactinCashOut.INDV_NomorRekeningTujuan
                    objKas.TransactionCashOut.CORP_FK_MsBentukBadanUsaha_Id = objSingleTransactinCashOut.CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault
                    objKas.TransactionCashOut.CORP_Nama = objSingleTransactinCashOut.CORP_Nama
                    objKas.TransactionCashOut.CORP_FK_MsBidangUsaha_Id = objSingleTransactinCashOut.CORP_FK_MsBidangUsaha_Id.GetValueOrDefault
                    objKas.TransactionCashOut.CORP_TipeAlamat = objSingleTransactinCashOut.CORP_TipeAlamat.GetValueOrDefault
                    objKas.TransactionCashOut.CORP_NamaJalan = objSingleTransactinCashOut.CORP_NamaJalan
                    objKas.TransactionCashOut.CORP_RTRW = objSingleTransactinCashOut.CORP_RTRW
                    objKas.TransactionCashOut.CORP_FK_MsKelurahan_Id = objSingleTransactinCashOut.CORP_FK_MsKelurahan_Id.GetValueOrDefault
                    objKas.TransactionCashOut.CORP_FK_MsKecamatan_Id = objSingleTransactinCashOut.CORP_FK_MsKecamatan_Id.GetValueOrDefault
                    objKas.TransactionCashOut.CORP_FK_MsKotaKab_Id = objSingleTransactinCashOut.CORP_FK_MsKotaKab_Id.GetValueOrDefault
                    objKas.TransactionCashOut.CORP_KodePos = objSingleTransactinCashOut.CORP_KodePos
                    objKas.TransactionCashOut.CORP_FK_MsProvince_Id = objSingleTransactinCashOut.CORP_FK_MsProvince_Id.GetValueOrDefault
                    objKas.TransactionCashOut.CORP_FK_MsNegara_Id = objSingleTransactinCashOut.CORP_FK_MsNegara_Id.GetValueOrDefault
                    objKas.TransactionCashOut.CORP_LN_NamaJalan = objSingleTransactinCashOut.CORP_LN_NamaJalan
                    objKas.TransactionCashOut.CORP_LN_FK_MsNegara_Id = objSingleTransactinCashOut.CORP_LN_FK_MsNegara_Id.GetValueOrDefault
                    objKas.TransactionCashOut.CORP_LN_FK_MsProvince_Id = objSingleTransactinCashOut.CORP_LN_FK_MsProvince_Id.GetValueOrDefault
                    objKas.TransactionCashOut.CORP_LN_MsKotaKab_Id = objSingleTransactinCashOut.CORP_LN_MsKotaKab_Id.GetValueOrDefault
                    objKas.TransactionCashOut.CORP_LN_KodePos = objSingleTransactinCashOut.CORP_LN_KodePos
                    objKas.TransactionCashOut.CORP_NPWP = objSingleTransactinCashOut.CORP_NPWP
                    objKas.TransactionCashOut.CORP_TujuanTransaksi = objSingleTransactinCashOut.CORP_TujuanTransaksi
                    objKas.TransactionCashOut.CORP_SumberDana = objSingleTransactinCashOut.CORP_SumberDana
                    objKas.TransactionCashOut.CORP_NamaBankLain = objSingleTransactinCashOut.CORP_NamaBankLain
                    objKas.TransactionCashOut.CORP_NomorRekeningTujuan = objSingleTransactinCashOut.CORP_NomorRekeningTujuan


                    'Insert Data Ke Detail Transaction
                    objKas.DetailTranscationCashOut = New List(Of LTKTDetailCashOutTransaction)

                    Dim objListDetailTransactionCashOut As TList(Of LTKTDetailCashOutTransaction) = DataRepository.LTKTDetailCashOutTransactionProvider.GetPaged(LTKTDetailCashOutTransactionColumn.FK_LTKTTransactionCashOut_Id.ToString & " = '" & objSingleTransactinCashOut.PK_LTKTTransactionCashOut_Id.ToString & "'", "", 0, Integer.MaxValue, 0)
                    For Each objSingleDetailTransactionCashOut As LTKTDetailCashOutTransaction In objListDetailTransactionCashOut
                        Dim objCalonInsert As New LTKTDetailCashOutTransaction
                        objCalonInsert.PK_LTKTDetailCashOutTransaction_Id = objSingleDetailTransactionCashOut.PK_LTKTDetailCashOutTransaction_Id
                        objCalonInsert.Asing_KursTransaksi = objSingleDetailTransactionCashOut.Asing_KursTransaksi
                        objCalonInsert.Asing_TotalKasKeluarDalamRupiah = objSingleDetailTransactionCashOut.Asing_TotalKasKeluarDalamRupiah
                        objCalonInsert.TotalKasKeluar = objSingleDetailTransactionCashOut.TotalKasKeluar
                        objCalonInsert.KasKeluar = objSingleDetailTransactionCashOut.KasKeluar
                        objCalonInsert.Asing_FK_MsCurrency_Id = objSingleDetailTransactionCashOut.Asing_FK_MsCurrency_Id

                        objKas.DetailTranscationCashOut.Add(objCalonInsert)
                    Next

                    objResume.Add(objKas)

                Next
            End Using
            SetnGetResumeKasMasukKasKeluarOLD = objResume
            old_grvTransaksi.DataSource = objResume
            old_grvTransaksi.DataBind()

            Dim totalKasMasuk As Decimal = 0
            Dim totalKasKeluar As Decimal = 0
            For Each kas As ResumeKasMasukKeluarLTKT In objResume
                If kas.Kas = "Kas Masuk" Then
                    totalKasMasuk += CDec(kas.TransactionNominal)
                Else
                    totalKasKeluar += CDec(kas.TransactionNominal)
                End If
            Next

            old_lblTotalKasMasuk.Text = ValidateBLL.FormatMoneyWithComma(totalKasMasuk)
            old_lblTotalKasKeluar.Text = ValidateBLL.FormatMoneyWithComma(totalKasKeluar)
        End If

    End Sub

    Sub loadResume()
        Dim objResume As List(Of ResumeKasMasukKeluarLTKT) = SetnGetResumeKasMasukKasKeluar

        Dim objApprovaldetail As TList(Of LTKT_ApprovalDetail) = DataRepository.LTKT_ApprovalDetailProvider.GetPaged(LTKT_ApprovalDetailColumn.FK_LTKT_Approval_Id.ToString & " = " & getAPPROVALPK.ToString, "", 0, Integer.MaxValue, 0)
        If objApprovaldetail.Count > 0 Then

            'load Kas Masuk ke objResume
            Using objListTransactionCashIn As TList(Of LTKTTransactionCashIn_ApprovalDetail) = DataRepository.LTKTTransactionCashIn_ApprovalDetailProvider.GetPaged(LTKTTransactionCashIn_ApprovalDetailColumn.FK_LTKT_ApprovalDetail_Id.ToString & " = " & objApprovaldetail(0).PK_LTKT_ApprovalDetail_Id.ToString, "", 0, Integer.MaxValue, 0)
                For Each objSingleTransactinCashIn As LTKTTransactionCashIn_ApprovalDetail In objListTransactionCashIn

                    Dim objKas As ResumeKasMasukKeluarLTKT = New ResumeKasMasukKeluarLTKT
                    'Insert untuk tampilan Grid
                    objKas.TransactionDate = objSingleTransactinCashIn.TanggalTransaksi
                    objKas.Branch = objSingleTransactinCashIn.NamaKantorPJK
                    objKas.TransactionNominal = objSingleTransactinCashIn.Total.ToString
                    objKas.AccountNumber = objSingleTransactinCashIn.NomorRekening
                    objKas.Kas = "Kas Masuk"
                    objKas.Type = "LTKT"

                    'Insert Data Ke ObjectTransaction
                    objKas.TransactionCashIn = New LTKTTransactionCashIn
                    objKas.TransactionCashIn.PK_LTKTTransactionCashIn_Id = objSingleTransactinCashIn.PK_LTKTTransactionCashIn_Id
                    objKas.TransactionCashIn.TanggalTransaksi = objSingleTransactinCashIn.TanggalTransaksi.GetValueOrDefault
                    objKas.TransactionCashIn.NamaKantorPJK = objSingleTransactinCashIn.NamaKantorPJK
                    objKas.TransactionCashIn.FK_MsKotaKab_Id = objSingleTransactinCashIn.FK_MsKotaKab_Id.GetValueOrDefault
                    objKas.TransactionCashIn.FK_MsProvince_Id = objSingleTransactinCashIn.FK_MsProvince_Id.GetValueOrDefault
                    objKas.TransactionCashIn.NomorRekening = objSingleTransactinCashIn.NomorRekening
                    objKas.TransactionCashIn.TipeTerlapor = objSingleTransactinCashIn.TipeTerlapor
                    objKas.TransactionCashIn.INDV_Gelar = objSingleTransactinCashIn.INDV_Gelar
                    objKas.TransactionCashIn.INDV_NamaLengkap = objSingleTransactinCashIn.INDV_NamaLengkap
                    objKas.TransactionCashIn.INDV_TempatLahir = objSingleTransactinCashIn.INDV_TempatLahir
                    objKas.TransactionCashIn.INDV_TanggalLahir = objSingleTransactinCashIn.INDV_TanggalLahir
                    objKas.TransactionCashIn.INDV_Kewarganegaraan = objSingleTransactinCashIn.INDV_Kewarganegaraan
                    objKas.TransactionCashIn.INDV_FK_MsNegara_Id = objSingleTransactinCashIn.INDV_FK_MsNegara_Id.GetValueOrDefault

                    objKas.TransactionCashIn.INDV_FK_MsIDType_Id = objSingleTransactinCashIn.INDV_FK_MsIDType_Id.GetValueOrDefault
                    objKas.TransactionCashIn.INDV_NomorId = objSingleTransactinCashIn.INDV_NomorId
                    objKas.TransactionCashIn.INDV_NPWP = objSingleTransactinCashIn.INDV_NPWP
                    objKas.TransactionCashIn.INDV_FK_MsPekerjaan_Id = objSingleTransactinCashIn.INDV_FK_MsPekerjaan_Id.GetValueOrDefault
                    objKas.TransactionCashIn.INDV_Jabatan = objSingleTransactinCashIn.INDV_Jabatan
                    objKas.TransactionCashIn.INDV_PenghasilanRataRata = objSingleTransactinCashIn.INDV_PenghasilanRataRata
                    objKas.TransactionCashIn.INDV_TempatBekerja = objSingleTransactinCashIn.INDV_TempatBekerja
                    objKas.TransactionCashIn.INDV_TujuanTransaksi = objSingleTransactinCashIn.INDV_TujuanTransaksi
                    objKas.TransactionCashIn.INDV_SumberDana = objSingleTransactinCashIn.INDV_SumberDana
                    objKas.TransactionCashIn.INDV_NamaBankLain = objSingleTransactinCashIn.INDV_NamaBankLain
                    objKas.TransactionCashIn.INDV_NomorRekeningTujuan = objSingleTransactinCashIn.INDV_NomorRekeningTujuan
                    objKas.TransactionCashIn.INDV_DOM_NamaJalan = objSingleTransactinCashIn.INDV_DOM_NamaJalan
                    objKas.TransactionCashIn.INDV_DOM_RTRW = objSingleTransactinCashIn.INDV_DOM_RTRW
                    objKas.TransactionCashIn.INDV_DOM_FK_MsKelurahan_Id = objSingleTransactinCashIn.INDV_DOM_FK_MsKelurahan_Id.GetValueOrDefault
                    objKas.TransactionCashIn.INDV_DOM_FK_MsKecamatan_Id = objSingleTransactinCashIn.INDV_DOM_FK_MsKecamatan_Id.GetValueOrDefault
                    objKas.TransactionCashIn.INDV_DOM_FK_MsKotaKab_Id = objSingleTransactinCashIn.INDV_DOM_FK_MsKotaKab_Id.GetValueOrDefault
                    objKas.TransactionCashIn.INDV_DOM_KodePos = objSingleTransactinCashIn.INDV_DOM_KodePos
                    objKas.TransactionCashIn.INDV_DOM_FK_MsProvince_Id = objSingleTransactinCashIn.INDV_DOM_FK_MsProvince_Id.GetValueOrDefault
                    objKas.TransactionCashIn.INDV_DOM_FK_MsNegara_Id = objSingleTransactinCashIn.INDV_DOM_FK_MsNegara_Id.GetValueOrDefault

                    objKas.TransactionCashIn.INDV_ID_NamaJalan = objSingleTransactinCashIn.INDV_ID_NamaJalan
                    objKas.TransactionCashIn.INDV_ID_RTRW = objSingleTransactinCashIn.INDV_ID_RTRW
                    objKas.TransactionCashIn.INDV_ID_FK_MsKelurahan_Id = objSingleTransactinCashIn.INDV_ID_FK_MsKelurahan_Id.GetValueOrDefault
                    objKas.TransactionCashIn.INDV_ID_FK_MsKecamatan_Id = objSingleTransactinCashIn.INDV_ID_FK_MsKecamatan_Id.GetValueOrDefault
                    objKas.TransactionCashIn.INDV_ID_FK_MsKotaKab_Id = objSingleTransactinCashIn.INDV_ID_FK_MsKotaKab_Id.GetValueOrDefault
                    objKas.TransactionCashIn.INDV_ID_KodePos = objSingleTransactinCashIn.INDV_ID_KodePos
                    objKas.TransactionCashIn.INDV_ID_FK_MsProvince_Id = objSingleTransactinCashIn.INDV_ID_FK_MsProvince_Id.GetValueOrDefault
                    objKas.TransactionCashIn.INDV_ID_FK_MsNegara_Id = objSingleTransactinCashIn.INDV_ID_FK_MsNegara_Id.GetValueOrDefault

                    objKas.TransactionCashIn.INDV_NA_NamaJalan = objSingleTransactinCashIn.INDV_NA_NamaJalan
                    objKas.TransactionCashIn.INDV_NA_FK_MsNegara_Id = objSingleTransactinCashIn.INDV_NA_FK_MsNegara_Id.GetValueOrDefault
                    objKas.TransactionCashIn.INDV_NA_FK_MsProvince_Id = objSingleTransactinCashIn.INDV_NA_FK_MsProvince_Id.GetValueOrDefault
                    objKas.TransactionCashIn.INDV_NA_FK_MsKotaKab_Id = objSingleTransactinCashIn.INDV_NA_FK_MsKotaKab_Id.GetValueOrDefault
                    objKas.TransactionCashIn.INDV_NA_KodePos = objSingleTransactinCashIn.INDV_NA_KodePos

                    objKas.TransactionCashIn.CORP_FK_MsBentukBadanUsaha_Id = objSingleTransactinCashIn.CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault
                    objKas.TransactionCashIn.CORP_Nama = objSingleTransactinCashIn.CORP_Nama
                    objKas.TransactionCashIn.CORP_FK_MsBidangUsaha_Id = objSingleTransactinCashIn.CORP_FK_MsBidangUsaha_Id.GetValueOrDefault
                    objKas.TransactionCashIn.CORP_TipeAlamat = objSingleTransactinCashIn.CORP_TipeAlamat.GetValueOrDefault
                    objKas.TransactionCashIn.CORP_NamaJalan = objSingleTransactinCashIn.CORP_NamaJalan
                    objKas.TransactionCashIn.CORP_RTRW = objSingleTransactinCashIn.CORP_RTRW
                    objKas.TransactionCashIn.CORP_FK_MsKelurahan_Id = objSingleTransactinCashIn.CORP_FK_MsKelurahan_Id.GetValueOrDefault
                    objKas.TransactionCashIn.CORP_FK_MsKecamatan_Id = objSingleTransactinCashIn.CORP_FK_MsKecamatan_Id.GetValueOrDefault
                    objKas.TransactionCashIn.CORP_FK_MsKotaKab_Id = objSingleTransactinCashIn.CORP_FK_MsKotaKab_Id.GetValueOrDefault
                    objKas.TransactionCashIn.CORP_KodePos = objSingleTransactinCashIn.CORP_KodePos
                    objKas.TransactionCashIn.CORP_FK_MsProvince_Id = objSingleTransactinCashIn.CORP_FK_MsProvince_Id.GetValueOrDefault
                    objKas.TransactionCashIn.CORP_FK_MsNegara_Id = objSingleTransactinCashIn.CORP_FK_MsNegara_Id.GetValueOrDefault
                    objKas.TransactionCashIn.CORP_LN_NamaJalan = objSingleTransactinCashIn.CORP_LN_NamaJalan
                    objKas.TransactionCashIn.CORP_LN_FK_MsNegara_Id = objSingleTransactinCashIn.CORP_LN_FK_MsNegara_Id.GetValueOrDefault
                    objKas.TransactionCashIn.CORP_LN_FK_MsProvince_Id = objSingleTransactinCashIn.CORP_LN_FK_MsProvince_Id.GetValueOrDefault
                    objKas.TransactionCashIn.CORP_LN_MsKotaKab_Id = objSingleTransactinCashIn.CORP_LN_MsKotaKab_Id.GetValueOrDefault
                    objKas.TransactionCashIn.CORP_LN_KodePos = objSingleTransactinCashIn.CORP_LN_KodePos
                    objKas.TransactionCashIn.CORP_NPWP = objSingleTransactinCashIn.CORP_NPWP
                    objKas.TransactionCashIn.CORP_TujuanTransaksi = objSingleTransactinCashIn.CORP_TujuanTransaksi
                    objKas.TransactionCashIn.CORP_SumberDana = objSingleTransactinCashIn.CORP_SumberDana
                    objKas.TransactionCashIn.CORP_NamaBankLain = objSingleTransactinCashIn.CORP_NamaBankLain
                    objKas.TransactionCashIn.CORP_NomorRekeningTujuan = objSingleTransactinCashIn.CORP_NomorRekeningTujuan

                    'Insert Data Ke Detail Transaction
                    objKas.DetailTransactionCashIn = New List(Of LTKTDetailCashInTransaction)

                    Dim objListDetailTransactionCashIn As TList(Of LTKTDetailCashInTransaction_ApprovalDetail) = DataRepository.LTKTDetailCashInTransaction_ApprovalDetailProvider.GetPaged(LTKTDetailCashInTransaction_ApprovalDetailColumn.FK_LTKTTransactionCashIn_ApprovalDetail_Id.ToString & " = '" & objSingleTransactinCashIn.PK_LTKTTransactionCashIn_ApprovalDetail_Id.ToString & "'", "", 0, Integer.MaxValue, 0)
                    For Each objSingleDetailTransactionCashIn As LTKTDetailCashInTransaction_ApprovalDetail In objListDetailTransactionCashIn
                        Dim objCalonInsert As New LTKTDetailCashInTransaction
                        objCalonInsert.PK_LTKTDetailCashInTransaction_Id = objSingleDetailTransactionCashIn.PK_LTKTDetailCashInTransaction_Id
                        objCalonInsert.Asing_KursTransaksi = objSingleDetailTransactionCashIn.Asing_KursTransaksi
                        objCalonInsert.Asing_TotalKasMasukDalamRupiah = objSingleDetailTransactionCashIn.Asing_TotalKasMasukDalamRupiah
                        objCalonInsert.TotalKasMasuk = objSingleDetailTransactionCashIn.TotalKasMasuk
                        objCalonInsert.KasMasuk = objSingleDetailTransactionCashIn.KasMasuk
                        objCalonInsert.Asing_FK_MsCurrency_Id = objSingleDetailTransactionCashIn.Asing_FK_MsCurrency_Id

                        objKas.DetailTransactionCashIn.Add(objCalonInsert)
                    Next

                    objResume.Add(objKas)

                Next
            End Using

            Using objListTransactionCashOut As TList(Of LTKTTransactionCashOut_ApprovalDetail) = DataRepository.LTKTTransactionCashOut_ApprovalDetailProvider.GetPaged(LTKTTransactionCashOut_ApprovalDetailColumn.FK_LTKT_ApprovalDetail_Id.ToString & " = " & objApprovaldetail(0).PK_LTKT_ApprovalDetail_Id.ToString, "", 0, Integer.MaxValue, 0)
                For Each objSingleTransactinCashOut As LTKTTransactionCashOut_ApprovalDetail In objListTransactionCashOut


                    Dim objKas As ResumeKasMasukKeluarLTKT = New ResumeKasMasukKeluarLTKT
                    'Insert untuk tampilan Grid
                    objKas.TransactionDate = objSingleTransactinCashOut.TanggalTransaksi
                    objKas.Branch = objSingleTransactinCashOut.NamaKantorPJK
                    objKas.TransactionNominal = objSingleTransactinCashOut.Total.ToString
                    objKas.AccountNumber = objSingleTransactinCashOut.NomorRekening
                    objKas.Kas = "Kas Keluar"
                    objKas.Type = "LTKT"

                    'Insert Data Ke ObjectTransaction
                    objKas.TransactionCashOut = New LTKTTransactionCashOut
                    objKas.TransactionCashOut.PK_LTKTTransactionCashOut_Id = objSingleTransactinCashOut.PK_LTKTTransactionCashOut_Id
                    objKas.TransactionCashOut.TanggalTransaksi = objSingleTransactinCashOut.TanggalTransaksi.GetValueOrDefault
                    objKas.TransactionCashOut.NamaKantorPJK = objSingleTransactinCashOut.NamaKantorPJK
                    objKas.TransactionCashOut.FK_MsKotaKab_Id = objSingleTransactinCashOut.FK_MsKotaKab_Id.GetValueOrDefault
                    objKas.TransactionCashOut.FK_MsProvince_Id = objSingleTransactinCashOut.FK_MsProvince_Id.GetValueOrDefault
                    objKas.TransactionCashOut.NomorRekening = objSingleTransactinCashOut.NomorRekening
                    objKas.TransactionCashOut.TipeTerlapor = objSingleTransactinCashOut.TipeTerlapor
                    objKas.TransactionCashOut.INDV_Gelar = objSingleTransactinCashOut.INDV_Gelar
                    objKas.TransactionCashOut.INDV_NamaLengkap = objSingleTransactinCashOut.INDV_NamaLengkap
                    objKas.TransactionCashOut.INDV_TempatLahir = objSingleTransactinCashOut.INDV_TempatLahir
                    objKas.TransactionCashOut.INDV_TanggalLahir = objSingleTransactinCashOut.INDV_TanggalLahir
                    objKas.TransactionCashOut.INDV_Kewarganegaraan = objSingleTransactinCashOut.INDV_Kewarganegaraan
                    objKas.TransactionCashOut.INDV_FK_MsNegara_Id = objSingleTransactinCashOut.INDV_FK_MsNegara_Id.GetValueOrDefault

                    objKas.TransactionCashOut.INDV_FK_MsIDType_Id = objSingleTransactinCashOut.INDV_FK_MsIDType_Id.GetValueOrDefault
                    objKas.TransactionCashOut.INDV_NomorId = objSingleTransactinCashOut.INDV_NomorId
                    objKas.TransactionCashOut.INDV_NPWP = objSingleTransactinCashOut.INDV_NPWP
                    objKas.TransactionCashOut.INDV_FK_MsPekerjaan_Id = objSingleTransactinCashOut.INDV_FK_MsPekerjaan_Id.GetValueOrDefault
                    objKas.TransactionCashOut.INDV_Jabatan = objSingleTransactinCashOut.INDV_Jabatan
                    objKas.TransactionCashOut.INDV_PenghasilanRataRata = objSingleTransactinCashOut.INDV_PenghasilanRataRata
                    objKas.TransactionCashOut.INDV_TempatBekerja = objSingleTransactinCashOut.INDV_TempatBekerja
                    objKas.TransactionCashOut.INDV_TujuanTransaksi = objSingleTransactinCashOut.INDV_TujuanTransaksi
                    objKas.TransactionCashOut.INDV_SumberDana = objSingleTransactinCashOut.INDV_SumberDana
                    objKas.TransactionCashOut.INDV_NamaBankLain = objSingleTransactinCashOut.INDV_NamaBankLain
                    objKas.TransactionCashOut.INDV_NomorRekeningTujuan = objSingleTransactinCashOut.INDV_NomorRekeningTujuan
                    objKas.TransactionCashOut.INDV_DOM_NamaJalan = objSingleTransactinCashOut.INDV_DOM_NamaJalan
                    objKas.TransactionCashOut.INDV_DOM_RTRW = objSingleTransactinCashOut.INDV_DOM_RTRW
                    objKas.TransactionCashOut.INDV_DOM_FK_MsKelurahan_Id = objSingleTransactinCashOut.INDV_DOM_FK_MsKelurahan_Id.GetValueOrDefault
                    objKas.TransactionCashOut.INDV_DOM_FK_MsKecamatan_Id = objSingleTransactinCashOut.INDV_DOM_FK_MsKecamatan_Id.GetValueOrDefault
                    objKas.TransactionCashOut.INDV_DOM_FK_MsKotaKab_Id = objSingleTransactinCashOut.INDV_DOM_FK_MsKotaKab_Id.GetValueOrDefault
                    objKas.TransactionCashOut.INDV_DOM_KodePos = objSingleTransactinCashOut.INDV_DOM_KodePos
                    objKas.TransactionCashOut.INDV_DOM_FK_MsProvince_Id = objSingleTransactinCashOut.INDV_DOM_FK_MsProvince_Id.GetValueOrDefault
                    objKas.TransactionCashOut.INDV_DOM_FK_MsNegara_Id = objSingleTransactinCashOut.INDV_DOM_FK_MsNegara_Id.GetValueOrDefault

                    objKas.TransactionCashOut.INDV_ID_NamaJalan = objSingleTransactinCashOut.INDV_ID_NamaJalan
                    objKas.TransactionCashOut.INDV_ID_RTRW = objSingleTransactinCashOut.INDV_ID_RTRW
                    objKas.TransactionCashOut.INDV_ID_FK_MsKelurahan_Id = objSingleTransactinCashOut.INDV_ID_FK_MsKelurahan_Id.GetValueOrDefault
                    objKas.TransactionCashOut.INDV_ID_FK_MsKecamatan_Id = objSingleTransactinCashOut.INDV_ID_FK_MsKecamatan_Id.GetValueOrDefault
                    objKas.TransactionCashOut.INDV_ID_FK_MsKotaKab_Id = objSingleTransactinCashOut.INDV_ID_FK_MsKotaKab_Id.GetValueOrDefault
                    objKas.TransactionCashOut.INDV_ID_KodePos = objSingleTransactinCashOut.INDV_ID_KodePos
                    objKas.TransactionCashOut.INDV_ID_FK_MsProvince_Id = objSingleTransactinCashOut.INDV_ID_FK_MsProvince_Id.GetValueOrDefault
                    objKas.TransactionCashOut.INDV_ID_FK_MsNegara_Id = objSingleTransactinCashOut.INDV_ID_FK_MsNegara_Id.GetValueOrDefault

                    objKas.TransactionCashOut.INDV_NA_NamaJalan = objSingleTransactinCashOut.INDV_NA_NamaJalan
                    objKas.TransactionCashOut.INDV_NA_FK_MsNegara_Id = objSingleTransactinCashOut.INDV_NA_FK_MsNegara_Id.GetValueOrDefault
                    objKas.TransactionCashOut.INDV_NA_FK_MsProvince_Id = objSingleTransactinCashOut.INDV_NA_FK_MsProvince_Id.GetValueOrDefault
                    objKas.TransactionCashOut.INDV_NA_FK_MsKotaKab_Id = objSingleTransactinCashOut.INDV_NA_FK_MsKotaKab_Id.GetValueOrDefault
                    objKas.TransactionCashOut.INDV_NA_KodePos = objSingleTransactinCashOut.INDV_NA_KodePos
                    objKas.TransactionCashOut.CORP_FK_MsBentukBadanUsaha_Id = objSingleTransactinCashOut.CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault
                    objKas.TransactionCashOut.CORP_Nama = objSingleTransactinCashOut.CORP_Nama
                    objKas.TransactionCashOut.CORP_FK_MsBidangUsaha_Id = objSingleTransactinCashOut.CORP_FK_MsBidangUsaha_Id.GetValueOrDefault
                    objKas.TransactionCashOut.CORP_TipeAlamat = objSingleTransactinCashOut.CORP_TipeAlamat.GetValueOrDefault
                    objKas.TransactionCashOut.CORP_NamaJalan = objSingleTransactinCashOut.CORP_NamaJalan
                    objKas.TransactionCashOut.CORP_RTRW = objSingleTransactinCashOut.CORP_RTRW
                    objKas.TransactionCashOut.CORP_FK_MsKelurahan_Id = objSingleTransactinCashOut.CORP_FK_MsKelurahan_Id.GetValueOrDefault
                    objKas.TransactionCashOut.CORP_FK_MsKecamatan_Id = objSingleTransactinCashOut.CORP_FK_MsKecamatan_Id.GetValueOrDefault
                    objKas.TransactionCashOut.CORP_FK_MsKotaKab_Id = objSingleTransactinCashOut.CORP_FK_MsKotaKab_Id.GetValueOrDefault
                    objKas.TransactionCashOut.CORP_KodePos = objSingleTransactinCashOut.CORP_KodePos
                    objKas.TransactionCashOut.CORP_FK_MsProvince_Id = objSingleTransactinCashOut.CORP_FK_MsProvince_Id.GetValueOrDefault
                    objKas.TransactionCashOut.CORP_FK_MsNegara_Id = objSingleTransactinCashOut.CORP_FK_MsNegara_Id.GetValueOrDefault
                    objKas.TransactionCashOut.CORP_LN_NamaJalan = objSingleTransactinCashOut.CORP_LN_NamaJalan
                    objKas.TransactionCashOut.CORP_LN_FK_MsNegara_Id = objSingleTransactinCashOut.CORP_LN_FK_MsNegara_Id.GetValueOrDefault
                    objKas.TransactionCashOut.CORP_LN_FK_MsProvince_Id = objSingleTransactinCashOut.CORP_LN_FK_MsProvince_Id.GetValueOrDefault
                    objKas.TransactionCashOut.CORP_LN_MsKotaKab_Id = objSingleTransactinCashOut.CORP_LN_MsKotaKab_Id.GetValueOrDefault
                    objKas.TransactionCashOut.CORP_LN_KodePos = objSingleTransactinCashOut.CORP_LN_KodePos
                    objKas.TransactionCashOut.CORP_NPWP = objSingleTransactinCashOut.CORP_NPWP
                    objKas.TransactionCashOut.CORP_TujuanTransaksi = objSingleTransactinCashOut.CORP_TujuanTransaksi
                    objKas.TransactionCashOut.CORP_SumberDana = objSingleTransactinCashOut.CORP_SumberDana
                    objKas.TransactionCashOut.CORP_NamaBankLain = objSingleTransactinCashOut.CORP_NamaBankLain
                    objKas.TransactionCashOut.CORP_NomorRekeningTujuan = objSingleTransactinCashOut.CORP_NomorRekeningTujuan

                    'Insert Data Ke Detail Transaction
                    objKas.DetailTranscationCashOut = New List(Of LTKTDetailCashOutTransaction)

                    Dim objListDetailTransactionCashOut As TList(Of LTKTDetailCashOutTransaction_ApprovalDetail) = DataRepository.LTKTDetailCashOutTransaction_ApprovalDetailProvider.GetPaged(LTKTDetailCashOutTransaction_ApprovalDetailColumn.FK_LTKTTransactionCashOut_ApprovalDetail_Id.ToString & " = '" & objSingleTransactinCashOut.PK_LTKTTransactionCashOut_ApprovalDetail_Id.ToString & "'", "", 0, Integer.MaxValue, 0)
                    For Each objSingleDetailTransactionCashOut As LTKTDetailCashOutTransaction_ApprovalDetail In objListDetailTransactionCashOut
                        Dim objCalonInsert As New LTKTDetailCashOutTransaction
                        objCalonInsert.PK_LTKTDetailCashOutTransaction_Id = objSingleDetailTransactionCashOut.PK_LTKTDetailCashOutTransaction_Id
                        objCalonInsert.Asing_KursTransaksi = objSingleDetailTransactionCashOut.Asing_KursTransaksi
                        objCalonInsert.Asing_TotalKasKeluarDalamRupiah = objSingleDetailTransactionCashOut.Asing_TotalKasKeluarDalamRupiah
                        objCalonInsert.TotalKasKeluar = objSingleDetailTransactionCashOut.TotalKasKeluar
                        objCalonInsert.KasKeluar = objSingleDetailTransactionCashOut.KasKeluar
                        objCalonInsert.Asing_FK_MsCurrency_Id = objSingleDetailTransactionCashOut.Asing_FK_MsCurrency_Id

                        objKas.DetailTranscationCashOut.Add(objCalonInsert)
                    Next

                    objResume.Add(objKas)

                Next
            End Using

            SetnGetResumeKasMasukKasKeluar = objResume
            grvTransaksi.DataSource = objResume
            grvTransaksi.DataBind()


            Dim totalKasMasuk As Decimal = 0
            Dim totalKasKeluar As Decimal = 0
            For Each kas As ResumeKasMasukKeluarLTKT In objResume
                If kas.Kas = "Kas Masuk" Then
                    totalKasMasuk += CDec(kas.TransactionNominal)
                Else
                    totalKasKeluar += CDec(kas.TransactionNominal)
                End If
            Next

            lblTotalKasMasuk.Text = ValidateBLL.FormatMoneyWithComma(totalKasMasuk)
            lblTotalKasKeluar.Text = ValidateBLL.FormatMoneyWithComma(totalKasKeluar)
        End If


    End Sub

    Sub loadLTKTToFieldOLD()
        Dim objApprovalDetail As TList(Of LTKT_ApprovalDetail) = DataRepository.LTKT_ApprovalDetailProvider.GetPaged(LTKT_ApprovalDetailColumn.FK_LTKT_Approval_Id.ToString & " = " & getAPPROVALPK.ToString, "", 0, Integer.MaxValue, 0)

        If objApprovalDetail.Count > 0 Then
            Using objLTKT As LTKT = DataRepository.LTKTProvider.GetByPK_LTKT_Id(objApprovalDetail(0).PK_LTKT_Id)
                If Not IsNothing(objLTKT) Then
                    'Ambil data buat cari PK Negara, Provinsi, KotaKab, dkk (ini variable yang bakal dpke berkali2)
                    Dim i As Integer = 0 'buat iterasi
                    Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetAll
                    Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
                    Dim objKelurahan As TList(Of MsKelurahan) = DataRepository.MsKelurahanProvider.GetAll
                    Dim objSKelurahan As MsKelurahan 'Penampung hasil search kelurahan
                    Dim objKecamatan As TList(Of MsKecamatan) = DataRepository.MsKecamatanProvider.GetAll
                    Dim objSkecamatan As MsKecamatan 'Penampung hasil search kecamatan
                    Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetAll
                    Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi
                    'Dim objMapNegara As TList(Of MappingMsNegaraNCBSPPATK) = DataRepository.MappingMsNegaraNCBSPPATKProvider.GetAll
                    'Dim objSMapNegara As MappingMsNegaraNCBSPPATK 'Penampung hasil search mappingnegara yang pke picker
                    'Dim objNegara As TList(Of MsNegaraNCBS) = DataRepository.MsNegaraNCBSProvider.GetAll
                    'Dim objSnegara As MsNegaraNCBS 'Penampung hasil search negara yang pke picker
                    Dim objPekerjaan As TList(Of MsPekerjaan) = DataRepository.MsPekerjaanProvider.GetAll
                    Dim objSPekerjaan As MsPekerjaan 'Penampung hasil search pekerjaan
                    Dim objBidangUsaha As TList(Of MsBidangUsaha) = DataRepository.MsBidangUsahaProvider.GetAll
                    Dim objSBidangUsaha As MsBidangUsaha 'Penampung hasil search bidang usaha

                    old_txtUmumPJKPelapor.Text = Safe(objLTKT.NamaPJKPelapor)
                    old_txtTglLaporan.Text = FormatDate(objLTKT.TanggalLaporan)
                    old_txtPejabatPelapor.Text = Safe(objLTKT.NamaPejabatPJKPelapor)
                    If objLTKT.NoLTKTKoreksi <> "" Then
                        old_cboTipeLaporan.SelectedIndex = 1
                        old_txtNoLTKTKoreksi.Text = Safe(objLTKT.NoLTKTKoreksi)
                        tipeLaporanCheckOLD()
                    End If
                    i = 0
                    For Each listKepemilikan As ListItem In old_cboTerlaporKepemilikan.Items
                        If objLTKT.FK_MsKepemilikan_Id.ToString = listKepemilikan.Value Then
                            old_cboTerlaporKepemilikan.SelectedIndex = i
                            Exit For
                        End If
                        i = i + 1
                    Next
                    old_txtLTKTInfoLainnya.Text = Safe(objLTKT.InformasiLainnya)
                    Me.Old_cboDebitCredit.SelectedValue = Safe(objLTKT.DebetCredit)
                    old_txtTerlaporNoRekening.Text = Safe(objLTKT.NoRekening)
                    old_txtCIFNo.Text = Safe(objLTKT.CIFNo)
                    SafeSelectedValue(old_rblTerlaporTipePelapor, objLTKT.TipeTerlapor)
                    LTKTTipeTerlaporChangeOld()
                    old_txtTerlaporGelar.Text = Safe(objLTKT.INDV_Gelar)
                    old_txtTerlaporNamaLengkap.Text = Safe(objLTKT.INDV_NamaLengkap)
                    old_txtTerlaporTempatLahir.Text = Safe(objLTKT.INDV_TempatLahir)
                    old_txtTerlaporTglLahir.Text = FormatDate(objLTKT.INDV_TanggalLahir)
                    'If CToDate2(old_txtTerlaporTglLahir.Text) = Date.MinValue Then
                    '    old_txtTerlaporTglLahir.text = safe (""
                    'End If
                    SafeSelectedValue(old_rblTerlaporKewarganegaraan, objLTKT.INDV_Kewarganegaraan)
                    SafeSelectedValue(old_cboTerlaporNegara, objLTKT.INDV_FK_MsNegara_Id.GetValueOrDefault)
                    'i = 0
                    'For Each listNegara As ListItem In old_cboTerlaporNegara.Items
                    '    If objLTKT.INDV_FK_MsNegara_Id.GetValueOrDefault.ToString = listNegara.Value Then
                    '        old_cboTerlaporNegara.SelectedIndex = i
                    '        Exit For
                    '    End If
                    '    i = i + 1
                    'Next
                    old_txtTerlaporDOMNamaJalan.Text = Safe(objLTKT.INDV_DOM_NamaJalan)
                    old_txtTerlaporDOMRTRW.Text = Safe(objLTKT.INDV_DOM_RTRW)
                    objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objLTKT.INDV_DOM_FK_MsKelurahan_Id)
                    If Not IsNothing(objSKelurahan) Then
                        old_txtTerlaporDOMKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
                        old_hfTerlaporDOMKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
                    End If
                    objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objLTKT.INDV_DOM_FK_MsKecamatan_Id)
                    If Not IsNothing(objSkecamatan) Then
                        old_txtTerlaporDOMKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
                        old_hfTerlaporDOMKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
                    End If
                    objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objLTKT.INDV_DOM_FK_MsKotaKab_Id)
                    If Not IsNothing(objSkotaKab) Then
                        old_txtTerlaporDOMKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                        old_hfTerlaporDOMKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                    End If
                    old_txtTerlaporDOMKodePos.Text = Safe(objLTKT.INDV_DOM_KodePos)
                    objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objLTKT.INDV_DOM_FK_MsProvince_Id)
                    If Not IsNothing(objSProvinsi) Then
                        old_txtTerlaporDOMProvinsi.Text = Safe(objSProvinsi.Nama)
                        old_hfTerlaporDOMProvinsi.Value = Safe(objSProvinsi.IdProvince)
                    End If
                    Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objLTKT.INDV_DOM_FK_MsNegara_Id.GetValueOrDefault(0))
                        If Not objNegaraRef Is Nothing Then

                            old_txtTerlaporDOMNegara.Text = Safe(objNegaraRef.NamaNegara)
                            old_hfTerlaporDOMNegara.Value = Safe(objNegaraRef.IDNegara)
                        End If
                    End Using
                    old_txtTerlaporIDNamaJalan.Text = Safe(objLTKT.INDV_ID_NamaJalan)
                    old_txtTerlaporIDRTRW.Text = Safe(objLTKT.INDV_ID_RTRW)
                    objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objLTKT.INDV_ID_FK_MsKelurahan_Id)
                    If Not IsNothing(objSKelurahan) Then
                        old_txtTerlaporIDKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
                        old_hfTerlaporIDKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
                    End If
                    objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objLTKT.INDV_ID_FK_MsKecamatan_Id)
                    If Not IsNothing(objSkecamatan) Then
                        old_txtTerlaporIDKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
                        old_hfTerlaporIDKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
                    End If
                    objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objLTKT.INDV_ID_FK_MsKotaKab_Id)
                    If Not IsNothing(objSkotaKab) Then
                        old_txtTerlaporIDKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                        old_hfTerlaporIDKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                    End If
                    old_txtTerlaporIDKodePos.Text = Safe(objLTKT.INDV_ID_KodePos)
                    objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objLTKT.INDV_ID_FK_MsProvince_Id)
                    If Not IsNothing(objSProvinsi) Then
                        old_txtTerlaporIDProvinsi.Text = Safe(objSProvinsi.Nama)
                        old_hfTerlaporIDProvinsi.Value = Safe(objSProvinsi.IdProvince)
                    End If
                    Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objLTKT.INDV_ID_FK_MsNegara_Id.GetValueOrDefault(0))
                        If Not objNegaraRef Is Nothing Then

                            old_txtTerlaporIDNegara.Text = Safe(objNegaraRef.NamaNegara)
                            old_hfTerlaporIDNegara.Value = Safe(objNegaraRef.IDNegara)
                        End If
                    End Using
                    old_txtTerlaporNANamaJalan.Text = Safe(objLTKT.INDV_NA_NamaJalan)
                    'objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objLTKT.INDV_NA_FK_MsNegara_Id)
                    'If Not IsNothing(objSMapNegara) Then
                    '    objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                    '    If Not IsNothing(objSnegara) Then
                    '        old_txtTerlaporNANegara.Text = Safe(objSnegara.NamaNegara)
                    '        old_hfTerlaporIDNegara.Value = Safe(objSnegara.IDNegaraNCBS)
                    '    End If
                    'End If
                    Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objLTKT.INDV_NA_FK_MsNegara_Id.GetValueOrDefault(0))
                        If Not objNegaraRef Is Nothing Then

                            old_txtTerlaporNANegara.Text = Safe(objNegaraRef.NamaNegara)
                            old_hfTerlaporNANegara.Value = Safe(objNegaraRef.IDNegara)
                        End If
                    End Using
                    objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objLTKT.INDV_NA_FK_MsProvince_Id)
                    If Not IsNothing(objSProvinsi) Then
                        old_txtTerlaporNAProvinsi.Text = Safe(objSProvinsi.Nama)
                        old_hfTerlaporNAProvinsi.Value = Safe(objSProvinsi.IdProvince)
                    End If
                    objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objLTKT.INDV_NA_FK_MsKotaKab_Id)
                    If Not IsNothing(objSkotaKab) Then
                        old_txtTerlaporNAKota.Text = Safe(objSkotaKab.NamaKotaKab)
                        old_hfTerlaporIDKota.Value = Safe(objSkotaKab.IDKotaKab)
                    End If
                    old_txtTerlaporNAKodePos.Text = Safe(objLTKT.INDV_NA_KodePos)
                    i = 0
                    For Each jenisDoc As ListItem In old_cboTerlaporJenisDocID.Items
                        If objLTKT.INDV_FK_MsIDType_Id.GetValueOrDefault.ToString = jenisDoc.Value Then
                            old_cboTerlaporJenisDocID.SelectedIndex = i
                            Exit For
                        End If
                        i = i + 1
                    Next
                    old_txtTerlaporNomorID.Text = Safe(objLTKT.INDV_NomorId)
                    old_txtTerlaporNPWP.Text = Safe(objLTKT.INDV_NPWP)
                    objSPekerjaan = objPekerjaan.Find(MsPekerjaanColumn.IDPekerjaan, objLTKT.INDV_FK_MsPekerjaan_Id)
                    If Not IsNothing(objSPekerjaan) Then
                        old_txtTerlaporPekerjaan.Text = Safe(objSPekerjaan.NamaPekerjaan)
                        old_hfTerlaporPekerjaan.Value = Safe(objSPekerjaan.IDPekerjaan)
                    End If
                    old_txtTerlaporJabatan.Text = Safe(objLTKT.INDV_Jabatan)
                    old_txtTerlaporPenghasilanRataRata.Text = Safe(objLTKT.INDV_PenghasilanRataRata)
                    old_txtTerlaporTempatKerja.Text = Safe(objLTKT.INDV_TempatBekerja)
                    i = 0
                    For Each listBentukBadanUsaha As ListItem In old_cboTerlaporCORPBentukBadanUsaha.Items
                        If objLTKT.CORP_FK_MsBentukBadanUsaha_Id.ToString = listBentukBadanUsaha.Value Then
                            old_cboTerlaporCORPBentukBadanUsaha.SelectedIndex = i
                            Exit For
                        End If
                        i = i + 1
                    Next
                    old_txtTerlaporCORPNama.Text = Safe(objLTKT.CORP_Nama)
                    objSBidangUsaha = objBidangUsaha.Find(MsBidangUsahaColumn.IdBidangUsaha, objLTKT.CORP_FK_MsBidangUsaha_Id)
                    If Not IsNothing(objSBidangUsaha) Then
                        old_txtTerlaporCORPBidangUsaha.Text = Safe(objSBidangUsaha.NamaBidangUsaha)
                        old_hfTerlaporCORPBidangUsaha.Value = Safe(objSBidangUsaha.IdBidangUsaha)
                    End If

                    SafeNumIndex(old_rblTerlaporCORPTipeAlamat, objLTKT.CORP_TipeAlamat)

                    old_txtTerlaporCORPDLNamaJalan.Text = Safe(objLTKT.CORP_NamaJalan)
                    old_txtTerlaporCORPDLRTRW.Text = Safe(objLTKT.CORP_RTRW)

                    objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objLTKT.CORP_FK_MsKelurahan_Id)
                    If Not IsNothing(objSKelurahan) Then
                        old_txtTerlaporCORPDLKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
                        old_hfTerlaporCORPDLKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
                    End If
                    objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objLTKT.CORP_FK_MsKecamatan_Id)
                    If Not IsNothing(objSkecamatan) Then
                        old_txtTerlaporCORPDLKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
                        old_hfTerlaporCORPDLKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
                    End If
                    objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objLTKT.CORP_FK_MsKotaKab_Id)
                    If Not IsNothing(objSkotaKab) Then
                        old_txtTerlaporCORPDLKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                        old_hfTerlaporCORPDLKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                    End If
                    old_txtTerlaporCORPDLKodePos.Text = Safe(objLTKT.CORP_KodePos)
                    objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objLTKT.CORP_FK_MsProvince_Id)
                    If Not IsNothing(objSProvinsi) Then
                        old_txtTerlaporCORPDLProvinsi.Text = Safe(objSProvinsi.Nama)
                        old_hfTerlaporCORPDLProvinsi.Value = Safe(objSProvinsi.IdProvince)
                    End If
                    'objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objLTKT.CORP_FK_MsNegara_Id)
                    'If Not IsNothing(objSMapNegara) Then
                    '    objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                    '    If Not IsNothing(objSnegara) Then
                    '        old_txtTerlaporCORPDLNegara.Text = Safe(objSnegara.NamaNegara)
                    '        old_hfTerlaporCORPDLNegara.Value = Safe(objSnegara.IDNegaraNCBS)
                    '    End If
                    'End If
                    Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objLTKT.CORP_FK_MsNegara_Id.GetValueOrDefault(0))
                        If Not objNegaraRef Is Nothing Then

                            old_txtTerlaporCORPDLNegara.Text = Safe(objNegaraRef.NamaNegara)
                            old_hfTerlaporCORPDLNegara.Value = Safe(objNegaraRef.IDNegara)
                        End If
                    End Using
                    old_txtTerlaporCORPLNNamaJalan.Text = Safe(objLTKT.CORP_LN_NamaJalan)
                    'objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objLTKT.CORP_LN_FK_MsNegara_Id)
                    'If Not IsNothing(objSMapNegara) Then
                    '    objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                    '    If Not IsNothing(objSnegara) Then
                    '        old_txtTerlaporCORPLNNegara.Text = Safe(objSnegara.NamaNegara)
                    '        old_hfTerlaporCORPLNNegara.Value = Safe(objSnegara.IDNegaraNCBS)
                    '    End If
                    'End If
                    Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objLTKT.CORP_LN_FK_MsNegara_Id.GetValueOrDefault(0))
                        If Not objNegaraRef Is Nothing Then

                            old_txtTerlaporCORPLNNegara.Text = Safe(objNegaraRef.NamaNegara)
                            old_hfTerlaporCORPLNNegara.Value = Safe(objNegaraRef.IDNegara)
                        End If
                    End Using
                    objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objLTKT.CORP_LN_FK_MsProvince_Id)
                    If Not IsNothing(objSProvinsi) Then
                        old_txtTerlaporCORPLNProvinsi.Text = Safe(objSProvinsi.Nama)
                        old_hfTerlaporCORPLNProvinsi.Value = Safe(objSProvinsi.IdProvince)
                    End If
                    objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objLTKT.CORP_LN_MsKotaKab_Id)
                    If Not IsNothing(objSkotaKab) Then
                        old_txtTerlaporCORPLNKota.Text = Safe(objSkotaKab.NamaKotaKab)
                        old_hfTerlaporCORPLNKota.Value = Safe(objSkotaKab.IDKotaKab)
                    End If
                    old_txtTerlaporCORPLNKodePos.Text = Safe(objLTKT.CORP_LN_KodePos)
                    old_txtTerlaporCORPNPWP.Text = Safe(objLTKT.CORP_NPWP)
                End If
            End Using
        End If



    End Sub



    Sub panelSetting()
        Dim objApproval As LTKT_Approval = DataRepository.LTKT_ApprovalProvider.GetByPK_LTKT_Approval_Id(getAPPROVALPK)
        If Not IsNothing(objApproval) Then
            If objApproval.FK_MsMode_Id.GetValueOrDefault = 1 Then
                NewPanel.Visible = True
                OldPanel.Visible = False
                LblOldTitle.Visible = False
            ElseIf objApproval.FK_MsMode_Id.GetValueOrDefault = 2 Then
                NewPanel.Visible = True
                OldPanel.Visible = True
            ElseIf objApproval.FK_MsMode_Id.GetValueOrDefault = 3 Then
                NewPanel.Visible = False
                OldPanel.Visible = True
                LblNewTitle.Visible = False
            End If
        End If
    End Sub

    Sub loadData()
        Using objApproval As LTKT_Approval = DataRepository.LTKT_ApprovalProvider.GetByPK_LTKT_Approval_Id(getAPPROVALPK)
            If Not IsNothing(objApproval) Then
                If objApproval.FK_MsMode_Id.GetValueOrDefault = 1 Then
                    loadLTKTToField()
                    loadResume()
                ElseIf objApproval.FK_MsMode_Id.GetValueOrDefault = 2 Then
                    loadLTKTToField()
                    loadResume()
                    loadLTKTToFieldOLD()
                    loadResumeOld()
                ElseIf objApproval.FK_MsMode_Id.GetValueOrDefault = 3 Then
                    loadLTKTToFieldOLD()
                    loadResumeOld()
                End If
            End If
        End Using

    End Sub


    Private Sub SetControlLoad()
        rblTerlaporTipePelapor.SelectedValue = 1
        divPerorangan.Visible = True
        divKorporasi.Visible = False

        rblTRXKMTipePelapor.SelectedValue = 1
        tblTRXKMTipePelapor.Visible = True
        tblTRXKMTipePelaporKorporasi.Visible = False

        rblTRXKKTipePelapor.SelectedValue = 1
        tblTRXKKTipePelaporPerorangan.Visible = True
        tblTRXKKTipePelaporKorporasi.Visible = False

        'bind MsKepemilikan
        Using objPemilik As TList(Of MsKepemilikan) = DataRepository.MsKepemilikanProvider.GetAll
            If objPemilik.Count > 0 Then
                cboTerlaporKepemilikan.Items.Clear()
                cboTerlaporKepemilikan.Items.Add("-Select-")
                For i As Integer = 0 To objPemilik.Count - 1
                    cboTerlaporKepemilikan.Items.Add(New ListItem(objPemilik(i).NamaKepemilikan, objPemilik(i).IDKepemilikan.ToString))
                Next
            End If
        End Using

        'bind MsNegara
        Using objNegara As TList(Of MsNegara) = DataRepository.MsNegaraProvider.GetAll
            If objNegara.Count > 0 Then
                cboTerlaporNegara.Items.Clear()
                cboTerlaporNegara.Items.Add("-Select-")

                cboTRXKMINDVNegara.Items.Clear()
                cboTRXKMINDVNegara.Items.Add("-Select-")

                cboTRXKKINDVNegara.Items.Clear()
                cboTRXKKINDVNegara.Items.Add("-Select-")
                For i As Integer = 0 To objNegara.Count - 1
                    cboTerlaporNegara.Items.Add(New ListItem(objNegara(i).NamaNegara, objNegara(i).IDNegara.ToString))
                    cboTRXKMINDVNegara.Items.Add(New ListItem(objNegara(i).NamaNegara, objNegara(i).IDNegara.ToString))
                    cboTRXKKINDVNegara.Items.Add(New ListItem(objNegara(i).NamaNegara, objNegara(i).IDNegara.ToString))
                Next
            End If
        End Using

        'Bind MsBentukBadanUsaha
        Using objBentukBadanUsaha As TList(Of MsBentukBidangUsaha) = DataRepository.MsBentukBidangUsahaProvider.GetAll
            If objBentukBadanUsaha.Count > 0 Then
                cboTerlaporCORPBentukBadanUsaha.Items.Clear()
                cboTerlaporCORPBentukBadanUsaha.Items.Add("-Select-")

                cboTRXKMCORPBentukBadanUsaha.Items.Clear()
                cboTRXKMCORPBentukBadanUsaha.Items.Add("-Select-")

                cboTRXKKCORPBentukBadanUsaha.Items.Clear()
                cboTRXKKCORPBentukBadanUsaha.Items.Add("-Select-")

                For i As Integer = 0 To objBentukBadanUsaha.Count - 1
                    cboTerlaporCORPBentukBadanUsaha.Items.Add(New ListItem(objBentukBadanUsaha(i).BentukBidangUsaha, objBentukBadanUsaha(i).IdBentukBidangUsaha.ToString))
                    cboTRXKMCORPBentukBadanUsaha.Items.Add(New ListItem(objBentukBadanUsaha(i).BentukBidangUsaha, objBentukBadanUsaha(i).IdBentukBidangUsaha.ToString))
                    cboTRXKKCORPBentukBadanUsaha.Items.Add(New ListItem(objBentukBadanUsaha(i).BentukBidangUsaha, objBentukBadanUsaha(i).IdBentukBidangUsaha.ToString))
                Next
            End If
        End Using

        'bind ID type
        Using objJenisID As TList(Of MsIDType) = DataRepository.MsIDTypeProvider.GetAll
            cboTerlaporJenisDocID.Items.Clear()
            cboTerlaporJenisDocID.Items.Add("-Select-")

            cboTRXKMINDVJenisID.Items.Clear()
            cboTRXKMINDVJenisID.Items.Add("-Select-")

            cboTRXKKINDVJenisID.Items.Clear()
            cboTRXKKINDVJenisID.Items.Add("-Select-")

            For i As Integer = 0 To objJenisID.Count - 1
                cboTerlaporJenisDocID.Items.Add(New ListItem(objJenisID(i).MsIDType_Name, objJenisID(i).Pk_MsIDType_Id.ToString))
                cboTRXKMINDVJenisID.Items.Add(New ListItem(objJenisID(i).MsIDType_Name, objJenisID(i).Pk_MsIDType_Id.ToString))
                cboTRXKKINDVJenisID.Items.Add(New ListItem(objJenisID(i).MsIDType_Name, objJenisID(i).Pk_MsIDType_Id.ToString))
            Next




        End Using



    End Sub

    Private Sub SetControlLoadOLD()
        old_rblTerlaporTipePelapor.SelectedValue = 1
        old_divPerorangan.Visible = True
        old_divKorporasi.Visible = False

        old_rblTRXKMTipePelapor.SelectedValue = 1
        old_tblTRXKMTipePelapor.Visible = True
        old_tblTRXKMTipePelaporKorporasi.Visible = False

        old_rblTRXKKTipePelapor.SelectedValue = 1
        old_tblTRXKKTipePelaporPerorangan.Visible = True
        old_tblTRXKKTipePelaporKorporasi.Visible = False

        'bind MsKepemilikan
        Using objPemilik As TList(Of MsKepemilikan) = DataRepository.MsKepemilikanProvider.GetAll
            If objPemilik.Count > 0 Then
                old_cboTerlaporKepemilikan.Items.Clear()
                old_cboTerlaporKepemilikan.Items.Add("-Select-")
                For i As Integer = 0 To objPemilik.Count - 1
                    old_cboTerlaporKepemilikan.Items.Add(New ListItem(objPemilik(i).NamaKepemilikan, objPemilik(i).IDKepemilikan.ToString))
                Next
            End If
        End Using

        'bind MsNegara
        Using objNegara As TList(Of MsNegara) = DataRepository.MsNegaraProvider.GetAll
            If objNegara.Count > 0 Then
                old_cboTerlaporNegara.Items.Clear()
                old_cboTerlaporNegara.Items.Add("-Select-")

                old_cboTRXKMINDVNegara.Items.Clear()
                old_cboTRXKMINDVNegara.Items.Add("-Select-")

                old_cboTRXKKINDVNegara.Items.Clear()
                old_cboTRXKKINDVNegara.Items.Add("-Select-")
                For i As Integer = 0 To objNegara.Count - 1
                    old_cboTerlaporNegara.Items.Add(New ListItem(objNegara(i).NamaNegara, objNegara(i).IDNegara.ToString))
                    old_cboTRXKMINDVNegara.Items.Add(New ListItem(objNegara(i).NamaNegara, objNegara(i).IDNegara.ToString))
                    old_cboTRXKKINDVNegara.Items.Add(New ListItem(objNegara(i).NamaNegara, objNegara(i).IDNegara.ToString))
                Next
            End If
        End Using

        'Bind MsBentukBadanUsaha
        Using objBentukBadanUsaha As TList(Of MsBentukBidangUsaha) = DataRepository.MsBentukBidangUsahaProvider.GetAll
            If objBentukBadanUsaha.Count > 0 Then
                old_cboTerlaporCORPBentukBadanUsaha.Items.Clear()
                old_cboTerlaporCORPBentukBadanUsaha.Items.Add("-Select-")

                old_cboTRXKMCORPBentukBadanUsaha.Items.Clear()
                old_cboTRXKMCORPBentukBadanUsaha.Items.Add("-Select-")

                old_cboTRXKKCORPBentukBadanUsaha.Items.Clear()
                old_cboTRXKKCORPBentukBadanUsaha.Items.Add("-Select-")

                For i As Integer = 0 To objBentukBadanUsaha.Count - 1
                    old_cboTerlaporCORPBentukBadanUsaha.Items.Add(New ListItem(objBentukBadanUsaha(i).BentukBidangUsaha, objBentukBadanUsaha(i).IdBentukBidangUsaha.ToString))
                    old_cboTRXKMCORPBentukBadanUsaha.Items.Add(New ListItem(objBentukBadanUsaha(i).BentukBidangUsaha, objBentukBadanUsaha(i).IdBentukBidangUsaha.ToString))
                    old_cboTRXKKCORPBentukBadanUsaha.Items.Add(New ListItem(objBentukBadanUsaha(i).BentukBidangUsaha, objBentukBadanUsaha(i).IdBentukBidangUsaha.ToString))
                Next
            End If
        End Using

        'bind ID type
        Using objJenisID As TList(Of MsIDType) = DataRepository.MsIDTypeProvider.GetAll
            old_cboTerlaporJenisDocID.Items.Clear()
            old_cboTerlaporJenisDocID.Items.Add("-Select-")

            old_cboTRXKMINDVJenisID.Items.Clear()
            old_cboTRXKMINDVJenisID.Items.Add("-Select-")

            old_cboTRXKKINDVJenisID.Items.Clear()
            old_cboTRXKKINDVJenisID.Items.Add("-Select-")

            For i As Integer = 0 To objJenisID.Count - 1
                old_cboTerlaporJenisDocID.Items.Add(New ListItem(objJenisID(i).MsIDType_Name, objJenisID(i).Pk_MsIDType_Id.ToString))
                old_cboTRXKMINDVJenisID.Items.Add(New ListItem(objJenisID(i).MsIDType_Name, objJenisID(i).Pk_MsIDType_Id.ToString))
                old_cboTRXKKINDVJenisID.Items.Add(New ListItem(objJenisID(i).MsIDType_Name, objJenisID(i).Pk_MsIDType_Id.ToString))
            Next




        End Using



    End Sub

    Sub tipeLaporanCheckOLD()
        If old_cboTipeLaporan.SelectedIndex = 0 Then
            old_tblLTKTKoreksi.Visible = False
        Else
            old_tblLTKTKoreksi.Visible = True
        End If
    End Sub

    Sub tipeLaporanCheck()
        If cboTipeLaporan.SelectedIndex = 0 Then
            tblLTKTKoreksi.Visible = False
        Else
            tblLTKTKoreksi.Visible = True
        End If
    End Sub



    Protected Sub cboTipeLaporan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipeLaporan.SelectedIndexChanged
        Try
            tipeLaporanCheck()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub old_cboTipeLaporan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles old_cboTipeLaporan.SelectedIndexChanged
        Try
            tipeLaporanCheckOLD()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub rblTerlaporTipePelapor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblTerlaporTipePelapor.SelectedIndexChanged
        Try
            LTKTTipeTerlaporChange()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub old_rblTerlaporTipePelapor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles old_rblTerlaporTipePelapor.SelectedIndexChanged
        Try
            LTKTTipeTerlaporChangeOld()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                SetControlLoad()
                SetControlLoadOLD()
                clearSession()
                loadData()
                loadApprovalInformation()
                panelSetting()
                compareLTKT()

                If MultiView1.ActiveViewIndex = 0 Then
                    If NewPanel.Visible = True Then
                        ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('IdTerlapor','Img1','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
                        ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('Transaksi','Img2','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
                    End If
                    If OldPanel.Visible = True Then
                        ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('old_IdTerlapor','old_Img1','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
                        ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('old_Transaksi','old_Img2','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
                    End If
                End If


            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try

    End Sub

    Protected Sub Menu1_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs) Handles Menu1.MenuItemClick
        Try
            MultiView1.ActiveViewIndex = CInt(Menu1.SelectedValue)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub old_Menu1_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs) Handles old_Menu1.MenuItemClick
        Try
            old_MultiView1.ActiveViewIndex = CInt(old_Menu1.SelectedValue)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub rblTRXKMTipePelapor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblTRXKMTipePelapor.SelectedIndexChanged
        Try
            If rblTRXKMTipePelapor.SelectedValue = 1 Then
                tblTRXKMTipePelapor.Visible = True
                tblTRXKMTipePelaporKorporasi.Visible = False
            Else
                tblTRXKMTipePelapor.Visible = False
                tblTRXKMTipePelaporKorporasi.Visible = True
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub rblTRXKKTipePelapor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblTRXKKTipePelapor.SelectedIndexChanged
        Try
            If rblTRXKKTipePelapor.SelectedValue = 1 Then
                tblTRXKKTipePelaporPerorangan.Visible = True
                tblTRXKKTipePelaporKorporasi.Visible = False
            Else
                tblTRXKKTipePelaporPerorangan.Visible = False
                tblTRXKKTipePelaporKorporasi.Visible = True
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub old_rblTRXKMTipePelapor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles old_rblTRXKMTipePelapor.SelectedIndexChanged
        Try
            If old_rblTRXKMTipePelapor.SelectedValue = 1 Then
                old_tblTRXKMTipePelapor.Visible = True
                old_tblTRXKMTipePelaporKorporasi.Visible = False
            Else
                old_tblTRXKMTipePelapor.Visible = False
                old_tblTRXKMTipePelaporKorporasi.Visible = True
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub old_rblTRXKKTipePelapor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles old_rblTRXKKTipePelapor.SelectedIndexChanged
        Try
            If old_rblTRXKKTipePelapor.SelectedValue = 1 Then
                old_tblTRXKKTipePelaporPerorangan.Visible = True
                old_tblTRXKKTipePelaporKorporasi.Visible = False
            Else
                old_tblTRXKKTipePelaporPerorangan.Visible = False
                old_tblTRXKKTipePelaporKorporasi.Visible = True
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub grvDetilKasKeluar_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grvDetilKasKeluar.RowDataBound
        Try
            If e.Row.RowType <> ListItemType.Footer And e.Row.RowType <> ListItemType.Header Then
                e.Row.Cells(0).Text = e.Row.RowIndex + 1
                If e.Row.Cells.Count > 2 Then
                    e.Row.Cells(4).Text = ValidateBLL.FormatMoneyWithComma(e.Row.Cells(4).Text)
                End If

            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub grvTRXKMDetilValutaAsing_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grvTRXKMDetilValutaAsing.RowDataBound
        Try
            If e.Row.RowType <> ListItemType.Footer And e.Row.RowType <> ListItemType.Header Then
                e.Row.Cells(0).Text = e.Row.RowIndex + 1
                If e.Row.Cells.Count > 2 Then
                    e.Row.Cells(4).Text = ValidateBLL.FormatMoneyWithComma(e.Row.Cells(4).Text)
                End If

            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub grvTransaksi_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grvTransaksi.RowDataBound
        Try
            If e.Row.RowType <> ListItemType.Footer And e.Row.RowType <> ListItemType.Header Then
                e.Row.Cells(0).Text = e.Row.RowIndex + 1
                If e.Row.Cells.Count > 2 Then
                    e.Row.Cells(6).Text = ValidateBLL.FormatMoneyWithComma(e.Row.Cells(6).Text)
                End If

            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub



    Protected Sub imgOKMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgOKMsg.Click
        Try
            Response.Redirect("LTKTApproval.aspx")
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Try
            Response.Redirect("LTKTApproval.aspx")
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub imgOkDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgApprove.Click
        Try
            Dim objApproval As LTKT_Approval = DataRepository.LTKT_ApprovalProvider.GetByPK_LTKT_Approval_Id(getAPPROVALPK)
            If objApproval.FK_MsMode_Id.GetValueOrDefault = 1 Then
                add()
            ElseIf objApproval.FK_MsMode_Id.GetValueOrDefault = 2 Then
                edit()
            ElseIf objApproval.FK_MsMode_Id.GetValueOrDefault = 3 Then
                delete()
            End If
            hideControl()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub imgReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgReject.Click
        Try
            'mtvPage.ActiveViewIndex = 2
            'lblReject.Text = "Reject Reason:"
            'hideControl()
            deleteApprovaRejected()
            hideControl()
            mtvPage.ActiveViewIndex = 1
            lblMsg.Text = "Data Rejected"
        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message.ToString
        End Try
    End Sub

    Protected Sub imgOkRejectReason_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgOkRejectReason.Click

        Try
            'Kirim Email ke yang punya data

            'Dim ReasonOfRejecting As String = txtReasonReject.Text
            'Dim ApproverUser As String = vbNullString
            'Dim EmailBodyData As String = vbNullString

            ''EmailBodyData = "Your LTKT is rejected by (User Id : " & Sahassa.AML.Commonly.SessionNIK & "-" & Sahassa.AML.Commonly.SessionUserId & ")<br>Here is the reason:<br> " & txtReasonReject.Text
            'EmailBodyData = EmailBLL.getEmailBodyTemplate("LTKT Reject", "LTKT", "", ReasonOfRejecting)


            'Using objLTKTApproval As LTKT_Approval = DataRepository.LTKT_ApprovalProvider.GetByPK_LTKT_Approval_Id(getAPPROVALPK)
            '    If Not IsNothing(objLTKTApproval) Then
            '        Using objMsUser As User = DataRepository.UserProvider.GetBypkUserID(objLTKTApproval.RequestedBy)
            '            If Not IsNothing(objMsUser) Then

            '                ApproverUser = objMsUser.UserEmailAddress & ";"


            '                'Using CommonEmail As New Sahassa.AML.EmailSend
            '                '    'CommonEmail.Subject = "Approval LTKT Rejeceted (User Id : " & Sahassa.AML.Commonly.SessionNIK & "-" & Sahassa.AML.Commonly.SessionUserId & ")"
            '                '    CommonEmail.Subject = "LTKT Reject"
            '                '    CommonEmail.Sender = "Cash Transaction Report Application"
            '                '    CommonEmail.Recipient = ApproverUser
            '                '    CommonEmail.Body = EmailBodyData
            '                '    CommonEmail.SendEmail()
            '                'End Using
            '            End If
            '        End Using
            '    End If
            'End Using
            deleteApprovaRejected()
            Response.Redirect("LTKTApproval.aspx")
        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

#Region "Fill Object From Control"
    Private Shared Sub FillDetailTransactionCashoutObject(ByVal objLTKTTransactionCashOut As LTKTTransactionCashOut, _
                                                                                                                         ByVal objSingleDetail As LTKTDetailCashOutTransaction, _
                                                                                                                         ByVal objLTKTDetailTransactionCashOut As LTKTDetailCashOutTransaction)
        FillOrNothing(objLTKTDetailTransactionCashOut.FK_LTKTTransactionCashOut_Id, objLTKTTransactionCashOut.PK_LTKTTransactionCashOut_Id, True, OLong)
        FillOrNothing(objLTKTDetailTransactionCashOut.KasKeluar, objSingleDetail.KasKeluar)
        FillOrNothing(objLTKTDetailTransactionCashOut.Asing_FK_MsCurrency_Id, objSingleDetail.Asing_FK_MsCurrency_Id)
        FillOrNothing(objLTKTDetailTransactionCashOut.Asing_KursTransaksi, objSingleDetail.Asing_KursTransaksi)
        FillOrNothing(objLTKTDetailTransactionCashOut.Asing_TotalKasKeluarDalamRupiah, objSingleDetail.Asing_TotalKasKeluarDalamRupiah)
        FillOrNothing(objLTKTDetailTransactionCashOut.TotalKasKeluar, objSingleDetail.TotalKasKeluar)
    End Sub

    Private Shared Sub FillTransactionDetailCashin(ByVal objLTKTTransactionCashIn As LTKTTransactionCashIn, _
                                                                                                                        ByVal objSingleDetail As LTKTDetailCashInTransaction, _
                                                                                                                        ByVal objLTKTDetailTransactionCashIn As LTKTDetailCashInTransaction)

        FillOrNothing(objLTKTDetailTransactionCashIn.FK_LTKTTransactionCashIn_Id, objLTKTTransactionCashIn.PK_LTKTTransactionCashIn_Id, True, OLong)
        FillOrNothing(objLTKTDetailTransactionCashIn.KasMasuk, objSingleDetail.KasMasuk)
        FillOrNothing(objLTKTDetailTransactionCashIn.Asing_FK_MsCurrency_Id, objSingleDetail.Asing_FK_MsCurrency_Id)
        FillOrNothing(objLTKTDetailTransactionCashIn.Asing_KursTransaksi, objSingleDetail.Asing_KursTransaksi)
        FillOrNothing(objLTKTDetailTransactionCashIn.Asing_TotalKasMasukDalamRupiah, objSingleDetail.Asing_TotalKasMasukDalamRupiah)
        FillOrNothing(objLTKTDetailTransactionCashIn.TotalKasMasuk, objSingleDetail.TotalKasMasuk)


    End Sub

    Private Shared Sub FillTransactionCasoutObject(ByVal objLTKTDetil As LTKT, _
                                                                                                                 ByVal objSingleResume As ResumeKasMasukKeluarLTKT, _
                                                                                                                 ByVal objLTKTTransactionCashOut As LTKTTransactionCashOut)
        FillOrNothing(objLTKTTransactionCashOut.FK_LTKT_Id, objLTKTDetil.PK_LTKT_Id, True, oInt)
        FillOrNothing(objLTKTTransactionCashOut.TanggalTransaksi, objSingleResume.TransactionCashOut.TanggalTransaksi)
        FillOrNothing(objLTKTTransactionCashOut.NamaKantorPJK, objSingleResume.TransactionCashOut.NamaKantorPJK)
        FillOrNothing(objLTKTTransactionCashOut.FK_MsKotaKab_Id, objSingleResume.TransactionCashOut.FK_MsKotaKab_Id)
        FillOrNothing(objLTKTTransactionCashOut.FK_MsProvince_Id, objSingleResume.TransactionCashOut.FK_MsProvince_Id)
        FillOrNothing(objLTKTTransactionCashOut.NomorRekening, objSingleResume.TransactionCashOut.NomorRekening)
        FillOrNothing(objLTKTTransactionCashOut.TipeTerlapor, objSingleResume.TransactionCashOut.TipeTerlapor)

        'individu
        FillOrNothing(objLTKTTransactionCashOut.INDV_Gelar, objSingleResume.TransactionCashOut.INDV_Gelar)
        FillOrNothing(objLTKTTransactionCashOut.INDV_NamaLengkap, objSingleResume.TransactionCashOut.INDV_NamaLengkap)
        FillOrNothing(objLTKTTransactionCashOut.INDV_TempatLahir, objSingleResume.TransactionCashOut.INDV_TempatLahir)
        FillOrNothing(objLTKTTransactionCashOut.INDV_TanggalLahir, objSingleResume.TransactionCashOut.INDV_TanggalLahir)
        FillOrNothing(objLTKTTransactionCashOut.INDV_Kewarganegaraan, objSingleResume.TransactionCashOut.INDV_Kewarganegaraan)
        FillOrNothing(objLTKTTransactionCashOut.INDV_FK_MsNegara_Id, objSingleResume.TransactionCashOut.INDV_FK_MsNegara_Id)
        FillOrNothing(objLTKTTransactionCashOut.INDV_FK_MsIDType_Id, objSingleResume.TransactionCashOut.INDV_FK_MsIDType_Id)
        FillOrNothing(objLTKTTransactionCashOut.INDV_NomorId, objSingleResume.TransactionCashOut.INDV_NomorId)
        FillOrNothing(objLTKTTransactionCashOut.INDV_NPWP, objSingleResume.TransactionCashOut.INDV_NPWP)
        FillOrNothing(objLTKTTransactionCashOut.INDV_FK_MsPekerjaan_Id, objSingleResume.TransactionCashOut.INDV_FK_MsPekerjaan_Id)
        FillOrNothing(objLTKTTransactionCashOut.INDV_Jabatan, objSingleResume.TransactionCashOut.INDV_Jabatan)
        FillOrNothing(objLTKTTransactionCashOut.INDV_PenghasilanRataRata, objSingleResume.TransactionCashOut.INDV_PenghasilanRataRata)
        FillOrNothing(objLTKTTransactionCashOut.INDV_TempatBekerja, objSingleResume.TransactionCashOut.INDV_TempatBekerja)
        FillOrNothing(objLTKTTransactionCashOut.INDV_TujuanTransaksi, objSingleResume.TransactionCashOut.INDV_TujuanTransaksi)
        FillOrNothing(objLTKTTransactionCashOut.INDV_SumberDana, objSingleResume.TransactionCashOut.INDV_SumberDana)
        FillOrNothing(objLTKTTransactionCashOut.INDV_NamaBankLain, objSingleResume.TransactionCashOut.INDV_NamaBankLain)
        FillOrNothing(objLTKTTransactionCashOut.INDV_NomorRekeningTujuan, objSingleResume.TransactionCashOut.INDV_NomorRekeningTujuan)
        'domisili
        FillOrNothing(objLTKTTransactionCashOut.INDV_DOM_NamaJalan, objSingleResume.TransactionCashOut.INDV_DOM_NamaJalan)
        FillOrNothing(objLTKTTransactionCashOut.INDV_DOM_RTRW, objSingleResume.TransactionCashOut.INDV_DOM_RTRW)
        FillOrNothing(objLTKTTransactionCashOut.INDV_DOM_FK_MsKelurahan_Id, objSingleResume.TransactionCashOut.INDV_DOM_FK_MsKelurahan_Id)
        FillOrNothing(objLTKTTransactionCashOut.INDV_DOM_FK_MsKecamatan_Id, objSingleResume.TransactionCashOut.INDV_DOM_FK_MsKecamatan_Id)
        FillOrNothing(objLTKTTransactionCashOut.INDV_DOM_FK_MsKotaKab_Id, objSingleResume.TransactionCashOut.INDV_DOM_FK_MsKotaKab_Id)
        FillOrNothing(objLTKTTransactionCashOut.INDV_DOM_KodePos, objSingleResume.TransactionCashOut.INDV_DOM_KodePos)
        FillOrNothing(objLTKTTransactionCashOut.INDV_DOM_FK_MsProvince_Id, objSingleResume.TransactionCashOut.INDV_DOM_FK_MsProvince_Id)
        FillOrNothing(objLTKTTransactionCashOut.INDV_DOM_FK_MsNegara_Id, objSingleResume.TransactionCashOut.INDV_DOM_FK_MsNegara_Id)
        'identitas
        FillOrNothing(objLTKTTransactionCashOut.INDV_ID_NamaJalan, objSingleResume.TransactionCashOut.INDV_ID_NamaJalan)
        FillOrNothing(objLTKTTransactionCashOut.INDV_ID_RTRW, objSingleResume.TransactionCashOut.INDV_ID_RTRW)
        FillOrNothing(objLTKTTransactionCashOut.INDV_ID_FK_MsKelurahan_Id, objSingleResume.TransactionCashOut.INDV_ID_FK_MsKelurahan_Id)
        FillOrNothing(objLTKTTransactionCashOut.INDV_ID_FK_MsKecamatan_Id, objSingleResume.TransactionCashOut.INDV_ID_FK_MsKecamatan_Id)
        FillOrNothing(objLTKTTransactionCashOut.INDV_ID_FK_MsKotaKab_Id, objSingleResume.TransactionCashOut.INDV_ID_FK_MsKotaKab_Id)
        FillOrNothing(objLTKTTransactionCashOut.INDV_ID_KodePos, objSingleResume.TransactionCashOut.INDV_ID_KodePos)
        FillOrNothing(objLTKTTransactionCashOut.INDV_ID_FK_MsProvince_Id, objSingleResume.TransactionCashOut.INDV_ID_FK_MsProvince_Id)
        FillOrNothing(objLTKTTransactionCashOut.INDV_ID_FK_MsNegara_Id, objSingleResume.TransactionCashOut.INDV_ID_FK_MsNegara_Id)
        'Negara asing
        FillOrNothing(objLTKTTransactionCashOut.INDV_NA_NamaJalan, objSingleResume.TransactionCashOut.INDV_NA_NamaJalan)
        FillOrNothing(objLTKTTransactionCashOut.INDV_NA_FK_MsNegara_Id, objSingleResume.TransactionCashOut.INDV_NA_FK_MsNegara_Id)
        FillOrNothing(objLTKTTransactionCashOut.INDV_NA_FK_MsProvince_Id, objSingleResume.TransactionCashOut.INDV_NA_FK_MsProvince_Id)
        FillOrNothing(objLTKTTransactionCashOut.INDV_NA_FK_MsKotaKab_Id, objSingleResume.TransactionCashOut.INDV_NA_FK_MsKotaKab_Id)
        FillOrNothing(objLTKTTransactionCashOut.INDV_NA_KodePos, objSingleResume.TransactionCashOut.INDV_NA_KodePos)
        'corp
        FillOrNothing(objLTKTTransactionCashOut.CORP_FK_MsBentukBadanUsaha_Id, objSingleResume.TransactionCashOut.CORP_FK_MsBentukBadanUsaha_Id)
        FillOrNothing(objLTKTTransactionCashOut.CORP_Nama, objSingleResume.TransactionCashOut.CORP_Nama)
        FillOrNothing(objLTKTTransactionCashOut.CORP_FK_MsBidangUsaha_Id, objSingleResume.TransactionCashOut.CORP_FK_MsBidangUsaha_Id)
        FillOrNothing(objLTKTTransactionCashOut.CORP_TipeAlamat, objSingleResume.TransactionCashOut.CORP_TipeAlamat)
        FillOrNothing(objLTKTTransactionCashOut.CORP_NamaJalan, objSingleResume.TransactionCashOut.CORP_NamaJalan)
        FillOrNothing(objLTKTTransactionCashOut.CORP_RTRW, objSingleResume.TransactionCashOut.CORP_RTRW)
        FillOrNothing(objLTKTTransactionCashOut.CORP_FK_MsKelurahan_Id, objSingleResume.TransactionCashOut.CORP_FK_MsKelurahan_Id)
        FillOrNothing(objLTKTTransactionCashOut.CORP_FK_MsKecamatan_Id, objSingleResume.TransactionCashOut.CORP_FK_MsKecamatan_Id)
        FillOrNothing(objLTKTTransactionCashOut.CORP_FK_MsKotaKab_Id, objSingleResume.TransactionCashOut.CORP_FK_MsKotaKab_Id)
        FillOrNothing(objLTKTTransactionCashOut.CORP_KodePos, objSingleResume.TransactionCashOut.CORP_KodePos)
        FillOrNothing(objLTKTTransactionCashOut.CORP_FK_MsProvince_Id, objSingleResume.TransactionCashOut.CORP_FK_MsProvince_Id)
        FillOrNothing(objLTKTTransactionCashOut.CORP_FK_MsNegara_Id, objSingleResume.TransactionCashOut.CORP_FK_MsNegara_Id)
        FillOrNothing(objLTKTTransactionCashOut.CORP_LN_NamaJalan, objSingleResume.TransactionCashOut.CORP_LN_NamaJalan)
        FillOrNothing(objLTKTTransactionCashOut.CORP_LN_FK_MsNegara_Id, objSingleResume.TransactionCashOut.CORP_LN_FK_MsNegara_Id)
        FillOrNothing(objLTKTTransactionCashOut.CORP_LN_FK_MsProvince_Id, objSingleResume.TransactionCashOut.CORP_LN_FK_MsProvince_Id)
        FillOrNothing(objLTKTTransactionCashOut.CORP_LN_MsKotaKab_Id, objSingleResume.TransactionCashOut.CORP_LN_MsKotaKab_Id)
        FillOrNothing(objLTKTTransactionCashOut.CORP_LN_KodePos, objSingleResume.TransactionCashOut.CORP_LN_KodePos)
        FillOrNothing(objLTKTTransactionCashOut.CORP_NPWP, objSingleResume.TransactionCashOut.CORP_NPWP)
        FillOrNothing(objLTKTTransactionCashOut.CORP_TujuanTransaksi, objSingleResume.TransactionCashOut.CORP_TujuanTransaksi)
        FillOrNothing(objLTKTTransactionCashOut.CORP_SumberDana, objSingleResume.TransactionCashOut.CORP_SumberDana)
        FillOrNothing(objLTKTTransactionCashOut.CORP_NamaBankLain, objSingleResume.TransactionCashOut.CORP_NamaBankLain)
        FillOrNothing(objLTKTTransactionCashOut.CORP_NomorRekeningTujuan, objSingleResume.TransactionCashOut.CORP_NomorRekeningTujuan)


        FillOrNothing(objLTKTTransactionCashOut.Total, objSingleResume.TransactionNominal, True, oDecimal)
        FillOrNothing(objLTKTTransactionCashOut.CreatedBy, Sahassa.AML.Commonly.SessionUserId)
        FillOrNothing(objLTKTTransactionCashOut.CreatedDate, Now(), True, oDate)
    End Sub

    Private Sub FillTransactionCashinObject(ByVal objLTKTDetil As LTKT, _
                                                                                                                 ByVal objSingleResume As ResumeKasMasukKeluarLTKT, _
                                                                                                                 ByVal objLTKTTransactionCashIn As LTKTTransactionCashIn)

        FillOrNothing(objLTKTTransactionCashIn.FK_LTKT_Id, objLTKTDetil.PK_LTKT_Id, True, oInt)
        FillOrNothing(objLTKTTransactionCashIn.PK_LTKTTransactionCashIn_Id, objSingleResume.TransactionCashIn.PK_LTKTTransactionCashIn_Id)
        FillOrNothing(objLTKTTransactionCashIn.TanggalTransaksi, objSingleResume.TransactionCashIn.TanggalTransaksi)
        FillOrNothing(objLTKTTransactionCashIn.NamaKantorPJK, objSingleResume.TransactionCashIn.NamaKantorPJK)
        FillOrNothing(objLTKTTransactionCashIn.FK_MsKotaKab_Id, objSingleResume.TransactionCashIn.FK_MsKotaKab_Id)
        FillOrNothing(objLTKTTransactionCashIn.FK_MsProvince_Id, objSingleResume.TransactionCashIn.FK_MsProvince_Id)
        FillOrNothing(objLTKTTransactionCashIn.NomorRekening, objSingleResume.TransactionCashIn.NomorRekening)
        FillOrNothing(objLTKTTransactionCashIn.TipeTerlapor, objSingleResume.TransactionCashIn.TipeTerlapor)

        'individu
        FillOrNothing(objLTKTTransactionCashIn.INDV_Gelar, objSingleResume.TransactionCashIn.INDV_Gelar)
        FillOrNothing(objLTKTTransactionCashIn.INDV_NamaLengkap, objSingleResume.TransactionCashIn.INDV_NamaLengkap)
        FillOrNothing(objLTKTTransactionCashIn.INDV_TempatLahir, objSingleResume.TransactionCashIn.INDV_TempatLahir)
        FillOrNothing(objLTKTTransactionCashIn.INDV_TanggalLahir, objSingleResume.TransactionCashIn.INDV_TanggalLahir)
        FillOrNothing(objLTKTTransactionCashIn.INDV_Kewarganegaraan, objSingleResume.TransactionCashIn.INDV_Kewarganegaraan)
        FillOrNothing(objLTKTTransactionCashIn.INDV_FK_MsNegara_Id, objSingleResume.TransactionCashIn.INDV_FK_MsNegara_Id)
        'domisili
        FillOrNothing(objLTKTTransactionCashIn.INDV_DOM_NamaJalan, objSingleResume.TransactionCashIn.INDV_DOM_NamaJalan)
        FillOrNothing(objLTKTTransactionCashIn.INDV_DOM_RTRW, objSingleResume.TransactionCashIn.INDV_DOM_RTRW)
        FillOrNothing(objLTKTTransactionCashIn.INDV_DOM_FK_MsKelurahan_Id, objSingleResume.TransactionCashIn.INDV_DOM_FK_MsKelurahan_Id)
        FillOrNothing(objLTKTTransactionCashIn.INDV_DOM_FK_MsKecamatan_Id, objSingleResume.TransactionCashIn.INDV_DOM_FK_MsKecamatan_Id)
        FillOrNothing(objLTKTTransactionCashIn.INDV_DOM_FK_MsKotaKab_Id, objSingleResume.TransactionCashIn.INDV_DOM_FK_MsKotaKab_Id)
        FillOrNothing(objLTKTTransactionCashIn.INDV_DOM_KodePos, objSingleResume.TransactionCashIn.INDV_DOM_KodePos)
        FillOrNothing(objLTKTTransactionCashIn.INDV_DOM_FK_MsProvince_Id, objSingleResume.TransactionCashIn.INDV_DOM_FK_MsProvince_Id)
        FillOrNothing(objLTKTTransactionCashIn.INDV_DOM_FK_MsNegara_Id, objSingleResume.TransactionCashIn.INDV_DOM_FK_MsNegara_Id)
        'identitas
        FillOrNothing(objLTKTTransactionCashIn.INDV_ID_NamaJalan, objSingleResume.TransactionCashIn.INDV_ID_NamaJalan)
        FillOrNothing(objLTKTTransactionCashIn.INDV_ID_RTRW, objSingleResume.TransactionCashIn.INDV_ID_RTRW)
        FillOrNothing(objLTKTTransactionCashIn.INDV_ID_FK_MsKelurahan_Id, objSingleResume.TransactionCashIn.INDV_ID_FK_MsKelurahan_Id)
        FillOrNothing(objLTKTTransactionCashIn.INDV_ID_FK_MsKecamatan_Id, objSingleResume.TransactionCashIn.INDV_ID_FK_MsKecamatan_Id)
        FillOrNothing(objLTKTTransactionCashIn.INDV_ID_FK_MsKotaKab_Id, objSingleResume.TransactionCashIn.INDV_ID_FK_MsKotaKab_Id)
        FillOrNothing(objLTKTTransactionCashIn.INDV_ID_KodePos, objSingleResume.TransactionCashIn.INDV_ID_KodePos)
        FillOrNothing(objLTKTTransactionCashIn.INDV_ID_FK_MsProvince_Id, objSingleResume.TransactionCashIn.INDV_ID_FK_MsProvince_Id)
        FillOrNothing(objLTKTTransactionCashIn.INDV_ID_FK_MsNegara_Id, objSingleResume.TransactionCashIn.INDV_ID_FK_MsNegara_Id)
        'Negara Asing
        FillOrNothing(objLTKTTransactionCashIn.INDV_NA_NamaJalan, objSingleResume.TransactionCashIn.INDV_NA_NamaJalan)
        FillOrNothing(objLTKTTransactionCashIn.INDV_NA_FK_MsNegara_Id, objSingleResume.TransactionCashIn.INDV_NA_FK_MsNegara_Id)
        FillOrNothing(objLTKTTransactionCashIn.INDV_NA_FK_MsProvince_Id, objSingleResume.TransactionCashIn.INDV_NA_FK_MsProvince_Id)
        FillOrNothing(objLTKTTransactionCashIn.INDV_NA_FK_MsKotaKab_Id, objSingleResume.TransactionCashIn.INDV_NA_FK_MsKotaKab_Id)
        FillOrNothing(objLTKTTransactionCashIn.INDV_NA_KodePos, objSingleResume.TransactionCashIn.INDV_NA_KodePos)
        'individu
        FillOrNothing(objLTKTTransactionCashIn.INDV_FK_MsIDType_Id, objSingleResume.TransactionCashIn.INDV_FK_MsIDType_Id)
        FillOrNothing(objLTKTTransactionCashIn.INDV_NomorId, objSingleResume.TransactionCashIn.INDV_NomorId)
        FillOrNothing(objLTKTTransactionCashIn.INDV_NPWP, objSingleResume.TransactionCashIn.INDV_NPWP)
        FillOrNothing(objLTKTTransactionCashIn.INDV_FK_MsPekerjaan_Id, objSingleResume.TransactionCashIn.INDV_FK_MsPekerjaan_Id)
        FillOrNothing(objLTKTTransactionCashIn.INDV_Jabatan, objSingleResume.TransactionCashIn.INDV_Jabatan)
        FillOrNothing(objLTKTTransactionCashIn.INDV_PenghasilanRataRata, objSingleResume.TransactionCashIn.INDV_PenghasilanRataRata)
        FillOrNothing(objLTKTTransactionCashIn.INDV_TempatBekerja, objSingleResume.TransactionCashIn.INDV_TempatBekerja)
        FillOrNothing(objLTKTTransactionCashIn.INDV_TujuanTransaksi, objSingleResume.TransactionCashIn.INDV_TujuanTransaksi)
        FillOrNothing(objLTKTTransactionCashIn.INDV_SumberDana, objSingleResume.TransactionCashIn.INDV_SumberDana)
        FillOrNothing(objLTKTTransactionCashIn.INDV_NamaBankLain, objSingleResume.TransactionCashIn.INDV_NamaBankLain)
        FillOrNothing(objLTKTTransactionCashIn.INDV_NomorRekeningTujuan, objSingleResume.TransactionCashIn.INDV_NomorRekeningTujuan)
        'corp
        FillOrNothing(objLTKTTransactionCashIn.CORP_FK_MsBentukBadanUsaha_Id, objSingleResume.TransactionCashIn.CORP_FK_MsBentukBadanUsaha_Id)
        FillOrNothing(objLTKTTransactionCashIn.CORP_Nama, objSingleResume.TransactionCashIn.CORP_Nama)
        FillOrNothing(objLTKTTransactionCashIn.CORP_FK_MsBidangUsaha_Id, objSingleResume.TransactionCashIn.CORP_FK_MsBidangUsaha_Id)
        FillOrNothing(objLTKTTransactionCashIn.CORP_TipeAlamat, objSingleResume.TransactionCashIn.CORP_TipeAlamat)
        FillOrNothing(objLTKTTransactionCashIn.CORP_NamaJalan, objSingleResume.TransactionCashIn.CORP_NamaJalan)
        FillOrNothing(objLTKTTransactionCashIn.CORP_RTRW, objSingleResume.TransactionCashIn.CORP_RTRW)
        FillOrNothing(objLTKTTransactionCashIn.CORP_FK_MsKelurahan_Id, objSingleResume.TransactionCashIn.CORP_FK_MsKelurahan_Id)
        FillOrNothing(objLTKTTransactionCashIn.CORP_FK_MsKecamatan_Id, objSingleResume.TransactionCashIn.CORP_FK_MsKecamatan_Id)
        FillOrNothing(objLTKTTransactionCashIn.CORP_FK_MsKotaKab_Id, objSingleResume.TransactionCashIn.CORP_FK_MsKotaKab_Id)
        FillOrNothing(objLTKTTransactionCashIn.CORP_KodePos, objSingleResume.TransactionCashIn.CORP_KodePos)
        FillOrNothing(objLTKTTransactionCashIn.CORP_FK_MsProvince_Id, objSingleResume.TransactionCashIn.CORP_FK_MsProvince_Id)
        FillOrNothing(objLTKTTransactionCashIn.CORP_FK_MsNegara_Id, objSingleResume.TransactionCashIn.CORP_FK_MsNegara_Id)
        FillOrNothing(objLTKTTransactionCashIn.CORP_LN_NamaJalan, objSingleResume.TransactionCashIn.CORP_LN_NamaJalan)
        FillOrNothing(objLTKTTransactionCashIn.CORP_LN_FK_MsNegara_Id, objSingleResume.TransactionCashIn.CORP_LN_FK_MsNegara_Id)
        FillOrNothing(objLTKTTransactionCashIn.CORP_LN_FK_MsProvince_Id, objSingleResume.TransactionCashIn.CORP_LN_FK_MsProvince_Id)
        FillOrNothing(objLTKTTransactionCashIn.CORP_LN_MsKotaKab_Id, objSingleResume.TransactionCashIn.CORP_LN_MsKotaKab_Id)
        FillOrNothing(objLTKTTransactionCashIn.CORP_LN_KodePos, objSingleResume.TransactionCashIn.CORP_LN_KodePos)
        FillOrNothing(objLTKTTransactionCashIn.CORP_NPWP, objSingleResume.TransactionCashIn.CORP_NPWP)
        FillOrNothing(objLTKTTransactionCashIn.CORP_TujuanTransaksi, objSingleResume.TransactionCashIn.CORP_TujuanTransaksi)
        FillOrNothing(objLTKTTransactionCashIn.CORP_SumberDana, objSingleResume.TransactionCashIn.CORP_SumberDana)
        FillOrNothing(objLTKTTransactionCashIn.CORP_NamaBankLain, objSingleResume.TransactionCashIn.CORP_NamaBankLain)
        FillOrNothing(objLTKTTransactionCashIn.CORP_NomorRekeningTujuan, objSingleResume.TransactionCashIn.CORP_NomorRekeningTujuan)

        FillOrNothing(objLTKTTransactionCashIn.Total, objSingleResume.TransactionNominal, True, oDecimal)
        FillOrNothing(objLTKTTransactionCashIn.CreatedBy, Sahassa.AML.Commonly.SessionUserId)
        FillOrNothing(objLTKTTransactionCashIn.CreatedDate, Now())
    End Sub

    Private Sub FillLTKTobject(ByVal objLTKTDetil As LTKT)
        FillOrNothing(objLTKTDetil.NamaPJKPelapor, txtUmumPJKPelapor.Text)
        FillOrNothing(objLTKTDetil.TanggalLaporan, txtTglLaporan.Text, True, oDate)
        FillOrNothing(objLTKTDetil.NamaPejabatPJKPelapor, txtPejabatPelapor.Text)
        FillOrNothing(objLTKTDetil.JenisLaporan, cboTipeLaporan.SelectedIndex.ToString)
        FillOrNothing(objLTKTDetil.NoLTKTKoreksi, txtNoLTKTKoreksi.Text)
        FillOrNothing(objLTKTDetil.InformasiLainnya, txtLTKTInfoLainnya.Text)
        FillOrNothing(objLTKTDetil.DebetCredit, cbodebetcredit.SelectedValue)


        FillOrNothing(objLTKTDetil.FK_MsKepemilikan_Id, cboTerlaporKepemilikan.SelectedValue, True, oInt)
        FillOrNothing(objLTKTDetil.NoRekening, txtTerlaporNoRekening.Text)
        If txtCIFNo.Text <> "" Then
            FillOrNothing(objLTKTDetil.CIFNo, txtCIFNo.Text)
        End If
        FillOrNothing(objLTKTDetil.TipeTerlapor, rblTerlaporTipePelapor.SelectedValue, True, oByte)
        'individu
        FillOrNothing(objLTKTDetil.INDV_Gelar, txtTerlaporGelar.Text)
        FillOrNothing(objLTKTDetil.INDV_NamaLengkap, txtTerlaporNamaLengkap.Text)
        FillOrNothing(objLTKTDetil.INDV_TempatLahir, txtTerlaporTempatLahir.Text)
        FillOrNothing(objLTKTDetil.INDV_TanggalLahir, txtTerlaporTglLahir.Text, True, oDate)
        FillOrNothing(objLTKTDetil.INDV_Kewarganegaraan, rblTerlaporKewarganegaraan.SelectedValue.ToString)
        FillOrNothing(objLTKTDetil.INDV_FK_MsNegara_Id, cboTerlaporNegara.SelectedValue, True, oInt)
        FillOrNothing(objLTKTDetil.INDV_FK_MsIDType_Id, cboTerlaporJenisDocID.SelectedValue, True, oInt)
        FillOrNothing(objLTKTDetil.INDV_NomorId, txtTerlaporNomorID.Text)
        FillOrNothing(objLTKTDetil.INDV_NPWP, txtTerlaporNPWP.Text)
        FillOrNothing(objLTKTDetil.INDV_FK_MsPekerjaan_Id, hfTerlaporPekerjaan.Value, True, oInt)
        FillOrNothing(objLTKTDetil.INDV_Jabatan, txtTerlaporJabatan.Text)
        FillOrNothing(objLTKTDetil.INDV_PenghasilanRataRata, txtTerlaporPenghasilanRataRata.Text)
        FillOrNothing(objLTKTDetil.INDV_TempatBekerja, txtTerlaporTempatKerja.Text)
        'domisili
        FillOrNothing(objLTKTDetil.INDV_DOM_NamaJalan, txtTerlaporDOMNamaJalan.Text)
        FillOrNothing(objLTKTDetil.INDV_DOM_RTRW, txtTerlaporDOMRTRW.Text)
        FillOrNothing(objLTKTDetil.INDV_DOM_FK_MsKelurahan_Id, hfTerlaporDOMKelurahan.Value, True, OLong)
        FillOrNothing(objLTKTDetil.INDV_DOM_FK_MsKecamatan_Id, hfTerlaporDOMKecamatan.Value, True, oInt)
        FillOrNothing(objLTKTDetil.INDV_DOM_FK_MsKotaKab_Id, hfTerlaporDOMKotaKab.Value, True, oInt)
        FillOrNothing(objLTKTDetil.INDV_DOM_KodePos, txtTerlaporDOMKodePos.Text)
        FillOrNothing(objLTKTDetil.INDV_DOM_FK_MsProvince_Id, hfTerlaporDOMProvinsi.Value, True, oInt)
        FillOrNothing(objLTKTDetil.INDV_DOM_FK_MsNegara_Id, hfTerlaporDOMNegara.Value, True, oInt)
        'identitas
        FillOrNothing(objLTKTDetil.INDV_ID_NamaJalan, txtTerlaporIDNamaJalan.Text)
        FillOrNothing(objLTKTDetil.INDV_ID_RTRW, txtTerlaporIDRTRW.Text)
        FillOrNothing(objLTKTDetil.INDV_ID_FK_MsKelurahan_Id, hfTerlaporIDKelurahan.Value, True, OLong)
        FillOrNothing(objLTKTDetil.INDV_ID_FK_MsKecamatan_Id, hfTerlaporIDKecamatan.Value, True, oInt)
        FillOrNothing(objLTKTDetil.INDV_ID_FK_MsKotaKab_Id, hfTerlaporIDKotaKab.Value, True, oInt)
        FillOrNothing(objLTKTDetil.INDV_ID_KodePos, txtTerlaporIDKodePos.Text)
        FillOrNothing(objLTKTDetil.INDV_ID_FK_MsProvince_Id, hfTerlaporIDProvinsi.Value, True, oInt)
        FillOrNothing(objLTKTDetil.INDV_ID_FK_MsNegara_Id, hfTerlaporIDNegara.Value, True, oInt)

        'negara asing
        FillOrNothing(objLTKTDetil.INDV_NA_NamaJalan, txtTerlaporNANamaJalan.Text)
        FillOrNothing(objLTKTDetil.INDV_NA_FK_MsNegara_Id, hfTerlaporIDNegara.Value, True, oInt)
        
        FillOrNothing(objLTKTDetil.INDV_NA_FK_MsProvince_Id, hfTerlaporNAProvinsi.Value, True, oInt)
        FillOrNothing(objLTKTDetil.INDV_NA_FK_MsKotaKab_Id, hfTerlaporIDKota.Value, True, oInt)
        FillOrNothing(objLTKTDetil.INDV_NA_KodePos, txtTerlaporNAKodePos.Text)

        'corp
        If cboTerlaporCORPBentukBadanUsaha.SelectedIndex <> 0 Then
            FillOrNothing(objLTKTDetil.CORP_FK_MsBentukBadanUsaha_Id, cboTerlaporCORPBentukBadanUsaha.SelectedValue, True, oInt)
        End If
        FillOrNothing(objLTKTDetil.CORP_Nama, txtTerlaporCORPNama.Text)
        FillOrNothing(objLTKTDetil.CORP_FK_MsBidangUsaha_Id, hfTerlaporCORPBidangUsaha.Value, True, oInt)

        If rblTerlaporCORPTipeAlamat.SelectedIndex <> 0 Then
            FillOrNothing(objLTKTDetil.CORP_TipeAlamat, rblTerlaporCORPTipeAlamat.SelectedIndex, True, oByte)
        End If
        FillOrNothing(objLTKTDetil.CORP_NamaJalan, txtTerlaporCORPDLNamaJalan.Text)
        FillOrNothing(objLTKTDetil.CORP_RTRW, txtTerlaporCORPDLRTRW.Text)
        FillOrNothing(objLTKTDetil.CORP_FK_MsKelurahan_Id, hfTerlaporCORPDLKelurahan.Value, True, OLong)
        FillOrNothing(objLTKTDetil.CORP_FK_MsKecamatan_Id, hfTerlaporCORPDLKecamatan.Value, True, oInt)
        FillOrNothing(objLTKTDetil.CORP_FK_MsKotaKab_Id, hfTerlaporCORPDLKotaKab.Value, True, oInt)
        FillOrNothing(objLTKTDetil.CORP_KodePos, txtTerlaporCORPDLKodePos.Text)
        FillOrNothing(objLTKTDetil.CORP_FK_MsProvince_Id, hfTerlaporCORPDLProvinsi.Value, True, oInt)

        FillOrNothing(objLTKTDetil.CORP_FK_MsNegara_Id, hfTerlaporCORPDLNegara.Value, True, oInt)

        FillOrNothing(objLTKTDetil.CORP_LN_NamaJalan, txtTerlaporCORPLNNamaJalan.Text)

        FillOrNothing(objLTKTDetil.CORP_LN_FK_MsNegara_Id, hfTerlaporCORPLNNegara.Value, True, oInt)

        FillOrNothing(objLTKTDetil.CORP_LN_FK_MsProvince_Id, hfTerlaporCORPLNProvinsi.Value, True, oInt)
        FillOrNothing(objLTKTDetil.CORP_LN_MsKotaKab_Id, hfTerlaporCORPLNKota.Value, True, oInt)
        FillOrNothing(objLTKTDetil.CORP_LN_KodePos, txtTerlaporCORPLNKodePos.Text)
        FillOrNothing(objLTKTDetil.CORP_NPWP, txtTerlaporCORPNPWP.Text)
        FillOrNothing(objLTKTDetil.CreatedBy, Sahassa.AML.Commonly.SessionUserId)
        FillOrNothing(objLTKTDetil.CreatedDate, Now(), True, oDate)
    End Sub

#End Region

#Region "Fill Control From Object..."
    Sub loadLTKTToField()
        Dim Lobjtemp As TList(Of LTKT_ApprovalDetail) = DataRepository.LTKT_ApprovalDetailProvider.GetPaged(LTKT_ApprovalDetailColumn.FK_LTKT_Approval_Id.ToString & " = " & getAPPROVALPK.ToString, "", 0, Integer.MaxValue, 0)
        If Lobjtemp.Count > 0 Then
            If Lobjtemp.Count = 0 Then Throw New Exception("Data not found")
            Using objLTKT As LTKT_ApprovalDetail = Lobjtemp(0)
                If Not IsNothing(objLTKT) Then

                    'Ambil data buat cari PK Negara, Provinsi, KotaKab, dkk (ini variable yang bakal dpke berkali2)
                    Dim i As Integer = 0 'buat iterasi
                    Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetAll
                    Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
                    Dim objKelurahan As TList(Of MsKelurahan) = DataRepository.MsKelurahanProvider.GetAll
                    Dim objSKelurahan As MsKelurahan 'Penampung hasil search kelurahan
                    Dim objKecamatan As TList(Of MsKecamatan) = DataRepository.MsKecamatanProvider.GetAll
                    Dim objSkecamatan As MsKecamatan 'Penampung hasil search kecamatan
                    Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetAll
                    Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi
                    'Dim objMapNegara As TList(Of MappingMsNegaraNCBSPPATK) = DataRepository.MappingMsNegaraNCBSPPATKProvider.GetAll
                    'Dim objSMapNegara As MappingMsNegaraNCBSPPATK 'Penampung hasil search mappingnegara yang pke picker
                    Dim objNegara As TList(Of MsNegaraNCBS) = DataRepository.MsNegaraNCBSProvider.GetAll
                    Dim objSnegara As MsNegaraNCBS 'Penampung hasil search negara yang pke picker
                    Dim objPekerjaan As TList(Of MsPekerjaan) = DataRepository.MsPekerjaanProvider.GetAll
                    Dim objSPekerjaan As MsPekerjaan 'Penampung hasil search pekerjaan
                    Dim objBidangUsaha As TList(Of MsBidangUsaha) = DataRepository.MsBidangUsahaProvider.GetAll
                    Dim objSBidangUsaha As MsBidangUsaha 'Penampung hasil search bidang usaha

                    txtUmumPJKPelapor.Text = Safe(objLTKT.NamaPJKPelapor)
                    txtTglLaporan.Text = FormatDate(objLTKT.TanggalLaporan)
                    txtPejabatPelapor.Text = Safe(objLTKT.NamaPejabatPJKPelapor)
                    If objLTKT.NoLTKTKoreksi <> "" Then
                        cboTipeLaporan.SelectedIndex = 1
                        txtNoLTKTKoreksi.Text = Safe(objLTKT.NoLTKTKoreksi)
                        tipeLaporanCheck()
                    End If
                    i = 0
                    For Each listKepemilikan As ListItem In cboTerlaporKepemilikan.Items
                        If objLTKT.FK_MsKepemilikan_ID.ToString = listKepemilikan.Value Then
                            cboTerlaporKepemilikan.SelectedIndex = i
                            Exit For
                        End If
                        i = i + 1
                    Next
                    txtLTKTInfoLainnya.Text = Safe(objLTKT.InformasiLainnya)
                Me.cbodebetcredit.SelectedValue = Safe(objLTKT.DebetCredit)
                    txtTerlaporNoRekening.Text = Safe(objLTKT.NoRekening)
                    txtCIFNo.Text = Safe(objLTKT.CIFNo)

                    SafeSelectedValue(rblTerlaporTipePelapor, objLTKT.TipeTerlapor)
                    LTKTTipeTerlaporChange()
                    txtTerlaporGelar.Text = Safe(objLTKT.INDV_Gelar)
                    txtTerlaporNamaLengkap.Text = Safe(objLTKT.INDV_NamaLengkap)
                    txtTerlaporTempatLahir.Text = Safe(objLTKT.INDV_TempatLahir)
                    txtTerlaporTglLahir.Text = FormatDate(objLTKT.INDV_TanggalLahir)
                    SafeSelectedValue(rblTerlaporKewarganegaraan, objLTKT.INDV_Kewarganegaraan)
                    SafeSelectedValue(cboTerlaporNegara, objLTKT.INDV_FK_MsNegara_Id.GetValueOrDefault)
                    'i = 0
                    'For Each listNegara As ListItem In cboTerlaporNegara.Items
                    '    If objLTKT.INDV_FK_MsNegara_Id.GetValueOrDefault.ToString = listNegara.Value Then
                    '        cboTerlaporNegara.SelectedValue = i
                    '        Exit For
                    '    End If
                    '    i = i + 1
                    'Next
                    txtTerlaporDOMNamaJalan.Text = Safe(objLTKT.INDV_DOM_NamaJalan)
                    txtTerlaporDOMRTRW.Text = Safe(objLTKT.INDV_DOM_RTRW)
                    objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objLTKT.INDV_DOM_FK_MsKelurahan_Id)
                    If Not IsNothing(objSKelurahan) Then
                        txtTerlaporDOMKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
                        hfTerlaporDOMKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
                    End If
                    objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objLTKT.INDV_DOM_FK_MsKecamatan_Id)
                    If Not IsNothing(objSkecamatan) Then
                        txtTerlaporDOMKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
                        hfTerlaporDOMKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
                    End If
                    objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objLTKT.INDV_DOM_FK_MsKotaKab_Id)
                    If Not IsNothing(objSkotaKab) Then
                        txtTerlaporDOMKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                        hfTerlaporDOMKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                    End If
                    txtTerlaporDOMKodePos.Text = Safe(objLTKT.INDV_DOM_KodePos)
                    objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objLTKT.INDV_DOM_FK_MsProvince_Id)
                    If Not IsNothing(objSProvinsi) Then
                        txtTerlaporDOMProvinsi.Text = Safe(objSProvinsi.Nama)
                        hfTerlaporDOMProvinsi.Value = Safe(objSProvinsi.IdProvince)
                    End If

                    Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objLTKT.INDV_DOM_FK_MsNegara_Id.GetValueOrDefault(0))
                        If Not objNegaraRef Is Nothing Then

                            txtTerlaporDOMNegara.Text = Safe(objNegaraRef.NamaNegara)
                            hfTerlaporDOMNegara.Value = Safe(objNegaraRef.IDNegara)
                        End If
                    End Using

                    txtTerlaporIDNamaJalan.Text = Safe(objLTKT.INDV_ID_NamaJalan)
                    txtTerlaporIDRTRW.Text = Safe(objLTKT.INDV_ID_RTRW)
                    objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objLTKT.INDV_ID_FK_MsKelurahan_Id)
                    If Not IsNothing(objSKelurahan) Then
                        txtTerlaporIDKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
                        hfTerlaporIDKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
                    End If
                    objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objLTKT.INDV_ID_FK_MsKecamatan_Id)
                    If Not IsNothing(objSkecamatan) Then
                        txtTerlaporIDKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
                        hfTerlaporIDKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
                    End If
                    objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objLTKT.INDV_ID_FK_MsKotaKab_Id)
                    If Not IsNothing(objSkotaKab) Then
                        txtTerlaporIDKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                        hfTerlaporIDKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                    End If
                    txtTerlaporIDKodePos.Text = Safe(objLTKT.INDV_ID_KodePos)
                    objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objLTKT.INDV_ID_FK_MsProvince_Id)
                    If Not IsNothing(objSProvinsi) Then
                        txtTerlaporIDProvinsi.Text = Safe(objSProvinsi.Nama)
                        hfTerlaporIDProvinsi.Value = Safe(objSProvinsi.IdProvince)
                    End If

                    Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objLTKT.INDV_ID_FK_MsNegara_Id.GetValueOrDefault(0))
                        If Not objNegaraRef Is Nothing Then
                            txtTerlaporIDNegara.Text = Safe(objNegaraRef.NamaNegara)
                            hfTerlaporIDNegara.Value = Safe(objNegaraRef.IDNegara)
                        End If
                    End Using

                    txtTerlaporNANamaJalan.Text = Safe(objLTKT.INDV_NA_NamaJalan)
                    'objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objLTKT.INDV_NA_FK_MsNegara_Id)
                    'If Not IsNothing(objSMapNegara) Then
                    '    objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                    '    If Not IsNothing(objSnegara) Then
                    '        txtTerlaporNANegara.Text = Safe(objSnegara.NamaNegara)
                    '        hfTerlaporIDNegara.Value = Safe(objSnegara.IDNegaraNCBS)
                    '    End If
                    'End If
                    Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objLTKT.INDV_NA_FK_MsNegara_Id.GetValueOrDefault(0))
                        If Not objNegaraRef Is Nothing Then
                            txtTerlaporNANegara.Text = Safe(objNegaraRef.NamaNegara)
                            hfTerlaporNANegara.Value = Safe(objNegaraRef.IDNegara)
                        End If
                    End Using
                    objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objLTKT.INDV_NA_FK_MsProvince_Id)
                    If Not IsNothing(objSProvinsi) Then
                        txtTerlaporNAProvinsi.Text = Safe(objSProvinsi.Nama)
                        hfTerlaporNAProvinsi.Value = Safe(objSProvinsi.IdProvince)
                    End If
                    objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objLTKT.INDV_NA_FK_MsKotaKab_Id)
                    If Not IsNothing(objSkotaKab) Then
                        txtTerlaporNAKota.Text = Safe(objSkotaKab.NamaKotaKab)
                        hfTerlaporIDKota.Value = Safe(objSkotaKab.IDKotaKab)
                    End If
                    txtTerlaporNAKodePos.Text = Safe(objLTKT.INDV_NA_KodePos)
                    i = 0
                    For Each jenisDoc As ListItem In cboTerlaporJenisDocID.Items
                        If objLTKT.INDV_FK_MsIDType_Id.GetValueOrDefault.ToString = jenisDoc.Value Then
                            cboTerlaporJenisDocID.SelectedIndex = i
                            Exit For
                        End If
                        i = i + 1
                    Next
                    txtTerlaporNomorID.Text = Safe(objLTKT.INDV_NomorId)
                    txtTerlaporNPWP.Text = Safe(objLTKT.INDV_NPWP)
                    objSPekerjaan = objPekerjaan.Find(MsPekerjaanColumn.IDPekerjaan, objLTKT.INDV_FK_MsPekerjaan_Id)
                    If Not IsNothing(objSPekerjaan) Then
                        txtTerlaporPekerjaan.Text = Safe(objSPekerjaan.NamaPekerjaan)
                        hfTerlaporPekerjaan.Value = Safe(objSPekerjaan.IDPekerjaan)
                    End If
                    txtTerlaporJabatan.Text = Safe(objLTKT.INDV_Jabatan)
                    txtTerlaporPenghasilanRataRata.Text = Safe(objLTKT.INDV_PenghasilanRataRata)
                    txtTerlaporTempatKerja.Text = Safe(objLTKT.INDV_TempatBekerja)
                    i = 0
                    For Each listBentukBadanUsaha As ListItem In cboTerlaporCORPBentukBadanUsaha.Items
                        If objLTKT.CORP_FK_MsBentukBadanUsaha_Id.ToString = listBentukBadanUsaha.Value Then
                            cboTerlaporCORPBentukBadanUsaha.SelectedIndex = i
                            Exit For
                        End If
                        i = i + 1
                    Next
                    txtTerlaporCORPNama.Text = Safe(objLTKT.CORP_Nama)
                    objSBidangUsaha = objBidangUsaha.Find(MsBidangUsahaColumn.IdBidangUsaha, objLTKT.CORP_FK_MsBidangUsaha_Id)
                    If Not IsNothing(objSBidangUsaha) Then
                        txtTerlaporCORPBidangUsaha.Text = Safe(objSBidangUsaha.NamaBidangUsaha)
                        hfTerlaporCORPBidangUsaha.Value = Safe(objSBidangUsaha.IdBidangUsaha)
                    End If
                    If objLTKT.CORP_TipeAlamat.GetValueOrDefault <> Nothing Then
                        SafeNumIndex(rblTerlaporCORPTipeAlamat, objLTKT.CORP_TipeAlamat)
                    End If
                    txtTerlaporCORPDLNamaJalan.Text = Safe(objLTKT.CORP_NamaJalan)
                    txtTerlaporCORPDLRTRW.Text = Safe(objLTKT.CORP_RTRW)

                    objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objLTKT.CORP_FK_MsKelurahan_Id)
                    If Not IsNothing(objSKelurahan) Then
                        txtTerlaporCORPDLKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
                        hfTerlaporCORPDLKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
                    End If
                    objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objLTKT.CORP_FK_MsKecamatan_Id)
                    If Not IsNothing(objSkecamatan) Then
                        txtTerlaporCORPDLKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
                        hfTerlaporCORPDLKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
                    End If
                    objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objLTKT.CORP_FK_MsKotaKab_Id)
                    If Not IsNothing(objSkotaKab) Then
                        txtTerlaporCORPDLKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                        hfTerlaporCORPDLKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                    End If
                    txtTerlaporCORPDLKodePos.Text = Safe(objLTKT.CORP_KodePos)
                    objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objLTKT.CORP_FK_MsProvince_Id)
                    If Not IsNothing(objSProvinsi) Then
                        txtTerlaporCORPDLProvinsi.Text = Safe(objSProvinsi.Nama)
                        hfTerlaporCORPDLProvinsi.Value = Safe(objSProvinsi.IdProvince)
                    End If
                    'objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objLTKT.CORP_FK_MsNegara_Id)
                    'If Not IsNothing(objSMapNegara) Then
                    '    objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                    '    If Not IsNothing(objSnegara) Then
                    '        txtTerlaporCORPDLNegara.Text = Safe(objSnegara.NamaNegara)
                    '        hfTerlaporCORPDLNegara.Value = Safe(objSnegara.IDNegaraNCBS)
                    '    End If
                    'End If
                    Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objLTKT.CORP_FK_MsNegara_Id.GetValueOrDefault(0))
                        If Not objNegaraRef Is Nothing Then
                            txtTerlaporCORPDLNegara.Text = Safe(objNegaraRef.NamaNegara)
                            hfTerlaporCORPDLNegara.Value = Safe(objNegaraRef.IDNegara)
                        End If
                    End Using
                    txtTerlaporCORPLNNamaJalan.Text = Safe(objLTKT.CORP_LN_NamaJalan)
                    'objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objLTKT.CORP_LN_FK_MsNegara_Id)
                    'If Not IsNothing(objSMapNegara) Then
                    '    objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                    '    If Not IsNothing(objSnegara) Then
                    '        txtTerlaporCORPLNNegara.Text = Safe(objSnegara.NamaNegara)
                    '        hfTerlaporCORPLNNegara.Value = Safe(objSnegara.IDNegaraNCBS)
                    '    End If
                    'End If
                    Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objLTKT.CORP_LN_FK_MsNegara_Id.GetValueOrDefault(0))
                        If Not objNegaraRef Is Nothing Then
                            txtTerlaporCORPLNNegara.Text = Safe(objNegaraRef.NamaNegara)
                            hfTerlaporCORPLNNegara.Value = Safe(objNegaraRef.IDNegara)
                        End If
                    End Using
                    objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objLTKT.CORP_LN_FK_MsProvince_Id)
                    If Not IsNothing(objSProvinsi) Then
                        txtTerlaporCORPLNProvinsi.Text = Safe(objSProvinsi.Nama)
                        hfTerlaporCORPLNProvinsi.Value = Safe(objSProvinsi.IdProvince)
                    End If
                    objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objLTKT.CORP_LN_MsKotaKab_Id)
                    If Not IsNothing(objSkotaKab) Then
                        txtTerlaporCORPLNKota.Text = Safe(objSkotaKab.NamaKotaKab)
                        hfTerlaporCORPLNKota.Value = Safe(objSkotaKab.IDKotaKab)
                    End If
                    txtTerlaporCORPLNKodePos.Text = Safe(objLTKT.CORP_LN_KodePos)
                    txtTerlaporCORPNPWP.Text = Safe(objLTKT.CORP_NPWP)

                End If
            End Using
        End If
    End Sub
#End Region

#End Region

#Region "events..."

    Protected Sub old_EditResume_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            clearKasMasukKasKeluarOld()
            Dim objgridviewrow As GridViewRow = CType(CType(sender, LinkButton).NamingContainer, GridViewRow)
            SetnGetRowEditOLD = objgridviewrow.RowIndex


            'LoadData ke KasMasuk dan Keluar
            Dim objResume As List(Of ResumeKasMasukKeluarLTKT) = SetnGetResumeKasMasukKasKeluarOLD
            Dim defaultcurrency As Integer
            Using objparam As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)
                defaultcurrency = objparam.MsSystemParameter_Value
            End Using
            'Ambil data buat cari PK Negara, Provinsi, KotaKab, dkk (ini variable yang bakal dpke berkali2)
            Dim i As Integer = 0 'buat iterasi
            Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetAll
            Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
            Dim objKelurahan As TList(Of MsKelurahan) = DataRepository.MsKelurahanProvider.GetAll
            Dim objSKelurahan As MsKelurahan 'Penampung hasil search kelurahan
            Dim objKecamatan As TList(Of MsKecamatan) = DataRepository.MsKecamatanProvider.GetAll
            Dim objSkecamatan As MsKecamatan 'Penampung hasil search kecamatan
            Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetAll
            Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi
            Dim objMapNegara As TList(Of MappingMsNegaraNCBSPPATK) = DataRepository.MappingMsNegaraNCBSPPATKProvider.GetAll
            Dim objSMapNegara As MappingMsNegaraNCBSPPATK 'Penampung hasil search mappingnegara yang pke picker
            Dim objNegara As TList(Of MsNegaraNCBS) = DataRepository.MsNegaraNCBSProvider.GetAll
            Dim objSnegara As MsNegaraNCBS 'Penampung hasil search negara yang pke picker
            Dim objPekerjaan As TList(Of MsPekerjaan) = DataRepository.MsPekerjaanProvider.GetAll
            Dim objSPekerjaan As MsPekerjaan 'Penampung hasil search pekerjaan
            Dim objBidangUsaha As TList(Of MsBidangUsaha) = DataRepository.MsBidangUsahaProvider.GetAll
            Dim objSBidangUsaha As MsBidangUsaha 'Penampung hasil search bidang usaha

            If objResume(SetnGetRowEditOLD).Kas = "Kas Masuk" Then

                old_MultiView1.ActiveViewIndex = 0
                old_Menu1.Items.Item(0).Selected = True

                'LoadData ke DetailKasMasuk

                Dim detilKasmasuk As Decimal = 0
                Dim objDetailKasMasuk As List(Of LTKTDetailCashInApprovalDetail) = SetnGetgrvTRXKMDetilValutaAsingOLD
                For Each obj As LTKTDetailCashInTransaction In objResume(SetnGetRowEditOLD).DetailTransactionCashIn
                    Dim singleDetailKasMasuk As New LTKTDetailCashInApprovalDetail

                    If obj.Asing_TotalKasMasukDalamRupiah.HasValue Then
                        If obj.Asing_TotalKasMasukDalamRupiah.Value > 0 Then
                            If obj.Asing_KursTransaksi.HasValue Then
                                If obj.Asing_KursTransaksi.Value > 0 Then
                                    Try
                                        singleDetailKasMasuk.Jumlah = (CDec(obj.Asing_TotalKasMasukDalamRupiah.GetValueOrDefault) / CDec(obj.Asing_KursTransaksi.GetValueOrDefault)).ToString
                                    Catch ex As Exception
                                        singleDetailKasMasuk.Jumlah = obj.Asing_TotalKasMasukDalamRupiah.GetValueOrDefault
                                    End Try
                                Else
                                    singleDetailKasMasuk.Jumlah = obj.Asing_TotalKasMasukDalamRupiah.GetValueOrDefault
                                End If
                            End If
                            singleDetailKasMasuk.JumlahRp = Safe(obj.Asing_TotalKasMasukDalamRupiah)
                            singleDetailKasMasuk.KursTransaksi = Safe(obj.Asing_KursTransaksi)
                            Dim ID As Decimal = 0
                            If obj.Asing_FK_MsCurrency_Id.HasValue Then
                                ID = obj.Asing_FK_MsCurrency_Id
                            Else 'Jika Nothing pake IDR
                                ID = defaultcurrency
                            End If

                            Dim objKurs As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(ID)
                            If Not IsNothing(objKurs) Then
                                singleDetailKasMasuk.MataUang = Safe(objKurs.Code) & " - " & Safe(objKurs.Name)
                            End If
                            objDetailKasMasuk.Add(singleDetailKasMasuk)
                        Else
                            If obj.KasMasuk.HasValue Then
                                singleDetailKasMasuk.Jumlah = 0
                                singleDetailKasMasuk.JumlahRp = obj.KasMasuk.GetValueOrDefault
                                singleDetailKasMasuk.KursTransaksi = 0
                                Dim objKurs As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(defaultcurrency) ' Indonesia IDR
                                If Not IsNothing(objKurs) Then
                                    singleDetailKasMasuk.MataUang = objKurs.Code & " - " & objKurs.Name
                                End If
                                detilKasmasuk += obj.KasMasuk.GetValueOrDefault
                                objDetailKasMasuk.Add(singleDetailKasMasuk)
                            End If
                        End If
                    ElseIf obj.KasMasuk.HasValue And obj.KasMasuk.Value > 0 Then
                        singleDetailKasMasuk.Jumlah = 0
                        singleDetailKasMasuk.JumlahRp = Safe(obj.KasMasuk)
                        singleDetailKasMasuk.KursTransaksi = 0
                        Dim objKurs As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(22) 'IDR Indonesia
                        If Not IsNothing(objKurs) Then
                            singleDetailKasMasuk.MataUang = Safe(objKurs.Code) & " - " & Safe(objKurs.Name)
                        End If
                        detilKasmasuk += obj.KasMasuk.GetValueOrDefault
                        objDetailKasMasuk.Add(singleDetailKasMasuk)
                    End If


                Next

                old_lblTRXKMDetilValutaAsingJumlahRp.Text = FormatMoneyWithComma(objResume(SetnGetRowEditOLD).TransactionNominal)
                old_grvTRXKMDetilValutaAsing.DataSource = objDetailKasMasuk
                old_grvTRXKMDetilValutaAsing.DataBind()

                'LoadData ke field Kas Masuk
                old_txtTRXKMTanggalTrx.Text = FormatDate(objResume(SetnGetRowEditOLD).TransactionCashIn.TanggalTransaksi)
                old_txtTRXKMNamaKantor.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashIn.NamaKantorPJK)

                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEditOLD).TransactionCashIn.FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    old_txtTRXKMKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    old_hfTRXKMKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEditOLD).TransactionCashIn.FK_MsProvince_Id)
                If Not objSProvinsi Is Nothing Then
                    old_txtTRXKMProvinsi.Text = Safe(objSProvinsi.Nama)
                    old_hfTRXKMProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If

                old_txtTRXKMNoRekening.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashIn.NomorRekening)
                SafeSelectedValue(old_rblTRXKMTipePelapor, objResume(SetnGetRowEditOLD).TransactionCashIn.TipeTerlapor)
                old_txtTRXKMINDVGelar.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashIn.INDV_Gelar)
                old_txtTRXKMINDVNamaLengkap.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashIn.INDV_NamaLengkap)
                old_txtTRXKMINDVTempatLahir.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashIn.INDV_TempatLahir)
                old_txtTRXKMINDVTanggalLahir.Text = FormatDate(objResume(SetnGetRowEditOLD).TransactionCashIn.INDV_TanggalLahir)


                If objResume(SetnGetRowEditOLD).TransactionCashIn.INDV_Kewarganegaraan <> "-" And objResume(SetnGetRowEditOLD).TransactionCashIn.INDV_Kewarganegaraan.Trim <> "" Then
                    SafeSelectedValue(old_rblTRXKMINDVKewarganegaraan, objResume(SetnGetRowEditOLD).TransactionCashIn.INDV_Kewarganegaraan)
                End If
                If old_rblTRXKMINDVKewarganegaraan.SelectedIndex <> -1 Then
                    i = 0
                    For Each itemcboNegara As ListItem In old_cboTRXKMINDVNegara.Items
                        If objResume(SetnGetRowEditOLD).TransactionCashIn.INDV_FK_MsNegara_Id.ToString = itemcboNegara.Value.ToString Then
                            old_cboTRXKMINDVNegara.SelectedIndex = i
                            Exit For
                        End If
                        i = i + 1
                    Next
                End If
                old_txtTRXKMINDVDOMNamaJalan.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashIn.INDV_DOM_NamaJalan)
                old_txtTRXKMINDVDOMRTRW.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashIn.INDV_DOM_RTRW)
                objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objResume(SetnGetRowEditOLD).TransactionCashIn.INDV_DOM_FK_MsKelurahan_Id)
                If Not IsNothing(objSKelurahan) Then
                    old_txtTRXKMINDVDOMKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
                    old_hfTRXKMINDVDOMKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
                End If
                objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objResume(SetnGetRowEditOLD).TransactionCashIn.INDV_DOM_FK_MsKecamatan_Id)
                If Not IsNothing(objSkecamatan) Then
                    old_txtTRXKMINDVDOMKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
                    old_hfTRXKMINDVDOMKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEditOLD).TransactionCashIn.INDV_DOM_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    old_txtTRXKMINDVDOMKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    old_hfTRXKMINDVDOMKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                old_txtTRXKMINDVDOMKodePos.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashIn.INDV_DOM_KodePos)
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEditOLD).TransactionCashIn.INDV_DOM_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    old_txtTRXKMINDVDOMProvinsi.Text = Safe(objSProvinsi.Nama)
                    old_hfTRXKMINDVDOMProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                'chkTRXKMINDVCopyDOM.Checked = False
                old_txtTRXKMINDVIDNamaJalan.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashIn.INDV_ID_NamaJalan)
                old_txtTRXKMINDVIDRTRW.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashIn.INDV_ID_RTRW)
                objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objResume(SetnGetRowEditOLD).TransactionCashIn.INDV_ID_FK_MsKelurahan_Id)
                If Not IsNothing(objSKelurahan) Then
                    old_txtTRXKMINDVIDKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
                    old_hfTRXKMINDVIDKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
                End If
                objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objResume(SetnGetRowEditOLD).TransactionCashIn.INDV_ID_FK_MsKecamatan_Id)
                If Not IsNothing(objSkecamatan) Then
                    old_txtTRXKMINDVIDKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
                    old_hfTRXKMINDVIDKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEditOLD).TransactionCashIn.INDV_ID_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    old_txtTRXKMINDVIDKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    old_hfTRXKMINDVIDKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                old_txtTRXKMINDVIDKodePos.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashIn.INDV_ID_KodePos)
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEditOLD).TransactionCashIn.INDV_ID_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    old_txtTRXKMINDVIDProvinsi.Text = Safe(objSProvinsi.Nama)
                    old_hfTRXKMINDVIDProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                old_txtTRXKMINDVNANamaJalan.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashIn.INDV_NA_NamaJalan)

                'objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objResume(SetnGetRowEditOLD).TransactionCashIn.INDV_NA_FK_MsNegara_Id)
                'If Not IsNothing(objSMapNegara) Then
                '    objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                '    If Not IsNothing(objSnegara) Then
                '        old_txtTRXKMINDVNANegara.Text = Safe(objSnegara.NamaNegara)
                '        old_hfTRXKMINDVNANegara.Value = Safe(objSnegara.IDNegaraNCBS)
                '    End If
                'End If
                Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objResume(SetnGetRowEditOLD).TransactionCashIn.INDV_NA_FK_MsNegara_Id.GetValueOrDefault(0))
                    If Not objNegaraRef Is Nothing Then
                        old_txtTRXKMINDVNANegara.Text = Safe(objNegaraRef.NamaNegara)
                        old_hfTRXKMINDVNANegara.Value = Safe(objNegaraRef.IDNegara)
                    End If
                End Using
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEditOLD).TransactionCashIn.INDV_NA_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    old_txtTRXKMINDVNAProvinsi.Text = Safe(objSProvinsi.Nama)
                    old_hfTRXKMINDVNAProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEditOLD).TransactionCashIn.INDV_NA_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    old_txtTRXKMINDVNAKota.Text = Safe(objSkotaKab.NamaKotaKab)
                    old_hfTRXKMINDVNAKota.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtTRXKMINDVNAKodePos.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashIn.INDV_NA_KodePos)
                If objResume(SetnGetRowEditOLD).TransactionCashIn.INDV_FK_MsIDType_Id.GetValueOrDefault <> Nothing Then
                    i = 0
                    For Each itemJenisID As ListItem In old_cboTRXKMINDVJenisID.Items
                        If itemJenisID.Value = objResume(SetnGetRowEditOLD).TransactionCashIn.INDV_FK_MsIDType_Id.ToString Then
                            old_cboTRXKMINDVJenisID.SelectedIndex = i
                            Exit For
                        End If
                        i = i + 1
                    Next
                End If


                old_txtTRXKMINDVNomorID.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashIn.INDV_NomorId)
                old_txtTRXKMINDVNPWP.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashIn.INDV_NPWP)
                objSPekerjaan = objPekerjaan.Find(MsPekerjaanColumn.IDPekerjaan, objResume(SetnGetRowEditOLD).TransactionCashIn.INDV_FK_MsPekerjaan_Id.GetValueOrDefault)
                If Not IsNothing(objSPekerjaan) Then
                    old_txtTRXKMINDVPekerjaan.Text = Safe(objSPekerjaan.NamaPekerjaan)
                    old_hfTRXKMINDVPekerjaan.Value = Safe(objSPekerjaan.IDPekerjaan)
                End If
                old_txtTRXKMINDVJabatan.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashIn.INDV_Jabatan)
                old_txtTRXKMINDVPenghasilanRataRata.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashIn.INDV_PenghasilanRataRata)
                old_txtTRXKMINDVTempatKerja.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashIn.INDV_TempatBekerja)
                old_txtTRXKMINDVTujuanTrx.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashIn.INDV_TujuanTransaksi)
                old_txtTRXKMINDVSumberDana.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashIn.INDV_SumberDana)
                old_txtTRXKMINDVNamaBankLain.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashIn.INDV_NamaBankLain)
                old_txtTRXKMINDVNoRekeningTujuan.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashIn.INDV_NomorRekeningTujuan)

                'CORP
                If objResume(SetnGetRowEditOLD).TransactionCashIn.CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault <> Nothing Then
                    i = 0
                    For Each itemBentukBadangUsaha As ListItem In old_cboTRXKMCORPBentukBadanUsaha.Items
                        If itemBentukBadangUsaha.Value.ToString = objResume(SetnGetRowEditOLD).TransactionCashIn.CORP_FK_MsBentukBadanUsaha_Id Then
                            old_cboTRXKMCORPBentukBadanUsaha.SelectedIndex = i
                            Exit For
                        End If
                        i = i + 1
                    Next
                End If
                old_txtTRXKMCORPNama.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashIn.CORP_Nama)
                objSBidangUsaha = objBidangUsaha.Find(MsBidangUsahaColumn.IdBidangUsaha, objResume(SetnGetRowEditOLD).TransactionCashIn.CORP_FK_MsBidangUsaha_Id)
                If Not IsNothing(objSBidangUsaha) Then
                    old_txtTRXKMCORPBidangUsaha.Text = Safe(objSBidangUsaha.NamaBidangUsaha)
                    old_hfTRXKMCORPBidangUsaha.Value = Safe(objSBidangUsaha.IdBidangUsaha)
                End If
                If objResume(SetnGetRowEditOLD).TransactionCashIn.CORP_TipeAlamat.GetValueOrDefault <> Nothing Then
                    SafeNumIndex(old_rblTRXKMCORPTipeAlamat, objResume(SetnGetRowEditOLD).TransactionCashIn.CORP_TipeAlamat)
                End If
                old_txtTRXKMCORPDLNamaJalan.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashIn.CORP_NamaJalan)
                old_txtTRXKMCORPDLRTRW.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashIn.CORP_RTRW)
                objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objResume(SetnGetRowEditOLD).TransactionCashIn.CORP_FK_MsKelurahan_Id)
                If Not IsNothing(objSKelurahan) Then
                    old_txtTRXKMCORPDLKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
                    old_hfTRXKMCORPDLKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
                End If
                objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objResume(SetnGetRowEditOLD).TransactionCashIn.CORP_FK_MsKecamatan_Id)
                If Not IsNothing(objSkecamatan) Then
                    old_txtTRXKMCORPDLKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
                    old_hfTRXKMCORPDLKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEditOLD).TransactionCashIn.CORP_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    old_txtTRXKMCORPDLKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    old_hfTRXKMCORPDLKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                old_txtTRXKMCORPDLKodePos.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashIn.CORP_KodePos)
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEditOLD).TransactionCashIn.CORP_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    old_txtTRXKMCORPDLProvinsi.Text = Safe(objSProvinsi.Nama)
                    old_hfTRXKMCORPDLProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If

                'objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objResume(SetnGetRowEditOLD).TransactionCashIn.CORP_FK_MsNegara_Id)
                'If Not IsNothing(objSMapNegara) Then
                '    objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                '    If Not IsNothing(objSnegara) Then
                '        old_txtTRXKMCORPDLNegara.Text = Safe(objSnegara.NamaNegara)
                '        old_hfTRXKMCORPDLNegara.Value = Safe(objSnegara.IDNegaraNCBS)
                '    End If
                'End If
                Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objResume(SetnGetRowEditOLD).TransactionCashIn.CORP_FK_MsNegara_Id.GetValueOrDefault(0))
                    If Not objNegaraRef Is Nothing Then
                        old_txtTRXKMCORPDLNegara.Text = Safe(objNegaraRef.NamaNegara)
                        old_hfTRXKMCORPDLNegara.Value = Safe(objNegaraRef.IDNegara)
                    End If
                End Using
                old_txtTRXKMCORPLNNamaJalan.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashIn.CORP_LN_NamaJalan)

                'objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objResume(SetnGetRowEditOLD).TransactionCashIn.CORP_LN_FK_MsNegara_Id)
                'If Not IsNothing(objSMapNegara) Then
                '    objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                '    If Not IsNothing(objSnegara) Then
                '        old_txtTRXKMCORPLNNegara.Text = Safe(objSnegara.NamaNegara)
                '        old_hfTRXKMCORPLNNegara.Value = Safe(objSnegara.IDNegaraNCBS)
                '    End If
                'End If
                Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objResume(SetnGetRowEditOLD).TransactionCashIn.CORP_LN_FK_MsNegara_Id.GetValueOrDefault(0))
                    If Not objNegaraRef Is Nothing Then
                        old_txtTRXKMCORPLNNegara.Text = Safe(objNegaraRef.NamaNegara)
                        old_hfTRXKMCORPLNNegara.Value = Safe(objNegaraRef.IDNegara)
                    End If
                End Using
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEditOLD).TransactionCashIn.CORP_LN_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    old_txtTRXKMCORPLNProvinsi.Text = Safe(objSProvinsi.Nama)
                    old_hfTRXKMCORPLNProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEditOLD).TransactionCashIn.CORP_LN_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    old_txtTRXKMCORPLNKota.Text = Safe(objSkotaKab.NamaKotaKab)
                    old_hfTRXKMCORPLNKota.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                old_txtTRXKMCORPLNKodePos.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashIn.CORP_LN_KodePos)
                old_txtTRXKMCORPNPWP.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashIn.CORP_NPWP)
                old_txtTRXKMCORPTujuanTrx.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashIn.CORP_TujuanTransaksi)
                old_txtTRXKMCORPSumberDana.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashIn.CORP_SumberDana)
                old_txtTRXKMCORPNamaBankLain.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashIn.CORP_NamaBankLain)
                old_txtTRXKMCORPNoRekeningTujuan.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashIn.CORP_NomorRekeningTujuan)


            ElseIf objResume(SetnGetRowEditOLD).Kas = "Kas Keluar" Then

                old_MultiView1.ActiveViewIndex = 1
                old_Menu1.Items.Item(1).Selected = True

                'LoadData ke DetailKasKeluar
                'Dim objDetailKasKeluar As List(Of LTKTDetailCashIn) = SetnGetgrvDetilKasKeluarOLD
                'For Each obj As LTKTDetailCashOutTransaction In objResume(SetnGetRowEditOLD).DetailTranscationCashOut
                '    Dim singleDetailKasKeluar As New LTKTDetailCashIn

                '    If obj.Asing_TotalKasKeluarDalamRupiah.GetValueOrDefault <> Nothing Then
                '        singleDetailKasKeluar.Jumlah = (CInt(obj.Asing_TotalKasKeluarDalamRupiah) / CInt(obj.Asing_KursTransaksi)).ToString
                '        singleDetailKasKeluar.JumlahRp = obj.Asing_TotalKasKeluarDalamRupiah.ToString
                '        singleDetailKasKeluar.KursTransaksi = obj.Asing_KursTransaksi.ToString
                '        Dim objKurs As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(obj.Asing_FK_MsCurrency_Id)
                '        If Not IsNothing(objKurs) Then
                '            singleDetailKasKeluar.MataUang = objKurs.Code & " - " & objKurs.Name
                '        End If
                '        objDetailKasKeluar.Add(singleDetailKasKeluar)
                '    End If


                '    If obj.KasKeluar.GetValueOrDefault <> 0 Then
                '        old_txtTRXKKDetailKasKeluar.Text = ValidateBLL.FormatMoneyWithComma(obj.KasKeluar.GetValueOrDefault)
                '    End If

                'Next

                Dim detailkaskeluar As Decimal = 0
                Dim objDetailKasKeluar As List(Of LTKTDetailCashInApprovalDetail) = SetnGetgrvDetilKasKeluarOLD
                For Each obj As LTKTDetailCashOutTransaction In objResume(SetnGetRowEditOLD).DetailTranscationCashOut
                    Dim singleDetailKasKeluar As New LTKTDetailCashInApprovalDetail

                    If obj.Asing_TotalKasKeluarDalamRupiah.HasValue Then
                        If obj.Asing_TotalKasKeluarDalamRupiah.Value > 0 Then

                            If obj.Asing_KursTransaksi.HasValue Then
                                If obj.Asing_KursTransaksi.Value > 0 Then
                                    Try
                                        singleDetailKasKeluar.Jumlah = (CDec(obj.Asing_TotalKasKeluarDalamRupiah.GetValueOrDefault) / CDec(obj.Asing_KursTransaksi.GetValueOrDefault)).ToString
                                    Catch ex As Exception
                                        singleDetailKasKeluar.Jumlah = obj.Asing_TotalKasKeluarDalamRupiah.GetValueOrDefault
                                    End Try

                                Else
                                    singleDetailKasKeluar.Jumlah = obj.Asing_TotalKasKeluarDalamRupiah.GetValueOrDefault
                                End If
                            End If

                            singleDetailKasKeluar.JumlahRp = obj.Asing_TotalKasKeluarDalamRupiah.ToString
                            singleDetailKasKeluar.KursTransaksi = obj.Asing_KursTransaksi.ToString
                            Dim ID As Decimal = 0
                            If obj.Asing_FK_MsCurrency_Id.HasValue Then
                                ID = obj.Asing_FK_MsCurrency_Id
                            Else 'Jika Nothing pake IDR
                                ID = defaultcurrency
                            End If

                            Dim objKurs As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(ID)
                            If Not IsNothing(objKurs) Then
                                singleDetailKasKeluar.MataUang = objKurs.Code & " - " & objKurs.Name
                            End If
                            objDetailKasKeluar.Add(singleDetailKasKeluar)
                        Else
                            If obj.KasKeluar.HasValue Then
                                singleDetailKasKeluar.Jumlah = 0
                                singleDetailKasKeluar.JumlahRp = obj.KasKeluar.GetValueOrDefault
                                singleDetailKasKeluar.KursTransaksi = 0
                                Dim objKurs As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(defaultcurrency) ' Indonesia IDR
                                If Not IsNothing(objKurs) Then
                                    singleDetailKasKeluar.MataUang = objKurs.Code & " - " & objKurs.Name
                                End If
                                detailkaskeluar += obj.KasKeluar.GetValueOrDefault
                                objDetailKasKeluar.Add(singleDetailKasKeluar)
                            End If
                        End If
                    ElseIf obj.KasKeluar.HasValue Then
                        singleDetailKasKeluar.Jumlah = 0
                        singleDetailKasKeluar.JumlahRp = obj.KasKeluar
                        singleDetailKasKeluar.KursTransaksi = 0
                        Dim objKurs As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(defaultcurrency) ' Indonesia IDR
                        If Not IsNothing(objKurs) Then
                            singleDetailKasKeluar.MataUang = objKurs.Code & " - " & objKurs.Name
                        End If
                        detailkaskeluar += obj.KasKeluar.GetValueOrDefault
                        objDetailKasKeluar.Add(singleDetailKasKeluar)
                    End If

                Next

                old_lblDetilKasKeluarJumlahRp.Text = ValidateBLL.FormatMoneyWithComma(objResume(SetnGetRowEditOLD).TransactionNominal)
                old_grvDetilKasKeluar.DataSource = objDetailKasKeluar
                old_grvDetilKasKeluar.DataBind()


                'LoadData ke field Kas Keluar
                old_txtTRXKKTanggalTransaksi.Text = FormatDate(objResume(SetnGetRowEditOLD).TransactionCashOut.TanggalTransaksi)
                old_txtTRXKKNamaKantor.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashOut.NamaKantorPJK)

                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEditOLD).TransactionCashOut.FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    old_txtTRXKKKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    old_hfTRXKKKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEditOLD).TransactionCashOut.FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    old_txtTRXKKProvinsi.Text = Safe(objSProvinsi.Nama)
                    old_hfTRXKKProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                old_txtTRXKKRekeningKK.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashOut.NomorRekening)
                SafeSelectedValue(old_rblTRXKKTipePelapor, objResume(SetnGetRowEditOLD).TransactionCashOut.TipeTerlapor)
                old_txtTRXKKINDVGelar.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashOut.INDV_Gelar)
                old_txtTRXKKINDVNamaLengkap.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashOut.INDV_NamaLengkap)
                old_txtTRXKKINDVTempatLahir.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashOut.INDV_TempatLahir)
                old_txtTRXKKINDVTglLahir.Text = FormatDate(objResume(SetnGetRowEditOLD).TransactionCashOut.INDV_TanggalLahir)

                If objResume(SetnGetRowEditOLD).TransactionCashOut.INDV_Kewarganegaraan <> "-" And objResume(SetnGetRowEditOLD).TransactionCashOut.INDV_Kewarganegaraan.Trim <> "" Then
                    SafeSelectedValue(old_rblTRXKKINDVKewarganegaraan, objResume(SetnGetRowEditOLD).TransactionCashOut.INDV_Kewarganegaraan)
                End If
                If old_rblTRXKKINDVKewarganegaraan.SelectedIndex <> -1 Then
                    i = 0
                    For Each itemcboNegara As ListItem In old_cboTRXKKINDVNegara.Items
                        If objResume(SetnGetRowEditOLD).TransactionCashOut.INDV_FK_MsNegara_Id.ToString = itemcboNegara.Value.ToString Then
                            old_cboTRXKKINDVNegara.SelectedIndex = i
                            Exit For
                        End If
                        i = i + 1
                    Next
                End If
                old_txtTRXKKINDVDOMNamaJalan.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashOut.INDV_DOM_NamaJalan)
                old_txtTRXKKINDVDOMRTRW.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashOut.INDV_DOM_RTRW)
                objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objResume(SetnGetRowEditOLD).TransactionCashOut.INDV_DOM_FK_MsKelurahan_Id)
                If Not IsNothing(objSKelurahan) Then
                    old_txtTRXKKINDVDOMKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
                    old_hfTRXKKINDVDOMKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
                End If
                objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objResume(SetnGetRowEditOLD).TransactionCashOut.INDV_DOM_FK_MsKecamatan_Id)
                If Not IsNothing(objSkecamatan) Then
                    old_txtTRXKKINDVDOMKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
                    old_hfTRXKKINDVDOMKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEditOLD).TransactionCashOut.INDV_DOM_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    old_txtTRXKKINDVDOMKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    old_hfTRXKKINDVDOMKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                old_txtTRXKKINDVDOMKodePos.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashOut.INDV_DOM_KodePos)
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEditOLD).TransactionCashOut.INDV_DOM_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    old_txtTRXKKINDVDOMProvinsi.Text = Safe(objSProvinsi.Nama)
                    old_hfTRXKKINDVDOMProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                'chkTRXKKINDVCopyDOM.Checked = False
                old_txtTRXKKINDVIDNamaJalan.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashOut.INDV_ID_NamaJalan)
                old_txtTRXKKINDVIDRTRW.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashOut.INDV_ID_RTRW)
                objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objResume(SetnGetRowEditOLD).TransactionCashOut.INDV_ID_FK_MsKelurahan_Id)
                If Not IsNothing(objSKelurahan) Then
                    old_txtTRXKKINDVIDKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
                    old_hfTRXKKINDVIDKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
                End If
                objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objResume(SetnGetRowEditOLD).TransactionCashOut.INDV_ID_FK_MsKecamatan_Id)
                If Not IsNothing(objSkecamatan) Then
                    old_txtTRXKKINDVIDKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
                    old_hfTRXKKINDVIDKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEditOLD).TransactionCashOut.INDV_ID_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    old_txtTRXKKINDVIDKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    old_hfTRXKKINDVIDKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                old_txtTRXKKINDVIDKodePos.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashOut.INDV_ID_KodePos)
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEditOLD).TransactionCashOut.INDV_ID_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    old_txtTRXKKINDVIDProvinsi.Text = Safe(objSProvinsi.Nama)
                    old_hfTRXKKINDVIDProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                old_txtTRXKKINDVNANamaJalan.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashOut.INDV_NA_NamaJalan)

                'objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objResume(SetnGetRowEditOLD).TransactionCashOut.INDV_NA_FK_MsNegara_Id)
                'If Not IsNothing(objSMapNegara) Then
                '    objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                '    If Not IsNothing(objSnegara) Then
                '        old_txtTRXKKINDVNANegara.Text = Safe(objSnegara.NamaNegara)
                '        old_hfTRXKKINDVNANegara.Value = Safe(objSnegara.IDNegaraNCBS)
                '    End If
                'End If

                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEditOLD).TransactionCashOut.INDV_NA_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    old_txtTRXKKINDVNAProvinsi.Text = Safe(objSProvinsi.Nama)
                    old_hfTRXKKINDVNAProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEditOLD).TransactionCashOut.INDV_NA_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    old_txtTRXKKINDVNAKota.Text = Safe(objSkotaKab.NamaKotaKab)
                    old_hfTRXKKINDVNAKota.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                old_txtTRXKKINDVNAKodePos.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashOut.INDV_NA_KodePos)
                If objResume(SetnGetRowEditOLD).TransactionCashOut.INDV_FK_MsIDType_Id.GetValueOrDefault <> Nothing Then
                    i = 0
                    For Each itemJenisID As ListItem In old_cboTRXKKINDVJenisID.Items
                        If itemJenisID.Value = objResume(SetnGetRowEditOLD).TransactionCashOut.INDV_FK_MsIDType_Id.ToString Then
                            old_cboTRXKKINDVJenisID.SelectedIndex = i
                            Exit For
                        End If
                        i = i + 1
                    Next
                End If
                old_txtTRXKKINDVNomorId.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashOut.INDV_NomorId)
                old_txtTRXKKINDVNPWP.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashOut.INDV_NPWP)
                objSPekerjaan = objPekerjaan.Find(MsPekerjaanColumn.IDPekerjaan, objResume(SetnGetRowEditOLD).TransactionCashOut.INDV_FK_MsPekerjaan_Id.GetValueOrDefault)
                If Not IsNothing(objSPekerjaan) Then
                    old_txtTRXKKINDVPekerjaan.Text = Safe(objSPekerjaan.NamaPekerjaan)
                    old_hfTRXKKINDVPekerjaan.Value = Safe(objSPekerjaan.IDPekerjaan)
                End If
                old_txtTRXKKINDVJabatan.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashOut.INDV_Jabatan)
                old_txtTRXKKINDVPenghasilanRataRata.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashOut.INDV_PenghasilanRataRata)
                old_txtTRXKKINDVTempatKerja.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashOut.INDV_TempatBekerja)
                old_txtTRXKKINDVTujuanTrx.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashOut.INDV_TujuanTransaksi)
                old_txtTRXKKINDVSumberDana.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashOut.INDV_SumberDana)
                old_txtTRXKKINDVNamaBankLain.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashOut.INDV_NamaBankLain)
                old_txtTRXKKINDVNoRekTujuan.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashOut.INDV_NomorRekeningTujuan)

                'CORP
                If objResume(SetnGetRowEditOLD).TransactionCashOut.CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault <> Nothing Then
                    i = 0
                    For Each itemBentukBadangUsaha As ListItem In old_cboTRXKKCORPBentukBadanUsaha.Items
                        If itemBentukBadangUsaha.Value.ToString = objResume(SetnGetRowEditOLD).TransactionCashOut.CORP_FK_MsBentukBadanUsaha_Id Then
                            old_cboTRXKKCORPBentukBadanUsaha.SelectedIndex = i
                            Exit For
                        End If
                        i = i + 1
                    Next
                End If
                If objResume(SetnGetRowEditOLD).TransactionCashOut.CORP_TipeAlamat.GetValueOrDefault <> Nothing Then
                    SafeNumIndex(old_rblTRXKKCORPTipeAlamat, objResume(SetnGetRowEditOLD).TransactionCashOut.CORP_TipeAlamat)
                End If
                old_txtTRXKKCORPNama.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashOut.CORP_Nama)
                objSBidangUsaha = objBidangUsaha.Find(MsBidangUsahaColumn.IdBidangUsaha, objResume(SetnGetRowEditOLD).TransactionCashOut.CORP_FK_MsBidangUsaha_Id)
                If Not IsNothing(objSBidangUsaha) Then
                    old_txtTRXKKCORPBidangUsaha.Text = Safe(objSBidangUsaha.NamaBidangUsaha)
                    old_hfTRXKKCORPBidangUsaha.Value = Safe(objSBidangUsaha.IdBidangUsaha)
                End If
                If objResume(SetnGetRowEditOLD).TransactionCashOut.CORP_TipeAlamat.GetValueOrDefault <> Nothing Then
                    SafeNumIndex(old_rblTRXKKCORPTipeAlamat, objResume(SetnGetRowEditOLD).TransactionCashOut.CORP_TipeAlamat)
                End If
                old_txtTRXKKCORPDLNamaJalan.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashOut.CORP_NamaJalan)
                old_txtTRXKKCORPDLRTRW.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashOut.CORP_RTRW)
                objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objResume(SetnGetRowEditOLD).TransactionCashOut.CORP_FK_MsKelurahan_Id)
                If Not IsNothing(objSKelurahan) Then
                    old_txtTRXKKCORPDLKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
                    old_hfTRXKKCORPDLKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
                End If
                objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objResume(SetnGetRowEditOLD).TransactionCashOut.CORP_FK_MsKecamatan_Id)
                If Not IsNothing(objSkecamatan) Then
                    old_txtTRXKKCORPDLKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
                    old_hfTRXKKCORPDLKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEditOLD).TransactionCashOut.CORP_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    old_txtTRXKKCORPDLKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    old_hfTRXKKCORPDLKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                old_txtTRXKKCORPDLKodePos.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashOut.CORP_KodePos)
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEditOLD).TransactionCashOut.CORP_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    old_txtTRXKKCORPDLProvinsi.Text = Safe(objSProvinsi.Nama)
                    old_hfTRXKKCORPDLProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If

                'objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objResume(SetnGetRowEditOLD).TransactionCashOut.CORP_FK_MsNegara_Id)
                'If Not IsNothing(objSMapNegara) Then
                '    objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                '    If Not IsNothing(objSnegara) Then
                '        old_txtTRXKKCORPDLNegara.Text = Safe(objSnegara.NamaNegara)
                '        old_hfTRXKKCORPDLNegara.Value = Safe(objSnegara.IDNegaraNCBS)
                '    End If
                'End If

                Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objResume(SetnGetRowEditOLD).TransactionCashOut.CORP_FK_MsNegara_Id.GetValueOrDefault(0))
                    If Not objNegaraRef Is Nothing Then
                        old_txtTRXKKCORPDLNegara.Text = Safe(objNegaraRef.NamaNegara)
                        old_hfTRXKKCORPDLNegara.Value = Safe(objNegaraRef.IDNegara)
                    End If
                End Using
                old_txtTRXKKCORPLNNamaJalan.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashOut.CORP_LN_NamaJalan)

                'objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objResume(SetnGetRowEditOLD).TransactionCashOut.CORP_LN_FK_MsNegara_Id)
                'If Not IsNothing(objSMapNegara) Then
                '    objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                '    If Not IsNothing(objSnegara) Then
                '        old_txtTRXKKCORPLNNegara.Text = Safe(objSnegara.NamaNegara)
                '        old_hfTRXKKCORPLNNegara.Value = Safe(objSnegara.IDNegaraNCBS)
                '    End If
                'End If
                Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objResume(SetnGetRowEditOLD).TransactionCashOut.CORP_LN_FK_MsNegara_Id.GetValueOrDefault(0))
                    If Not objNegaraRef Is Nothing Then
                        old_txtTRXKKCORPLNNegara.Text = Safe(objNegaraRef.NamaNegara)
                        old_hfTRXKKCORPLNNegara.Value = Safe(objNegaraRef.IDNegara)
                    End If
                End Using
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEditOLD).TransactionCashOut.CORP_LN_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    old_txtTRXKKCORPLNProvinsi.Text = Safe(objSProvinsi.Nama)
                    old_hfTRXKKCORPLNProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEditOLD).TransactionCashOut.CORP_LN_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    old_txtTRXKKCORPLNKota.Text = Safe(objSkotaKab.NamaKotaKab)
                    old_hfTRXKKCORPLNKota.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                old_txtTRXKKCORPLNKodePos.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashOut.CORP_LN_KodePos)
                old_txtTRXKKCORPNPWP.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashOut.CORP_NPWP)
                old_txtTRXKKCORPTujuanTrx.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashOut.CORP_TujuanTransaksi)
                old_txtTRXKKCORPSumberDana.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashOut.CORP_SumberDana)
                old_txtTRXKKCORPNamaBankLain.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashOut.CORP_NamaBankLain)
                old_txtTRXKKCORPNoRekeningTujuan.Text = Safe(objResume(SetnGetRowEditOLD).TransactionCashOut.CORP_NomorRekeningTujuan)

            End If

            ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('old_IdTerlapor','old_Img1','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
            ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('old_Umum','old_searchimage2','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)

        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub EditResume_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            clearKasMasukKasKeluar()
            Dim objgridviewrow As GridViewRow = CType(CType(sender, LinkButton).NamingContainer, GridViewRow)
            SetnGetRowEdit = objgridviewrow.RowIndex

            'LoadData ke KasMasuk dan Keluar
            Dim objResume As List(Of ResumeKasMasukKeluarLTKT) = SetnGetResumeKasMasukKasKeluar
            Dim defaultcurrency As Integer
            Using objparam As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)
                defaultcurrency = objparam.MsSystemParameter_Value
            End Using
            'Ambil data buat cari PK Negara, Provinsi, KotaKab, dkk (ini variable yang bakal dpke berkali2)
            Dim i As Integer = 0 'buat iterasi
            Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetAll
            Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
            Dim objKelurahan As TList(Of MsKelurahan) = DataRepository.MsKelurahanProvider.GetAll
            Dim objSKelurahan As MsKelurahan 'Penampung hasil search kelurahan
            Dim objKecamatan As TList(Of MsKecamatan) = DataRepository.MsKecamatanProvider.GetAll
            Dim objSkecamatan As MsKecamatan 'Penampung hasil search kecamatan
            Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetAll
            Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi
            Dim objMapNegara As TList(Of MappingMsNegaraNCBSPPATK) = DataRepository.MappingMsNegaraNCBSPPATKProvider.GetAll
            Dim objSMapNegara As MappingMsNegaraNCBSPPATK 'Penampung hasil search mappingnegara yang pke picker
            Dim objNegara As TList(Of MsNegaraNCBS) = DataRepository.MsNegaraNCBSProvider.GetAll
            Dim objSnegara As MsNegaraNCBS 'Penampung hasil search negara yang pke picker
            Dim objPekerjaan As TList(Of MsPekerjaan) = DataRepository.MsPekerjaanProvider.GetAll
            Dim objSPekerjaan As MsPekerjaan 'Penampung hasil search pekerjaan
            Dim objBidangUsaha As TList(Of MsBidangUsaha) = DataRepository.MsBidangUsahaProvider.GetAll
            Dim objSBidangUsaha As MsBidangUsaha 'Penampung hasil search bidang usaha

            If objResume(SetnGetRowEdit).Kas = "Kas Masuk" Then

                MultiView1.ActiveViewIndex = 0
                Menu1.Items.Item(0).Selected = True

                'LoadData ke DetailKasMasuk
                Dim detailkasMasuk As Decimal = 0
                Dim objDetailKasMasuk As List(Of LTKTDetailCashInApprovalDetail) = SetnGetgrvTRXKMDetilValutaAsing
                For Each obj As LTKTDetailCashInTransaction In objResume(SetnGetRowEdit).DetailTransactionCashIn
                    Dim singleDetailKasMasuk As New LTKTDetailCashInApprovalDetail

                    If obj.Asing_TotalKasMasukDalamRupiah.HasValue Then

                        If obj.Asing_TotalKasMasukDalamRupiah.Value > 0 Then

                            If obj.Asing_KursTransaksi.HasValue Then
                                If obj.Asing_KursTransaksi.Value > 0 Then
                                    Try
                                        singleDetailKasMasuk.Jumlah = (CDec(obj.Asing_TotalKasMasukDalamRupiah.GetValueOrDefault) / CDec(obj.Asing_KursTransaksi.GetValueOrDefault)).ToString
                                    Catch ex As Exception
                                        singleDetailKasMasuk.Jumlah = obj.Asing_TotalKasMasukDalamRupiah.GetValueOrDefault
                                    End Try
                                Else
                                    singleDetailKasMasuk.Jumlah = obj.Asing_TotalKasMasukDalamRupiah.GetValueOrDefault
                                End If
                            End If

                            singleDetailKasMasuk.JumlahRp = obj.Asing_TotalKasMasukDalamRupiah.GetValueOrDefault
                            singleDetailKasMasuk.KursTransaksi = obj.Asing_KursTransaksi.GetValueOrDefault

                            Dim ID As Decimal = 0
                            If obj.Asing_FK_MsCurrency_Id.HasValue Then
                                ID = obj.Asing_FK_MsCurrency_Id
                            Else 'Jika Nothing pake IDR
                                ID = defaultcurrency
                            End If

                            Dim objKurs As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(ID)
                            If Not IsNothing(objKurs) Then
                                singleDetailKasMasuk.MataUang = objKurs.Code & " - " & objKurs.Name
                            End If

                            objDetailKasMasuk.Add(singleDetailKasMasuk)
                        Else
                            If obj.KasMasuk.HasValue Then
                                singleDetailKasMasuk.Jumlah = 0
                                singleDetailKasMasuk.JumlahRp = obj.KasMasuk.GetValueOrDefault
                                singleDetailKasMasuk.KursTransaksi = 0
                                Dim objKurs As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(defaultcurrency) ' Indonesia IDR
                                If Not IsNothing(objKurs) Then
                                    singleDetailKasMasuk.MataUang = objKurs.Code & " - " & objKurs.Name
                                End If
                                detailkasMasuk += obj.KasMasuk.GetValueOrDefault
                                objDetailKasMasuk.Add(singleDetailKasMasuk)
                            End If
                        End If

                    ElseIf obj.KasMasuk.HasValue Then
                        singleDetailKasMasuk.Jumlah = 0
                        singleDetailKasMasuk.JumlahRp = obj.KasMasuk.GetValueOrDefault
                        singleDetailKasMasuk.KursTransaksi = 0
                        Dim objKurs As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(defaultcurrency) ' Indonesia IDR
                        If Not IsNothing(objKurs) Then
                            singleDetailKasMasuk.MataUang = objKurs.Code & " - " & objKurs.Name
                        End If
                        detailkasMasuk += obj.KasMasuk.GetValueOrDefault
                        objDetailKasMasuk.Add(singleDetailKasMasuk)
                    End If

                Next
                lblTRXKMDetilValutaAsingJumlahRp.Text = ValidateBLL.FormatMoneyWithComma(objResume(SetnGetRowEdit).TransactionNominal)

                'Gridview Transaksi Kas masuk Detail Valuta Asing
                grvTRXKMDetilValutaAsing.DataSource = objDetailKasMasuk
                grvTRXKMDetilValutaAsing.DataBind()



                'LoadData ke field Kas Masuk
                txtTRXKMTanggalTrx.Text = FormatDate(objResume(SetnGetRowEdit).TransactionCashIn.TanggalTransaksi)
                txtTRXKMNamaKantor.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.NamaKantorPJK)

                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashIn.FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTRXKMKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTRXKMKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashIn.FK_MsProvince_Id)
                If Not objSProvinsi Is Nothing Then
                    txtTRXKMProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTRXKMProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                txtTRXKMNoRekening.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.NomorRekening)
                SafeSelectedValue(rblTRXKMTipePelapor, objResume(SetnGetRowEdit).TransactionCashIn.TipeTerlapor)
                txtTRXKMINDVGelar.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_Gelar)
                txtTRXKMINDVNamaLengkap.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_NamaLengkap)
                txtTRXKMINDVTempatLahir.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_TempatLahir)
                txtTRXKMINDVTanggalLahir.Text = FormatDate(objResume(SetnGetRowEdit).TransactionCashIn.INDV_TanggalLahir)
                'If txtTRXKMINDVTanggalLahir.Text = Date.MinValue Then
                '    txtTRXKMINDVTanggalLahir.Text = ""
                'End If
                If objResume(SetnGetRowEdit).TransactionCashIn.INDV_Kewarganegaraan <> "-" And objResume(SetnGetRowEdit).TransactionCashIn.INDV_Kewarganegaraan.Trim <> "" Then
                    SafeSelectedValue(rblTRXKMINDVKewarganegaraan, objResume(SetnGetRowEdit).TransactionCashIn.INDV_Kewarganegaraan)
                End If
                If rblTRXKMINDVKewarganegaraan.SelectedIndex <> -1 Then
                    i = 0
                    For Each itemCboNegara As ListItem In cboTRXKMINDVNegara.Items
                        If objResume(SetnGetRowEdit).TransactionCashIn.INDV_FK_MsNegara_Id.ToString = itemCboNegara.Value.ToString Then
                            cboTRXKMINDVNegara.SelectedIndex = i
                            Exit For
                        End If
                        i = i + 1
                    Next
                End If
                txtTRXKMINDVDOMNamaJalan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_DOM_NamaJalan)
                txtTRXKMINDVDOMRTRW.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_DOM_RTRW)
                objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objResume(SetnGetRowEdit).TransactionCashIn.INDV_DOM_FK_MsKelurahan_Id)
                If Not IsNothing(objSKelurahan) Then
                    txtTRXKMINDVDOMKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
                    hfTRXKMINDVDOMKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
                End If
                objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objResume(SetnGetRowEdit).TransactionCashIn.INDV_DOM_FK_MsKecamatan_Id)
                If Not IsNothing(objSkecamatan) Then
                    txtTRXKMINDVDOMKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
                    hfTRXKMINDVDOMKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashIn.INDV_DOM_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTRXKMINDVDOMKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTRXKMINDVDOMKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtTRXKMINDVDOMKodePos.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_DOM_KodePos)
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashIn.INDV_DOM_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTRXKMINDVDOMProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTRXKMINDVDOMProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                'chkTRXKMINDVCopyDOM.Checked = False
                txtTRXKMINDVIDNamaJalan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_ID_NamaJalan)
                txtTRXKMINDVIDRTRW.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_ID_RTRW)
                objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objResume(SetnGetRowEdit).TransactionCashIn.INDV_ID_FK_MsKelurahan_Id)
                If Not IsNothing(objSKelurahan) Then
                    txtTRXKMINDVIDKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
                    hfTRXKMINDVIDKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
                End If
                objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objResume(SetnGetRowEdit).TransactionCashIn.INDV_ID_FK_MsKecamatan_Id)
                If Not IsNothing(objSkecamatan) Then
                    txtTRXKMINDVIDKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
                    hfTRXKMINDVIDKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashIn.INDV_ID_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTRXKMINDVIDKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTRXKMINDVIDKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtTRXKMINDVIDKodePos.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_ID_KodePos)
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashIn.INDV_ID_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTRXKMINDVIDProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTRXKMINDVIDProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                txtTRXKMINDVNANamaJalan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_NA_NamaJalan)

                'objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objResume(SetnGetRowEdit).TransactionCashIn.INDV_NA_FK_MsNegara_Id)
                'If Not IsNothing(objSMapNegara) Then
                '    objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                '    If Not IsNothing(objSnegara) Then
                '        txtTRXKMINDVNANegara.Text = Safe(objSnegara.NamaNegara)
                '        hfTRXKMINDVNANegara.Value = Safe(objSnegara.IDNegaraNCBS)
                '    End If
                'End If

                Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objResume(SetnGetRowEdit).TransactionCashIn.INDV_NA_FK_MsNegara_Id.GetValueOrDefault(0))
                    If Not objNegaraRef Is Nothing Then
                        txtTRXKMINDVNANegara.Text = Safe(objNegaraRef.NamaNegara)
                        hfTRXKMINDVNANegara.Value = Safe(objNegaraRef.IDNegara)
                    End If
                End Using
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashIn.INDV_NA_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTRXKMINDVNAProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTRXKMINDVNAProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashIn.INDV_NA_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTRXKMINDVNAKota.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTRXKMINDVNAKota.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtTRXKMINDVNAKodePos.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_NA_KodePos)
                If objResume(SetnGetRowEdit).TransactionCashIn.INDV_FK_MsIDType_Id.GetValueOrDefault <> Nothing Then
                    i = 0
                    For Each itemJenisID As ListItem In cboTRXKMINDVJenisID.Items
                        If itemJenisID.Value = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_FK_MsIDType_Id) Then
                            cboTRXKMINDVJenisID.SelectedIndex = i
                            Exit For
                        End If
                        i = i + 1
                    Next
                End If
                txtTRXKMINDVNomorID.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_NomorId)
                txtTRXKMINDVNPWP.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_NPWP)
                objSPekerjaan = objPekerjaan.Find(MsPekerjaanColumn.IDPekerjaan, objResume(SetnGetRowEdit).TransactionCashIn.INDV_FK_MsPekerjaan_Id.GetValueOrDefault)
                If Not IsNothing(objSPekerjaan) Then
                    txtTRXKMINDVPekerjaan.Text = Safe(objSPekerjaan.NamaPekerjaan)
                    hfTRXKMINDVPekerjaan.Value = Safe(objSPekerjaan.IDPekerjaan)
                End If
                txtTRXKMINDVJabatan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_Jabatan)
                txtTRXKMINDVPenghasilanRataRata.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_PenghasilanRataRata)
                txtTRXKMINDVTempatKerja.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_TempatBekerja)
                txtTRXKMINDVTujuanTrx.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_TujuanTransaksi)
                txtTRXKMINDVSumberDana.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_SumberDana)
                txtTRXKMINDVNamaBankLain.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_NamaBankLain)
                txtTRXKMINDVNoRekeningTujuan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_NomorRekeningTujuan)

                'CORP
                If objResume(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault <> Nothing Then
                    i = 0
                    For Each itemBentukBadangUsaha As ListItem In cboTRXKMCORPBentukBadanUsaha.Items
                        If itemBentukBadangUsaha.Value.ToString = objResume(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsBentukBadanUsaha_Id Then
                            cboTRXKMCORPBentukBadanUsaha.SelectedIndex = i
                            Exit For
                        End If
                        i = i + 1
                    Next
                End If

                txtTRXKMCORPNama.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_Nama)
                objSBidangUsaha = objBidangUsaha.Find(MsBidangUsahaColumn.IdBidangUsaha, objResume(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsBidangUsaha_Id)
                If Not IsNothing(objSBidangUsaha) Then
                    txtTRXKMCORPBidangUsaha.Text = Safe(objSBidangUsaha.NamaBidangUsaha)
                    hfTRXKMCORPBidangUsaha.Value = Safe(objSBidangUsaha.IdBidangUsaha)
                End If
                If objResume(SetnGetRowEdit).TransactionCashIn.CORP_TipeAlamat.GetValueOrDefault <> Nothing Then
                    SafeNumIndex(rblTRXKMCORPTipeAlamat, objResume(SetnGetRowEdit).TransactionCashIn.CORP_TipeAlamat)
                End If
                txtTRXKMCORPDLNamaJalan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_NamaJalan)
                txtTRXKMCORPDLRTRW.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_RTRW)
                objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objResume(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsKelurahan_Id)
                If Not IsNothing(objSKelurahan) Then
                    txtTRXKMCORPDLKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
                    hfTRXKMCORPDLKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
                End If
                objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objResume(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsKecamatan_Id)
                If Not IsNothing(objSkecamatan) Then
                    txtTRXKMCORPDLKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
                    hfTRXKMCORPDLKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTRXKMCORPDLKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTRXKMCORPDLKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtTRXKMCORPDLKodePos.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_KodePos)
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTRXKMCORPDLProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTRXKMCORPDLProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                'objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objResume(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsNegara_Id)
                'If Not IsNothing(objSMapNegara) Then
                '    objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                '    If Not IsNothing(objSnegara) Then
                '        txtTRXKMCORPDLNegara.Text = Safe(objSnegara.NamaNegara)
                '        hfTRXKMCORPDLNegara.Value = Safe(objSnegara.IDNegaraNCBS)
                '    End If
                'End If
                Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objResume(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsNegara_Id.GetValueOrDefault(0))
                    If Not objNegaraRef Is Nothing Then
                        txtTRXKMCORPDLNegara.Text = Safe(objNegaraRef.NamaNegara)
                        hfTRXKMCORPDLNegara.Value = Safe(objNegaraRef.IDNegara)
                    End If
                End Using


                txtTRXKMCORPLNNamaJalan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_LN_NamaJalan)
                'objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objResume(SetnGetRowEdit).TransactionCashIn.CORP_LN_FK_MsNegara_Id)
                'If Not IsNothing(objSMapNegara) Then
                '    objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                '    If Not IsNothing(objSnegara) Then
                '        txtTRXKMCORPLNNegara.Text = Safe(objSnegara.NamaNegara)
                '        hfTRXKMCORPLNNegara.Value = Safe(objSnegara.IDNegaraNCBS)
                '    End If
                'End If

                Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objResume(SetnGetRowEdit).TransactionCashIn.CORP_LN_FK_MsNegara_Id.GetValueOrDefault(0))
                    If Not objNegaraRef Is Nothing Then
                        txtTRXKMCORPLNNegara.Text = Safe(objNegaraRef.NamaNegara)
                        hfTRXKMCORPLNNegara.Value = Safe(objNegaraRef.IDNegara)
                    End If
                End Using
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashIn.CORP_LN_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTRXKMCORPLNProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTRXKMCORPLNProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashIn.CORP_LN_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTRXKMCORPLNKota.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTRXKMCORPLNKota.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtTRXKMCORPLNKodePos.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_LN_KodePos)
                txtTRXKMCORPNPWP.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_NPWP)
                txtTRXKMCORPTujuanTrx.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_TujuanTransaksi)
                txtTRXKMCORPSumberDana.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_SumberDana)
                txtTRXKMCORPNamaBankLain.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_NamaBankLain)
                txtTRXKMCORPNoRekeningTujuan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_NomorRekeningTujuan)


            ElseIf objResume(SetnGetRowEdit).Kas = "Kas Keluar" Then

                MultiView1.ActiveViewIndex = 1
                Menu1.Items.Item(1).Selected = True

                'LoadData ke DetailKasKeluar
                Dim detailkaskeluar As Decimal = 0
                Dim objDetailKasKeluar As List(Of LTKTDetailCashInApprovalDetail) = SetnGetgrvDetilKasKeluar
                For Each obj As LTKTDetailCashOutTransaction In objResume(SetnGetRowEdit).DetailTranscationCashOut
                    Dim singleDetailKasKeluar As New LTKTDetailCashInApprovalDetail

                    If obj.Asing_TotalKasKeluarDalamRupiah.HasValue Then

                        If obj.Asing_TotalKasKeluarDalamRupiah.Value > 0 Then

                            If obj.Asing_KursTransaksi.HasValue Then
                                If obj.Asing_KursTransaksi.Value > 0 Then
                                    Try
                                        singleDetailKasKeluar.Jumlah = (CDec(obj.Asing_TotalKasKeluarDalamRupiah.GetValueOrDefault) / CDec(obj.Asing_KursTransaksi.GetValueOrDefault)).ToString
                                    Catch ex As Exception
                                        singleDetailKasKeluar.Jumlah = obj.Asing_TotalKasKeluarDalamRupiah.GetValueOrDefault
                                    End Try

                                Else
                                    singleDetailKasKeluar.Jumlah = obj.Asing_TotalKasKeluarDalamRupiah.GetValueOrDefault
                                End If
                            End If

                            singleDetailKasKeluar.JumlahRp = obj.Asing_TotalKasKeluarDalamRupiah.GetValueOrDefault
                            singleDetailKasKeluar.KursTransaksi = obj.Asing_KursTransaksi.GetValueOrDefault

                            Dim ID As Decimal = 0
                            If obj.Asing_FK_MsCurrency_Id.HasValue Then
                                ID = obj.Asing_FK_MsCurrency_Id
                            Else 'Jika Nothing pake IDR
                                ID = defaultcurrency
                            End If

                            Dim objKurs As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(ID)
                            If Not IsNothing(objKurs) Then
                                singleDetailKasKeluar.MataUang = objKurs.Code & " - " & objKurs.Name
                            End If

                            objDetailKasKeluar.Add(singleDetailKasKeluar)
                        Else
                            If obj.KasKeluar.HasValue Then
                                singleDetailKasKeluar.Jumlah = 0
                                singleDetailKasKeluar.JumlahRp = obj.KasKeluar.GetValueOrDefault
                                singleDetailKasKeluar.KursTransaksi = 0
                                Dim objKurs As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(defaultcurrency) ' Indonesia IDR
                                If Not IsNothing(objKurs) Then
                                    singleDetailKasKeluar.MataUang = objKurs.Code & " - " & objKurs.Name
                                End If
                                detailkaskeluar += obj.KasKeluar.GetValueOrDefault
                                objDetailKasKeluar.Add(singleDetailKasKeluar)
                            End If
                        End If

                    ElseIf obj.KasKeluar.HasValue Then
                        singleDetailKasKeluar.Jumlah = 0
                        singleDetailKasKeluar.JumlahRp = obj.KasKeluar.GetValueOrDefault
                        singleDetailKasKeluar.KursTransaksi = 0
                        Dim objKurs As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(defaultcurrency) ' Indonesia IDR
                        If Not IsNothing(objKurs) Then
                            singleDetailKasKeluar.MataUang = objKurs.Code & " - " & objKurs.Name
                        End If
                        detailkaskeluar += obj.KasKeluar.GetValueOrDefault
                        objDetailKasKeluar.Add(singleDetailKasKeluar)
                    End If

                Next
                lblDetilKasKeluarJumlahRp.Text = ValidateBLL.FormatMoneyWithComma(objResume(SetnGetRowEdit).TransactionNominal)

                'Detail Kas Keluar GridView
                grvDetilKasKeluar.DataSource = objDetailKasKeluar
                grvDetilKasKeluar.DataBind()


                'LoadData ke field Kas Keluar
                txtTRXKKTanggalTransaksi.Text = FormatDate(objResume(SetnGetRowEdit).TransactionCashOut.TanggalTransaksi)
                txtTRXKKNamaKantor.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.NamaKantorPJK)

                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashOut.FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTRXKKKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTRXKKKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashOut.FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTRXKKProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTRXKKProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                txtTRXKKRekeningKK.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.NomorRekening)
                SafeSelectedValue(rblTRXKKTipePelapor, objResume(SetnGetRowEdit).TransactionCashOut.TipeTerlapor)
                'Individu
                txtTRXKKINDVGelar.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_Gelar)
                txtTRXKKINDVNamaLengkap.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_NamaLengkap)
                txtTRXKKINDVTempatLahir.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_TempatLahir)
                txtTRXKKINDVTglLahir.Text = FormatDate(objResume(SetnGetRowEdit).TransactionCashOut.INDV_TanggalLahir)
                'If txtTRXKKINDVTglLahir.Text = Date.MinValue Then
                '    txtTRXKKINDVTglLahir.Text = ""
                'End If
                If objResume(SetnGetRowEdit).TransactionCashOut.INDV_Kewarganegaraan <> "-" And objResume(SetnGetRowEdit).TransactionCashOut.INDV_Kewarganegaraan.Trim <> "" Then
                    SafeSelectedValue(rblTRXKKINDVKewarganegaraan, objResume(SetnGetRowEdit).TransactionCashOut.INDV_Kewarganegaraan)
                End If
                If rblTRXKKINDVKewarganegaraan.SelectedIndex <> -1 Then
                    i = 0
                    For Each itemCboNegara As ListItem In cboTRXKKINDVNegara.Items
                        If objResume(SetnGetRowEdit).TransactionCashOut.INDV_FK_MsNegara_Id.ToString = itemCboNegara.Value.ToString Then
                            cboTRXKKINDVNegara.SelectedIndex = i
                            Exit For
                        End If
                        i = i + 1
                    Next
                End If
                If objResume(SetnGetRowEdit).TransactionCashOut.CORP_TipeAlamat.GetValueOrDefault <> Nothing Then
                    rblTRXKKCORPTipeAlamat.SelectedIndex = CInt(objResume(SetnGetRowEdit).TransactionCashOut.CORP_TipeAlamat)
                End If
                txtTRXKKINDVDOMNamaJalan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_DOM_NamaJalan)
                txtTRXKKINDVDOMRTRW.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_DOM_RTRW)
                objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objResume(SetnGetRowEdit).TransactionCashOut.INDV_DOM_FK_MsKelurahan_Id)
                If Not IsNothing(objSKelurahan) Then
                    txtTRXKKINDVDOMKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
                    hfTRXKKINDVDOMKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
                End If
                objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objResume(SetnGetRowEdit).TransactionCashOut.INDV_DOM_FK_MsKecamatan_Id)
                If Not IsNothing(objSkecamatan) Then
                    txtTRXKKINDVDOMKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
                    hfTRXKKINDVDOMKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashOut.INDV_DOM_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTRXKKINDVDOMKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTRXKKINDVDOMKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtTRXKKINDVDOMKodePos.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_DOM_KodePos)
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashOut.INDV_DOM_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTRXKKINDVDOMProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTRXKKINDVDOMProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                'chkTRXKKINDVCopyDOM.Checked = False
                txtTRXKKINDVIDNamaJalan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_ID_NamaJalan)
                txtTRXKKINDVIDRTRW.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_ID_RTRW)
                objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objResume(SetnGetRowEdit).TransactionCashOut.INDV_ID_FK_MsKelurahan_Id)
                If Not IsNothing(objSKelurahan) Then
                    txtTRXKKINDVIDKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
                    hfTRXKKINDVIDKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
                End If
                objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objResume(SetnGetRowEdit).TransactionCashOut.INDV_ID_FK_MsKecamatan_Id)
                If Not IsNothing(objSkecamatan) Then
                    txtTRXKKINDVIDKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
                    hfTRXKKINDVIDKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashOut.INDV_ID_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTRXKKINDVIDKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTRXKKINDVIDKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtTRXKKINDVIDKodePos.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_ID_KodePos)
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashOut.INDV_ID_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTRXKKINDVIDProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTRXKKINDVIDProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                txtTRXKKINDVNANamaJalan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_NA_NamaJalan)

                'objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objResume(SetnGetRowEdit).TransactionCashOut.INDV_NA_FK_MsNegara_Id)
                'If Not IsNothing(objSMapNegara) Then
                '    objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                '    If Not IsNothing(objSnegara) Then
                '        txtTRXKKINDVNANegara.Text = Safe(objSnegara.NamaNegara)
                '        hfTRXKKINDVNANegara.Value = Safe(objSnegara.IDNegaraNCBS)
                '    End If
                'End If
                Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objResume(SetnGetRowEdit).TransactionCashOut.INDV_NA_FK_MsNegara_Id.GetValueOrDefault(0))
                    If Not objNegaraRef Is Nothing Then
                        txtTRXKKINDVNANegara.Text = Safe(objNegaraRef.NamaNegara)
                        hfTRXKKINDVNANegara.Value = Safe(objNegaraRef.IDNegara)
                    End If
                End Using
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashOut.INDV_NA_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTRXKKINDVNAProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTRXKKINDVNAProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashOut.INDV_NA_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTRXKKINDVNAKota.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTRXKKINDVNAKota.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtTRXKKINDVNAKodePos.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_NA_KodePos)
                If objResume(SetnGetRowEdit).TransactionCashOut.INDV_FK_MsIDType_Id.GetValueOrDefault <> Nothing Then
                    i = 0
                    For Each itemJenisID As ListItem In cboTRXKKINDVJenisID.Items
                        If itemJenisID.Value = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_FK_MsIDType_Id) Then
                            cboTRXKKINDVJenisID.SelectedIndex = i
                            Exit For
                        End If
                        i = i + 1
                    Next
                End If
                txtTRXKKINDVNomorId.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_NomorId)
                txtTRXKKINDVNPWP.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_NPWP)
                objSPekerjaan = objPekerjaan.Find(MsPekerjaanColumn.IDPekerjaan, objResume(SetnGetRowEdit).TransactionCashOut.INDV_FK_MsPekerjaan_Id.GetValueOrDefault)
                If Not IsNothing(objSPekerjaan) Then
                    txtTRXKKINDVPekerjaan.Text = Safe(objSPekerjaan.NamaPekerjaan)
                    hfTRXKKINDVPekerjaan.Value = Safe(objSPekerjaan.IDPekerjaan)
                End If
                txtTRXKKINDVJabatan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_Jabatan)
                txtTRXKKINDVPenghasilanRataRata.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_PenghasilanRataRata)
                txtTRXKKINDVTempatKerja.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_TempatBekerja)
                txtTRXKKINDVTujuanTrx.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_TujuanTransaksi)
                txtTRXKKINDVSumberDana.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_SumberDana)
                txtTRXKKINDVNamaBankLain.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_NamaBankLain)
                txtTRXKKINDVNoRekTujuan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_NomorRekeningTujuan)

                'CORP
                If objResume(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault <> Nothing Then
                    i = 0
                    For Each itemBentukBadangUsaha As ListItem In cboTRXKKCORPBentukBadanUsaha.Items
                        If itemBentukBadangUsaha.Value.ToString = objResume(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsBentukBadanUsaha_Id Then
                            cboTRXKKCORPBentukBadanUsaha.SelectedIndex = i
                            Exit For
                        End If
                        i = i + 1
                    Next
                End If

                txtTRXKKCORPNama.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_Nama)
                objSBidangUsaha = objBidangUsaha.Find(MsBidangUsahaColumn.IdBidangUsaha, objResume(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsBidangUsaha_Id)
                If Not IsNothing(objSBidangUsaha) Then
                    txtTRXKKCORPBidangUsaha.Text = Safe(objSBidangUsaha.NamaBidangUsaha)
                    hfTRXKKCORPBidangUsaha.Value = Safe(objSBidangUsaha.IdBidangUsaha)
                End If
                If objResume(SetnGetRowEdit).TransactionCashOut.CORP_TipeAlamat.GetValueOrDefault <> Nothing Then
                    SafeNumIndex(rblTRXKKCORPTipeAlamat, objResume(SetnGetRowEdit).TransactionCashOut.CORP_TipeAlamat)
                End If
                txtTRXKKCORPDLNamaJalan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_NamaJalan)
                txtTRXKKCORPDLRTRW.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_RTRW)
                objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objResume(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsKelurahan_Id)
                If Not IsNothing(objSKelurahan) Then
                    txtTRXKKCORPDLKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
                    hfTRXKKCORPDLKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
                End If
                objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objResume(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsKecamatan_Id)
                If Not IsNothing(objSkecamatan) Then
                    txtTRXKKCORPDLKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
                    hfTRXKKCORPDLKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTRXKKCORPDLKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTRXKKCORPDLKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtTRXKKCORPDLKodePos.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_KodePos)
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTRXKKCORPDLProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTRXKKCORPDLProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If

                'objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objResume(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsNegara_Id)
                'If Not IsNothing(objSMapNegara) Then
                '    objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                '    If Not IsNothing(objSnegara) Then
                '        txtTRXKKCORPDLNegara.Text = Safe(objSnegara.NamaNegara)
                '        hfTRXKKCORPDLNegara.Value = Safe(objSnegara.IDNegaraNCBS)
                '    End If
                'End If
                Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objResume(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsNegara_Id.GetValueOrDefault(0))
                    If Not objNegaraRef Is Nothing Then
                        txtTRXKKCORPDLNegara.Text = Safe(objNegaraRef.NamaNegara)
                        hfTRXKKCORPDLNegara.Value = Safe(objNegaraRef.IDNegara)
                    End If
                End Using
                txtTRXKKCORPLNNamaJalan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_LN_NamaJalan)

                'objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objResume(SetnGetRowEdit).TransactionCashOut.CORP_LN_FK_MsNegara_Id)
                'If Not IsNothing(objSMapNegara) Then
                '    objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                '    If Not IsNothing(objSnegara) Then
                '        txtTRXKKCORPLNNegara.Text = Safe(objSnegara.NamaNegara)
                '        hfTRXKKCORPLNNegara.Value = Safe(objSnegara.IDNegaraNCBS)
                '    End If
                'End If
                Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objResume(SetnGetRowEdit).TransactionCashOut.CORP_LN_FK_MsNegara_Id.GetValueOrDefault(0))
                    If Not objNegaraRef Is Nothing Then
                        txtTRXKKCORPLNNegara.Text = Safe(objNegaraRef.NamaNegara)
                        hfTRXKKCORPLNNegara.Value = Safe(objNegaraRef.IDNegara)
                    End If
                End Using
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashOut.CORP_LN_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTRXKKCORPLNProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTRXKKCORPLNProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashOut.CORP_LN_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTRXKKCORPLNKota.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTRXKKCORPLNKota.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtTRXKKCORPLNKodePos.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_LN_KodePos)
                txtTRXKKCORPNPWP.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_NPWP)
                txtTRXKKCORPTujuanTrx.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_TujuanTransaksi)
                txtTRXKKCORPSumberDana.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_SumberDana)
                txtTRXKKCORPNamaBankLain.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_NamaBankLain)
                txtTRXKKCORPNoRekeningTujuan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_NomorRekeningTujuan)

            End If

            ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('IdTerlapor','Img1','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
            ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('Umum','searchimage2','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)

        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Sub add()
        Dim defaultDate As New Date(1900, 1, 1)
        Dim objResume As List(Of ResumeKasMasukKeluarLTKT) = SetnGetResumeKasMasukKasKeluar
        'Buat LTKT 


        'ini dipake terus buat auditTrail
        'Dim objAuditTrailType As MsAuditTrailEntityType = DataRepository.MsAuditTrailEntityTypeProvider.GetByPk_MsAuditTrailEntityTypeId(18)



        'LTKT Detil 
        Using objLTKTDetil As New LTKT
            'isi object dari control
            FillLTKTobject(objLTKTDetil)


            DataRepository.LTKTProvider.Save(objLTKTDetil)
            AuditTrailBLL.InsertAuditTrail(AuditTrailBLL.AuditTrailOperation.Add, Date.Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Accepted add new data LTKT", objLTKTDetil, Nothing)
            For Each objSingleResume As ResumeKasMasukKeluarLTKT In objResume


                'transaction  detail
                If objSingleResume.Kas = "Kas Masuk" Then
                    Using objLTKTTransactionCashIn As New LTKTTransactionCashIn

                        FillTransactionCashinObject(objLTKTDetil, objSingleResume, objLTKTTransactionCashIn)

                        DataRepository.LTKTTransactionCashInProvider.Save(objLTKTTransactionCashIn)
                        AuditTrailBLL.InsertAuditTrail(AuditTrailBLL.AuditTrailOperation.Add, Date.Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Accepted add new data LTKT Transaction CashIn", objLTKTTransactionCashIn, Nothing)

                        'Insert Detail Transaction 
                        For Each objSingleDetail As LTKTDetailCashInTransaction In objSingleResume.DetailTransactionCashIn
                            Using objLTKTDetailTransactionCashIn As New LTKTDetailCashInTransaction
                                'isi object dari control
                                FillTransactionDetailCashin(objLTKTTransactionCashIn, _
                                    objSingleDetail, _
                                    objLTKTDetailTransactionCashIn)


                                DataRepository.LTKTDetailCashInTransactionProvider.Save(objLTKTDetailTransactionCashIn)
                                AuditTrailBLL.InsertAuditTrail(AuditTrailBLL.AuditTrailOperation.Add, Date.Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Accepted add new data LTKT Transaction CashIn Detail ", objLTKTDetailTransactionCashIn, Nothing)
                            End Using
                        Next


                    End Using
                Else
                    Using objLTKTTransactionCashOut As New LTKTTransactionCashOut
                        'isi object dari control
                        FillTransactionCasoutObject(objLTKTDetil, _
                                   objSingleResume, _
                                   objLTKTTransactionCashOut)


                        DataRepository.LTKTTransactionCashOutProvider.Save(objLTKTTransactionCashOut)
                        AuditTrailBLL.InsertAuditTrail(AuditTrailBLL.AuditTrailOperation.Add, Date.Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Accepted add new data LTKT Transaction CashOut ", objLTKTTransactionCashOut, Nothing)
                        'Insert Detail Transaction 
                        For Each objSingleDetail As LTKTDetailCashOutTransaction In objSingleResume.DetailTranscationCashOut
                            Using objLTKTDetailTransactionCashOut As New LTKTDetailCashOutTransaction
                                'isi object dari control
                                FillDetailTransactionCashoutObject(objLTKTTransactionCashOut, _
                                    objSingleDetail, _
                                    objLTKTDetailTransactionCashOut)


                                DataRepository.LTKTDetailCashOutTransactionProvider.Save(objLTKTDetailTransactionCashOut)
                                AuditTrailBLL.InsertAuditTrail(AuditTrailBLL.AuditTrailOperation.Add, Date.Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Accepted add new data LTKT Transaction CashOut Detail", objLTKTDetailTransactionCashOut, Nothing)
                            End Using
                        Next

                    End Using
                End If
            Next
        End Using

        'delete approval
        deleteApproval()

        mtvPage.ActiveViewIndex = 1
        lblMsg.Text = "Data adding accepted"
    End Sub

    Sub edit()
        Dim defaultDate As New Date(1900, 1, 1)
        Dim objResume As List(Of ResumeKasMasukKeluarLTKT) = SetnGetResumeKasMasukKasKeluar
        'Buat LTKT 


        'ini dipake terus buat auditTrail
        'Dim objAuditTrailType As MsAuditTrailEntityType = DataRepository.MsAuditTrailEntityTypeProvider.GetByPk_MsAuditTrailEntityTypeId(18)

        Dim LobjApprovalDetail As TList(Of LTKT_ApprovalDetail) = DataRepository.LTKT_ApprovalDetailProvider.GetPaged("FK_LTKT_approval_id =  " & getAPPROVALPK, "", 0, 1, Nothing)
        Dim objApprovalDetail As LTKT_ApprovalDetail = LobjApprovalDetail(0)

        'LTKT Detil 
        Using objLTKTDetil As LTKT = DataRepository.LTKTProvider.GetByPK_LTKT_Id(objApprovalDetail.PK_LTKT_Id)
            FillLTKTobject(objLTKTDetil)


            DataRepository.LTKTProvider.Save(objLTKTDetil)
            AuditTrailBLL.InsertAuditTrail(AuditTrailBLL.AuditTrailOperation.Edit, Date.Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Accepted edit new data LTKT", objLTKTDetil, Nothing)

            'hapus TransctionCashin sampai ke detailnya
            Using deleteTransactionCashinList As TList(Of LTKTTransactionCashIn) = DataRepository.LTKTTransactionCashInProvider.GetPaged(LTKTTransactionCashInColumn.FK_LTKT_Id.ToString & " = " & objLTKTDetil.PK_LTKT_Id.ToString, "", 0, Integer.MaxValue, 0)
                If deleteTransactionCashinList.Count > 0 Then
                    For Each singleDeleteTransactionCashinList As LTKTTransactionCashIn In deleteTransactionCashinList
                        Using deleteTransactionCashinDetailList As TList(Of LTKTDetailCashInTransaction) = DataRepository.LTKTDetailCashInTransactionProvider.GetPaged(LTKTDetailCashInTransactionColumn.FK_LTKTTransactionCashIn_Id.ToString & " = " & singleDeleteTransactionCashinList.PK_LTKTTransactionCashIn_Id.ToString, "", 0, Integer.MaxValue, 0)
                            If deleteTransactionCashinDetailList.Count > 0 Then
                                For Each singleDeleteTransactionCashinDetailList As LTKTDetailCashInTransaction In deleteTransactionCashinDetailList
                                    DataRepository.LTKTDetailCashInTransactionProvider.Delete(singleDeleteTransactionCashinDetailList)
                                Next
                            End If
                        End Using
                        DataRepository.LTKTTransactionCashInProvider.Delete(singleDeleteTransactionCashinList)
                    Next
                End If
            End Using

            'hapus TransctionCashOut sampai ke detailnya
            Using deleteTransactionCashOutList As TList(Of LTKTTransactionCashOut) = DataRepository.LTKTTransactionCashOutProvider.GetPaged(LTKTTransactionCashOutColumn.FK_LTKT_Id.ToString & " = " & objLTKTDetil.PK_LTKT_Id.ToString, "", 0, Integer.MaxValue, 0)
                If deleteTransactionCashOutList.Count > 0 Then
                    For Each singleDeleteTransactionCashOutList As LTKTTransactionCashOut In deleteTransactionCashOutList
                        Using deleteTransactionCashOutDetailList As TList(Of LTKTDetailCashOutTransaction) = DataRepository.LTKTDetailCashOutTransactionProvider.GetPaged(LTKTDetailCashOutTransactionColumn.FK_LTKTTransactionCashOut_Id.ToString & " = " & singleDeleteTransactionCashOutList.PK_LTKTTransactionCashOut_Id.ToString, "", 0, Integer.MaxValue, 0)
                            If deleteTransactionCashOutDetailList.Count > 0 Then
                                For Each singleDeleteTransactionCashOutDetailList As LTKTDetailCashOutTransaction In deleteTransactionCashOutDetailList
                                    DataRepository.LTKTDetailCashOutTransactionProvider.Delete(singleDeleteTransactionCashOutDetailList)
                                Next
                            End If
                        End Using
                        DataRepository.LTKTTransactionCashOutProvider.Delete(singleDeleteTransactionCashOutList)
                    Next
                End If
            End Using



            For Each objSingleResume As ResumeKasMasukKeluarLTKT In objResume

                'transaction  detail
                If objSingleResume.Kas = "Kas Masuk" Then
                    Using objLTKTTransactionCashIn As New LTKTTransactionCashIn

                        FillTransactionCashinObject(objLTKTDetil, objSingleResume, objLTKTTransactionCashIn)

                        DataRepository.LTKTTransactionCashInProvider.Save(objLTKTTransactionCashIn)
                        AuditTrailBLL.InsertAuditTrail(AuditTrailBLL.AuditTrailOperation.Edit, Date.Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Accepted add new data LTKT Transaction CashIn", objLTKTTransactionCashIn, Nothing)

                        'Insert Detail Transaction 
                        For Each objSingleDetail As LTKTDetailCashInTransaction In objSingleResume.DetailTransactionCashIn
                            Using objLTKTDetailTransactionCashIn As New LTKTDetailCashInTransaction
                                FillTransactionDetailCashin(objLTKTTransactionCashIn, objSingleDetail, objLTKTDetailTransactionCashIn)

                                DataRepository.LTKTDetailCashInTransactionProvider.Save(objLTKTDetailTransactionCashIn)
                                AuditTrailBLL.InsertAuditTrail(AuditTrailBLL.AuditTrailOperation.Edit, Date.Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Accepted add new data LTKT Transaction CashIn Detail ", objLTKTDetailTransactionCashIn, Nothing)
                            End Using
                        Next


                    End Using
                Else

                    Using objLTKTTransactionCashOut As New LTKTTransactionCashOut
                        FillTransactionCasoutObject(objLTKTDetil, objSingleResume, objLTKTTransactionCashOut)

                        DataRepository.LTKTTransactionCashOutProvider.Save(objLTKTTransactionCashOut)
                        AuditTrailBLL.InsertAuditTrail(AuditTrailBLL.AuditTrailOperation.Edit, Date.Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Accepted add new data LTKT Transaction CashOut ", objLTKTTransactionCashOut, Nothing)
                        'Insert Detail Transaction 
                        For Each objSingleDetail As LTKTDetailCashOutTransaction In objSingleResume.DetailTranscationCashOut
                            Using objLTKTDetailTransactionCashOut As New LTKTDetailCashOutTransaction
                                FillDetailTransactionCashoutObject(objLTKTTransactionCashOut, objSingleDetail, objLTKTDetailTransactionCashOut)

                                DataRepository.LTKTDetailCashOutTransactionProvider.Save(objLTKTDetailTransactionCashOut)
                                AuditTrailBLL.InsertAuditTrail(AuditTrailBLL.AuditTrailOperation.Edit, Date.Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Accepted add new data LTKT Transaction CashOut Detail", objLTKTDetailTransactionCashOut, Nothing)
                            End Using
                        Next

                    End Using
                End If
            Next
        End Using

        'delete approval
        deleteApproval()

        mtvPage.ActiveViewIndex = 1
        lblMsg.Text = "Data editing accepted"
    End Sub

    Sub delete()
        'Dim objAuditTrailType As MsAuditTrailEntityType = DataRepository.MsAuditTrailEntityTypeProvider.GetByPk_MsAuditTrailEntityTypeId(18)

        Using objApprovalDetail As TList(Of LTKT_ApprovalDetail) = DataRepository.LTKT_ApprovalDetailProvider.GetPaged(LTKT_ApprovalDetailColumn.FK_LTKT_Approval_Id.ToString & " = " & getAPPROVALPK.ToString, "", 0, Integer.MaxValue, 0)
            If objApprovalDetail.Count > 0 Then
                Using objLTKT As LTKT = DataRepository.LTKTProvider.GetByPK_LTKT_Id(objApprovalDetail(0).PK_LTKT_Id)
                    If Not IsNothing(objLTKT) Then
                        'delete cash in
                        Using objLTKTTransactionCashInList As TList(Of LTKTTransactionCashIn) = DataRepository.LTKTTransactionCashInProvider.GetPaged(LTKTTransactionCashInColumn.FK_LTKT_Id.ToString & " = " & objLTKT.PK_LTKT_Id.ToString, "", 0, Integer.MaxValue, 0)
                            If objLTKTTransactionCashInList.Count > 0 Then
                                For Each singleTCashin As LTKTTransactionCashIn In objLTKTTransactionCashInList
                                    Using objTransactionDetailCashin As TList(Of LTKTDetailCashInTransaction) = DataRepository.LTKTDetailCashInTransactionProvider.GetPaged(LTKTDetailCashInTransactionColumn.FK_LTKTTransactionCashIn_Id.ToString & " = " & singleTCashin.PK_LTKTTransactionCashIn_Id.ToString, "", 0, Integer.MaxValue, 0)
                                        If objTransactionDetailCashin.Count > 0 Then
                                            For Each singleObjTransactionDetailCashin As LTKTDetailCashInTransaction In objTransactionDetailCashin
                                                AuditTrailBLL.InsertAuditTrail(AuditTrailBLL.AuditTrailOperation.Delete, Date.Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Accepted delete data LTKT transaction Cash in Detail", Nothing, singleObjTransactionDetailCashin)
                                                DataRepository.LTKTDetailCashInTransactionProvider.Delete(singleObjTransactionDetailCashin)
                                            Next
                                        End If
                                    End Using
                                    AuditTrailBLL.InsertAuditTrail(AuditTrailBLL.AuditTrailOperation.Delete, Date.Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Accepted delete data LTKT Transaction Cash in", Nothing, singleTCashin)
                                    DataRepository.LTKTTransactionCashInProvider.Delete(singleTCashin)
                                Next
                            End If
                        End Using

                        'delete cash in
                        Using objLTKTTransactionCashOutList As TList(Of LTKTTransactionCashOut) = DataRepository.LTKTTransactionCashOutProvider.GetPaged(LTKTTransactionCashOutColumn.FK_LTKT_Id.ToString & " = " & objLTKT.PK_LTKT_Id.ToString, "", 0, Integer.MaxValue, 0)
                            If objLTKTTransactionCashOutList.Count > 0 Then
                                For Each singleTCashOut As LTKTTransactionCashOut In objLTKTTransactionCashOutList
                                    Using objTransactionDetailCashOut As TList(Of LTKTDetailCashOutTransaction) = DataRepository.LTKTDetailCashOutTransactionProvider.GetPaged(LTKTDetailCashOutTransactionColumn.FK_LTKTTransactionCashOut_Id.ToString & " = " & singleTCashOut.PK_LTKTTransactionCashOut_Id.ToString, "", 0, Integer.MaxValue, 0)
                                        If objTransactionDetailCashOut.Count > 0 Then
                                            For Each singleObjTransactionDetailCashOut As LTKTDetailCashOutTransaction In objTransactionDetailCashOut
                                                AuditTrailBLL.InsertAuditTrail(AuditTrailBLL.AuditTrailOperation.Delete, Date.Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Accepted delete data LTKT transaction CashOut Detail", Nothing, singleObjTransactionDetailCashOut)
                                                DataRepository.LTKTDetailCashOutTransactionProvider.Delete(singleObjTransactionDetailCashOut)
                                            Next
                                        End If
                                    End Using
                                    AuditTrailBLL.InsertAuditTrail(AuditTrailBLL.AuditTrailOperation.Delete, Date.Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Accepted delete data LTKT Transaction CashOut", Nothing, singleTCashOut)
                                    DataRepository.LTKTTransactionCashOutProvider.Delete(singleTCashOut)
                                Next
                            End If
                        End Using
                    End If
                    AuditTrailBLL.InsertAuditTrail(AuditTrailBLL.AuditTrailOperation.Delete, Date.Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Accepted delete data LTKT ", Nothing, objLTKT)
                    DataRepository.LTKTProvider.Delete(objLTKT)
                End Using
            End If
        End Using

        'delete approval
        deleteApproval()

        mtvPage.ActiveViewIndex = 1
        lblMsg.Text = "Data deletetion accepted"
    End Sub


#End Region

End Class



#Region "Class LTKTDetailCashIn"
Public Class LTKTDetailCashInApprovalDetail

    Private _mataUang As String
    Public Property MataUang() As String
        Get
            Return _mataUang
        End Get
        Set(ByVal value As String)
            _mataUang = value
        End Set
    End Property

    Private _kursTransaksi As String
    Public Property KursTransaksi() As String
        Get
            Return _kursTransaksi
        End Get
        Set(ByVal value As String)
            _kursTransaksi = value
        End Set
    End Property

    Private _jumlah As String
    Public Property Jumlah() As String
        Get
            Return _jumlah
        End Get
        Set(ByVal value As String)
            _jumlah = value
        End Set
    End Property

    Private _jumlahRp As String
    Public Property JumlahRp() As String
        Get
            Return _jumlahRp
        End Get
        Set(ByVal value As String)
            _jumlahRp = value
        End Set
    End Property

End Class
#End Region

#Region " Class ResumeKasMasukKeluarLTKT"
Public Class ResumeKasMasukKeluarLTKT

    Private _kas As String
    Public Property Kas() As String
        Get
            Return _kas
        End Get
        Set(ByVal value As String)
            _kas = value
        End Set
    End Property

    Private _transactionDate As String
    Public Property TransactionDate() As String
        Get
            Return _transactionDate
        End Get
        Set(ByVal value As String)
            _transactionDate = value
        End Set
    End Property

    Private _branch As String
    Public Property Branch() As String
        Get
            Return _branch
        End Get
        Set(ByVal value As String)
            _branch = value
        End Set
    End Property

    Private _accountNumber As String
    Public Property AccountNumber() As String
        Get
            Return _accountNumber
        End Get
        Set(ByVal value As String)
            _accountNumber = value
        End Set
    End Property

    Private _Type As String
    Public Property Type() As String
        Get
            Return _Type
        End Get
        Set(ByVal value As String)
            _Type = value
        End Set
    End Property

    Private _transactionNominal As String
    Public Property TransactionNominal() As String
        Get
            Return _transactionNominal
        End Get
        Set(ByVal value As String)
            _transactionNominal = value
        End Set
    End Property


    Private _transactionCashIn As LTKTTransactionCashIn
    Public Property TransactionCashIn() As LTKTTransactionCashIn
        Get
            Return _transactionCashIn
        End Get
        Set(ByVal value As LTKTTransactionCashIn)
            _transactionCashIn = value
        End Set
    End Property


    Private _transactionCashOut As LTKTTransactionCashOut
    Public Property TransactionCashOut() As LTKTTransactionCashOut
        Get
            Return _transactionCashOut
        End Get
        Set(ByVal value As LTKTTransactionCashOut)
            _transactionCashOut = value
        End Set
    End Property


    Private _detailTransactionCashIn As List(Of LTKTDetailCashInTransaction)
    Public Property DetailTransactionCashIn() As List(Of LTKTDetailCashInTransaction)
        Get
            Return _detailTransactionCashIn
        End Get
        Set(ByVal value As List(Of LTKTDetailCashInTransaction))
            _detailTransactionCashIn = value
        End Set
    End Property


    Private _detailTransactionCashOut As List(Of LTKTDetailCashOutTransaction)
    Public Property DetailTranscationCashOut() As List(Of LTKTDetailCashOutTransaction)
        Get
            Return _detailTransactionCashOut
        End Get
        Set(ByVal value As List(Of LTKTDetailCashOutTransaction))
            _detailTransactionCashOut = value
        End Set
    End Property



End Class

#End Region

