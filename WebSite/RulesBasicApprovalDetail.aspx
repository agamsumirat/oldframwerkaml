<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="RulesBasicApprovalDetail.aspx.vb" Inherits="RulesBasicApprovalDetail"
    Title="Basic Rules - Approval Detail" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    <table cellspacing="4" cellpadding="0" width="100%">
        <tr>
            <td valign="bottom" align="left">
                <img height="15" src="images/dot_title.gif" width="15"></td>
            <td class="maintitle" valign="bottom" width="99%">
                <asp:Label ID="LabelTitle" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td align="left" valign="bottom">
            </td>
            <td class="maintitle" valign="bottom" width="99%">
                Activity :<asp:Label ID="LabelActivity" runat="server"></asp:Label></td>
        </tr>
    </table>
    <table style="width: 100%" id="TableDetail" runat="server">
        <tr>
            <td>
                <asp:Panel ID="PanelOldValue" runat="server" GroupingText="OLD VALUE" Height="50px"
                    Width="100%">
                    <table style="width: 100%">
                        <tr>
                            <td nowrap="nowrap" width="1%">
                                Basic Rules Name</td>
                            <td width="1">
                                :</td>
                            <td>
                                <asp:Label ID="LabelBasicRulesNameOld" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" style="height: 15px" width="1%">
                                Description</td>
                            <td style="height: 15px" width="1">
                                :</td>
                            <td style="height: 15px">
                                <asp:Label ID="LabelBasicRulesDescriptionOld" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" width="1%">
                                Alert To</td>
                            <td width="1">
                                :</td>
                            <td>
                                <asp:Label ID="LabelBasicRulesAlertToOld" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" style="height: 15px" width="1%">
                                Enable</td>
                            <td style="height: 15px" width="1">
                            </td>
                            <td style="height: 15px">
                                <asp:Label ID="LabelBasicRulesEnableOld" runat="server"></asp:Label></td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td>
                <asp:Panel ID="PanelNewValue" runat="server" GroupingText="NEW VALUE" Height="50px"
                    Width="100%">
                    <table style="width: 100%">
                        <tr>
                            <td nowrap="nowrap" width="1%">
                                Basic Rules Name</td>
                            <td width="1">
                                :</td>
                            <td>
                                <asp:Label ID="LabelBasicRulesNameNew" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" style="height: 15px" width="1%">
                                Description</td>
                            <td style="height: 15px" width="1">
                                :</td>
                            <td style="height: 15px">
                                <asp:Label ID="LabelBasicRulesDescriptionnew" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" width="1%">
                                Alert To</td>
                            <td width="1">
                                :</td>
                            <td>
                                <asp:Label ID="LabelBasicRulesAlertToNew" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" style="height: 15px" width="1%">
                                Enable</td>
                            <td style="height: 15px" width="1">
                            </td>
                            <td style="height: 15px">
                                <asp:Label ID="LabelBasicRulesEnableNew" runat="server"></asp:Label></td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <ajax:AjaxPanel ID="AjaxPanel2" runat="server" Width="100%">
                    <asp:Menu ID="MenuTab" runat="server" Orientation="Horizontal" CssClass="tabs">
                        <Items>
                            <asp:MenuItem Text="Customer Data" Value="0" Selected="True"></asp:MenuItem>
                            <asp:MenuItem Text="Transaction" Value="1"></asp:MenuItem>
                        </Items>
                        <StaticMenuItemStyle CssClass="tab" />
                        <StaticSelectedStyle CssClass="selectedTab" />
                    </asp:Menu>                
                    <asp:MultiView ID="MultiViewTab" runat="server">
                        <asp:View ID="ViewCustomerData" runat="server">
                            <table style="width: 100%">
                                <tr>
                                    <td style="height: 384px" valign="top">
                                        <asp:Panel ID="PanelCustomerDataOLDValue" runat="server" GroupingText="OLD VALUE"
                                            Height="100%" Width="100%">
                                            <table style="width: 100%">
                                                <tr>
                                                    <td>
                                                        <asp:Panel ID="PanelCustomer" runat="server" GroupingText="CUSTOMER" Height="100%"
                                                            Width="100%">
                                                            <table style="width: 100%">
                                                                <tr>
                                                                    <td nowrap="nowrap" width="1%">
                                                                        Customer Of</td>
                                                                    <td width="1">
                                                                        :</td>
                                                                    <td>
                                                                        <asp:Label ID="LabelCustomerOfOld" runat="server"></asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td nowrap="nowrap">
                                                                        Customer Type</td>
                                                                    <td width="1">
                                                                        :</td>
                                                                    <td>
                                                                        <asp:Label ID="LabelCustomerTypeOld" runat="server"></asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td nowrap="nowrap">
                                                                        Negative List</td>
                                                                    <td width="1">
                                                                        :</td>
                                                                    <td>
                                                                        <asp:Label ID="LabelNegativeListOld" runat="server"></asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td nowrap="nowrap">
                                                                        High Risk Business</td>
                                                                    <td width="1">
                                                                        :</td>
                                                                    <td>
                                                                        <asp:Label ID="LabelHighRiskBusinessOld" runat="server"></asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td nowrap="nowrap">
                                                                        High Risk Country</td>
                                                                    <td width="1">
                                                                        :</td>
                                                                    <td>
                                                                        <asp:Label ID="LabelHighRiskCountryOld" runat="server"></asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td nowrap="nowrap">
                                                                        Customer Sub Type</td>
                                                                    <td width="1">
                                                                        :</td>
                                                                    <td>
                                                                        <asp:Label ID="LabelCustomerSubTypeOld" runat="server"></asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td nowrap="nowrap">
                                                                        Business Type</td>
                                                                    <td width="1">
                                                                        :</td>
                                                                    <td>
                                                                        <asp:Label ID="LabelBusinessTypeOld" runat="server"></asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td nowrap="nowrap">
                                                                        Internal Industri Code</td>
                                                                    <td width="1">
                                                                        :</td>
                                                                    <td>
                                                                        <asp:Label ID="LabelInternalIndustriCodeOld" runat="server"></asp:Label></td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Panel ID="PanelAccountOldValue" runat="server" GroupingText="ACCOUNT" Height="100%"
                                                            Width="100%">
                                                            <table style="width: 100%">
                                                                <tr>
                                                                    <td nowrap="nowrap" width="1%">
                                                                        Opened With</td>
                                                                    <td width="1">
                                                                        :</td>
                                                                    <td>
                                                                        <asp:Label ID="LabelOpenedWithOld" runat="server"></asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td nowrap="nowrap" width="1%">
                                                                        Account Type</td>
                                                                    <td width="1">
                                                                        :</td>
                                                                    <td>
                                                                        <asp:Label ID="LabelAccountTypeOld" runat="server"></asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td nowrap="nowrap" width="1%" style="height: 15px">
                                                                        Account Status</td>
                                                                    <td width="1" style="height: 15px">
                                                                        :</td>
                                                                    <td style="height: 15px">
                                                                        <asp:Label ID="LabelAccountStatusOld" runat="server"></asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td nowrap="nowrap" width="1%">
                                                                        Segmentation</td>
                                                                    <td width="1">
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="LabelSegmentationOld" runat="server"></asp:Label></td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                    <td valign="top" style="height: 384px">
                                        <asp:Panel ID="PanelCustomerDataNewValue" runat="server" GroupingText="NEW VALUE"
                                            Height="100%" Width="100%">
                                            <table style="width: 100%">
                                                <tr>
                                                    <td>
                                                        <asp:Panel ID="Panel1" runat="server" GroupingText="CUSTOMER" Height="100%" Width="100%">
                                                            <table style="width: 100%">
                                                                <tr>
                                                                    <td nowrap="nowrap" width="1%">
                                                                        Customer Of</td>
                                                                    <td width="1">
                                                                        :</td>
                                                                    <td>
                                                                        <asp:Label ID="LabelCustomerOfNew" runat="server"></asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td nowrap="nowrap">
                                                                        Customer Type</td>
                                                                    <td width="1">
                                                                        :</td>
                                                                    <td>
                                                                        <asp:Label ID="LabelCustomerTypeNew" runat="server"></asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td nowrap="nowrap">
                                                                        Negative List</td>
                                                                    <td width="1">
                                                                        :</td>
                                                                    <td>
                                                                        <asp:Label ID="LabelNegativeListNew" runat="server"></asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td nowrap="nowrap">
                                                                        High Risk Business</td>
                                                                    <td width="1">
                                                                        :</td>
                                                                    <td>
                                                                        <asp:Label ID="LabelHighRiskBusinessNew" runat="server"></asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td nowrap="nowrap">
                                                                        High Risk Country</td>
                                                                    <td width="1">
                                                                        :</td>
                                                                    <td>
                                                                        <asp:Label ID="LabelHighRiskCountryNew" runat="server"></asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td nowrap="nowrap">
                                                                        Customer Sub Type</td>
                                                                    <td width="1">
                                                                        :</td>
                                                                    <td>
                                                                        <asp:Label ID="LabelCustomerSubTypeNew" runat="server"></asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td nowrap="nowrap">
                                                                        Business Type</td>
                                                                    <td width="1">
                                                                        :</td>
                                                                    <td>
                                                                        <asp:Label ID="LabelBusinessTypeNew" runat="server"></asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td nowrap="nowrap">
                                                                        Internal Industri Code</td>
                                                                    <td width="1">
                                                                        :</td>
                                                                    <td>
                                                                        <asp:Label ID="LabelInternalIndustriCodeNew" runat="server"></asp:Label></td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Panel ID="Panel2" runat="server" GroupingText="ACCOUNT" Height="100%" Width="100%">
                                                            <table style="width: 100%">
                                                                <tr>
                                                                    <td nowrap="nowrap" width="1%">
                                                                        Opened With</td>
                                                                    <td width="1">
                                                                        :</td>
                                                                    <td>
                                                                        <asp:Label ID="LabelOpenedWithNew" runat="server"></asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td nowrap="nowrap" width="1%">
                                                                        Account Type</td>
                                                                    <td width="1">
                                                                        :</td>
                                                                    <td>
                                                                        <asp:Label ID="LabelAccountTypeNew" runat="server"></asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td nowrap="nowrap" width="1%">
                                                                        Account Status</td>
                                                                    <td width="1">
                                                                        :</td>
                                                                    <td>
                                                                        <asp:Label ID="LabelAccountStatusNew" runat="server"></asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td nowrap="nowrap" width="1%">
                                                                        Segmentation</td>
                                                                    <td width="1">
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="LabelSegmentationNew" runat="server"></asp:Label></td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </asp:View>
                        <asp:View ID="ViewTransaction" runat="server">
                            <table style="width: 100%">
                                <tr>
                                    <td style="height: 290px" valign="top">
                                        <asp:Panel ID="PanelTransactionOld" runat="server" GroupingText="OLD VALUE" Height="100%"
                                            Width="100%">
                                            <table style="width: 100%">
                                                <tr>
                                                    <td>
                                                        <asp:Panel ID="Panel4" runat="server" GroupingText="TRANSACTION" Height="100%" Width="100%">
                                                            <table style="width: 100%">
                                                                <tr>
                                                                    <td colspan="2" nowrap="nowrap" width="1%">
                                                                        Transaction Type</td>
                                                                    <td width="1">
                                                                        :</td>
                                                                    <td>
                                                                        <asp:Label ID="LabelTransactionTypeOld" runat="server"></asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" nowrap="nowrap">
                                                                        Amount</td>
                                                                    <td width="1">
                                                                        :</td>
                                                                    <td>
                                                                        <asp:Label ID="LabelAmountOld" runat="server"></asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" nowrap="nowrap">
                                                                        Check Amount Transaction &gt; Limit Transaction</td>
                                                                    <td width="1">
                                                                        :</td>
                                                                    <td>
                                                                        <asp:Label ID="LabelCheckAmountOld" runat="server"></asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" nowrap="nowrap">
                                                                        Credit / Debit</td>
                                                                    <td width="1">
                                                                        :</td>
                                                                    <td>
                                                                        <asp:Label ID="LabelCreditDebitOld" runat="server"></asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" nowrap="nowrap">
                                                                        Within</td>
                                                                    <td width="1">
                                                                        :</td>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="1" nowrap="nowrap" width="15">
                                                                        &nbsp; &nbsp; &nbsp;
                                                                    </td>
                                                                    <td nowrap="nowrap">
                                                                        Transaction Frequency</td>
                                                                    <td width="1">
                                                                        :</td>
                                                                    <td>
                                                                        <asp:Label ID="LabelTransactionFrequencyOld" runat="server"></asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="1" nowrap="nowrap" width="15">
                                                                    </td>
                                                                    <td nowrap="nowrap">
                                                                        Transaction Period</td>
                                                                    <td width="1">
                                                                        :</td>
                                                                    <td>
                                                                        <asp:Label ID="LabelTransactionPeriodOld" runat="server"></asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" nowrap="nowrap">
                                                                        Auxiliary Transaction Code</td>
                                                                    <td width="1">
                                                                        :</td>
                                                                    <td>
                                                                        <asp:Label ID="LabelAuxiliaryOld" runat="server"></asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" nowrap="nowrap">
                                                                        Country Code</td>
                                                                    <td width="1">
                                                                        :</td>
                                                                    <td>
                                                                        <asp:Label ID="LabelCountryCodeOld" runat="server"></asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" nowrap="nowrap">
                                                                        1 CIF &amp; Related Source Found</td>
                                                                    <td width="1">
                                                                        :</td>
                                                                    <td>
                                                                        <asp:Label ID="LabelCIFRelatedOld" runat="server"></asp:Label></td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                    <td style="height: 290px" valign="top">
                                        <asp:Panel ID="PanelTransactionNew" runat="server" GroupingText="NEW VALUE" Height="100%"
                                            Width="100%">
                                            <table style="width: 100%">
                                                <tr>
                                                    <td>
                                                        <asp:Panel ID="Panel6" runat="server" GroupingText="TRANSACTION" Height="100%" Width="100%">
                                                            <table style="width: 100%">
                                                                <tr>
                                                                    <td colspan="2" nowrap="nowrap" width="1%">
                                                                        Transaction Type</td>
                                                                    <td width="1">
                                                                        :</td>
                                                                    <td>
                                                                        <asp:Label ID="LabelTransactionTypeNew" runat="server"></asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" nowrap="nowrap">
                                                                        Amount</td>
                                                                    <td width="1">
                                                                        :</td>
                                                                    <td>
                                                                        <asp:Label ID="LabelAmountNew" runat="server"></asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" nowrap="nowrap">
                                                                        Check Amount Transaction &gt; Limit Transaction</td>
                                                                    <td width="1">
                                                                        :</td>
                                                                    <td>
                                                                        <asp:Label ID="LabelCheckAmountNew" runat="server"></asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" nowrap="nowrap">
                                                                        Credit / Debit</td>
                                                                    <td width="1">
                                                                        :</td>
                                                                    <td>
                                                                        <asp:Label ID="LabelCreditDebitNew" runat="server"></asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" nowrap="nowrap">
                                                                        Within</td>
                                                                    <td width="1">
                                                                        :</td>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="1" nowrap="nowrap" width="15">
                                                                        &nbsp; &nbsp; &nbsp;
                                                                    </td>
                                                                    <td nowrap="nowrap">
                                                                        Transaction Frequency</td>
                                                                    <td width="1">
                                                                        :</td>
                                                                    <td>
                                                                        <asp:Label ID="LabelTransactionFrequencyNew" runat="server"></asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="1" nowrap="nowrap" width="15">
                                                                    </td>
                                                                    <td nowrap="nowrap">
                                                                        Transaction Period</td>
                                                                    <td width="1">
                                                                        :</td>
                                                                    <td>
                                                                        <asp:Label ID="LabelTransactionPeriodNew" runat="server"></asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" nowrap="nowrap">
                                                                        Auxiliary Transaction Code</td>
                                                                    <td width="1">
                                                                        :</td>
                                                                    <td>
                                                                        <asp:Label ID="LabelAuxiliaryNew" runat="server"></asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" nowrap="nowrap">
                                                                        Country Code</td>
                                                                    <td width="1">
                                                                        :</td>
                                                                    <td>
                                                                        <asp:Label ID="LabelCountryCodeNew" runat="server"></asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" nowrap="nowrap">
                                                                        1 CIF &amp; Related Source Found</td>
                                                                    <td width="1">
                                                                        :</td>
                                                                    <td>
                                                                        <asp:Label ID="LabelCIFRelatedNew" runat="server"></asp:Label></td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </asp:View>
                    </asp:MultiView>
                </ajax:AjaxPanel>
            </td>
        </tr>
        
    </table>
    <table width="100%">
           <TR class="formText" id="Button11" bgColor="#dddddd" height="30" >
		    <TD style="width: 22px"><IMG height="15" src="images/arrow.gif" width="15"></TD>
		    <TD colSpan="7">
			    <TABLE cellSpacing="0" cellPadding="3" border="0">
				    <TR>
					    <TD><asp:imagebutton id="ImageAccept" runat="server" CausesValidation="True" SkinID="AcceptButton"></asp:imagebutton></TD>
					    <TD><asp:imagebutton id="ImageReject" runat="server" CausesValidation="False" SkinID="RejectButton"></asp:imagebutton></TD>
					    <td style="width: 35px"><asp:imagebutton id="ImageBack" runat="server" CausesValidation="False" SkinID="BackButton"></asp:imagebutton></td>
				    </TR>
			    </TABLE>
                <asp:CustomValidator ID="cvalPageErr" runat="server" Display="none"></asp:CustomValidator></TD>
	    </TR>
	    </table> 
</asp:Content>
