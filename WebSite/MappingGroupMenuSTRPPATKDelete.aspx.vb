Option Strict On
Option Explicit On
Imports amlbll
Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports SahassaNettier.Services
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper


Imports System.IO
Imports System.Collections.Generic

Partial Class MappingGroupMenuSTRPPATKDelete
    Inherits Parent

    Public ReadOnly Property PK_MappingGroupMenuSTRPPATK_ID() As Integer
        Get
            Dim strTemp As String = Request.Params("PK_MappingGroupMenuSTRPPATK_ID")
            Dim intResult As Integer
            If Integer.TryParse(strTemp, intResult) Then
                Return intResult
            Else
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = "Parameter PK_MappingGroupMenuSTRPPATK_ID is invalid."

            End If
        End Get
    End Property

    Public ReadOnly Property Objvw_MappingGroupMenuSTRPPATK() As VList(Of vw_MappingGroupMenuSTRPPATK)
        Get
            If Session("Objvw_MappingGroupMenuSTRPPATK.MappingGroupMenuSTRPPATKDelete") Is Nothing Then

                Session("Objvw_MappingGroupMenuSTRPPATK.MappingGroupMenuSTRPPATKDelete") = DataRepository.vw_MappingGroupMenuSTRPPATKProvider.GetPaged("PK_MappingGroupMenuSTRPPATK_ID = '" & PK_MappingGroupMenuSTRPPATK_ID & "'", "", 0, Integer.MaxValue, 0)

            End If
            Return CType(Session("Objvw_MappingGroupMenuSTRPPATK.MappingGroupMenuSTRPPATKDelete"), VList(Of vw_MappingGroupMenuSTRPPATK))
        End Get
    End Property


    Private Sub loaddata()

        If Objvw_MappingGroupMenuSTRPPATK.Count > 0 Then
            TxtDelete.Text = Objvw_MappingGroupMenuSTRPPATK(0).GroupName

        Else
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = MappingGroupMenuSTRPPATKBLL.DATA_NOTVALID
        End If
    End Sub

    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim oTrans As SqlTransaction = Nothing
        Try

            Using ObjList As MappingGroupMenuSTRPPATK = DataRepository.MappingGroupMenuSTRPPATKProvider.GetByPK_MappingGroupMenuSTRPPATK_ID(PK_MappingGroupMenuSTRPPATK_ID)
                If ObjList Is Nothing Then
                    Throw New AMLBLL.SahassaException(MappingGroupMenuSTRPPATKEnum.DATA_NOTVALID)
                End If
            End Using

            Dim groupID As Integer = 0
            Dim groupname As String = ""

            Using Objlist As TList(Of MappingGroupMenuSTRPPATK) = DataRepository.MappingGroupMenuSTRPPATKProvider.GetPaged("PK_MappingGroupMenuSTRPPATK_ID = '" & PK_MappingGroupMenuSTRPPATK_ID & "'", "", 0, Integer.MaxValue, 0)
                If Objlist.Count > 0 Then

                    AMLBLL.MappingGroupMenuSTRPPATKBLL.IsDataValidDeleteApproval(CStr(PK_MappingGroupMenuSTRPPATK_ID))

                    If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                        Using Objvw_MappingGroupMenuSTRPPATK As VList(Of vw_MappingGroupMenuSTRPPATK) = DataRepository.vw_MappingGroupMenuSTRPPATKProvider.GetPaged("PK_MappingGroupMenuSTRPPATK_ID = '" & PK_MappingGroupMenuSTRPPATK_ID & "'", "", 0, Integer.MaxValue, 0)
                            Me.LblSuccess.Text = "Data deleted successfully"
                            Me.LblSuccess.Visible = True
                            Using ObjMappingGroupMenuSTRPPATK As TList(Of MappingGroupMenuSTRPPATK) = DataRepository.MappingGroupMenuSTRPPATKProvider.GetPaged("PK_MappingGroupMenuSTRPPATK_ID = '" & PK_MappingGroupMenuSTRPPATK_ID & "'", "", 0, Integer.MaxValue, 0)
                                groupID = CInt(ObjMappingGroupMenuSTRPPATK(0).FK_Group_ID)
                                AMLBLL.MappingGroupMenuSTRPPATKBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "MappingGroupMenuSTRPPATK", "GroupName", "Delete", "", Objvw_MappingGroupMenuSTRPPATK(0).GroupName, "Acc")
                                DataRepository.MappingGroupMenuSTRPPATKProvider.Delete(ObjMappingGroupMenuSTRPPATK)
                            End Using
                        End Using
                    Else

                        Using ObjMappingGroupMenuSTRPPATK_Approval As New MappingGroupMenuSTRPPATK_Approval
                            Using ObjMappingGroupMenuSTRPPATKList As VList(Of vw_MappingGroupMenuSTRPPATK) = DataRepository.vw_MappingGroupMenuSTRPPATKProvider.GetPaged("PK_MappingGroupMenuSTRPPATK_ID = '" & PK_MappingGroupMenuSTRPPATK_ID & "'", "", 0, Integer.MaxValue, 0)
                                If ObjMappingGroupMenuSTRPPATKList.Count > 0 Then

                                    groupname = ObjMappingGroupMenuSTRPPATKList(0).GroupName
                                    groupID = CInt(ObjMappingGroupMenuSTRPPATKList(0).FK_Group_ID)
                                    ObjMappingGroupMenuSTRPPATK_Approval.FK_MsUserID = Sahassa.AML.Commonly.SessionPkUserId
                                    ObjMappingGroupMenuSTRPPATK_Approval.GroupMenu = groupname
                                    ObjMappingGroupMenuSTRPPATK_Approval.FK_ModeID = CInt(Sahassa.AML.Commonly.TypeMode.Delete)
                                    ObjMappingGroupMenuSTRPPATK_Approval.CreatedDate = Now

                                    DataRepository.MappingGroupMenuSTRPPATK_ApprovalProvider.Save(ObjMappingGroupMenuSTRPPATK_Approval)

                                    Using ObjMappingGroupMenuSTRPPATK_approvalDetail As New MappingGroupMenuSTRPPATK_ApprovalDetail
                                        ObjMappingGroupMenuSTRPPATK_approvalDetail.FK_MappingGroupMenuSTRPPATK_Approval_ID = ObjMappingGroupMenuSTRPPATK_Approval.PK_MappingGroupMenuSTRPPATK_Approval_ID
                                        ObjMappingGroupMenuSTRPPATK_approvalDetail.FK_Group_ID = groupID
                                        ObjMappingGroupMenuSTRPPATK_approvalDetail.PK_MappingGroupMenuSTRPPATK_ID = PK_MappingGroupMenuSTRPPATK_ID
                                        DataRepository.MappingGroupMenuSTRPPATK_ApprovalDetailProvider.Save(ObjMappingGroupMenuSTRPPATK_approvalDetail)
                                    End Using

                                    'Sahassa.AML.Commonly.SessionIntendedPage = "MappingGroupMenuSTRPPATKView.aspx"
                                    'Me.Response.Redirect("MappingGroupMenuSTRPPATKView.aspx", False)
                                    Using ObjGroup As TList(Of Group) = DataRepository.GroupProvider.GetPaged("GroupID = '" & groupID & "'", "", 0, Integer.MaxValue, 0)
                                        AMLBLL.MappingGroupMenuSTRPPATKBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "MappingGroupMenuSTRPPATK", "GroupName", "Delete", "", ObjGroup(0).GroupName, "PendingApproval")
                                        Me.LblSuccess.Text = "Data deleted successfully and waiting for Approval."
                                        Me.LblSuccess.Visible = True
                                    End Using
                                End If
                            End Using
                        End Using
                    End If
                    MultiView1.ActiveViewIndex = 1
                Else
                    Me.LblSuccess.Text = "Data already deleted"
                    Me.LblSuccess.Visible = True
                End If
            End Using

        Catch ex As Exception
            If Not oTrans Is Nothing Then oTrans.Rollback()
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not oTrans Is Nothing Then
                oTrans.Dispose()
                oTrans = Nothing
            End If
        End Try
    End Sub



    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "MappingGroupMenuSTRPPATKView.aspx"
            Me.Response.Redirect("MappingGroupMenuSTRPPATKView.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'Using Transcope As New Transactions.TransactionScope
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                '    Transcope.Complete()
            End Using
            Session("Objvw_MappingGroupMenuSTRPPATK.MappingGroupMenuSTRPPATKDelete") = Nothing
            If Not Page.IsPostBack Then
                loaddata()
                MultiView1.ActiveViewIndex = 0
            End If
            'End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub btnOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnOK.Click
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "MappingGroupMenuSTRPPATKView.aspx"
            Me.Response.Redirect("MappingGroupMenuSTRPPATKView.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class
