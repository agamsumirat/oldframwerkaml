Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Partial Class UploadUSRCustodyApprovalDetail
    Inherits Parent

    Public ReadOnly Property PkUSRCustodyApproval() As Long
        Get
            Dim strtemppk As String = Request.Params("PK_AccountUSRCustody_ApprovalID")
            Dim lngRetval As Long
            If Long.TryParse(strtemppk, lngRetval) Then
                Return lngRetval
            Else
                Throw New Exception("PK_AccountUSRCustody_ApprovalID not valid")
            End If
        End Get
    End Property




    Sub ClearSession()

    End Sub

    Sub BindGrid()
        GridView1.DataSource = AMLBLL.USRAccountCustodyBLL.GetUSRCustodyApprovalDetailByPk(Me.PkUSRCustodyApproval)
        GridView1.PageSize = Sahassa.AML.Commonly.SessionPagingLimit
        GridView1.DataBind()
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                ClearSession()
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using

                LabelTitle.Text = "Upload URSCustody Pending Approval Detail"
                LabelActivity.Text = "Edit"
                BindGrid()
            End If

        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        Try
            GridView1.PageIndex = e.NewPageIndex
            BindGrid()

        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImageAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click
        Try

            AMLBLL.USRAccountCustodyBLL.AcceptUSRCustodyAccount(Me.PkUSRCustodyApproval)
            Page.ClientScript.RegisterStartupScript(Me.GetType, Guid.NewGuid.ToString, "  window.alert('URS Custody Data has been accepted.');window.location='UploadURSCustody_Approval.aspx'", True)
            'Response.Redirect("UploadURSCustody_Approval.aspx", False)
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImageReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click
        Try
            AMLBLL.USRAccountCustodyBLL.RejectUSRCustodyAccount(Me.PkUSRCustodyApproval)
            Page.ClientScript.RegisterStartupScript(Me.GetType, Guid.NewGuid.ToString, "  window.alert('URS Custody Data has been rejected.');window.location='UploadURSCustody_Approval.aspx'", True)
            'Response.Redirect("UploadURSCustody_Approval.aspx", False)
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImageBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click
        Try

            Response.Redirect("UploadURSCustody_Approval.aspx", False)
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub
End Class
