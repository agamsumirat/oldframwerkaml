Option Strict On
Option Explicit On
Imports amlbll
Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports SahassaNettier.Services
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper


Imports System.IO
Imports System.Collections.Generic

Partial Class MappingUserIdHiportJiveApprovalDetail
    Inherits Parent

    Public ReadOnly Property PK_MappingUserIdHIPORTJIVE_ApprovalDetail_ID() As Integer
        Get
            Dim strTemp As String = Request.Params("PK_MappingUserIdHIPORTJIVE_ApprovalDetail_ID")
            Dim intResult As Integer
            If Integer.TryParse(strTemp, intResult) Then
                Return intResult
            Else
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = "Parameter PK_MappingUserIdHIPORTJIVE_ApprovalDetail_ID is invalid."

            End If
        End Get
    End Property

    Public ReadOnly Property ObjMappingUserIdHIPORTJIVE_ApprovalDetail() As TList(Of MappingUserIdHiportJive_ApprovalDetail)
        Get
            If Session("MappingUserIdHIPORTJIVEApprovalDetail.ObjMappingUserIdHIPORTJIVE_ApprovalDetail") Is Nothing Then

                Session("MappingUserIdHIPORTJIVEApprovalDetail.ObjMappingUserIdHIPORTJIVE_ApprovalDetail") = DataRepository.MappingUserIdHIPORTJIVE_ApprovalDetailProvider.GetPaged("PK_MappingUserIdHIPORTJIVE_ApprovalDetail_ID = '" & PK_MappingUserIdHIPORTJIVE_ApprovalDetail_ID & "'", "", 0, Integer.MaxValue, 0)

            End If
            Return CType(Session("MappingUserIdHIPORTJIVEApprovalDetail.ObjMappingUserIdHIPORTJIVE_ApprovalDetail"), TList(Of MappingUserIdHIPORTJIVE_ApprovalDetail))
        End Get
    End Property


    Sub LoadDataAdd()
        Try
            PanelOld.Visible = False
            PanelNew.Visible = True

            LblAction.Text = "Add"

            If ObjMappingUserIdHIPORTJIVE_ApprovalDetail.Count > 0 Then
                Using objUserNew As User = DataRepository.UserProvider.GetBypkUserID(CInt(ObjMappingUserIdHIPORTJIVE_ApprovalDetail(0).FK_User_ID))
                    LblFK_User_ID_New.Text = objUserNew.UserID & " - " & objUserNew.UserName
                    KodeAO_New.Text = ObjMappingUserIdHIPORTJIVE_ApprovalDetail(0).Kode_AO
                End Using
            Else
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = MappingUserIdHIPORTJIVEBLL.DATA_NOTVALID
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub loaddataEdit()
        Try
            PanelOld.Visible = True
            PanelNew.Visible = True

            LblAction.Text = "Edit"

            If ObjMappingUserIdHIPORTJIVE_ApprovalDetail.Count > 0 Then
                Using objUserOld As User = DataRepository.UserProvider.GetBypkUserID(CInt(ObjMappingUserIdHIPORTJIVE_ApprovalDetail(0).FK_User_ID_Old))
                    LblFK_User_ID_Old.Text = objUserOld.UserID & " - " & objUserOld.UserName
                    KodeAO_OLD.Text = ObjMappingUserIdHIPORTJIVE_ApprovalDetail(0).Kode_AO_OLD
                End Using

                Using objUserNew As User = DataRepository.UserProvider.GetBypkUserID(CInt(ObjMappingUserIdHIPORTJIVE_ApprovalDetail(0).FK_User_ID))
                    LblFK_User_ID_New.Text = objUserNew.UserID & " - " & objUserNew.UserName
                    KodeAO_New.Text = ObjMappingUserIdHIPORTJIVE_ApprovalDetail(0).Kode_AO
                End Using

            Else
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = MappingUserIdHIPORTJIVEBLL.DATA_NOTVALID
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try

    End Sub

    Sub LoadDataDelete()
        Try
            PanelOld.Visible = False
            PanelNew.Visible = True

            LblAction.Text = "Delete"

            If ObjMappingUserIdHIPORTJIVE_ApprovalDetail.Count > 0 Then
                Using objUserNew As User = DataRepository.UserProvider.GetBypkUserID(CInt(ObjMappingUserIdHIPORTJIVE_ApprovalDetail(0).FK_User_ID))
                    LblFK_User_ID_New.Text = objUserNew.UserID & " - " & objUserNew.UserName
                    KodeAO_New.Text = ObjMappingUserIdHIPORTJIVE_ApprovalDetail(0).Kode_AO
                End Using
            Else
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = MappingUserIdHIPORTJIVEBLL.DATA_NOTVALID
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Sub LoadData()
        Try
            If ObjMappingUserIdHIPORTJIVE_ApprovalDetail.Count > 0 Then
                Dim ListModeID As Integer = 0
                Using objMappingUserIdHIPORTJIVE_Approval As MappingUserIdHIPORTJIVE_Approval = DataRepository.MappingUserIdHIPORTJIVE_ApprovalProvider.GetByPK_MappingUserIdHIPORTJIVE_Approval_ID(CInt(ObjMappingUserIdHIPORTJIVE_ApprovalDetail(0).FK_MappingUserIdHIPORTJIVE_Approval_ID))
                    ListModeID = CInt(objMappingUserIdHIPORTJIVE_Approval.FK_ModeID)
                    If ListModeID = 1 Then
                        LoadDataAdd()
                    ElseIf ListModeID = 2 Then
                        loaddataEdit()
                    ElseIf ListModeID = 3 Then
                        LoadDataDelete()
                    End If
                End Using

            Else
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = MappingUserIdHIPORTJIVEBLL.DATA_NOTVALID
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "MappingUserIdHIPORTJIVEApproval.aspx"
            Me.Response.Redirect("MappingUserIdHIPORTJIVEApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not IsPostBack Then
                Session("MappingUserIdHIPORTJIVEApprovalDetail.ObjMappingUserIdHIPORTJIVE_ApprovalDetail") = Nothing
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                    LabelTitle.Text = "MApping User ID Hiport Jive Approval Detail"
                    LoadData()



                End Using
            End If

        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub



    Protected Sub ImageAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click
        Try

            Using ObjMappingUserIdHiportJive_ApprovalDetail As MappingUserIdHiportJive_ApprovalDetail = DataRepository.MappingUserIdHiportJive_ApprovalDetailProvider.GetByPK_MappingUserIdHiportJive_ApprovalDetail_ID(PK_MappingUserIdHiportJive_ApprovalDetail_ID)
                If ObjMappingUserIdHIPORTJIVE_ApprovalDetail Is Nothing Then
                    Throw New AMLBLL.SahassaException(MappingUserIdHiportJiveEnum.DATA_NOTVALID)
                End If
            End Using

            If LblAction.Text = "Add" Then
                AMLBLL.MappingUserIdHIPORTJIVEBLL.AcceptAddHIPORTJIVE(PK_MappingUserIdHIPORTJIVE_ApprovalDetail_ID)
            ElseIf LblAction.Text = "Edit" Then
                Using ObjList As MappingUserIdHiportJive = DataRepository.MappingUserIdHiportJiveProvider.GetByPK_MappingUserIdHiportJive_ID(ObjMappingUserIdHIPORTJIVE_ApprovalDetail(0).PK_MappingUserIdHiportJive_ID)
                    If ObjList Is Nothing Then
                        Throw New AMLBLL.SahassaException(MappingUserIdHiportJiveEnum.DATA_NOTVALID)
                    End If
                End Using
                AMLBLL.MappingUserIdHiportJiveBLL.AcceptEditHIPORTJIVE(PK_MappingUserIdHIPORTJIVE_ApprovalDetail_ID)
            ElseIf LblAction.Text = "Delete" Then
                Using ObjList As MappingUserIdHiportJive = DataRepository.MappingUserIdHiportJiveProvider.GetByPK_MappingUserIdHiportJive_ID(ObjMappingUserIdHIPORTJIVE_ApprovalDetail(0).PK_MappingUserIdHiportJive_ID)
                    If ObjList Is Nothing Then
                        Throw New AMLBLL.SahassaException(MappingUserIdHiportJiveEnum.DATA_NOTVALID)
                    End If
                End Using
                AMLBLL.MappingUserIdHiportJiveBLL.AcceptDeleteHIPORTJIVE(PK_MappingUserIdHIPORTJIVE_ApprovalDetail_ID)
            End If

            Sahassa.AML.Commonly.SessionIntendedPage = "MappingUserIdHIPORTJIVEApproval.aspx"

            Me.Response.Redirect("MappingUserIdHIPORTJIVEApproval.aspx", False)

        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImageReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click
        Try

            Using ObjMappingUserIdHiportJive_ApprovalDetail As MappingUserIdHiportJive_ApprovalDetail = DataRepository.MappingUserIdHiportJive_ApprovalDetailProvider.GetByPK_MappingUserIdHiportJive_ApprovalDetail_ID(PK_MappingUserIdHIPORTJIVE_ApprovalDetail_ID)
                If ObjMappingUserIdHiportJive_ApprovalDetail Is Nothing Then
                    Throw New AMLBLL.SahassaException(MappingUserIdHiportJiveEnum.DATA_NOTVALID)
                End If
            End Using

            If LblAction.Text = "Add" Then
                AMLBLL.MappingUserIdHiportJiveBLL.RejectAddHIPORTJIVE(PK_MappingUserIdHIPORTJIVE_ApprovalDetail_ID)
            ElseIf LblAction.Text = "Edit" Then
                AMLBLL.MappingUserIdHiportJiveBLL.RejectEditHIPORTJIVE(PK_MappingUserIdHIPORTJIVE_ApprovalDetail_ID)
            ElseIf LblAction.Text = "Delete" Then
                AMLBLL.MappingUserIdHiportJiveBLL.RejectDeleteHIPORTJIVE(PK_MappingUserIdHIPORTJIVE_ApprovalDetail_ID)
            End If

            Sahassa.AML.Commonly.SessionIntendedPage = "MappingUserIdHIPORTJIVEApproval.aspx"

            Me.Response.Redirect("MappingUserIdHIPORTJIVEApproval.aspx", False)


        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
End Class


