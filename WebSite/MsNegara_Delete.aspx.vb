#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports Sahassa.AML
#End Region

Partial Class MsNegara_Delete
    Inherits Parent

#Region "Function"

    Sub ChangeMultiView(ByVal index As Integer)
        MtvMsUser.ActiveViewIndex = index
    End Sub

    Private Sub BindListMapping()
        LBMapping.DataSource = Listmaping
        LBMapping.DataTextField = "Nama"
        LBMapping.DataValueField = "IDNegaraNCBS"
        LBMapping.DataBind()
    End Sub


#End Region

#Region "events..."

    Public ReadOnly Property parID() As String
        Get
            Return Request.Item("ID")
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                'AuditTrailBLL.InsertAuditTrailUserAccess(SessionCurrentPage)
                Listmaping = New TList(Of MappingMsNegaraNCBSPPATK)
                LoadData()
            Catch ex As Exception
                LogError(ex)
                CvalPageErr.IsValid = False
                CvalPageErr.ErrorMessage = ex.Message
            End Try
        End If

    End Sub

    Private Sub LoadData()
        Dim ObjMsNegara As MsNegara = DataRepository.MsNegaraProvider.GetPaged(MsNegaraColumn.IDNegara.ToString & "=" & parID, "", 0, 1, Nothing)(0)
        If ObjMsNegara Is Nothing Then Throw New Exception("Data Not Found")
        With ObjMsNegara
            SafeDefaultValue = "-"
            lblIDNegara.Text = .IDNegara
            lblNamaNegara.Text = Safe(.NamaNegara)

            'other info
            Dim Omsuser As TList(Of User)
            Omsuser = DataRepository.UserProvider.GetByUserID(.CreatedBy)
            If Omsuser.Count > 0 Then
                lblCreatedBy.Text = Omsuser(0).UserName
            End If
            lblCreatedDate.Text = FormatDate(.CreatedDate)
            Omsuser = DataRepository.UserProvider.GetByUserID(.LastUpdatedBy)
            If Omsuser.Count > 0 Then
                lblUpdatedby.Text = Omsuser(0).UserName
            End If
            lblUpdatedDate.Text = FormatDate(.LastUpdatedDate)
            lblActivation.Text = SafeActiveInactive(.Activation)
            Dim L_objMappingMsNegaraNCBSPPATK As TList(Of MappingMsNegaraNCBSPPATK)
            L_objMappingMsNegaraNCBSPPATK = DataRepository.MappingMsNegaraNCBSPPATKProvider.GetPaged(MappingMsNegaraNCBSPPATKColumn.IDNegara.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)

            Listmaping.AddRange(L_objMappingMsNegaraNCBSPPATK)
        End With
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            BindListMapping()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

#End Region

#Region "Property..."

    ''' <summary>
    ''' Menyimpan Item untuk mapping sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Listmaping() As TList(Of MappingMsNegaraNCBSPPATK)
        Get
            Return Session("Listmaping.data")
        End Get
        Set(ByVal value As TList(Of MappingMsNegaraNCBSPPATK))
            Session("Listmaping.data") = value
        End Set
    End Property

#End Region

#Region "events..."
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Response.Redirect("MsNegara_View.aspx")
    End Sub

    Protected Sub imgOk_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgOk.Click
        Try
            Page.Validate("handle")
            If Page.IsValid Then

                ' =========== Insert Header Approval
                Dim KeyHeaderApproval As Integer
                Using ObjMsNegara_Approval As New MsNegara_Approval
                    With ObjMsNegara_Approval
                        .FK_MsMode_Id = 3
                        FillOrNothing(.RequestedBy, SessionPkUserId)
                        FillOrNothing(.RequestedDate, Date.Now)
                        .IsUpload = False
                    End With
                    DataRepository.MsNegara_ApprovalProvider.Save(ObjMsNegara_Approval)
                    KeyHeaderApproval = ObjMsNegara_Approval.PK_MsNegara_Approval_Id
                End Using
                '============ Insert Detail Approval
                Using objMsNegara_ApprovalDetail As New MsNegara_ApprovalDetail()
                    With objMsNegara_ApprovalDetail
                        Dim ObjMsNegara As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(parID)
                        FillOrNothing(.IDNegara, lblIDNegara.Text, True, oInt)
                        FillOrNothing(.NamaNegara, lblNamaNegara.Text, True, Ovarchar)

                        FillOrNothing(.IDNegara, ObjMsNegara.IDNegara)
                        FillOrNothing(.Activation, ObjMsNegara.Activation)
                        FillOrNothing(.CreatedDate, ObjMsNegara.CreatedDate)
                        FillOrNothing(.CreatedBy, ObjMsNegara.CreatedBy)
                        FillOrNothing(.FK_MsNegara_Approval_Id, KeyHeaderApproval)
                    End With
                    DataRepository.MsNegara_ApprovalDetailProvider.Save(objMsNegara_ApprovalDetail)
                    Dim keyHeaderApprovalDetail As Integer = objMsNegara_ApprovalDetail.PK_MsNegara_ApprovalDetail_Id

                    '========= Insert mapping item 
                    Dim LobjMappingMsNegaraNCBSPPATK_Approval_Detail As New TList(Of MappingMsNegaraNCBSPPATK_Approval_Detail)

                    Dim L_ObjMappingMsNegaraNCBSPPATK As TList(Of MappingMsNegaraNCBSPPATK) = DataRepository.MappingMsNegaraNCBSPPATKProvider.GetPaged(MappingMsNegaraNCBSPPATKColumn.IDNegara.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)

                    For Each objMappingMsNegaraNCBSPPATK As MappingMsNegaraNCBSPPATK In L_ObjMappingMsNegaraNCBSPPATK
                        Dim objMappingMsNegaraNCBSPPATK_Approval_Detail As New MappingMsNegaraNCBSPPATK_Approval_Detail
                        With objMappingMsNegaraNCBSPPATK_Approval_Detail
                            FillOrNothing(.IDNegara, objMappingMsNegaraNCBSPPATK.PK_MappingMsNegaraNCBSPPATK_Id)
                            FillOrNothing(.IDNegaraNCBS, objMappingMsNegaraNCBSPPATK.IDNegaraNCBS)
                            FillOrNothing(.PK_MsNegara_Approval_Id, KeyHeaderApproval)
                            FillOrNothing(.PK_MappingMsNegaraNCBSPPATK_Id, objMappingMsNegaraNCBSPPATK.PK_MappingMsNegaraNCBSPPATK_Id)
                            .Nama = objMappingMsNegaraNCBSPPATK.Nama
                            LobjMappingMsNegaraNCBSPPATK_Approval_Detail.Add(objMappingMsNegaraNCBSPPATK_Approval_Detail)
                        End With
                        DataRepository.MappingMsNegaraNCBSPPATK_Approval_DetailProvider.Save(LobjMappingMsNegaraNCBSPPATK_Approval_Detail)
                    Next
                    'session Maping item Clear
                    Listmaping.Clear()
                    Me.imgOk.Visible = False
                    Me.ImageCancel.Visible = False
                    LblConfirmation.Text = "Data has been Delete and waiting for approval"
                    MtvMsUser.ActiveViewIndex = 1
                End Using
            End If
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImgBtnAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAdd.Click
        Response.Redirect("MsNegara_View.aspx")
    End Sub

#End Region

End Class



