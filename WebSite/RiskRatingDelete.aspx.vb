Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Partial Class RiskRatingDelete
    Inherits System.Web.UI.Page

    ''' <summary>
    ''' get user id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetRiskRatingId() As String
        Get
            Return Me.Request.Params("RiskRatingID")
        End Get
    End Property

    ''' <summary>
    ''' cancel handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Me.Response.Redirect("RiskRatingView.aspx", False)
    End Sub

    Private Sub InsertAuditTrail()
        Try
            Dim RiskRatingID_Old As Int32 = ViewState("RiskRatingID_Old")
            Dim RiskRatingName_Old As String = ViewState("RiskRatingName_Old")
            Dim RiskRatingDescription_Old As String = ViewState("RiskRatingDescription_Old")
            Dim RiskScoreFrom_Old As Int16 = ViewState("RiskScoreFrom_Old")
            Dim RiskScoreTo_Old As Int16 = ViewState("RiskScoreTo_Old")
            Dim RiskRatingCreatedDate_Old As DateTime = ViewState("RiskRatingCreatedDate_Old")

            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(6)
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "RiskRatingID", "Delete", RiskRatingID_Old, RiskRatingID_Old, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "CreatedDate", "Delete", CDate(RiskRatingCreatedDate_Old).ToString("dd-MMMM-yyyy HH:mm"), CDate(RiskRatingCreatedDate_Old).ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "RiskRatingName", "Delete", "", RiskRatingName_Old, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "Description", "Delete", "", RiskRatingDescription_Old, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "RiskScoreFrom", "Delete", "", RiskScoreFrom_Old, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "RiskRating", "RiskScoreTo", "Delete", "", RiskScoreTo_Old, "Accepted")
            End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub DeleteRiskRatingBySU()
        Try
            Using AccessRiskRating As New AMLDAL.AMLDataSetTableAdapters.RiskRatingTableAdapter
                AccessRiskRating.DeleteRiskRating(CInt(Me.GetRiskRatingId))
            End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    ''' <summary>
    ''' update handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            Dim RiskScoreFrom As Int16 = Me.TextRiskScoreFrom.Text
            Dim RiskScoreTo As Int16 = Me.TextRiskScoreTo.Text

            'Lakukan pencegahan bila nilai Risk Score From lebih besar/sama dengan nilai Risk Score To
            If RiskScoreFrom >= RiskScoreTo Then
                Me.cvalPageError.IsValid = False
                Me.cvalPageError.ErrorMessage = "Risk Score value must be from a smaller number to a larger number"
            Else
                Dim RiskRatingID As Int32 = Me.LabelRiskRatingID.Text
                Dim RiskRatingName As String = Me.TextRiskRatingName.Text
                Dim RiskRatingDescription As String = Me.TextRiskRatingDescription.Text

                'Buat variabel untuk menampung nilai-nilai lama
                Dim RiskRatingID_Old As Int32 = ViewState("RiskRatingID_Old")
                Dim RiskRatingName_Old As String = ViewState("RiskRatingName_Old")
                Dim RiskRatingDescription_Old As String = ViewState("RiskRatingDescription_Old")
                Dim RiskScoreFrom_Old As Int16 = ViewState("RiskScoreFrom_Old")
                Dim RiskScoreTo_Old As Int16 = ViewState("RiskScoreTo_Old")
                Dim RiskRatingCreatedDate_Old As DateTime = ViewState("RiskRatingCreatedDate_Old")

                Dim RiskRatingPendingApprovalID As Int64
                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                    Me.InsertAuditTrail()
                    Me.DeleteRiskRatingBySU()
                    Me.LblSucces.Visible = True
                    Me.LblSucces.Text = "Success to Delete Risk Rating."
                Else
                    'Using TransScope As New Transactions.TransactionScope
                    Dim rows_affected As Integer

                    Using AccessRiskRatingPendingApproval As New AMLDAL.AMLDataSetTableAdapters.RiskRating_PendingApprovalTableAdapter
                        oSQLTrans = BeginTransaction(AccessRiskRatingPendingApproval, IsolationLevel.ReadUncommitted)
                        'Tambahkan ke dalam tabel RiskRating_PendingApproval dengan ModeID = 3 (Delete)
                        RiskRatingPendingApprovalID = AccessRiskRatingPendingApproval.InsertRiskRating_PendingApproval(Sahassa.AML.Commonly.SessionUserId, Now, "Risk Rating Delete", 3)
                        rows_affected += 1
                    End Using

                    Using AccessRiskRatingApproval As New AMLDAL.AMLDataSetTableAdapters.RiskRating_ApprovalTableAdapter
                        SetTransaction(AccessRiskRatingApproval, oSQLTrans)
                        'Tambahkan ke dalam tabel RiskRating_Approval dengan ModeID = 3 (Delete) 
                        rows_affected += AccessRiskRatingApproval.Insert(RiskRatingPendingApprovalID, 3, RiskRatingID, RiskRatingName, RiskRatingDescription, RiskScoreFrom, RiskScoreTo, Now, RiskRatingID_Old, RiskRatingName_Old, RiskRatingDescription_Old, RiskScoreFrom_Old, RiskScoreTo_Old, RiskRatingCreatedDate_Old)
                    End Using

                    'Bila kedua transaksi di atas berhasil maka commit transaksi2 tsb
                    If rows_affected = 2 Then
                        oSQLTrans.Commit()
                        'TransScope.Complete()
                    End If
                    Me.Response.Redirect("RiskRatingView.aspx", False)
                    'End Using
                End If
            End If
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub

    ''' <summary>
    ''' filledit data
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillEditData()
        Using AccessRiskRating As New AMLDAL.AMLDataSetTableAdapters.RiskRatingTableAdapter
            Using TableRiskRating As AMLDAL.AMLDataSet.RiskRatingDataTable = AccessRiskRating.GetDataByRiskRatingID(Me.GetRiskRatingId)
                If TableRiskRating.Rows.Count > 0 Then
                    Dim TableRowRiskRating As AMLDAL.AMLDataSet.RiskRatingRow = TableRiskRating.Rows(0)

                    ViewState("RiskRatingID_Old") = TableRowRiskRating.RiskRatingID
                    Me.LabelRiskRatingID.Text = ViewState("RiskRatingID_Old")

                    ViewState("RiskRatingName_Old") = TableRowRiskRating.RiskRatingName
                    Me.TextRiskRatingName.Text = ViewState("RiskRatingName_Old")

                    ViewState("RiskRatingDescription_Old") = TableRowRiskRating.Description
                    Me.TextRiskRatingDescription.Text = ViewState("RiskRatingDescription_Old")

                    ViewState("RiskScoreFrom_Old") = TableRowRiskRating.RiskScoreFrom
                    Me.TextRiskScoreFrom.Text = ViewState("RiskScoreFrom_Old")

                    ViewState("RiskScoreTo_Old") = TableRowRiskRating.RiskScoreTo
                    Me.TextRiskScoreTo.Text = ViewState("RiskScoreTo_Old")

                    ViewState("RiskRatingCreatedDate_Old") = TableRowRiskRating.CreatedDate
                End If
            End Using
        End Using
    End Sub

    ''' <summary>
    ''' page load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.FillEditData()
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
End Class