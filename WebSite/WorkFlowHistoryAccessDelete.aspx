﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="WorkFlowHistoryAccessDelete.aspx.vb" Inherits="WorkFlowHistoryAccessDelete" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
	<table id="title"  border="2" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr>
            <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Workflow History Access- Delete&nbsp;
                    <hr />
                </strong>
                <asp:Label ID="LblSuccess" runat="server" CssClass="validationok" Visible="False"
                    Width="100%"></asp:Label></td>
        </tr>
       <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; border-top-style: none; border-right-style: none;
                border-left-style: none; height: 24px; border-bottom-style: none">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/validationsign_animate.gif" /></td>
            <td bgcolor="#ffffff" colspan="3" style="border-top-style: none; border-right-style: none;
                border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>
                The following Workflow History Access will be deleted :</strong></td>
        </tr>
    </table>	
	<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2" height="72">          
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
            <td bgcolor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;" width="20%">
                Workflow History Access ID</td>
            <td bgcolor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;" width="5">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;" width="80%">
                <asp:Label ID="LblHistoryID" runat="server"></asp:Label></td>
        </tr>
		<tr class="formText">
			<td bgColor="#ffffff" style="width: 24px; height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">&nbsp;</td>
			<td width="20%" bgColor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
                Worflow Step</td>
			<td width="5" bgColor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">:</td>
			<td width="80%" bgColor="#ffffff" style="height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
                <asp:Label ID="LblWorkflowStep" runat="server"></asp:Label></td>
		</tr>
		<tr class="formText">
			<td bgColor="#ffffff" 
                style="width: 24px; height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">&nbsp;</td>
			<td bgColor="#ffffff" 
                style="height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
                Access View Other<br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" 
                style="height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
                :<br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" 
                style="height: 24px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
                <asp:Label ID="LblAccessViewOther" runat="server"></asp:Label></td>
		</tr>
		<tr class="formText" bgColor="#dddddd" height="30">
			<td style="width: 24px"><IMG height="15" src="images/arrow.gif" width="15"></td>
			<td colSpan="3" style="height: 9px">
				<table cellSpacing="0" cellPadding="3" border="0">
					<tr>
						<td><asp:imagebutton id="ImageUpdate" runat="server" CausesValidation="True" SkinID="DeleteButton"></asp:imagebutton></td>
						<td><asp:imagebutton id="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton"></asp:imagebutton></td>
					</tr>
				</table>          
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
		</tr>
	</table>
	
</asp:Content>


