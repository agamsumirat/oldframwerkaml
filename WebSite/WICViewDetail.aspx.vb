#Region "Imports..."
Imports Sahassanettier.Data
Imports Sahassanettier.Entities
Imports System.Data
Imports System.Collections.Generic
Imports AMLBLL
Imports AMLBLL.ValidateBLL
#End Region

Partial Class WICViewDetail
    Inherits Parent

#Region "properties..."

    ReadOnly Property getWICPK() As Integer
        Get
            If Session("WICViewDetail.WICPK") = Nothing Then
                Session("WICViewDetail.WICPK") = CInt(Request.Params("WIC_ID"))
            End If
            Return Session("WICViewDetail.WICPK")
        End Get
    End Property

    Public Property SetnGetResumeKasMasukKasKeluar() As List(Of ResumeKasMasukKeluarWIC)
        Get
            If Session("WICViewDetail.ResumeKasMasukKasKeluar") Is Nothing Then
                Session("WICViewDetail.ResumeKasMasukKasKeluar") = New List(Of ResumeKasMasukKeluarWIC)
            End If
            Return Session("WICViewDetail.ResumeKasMasukKasKeluar")
        End Get
        Set(ByVal value As List(Of ResumeKasMasukKeluarWIC))
            Session("WICViewDetail.ResumeKasMasukKasKeluar") = value
        End Set
    End Property

    Public Property SetnGetgrvTRXKMDetilValutaAsing() As List(Of WICDetailCashInDetail)
        Get
            If Session("WICViewDetail.grvTRXKMDetilValutaAsingDATA") Is Nothing Then
                Session("WICViewDetail.grvTRXKMDetilValutaAsingDATA") = New List(Of WICDetailCashInDetail)
            End If
            Return Session("WICViewDetail.grvTRXKMDetilValutaAsingDATA")
        End Get
        Set(ByVal value As List(Of WICDetailCashInDetail))
            Session("WICViewDetail.grvTRXKMDetilValutaAsing") = value
        End Set
    End Property

    Public Property SetnGetgrvDetilKasKeluar() As List(Of WICDetailCashInDetail)
        Get
            If Session("WICViewDetail.grvDetilKasKeluarDATA") Is Nothing Then
                Session("WICViewDetail.grvDetilKasKeluarDATA") = New List(Of WICDetailCashInDetail)
            End If
            Return Session("WICViewDetail.grvDetilKasKeluarDATA")
        End Get
        Set(ByVal value As List(Of WICDetailCashInDetail))
            Session("WICViewDetail.grvDetilKasKeluarDATA") = value
        End Set
    End Property

    Public Property SetnGetRowEdit() As Integer
        Get
            If Session("WICViewDetail.RowEdit") Is Nothing Then
                Session("WICViewDetail.RowEdit") = -1
            End If
            Return Session("WICViewDetail.RowEdit")
        End Get
        Set(ByVal value As Integer)
            Session("WICViewDetail.RowEdit") = value
        End Set
    End Property


#End Region

#Region "Function..."
    Sub clearKasMasukKasKeluar()
        'bersihin grid
        Session("WICViewDetail.grvTRXKMDetilValutaAsingDATA") = Nothing
        Session("WICViewDetail.grvDetilKasKeluarDATA") = Nothing
        grvDetilKasKeluar.DataSource = SetnGetgrvDetilKasKeluar
        grvDetilKasKeluar.DataBind()
        grvTRXKMDetilValutaAsing.DataSource = SetnGetgrvTRXKMDetilValutaAsing
        grvTRXKMDetilValutaAsing.DataBind()

        'bersihin sisa Kas Masuk
        txtTRXKMTanggalTrx.Text = ""
        txtTRXKMNamaKantor.Text = ""
        txtTRXKMKotaKab.Text = ""
        hfTRXKMKotaKab.Value = ""
        txtTRXKMProvinsi.Text = ""
        hfTRXKMProvinsi.Value = ""
        txtTRXKMDetilKasMasuk.Text = ""
        txtTRXKMDetilMataUang.Text = ""
        hfTRXKMDetilMataUang.Value = ""
        txtTRXKMDetailKursTrx.Text = ""
        txtTRXKMDetilJumlah.Text = ""
        lblTRXKMDetilValutaAsingJumlahRp.Text = ""
        txtTRXKMNoRekening.Text = ""
        txtTRXKMINDVGelar.Text = ""
        txtTRXKMINDVNamaLengkap.Text = ""
        txtTRXKMINDVTempatLahir.Text = ""
        txtTRXKMINDVTanggalLahir.Text = ""
        rblTRXKMINDVKewarganegaraan.SelectedIndex = -1
        cboTRXKMINDVNegara.SelectedIndex = 0
        txtTRXKMINDVDOMNamaJalan.Text = ""
        txtTRXKMINDVDOMRTRW.Text = ""
        txtTRXKMINDVDOMKelurahan.Text = ""
        hfTRXKMINDVDOMKelurahan.Value = ""
        txtTRXKMINDVDOMKecamatan.Text = ""
        hfTRXKMINDVDOMKecamatan.Value = ""
        txtTRXKMINDVDOMKotaKab.Text = ""
        hfTRXKMINDVDOMKotaKab.Value = ""
        txtTRXKMINDVDOMKodePos.Text = ""
        txtTRXKMINDVDOMProvinsi.Text = ""
        hfTRXKMINDVDOMProvinsi.Value = ""
        'chkTRXKMINDVCopyDOM.Checked = False
        txtTRXKMINDVIDNamaJalan.Text = ""
        txtTRXKMINDVIDRTRW.Text = ""
        txtTRXKMINDVIDKelurahan.Text = ""
        hfTRXKMINDVIDKelurahan.Value = ""
        txtTRXKMINDVIDKecamatan.Text = ""
        hfTRXKMINDVIDKecamatan.Value = ""
        txtTRXKMINDVIDKotaKab.Text = ""
        hfTRXKMINDVIDKotaKab.Value = ""
        txtTRXKMINDVIDKodePos.Text = ""
        txtTRXKMINDVIDProvinsi.Text = ""
        hfTRXKMINDVIDProvinsi.Value = ""
        txtTRXKMINDVNANamaJalan.Text = ""
        txtTRXKMINDVNANegara.Text = ""
        hfTRXKMINDVNANegara.Value = ""
        txtTRXKMINDVNAProvinsi.Text = ""
        hfTRXKMINDVNAProvinsi.Value = ""
        txtTRXKMINDVNAKota.Text = ""
        hfTRXKMINDVNAKota.Value = ""
        txtTRXKMINDVNAKodePos.Text = ""
        cboTRXKMINDVJenisID.SelectedIndex = 0
        txtTRXKMINDVNomorID.Text = ""
        txtTRXKMINDVNPWP.Text = ""
        txtTRXKMINDVPekerjaan.Text = ""
        hfTRXKMINDVPekerjaan.Value = ""
        txtTRXKMINDVJabatan.Text = ""
        txtTRXKMINDVPenghasilanRataRata.Text = ""
        txtTRXKMINDVTempatKerja.Text = ""
        txtTRXKMINDVTujuanTrx.Text = ""
        txtTRXKMINDVSumberDana.Text = ""
        txtTRXKMINDVNamaBankLain.Text = ""
        txtTRXKMINDVNoRekeningTujuan.Text = ""

        cboTRXKMCORPBentukBadanUsaha.SelectedIndex = 0
        txtTRXKMCORPNama.Text = ""
        txtTRXKMCORPBidangUsaha.Text = ""
        hfTRXKMCORPBidangUsaha.Value = ""
        txtTRXKMCORPDLNamaJalan.Text = ""
        txtTRXKMCORPDLRTRW.Text = ""
        txtTRXKMCORPDLKelurahan.Text = ""
        hfTRXKMCORPDLKelurahan.Value = ""
        txtTRXKMCORPDLKecamatan.Text = ""
        hfTRXKMCORPDLKecamatan.Value = ""
        txtTRXKMCORPDLKotaKab.Text = ""
        hfTRXKMCORPDLKotaKab.Value = ""
        txtTRXKMCORPDLKodePos.Text = ""
        txtTRXKMCORPDLProvinsi.Text = ""
        hfTRXKMCORPDLProvinsi.Value = ""
        txtTRXKMCORPDLNegara.Text = ""
        hfTRXKMCORPDLNegara.Value = ""
        txtTRXKMCORPLNNamaJalan.Text = ""
        txtTRXKMCORPLNNegara.Text = ""
        hfTRXKMCORPLNNegara.Value = ""
        txtTRXKMCORPLNProvinsi.Text = ""
        hfTRXKMCORPLNProvinsi.Value = ""
        txtTRXKMCORPLNKota.Text = ""
        hfTRXKMCORPLNKota.Value = ""
        txtTRXKMCORPLNKodePos.Text = ""
        txtTRXKMCORPNPWP.Text = ""
        txtTRXKMCORPTujuanTrx.Text = ""
        txtTRXKMCORPSumberDana.Text = ""
        txtTRXKMCORPNamaBankLain.Text = ""
        txtTRXKMCORPNoRekeningTujuan.Text = ""

        'Bersihin sisa Kas Keluar
        txtTRXKKTanggalTransaksi.Text = ""
        txtTRXKKNamaKantor.Text = ""
        txtTRXKKKotaKab.Text = ""
        hfTRXKKKotaKab.Value = ""
        txtTRXKKProvinsi.Text = ""
        hfTRXKKProvinsi.Value = ""
        txtTRXKKDetailKasKeluar.Text = ""
        txtTRXKKDetilMataUang.Text = ""
        hfTRXKKDetilMataUang.Value = ""
        txtTRXKKDetilKursTrx.Text = ""
        txtTRXKKDetilJumlah.Text = ""
        lblDetilKasKeluarJumlahRp.Text = ""
        'txtTRXKKNoRekening.Text = ""
        txtTRXKKINDVGelar.Text = ""
        txtTRXKKINDVNamaLengkap.Text = ""
        txtTRXKKINDVTempatLahir.Text = ""
        txtTRXKKINDVTglLahir.Text = ""
        rblTRXKKINDVKewarganegaraan.SelectedIndex = -1
        cboTRXKKINDVNegara.SelectedIndex = 0
        txtTRXKKINDVDOMNamaJalan.Text = ""
        txtTRXKKINDVDOMRTRW.Text = ""
        txtTRXKKINDVDOMKelurahan.Text = ""
        hfTRXKKINDVDOMKelurahan.Value = ""
        txtTRXKKINDVDOMKecamatan.Text = ""
        hfTRXKKINDVDOMKecamatan.Value = ""
        txtTRXKKINDVDOMKotaKab.Text = ""
        hfTRXKKINDVDOMKotaKab.Value = ""
        txtTRXKKINDVDOMKodePos.Text = ""
        txtTRXKKINDVDOMProvinsi.Text = ""
        hfTRXKKINDVDOMProvinsi.Value = ""
        'chkTRXKKINDVIDCopyDOM.Checked = False
        txtTRXKKINDVIDNamaJalan.Text = ""
        txtTRXKKINDVIDRTRW.Text = ""
        txtTRXKKINDVIDKelurahan.Text = ""
        hfTRXKKINDVIDKelurahan.Value = ""
        txtTRXKKINDVIDKecamatan.Text = ""
        hfTRXKKINDVIDKecamatan.Value = ""
        txtTRXKKINDVIDKotaKab.Text = ""
        hfTRXKKINDVIDKotaKab.Value = ""
        txtTRXKKINDVIDKodePos.Text = ""
        txtTRXKKINDVIDProvinsi.Text = ""
        hfTRXKKINDVIDProvinsi.Value = ""
        txtTRXKKINDVNANamaJalan.Text = ""
        txtTRXKKINDVNANegara.Text = ""
        hfTRXKKINDVNANegara.Value = ""
        txtTRXKKINDVNAProvinsi.Text = ""
        hfTRXKKINDVNAProvinsi.Value = ""
        txtTRXKKINDVNAKota.Text = ""
        hfTRXKKINDVNAKota.Value = ""
        txtTRXKKINDVNAKodePos.Text = ""
        cboTRXKKINDVJenisID.SelectedIndex = 0
        txtTRXKKINDVNomorId.Text = ""
        txtTRXKKINDVNPWP.Text = ""
        txtTRXKKINDVPekerjaan.Text = ""
        hfTRXKKINDVPekerjaan.Value = ""
        txtTRXKKINDVJabatan.Text = ""
        txtTRXKKINDVPenghasilanRataRata.Text = ""
        txtTRXKKINDVTempatKerja.Text = ""
        txtTRXKKINDVTujuanTrx.Text = ""
        txtTRXKKINDVSumberDana.Text = ""
        txtTRXKKINDVNamaBankLain.Text = ""
        txtTRXKKINDVNoRekTujuan.Text = ""

        cboTRXKKCORPBentukBadanUsaha.SelectedIndex = 0
        txtTRXKKCORPNama.Text = ""
        txtTRXKKCORPBidangUsaha.Text = ""
        hfTRXKKCORPBidangUsaha.Value = ""
        txtTRXKKCORPDLNamaJalan.Text = ""
        txtTRXKKCORPDLRTRW.Text = ""
        txtTRXKKCORPDLKelurahan.Text = ""
        hfTRXKKCORPDLKelurahan.Value = ""
        txtTRXKKCORPDLKecamatan.Text = ""
        hfTRXKKCORPDLKecamatan.Value = ""
        txtTRXKKCORPDLKotaKab.Text = ""
        hfTRXKKCORPDLKotaKab.Value = ""
        txtTRXKKCORPDLKodePos.Text = ""
        txtTRXKKCORPDLProvinsi.Text = ""
        hfTRXKKCORPDLProvinsi.Value = ""
        txtTRXKKCORPDLNegara.Text = ""
        hfTRXKKCORPDLNegara.Value = ""
        txtTRXKKCORPLNNamaJalan.Text = ""
        txtTRXKKCORPLNNegara.Text = ""
        hfTRXKKCORPLNNegara.Value = ""
        txtTRXKKCORPLNProvinsi.Text = ""
        hfTRXKKCORPLNProvinsi.Value = ""
        txtTRXKKCORPLNKota.Text = ""
        hfTRXKKCORPLNKota.Value = ""
        txtTRXKKCORPLNKodePos.Text = ""
        txtTRXKKCORPNPWP.Text = ""
        txtTRXKKCORPTujuanTrx.Text = ""
        txtTRXKKCORPNamaBankLain.Text = ""
        txtTRXKKCORPNoRekeningTujuan.Text = ""


    End Sub

    Function JumlahKeseluruhanRp(ByVal objKas As List(Of WICDetailCashInDetail)) As Decimal
        Dim total As Decimal = 0
        For Each obj As WICDetailCashInDetail In objKas
            total = total + CDec(obj.JumlahRp)
        Next
        Return total
    End Function

    Private Sub SetControlLoad()
        rblTerlaporTipePelapor.SelectedValue = 1
        divPerorangan.Visible = True
        divKorporasi.Visible = False

        rblTRXKMTipePelapor.SelectedValue = 1
        tblTRXKMTipePelapor.Visible = True
        tblTRXKMTipePelaporKorporasi.Visible = False

        rblTRXKKTipePelapor.SelectedValue = 1
        tblTRXKKTipePelaporPerorangan.Visible = True
        tblTRXKKTipePelaporKorporasi.Visible = False

        'bind MsKepemilikan
        Using objPemilik As TList(Of MsKepemilikan) = DataRepository.MsKepemilikanProvider.GetAll
            If objPemilik.Count > 0 Then
                cboTerlaporKepemilikan.Items.Clear()
                cboTerlaporKepemilikan.Items.Add("-Select-")
                For i As Integer = 0 To objPemilik.Count - 1
                    cboTerlaporKepemilikan.Items.Add(New ListItem(objPemilik(i).NamaKepemilikan, objPemilik(i).IDKepemilikan.ToString))
                Next
            End If
        End Using

        'bind MsNegara
        Using objNegara As TList(Of MsNegara) = DataRepository.MsNegaraProvider.GetAll
            If objNegara.Count > 0 Then
                cboTerlaporNegara.Items.Clear()
                cboTerlaporNegara.Items.Add("-Select-")

                cboTRXKMINDVNegara.Items.Clear()
                cboTRXKMINDVNegara.Items.Add("-Select-")

                cboTRXKKINDVNegara.Items.Clear()
                cboTRXKKINDVNegara.Items.Add("-Select-")
                For i As Integer = 0 To objNegara.Count - 1
                    cboTerlaporNegara.Items.Add(New ListItem(objNegara(i).NamaNegara, objNegara(i).IDNegara.ToString))
                    cboTRXKMINDVNegara.Items.Add(New ListItem(objNegara(i).NamaNegara, objNegara(i).IDNegara.ToString))
                    cboTRXKKINDVNegara.Items.Add(New ListItem(objNegara(i).NamaNegara, objNegara(i).IDNegara.ToString))
                Next
            End If
        End Using

        'Bind MsBentukBadanUsaha
        Using objBentukBadanUsaha As TList(Of MsBentukBidangUsaha) = DataRepository.MsBentukBidangUsahaProvider.GetAll
            If objBentukBadanUsaha.Count > 0 Then
                cboTerlaporCORPBentukBadanUsaha.Items.Clear()
                cboTerlaporCORPBentukBadanUsaha.Items.Add("-Select-")

                cboTRXKMCORPBentukBadanUsaha.Items.Clear()
                cboTRXKMCORPBentukBadanUsaha.Items.Add("-Select-")

                cboTRXKKCORPBentukBadanUsaha.Items.Clear()
                cboTRXKKCORPBentukBadanUsaha.Items.Add("-Select-")

                For i As Integer = 0 To objBentukBadanUsaha.Count - 1
                    cboTerlaporCORPBentukBadanUsaha.Items.Add(New ListItem(objBentukBadanUsaha(i).BentukBidangUsaha, objBentukBadanUsaha(i).IdBentukBidangUsaha.ToString))
                    cboTRXKMCORPBentukBadanUsaha.Items.Add(New ListItem(objBentukBadanUsaha(i).BentukBidangUsaha, objBentukBadanUsaha(i).IdBentukBidangUsaha.ToString))
                    cboTRXKKCORPBentukBadanUsaha.Items.Add(New ListItem(objBentukBadanUsaha(i).BentukBidangUsaha, objBentukBadanUsaha(i).IdBentukBidangUsaha.ToString))
                Next
            End If
        End Using

        'bind ID type
        Using objJenisID As TList(Of MsIDType) = DataRepository.MsIDTypeProvider.GetAll
            cboTerlaporJenisDocID.Items.Clear()
            cboTerlaporJenisDocID.Items.Add("-Select-")

            cboTRXKMINDVJenisID.Items.Clear()
            cboTRXKMINDVJenisID.Items.Add("-Select-")

            cboTRXKKINDVJenisID.Items.Clear()
            cboTRXKKINDVJenisID.Items.Add("-Select-")

            For i As Integer = 0 To objJenisID.Count - 1
                cboTerlaporJenisDocID.Items.Add(New ListItem(objJenisID(i).MsIDType_Name, objJenisID(i).Pk_MsIDType_Id.ToString))
                cboTRXKMINDVJenisID.Items.Add(New ListItem(objJenisID(i).MsIDType_Name, objJenisID(i).Pk_MsIDType_Id.ToString))
                cboTRXKKINDVJenisID.Items.Add(New ListItem(objJenisID(i).MsIDType_Name, objJenisID(i).Pk_MsIDType_Id.ToString))
            Next




        End Using



    End Sub

    Sub clearSession()
        Session("WICViewDetail.grvTRXKMDetilValutaAsingDATA") = Nothing
        Session("WICViewDetail.grvDetilKasKeluarDATA") = Nothing
        Session("WICViewDetail.ResumeKasMasukKasKeluar") = Nothing
        Session("WICViewDetail.RowEdit") = Nothing
        Session("WICViewDetail.WICPK") = Nothing
    End Sub

    Sub loadResume()
        Dim objResume As List(Of ResumeKasMasukKeluarWIC) = SetnGetResumeKasMasukKasKeluar

        'load Kas Masuk ke objResume
        Using objListTransactionCashIn As TList(Of WICTransactionCashIn) = DataRepository.WICTransactionCashInProvider.GetPaged(WICTransactionCashInColumn.FK_WIC_Id.ToString & " = " & getWICPK.ToString, "", 0, Integer.MaxValue, 0)
            For Each objSingleTransactinCashIn As WICTransactionCashIn In objListTransactionCashIn


                Dim objKas As ResumeKasMasukKeluarWIC = New ResumeKasMasukKeluarWIC
                'Insert untuk tampilan Grid
                objKas.TransactionDate = objSingleTransactinCashIn.TanggalTransaksi
                objKas.Branch = objSingleTransactinCashIn.NamaKantorPJK
                objKas.TransactionNominal = objSingleTransactinCashIn.Total.ToString
                objKas.AccountNumber = objSingleTransactinCashIn.NomorRekening
                objKas.Kas = "Kas Masuk"
                objKas.Type = "WIC"

                'Insert Data Ke ObjectTransaction
                objKas.TransactionCashIn = New WICTransactionCashIn
                objKas.TransactionCashIn.PK_WICTransactionCashIn_Id = objSingleTransactinCashIn.PK_WICTransactionCashIn_Id
                objKas.TransactionCashIn.TanggalTransaksi = objSingleTransactinCashIn.TanggalTransaksi
                objKas.TransactionCashIn.NamaKantorPJK = objSingleTransactinCashIn.NamaKantorPJK
                objKas.TransactionCashIn.FK_MsKotaKab_Id = objSingleTransactinCashIn.FK_MsKotaKab_Id
                objKas.TransactionCashIn.FK_MsProvince_Id = objSingleTransactinCashIn.FK_MsProvince_Id
                objKas.TransactionCashIn.NomorRekening = objSingleTransactinCashIn.NomorRekening
                objKas.TransactionCashIn.TipeTerlapor = objSingleTransactinCashIn.TipeTerlapor
                'Individu
                objKas.TransactionCashIn.INDV_Gelar = objSingleTransactinCashIn.INDV_Gelar
                objKas.TransactionCashIn.INDV_NamaLengkap = objSingleTransactinCashIn.INDV_NamaLengkap
                objKas.TransactionCashIn.INDV_TempatLahir = objSingleTransactinCashIn.INDV_TempatLahir
                objKas.TransactionCashIn.INDV_TanggalLahir = objSingleTransactinCashIn.INDV_TanggalLahir
                objKas.TransactionCashIn.INDV_Kewarganegaraan = objSingleTransactinCashIn.INDV_Kewarganegaraan
                objKas.TransactionCashIn.INDV_FK_MsNegara_Id = objSingleTransactinCashIn.INDV_FK_MsNegara_Id
                objKas.TransactionCashIn.INDV_DOM_NamaJalan = objSingleTransactinCashIn.INDV_DOM_NamaJalan
                objKas.TransactionCashIn.INDV_DOM_RTRW = objSingleTransactinCashIn.INDV_DOM_RTRW
                objKas.TransactionCashIn.INDV_DOM_FK_MsKelurahan_Id = objSingleTransactinCashIn.INDV_DOM_FK_MsKelurahan_Id
                objKas.TransactionCashIn.INDV_DOM_FK_MsKecamatan_Id = objSingleTransactinCashIn.INDV_DOM_FK_MsKecamatan_Id
                objKas.TransactionCashIn.INDV_DOM_FK_MsKotaKab_Id = objSingleTransactinCashIn.INDV_DOM_FK_MsKotaKab_Id
                objKas.TransactionCashIn.INDV_DOM_KodePos = objSingleTransactinCashIn.INDV_DOM_KodePos
                objKas.TransactionCashIn.INDV_DOM_FK_MsProvince_Id = objSingleTransactinCashIn.INDV_DOM_FK_MsProvince_Id
                objKas.TransactionCashIn.INDV_DOM_FK_MsNegara_Id = objSingleTransactinCashIn.INDV_DOM_FK_MsNegara_Id

                objKas.TransactionCashIn.INDV_ID_NamaJalan = objSingleTransactinCashIn.INDV_ID_NamaJalan
                objKas.TransactionCashIn.INDV_ID_RTRW = objSingleTransactinCashIn.INDV_ID_RTRW
                objKas.TransactionCashIn.INDV_ID_FK_MsKelurahan_Id = objSingleTransactinCashIn.INDV_ID_FK_MsKelurahan_Id
                objKas.TransactionCashIn.INDV_ID_FK_MsKecamatan_Id = objSingleTransactinCashIn.INDV_ID_FK_MsKecamatan_Id
                objKas.TransactionCashIn.INDV_ID_FK_MsKotaKab_Id = objSingleTransactinCashIn.INDV_ID_FK_MsKotaKab_Id
                objKas.TransactionCashIn.INDV_ID_KodePos = objSingleTransactinCashIn.INDV_ID_KodePos
                objKas.TransactionCashIn.INDV_ID_FK_MsProvince_Id = objSingleTransactinCashIn.INDV_ID_FK_MsProvince_Id
                objKas.TransactionCashIn.INDV_ID_FK_MsNegara_Id = objSingleTransactinCashIn.INDV_ID_FK_MsNegara_Id

                objKas.TransactionCashIn.INDV_NA_NamaJalan = objSingleTransactinCashIn.INDV_NA_NamaJalan
                objKas.TransactionCashIn.INDV_NA_FK_MsNegara_Id = objSingleTransactinCashIn.INDV_NA_FK_MsNegara_Id
                objKas.TransactionCashIn.INDV_NA_FK_MsProvince_Id = objSingleTransactinCashIn.INDV_NA_FK_MsProvince_Id
                objKas.TransactionCashIn.INDV_NA_FK_MsKotaKab_Id = objSingleTransactinCashIn.INDV_NA_FK_MsKotaKab_Id
                objKas.TransactionCashIn.INDV_NA_KodePos = objSingleTransactinCashIn.INDV_NA_KodePos
                objKas.TransactionCashIn.INDV_FK_MsIDType_Id = objSingleTransactinCashIn.INDV_FK_MsIDType_Id
                objKas.TransactionCashIn.INDV_NomorId = objSingleTransactinCashIn.INDV_NomorId
                objKas.TransactionCashIn.INDV_NPWP = objSingleTransactinCashIn.INDV_NPWP
                objKas.TransactionCashIn.INDV_FK_MsPekerjaan_Id = objSingleTransactinCashIn.INDV_FK_MsPekerjaan_Id
                objKas.TransactionCashIn.INDV_Jabatan = objSingleTransactinCashIn.INDV_Jabatan
                objKas.TransactionCashIn.INDV_PenghasilanRataRata = objSingleTransactinCashIn.INDV_PenghasilanRataRata
                objKas.TransactionCashIn.INDV_TempatBekerja = objSingleTransactinCashIn.INDV_TempatBekerja
                objKas.TransactionCashIn.INDV_TujuanTransaksi = objSingleTransactinCashIn.INDV_TujuanTransaksi
                objKas.TransactionCashIn.INDV_SumberDana = objSingleTransactinCashIn.INDV_SumberDana
                objKas.TransactionCashIn.INDV_NamaBankLain = objSingleTransactinCashIn.INDV_NamaBankLain
                objKas.TransactionCashIn.INDV_NomorRekeningTujuan = objSingleTransactinCashIn.INDV_NomorRekeningTujuan

                objKas.TransactionCashIn.CORP_FK_MsBentukBadanUsaha_Id = objSingleTransactinCashIn.CORP_FK_MsBentukBadanUsaha_Id
                objKas.TransactionCashIn.CORP_Nama = objSingleTransactinCashIn.CORP_Nama
                objKas.TransactionCashIn.CORP_FK_MsBidangUsaha_Id = objSingleTransactinCashIn.CORP_FK_MsBidangUsaha_Id
                objKas.TransactionCashIn.CORP_TipeAlamat = objSingleTransactinCashIn.CORP_TipeAlamat
                objKas.TransactionCashIn.CORP_NamaJalan = objSingleTransactinCashIn.CORP_NamaJalan
                objKas.TransactionCashIn.CORP_RTRW = objSingleTransactinCashIn.CORP_RTRW
                objKas.TransactionCashIn.CORP_FK_MsKelurahan_Id = objSingleTransactinCashIn.CORP_FK_MsKelurahan_Id
                objKas.TransactionCashIn.CORP_FK_MsKecamatan_Id = objSingleTransactinCashIn.CORP_FK_MsKecamatan_Id
                objKas.TransactionCashIn.CORP_FK_MsKotaKab_Id = objSingleTransactinCashIn.CORP_FK_MsKotaKab_Id
                objKas.TransactionCashIn.CORP_KodePos = objSingleTransactinCashIn.CORP_KodePos
                objKas.TransactionCashIn.CORP_FK_MsProvince_Id = objSingleTransactinCashIn.CORP_FK_MsProvince_Id
                objKas.TransactionCashIn.CORP_FK_MsNegara_Id = objSingleTransactinCashIn.CORP_FK_MsNegara_Id
                objKas.TransactionCashIn.CORP_LN_NamaJalan = objSingleTransactinCashIn.CORP_LN_NamaJalan
                objKas.TransactionCashIn.CORP_LN_FK_MsNegara_Id = objSingleTransactinCashIn.CORP_LN_FK_MsNegara_Id
                objKas.TransactionCashIn.CORP_LN_FK_MsProvince_Id = objSingleTransactinCashIn.CORP_LN_FK_MsProvince_Id
                objKas.TransactionCashIn.CORP_LN_MsKotaKab_Id = objSingleTransactinCashIn.CORP_LN_MsKotaKab_Id
                objKas.TransactionCashIn.CORP_LN_KodePos = objSingleTransactinCashIn.CORP_LN_KodePos
                objKas.TransactionCashIn.CORP_NPWP = objSingleTransactinCashIn.CORP_NPWP
                objKas.TransactionCashIn.CORP_TujuanTransaksi = objSingleTransactinCashIn.CORP_TujuanTransaksi
                objKas.TransactionCashIn.CORP_SumberDana = objSingleTransactinCashIn.CORP_SumberDana
                objKas.TransactionCashIn.CORP_NamaBankLain = objSingleTransactinCashIn.CORP_NamaBankLain
                objKas.TransactionCashIn.CORP_NomorRekeningTujuan = objSingleTransactinCashIn.CORP_NomorRekeningTujuan


                'Insert Data Ke Detail Transaction
                objKas.DetailTransactionCashIn = New List(Of WICDetailCashInTransaction)

                Dim objListDetailTransactionCashIn As TList(Of WICDetailCashInTransaction) = DataRepository.WICDetailCashInTransactionProvider.GetPaged(WICDetailCashInTransactionColumn.FK_WICTransactionCashIn_Id.ToString & " = '" & objSingleTransactinCashIn.PK_WICTransactionCashIn_Id.ToString & "'", "", 0, Integer.MaxValue, 0)
                For Each objSingleDetailTransactionCashIn As WICDetailCashInTransaction In objListDetailTransactionCashIn
                    Dim objCalonInsert As New WICDetailCashInTransaction
                    objCalonInsert.PK_WICDetailCashInTransaction_Id = objSingleDetailTransactionCashIn.PK_WICDetailCashInTransaction_Id
                    objCalonInsert.Asing_KursTransaksi = objSingleDetailTransactionCashIn.Asing_KursTransaksi
                    objCalonInsert.Asing_TotalKasMasukDalamRupiah = objSingleDetailTransactionCashIn.Asing_TotalKasMasukDalamRupiah
                    objCalonInsert.TotalKasMasuk = objSingleDetailTransactionCashIn.TotalKasMasuk
                    objCalonInsert.KasMasuk = objSingleDetailTransactionCashIn.KasMasuk
                    objCalonInsert.Asing_FK_MsCurrency_Id = objSingleDetailTransactionCashIn.Asing_FK_MsCurrency_Id

                    objKas.DetailTransactionCashIn.Add(objCalonInsert)
                Next

                objResume.Add(objKas)

            Next
        End Using

        Using objListTransactionCashOut As TList(Of WICTransactionCashOut) = DataRepository.WICTransactionCashOutProvider.GetPaged(WICTransactionCashOutColumn.FK_WIC_Id.ToString & " = " & getWICPK.ToString, "", 0, Integer.MaxValue, 0)
            For Each objSingleTransactinCashOut As WICTransactionCashOut In objListTransactionCashOut


                Dim objKas As ResumeKasMasukKeluarWIC = New ResumeKasMasukKeluarWIC
                'Insert untuk tampilan Grid
                objKas.TransactionDate = objSingleTransactinCashOut.TanggalTransaksi
                objKas.Branch = objSingleTransactinCashOut.NamaKantorPJK
                objKas.TransactionNominal = objSingleTransactinCashOut.Total.ToString
                objKas.AccountNumber = objSingleTransactinCashOut.NomorRekening
                objKas.Kas = "Kas Keluar"
                objKas.Type = "WIC"

                'Insert Data Ke ObjectTransaction
                objKas.TransactionCashOut = New WICTransactionCashOut
                objKas.TransactionCashOut.PK_WICTransactionCashOut_Id = objSingleTransactinCashOut.PK_WICTransactionCashOut_Id

                objKas.TransactionCashOut.TanggalTransaksi = objSingleTransactinCashOut.TanggalTransaksi
                objKas.TransactionCashOut.NamaKantorPJK = objSingleTransactinCashOut.NamaKantorPJK
                objKas.TransactionCashOut.FK_MsKotaKab_Id = objSingleTransactinCashOut.FK_MsKotaKab_Id
                objKas.TransactionCashOut.FK_MsProvince_Id = objSingleTransactinCashOut.FK_MsProvince_Id
                objKas.TransactionCashOut.NomorRekening = objSingleTransactinCashOut.NomorRekening
                objKas.TransactionCashOut.TipeTerlapor = objSingleTransactinCashOut.TipeTerlapor
                'Individu
                objKas.TransactionCashOut.INDV_Gelar = objSingleTransactinCashOut.INDV_Gelar
                objKas.TransactionCashOut.INDV_NamaLengkap = objSingleTransactinCashOut.INDV_NamaLengkap
                objKas.TransactionCashOut.INDV_TempatLahir = objSingleTransactinCashOut.INDV_TempatLahir
                objKas.TransactionCashOut.INDV_TanggalLahir = objSingleTransactinCashOut.INDV_TanggalLahir
                objKas.TransactionCashOut.INDV_Kewarganegaraan = objSingleTransactinCashOut.INDV_Kewarganegaraan
                objKas.TransactionCashOut.INDV_FK_MsNegara_Id = objSingleTransactinCashOut.INDV_FK_MsNegara_Id
                objKas.TransactionCashOut.INDV_DOM_NamaJalan = objSingleTransactinCashOut.INDV_DOM_NamaJalan
                objKas.TransactionCashOut.INDV_DOM_RTRW = objSingleTransactinCashOut.INDV_DOM_RTRW
                objKas.TransactionCashOut.INDV_DOM_FK_MsKelurahan_Id = objSingleTransactinCashOut.INDV_DOM_FK_MsKelurahan_Id
                objKas.TransactionCashOut.INDV_DOM_FK_MsKecamatan_Id = objSingleTransactinCashOut.INDV_DOM_FK_MsKecamatan_Id
                objKas.TransactionCashOut.INDV_DOM_FK_MsKotaKab_Id = objSingleTransactinCashOut.INDV_DOM_FK_MsKotaKab_Id
                objKas.TransactionCashOut.INDV_DOM_KodePos = objSingleTransactinCashOut.INDV_DOM_KodePos
                objKas.TransactionCashOut.INDV_DOM_FK_MsProvince_Id = objSingleTransactinCashOut.INDV_DOM_FK_MsProvince_Id
                objKas.TransactionCashOut.INDV_DOM_FK_MsNegara_Id = objSingleTransactinCashOut.INDV_DOM_FK_MsNegara_Id

                objKas.TransactionCashOut.INDV_ID_NamaJalan = objSingleTransactinCashOut.INDV_ID_NamaJalan
                objKas.TransactionCashOut.INDV_ID_RTRW = objSingleTransactinCashOut.INDV_ID_RTRW
                objKas.TransactionCashOut.INDV_ID_FK_MsKelurahan_Id = objSingleTransactinCashOut.INDV_ID_FK_MsKelurahan_Id
                objKas.TransactionCashOut.INDV_ID_FK_MsKecamatan_Id = objSingleTransactinCashOut.INDV_ID_FK_MsKecamatan_Id
                objKas.TransactionCashOut.INDV_ID_FK_MsKotaKab_Id = objSingleTransactinCashOut.INDV_ID_FK_MsKotaKab_Id
                objKas.TransactionCashOut.INDV_ID_KodePos = objSingleTransactinCashOut.INDV_ID_KodePos
                objKas.TransactionCashOut.INDV_ID_FK_MsProvince_Id = objSingleTransactinCashOut.INDV_ID_FK_MsProvince_Id
                objKas.TransactionCashOut.INDV_ID_FK_MsNegara_Id = objSingleTransactinCashOut.INDV_ID_FK_MsNegara_Id

                objKas.TransactionCashOut.INDV_NA_NamaJalan = objSingleTransactinCashOut.INDV_NA_NamaJalan
                objKas.TransactionCashOut.INDV_NA_FK_MsNegara_Id = objSingleTransactinCashOut.INDV_NA_FK_MsNegara_Id
                objKas.TransactionCashOut.INDV_NA_FK_MsProvince_Id = objSingleTransactinCashOut.INDV_NA_FK_MsProvince_Id
                objKas.TransactionCashOut.INDV_NA_FK_MsKotaKab_Id = objSingleTransactinCashOut.INDV_NA_FK_MsKotaKab_Id
                objKas.TransactionCashOut.INDV_NA_KodePos = objSingleTransactinCashOut.INDV_NA_KodePos
                objKas.TransactionCashOut.INDV_FK_MsIDType_Id = objSingleTransactinCashOut.INDV_FK_MsIDType_Id
                objKas.TransactionCashOut.INDV_NomorId = objSingleTransactinCashOut.INDV_NomorId
                objKas.TransactionCashOut.INDV_NPWP = objSingleTransactinCashOut.INDV_NPWP
                objKas.TransactionCashOut.INDV_FK_MsPekerjaan_Id = objSingleTransactinCashOut.INDV_FK_MsPekerjaan_Id
                objKas.TransactionCashOut.INDV_Jabatan = objSingleTransactinCashOut.INDV_Jabatan
                objKas.TransactionCashOut.INDV_PenghasilanRataRata = objSingleTransactinCashOut.INDV_PenghasilanRataRata
                objKas.TransactionCashOut.INDV_TempatBekerja = objSingleTransactinCashOut.INDV_TempatBekerja
                objKas.TransactionCashOut.INDV_TujuanTransaksi = objSingleTransactinCashOut.INDV_TujuanTransaksi
                objKas.TransactionCashOut.INDV_SumberDana = objSingleTransactinCashOut.INDV_SumberDana
                objKas.TransactionCashOut.INDV_NamaBankLain = objSingleTransactinCashOut.INDV_NamaBankLain
                objKas.TransactionCashOut.INDV_NomorRekeningTujuan = objSingleTransactinCashOut.INDV_NomorRekeningTujuan

                objKas.TransactionCashOut.CORP_FK_MsBentukBadanUsaha_Id = objSingleTransactinCashOut.CORP_FK_MsBentukBadanUsaha_Id
                objKas.TransactionCashOut.CORP_Nama = objSingleTransactinCashOut.CORP_Nama
                objKas.TransactionCashOut.CORP_FK_MsBidangUsaha_Id = objSingleTransactinCashOut.CORP_FK_MsBidangUsaha_Id
                objKas.TransactionCashOut.CORP_TipeAlamat = objSingleTransactinCashOut.CORP_TipeAlamat
                objKas.TransactionCashOut.CORP_NamaJalan = objSingleTransactinCashOut.CORP_NamaJalan
                objKas.TransactionCashOut.CORP_RTRW = objSingleTransactinCashOut.CORP_RTRW
                objKas.TransactionCashOut.CORP_FK_MsKelurahan_Id = objSingleTransactinCashOut.CORP_FK_MsKelurahan_Id
                objKas.TransactionCashOut.CORP_FK_MsKecamatan_Id = objSingleTransactinCashOut.CORP_FK_MsKecamatan_Id
                objKas.TransactionCashOut.CORP_FK_MsKotaKab_Id = objSingleTransactinCashOut.CORP_FK_MsKotaKab_Id
                objKas.TransactionCashOut.CORP_KodePos = objSingleTransactinCashOut.CORP_KodePos
                objKas.TransactionCashOut.CORP_FK_MsProvince_Id = objSingleTransactinCashOut.CORP_FK_MsProvince_Id
                objKas.TransactionCashOut.CORP_FK_MsNegara_Id = objSingleTransactinCashOut.CORP_FK_MsNegara_Id
                objKas.TransactionCashOut.CORP_LN_NamaJalan = objSingleTransactinCashOut.CORP_LN_NamaJalan
                objKas.TransactionCashOut.CORP_LN_FK_MsNegara_Id = objSingleTransactinCashOut.CORP_LN_FK_MsNegara_Id
                objKas.TransactionCashOut.CORP_LN_FK_MsProvince_Id = objSingleTransactinCashOut.CORP_LN_FK_MsProvince_Id
                objKas.TransactionCashOut.CORP_LN_MsKotaKab_Id = objSingleTransactinCashOut.CORP_LN_MsKotaKab_Id
                objKas.TransactionCashOut.CORP_LN_KodePos = objSingleTransactinCashOut.CORP_LN_KodePos
                objKas.TransactionCashOut.CORP_NPWP = objSingleTransactinCashOut.CORP_NPWP
                objKas.TransactionCashOut.CORP_TujuanTransaksi = objSingleTransactinCashOut.CORP_TujuanTransaksi
                objKas.TransactionCashOut.CORP_SumberDana = objSingleTransactinCashOut.CORP_SumberDana
                objKas.TransactionCashOut.CORP_NamaBankLain = objSingleTransactinCashOut.CORP_NamaBankLain
                objKas.TransactionCashOut.CORP_NomorRekeningTujuan = objSingleTransactinCashOut.CORP_NomorRekeningTujuan


                'Insert Data Ke Detail Transaction
                objKas.DetailTranscationCashOut = New List(Of WICDetailCashOutTransaction)

                Dim objListDetailTransactionCashOut As TList(Of WICDetailCashOutTransaction) = DataRepository.WICDetailCashOutTransactionProvider.GetPaged(WICDetailCashOutTransactionColumn.FK_WICTransactionCashOut_Id.ToString & " = '" & objSingleTransactinCashOut.PK_WICTransactionCashOut_Id.ToString & "'", "", 0, Integer.MaxValue, 0)
                For Each objSingleDetailTransactionCashOut As WICDetailCashOutTransaction In objListDetailTransactionCashOut
                    Dim objCalonInsert As New WICDetailCashOutTransaction
                    objCalonInsert.PK_WICDetailCashOutTransaction_Id = objSingleDetailTransactionCashOut.PK_WICDetailCashOutTransaction_Id
                    objCalonInsert.Asing_KursTransaksi = objSingleDetailTransactionCashOut.Asing_KursTransaksi
                    objCalonInsert.Asing_TotalKasKeluarDalamRupiah = objSingleDetailTransactionCashOut.Asing_TotalKasKeluarDalamRupiah
                    objCalonInsert.TotalKasKeluar = objSingleDetailTransactionCashOut.TotalKasKeluar
                    objCalonInsert.KasKeluar = objSingleDetailTransactionCashOut.KasKeluar
                    objCalonInsert.Asing_FK_MsCurrency_Id = objSingleDetailTransactionCashOut.Asing_FK_MsCurrency_Id

                    objKas.DetailTranscationCashOut.Add(objCalonInsert)
                Next

                objResume.Add(objKas)

            Next
        End Using

        SetnGetResumeKasMasukKasKeluar = objResume
        grvTransaksi.DataSource = objResume
        grvTransaksi.DataBind()


        Dim totalKasMasuk As Decimal = 0
        Dim totalKasKeluar As Decimal = 0
        For Each kas As ResumeKasMasukKeluarWIC In objResume
            If kas.Kas = "Kas Masuk" Then
                totalKasMasuk += CDec(kas.TransactionNominal)
            Else
                totalKasKeluar += CDec(kas.TransactionNominal)
            End If
        Next

        lblTotalKasMasuk.Text = ValidateBLL.FormatMoneyWithComma(totalKasMasuk)
        lblTotalKasKeluar.Text = ValidateBLL.FormatMoneyWithComma(totalKasKeluar)

    End Sub

    'Sub loadWICToField()
    '    Using objWic As WIC = DataRepository.WICProvider.GetByPK_WIC_Id(getWICPK)
    '        SafeDefaultValue = ""
    '        If Not IsNothing(objWic) Then

    '            'Ambil data buat cari PK Negara, Provinsi, KotaKab, dkk (ini variable yang bakal dpke berkali2)
    '            Dim i As Integer = 0 'buat iterasi
    '            Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetAll
    '            Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
    '            Dim objKelurahan As TList(Of MsKelurahan) = DataRepository.MsKelurahanProvider.GetAll
    '            Dim objSKelurahan As MsKelurahan 'Penampung hasil search kelurahan
    '            Dim objKecamatan As TList(Of MsKecamatan) = DataRepository.MsKecamatanProvider.GetAll
    '            Dim objSkecamatan As MsKecamatan 'Penampung hasil search kecamatan
    '            Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetAll
    '            Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi
    '            Dim objMapNegara As TList(Of MappingMsNegaraNCBSPPATK) = DataRepository.MappingMsNegaraNCBSPPATKProvider.GetAll
    '            Dim objSMapNegara As MappingMsNegaraNCBSPPATK 'Penampung hasil search mappingnegara yang pke picker
    '            Dim objNegara As TList(Of MsNegaraNCBS) = DataRepository.MsNegaraNCBSProvider.GetAll
    '            Dim objSnegara As MsNegaraNCBS 'Penampung hasil search negara yang pke picker
    '            Dim objPekerjaan As TList(Of MsPekerjaan) = DataRepository.MsPekerjaanProvider.GetAll
    '            Dim objSPekerjaan As MsPekerjaan 'Penampung hasil search pekerjaan
    '            Dim objBidangUsaha As TList(Of MsBidangUsaha) = DataRepository.MsBidangUsahaProvider.GetAll
    '            Dim objSBidangUsaha As MsBidangUsaha 'Penampung hasil search bidang usaha

    '            txtUmumPJKPelapor.Text = Safe(objWic.NamaPJKPelapor)
    '            txtTglLaporan.Text = FormatDate(objWic.TanggalLaporan)
    '            txtPejabatPelapor.Text = Safe(objWic.NamaPejabatPJKPelapor)
    '            If objWic.NoWICKoreksi <> "" Then
    '                cboTipeLaporan.SelectedIndex = 1
    '                txtNoLTKTKoreksi.Text = Safe(objWic.NoWICKoreksi)
    '                tipeLaporanCheck()
    '            End If
    '            i = 0
    '            For Each listKepemilikan As ListItem In cboTerlaporKepemilikan.Items
    '                If objWic.FK_MsKepemilikan_ID.ToString = listKepemilikan.Value Then
    '                    cboTerlaporKepemilikan.SelectedIndex = i
    '                    Exit For
    '                End If
    '                i = i + 1
    '            Next
    '            txtWICInformasiLainnya.Text = Safe(objWic.InformasiLainnya)
    '            txtTerlaporNoRekening.Text = Safe(objWic.NoRekening)
    '            rblTerlaporTipePelapor.SelectedIndex = CInt(objWic.TipeTerlapor)
    '            WICTipeTerlaporChange()
    '            txtTerlaporGelar.Text = Safe(objWic.INDV_Gelar)
    '            txtTerlaporNamaLengkap.Text = Safe(objWic.INDV_NamaLengkap)
    '            txtTerlaporTempatLahir.Text = Safe(objWic.INDV_TempatLahir)
    '            txtTerlaporTglLahir.Text = FormatDate(objWic.INDV_TanggalLahir)
    '            rblTerlaporKewarganegaraan.SelectedValue = objWic.INDV_Kewarganegaraan
    '            i = 0
    '            For Each listNegara As ListItem In cboTerlaporNegara.Items
    '                If objWic.INDV_FK_MsNegara_Id.ToString = listNegara.Value Then
    '                    cboTerlaporNegara.SelectedIndex = i
    '                    Exit For
    '                End If
    '                i = i + 1
    '            Next
    '            txtTerlaporDOMNamaJalan.Text = Safe(objWic.INDV_DOM_NamaJalan)
    '            txtTerlaporDOMRTRW.Text = Safe(objWic.INDV_DOM_RTRW)
    '            objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objWic.INDV_DOM_FK_MsKelurahan_Id)
    '            If Not IsNothing(objSKelurahan) Then
    '                txtTerlaporDOMKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
    '                hfTerlaporDOMKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
    '            End If
    '            objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objWic.INDV_DOM_FK_MsKecamatan_Id)
    '            If Not IsNothing(objSkecamatan) Then
    '                txtTerlaporDOMKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
    '                hfTerlaporDOMKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
    '            End If
    '            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objWic.INDV_DOM_FK_MsKotaKab_Id)
    '            If Not IsNothing(objSkotaKab) Then
    '                txtTerlaporDOMKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
    '                hfTerlaporDOMKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
    '            End If
    '            txtTerlaporDOMKodePos.Text = Safe(objWic.INDV_DOM_KodePos)
    '            objSProvinsi = objProvinsi.Find(MsProvinceColumn.PK_MsPropincePPATK_ID, objWic.INDV_DOM_FK_MsProvince_Id)
    '            If Not IsNothing(objSProvinsi) Then
    '                txtTerlaporDOMProvinsi.Text = Safe(objSProvinsi.MsProvicePPATKName)
    '                hfTerlaporDOMProvinsi.Value = Safe(objSProvinsi.PK_MsPropincePPATK_ID)
    '            End If
    '            txtTerlaporIDNamaJalan.Text = Safe(objWic.INDV_ID_NamaJalan)
    '            txtTerlaporIDRTRW.Text = Safe(objWic.INDV_ID_RTRW)
    '            objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objWic.INDV_ID_FK_MsKelurahan_Id)
    '            If Not IsNothing(objSKelurahan) Then
    '                txtTerlaporIDKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
    '                hfTerlaporIDKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
    '            End If
    '            objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objWic.INDV_ID_FK_MsKecamatan_Id)
    '            If Not IsNothing(objSkecamatan) Then
    '                txtTerlaporIDKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
    '                hfTerlaporIDKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
    '            End If
    '            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objWic.INDV_ID_FK_MsKotaKab_Id)
    '            If Not IsNothing(objSkotaKab) Then
    '                txtTerlaporIDKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
    '                hfTerlaporIDKotaKab.Value = objSkotaKab.IDKotaKab
    '            End If
    '            txtTerlaporIDKodePos.Text = Safe(objWic.INDV_ID_KodePos)
    '            objSProvinsi = objProvinsi.Find(MsProvinceColumn.PK_MsPropincePPATK_ID, objWic.INDV_ID_FK_MsProvince_Id)
    '            If Not IsNothing(objSProvinsi) Then
    '                txtTerlaporIDProvinsi.Text = Safe(objSProvinsi.MsProvicePPATKName)
    '                hfTerlaporIDProvinsi.Value = Safe(objSProvinsi.PK_MsPropincePPATK_ID)
    '            End If
    '            txtTerlaporNANamaJalan.Text = Safe(objWic.INDV_NA_NamaJalan)
    '            objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objWic.INDV_NA_FK_MsNegara_Id)
    '            If Not IsNothing(objSMapNegara) Then
    '                objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
    '                If Not IsNothing(objSnegara) Then
    '                    txtTerlaporNANegara.Text = Safe(objSnegara.NamaNegara)
    '                    hfTerlaporIDNegara.Value = objSnegara.IDNegaraNCBS
    '                End If
    '            End If
    '            objSProvinsi = objProvinsi.Find(MsProvinceColumn.PK_MsPropincePPATK_ID, objWic.INDV_NA_FK_MsProvince_Id)
    '            If Not IsNothing(objSProvinsi) Then
    '                txtTerlaporNAProvinsi.Text = Safe(objSProvinsi.MsProvicePPATKName)
    '                hfTerlaporNAProvinsi.Value = Safe(objSProvinsi.PK_MsPropincePPATK_ID)
    '            End If
    '            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objWic.INDV_NA_FK_MsKotaKab_Id)
    '            If Not IsNothing(objSkotaKab) Then
    '                txtTerlaporNAKota.Text = Safe(objSkotaKab.NamaKotaKab)
    '                hfTerlaporIDKota.Value = Safe(objSkotaKab.IDKotaKab)
    '            End If
    '            txtTerlaporNAKodePos.Text = Safe(objWic.INDV_NA_KodePos)
    '            i = 0
    '            For Each jenisDoc As ListItem In cboTerlaporJenisDocID.Items
    '                If objWic.INDV_FK_MsIDType_Id.ToString = jenisDoc.Value Then
    '                    cboTerlaporJenisDocID.SelectedIndex = i
    '                    Exit For
    '                End If
    '                i = i + 1
    '            Next
    '            txtTerlaporNomorID.Text = Safe(objWic.INDV_NomorId)
    '            txtTerlaporNPWP.Text = Safe(objWic.INDV_NPWP)
    '            objSPekerjaan = objPekerjaan.Find(MsPekerjaanColumn.IDPekerjaan, objWic.INDV_FK_MsPekerjaan_Id)
    '            If Not IsNothing(objSPekerjaan) Then
    '                txtTerlaporPekerjaan.Text = Safe(objSPekerjaan.NamaPekerjaan)
    '                hfTerlaporPekerjaan.Value = Safe(objSPekerjaan.IDPekerjaan)
    '            End If
    '            txtTerlaporJabatan.Text = Safe(objWic.INDV_Jabatan)
    '            txtTerlaporPenghasilanRataRata.Text = Safe(objWic.INDV_PenghasilanRataRata)
    '            txtTerlaporTempatKerja.Text = Safe(objWic.INDV_TempatBekerja)
    '            i = 0
    '            For Each listBentukBadanUsaha As ListItem In cboTerlaporCORPBentukBadanUsaha.Items
    '                If objWic.CORP_FK_MsBentukBadanUsaha_Id.ToString = listBentukBadanUsaha.Value Then
    '                    cboTerlaporCORPBentukBadanUsaha.SelectedIndex = i
    '                    Exit For
    '                End If
    '                i = i + 1
    '            Next
    '            txtTerlaporCORPNama.Text = Safe(objWic.CORP_Nama)
    '            objSBidangUsaha = objBidangUsaha.Find(MsBidangUsahaColumn.BidangUsahaId, objWic.CORP_FK_MsBidangUsaha_Id)
    '            If Not IsNothing(objSBidangUsaha) Then
    '                txtTerlaporCORPBidangUsaha.Text = Safe(objSBidangUsaha.NamaBidangUsaha)
    '                hfTerlaporCORPBidangUsaha.Value = Safe(objSBidangUsaha.BidangUsahaId)
    '            End If

    '            rblTerlaporCORPTipeAlamat.SelectedIndex = CInt(objWic.CORP_TipeAlamat)

    '            txtTerlaporCORPDLNamaJalan.Text = Safe(objWic.CORP_NamaJalan)
    '            txtTerlaporCORPDLRTRW.Text = Safe(objWic.CORP_RTRW)

    '            objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objWic.CORP_FK_MsKelurahan_Id)
    '            If Not IsNothing(objSKelurahan) Then
    '                txtTerlaporCORPDLKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
    '                hfTerlaporCORPDLKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
    '            End If
    '            objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objWic.CORP_FK_MsKecamatan_Id)
    '            If Not IsNothing(objSkecamatan) Then
    '                txtTerlaporCORPDLKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
    '                hfTerlaporCORPDLKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
    '            End If
    '            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objWic.CORP_FK_MsKotaKab_Id)
    '            If Not IsNothing(objSkotaKab) Then
    '                txtTerlaporCORPDLKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
    '                hfTerlaporCORPDLKotaKab.Value = objSkotaKab.IDKotaKab
    '            End If
    '            txtTerlaporCORPDLKodePos.Text = Safe(objWic.CORP_KodePos)
    '            objSProvinsi = objProvinsi.Find(MsProvinceColumn.PK_MsPropincePPATK_ID, objWic.CORP_FK_MsProvince_Id)
    '            If Not IsNothing(objSProvinsi) Then
    '                txtTerlaporCORPDLProvinsi.Text = Safe(objSProvinsi.MsProvicePPATKName)
    '                hfTerlaporCORPDLProvinsi.Value = objSProvinsi.PK_MsPropincePPATK_ID
    '            End If
    '            objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objWic.CORP_FK_MsNegara_Id)
    '            If Not IsNothing(objSMapNegara) Then
    '                objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
    '                If Not IsNothing(objSnegara) Then
    '                    txtTerlaporCORPDLNegara.Text = Safe(objSnegara.NamaNegara)
    '                    hfTerlaporCORPDLNegara.Value = Safe(objSnegara.IDNegaraNCBS)
    '                End If
    '            End If
    '            txtTerlaporCORPLNNamaJalan.Text = Safe(objWic.CORP_LN_NamaJalan)
    '            objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objWic.CORP_LN_FK_MsNegara_Id)
    '            If Not IsNothing(objSMapNegara) Then
    '                objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
    '                If Not IsNothing(objSnegara) Then
    '                    txtTerlaporCORPLNNegara.Text = Safe(objSnegara.NamaNegara)
    '                    hfTerlaporCORPLNNegara.Value = Safe(objSnegara.IDNegaraNCBS)
    '                End If
    '            End If
    '            objSProvinsi = objProvinsi.Find(MsProvinceColumn.PK_MsPropincePPATK_ID, objWic.CORP_LN_FK_MsProvince_Id)
    '            If Not IsNothing(objSProvinsi) Then
    '                txtTerlaporCORPLNProvinsi.Text = Safe(objSProvinsi.MsProvicePPATKName)
    '                hfTerlaporCORPLNProvinsi.Value = Safe(objSProvinsi.PK_MsPropincePPATK_ID)
    '            End If
    '            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objWic.CORP_LN_MsKotaKab_Id)
    '            If Not IsNothing(objSkotaKab) Then
    '                txtTerlaporCORPLNKota.Text = Safe(objSkotaKab.NamaKotaKab)
    '                hfTerlaporCORPLNKota.Value = Safe(objSkotaKab.IDKotaKab)
    '            End If
    '            txtTerlaporCORPLNKodePos.Text = Safe(objWic.CORP_LN_KodePos)
    '            txtTerlaporCORPNPWP.Text = Safe(objWic.CORP_NPWP)

    '        End If





    '    End Using
    'End Sub

    Sub WICTipeTerlaporChange()
        Try
            If rblTerlaporTipePelapor.SelectedValue = 1 Then
                divPerorangan.Visible = True
                divKorporasi.Visible = False
            Else
                divPerorangan.Visible = False
                divKorporasi.Visible = True
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Sub tipeLaporanCheck()
        If cboTipeLaporan.SelectedIndex = 0 Then
            tblWICKoreksi.Visible = False
        Else
            tblWICKoreksi.Visible = True
        End If
    End Sub
#End Region

#Region "events..."

    Protected Sub cboTipeLaporan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipeLaporan.SelectedIndexChanged
        Try
            tipeLaporanCheck()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub rblTerlaporTipePelapor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblTerlaporTipePelapor.SelectedIndexChanged
        Try
            WICTipeTerlaporChange()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then

                clearSession()
                SetControlLoad()
                loadWICToField()
                loadResume()
                If MultiView1.ActiveViewIndex = 0 Then
                    ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('IdTerlapor','Img1','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
                    ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('Transaksi','Img2','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
                End If
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Menu1_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs) Handles Menu1.MenuItemClick
        Try
            MultiView1.ActiveViewIndex = CInt(Menu1.SelectedValue)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub rblTRXKMTipePelapor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblTRXKMTipePelapor.SelectedIndexChanged
        Try
            If rblTRXKMTipePelapor.SelectedValue = 1 Then
                tblTRXKMTipePelapor.Visible = True
                tblTRXKMTipePelaporKorporasi.Visible = False
            Else
                tblTRXKMTipePelapor.Visible = False
                tblTRXKMTipePelaporKorporasi.Visible = True
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub rblTRXKKTipePelapor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblTRXKKTipePelapor.SelectedIndexChanged
        Try
            If rblTRXKKTipePelapor.SelectedValue = 1 Then
                tblTRXKKTipePelaporPerorangan.Visible = True
                tblTRXKKTipePelaporKorporasi.Visible = False
            Else
                tblTRXKKTipePelaporPerorangan.Visible = False
                tblTRXKKTipePelaporKorporasi.Visible = True
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub grvDetilKasKeluar_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grvDetilKasKeluar.RowDataBound
        Try
            If e.Row.RowType <> ListItemType.Footer And e.Row.RowType <> ListItemType.Header Then
                e.Row.Cells(0).Text = e.Row.RowIndex + 1
                If e.Row.Cells.Count > 2 Then
                    e.Row.Cells(4).Text = ValidateBLL.FormatMoneyWithComma(e.Row.Cells(4).Text)
                End If

            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub grvTRXKMDetilValutaAsing_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grvTRXKMDetilValutaAsing.PageIndexChanging
        Try
            grvTRXKMDetilValutaAsing.PageIndex = e.NewPageIndex
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub grvTRXKMDetilValutaAsing_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grvTRXKMDetilValutaAsing.RowDataBound
        Try
            If e.Row.RowType <> ListItemType.Footer And e.Row.RowType <> ListItemType.Header Then
                e.Row.Cells(0).Text = e.Row.RowIndex + 1
                If e.Row.Cells.Count > 2 Then
                    e.Row.Cells(4).Text = ValidateBLL.FormatMoneyWithComma(e.Row.Cells(4).Text)
                End If

            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub grvTransaksi_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grvTransaksi.RowDataBound
        Try
            If e.Row.RowType <> ListItemType.Footer And e.Row.RowType <> ListItemType.Header Then
                e.Row.Cells(0).Text = e.Row.RowIndex + 1
                If e.Row.Cells.Count > 2 Then
                    e.Row.Cells(6).Text = ValidateBLL.FormatMoneyWithComma(e.Row.Cells(6).Text)
                End If
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    'Protected Sub EditResume_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        clearKasMasukKasKeluar()
    '        Dim objgridviewrow As GridViewRow = CType(CType(sender, LinkButton).NamingContainer, GridViewRow)
    '        SetnGetRowEdit = objgridviewrow.RowIndex

    '        'LoadData ke KasMasuk dan Keluar
    '        Dim objResume As List(Of ResumeKasMasukKeluarWIC) = SetnGetResumeKasMasukKasKeluar

    '        'Ambil data buat cari PK Negara, Provinsi, KotaKab, dkk (ini variable yang bakal dpke berkali2)
    '        Dim i As Integer = 0 'buat iterasi
    '        Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetAll
    '        Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
    '        Dim objKelurahan As TList(Of MsKelurahan) = DataRepository.MsKelurahanProvider.GetAll
    '        Dim objSKelurahan As MsKelurahan 'Penampung hasil search kelurahan
    '        Dim objKecamatan As TList(Of MsKecamatan) = DataRepository.MsKecamatanProvider.GetAll
    '        Dim objSkecamatan As MsKecamatan 'Penampung hasil search kecamatan
    '        Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetAll
    '        Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi
    '        Dim objMapNegara As TList(Of MappingMsNegaraNCBSPPATK) = DataRepository.MappingMsNegaraNCBSPPATKProvider.GetAll
    '        Dim objSMapNegara As MappingMsNegaraNCBSPPATK 'Penampung hasil search mappingnegara yang pke picker
    '        Dim objNegara As TList(Of MsNegaraNCBS) = DataRepository.MsNegaraNCBSProvider.GetAll
    '        Dim objSnegara As MsNegaraNCBS 'Penampung hasil search negara yang pke picker
    '        Dim objPekerjaan As TList(Of MsPekerjaan) = DataRepository.MsPekerjaanProvider.GetAll
    '        Dim objSPekerjaan As MsPekerjaan 'Penampung hasil search pekerjaan
    '        Dim objBidangUsaha As TList(Of MsBidangUsaha) = DataRepository.MsBidangUsahaProvider.GetAll
    '        Dim objSBidangUsaha As MsBidangUsaha 'Penampung hasil search bidang usaha

    '        If objResume(SetnGetRowEdit).Kas = "Kas Masuk" Then

    '            MultiView1.ActiveViewIndex = 0
    '            Menu1.Items.Item(0).Selected = True

    '            'LoadData ke DetailKasMasuk
    '            Dim objDetailKasMasuk As List(Of WICDetailCashIn) = SetnGetgrvTRXKMDetilValutaAsing
    '            For Each obj As WICDetailCashInTransaction In objResume(SetnGetRowEdit).DetailTransactionCashIn
    '                Dim singleDetailKasMasuk As New WICDetailCashIn

    '                If obj.Asing_TotalKasMasukDalamRupiah.HasValue Then
    '                    singleDetailKasMasuk.Jumlah = (CDec(obj.Asing_TotalKasMasukDalamRupiah) / CDec(obj.Asing_KursTransaksi)).ToString
    '                    singleDetailKasMasuk.JumlahRp = obj.Asing_TotalKasMasukDalamRupiah.ToString
    '                    singleDetailKasMasuk.KursTransaksi = obj.Asing_KursTransaksi.ToString
    '                    Dim objKurs As MsCurrency = DataRepository.MsCurrencyProvider.GetByPk_MsCurrency_Id(obj.Asing_FK_MsCurrency_Id)
    '                    If Not IsNothing(objKurs) Then
    '                        singleDetailKasMasuk.MataUang = objKurs.MsCurrency_Code & " - " & objKurs.MsCurrency_Description
    '                    End If
    '                    objDetailKasMasuk.Add(singleDetailKasMasuk)
    '                End If


    '                If obj.KasMasuk.Value <> 0 Then
    '                    txtTRXKMDetilKasMasuk.Text = ValidateBLL.FormatMoneyWithComma(obj.KasMasuk)
    '                End If
    '            Next
    '            lblTRXKMDetilValutaAsingJumlahRp.Text = ValidateBLL.FormatMoneyWithComma(objResume(SetnGetRowEdit).TransactionNominal)
    '            grvTRXKMDetilValutaAsing.DataSource = objDetailKasMasuk
    '            grvTRXKMDetilValutaAsing.DataBind()



    '            'LoadData ke field Kas Masuk
    '            txtTRXKMTanggalTrx.Text = FormatDate(objResume(SetnGetRowEdit).TransactionCashIn.TanggalTransaksi)
    '            txtTRXKMNamaKantor.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.NamaKantorPJK.ToString)

    '            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashIn.FK_MsKotaKab_Id)
    '            If Not IsNothing(objSkotaKab) Then
    '                txtTRXKMKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
    '                hfTRXKMKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
    '            End If
    '            objSProvinsi = objProvinsi.Find(MsProvinceColumn.PK_MsPropincePPATK_ID, objResume(SetnGetRowEdit).TransactionCashIn.FK_MsProvince_Id)
    '            If Not objSProvinsi Is Nothing Then
    '                txtTRXKMProvinsi.Text = Safe(objSProvinsi.MsProvicePPATKName)
    '                hfTRXKMProvinsi.Value = Safe(objSProvinsi.PK_MsPropincePPATK_ID.ToString)
    '            End If
    '            txtTRXKMNoRekening.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.NomorRekening)
    '            txtTRXKMINDVGelar.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_Gelar)
    '            txtTRXKMINDVNamaLengkap.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_NamaLengkap)
    '            txtTRXKMINDVTempatLahir.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_TempatLahir)
    '            txtTRXKMINDVTanggalLahir.Text = FormatDate(objResume(SetnGetRowEdit).TransactionCashIn.INDV_TanggalLahir)
    '            'If txtTRXKMINDVTanggalLahir.Text = Date.MinValue Then
    '            '    txtTRXKMINDVTanggalLahir.Text = ""
    '            'End If
    '            If objResume(SetnGetRowEdit).TransactionCashIn.INDV_Kewarganegaraan <> "-" And objResume(SetnGetRowEdit).TransactionCashIn.INDV_Kewarganegaraan.Trim <> "" Then
    '                rblTRXKMINDVKewarganegaraan.SelectedValue = objResume(SetnGetRowEdit).TransactionCashIn.INDV_Kewarganegaraan
    '            End If
    '            If rblTRXKMINDVKewarganegaraan.SelectedIndex <> -1 Then
    '                i = 0
    '                For Each itemCboNegara As ListItem In cboTRXKMINDVNegara.Items
    '                    If objResume(SetnGetRowEdit).TransactionCashIn.INDV_FK_MsNegara_Id.ToString = itemCboNegara.Value.ToString Then
    '                        cboTRXKMINDVNegara.SelectedIndex = i
    '                        Exit For
    '                    End If
    '                    i = i + 1
    '                Next
    '            End If
    '            txtTRXKMINDVDOMNamaJalan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_DOM_NamaJalan)
    '            txtTRXKMINDVDOMRTRW.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_DOM_RTRW)
    '            objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objResume(SetnGetRowEdit).TransactionCashIn.INDV_DOM_FK_MsKelurahan_Id)
    '            If Not IsNothing(objSKelurahan) Then
    '                txtTRXKMINDVDOMKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
    '                hfTRXKMINDVDOMKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
    '            End If
    '            objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objResume(SetnGetRowEdit).TransactionCashIn.INDV_DOM_FK_MsKecamatan_Id)
    '            If Not IsNothing(objSkecamatan) Then
    '                txtTRXKMINDVDOMKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
    '                hfTRXKMINDVDOMKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
    '            End If
    '            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashIn.INDV_DOM_FK_MsKotaKab_Id)
    '            If Not IsNothing(objSkotaKab) Then
    '                txtTRXKMINDVDOMKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
    '                hfTRXKMINDVDOMKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
    '            End If
    '            txtTRXKMINDVDOMKodePos.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_DOM_KodePos)
    '            objSProvinsi = objProvinsi.Find(MsProvinceColumn.PK_MsPropincePPATK_ID, objResume(SetnGetRowEdit).TransactionCashIn.INDV_DOM_FK_MsProvince_Id)
    '            If Not IsNothing(objSProvinsi) Then
    '                txtTRXKMINDVDOMProvinsi.Text = Safe(objSProvinsi.MsProvicePPATKName)
    '                hfTRXKMINDVDOMProvinsi.Value = Safe(objSProvinsi.PK_MsPropincePPATK_ID)
    '            End If
    '            'chkTRXKMINDVCopyDOM.Checked = False
    '            txtTRXKMINDVIDNamaJalan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_ID_NamaJalan)
    '            txtTRXKMINDVIDRTRW.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_ID_RTRW)
    '            objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objResume(SetnGetRowEdit).TransactionCashIn.INDV_ID_FK_MsKelurahan_Id)
    '            If Not IsNothing(objSKelurahan) Then
    '                txtTRXKMINDVIDKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
    '                hfTRXKMINDVIDKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
    '            End If
    '            objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objResume(SetnGetRowEdit).TransactionCashIn.INDV_ID_FK_MsKecamatan_Id)
    '            If Not IsNothing(objSkecamatan) Then
    '                txtTRXKMINDVIDKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
    '                hfTRXKMINDVIDKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
    '            End If
    '            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashIn.INDV_ID_FK_MsKotaKab_Id)
    '            If Not IsNothing(objSkotaKab) Then
    '                txtTRXKMINDVIDKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
    '                hfTRXKMINDVIDKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
    '            End If
    '            txtTRXKMINDVIDKodePos.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_ID_KodePos)
    '            objSProvinsi = objProvinsi.Find(MsProvinceColumn.PK_MsPropincePPATK_ID, objResume(SetnGetRowEdit).TransactionCashIn.INDV_ID_FK_MsProvince_Id)
    '            If Not IsNothing(objSProvinsi) Then
    '                txtTRXKMINDVIDProvinsi.Text = Safe(objSProvinsi.MsProvicePPATKName)
    '                hfTRXKMINDVIDProvinsi.Value = Safe(objSProvinsi.PK_MsPropincePPATK_ID)
    '            End If
    '            txtTRXKMINDVNANamaJalan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_NA_NamaJalan)
    '            objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objResume(SetnGetRowEdit).TransactionCashIn.INDV_NA_FK_MsNegara_Id)
    '            If Not IsNothing(objSMapNegara) Then
    '                objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
    '                If Not IsNothing(objSnegara) Then
    '                    txtTRXKMINDVNANegara.Text = Safe(objSnegara.NamaNegara)
    '                    hfTRXKMINDVNANegara.Value = Safe(objSnegara.IDNegaraNCBS)
    '                End If
    '            End If
    '            objSProvinsi = objProvinsi.Find(MsProvinceColumn.PK_MsPropincePPATK_ID, objResume(SetnGetRowEdit).TransactionCashIn.INDV_NA_FK_MsProvince_Id)
    '            If Not IsNothing(objSProvinsi) Then
    '                txtTRXKMINDVNAProvinsi.Text = Safe(objSProvinsi.MsProvicePPATKName)
    '                hfTRXKMINDVNAProvinsi.Value = Safe(objSProvinsi.PK_MsPropincePPATK_ID)
    '            End If
    '            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashIn.INDV_NA_FK_MsKotaKab_Id)
    '            If Not IsNothing(objSkotaKab) Then
    '                txtTRXKMINDVNAKota.Text = Safe(objSkotaKab.NamaKotaKab)
    '                hfTRXKMINDVNAKota.Value = Safe(objSkotaKab.IDKotaKab)
    '            End If
    '            txtTRXKMINDVNAKodePos.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_NA_KodePos)
    '            If objResume(SetnGetRowEdit).TransactionCashIn.INDV_FK_MsIDType_Id.HasValue Then
    '                i = 0
    '                For Each itemJenisID As ListItem In cboTRXKMINDVJenisID.Items
    '                    If itemJenisID.Value = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_FK_MsIDType_Id.ToString) Then
    '                        cboTRXKMINDVJenisID.SelectedIndex = i
    '                        Exit For
    '                    End If
    '                    i = i + 1
    '                Next
    '            End If
    '            txtTRXKMINDVNomorID.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_NomorId)
    '            txtTRXKMINDVNPWP.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_NPWP)
    '            objSPekerjaan = objPekerjaan.Find(MsPekerjaanColumn.IDPekerjaan, objResume(SetnGetRowEdit).TransactionCashIn.INDV_FK_MsPekerjaan_Id)
    '            If Not IsNothing(objSPekerjaan) Then
    '                txtTRXKMINDVPekerjaan.Text = Safe(objSPekerjaan.NamaPekerjaan)
    '                hfTRXKMINDVPekerjaan.Value = Safe(objSPekerjaan.IDPekerjaan)
    '            End If
    '            txtTRXKMINDVJabatan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_Jabatan)
    '            txtTRXKMINDVPenghasilanRataRata.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_PenghasilanRataRata)
    '            txtTRXKMINDVTempatKerja.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_TempatBekerja)
    '            txtTRXKMINDVTujuanTrx.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_TujuanTransaksi)
    '            txtTRXKMINDVSumberDana.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_SumberDana)
    '            txtTRXKMINDVNamaBankLain.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_NamaBankLain)
    '            txtTRXKMINDVNoRekeningTujuan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_NomorRekeningTujuan)

    '            'CORP
    '            If objResume(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsBentukBadanUsaha_Id.HasValue Then
    '                i = 0
    '                For Each itemBentukBadangUsaha As ListItem In cboTRXKMCORPBentukBadanUsaha.Items
    '                    If itemBentukBadangUsaha.Value.ToString = objResume(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsBentukBadanUsaha_Id Then
    '                        cboTRXKMCORPBentukBadanUsaha.SelectedIndex = i
    '                        Exit For
    '                    End If
    '                    i = i + 1
    '                Next
    '            End If

    '            txtTRXKMCORPNama.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_Nama)
    '            objSBidangUsaha = objBidangUsaha.Find(MsBidangUsahaColumn.BidangUsahaId, objResume(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsBidangUsaha_Id)
    '            If Not IsNothing(objSBidangUsaha) Then
    '                txtTRXKMCORPBidangUsaha.Text = Safe(objSBidangUsaha.NamaBidangUsaha)
    '                hfTRXKMCORPBidangUsaha.Value = Safe(objSBidangUsaha.BidangUsahaId)
    '            End If
    '            If objResume(SetnGetRowEdit).TransactionCashIn.CORP_TipeAlamat.HasValue Then
    '                rblTRXKMCORPTipeAlamat.SelectedIndex = CInt(objResume(SetnGetRowEdit).TransactionCashIn.CORP_TipeAlamat)
    '            End If
    '            txtTRXKMCORPDLNamaJalan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_NamaJalan)
    '            txtTRXKMCORPDLRTRW.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_RTRW)
    '            objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objResume(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsKelurahan_Id)
    '            If Not IsNothing(objSKelurahan) Then
    '                txtTRXKMCORPDLKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
    '                hfTRXKMCORPDLKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
    '            End If
    '            objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objResume(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsKecamatan_Id)
    '            If Not IsNothing(objSkecamatan) Then
    '                txtTRXKMCORPDLKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
    '                hfTRXKMCORPDLKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
    '            End If
    '            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsKotaKab_Id)
    '            If Not IsNothing(objSkotaKab) Then
    '                txtTRXKMCORPDLKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
    '                hfTRXKMCORPDLKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
    '            End If
    '            txtTRXKMCORPDLKodePos.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_KodePos)
    '            objSProvinsi = objProvinsi.Find(MsProvinceColumn.PK_MsPropincePPATK_ID, objResume(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsProvince_Id)
    '            If Not IsNothing(objSProvinsi) Then
    '                txtTRXKMCORPDLProvinsi.Text = Safe(objSProvinsi.MsProvicePPATKName)
    '                hfTRXKMCORPDLProvinsi.Value = Safe(objSProvinsi.PK_MsPropincePPATK_ID)
    '            End If
    '            objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objResume(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsNegara_Id)
    '            If Not IsNothing(objSMapNegara) Then
    '                objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
    '                If Not IsNothing(objSnegara) Then
    '                    txtTRXKMCORPDLNegara.Text = Safe(objSnegara.NamaNegara)
    '                    hfTRXKMCORPDLNegara.Value = Safe(objSnegara.IDNegaraNCBS)
    '                End If
    '            End If
    '            txtTRXKMCORPLNNamaJalan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_LN_NamaJalan)
    '            objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objResume(SetnGetRowEdit).TransactionCashIn.CORP_LN_FK_MsNegara_Id)
    '            If Not IsNothing(objSMapNegara) Then
    '                objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
    '                If Not IsNothing(objSnegara) Then
    '                    txtTRXKMCORPLNNegara.Text = Safe(objSnegara.NamaNegara)
    '                    hfTRXKMCORPLNNegara.Value = Safe(objSnegara.IDNegaraNCBS)
    '                End If
    '            End If
    '            objSProvinsi = objProvinsi.Find(MsProvinceColumn.PK_MsPropincePPATK_ID, objResume(SetnGetRowEdit).TransactionCashIn.CORP_LN_FK_MsProvince_Id)
    '            If Not IsNothing(objSProvinsi) Then
    '                txtTRXKMCORPLNProvinsi.Text = Safe(objSProvinsi.MsProvicePPATKName)
    '                hfTRXKMCORPLNProvinsi.Value = Safe(objSProvinsi.PK_MsPropincePPATK_ID)
    '            End If
    '            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashIn.CORP_LN_MsKotaKab_Id)
    '            If Not IsNothing(objSkotaKab) Then
    '                txtTRXKMCORPLNKota.Text = Safe(objSkotaKab.NamaKotaKab)
    '                hfTRXKMCORPLNKota.Value = Safe(objSkotaKab.IDKotaKab)
    '            End If
    '            txtTRXKMCORPLNKodePos.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_LN_KodePos)
    '            txtTRXKMCORPNPWP.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_NPWP)
    '            txtTRXKMCORPTujuanTrx.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_TujuanTransaksi)
    '            txtTRXKMCORPSumberDana.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_SumberDana)
    '            txtTRXKMCORPNamaBankLain.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_NamaBankLain)
    '            txtTRXKMCORPNoRekeningTujuan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_NomorRekeningTujuan)


    '        ElseIf objResume(SetnGetRowEdit).Kas = "Kas Keluar" Then

    '            MultiView1.ActiveViewIndex = 1
    '            Menu1.Items.Item(1).Selected = True

    '            'LoadData ke DetailKasKeluar
    '            Dim objDetailKasKeluar As List(Of WICDetailCashIn) = SetnGetgrvDetilKasKeluar
    '            For Each obj As WICDetailCashOutTransaction In objResume(SetnGetRowEdit).DetailTranscationCashOut
    '                Dim singleDetailKasKeluar As New WICDetailCashIn

    '                If obj.Asing_TotalKasKeluarDalamRupiah.HasValue Then
    '                    singleDetailKasKeluar.Jumlah = (CDec(obj.Asing_TotalKasKeluarDalamRupiah) / CDec(obj.Asing_KursTransaksi)).ToString
    '                    singleDetailKasKeluar.JumlahRp = obj.Asing_TotalKasKeluarDalamRupiah.ToString
    '                    singleDetailKasKeluar.KursTransaksi = obj.Asing_KursTransaksi.ToString
    '                    Dim objKurs As MsCurrency = DataRepository.MsCurrencyProvider.GetByPk_MsCurrency_Id(obj.Asing_FK_MsCurrency_Id)
    '                    If Not IsNothing(objKurs) Then
    '                        singleDetailKasKeluar.MataUang = objKurs.MsCurrency_Code & " - " & objKurs.MsCurrency_Description
    '                    End If
    '                    objDetailKasKeluar.Add(singleDetailKasKeluar)
    '                End If



    '                If obj.KasKeluar.Value <> 0 Then
    '                    txtTRXKKDetailKasKeluar.Text = ValidateBLL.FormatMoneyWithComma(obj.KasKeluar)
    '                End If

    '            Next
    '            lblDetilKasKeluarJumlahRp.Text = ValidateBLL.FormatMoneyWithComma(objResume(SetnGetRowEdit).TransactionNominal)
    '            grvDetilKasKeluar.DataSource = objDetailKasKeluar
    '            grvDetilKasKeluar.DataBind()


    '            'LoadData ke field Kas Keluar
    '            txtTRXKKTanggalTransaksi.Text = FormatDate(objResume(SetnGetRowEdit).TransactionCashOut.TanggalTransaksi)
    '            txtTRXKKNamaKantor.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.NamaKantorPJK.ToString)

    '            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashOut.FK_MsKotaKab_Id)
    '            If Not IsNothing(objSkotaKab) Then
    '                txtTRXKKKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
    '                hfTRXKKKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
    '            End If
    '            objSProvinsi = objProvinsi.Find(MsProvinceColumn.PK_MsPropincePPATK_ID, objResume(SetnGetRowEdit).TransactionCashOut.FK_MsProvince_Id)
    '            If Not IsNothing(objSProvinsi) Then
    '                txtTRXKKProvinsi.Text = Safe(objSProvinsi.MsProvicePPATKName)
    '                hfTRXKKProvinsi.Value = Safe(objSProvinsi.PK_MsPropincePPATK_ID.ToString)
    '            End If
    '            txtTRXKKRekeningKK.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.NomorRekening)
    '            txtTRXKKINDVGelar.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_Gelar)
    '            txtTRXKKINDVNamaLengkap.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_NamaLengkap)
    '            txtTRXKKINDVTempatLahir.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_TempatLahir)
    '            txtTRXKKINDVTglLahir.Text = FormatDate(objResume(SetnGetRowEdit).TransactionCashOut.INDV_TanggalLahir)
    '            If txtTRXKKINDVTglLahir.Text = Date.MinValue Then
    '                txtTRXKKINDVTglLahir.Text = ""
    '            End If
    '            If objResume(SetnGetRowEdit).TransactionCashOut.INDV_Kewarganegaraan <> "-" And objResume(SetnGetRowEdit).TransactionCashOut.INDV_Kewarganegaraan.Trim <> "" Then
    '                rblTRXKKINDVKewarganegaraan.SelectedValue = objResume(SetnGetRowEdit).TransactionCashOut.INDV_Kewarganegaraan
    '            End If
    '            If rblTRXKKINDVKewarganegaraan.SelectedIndex <> -1 Then
    '                i = 0
    '                For Each itemCboNegara As ListItem In cboTRXKKINDVNegara.Items
    '                    If objResume(SetnGetRowEdit).TransactionCashOut.INDV_FK_MsNegara_Id.ToString = itemCboNegara.Value.ToString Then
    '                        cboTRXKKINDVNegara.SelectedIndex = i
    '                        Exit For
    '                    End If
    '                    i = i + 1
    '                Next
    '            End If
    '            txtTRXKKINDVDOMNamaJalan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_DOM_NamaJalan)
    '            txtTRXKKINDVDOMRTRW.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_DOM_RTRW)
    '            objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objResume(SetnGetRowEdit).TransactionCashOut.INDV_DOM_FK_MsKelurahan_Id)
    '            If Not IsNothing(objSKelurahan) Then
    '                txtTRXKKINDVDOMKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
    '                hfTRXKKINDVDOMKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
    '            End If
    '            objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objResume(SetnGetRowEdit).TransactionCashOut.INDV_DOM_FK_MsKecamatan_Id)
    '            If Not IsNothing(objSkecamatan) Then
    '                txtTRXKKINDVDOMKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
    '                hfTRXKKINDVDOMKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
    '            End If
    '            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashOut.INDV_DOM_FK_MsKotaKab_Id)
    '            If Not IsNothing(objSkotaKab) Then
    '                txtTRXKKINDVDOMKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
    '                hfTRXKKINDVDOMKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
    '            End If
    '            txtTRXKKINDVDOMKodePos.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_DOM_KodePos)
    '            objSProvinsi = objProvinsi.Find(MsProvinceColumn.PK_MsPropincePPATK_ID, objResume(SetnGetRowEdit).TransactionCashOut.INDV_DOM_FK_MsProvince_Id)
    '            If Not IsNothing(objSProvinsi) Then
    '                txtTRXKKINDVDOMProvinsi.Text = Safe(objSProvinsi.MsProvicePPATKName)
    '                hfTRXKKINDVDOMProvinsi.Value = Safe(objSProvinsi.PK_MsPropincePPATK_ID)
    '            End If
    '            'chkTRXKKINDVCopyDOM.Checked = False
    '            txtTRXKKINDVIDNamaJalan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_ID_NamaJalan)
    '            txtTRXKKINDVIDRTRW.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_ID_RTRW)
    '            objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objResume(SetnGetRowEdit).TransactionCashOut.INDV_ID_FK_MsKelurahan_Id)
    '            If Not IsNothing(objSKelurahan) Then
    '                txtTRXKKINDVIDKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
    '                hfTRXKKINDVIDKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
    '            End If
    '            objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objResume(SetnGetRowEdit).TransactionCashOut.INDV_ID_FK_MsKecamatan_Id)
    '            If Not IsNothing(objSkecamatan) Then
    '                txtTRXKKINDVIDKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
    '                hfTRXKKINDVIDKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
    '            End If
    '            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashOut.INDV_ID_FK_MsKotaKab_Id)
    '            If Not IsNothing(objSkotaKab) Then
    '                txtTRXKKINDVIDKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
    '                hfTRXKKINDVIDKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
    '            End If
    '            txtTRXKKINDVIDKodePos.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_ID_KodePos)
    '            objSProvinsi = objProvinsi.Find(MsProvinceColumn.PK_MsPropincePPATK_ID, objResume(SetnGetRowEdit).TransactionCashOut.INDV_ID_FK_MsProvince_Id)
    '            If Not IsNothing(objSProvinsi) Then
    '                txtTRXKKINDVIDProvinsi.Text = Safe(objSProvinsi.MsProvicePPATKName)
    '                hfTRXKKINDVIDProvinsi.Value = Safe(objSProvinsi.PK_MsPropincePPATK_ID)
    '            End If
    '            txtTRXKKINDVNANamaJalan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_NA_NamaJalan)
    '            objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objResume(SetnGetRowEdit).TransactionCashOut.INDV_NA_FK_MsNegara_Id)
    '            If Not IsNothing(objSMapNegara) Then
    '                objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
    '                If Not IsNothing(objSnegara) Then
    '                    txtTRXKKINDVNANegara.Text = Safe(objSnegara.NamaNegara)
    '                    hfTRXKKINDVNANegara.Value = Safe(objSnegara.IDNegaraNCBS)
    '                End If
    '            End If
    '            objSProvinsi = objProvinsi.Find(MsProvinceColumn.PK_MsPropincePPATK_ID, objResume(SetnGetRowEdit).TransactionCashOut.INDV_NA_FK_MsProvince_Id)
    '            If Not IsNothing(objSProvinsi) Then
    '                txtTRXKKINDVNAProvinsi.Text = Safe(objSProvinsi.MsProvicePPATKName)
    '                hfTRXKKINDVNAProvinsi.Value = Safe(objSProvinsi.PK_MsPropincePPATK_ID)
    '            End If
    '            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashOut.INDV_NA_FK_MsKotaKab_Id)
    '            If Not IsNothing(objSkotaKab) Then
    '                txtTRXKKINDVNAKota.Text = Safe(objSkotaKab.NamaKotaKab)
    '                hfTRXKKINDVNAKota.Value = Safe(objSkotaKab.IDKotaKab)
    '            End If
    '            txtTRXKKINDVNAKodePos.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_NA_KodePos)
    '            If objResume(SetnGetRowEdit).TransactionCashOut.INDV_FK_MsIDType_Id.HasValue Then
    '                i = 0
    '                For Each itemJenisID As ListItem In cboTRXKKINDVJenisID.Items
    '                    If itemJenisID.Value = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_FK_MsIDType_Id.ToString) Then
    '                        cboTRXKKINDVJenisID.SelectedIndex = i
    '                        Exit For
    '                    End If
    '                    i = i + 1
    '                Next
    '            End If
    '            txtTRXKKINDVNomorId.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_NomorId)
    '            txtTRXKKINDVNPWP.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_NPWP)
    '            objSPekerjaan = objPekerjaan.Find(MsPekerjaanColumn.IDPekerjaan, objResume(SetnGetRowEdit).TransactionCashOut.INDV_FK_MsPekerjaan_Id)
    '            If Not IsNothing(objSPekerjaan) Then
    '                txtTRXKKINDVPekerjaan.Text = Safe(objSPekerjaan.NamaPekerjaan)
    '                hfTRXKKINDVPekerjaan.Value = Safe(objSPekerjaan.IDPekerjaan)
    '            End If
    '            txtTRXKKINDVJabatan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_Jabatan)
    '            txtTRXKKINDVPenghasilanRataRata.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_PenghasilanRataRata)
    '            txtTRXKKINDVTempatKerja.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_TempatBekerja)
    '            txtTRXKKINDVTujuanTrx.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_TujuanTransaksi)
    '            txtTRXKKINDVSumberDana.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_SumberDana)
    '            txtTRXKKINDVNamaBankLain.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_NamaBankLain)
    '            txtTRXKKINDVNoRekTujuan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_NomorRekeningTujuan)

    '            'CORP
    '            If objResume(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsBentukBadanUsaha_Id.HasValue Then
    '                i = 0
    '                For Each itemBentukBadangUsaha As ListItem In cboTRXKKCORPBentukBadanUsaha.Items
    '                    If itemBentukBadangUsaha.Value.ToString = objResume(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsBentukBadanUsaha_Id Then
    '                        cboTRXKKCORPBentukBadanUsaha.SelectedIndex = i
    '                        Exit For
    '                    End If
    '                    i = i + 1
    '                Next
    '            End If

    '            txtTRXKKCORPNama.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_Nama)
    '            objSBidangUsaha = objBidangUsaha.Find(MsBidangUsahaColumn.BidangUsahaId, objResume(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsBidangUsaha_Id)
    '            If Not IsNothing(objSBidangUsaha) Then
    '                txtTRXKKCORPBidangUsaha.Text = Safe(objSBidangUsaha.NamaBidangUsaha)
    '                hfTRXKKCORPBidangUsaha.Value = Safe(objSBidangUsaha.BidangUsahaId)
    '            End If
    '            If objResume(SetnGetRowEdit).TransactionCashOut.CORP_TipeAlamat.HasValue Then
    '                rblTRXKKCORPTipeAlamat.SelectedIndex = CInt(objResume(SetnGetRowEdit).TransactionCashOut.CORP_TipeAlamat)
    '            End If
    '            txtTRXKKCORPDLNamaJalan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_NamaJalan)
    '            txtTRXKKCORPDLRTRW.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_RTRW)
    '            objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objResume(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsKelurahan_Id)
    '            If Not IsNothing(objSKelurahan) Then
    '                txtTRXKKCORPDLKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
    '                hfTRXKKCORPDLKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
    '            End If
    '            objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objResume(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsKecamatan_Id)
    '            If Not IsNothing(objSkecamatan) Then
    '                txtTRXKKCORPDLKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
    '                hfTRXKKCORPDLKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
    '            End If
    '            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsKotaKab_Id)
    '            If Not IsNothing(objSkotaKab) Then
    '                txtTRXKKCORPDLKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
    '                hfTRXKKCORPDLKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
    '            End If
    '            txtTRXKKCORPDLKodePos.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_KodePos)
    '            objSProvinsi = objProvinsi.Find(MsProvinceColumn.PK_MsPropincePPATK_ID, objResume(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsProvince_Id)
    '            If Not IsNothing(objSProvinsi) Then
    '                txtTRXKKCORPDLProvinsi.Text = Safe(objSProvinsi.MsProvicePPATKName)
    '                hfTRXKKCORPDLProvinsi.Value = Safe(objSProvinsi.PK_MsPropincePPATK_ID)
    '            End If
    '            objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objResume(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsNegara_Id)
    '            If Not IsNothing(objSMapNegara) Then
    '                objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
    '                If Not IsNothing(objSnegara) Then
    '                    txtTRXKKCORPDLNegara.Text = Safe(objSnegara.NamaNegara)
    '                    hfTRXKKCORPDLNegara.Value = Safe(objSnegara.IDNegaraNCBS)
    '                End If
    '            End If
    '            txtTRXKKCORPLNNamaJalan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_LN_NamaJalan)
    '            objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objResume(SetnGetRowEdit).TransactionCashOut.CORP_LN_FK_MsNegara_Id)
    '            If Not IsNothing(objSMapNegara) Then
    '                objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
    '                If Not IsNothing(objSnegara) Then
    '                    txtTRXKKCORPLNNegara.Text = Safe(objSnegara.NamaNegara)
    '                    hfTRXKKCORPLNNegara.Value = Safe(objSnegara.IDNegaraNCBS)
    '                End If
    '            End If
    '            objSProvinsi = objProvinsi.Find(MsProvinceColumn.PK_MsPropincePPATK_ID, objResume(SetnGetRowEdit).TransactionCashOut.CORP_LN_FK_MsProvince_Id)
    '            If Not IsNothing(objSProvinsi) Then
    '                txtTRXKKCORPLNProvinsi.Text = Safe(objSProvinsi.MsProvicePPATKName)
    '                hfTRXKKCORPLNProvinsi.Value = Safe(objSProvinsi.PK_MsPropincePPATK_ID)
    '            End If
    '            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashOut.CORP_LN_MsKotaKab_Id)
    '            If Not IsNothing(objSkotaKab) Then
    '                txtTRXKKCORPLNKota.Text = Safe(objSkotaKab.NamaKotaKab)
    '                hfTRXKKCORPLNKota.Value = Safe(objSkotaKab.IDKotaKab)
    '            End If
    '            txtTRXKKCORPLNKodePos.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_LN_KodePos)
    '            txtTRXKKCORPNPWP.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_NPWP)
    '            txtTRXKKCORPTujuanTrx.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_TujuanTransaksi)
    '            txtTRXKKCORPSumberDana.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_SumberDana)
    '            txtTRXKKCORPNamaBankLain.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_NamaBankLain)
    '            txtTRXKKCORPNoRekeningTujuan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_NomorRekeningTujuan)

    '        End If

    '        ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('IdTerlapor','Img1','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
    '        ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('Umum','searchimage2','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
    '        Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
    '    Catch ex As Exception
    '        'LogError(ex)
    '        Me.cvalPageErr.IsValid = False
    '        Me.cvalPageErr.ErrorMessage = ex.Message
    '    End Try

    'End Sub

    Protected Sub imgOKMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgOKMsg.Click
        Try
            Response.Redirect("WICView.aspx")
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Try
            If Request.Params("from") = "report" Then
                Response.Redirect("ReportHistoryView.aspx")
            End If
            Response.Redirect("WICView.aspx")
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub grvTRXKMDetilValutaAsing_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles grvTRXKMDetilValutaAsing.SelectedIndexChanged
        Try

        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub grvTRXKMDetilValutaAsing_SelectedIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles grvTRXKMDetilValutaAsing.SelectedIndexChanging
        Try

        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

#End Region

#Region "Similar Function..."

    Sub loadWICToField()
        Using objWic As WIC = DataRepository.WICProvider.GetByPK_WIC_Id(getWICPK)
            SafeDefaultValue = ""
            If Not IsNothing(objWic) Then

                'Ambil data buat cari PK Negara, Provinsi, KotaKab, dkk (ini variable yang bakal dpke berkali2)
                Dim i As Integer = 0 'buat iterasi
                Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetAll
                Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
                Dim objKelurahan As TList(Of MsKelurahan) = DataRepository.MsKelurahanProvider.GetAll
                Dim objSKelurahan As MsKelurahan 'Penampung hasil search kelurahan
                Dim objKecamatan As TList(Of MsKecamatan) = DataRepository.MsKecamatanProvider.GetAll
                Dim objSkecamatan As MsKecamatan 'Penampung hasil search kecamatan
                Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetAll
                Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi
                Dim objMapNegara As TList(Of MappingMsNegaraNCBSPPATK) = DataRepository.MappingMsNegaraNCBSPPATKProvider.GetAll
                Dim objSMapNegara As MappingMsNegaraNCBSPPATK 'Penampung hasil search mappingnegara yang pke picker
                Dim objNegara As TList(Of MsNegaraNCBS) = DataRepository.MsNegaraNCBSProvider.GetAll
                Dim objSnegara As MsNegaraNCBS 'Penampung hasil search negara yang pke picker
                Dim objPekerjaan As TList(Of MsPekerjaan) = DataRepository.MsPekerjaanProvider.GetAll
                Dim objSPekerjaan As MsPekerjaan 'Penampung hasil search pekerjaan
                Dim objBidangUsaha As TList(Of MsBidangUsaha) = DataRepository.MsBidangUsahaProvider.GetAll
                Dim objSBidangUsaha As MsBidangUsaha 'Penampung hasil search bidang usaha

                txtUmumPJKPelapor.Text = Safe(objWic.NamaPJKPelapor)
                txtTglLaporan.Text = FormatDate(objWic.TanggalLaporan)
                txtPejabatPelapor.Text = Safe(objWic.NamaPejabatPJKPelapor)
                If objWic.NoWICKoreksi <> "" Then
                    cboTipeLaporan.SelectedIndex = 1
                    txtNoLTKTKoreksi.Text = Safe(objWic.NoWICKoreksi)
                    tipeLaporanCheck()
                End If
                i = 0
                For Each listKepemilikan As ListItem In cboTerlaporKepemilikan.Items
                    If objWic.FK_MsKepemilikan_ID.ToString = listKepemilikan.Value Then
                        cboTerlaporKepemilikan.SelectedIndex = i
                        Exit For
                    End If
                    i = i + 1
                Next
                txtWICInformasiLainnya.Text = Safe(objWic.InformasiLainnya)
                txtTerlaporNoRekening.Text = Safe(objWic.NoRekening)
                SafeSelectedValue(rblTerlaporTipePelapor, objWic.TipeTerlapor.GetValueOrDefault(1))
                WICTipeTerlaporChange()
                txtTerlaporGelar.Text = Safe(objWic.INDV_Gelar)
                txtTerlaporNamaLengkap.Text = Safe(objWic.INDV_NamaLengkap)
                txtTerlaporTempatLahir.Text = Safe(objWic.INDV_TempatLahir)
                txtTerlaporTglLahir.Text = FormatDate(objWic.INDV_TanggalLahir)
                SafeSelectedValue(rblTerlaporKewarganegaraan, objWic.INDV_Kewarganegaraan)
                i = 0
                For Each listNegara As ListItem In cboTerlaporNegara.Items
                    If objWic.INDV_FK_MsNegara_Id.ToString = listNegara.Value Then
                        cboTerlaporNegara.SelectedIndex = i
                        Exit For
                    End If
                    i = i + 1
                Next
                txtTerlaporDOMNamaJalan.Text = Safe(objWic.INDV_DOM_NamaJalan)
                txtTerlaporDOMRTRW.Text = Safe(objWic.INDV_DOM_RTRW)
                objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objWic.INDV_DOM_FK_MsKelurahan_Id)
                If Not IsNothing(objSKelurahan) Then
                    txtTerlaporDOMKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
                    hfTerlaporDOMKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
                End If
                objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objWic.INDV_DOM_FK_MsKecamatan_Id)
                If Not IsNothing(objSkecamatan) Then
                    txtTerlaporDOMKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
                    hfTerlaporDOMKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objWic.INDV_DOM_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTerlaporDOMKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTerlaporDOMKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtTerlaporDOMKodePos.Text = Safe(objWic.INDV_DOM_KodePos)
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objWic.INDV_DOM_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTerlaporDOMProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTerlaporDOMProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                Using objNegaraDom As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objWic.INDV_DOM_FK_MsNegara_Id.GetValueOrDefault(0))
                    If Not objNegaraDom Is Nothing Then

                        txtTerlaporDOMNegara.Text = Safe(objNegaraDom.NamaNegara)
                        hfTerlaporDOMNegara.Value = Safe(objNegaraDom.IDNegara)
                    End If
                End Using
                txtTerlaporIDNamaJalan.Text = Safe(objWic.INDV_ID_NamaJalan)
                txtTerlaporIDRTRW.Text = Safe(objWic.INDV_ID_RTRW)
                objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objWic.INDV_ID_FK_MsKelurahan_Id)
                If Not IsNothing(objSKelurahan) Then
                    txtTerlaporIDKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
                    hfTerlaporIDKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
                End If
                objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objWic.INDV_ID_FK_MsKecamatan_Id)
                If Not IsNothing(objSkecamatan) Then
                    txtTerlaporIDKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
                    hfTerlaporIDKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objWic.INDV_ID_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTerlaporIDKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTerlaporIDKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtTerlaporIDKodePos.Text = Safe(objWic.INDV_ID_KodePos)
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objWic.INDV_ID_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTerlaporIDProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTerlaporIDProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objWic.INDV_ID_FK_MsNegara_Id.GetValueOrDefault(0))
                    If Not objNegaraRef Is Nothing Then

                        txtTerlaporIDNegara.Text = Safe(objNegaraRef.NamaNegara)
                        hfTerlaporIDNegara.Value = Safe(objNegaraRef.IDNegara)
                    End If
                End Using

                txtTerlaporNANamaJalan.Text = Safe(objWic.INDV_NA_NamaJalan)
              
                Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objWic.INDV_NA_FK_MsNegara_Id.GetValueOrDefault(0))
                    If Not objNegaraRef Is Nothing Then

                        txtTerlaporNANegara.Text = Safe(objNegaraRef.NamaNegara)
                        hfTerlaporNANegara.Value = Safe(objNegaraRef.IDNegara)
                    End If
                End Using
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objWic.INDV_NA_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTerlaporNAProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTerlaporNAProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objWic.INDV_NA_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTerlaporNAKota.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTerlaporIDKota.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtTerlaporNAKodePos.Text = Safe(objWic.INDV_NA_KodePos)
                i = 0
                For Each jenisDoc As ListItem In cboTerlaporJenisDocID.Items
                    If objWic.INDV_FK_MsIDType_Id.ToString = jenisDoc.Value Then
                        cboTerlaporJenisDocID.SelectedIndex = i
                        Exit For
                    End If
                    i = i + 1
                Next
                txtTerlaporNomorID.Text = Safe(objWic.INDV_NomorId)
                txtTerlaporNPWP.Text = Safe(objWic.INDV_NPWP)
                objSPekerjaan = objPekerjaan.Find(MsPekerjaanColumn.IDPekerjaan, objWic.INDV_FK_MsPekerjaan_Id)
                If Not IsNothing(objSPekerjaan) Then
                    txtTerlaporPekerjaan.Text = Safe(objSPekerjaan.NamaPekerjaan)
                    hfTerlaporPekerjaan.Value = Safe(objSPekerjaan.IDPekerjaan)
                End If
                txtTerlaporJabatan.Text = Safe(objWic.INDV_Jabatan)
                txtTerlaporPenghasilanRataRata.Text = Safe(objWic.INDV_PenghasilanRataRata)
                txtTerlaporTempatKerja.Text = Safe(objWic.INDV_TempatBekerja)
                i = 0
                For Each listBentukBadanUsaha As ListItem In cboTerlaporCORPBentukBadanUsaha.Items
                    If objWic.CORP_FK_MsBentukBadanUsaha_Id.ToString = listBentukBadanUsaha.Value Then
                        cboTerlaporCORPBentukBadanUsaha.SelectedIndex = i
                        Exit For
                    End If
                    i = i + 1
                Next
                txtTerlaporCORPNama.Text = Safe(objWic.CORP_Nama)
                objSBidangUsaha = objBidangUsaha.Find(MsBidangUsahaColumn.IdBidangUsaha, objWic.CORP_FK_MsBidangUsaha_Id)
                If Not IsNothing(objSBidangUsaha) Then
                    txtTerlaporCORPBidangUsaha.Text = Safe(objSBidangUsaha.NamaBidangUsaha)
                    hfTerlaporCORPBidangUsaha.Value = Safe(objSBidangUsaha.IdBidangUsaha)
                End If

                SafeNumIndex(rblTerlaporCORPTipeAlamat, objWic.CORP_TipeAlamat)

                txtTerlaporCORPDLNamaJalan.Text = Safe(objWic.CORP_NamaJalan)
                txtTerlaporCORPDLRTRW.Text = Safe(objWic.CORP_RTRW)

                objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objWic.CORP_FK_MsKelurahan_Id)
                If Not IsNothing(objSKelurahan) Then
                    txtTerlaporCORPDLKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
                    hfTerlaporCORPDLKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
                End If
                objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objWic.CORP_FK_MsKecamatan_Id)
                If Not IsNothing(objSkecamatan) Then
                    txtTerlaporCORPDLKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
                    hfTerlaporCORPDLKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objWic.CORP_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTerlaporCORPDLKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTerlaporCORPDLKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtTerlaporCORPDLKodePos.Text = Safe(objWic.CORP_KodePos)
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objWic.CORP_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTerlaporCORPDLProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTerlaporCORPDLProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                'objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objWic.CORP_FK_MsNegara_Id)
                'If Not IsNothing(objSMapNegara) Then
                '    objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                '    If Not IsNothing(objSnegara) Then
                '        txtTerlaporCORPDLNegara.Text = Safe(objSnegara.NamaNegara)
                '        hfTerlaporCORPDLNegara.Value = Safe(objSnegara.IDNegaraNCBS)
                '    End If
                'End If
                Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objWic.CORP_FK_MsNegara_Id.GetValueOrDefault(0))
                    If Not objNegaraRef Is Nothing Then

                        txtTerlaporCORPDLNegara.Text = Safe(objNegaraRef.NamaNegara)
                        hfTerlaporCORPDLNegara.Value = Safe(objNegaraRef.IDNegara)
                    End If
                End Using
                txtTerlaporCORPLNNamaJalan.Text = Safe(objWic.CORP_LN_NamaJalan)
                'objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objWic.CORP_LN_FK_MsNegara_Id)
                'If Not IsNothing(objSMapNegara) Then
                '    objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                '    If Not IsNothing(objSnegara) Then
                '        txtTerlaporCORPLNNegara.Text = Safe(objSnegara.NamaNegara)
                '        hfTerlaporCORPLNNegara.Value = Safe(objSnegara.IDNegaraNCBS)
                '    End If
                'End If
                Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objWic.CORP_LN_FK_MsNegara_Id.GetValueOrDefault(0))
                    If Not objNegaraRef Is Nothing Then

                        txtTerlaporCORPLNNegara.Text = Safe(objNegaraRef.NamaNegara)
                        hfTerlaporCORPLNNegara.Value = Safe(objNegaraRef.IDNegara)
                    End If
                End Using
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objWic.CORP_LN_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTerlaporCORPLNProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTerlaporCORPLNProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objWic.CORP_LN_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTerlaporCORPLNKota.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTerlaporCORPLNKota.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtTerlaporCORPLNKodePos.Text = Safe(objWic.CORP_LN_KodePos)
                txtTerlaporCORPNPWP.Text = Safe(objWic.CORP_NPWP)

            End If





        End Using
    End Sub

    Protected Sub EditResume_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            clearKasMasukKasKeluar()
            Dim objgridviewrow As GridViewRow = CType(CType(sender, LinkButton).NamingContainer, GridViewRow)
            SetnGetRowEdit = objgridviewrow.RowIndex

            'LoadData ke KasMasuk dan Keluar
            Dim objResume As List(Of ResumeKasMasukKeluarWIC) = SetnGetResumeKasMasukKasKeluar

            'Ambil data buat cari PK Negara, Provinsi, KotaKab, dkk (ini variable yang bakal dpke berkali2)
            Dim i As Integer = 0 'buat iterasi
            Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetAll
            Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
            Dim objKelurahan As TList(Of MsKelurahan) = DataRepository.MsKelurahanProvider.GetAll
            Dim objSKelurahan As MsKelurahan 'Penampung hasil search kelurahan
            Dim objKecamatan As TList(Of MsKecamatan) = DataRepository.MsKecamatanProvider.GetAll
            Dim objSkecamatan As MsKecamatan 'Penampung hasil search kecamatan
            Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetAll
            Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi
            Dim objMapNegara As TList(Of MappingMsNegaraNCBSPPATK) = DataRepository.MappingMsNegaraNCBSPPATKProvider.GetAll
            Dim objSMapNegara As MappingMsNegaraNCBSPPATK 'Penampung hasil search mappingnegara yang pke picker
            Dim objNegara As TList(Of MsNegaraNCBS) = DataRepository.MsNegaraNCBSProvider.GetAll
            Dim objSnegara As MsNegaraNCBS 'Penampung hasil search negara yang pke picker
            Dim objPekerjaan As TList(Of MsPekerjaan) = DataRepository.MsPekerjaanProvider.GetAll
            Dim objSPekerjaan As MsPekerjaan 'Penampung hasil search pekerjaan
            Dim objBidangUsaha As TList(Of MsBidangUsaha) = DataRepository.MsBidangUsahaProvider.GetAll
            Dim objSBidangUsaha As MsBidangUsaha 'Penampung hasil search bidang usaha

            If objResume(SetnGetRowEdit).Kas = "Kas Masuk" Then

                MultiView1.ActiveViewIndex = 0
                Menu1.Items.Item(0).Selected = True

                'LoadData ke DetailKasMasuk
                Dim detailkasMasuk As Decimal = 0
                Dim objDetailKasMasuk As List(Of WICDetailCashInDetail) = SetnGetgrvTRXKMDetilValutaAsing
                For Each obj As WICDetailCashInTransaction In objResume(SetnGetRowEdit).DetailTransactionCashIn
                    Dim singleDetailKasMasuk As New WICDetailCashInDetail

                    If obj.Asing_TotalKasMasukDalamRupiah.HasValue Then

                        If obj.Asing_TotalKasMasukDalamRupiah.Value > 0 Then

                            If obj.Asing_KursTransaksi.HasValue Then
                                If obj.Asing_KursTransaksi.Value > 0 Then
                                    Try
                                        singleDetailKasMasuk.Jumlah = (CDec(obj.Asing_TotalKasMasukDalamRupiah.GetValueOrDefault) / CDec(obj.Asing_KursTransaksi.GetValueOrDefault)).ToString
                                    Catch ex As Exception
                                        singleDetailKasMasuk.Jumlah = obj.Asing_TotalKasMasukDalamRupiah.GetValueOrDefault
                                    End Try
                                Else
                                    singleDetailKasMasuk.Jumlah = obj.Asing_TotalKasMasukDalamRupiah.GetValueOrDefault
                                End If
                            End If

                            singleDetailKasMasuk.JumlahRp = obj.Asing_TotalKasMasukDalamRupiah.GetValueOrDefault
                            singleDetailKasMasuk.KursTransaksi = obj.Asing_KursTransaksi.GetValueOrDefault

                            Dim ID As Decimal = 0
                            If obj.Asing_FK_MsCurrency_Id.HasValue Then
                                ID = obj.Asing_FK_MsCurrency_Id
                            Else 'Jika Nothing pake IDR
                                ID = 12
                            End If

                            Dim objKurs As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(ID)
                            If Not IsNothing(objKurs) Then
                                singleDetailKasMasuk.MataUang = objKurs.Code & " - " & objKurs.Name
                            End If

                            objDetailKasMasuk.Add(singleDetailKasMasuk)
                        Else
                            If obj.KasMasuk.HasValue Then
                                singleDetailKasMasuk.Jumlah = 0
                                singleDetailKasMasuk.JumlahRp = obj.KasMasuk.GetValueOrDefault
                                singleDetailKasMasuk.KursTransaksi = 0
                                Dim objKurs As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(12) ' Indonesia IDR
                                If Not IsNothing(objKurs) Then
                                    singleDetailKasMasuk.MataUang = objKurs.Code & " - " & objKurs.Name
                                End If
                                detailkasMasuk += obj.KasMasuk.GetValueOrDefault
                                objDetailKasMasuk.Add(singleDetailKasMasuk)
                            End If
                        End If

                    ElseIf obj.KasMasuk.HasValue Then
                        singleDetailKasMasuk.Jumlah = 0
                        singleDetailKasMasuk.JumlahRp = obj.KasMasuk.GetValueOrDefault
                        singleDetailKasMasuk.KursTransaksi = 0
                        Dim objKurs As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(12) ' Indonesia IDR
                        If Not IsNothing(objKurs) Then
                            singleDetailKasMasuk.MataUang = objKurs.Code & " - " & objKurs.Name
                        End If
                        detailkasMasuk += obj.KasMasuk.GetValueOrDefault
                        objDetailKasMasuk.Add(singleDetailKasMasuk)
                    End If

                Next
                lblTRXKMDetilValutaAsingJumlahRp.Text = ValidateBLL.FormatMoneyWithComma(objResume(SetnGetRowEdit).TransactionNominal)
                grvTRXKMDetilValutaAsing.DataSource = objDetailKasMasuk
                grvTRXKMDetilValutaAsing.DataBind()



                'LoadData ke field Kas Masuk
                txtTRXKMTanggalTrx.Text = FormatDate(objResume(SetnGetRowEdit).TransactionCashIn.TanggalTransaksi)
                txtTRXKMNamaKantor.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.NamaKantorPJK.ToString)

                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashIn.FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTRXKMKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTRXKMKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashIn.FK_MsProvince_Id)
                If Not objSProvinsi Is Nothing Then
                    txtTRXKMProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTRXKMProvinsi.Value = Safe(objSProvinsi.IdProvince.ToString)
                End If
                txtTRXKMNoRekening.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.NomorRekening)
                SafeSelectedValue(rblTRXKMTipePelapor, objResume(SetnGetRowEdit).TransactionCashIn.TipeTerlapor)

                txtTRXKMINDVGelar.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_Gelar)
                txtTRXKMINDVNamaLengkap.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_NamaLengkap)
                txtTRXKMINDVTempatLahir.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_TempatLahir)
                txtTRXKMINDVTanggalLahir.Text = FormatDate(objResume(SetnGetRowEdit).TransactionCashIn.INDV_TanggalLahir)

                If objResume(SetnGetRowEdit).TransactionCashIn.INDV_Kewarganegaraan <> "-" And objResume(SetnGetRowEdit).TransactionCashIn.INDV_Kewarganegaraan.Trim <> "" Then
                    SafeSelectedValue(rblTRXKMINDVKewarganegaraan, objResume(SetnGetRowEdit).TransactionCashIn.INDV_Kewarganegaraan)
                End If
                If rblTRXKMINDVKewarganegaraan.SelectedValue > 0 Then
                    i = 0
                    For Each itemCboNegara As ListItem In cboTRXKMINDVNegara.Items
                        If objResume(SetnGetRowEdit).TransactionCashIn.INDV_FK_MsNegara_Id.ToString = itemCboNegara.Value.ToString Then
                            cboTRXKMINDVNegara.SelectedIndex = i
                            Exit For
                        End If
                        i = i + 1
                    Next
                End If
                txtTRXKMINDVDOMNamaJalan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_DOM_NamaJalan)
                txtTRXKMINDVDOMRTRW.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_DOM_RTRW)
                objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objResume(SetnGetRowEdit).TransactionCashIn.INDV_DOM_FK_MsKelurahan_Id)
                If Not IsNothing(objSKelurahan) Then
                    txtTRXKMINDVDOMKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
                    hfTRXKMINDVDOMKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
                End If
                objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objResume(SetnGetRowEdit).TransactionCashIn.INDV_DOM_FK_MsKecamatan_Id)
                If Not IsNothing(objSkecamatan) Then
                    txtTRXKMINDVDOMKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
                    hfTRXKMINDVDOMKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashIn.INDV_DOM_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTRXKMINDVDOMKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTRXKMINDVDOMKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtTRXKMINDVDOMKodePos.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_DOM_KodePos)
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashIn.INDV_DOM_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTRXKMINDVDOMProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTRXKMINDVDOMProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If

                Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objResume(SetnGetRowEdit).TransactionCashIn.INDV_DOM_FK_MsNegara_Id.GetValueOrDefault(0))
                    If Not objNegaraRef Is Nothing Then

                        txtTRXKMINDVDOMNegara.Text = Safe(objNegaraRef.NamaNegara)
                        hfTRXKMINDVDOMNegara.Value = Safe(objNegaraRef.IDNegara)
                    End If
                End Using

                'chkTRXKMINDVCopyDOM.Checked = False
                txtTRXKMINDVIDNamaJalan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_ID_NamaJalan)
                txtTRXKMINDVIDRTRW.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_ID_RTRW)
                objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objResume(SetnGetRowEdit).TransactionCashIn.INDV_ID_FK_MsKelurahan_Id)
                If Not IsNothing(objSKelurahan) Then
                    txtTRXKMINDVIDKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
                    hfTRXKMINDVIDKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
                End If
                objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objResume(SetnGetRowEdit).TransactionCashIn.INDV_ID_FK_MsKecamatan_Id)
                If Not IsNothing(objSkecamatan) Then
                    txtTRXKMINDVIDKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
                    hfTRXKMINDVIDKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashIn.INDV_ID_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTRXKMINDVIDKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTRXKMINDVIDKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtTRXKMINDVIDKodePos.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_ID_KodePos)
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashIn.INDV_ID_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTRXKMINDVIDProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTRXKMINDVIDProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                txtTRXKMINDVNANamaJalan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_NA_NamaJalan)

                'objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objResume(SetnGetRowEdit).TransactionCashIn.INDV_NA_FK_MsNegara_Id)
                'If Not IsNothing(objSMapNegara) Then
                '    objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                '    If Not IsNothing(objSnegara) Then
                '        txtTRXKMINDVNANegara.Text = Safe(objSnegara.NamaNegara)
                '        hfTRXKMINDVNANegara.Value = Safe(objSnegara.IDNegaraNCBS)
                '    End If
                'End If

                Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objResume(SetnGetRowEdit).TransactionCashIn.INDV_NA_FK_MsNegara_Id.GetValueOrDefault(0))
                    If Not objNegaraRef Is Nothing Then

                        txtTRXKMINDVNANegara.Text = Safe(objNegaraRef.NamaNegara)
                        hfTRXKMINDVNANegara.Value = Safe(objNegaraRef.IDNegara)
                    End If
                End Using


                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashIn.INDV_NA_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTRXKMINDVNAProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTRXKMINDVNAProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashIn.INDV_NA_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTRXKMINDVNAKota.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTRXKMINDVNAKota.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtTRXKMINDVNAKodePos.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_NA_KodePos)
                If objResume(SetnGetRowEdit).TransactionCashIn.INDV_FK_MsIDType_Id.GetValueOrDefault <> Nothing Then
                    i = 0
                    For Each itemJenisID As ListItem In cboTRXKMINDVJenisID.Items
                        If itemJenisID.Value = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_FK_MsIDType_Id.ToString) Then
                            cboTRXKMINDVJenisID.SelectedIndex = i
                            Exit For
                        End If
                        i = i + 1
                    Next
                End If
                txtTRXKMINDVNomorID.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_NomorId)
                txtTRXKMINDVNPWP.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_NPWP)
                objSPekerjaan = objPekerjaan.Find(MsPekerjaanColumn.IDPekerjaan, objResume(SetnGetRowEdit).TransactionCashIn.INDV_FK_MsPekerjaan_Id.GetValueOrDefault)
                If Not IsNothing(objSPekerjaan) Then
                    txtTRXKMINDVPekerjaan.Text = Safe(objSPekerjaan.NamaPekerjaan)
                    hfTRXKMINDVPekerjaan.Value = Safe(objSPekerjaan.IDPekerjaan)
                End If
                txtTRXKMINDVJabatan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_Jabatan)
                txtTRXKMINDVPenghasilanRataRata.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_PenghasilanRataRata)
                txtTRXKMINDVTempatKerja.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_TempatBekerja)
                txtTRXKMINDVTujuanTrx.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_TujuanTransaksi)
                txtTRXKMINDVSumberDana.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_SumberDana)
                txtTRXKMINDVNamaBankLain.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_NamaBankLain)
                txtTRXKMINDVNoRekeningTujuan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.INDV_NomorRekeningTujuan)

                'CORP
                If objResume(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault <> Nothing Then
                    i = 0
                    For Each itemBentukBadangUsaha As ListItem In cboTRXKMCORPBentukBadanUsaha.Items
                        If itemBentukBadangUsaha.Value.ToString = objResume(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsBentukBadanUsaha_Id Then
                            cboTRXKMCORPBentukBadanUsaha.SelectedIndex = i
                            Exit For
                        End If
                        i = i + 1
                    Next
                End If

                txtTRXKMCORPNama.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_Nama)
                objSBidangUsaha = objBidangUsaha.Find(MsBidangUsahaColumn.IdBidangUsaha, objResume(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsBidangUsaha_Id)
                If Not IsNothing(objSBidangUsaha) Then
                    txtTRXKMCORPBidangUsaha.Text = Safe(objSBidangUsaha.NamaBidangUsaha)
                    hfTRXKMCORPBidangUsaha.Value = Safe(objSBidangUsaha.IdBidangUsaha)
                End If
                If objResume(SetnGetRowEdit).TransactionCashIn.CORP_TipeAlamat.HasValue Then
                    SafeNumIndex(rblTRXKMCORPTipeAlamat, objResume(SetnGetRowEdit).TransactionCashIn.CORP_TipeAlamat)
                End If
                txtTRXKMCORPDLNamaJalan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_NamaJalan)
                txtTRXKMCORPDLRTRW.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_RTRW)
                objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objResume(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsKelurahan_Id)
                If Not IsNothing(objSKelurahan) Then
                    txtTRXKMCORPDLKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
                    hfTRXKMCORPDLKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
                End If
                objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objResume(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsKecamatan_Id)
                If Not IsNothing(objSkecamatan) Then
                    txtTRXKMCORPDLKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
                    hfTRXKMCORPDLKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTRXKMCORPDLKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTRXKMCORPDLKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtTRXKMCORPDLKodePos.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_KodePos)
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTRXKMCORPDLProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTRXKMCORPDLProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                'objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objResume(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsNegara_Id)
                'If Not IsNothing(objSMapNegara) Then
                '    objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                '    If Not IsNothing(objSnegara) Then
                '        txtTRXKMCORPDLNegara.Text = Safe(objSnegara.NamaNegara)
                '        hfTRXKMCORPDLNegara.Value = Safe(objSnegara.IDNegaraNCBS)
                '    End If
                'End If

                Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objResume(SetnGetRowEdit).TransactionCashIn.CORP_FK_MsNegara_Id.GetValueOrDefault(0))
                    If Not objNegaraRef Is Nothing Then

                        txtTRXKMCORPDLNegara.Text = Safe(objNegaraRef.NamaNegara)
                        hfTRXKMCORPDLNegara.Value = Safe(objNegaraRef.IDNegara)
                    End If
                End Using
                txtTRXKMCORPLNNamaJalan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_LN_NamaJalan)
                'objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objResume(SetnGetRowEdit).TransactionCashIn.CORP_LN_FK_MsNegara_Id)
                'If Not IsNothing(objSMapNegara) Then
                '    objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                '    If Not IsNothing(objSnegara) Then
                '        txtTRXKMCORPLNNegara.Text = Safe(objSnegara.NamaNegara)
                '        hfTRXKMCORPLNNegara.Value = Safe(objSnegara.IDNegaraNCBS)
                '    End If
                'End If


                Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objResume(SetnGetRowEdit).TransactionCashIn.CORP_LN_FK_MsNegara_Id.GetValueOrDefault(0))
                    If Not objNegaraRef Is Nothing Then

                        txtTRXKMCORPLNNegara.Text = Safe(objNegaraRef.NamaNegara)
                        hfTRXKMCORPLNNegara.Value = Safe(objNegaraRef.IDNegara)
                    End If
                End Using
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashIn.CORP_LN_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTRXKMCORPLNProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTRXKMCORPLNProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashIn.CORP_LN_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTRXKMCORPLNKota.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTRXKMCORPLNKota.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtTRXKMCORPLNKodePos.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_LN_KodePos)
                txtTRXKMCORPNPWP.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_NPWP)
                txtTRXKMCORPTujuanTrx.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_TujuanTransaksi)
                txtTRXKMCORPSumberDana.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_SumberDana)
                txtTRXKMCORPNamaBankLain.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_NamaBankLain)
                txtTRXKMCORPNoRekeningTujuan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashIn.CORP_NomorRekeningTujuan)

                'imgAdd.Visible = False
                'imgClearAll.Visible = False
                'imgSaveEdit.Visible = True
                'imgCancelEdit.Visible = True

            ElseIf objResume(SetnGetRowEdit).Kas = "Kas Keluar" Then

                MultiView1.ActiveViewIndex = 1
                Menu1.Items.Item(1).Selected = True

                'LoadData ke DetailKasKeluar
                Dim detailkaskeluar As Decimal = 0
                Dim objDetailKasKeluar As List(Of WICDetailCashInDetail) = SetnGetgrvDetilKasKeluar
                For Each obj As WICDetailCashOutTransaction In objResume(SetnGetRowEdit).DetailTranscationCashOut
                    Dim singleDetailKasKeluar As New WICDetailCashInDetail

                    If obj.Asing_TotalKasKeluarDalamRupiah.HasValue Then

                        If obj.Asing_TotalKasKeluarDalamRupiah.Value > 0 Then

                            If obj.Asing_KursTransaksi.HasValue Then
                                If obj.Asing_KursTransaksi.Value > 0 Then
                                    Try
                                        singleDetailKasKeluar.Jumlah = (CDec(obj.Asing_TotalKasKeluarDalamRupiah.GetValueOrDefault) / CDec(obj.Asing_KursTransaksi.GetValueOrDefault)).ToString
                                    Catch ex As Exception
                                        singleDetailKasKeluar.Jumlah = obj.Asing_TotalKasKeluarDalamRupiah.GetValueOrDefault
                                    End Try

                                Else
                                    singleDetailKasKeluar.Jumlah = obj.Asing_TotalKasKeluarDalamRupiah.GetValueOrDefault
                                End If
                            End If

                            singleDetailKasKeluar.JumlahRp = obj.Asing_TotalKasKeluarDalamRupiah.GetValueOrDefault
                            singleDetailKasKeluar.KursTransaksi = obj.Asing_KursTransaksi.GetValueOrDefault

                            Dim ID As Decimal = 0
                            If obj.Asing_FK_MsCurrency_Id.HasValue Then
                                ID = obj.Asing_FK_MsCurrency_Id
                            Else 'Jika Nothing pake IDR
                                ID = 12
                            End If

                            Dim objKurs As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(ID)
                            If Not IsNothing(objKurs) Then
                                singleDetailKasKeluar.MataUang = objKurs.Code & " - " & objKurs.Name
                            End If

                            objDetailKasKeluar.Add(singleDetailKasKeluar)
                        Else
                            If obj.KasKeluar.HasValue Then
                                singleDetailKasKeluar.Jumlah = 0
                                singleDetailKasKeluar.JumlahRp = obj.KasKeluar.GetValueOrDefault
                                singleDetailKasKeluar.KursTransaksi = 0
                                Dim objKurs As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(12) ' Indonesia IDR
                                If Not IsNothing(objKurs) Then
                                    singleDetailKasKeluar.MataUang = objKurs.Code & " - " & objKurs.Name
                                End If
                                detailkaskeluar += obj.KasKeluar.GetValueOrDefault
                                objDetailKasKeluar.Add(singleDetailKasKeluar)
                            End If
                        End If

                    ElseIf obj.KasKeluar.HasValue Then
                        singleDetailKasKeluar.Jumlah = 0
                        singleDetailKasKeluar.JumlahRp = obj.KasKeluar.GetValueOrDefault
                        singleDetailKasKeluar.KursTransaksi = 0
                        Dim objKurs As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(12) ' Indonesia IDR
                        If Not IsNothing(objKurs) Then
                            singleDetailKasKeluar.MataUang = objKurs.Code & " - " & objKurs.Name
                        End If
                        detailkaskeluar += obj.KasKeluar.GetValueOrDefault
                        objDetailKasKeluar.Add(singleDetailKasKeluar)
                    End If

                Next
                lblDetilKasKeluarJumlahRp.Text = ValidateBLL.FormatMoneyWithComma(objResume(SetnGetRowEdit).TransactionNominal)
                grvDetilKasKeluar.DataSource = objDetailKasKeluar
                grvDetilKasKeluar.DataBind()


                'LoadData ke field Kas Keluar
                txtTRXKKTanggalTransaksi.Text = FormatDate(objResume(SetnGetRowEdit).TransactionCashOut.TanggalTransaksi)
                txtTRXKKNamaKantor.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.NamaKantorPJK.ToString)

                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashOut.FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTRXKKKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTRXKKKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashOut.FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTRXKKProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTRXKKProvinsi.Value = Safe(objSProvinsi.IdProvince.ToString)
                End If
                txtTRXKKRekeningKK.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.NomorRekening)
                SafeSelectedValue(rblTRXKKTipePelapor, objResume(SetnGetRowEdit).TransactionCashOut.TipeTerlapor)
                'Individu
                txtTRXKKINDVGelar.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_Gelar)
                txtTRXKKINDVNamaLengkap.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_NamaLengkap)
                txtTRXKKINDVTempatLahir.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_TempatLahir)
                txtTRXKKINDVTglLahir.Text = FormatDate(objResume(SetnGetRowEdit).TransactionCashOut.INDV_TanggalLahir)

                If objResume(SetnGetRowEdit).TransactionCashOut.INDV_Kewarganegaraan <> "-" And objResume(SetnGetRowEdit).TransactionCashOut.INDV_Kewarganegaraan.Trim <> "" Then
                    SafeSelectedValue(rblTRXKKINDVKewarganegaraan, objResume(SetnGetRowEdit).TransactionCashOut.INDV_Kewarganegaraan)
                End If
                If rblTRXKKINDVKewarganegaraan.SelectedIndex <> -1 Then
                    i = 0
                    For Each itemCboNegara As ListItem In cboTRXKKINDVNegara.Items
                        If objResume(SetnGetRowEdit).TransactionCashOut.INDV_FK_MsNegara_Id.ToString = itemCboNegara.Value.ToString Then
                            cboTRXKKINDVNegara.SelectedIndex = i
                            Exit For
                        End If
                        i = i + 1
                    Next
                End If
                If objResume(SetnGetRowEdit).TransactionCashOut.CORP_TipeAlamat.GetValueOrDefault <> Nothing Then
                    rblTRXKKCORPTipeAlamat.SelectedIndex = CInt(objResume(SetnGetRowEdit).TransactionCashOut.CORP_TipeAlamat)
                End If
                txtTRXKKINDVDOMNamaJalan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_DOM_NamaJalan)
                txtTRXKKINDVDOMRTRW.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_DOM_RTRW)
                objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objResume(SetnGetRowEdit).TransactionCashOut.INDV_DOM_FK_MsKelurahan_Id)
                If Not IsNothing(objSKelurahan) Then
                    txtTRXKKINDVDOMKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
                    hfTRXKKINDVDOMKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
                End If
                objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objResume(SetnGetRowEdit).TransactionCashOut.INDV_DOM_FK_MsKecamatan_Id)
                If Not IsNothing(objSkecamatan) Then
                    txtTRXKKINDVDOMKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
                    hfTRXKKINDVDOMKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashOut.INDV_DOM_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTRXKKINDVDOMKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTRXKKINDVDOMKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtTRXKKINDVDOMKodePos.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_DOM_KodePos)
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashOut.INDV_DOM_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTRXKKINDVDOMProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTRXKKINDVDOMProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                'chkTRXKKINDVCopyDOM.Checked = False
                txtTRXKKINDVIDNamaJalan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_ID_NamaJalan)
                txtTRXKKINDVIDRTRW.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_ID_RTRW)
                objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objResume(SetnGetRowEdit).TransactionCashOut.INDV_ID_FK_MsKelurahan_Id)
                If Not IsNothing(objSKelurahan) Then
                    txtTRXKKINDVIDKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
                    hfTRXKKINDVIDKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
                End If
                objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objResume(SetnGetRowEdit).TransactionCashOut.INDV_ID_FK_MsKecamatan_Id)
                If Not IsNothing(objSkecamatan) Then
                    txtTRXKKINDVIDKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
                    hfTRXKKINDVIDKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashOut.INDV_ID_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTRXKKINDVIDKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTRXKKINDVIDKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtTRXKKINDVIDKodePos.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_ID_KodePos)
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashOut.INDV_ID_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTRXKKINDVIDProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTRXKKINDVIDProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                txtTRXKKINDVNANamaJalan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_NA_NamaJalan)
                'objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objResume(SetnGetRowEdit).TransactionCashOut.INDV_NA_FK_MsNegara_Id)
                'If Not IsNothing(objSMapNegara) Then
                '    objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                '    If Not IsNothing(objSnegara) Then
                '        txtTRXKKINDVNANegara.Text = Safe(objSnegara.NamaNegara)
                '        hfTRXKKINDVNANegara.Value = Safe(objSnegara.IDNegaraNCBS)
                '    End If
                'End If

                Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objResume(SetnGetRowEdit).TransactionCashOut.INDV_NA_FK_MsNegara_Id.GetValueOrDefault(0))
                    If Not objNegaraRef Is Nothing Then

                        txtTRXKKINDVNANegara.Text = Safe(objNegaraRef.NamaNegara)
                        hfTRXKKINDVNANegara.Value = Safe(objNegaraRef.IDNegara)
                    End If
                End Using
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashOut.INDV_NA_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTRXKKINDVNAProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTRXKKINDVNAProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashOut.INDV_NA_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTRXKKINDVNAKota.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTRXKKINDVNAKota.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtTRXKKINDVNAKodePos.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_NA_KodePos)
                If objResume(SetnGetRowEdit).TransactionCashOut.INDV_FK_MsIDType_Id.GetValueOrDefault <> Nothing Then
                    i = 0
                    For Each itemJenisID As ListItem In cboTRXKKINDVJenisID.Items
                        If itemJenisID.Value = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_FK_MsIDType_Id.ToString) Then
                            cboTRXKKINDVJenisID.SelectedIndex = i
                            Exit For
                        End If
                        i = i + 1
                    Next
                End If
                txtTRXKKINDVNomorId.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_NomorId)
                txtTRXKKINDVNPWP.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_NPWP)
                objSPekerjaan = objPekerjaan.Find(MsPekerjaanColumn.IDPekerjaan, objResume(SetnGetRowEdit).TransactionCashOut.INDV_FK_MsPekerjaan_Id.GetValueOrDefault)
                If Not IsNothing(objSPekerjaan) Then
                    txtTRXKKINDVPekerjaan.Text = Safe(objSPekerjaan.NamaPekerjaan)
                    hfTRXKKINDVPekerjaan.Value = Safe(objSPekerjaan.IDPekerjaan)
                End If
                txtTRXKKINDVJabatan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_Jabatan)
                txtTRXKKINDVPenghasilanRataRata.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_PenghasilanRataRata)
                txtTRXKKINDVTempatKerja.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_TempatBekerja)
                txtTRXKKINDVTujuanTrx.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_TujuanTransaksi)
                txtTRXKKINDVSumberDana.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_SumberDana)
                txtTRXKKINDVNamaBankLain.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_NamaBankLain)
                txtTRXKKINDVNoRekTujuan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.INDV_NomorRekeningTujuan)

                'CORP
                If objResume(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault <> Nothing Then
                    i = 0
                    For Each itemBentukBadangUsaha As ListItem In cboTRXKKCORPBentukBadanUsaha.Items
                        If itemBentukBadangUsaha.Value.ToString = objResume(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsBentukBadanUsaha_Id Then
                            cboTRXKKCORPBentukBadanUsaha.SelectedIndex = i
                            Exit For
                        End If
                        i = i + 1
                    Next
                End If

                txtTRXKKCORPNama.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_Nama)
                objSBidangUsaha = objBidangUsaha.Find(MsBidangUsahaColumn.IdBidangUsaha, objResume(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsBidangUsaha_Id)
                If Not IsNothing(objSBidangUsaha) Then
                    txtTRXKKCORPBidangUsaha.Text = Safe(objSBidangUsaha.NamaBidangUsaha)
                    hfTRXKKCORPBidangUsaha.Value = Safe(objSBidangUsaha.IdBidangUsaha)
                End If
                If objResume(SetnGetRowEdit).TransactionCashOut.CORP_TipeAlamat.GetValueOrDefault <> Nothing Then
                    SafeNumIndex(rblTRXKKCORPTipeAlamat, objResume(SetnGetRowEdit).TransactionCashOut.CORP_TipeAlamat)
                End If
                txtTRXKKCORPDLNamaJalan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_NamaJalan)
                txtTRXKKCORPDLRTRW.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_RTRW)
                objSKelurahan = objKelurahan.Find(MsKelurahanColumn.IDKelurahan, objResume(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsKelurahan_Id)
                If Not IsNothing(objSKelurahan) Then
                    txtTRXKKCORPDLKelurahan.Text = Safe(objSKelurahan.NamaKelurahan)
                    hfTRXKKCORPDLKelurahan.Value = Safe(objSKelurahan.IDKelurahan)
                End If
                objSkecamatan = objKecamatan.Find(MsKecamatanColumn.IDKecamatan, objResume(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsKecamatan_Id)
                If Not IsNothing(objSkecamatan) Then
                    txtTRXKKCORPDLKecamatan.Text = Safe(objSkecamatan.NamaKecamatan)
                    hfTRXKKCORPDLKecamatan.Value = Safe(objSkecamatan.IDKecamatan)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTRXKKCORPDLKotaKab.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTRXKKCORPDLKotaKab.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtTRXKKCORPDLKodePos.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_KodePos)
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTRXKKCORPDLProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTRXKKCORPDLProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                'objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objResume(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsNegara_Id)
                'If Not IsNothing(objSMapNegara) Then
                '    objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                '    If Not IsNothing(objSnegara) Then
                '        txtTRXKKCORPDLNegara.Text = Safe(objSnegara.NamaNegara)
                '        hfTRXKKCORPDLNegara.Value = Safe(objSnegara.IDNegaraNCBS)
                '    End If
                'End If


                Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objResume(SetnGetRowEdit).TransactionCashOut.CORP_FK_MsNegara_Id.GetValueOrDefault(0))
                    If Not objNegaraRef Is Nothing Then

                        txtTRXKKCORPDLNegara.Text = Safe(objNegaraRef.NamaNegara)
                        hfTRXKKCORPDLNegara.Value = Safe(objNegaraRef.IDNegara)
                    End If
                End Using
                txtTRXKKCORPLNNamaJalan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_LN_NamaJalan)
                'objSMapNegara = objMapNegara.Find(MappingMsNegaraNCBSPPATKColumn.IDNegara, objResume(SetnGetRowEdit).TransactionCashOut.CORP_LN_FK_MsNegara_Id)
                'If Not IsNothing(objSMapNegara) Then
                '    objSnegara = objNegara.Find(MsNegaraNCBSColumn.IDNegaraNCBS, objSMapNegara.IDNegaraNCBS)
                '    If Not IsNothing(objSnegara) Then
                '        txtTRXKKCORPLNNegara.Text = Safe(objSnegara.NamaNegara)
                '        hfTRXKKCORPLNNegara.Value = Safe(objSnegara.IDNegaraNCBS)
                '    End If
                'End If


                Using objNegaraRef As MsNegara = DataRepository.MsNegaraProvider.GetByIDNegara(objResume(SetnGetRowEdit).TransactionCashOut.CORP_LN_FK_MsNegara_Id.GetValueOrDefault(0))
                    If Not objNegaraRef Is Nothing Then

                        txtTRXKKCORPLNNegara.Text = Safe(objNegaraRef.NamaNegara)
                        hfTRXKKCORPLNNegara.Value = Safe(objNegaraRef.IDNegara)
                    End If
                End Using
                objSProvinsi = objProvinsi.Find(MsProvinceColumn.IdProvince, objResume(SetnGetRowEdit).TransactionCashOut.CORP_LN_FK_MsProvince_Id)
                If Not IsNothing(objSProvinsi) Then
                    txtTRXKKCORPLNProvinsi.Text = Safe(objSProvinsi.Nama)
                    hfTRXKKCORPLNProvinsi.Value = Safe(objSProvinsi.IdProvince)
                End If
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objResume(SetnGetRowEdit).TransactionCashOut.CORP_LN_MsKotaKab_Id)
                If Not IsNothing(objSkotaKab) Then
                    txtTRXKKCORPLNKota.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfTRXKKCORPLNKota.Value = Safe(objSkotaKab.IDKotaKab)
                End If
                txtTRXKKCORPLNKodePos.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_LN_KodePos)
                txtTRXKKCORPNPWP.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_NPWP)
                txtTRXKKCORPTujuanTrx.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_TujuanTransaksi)
                txtTRXKKCORPSumberDana.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_SumberDana)
                txtTRXKKCORPNamaBankLain.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_NamaBankLain)
                txtTRXKKCORPNoRekeningTujuan.Text = Safe(objResume(SetnGetRowEdit).TransactionCashOut.CORP_NomorRekeningTujuan)

                'imgAdd.Visible = False
                'imgClearAll.Visible = False
                'imgSaveEdit.Visible = True
                'imgCancelEdit.Visible = True
            End If


            ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('IdTerlapor','Img1','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
            ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('Umum','searchimage2','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)

        Catch ex As Exception
            LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

#End Region

End Class

#Region "Class WICDetailCashIn"
Public Class WICDetailCashInDetail

    Private _mataUang As String
    Public Property MataUang() As String
        Get
            Return _mataUang
        End Get
        Set(ByVal value As String)
            _mataUang = value
        End Set
    End Property

    Private _kursTransaksi As String
    Public Property KursTransaksi() As String
        Get
            Return _kursTransaksi
        End Get
        Set(ByVal value As String)
            _kursTransaksi = value
        End Set
    End Property

    Private _jumlah As String
    Public Property Jumlah() As String
        Get
            Return _jumlah
        End Get
        Set(ByVal value As String)
            _jumlah = value
        End Set
    End Property

    Private _jumlahRp As String
    Public Property JumlahRp() As String
        Get
            Return _jumlahRp
        End Get
        Set(ByVal value As String)
            _jumlahRp = value
        End Set
    End Property

End Class
#End Region

#Region "Class ResumeKasMasukKeluarWIC"
Public Class ResumeKasMasukKeluarWIC

    Private _kas As String
    Public Property Kas() As String
        Get
            Return _kas
        End Get
        Set(ByVal value As String)
            _kas = value
        End Set
    End Property

    Private _transactionDate As String
    Public Property TransactionDate() As String
        Get
            Return _transactionDate
        End Get
        Set(ByVal value As String)
            _transactionDate = value
        End Set
    End Property

    Private _branch As String
    Public Property Branch() As String
        Get
            Return _branch
        End Get
        Set(ByVal value As String)
            _branch = value
        End Set
    End Property

    Private _accountNumber As String
    Public Property AccountNumber() As String
        Get
            Return _accountNumber
        End Get
        Set(ByVal value As String)
            _accountNumber = value
        End Set
    End Property

    Private _Type As String
    Public Property Type() As String
        Get
            Return _Type
        End Get
        Set(ByVal value As String)
            _Type = value
        End Set
    End Property

    Private _transactionNominal As String
    Public Property TransactionNominal() As String
        Get
            Return _transactionNominal
        End Get
        Set(ByVal value As String)
            _transactionNominal = value
        End Set
    End Property

    Private _transactionCashIn As WICTransactionCashIn
    Public Property TransactionCashIn() As WICTransactionCashIn
        Get
            Return _transactionCashIn
        End Get
        Set(ByVal value As WICTransactionCashIn)
            _transactionCashIn = value
        End Set
    End Property

    Private _transactionCashOut As WICTransactionCashOut
    Public Property TransactionCashOut() As WICTransactionCashOut
        Get
            Return _transactionCashOut
        End Get
        Set(ByVal value As WICTransactionCashOut)
            _transactionCashOut = value
        End Set
    End Property

    Private _detailTransactionCashIn As List(Of WICDetailCashInTransaction)
    Public Property DetailTransactionCashIn() As List(Of WICDetailCashInTransaction)
        Get
            Return _detailTransactionCashIn
        End Get
        Set(ByVal value As List(Of WICDetailCashInTransaction))
            _detailTransactionCashIn = value
        End Set
    End Property

    Private _detailTransactionCashOut As List(Of WICDetailCashOutTransaction)
    Public Property DetailTranscationCashOut() As List(Of WICDetailCashOutTransaction)
        Get
            Return _detailTransactionCashOut
        End Get
        Set(ByVal value As List(Of WICDetailCashOutTransaction))
            _detailTransactionCashOut = value
        End Set
    End Property



End Class
#End Region

