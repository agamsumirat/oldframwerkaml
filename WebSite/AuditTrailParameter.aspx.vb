Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Partial Class AuditTrailParameter
    Inherits Parent

#Region "Change Event Radio Button"


    ''' <summary>
    ''' Disable textbox
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Disable()
        Try
            Me.TextEmail.Enabled = False
            Me.TextRecordBeforeReachOut.Enabled = False
            Me.TextMaxRecordAuditTrail.Enabled = True
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' Enable
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Enable()
        Me.TextEmail.Enabled = True
        Me.TextRecordBeforeReachOut.Enabled = True
        Me.TextMaxRecordAuditTrail.Enabled = True
        Me.RadioOverwrite.Checked = False
    End Sub
    ''' <summary>
    ''' overwrite
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub RadioOverwrite_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadioOverwrite.CheckedChanged
        Try
            Disable()
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' Alert
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub RadioAlert_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadioAlert.CheckedChanged
        Try
            Enable()
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' manualy
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub RadioManually_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadioManually.CheckedChanged
        Try
            Disable()
            Me.TextMaxRecordAuditTrail.Enabled = False
            Me.TextMaxRecordAuditTrail.Text = 0
            Me.RadioOverwrite.Checked = False
        Catch
            Throw
        End Try
    End Sub

#End Region

#Region "Cek Approval"
    ''' <summary>
    ''' cek approval
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CekApproval() As Boolean
        Try
            Using AccessCountApproval As New AMLDAL.AMLDataSetTableAdapters.CountAuditParameterApprovalTableAdapter
                If CInt(AccessCountApproval.Jml) > 0 Then
                    Return False
                Else
                    Return True
                End If
            End Using
        Catch
            Throw
        End Try
    End Function
#End Region

#Region "Alert Option"
    ''' <summary>
    ''' Get Alert Option 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function AlertOption() As Integer
        If Me.RadioAlert.Checked Then
            Return 2
        ElseIf Me.RadioOverwrite.Checked Then
            Return 1
        Else
            Return 3
        End If
    End Function
#End Region

#Region "Insert & Update"


    ''' <summary>
    ''' Insert to Approval
    ''' </summary>
    ''' <param name="mode"></param>
    ''' <remarks></remarks>
    Public Sub InsertIntoApproval(ByVal mode As Integer)
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            Dim AlertOpt As Integer = AlertOption()
            If mode = 1 Then ' Add
                ' 1. Insert ke Audit Trail Parameter Pending Approval
                Using AccessInsertAuditTrailApproval As New AMLDAL.AMLDataSetTableAdapters.InsertAuditTrailParameterApprovalTableAdapter
                    oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessInsertAuditTrailApproval, Data.IsolationLevel.ReadUncommitted)
                    If Me.RadioOverwrite.Checked Then
                        Me.TextRecordBeforeReachOut.Text = 0
                        Me.TextEmail.Text = ""
                    ElseIf Me.RadioManually.Checked Then
                        Me.TextRecordBeforeReachOut.Text = 0
                        Me.TextEmail.Text = ""
                    End If
                    Dim ApprovalId As Integer = AccessInsertAuditTrailApproval.InsertATApproval(CInt(Me.TextMaxRecordAuditTrail.Text), CInt(AlertOpt), Me.TextEmail.Text, CInt(Me.TextRecordBeforeReachOut.Text), Now, Now, Nothing, 0, Nothing, 0, Now, Now).Rows(0)("ApprovalID")
                    '2. Insert Parameter Pending
                    Using AccessParameterPending As New AMLDAL.AMLDataSetTableAdapters.Parameters_PendingApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessParameterPending, oSQLTrans)
                        Dim Parameters_PendingApprovalID As Integer = AccessParameterPending.InsertParameters_PendingApproval(Sahassa.AML.Commonly.SessionUserId, Now, 1)

                        '3. Insert Parameter Approval
                        Using AccessParameterApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_ApprovalTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessParameterApproval, oSQLTrans)
                            AccessParameterApproval.InsertParameters_Approval(CInt(Parameters_PendingApprovalID), 1, 10, ApprovalId)
                        End Using
                    End Using
                End Using
            ElseIf mode = 2 Then 'Edit
                Using AccessGetAuditTrailParameterOld As New AMLDAL.AMLDataSetTableAdapters.AuditTrailParameterTableAdapter
                    oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessGetAuditTrailParameterOld, Data.IsolationLevel.ReadUncommitted)
                    'Sahassa.AML.TableAdapterHelper.SetTransaction(AccessGetAuditTrailParameterOld, oSQLTrans)
                    Dim Row_Old As AMLDAL.AMLDataSet.AuditTrailParameterRow = AccessGetAuditTrailParameterOld.GetData.Rows(0)

                    ' 1. Insert ke Audit Trail Parameter Pending Approval
                    Using AccessInsertAuditTrailApproval As New AMLDAL.AMLDataSetTableAdapters.InsertAuditTrailParameterApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessInsertAuditTrailApproval, oSQLTrans)
                        Dim ApprovalId As Integer = AccessInsertAuditTrailApproval.InsertATApproval(CInt(Me.TextMaxRecordAuditTrail.Text), CInt(AlertOpt), Me.TextEmail.Text, CInt(Me.TextRecordBeforeReachOut.Text), Row_Old.CreatedDate, Now, Row_Old.MaximumTrailRecord, Row_Old.AlertOptions, Row_Old.OperatorEmailAddress, Row_Old.RecordCountBeforeReachOut, Row_Old.CreatedDate, Row_Old.LastUpdateDate).Rows(0)("ApprovalID")

                        '2. Insert Parameter Pending
                        Using AccessParameterPending As New AMLDAL.AMLDataSetTableAdapters.Parameters_PendingApprovalTableAdapter
                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessParameterPending, oSQLTrans)
                            Dim Parameters_PendingApprovalID As Integer = AccessParameterPending.InsertParameters_PendingApproval(Sahassa.AML.Commonly.SessionUserId, Now, 2)

                            '3. Insert Parameter Approval
                            Using AccessParameterApproval As New AMLDAL.AMLDataSetTableAdapters.Parameters_ApprovalTableAdapter
                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessParameterApproval, oSQLTrans)
                                AccessParameterApproval.InsertParameters_Approval(CInt(Parameters_PendingApprovalID), 2, 10, ApprovalId)
                            End Using
                        End Using
                    End Using
                End Using
            End If
            oSQLTrans.Commit()
            'End Using
        Catch
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub
    ''' <summary>
    ''' Insert Audit Trail SU
    ''' </summary>
    ''' <param name="mode"></param>
    ''' <param name="Action"></param>
    ''' <remarks></remarks>
    Private Sub InsertAuditTrailSU(ByVal mode As String, ByVal Action As String)
        Try
            Using AccessAuditTrailParameter As New AMLDAL.AMLDataSetTableAdapters.AuditTrailParameterTableAdapter
                Using dtAuditTrailParameter As AMLDAL.AMLDataSet.AuditTrailParameterDataTable = AccessAuditTrailParameter.GetData
                    Dim RowAuditTrailParameter As AMLDAL.AMLDataSet.AuditTrailParameterRow = dtAuditTrailParameter.Rows(0)
                    If RowAuditTrailParameter.AlertOptions = 1 Then 'alert Overwrite
                        If Not Sahassa.AML.AuditTrailAlert.CheckOverwrite(6) Then
                            Throw New Exception("There is a problem in checking audittrail alert overwrite")
                        End If
                    ElseIf RowAuditTrailParameter.AlertOptions = 2 Then 'alert mail
                        If Not Sahassa.AML.AuditTrailAlert.CheckMailAlert(6) Then
                            Throw New Exception("There is a problem in checking audittrail alert by mail.")
                        End If
                    End If ' yg manual tidak perlu d cek krn ada audittrail maintenance
                End Using
            End Using

            Dim AlertOption_Old = ""
            Dim AlertOption_New = ""
            Dim OperatorMail_Old = ""
            Dim OperatorMail_New = ""
            Dim Limit_Old = ""
            Dim Limit_New As String = ""
            Dim strMaxRecord As String
            Using AccessAuditTrailParameter As New AMLDAL.AMLDataSetTableAdapters.AuditTrailParameterTableAdapter
                Dim row As AMLDAL.AMLDataSet.AuditTrailParameterRow = AccessAuditTrailParameter.GetData.Rows(0)

                If mode.ToLower = "edit" Then
                    Select Case AlertOption()
                        Case 1
                            AlertOption_New = "Overwrite"
                            OperatorMail_New = ""
                            Limit_New = 0
                        Case 2
                            AlertOption_New = "Alert By Mail"
                            OperatorMail_New = Me.TextEmail.Text
                        Case 3
                            AlertOption_New = "Manually"
                            OperatorMail_New = ""
                            Limit_New = 0
                    End Select

                    Select Case row.AlertOptions
                        Case 1
                            AlertOption_Old = "Overwrite"
                            OperatorMail_Old = ""
                            Limit_Old = 0
                        Case 2
                            AlertOption_Old = "Alert By Mail"
                            OperatorMail_Old = row.OperatorEmailAddress
                        Case 3
                            AlertOption_Old = "Manually"
                            OperatorMail_Old = ""
                            Limit_Old = 0
                    End Select
                    strMaxRecord = Me.TextMaxRecordAuditTrail.Text
                Else
                    strMaxRecord = 0
                End If


                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - AuditTrailParameter", "MaximumTrailRecord", mode, strMaxRecord, strMaxRecord, Action)
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - AuditTrailParameter", "AlertOptions", mode, AlertOption_Old, AlertOption_New, Action)
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - AuditTrailParameter", "OperatorEmailAddress", mode, OperatorMail_Old, OperatorMail_New, Action)
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - AuditTrailParameter", "RecordCountBeforeReachOut", mode, Limit_Old, Limit_New, Action)
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - AuditTrailParameter", "CreatedDate", mode, row.CreatedDate, Now, Action)
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - AuditTrailParameter", "LastUpdateDate", mode, row.LastUpdateDate, Now, Action)
                End Using
            End Using
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' Insert by SU
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub InsertBySU()
        Try
            Dim AlertOpt As Integer = AlertOption()
            Using InsertAuditTrailParameter As New AMLDAL.AMLDataSetTableAdapters.AuditTrailParameterTableAdapter
                If InsertAuditTrailParameter.InsertAuditTrailParameter(CInt(Me.TextMaxRecordAuditTrail.Text), CInt(AlertOpt), Me.TextEmail.Text, CInt(Me.TextRecordBeforeReachOut.Text), Now, Now) > 0 Then
                    Me.InsertAuditTrailSU("Add", "Accepted")
                    Me.LblSuccess.Visible = True
                    Me.LblSuccess.Text = "AuditTrail Parameter succeed to insert. "
                Else
                    Throw New Exception("Failed to insert AuditTrail Parameter.")
                End If
            End Using

        Catch
            Throw
        End Try
    End Sub

    ''' <summary>
    ''' Update By SU
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub UpdateBySU()
        Try
            Dim AlertOpt As Integer = AlertOption()
            Using UpdateAuditTrailParameter As New AMLDAL.AMLDataSetTableAdapters.AuditTrailParameterTableAdapter
                If UpdateAuditTrailParameter.UpdateAuditTrailParameter(CInt(Me.TextMaxRecordAuditTrail.Text), CInt(AlertOpt), Me.TextEmail.Text, CInt(Me.TextRecordBeforeReachOut.Text), Now) > 0 Then
                    Me.InsertAuditTrailSU("Edit", "Accepted")
                    Me.LblSuccess.Visible = True
                    Me.LblSuccess.Text = "AuditTrail Parameter succeed to update. "
                Else
                    Throw New Exception("Failed to update AuditTrail Parameter.")
                End If
            End Using

        Catch
            Throw
        End Try
    End Sub
#End Region

    ''' <summary>
    ''' Load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If CekApproval() Then
                If Not Me.IsPostBack Then
                    Using AccessAuditTrail As New AMLDAL.AMLDataSetTableAdapters.AuditTrailParameterTableAdapter
                        Using dtAuditTrail As AMLDAL.AMLDataSet.AuditTrailParameterDataTable = AccessAuditTrail.GetData
                            If dtAuditTrail.Rows.Count > 0 Then ' Edit
                                Dim Row As AMLDAL.AMLDataSet.AuditTrailParameterRow = dtAuditTrail.Rows(0)
                                If Row.AlertOptions = 1 Then ' 1 -> overwrite
                                    Disable()
                                    Me.RadioOverwrite.Checked = True
                                ElseIf Row.AlertOptions = 2 Then ' 2 -> Alert
                                    Enable()
                                    Me.RadioAlert.Checked = True
                                Else ' 3 -> Manually
                                    Disable()
                                    Me.RadioManually.Checked = True
                                    Me.TextMaxRecordAuditTrail.Enabled = False
                                    Me.RadioOverwrite.Checked = False
                                End If
                                Me.TextMaxRecordAuditTrail.Text = Row.MaximumTrailRecord
                                Me.TextEmail.Text = Row.OperatorEmailAddress
                                Me.TextRecordBeforeReachOut.Text = Row.RecordCountBeforeReachOut
                            Else 'Add
                                Me.RadioOverwrite.Checked = True
                                Disable()
                            End If
                        End Using
                    End Using

                    Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                        Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                        AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                    End Using
                End If
            Else
                Throw New Exception("Audit Trail Parameter is waiting for Approved.")
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
   
    ''' <summary>
    ''' Save Audit Trail
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Try
            If Me.CekApproval Then
                Using AccessAuditTrail As New AMLDAL.AMLDataSetTableAdapters.AuditTrailParameterTableAdapter
                    Using dtAuditTrail As AMLDAL.AMLDataSet.AuditTrailParameterDataTable = AccessAuditTrail.GetData
                        If dtAuditTrail.Rows.Count > 0 Then ' Edit
                            If Sahassa.AML.Commonly.SessionPkUserId = 1 Then ' SU
                                UpdateBySU()
                            Else ' Selain SU masuk Approval
                                Me.InsertIntoApproval(2)

                                Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=8882"

                                Me.Response.Redirect("MessagePending.aspx?MessagePendingID=8882", False)
                            End If
                        Else ' Add new parameter
                            Dim AlertOpt As Integer = AlertOption()
                            If Sahassa.AML.Commonly.SessionPkUserId = 1 Then ' SU
                                InsertBySU()
                            Else ' Selain SU masuk Approval
                                Me.InsertIntoApproval(1)

                                Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=8881"

                                Me.Response.Redirect("MessagePending.aspx?MessagePendingID=8881", False)
                            End If
                        End If
                    End Using
                End Using
            Else
                Throw New Exception("AuditTrail Parameter is waiting for Approved.")
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "Default.aspx"

        Me.Response.Redirect("Default.aspx", False)
    End Sub
End Class
