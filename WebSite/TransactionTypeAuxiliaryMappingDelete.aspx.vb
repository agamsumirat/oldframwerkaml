
Partial Class TransactionTypeAuxiliaryMappingDelete
    Inherits Parent

    Private ReadOnly Property ParamAuxTransCode() As String
        Get
            Return Me.Request.Params("AuxTransCode")
        End Get
    End Property

    Private ReadOnly Property ParamAuxTransCodeDesc() As String
        Get
            Return Me.Request.Params("AuxTransCodeDesc")
        End Get
    End Property

    Private ReadOnly Property ParamTransactionTypeId() As String
        Get
            Return Me.Request.Params("TransactionTypeId")
        End Get
    End Property


    Private ReadOnly Property ParamTransactionTypeName() As String
        Get
            Return Me.Request.Params("TransactionTypeName")
        End Get
    End Property

    Private Function CheckDataExists() As Boolean
        Dim AuxTransCode As Integer = Me.ParamAuxTransCode
        Using CekData As New AMLDAL.TransactionTypeAuxiliaryMappingTableAdapters.QueriesTableAdapter
            Dim count As Int32 = CekData.CekTransactionTypeAuxiliaryTransactionCodeMapping(AuxTransCode)

            If count = 0 Then
                Return False
            Else
                Return True
            End If
        End Using
    End Function

    Public Function CheckDataPendingApprovalStatus() As Boolean
        Dim AuxTransCode As Integer = Me.ParamAuxTransCode
        Using ApprovalTableAdapter As New AMLDAL.TransactionTypeAuxiliaryMappingTableAdapters.QueriesTableAdapter
            Dim count As Int32 = ApprovalTableAdapter.CekStatusTransactionTypeAuxiliaryTransactionCodeMapping_PendingApproval(AuxTransCode)

            If count = 0 Then
                Return False
            Else
                Return True
            End If
        End Using
    End Function

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                    'End Using
                End Using
                Me.LblAuxType.Text = Me.ParamAuxTransCode & " - " & Me.ParamAuxTransCodeDesc
                Me.LblTransId.Text = Me.ParamTransactionTypeId & " - " & Me.ParamTransactionTypeName
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImageButtonCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "TransactionTypeAuxiliaryMapping.aspx"
        Response.Redirect("TransactionTypeAuxiliaryMapping.aspx", False)
    End Sub

    Protected Sub ImageButtonDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonDelete.Click
        Try
            If Me.CheckDataPendingApprovalStatus = True Then
                Throw New Exception("Cannot delete the following data: '" & Me.LblAuxType.Text & "' because it is currently waiting for approval.")
            End If

            If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                If DeleteBySu() Then
                    Sahassa.AML.Commonly.SessionIntendedPage = "TransactionTypeAuxiliaryMapping.aspx"
                    Response.Redirect("TransactionTypeAuxiliaryMapping.aspx", False)
                End If
            Else
                If DeleteData() Then
                    Me.Response.Redirect("MessagePending.aspx?MessagePendingID=81306", False)
                End If
            End If
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Function DeleteBySu() As Boolean
        Try
            Using AuxiliaryTrans As New AMLDAL.TransactionTypeAuxiliaryMappingTableAdapters.UpdateTransactionTypeAuxiliaryTransactionCodeMappingTableAdapter
                Dim TransIdOld As String = Me.ParamTransactionTypeId
                Dim AuxTransCode As Integer = Me.ParamAuxTransCode

                AuxiliaryTrans.DeleteTransactionTypeAuxiliaryTransactionCodeMapping(AuxTransCode)

                Using AccessUser As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "TransactionTypeAuxiliaryMappingEdit", "TransactionTypeId", "Delete", TransIdOld, Nothing, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "TransactionTypeAuxiliaryMappingEdit", "AuxiliaryTransactionCode", "Delete", AuxTransCode, Nothing, "Accepted")
                    End Using
                End Using

            End Using

            Return True
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Function

    Private Function DeleteData() As Boolean
        Dim ApprovalMasterId As Int64

        Try
            Dim TransIdOld As String = Me.ParamTransactionTypeId
            Dim AuxTransCode As Integer = Me.ParamAuxTransCode
            Dim DeletedDate As DateTime = DateTime.Now
            Dim DeletedBy As String = Sahassa.AML.Commonly.SessionUserId

            If Not IsNumeric(TransIdOld) Then TransIdOld = Nothing

            Using ApprovalTableAdapter As New AMLDAL.TransactionTypeAuxiliaryMappingTableAdapters.UpdateTransactionTypeAuxiliaryTransactionCodeMappingTableAdapter

                ApprovalMasterId = ApprovalTableAdapter.InsertTransactionTypeAuxiliaryTransactionCodeMapping_Approval(DeletedDate, DeletedBy, 3)

                ApprovalTableAdapter.InsertTransactionTypeAuxiliaryTransactionCodeMapping_PendingApproval(ApprovalMasterId, TransIdOld, AuxTransCode, TransIdOld, AuxTransCode)

                Return True
            End Using
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Function
End Class
