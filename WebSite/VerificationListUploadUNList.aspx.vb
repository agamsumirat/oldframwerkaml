Imports System.IO
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Xml
Imports AMLBLL

Partial Class VerificationListUploadUNList
    Inherits Parent

    Private dsUNList As New Data.DataSet

    Public ReadOnly Property GetFocusID() As String
        Get
            Return Me.FileUploadUNList.ClientID
        End Get
    End Property

    Private Sub InsertAuditTrail()
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(1)
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "VerificationList", "ALL", "Update", "", "", "Upload UN List")
            End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Function ExecNonQuery(ByVal SQLQuery As String) As Integer
        Dim SqlConn As SqlConnection = Nothing
        Dim SqlCmd As SqlCommand = Nothing
        Try
            SqlConn = New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("AMLConnectionString").ToString)
            SqlConn.Open()
            SqlCmd = New SqlCommand
            SqlCmd.Connection = SqlConn
            SqlCmd.CommandTimeout = System.Configuration.ConfigurationManager.AppSettings.Get("SQLCommandTimeout")
            SqlCmd.CommandText = SQLQuery
            Return SqlCmd.ExecuteNonQuery()
        Catch tex As Threading.ThreadAbortException
            Throw tex
        Catch ex As Exception
            Throw ex
        Finally
            If Not SqlConn Is Nothing Then
                SqlConn.Close()
                SqlConn.Dispose()
                SqlConn = Nothing
            End If
            If Not SqlCmd Is Nothing Then
                SqlCmd.Dispose()
                SqlCmd = Nothing
            End If
        End Try
    End Function

    Private Function getStringFieldValue(ByVal objValue As Object) As String
        Dim strValue As String = ""
        Try
            If Not IsDBNull(objValue) Then
                strValue = CStr(objValue)
            End If
            Return strValue
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function ExecuteProcessUNList(ByVal ProcessDate As DateTime) As Boolean
        ExecNonQuery("EXEC usp_ProcessUNList '" & ProcessDate.ToString("yyyy-MM-dd") & "'")
        Return True
    End Function

    Private Function CekApproval() As Boolean
        Using ObjUNListBLL As New UNListBLL
            Return ObjUNListBLL.CheckUNListPendingApproval
        End Using
    End Function

    Protected Sub ImageUpload_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageUpload.Click
        Try
            If Me.CekApproval Then
                If Me.FileUploadUNList.PostedFile.ContentLength <> 0 Then
                    If Me.FileUploadUNList.HasFile Then
                        Dim destDir As String = Server.MapPath("Upload")
                        Dim fName As String = Path.GetFileName(Me.FileUploadUNList.PostedFile.FileName)
                        Dim destPath As String = Path.Combine(destDir, fName)
                        Me.FileUploadUNList.PostedFile.SaveAs(destPath)

                        Dim ObjVerificationUNList As List(Of VerificationListMasterContainer) = Me.ReadXMLFile(destPath, 0)

                        Using ObjUNListBLL As New UNListBLL
                            ObjUNListBLL.SaveToUNList(ObjVerificationUNList)

                            If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                                ' INSERT TO Audit Trail
                                InsertAuditTrail()
                                ' Update Verification List
                                ExecuteProcessUNList(Now())
                                Me.LblSuccess.Visible = True
                                Me.LblSuccess.Text = "Success to Update Verification List for Category UN List."
                            Else
                                ObjUNListBLL.SaveToApproval()

                                Dim MessagePendingID As Integer = 9002 'MessagePendingID 8865 = UNList
                                Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID, False)
                            End If
                        End Using
                    Else
                        Throw New Exception("Choose an UN-List.xls file first.")
                    End If
                End If
            Else
                Throw New Exception("Upload UN is already waiting for approval.")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageButtonCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonCancel.Click
        Me.Response.Redirect("Default.aspx", False)
    End Sub

    Public Function ReadXMLFile(ByVal xmlFilePath As String, ByRef countAllData As Integer) As List(Of VerificationListMasterContainer)
        countAllData = 0
        Dim objMasterList As List(Of VerificationListMasterContainer) = New List(Of VerificationListMasterContainer)
        Dim objAddressChildList As List(Of VerificationListAddressContainer) = New List(Of VerificationListAddressContainer)
        Dim objAliasChildList As List(Of VerificationListAliasContainer) = New List(Of VerificationListAliasContainer)
        Dim objIDNumberChildList As List(Of VerificationListIdNumberContainer) = New List(Of VerificationListIdNumberContainer)
        Try
            Dim doc As XmlDocument = New XmlDocument
            doc.Load(xmlFilePath)
            Dim nodeList As XmlNodeList = doc.GetElementsByTagName("INDIVIDUALS")
            For Each node1 As XmlNode In nodeList
                Dim elem As XmlElement = CType(node1, XmlElement)
                If elem.HasChildNodes Then
                    Dim nodeList2 As XmlNodeList = elem.GetElementsByTagName("INDIVIDUAL")
                    For Each node11 As XmlNode In nodeList2
                        Dim vm As VerificationListMasterContainer = New VerificationListMasterContainer
                        Dim strFirstName As String = ""
                        Dim strSecondName As String = ""
                        Dim strThirdName As String = ""
                        Dim elem2 As XmlElement = CType(node11, XmlElement)
                        If elem2.HasChildNodes Then
                            objAliasChildList = New List(Of VerificationListAliasContainer)
                            objIDNumberChildList = New List(Of VerificationListIdNumberContainer)
                            objAddressChildList = New List(Of VerificationListAddressContainer)
                            Dim childNodes As XmlNodeList = elem2.ChildNodes
                            For Each nodeTemp As XmlNode In childNodes
                                Select Case (nodeTemp.Name)
                                    Case "FIRST_NAME"
                                        strFirstName = nodeTemp.InnerText
                                    Case "SECOND_NAME"
                                        strSecondName = nodeTemp.InnerText
                                    Case "THIRD_NAME"
                                        strThirdName = nodeTemp.InnerText
                                    Case "SORT_KEY"
                                        vm.DisplayName = nodeTemp.InnerText
                                    Case "SORT_KEY_LAST_MOD"

                                        vm.DateUpdated = Sahassa.AML.Commonly.ConvertToDate("yyyy-MM-dd", nodeTemp.InnerText.Trim)
                                    Case "REFERENCE_NUMBER"
                                        vm.RefId = nodeTemp.InnerText
                                    Case "LISTED_ON"
                                        vm.DateEntered = Sahassa.AML.Commonly.ConvertToDate("yyyy-MM-dd", nodeTemp.InnerText.Trim)
                                    Case "COMMENTS1"
                                        If (nodeTemp.InnerText.Length > 255) Then
                                            'bagi menjadi beberapa custom remarks
                                            Dim cutter As Integer = (nodeTemp.InnerText.Length / 255)
                                            If (cutter >= 5) Then
                                                Dim maxData As String = nodeTemp.InnerText.Substring(0, (5 * 255))
                                                vm.CustomRemark1 = maxData.Substring(0, 255)
                                                vm.CustomRemark2 = maxData.Substring(((1 * 255) _
                                                                + 1), 255)
                                                vm.CustomRemark3 = maxData.Substring(((2 * 255) _
                                                                + 1), 255)
                                                vm.CustomRemark4 = maxData.Substring(((3 * 255) _
                                                                + 1), 255)
                                                vm.CustomRemark5 = maxData.Substring(((4 * 255) _
                                                                + 1))
                                            ElseIf (cutter < 5) Then
                                                Dim i As Integer = 1
                                                Do While (i <= cutter)
                                                    Select Case (i)
                                                        Case 1
                                                            vm.CustomRemark1 = nodeTemp.InnerText.Substring(0, 255)
                                                        Case 2
                                                            vm.CustomRemark2 = nodeTemp.InnerText.Substring(((1 * 255) _
                                                                            + 1), Math.Min(255, nodeTemp.InnerText.Length - (1 * 255) - 1))
                                                        Case 3
                                                            vm.CustomRemark3 = nodeTemp.InnerText.Substring(((2 * 255) _
                                                                            + 1), Math.Min(255, nodeTemp.InnerText.Length - (2 * 255) - 1))
                                                        Case 4
                                                            vm.CustomRemark4 = nodeTemp.InnerText.Substring(((3 * 255) _
                                                                            + 1), Math.Min(255, nodeTemp.InnerText.Length - (3 * 255) - 1))
                                                    End Select
                                                    i = (i + 1)
                                                Loop
                                            End If
                                        Else
                                            vm.CustomRemark1 = nodeTemp.InnerText
                                        End If
                                    Case "LIST_TYPE"
                                        If nodeTemp.HasChildNodes Then
                                            Dim lt As XmlNodeList = nodeTemp.ChildNodes
                                            For Each nd As XmlNode In lt
                                                If (nd.Name = "VALUE") Then
                                                    If (nd.InnerText = "UN List") Then
                                                        vm.VerificationListCategoryId = 3
                                                    End If
                                                End If
                                            Next
                                        End If
                                    Case "LAST_DAY_UPDATED"
                                        If nodeTemp.HasChildNodes Then
                                            Dim updList As XmlNodeList = nodeTemp.ChildNodes
                                            Dim arUpd As List(Of String) = New List(Of String)
                                            For Each nd As XmlNode In updList

                                                arUpd.Add(Sahassa.AML.Commonly.ConvertToDate("yyyy-MM-dd", nd.InnerText.Trim))
                                            Next
                                            arUpd.Sort()

                                            vm.DateOfData = Convert.ToDateTime(arUpd((arUpd.Count - 1)))
                                            'diambil updated date
                                        End If
                                    Case "INDIVIDUAL_DATE_OF_BIRTH"
                                        If nodeTemp.HasChildNodes Then
                                            Dim ndBirthList As XmlNodeList = nodeTemp.ChildNodes
                                            For Each ndBirth As XmlNode In ndBirthList
                                                If (ndBirth.Name = "DATE") Then
                                                    vm.DateOfBirth = Convert.ToDateTime(ndBirth.InnerText.Substring(0, 10))
                                                ElseIf (ndBirth.Name = "YEAR") Then
                                                    vm.DateOfBirth = New DateTime(Convert.ToInt32(ndBirth.InnerText), 1, 1)
                                                Else
                                                    vm.DateOfBirth = New DateTime(1900, 1, 1)
                                                End If
                                            Next
                                        End If
                                End Select
                                If (Convert.ToDateTime(vm.DateOfData).ToString("dd/MM/yyyy") = "1/1/0001") Then
                                    vm.DateOfData = New DateTime(1900, 1, 1)
                                End If
                                ''masukkan juga sebagai alias untuk nama aslinya
                                'If (nodeTemp.Name = "SORT_KEY") Then
                                '    Dim va As VerificationListAliasContainer = New VerificationListAliasContainer
                                '    va.Name = nodeTemp.InnerText

                                '    'tambahkan ke VerificationListAliasContainer list
                                '    objAliasChildList.Add(va)
                                'End If
                                If (nodeTemp.Name = "INDIVIDUAL_ALIAS") Then
                                    If nodeTemp.HasChildNodes Then
                                        Dim ndAliasList As XmlNodeList = nodeTemp.ChildNodes
                                        For Each ndAlias As XmlNode In ndAliasList
                                            Dim val As VerificationListAliasContainer = New VerificationListAliasContainer
                                            If (ndAlias.Name = "ALIAS_NAME") Then
                                                val.Name = ndAlias.InnerText
                                                objAliasChildList.Add(val)
                                            End If
                                        Next
                                    End If
                                End If
                                'tambahkan ke VerificationListMasterContainer
                                vm.lstVerificationList_Alias = objAliasChildList
                                If (nodeTemp.Name = "INDIVIDUAL_DOCUMENT") Then
                                    If nodeTemp.HasChildNodes Then
                                        Dim ndIdList As XmlNodeList = nodeTemp.ChildNodes
                                        For Each ndId As XmlNode In ndIdList
                                            Dim vid As VerificationListIdNumberContainer = New VerificationListIdNumberContainer
                                            If (ndId.Name = "NUMBER") Then
                                                If ndId.InnerText.Contains("number") Then
                                                    vid.IDNumber = ndId.InnerText.Replace("number", "").Trim
                                                ElseIf (ndId.InnerText.Contains("Passport number") OrElse ndId.InnerText.Contains("passport number")) Then
                                                    If ndId.InnerText.Contains("Passport number") Then
                                                        vid.IDNumber = ndId.InnerText.Replace("Passport number", "").Trim
                                                    Else
                                                        vid.IDNumber = ndId.InnerText.Replace("passport number", "").Trim
                                                    End If
                                                ElseIf ndId.InnerText.Contains("German passport number ") Then
                                                    vid.IDNumber = ndId.InnerText.Replace("German passport number ", "").Trim
                                                ElseIf ndId.InnerText.Contains("Kenyan passport number ") Then
                                                    vid.IDNumber = ndId.InnerText.Replace("Kenyan passport number ", "").Trim
                                                ElseIf ndId.InnerText.Contains("Kenyan identity card number ") Then
                                                    vid.IDNumber = ndId.InnerText.Replace("Kenyan identity card number ", "").Trim
                                                ElseIf ndId.InnerText.Contains("Passport ") Then
                                                    vid.IDNumber = ndId.InnerText.Replace("Passport ", "").Trim
                                                ElseIf ndId.InnerText.Contains("passport ") Then
                                                    vid.IDNumber = ndId.InnerText.Replace("passport ", "").Trim
                                                Else
                                                    vid.IDNumber = ndId.InnerText
                                                End If
                                                'tambahkan ke VerificationListIdNumberContainer List
                                                objIDNumberChildList.Add(vid)
                                            End If
                                        Next
                                    End If
                                End If
                                vm.lstVerificationList_IDNumber = objIDNumberChildList
                                If (nodeTemp.Name = "INDIVIDUAL_ADDRESS") Then
                                    If nodeTemp.HasChildNodes Then
                                        Dim ndAddList As XmlNodeList = nodeTemp.ChildNodes
                                        For Each ndAdd As XmlNode In ndAddList
                                            Dim vad As VerificationListAddressContainer = New VerificationListAddressContainer
                                            If (ndAdd.Name = "COUNTRY") Then
                                                vad.Address = ndAdd.InnerText
                                                vad.IsLocalAddress = 0
                                                vad.AddressTypeId = 1
                                                objAddressChildList.Add(vad)
                                            ElseIf (ndAdd.Name = "STREET") Then
                                                If (Not (ndAdd.NextSibling) Is Nothing) Then
                                                    If ((ndAdd.NextSibling.Name = "CITY") _
                                                                OrElse (ndAdd.NextSibling.Name = "COUNTRY")) Then
                                                        vad.Address = (ndAdd.InnerText + (" " _
                                                                    + (ndAdd.NextSibling.InnerText + (" " + ndAdd.NextSibling.InnerText))))
                                                        vad.IsLocalAddress = 0
                                                        vad.AddressTypeId = 1
                                                        objAddressChildList.Add(vad)
                                                    End If
                                                End If
                                            End If
                                        Next
                                    End If
                                End If
                                vm.lstVerificationList_Address = objAddressChildList
                            Next
                        End If

                        'nama asli diambil dari sort_key, jika kosong diisi dari first, second dan third name
                        If vm.DisplayName = "" Then
                            vm.DisplayName = Trim(strFirstName & " " & strSecondName & " " & strThirdName)
                        End If
                        vm.VerificationListCategoryId = 3
                        vm.VerificationListTypeId = 2
                        objMasterList.Add(vm)

                        'masukkan juga sebagai alias untuk nama aslinya
                        Dim va As VerificationListAliasContainer = New VerificationListAliasContainer
                        va.Name = vm.DisplayName
                        objAliasChildList.Add(va)
                    Next
                End If
            Next
            Dim nodeEntitiesList As XmlNodeList = doc.GetElementsByTagName("ENTITIES")
            For Each nodeEnt As XmlNode In nodeEntitiesList
                Dim elemEnt As XmlElement = CType(nodeEnt, XmlElement)
                If elemEnt.HasChildNodes Then
                    Dim elemEnt2 As XmlNodeList = elemEnt.GetElementsByTagName("ENTITY")
                    For Each ndEnt2 As XmlNode In elemEnt2
                        Dim vm As VerificationListMasterContainer = New VerificationListMasterContainer
                        If ndEnt2.HasChildNodes Then
                            objAliasChildList = New List(Of VerificationListAliasContainer)
                            objAddressChildList = New List(Of VerificationListAddressContainer)
                            Dim ndEnt3List As XmlNodeList = ndEnt2.ChildNodes
                            For Each ndEnt3 As XmlNode In ndEnt3List
                                Select Case (ndEnt3.Name)
                                    Case "FIRST_NAME"
                                        vm.DisplayName = ndEnt3.InnerText
                                    Case "REFERENCE_NUMBER"
                                        vm.RefId = ndEnt3.InnerText
                                    Case "LISTED_ON"
                                        vm.DateEntered = Convert.ToDateTime(ndEnt3.InnerText.Substring(0, 10))
                                    Case "COMMENTS1"
                                        If (ndEnt3.InnerText.Length > 255) Then
                                            vm.CustomRemark1 = ndEnt3.InnerText.Substring(0, 255)
                                        Else
                                            vm.CustomRemark1 = ndEnt3.InnerText
                                        End If
                                    Case "SORT_KEY_LAST_MOD"

                                        vm.DateUpdated = Sahassa.AML.Commonly.ConvertToDate("yyyy-MM-dd", ndEnt3.InnerText.Trim)
                                End Select
                                If (ndEnt3.Name = "FIRST_NAME") Then
                                    Dim val As VerificationListAliasContainer = New VerificationListAliasContainer
                                    val.Name = ndEnt3.InnerText
                                    objAliasChildList.Add(val)
                                End If
                                If (ndEnt3.Name = "ENTITY_ALIAS") Then
                                    If ndEnt3.HasChildNodes Then
                                        Dim ndEnt33List As XmlNodeList = ndEnt3.ChildNodes
                                        For Each ndEnt33 As XmlNode In ndEnt33List
                                            Dim val As VerificationListAliasContainer = New VerificationListAliasContainer
                                            If (ndEnt33.Name = "ALIAS_NAME") Then
                                                val.Name = ndEnt33.InnerText
                                                objAliasChildList.Add(val)
                                            End If
                                        Next
                                    End If
                                End If
                                If (ndEnt3.Name = "ENTITY_ADDRESS") Then
                                    If ndEnt3.HasChildNodes Then
                                        Dim ndEnt33List As XmlNodeList = ndEnt3.ChildNodes
                                        For Each ndEnt33 As XmlNode In ndEnt33List
                                            Dim vad As VerificationListAddressContainer = New VerificationListAddressContainer
                                            If (ndEnt33.Name = "STATE_PROVINCE") Then
                                                If Not (ndEnt33.NextSibling Is Nothing) Then
                                                    If (ndEnt33.NextSibling.InnerText = "COUNTRY") Then
                                                        vad.Address = (ndEnt33.InnerText + (" " + ndEnt33.NextSibling.InnerText))
                                                        vad.AddressTypeId = 1
                                                        vad.IsLocalAddress = 0
                                                        objAddressChildList.Add(vad)
                                                    End If
                                                Else
                                                    vad.Address = ndEnt33.InnerText
                                                    vad.AddressTypeId = 1
                                                    vad.IsLocalAddress = 0
                                                    objAddressChildList.Add(vad)
                                                End If
                                            End If
                                        Next
                                    End If
                                End If
                            Next
                        End If
                        vm.lstVerificationList_Address = objAddressChildList
                        vm.lstVerificationList_Alias = objAliasChildList
                        If (vm.DateOfData.ToString("yyyy-MM-dd") = "0001-01-01") Then
                            vm.DateOfData = vm.DateUpdated
                        End If
                        vm.VerificationListCategoryId = 3
                        vm.VerificationListTypeId = 2

                        If (vm.CustomRemark1 Is Nothing) Then
                            vm.CustomRemark1 = ""
                        End If
                        If (vm.CustomRemark2 Is Nothing) Then
                            vm.CustomRemark2 = ""
                        End If
                        If (vm.CustomRemark3 Is Nothing) Then
                            vm.CustomRemark3 = ""
                        End If
                        If (vm.CustomRemark4 Is Nothing) Then
                            vm.CustomRemark4 = ""
                        End If
                        If (vm.CustomRemark5 Is Nothing) Then
                            vm.CustomRemark5 = ""
                        End If
                        objMasterList.Add(vm)
                    Next
                End If
            Next
        Catch e As Exception
            Throw
        End Try
        countAllData = objMasterList.Count
        Return objMasterList
    End Function

End Class
