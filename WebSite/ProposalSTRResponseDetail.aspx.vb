Imports System.Data.SqlClient
Imports Sahassa.AML.TableAdapterHelper
Partial Class ProposalSTRResponseDetail
    Inherits Parent

    Public Enum EnProposalStatus
        bNew = 1
        InProgress
        Complated
    End Enum
    Public Enum EnProposalEventType
        WorkFlowStated = 1
        TaskStarted
        TaskCompleted
        TaskCanceledBySystem
        WorkFlowFinish
    End Enum
    Public Enum EnProposalResponse
        DilaporkankePPATKsebagaiSTR = 1
        DilaporkankePPATKsebagaiInformasi
        TidakDilaporkankePPATKsebagaiSTR
    End Enum
    Private _ProposalSTR As AMLDAL.ProposalSTR.ProposalSTRRow
    Private ReadOnly Property ProposalSTR() As AMLDAL.ProposalSTR.ProposalSTRRow
        Get
            Try
                If Not _ProposalSTR Is Nothing Then
                    Return _ProposalSTR
                Else
                    Using adapter As New AMLDAL.ProposalSTRTableAdapters.ProposalSTRTableAdapter
                        Using otable As AMLDAL.ProposalSTR.ProposalSTRDataTable = adapter.GetProposalSTRByPK(Me.PRoposalSTRID)
                            If otable.Rows.Count > 0 Then
                                _ProposalSTR = otable.Rows(0)
                                Return _ProposalSTR
                            Else
                                _ProposalSTR = Nothing
                                Return _ProposalSTR
                            End If
                        End Using
                    End Using
                End If
            Catch ex As Exception
                LogError(ex)
                Throw
            End Try
        End Get
    End Property
    Private _PRoposalSTRID As Integer
    Private ReadOnly Property PRoposalSTRID() As Integer
        Get
            Try
                Dim temp As String
                Dim jml As Integer
                If _PRoposalSTRID = 0 Then
                    temp = Request.Params("PKProposalSTRID")
                    If temp = "" Or Not IsNumeric(temp) Then
                        Throw New Exception("ProposalSTRID is invalid")
                    End If
                    'cek status proposalstrworkflowhistory kalau = bcomplate=false dan baktifworkflow=true untuk eventype=2(task started) sudah 0 maka redirect
                    Using adapter As New AMLDAL.ProposalSTRTableAdapters.ProposalSTRWorkflowHistoryTableAdapter
                        jml = adapter.CountProposalSTRWorkflowHistory(EnProposalEventType.TaskStarted, temp, False, "%" & Sahassa.AML.Commonly.SessionUserId & "%")
                        If jml = 0 Then
                            Response.Redirect("ProposalSTRResponseView.aspx", False)
                        Else
                            _PRoposalSTRID = temp
                            Return _PRoposalSTRID
                        End If
                    End Using
                Else
                    Return _PRoposalSTRID
                End If
            Catch ex As Exception
                LogError(ex)
                Throw
            End Try
        End Get
    End Property
    Private _ProposalSTRWorkFlowHistory As AMLDAL.ProposalSTR.ProposalSTRWorkflowHistoryRow
    ''' <summary>
    ''' mendapatkan current workflow history yang harus di complate(cari taskstarted dan bcomplate=false) 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ProposalSTRWorkFlowHistory() As AMLDAL.ProposalSTR.ProposalSTRWorkflowHistoryRow
        Get
            Try
                If Not _ProposalSTRWorkFlowHistory Is Nothing Then
                    Return _ProposalSTRWorkFlowHistory
                Else
                    Using adapter As New AMLDAL.ProposalSTRTableAdapters.ProposalSTRWorkflowHistoryTableAdapter
                        Using otable As AMLDAL.ProposalSTR.ProposalSTRWorkflowHistoryDataTable = adapter.GetProposalSTRWorkflowHistory(EnProposalEventType.TaskStarted, Me.PRoposalSTRID, False, "%" & Sahassa.AML.Commonly.SessionUserId & "%")
                            If otable.Rows.Count > 0 Then
                                _ProposalSTRWorkFlowHistory = otable.Rows(0)
                                Return _ProposalSTRWorkFlowHistory
                            Else
                                _ProposalSTRWorkFlowHistory = Nothing
                                Return _ProposalSTRWorkFlowHistory
                            End If
                        End Using
                    End Using
                End If
                
            Catch ex As Exception
                LogError(ex)
                Throw
            End Try
        End Get
    End Property
    Private KetParallel As String
    Private _ProposalSTRSettingWorkflow As AMLDAL.ProposalSTR.ProposalSTRSettingWorkflowRow
    ''' <summary>
    ''' Get Current workflow for current user
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ProposalSTRSettingWorkflow() As AMLDAL.ProposalSTR.ProposalSTRSettingWorkflowRow
        Get
            Try
                If Not _ProposalSTRSettingWorkflow Is Nothing Then
                    Return _ProposalSTRSettingWorkflow
                Else
                    Using adapter As New AMLDAL.ProposalSTRTableAdapters.ProposalSTRSettingWorkflowTableAdapter
                        If Not ProposalSTRWorkFlowHistory Is Nothing Then
                            Dim arrSetting() As AMLDAL.ProposalSTR.ProposalSTRSettingWorkflowRow = adapter.GetSettingWorkflow().Select("workflowstep= '" & ProposalSTRWorkFlowHistory.WorkflowStep.Replace("'", "''") & "'")
                            If arrSetting.Length > 0 Then
                                For j As Integer = 0 To arrSetting.Length - 1
                                    Dim strApprover As String = arrSetting(j).Approvers
                                    Dim arrApprover() As String = strApprover.Split("|")
                                    If arrApprover.Length > 0 Then
                                        For i As Integer = 0 To arrApprover.Length - 1
                                            If arrApprover(i) = Sahassa.AML.Commonly.SessionPkUserId Then
                                                KetParallel = "Parallel " & j + 1
                                                _ProposalSTRSettingWorkflow = arrSetting(j)
                                                Return _ProposalSTRSettingWorkflow
                                            End If
                                        Next
                                    Else
                                        KetParallel = ""
                                    End If
                                Next
                            End If
                        End If

                    End Using
                End If
                

            Catch ex As Exception
                LogError(ex)
                Throw
            End Try
        End Get
    End Property
    Private Function IsHighestWorkFlow(ByRef osqltrans As SqlTransaction) As Boolean
        Dim strworkflowstep As String = ""
        Try
            Using adapter As New AMLDAL.ProposalSTRTableAdapters.ProposalSTRSettingWorkflowTableAdapter
                SetTransaction(adapter, osqltrans)
                Dim orow() As AMLDAL.ProposalSTR.ProposalSTRSettingWorkflowRow = adapter.GetSettingWorkflow.Select("", "serialno desc")
                If orow.Length > 0 Then
                    strworkflowstep = orow(0).WorkflowStep
                    If strworkflowstep.ToLower = ProposalSTRWorkFlowHistory.WorkflowStep.ToLower Then
                        Return True
                    Else
                        Return False
                    End If

                Else
                    Return False
                End If
            End Using
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Function
    Private Function IsWorkFlowComplate(ByRef osqltrans As SqlTransaction) As Boolean
        Try
            Using adapter As New AMLDAL.ProposalSTRTableAdapters.ProposalSTRWorkflowHistoryTableAdapter
                SetTransaction(adapter, osqltrans)
                Dim jml As Integer = adapter.GetProposalSTRWorkflowHistory(EnProposalEventType.WorkFlowStated, Me.PRoposalSTRID, False, "%" & Sahassa.AML.Commonly.SessionUserId & "%").Select("workflowstep= '" & Me.ProposalSTRWorkFlowHistory.WorkflowStep & "'").Length
                If jml > 0 Then
                    Return False
                Else
                    Return True
                End If
            End Using
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Function
    Private Function GetSettingWorkFlowBerikutnya(ByRef osqltrans As SqlTransaction) As AMLDAL.ProposalSTR.ProposalSTRSettingWorkflowRow()

        Try
            'dari currentworkflow serialnya di naikin 1
            Using Adapter As New AMLDAL.ProposalSTRTableAdapters.ProposalSTRSettingWorkflowTableAdapter
                SetTransaction(Adapter, osqltrans)
                Return Adapter.GetSettingWorkflow.Select("serialno =" & Me.ProposalSTRSettingWorkflow.SerialNo + 1)
            End Using

        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Function
    Private Function GetEmailAddress(ByVal PKUserID As String) As String
        Try
            Using adapter As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                Using otable As AMLDAL.AMLDataSet.UserDataTable = adapter.GetDataByUserID(PKUserID)
                    If otable.Rows.Count > 0 Then
                        If CType(otable.Rows(0), AMLDAL.AMLDataSet.UserRow).IsUserEmailAddressNull Then
                            Return ""
                        Else
                            Return CType(otable.Rows(0), AMLDAL.AMLDataSet.UserRow).UserEmailAddress
                        End If
                    Else
                        Return ""
                    End If
                End Using
            End Using
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Function
    Private Function GetUserIDbyPK(ByVal PKUSERID As String) As String
        Try
            Using adapter As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                Using oTable As AMLDAL.AMLDataSet.UserDataTable = adapter.GetDataByUserID(PKUSERID)
                    If oTable.Rows.Count > 0 Then
                        Return CType(oTable.Rows(0), AMLDAL.AMLDataSet.UserRow).UserID
                    End If
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Sub SendEmail(ByVal StrRecipientTo As String, ByVal StrRecipientCC As String, ByVal StrSubject As String, ByVal strbody As String)
        Dim oEmail As Sahassa.AML.EMail
        Try
            oEmail = New Sahassa.AML.EMail
            oEmail.Sender = System.Configuration.ConfigurationManager.AppSettings("FromMailAddress")
            oEmail.Recipient = StrRecipientTo
            oEmail.RecipientCC = StrRecipientCC
            oEmail.Subject = StrSubject
            oEmail.Body = strbody.Replace(vbLf, "<br>")
            oEmail.SendEmail()
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Sub
    Private Function getEmailBody(ByVal serialno As Integer) As String
        Try
            Using adapter As New AMLDAL.ProposalSTRTableAdapters.ProposalSTRWorkflowEmailTemplateTableAdapter
                Dim orow() As AMLDAL.ProposalSTR.ProposalSTRWorkflowEmailTemplateRow = adapter.GetData.Select("serialno=" & serialno)
                If orow.Length > 0 Then
                    If Not orow(0).IsBody_EmailTemplateNull Then
                        Return orow(0).Body_EmailTemplate
                    Else
                        Return ""
                    End If
                Else
                    Return ""
                End If
            End Using

        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Function
    Private Function getEmailSubject(ByVal serialno As Integer) As String
        Try
            Using adapter As New AMLDAL.ProposalSTRTableAdapters.ProposalSTRWorkflowEmailTemplateTableAdapter
                Dim orow() As AMLDAL.ProposalSTR.ProposalSTRWorkflowEmailTemplateRow = adapter.GetData.Select("serialno=" & serialno)
                If orow.Length > 0 Then
                    If Not orow(0).IsSubject_EmailTemplateNull Then
                        Return orow(0).Subject_EmailTemplate
                    Else
                        Return ""
                    End If
                Else
                    Return ""
                End If
            End Using



        Catch ex As Exception
            LogError(ex)
            Throw


        End Try
    End Function
    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim osqltrans As SqlTransaction = Nothing

        Try


            'insert table proposalstrworkflowhistory set workflowtype=taskcomplated 
            Using adapter As New AMLDAL.ProposalSTRTableAdapters.ProposalSTRWorkflowHistoryTableAdapter
                osqltrans = BeginTransaction(adapter)
                adapter.Insert(Me.PRoposalSTRID, cboProposalSTRResponse.SelectedValue, EnProposalEventType.TaskCompleted, Me.ProposalSTRWorkFlowHistory.WorkflowStep, Sahassa.AML.Commonly.SessionUserId, TextBoxComment.Text.Trim, KetParallel, True, Now)
                adapter.UpdateProposalSTRBComplate(True, Me.ProposalSTRWorkFlowHistory.PK_ProposalSTRWorkflowHistoryID)
                'update statusproposalstr jadi inprogress
                Using adapterSTR As New AMLDAL.ProposalSTRTableAdapters.ProposalSTRTableAdapter
                    SetTransaction(adapterSTR, osqltrans)
                    adapterSTR.UpdateProposalSTRStatusSTR(EnProposalStatus.InProgress, Me.PRoposalSTRID)


                    If IsHighestWorkFlow(osqltrans) Then
                        If IsWorkFlowComplate(osqltrans) Then
                            'update statusworkflow jadi complate
                            adapterSTR.UpdateProposalSTRStatusSTR(EnProposalStatus.Complated, Me.PRoposalSTRID)

                            ':insert ke historyworkflo dengan status Workflowcomplate
                            adapter.Insert(Me.PRoposalSTRID, Nothing, EnProposalEventType.WorkFlowFinish, "", "", "", "", True, Now)


                        End If
                    Else
                        If IsWorkFlowComplate(osqltrans) Then
                            'Create task ke step berikutnya
                            Dim oworkflowSerialBerikutnya() As AMLDAL.ProposalSTR.ProposalSTRSettingWorkflowRow = GetSettingWorkFlowBerikutnya(osqltrans)
                            For i As Integer = 0 To oworkflowSerialBerikutnya.Length - 1
                                Dim strPKUserApprover As String = oworkflowSerialBerikutnya(0).Approvers
                                Dim arrPKUserApprover() As String = strPKUserApprover.Split("|")
                                Dim strPKNotifyUser As String = oworkflowSerialBerikutnya(0).NotifyOthers
                                Dim arrPKNOtifyUser() As String = strPKNotifyUser.Split("|")
                                Dim strEmailRecepients As String = ""
                                Dim strEmailCC As String = ""
                                Dim strEmailSubject As String = ""
                                Dim strEmailBody As String = ""

                                Dim strListUser As String = ""
                                Dim strDescription As String = ""
                                For j As Integer = 0 To arrPKUserApprover.Length - 1
                                    strListUser += GetUserIDbyPK(arrPKUserApprover(j))
                                    strEmailRecepients += GetEmailAddress(arrPKUserApprover(j))
                                    If j <> arrPKUserApprover.Length - 1 Then
                                        strListUser += ";"
                                        strEmailRecepients += ";"
                                    End If
                                Next
                                For j As Integer = 0 To arrPKNOtifyUser.Length - 1
                                    strEmailCC += GetEmailAddress(arrPKNOtifyUser(j))
                                    If j <> arrPKNOtifyUser.Length - 1 Then
                                        strEmailCC += ";"
                                    End If
                                Next
                                strEmailSubject = getEmailSubject(oworkflowSerialBerikutnya(i).SerialNo)
                                strEmailBody = getEmailBody(oworkflowSerialBerikutnya(i).SerialNo)
                                If oworkflowSerialBerikutnya.Length > 1 Then
                                    strDescription = "Parallel " & i + 1
                                Else
                                    strDescription = ""
                                End If
                                adapter.Insert(Me.PRoposalSTRID, Nothing, EnProposalEventType.TaskStarted, oworkflowSerialBerikutnya(i).WorkflowStep, strListUser, "", "", False, Now)
                                SendEmail(strEmailRecepients, strEmailCC, strEmailSubject, strEmailBody)
                            Next
                            'insert ke historyworkflow untuk task started worflow serial berikutnya

                        End If
                    End If

                    osqltrans.Commit()
                    Response.Redirect("ProposalSTRResponseView.aspx", False)
                End Using
            End Using

        Catch ex As Exception
            If Not osqltrans Is Nothing Then
                osqltrans.Rollback()
            End If
            LogError(ex)
            CValPageError.IsValid = False
            CValPageError.ErrorMessage = ex.Message
        Finally
            If Not osqltrans Is Nothing Then
                osqltrans.Dispose()
            End If
        End Try
    End Sub
    ''' <summary>
    ''' load data  proposalstr
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub LoadData()
        Try
            If Not Me.ProposalSTR Is Nothing Then
                If Not ProposalSTR.IsKasusPolisiNull Then
                    Me.TextKasusPosisi.Text = ProposalSTR.KasusPolisi
                End If
                If Not ProposalSTR.IsIndicatorMencurigakanNull Then
                    Me.TextIndikatorMencurigakan.Text = ProposalSTR.IndicatorMencurigakan
                End If
                If Not ProposalSTR.IsUnsurTKMNull Then
                    Me.TextUnsurTKM.Text = ProposalSTR.UnsurTKM
                End If
                If Not ProposalSTR.IsLainLainNull Then
                    Me.TextLainLain.Text = Me.ProposalSTR.LainLain
                End If
                If Not ProposalSTR.IsKesimpulanNull Then
                    Me.TextKesimpulan.Text = Me.ProposalSTR.Kesimpulan
                End If
            End If
            Using adapter As New AMLDAL.ProposalSTRTableAdapters.ProposalSTRResponse1TableAdapter
                cboProposalSTRResponse.DataSource = adapter.GetData()
                cboProposalSTRResponse.DataTextField = "ProposalSTRResponseDescription"
                cboProposalSTRResponse.DataValueField = "PK_ProposalSTRResponseID"
                cboProposalSTRResponse.DataBind()
            End Using
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                LoadData()
            End If

            Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

            End Using
        Catch ex As Exception
            LogError(ex)
            CValPageError.IsValid = False
            CValPageError.ErrorMessage = ex.Message
        End Try
    End Sub
End Class
