﻿Imports SahassaNettier.Entities
Imports SahassaNettier.Data
Imports AMLBLL

Partial Class VIPCodeApprovalDetailView
    Inherits Parent

    ReadOnly Property GetPk_VIPCodeApproval As Long
        Get
            If IsNumeric(Request.Params("VIPCodeApprovalID")) Then
                If Not IsNothing(Session("VIPCodeApprovalDetail.PK")) Then
                    Return CLng(Session("VIPCodeApprovalDetail.PK"))
                Else
                    Session("VIPCodeApprovalDetail.PK") = Request.Params("VIPCodeApprovalID")
                    Return CLng(Session("VIPCodeApprovalDetail.PK"))
                End If
            End If
            Return 0
        End Get
    End Property

    Sub clearSession()
        Session("VIPCodeApprovalDetail.PK") = Nothing
    End Sub

    Sub LoadDataAll()
        Using objVIPCodeApproval As SahassaNettier.Entities.VIPCodeApproval = VIPCodeBLL.getVIPCodeApprovalByPK(GetPk_VIPCodeApproval)
            If Not IsNothing(objVIPCodeApproval) Then
                If objVIPCodeApproval.FK_ModeID.GetValueOrDefault(0) = Sahassa.AML.Commonly.TypeMode.Add Then
                    LblAction.Text = "Add"
                ElseIf objVIPCodeApproval.FK_ModeID.GetValueOrDefault(0) = Sahassa.AML.Commonly.TypeMode.Edit Then
                    LblAction.Text = "Edit"
                ElseIf objVIPCodeApproval.FK_ModeID.GetValueOrDefault(0) = Sahassa.AML.Commonly.TypeMode.Delete Then
                    LblAction.Text = "Delete"
                ElseIf objVIPCodeApproval.FK_ModeID.GetValueOrDefault(0) = Sahassa.AML.Commonly.TypeMode.Activation Then
                    LblAction.Text = "Activation"
                End If
            End If

            Using objUser As User = UserBLL.GetUserByPkUserID(objVIPCodeApproval.FK_MsUserID)
                If Not IsNothing(objUser) Then
                    LblRequestBy.Text = objUser.UserName
                End If
            End Using
            LblRequestDate.Text = CDate(objVIPCodeApproval.CreatedDate).ToString("dd-MM-yyyy")


            Using objVIPCodeApprovalDetail As VIPCodeApprovalDetail = VIPCodeBLL.getVIPCodeApprovalDetailByPKApproval(GetPk_VIPCodeApproval)
                If LblAction.Text = "Add" Then
                    LoadNewData()
                ElseIf LblAction.Text = "Edit" Then
                    LoadNewData()
                    LoadOldData(objVIPCodeApprovalDetail.PK_VIPCode_ID)
                    compareDataByContainer(PanelNew, PanelOld)
                ElseIf LblAction.Text = "Delete" Then
                    LoadOldData(objVIPCodeApprovalDetail.PK_VIPCode_ID)
                ElseIf LblAction.Text = "Activation" Then
                    LoadNewData()
                    LoadOldData(objVIPCodeApprovalDetail.PK_VIPCode_ID)
                    compareDataByContainer(PanelNew, PanelOld)
                End If
            End Using


        End Using

    End Sub

    Sub LoadNewData()
        Using objVIPCodeApprovalDetail As VIPCodeApprovalDetail = VIPCodeBLL.getVIPCodeApprovalDetailByPKApproval(GetPk_VIPCodeApproval)
            If Not IsNothing(objVIPCodeApprovalDetail) Then
                txtVIPCode.Text = objVIPCodeApprovalDetail.VIPCode.ToString
                txtDescription.Text = objVIPCodeApprovalDetail.VIPCodeDescription.ToString
                txtActivation.Text = objVIPCodeApprovalDetail.Activation.GetValueOrDefault(0).ToString
                PanelNew.Visible = True
            End If
        End Using
    End Sub

    Sub LoadOldData(ByVal PK_VIPCode_ID As Long)
        Using objVIPCode As VIPCode = VIPCodeBLL.getVIPCodeByPk(PK_VIPCode_ID)
            If Not IsNothing(objVIPCode) Then
                old_txtVIPCode.Text = objVIPCode.VIPCODE.ToString
                old_txtDescription.Text = objVIPCode.VIPCodeDescription.ToString
                old_txtActivation.Text = objVIPCode.Activation.GetValueOrDefault(0).ToString
                PanelOld.Visible = True
            End If
        End Using
    End Sub


    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                clearSession()
                mtvApproval.ActiveViewIndex = 0

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using

                LoadDataAll()
            End If
        Catch ex As Exception
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImgBtnAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAccept.Click
        Try
            If LblAction.Text = "Add" Then
                If VIPCodeBLL.acceptAddOrDelete(GetPk_VIPCodeApproval, "Add") Then
                    mtvApproval.ActiveViewIndex = 1
                    lblMessage.Text = "Adding Data Approved"
                    AjaxPanel1.Visible = False
                End If
            ElseIf LblAction.Text = "Edit" Then
                If VIPCodeBLL.acceptEditOrActivate(GetPk_VIPCodeApproval, "Edit") Then
                    mtvApproval.ActiveViewIndex = 1
                    lblMessage.Text = "Editing Data Approved"
                    AjaxPanel1.Visible = False
                End If
            ElseIf LblAction.Text = "Delete" Then
                If VIPCodeBLL.acceptAddOrDelete(GetPk_VIPCodeApproval, "Delete") Then
                    mtvApproval.ActiveViewIndex = 1
                    lblMessage.Text = "Deleting Data Approved"
                    AjaxPanel1.Visible = False
                End If
            ElseIf LblAction.Text = "Activation" Then
                If VIPCodeBLL.acceptEditOrActivate(GetPk_VIPCodeApproval, "Activation") Then
                    mtvApproval.ActiveViewIndex = 1
                    lblMessage.Text = "Activation Data Approved"
                    AjaxPanel1.Visible = False
                End If
            End If
        Catch ex As Exception
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub ImgBtnReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnReject.Click
        Try
            If LblAction.Text = "Add" Then
                If VIPCodeBLL.rejectAddOrDelete(GetPk_VIPCodeApproval, "Add") Then
                    mtvApproval.ActiveViewIndex = 1
                    lblMessage.Text = "Adding Data Rejeceted"
                    AjaxPanel1.Visible = False
                End If
            ElseIf LblAction.Text = "Edit" Then
                If VIPCodeBLL.rejectEditOrActivate(GetPk_VIPCodeApproval, "Edit") Then
                    mtvApproval.ActiveViewIndex = 1
                    lblMessage.Text = "Editing Data Rejeceted"
                    AjaxPanel1.Visible = False
                End If
            ElseIf LblAction.Text = "Delete" Then
                If VIPCodeBLL.rejectAddOrDelete(GetPk_VIPCodeApproval, "Delete") Then
                    mtvApproval.ActiveViewIndex = 1
                    lblMessage.Text = "Deleting Data Rejeceted"
                    AjaxPanel1.Visible = False
                End If
            ElseIf LblAction.Text = "Activation" Then
                If VIPCodeBLL.rejectEditOrActivate(GetPk_VIPCodeApproval, "Activation") Then
                    mtvApproval.ActiveViewIndex = 1
                    lblMessage.Text = "Activation Data Rejeceted"
                    AjaxPanel1.Visible = False
                End If
            End If
        Catch ex As Exception
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImgBackAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBackAdd.Click
        Try
            Response.Redirect("VIPCodeApproval.aspx")
        Catch ex As Exception
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub btnOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnOK.Click
        Try
            Response.Redirect("VIPCodeApproval.aspx")
        Catch ex As Exception
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Sub compareDataByContainer(ByRef containerNew As Control, ByRef containerOld As Control)
        Dim index As Integer = 0
        For Each item As Control In containerNew.Controls
            If TypeOf (item) Is Label Then

                If CType(item, Label).Text.ToLower <> "old value" And CType(item, Label).Text.ToLower <> "new value" Then
                    If CType(item, Label).Text <> CType(containerOld.Controls(index), Label).Text Then
                        CType(item, Label).ForeColor = Drawing.Color.Red
                        CType(containerOld.Controls(index), Label).ForeColor = Drawing.Color.Red
                    End If
                End If
            End If
            index = index + 1
        Next
    End Sub
End Class
