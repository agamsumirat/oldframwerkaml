<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ReportOverBookingTunai.aspx.vb" Inherits="ReportOverBookingTunai" title="Reprt Over Booking Tunai" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    <table cellspacing="0" cellpadding="0" width="100%">
        <tr class="MainTitle">
            <td bgcolor="#ffffff" colspan="4">
                <img src="Images/dot_title.gif" width="17" height="17">&nbsp; <strong><span style="font-size: 18px">
                    Report Over Booking Di Process Tunai</span></strong>&nbsp;
                <hr />
            </td>
        </tr>
        <tr>
            <td height="20px" colspan="4">
                <table width="100%">
                    <tr>
                        <td style="width: 200px">
                            Transaction Date</td>
                        <td style="width: 1px">
                            :</td>
                        <td style="width: 80%">
                            <asp:TextBox ID="TxtStartDate" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox><input
                                id="popUpStartDate" runat="server" name="popUpCalc" style="border-right: #ffffff 0px solid;
                                border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
                                border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
                                height: 17px" title="Click to show calendar" type="button" />
                            to<asp:TextBox ID="TxtEndDate" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox><input
                                id="PopUpEndDate" runat="server" name="popUpCalcEnd" style="border-right: #ffffff 0px solid;
                                border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
                                border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
                                height: 17px" title="Click to show calendar" type="button" /></td>
                    </tr>
                    <tr>
                        <td>
                            Account No</td>
                        <td>
                            :</td>
                        <td style="width: 80%">
                            <asp:TextBox ID="TxtAccountNo" runat="server" CssClass="searcheditbox" EnableViewState="False"
                                MaxLength="100" TabIndex="2" Width="200px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ImageButton ID="ImageButtonSearch" runat="server" ImageUrl="~/Images/button/search.gif"
                                TabIndex="3" /></td>
                        <td>
                        </td>
                        <td style="width: 80%">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        
          </table>
          
    <asp:Panel ID="PanelData" runat="server" Visible="false">
         <table>
        
        <tr class="formText">
            <td colspan="4" bgcolor="#dddddd" height="20px">
                &nbsp;&nbsp;<b><asp:Label ID="Label2" runat="server" Text="DISPLAY COLUMN"></asp:Label></b>
            </td>
        </tr>
        <tr>
            <td height="8px" colspan="4">
            </td>
        </tr>
        <tr>
            <td colspan="4" valign="top">
                <ajax:AjaxPanel ID="AjaxPanel1" runat="server">
                    <asp:DataList ID="FieldList" runat="server" RepeatColumns="5">
                        <ItemTemplate>
                            <asp:CheckBox ID="ChkField" runat="server" Text='<%# Bind("column_name") %>' />
                        </ItemTemplate>
                    </asp:DataList>&nbsp;
                    <asp:Panel ID="Panel2" runat="server" GroupingText="Select All Field" Width="200px">
                        <asp:CheckBox ID="ChkSelectAll" runat="server" AutoPostBack="True" Text="Select All Field" /></asp:Panel>
                </ajax:AjaxPanel>
            </td>
        </tr>
        <tr>
            <td height="20px" colspan="4">
            </td>
        </tr>
        <tr class="formText">
            <td colspan="4" bgcolor="#dddddd" height="20px">
                &nbsp;&nbsp;<b><asp:Label ID="Label3" runat="server" Text="FILTER CRITERIA"></asp:Label></b>
            </td>
        </tr>
        <tr>
            <td height="20px" colspan="4">
            </td>
        </tr>
        <tr>
            <td colspan="4" width="100%" align="left">
                <asp:Panel ID="Panel1" runat="server" GroupingText="Field" Width="100%">
                    <table style="width: 100%" align="left">
                        <tr align="left">
                            <td valign="top" width="1%" align="left">
                                <ajax:AjaxPanel ID="AjaxPanel11" runat="server">
                                    <asp:Panel ID="PanelAndOr" runat="server" Visible="False">
                                        <asp:DropDownList ID="CboAndOR" runat="server" CssClass="combobox">
                                            <asp:ListItem>AND</asp:ListItem>
                                            <asp:ListItem>OR</asp:ListItem>
                                        </asp:DropDownList></asp:Panel>
                                    &nbsp;</ajax:AjaxPanel>
                            </td>
                            <td valign="top" width="7%" align="left">
                                <ajax:AjaxPanel ID="AjaxPanel3" runat="server">
                                    <asp:DropDownList ID="CboFilter" runat="server" CssClass="combobox" AutoPostBack="True"
                                        Visible="False">
                                    </asp:DropDownList>&nbsp;
                                </ajax:AjaxPanel>
                            </td>
                            <td valign="top" width="3%" align="left">
                                <ajax:AjaxPanel ID="AjaxPanel4" runat="server">
                                    <asp:DropDownList ID="cboOperator" runat="server" CssClass="combobox" Visible="False"
                                        AutoPostBack="True">
                                    </asp:DropDownList>&nbsp;
                                </ajax:AjaxPanel>
                            </td>
                            <td valign="top" width="5%" align="left">
                                <ajax:AjaxPanel ID="AjaxPanel5" runat="server">
                                    <asp:Panel ID="PanelAwal" runat="server" Visible="False">
                                        <asp:TextBox ID="TxtFieldAwal" runat="server" CssClass="textbox" Width="100px"></asp:TextBox><input
                                            id="popUp1" runat="server" name="popUpCalc1" style="border-right: #ffffff 0px solid;
                                            border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
                                            border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
                                            height: 17px" title="Click to show calendar" type="button" /></asp:Panel>
                                    &nbsp;
                                </ajax:AjaxPanel>
                            </td>
                            <td valign="top" width="2%" align="left">
                            <ajax:AjaxPanel ID="AjaxPanel6" runat="server">
                                <asp:Panel ID="PanelAnd" runat="server" Visible="False">
                                    <asp:Label ID="LblAnd" runat="server" Text="And"></asp:Label></asp:Panel>
                                &nbsp;
                                </ajax:AjaxPanel>
                            </td>
                            <td valign="top" width="5%" align="left">
                            <ajax:AjaxPanel ID="AjaxPanel7" runat="server">
                                <asp:Panel ID="PanelAkhir" runat="server" Visible="False">
                                    <asp:TextBox ID="TxtFieldAkhir" runat="server" CssClass="textbox" Width="100px"></asp:TextBox><input
                                        id="popUp2" runat="server" name="popUpCalc2" style="border-right: #ffffff 0px solid;
                                        border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
                                        border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
                                        height: 17px" title="Click to show calendar" type="button" /></asp:Panel>
                                &nbsp;
                                  </ajax:AjaxPanel>
                            </td>
                            <td valign="top" width="99%" align="left">
                            <ajax:AjaxPanel ID="AjaxPanel8" runat="server">
                                <asp:ImageButton ID="ImageAddFilter" runat="server" ImageUrl="~/images/button/add.gif"
                                    Visible="False" />
                                    </ajax:AjaxPanel>
                            </td>
                        </tr>
                        <tr>
                        <td valign="top" width="100%" align="left" colspan="7">
                        <ajax:AjaxPanel ID="AjaxPanel10" runat="server">
                            <asp:GridView ID="GridViewList" runat="server" AllowSorting="True" BackColor="White"
                        BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black"
                        EnableTheming="False" DataKeyNames="pk" AutoGenerateColumns="false">
                        <RowStyle BackColor="#F7F7DE" VerticalAlign="Top" Wrap="False" />
                        <FooterStyle BackColor="#CCCC99" />
                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                        <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                        <AlternatingRowStyle BackColor="White" />
                        
                                <Columns>
                                    <asp:BoundField DataField="AndOr" HeaderText="And Or" />
                                    <asp:BoundField DataField="columnName" HeaderText="ColumnName" />
                                    <asp:BoundField DataField="Operator" HeaderText="Operator" />
                                    <asp:BoundField DataField="nilai" HeaderText="Value" />
                                    <asp:CommandField ShowDeleteButton="True" />
                                </Columns>
                            </asp:GridView>
                            </ajax:AjaxPanel>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td height="20px" colspan="4">
            </td>
        </tr>
        <tr class="formText">
            <td colspan="4" bgcolor="#dddddd" height="30px">
                <ajax:AjaxPanel ID="AjaxPanel9" runat="server">
                    &nbsp;&nbsp;<img height="15" src="images/arrow.gif" width="15">
                    &nbsp;&nbsp;<asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/button/generate.gif" /></ajax:AjaxPanel>
            </td>
        </tr>
        <tr>
            <td height="20px" colspan="4">
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <ajax:AjaxPanel ID="AjaxPanel12" runat="server">
                    <asp:GridView ID="GridViewQuery" runat="server" AllowSorting="True" BackColor="White"
                        BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black"
                        EnableTheming="False" Width="100%">
                        <RowStyle BackColor="#F7F7DE" VerticalAlign="Top" Wrap="False" />
                        <FooterStyle BackColor="#CCCC99" />
                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                        <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" Wrap="false" />
                        <AlternatingRowStyle BackColor="White" />
                        <EmptyDataTemplate>
                            There is no Matching Record
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField>
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="LblNo" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <asp:Panel ID="PanelNavigasi" runat="server" Visible="false">
                        <table id="table_3" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                            bgcolor="#ffffff" border="2">
                            <tr class="regtext" align="center" bgcolor="#dddddd">
                                <td valign="top" align="left" width="50%" bgcolor="#ffffff">
                                    Page &nbsp;
                                    <ajax:AjaxPanel ID="ajaxPanel_TotalPages" runat="server">
                                        <asp:Label ID="label_CurrentPage" runat="server" CssClass="regtext">
                            0
                                        </asp:Label>
                                        &nbsp;of&nbsp;
                                        <asp:Label ID="label_TotalPages" runat="server" CssClass="regtext">
                            0
                                        </asp:Label>
                                    </ajax:AjaxPanel>
                                </td>
                                <td valign="top" align="right" width="50%" bgcolor="#ffffff">
                                    Total Records &nbsp;
                                    <ajax:AjaxPanel ID="ajaxPanel_TotalRows" runat="server">
                                        <asp:Label ID="label_TotalRows" runat="server">
                            0
                                        </asp:Label>
                                    </ajax:AjaxPanel>
                                </td>
                            </tr>
                        </table>
                        <table id="table_4" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                            bgcolor="#ffffff" border="2">
                            <tr bgcolor="#ffffff">
                                <td class="regtext" valign="middle" align="left" colspan="11" height="7">
                                    <hr color="#f40101" noshade size="1">
                                    Page Size
                                    <ajax:AjaxPanel ID="ajaxPanel13" runat="server">
                                        <asp:DropDownList ID="CboPageSize" runat="server" CssClass="combobox" AutoPostBack="True">
                                            <asp:ListItem>10</asp:ListItem>
                                            <asp:ListItem>20</asp:ListItem>
                                            <asp:ListItem>30</asp:ListItem>
                                            <asp:ListItem>40</asp:ListItem>
                                            <asp:ListItem>50</asp:ListItem>
                                            <asp:ListItem>75</asp:ListItem>
                                            <asp:ListItem>100</asp:ListItem>
                                        </asp:DropDownList>
                                    </ajax:AjaxPanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="regtext" valign="middle" align="left" width="63" bgcolor="#ffffff">
                                    Go to page
                                </td>
                                <td class="regtext" valign="middle" align="left" width="5" bgcolor="#ffffff">
                                    <font face="Verdana, Arial, Helvetica, sans-serif" size="1">
                                        <ajax:AjaxPanel ID="ajaxPanel_GoTo" runat="server">
                                            <asp:TextBox ID="textBox_GoToPage" runat="server" Width="38px" CssClass="searcheditbox"></asp:TextBox>
                                        </ajax:AjaxPanel>
                                    </font>
                                </td>
                                <td class="regtext" valign="middle" align="left" bgcolor="#ffffff">
                                    <ajax:AjaxPanel ID="ajaxPanel_Go" runat="server">
                                        <asp:ImageButton ID="imageButton_Go" runat="server" ImageUrl="~/images/button/go.gif">
                                        </asp:ImageButton>
                                    </ajax:AjaxPanel>
                                </td>
                                <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                                    <img height="5" src="images/first.gif" width="6">
                                </td>
                                <td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff">
                                    <ajax:AjaxPanel ID="ajaxPanel_First" runat="server">
                                        <asp:LinkButton ID="linkButton_First" runat="server" CssClass="regtext" CommandName="First"
                                            OnCommand="PageNavigate">
                            First
                                        </asp:LinkButton>
                                    </ajax:AjaxPanel>
                                </td>
                                <td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff">
                                    <img height="5" src="images/prev.gif" width="6">
                                </td>
                                <td class="regtext" valign="middle" align="right" width="14" bgcolor="#ffffff">
                                    <ajax:AjaxPanel ID="ajaxPanel_Previous" runat="server">
                                        <asp:LinkButton ID="linkButton_Previous" runat="server" CssClass="regtext" CommandName="Prev"
                                            OnCommand="PageNavigate">
                            Previous
                                        </asp:LinkButton>
                                    </ajax:AjaxPanel>
                                </td>
                                <td class="regtext" valign="middle" align="right" width="60" bgcolor="#ffffff">
                                    <ajax:AjaxPanel ID="ajaxPanel_Next" runat="server">
                                        <a class="pageNav" href="#">
                                            <asp:LinkButton ID="linkButton_Next" runat="server" CssClass="regtext" CommandName="Next"
                                                OnCommand="PageNavigate">
                                Next
                                            </asp:LinkButton>
                                        </a>
                                    </ajax:AjaxPanel>
                                </td>
                                <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                                    <img height="5" src="images/next.gif" width="6">
                                </td>
                                <td class="regtext" valign="middle" align="left" width="25" bgcolor="#ffffff">
                                    <ajax:AjaxPanel ID="ajaxPanel_Last" runat="server">
                                        <asp:LinkButton ID="linkButton_Last" runat="server" CssClass="regtext" CommandName="Last"
                                            OnCommand="PageNavigate">
                            Last
                                        </asp:LinkButton>
                                    </ajax:AjaxPanel>
                                </td>
                                <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                                    <img height="5" src="images/last.gif" width="6">
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </ajax:AjaxPanel>
            </td>
        </tr>
        <tr class="formText">
            <td colspan="4" bgcolor="#dddddd" height="30px" valign="middle">
                &nbsp;&nbsp;<img height="15" src="images/arrow.gif" width="15">
                &nbsp;&nbsp;<b>Export Report to : </b>&nbsp;<asp:DropDownList ID="CboType" runat="server"
                    CssClass="combobox">
                    <asp:ListItem Value="0">Excel</asp:ListItem>
                    <asp:ListItem Value="1">Word</asp:ListItem>
                    <asp:ListItem Value="2">PDF</asp:ListItem>
                    <asp:ListItem Value="3">CSV</asp:ListItem>
                </asp:DropDownList>
                &nbsp;
                <asp:ImageButton ID="ImageExportWord" runat="server" ImageUrl="~/images/button/export.gif" />
            </td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" colspan="2" height="24">
                <asp:CustomValidator ID="CValPageError" runat="server" Display="None"></asp:CustomValidator>&nbsp;
                <asp:CustomValidator ID="CvalHandleErr" runat="server" Display="None" ValidationGroup="handle"></asp:CustomValidator>
            </td>
        </tr>
    </table>
    </asp:Panel>
          
   
    <script src="script/popcalendar_crypt.js"></script>

</asp:Content>
