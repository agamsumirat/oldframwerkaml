Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper
Imports AMLBLL
Partial Class ChangePassword
    Inherits Parent

    ''' <summary>
    ''' get id for focues
    ''' </summary>
    ''' <value></value>
    ''' <returns>element control id</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property GetFocusID() As String
        Get
            Return Me.TextOldPassword.ClientID
        End Get
    End Property

    ''' <summary>
    ''' get user id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetPkUserId() As Int64
        Get
            Return Sahassa.AML.Commonly.SessionPkUserId
        End Get
    End Property

    Private Function CheckOldPasswordIsOK() As Boolean
        Dim salt As String = Session("ChangePasswordSalt")

        Using AccessUser As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
            Dim UserTable As Data.DataTable = AccessUser.GetDataByUserID(GetPkUserId)
            If UserTable.Rows.Count > 0 Then
                Dim UserRow As AMLDAL.AMLDataSet.UserRow = UserTable.Rows(0)

                If Sahassa.AML.Commonly.Encrypt(Me.TextOldPassword.Text, salt) = UserRow.UserPassword Then
                    Return True
                Else
                    Return False
                End If
            Else
                Throw New Exception("Cannot load ")
            End If
        End Using
    End Function

    ''' <summary>
    ''' save button handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Page.Validate()
        Dim oTrans As SqlTransaction = Nothing
        If Page.IsValid Then
            Try
                If Me.CheckOldPasswordIsOK Then
                    Dim MinimumPasswordLength As Int32 = Me.GetMinimumPasswordLength
                    If Me.TextNewPassword.Text.Length < MinimumPasswordLength Then
                        Throw New Exception("Cannot change password for the following User : " & Me.LabelUserId.Text & " because the minimum password length is " & MinimumPasswordLength & " characters")
                    Else
                        Dim PasswordRecycleCount As Int32 = 3
                        LoginParameterBLL.CekPasswordChar(Me.TextNewPassword.Text.Trim)
                        LoginParameterBLL.CekPasswordCombination(Me.TextNewPassword.Text.Trim)
                        'Encrypt password baru
                        Dim salt As String = Session("ChangePasswordSalt")
                        Dim NewPassword As String = Sahassa.AML.Commonly.Encrypt(Me.TextNewPassword.Text, salt)

                        'Periksa apakah password itu melanggar aturan Password Recycle Count atau tidak
                        Using AccessLoginParameter As New AMLDAL.AMLDataSetTableAdapters.LoginParameterTableAdapter
                            Using LoginParamterTable As AMLDAL.AMLDataSet.LoginParameterDataTable = AccessLoginParameter.GetData

                                If LoginParamterTable.Rows.Count <> 0 Then
                                    Dim LoginParameterRow As AMLDAL.AMLDataSet.LoginParameterRow = LoginParamterTable.Rows(0)
                                    PasswordRecycleCount = LoginParameterRow.PasswordRecycleCount
                                End If
                            End Using
                        End Using

                        If Me.CheckIfPasswordIsOK(Me.LabelUserId.Text, NewPassword, PasswordRecycleCount) Then

                            Using AccessHistoryPassword As New AMLDAL.AMLDataSetTableAdapters.HistoryPasswordTableAdapter
                                oTrans = BeginTransaction(AccessHistoryPassword)
                                AccessHistoryPassword.Insert(Me.LabelUserId.Text, NewPassword, Now)
                            End Using

                            Using AccessUser As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                                SetTransaction(AccessUser, oTrans)
                                AccessUser.ChangeUserPassword(Me.LabelUserId.Text, NewPassword, Now)
                            End Using

                            oTrans.Commit()

                            Dim MessagePendingID As Integer = 1001 'MessagePendingID 1001 = Change Password  
                            Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & Me.LabelUserId.Text

                            Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & Me.LabelUserId.Text, False)
                        Else
                            Throw New Exception("Cannot change password for the following User : " & Me.LabelUserId.Text & " because the new password violates the Password Recycle Count rule.")
                        End If
                    End If
                Else
                    Throw New Exception("Old Password is incorrect.")
                End If
            Catch ex As Exception
                If Not oTrans Is Nothing Then oTrans.Rollback()
                Me.cvalPageError.IsValid = False
                Me.cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            Finally
                If Not oTrans Is Nothing Then
                    oTrans.Dispose()
                    oTrans = Nothing
                End If
            End Try
        End If
    End Sub

    ''' <summary>
    ''' filledit data
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillEditData()
        Using AccessUser As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
            Using TableUser As AMLDAL.AMLDataSet.UserDataTable = AccessUser.GetDataByUserID(Me.GetPkUserId)
                If TableUser.Rows.Count > 0 Then
                    Dim TableRowUser As AMLDAL.AMLDataSet.UserRow = TableUser.Rows(0)
                    Session("ChangePasswordUserID_Old") = TableRowUser.UserID
                    Me.LabelUserId.Text = Session("ChangePasswordUserID_Old")

                    Session("ChangePasswordUserName_Old") = TableRowUser.UserName
                    Me.LabelUserName.Text = Session("ChangePasswordUserName_Old")

                    Session("ChangePasswordSalt") = TableRowUser.UserPasswordSalt
                Else
                    Throw New Exception("Cannot load data from the following User: " & Me.LabelUserId.Text & " because that User no longer exists in the database.")
                End If
            End Using
        End Using
    End Sub

    ''' <summary>
    ''' page load event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.FillEditData()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' cancel event handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "Settings.aspx"

        Me.Response.Redirect("Settings.aspx", False)
    End Sub


    ''' <summary>
    ''' Get Minimum Password Length
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetMinimumPasswordLength() As Int32
        Using AccessLoginParameter As New AMLDAL.AMLDataSetTableAdapters.LoginParameterTableAdapter
            Using LoginParameterTable As AMLDAL.AMLDataSet.LoginParameterDataTable = AccessLoginParameter.GetData

                If LoginParameterTable.Rows.Count = 0 Then
                    Return 6 'Default minimum password length = 6
                Else
                    Dim LoginParameterRow As AMLDAL.AMLDataSet.LoginParameterRow = LoginParameterTable.Rows(0)
                    Return LoginParameterRow.MinimumPasswordLength
                End If
            End Using
        End Using
    End Function

    Private Function CheckIfPasswordIsOK(ByVal UserId As String, ByVal EncryptedPassword As String, ByVal PasswordRecycleCount As Int32) As Boolean
        Try
            Dim connectionString As String = WebConfigurationManager.ConnectionStrings("AMLConnectionString").ConnectionString
            Dim con As New SqlConnection(connectionString)
            Dim QueryString As String
            'QueryString = String.Format("select count(*) from (select top {0} HistoryPasswordUserPassword from HistoryPassword order by HistoryPasswordEntryDate desc)a where HistoryPasswordUserPassword = '{1}'", PasswordRecycleCount, EncryptedPassword)
            QueryString = String.Format("select count(*) from (select top {0} HistoryPasswordUserPassword from HistoryPassword where HistoryPasswordUserId='{1}' order by HistoryPasswordEntryDate desc)a where HistoryPasswordUserPassword = '{2}'", PasswordRecycleCount, UserId, EncryptedPassword)

            Dim cmd As New SqlCommand()
            Dim count As Int32

            cmd.Connection = con
            cmd.CommandType = CommandType.Text
            cmd.CommandText = QueryString

            con.Open()
            count = CType(cmd.ExecuteScalar(), Int32)
            con.Close()

            If count > 0 Then
                Return False
            Else
                Return True
            End If
        Catch
            Throw
        End Try
    End Function
End Class