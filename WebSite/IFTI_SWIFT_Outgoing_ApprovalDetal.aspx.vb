﻿#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports AMLBLL.AuditTrailBLL
Imports SahassaNettier.Entities
Imports SahassaNettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports Sahassa.AML
Imports System.Collections.Generic
Imports System.Data.SqlClient
#End Region

Partial Class IFTI_SWIFT_Outgoing_ApprovalDetal
    Inherits Parent
    Private ReadOnly BindGridFromExcel As Boolean
#Region "Property"

    Public ReadOnly Property SetnGetUserID() As String
        Get
            Return SessionPkUserId
        End Get

    End Property

    Private Property GetTotalInsert() As Double
        Get
            Return Session("GetTotalInsert")
        End Get
        Set(ByVal value As Double)
            Session("GetTotalInsert") = value
        End Set
    End Property

    Private Property GetTotalUpdate() As Double
        Get
            Return Session("GetTotalUpdate")
        End Get
        Set(ByVal value As Double)
            Session("GetTotalUpdate") = value
        End Set
    End Property

    Public ReadOnly Property parID As String
        Get
            Return Request.Params("PK_IFTI_Approval_Id")
        End Get
    End Property

    Public Property SetIgnoreID() As String
        Get
            If Not Session("IFTI_ApprovalDetail.setPK_MsIgnoreList_Approval_Id") Is Nothing Then
                Return CStr(Session("IFTI_ApprovalDetail.setPK_MsIgnoreList_Approval_Id"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("MsIgnoreList_ApprovalDetail.setPK_MsIgnoreList_Approval_Id") = value
        End Set
    End Property

    Public Property SetIgnoreKeyword() As String
        Get
            If Not Session("MsIgnoreList_ApprovalDetail.SetIgnoreKeyword") Is Nothing Then
                Return CStr(Session("MsIgnoreList_ApprovalDetail.SetIgnoreKeyword"))
            Else
                Return ""
            End If

        End Get
        Set(ByVal value As String)
            Session("MsIgnoreList_ApprovalDetail.SetIgnoreKeyword") = value
        End Set
    End Property

    Private Property SetnGetSort() As String
        Get
            Return CType(IIf(Session("IFTI_Approval_Detail.Sort") Is Nothing, " FK_IFTI_Approval_Id  asc", Session("IFTI_Approval_Detail.Sort")), String)
        End Get
        Set(ByVal Value As String)
            Session("IFTI_Approval_Detail.Sort") = Value
        End Set
    End Property

    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return CType(IIf(Session("IFTI_Approval_Detail.SelectedItem") Is Nothing, New ArrayList, Session("IFTI_Approval_Detail.SelectedItem")), ArrayList)
        End Get
        Set(ByVal value As ArrayList)
            Session("IFTI_Approval_Detail.SelectedItem") = value
        End Set
    End Property

    Private Property SetnGetRowTotal() As Int32
        Get
            Return CType(IIf(Session("IFTI_Approval_Detail.RowTotal") Is Nothing, 0, Session("IFTI_Approval_Detail.RowTotal")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("IFTI_Approval_Detail.RowTotal") = Value
        End Set
    End Property

    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return iTotalPages(SetnGetRowTotal)
        End Get
    End Property

    Private Property SetnGetCurrentPage() As Int32
        Get
            Return CType(IIf(Session("IFTI_Approval_Detail.CurrentPage") Is Nothing, 0, Session("IFTI_Approval_Detail.CurrentPage")), Int32)
        End Get
        Set(ByVal Value As Int32)
            Session("IFTI_Approval_Detail.CurrentPage") = Value
        End Set
    End Property

    Private Sub LoadIDType()
        Me.CboB1PengirimNasInd_jenisidentitasNew.Items.Clear()
        Me.CboB1PengirimNasInd_jenisidentitasNew.Items.Add("-Select-")
        Me.CboB1PengirimNasInd_jenisidentitasNew.AppendDataBoundItems = True
        Me.CboB1PengirimNasInd_jenisidentitasNew.DataSource = DataRepository.IFTI_IDTypeProvider.GetAll()
        Me.CboB1PengirimNasInd_jenisidentitasNew.DataTextField = "IDType"
        Me.CboB1PengirimNasInd_jenisidentitasNew.DataValueField = "PK_IFTI_IDType"
        Me.CboB1PengirimNasInd_jenisidentitasNew.DataBind()

        Me.CboB1PengirimNasInd_jenisidentitasOld.Items.Clear()
        Me.CboB1PengirimNasInd_jenisidentitasOld.Items.Add("-Select-")
        Me.CboB1PengirimNasInd_jenisidentitasOld.AppendDataBoundItems = True
        Me.CboB1PengirimNasInd_jenisidentitasOld.DataSource = DataRepository.IFTI_IDTypeProvider.GetAll()
        Me.CboB1PengirimNasInd_jenisidentitasOld.DataTextField = "IDType"
        Me.CboB1PengirimNasInd_jenisidentitasOld.DataValueField = "PK_IFTI_IDType"
        Me.CboB1PengirimNasInd_jenisidentitasOld.DataBind()

        Me.CboB1PengirimNonNasabah_JenisDokumenNew.Items.Clear()
        Me.CboB1PengirimNonNasabah_JenisDokumenNew.Items.Add("-Select-")
        Me.CboB1PengirimNonNasabah_JenisDokumenNew.AppendDataBoundItems = True
        Me.CboB1PengirimNonNasabah_JenisDokumenNew.DataSource = DataRepository.IFTI_IDTypeProvider.GetAll()
        Me.CboB1PengirimNonNasabah_JenisDokumenNew.DataTextField = "IDType"
        Me.CboB1PengirimNonNasabah_JenisDokumenNew.DataValueField = "PK_IFTI_IDType"
        Me.CboB1PengirimNonNasabah_JenisDokumenNew.DataBind()

        Me.CboB1PengirimNonNasabah_JenisDokumenOld.Items.Clear()
        Me.CboB1PengirimNonNasabah_JenisDokumenOld.Items.Add("-Select-")
        Me.CboB1PengirimNonNasabah_JenisDokumenOld.AppendDataBoundItems = True
        Me.CboB1PengirimNonNasabah_JenisDokumenOld.DataSource = DataRepository.IFTI_IDTypeProvider.GetAll()
        Me.CboB1PengirimNonNasabah_JenisDokumenOld.DataTextField = "IDType"
        Me.CboB1PengirimNonNasabah_JenisDokumenOld.DataValueField = "PK_IFTI_IDType"
        Me.CboB1PengirimNonNasabah_JenisDokumenOld.DataBind()

        Me.CboB2PengirimNasInd_jenisidentitasNew.Items.Clear()
        Me.CboB2PengirimNasInd_jenisidentitasNew.Items.Add("-Select-")
        Me.CboB2PengirimNasInd_jenisidentitasNew.AppendDataBoundItems = True
        Me.CboB2PengirimNasInd_jenisidentitasNew.DataSource = DataRepository.IFTI_IDTypeProvider.GetAll()
        Me.CboB2PengirimNasInd_jenisidentitasNew.DataTextField = "IDType"
        Me.CboB2PengirimNasInd_jenisidentitasNew.DataValueField = "PK_IFTI_IDType"
        Me.CboB2PengirimNasInd_jenisidentitasNew.DataBind()

        Me.CboB2PengirimNasInd_jenisidentitasOld.Items.Clear()
        Me.CboB2PengirimNasInd_jenisidentitasOld.Items.Add("-Select-")
        Me.CboB2PengirimNasInd_jenisidentitasOld.AppendDataBoundItems = True
        Me.CboB2PengirimNasInd_jenisidentitasOld.DataSource = DataRepository.IFTI_IDTypeProvider.GetAll()
        Me.CboB2PengirimNasInd_jenisidentitasOld.DataTextField = "IDType"
        Me.CboB2PengirimNasInd_jenisidentitasOld.DataValueField = "PK_IFTI_IDType"
        Me.CboB2PengirimNasInd_jenisidentitasOld.DataBind()

        Me.CBOBenfOwnerNasabah_JenisDokumenNew.Items.Clear()
        Me.CBOBenfOwnerNasabah_JenisDokumenNew.Items.Add("-Select-")
        Me.CBOBenfOwnerNasabah_JenisDokumenNew.AppendDataBoundItems = True
        Me.CBOBenfOwnerNasabah_JenisDokumenNew.DataSource = DataRepository.IFTI_IDTypeProvider.GetAll()
        Me.CBOBenfOwnerNasabah_JenisDokumenNew.DataTextField = "IDType"
        Me.CBOBenfOwnerNasabah_JenisDokumenNew.DataValueField = "PK_IFTI_IDType"
        Me.CBOBenfOwnerNasabah_JenisDokumenNew.DataBind()

        Me.CBOBenfOwnerNasabah_JenisDokumenOld.Items.Clear()
        Me.CBOBenfOwnerNasabah_JenisDokumenOld.Items.Add("-Select-")
        Me.CBOBenfOwnerNasabah_JenisDokumenOld.AppendDataBoundItems = True
        Me.CBOBenfOwnerNasabah_JenisDokumenOld.DataSource = DataRepository.IFTI_IDTypeProvider.GetAll()
        Me.CBOBenfOwnerNasabah_JenisDokumenOld.DataTextField = "IDType"
        Me.CBOBenfOwnerNasabah_JenisDokumenOld.DataValueField = "PK_IFTI_IDType"
        Me.CBOBenfOwnerNasabah_JenisDokumenOld.DataBind()

        '-----
        Me.CBOBenfOwnerNonNasabah_JenisDokumenNew.Items.Clear()
        Me.CBOBenfOwnerNonNasabah_JenisDokumenNew.Items.Add("-Select-")
        Me.CBOBenfOwnerNonNasabah_JenisDokumenNew.AppendDataBoundItems = True
        Me.CBOBenfOwnerNonNasabah_JenisDokumenNew.DataSource = DataRepository.IFTI_IDTypeProvider.GetAll()
        Me.CBOBenfOwnerNonNasabah_JenisDokumenNew.DataTextField = "IDType"
        Me.CBOBenfOwnerNonNasabah_JenisDokumenNew.DataValueField = "PK_IFTI_IDType"
        Me.CBOBenfOwnerNonNasabah_JenisDokumenNew.DataBind()
        '-----
        Me.CBOBenfOwnerNonNasabah_JenisDokumenOld.Items.Clear()
        Me.CBOBenfOwnerNonNasabah_JenisDokumenOld.Items.Add("-Select-")
        Me.CBOBenfOwnerNonNasabah_JenisDokumenOld.AppendDataBoundItems = True
        Me.CBOBenfOwnerNonNasabah_JenisDokumenOld.DataSource = DataRepository.IFTI_IDTypeProvider.GetAll()
        Me.CBOBenfOwnerNonNasabah_JenisDokumenOld.DataTextField = "IDType"
        Me.CBOBenfOwnerNonNasabah_JenisDokumenOld.DataValueField = "PK_IFTI_IDType"
        Me.CBOBenfOwnerNonNasabah_JenisDokumenOld.DataBind()

        '-----
        Me.CBOPenerimaNasabah_IND_JenisIdenNew.Items.Clear()
        Me.CBOPenerimaNasabah_IND_JenisIdenNew.Items.Add("-Select-")
        Me.CBOPenerimaNasabah_IND_JenisIdenNew.AppendDataBoundItems = True
        Me.CBOPenerimaNasabah_IND_JenisIdenNew.DataSource = DataRepository.IFTI_IDTypeProvider.GetAll()
        Me.CBOPenerimaNasabah_IND_JenisIdenNew.DataTextField = "IDType"
        Me.CBOPenerimaNasabah_IND_JenisIdenNew.DataValueField = "PK_IFTI_IDType"
        Me.CBOPenerimaNasabah_IND_JenisIdenNew.DataBind()

        '-----
        Me.CBOPenerimaNasabah_IND_JenisIdenOld.Items.Clear()
        Me.CBOPenerimaNasabah_IND_JenisIdenOld.Items.Add("-Select-")
        Me.CBOPenerimaNasabah_IND_JenisIdenOld.AppendDataBoundItems = True
        Me.CBOPenerimaNasabah_IND_JenisIdenOld.DataSource = DataRepository.IFTI_IDTypeProvider.GetAll()
        Me.CBOPenerimaNasabah_IND_JenisIdenOld.DataTextField = "IDType"
        Me.CBOPenerimaNasabah_IND_JenisIdenOld.DataValueField = "PK_IFTI_IDType"
        Me.CBOPenerimaNasabah_IND_JenisIdenOld.DataBind()

    End Sub

    Public ReadOnly Property SetnGetBindTable(Optional ByVal AllRecord As Boolean = False) As TList(Of IFTI_Approval_Beneficiary)
        Get
            Dim TotalDisplay As Integer = 0
            Dim strWhereClause(-1) As String
            Dim strAllWhereClause As String = ""
            If SetnGetCurrentPage > GetPageTotal - 1 And GetPageTotal - 1 > 0 Then
                SetnGetCurrentPage = GetPageTotal - 1
            End If

            ReDim Preserve strWhereClause(strWhereClause.Length)
            strWhereClause(strWhereClause.Length - 1) = "FK_IFTI_Approval_id=" & parID

            If AllRecord = False Then
                TotalDisplay = GetDisplayedTotalRow
            Else
                TotalDisplay = Integer.MaxValue
            End If

            strAllWhereClause = String.Join(" and ", strWhereClause)
            Return DataRepository.IFTI_Approval_BeneficiaryProvider.GetPaged(strAllWhereClause, SetnGetSort, SetnGetCurrentPage, TotalDisplay, SetnGetRowTotal)

        End Get

    End Property
    Private _DataNonSwiftIn As IFTI_Approval_Detail
    Private ReadOnly Property DataNonSwiftIn() As IFTI_Approval_Detail
        Get
            Try
                If _DataNonSwiftIn Is Nothing Then
                    Dim otable As IFTI_Approval_Detail = DataRepository.IFTI_Approval_DetailProvider.GetPaged("FK_IFTI_Approval_id =" & parID, "", 0, Integer.MaxValue, 0)(0)
                    If Not otable Is Nothing Then
                        _DataNonSwiftIn = otable
                        Return _DataNonSwiftIn
                    Else
                        Return Nothing
                    End If

                Else
                    Return _DataNonSwiftIn
                End If
            Catch ex As Exception
                LogError(ex)
                Throw
            End Try
        End Get
    End Property
    Private _Databenefeciary As IFTI_Beneficiary
    Private ReadOnly Property Databenefeciary() As IFTI_Beneficiary
        Get
            Dim test As Integer = Session("FK_IFTI_Beneficiary_ID")
            Try
                If _Databenefeciary Is Nothing Then
                    Using otable As IFTI_Beneficiary = DataRepository.IFTI_BeneficiaryProvider.GetPaged("PK_IFTI_Beneficiary_ID =" & test, "", 0, Integer.MaxValue, 0)(0)
                        If Not otable Is Nothing Then
                            _Databenefeciary = otable
                            Return _Databenefeciary
                        Else
                            Return Nothing
                        End If
                    End Using
                Else
                    Return _Databenefeciary
                End If
            Catch ex As Exception
                LogError(ex)
                Throw
            End Try
        End Get
    End Property
    Private _DatabenefeciaryApproval As IFTI_Approval_Beneficiary
    Private ReadOnly Property DatabenefeciaryApproval() As IFTI_Approval_Beneficiary
        Get
            Dim test As Integer = Session("PK_IFTI_Approval_Beneficiary_ID")
            Try
                If _DatabenefeciaryApproval Is Nothing Then
                    Using otable As IFTI_Approval_Beneficiary = DataRepository.IFTI_Approval_BeneficiaryProvider.GetPaged("FK_IFTI_Approval_Id =" & DataNonSwiftIn.FK_IFTI_Approval_Id.ToString, "", 0, Integer.MaxValue, 0)(0)
                        Dim otables As IFTI_Approval_Beneficiary = DataRepository.IFTI_Approval_BeneficiaryProvider.GetPaged("PK_IFTI_Approval_Beneficiary_ID =" & test, "", 0, Integer.MaxValue, 0)(0)
                        If Not otables Is Nothing Then
                            _DatabenefeciaryApproval = otables
                            Return _DatabenefeciaryApproval
                        Else
                            Return Nothing
                        End If
                    End Using
                Else
                    Return _DatabenefeciaryApproval
                End If
            Catch ex As Exception
                LogError(ex)
                Throw
            End Try
        End Get
    End Property

    Private Sub bindgrid()
        Try
            If Not DataNonSwiftIn Is Nothing Then

                '++++++++++++++++++++++++++++++++++++++++UMUM+++++++++++++++++++++++++++++++++++++++++++'

                If Not DataNonSwiftIn.LTDLNNo = "" Then
                    SWIFTOutUmum_LTDNNew.Text = DataNonSwiftIn.LTDLNNo
                End If
                If Not DataNonSwiftIn.LTDLNNo_Old = "" Then
                    SWIFTOutUmum_LTDNOld.Text = DataNonSwiftIn.LTDLNNo_Old
                End If
                If Not DataNonSwiftIn.LTDLNNoKoreksi = "" Then
                    SWIFTOutUmum_LtdnKoreksiNew.Text = DataNonSwiftIn.LTDLNNoKoreksi
                End If
                If Not DataNonSwiftIn.LTDLNNoKoreksi_Old = "" Then
                    SWIFTOutUmum_LtdnKoreksiOld.Text = DataNonSwiftIn.LTDLNNoKoreksi_Old
                End If
                If Not DataNonSwiftIn.TanggalLaporan = "" Then
                    SWIFTOutUmum_TanggalLaporanNew.Text = DataNonSwiftIn.TanggalLaporan
                End If
                If Not DataNonSwiftIn.TanggalLaporan_Old = "" Then
                    SWIFTOutUmum_TanggalLaporanOld.Text = DataNonSwiftIn.TanggalLaporan_Old
                End If
                If Not DataNonSwiftIn.NamaPJKBankPelapor = "" Then
                    SWIFTOutUmum_NamaPJKBankNew.Text = DataNonSwiftIn.NamaPJKBankPelapor
                End If
                If Not DataNonSwiftIn.NamaPJKBankPelapor_Old = "" Then
                    SWIFTOutUmum_NamaPJKBankOld.Text = DataNonSwiftIn.NamaPJKBankPelapor_Old
                End If
                If Not DataNonSwiftIn.NamaPejabatPJKBankPelapor = "" Then
                    SWIFTOutUmum_NamaPejabatPJKBankNew.Text = DataNonSwiftIn.NamaPejabatPJKBankPelapor
                End If
                If Not DataNonSwiftIn.NamaPejabatPJKBankPelapor_Old = "" Then
                    SWIFTOutUmum_NamaPejabatPJKBankOld.Text = DataNonSwiftIn.NamaPejabatPJKBankPelapor_Old
                End If
                If Not DataNonSwiftIn.JenisLaporan = "" Then
                    RbSWIFTOutUmum_JenisLaporanNew.SelectedValue = DataNonSwiftIn.JenisLaporan.GetValueOrDefault
                End If
                If Not DataNonSwiftIn.JenisLaporan_Old = "" Then
                    RbSWIFTOutUmum_JenisLaporanOld.SelectedValue = DataNonSwiftIn.JenisLaporan_Old.GetValueOrDefault
                End If

                '++++++++++++++++++++++++++++++++++++++++++PENGIRIM+++++++++++++++++++++++++++++++++++++++++++'

                If Not DataNonSwiftIn.Sender_Nasabah_INDV_NoRekening = "" Then
                    TxtB1SWIFTOutPengirim_rekeningNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NoRekening
                End If
                If Not DataNonSwiftIn.Sender_Nasabah_INDV_NoRekening_Old = "" Then
                    TxtB1SWIFTOutPengirim_rekeningOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NoRekening_Old
                End If

                If ObjectAntiNull(DataNonSwiftIn.Sender_PJKBank_Type_Old) = True Then
                    Select Case DataNonSwiftIn.Sender_PJKBank_Type_Old.GetValueOrDefault
                        Case 1
                            Me.cboNonSwiftOutPenerima_TipePJKBankOld.SelectedValue = 1
                            PengirimPJK.ActiveViewIndex = 0
                        Case 2
                            Me.cboNonSwiftOutPenerima_TipePJKBankOld.SelectedValue = 2
                            PengirimPJK.ActiveViewIndex = 1
                    End Select
                End If


                '++++++++++++++++++++++++++++++++++++++++++B.1 PENGIRIM+++++++++++++++++++++++++++++++++++++++++++'
                'old
                If ObjectAntiNull(DataNonSwiftIn.Sender_FK_IFTI_NasabahType_ID_Old) = True Then
                    Select Case DataNonSwiftIn.Sender_FK_IFTI_NasabahType_ID_Old.GetValueOrDefault(0)
                        Case 1
                            Me.RbB1PengirimNasabah_TipePengirimOld.SelectedValue = 1
                            MultiViewB1SWIFTOutIdenPengirim.ActiveViewIndex = 0
                            RbB1_IdenPengirimNas_TipeNasabahOld.Enabled = True
                        Case 2
                            Me.RbB1PengirimNasabah_TipePengirimOld.SelectedValue = 1
                            MultiViewB1SWIFTOutIdenPengirim.ActiveViewIndex = 0
                            RbB1_IdenPengirimNas_TipeNasabahOld.Enabled = True
                        Case 3
                            Me.RbB1PengirimNasabah_TipePengirimOld.SelectedValue = 2
                            MultiViewB1SWIFTOutIdenPengirim.ActiveViewIndex = 1
                            PengirimAsalNonNasabah()
                    End Select
                End If

                'new
                If ObjectAntiNull(DataNonSwiftIn.Sender_FK_IFTI_NasabahType_ID) = True Then
                    Select Case DataNonSwiftIn.Sender_FK_IFTI_NasabahType_ID.GetValueOrDefault(0)
                        Case 1
                            Me.RbB1PengirimNasabah_TipePengirimNew.SelectedValue = 1
                        Case 2
                            Me.RbB1PengirimNasabah_TipePengirimNew.SelectedValue = 1
                        Case 3
                            Me.RbB1PengirimNasabah_TipePengirimNew.SelectedValue = 2
                    End Select
                End If
                'old
                If ObjectAntiNull(DataNonSwiftIn.Sender_FK_IFTI_NasabahType_ID_Old) = True Then
                    Select Case DataNonSwiftIn.Sender_FK_IFTI_NasabahType_ID_Old.GetValueOrDefault(0)
                        Case 1
                            Me.RbB1_IdenPengirimNas_TipeNasabahOld.SelectedValue = 1
                            MultiViewB1PengirimNasabah.ActiveViewIndex = 0
                            PengirimAsalNasabah()
                        Case 2
                            Me.RbB1_IdenPengirimNas_TipeNasabahOld.SelectedValue = 2
                            MultiViewB1PengirimNasabah.ActiveViewIndex = 1
                            PengirimAsalKorporasi()

                    End Select
                End If
                'new
                If ObjectAntiNull(DataNonSwiftIn.Sender_FK_IFTI_NasabahType_ID) = True Then
                    Select Case DataNonSwiftIn.Sender_FK_IFTI_NasabahType_ID.GetValueOrDefault
                        Case 1
                            Me.RbB1_IdenPengirimNas_TipeNasabahNew.SelectedValue = 1
                            Me.MultiViewB1PengirimNasabah_new.ActiveViewIndex = 0
                            PengirimAsalNasabahNew()
                        Case 2
                            Me.RbB1_IdenPengirimNas_TipeNasabahNew.SelectedValue = 2
                            MultiViewB1PengirimNasabah_new.ActiveViewIndex = 1
                            PengirimAsalKorporasiNew()
                    End Select
                End If

                '++++++++++++++++++++++++++++++++++++++++B.2 PENGIRIM+++++++++++++++++++++++++++++++++++++++++++'

                If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_NoRekening) = True Then
                    TxtB2SWIFTOutPengirim_rekeningNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NoRekening
                End If
                If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_NoRekening_Old) = True Then
                    TxtB2SWIFTOutPengirim_rekeningOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NoRekening_Old
                End If
                'old
                If ObjectAntiNull(DataNonSwiftIn.Sender_FK_IFTI_NasabahType_ID_Old) = True Then
                    Select Case DataNonSwiftIn.Sender_FK_IFTI_NasabahType_ID_Old.GetValueOrDefault(0)
                        Case 4
                            Me.RbB2_IdenPengirimNas_TipeNasabahOld.SelectedValue = 1
                            Me.MultiViewB2PengirimNasabah.ActiveViewIndex = 0
                            PengirimPenerusNasabah()
                        Case 5
                            Me.RbB2_IdenPengirimNas_TipeNasabahOld.SelectedValue = 2
                            MultiViewB2PengirimNasabah.ActiveViewIndex = 1
                            PengirimPenerusKorporasi()

                    End Select
                End If
                'new
                If ObjectAntiNull(DataNonSwiftIn.Sender_FK_IFTI_NasabahType_ID) = True Then
                    Select Case DataNonSwiftIn.Sender_FK_IFTI_NasabahType_ID.GetValueOrDefault(0)
                        Case 4
                            Me.RbB2_IdenPengirimNas_TipeNasabahNew.SelectedValue = 1
                            Me.MultiViewB2PengirimNasabah_new.ActiveViewIndex = 0
                            PengirimPenerusNasabahNew()
                        Case 5
                            Me.RbB2_IdenPengirimNas_TipeNasabahNew.SelectedValue = 2
                            MultiViewB2PengirimNasabah_new.ActiveViewIndex = 1
                            PengirimPenerusKorporasiNew()
                    End Select
                End If

                '+++++++++++++++++++++++++++++++++++++++++++++BENEFICIARY OWNER++++++++++++++++++++++++++++++++++++++++++++'
                If ObjectAntiNull(DataNonSwiftIn.BeneficialOwner_Keterlibatan) = True Then
                    Select Case DataNonSwiftIn.BeneficialOwner_Keterlibatan.GetValueOrDefault
                        Case 1
                            Me.KeterlibatanBenefNew.SelectedValue = DataNonSwiftIn.BeneficialOwner_Keterlibatan
                            Me.KeterlibatanBenefOld.SelectedValue = DataNonSwiftIn.BeneficialOwner_Keterlibatan_Old
                            BenfOwnerNasabah_rekeningNew.Text = DataNonSwiftIn.BeneficialOwner_NoRekening
                            BenfOwnerNasabah_rekeningOld.Text = DataNonSwiftIn.BeneficialOwner_NoRekening_Old
                            BenfOwnerHubunganPemilikDanaNew.Text = DataNonSwiftIn.BeneficialOwner_HubunganDenganPemilikDana
                            BenfOwnerHubunganPemilikDanaOld.Text = DataNonSwiftIn.BeneficialOwner_HubunganDenganPemilikDana_Old

                            If ObjectAntiNull(DataNonSwiftIn.FK_IFTI_BeneficialOwnerType_ID) = True Then
                                Select Case DataNonSwiftIn.FK_IFTI_BeneficialOwnerType_ID.GetValueOrDefault
                                    Case 1
                                        Me.Rb_BOwnerNas_TipePengirimOld.SelectedValue = 1
                                        MultiViewSwiftOutBOwner.ActiveViewIndex = 0
                                        BenefNasabah()
                                    Case 3
                                        Me.Rb_BOwnerNas_TipePengirimOld.SelectedValue = 1
                                        MultiViewSwiftOutBOwner.ActiveViewIndex = 0
                                    Case 2
                                        Me.Rb_BOwnerNas_TipePengirimOld.SelectedValue = 2
                                        MultiViewSwiftOutBOwner.ActiveViewIndex = 1
                                        BenefNonNasabah()
                                End Select
                            End If

                            If ObjectAntiNull(DataNonSwiftIn.FK_IFTI_BeneficialOwnerType_ID_Old) = True Then
                                Select Case DataNonSwiftIn.FK_IFTI_BeneficialOwnerType_ID_Old.GetValueOrDefault
                                    Case 1
                                        Me.Rb_BOwnerNas_TipePengirimNew.SelectedValue = 1
                                    Case 2
                                        Me.Rb_BOwnerNas_TipePengirimNew.SelectedValue = 1
                                    Case 3
                                        Me.Rb_BOwnerNas_TipePengirimNew.SelectedValue = 2
                                End Select
                            End If

                        Case 2
                            Me.KeterlibatanBenefNew.SelectedValue = DataNonSwiftIn.BeneficialOwner_Keterlibatan.GetValueOrDefault
                            Me.KeterlibatanBenefOld.SelectedValue = DataNonSwiftIn.BeneficialOwner_Keterlibatan_Old.GetValueOrDefault
                            MultiViewSwiftOutBOwner.Visible = False
                            Me.row1.Visible = False
                            Me.row2.Visible = False
                    End Select
                End If

                '+++++++++++++++++++++++++++++++++++++++++++++PENERIMA++++++++++++++++++++++++++++++++++++++++++++'
                databindgrid()

                Transaksi()
                InfoLain()

            End If

        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    Private Sub PengirimAsalNasabah()

        '---------------------------------------------Negara-------------------------------------'
        'Dim idnegara As String = DataNonSwiftIn.Sender_Nasabah_INDV_Negara.GetValueOrDefault(0)
        Dim idnegara_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_Negara_Old.GetValueOrDefault(0)
        'Dim getnegaratype As MsNegara
        Dim getnegaratype_old As MsNegara
        'getnegaratype = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara)
        getnegaratype_old = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara_old)

        '------------------------------------------------pekerjaan-------------------------------------'
        'Dim idpekerjaan As String = DataNonSwiftIn.Sender_Nasabah_INDV_Pekerjaan.GetValueOrDefault(0)
        Dim idpekerjaan_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_Pekerjaan_Old.GetValueOrDefault(0)
        'Dim getpekerjaantype As MsPekerjaan
        Dim getpekerjaantype_old As MsPekerjaan
        'getpekerjaantype = DataRepository.MsPekerjaanProvider.GetByIDPekerjaan(idpekerjaan)
        getpekerjaantype_old = DataRepository.MsPekerjaanProvider.GetByIDPekerjaan(idpekerjaan_old)

        '------------------------------------------------Kota kab---------------------------------------------'
        'Dim idKotakab As String = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_KotaKab.GetValueOrDefault(0)
        Dim idKotakab_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_KotaKab_Old.GetValueOrDefault(0)
        'Dim getKotakabtype As MsKotaKab
        Dim getKotakabtype_old As MsKotaKab
        'getKotakabtype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab)
        getKotakabtype_old = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab_old)

        '-------------------------------------------------propinsi------------------------------------------------'
        'Dim idPropinsi As String = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_Propinsi.GetValueOrDefault(0)
        Dim idPropinsi_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_Propinsi_Old.GetValueOrDefault(0)
        'Dim getPropinsitype As MsProvince
        Dim getPropinsitype_old As MsProvince
        'getPropinsitype = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi)
        getPropinsitype_old = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi_old)

        '------------------------------------------------Kota kab Iden---------------------------------------------'
        'Dim idKotakabiden As String = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_KotaKab.GetValueOrDefault(0)
        Dim idKotakabiden_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_ID_KotaKab_Old.GetValueOrDefault(0)
        'Dim getKotakabidentype As MsKotaKab
        Dim getKotakabidentype_old As MsKotaKab
        'getKotakabidentype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab)
        getKotakabidentype_old = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakabiden_old)

        '-------------------------------------------------propinsi Iden------------------------------------------------'
        'Dim idPropinsiiden As String = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_Propinsi.GetValueOrDefault(0)
        Dim idPropinsiiden_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_ID_Propinsi_Old.GetValueOrDefault(0)
        'Dim getPropinsiidentype As MsProvince
        Dim getPropinsiidentype_old As MsProvince
        'getPropinsiidentype = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi)
        getPropinsiidentype_old = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsiiden_old)
        '============================================NASABAH INDIVIDU========================================='

        'If Not DataNonSwiftIn.Sender_Nasabah_INDV_NamaLengkap = "" Then
        '    TxtB1IdenPengirimNas_Ind_NamaLengkapNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NamaLengkap
        'End If
        If Not DataNonSwiftIn.Sender_Nasabah_INDV_NamaLengkap_Old = "" Then
            TxtB1IdenPengirimNas_Ind_NamaLengkapOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NamaLengkap_Old
        End If
        'If Not DataNonSwiftIn.Sender_Nasabah_INDV_TanggalLahir = "" Then
        '    TxtB1IdenPengirimNas_Ind_tanggalLahirNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_TanggalLahir
        'End If
        If Not DataNonSwiftIn.Sender_Nasabah_INDV_TanggalLahir_Old = "" Then
            TxtB1IdenPengirimNas_Ind_tanggalLahirOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_TanggalLahir_Old
        End If
        'If Not DataNonSwiftIn.Sender_Nasabah_INDV_KewargaNegaraan = "" Then
        '    RbB1_IdenPengirimNas_Ind_kewarganegaraanNew.SelectedValue = DataNonSwiftIn.Sender_Nasabah_INDV_KewargaNegaraan.GetValueOrDefault
        'End If
        If Not DataNonSwiftIn.Sender_Nasabah_INDV_KewargaNegaraan_Old = "" Then
            RbB1_IdenPengirimNas_Ind_kewarganegaraanOld.SelectedValue = DataNonSwiftIn.Sender_Nasabah_INDV_KewargaNegaraan_Old.GetValueOrDefault
        End If
        'If Not IsNothing(getnegaratype) Then
        '    TxtB1IdenPengirimNas_Ind_negaraNew.Text = getStringFieldValue(getnegaratype.NamaNegara)
        'Else
        'TxtB1IdenPengirimNas_Ind_negaraNew.Text = ""
        'End If
        If Not IsNothing(getnegaratype_old) Then
            TxtB1IdenPengirimNas_Ind_negaraOld.Text = ObjectAntiNull(getnegaratype_old.NamaNegara)
        Else
            TxtB1IdenPengirimNas_Ind_negaraOld.Text = ""
        End If
        'If Not IsNothing(getpekerjaantype) Then
        '    TxtB1IdenPengirimNas_Ind_pekerjaanNew.Text = getpekerjaantype.NamaPekerjaan
        'End If
        If Not IsNothing(getpekerjaantype_old) Then
            TxtB1IdenPengirimNas_Ind_pekerjaanOld.Text = getpekerjaantype_old.NamaPekerjaan
        End If

        'TxtB1IdenPengirimNas_Ind_PekerjaanlainNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_PekerjaanLainnya
        TxtB1IdenPengirimNas_Ind_PekerjaanlainOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_PekerjaanLainnya_Old

        'TxtB1IdenPengirimNas_Ind_AlamatNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_Alamat
        TxtB1IdenPengirimNas_Ind_AlamatOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_Alamat_Old
        'If Not IsNothing(getKotakabtype) Then
        '    TxtB1IdenPengirimNas_Ind_KotaNew.Text = getKotakabtype.NamaKotaKab
        'End If
        If Not IsNothing(getKotakabtype_old) Then
            TxtB1IdenPengirimNas_Ind_KotaOld.Text = getKotakabtype_old.NamaKotaKab
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_DOM_KotaKabLainnya) = True Then
        '    TxtB1IdenPengirimNas_Ind_kotalainNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_KotaKabLainnya
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_DOM_KotaKabLainnya_Old) = True Then
            TxtB1IdenPengirimNas_Ind_kotalainOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_KotaKabLainnya_Old
        End If
        'TxtB1IdenPengirimNas_Ind_provinsiNew.Text = getPropinsitype.Nama
        If Not IsNothing(getPropinsitype_old) Then
            TxtB1IdenPengirimNas_Ind_provinsiOld.Text = getPropinsitype_old.Nama
        End If
        'TxtB1IdenPengirimNas_Ind_provinsiLainNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_PropinsiLainnya
        TxtB1IdenPengirimNas_Ind_provinsiLainOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_PropinsiLainnya_Old

        'TxtB1IdenPengirimNas_Ind_alamatIdentitasNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_Alamat
        TxtB1IdenPengirimNas_Ind_alamatIdentitasOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_Alamat_Old
        'If Not IsNothing(getKotakabidentype) Then
        '    TxtB1IdenPengirimNas_Ind_kotaIdentitasNew.Text = getKotakabidentype.NamaKotaKab
        'End If
        If Not IsNothing(getKotakabidentype_old) Then
            TxtB1IdenPengirimNas_Ind_kotaIdentitasOld.Text = getKotakabidentype_old.NamaKotaKab
        End If

        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_ID_KotaKabLainnya) = True Then
        '    TxtB1IdenPengirimNas_Ind_KotaLainIdenNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_KotaKabLainnya
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_ID_KotaKabLainnya_Old) = True Then
            TxtB1IdenPengirimNas_Ind_KotaLainIdenOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_KotaKabLainnya_Old
        End If
        'If Not IsNothing(getPropinsiidentype) Then
        '    TxtB1IdenPengirimNas_Ind_ProvinsiIdenNew.Text = getPropinsiidentype.Nama
        'End If
        If Not IsNothing(getPropinsiidentype_old) Then
            TxtB1IdenPengirimNas_Ind_ProvinsiIdenOld.Text = getPropinsiidentype_old.Nama
        End If
        'TxtB1IdenPengirimNas_Ind_ProvinsilainIdenNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_PropinsiLainnya
        TxtB1IdenPengirimNas_Ind_ProvinsilainIdenOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_PropinsiLainnya_Old
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_FK_IFTI_IDType) = True Then
        '    CboB1PengirimNasInd_jenisidentitasNew.SelectedValue = DataNonSwiftIn.Sender_Nasabah_INDV_FK_IFTI_IDType
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_FK_IFTI_IDType_Old) = True Then
            CboB1PengirimNasInd_jenisidentitasOld.SelectedValue = DataNonSwiftIn.Sender_Nasabah_INDV_FK_IFTI_IDType_Old
        End If
        'If Not DataNonSwiftIn.Sender_Nasabah_INDV_NomorID = "" Then
        '    TxtB1IdenPengirimNas_Ind_noIdentitasNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NomorID
        'End If
        TxtB1IdenPengirimNas_Ind_noIdentitasOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NomorID_Old
    End Sub
    Private Sub PengirimAsalNasabahNew()

        '---------------------------------------------Negara-------------------------------------'
        Dim idnegara As String = DataNonSwiftIn.Sender_Nasabah_INDV_Negara.GetValueOrDefault(0)
        ' Dim idnegara_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_Negara_Old.GetValueOrDefault(0)
        Dim getnegaratype As MsNegara
        'Dim getnegaratype_old As MsNegara
        getnegaratype = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara)
        'getnegaratype_old = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara_old)

        '------------------------------------------------pekerjaan-------------------------------------'
        Dim idpekerjaan As String = DataNonSwiftIn.Sender_Nasabah_INDV_Pekerjaan.GetValueOrDefault(0)
        'Dim idpekerjaan_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_Pekerjaan_Old.GetValueOrDefault(0)
        Dim getpekerjaantype As MsPekerjaan
        'Dim getpekerjaantype_old As MsPekerjaan
        getpekerjaantype = DataRepository.MsPekerjaanProvider.GetByIDPekerjaan(idpekerjaan)
        'getpekerjaantype_old = DataRepository.MsPekerjaanProvider.GetByIDPekerjaan(idpekerjaan_old)

        '------------------------------------------------Kota kab---------------------------------------------'
        Dim idKotakab As String = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_KotaKab.GetValueOrDefault(0)
        'Dim idKotakab_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_KotaKab_Old.GetValueOrDefault(0)
        Dim getKotakabtype As MsKotaKab
        'Dim getKotakabtype_old As MsKotaKab
        getKotakabtype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab)
        'getKotakabtype_old = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab_old)

        '-------------------------------------------------propinsi------------------------------------------------'
        Dim idPropinsi As String = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_Propinsi.GetValueOrDefault(0)
        'Dim idPropinsi_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_Propinsi_Old.GetValueOrDefault(0)
        Dim getPropinsitype As MsProvince
        'Dim getPropinsitype_old As MsProvince
        getPropinsitype = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi)
        'getPropinsitype_old = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi_old)

        '------------------------------------------------Kota kab Iden---------------------------------------------'
        Dim idKotakabiden As String = DataNonSwiftIn.Sender_Nasabah_INDV_ID_KotaKab.GetValueOrDefault(0)
        'Dim idKotakabiden_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_KotaKab_Old.GetValueOrDefault(0)
        Dim getKotakabidentype As MsKotaKab
        'Dim getKotakabidentype_old As MsKotaKab
        getKotakabidentype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakabiden)
        'getKotakabidentype_old = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab_old)

        '-------------------------------------------------propinsi Iden------------------------------------------------'
        Dim idPropinsiiden As String = DataNonSwiftIn.Sender_Nasabah_INDV_ID_Propinsi.GetValueOrDefault(0)
        'Dim idPropinsiiden_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_Propinsi_Old.GetValueOrDefault(0)
        Dim getPropinsiidentype As MsProvince
        'Dim getPropinsiidentype_old As MsProvince
        getPropinsiidentype = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsiiden)
        'getPropinsiidentype_old = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi_old)
        '============================================NASABAH INDIVIDU========================================='

        If Not DataNonSwiftIn.Sender_Nasabah_INDV_NamaLengkap = "" Then
            TxtB1IdenPengirimNas_Ind_NamaLengkapNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NamaLengkap
        End If
        'If Not DataNonSwiftIn.Sender_Nasabah_INDV_NamaLengkap_Old = "" Then
        '    TxtB1IdenPengirimNas_Ind_NamaLengkapOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NamaLengkap_Old
        'End If
        If Not DataNonSwiftIn.Sender_Nasabah_INDV_TanggalLahir = "" Then
            TxtB1IdenPengirimNas_Ind_tanggalLahirNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_TanggalLahir
        End If
        'If Not DataNonSwiftIn.Sender_Nasabah_INDV_TanggalLahir_Old = "" Then
        '    TxtB1IdenPengirimNas_Ind_tanggalLahirOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_TanggalLahir_Old
        'End If
        If Not DataNonSwiftIn.Sender_Nasabah_INDV_KewargaNegaraan = "" Then
            RbB1_IdenPengirimNas_Ind_kewarganegaraanNew.SelectedValue = DataNonSwiftIn.Sender_Nasabah_INDV_KewargaNegaraan.GetValueOrDefault
        End If
        'If Not DataNonSwiftIn.Sender_Nasabah_INDV_KewargaNegaraan_Old = "" Then
        '    RbB1_IdenPengirimNas_Ind_kewarganegaraanOld.SelectedValue = DataNonSwiftIn.Sender_Nasabah_INDV_KewargaNegaraan_Old.GetValueOrDefault
        'End If
        If Not IsNothing(getnegaratype) Then
            TxtB1IdenPengirimNas_Ind_negaraNew.Text = getStringFieldValue(getnegaratype.NamaNegara)
        Else
            TxtB1IdenPengirimNas_Ind_negaraNew.Text = ""
        End If
        'If Not IsNothing(getnegaratype_old) Then
        '    TxtB1IdenPengirimNas_Ind_negaraOld.Text = ObjectAntiNull(getnegaratype_old.NamaNegara)
        'Else
        '    TxtB1IdenPengirimNas_Ind_negaraOld.Text = ""
        'End If
        If Not IsNothing(getpekerjaantype) Then
            TxtB1IdenPengirimNas_Ind_pekerjaanNew.Text = getpekerjaantype.NamaPekerjaan
        End If
        'If Not IsNothing(getpekerjaantype_old) Then
        '    TxtB1IdenPengirimNas_Ind_pekerjaanOld.Text = getpekerjaantype_old.NamaPekerjaan
        'End If

        TxtB1IdenPengirimNas_Ind_PekerjaanlainNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_PekerjaanLainnya
        'TxtB1IdenPengirimNas_Ind_PekerjaanlainOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_PekerjaanLainnya_Old

        TxtB1IdenPengirimNas_Ind_AlamatNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_Alamat
        'TxtB1IdenPengirimNas_Ind_AlamatOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_Alamat_Old
        If Not IsNothing(getKotakabtype) Then
            TxtB1IdenPengirimNas_Ind_KotaNew.Text = getKotakabtype.NamaKotaKab
        End If
        'If Not IsNothing(getKotakabtype_old) Then
        '    TxtB1IdenPengirimNas_Ind_KotaOld.Text = getKotakabtype_old.NamaKotaKab
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_DOM_KotaKabLainnya) = True Then
            TxtB1IdenPengirimNas_Ind_kotalainNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_KotaKabLainnya
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_DOM_KotaKabLainnya_Old) = True Then
        '    TxtB1IdenPengirimNas_Ind_kotalainOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_KotaKabLainnya_Old
        'End If
        If Not IsNothing(getPropinsitype) Then
            TxtB1IdenPengirimNas_Ind_provinsiNew.Text = getPropinsitype.Nama
        End If

        'If Not IsNothing(getPropinsitype_old) Then
        '    TxtB1IdenPengirimNas_Ind_provinsiOld.Text = getPropinsitype_old.Nama
        'End If
        TxtB1IdenPengirimNas_Ind_provinsiLainNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_PropinsiLainnya
        'TxtB1IdenPengirimNas_Ind_provinsiLainOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_PropinsiLainnya_Old

        TxtB1IdenPengirimNas_Ind_alamatIdentitasNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_Alamat
        'TxtB1IdenPengirimNas_Ind_alamatIdentitasOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_Alamat_Old
        If Not IsNothing(getKotakabidentype) Then
            TxtB1IdenPengirimNas_Ind_kotaIdentitasNew.Text = getKotakabidentype.NamaKotaKab
        End If
        'If Not IsNothing(getKotakabidentype_old) Then
        '    TxtB1IdenPengirimNas_Ind_kotaIdentitasOld.Text = getKotakabidentype_old.NamaKotaKab
        'End If

        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_ID_KotaKabLainnya) = True Then
            TxtB1IdenPengirimNas_Ind_KotaLainIdenNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_KotaKabLainnya
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_ID_KotaKabLainnya_Old) = True Then
        '    TxtB1IdenPengirimNas_Ind_KotaLainIdenOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_KotaKabLainnya_Old
        'End If
        If Not IsNothing(getPropinsiidentype) Then
            TxtB1IdenPengirimNas_Ind_ProvinsiIdenNew.Text = getPropinsiidentype.Nama
        End If
        'If Not IsNothing(getPropinsiidentype_old) Then
        '    TxtB1IdenPengirimNas_Ind_ProvinsiIdenOld.Text = getPropinsiidentype_old.Nama
        'End If
        TxtB1IdenPengirimNas_Ind_ProvinsilainIdenNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_PropinsiLainnya
        'TxtB1IdenPengirimNas_Ind_ProvinsilainIdenOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_PropinsiLainnya_Old
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_FK_IFTI_IDType) = True Then
            CboB1PengirimNasInd_jenisidentitasNew.SelectedValue = DataNonSwiftIn.Sender_Nasabah_INDV_FK_IFTI_IDType
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_FK_IFTI_IDType_Old) = True Then
        '    CboB1PengirimNasInd_jenisidentitasOld.SelectedValue = DataNonSwiftIn.Sender_Nasabah_INDV_FK_IFTI_IDType_Old
        'End If
        If Not DataNonSwiftIn.Sender_Nasabah_INDV_NomorID = "" Then
            TxtB1IdenPengirimNas_Ind_noIdentitasNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NomorID
        End If
        'TxtB1IdenPengirimNas_Ind_noIdentitasOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NomorID_Old
    End Sub
    Private Sub PengirimAsalKorporasi()

        '---------------------------------------Bentuk Badan Usaha------------------------------------'
        'Dim idBentuk As String = DataNonSwiftIn.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault(0)
        Dim idBentuk_old As String = DataNonSwiftIn.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id_Old.GetValueOrDefault(0)
        'Dim getBentuktype As MsBentukBidangUsaha
        Dim getBentuktype_old As MsBentukBidangUsaha
        'getBentuktype = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(idBentuk)
        getBentuktype_old = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(idBentuk_old)

        '---------------------------------------Badan Usaha------------------------------------'
        'Dim idBadan As String = DataNonSwiftIn.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id.GetValueOrDefault(0)
        Dim idBadan_old As String = DataNonSwiftIn.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id_Old.GetValueOrDefault(0)
        'Dim getBadantype As MsBidangUsaha
        Dim getBadantype_old As MsBidangUsaha
        'getBadantype = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(idBadan)
        getBadantype_old = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(idBadan_old)

        '------------------------------------------------Kota kab---------------------------------------------'
        'Dim idKotakab As String = DataNonSwiftIn.Sender_Nasabah_COR_KotaKab.GetValueOrDefault(0)
        Dim idKotakab_old As String = DataNonSwiftIn.Sender_Nasabah_COR_KotaKab_Old.GetValueOrDefault(0)
        'Dim getKotakabtype As MsKotaKab
        Dim getKotakabtype_old As MsKotaKab
        'getKotakabtype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab)
        getKotakabtype_old = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab_old)

        '-------------------------------------------------propinsi------------------------------------------------'
        'Dim idPropinsi As String = DataNonSwiftIn.Sender_Nasabah_CORP_Propinsi.GetValueOrDefault(0)
        Dim idPropinsi_old As String = DataNonSwiftIn.Sender_Nasabah_CORP_Propinsi_Old.GetValueOrDefault(0)
        'Dim getPropinsitype As MsProvince
        Dim getPropinsitype_old As MsProvince
        'getPropinsitype = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi)
        getPropinsitype_old = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi_old)

        '=========================================KORPORASI============================================='
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id) = True Then
        '    TxtB1IdenPengirimNas_Corp_BentukBadanUsahaNew.Text = getBentuktype.BentukBidangUsaha
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id_Old) = True Then
            TxtB1IdenPengirimNas_Corp_BentukBadanUsahaOld.Text = getBentuktype_old.BentukBidangUsaha
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_BentukBadanUsahaLainnya) = True Then
        '    TxtB1IdenPengirimNas_Corp_BentukBadanUsahaLainNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_BentukBadanUsahaLainnya
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_BentukBadanUsahaLainnya_Old) = True Then
            TxtB1IdenPengirimNas_Corp_BentukBadanUsahaLainOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_BentukBadanUsahaLainnya_Old
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_NamaKorporasi) = True Then
        '    TxtB1IdenPengirimNas_Corp_NamaKorpNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NamaKorporasi
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_NamaKorporasi_Old) = True Then
            TxtB1IdenPengirimNas_Corp_NamaKorpOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NamaKorporasi_Old
        End If

        'TxtB1IdenPengirimNas_Corp_BidangUsahaKorpNew.Text = getBadantype.NamaBidangUsaha
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id_Old) = True Then
            TxtB1IdenPengirimNas_Corp_BidangUsahaKorpOld.Text = getBadantype_old.NamaBidangUsaha
        End If
        'TxtB1IdenPengirimNas_Corp_BidangUsahaLainnyaKorpNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_BidangUsahaLainnya
        TxtB1IdenPengirimNas_Corp_BidangUsahaLainnyaKorpOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_BidangUsahaLainnya_Old
        'TxtB1IdenPengirimNas_Corp_alamatkorpNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_AlamatLengkap
        TxtB1IdenPengirimNas_Corp_alamatkorpOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_AlamatLengkap_Old

        'TxtB1IdenPengirimNas_Corp_kotakorpNew.Text = getKotakabtype.NamaKotaKab
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_COR_KotaKab_Old) = True Then
            TxtB1IdenPengirimNas_Corp_kotakorpOld.Text = getKotakabtype_old.NamaKotaKab
        End If
        'TxtB1IdenPengirimNas_Corp_kotakorplainNew.Text = DataNonSwiftIn.Sender_Nasabah_COR_KotaKabLainnya
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_COR_KotaKabLainnya_Old) = True Then
            TxtB1IdenPengirimNas_Corp_kotakorplainOld.Text = DataNonSwiftIn.Sender_Nasabah_COR_KotaKabLainnya_Old
        End If
        'TxtB1IdenPengirimNas_Corp_provKorpNew.Text = getPropinsitype.Nama
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_Propinsi_Old) = True Then
            TxtB1IdenPengirimNas_Corp_provKorpOld.Text = getPropinsitype_old.Nama
        End If
        'TxtB1IdenPengirimNas_Corp_provKorpLainNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_PropinsiLainnya
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_PropinsiLainnya_Old) = True Then
            TxtB1IdenPengirimNas_Corp_provKorpLainOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_PropinsiLainnya_Old
        End If
        'TxtB1IdenPengirimNas_Corp_AlamatLuarNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_LN_AlamatLengkap
        TxtB1IdenPengirimNas_Corp_AlamatLuarOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_LN_AlamatLengkap_Old
        'TxtB1IdenPengirimNas_Corp_AlamatAsalNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_Alamat_KantorCabangPengirim
        TxtB1IdenPengirimNas_Corp_AlamatAsalOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_Alamat_KantorCabangPengirim_Old
    End Sub
    Private Sub PengirimAsalKorporasiNew()

        '---------------------------------------Bentuk Badan Usaha------------------------------------'
        Dim idBentuk As String = DataNonSwiftIn.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault(0)
        'Dim idBentuk_old As String = DataNonSwiftIn.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id_Old.GetValueOrDefault(0)
        Dim getBentuktype As MsBentukBidangUsaha
        'Dim getBentuktype_old As MsBentukBidangUsaha
        getBentuktype = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(idBentuk)
        'getBentuktype_old = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(idBentuk_old)

        '---------------------------------------Badan Usaha------------------------------------'
        Dim idBadan As String = DataNonSwiftIn.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id.GetValueOrDefault(0)
        'Dim idBadan_old As String = DataNonSwiftIn.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id_Old.GetValueOrDefault(0)
        Dim getBadantype As MsBidangUsaha
        'Dim getBadantype_old As MsBidangUsaha
        getBadantype = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(idBadan)
        'getBadantype_old = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(idBadan_old)

        '------------------------------------------------Kota kab---------------------------------------------'
        Dim idKotakab As String = DataNonSwiftIn.Sender_Nasabah_COR_KotaKab.GetValueOrDefault(0)
        'Dim idKotakab_old As String = DataNonSwiftIn.Sender_Nasabah_COR_KotaKab_Old.GetValueOrDefault(0)
        Dim getKotakabtype As MsKotaKab
        'Dim getKotakabtype_old As MsKotaKab
        getKotakabtype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab)
        'getKotakabtype_old = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab_old)

        '-------------------------------------------------propinsi------------------------------------------------'
        Dim idPropinsi As String = DataNonSwiftIn.Sender_Nasabah_CORP_Propinsi.GetValueOrDefault(0)
        'Dim idPropinsi_old As String = DataNonSwiftIn.Sender_Nasabah_CORP_Propinsi_Old.GetValueOrDefault(0)
        Dim getPropinsitype As MsProvince
        'Dim getPropinsitype_old As MsProvince
        getPropinsitype = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi)
        'getPropinsitype_old = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi_old)

        '=========================================KORPORASI============================================='
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id) = True Then
            TxtB1IdenPengirimNas_Corp_BentukBadanUsahaNew.Text = getBentuktype.BentukBidangUsaha
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id_Old) = True Then
        '    TxtB1IdenPengirimNas_Corp_BentukBadanUsahaOld.Text = getBentuktype_old.BentukBidangUsaha
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_BentukBadanUsahaLainnya) = True Then
            TxtB1IdenPengirimNas_Corp_BentukBadanUsahaLainNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_BentukBadanUsahaLainnya
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_BentukBadanUsahaLainnya_Old) = True Then
        '    TxtB1IdenPengirimNas_Corp_BentukBadanUsahaLainOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_BentukBadanUsahaLainnya_Old
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_NamaKorporasi) = True Then
            TxtB1IdenPengirimNas_Corp_NamaKorpNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NamaKorporasi
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_NamaKorporasi_Old) = True Then
        '    TxtB1IdenPengirimNas_Corp_NamaKorpOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NamaKorporasi_Old
        'End If

        TxtB1IdenPengirimNas_Corp_BidangUsahaKorpNew.Text = getBadantype.NamaBidangUsaha
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id_Old) = True Then
        '    TxtB1IdenPengirimNas_Corp_BidangUsahaKorpOld.Text = getBadantype_old.NamaBidangUsaha
        'End If
        TxtB1IdenPengirimNas_Corp_BidangUsahaLainnyaKorpNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_BidangUsahaLainnya
        'TxtB1IdenPengirimNas_Corp_BidangUsahaLainnyaKorpOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_BidangUsahaLainnya_Old
        TxtB1IdenPengirimNas_Corp_alamatkorpNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_AlamatLengkap
        'TxtB1IdenPengirimNas_Corp_alamatkorpOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_AlamatLengkap_Old

        TxtB1IdenPengirimNas_Corp_kotakorpNew.Text = getKotakabtype.NamaKotaKab
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_COR_KotaKab_Old) = True Then
        '    TxtB1IdenPengirimNas_Corp_kotakorpOld.Text = getKotakabtype_old.NamaKotaKab
        'End If
        TxtB1IdenPengirimNas_Corp_kotakorplainNew.Text = DataNonSwiftIn.Sender_Nasabah_COR_KotaKabLainnya
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_COR_KotaKabLainnya_Old) = True Then
        '    TxtB1IdenPengirimNas_Corp_kotakorplainOld.Text = DataNonSwiftIn.Sender_Nasabah_COR_KotaKabLainnya_Old
        'End If
        TxtB1IdenPengirimNas_Corp_provKorpNew.Text = getPropinsitype.Nama
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_Propinsi_Old) = True Then
        '    TxtB1IdenPengirimNas_Corp_provKorpOld.Text = getPropinsitype_old.Nama
        'End If
        TxtB1IdenPengirimNas_Corp_provKorpLainNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_PropinsiLainnya
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_PropinsiLainnya_Old) = True Then
        '    TxtB1IdenPengirimNas_Corp_provKorpLainOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_PropinsiLainnya_Old
        'End If
        TxtB1IdenPengirimNas_Corp_AlamatLuarNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_LN_AlamatLengkap
        'TxtB1IdenPengirimNas_Corp_AlamatLuarOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_LN_AlamatLengkap_Old
        TxtB1IdenPengirimNas_Corp_AlamatAsalNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_Alamat_KantorCabangPengirim
        'TxtB1IdenPengirimNas_Corp_AlamatAsalOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_Alamat_KantorCabangPengirim_Old
    End Sub
    Private Sub PengirimAsalNonNasabah()

        '------------------------------------------------Kota kab---------------------------------------------'
        Dim idKotakab As String = DataNonSwiftIn.Sender_NonNasabah_ID_KotaKab.GetValueOrDefault(0)
        Dim idKotakab_old As String = DataNonSwiftIn.Sender_NonNasabah_ID_KotaKab_Old.GetValueOrDefault(0)
        Dim getKotakabtype As MsKotaKab
        Dim getKotakabtype_old As MsKotaKab
        getKotakabtype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab)
        getKotakabtype_old = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab_old)

        '-------------------------------------------------propinsi------------------------------------------------'
        Dim idPropinsi As String = DataNonSwiftIn.Sender_NonNasabah_ID_Propinsi.GetValueOrDefault(0)
        Dim idPropinsi_old As String = DataNonSwiftIn.Sender_NonNasabah_ID_Propinsi_Old.GetValueOrDefault(0)
        Dim getPropinsitype As MsProvince
        Dim getPropinsitype_old As MsProvince
        getPropinsitype = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi)
        getPropinsitype_old = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi_old)

        '==============================================NON NASABAH==========================================='
        If ObjectAntiNull(DataNonSwiftIn.FK_IFTI_NonNasabahNominalType_ID) Then
            RbB1PengirimNonNasabah_100JutaNew.SelectedValue = DataNonSwiftIn.FK_IFTI_NonNasabahNominalType_ID
        End If
        If ObjectAntiNull(DataNonSwiftIn.FK_IFTI_NonNasabahNominalType_ID_Old) = True Then
            RbB1PengirimNonNasabah_100JutaOld.SelectedValue = DataNonSwiftIn.FK_IFTI_NonNasabahNominalType_ID_Old
        End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_NonNasabah_NamaLengkap) = True Then
            TxtB1PengirimNonNasabah_namaNew.Text = DataNonSwiftIn.Sender_NonNasabah_NamaLengkap
        End If
        TxtB1PengirimNonNasabah_namaOld.Text = DataNonSwiftIn.Sender_NonNasabah_NamaLengkap_Old
        If ObjectAntiNull(DataNonSwiftIn.Sender_NonNasabah_TanggalLahir) = True Then
            TxtB1PengirimNonNasabah_TanggalLahirNew.Text = DataNonSwiftIn.Sender_NonNasabah_TanggalLahir
        End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_NonNasabah_TanggalLahir_Old) = True Then
            TxtB1PengirimNonNasabah_TanggalLahirOld.Text = DataNonSwiftIn.Sender_NonNasabah_TanggalLahir_Old
        End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_NonNasabah_ID_Alamat) = True Then
            TxtB1PengirimNonNasabah_alamatidenNew.Text = DataNonSwiftIn.Sender_NonNasabah_ID_Alamat
        End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_NonNasabah_ID_Alamat_Old) = True Then
            TxtB1PengirimNonNasabah_alamatidenOld.Text = DataNonSwiftIn.Sender_NonNasabah_ID_Alamat_Old
        End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_NonNasabah_ID_KotaKab) = True Then
            TxtB1PengirimNonNasabah_kotaIdenNew.Text = getKotakabtype.NamaKotaKab
        End If
        TxtB1PengirimNonNasabah_kotaIdenOld.Text = getKotakabtype_old.NamaKotaKab
        TxtB1PengirimNonNasabah_KotaLainIdenNew.Text = DataNonSwiftIn.Sender_NonNasabah_ID_KotaKabLainnya
        TxtB1PengirimNonNasabah_KotaLainIdenOld.Text = DataNonSwiftIn.Sender_NonNasabah_ID_KotaKabLainnya_Old
        If ObjectAntiNull(DataNonSwiftIn.Sender_NonNasabah_ID_Propinsi) = True Then
            TxtB1PengirimNonNasabah_ProvIdenNew.Text = getPropinsitype.Nama
        End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_NonNasabah_ID_Propinsi_Old) = True Then
            TxtB1PengirimNonNasabah_ProvIdenOld.Text = getPropinsitype_old.Nama
        End If
        TxtB1PengirimNonNasabah_ProvLainIdenNew.Text = DataNonSwiftIn.Sender_NonNasabah_ID_PropinsiLainnya
        TxtB1PengirimNonNasabah_ProvLainIdenOld.Text = DataNonSwiftIn.Sender_NonNasabah_ID_PropinsiLainnya_Old
        TxtB1PengirimNonNasabah_NoTeleponNew.Text = DataNonSwiftIn.Sender_NonNasabah_NoTelp
        TxtB1PengirimNonNasabah_NoTeleponOld.Text = DataNonSwiftIn.Sender_NonNasabah_NoTelp_Old
        If ObjectAntiNull(DataNonSwiftIn.Sender_NonNasabah_FK_IFTI_IDType) = True Then
            CboB1PengirimNonNasabah_JenisDokumenNew.SelectedValue = DataNonSwiftIn.Sender_NonNasabah_FK_IFTI_IDType
        End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_NonNasabah_FK_IFTI_IDType_Old) = True Then
            CboB1PengirimNonNasabah_JenisDokumenOld.SelectedValue = DataNonSwiftIn.Sender_NonNasabah_FK_IFTI_IDType_Old
        End If
        TxtB1PengirimNonNasabah_NomorIdenNew.Text = DataNonSwiftIn.Sender_NonNasabah_NomorID
        TxtB1PengirimNonNasabah_NomorIdenOld.Text = DataNonSwiftIn.Sender_NonNasabah_NomorID_Old

    End Sub
    Private Sub PengirimPenerusNasabah()

        '---------------------------------------------Negara-------------------------------------'
        'Dim idnegara As String = DataNonSwiftIn.Sender_Nasabah_INDV_Negara.GetValueOrDefault(0)
        Dim idnegara_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_Negara_Old.GetValueOrDefault(0)
        'Dim getnegaratype As MsNegara
        Dim getnegaratype_old As MsNegara
        'getnegaratype = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara)
        getnegaratype_old = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara_old)

        '------------------------------------------------pekerjaan-------------------------------------'
        'Dim idpekerjaan As String = DataNonSwiftIn.Sender_Nasabah_INDV_Pekerjaan.GetValueOrDefault(0)
        Dim idpekerjaan_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_Pekerjaan_Old.GetValueOrDefault(0)
        'Dim getpekerjaantype As MsPekerjaan
        Dim getpekerjaantype_old As MsPekerjaan
        'getpekerjaantype = DataRepository.MsPekerjaanProvider.GetByIDPekerjaan(idpekerjaan)
        getpekerjaantype_old = DataRepository.MsPekerjaanProvider.GetByIDPekerjaan(idpekerjaan_old)

        '------------------------------------------------Kota kab---------------------------------------------'
        'Dim idKotakab As String = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_KotaKab.GetValueOrDefault(0)
        Dim idKotakab_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_KotaKab_Old.GetValueOrDefault(0)
        'Dim getKotakabtype As MsKotaKab
        Dim getKotakabtype_old As MsKotaKab
        'getKotakabtype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab)
        getKotakabtype_old = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab_old)

        '-------------------------------------------------propinsi------------------------------------------------'
        'Dim idPropinsi As String = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_Propinsi.GetValueOrDefault(0)
        Dim idPropinsi_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_Propinsi_Old.GetValueOrDefault(0)
        'Dim getPropinsitype As MsProvince
        Dim getPropinsitype_old As MsProvince
        'getPropinsitype = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi)
        getPropinsitype_old = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi_old)

        '------------------------------------------------Kota kab Iden---------------------------------------------'
        'Dim idKotakabiden As String = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_KotaKab.GetValueOrDefault(0)
        Dim idKotakabiden_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_ID_KotaKab_Old.GetValueOrDefault(0)
        'Dim getKotakabidentype As MsKotaKab
        Dim getKotakabidentype_old As MsKotaKab
        'getKotakabidentype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab)
        getKotakabidentype_old = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakabiden_old)

        '-------------------------------------------------propinsi Iden------------------------------------------------'
        'Dim idPropinsiiden As String = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_Propinsi.GetValueOrDefault(0)
        Dim idPropinsiiden_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_ID_Propinsi_Old.GetValueOrDefault(0)
        'Dim getPropinsiidentype As MsProvince
        Dim getPropinsiidentype_old As MsProvince
        'getPropinsiidentype = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi)
        getPropinsiidentype_old = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsiiden_old)

        '==================================NASABAH INDIVIDU========================================='
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_NamaBank) = True Then
        '    TxtB2IdenPengirimNas_Ind_NamaBankNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NamaBank
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_NamaBank_Old) = True Then
            TxtB2IdenPengirimNas_Ind_NamaBankOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NamaBank_Old
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_NamaLengkap) = True Then
        '    TxtB2IdenPengirimNas_Ind_NamaLengkapNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NamaLengkap
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_NamaLengkap_Old) = True Then
            TxtB2IdenPengirimNas_Ind_NamaLengkapOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NamaLengkap_Old
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_TanggalLahir) = True Then
        '    TxtB2IdenPengirimNas_Ind_tanggalLahirNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_TanggalLahir
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_TanggalLahir_Old) = True Then
            TxtB2IdenPengirimNas_Ind_tanggalLahirOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_TanggalLahir_Old
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_KewargaNegaraan) = True Then
        '    RbB2_IdenPengirimNas_Ind_kewarganegaraanNew.SelectedValue = DataNonSwiftIn.Sender_Nasabah_INDV_KewargaNegaraan.GetValueOrDefault
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_KewargaNegaraan_Old) = True Then
            RbB2_IdenPengirimNas_Ind_kewarganegaraanOld.SelectedValue = DataNonSwiftIn.Sender_Nasabah_INDV_KewargaNegaraan_Old.GetValueOrDefault
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_Negara) = True Then
        '    TxtB2IdenPengirimNas_Ind_negaraNew.Text = getnegaratype.NamaNegara
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_Negara_Old) = True Then
            TxtB2IdenPengirimNas_Ind_negaraOld.Text = getnegaratype_old.NamaNegara
        End If

        'TxtB2IdenPengirimNas_Ind_pekerjaanNew.Text = getpekerjaantype.NamaPekerjaan
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_Pekerjaan_Old) = True Then
            TxtB2IdenPengirimNas_Ind_pekerjaanOld.Text = getpekerjaantype_old.NamaPekerjaan
        End If

        'TxtB2IdenPengirimNas_Ind_PekerjaanlainNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_PekerjaanLainnya
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_PekerjaanLainnya_Old) = True Then
            TxtB2IdenPengirimNas_Ind_PekerjaanlainOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_PekerjaanLainnya_Old
        End If

        'TxtB2IdenPengirimNas_Ind_AlamatNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_Alamat
        TxtB2IdenPengirimNas_Ind_AlamatOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_Alamat_Old
        'TxtB2IdenPengirimNas_Ind_KotaNew.Text = getKotakabtype.NamaKotaKab
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_DOM_KotaKab_Old) = True Then
            TxtB2IdenPengirimNas_Ind_KotaOld.Text = getKotakabtype_old.NamaKotaKab
        End If
        'TxtB2IdenPengirimNas_Ind_kotalainNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_KotaKabLainnya
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_DOM_KotaKabLainnya_Old) = True Then
            TxtB2IdenPengirimNas_Ind_kotalainOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_KotaKabLainnya_Old
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_DOM_Propinsi) = True Then
        '    TxtB2IdenPengirimNas_Ind_provinsiNew.Text = getPropinsitype.Nama
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_DOM_Propinsi_Old) = True Then
            TxtB2IdenPengirimNas_Ind_provinsiOld.Text = getPropinsitype_old.Nama
        End If
        'TxtB2IdenPengirimNas_Ind_provinsiLainNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_PropinsiLainnya
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_DOM_PropinsiLainnya_Old) = True Then
            TxtB2IdenPengirimNas_Ind_provinsiLainOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_PropinsiLainnya_Old
        End If

        'TxtB2IdenPengirimNas_Ind_alamatIdentitasNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_Alamat
        TxtB2IdenPengirimNas_Ind_alamatIdentitasOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_Alamat_Old
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_ID_KotaKab) = True Then
        '    TxtB2IdenPengirimNas_Ind_kotaIdentitasNew.Text = getKotakabidentype.NamaKotaKab
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_ID_KotaKab_Old) = True Then
            TxtB2IdenPengirimNas_Ind_kotaIdentitasOld.Text = getKotakabidentype_old.NamaKotaKab
        End If
        'TxtB2IdenPengirimNas_Ind_KotaLainIdenNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_KotaKabLainnya
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_ID_KotaKabLainnya_Old) = True Then
            TxtB2IdenPengirimNas_Ind_KotaLainIdenOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_KotaKabLainnya_Old
        End If
        'TxtB2IdenPengirimNas_Ind_ProvinsiIdenNew.Text = getPropinsiidentype.Nama
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_ID_Propinsi_Old) = True Then
            TxtB2IdenPengirimNas_Ind_ProvinsiIdenOld.Text = getPropinsiidentype_old.Nama
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_ID_PropinsiLainnya) = True Then
        '    TxtB2IdenPengirimNas_Ind_ProvinsilainIdenNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_PropinsiLainnya
        'End If
        TxtB2IdenPengirimNas_Ind_ProvinsilainIdenOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_PropinsiLainnya_Old
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_FK_IFTI_IDType) = True Then
        '    CboB2PengirimNasInd_jenisidentitasNew.SelectedValue = DataNonSwiftIn.Sender_Nasabah_INDV_FK_IFTI_IDType
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_FK_IFTI_IDType_Old) = True Then
            CboB2PengirimNasInd_jenisidentitasOld.SelectedValue = DataNonSwiftIn.Sender_Nasabah_INDV_FK_IFTI_IDType_Old
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_NomorID) = True Then
        '    TxtB2IdenPengirimNas_Ind_noIdentitasNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NomorID
        'End If
        TxtB2IdenPengirimNas_Ind_noIdentitasOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NomorID_Old
    End Sub
    Private Sub PengirimPenerusNasabahNew()

        '---------------------------------------------Negara-------------------------------------'
        Dim idnegara As String = DataNonSwiftIn.Sender_Nasabah_INDV_Negara.GetValueOrDefault(0)
        'Dim idnegara_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_Negara_Old.GetValueOrDefault(0)
        Dim getnegaratype As MsNegara
        'Dim getnegaratype_old As MsNegara
        getnegaratype = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara)
        'getnegaratype_old = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara_old)

        '------------------------------------------------pekerjaan-------------------------------------'
        Dim idpekerjaan As String = DataNonSwiftIn.Sender_Nasabah_INDV_Pekerjaan.GetValueOrDefault(0)
        'Dim idpekerjaan_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_Pekerjaan_Old.GetValueOrDefault(0)
        Dim getpekerjaantype As MsPekerjaan
        'Dim getpekerjaantype_old As MsPekerjaan
        getpekerjaantype = DataRepository.MsPekerjaanProvider.GetByIDPekerjaan(idpekerjaan)
        'getpekerjaantype_old = DataRepository.MsPekerjaanProvider.GetByIDPekerjaan(idpekerjaan_old)

        '------------------------------------------------Kota kab---------------------------------------------'
        Dim idKotakab As String = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_KotaKab.GetValueOrDefault(0)
        'Dim idKotakab_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_KotaKab_Old.GetValueOrDefault(0)
        Dim getKotakabtype As MsKotaKab
        'Dim getKotakabtype_old As MsKotaKab
        getKotakabtype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab)
        'getKotakabtype_old = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab_old)

        '-------------------------------------------------propinsi------------------------------------------------'
        Dim idPropinsi As String = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_Propinsi.GetValueOrDefault(0)
        'Dim idPropinsi_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_Propinsi_Old.GetValueOrDefault(0)
        Dim getPropinsitype As MsProvince
        'Dim getPropinsitype_old As MsProvince
        getPropinsitype = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi)
        'getPropinsitype_old = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi_old)

        '------------------------------------------------Kota kab Iden---------------------------------------------'
        Dim idKotakabiden As String = DataNonSwiftIn.Sender_Nasabah_INDV_ID_KotaKab.GetValueOrDefault(0)
        'Dim idKotakabiden_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_KotaKab_Old.GetValueOrDefault(0)
        Dim getKotakabidentype As MsKotaKab
        'Dim getKotakabidentype_old As MsKotaKab
        getKotakabidentype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakabiden)
        'getKotakabidentype_old = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab_old)

        '-------------------------------------------------propinsi Iden------------------------------------------------'
        Dim idPropinsiiden As String = DataNonSwiftIn.Sender_Nasabah_INDV_ID_Propinsi.GetValueOrDefault(0)
        'Dim idPropinsiiden_old As String = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_Propinsi_Old.GetValueOrDefault(0)
        Dim getPropinsiidentype As MsProvince
        'Dim getPropinsiidentype_old As MsProvince
        getPropinsiidentype = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsiiden)
        'getPropinsiidentype_old = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi_old)

        '==================================NASABAH INDIVIDU========================================='
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_NamaBank) = True Then
            TxtB2IdenPengirimNas_Ind_NamaBankNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NamaBank
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_NamaBank_Old) = True Then
        '    TxtB2IdenPengirimNas_Ind_NamaBankOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NamaBank_Old
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_NamaLengkap) = True Then
            TxtB2IdenPengirimNas_Ind_NamaLengkapNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NamaLengkap
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_NamaLengkap_Old) = True Then
        '    TxtB2IdenPengirimNas_Ind_NamaLengkapOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NamaLengkap_Old
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_TanggalLahir) = True Then
            TxtB2IdenPengirimNas_Ind_tanggalLahirNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_TanggalLahir
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_TanggalLahir_Old) = True Then
        '    TxtB2IdenPengirimNas_Ind_tanggalLahirOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_TanggalLahir_Old
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_KewargaNegaraan) = True Then
            RbB2_IdenPengirimNas_Ind_kewarganegaraanNew.SelectedValue = DataNonSwiftIn.Sender_Nasabah_INDV_KewargaNegaraan.GetValueOrDefault
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_KewargaNegaraan_Old) = True Then
        '    RbB2_IdenPengirimNas_Ind_kewarganegaraanOld.SelectedValue = DataNonSwiftIn.Sender_Nasabah_INDV_KewargaNegaraan_Old.GetValueOrDefault
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_Negara) = True Then
            TxtB2IdenPengirimNas_Ind_negaraNew.Text = getnegaratype.NamaNegara
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_Negara_Old) = True Then
        '    TxtB2IdenPengirimNas_Ind_negaraOld.Text = getnegaratype_old.NamaNegara
        'End If

        TxtB2IdenPengirimNas_Ind_pekerjaanNew.Text = getpekerjaantype.NamaPekerjaan
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_Pekerjaan_Old) = True Then
        '    TxtB2IdenPengirimNas_Ind_pekerjaanOld.Text = getpekerjaantype_old.NamaPekerjaan
        'End If

        TxtB2IdenPengirimNas_Ind_PekerjaanlainNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_PekerjaanLainnya
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_PekerjaanLainnya_Old) = True Then
        '    TxtB2IdenPengirimNas_Ind_PekerjaanlainOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_PekerjaanLainnya_Old
        'End If

        TxtB2IdenPengirimNas_Ind_AlamatNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_Alamat
        'TxtB2IdenPengirimNas_Ind_AlamatOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_Alamat_Old
        TxtB2IdenPengirimNas_Ind_KotaNew.Text = getKotakabtype.NamaKotaKab
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_DOM_KotaKab_Old) = True Then
        '    TxtB2IdenPengirimNas_Ind_KotaOld.Text = getKotakabtype_old.NamaKotaKab
        'End If
        TxtB2IdenPengirimNas_Ind_kotalainNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_KotaKabLainnya
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_DOM_KotaKabLainnya_Old) = True Then
        '    TxtB2IdenPengirimNas_Ind_kotalainOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_KotaKabLainnya_Old
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_DOM_Propinsi) = True Then
            TxtB2IdenPengirimNas_Ind_provinsiNew.Text = getPropinsitype.Nama
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_DOM_Propinsi_Old) = True Then
        '    TxtB2IdenPengirimNas_Ind_provinsiOld.Text = getPropinsitype_old.Nama
        'End If
        TxtB2IdenPengirimNas_Ind_provinsiLainNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_PropinsiLainnya
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_DOM_PropinsiLainnya_Old) = True Then
        '    TxtB2IdenPengirimNas_Ind_provinsiLainOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_DOM_PropinsiLainnya_Old
        'End If

        TxtB2IdenPengirimNas_Ind_alamatIdentitasNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_Alamat
        'TxtB2IdenPengirimNas_Ind_alamatIdentitasOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_Alamat_Old
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_ID_KotaKab) = True Then
            TxtB2IdenPengirimNas_Ind_kotaIdentitasNew.Text = getKotakabidentype.NamaKotaKab
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_ID_KotaKab_Old) = True Then
        '    TxtB2IdenPengirimNas_Ind_kotaIdentitasOld.Text = getKotakabidentype_old.NamaKotaKab
        'End If
        TxtB2IdenPengirimNas_Ind_KotaLainIdenNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_KotaKabLainnya
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_ID_KotaKabLainnya_Old) = True Then
        '    TxtB2IdenPengirimNas_Ind_KotaLainIdenOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_KotaKabLainnya_Old
        'End If
        TxtB2IdenPengirimNas_Ind_ProvinsiIdenNew.Text = getPropinsiidentype.Nama
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_ID_Propinsi_Old) = True Then
        '    TxtB2IdenPengirimNas_Ind_ProvinsiIdenOld.Text = getPropinsiidentype_old.Nama
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_ID_PropinsiLainnya) = True Then
            TxtB2IdenPengirimNas_Ind_ProvinsilainIdenNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_PropinsiLainnya
        End If
        'TxtB2IdenPengirimNas_Ind_ProvinsilainIdenOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_ID_PropinsiLainnya_Old
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_FK_IFTI_IDType) = True Then
            CboB2PengirimNasInd_jenisidentitasNew.SelectedValue = DataNonSwiftIn.Sender_Nasabah_INDV_FK_IFTI_IDType
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_FK_IFTI_IDType_Old) = True Then
        '    CboB2PengirimNasInd_jenisidentitasOld.SelectedValue = DataNonSwiftIn.Sender_Nasabah_INDV_FK_IFTI_IDType_Old
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_NomorID) = True Then
            TxtB2IdenPengirimNas_Ind_noIdentitasNew.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NomorID
        End If
        'TxtB2IdenPengirimNas_Ind_noIdentitasOld.Text = DataNonSwiftIn.Sender_Nasabah_INDV_NomorID_Old
    End Sub
    Private Sub PengirimPenerusKorporasi()

        '---------------------------------------Badan Usaha------------------------------------'
        'Dim idBadan As String = DataNonSwiftIn.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id.GetValueOrDefault(0)
        Dim idBadan_old As String = DataNonSwiftIn.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id_Old.GetValueOrDefault(0)
        'Dim getBadantype As MsBidangUsaha
        Dim getBadantype_old As MsBidangUsaha
        'getBadantype = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(idBadan)
        getBadantype_old = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(idBadan_old)

        '-------------------------------------Bentuk Badan Usaha---------------------------------------'
        'Dim idBentuk As String = DataNonSwiftIn.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault(0)
        Dim idBentuk_old As String = DataNonSwiftIn.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id_Old.GetValueOrDefault(0)
        'Dim getBentuktype As MsBentukBidangUsaha
        Dim getBentuktype_old As MsBentukBidangUsaha
        'getBentuktype = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(idBentuk)
        getBentuktype_old = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(idBentuk_old)

        '------------------------------------------------Kota kab---------------------------------------------'
        'Dim idKotakab As String = DataNonSwiftIn.Sender_Nasabah_COR_KotaKab.GetValueOrDefault(0)
        Dim idKotakab_old As String = DataNonSwiftIn.Sender_Nasabah_COR_KotaKab_Old.GetValueOrDefault(0)
        'Dim getKotakabtype As MsKotaKab
        Dim getKotakabtype_old As MsKotaKab
        'getKotakabtype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab)
        getKotakabtype_old = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab_old)

        '-------------------------------------------------propinsi------------------------------------------------'
        'Dim idPropinsi As String = DataNonSwiftIn.Sender_Nasabah_CORP_Propinsi.GetValueOrDefault(0)
        Dim idPropinsi_old As String = DataNonSwiftIn.Sender_Nasabah_CORP_Propinsi_Old.GetValueOrDefault(0)
        'Dim getPropinsitype As MsProvince
        Dim getPropinsitype_old As MsProvince
        'getPropinsitype = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi)
        getPropinsitype_old = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi_old)

        '=========================================KORPORASI============================================='
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_NamaBank) = True Then
        '    TxtB2IdenPengirimNas_Corp_NamaBankNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NamaBank
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_NamaBank_Old) = True Then
            TxtB2IdenPengirimNas_Corp_NamaBankOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NamaBank_Old
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id) = True Then
        '    TxtB2IdenPengirimNas_Corp_BentukBadanUsahaNew.Text = getBentuktype.BentukBidangUsaha
        'End If
        TxtB2IdenPengirimNas_Corp_BentukBadanUsahaOld.Text = getBentuktype_old.BentukBidangUsaha
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_BentukBadanUsahaLainnya) = True Then
        '    TxtB2IdenPengirimNas_Corp_BentukBadanUsahaLainNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_BentukBadanUsahaLainnya
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_BentukBadanUsahaLainnya_Old) = True Then
            TxtB2IdenPengirimNas_Corp_BentukBadanUsahaLainOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_BentukBadanUsahaLainnya_Old
        End If
        'TxtB2IdenPengirimNas_Corp_NamaKorpNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NamaKorporasi
        TxtB2IdenPengirimNas_Corp_NamaKorpOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NamaKorporasi_Old
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id) = True Then
        '    TxtB2IdenPengirimNas_Corp_BidangUsahaKorpNew.Text = getBadantype.NamaBidangUsaha
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id_Old) = True Then
            TxtB2IdenPengirimNas_Corp_BidangUsahaKorpOld.Text = getBadantype_old.NamaBidangUsaha
        End If
        'TxtB2IdenPengirimNas_Corp_BidangUsahaLainnyaKorpNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_BidangUsahaLainnya
        TxtB2IdenPengirimNas_Corp_BidangUsahaLainnyaKorpOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_BidangUsahaLainnya_Old
        'TxtB2IdenPengirimNas_Corp_alamatkorpNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_AlamatLengkap
        TxtB2IdenPengirimNas_Corp_alamatkorpOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_AlamatLengkap_Old
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_COR_KotaKab) = True Then
        '    TxtB2IdenPengirimNas_Corp_kotakorpNew.Text = getKotakabtype.NamaKotaKab
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_COR_KotaKab_Old) = True Then
            TxtB2IdenPengirimNas_Corp_kotakorpOld.Text = getKotakabtype_old.NamaKotaKab
        End If
        'TxtB2IdenPengirimNas_Corp_kotakorplainNew.Text = DataNonSwiftIn.Sender_Nasabah_COR_KotaKabLainnya
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_COR_KotaKabLainnya_Old) = True Then
            TxtB2IdenPengirimNas_Corp_kotakorplainOld.Text = DataNonSwiftIn.Sender_Nasabah_COR_KotaKabLainnya_Old
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_Propinsi) = True Then
        '    TxtB2IdenPengirimNas_Corp_provKorpNew.Text = getPropinsitype.Nama
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_Propinsi_Old) = True Then
            TxtB2IdenPengirimNas_Corp_provKorpOld.Text = getPropinsitype_old.Nama
        End If
        'TxtB2IdenPengirimNas_Corp_provKorpLainNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_PropinsiLainnya
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_PropinsiLainnya_Old) = True Then
            TxtB2IdenPengirimNas_Corp_provKorpLainOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_PropinsiLainnya_Old
        End If
        'TxtB2IdenPengirimNas_Corp_AlamatLuarNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_LN_AlamatLengkap
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_LN_AlamatLengkap_Old) = True Then
            TxtB2IdenPengirimNas_Corp_AlamatLuarOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_LN_AlamatLengkap_Old
        End If
        'TxtB2IdenPengirimNas_Corp_AlamatAsalNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_Alamat_KantorCabangPengirim
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_Alamat_KantorCabangPengirim_Old) = True Then
            TxtB2IdenPengirimNas_Corp_AlamatAsalOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_Alamat_KantorCabangPengirim_Old
        End If
    End Sub
    Private Sub PengirimPenerusKorporasiNew()

        '---------------------------------------Badan Usaha------------------------------------'
        Dim idBadan As String = DataNonSwiftIn.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id.GetValueOrDefault(0)
        'Dim idBadan_old As String = DataNonSwiftIn.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id_Old.GetValueOrDefault(0)
        Dim getBadantype As MsBidangUsaha
        'Dim getBadantype_old As MsBidangUsaha
        getBadantype = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(idBadan)
        'getBadantype_old = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(idBadan_old)

        '-------------------------------------Bentuk Badan Usaha---------------------------------------'
        Dim idBentuk As String = DataNonSwiftIn.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault(0)
        'Dim idBentuk_old As String = DataNonSwiftIn.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id_Old.GetValueOrDefault(0)
        Dim getBentuktype As MsBentukBidangUsaha
        'Dim getBentuktype_old As MsBentukBidangUsaha
        getBentuktype = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(idBentuk)
        'getBentuktype_old = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(idBentuk)

        '------------------------------------------------Kota kab---------------------------------------------'
        Dim idKotakab As String = DataNonSwiftIn.Sender_Nasabah_COR_KotaKab.GetValueOrDefault(0)
        'Dim idKotakab_old As String = DataNonSwiftIn.Sender_Nasabah_COR_KotaKab_Old.GetValueOrDefault(0)
        Dim getKotakabtype As MsKotaKab
        'Dim getKotakabtype_old As MsKotaKab
        getKotakabtype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab)
        'getKotakabtype_old = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab_old)

        '-------------------------------------------------propinsi------------------------------------------------'
        Dim idPropinsi As String = DataNonSwiftIn.Sender_Nasabah_CORP_Propinsi.GetValueOrDefault(0)
        'Dim idPropinsi_old As String = DataNonSwiftIn.Sender_Nasabah_CORP_Propinsi_Old.GetValueOrDefault(0)
        Dim getPropinsitype As MsProvince
        'Dim getPropinsitype_old As MsProvince
        getPropinsitype = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi)
        'getPropinsitype_old = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi_old)

        '=========================================KORPORASI============================================='
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_NamaBank) = True Then
            TxtB2IdenPengirimNas_Corp_NamaBankNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NamaBank
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_INDV_NamaBank_Old) = True Then
        '    TxtB2IdenPengirimNas_Corp_NamaBankOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NamaBank_Old
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id) = True Then
            TxtB2IdenPengirimNas_Corp_BentukBadanUsahaNew.Text = getBentuktype.BentukBidangUsaha
        End If
        'TxtB2IdenPengirimNas_Corp_BentukBadanUsahaOld.Text = getBentuktype_old.BentukBidangUsaha
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_BentukBadanUsahaLainnya) = True Then
            TxtB2IdenPengirimNas_Corp_BentukBadanUsahaLainNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_BentukBadanUsahaLainnya
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_BentukBadanUsahaLainnya_Old) = True Then
        '    TxtB2IdenPengirimNas_Corp_BentukBadanUsahaLainOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_BentukBadanUsahaLainnya_Old
        'End If
        TxtB2IdenPengirimNas_Corp_NamaKorpNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NamaKorporasi
        'TxtB2IdenPengirimNas_Corp_NamaKorpOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_NamaKorporasi_Old
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id) = True Then
            TxtB2IdenPengirimNas_Corp_BidangUsahaKorpNew.Text = getBadantype.NamaBidangUsaha
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id_Old) = True Then
        '    TxtB2IdenPengirimNas_Corp_BidangUsahaKorpOld.Text = getBadantype_old.NamaBidangUsaha
        'End If
        TxtB2IdenPengirimNas_Corp_BidangUsahaLainnyaKorpNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_BidangUsahaLainnya
        'TxtB2IdenPengirimNas_Corp_BidangUsahaLainnyaKorpOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_BidangUsahaLainnya_Old
        TxtB2IdenPengirimNas_Corp_alamatkorpNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_AlamatLengkap
        'TxtB2IdenPengirimNas_Corp_alamatkorpOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_AlamatLengkap_Old
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_COR_KotaKab) = True Then
            TxtB2IdenPengirimNas_Corp_kotakorpNew.Text = getKotakabtype.NamaKotaKab
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_COR_KotaKab_Old) = True Then
        '    TxtB2IdenPengirimNas_Corp_kotakorpOld.Text = getKotakabtype_old.NamaKotaKab
        'End If
        TxtB2IdenPengirimNas_Corp_kotakorplainNew.Text = DataNonSwiftIn.Sender_Nasabah_COR_KotaKabLainnya
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_COR_KotaKabLainnya_Old) = True Then
        '    TxtB2IdenPengirimNas_Corp_kotakorplainOld.Text = DataNonSwiftIn.Sender_Nasabah_COR_KotaKabLainnya_Old
        'End If
        If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_Propinsi) = True Then
            TxtB2IdenPengirimNas_Corp_provKorpNew.Text = getPropinsitype.Nama
        End If
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_Propinsi_Old) = True Then
        '    TxtB2IdenPengirimNas_Corp_provKorpOld.Text = getPropinsitype_old.Nama
        'End If
        TxtB2IdenPengirimNas_Corp_provKorpLainNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_PropinsiLainnya
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_PropinsiLainnya_Old) = True Then
        '    TxtB2IdenPengirimNas_Corp_provKorpLainOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_PropinsiLainnya_Old
        'End If
        TxtB2IdenPengirimNas_Corp_AlamatLuarNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_LN_AlamatLengkap
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_LN_AlamatLengkap_Old) = True Then
        '    TxtB2IdenPengirimNas_Corp_AlamatLuarOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_LN_AlamatLengkap_Old
        'End If
        TxtB2IdenPengirimNas_Corp_AlamatAsalNew.Text = DataNonSwiftIn.Sender_Nasabah_CORP_Alamat_KantorCabangPengirim
        'If ObjectAntiNull(DataNonSwiftIn.Sender_Nasabah_CORP_Alamat_KantorCabangPengirim_Old) = True Then
        '    TxtB2IdenPengirimNas_Corp_AlamatAsalOld.Text = DataNonSwiftIn.Sender_Nasabah_CORP_Alamat_KantorCabangPengirim_Old
        'End If
    End Sub
    Private Sub BenefNasabah()

        '------------------------------------------------Kota kab---------------------------------------------'
        Dim idKotakab As String = DataNonSwiftIn.BeneficialOwner_ID_KotaKab.GetValueOrDefault(0)
        Dim idKotakab_old As String = DataNonSwiftIn.BeneficialOwner_ID_KotaKab_Old.GetValueOrDefault(0)
        Dim getKotakabtype As MsKotaKab
        Dim getKotakabtype_old As MsKotaKab
        getKotakabtype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab)
        getKotakabtype_old = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab_old)

        '-------------------------------------------------propinsi------------------------------------------------'
        Dim idPropinsi As String = DataNonSwiftIn.BeneficialOwner_ID_Propinsi.GetValueOrDefault(0)
        Dim idPropinsi_old As String = DataNonSwiftIn.BeneficialOwner_ID_Propinsi_Old.GetValueOrDefault(0)
        Dim getPropinsitype As MsProvince
        Dim getPropinsitype_old As MsProvince
        getPropinsitype = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi)
        getPropinsitype_old = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi_old)

        '============================================NASABAH==========================================='

        BenfOwnerNasabah_NamaNew.Text = DataNonSwiftIn.BeneficialOwner_NamaLengkap
        BenfOwnerNasabah_NamaOld.Text = DataNonSwiftIn.BeneficialOwner_NamaLengkap_Old
        If ObjectAntiNull(DataNonSwiftIn.BeneficialOwner_TanggalLahir) = True Then
            BenfOwnerNasabah_tanggalLahirNew.Text = DataNonSwiftIn.BeneficialOwner_TanggalLahir
        End If
        If ObjectAntiNull(DataNonSwiftIn.BeneficialOwner_TanggalLahir_Old) = True Then
            BenfOwnerNasabah_tanggalLahirOld.Text = DataNonSwiftIn.BeneficialOwner_TanggalLahir_Old
        End If
        If ObjectAntiNull(DataNonSwiftIn.BeneficialOwner_ID_Alamat) = True Then
            BenfOwnerNasabah_AlamatIdenNew.Text = DataNonSwiftIn.BeneficialOwner_ID_Alamat
        End If
        BenfOwnerNasabah_AlamatIdenOld.Text = DataNonSwiftIn.BeneficialOwner_ID_Alamat_Old
        If ObjectAntiNull(DataNonSwiftIn.BeneficialOwner_ID_KotaKab) = True Then
            BenfOwnerNasabah_KotaIdenNew.Text = getKotakabtype.NamaKotaKab
        End If
        If ObjectAntiNull(DataNonSwiftIn.BeneficialOwner_ID_KotaKab_Old) = True Then
            BenfOwnerNasabah_KotaIdenOld.Text = getKotakabtype_old.NamaKotaKab
        End If
        BenfOwnerNasabah_kotaLainIdenNew.Text = DataNonSwiftIn.BeneficialOwner_ID_KotaKabLainnya
        BenfOwnerNasabah_kotaLainIdenOld.Text = DataNonSwiftIn.BeneficialOwner_ID_KotaKabLainnya_Old
        If ObjectAntiNull(DataNonSwiftIn.BeneficialOwner_ID_Propinsi) = True Then
            BenfOwnerNasabah_ProvinsiIdenNew.Text = getPropinsitype.Nama
        End If
        If ObjectAntiNull(DataNonSwiftIn.BeneficialOwner_ID_Propinsi_Old) = True Then
            BenfOwnerNasabah_ProvinsiIdenOld.Text = getPropinsitype_old.Nama
        End If
        BenfOwnerNasabah_ProvinsiLainIdenNew.Text = DataNonSwiftIn.BeneficialOwner_ID_PropinsiLainnya
        BenfOwnerNasabah_ProvinsiLainIdenOld.Text = DataNonSwiftIn.BeneficialOwner_ID_PropinsiLainnya_Old
        If ObjectAntiNull(DataNonSwiftIn.BeneficialOwner_FK_IFTI_IDType) = True Then
            CBOBenfOwnerNasabah_JenisDokumenNew.Text = DataNonSwiftIn.BeneficialOwner_FK_IFTI_IDType
        End If
        If ObjectAntiNull(DataNonSwiftIn.BeneficialOwner_FK_IFTI_IDType_Old) = True Then
            CBOBenfOwnerNasabah_JenisDokumenOld.Text = DataNonSwiftIn.BeneficialOwner_FK_IFTI_IDType_Old
        End If
        If ObjectAntiNull(DataNonSwiftIn.BeneficialOwner_NomorID) = True Then
            BenfOwnerNasabah_NomorIdenMew.Text = DataNonSwiftIn.BeneficialOwner_NomorID
        End If
        BenfOwnerNasabah_NomorIdenOld.Text = DataNonSwiftIn.BeneficialOwner_NomorID_Old
    End Sub
    Private Sub BenefNonNasabah()

        '------------------------------------------------Kota kab---------------------------------------------'
        Dim idKotakab As String = DataNonSwiftIn.BeneficialOwner_ID_KotaKab.GetValueOrDefault(0)
        Dim idKotakab_old As String = DataNonSwiftIn.BeneficialOwner_ID_KotaKab_Old.GetValueOrDefault(0)
        Dim getKotakabtype As MsKotaKab
        Dim getKotakabtype_old As MsKotaKab
        getKotakabtype = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab)
        getKotakabtype_old = DataRepository.MsKotaKabProvider.GetByIDKotaKab(idKotakab_old)

        '-------------------------------------------------propinsi------------------------------------------------'
        Dim idPropinsi As String = DataNonSwiftIn.BeneficialOwner_ID_Propinsi.GetValueOrDefault(0)
        Dim idPropinsi_old As String = DataNonSwiftIn.BeneficialOwner_ID_Propinsi_Old.GetValueOrDefault(0)
        Dim getPropinsitype As MsProvince
        Dim getPropinsitype_old As MsProvince
        getPropinsitype = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi)
        getPropinsitype_old = DataRepository.MsProvinceProvider.GetByIdProvince(idPropinsi_old)

        '==================================================NON NASABAH=========================================='

        BenfOwnerNonNasabah_NamaNew.Text = DataNonSwiftIn.BeneficialOwner_NamaLengkap
        BenfOwnerNonNasabah_NamaOld.Text = DataNonSwiftIn.BeneficialOwner_NamaLengkap_Old
        If ObjectAntiNull(DataNonSwiftIn.BeneficialOwner_TanggalLahir) = True Then
            BenfOwnerNonNasabah_TanggalLahirNew.Text = DataNonSwiftIn.BeneficialOwner_TanggalLahir
        End If
        If ObjectAntiNull(DataNonSwiftIn.BeneficialOwner_TanggalLahir_Old) = True Then
            BenfOwnerNonNasabah_TanggalLahirOld.Text = DataNonSwiftIn.BeneficialOwner_TanggalLahir_Old
        End If
        BenfOwnerNonNasabah_AlamatIdenNew.Text = DataNonSwiftIn.BeneficialOwner_ID_Alamat
        BenfOwnerNonNasabah_AlamatIdenOld.Text = DataNonSwiftIn.BeneficialOwner_ID_Alamat_Old
        If Not IsNothing(getKotakabtype) Then
            BenfOwnerNonNasabah_KotaIdenNew.Text = getKotakabtype.NamaKotaKab
        End If
        If Not IsNothing(getKotakabtype_old) Then
            BenfOwnerNonNasabah_KotaIdenOld.Text = getKotakabtype_old.NamaKotaKab
        End If
        BenfOwnerNonNasabah_KotaLainIdenNew.Text = DataNonSwiftIn.BeneficialOwner_ID_KotaKabLainnya
        BenfOwnerNonNasabah_KotaLainIdenOld.Text = DataNonSwiftIn.BeneficialOwner_ID_KotaKabLainnya_Old
        If Not IsNothing(getPropinsitype) Then
            BenfOwnerNonNasabah_ProvinsiIdennew.Text = getPropinsitype.Nama
        End If
        If Not IsNothing(getPropinsitype_old) Then
            BenfOwnerNonNasabah_ProvinsiIdenOld.Text = getPropinsitype_old.Nama
        End If
        BenfOwnerNonNasabah_ProvinsiLainIdenNew.Text = DataNonSwiftIn.BeneficialOwner_ID_PropinsiLainnya
        If ObjectAntiNull(DataNonSwiftIn.BeneficialOwner_ID_PropinsiLainnya_Old) = True Then
            BenfOwnerNonNasabah_ProvinsiLainIdenOld.Text = DataNonSwiftIn.BeneficialOwner_ID_PropinsiLainnya_Old
        End If
        If ObjectAntiNull(DataNonSwiftIn.BeneficialOwner_FK_IFTI_IDType) = True Then
            CBOBenfOwnerNonNasabah_JenisDokumenNew.SelectedValue = DataNonSwiftIn.BeneficialOwner_FK_IFTI_IDType
        End If
        If ObjectAntiNull(DataNonSwiftIn.BeneficialOwner_FK_IFTI_IDType_Old) = True Then
            CBOBenfOwnerNonNasabah_JenisDokumenOld.SelectedValue = DataNonSwiftIn.BeneficialOwner_FK_IFTI_IDType_Old
        End If
        If ObjectAntiNull(DataNonSwiftIn.BeneficialOwner_NomorID) = True Then
            BenfOwnerNonNasabah_NomorIdentitasNew.Text = DataNonSwiftIn.BeneficialOwner_NomorID
        End If
        If ObjectAntiNull(DataNonSwiftIn.BeneficialOwner_NomorID_Old) = True Then
            BenfOwnerNonNasabah_NomorIdentitasOld.Text = DataNonSwiftIn.BeneficialOwner_NomorID_Old
        End If

    End Sub
    Private Sub PenerimaNasabah()

        '---------------------------------------------Negara-------------------------------------'
        Dim idnegara As String = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_Negara.GetValueOrDefault(0)
        Dim idnegara_old As String = Databenefeciary.Beneficiary_Nasabah_INDV_Negara.GetValueOrDefault(0)
        Dim getnegaratype As MsNegara
        Dim getnegaratype_old As MsNegara
        getnegaratype = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara)
        getnegaratype_old = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara_old)

        '---------------------------------------------Negara Iden-------------------------------------'
        Dim idnegaraiden As String = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Negara.GetValueOrDefault(0)
        Dim idnegaraiden_old As String = Databenefeciary.Beneficiary_Nasabah_INDV_ID_Negara.GetValueOrDefault(0)
        Dim getnegaraidentype As MsNegara
        Dim getnegaraidentype_old As MsNegara
        getnegaraidentype = DataRepository.MsNegaraProvider.GetByIDNegara(idnegaraiden)
        getnegaraidentype_old = DataRepository.MsNegaraProvider.GetByIDNegara(idnegaraiden_old)


        '============================================NASABAH==========================================='

        txtPenerimaNasabah_IND_namaNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NamaLengkap
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_NamaLengkap) = True Then
            txtPenerimaNasabah_IND_namaOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_NamaLengkap
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_TanggalLahir) = True Then
            txtPenerimaNasabah_IND_TanggalLahirNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_TanggalLahir
        End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_TanggalLahir) = True Then
            txtPenerimaNasabah_IND_TanggalLahirOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_TanggalLahir
        End If
        RbPenerimaNasabah_IND_WarganegaraNew.SelectedValue = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_KewargaNegaraan
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_KewargaNegaraan) = True Then
            RbPenerimaNasabah_IND_WarganegaraOld.SelectedValue = Databenefeciary.Beneficiary_Nasabah_INDV_KewargaNegaraan
        End If
        If Not IsNothing(getnegaratype) Then
            txtPenerimaNasabah_IND_negaraNew.Text = getnegaratype.NamaNegara
        End If
        If Not IsNothing(getnegaratype_old) Then
            txtPenerimaNasabah_IND_negaraOld.Text = getnegaratype_old.NamaNegara
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NegaraLainnya) = True Then
            txtPenerimaNasabah_IND_negaraLainNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NegaraLainnya
        End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_ID_NegaraLainnya) = True Then
            txtPenerimaNasabah_IND_negaraLainOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_ID_NegaraLainnya
        End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_ID_Alamat_Iden) = True Then
            txtPenerimaNasabah_IND_alamatIdenOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_ID_Alamat_Iden
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_NegaraBagian) = True Then
            txtPenerimaNasabah_IND_negaraBagianNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_NegaraBagian
        End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_ID_NegaraBagian) = True Then
            txtPenerimaNasabah_IND_negaraBagianOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_ID_NegaraBagian
        End If
        If Not IsNothing(getnegaraidentype) Then
            txtPenerimaNasabah_IND_negaraIdenNew.Text = getnegaraidentype.NamaNegara
        End If
        If Not IsNothing(getnegaraidentype_old) Then
            txtPenerimaNasabah_IND_negaraIdenOld.Text = getnegaraidentype_old.NamaNegara
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_NegaraLainnya) = True Then
            txtPenerimaNasabah_IND_negaraLainIdenNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_NegaraLainnya
        End If
        txtPenerimaNasabah_IND_negaraLainIdenOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_ID_NegaraLainnya
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_FK_IFTI_IDType) = True Then
            CBOPenerimaNasabah_IND_JenisIdenNew.SelectedValue = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_FK_IFTI_IDType
        End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_FK_IFTI_IDType) = True Then
            CBOPenerimaNasabah_IND_JenisIdenOld.SelectedValue = Databenefeciary.Beneficiary_Nasabah_INDV_FK_IFTI_IDType
        End If
        txtPenerimaNasabah_IND_NomorIdenNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NomorID
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_NomorID) = True Then
            txtPenerimaNasabah_IND_NomorIdenOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_NomorID
        End If
        txtPenerimaNasabah_IND_NilaiTransaksiKeuanganNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan) = True Then
            txtPenerimaNasabah_IND_NilaiTransaksiKeuanganOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan
        End If
    End Sub
    Private Sub PenerimaNasabahx()

        '---------------------------------------------Negara-------------------------------------'
        Dim idnegara As String = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_Negara.GetValueOrDefault(0)

        Dim getnegaratype As MsNegara

        getnegaratype = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara)


        '---------------------------------------------Negara Iden-------------------------------------'
        Dim idnegaraiden As String = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Negara.GetValueOrDefault(0)

        Dim getnegaraidentype As MsNegara

        getnegaraidentype = DataRepository.MsNegaraProvider.GetByIDNegara(idnegaraiden)



        '============================================NASABAH==========================================='

        txtPenerimaNasabah_IND_namaNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NamaLengkap
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_TanggalLahir) = True Then
            txtPenerimaNasabah_IND_TanggalLahirNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_TanggalLahir
        End If
        RbPenerimaNasabah_IND_WarganegaraNew.SelectedValue = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_KewargaNegaraan.GetValueOrDefault

        If Not IsNothing(getnegaratype) Then
            txtPenerimaNasabah_IND_negaraNew.Text = getnegaratype.NamaNegara
        End If

        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NegaraLainnya) = True Then
            txtPenerimaNasabah_IND_negaraLainNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NegaraLainnya
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Alamat_Iden) = True Then
            txtPenerimaNasabah_IND_alamatIdenNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_Alamat_Iden
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_NegaraBagian) = True Then
            txtPenerimaNasabah_IND_negaraBagianNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_NegaraBagian
        End If

        If Not IsNothing(getnegaraidentype) Then
            txtPenerimaNasabah_IND_negaraIdenNew.Text = getnegaraidentype.NamaNegara
        End If

        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_NegaraLainnya) = True Then
            txtPenerimaNasabah_IND_negaraLainIdenNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_ID_NegaraLainnya
        End If

        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_FK_IFTI_IDType) = True Then
            CBOPenerimaNasabah_IND_JenisIdenNew.SelectedValue = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_FK_IFTI_IDType
        End If

        txtPenerimaNasabah_IND_NomorIdenNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NomorID

        txtPenerimaNasabah_IND_NilaiTransaksiKeuanganNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan

    End Sub
    Private Sub PenerimaKorporasi()
        '---------------------------------------Bentuk Badan Usaha------------------------------------------'
        Dim idBentuk As String = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id.GetValueOrDefault(0)
        Dim idBentuk_old As String = Databenefeciary.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id.GetValueOrDefault(0)
        Dim getBentuktype As MsBentukBidangUsaha
        Dim getBentuktype_old As MsBentukBidangUsaha
        getBentuktype = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(idBentuk)
        getBentuktype_old = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(idBentuk)
        '---------------------------------------Bentuk Badan Usaha------------------------------------------'
        Dim idBadan As String = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault(0)
        Dim idBadan_old As String = Databenefeciary.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault(0)
        Dim getbadantype As MsBidangUsaha
        Dim getbadantype_old As MsBidangUsaha
        getbadantype = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(idBadan)
        getbadantype_old = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(idBadan)

        '---------------------------------------------Negara-------------------------------------'
        Dim idnegara As String = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_Negara.GetValueOrDefault(0)
        Dim idnegara_old As String = Databenefeciary.Beneficiary_Nasabah_CORP_ID_Negara.GetValueOrDefault(0)
        Dim getnegaratype As MsNegara
        Dim getnegaratype_old As MsNegara
        getnegaratype = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara)
        getnegaratype_old = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara_old)

        '==================================================KORPORASI=========================================='
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id) = True Then
            txtPenerimaNasabah_Korp_BentukBadanUsahaNew.Text = getBentuktype.BentukBidangUsaha
        End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id) = True Then
            txtPenerimaNasabah_Korp_BentukBadanUsahaOld.Text = getBentuktype_old.BentukBidangUsaha
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya) = True Then
            txtPenerimaNasabah_Korp_BentukBadanUsahaLainnyaNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya
        End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya) = True Then
            txtPenerimaNasabah_Korp_BentukBadanUsahaLainnyaOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_NamaKorporasi) = True Then
            txtPenerimaNasabah_Korp_namaKorpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_NamaKorporasi
        End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_NamaKorporasi) = True Then
            txtPenerimaNasabah_Korp_namaKorpOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_NamaKorporasi
        End If
        If Not IsNothing(getbadantype) Then
            txtPenerimaNasabah_Korp_BidangUsahaKorpNew.Text = getbadantype.NamaBidangUsaha
        End If
        If Not IsNothing(getbadantype_old) Then
            txtPenerimaNasabah_Korp_BidangUsahaKorpOld.Text = getbadantype_old.NamaBidangUsaha
        End If
        txtPenerimaNasabah_Korp_BidangUsahaLainKorpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_BidangUsahaLainnya
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_BidangUsahaLainnya) = True Then
            txtPenerimaNasabah_Korp_BidangUsahaLainKorpOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_BidangUsahaLainnya
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_AlamatLengkap) = True Then
            txtPenerimaNasabah_Korp_AlamatKorpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_AlamatLengkap
        End If
        txtPenerimaNasabah_Korp_AlamatKorpOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_AlamatLengkap
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_NegaraBagian) = True Then
            txtPenerimaNasabah_Korp_KotaNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_NegaraBagian
        End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_ID_NegaraBagian) = True Then
            txtPenerimaNasabah_Korp_KotaOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_ID_NegaraBagian
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_Negara) = True Then
            txtPenerimaNasabah_Korp_NegaraKorpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_Negara
        End If
        If Not IsNothing(getnegaratype_old) Then
            txtPenerimaNasabah_Korp_NegaraKorpOld.Text = getnegaratype_old.NamaNegara
        End If
        If Not IsNothing(getnegaratype) Then
            txtPenerimaNasabah_Korp_NilaiTransaksiKeuanganNew.Text = getnegaratype.NamaNegara
        End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan) = True Then
            txtPenerimaNasabah_Korp_NilaiTransaksiKeuanganOld.Text = Databenefeciary.Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan
        End If
    End Sub
    Private Sub PenerimaKorporasix()
        '---------------------------------------Bentuk Badan Usaha------------------------------------------'
        Dim idBadan As String = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id.GetValueOrDefault(0)

        Dim getBadantype As MsBidangUsaha

        getBadantype = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(idBadan)

        '---------------------------------------Bentuk Badan Usaha------------------------------------------'
        Dim idBentuk As String = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault(0)

        Dim getBentuktype As MsBentukBidangUsaha

        getBentuktype = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(idBentuk)

        '---------------------------------------------Negara-------------------------------------'
        Dim idnegara As String = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_Negara.GetValueOrDefault(0)

        Dim getnegaratype As MsNegara

        getnegaratype = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara)


        '==================================================KORPORASI=========================================='
        If Not IsNothing(getBentuktype) Then
            txtPenerimaNasabah_Korp_BentukBadanUsahaNew.Text = getBentuktype.BentukBidangUsaha
        End If

        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya) = True Then
            txtPenerimaNasabah_Korp_BentukBadanUsahaLainnyaNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya
        End If

        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_NamaKorporasi) = True Then
            txtPenerimaNasabah_Korp_namaKorpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_NamaKorporasi
        End If
       
        If Not IsNothing(getBadantype) Then
            txtPenerimaNasabah_Korp_BidangUsahaKorpNew.Text = getBadantype.NamaBidangUsaha
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_BidangUsahaLainnya) Then
            txtPenerimaNasabah_Korp_BidangUsahaLainKorpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_BidangUsahaLainnya
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_AlamatLengkap) = True Then
            txtPenerimaNasabah_Korp_AlamatKorpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_AlamatLengkap
        End If

        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_NegaraBagian) = True Then
            txtPenerimaNasabah_Korp_KotaNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_NegaraBagian
        End If

        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_Negara) = True Then
            txtPenerimaNasabah_Korp_NegaraKorpNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_ID_Negara
        End If

        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan) = True Then
            txtPenerimaNasabah_Korp_NilaiTransaksiKeuanganNew.Text = getnegaratype.NamaNegara
        End If

    End Sub
    Private Sub PenerimaNonNasabah()
        Me.RekeningNasabah.Visible = False
        '---------------------------------------------Negara-------------------------------------'
        Dim idnegara As String = DatabenefeciaryApproval.Beneficiary_NonNasabah_ID_Negara.GetValueOrDefault(0)
        Dim idnegara_old As String = Databenefeciary.Beneficiary_NonNasabah_ID_Negara.GetValueOrDefault(0)
        Dim getnegaratype As MsNegara
        Dim getnegaratype_old As MsNegara
        getnegaratype = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara)
        getnegaratype_old = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara_old)

        '==================================================NON NASABAH============================================='
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_NonNasabah_NoRekening) = True Then
            txtPenerimaNonNasabah_rekeningNew.Text = DatabenefeciaryApproval.Beneficiary_NonNasabah_NoRekening
        End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_NonNasabah_NoRekening) = True Then
            txtPenerimaNonNasabah_rekeningOld.Text = Databenefeciary.Beneficiary_NonNasabah_NoRekening
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_NonNasabah_NamaBank) = True Then
            txtPenerimaNonNasabah_namabankNew.Text = DatabenefeciaryApproval.Beneficiary_NonNasabah_NamaBank
        End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_NonNasabah_NamaBank) = True Then
            txtPenerimaNonNasabah_namabankOld.Text = Databenefeciary.Beneficiary_NonNasabah_NamaBank
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_NonNasabah_NamaLengkap) = True Then
            txtPenerimaNonNasabah_NamaNew.Text = DatabenefeciaryApproval.Beneficiary_NonNasabah_NamaLengkap
        End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_NonNasabah_NamaLengkap) = True Then
            txtPenerimaNonNasabah_NamaOld.Text = Databenefeciary.Beneficiary_NonNasabah_NamaLengkap
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_NonNasabah_ID_Alamat) = True Then
            txtPenerimaNonNasabah_AlamatNew.Text = DatabenefeciaryApproval.Beneficiary_NonNasabah_ID_Alamat
        End If
        txtPenerimaNonNasabah_AlamatOld.Text = Databenefeciary.Beneficiary_NonNasabah_ID_Alamat
        If Not IsNothing(getnegaratype) Then
            txtPenerimaNonNasabah_NegaraNew.Text = getnegaratype.NamaNegara
        End If
        If Not IsNothing(getnegaratype_old) Then
            txtPenerimaNonNasabah_NegaraOld.Text = getnegaratype_old.NamaNegara
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_NonNasabah_ID_NegaraLainnya) = True Then
            txtPenerimaNonNasabah_negaraLainNew.Text = DatabenefeciaryApproval.Beneficiary_NonNasabah_ID_NegaraLainnya
        End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_NonNasabah_ID_NegaraLainnya) = True Then
            txtPenerimaNonNasabah_negaraLainOld.Text = Databenefeciary.Beneficiary_NonNasabah_ID_NegaraLainnya
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_NonNasabah_NilaiTransaksikeuangan) = True Then
            txtPenerimaNonNasabah_nilaiTransaksiKeuanganNew.Text = DatabenefeciaryApproval.Beneficiary_NonNasabah_NilaiTransaksikeuangan
        End If
        If ObjectAntiNull(Databenefeciary.Beneficiary_NonNasabah_NilaiTransaksikeuangan) = True Then
            txtPenerimaNonNasabah_nilaiTransaksiKeuanganOld.Text = Databenefeciary.Beneficiary_NonNasabah_NilaiTransaksikeuangan
        End If
    End Sub
    Private Sub PenerimaNonNasabahx()
        Me.RekeningNasabah.Visible = False
        '---------------------------------------------Negara-------------------------------------'
        Dim idnegara As String = DatabenefeciaryApproval.Beneficiary_NonNasabah_ID_Negara.GetValueOrDefault(0)
        Dim getnegaratype As MsNegara
        getnegaratype = DataRepository.MsNegaraProvider.GetByIDNegara(idnegara)
       
        '==================================================NON NASABAH============================================='
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_NonNasabah_NoRekening) = True Then
            txtPenerimaNonNasabah_rekeningNew.Text = DatabenefeciaryApproval.Beneficiary_NonNasabah_NoRekening
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_NonNasabah_NamaBank) = True Then
            txtPenerimaNonNasabah_namabankNew.Text = DatabenefeciaryApproval.Beneficiary_NonNasabah_NamaBank
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_NonNasabah_NamaLengkap) = True Then
            txtPenerimaNonNasabah_NamaNew.Text = DatabenefeciaryApproval.Beneficiary_NonNasabah_NamaLengkap
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_NonNasabah_ID_Alamat) = True Then
            txtPenerimaNonNasabah_AlamatNew.Text = DatabenefeciaryApproval.Beneficiary_NonNasabah_ID_Alamat
        End If
        If Not IsNothing(getnegaratype) Then
            txtPenerimaNonNasabah_NegaraNew.Text = getnegaratype.NamaNegara
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_NonNasabah_ID_NegaraLainnya) = True Then
            txtPenerimaNonNasabah_negaraLainNew.Text = DatabenefeciaryApproval.Beneficiary_NonNasabah_ID_NegaraLainnya
        End If
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_NonNasabah_NilaiTransaksikeuangan) = True Then
            txtPenerimaNonNasabah_nilaiTransaksiKeuanganNew.Text = DatabenefeciaryApproval.Beneficiary_NonNasabah_NilaiTransaksikeuangan
        End If

    End Sub
    Private Sub Transaksi()
        '+++++++++++++++++++++++++++++++++++++++++++++TRANSAKSI+++++++++++++++++++++++++++++++++++++++++++++'
        Dim idCurrent As Integer = DataNonSwiftIn.ValueDate_FK_Currency_ID.GetValueOrDefault(0)
        Dim idcurrent_old As Integer = DataNonSwiftIn.ValueDate_FK_Currency_ID_Old.GetValueOrDefault(0)
        Dim getbadantype As MsCurrency
        Dim getbadantype_old As MsCurrency
        getbadantype = DataRepository.MsCurrencyProvider.GetByIdCurrency(idCurrent)
        getbadantype_old = DataRepository.MsCurrencyProvider.GetByIdCurrency(idcurrent_old)

        Dim idCurrent1 As String = DataNonSwiftIn.Instructed_Currency.GetValueOrDefault(0)
        Dim idcurrent1_old As String = DataNonSwiftIn.Instructed_Currency_Old.GetValueOrDefault(0)
        Dim getbadantype1 As MsCurrency
        Dim getbadantype1_old As MsCurrency
        getbadantype1 = DataRepository.MsCurrencyProvider.GetByIdCurrency(idCurrent1)
        getbadantype1_old = DataRepository.MsCurrencyProvider.GetByIdCurrency(idcurrent1_old)

        Transaksi_SwiftIntanggalNew.Text = DataNonSwiftIn.TanggalTransaksi
        Transaksi_SwiftIntanggalOld.Text = DataNonSwiftIn.TanggalTransaksi_Old
        Transaksi_SwiftInwaktutransaksiNew.Text = DataNonSwiftIn.TanggalTransaksi
        Transaksi_SwiftInwaktutransaksiOld.Text = DataNonSwiftIn.TanggalTransaksi_Old
        Transaksi_SwiftInSenderNew.Text = DataNonSwiftIn.SenderReference
        Transaksi_SwiftInSenderOld.Text = DataNonSwiftIn.SenderReference_Old
        Transaksi_SwiftInBankOperationCodeNew.Text = DataNonSwiftIn.BankOperationCode
        Transaksi_SwiftInBankOperationCodeOld.Text = DataNonSwiftIn.BankOperationCode_Old
        Transaksi_SwiftInInstructionCodeNew.Text = DataNonSwiftIn.InstructionCode
        Transaksi_SwiftInInstructionCodeOld.Text = DataNonSwiftIn.InstructionCode_Old
        Transaksi_SwiftInkantorCabangPengirimNew.Text = DataNonSwiftIn.KantorCabangPenyelengaraPengirimAsal
        Transaksi_SwiftInkantorCabangPengirimOld.Text = DataNonSwiftIn.KantorCabangPenyelengaraPengirimAsal_Old
        Transaksi_SwiftInkodeTipeTransaksiNew.Text = DataNonSwiftIn.TransactionCode
        Transaksi_SwiftInkodeTipeTransaksiOld.Text = DataNonSwiftIn.TransactionCode_Old
        If ObjectAntiNull(DataNonSwiftIn.TanggalTransaksi) = True Then
            Transaksi_SwiftInValueTanggalTransaksiNew.Text = DataNonSwiftIn.TanggalTransaksi
        End If
        If ObjectAntiNull(DataNonSwiftIn.TanggalTransaksi_Old) = True Then
            Transaksi_SwiftInValueTanggalTransaksiOld.Text = DataNonSwiftIn.TanggalTransaksi_Old
        End If
        If ObjectAntiNull(DataNonSwiftIn.ValueDate_NilaiTransaksi) = True Then
            TtxNilaitransold.Text = DataNonSwiftIn.ValueDate_NilaiTransaksi
        End If
        If ObjectAntiNull(DataNonSwiftIn.ValueDate_NilaiTransaksi_Old) = True Then
            TtxNilaitransold.Text = DataNonSwiftIn.ValueDate_NilaiTransaksi_Old
        End If
        If ObjectAntiNull(DataNonSwiftIn.ValueDate_FK_Currency_ID) = True Then
            Transaksi_SwiftInMataUangTransaksiNew.Text = getbadantype.Name
        End If
        If ObjectAntiNull(DataNonSwiftIn.ValueDate_FK_Currency_ID_Old) = True Then
            Transaksi_SwiftInMataUangTransaksiOld.Text = getbadantype_old.Name
        End If
        If ObjectAntiNull(DataNonSwiftIn.ValueDate_CurrencyLainnya) = True Then
            Transaksi_SwiftInMataUangTransaksiLainnyaNew.Text = DataNonSwiftIn.ValueDate_CurrencyLainnya
        End If
        Transaksi_SwiftInMataUangTransaksiLainnyaOld.Text = DataNonSwiftIn.ValueDate_CurrencyLainnya_Old
        If ObjectAntiNull(DataNonSwiftIn.ValueDate_NilaiTransaksiIDR) = True Then
            Transaksi_SwiftInAmountdalamRupiahNew.Text = DataNonSwiftIn.ValueDate_NilaiTransaksiIDR
        End If
        If ObjectAntiNull(DataNonSwiftIn.ValueDate_NilaiTransaksiIDR_Old) = True Then
            Transaksi_SwiftInAmountdalamRupiahOld.Text = DataNonSwiftIn.ValueDate_NilaiTransaksiIDR_Old
        End If
        If ObjectAntiNull(DataNonSwiftIn.Instructed_Currency) = True Then
            Transaksi_SwiftIncurrencyNew.Text = getStringFieldValue(getbadantype1.Code)
        End If
        If ObjectAntiNull(DataNonSwiftIn.Instructed_Currency_Old) = True Then
            Transaksi_SwiftIncurrencyOld.Text = getStringFieldValue(getbadantype1_old.Code)
        End If
        If ObjectAntiNull(DataNonSwiftIn.Instructed_CurrencyLainnya) = True Then
            Transaksi_SwiftIncurrencyLainnyaNew.Text = DataNonSwiftIn.Instructed_CurrencyLainnya
        End If
        Transaksi_SwiftIncurrencyLainnyaOld.Text = DataNonSwiftIn.Instructed_CurrencyLainnya_Old
        If ObjectAntiNull(DataNonSwiftIn.Instructed_Amount) = True Then
            Transaksi_SwiftIninstructedAmountNew.Text = DataNonSwiftIn.Instructed_Amount
        End If
        If ObjectAntiNull(DataNonSwiftIn.Instructed_Amount_Old) = True Then
            Transaksi_SwiftIninstructedAmountOld.Text = DataNonSwiftIn.Instructed_Amount_Old
        End If
        If ObjectAntiNull(DataNonSwiftIn.ExchangeRate) = True Then
            Transaksi_SwiftInnilaiTukarNew.Text = DataNonSwiftIn.ExchangeRate
        End If
        If ObjectAntiNull(DataNonSwiftIn.ExchangeRate_Old) = True Then
            Transaksi_SwiftInnilaiTukarOld.Text = DataNonSwiftIn.ExchangeRate_Old
        End If
        If ObjectAntiNull(DataNonSwiftIn.SendingInstitution) = True Then
            Transaksi_SwiftInsendingInstitutionNew.Text = DataNonSwiftIn.SendingInstitution
        End If
        Transaksi_SwiftInsendingInstitutionOld.Text = DataNonSwiftIn.SendingInstitution_Old
        If ObjectAntiNull(DataNonSwiftIn.TujuanTransaksi) = True Then
            Transaksi_SwiftInTujuanTransaksiNew.Text = DataNonSwiftIn.TujuanTransaksi
        End If
        If ObjectAntiNull(DataNonSwiftIn.TujuanTransaksi_Old) = True Then
            Transaksi_SwiftInTujuanTransaksiOld.Text = DataNonSwiftIn.TujuanTransaksi_Old
        End If
        If ObjectAntiNull(DataNonSwiftIn.SumberPenggunaanDana) = True Then
            Transaksi_SwiftInSumberPenggunaanDanaNew.Text = DataNonSwiftIn.SumberPenggunaanDana
        End If
        If ObjectAntiNull(DataNonSwiftIn.SumberPenggunaanDana_Old) = True Then
            Transaksi_SwiftInSumberPenggunaanDanaOld.Text = DataNonSwiftIn.SumberPenggunaanDana_Old
        End If
    End Sub
    Private Sub InfoLain()
        '++++++++++++++++++++++++++++++++++++++++++INFORMASI LAINNYA+++++++++++++++++++++++++++++++++++++++++'

        InformasiLainnya_SWIFTOutSenderNew.Text = DataNonSwiftIn.InformationAbout_SenderCorrespondent
        InformasiLainnya_SWIFTOutSenderOld.Text = DataNonSwiftIn.InformationAbout_SenderCorrespondent_Old
        InformasiLainnya_SWIFTOutreceiverNew.Text = DataNonSwiftIn.InformationAbout_ReceiverCorrespondent
        InformasiLainnya_SWIFTOutreceiverOld.Text = DataNonSwiftIn.InformationAbout_ReceiverCorrespondent_Old
        InformasiLainnya_SWIFTOutthirdReimbursementNew.Text = DataNonSwiftIn.InformationAbout_Thirdreimbursementinstitution
        InformasiLainnya_SWIFTOutthirdReimbursementOld.Text = DataNonSwiftIn.InformationAbout_Thirdreimbursementinstitution_Old
        InformasiLainnya_SWIFTOutintermediaryNew.Text = DataNonSwiftIn.InformationAbout_IntermediaryInstitution
        InformasiLainnya_SWIFTOutintermediaryOld.Text = DataNonSwiftIn.InformationAbout_IntermediaryInstitution_Old
        InformasiLainnya_SWIFTOutRemittanceNew.Text = DataNonSwiftIn.RemittanceInformation
        InformasiLainnya_SWIFTOutRemittanceOld.Text = DataNonSwiftIn.RemittanceInformation_Old
        InformasiLainnya_SWIFTOutSenderToReceiverNew.Text = DataNonSwiftIn.SendertoReceiverInformation
        InformasiLainnya_SWIFTOutSenderToReceiverOld.Text = DataNonSwiftIn.SendertoReceiverInformation_Old
        InformasiLainnya_SWIFTOutRegulatoryReportNew.Text = DataNonSwiftIn.RegulatoryReporting
        InformasiLainnya_SWIFTOutRegulatoryReportOld.Text = DataNonSwiftIn.RegulatoryReporting_Old
        InformasiLainnya_SWIFTOutEnvelopeContentsNew.Text = DataNonSwiftIn.EnvelopeContents
        InformasiLainnya_SWIFTOutEnvelopeContentsOld.Text = DataNonSwiftIn.EnvelopeContents_Old
    End Sub

#End Region

#Region "Function"
    Private Sub ClearThisPageSessions()

        SetnGetRowTotal = Nothing
        SetnGetSort = Nothing
        SetnGetCurrentPage = 0
    End Sub
    Sub HideControl()
        BtnReject.Visible = False
        BtnSave.Visible = False
        BtnCancel.Visible = False
    End Sub
    Private Sub LoadData()
        Dim PkObject As String
        Dim ObjIFTI_Approval As vw_IFTI_Approval = DataRepository.vw_IFTI_ApprovalProvider.GetPaged("PK_IFTI_Approval_id = " & parID, "", 0, Integer.MaxValue, 0)(0)
        With ObjIFTI_Approval
            SafeDefaultValue = "-"
            txtMsUser_StaffName.Text = .RequestedBy
            txtRequestedDate1.Text = .RequestedDate

            'other info
            Dim Omsuser As User
            Omsuser = DataRepository.UserProvider.GetBypkUserID(.RequestedBy)
            If Omsuser IsNot Nothing Then
                txtMsUser_StaffName.Text = Omsuser.UserName
            Else
                txtMsUser_StaffName.Text = SafeDefaultValue
            End If

            Omsuser = DataRepository.UserProvider.GetBypkUserID(.RequestedBy)
            If Omsuser IsNot Nothing Then
                txtMsUser_StaffName.Text = Omsuser.UserName
            Else
                txtMsUser_StaffName.Text = SafeDefaultValue
            End If
        End With
        PkObject = ObjIFTI_Approval.PK_IFTI_Approval_Id

    End Sub
#End Region

#Region "Event"

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        Response.Redirect("IFTI_Approvals.aspx")
    End Sub


    Protected Sub BtnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReject.Click
        Try
            Dim Key_ApprovalID As String = "0"
            Dim Idx As IFTI_Approval_Detail = DataRepository.IFTI_Approval_DetailProvider.GetPaged("FK_IFTI_Approval_id = " & parID, "", 0, Integer.MaxValue, 0)(0)
            '----------------------------------------------------------------------------------------------------------------------------------------------------------------
            Dim IFTI_ApprovalDetail As IFTI_Approval_Detail
            Dim IFTI_Approval As IFTI_Approval
            Dim IFTI_BENEFICIARY_APPROVAL As IFTI_Approval_Beneficiary
            'Get IFTI Approval beneficiary
            IFTI_BENEFICIARY_APPROVAL = DataRepository.IFTI_Approval_BeneficiaryProvider.GetPaged( _
                IFTI_Approval_BeneficiaryColumn.FK_IFTI_Approval_Id.ToString & "=" & Idx.FK_IFTI_Approval_Id.ToString, _
                "", 0, Integer.MaxValue, Nothing)(0)
            'Get IFTI Approval detail
            IFTI_ApprovalDetail = DataRepository.IFTI_Approval_DetailProvider.GetPaged( _
                IFTI_Approval_DetailColumn.PK_IFTI_Approval_Detail_Id.ToString & "=" & Idx.PK_IFTI_Approval_Detail_Id.ToString, _
                "", 0, Integer.MaxValue, Nothing)(0)
            'Get IFTI Approval
            IFTI_Approval = DataRepository.IFTI_ApprovalProvider.GetPaged( _
                IFTI_ApprovalColumn.PK_IFTI_Approval_Id.ToString & "=" & Idx.FK_IFTI_Approval_Id.ToString, _
                "", 0, Integer.MaxValue, Nothing)(0)
            Key_ApprovalID = IFTI_ApprovalDetail.PK_IFTI_ID
            'Get Key IFTI
            Dim IFTIID As String = IFTI_ApprovalDetail.PK_IFTI_ID

            'delete IFTI_approval and IFTI_ApprovalDetail

            DataRepository.IFTI_Approval_DetailProvider.Delete(IFTI_ApprovalDetail)
            DataRepository.IFTI_ApprovalProvider.Delete(IFTI_Approval)
            DataRepository.IFTI_Approval_BeneficiaryProvider.Delete(IFTI_BENEFICIARY_APPROVAL)

            BtnCancel.Visible = False

            MultiView1.ActiveViewIndex = 1
            LblConfirmation.Text = "Rejected Success"

            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = "Rejected Success"
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub BtnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        'Dim oSQLTrans As SqlTransaction = Nothing
        Dim test As Integer = Session("FK_IFTI_Beneficiary_ID")
        Dim otrans As TransactionManager = Nothing

        Try
            otrans = New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)
            otrans.BeginTransaction()

            Using iftiApprovalDetailAdapter As IFTI_Approval_Detail = DataRepository.IFTI_Approval_DetailProvider.GetPaged("FK_IFTI_Approval_id =" & parID, "", 0, Integer.MaxValue, 0)(0)

                Using RulesiftiApprovalDetailAdapter As IFTI_Approval_Detail = DataRepository.IFTI_Approval_DetailProvider.GetPaged("FK_IFTI_Approval_id =" & parID, "", 0, Integer.MaxValue, 0)(0)
                    Using IFTIAdapter As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(RulesiftiApprovalDetailAdapter.PK_IFTI_ID)

                        'Bila RulesBasic_ApprovalTable.Rows.Count > 0 berarti Basic Rule tsb masih ada dlm tabel RulesBasic_Approval
                        If Not RulesiftiApprovalDetailAdapter Is Nothing Then
                            Dim IFTIApprovalTableRow As IFTI_Approval_Detail
                            IFTIApprovalTableRow = RulesiftiApprovalDetailAdapter


                            Dim Counter As Integer
                            'Using IFTIQueryAdapter As New AMLDAL.BasicRulesDataSetTableAdapters.QueriesTableAdapter
                            'Using IFTIQueryAdapter As TList(Of IFTI) = DataRepository.IFTIProvider.GetPaged("LTDLNNO='" & SWIFTOutUmum_LTDNNew.Text & "'", "", 0, Integer.MaxValue, 0)
                            '    'Counter = RulesBasicQueryAdapter.Count.ToString(IFTIApprovalTableRow.KYE_RulesBasicName)
                            'End Using

                            AcceptDeleteBeneficiary()

                            'Bila counter = 0 berarti Data IFTI tsb belum ada dlm tabel IFTI, maka boleh ditambahkan
                            If Counter = 0 Then
                                With IFTIAdapter
                                    .IsDataValid = 1
                                    .FK_IFTI_Type_ID = IFTIApprovalTableRow.FK_IFTI_Type_ID
                                    .LTDLNNo = IFTIApprovalTableRow.LTDLNNo
                                    .LTDLNNoKoreksi = IFTIApprovalTableRow.LTDLNNoKoreksi
                                    .TanggalLaporan = IFTIApprovalTableRow.TanggalLaporan
                                    .NamaPJKBankPelapor = IFTIApprovalTableRow.NamaPJKBankPelapor
                                    .NamaPejabatPJKBankPelapor = IFTIApprovalTableRow.NamaPejabatPJKBankPelapor
                                    .JenisLaporan = IFTIApprovalTableRow.JenisLaporan
                                    .Sender_FK_IFTI_NasabahType_ID = IFTIApprovalTableRow.Sender_FK_IFTI_NasabahType_ID
                                    .Sender_Nasabah_INDV_NamaBank = IFTIApprovalTableRow.Sender_Nasabah_INDV_NamaBank
                                    .Sender_Nasabah_INDV_NoRekening = IFTIApprovalTableRow.Sender_Nasabah_INDV_NoRekening
                                    .Sender_Nasabah_INDV_NamaLengkap = IFTIApprovalTableRow.Sender_Nasabah_INDV_NamaLengkap
                                    .Sender_Nasabah_INDV_TanggalLahir = IFTIApprovalTableRow.Sender_Nasabah_INDV_TanggalLahir
                                    .Sender_Nasabah_INDV_KewargaNegaraan = IFTIApprovalTableRow.Sender_Nasabah_INDV_KewargaNegaraan
                                    .Sender_Nasabah_INDV_Negara = IFTIApprovalTableRow.Sender_Nasabah_INDV_KewargaNegaraan
                                    .Sender_Nasabah_INDV_NegaraLainnya = IFTIApprovalTableRow.Sender_Nasabah_INDV_NegaraLainnya
                                    .Sender_Nasabah_INDV_Pekerjaan = IFTIApprovalTableRow.Sender_Nasabah_INDV_Pekerjaan
                                    .Sender_Nasabah_INDV_PekerjaanLainnya = IFTIApprovalTableRow.Sender_Nasabah_INDV_PekerjaanLainnya
                                    .Sender_Nasabah_INDV_DOM_Alamat = IFTIApprovalTableRow.Sender_Nasabah_INDV_DOM_Alamat
                                    .Sender_Nasabah_INDV_DOM_Propinsi = IFTIApprovalTableRow.Sender_Nasabah_INDV_DOM_Propinsi
                                    .Sender_Nasabah_INDV_DOM_PropinsiLainnya = IFTIApprovalTableRow.Sender_Nasabah_INDV_DOM_PropinsiLainnya
                                    .Sender_Nasabah_INDV_DOM_KotaKab = IFTIApprovalTableRow.Sender_Nasabah_INDV_DOM_KotaKab
                                    .Sender_Nasabah_INDV_DOM_KotaKabLainnya = IFTIApprovalTableRow.Sender_Nasabah_INDV_DOM_KotaKabLainnya
                                    .Sender_Nasabah_INDV_ID_Alamat = IFTIApprovalTableRow.Sender_Nasabah_INDV_ID_Alamat
                                    .Sender_Nasabah_INDV_ID_Propinsi = IFTIApprovalTableRow.Sender_Nasabah_INDV_ID_Propinsi
                                    .Sender_Nasabah_INDV_ID_PropinsiLainnya = IFTIApprovalTableRow.Sender_Nasabah_INDV_ID_PropinsiLainnya
                                    .Sender_Nasabah_INDV_ID_KotaKab = IFTIApprovalTableRow.Sender_Nasabah_INDV_ID_KotaKab
                                    .Sender_Nasabah_INDV_ID_KotaKabLainnya = IFTIApprovalTableRow.Sender_Nasabah_INDV_ID_KotaKabLainnya
                                    .Sender_Nasabah_INDV_FK_IFTI_IDType = IFTIApprovalTableRow.Sender_Nasabah_INDV_FK_IFTI_IDType
                                    .Sender_Nasabah_INDV_NomorID = IFTIApprovalTableRow.Sender_Nasabah_INDV_NomorID
                                    .Sender_Nasabah_CORP_NoRekening = IFTIApprovalTableRow.Sender_Nasabah_CORP_NoRekening
                                    .Sender_Nasabah_CORP_NamaBank = IFTIApprovalTableRow.Sender_Nasabah_CORP_NamaBank
                                    .Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id = IFTIApprovalTableRow.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id
                                    .Sender_Nasabah_CORP_BentukBadanUsahaLainnya = IFTIApprovalTableRow.Sender_Nasabah_CORP_BentukBadanUsahaLainnya
                                    .Sender_Nasabah_CORP_NamaKorporasi = IFTIApprovalTableRow.Sender_Nasabah_CORP_NamaKorporasi
                                    .Sender_Nasabah_CORP_FK_MsBidangUsaha_Id = IFTIApprovalTableRow.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id
                                    .Sender_Nasabah_CORP_BidangUsahaLainnya = IFTIApprovalTableRow.Sender_Nasabah_CORP_BidangUsahaLainnya
                                    .Sender_Nasabah_CORP_AlamatLengkap = IFTIApprovalTableRow.Sender_Nasabah_CORP_AlamatLengkap
                                    .Sender_Nasabah_CORP_Propinsi = IFTIApprovalTableRow.Sender_Nasabah_CORP_Propinsi
                                    .Sender_Nasabah_CORP_PropinsiLainnya = IFTIApprovalTableRow.Sender_Nasabah_CORP_PropinsiLainnya
                                    .Sender_Nasabah_CORP_KotaKab = IFTIApprovalTableRow.Sender_Nasabah_COR_KotaKab
                                    .Sender_Nasabah_CORP_KotaKabLainnya = IFTIApprovalTableRow.Sender_Nasabah_COR_KotaKabLainnya
                                    .Sender_Nasabah_CORP_LN_AlamatLengkap = IFTIApprovalTableRow.Sender_Nasabah_CORP_LN_AlamatLengkap
                                    .Sender_Nasabah_CORP_Alamat_KantorCabangPengirim = IFTIApprovalTableRow.Sender_Nasabah_CORP_Alamat_KantorCabangPengirim
                                    .FK_IFTI_NonNasabahNominalType_ID = IFTIApprovalTableRow.FK_IFTI_NonNasabahNominalType_ID
                                    .Sender_NonNasabah_NamaBank = IFTIApprovalTableRow.Sender_NonNasabah_NamaBank
                                    .Sender_NonNasabah_NamaLengkap = IFTIApprovalTableRow.Sender_NonNasabah_NamaLengkap
                                    .Sender_NonNasabah_TanggalLahir = IFTIApprovalTableRow.Sender_NonNasabah_TanggalLahir
                                    .Sender_NonNasabah_ID_Alamat = IFTIApprovalTableRow.Sender_NonNasabah_ID_Alamat
                                    .Sender_NonNasabah_ID_Propinsi = IFTIApprovalTableRow.Sender_NonNasabah_ID_Propinsi
                                    .Sender_NonNasabah_ID_PropinsiLainnya = IFTIApprovalTableRow.Sender_NonNasabah_ID_PropinsiLainnya
                                    .Sender_NonNasabah_ID_KotaKab = IFTIApprovalTableRow.Sender_NonNasabah_ID_KotaKab
                                    .Sender_NonNasabah_ID_KotaKabLainnya = IFTIApprovalTableRow.Sender_NonNasabah_ID_KotaKabLainnya
                                    .Sender_NonNasabah_FK_IFTI_IDType = IFTIApprovalTableRow.Sender_NonNasabah_FK_IFTI_IDType
                                    .Sender_NonNasabah_NomorID = IFTIApprovalTableRow.Sender_NonNasabah_NomorID
                                    .FK_IFTI_BeneficialOwnerType_ID = IFTIApprovalTableRow.FK_IFTI_BeneficialOwnerType_ID
                                    .FK_IFTI_KeterlibatanBeneficialOwner_ID = IFTIApprovalTableRow.BeneficialOwner_Keterlibatan
                                    .BeneficialOwner_HubunganDenganPemilikDana = IFTIApprovalTableRow.BeneficialOwner_HubunganDenganPemilikDana
                                    .BeneficialOwner_NoRekening = IFTIApprovalTableRow.BeneficialOwner_NoRekening
                                    .BeneficialOwner_NamaLengkap = IFTIApprovalTableRow.BeneficialOwner_NamaLengkap
                                    .BeneficialOwner_TanggalLahir = IFTIApprovalTableRow.BeneficialOwner_TanggalLahir
                                    .BeneficialOwner_ID_Alamat = IFTIApprovalTableRow.BeneficialOwner_ID_Alamat
                                    .BeneficialOwner_ID_Propinsi = IFTIApprovalTableRow.BeneficialOwner_ID_Propinsi
                                    .BeneficialOwner_ID_PropinsiLainnya = IFTIApprovalTableRow.BeneficialOwner_ID_PropinsiLainnya
                                    .BeneficialOwner_ID_KotaKab = IFTIApprovalTableRow.BeneficialOwner_ID_KotaKab
                                    .BeneficialOwner_ID_KotaKabLainnya = IFTIApprovalTableRow.BeneficialOwner_ID_KotaKabLainnya
                                    .BeneficialOwner_FK_IFTI_IDType = IFTIApprovalTableRow.BeneficialOwner_FK_IFTI_IDType
                                    .BeneficialOwner_NomorID = IFTIApprovalTableRow.BeneficialOwner_NomorID
                                    .TanggalTransaksi = IFTIApprovalTableRow.TanggalTransaksi
                                    .TimeIndication = IFTIApprovalTableRow.TimeIndication
                                    .SenderReference = IFTIApprovalTableRow.SenderReference
                                    .BankOperationCode = IFTIApprovalTableRow.BankOperationCode
                                    .InstructionCode = IFTIApprovalTableRow.InstructionCode
                                    .KantorCabangPenyelengaraPengirimAsal = IFTIApprovalTableRow.KantorCabangPenyelengaraPengirimAsal
                                    .TransactionCode = IFTIApprovalTableRow.TransactionCode
                                    .ValueDate_TanggalTransaksi = IFTIApprovalTableRow.ValueDate_TanggalTransaksi
                                    .ValueDate_NilaiTransaksi = IFTIApprovalTableRow.ValueDate_NilaiTransaksi
                                    .ValueDate_FK_Currency_ID = IFTIApprovalTableRow.ValueDate_FK_Currency_ID
                                    .ValueDate_CurrencyLainnya = IFTIApprovalTableRow.ValueDate_CurrencyLainnya
                                    .ValueDate_NilaiTransaksiIDR = IFTIApprovalTableRow.ValueDate_NilaiTransaksiIDR
                                    .Instructed_Currency = IFTIApprovalTableRow.Instructed_Currency
                                    .Instructed_CurrencyLainnya = IFTIApprovalTableRow.Instructed_CurrencyLainnya
                                    .Instructed_Amount = IFTIApprovalTableRow.Instructed_Amount
                                    .ExchangeRate = IFTIApprovalTableRow.ExchangeRate
                                    .SendingInstitution = IFTIApprovalTableRow.SendingInstitution
                                    .TujuanTransaksi = IFTIApprovalTableRow.TujuanTransaksi
                                    .SumberPenggunaanDana = IFTIApprovalTableRow.SumberPenggunaanDana
                                    .InformationAbout_SenderCorrespondent = IFTIApprovalTableRow.InformationAbout_SenderCorrespondent
                                    .InformationAbout_ReceiverCorrespondent = IFTIApprovalTableRow.InformationAbout_ReceiverCorrespondent
                                    .InformationAbout_Thirdreimbursementinstitution = IFTIApprovalTableRow.InformationAbout_Thirdreimbursementinstitution
                                    .InformationAbout_IntermediaryInstitution = IFTIApprovalTableRow.InformationAbout_IntermediaryInstitution
                                    .RemittanceInformation = IFTIApprovalTableRow.RemittanceInformation
                                    .SendertoReceiverInformation = IFTIApprovalTableRow.SendertoReceiverInformation
                                    .RegulatoryReporting = IFTIApprovalTableRow.RegulatoryReporting
                                    .EnvelopeContents = IFTIApprovalTableRow.EnvelopeContents
                                    .LastUpdateDate = Now.ToString("yyyy-MM-dd")
                                End With
                                DataRepository.IFTIProvider.Save(otrans, IFTIAdapter)

                                'delete item tersebut dalam table ifti_approval & ifti_approval_detail
                                Dim DeleteDetail As IFTI_Approval_Detail = DataRepository.IFTI_Approval_DetailProvider.GetPaged("FK_IFTI_Approval_Id =" & parID, "", 0, Integer.MaxValue, 0)(0)
                                DataRepository.IFTI_Approval_DetailProvider.Delete(DeleteDetail)

                                Dim delete As IFTI_Approval = DataRepository.IFTI_ApprovalProvider.GetByPK_IFTI_Approval_Id(parID)
                                DataRepository.IFTI_ApprovalProvider.Delete(otrans, delete)

                                'delete item error list dari ifti_errorlist
                                Dim DeleteError As TList(Of IFTI_ErrorDescription) = DataRepository.IFTI_ErrorDescriptionProvider.GetPaged("FK_IFTI_ID = " & IFTIAdapter.PK_IFTI_ID, "", 0, Integer.MaxValue, 0)

                                DataRepository.IFTI_ErrorDescriptionProvider.Delete(DeleteError)
                                GenerateListOfGeneratedIFTI(otrans)

                                otrans.Commit()
                            End If
                        End If
                    End Using
                End Using
            End Using

            MultiView1.ActiveViewIndex = 1
            LblConfirmation.Text = "Data Accepted to IFTI Table"
            'End Using
        Catch ex As Exception
            If Not otrans Is Nothing Then
                otrans.Rollback()
            End If
            LogError(ex)
            Throw
        Finally
            If Not otrans Is Nothing Then
                otrans.Dispose()
                otrans = Nothing
            End If
        End Try
    End Sub
#End Region


    Function GenerateListOfGeneratedIFTI(ByVal objtransaction As TransactionManager) As Boolean

        Using objCommand As SqlCommand = Commonly.GetSQLCommandStoreProcedure("usp_GenerateListOfGeneratedIfti")
            DataRepository.Provider.ExecuteNonQuery(objtransaction, objCommand)
        End Using


    End Function

    Private Sub AcceptDeleteBeneficiary()
        Dim otrans As TransactionManager = Nothing
        Try
            otrans = New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)
            otrans.BeginTransaction()
            For Each idx As IFTI_Approval_Beneficiary In SetnGetBindTable
                Dim objintFK_IFTI_Beneficiary_ID As Integer = 0
                If idx.FK_IFTI_Beneficiary_ID.ToString = "" Then
                    objintFK_IFTI_Beneficiary_ID = 0
                Else
                    objintFK_IFTI_Beneficiary_ID = idx.FK_IFTI_Beneficiary_ID.ToString
                End If
                Dim countID As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("PK_IFTI_Beneficiary_ID = " & objintFK_IFTI_Beneficiary_ID, "", 0, Integer.MaxValue, 0)
                If countID.Count = 1 Then
                    Dim benefapp As IFTI_Beneficiary = DataRepository.IFTI_BeneficiaryProvider.GetPaged("PK_IFTI_Beneficiary_ID = " & objintFK_IFTI_Beneficiary_ID, "", 0, Integer.MaxValue, 0)(0)
                    With benefapp
                        .FK_IFTI_ID = idx.FK_IFTI_ID
                        .FK_IFTI_NasabahType_ID = idx.FK_IFTI_NasabahType_ID
                        .Beneficiary_Nasabah_INDV_NoRekening = idx.Beneficiary_Nasabah_INDV_NoRekening
                        .Beneficiary_Nasabah_INDV_NamaLengkap = idx.Beneficiary_Nasabah_INDV_NamaLengkap
                        .Beneficiary_Nasabah_INDV_TanggalLahir = idx.Beneficiary_Nasabah_INDV_TanggalLahir
                        .Beneficiary_Nasabah_INDV_KewargaNegaraan = idx.Beneficiary_Nasabah_INDV_KewargaNegaraan
                        .Beneficiary_Nasabah_INDV_Pekerjaan = idx.Beneficiary_Nasabah_INDV_Pekerjaan
                        .Beneficiary_Nasabah_INDV_PekerjaanLainnya = idx.Beneficiary_Nasabah_INDV_PekerjaanLainnya
                        .Beneficiary_Nasabah_INDV_Negara = idx.Beneficiary_Nasabah_INDV_Negara
                        .Beneficiary_Nasabah_INDV_NegaraLainnya = idx.Beneficiary_Nasabah_INDV_NegaraLainnya
                        .Beneficiary_Nasabah_INDV_ID_Alamat_Dom = idx.Beneficiary_Nasabah_INDV_ID_Alamat_Dom
                        .Beneficiary_Nasabah_INDV_ID_KotaKab_Dom = idx.Beneficiary_Nasabah_INDV_ID_KotaKab_Dom
                        .Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom = idx.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom
                        .Beneficiary_Nasabah_INDV_ID_Propinsi_Dom = idx.Beneficiary_Nasabah_INDV_ID_Propinsi_Dom
                        .Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom = idx.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom
                        .Beneficiary_Nasabah_INDV_ID_Alamat_Iden = idx.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom
                        .Beneficiary_Nasabah_INDV_ID_KotaKab_Iden = idx.Beneficiary_Nasabah_INDV_ID_KotaKab_Iden
                        .Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden = idx.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden
                        .Beneficiary_Nasabah_INDV_ID_Propinsi_Iden = idx.Beneficiary_Nasabah_INDV_ID_Propinsi_Iden
                        .Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden = idx.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden
                        .Beneficiary_Nasabah_INDV_ID_NegaraBagian = idx.Beneficiary_Nasabah_INDV_ID_NegaraBagian
                        .Beneficiary_Nasabah_INDV_ID_Negara = idx.Beneficiary_Nasabah_INDV_ID_Negara
                        .Beneficiary_Nasabah_INDV_ID_NegaraLainnya = idx.Beneficiary_Nasabah_INDV_ID_NegaraLainnya
                        .Beneficiary_Nasabah_INDV_NoTelp = idx.Beneficiary_Nasabah_INDV_NoTelp
                        .Beneficiary_Nasabah_INDV_FK_IFTI_IDType = idx.Beneficiary_Nasabah_INDV_FK_IFTI_IDType
                        .Beneficiary_Nasabah_INDV_NomorID = idx.Beneficiary_Nasabah_INDV_NomorID
                        .Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan = idx.Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan
                        .Beneficiary_Nasabah_CORP_NoRekening = idx.Beneficiary_Nasabah_CORP_NoRekening
                        .Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id = idx.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id
                        .Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya = idx.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya
                        .Beneficiary_Nasabah_CORP_NamaKorporasi = idx.Beneficiary_Nasabah_CORP_NamaKorporasi
                        .Beneficiary_Nasabah_CORP_NoTelp = idx.Beneficiary_Nasabah_CORP_NoTelp
                        .Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan = idx.Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan
                        .Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id = idx.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id
                        .Beneficiary_Nasabah_CORP_BidangUsahaLainnya = idx.Beneficiary_Nasabah_CORP_BidangUsahaLainnya
                        .Beneficiary_Nasabah_CORP_AlamatLengkap = idx.Beneficiary_Nasabah_CORP_AlamatLengkap
                        .Beneficiary_Nasabah_CORP_ID_KotaKab = idx.Beneficiary_Nasabah_CORP_ID_KotaKab
                        .Beneficiary_Nasabah_CORP_ID_KotaKabLainnya = idx.Beneficiary_Nasabah_CORP_ID_KotaKabLainnya
                        .Beneficiary_Nasabah_CORP_ID_Propinsi = idx.Beneficiary_Nasabah_CORP_ID_Propinsi
                        .Beneficiary_Nasabah_CORP_ID_PropinsiLainnya = idx.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya
                        .Beneficiary_Nasabah_CORP_ID_NegaraBagian = idx.Beneficiary_Nasabah_CORP_ID_NegaraBagian
                        .Beneficiary_Nasabah_CORP_ID_Negara = idx.Beneficiary_Nasabah_CORP_ID_Negara
                        .Beneficiary_Nasabah_CORP_ID_NegaraLainnya = idx.Beneficiary_Nasabah_CORP_ID_NegaraLainnya
                        .Beneficiary_NonNasabah_NoRekening = idx.Beneficiary_NonNasabah_NoRekening
                        .Beneficiary_NonNasabah_KodeRahasia = idx.Beneficiary_NonNasabah_KodeRahasia
                        .Beneficiary_NonNasabah_NamaLengkap = idx.Beneficiary_NonNasabah_NamaLengkap
                        .Beneficiary_NonNasabah_NamaBank = idx.Beneficiary_NonNasabah_NamaBank
                        .Beneficiary_NonNasabah_TanggalLahir = idx.Beneficiary_NonNasabah_TanggalLahir
                        .Beneficiary_NonNasabah_NoTelp = idx.Beneficiary_NonNasabah_NoTelp
                        .Beneficiary_NonNasabah_NilaiTransaksikeuangan = idx.Beneficiary_NonNasabah_NilaiTransaksikeuangan
                        .Beneficiary_NonNasabah_ID_Alamat = idx.Beneficiary_NonNasabah_ID_Alamat
                        .Beneficiary_NonNasabah_ID_NegaraBagian = idx.Beneficiary_NonNasabah_ID_NegaraBagian
                        .Beneficiary_NonNasabah_ID_Negara = idx.Beneficiary_NonNasabah_ID_Negara
                        .Beneficiary_NonNasabah_ID_NegaraLainnya = idx.Beneficiary_NonNasabah_ID_NegaraLainnya
                        .Beneficiary_NonNasabah_FK_IFTI_IDType = idx.Beneficiary_NonNasabah_FK_IFTI_IDType
                        .Beneficiary_NonNasabah_NomorID = idx.Beneficiary_NonNasabah_NomorID
                    End With
                    DataRepository.IFTI_BeneficiaryProvider.Save(otrans, benefapp)

                Else
                    Dim newbenef As New IFTI_Beneficiary
                    With newbenef
                        .FK_IFTI_ID = idx.FK_IFTI_ID
                        .FK_IFTI_NasabahType_ID = idx.FK_IFTI_NasabahType_ID
                        .Beneficiary_Nasabah_INDV_NoRekening = idx.Beneficiary_Nasabah_INDV_NoRekening
                        .Beneficiary_Nasabah_INDV_NamaLengkap = idx.Beneficiary_Nasabah_INDV_NamaLengkap
                        .Beneficiary_Nasabah_INDV_TanggalLahir = idx.Beneficiary_Nasabah_INDV_TanggalLahir
                        .Beneficiary_Nasabah_INDV_KewargaNegaraan = idx.Beneficiary_Nasabah_INDV_KewargaNegaraan
                        .Beneficiary_Nasabah_INDV_Pekerjaan = idx.Beneficiary_Nasabah_INDV_Pekerjaan
                        .Beneficiary_Nasabah_INDV_PekerjaanLainnya = idx.Beneficiary_Nasabah_INDV_PekerjaanLainnya
                        .Beneficiary_Nasabah_INDV_Negara = idx.Beneficiary_Nasabah_INDV_Negara
                        .Beneficiary_Nasabah_INDV_NegaraLainnya = idx.Beneficiary_Nasabah_INDV_NegaraLainnya
                        .Beneficiary_Nasabah_INDV_ID_Alamat_Dom = idx.Beneficiary_Nasabah_INDV_ID_Alamat_Dom
                        .Beneficiary_Nasabah_INDV_ID_KotaKab_Dom = idx.Beneficiary_Nasabah_INDV_ID_KotaKab_Dom
                        .Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom = idx.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom
                        .Beneficiary_Nasabah_INDV_ID_Propinsi_Dom = idx.Beneficiary_Nasabah_INDV_ID_Propinsi_Dom
                        .Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom = idx.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom
                        .Beneficiary_Nasabah_INDV_ID_Alamat_Iden = idx.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom
                        .Beneficiary_Nasabah_INDV_ID_KotaKab_Iden = idx.Beneficiary_Nasabah_INDV_ID_KotaKab_Iden
                        .Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden = idx.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden
                        .Beneficiary_Nasabah_INDV_ID_Propinsi_Iden = idx.Beneficiary_Nasabah_INDV_ID_Propinsi_Iden
                        .Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden = idx.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden
                        .Beneficiary_Nasabah_INDV_ID_NegaraBagian = idx.Beneficiary_Nasabah_INDV_ID_NegaraBagian
                        .Beneficiary_Nasabah_INDV_ID_Negara = idx.Beneficiary_Nasabah_INDV_ID_Negara
                        .Beneficiary_Nasabah_INDV_ID_NegaraLainnya = idx.Beneficiary_Nasabah_INDV_ID_NegaraLainnya
                        .Beneficiary_Nasabah_INDV_NoTelp = idx.Beneficiary_Nasabah_INDV_NoTelp
                        .Beneficiary_Nasabah_INDV_FK_IFTI_IDType = idx.Beneficiary_Nasabah_INDV_FK_IFTI_IDType
                        .Beneficiary_Nasabah_INDV_NomorID = idx.Beneficiary_Nasabah_INDV_NomorID
                        .Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan = idx.Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan
                        .Beneficiary_Nasabah_CORP_NoRekening = idx.Beneficiary_Nasabah_CORP_NoRekening
                        .Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id = idx.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id
                        .Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya = idx.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya
                        .Beneficiary_Nasabah_CORP_NamaKorporasi = idx.Beneficiary_Nasabah_CORP_NamaKorporasi
                        .Beneficiary_Nasabah_CORP_NoTelp = idx.Beneficiary_Nasabah_CORP_NoTelp
                        .Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan = idx.Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan
                        .Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id = idx.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id
                        .Beneficiary_Nasabah_CORP_BidangUsahaLainnya = idx.Beneficiary_Nasabah_CORP_BidangUsahaLainnya
                        .Beneficiary_Nasabah_CORP_AlamatLengkap = idx.Beneficiary_Nasabah_CORP_AlamatLengkap
                        .Beneficiary_Nasabah_CORP_ID_KotaKab = idx.Beneficiary_Nasabah_CORP_ID_KotaKab
                        .Beneficiary_Nasabah_CORP_ID_KotaKabLainnya = idx.Beneficiary_Nasabah_CORP_ID_KotaKabLainnya
                        .Beneficiary_Nasabah_CORP_ID_Propinsi = idx.Beneficiary_Nasabah_CORP_ID_Propinsi
                        .Beneficiary_Nasabah_CORP_ID_PropinsiLainnya = idx.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya
                        .Beneficiary_Nasabah_CORP_ID_NegaraBagian = idx.Beneficiary_Nasabah_CORP_ID_NegaraBagian
                        .Beneficiary_Nasabah_CORP_ID_Negara = idx.Beneficiary_Nasabah_CORP_ID_Negara
                        .Beneficiary_Nasabah_CORP_ID_NegaraLainnya = idx.Beneficiary_Nasabah_CORP_ID_NegaraLainnya
                        .Beneficiary_NonNasabah_NoRekening = idx.Beneficiary_NonNasabah_NoRekening
                        .Beneficiary_NonNasabah_KodeRahasia = idx.Beneficiary_NonNasabah_KodeRahasia
                        .Beneficiary_NonNasabah_NamaLengkap = idx.Beneficiary_NonNasabah_NamaLengkap
                        .Beneficiary_NonNasabah_NamaBank = idx.Beneficiary_NonNasabah_NamaBank
                        .Beneficiary_NonNasabah_TanggalLahir = idx.Beneficiary_NonNasabah_TanggalLahir
                        .Beneficiary_NonNasabah_NoTelp = idx.Beneficiary_NonNasabah_NoTelp
                        .Beneficiary_NonNasabah_NilaiTransaksikeuangan = idx.Beneficiary_NonNasabah_NilaiTransaksikeuangan
                        .Beneficiary_NonNasabah_ID_Alamat = idx.Beneficiary_NonNasabah_ID_Alamat
                        .Beneficiary_NonNasabah_ID_NegaraBagian = idx.Beneficiary_NonNasabah_ID_NegaraBagian
                        .Beneficiary_NonNasabah_ID_Negara = idx.Beneficiary_NonNasabah_ID_Negara
                        .Beneficiary_NonNasabah_ID_NegaraLainnya = idx.Beneficiary_NonNasabah_ID_NegaraLainnya
                        .Beneficiary_NonNasabah_FK_IFTI_IDType = idx.Beneficiary_NonNasabah_FK_IFTI_IDType
                        .Beneficiary_NonNasabah_NomorID = idx.Beneficiary_NonNasabah_NomorID
                    End With
                    DataRepository.IFTI_BeneficiaryProvider.Save(otrans, newbenef)
                End If

                Dim delete As IFTI_Approval_Beneficiary = DataRepository.IFTI_Approval_BeneficiaryProvider.GetPaged("PK_IFTI_Approval_Beneficiary_ID =" & idx.PK_IFTI_Approval_Beneficiary_ID.ToString, "", 0, Integer.MaxValue, 0)(0)
                DataRepository.IFTI_Approval_BeneficiaryProvider.Delete(otrans, delete)

            Next

            'End Using
            otrans.Commit()
        Catch ex As Exception
            otrans.Rollback()
            Throw
        Finally
            If Not otrans Is Nothing Then
                otrans.Dispose()
                otrans = Nothing
            End If
        End Try
    End Sub
    Private Sub databindgrid()
        GV.CurrentPageIndex = SetnGetCurrentPage
        GV.VirtualItemCount = SetnGetRowTotal
        GV.DataSource = SetnGetBindTable
        GV.DataBind()
        If SetnGetRowTotal > 0 Then
            LabelNoRecordFound.Visible = False
        Else
            LabelNoRecordFound.Visible = True
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            LoadIDType()
            LoadData()
            bindgrid()
            If Not Page.IsPostBack Then
                ClearThisPageSessions()
                AuditTrailBLL.InsertAuditTrailUserAccess(SessionCurrentPage)
            End If
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub


    Protected Sub ImgBtnAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAdd.Click
        Response.Redirect("IFTI_Approvals.aspx")
    End Sub

    Protected Sub GV_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GV.EditCommand
        Dim TypeId1 As Int64
        Dim TypeId2 As Int64
        TypeId1 = e.Item.Cells(15).Text
        Session("PK_IFTI_Approval_Beneficiary_ID") = TypeId1
        If ObjectAntiNull(DatabenefeciaryApproval.FK_IFTI_Beneficiary_ID.ToString) = True Then
            TypeId2 = DatabenefeciaryApproval.FK_IFTI_Beneficiary_ID
        Else
            TypeId2 = ObjectAntiNull(e.Item.Cells(16).Text)
        End If
        Session("FK_IFTI_Beneficiary_ID") = DatabenefeciaryApproval.FK_IFTI_Beneficiary_ID.ToString
        Me.DataPenerima.ActiveViewIndex = 0
        Using getid As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("PK_IFTI_Beneficiary_ID = " & TypeId2, "", 0, Integer.MaxValue, 0)
            If (getid.Count = 1) Then
                LoadWithID()
            Else
                If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NoRekening) = True Then
                    txtPenerima_rekeningNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NoRekening
                End If

                If ObjectAntiNull(DatabenefeciaryApproval.FK_IFTI_NasabahType_ID) = True Then
                    Select Case DatabenefeciaryApproval.FK_IFTI_NasabahType_ID.GetValueOrDefault
                        Case 1
                            Me.RBPenerimaNasabah_TipePengirimNew.SelectedValue = 1
                            MultiViewSwiftOutIdenPenerima.ActiveViewIndex = 0
                        Case 2
                            Me.RBPenerimaNasabah_TipePengirimNew.SelectedValue = 1
                            MultiViewSwiftOutIdenPenerima.ActiveViewIndex = 0
                        Case 3
                            Me.RBPenerimaNasabah_TipePengirimNew.SelectedValue = 2
                            MultiViewSwiftOutIdenPenerima.ActiveViewIndex = 1
                            PenerimaNonNasabahx()
                    End Select
                End If

                If ObjectAntiNull(DatabenefeciaryApproval.FK_IFTI_NasabahType_ID) = True Then
                    Select Case DatabenefeciaryApproval.FK_IFTI_NasabahType_ID.GetValueOrDefault
                        Case 1
                            Me.Rb_IdenPenerimaNas_TipeNasabahNew.SelectedValue = 1
                            Me.MultiViewPenerimaNasabah_New.ActiveViewIndex = 0
                            PenerimaNasabahx()
                        Case 2
                            Me.Rb_IdenPenerimaNas_TipeNasabahNew.SelectedValue = 2
                            MultiViewPenerimaNasabah_New.ActiveViewIndex = 1
                            PenerimaKorporasix()
                    End Select
                End If
            End If
        End Using
    End Sub

    Protected Sub GV_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GV.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                If BindGridFromExcel = True Then
                    e.Item.Cells(0).Text = CStr((e.Item.ItemIndex + 1))
                Else
                    e.Item.Cells(0).Text = CStr((e.Item.ItemIndex + 1) + (SetnGetCurrentPage * GetDisplayedTotalRow))
                End If
            End If
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    Private Sub LoadWithID()
        Me.DataPenerima.ActiveViewIndex = 0
        If ObjectAntiNull(DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NoRekening) = True Then
            txtPenerima_rekeningNew.Text = DatabenefeciaryApproval.Beneficiary_Nasabah_INDV_NoRekening
        End If

        If ObjectAntiNull(Databenefeciary.Beneficiary_Nasabah_INDV_NoRekening) = True Then
            txtPenerima_rekeningOld.Text = Databenefeciary.Beneficiary_Nasabah_INDV_NoRekening
        End If

        If ObjectAntiNull(Databenefeciary.FK_IFTI_NasabahType_ID) = True Then
            Select Case Databenefeciary.FK_IFTI_NasabahType_ID.GetValueOrDefault
                Case 1
                    Me.RBPenerimaNasabah_TipePengirimOld.SelectedValue = 1
                    MultiViewSwiftOutIdenPenerima.ActiveViewIndex = 0
                Case 2
                    Me.RBPenerimaNasabah_TipePengirimOld.SelectedValue = 1
                    MultiViewSwiftOutIdenPenerima.ActiveViewIndex = 0
                Case 3
                    Me.RBPenerimaNasabah_TipePengirimOld.SelectedValue = 2
                    MultiViewSwiftOutIdenPenerima.ActiveViewIndex = 1
                    PenerimaNonNasabah()
            End Select
        End If

        If ObjectAntiNull(DatabenefeciaryApproval.FK_IFTI_NasabahType_ID) = True Then
            Select Case DatabenefeciaryApproval.FK_IFTI_NasabahType_ID.GetValueOrDefault
                Case 1
                    Me.RBPenerimaNasabah_TipePengirimNew.SelectedValue = 1
                Case 2
                    Me.RBPenerimaNasabah_TipePengirimNew.SelectedValue = 1
                Case 3
                    Me.RBPenerimaNasabah_TipePengirimNew.SelectedValue = 2
            End Select
        End If

        If ObjectAntiNull(Databenefeciary.FK_IFTI_NasabahType_ID) = True Then
            Select Case DatabenefeciaryApproval.FK_IFTI_NasabahType_ID.GetValueOrDefault
                Case 1
                    Me.Rb_IdenPenerimaNas_TipeNasabahOld.SelectedValue = 1
                    'MultiViewPenerimaNasabah.ActiveViewIndex = 0
                    ' PenerimaNasabah()
                Case 2
                    Me.Rb_IdenPenerimaNas_TipeNasabahOld.SelectedValue = 2
                    ' MultiViewPenerimaNasabah.ActiveViewIndex = 1
                    ' PenerimaKorporasi()

            End Select
        End If

        If ObjectAntiNull(DatabenefeciaryApproval.FK_IFTI_NasabahType_ID) = True Then
            Select Case DatabenefeciaryApproval.FK_IFTI_NasabahType_ID.GetValueOrDefault
                Case 1
                    Me.Rb_IdenPenerimaNas_TipeNasabahNew.SelectedValue = 1
                Case 2
                    Me.Rb_IdenPenerimaNas_TipeNasabahNew.SelectedValue = 2
            End Select
        End If
    End Sub
End Class
