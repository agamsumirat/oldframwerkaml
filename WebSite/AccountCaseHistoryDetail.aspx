<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AccountCaseHistoryDetail.aspx.vb" Inherits="AccountCaseHistoryDetail" MasterPageFile="~/MasterPage.master" %>

<%--<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CustomerInformation.aspx.vb" Inherits="CustomerInformation" MasterPageFile="~/MasterPage.master" %>--%>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
  <table id="title"  border="2" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr>

            <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/dot_title.gif" />
                <strong>Account Case History - Detail&nbsp;
                </strong>
            </td>
        </tr>
      <tr>
          <td bgcolor="#ffffff" colspan="3" style="font-size: 18px; border-top-style: none;
              border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
              <ajax:AjaxPanel ID="AjaxPanel3" runat="server">
                  <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></ajax:AjaxPanel></td>
      </tr>
        <tr>
            <td>
                    <table bgcolor="#dddddd" border="2" bordercolor="#ffffff" cellpadding="1" cellspacing="1"
                        width="100%">
                        <tr style="background-color: #ffffff">
                            <td width="10%">
                                Account Number</td>
                            <td style="width: 1%">
                                :</td>
                            <td style="width: 75%">
                                &nbsp;<asp:Label ID="LblAccountNumber" runat="server"></asp:Label>
                                <asp:Label ID="LblBranchID" runat="server" Visible="False"></asp:Label></td>
                        </tr>
                        <tr style="background-color: #ffffff">
                            <td>
                                Name</td>
                            <td style="width: 100px; height: 17px">
                                :</td>
                            <td style="width: 100px; height: 17px">
                                &nbsp;<asp:Label ID="LblName" runat="server" Width="320px"></asp:Label></td>
                        </tr>
                        <tr style="background-color: #ffffff">
                            <td>
                                Branch</td>
                            <td style="width: 100px; height: 17px">
                                :</td>
                            <td style="width: 100px; height: 17px">
                                &nbsp;<asp:Label ID="LblBranch" runat="server" Width="250px"></asp:Label></td>
                        </tr>
                    </table>                
            </td>
        <tr>
            <td>
                <table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
                    border="2">
                    <tr>
                        <td bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel4" runat="server">
                   <asp:DataGrid ID="GridCaseHist" runat="server" AllowCustomPaging="True" AutoGenerateColumns="False"
                        BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px"
                        CellPadding="4" ForeColor="Black" GridLines="Vertical" Width="100%" AllowSorting="True">
                        <FooterStyle BackColor="#CCCC99" />
                        <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages" />
                        <AlternatingItemStyle BackColor="White" />
                        <ItemStyle BackColor="#F7F7DE" />
                        <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                        <Columns>
                            <asp:TemplateColumn><ItemTemplate>
                            <asp:CheckBox id="CheckBoxExporttoExcel" runat="server"></asp:CheckBox> 
                            </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="PK_CaseManagementID" HeaderText="PK_CaseManagementID" Visible=False>
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Case Number" SortExpression="PK_CaseManagementID desc">
                                <ItemTemplate>
                                        <%#GenerateCaseNoLink(Eval("PK_CaseManagementID"))%>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="CreatedDate" HeaderText="Created Date" SortExpression="CaseManagement.CreatedDate desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CaseStatusDescription" HeaderText="Case Status" SortExpression="CaseStatusDescription desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="IsReportedtoRegulator" HeaderText="Reported To Regulator"
                                SortExpression="CaseManagement.IsReportedtoRegulator desc"></asp:BoundColumn>
                            
              </Columns>
                                    <EditItemStyle BackColor="#7C6F57" />
            </asp:datagrid>
                            <asp:Label ID="LabelNoRecordFound" runat="server" CssClass="text" Visible="False"></asp:Label></ajax:ajaxpanel>
                            </td>
                    </tr>
                    <tr>
                        <td style="background-color:#ffffff">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tr>
                        <td nowrap><ajax:ajaxpanel id="AjaxPanel5" runat="server">&nbsp;<asp:CheckBox ID="CheckBoxSelectAll" runat="server" Text="Select All" AutoPostBack="True" />
                          &nbsp; </ajax:ajaxpanel> </td>
                         <td width="99%">&nbsp;&nbsp;<asp:LinkButton ID="LinkButtonExportExcel" runat="server">Export 
                            to Excel</asp:LinkButton></td>
                        </tr>
                  </table></td>
                    </tr>
                    <tr>
                        <td bgColor="#ffffff">
                            <TABLE id="Table3" borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#ffffff"
                                border="2">
                                <TR class="regtext" align="center" bgColor="#dddddd">
                                    <TD vAlign="top" align="left" width="50%" bgColor="#ffffff"><asp:Label ID="Label9" runat="server" Text="Page"></asp:Label>&nbsp;<ajax:ajaxpanel id="AjaxPanel6" runat="server"><asp:label id="PageCurrentPage" runat="server" CssClass="regtext">0</asp:label>&nbsp;<asp:Label ID="Label10" runat="server" Text="of"></asp:Label>&nbsp;
                                        <asp:label id="PageTotalPages" runat="server" CssClass="regtext">0</asp:label></ajax:ajaxpanel></TD>
                                    <TD vAlign="top" align="right" width="50%" bgColor="#ffffff"><asp:Label ID="Label11" runat="server" Text="Total Records"></asp:Label>&nbsp;
                                        <ajax:ajaxpanel id="AjaxPanel7" runat="server"><asp:label id="PageTotalRows" runat="server">0</asp:label></ajax:ajaxpanel></TD>
                                </TR>
                            </TABLE>
                            <TABLE id="Table4" borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#ffffff"
                                border="2">
                                <TR bgColor="#ffffff">
                                    <TD class="regtext" vAlign="middle" align="left" colSpan="11" height="7">
                                        <HR color="#f40101" noShade SIZE="1">
                                    </TD>
                                </TR>
                                <TR>
                                    <TD class="regtext" vAlign="middle" align="left" width="63" bgColor="#ffffff"><asp:Label ID="Label12" runat="server" Text="Go to page"></asp:Label></TD>
                                    <TD class="regtext" vAlign="middle" align="left" width="5" bgColor="#ffffff"><FONT face="Verdana, Arial, Helvetica, sans-serif" size="1">
                                                <ajax:ajaxpanel id="AjaxPanel8" runat="server"><asp:textbox id="TextGoToPage" runat="server" Width="38px" CssClass="searcheditbox"></asp:textbox></ajax:ajaxpanel>
                                            </FONT></TD>
                                    <TD class="regtext" vAlign="middle" align="left" bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel9" runat="server">
                                            <asp:imagebutton id="ImageButtonGo" runat="server" SkinID="GoButton" ImageUrl="~/Images/button/go.gif"></asp:imagebutton></ajax:ajaxpanel>
                                        </TD>
                                    <TD class="regtext" vAlign="middle" align="left" width="6" bgColor="#ffffff"><IMG height="5" src="images/first.gif" width="6">
                                    </TD>
                                    <TD class="regtext" vAlign="middle" align="right" width="6" bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel10" runat="server">
                                            <asp:linkbutton id="LinkButtonFirst" runat="server" CssClass="regtext" CommandName="First" OnCommand="PageNavigate">First</asp:linkbutton></ajax:ajaxpanel>
                                        </TD>
                                    <TD class="regtext" vAlign="middle" align="right" width="6" bgColor="#ffffff"><IMG height="5" src="images/prev.gif" width="6"></TD>
                                    <TD class="regtext" vAlign="middle" align="right" width="14" bgColor="#ffffff">
                                    <ajax:ajaxpanel id="AjaxPanel11" runat="server">
                                            <asp:linkbutton id="LinkButtonPrevious" runat="server" CssClass="regtext" CommandName="Prev" OnCommand="PageNavigate">Previous</asp:linkbutton></ajax:ajaxpanel>
                                        </TD>
                                    <TD class="regtext" vAlign="middle" align="right" width="60" bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel12" runat="server"><A class="pageNav" href="#">
                                                <asp:linkbutton id="LinkButtonNext" runat="server" CssClass="regtext" CommandName="Next" OnCommand="PageNavigate">Next</asp:linkbutton></A></ajax:ajaxpanel></TD>
                                    <TD class="regtext" vAlign="middle" align="left" width="6" bgColor="#ffffff"><IMG height="5" src="images/next.gif" width="6"></TD>
                                    <TD class="regtext" vAlign="middle" align="left" width="25" bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel13" runat="server">
                                            <asp:linkbutton id="LinkButtonLast" runat="server" CssClass="regtext" CommandName="Last" OnCommand="PageNavigate">Last</asp:linkbutton></ajax:ajaxpanel>
                                        </TD>
                                    <TD class="regtext" vAlign="middle" align="left" width="6" bgColor="#ffffff"><IMG height="5" src="images/last.gif" width="6"></TD>
                                </TR>
                            </TABLE>
                        </td>
                    </tr>
                </table>
            
            </td>
        </tr>   
        <tr>
            <td>
                <table bgcolor="Gainsboro" width="100%">
                    &nbsp;<asp:ImageButton ID="ImageBack" runat="server" SkinID="BackButton" ImageUrl="~/Images/button/back.gif"/>
                </table>
            </td>
        </tr> 
    </table>    
</asp:Content>