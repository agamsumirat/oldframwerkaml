﻿Option Explicit On
Option Strict On
Imports AMLBLL
Imports SahassaNettier.Entities
Imports SahassaNettier.Data
Imports SahassaNettier.Services
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports Sahassa.AML.Commonly

Partial Class DataUpdatingAlertEdit
    Inherits Parent



    Public Property ObjMediumRiskCustomer() As DataUpdatingParameterAlert
        Get
            If Session("DataUpdatingAlertEdit.ObjMediumRiskCustomer") Is Nothing Then
                Session("DataUpdatingAlertEdit.ObjMediumRiskCustomer") = DataUpdatingAlertBLL.GetDataUpdatingParameterAlertByPk(3)
                Return CType(Session("DataUpdatingAlertEdit.ObjMediumRiskCustomer"), DataUpdatingParameterAlert)
            Else
                Return CType(Session("DataUpdatingAlertEdit.ObjMediumRiskCustomer"), DataUpdatingParameterAlert)
            End If
        End Get
        Set(ByVal value As DataUpdatingParameterAlert)
            Session("DataUpdatingAlertEdit.ObjMediumRiskCustomer") = value
        End Set
    End Property

    Public Property ObjHighRiskCustomer() As DataUpdatingParameterAlert
        Get
            If Session("DataUpdatingAlertEdit.ObjHighRiskCustomer") Is Nothing Then
                Session("DataUpdatingAlertEdit.ObjHighRiskCustomer") = DataUpdatingAlertBLL.GetDataUpdatingParameterAlertByPk(1)
                Return CType(Session("DataUpdatingAlertEdit.ObjHighRiskCustomer"), DataUpdatingParameterAlert)
            Else
                Return CType(Session("DataUpdatingAlertEdit.ObjHighRiskCustomer"), DataUpdatingParameterAlert)
            End If
        End Get
        Set(value As DataUpdatingParameterAlert)
            Session("DataUpdatingAlertEdit.ObjHighRiskCustomer") = value
        End Set
    End Property


    Public Property ObjNonHighRiskCustomer() As DataUpdatingParameterAlert
        Get
            If Session("DataUpdatingAlertEdit.ObjNonHighRiskCustomer") Is Nothing Then
                Session("DataUpdatingAlertEdit.ObjNonHighRiskCustomer") = DataUpdatingAlertBLL.GetDataUpdatingParameterAlertByPk(2)
                Return CType(Session("DataUpdatingAlertEdit.ObjNonHighRiskCustomer"), DataUpdatingParameterAlert)
            Else
                Return CType(Session("DataUpdatingAlertEdit.ObjNonHighRiskCustomer"), DataUpdatingParameterAlert)
            End If
        End Get
        Set(value As DataUpdatingParameterAlert)
            Session("DataUpdatingAlertEdit.ObjNonHighRiskCustomer") = value
        End Set
    End Property


    Sub ClearSession()
        ObjHighRiskCustomer = Nothing
        ObjNonHighRiskCustomer = Nothing
        ObjMediumRiskCustomer = Nothing
    End Sub

    Sub LoadData()
        If Not ObjHighRiskCustomer Is Nothing Then
            ChkHighRiskCustomer.Checked = ObjHighRiskCustomer.Activation.GetValueOrDefault(False)
            TxtHighRiskCustomer.Text = ObjHighRiskCustomer.YearOnOpeningCIF.GetValueOrDefault(0).ToString
        End If
        If Not ObjMediumRiskCustomer Is Nothing Then
            ChkMediumRiskCustomer.Checked = ObjMediumRiskCustomer.Activation.GetValueOrDefault(False)
            TxtMediumRiskCustomer.Text = ObjMediumRiskCustomer.YearOnOpeningCIF.GetValueOrDefault(0).ToString
        End If
        If Not ObjNonHighRiskCustomer Is Nothing Then
            ChkNonHighRiskCustomer.Checked = ObjNonHighRiskCustomer.Activation.GetValueOrDefault(False)
            TxtNonHighRiskCustomer.Text = ObjNonHighRiskCustomer.YearOnOpeningCIF.GetValueOrDefault(0).ToString
        End If
    End Sub

    Protected Sub Page_Load1(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then

                ClearSession()
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using
                LoadData()
            End If
            

        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBackAdd_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImgBackAdd.Click
        Try

            Response.Redirect("Default.aspx", False)

        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub
    Function IsDataValid() As Boolean
        Dim strErrorMessage As New StringBuilder

        If Not Integer.TryParse(TxtHighRiskCustomer.Text, 0) Then
            strErrorMessage.Append("Year On Opening CIF for High Risk Customer Must Numeric.<br/>")
        End If
        If Not Integer.TryParse(TxtMediumRiskCustomer.Text, 0) Then
            strErrorMessage.Append("Year On Opening CIF for medium Risk Customer Must Numeric.<br/>")
        End If
        If Not Integer.TryParse(TxtNonHighRiskCustomer.Text, 0) Then
            strErrorMessage.Append("Year On Opening CIF for Non High Risk Customer Must Numeric.<br/>")
        End If

       
        If DataUpdatingAlertBLL.CountPendingApproval > 0 Then
            strErrorMessage.Append("Data Updating Alert Already Exist In Pending Approval.Please Approve Or Reject First Before Edit Again.<br/>")
        End If

        If strErrorMessage.ToString.Trim.Length > 0 Then
            Throw New Exception(strErrorMessage.ToString)
        Else
            Return True
        End If

    End Function

    Protected Sub ImgBtnSave_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnSave.Click
        Try

            If IsDataValid Then
                ObjHighRiskCustomer.Activation = ChkHighRiskCustomer.Checked
                ObjHighRiskCustomer.YearOnOpeningCIF = CInt(TxtHighRiskCustomer.Text)

                ObjMediumRiskCustomer.Activation = ChkMediumRiskCustomer.Checked
                ObjMediumRiskCustomer.YearOnOpeningCIF = CInt(TxtMediumRiskCustomer.Text)

                ObjNonHighRiskCustomer.Activation = ChkNonHighRiskCustomer.Checked
                ObjNonHighRiskCustomer.YearOnOpeningCIF = CInt(TxtNonHighRiskCustomer.Text)

                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                    DataUpdatingAlertBLL.SaveDirect(ObjHighRiskCustomer, ObjMediumRiskCustomer, ObjNonHighRiskCustomer)
                    LblConfirmation.Text = "Data Updating Alert Saved"
                    ImgBtnSave.Visible = False
                    ImgBackAdd.Visible = False
                Else

                    DataUpdatingAlertBLL.SaveApproval(ObjHighRiskCustomer, ObjMediumRiskCustomer, ObjNonHighRiskCustomer)
                    LblConfirmation.Text = "Data Updating Alert Saved Into Pending Approval"
                    ImgBtnSave.Visible = False
                    ImgBackAdd.Visible = False
                End If
                MtvTransactionAmount.ActiveViewIndex = 1

            End If

        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImgBack_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImgBack.Click
        Try

            Response.Redirect("Default.aspx", False)

        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub
End Class
