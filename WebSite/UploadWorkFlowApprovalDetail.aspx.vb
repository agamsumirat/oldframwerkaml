Imports SahassaNettier.Entities
Partial Class UploadWorkFlowApprovalDetail
    Inherits Parent


    Public ReadOnly Property PK_CaseManagementWorkFlowUpload_ID() As Integer
        Get
            Return Request.Params("PK_CaseManagementWorkFlowUpload_ID")
        End Get


    End Property


    Public ReadOnly Property objCaseManagementWorkFlowUploadDetail() As EkaDataNettier.Entities.TList(Of EkaDataNettier.Entities.CaseManagementWorkFlowUploadDetail)
        Get
            If Session("UploadWorkFlowApprovalDetail.objCaseManagementWorkFlowUploadDetail") Is Nothing Then
                Session("UploadWorkFlowApprovalDetail.objCaseManagementWorkFlowUploadDetail") = AMLBLL.WorkFlowTemplateBll.GetTCaseManagementWorkFlowUploadDetail(CaseManagementWorkFlowUploadDetailColumn.FK_CaseManagementWorkFlowUpload_ID.ToString & "=" & Me.PK_CaseManagementWorkFlowUpload_ID, "", 0, Integer.MaxValue, 0)
            End If
            Return Session("UploadWorkFlowApprovalDetail.objCaseManagementWorkFlowUploadDetail")
        End Get

    End Property


    Sub ClearSession()
        Session("UploadWorkFlowApprovalDetail.objCaseManagementWorkFlowUploadDetail") = Nothing
    End Sub


    Sub LoadDataWorkFlow()
        GridView1.DataSource = Me.objCaseManagementWorkFlowUploadDetail
        GridView1.DataBind()
    End Sub
  
    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try

            If Not IsPostBack Then
                ClearSession()
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                    LabelTitle.Text = "Upload WorkFlow Approval Detail"
                    LoadDataWorkFlow()

                End Using
            End If

        Catch ex As Exception
            LogError(ex)
            CValPageError.IsValid = False
            CValPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound

        Try

            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim objLableno As Label = CType(e.Row.FindControl("labelno"), Label)
                objLableno.Text = e.Row.RowIndex + 1
                Dim objLabelApprover As Label = CType(e.Row.FindControl("labelapprover"), Label)
                Dim arrdataApprover() As String = objLabelApprover.Text.Split("|")


                Dim strUserid As String = ""
                For index As Integer = 0 To arrdataApprover.Length - 1
                    Dim strpkuserid As String = arrdataApprover(index)
                    Using objtempuser As User = AMLBLL.UserBLL.GetUserByPkUserID(strpkuserid)
                        If Not objtempuser Is Nothing Then
                            strUserid += objtempuser.UserID & ";"
                        End If
                    End Using
                Next
                If strUserid.Length > 0 Then
                    strUserid = strUserid.Substring(0, strUserid.Length - 1)
                End If

                objLabelApprover.Text = strUserid



                Dim objLabelnotifyother As Label = CType(e.Row.FindControl("labelnotifyothers"), Label)
                Dim struseridnotify As String = ""
                Dim arrNotify() As String = objLabelnotifyother.Text.Split("|")

                For index As Integer = 0 To arrNotify.Length - 1
                    Dim strpknotify As String = arrNotify(index)
                    Using objtempuser As User = AMLBLL.UserBLL.GetUserByPkUserID(strpknotify)
                        If Not objtempuser Is Nothing Then
                            struseridnotify += objtempuser.UserID & ";"
                        End If
                    End Using

                Next
                If struseridnotify.Length > 0 Then
                    struseridnotify = struseridnotify.Substring(0, struseridnotify.Length - 1)
                End If

                objLabelnotifyother.Text = struseridnotify

            End If
        Catch ex As Exception
            LogError(ex)
            CValPageError.IsValid = False
            CValPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub imgCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCancel.Click

        Try

            Response.Redirect("UploadWorkFlowApproval.aspx", False)
        Catch ex As Exception
            LogError(ex)
            CValPageError.IsValid = False
            CValPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub imgAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgAccept.Click

        Try

            AMLBLL.WorkFlowTemplateBll.AcceptWorkFlow(Me.PK_CaseManagementWorkFlowUpload_ID)
            Response.Redirect("UploadWorkFlowApproval.aspx", False)
        Catch ex As Exception
            LogError(ex)
            CValPageError.IsValid = False
            CValPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub imgreject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgreject.Click

        Try
            AMLBLL.WorkFlowTemplateBll.RejectWorkFlow(Me.PK_CaseManagementWorkFlowUpload_ID)
            Response.Redirect("UploadWorkFlowApproval.aspx", False)
        Catch ex As Exception
            LogError(ex)
            CValPageError.IsValid = False
            CValPageError.ErrorMessage = ex.Message
        End Try

    End Sub
End Class
