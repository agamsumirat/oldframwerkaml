Partial Class VerificationListView
    Inherits Parent

#Region " Property "
    ''' <summary>
    ''' selected item store
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("MsVerificationListViewSelected") Is Nothing, New ArrayList, Session("MsVerificationListViewSelected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("MsVerificationListViewSelected") = value
        End Set
    End Property
    ''' <summary>
    ''' setnget search field
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetFieldSearch() As String
        Get
            Return IIf(Session("MsVerificationListViewFieldSearch") Is Nothing, "", Session("MsVerificationListViewFieldSearch"))
        End Get
        Set(ByVal Value As String)
            Session("MsVerificationListViewFieldSearch") = Value
        End Set
    End Property
    ''' <summary>
    ''' search value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetValueSearch() As String
        Get
            Return IIf(Session("MsVerificationListViewValueSearch") Is Nothing, "", Session("MsVerificationListViewValueSearch"))
        End Get
        Set(ByVal Value As String)
            Session("MsVerificationListViewValueSearch") = Value
        End Set
    End Property
    ''' <summary>
    ''' sort expresion
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("MsVerificationListViewSort") Is Nothing, "VerificationListId  asc", Session("MsVerificationListViewSort"))
        End Get
        Set(ByVal Value As String)
            Session("MsVerificationListViewSort") = Value
        End Set
    End Property
    ''' <summary>
    ''' current page index
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("MsVerificationListViewCurrentPage") Is Nothing, 0, Session("MsVerificationListViewCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("MsVerificationListViewCurrentPage") = Value
        End Set
    End Property
    ''' <summary>
    ''' total pages
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    ''' <summary>
    ''' row total
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("MsVerificationListViewRowTotal") Is Nothing, 0, Session("MsVerificationListViewRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("MsVerificationListViewRowTotal") = Value
        End Set
    End Property
    ''' <summary>
    ''' save bind table
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetBindTable() As Data.DataTable
        Get
            Return IIf(Session("MsVerificationListViewData") Is Nothing, New AMLDAL.AMLDataSet.VerificationList_MasterDataTable, Session("MsVerificationListViewData"))
        End Get
        Set(ByVal value As Data.DataTable)
            Session("MsVerificationListViewData") = value
        End Set
    End Property

    ''' <summary>
    ''' Nama Table
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property Tables() As String
        Get
            Return "VerificationList_Master inner join  VerificationListCategory on VerificationList_Master.VerificationListCategoryId = VerificationListCategory.CategoryID inner join VerificationListType on VerificationList_Master.VerificationListTypeId = VerificationListType.pk_Verification_List_Type_Id"
        End Get
    End Property

    ''' <summary>
    ''' PK
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property PK() As String
        Get
            Return "VerificationList_Master.VerificationListId"
        End Get
    End Property

    ''' <summary>
    ''' Fields
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property Fields() As String
        Get
            Return "VerificationList_Master.DateEntered, VerificationList_Master.DateUpdated, VerificationList_Master.DateOfData, VerificationList_Master.VerificationListId, VerificationList_Master.DisplayName, VerificationList_Master.DateOfBirth, VerificationListType.ListTypeName, VerificationListCategory.CategoryName"
        End Get
    End Property

    ''' <summary>
    ''' Page Size
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property PageSize() As Int16
        Get
            Return IIf(Session("PageSize") IsNot Nothing, Session("PageSize"), 10)
        End Get
        Set(ByVal value As Int16)
            Session("PageSize") = value
        End Set
    End Property

#End Region

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' fill search
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub FillSearch()
        Try
            Me.ComboSearch.Items.Add(New ListItem("...", ""))
            Me.ComboSearch.Items.Add(New ListItem("Entry Date", "DateEntered BETWEEN '-=Search=- 00:00' AND '-=Search=- 23:59'"))
            Me.ComboSearch.Items.Add(New ListItem("Updated Date", "DateUpdated BETWEEN '-=Search=- 00:00' AND '-=Search=- 23:59'"))
            Me.ComboSearch.Items.Add(New ListItem("Date Of Data", "DateOfData BETWEEN '-=Search=- 00:00' AND '-=Search=- 23:59'"))
            Me.ComboSearch.Items.Add(New ListItem("Name", "DisplayName Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Date of Birth", "DateOfBirth BETWEEN '-=Search=- 00:00' AND '-=Search=- 23:59'"))
            Me.ComboSearch.Items.Add(New ListItem("List Type", "ListTypeName Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Category Name", "CategoryName Like '%-=Search=-%'"))
        Catch
            Throw
        End Try
    End Sub

    Private Sub ClearThisPageSessions()
        Session("MsVerificationListViewSelected") = Nothing
        Session("MsVerificationListViewFieldSearch") = Nothing
        Session("MsVerificationListViewValueSearch") = Nothing
        Session("MsVerificationListViewSort") = Nothing
        Session("MsVerificationListViewCurrentPage") = Nothing
        Session("MsVerificationListViewRowTotal") = Nothing
        Session("MsVerificationListViewData") = Nothing
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not Me.IsPostBack Then
                Me.ClearThisPageSessions()

                Me.ComboSearch.Attributes.Add("onchange", "javascript:Check4Calendar(this, '" & Me.TextSearch.ClientID & "', '" & Me.popUp.ClientID & "', 1,2,3,5);")
                Me.popUp.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TextSearch.ClientID & "'), 'dd-mmm-yyyy')")
                Me.popUp.Style.Add("display", "none")

                If Sahassa.AML.Commonly.SessionPagingLimit <> "" Then
                    Me.PageSize = Sahassa.AML.Commonly.SessionPagingLimit
                Else
                    Me.PageSize = 10
                End If
                Me.FillSearch()
                Me.GridMSUserView.PageSize = Me.PageSize

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    '        Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' page navigate button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
            Me.CollectSelected()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' bind grid
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Sub BindGrid()
        Me.SetnGetBindTable = Sahassa.AML.Commonly.GetDatasetPaging(Me.Tables, Me.PK, Me.SetnGetSort, Me.SetnGetCurrentPage + 1, Me.PageSize, Me.Fields, Me.SetnGetFieldSearch.Replace("-=Search=-", Me.SetnGetValueSearch.Replace("'", "''")), "")
        Me.GridMSUserView.DataSource = Me.SetnGetBindTable
        Me.SetnGetRowTotal = Sahassa.AML.Commonly.GetTableCount(Me.Tables, Me.SetnGetFieldSearch.Replace("-=Search=-", Me.SetnGetValueSearch.Replace("'", "''")))
        'Using AccessVerificationList As New AMLDAL.AMLDataSetTableAdapters.SelectVerificationListMasterDataViewTableAdapter
        '    Using dt As Data.DataTable = AccessVerificationList.GetData
        '        Dim TotalRow() As Data.DataRow = dt.Select(Me.SetnGetFieldSearch.Replace("-=Search=-", Me.SetnGetValueSearch.Replace("'", "''")))
        '        Me.SetnGetRowTotal = TotalRow.Length
        '    End Using
        'End Using
        Me.GridMSUserView.VirtualItemCount = Me.SetnGetRowTotal
        Me.GridMSUserView.DataBind()
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' set navigate info
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.ComboSearch.SelectedValue = Me.SetnGetFieldSearch
            Me.TextSearch.Text = Me.SetnGetValueSearch
        Catch
            Throw
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' change sort expression
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridMSUserView.SortCommand
        Dim GridUser As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' image button search
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Try
            Me.CollectSelected()
            Me.SetnGetFieldSearch = Me.ComboSearch.SelectedValue
            If Me.ComboSearch.SelectedIndex = 0 Then
                Me.TextSearch.Text = ""
                Me.SetnGetValueSearch = Me.TextSearch.Text
            ElseIf Me.ComboSearch.SelectedIndex = 1 Or Me.ComboSearch.SelectedIndex = 2 Or Me.ComboSearch.SelectedIndex = 3 Or Me.ComboSearch.SelectedIndex = 5 Then
                Dim DateSearch As DateTime
                Try
                    DateSearch = DateTime.Parse(Me.TextSearch.Text, New System.Globalization.CultureInfo("id-ID"))
                Catch ex As ArgumentOutOfRangeException
                    Throw New Exception("Input character cannot be convert to datetime.")
                Catch ex As FormatException
                    Throw New Exception("Unknown format date")
                End Try
                Me.SetnGetValueSearch = DateSearch.ToString("dd-MMM-yyyy")
            Else
                Me.SetnGetValueSearch = Me.TextSearch.Text
            End If
            Me.SetnGetCurrentPage = 0
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' cek if VerificationListID already edited
    ''' </summary>
    ''' <param name="strVerificationListID"></param>
    ''' <returns></returns>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Function CekIsEdited(ByVal strVerificationListID As String) As Boolean
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Master_ApprovalTableAdapter
            Dim count As Int32 = AccessPending.CountVerificationList_MasterApproval(strVerificationListID)
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        End Using
    End Function

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' grid edit command handler
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridMSUserView.EditCommand
        Dim strVerificationListID As String = e.Item.Cells(1).Text
        Dim Name As String = e.Item.Cells(5).Text
        Try
            If Me.CekIsEdited(strVerificationListID) = False Then
                Sahassa.AML.Commonly.SessionIntendedPage = "VerificationListEdit.aspx?VerificationListId=" & strVerificationListID

                Me.Response.Redirect("VerificationListEdit.aspx?VerificationListId=" & strVerificationListID, False)
            Else
                Throw New Exception("Cannot edit the Verification List for the following person : '" & Name & "' because it is currently waiting for approval")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' is deleted
    ''' </summary>
    ''' <param name="strVerificationListID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function IsDeleted(ByVal strVerificationListID As String) As Boolean
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Master_ApprovalTableAdapter
            Dim count As Int32 = AccessPending.CountVerificationList_MasterApproval(strVerificationListID)
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        End Using
    End Function


    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' grid delete event handler
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridMSUserView.DeleteCommand
        Dim strVerificationListID As String = e.Item.Cells(1).Text
        Dim Name As String = e.Item.Cells(5).Text

        Try
            If Me.IsDeleted(strVerificationListID) = False Then
                Sahassa.AML.Commonly.SessionIntendedPage = "VerificationListDelete.aspx?VerificationListId=" & strVerificationListID

                Me.Response.Redirect("VerificationListDelete.aspx?VerificationListId=" & strVerificationListID, False)
            Else
                Throw New Exception("Cannot delete the Verification List for the following person : '" & Name & "' because it is currently waiting for approval")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' go button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	14/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            Me.CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be a positive number greater than zero.")
                End If
            Else
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get item bound
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridMSUserView.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Dim chkBox As CheckBox = e.Item.FindControl("CheckBoxExporttoExcel")
                chkBox.Checked = Me.SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    ''' <summary>
    ''' collect sub
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = gridRow.FindControl("CheckBoxExporttoExcel")
                Dim VerificationListID As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
                If ArrTarget.Contains(VerificationListID) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(VerificationListID)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(VerificationListID)
                End If
                Me.SetnGetSelectedItem = ArrTarget
            End If
        Next
    End Sub
    ''' <summary>
    ''' Get Category Name
    ''' </summary>
    ''' <param name="pk_category_id"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetCategoryName(ByVal pk_category_id As Integer) As String
        Try
            Using AccessCategory As New AMLDAL.AMLDataSetTableAdapters.VerificationListCategoryTableAdapter
                Using dt As Data.DataTable = AccessCategory.GetDataByCategoryID(pk_category_id)
                    Dim row As AMLDAL.AMLDataSet.VerificationListCategoryRow = dt.Rows(0)
                    Dim CategoryName As String = row.CategoryName
                    Return CategoryName
                End Using
            End Using
        Catch
            Throw
        End Try
    End Function
    ''' <summary>
    ''' Get Type Name
    ''' </summary>
    ''' <param name="pk_type_id"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetTypeName(ByVal pk_type_id As Integer) As String
        Try
            Using AccessGetType As New AMLDAL.AMLDataSetTableAdapters.VerificationListTypeTableAdapter
                Dim TypeName As String = AccessGetType.GetListTypeNameByListTypeID(pk_type_id)
                Return TypeName
            End Using
        Catch
            Throw
        End Try
    End Function
    ''' <summary>
    ''' Get Type Address
    ''' </summary>
    ''' <param name="pk_type_id"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetTypeAddress(ByVal pk_type_id As Integer) As String
        Try
            Using AccessTypeAddress As New AMLDAL.AMLDataSetTableAdapters.AddressTypeTableAdapter
                Dim AddressTypeName As String = AccessTypeAddress.GetAddressTypeDescriptionByAddressTypeID(pk_type_id)
                Return AddressTypeName
            End Using
        Catch
            Throw
        End Try
    End Function

    Private Sub BindSelected()

        Dim objdt As Data.DataTable = Sahassa.AML.Commonly.GetDatasetPaging(Me.Tables, Me.PK, Me.SetnGetSort, Me.SetnGetCurrentPage + 1, Me.PageSize, Me.Fields, Me.SetnGetFieldSearch.Replace("-=Search=-", Me.SetnGetValueSearch.Replace("'", "''")), "")
        Dim objdtfilter As New Data.DataTable

        objdtfilter = objdt.Clone

        For Each IdPk As String In Me.SetnGetSelectedItem
            Dim Rows() As Data.DataRow = objdt.Select("VerificationListId= '" & IdPk & "'")
            If Rows.Length > 0 Then

                objdtfilter.ImportRow(Rows(0))
            End If

        Next

        Me.GridMSUserView.DataSource = objdtfilter
        Me.GridMSUserView.AllowPaging = False
        Me.GridMSUserView.DataBind()

        'Sembunyikan kolom ke 0, 8, 9 agar tidak ikut diekspor ke excel
        Me.GridMSUserView.Columns(0).Visible = False
        Me.GridMSUserView.Columns(8).Visible = False
        Me.GridMSUserView.Columns(9).Visible = False

    End Sub

    Private Sub BindAll()

        'Untuk mengambil semua data, dianggap paling banyak adalah 9.999.999 juta data
        Dim totalData As Integer = 9999999
        Dim objdt As Data.DataTable = Sahassa.AML.Commonly.GetDatasetPaging(Me.Tables, Me.PK, Me.SetnGetSort, Me.SetnGetCurrentPage + 1, totalData, Me.Fields, Me.SetnGetFieldSearch.Replace("-=Search=-", Me.SetnGetValueSearch.Replace("'", "''")), "")

        Me.GridMSUserView.DataSource = objdt
        Me.GridMSUserView.AllowPaging = False
        Me.GridMSUserView.DataBind()

        'Sembunyikan kolom ke 0, 8, 9 agar tidak ikut diekspor ke excel
        Me.GridMSUserView.Columns(0).Visible = False
        Me.GridMSUserView.Columns(8).Visible = False
        Me.GridMSUserView.Columns(9).Visible = False

    End Sub

    ''' <summary>
    ''' clear all control except control
    ''' </summary>
    ''' <param name="control">excluded control</param>
    ''' <remarks></remarks>
    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub 'ClearControls

    Public Sub ExportToExcel()
        Try
            Me.CollectSelected()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelected()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=VerificationListView.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridMSUserView)
            GridMSUserView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub


    ''' <summary>
    ''' Export To Excel
    ''' </summary>
    ''' <remarks></remarks>
    'Public Sub ExportToExcel()
    '    Dim range As String = ""
    '    Dim xlApp As Excel.ApplicationClass
    '    Dim xlWorkbooks As Excel.Workbooks
    '    Dim xlWorkbook As Excel.Workbook
    '    Dim xlWorkSheets As Excel.Sheets
    '    Dim xlWorkSheet As Excel.Worksheet
    '    Dim xlRange As Excel.Range
    '    Dim xlCells As Excel.Range
    '    Dim xlFont As Excel.Font
    '    'Dim xlInterior As Excel.Interior
    '    'Dim xlcomment As Excel.Comment

    '    Try
    '        xlApp = New Excel.Application
    '        xlApp.DisplayAlerts = False
    '        xlApp.UserControl = False
    '        xlApp.Visible = False
    '        xlWorkbooks = xlApp.Workbooks
    '        xlWorkbook = xlWorkbooks.Add
    '        xlWorkSheets = xlWorkbook.Worksheets

    '        Dim ArrCollectVerificationList As ArrayList = Me.SetnGetSelectedItem


    '        ' ID Number
    '        Try
    '            xlWorkSheet = xlWorkSheets.Add
    '            xlWorkSheet.Name = "Verification List - IDNumber"
    '            xlCells = xlWorkSheet.Cells

    '            range = "A1:B1"
    '            xlRange = xlWorkSheet.Range(range)
    '            xlRange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter
    '            xlRange.Interior.Color = RGB(192, 0, 0)
    '            xlRange.Merge()
    '            xlRange.Value = "Verification List IDNumber"
    '            NAR(xlRange)

    '            ' Create Header Alias
    '            Dim HeaderMasterAlias() As String = {"VerificationListId", "IDNumber"}
    '            For i As Integer = 0 To 1
    '                xlCells = xlWorkSheet.Cells
    '                xlRange = xlCells(2, i + 1)
    '                xlRange.Value = HeaderMasterAlias(i)
    '                xlFont = xlRange.Font
    '                xlFont.Bold = True
    '                NAR(xlFont)
    '                NAR(xlRange)
    '            Next

    '            Dim IntCounter As Integer = 3
    '            Using AccessIDNumberVerification As New AMLDAL.VerificationListViewExportToExcelTableAdapters.VerificationList_IDNumberTableAdapter
    '                For i As Int64 = 0 To ArrCollectVerificationList.Count - 1
    '                    Using dtIDNumber As Data.DataTable = AccessIDNumberVerification.GetDataByVerificationListId(CInt(ArrCollectVerificationList(i)))
    '                        For Each RowIDNumber As AMLDAL.VerificationListViewExportToExcel.VerificationList_IDNumberRow In dtIDNumber.Rows
    '                            Dim StrValueIdNumber() As String = {ArrCollectVerificationList(i), RowIDNumber.IDNumber}
    '                            ' isi IdNumber
    '                            For j As Integer = 0 To 1
    '                                xlCells = xlWorkSheet.Cells
    '                                xlRange = xlCells(IntCounter, j + 1)
    '                                xlRange.Value = StrValueIdNumber(j)
    '                                NAR(xlRange)
    '                            Next
    '                            IntCounter += 1
    '                        Next
    '                    End Using
    '                Next
    '            End Using
    '            xlWorkSheet.Columns.AutoFit()
    '        Catch
    '            Throw
    '        Finally
    '            NAR(xlWorkSheet)
    '        End Try

    '        ' isi Alias
    '        Try
    '            xlWorkSheet = xlWorkSheets.Add
    '            xlWorkSheet.Name = "Verification List - Alias"
    '            xlCells = xlWorkSheet.Cells

    '            range = "A1:B1"
    '            xlRange = xlWorkSheet.Range(range)
    '            xlRange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter
    '            xlRange.Interior.Color = RGB(192, 0, 0)
    '            xlRange.Merge()
    '            xlRange.Value = "Verification List Alias"
    '            NAR(xlRange)

    '            ' Create Header Alias
    '            Dim HeaderMasterAlias() As String = {"VerificationListId", "Alias"}
    '            For i As Integer = 0 To 1
    '                xlCells = xlWorkSheet.Cells
    '                xlRange = xlCells(2, i + 1)
    '                xlRange.Value = HeaderMasterAlias(i)
    '                xlFont = xlRange.Font
    '                xlFont.Bold = True
    '                NAR(xlFont)
    '                NAR(xlRange)
    '            Next
    '            Dim IntCounter As Integer = 3
    '            Using AccessAliasVerification As New AMLDAL.VerificationListViewExportToExcelTableAdapters.VerificationList_AliasTableAdapter
    '                For i As Int64 = 0 To ArrCollectVerificationList.Count - 1
    '                    Using dtAlias As Data.DataTable = AccessAliasVerification.GetDataByID(CInt(ArrCollectVerificationList(i)))
    '                        For Each RowAlias As AMLDAL.VerificationListViewExportToExcel.VerificationList_AliasRow In dtAlias.Rows
    '                            Dim StrValueAlias() As String = {ArrCollectVerificationList(i), RowAlias.Name}
    '                            ' isi address
    '                            For j As Integer = 0 To 1
    '                                xlCells = xlWorkSheet.Cells
    '                                xlRange = xlCells(IntCounter, j + 1)
    '                                xlRange.Value = StrValueAlias(j)
    '                                NAR(xlRange)
    '                            Next
    '                            IntCounter += 1
    '                        Next
    '                    End Using
    '                Next
    '            End Using
    '            xlWorkSheet.Columns.AutoFit()
    '        Catch
    '            Throw
    '        Finally
    '            NAR(xlWorkSheet)
    '        End Try

    '        ' Address
    '        Try
    '            xlWorkSheet = xlWorkSheets.Add
    '            xlWorkSheet.Name = "Verification List - Address"
    '            xlCells = xlWorkSheet.Cells

    '            range = "A1:E1"
    '            xlRange = xlWorkSheet.Range(range)
    '            xlRange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter
    '            xlRange.Interior.Color = RGB(192, 0, 0)
    '            xlRange.Merge()
    '            xlRange.Value = "Verification List Address"
    '            NAR(xlRange)

    '            ' Create Header Address
    '            Dim HeaderMasterAddress() As String = {"VerificationListId", "Address", "Postal Code", "Address Type Name", "IsLocalAddress"}
    '            For i As Integer = 0 To 4
    '                xlCells = xlWorkSheet.Cells
    '                xlRange = xlCells(2, i + 1)
    '                xlRange.Value = HeaderMasterAddress(i)
    '                xlFont = xlRange.Font
    '                xlFont.Bold = True
    '                NAR(xlFont)
    '                NAR(xlCells)
    '            Next
    '            Dim IntCounter As Integer = 3
    '            Using AccessAddressVerification As New AMLDAL.VerificationListViewExportToExcelTableAdapters.VerificationList_AddressTableAdapter
    '                For i As Int64 = 0 To ArrCollectVerificationList.Count - 1
    '                    Using dtAddress As Data.DataTable = AccessAddressVerification.GetData
    '                        For Each RowAddress As AMLDAL.VerificationListViewExportToExcel.VerificationList_AddressRow In dtAddress.Select("VerificationListId=" & ArrCollectVerificationList(i))
    '                            Dim StrValueAddress() As String = {ArrCollectVerificationList(i), RowAddress.Address, RowAddress.Item("PostalCode").ToString, Me.GetTypeAddress(CInt(RowAddress.AddressTypeId)), IIf(RowAddress.IsLocalAddress = True, "Yes", "No")}
    '                            ' isi address
    '                            For j As Integer = 0 To 4
    '                                xlCells = xlWorkSheet.Cells
    '                                xlRange = xlCells(IntCounter, j + 1)
    '                                xlRange.Value = StrValueAddress(j)
    '                                NAR(xlRange)
    '                            Next
    '                            IntCounter += 1
    '                        Next
    '                    End Using
    '                Next
    '            End Using
    '            xlWorkSheet.Columns.AutoFit()
    '        Catch
    '            Throw
    '        Finally
    '            NAR(xlWorkSheet)
    '        End Try


    '        Try
    '            xlWorkSheet = xlWorkSheets.Add
    '            xlWorkSheet.Name = "Verification List - Master"

    '            xlCells = xlWorkSheet.Cells

    '            range = "A1:N1"
    '            xlRange = xlWorkSheet.Range(range)
    '            xlRange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter
    '            xlRange.Interior.Color = RGB(192, 0, 0)
    '            xlRange.Merge()
    '            xlRange.Value = "Verification List Master"
    '            NAR(xlRange)

    '            ' Create Header Verification Master 
    '            Dim HeaderMaster() As String = {"VerificationListId", "Date Entered", "Date Updated", "DateOfData", "Display Name", "Date Of Birth", "Type Name", "Category Name", "Custom Remark 1", "Custom Remark 2", "Custom Remark 3", "Custom Remark 3", "Custom Remark 4", "Custom Remark 5", "Ref Id"}
    '            For i As Integer = 0 To 13
    '                xlCells = xlWorkSheet.Cells
    '                xlRange = xlCells(2, i + 1)
    '                xlRange.Value = HeaderMaster(i)
    '                xlFont = xlRange.Font
    '                xlFont.Bold = True
    '                NAR(xlFont)
    '                NAR(xlRange)
    '            Next

    '            Using AccessVerificationListMaster As New AMLDAL.VerificationListViewExportToExcelTableAdapters.VerificationList_MasterTableAdapter
    '                For i As Int64 = 0 To ArrCollectVerificationList.Count - 1
    '                    Using dtVerificationMaster As Data.DataTable = AccessVerificationListMaster.GetDataByID(CInt(ArrCollectVerificationList(i)))
    '                        Dim RowMaster As AMLDAL.VerificationListViewExportToExcel.VerificationList_MasterRow = dtVerificationMaster.Rows(0)
    '                        Dim DateEntered, DateUpdated, DateOfData, DateOfBirth As String

    '                        DateEntered = RowMaster.DateEntered
    '                        DateUpdated = RowMaster.DateUpdated
    '                        DateOfData = RowMaster.DateUpdated
    '                        DateOfBirth = RowMaster.DateOfBirth

    '                        Dim StrValueVerificationMaster() As String = {RowMaster.VerificationListId, DateEntered, DateUpdated, DateOfData, RowMaster.DisplayName, DateOfBirth, Me.GetTypeName(CInt(RowMaster.VerificationListTypeId)), Me.GetCategoryName(CInt(RowMaster.VerificationListCategoryId)), RowMaster.CustomRemark1, RowMaster.CustomRemark2, RowMaster.CustomRemark3, RowMaster.CustomRemark4, RowMaster.CustomRemark5, ""}
    '                        For j As Integer = 0 To 13
    '                            xlCells = xlWorkSheet.Cells
    '                            xlRange = xlCells(3 + i, j + 1)
    '                            xlRange.Value = StrValueVerificationMaster(j)
    '                            NAR(xlRange)
    '                        Next
    '                    End Using
    '                Next
    '            End Using
    '            xlWorkSheet.Columns.AutoFit()
    '        Catch
    '            Throw
    '        Finally
    '            NAR(xlWorkSheet)
    '        End Try



    '        xlWorkbook.SaveAs(Server.MapPath("temp") & "\" & Sahassa.AML.Commonly.SessionUserId & "VerificationListView.xls")
    '        NAR(xlRange)
    '        NAR(xlWorkSheet)
    '        NAR(xlWorkbook)
    '        NAR(xlWorkbooks)
    '        If Not (xlApp Is Nothing) Then
    '            xlApp.Quit()
    '        End If
    '        NAR(xlApp)
    '        'GC cleanup 
    '        System.GC.Collect()
    '        System.GC.WaitForPendingFinalizers()
    '        Response.Redirect("temp/" & Sahassa.AML.Commonly.SessionUserId & "VerificationListView.xls", False)
    '    Catch ex As Exception
    '        Me.cvalPageError.IsValid = False
    '        Me.cvalPageError.ErrorMessage = ex.Message
    '        LogError(ex)
    '    Finally
    '        NAR(xlRange)
    '        NAR(xlWorkSheet)
    '        NAR(xlWorkbook)
    '        NAR(xlWorkbooks)
    '        If Not (xlApp Is Nothing) Then
    '            xlApp.Quit()
    '        End If
    '        NAR(xlApp)
    '        'GC cleanup 
    '        System.GC.Collect()
    '        System.GC.WaitForPendingFinalizers()
    '    End Try
    'End Sub

    ''' <summary>
    ''' NAR
    ''' </summary>
    ''' <param name="obj"></param>
    ''' <remarks></remarks>
    Private Sub NAR(ByRef obj As Object)
        Dim intReferenceCount As Integer
        If Not obj Is Nothing Then
            Try
                Do
                    intReferenceCount = System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
                Loop While intReferenceCount > 0
            Catch
            Finally
                obj = Nothing
            End Try
        End If
    End Sub

    ''' <summary>
    ''' prerender event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    '''' <summary>
    '''' clear all control except control
    '''' </summary>
    '''' <param name="control">excluded control</param>
    '''' <remarks></remarks>
    'Private Sub ClearControls(ByVal control As Control)
    '    Dim i As Integer
    '    For i = control.Controls.Count - 1 To 0 Step -1
    '        ClearControls(control.Controls(i))
    '    Next i
    '    If Not TypeOf control Is TableCell Then
    '        If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
    '            Dim literal As New LiteralControl
    '            control.Parent.Controls.Add(literal)
    '            Try
    '                literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
    '            Catch
    '            End Try
    '            control.Parent.Controls.Remove(control)
    '        Else
    '            If Not (control.GetType().GetProperty("Text") Is Nothing) Then
    '                Dim literal As New LiteralControl
    '                control.Parent.Controls.Add(literal)
    '                literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
    '                control.Parent.Controls.Remove(control)
    '            End If
    '        End If
    '    End If
    'End Sub 'ClearControls

    '''' <summary>
    '''' bind selected item
    '''' </summary>
    '''' <remarks></remarks>
    'Private Sub BindSelected()
    '    Dim Rows As New ArrayList
    '    For Each IdPk As Int32 In Me.SetnGetSelectedItem
    '        Dim rowData() As AMLDAL.AMLDataSet.VerificationList_MasterRow = Me.SetnGetBindTable.Select("VerificationListId = '" & IdPk & "'")
    '        If rowData.Length > 0 Then
    '            Rows.Add(rowData(0))
    '        End If
    '    Next
    '    Me.GridMSUserView.DataSource = Rows
    '    Me.GridMSUserView.AllowPaging = False
    '    Me.GridMSUserView.DataBind()

    '    'Sembunyikan kolom ke 0,1,9 & 10 agar tidak ikut diekspor ke excel
    '    Me.GridMSUserView.Columns(0).Visible = False
    '    Me.GridMSUserView.Columns(1).Visible = False
    '    Me.GridMSUserView.Columns(9).Visible = False
    '    Me.GridMSUserView.Columns(10).Visible = False
    'End Sub

    ''' <summary>
    ''' export button 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Me.CollectSelected()
            'Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            'Me.BindSelected()
            'Response.Clear()
            'Response.AddHeader("content-disposition", "attachment;filename=VerificationListView.xls")
            'Response.Charset = ""
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            'Response.ContentType = "application/vnd.xls"
            'Me.EnableSession = False
            'Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            'Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            'ClearControls(GridMSUserView)
            'GridMSUserView.RenderControl(htmlWrite)
            'Response.Write(strStyle)
            'Response.Write(stringWrite.ToString())
            'Response.End()
            Me.ExportToExcel()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    ''' <summary>
    ''' select all
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        'Me.SetnGetSelectedItem.Clear()
        'For Each gridRow As DataGridItem In Me.GridMSUserView.Items
        '    If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
        '        Dim VerificationListID As String = gridRow.Cells(1).Text
        '        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        '        If Me.SetnGetSelectedItem.Contains(VerificationListID) Then
        '            If Me.CheckBoxSelectAll.Checked Then
        '                ArrTarget.Add(VerificationListID)
        '            Else
        '                ArrTarget.Remove(VerificationListID)
        '            End If
        '        Else
        '            If Me.CheckBoxSelectAll.Checked Then
        '                ArrTarget.Add(VerificationListID)
        '            End If
        '        End If
        '        Me.SetnGetSelectedItem = ArrTarget
        '    End If
        'Next

        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        For Each gridRow As DataGridItem In GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(1).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItem.Contains(StrKey) = False Then
                        ArrTarget.Add(StrKey)
                    End If
                Else
                    If Me.SetnGetSelectedItem.Contains(StrKey) Then
                        ArrTarget.Remove(StrKey)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItem = ArrTarget
    End Sub

    Protected Sub LinkButtonAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonAddNew.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "VerificationListAdd.aspx"

        Me.Response.Redirect("VerificationListAdd.aspx", False)
    End Sub

    Private Sub ExportAllToExcel()
        Try
            Me.CollectSelected()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindAll()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=VerificationListView.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridMSUserView)
            GridMSUserView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    'Public Sub ExportAllToExcel()
    '    Dim range As String = ""
    '    Dim xlApp As Excel.ApplicationClass
    '    Dim xlWorkbooks As Excel.Workbooks
    '    Dim xlWorkbook As Excel.Workbook
    '    Dim xlWorkSheets As Excel.Sheets
    '    Dim xlWorkSheet As Excel.Worksheet
    '    Dim xlRange As Excel.Range
    '    Dim xlCells As Excel.Range
    '    Dim xlFont As Excel.Font
    '    'Dim xlInterior As Excel.Interior
    '    'Dim xlcomment As Excel.Comment

    '    Try
    '        xlApp = New Excel.Application
    '        xlApp.DisplayAlerts = False
    '        xlApp.UserControl = False
    '        xlApp.Visible = False
    '        xlWorkbooks = xlApp.Workbooks
    '        xlWorkbook = xlWorkbooks.Add
    '        xlWorkSheets = xlWorkbook.Worksheets

    '        Dim ArrCollectVerificationList As New ArrayList
    '        Dim AllTable As Data.DataTable = Sahassa.AML.Commonly.GetDatasetPaging(Me.Tables, Me.PK, Me.SetnGetSort, 0, 20, Me.Fields, Me.SetnGetFieldSearch.Replace("-=Search=-", Me.SetnGetValueSearch.Replace("'", "''")), "")
    '        For Each dr As Data.DataRow In AllTable.Rows
    '            ArrCollectVerificationList.Add(dr.Item("VerificationListId"))
    '        Next

    '        ' ID Number
    '        Try
    '            xlWorkSheet = xlWorkSheets.Add
    '            xlWorkSheet.Name = "Verification List - IDNumber"
    '            xlCells = xlWorkSheet.Cells

    '            range = "A1:B1"
    '            xlRange = xlWorkSheet.Range(range)
    '            xlRange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter
    '            xlRange.Interior.Color = RGB(192, 0, 0)
    '            xlRange.Merge()
    '            xlRange.Value = "Verification List IDNumber"
    '            NAR(xlRange)

    '            ' Create Header Alias
    '            Dim HeaderMasterAlias() As String = {"VerificationListId", "IDNumber"}
    '            For i As Integer = 0 To 1
    '                xlCells = xlWorkSheet.Cells
    '                xlRange = xlCells(2, i + 1)
    '                xlRange.Value = HeaderMasterAlias(i)
    '                xlFont = xlRange.Font
    '                xlFont.Bold = True
    '                NAR(xlFont)
    '                NAR(xlRange)
    '            Next

    '            Dim IntCounter As Integer = 3
    '            Using AccessIDNumberVerification As New AMLDAL.VerificationListViewExportToExcelTableAdapters.VerificationList_IDNumberTableAdapter
    '                For i As Int64 = 0 To ArrCollectVerificationList.Count - 1
    '                    Using dtIDNumber As Data.DataTable = AccessIDNumberVerification.GetDataByVerificationListId(CInt(ArrCollectVerificationList(i)))
    '                        For Each RowIDNumber As AMLDAL.VerificationListViewExportToExcel.VerificationList_IDNumberRow In dtIDNumber.Rows
    '                            Dim StrValueIdNumber() As String = {ArrCollectVerificationList(i), RowIDNumber.IDNumber}
    '                            ' isi IdNumber
    '                            For j As Integer = 0 To 1
    '                                xlCells = xlWorkSheet.Cells
    '                                xlRange = xlCells(IntCounter, j + 1)
    '                                xlRange.Value = StrValueIdNumber(j)
    '                                NAR(xlRange)
    '                            Next
    '                            IntCounter += 1
    '                        Next
    '                    End Using
    '                Next
    '            End Using
    '            xlWorkSheet.Columns.AutoFit()
    '        Catch
    '            Throw
    '        Finally
    '            NAR(xlWorkSheet)
    '        End Try

    '        ' isi Alias
    '        Try
    '            xlWorkSheet = xlWorkSheets.Add
    '            xlWorkSheet.Name = "Verification List - Alias"
    '            xlCells = xlWorkSheet.Cells

    '            range = "A1:B1"
    '            xlRange = xlWorkSheet.Range(range)
    '            xlRange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter
    '            xlRange.Interior.Color = RGB(192, 0, 0)
    '            xlRange.Merge()
    '            xlRange.Value = "Verification List Alias"
    '            NAR(xlRange)

    '            ' Create Header Alias
    '            Dim HeaderMasterAlias() As String = {"VerificationListId", "Alias"}
    '            For i As Integer = 0 To 1
    '                xlCells = xlWorkSheet.Cells
    '                xlRange = xlCells(2, i + 1)
    '                xlRange.Value = HeaderMasterAlias(i)
    '                xlFont = xlRange.Font
    '                xlFont.Bold = True
    '                NAR(xlFont)
    '                NAR(xlRange)
    '            Next
    '            Dim IntCounter As Integer = 3
    '            Using AccessAliasVerification As New AMLDAL.VerificationListViewExportToExcelTableAdapters.VerificationList_AliasTableAdapter
    '                For i As Int64 = 0 To ArrCollectVerificationList.Count - 1
    '                    Using dtAlias As Data.DataTable = AccessAliasVerification.GetDataByID(CInt(ArrCollectVerificationList(i)))
    '                        For Each RowAlias As AMLDAL.VerificationListViewExportToExcel.VerificationList_AliasRow In dtAlias.Rows
    '                            Dim StrValueAlias() As String = {ArrCollectVerificationList(i), RowAlias.Name}
    '                            ' isi address
    '                            For j As Integer = 0 To 1
    '                                xlCells = xlWorkSheet.Cells
    '                                xlRange = xlCells(IntCounter, j + 1)
    '                                xlRange.Value = StrValueAlias(j)
    '                                NAR(xlRange)
    '                            Next
    '                            IntCounter += 1
    '                        Next
    '                    End Using
    '                Next
    '            End Using
    '            xlWorkSheet.Columns.AutoFit()
    '        Catch
    '            Throw
    '        Finally
    '            NAR(xlWorkSheet)
    '        End Try

    '        ' Address
    '        Try
    '            xlWorkSheet = xlWorkSheets.Add
    '            xlWorkSheet.Name = "Verification List - Address"
    '            xlCells = xlWorkSheet.Cells

    '            range = "A1:E1"
    '            xlRange = xlWorkSheet.Range(range)
    '            xlRange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter
    '            xlRange.Interior.Color = RGB(192, 0, 0)
    '            xlRange.Merge()
    '            xlRange.Value = "Verification List Address"
    '            NAR(xlRange)

    '            ' Create Header Address
    '            Dim HeaderMasterAddress() As String = {"VerificationListId", "Address", "Postal Code", "Address Type Name", "IsLocalAddress"}
    '            For i As Integer = 0 To 4
    '                xlCells = xlWorkSheet.Cells
    '                xlRange = xlCells(2, i + 1)
    '                xlRange.Value = HeaderMasterAddress(i)
    '                xlFont = xlRange.Font
    '                xlFont.Bold = True
    '                NAR(xlFont)
    '                NAR(xlCells)
    '            Next
    '            Dim IntCounter As Integer = 3
    '            Using AccessAddressVerification As New AMLDAL.VerificationListViewExportToExcelTableAdapters.VerificationList_AddressTableAdapter
    '                For i As Int64 = 0 To ArrCollectVerificationList.Count - 1
    '                    Using dtAddress As Data.DataTable = AccessAddressVerification.GetData
    '                        For Each RowAddress As AMLDAL.VerificationListViewExportToExcel.VerificationList_AddressRow In dtAddress.Select("VerificationListId=" & ArrCollectVerificationList(i))
    '                            Dim StrValueAddress() As String = {ArrCollectVerificationList(i), RowAddress.Address, RowAddress.Item("PostalCode").ToString, Me.GetTypeAddress(CInt(RowAddress.AddressTypeId)), IIf(RowAddress.IsLocalAddress = True, "Yes", "No")}
    '                            ' isi address
    '                            For j As Integer = 0 To 4
    '                                xlCells = xlWorkSheet.Cells
    '                                xlRange = xlCells(IntCounter, j + 1)
    '                                xlRange.Value = StrValueAddress(j)
    '                                NAR(xlRange)
    '                            Next
    '                            IntCounter += 1
    '                        Next
    '                    End Using
    '                Next
    '            End Using
    '            xlWorkSheet.Columns.AutoFit()
    '        Catch
    '            Throw
    '        Finally
    '            NAR(xlWorkSheet)
    '        End Try


    '        Try
    '            xlWorkSheet = xlWorkSheets.Add
    '            xlWorkSheet.Name = "Verification List - Master"

    '            xlCells = xlWorkSheet.Cells

    '            range = "A1:N1"
    '            xlRange = xlWorkSheet.Range(range)
    '            xlRange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter
    '            xlRange.Interior.Color = RGB(192, 0, 0)
    '            xlRange.Merge()
    '            xlRange.Value = "Verification List Master"
    '            NAR(xlRange)

    '            ' Create Header Verification Master 
    '            Dim HeaderMaster() As String = {"VerificationListId", "Date Entered", "Date Updated", "DateOfData", "Display Name", "Date Of Birth", "Type Name", "Category Name", "Custom Remark 1", "Custom Remark 2", "Custom Remark 3", "Custom Remark 3", "Custom Remark 4", "Custom Remark 5", "Ref Id"}
    '            For i As Integer = 0 To 13
    '                xlCells = xlWorkSheet.Cells
    '                xlRange = xlCells(2, i + 1)
    '                xlRange.Value = HeaderMaster(i)
    '                xlFont = xlRange.Font
    '                xlFont.Bold = True
    '                NAR(xlFont)
    '                NAR(xlRange)
    '            Next

    '            Using AccessVerificationListMaster As New AMLDAL.VerificationListViewExportToExcelTableAdapters.VerificationList_MasterTableAdapter
    '                For i As Int64 = 0 To ArrCollectVerificationList.Count - 1
    '                    Using dtVerificationMaster As Data.DataTable = AccessVerificationListMaster.GetDataByID(CInt(ArrCollectVerificationList(i)))
    '                        Dim RowMaster As AMLDAL.VerificationListViewExportToExcel.VerificationList_MasterRow = dtVerificationMaster.Rows(0)
    '                        Dim DateEntered, DateUpdated, DateOfData, DateOfBirth As String

    '                        DateEntered = RowMaster.DateEntered
    '                        DateUpdated = RowMaster.DateUpdated
    '                        DateOfData = RowMaster.DateUpdated
    '                        DateOfBirth = RowMaster.DateOfBirth

    '                        Dim StrValueVerificationMaster() As String = {RowMaster.VerificationListId, DateEntered, DateUpdated, DateOfData, RowMaster.DisplayName, DateOfBirth, Me.GetTypeName(CInt(RowMaster.VerificationListTypeId)), Me.GetCategoryName(CInt(RowMaster.VerificationListCategoryId)), RowMaster.CustomRemark1, RowMaster.CustomRemark2, RowMaster.CustomRemark3, RowMaster.CustomRemark4, RowMaster.CustomRemark5, ""}
    '                        For j As Integer = 0 To 13
    '                            xlCells = xlWorkSheet.Cells
    '                            xlRange = xlCells(3 + i, j + 1)
    '                            xlRange.Value = StrValueVerificationMaster(j)
    '                            NAR(xlRange)
    '                        Next
    '                    End Using
    '                Next
    '            End Using
    '            xlWorkSheet.Columns.AutoFit()
    '        Catch
    '            Throw
    '        Finally
    '            NAR(xlWorkSheet)
    '        End Try



    '        xlWorkbook.SaveAs(Server.MapPath("temp") & "\" & Sahassa.AML.Commonly.SessionUserId & "VerificationListView.xls")
    '        NAR(xlRange)
    '        NAR(xlWorkSheet)
    '        NAR(xlWorkbook)
    '        NAR(xlWorkbooks)
    '        If Not (xlApp Is Nothing) Then
    '            xlApp.Quit()
    '        End If
    '        NAR(xlApp)
    '        'GC cleanup 
    '        System.GC.Collect()
    '        System.GC.WaitForPendingFinalizers()
    '        Response.Redirect("temp/" & Sahassa.AML.Commonly.SessionUserId & "VerificationListView.xls", False)
    '    Catch ex As Exception
    '        Me.cvalPageError.IsValid = False
    '        Me.cvalPageError.ErrorMessage = ex.Message
    '        LogError(ex)
    '    Finally
    '        NAR(xlRange)
    '        NAR(xlWorkSheet)
    '        NAR(xlWorkbook)
    '        NAR(xlWorkbooks)
    '        If Not (xlApp Is Nothing) Then
    '            xlApp.Quit()
    '        End If
    '        NAR(xlApp)
    '        'GC cleanup 
    '        System.GC.Collect()
    '        System.GC.WaitForPendingFinalizers()
    '    End Try
    'End Sub

    Protected Sub LnkBtnExportAllToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LnkBtnExportAllToExcel.Click
        Try
            Me.ExportAllToExcel()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class