﻿Imports CDDLNPNettier.Data
Imports CDDLNPNettier.Entities
Imports AMLBLL
Imports AMLBLL.CDDLNPBLL
Imports System.Data
Imports System.Drawing
Imports Microsoft.Win32

Partial Class CDDLNPPrint
    Inherits Parent

    Private ReadOnly Property GetPK_CDDLNP_ID() As Int32
        Get
            Return Request.QueryString("CID")
        End Get
    End Property

    Private ReadOnly Property GetFK_CDD_Type_ID() As Int32
        Get
            Return Request.QueryString("TID")
        End Get
    End Property

    Private ReadOnly Property GetFK_CDD_NasabahType_ID() As Int32
        Get
            Return Request.QueryString("NTID")
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Dim strStatement1 As String = "Saya menyatakan bahwa berdasarkan pengetahuan saya yang sebenar-benarnya, informasi yang terdapat pada lembar EDD ini adalah benar dan telah diverifikasi sesuai dengan ketentuan [EDD] PT Bank CIMB Niaga yang berlaku, dan bahwa ketentuan [EDD] PT Bank CIMB Niaga telah dipenuhi"
            Dim strStatement2 As String = "Saya menyatakan bahwa saya telah melakukan review terhadap EDD ini dan menyatakan setuju dengan hasil EDD yang dilakukan oleh RM, dan berdasarkan pengetahuan saya, ketentuan [EDD] PT Bank CIMB Niaga telah dipenuhi"

            'Set CDD Customer Type
            Using objCustomerType As CDD_NasabahType = DataRepository.CDD_NasabahTypeProvider.GetByPK_CDD_NasabahType_ID(Me.GetFK_CDD_NasabahType_ID)
                If objCustomerType IsNot Nothing Then
                    SessionCustomerType = objCustomerType.CDD_NasabahType_ID
                End If
            End Using

            'Set Level
            Using objLevel As TList(Of CDDLNP_WorkflowParameter) = DataRepository.CDDLNP_WorkflowParameterProvider.GetPaged(String.Empty, "Level asc", 0, 4, 0)
                If objLevel.Count = 4 Then
                    lblLevel1.Text = objLevel(0).GroupWorkflow
                    lblLevel2.Text = objLevel(1).GroupWorkflow
                    lblLevel3.Text = objLevel(2).GroupWorkflow
                    lblLevel4.Text = objLevel(3).GroupWorkflow
                End If
            End Using

            'Set CDD Type
            Using objCDDType As CDD_Type = DataRepository.CDD_TypeProvider.GetByPK_CDD_Type_ID(Me.GetFK_CDD_Type_ID)
                If objCDDType IsNot Nothing Then
                    lblHeader.Text = "EDD " & objCDDType.CDD_Type
                End If
            End Using

            'Buat form
            Using objCDDLNP As New CDDLNPBLL
                objCDDLNP.ConstructFormDelete(Me.TableCategory, Me.GetFK_CDD_Type_ID, Me.GetFK_CDD_NasabahType_ID, Me.GetPK_CDDLNP_ID)
                objCDDLNP.LoadAnswerView(Me.TableCategory, Me.GetPK_CDDLNP_ID)
                objCDDLNP.HideUncheck(Me.TableCategory, True)
            End Using

            Dim intPK_CDDLNP_ID As Int32 = Me.GetPK_CDDLNP_ID
            Using objCDDLNP As CDDLNP = DataRepository.CDDLNPProvider.GetByPK_CDDLNP_ID(Me.GetPK_CDDLNP_ID)
                'Set AML Risk Rating
                Dim lblAMLResult As Label = CType(TableCategory.FindControl("lblAMLResult"), Label)
                lblAMLResult.Text = objCDDLNP.AMLRiskRatingPersonal

                'Approval Untuk Beneficial owner menggunakan approval dari anaknya.
                If objCDDLNP.FK_RefParent_ID.GetValueOrDefault > 0 Then
                    intPK_CDDLNP_ID = objCDDLNP.FK_RefParent_ID.GetValueOrDefault

                    'Set Header untuk beneficial owner
                    Using objCustomer As CDDLNP = DataRepository.CDDLNPProvider.GetByPK_CDDLNP_ID(intPK_CDDLNP_ID)
                        If objCustomer IsNot Nothing Then
                            lblHeader.Text = "EDD Beneficial Owner of " & objCustomer.Nama
                        End If
                    End Using
                End If

                'Set Workflow
                Dim IsPEP As Boolean = objCDDLNP.IsPEP.GetValueOrDefault
                Using objCDDLNPParent As TList(Of CDDLNP) = DataRepository.CDDLNPProvider.GetPaged("FK_RefParent_ID = " & Me.GetPK_CDDLNP_ID, String.Empty, 0, 1, 0)
                    If objCDDLNPParent.Count > 0 Then
                        IsPEP = IsPEP Or objCDDLNPParent(0).IsPEP.GetValueOrDefault
                    End If
                End Using
                Dim intLastApprovalLevel As Int32 = DataRepository.Provider.ExecuteScalar(Data.CommandType.Text, "usp_GetFinalApprovalLevel '" & objCDDLNP.AMLRiskRatingGabungan & "', " & IsPEP)

                Dim IsNeedLevel4Approval As Boolean = (intLastApprovalLevel >= 4)
                Dim IsNeedLevel3Approval As Boolean = (intLastApprovalLevel >= 3)
                'Minimum Approval adalah RM (Lvl 1) dan Team Head (Lvl 2), sehingga tidak perlu di set visible

                tdApproval4.Visible = IsNeedLevel4Approval
                tdStatement4.Visible = IsNeedLevel4Approval
                tdUser4.Visible = IsNeedLevel4Approval
                tdDate4.Visible = IsNeedLevel4Approval

                tdApproval3.Visible = IsNeedLevel3Approval
                tdStatement3.Visible = IsNeedLevel3Approval
                tdUser3.Visible = IsNeedLevel3Approval
                tdDate3.Visible = IsNeedLevel3Approval

                Select Case objCDDLNP.FK_CDD_NasabahType_ID.GetValueOrDefault(0)
                    Case 1, 2
                        strStatement1 = strStatement1.Replace("[EDD]", "EDD Private Banking")
                        strStatement2 = strStatement2.Replace("[EDD]", "EDD Private Banking")

                    Case 3, 4
                        strStatement1 = strStatement1.Replace("[EDD]", "EDD")
                        strStatement2 = strStatement2.Replace("[EDD]", "EDD")

                    Case Else
                        strStatement1 = strStatement1.Replace("[EDD]", "EDD")
                        strStatement2 = strStatement2.Replace("[EDD]", "EDD")

                End Select
            End Using

            'Cek apakah status approval terakhir adalah on progress (2) atau approved (5), selain itu dianggap belum di approve oleh siapa pun.
            If DataRepository.vw_CDDLNP_ViewProvider.GetTotalItems("PK_CDDLNP_ID = " & intPK_CDDLNP_ID & " AND LastApprovalStatus IN (2,5)", 0) > 0 Then

                Dim RequestedDate As DateTime

                Using objApproval As TList(Of CDDLNP_Approval) = DataRepository.CDDLNP_ApprovalProvider.GetPaged("FK_CDDLNP_ID = " & intPK_CDDLNP_ID & " AND LastWorkflowLevel = 1 AND LastApprovalStatus IN (2,5)", "PK_CDDLNP_Approval_ID desc", 0, Int32.MaxValue, 0)
                    If objApproval.Count > 0 Then
                        lblUser1.Text = GetUserName(objApproval(0).LastApprovalBy)
                        lblDate1.Text = objApproval(0).LastApprovalDate.GetValueOrDefault.ToString("dd-MMM-yyyy HH:mm")
                        RequestedDate = objApproval(0).LastApprovalDate
                        lblStatement1.Text = strStatement1
                    End If
                End Using

                Using objApproval As TList(Of CDDLNP_Approval) = DataRepository.CDDLNP_ApprovalProvider.GetPaged("FK_CDDLNP_ID = " & intPK_CDDLNP_ID & " AND LastWorkflowLevel = 2 AND LastApprovalStatus IN (2,5) AND LastApprovalDate > '" & RequestedDate & "'", "PK_CDDLNP_Approval_ID desc", 0, Int32.MaxValue, 0)
                    If objApproval.Count > 0 Then
                        lblUser2.Text = GetUserName(objApproval(0).LastApprovalBy)
                        lblDate2.Text = objApproval(0).LastApprovalDate.GetValueOrDefault.ToString("dd-MMM-yyyy HH:mm")
                        RequestedDate = objApproval(0).LastApprovalDate
                        lblStatement2.Text = strStatement2
                    End If
                End Using

                Using objApproval As TList(Of CDDLNP_Approval) = DataRepository.CDDLNP_ApprovalProvider.GetPaged("FK_CDDLNP_ID = " & intPK_CDDLNP_ID & " AND LastWorkflowLevel = 3 AND LastApprovalStatus IN (2,5) AND LastApprovalDate > '" & RequestedDate & "'", "PK_CDDLNP_Approval_ID desc", 0, Int32.MaxValue, 0)
                    If objApproval.Count > 0 Then
                        lblUser3.Text = GetUserName(objApproval(0).LastApprovalBy)
                        lblDate3.Text = objApproval(0).LastApprovalDate.GetValueOrDefault.ToString("dd-MMM-yyyy HH:mm")
                        RequestedDate = objApproval(0).LastApprovalDate
                        lblStatement3.Text = strStatement2
                    End If
                End Using

                Using objApproval As TList(Of CDDLNP_Approval) = DataRepository.CDDLNP_ApprovalProvider.GetPaged("FK_CDDLNP_ID = " & intPK_CDDLNP_ID & " AND LastWorkflowLevel = 4 AND LastApprovalStatus IN (2,5) AND LastApprovalDate > '" & RequestedDate & "'", "PK_CDDLNP_Approval_ID desc", 0, Int32.MaxValue, 0)
                    If objApproval.Count > 0 Then
                        lblUser4.Text = GetUserName(objApproval(0).LastApprovalBy)
                        lblDate4.Text = objApproval(0).LastApprovalDate.GetValueOrDefault.ToString("dd-MMM-yyyy HH:mm")
                        RequestedDate = objApproval(0).LastApprovalDate
                        lblStatement4.Text = strStatement2
                    End If
                End Using

            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Function GetUserName(ByVal strUserID As String) As String
        Using objUser As SahassaNettier.Entities.TList(Of SahassaNettier.Entities.User) = SahassaNettier.Data.DataRepository.UserProvider.GetByUserID(strUserID)
            If objUser.Count > 0 Then
                Return objUser(0).UserName
            Else
                Return strUserID
            End If
        End Using
    End Function

    'Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImagePrint.Click
    '    Try
    '        Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
    '        Response.Clear()
    '        Response.ClearHeaders()
    '        Response.AddHeader("content-disposition", "attachment;filename=SalesRejectedDetail.doc")
    '        Response.Charset = ""
    '        Response.Cache.SetCacheability(HttpCacheability.NoCache)
    '        Response.ContentType = "application/vnd.word"
    '        Me.EnableViewState = False
    '        Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
    '        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
    '        Me.RenderControl(htmlWrite)
    '        Response.Write(strStyle)
    '        Response.Write(stringWrite.ToString())
    '        Response.End()
    '    Catch ex As Exception
    '        Throw
    '    End Try
    'End Sub

    'Public Sub SetIEFooter()
    '    Dim IERegKey As RegistryKey
    '    Dim footer As String

    '    IERegKey = Registry.CurrentUser.OpenSubKey("Software\\Microsoft\\Internet Explorer\\PageSetup", True)
    '    footer = IERegKey.GetValue("footer", 0)

    '    IERegKey.SetValue("footer", "")
    '    IERegKey.SetValue("header", "")


    '    ' Reset the values after printing is completed
    '    IERegKey.SetValue("footer", footer)

    'End Sub
End Class