Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Partial Class VerificationListDelete
    Inherits Parent

#Region " Delete Verification List dan Audit trail"
    Private Sub DeleteVerificationListBySU()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            'Using TransScope As New Transactions.TransactionScope
            Dim VerificationListId As Int32 = Session("VerificationListId")

            Using AccessVerificationList As New AMLDAL.AMLDataSetTableAdapters.VerificationList_MasterTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessVerificationList, Data.IsolationLevel.ReadUncommitted)
                VerificationListId = AccessVerificationList.DeleteVerificationListMasterByVerificationListID(VerificationListId)
            End Using

            Using AccessVerificationListAlias As New AMLDAL.AMLDataSetTableAdapters.VerificationList_AliasTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessVerificationListAlias, oSQLTrans)
                AccessVerificationListAlias.DeleteVerificationList_AliasByVerificationList_Alias_ID(VerificationListId)
            End Using

            Using AccessVerificationListAddress As New AMLDAL.AMLDataSetTableAdapters.VerificationList_AddressTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessVerificationListAddress, oSQLTrans)
                AccessVerificationListAddress.DeleteVerificationList_AddressByVerificationList_Address_ID(VerificationListId)
            End Using

            Using AccessVerificationListIDNumber As New AMLDAL.AMLDataSetTableAdapters.VerificationList_IDNumberTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessVerificationListIDNumber, oSQLTrans)
                AccessVerificationListIDNumber.DeleteVerificationList_IDNumberByVerificationList_IDNumber_ID(VerificationListId)
            End Using

            oSQLTrans.Commit()
            'End Using
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub

    Private Sub InsertAuditTrail()
        Dim oSQLTrans As SqlTransaction = Nothing

        Try
            Dim objTableAliases As Data.DataTable = CType(Session("AliasesDatatable_Delete"), DataTable)
            Dim objTableAddresses As Data.DataTable = CType(Session("AddressesDatatable_Delete"), DataTable)
            Dim objTableIDNos As Data.DataTable = CType(Session("IDNosDatatable_Delete"), DataTable)
            Dim objTableCustomRemarks As Data.DataTable = CType(Session("CustomRemarksDatatable_Delete"), DataTable)

            Dim DisplayName As String = Session("DisplayName")
            Dim VerificationListId As Int32 = Session("VerificationListId")

            Dim DateEntered As String = "N/A"
            If Session("DateEntered") <> "01-Jan-1900" Then
                DateEntered = String.Format("{0:dd-MMM-yyyy}", Session("DateEntered"))
            End If

            Dim DateUpdated As String = "N/A"
            If Session("DateUpdated") <> "01-Jan-1900" Then
                DateUpdated = String.Format("{0:dd-MMM-yyyy}", Session("DateUpdated"))
            End If

            Dim DateOfData As String = "N/A"
            If Session("DateOfData") <> "01-Jan-1900" Then
                DateOfData = String.Format("{0:dd-MMM-yyyy}", Session("DateOfData"))
            End If

            Dim DOB As String = Me.LabelDOB.Text

            Dim ListType As String = Me.LabelVerificationListType.Text
            Dim CategoryName As String = Me.LabelCategory.Text
            Dim RefId As String = Session("RefId")

            Dim counter As Int32 = 14 + objTableAliases.Rows.Count + (3 * objTableAddresses.Rows.Count) + objTableIDNos.Rows.Count

            Dim CustomerRemarks As String() = New String(4) {"", "", "", "", ""}
            Dim i As Integer = 0

            For Each row As Data.DataRow In objTableCustomRemarks.Rows
                CustomerRemarks(i) = CType(row("CustomRemarks"), String)
                i += 1
            Next

            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(counter)
            'Using TransScope As New Transactions.TransactionScope
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAudit, Data.IsolationLevel.ReadUncommitted)
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List", "VerificationListId", "Delete", VerificationListId, "", "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List", "DateEntered", "Delete", DateEntered, "", "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List", "DateUpdated", "Delete", DateUpdated, "", "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List", "DateOfData", "Delete", DateOfData, "", "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List", "DisplayName", "Delete", DisplayName, "", "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List", "DateOfBirth", "Delete", DOB, "", "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List", "VerificationListTypeId", "Delete", ListType, "", "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List", "VerificationListCategoryId", "Delete", CategoryName, "", "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List", "CustomRemark1", "Delete", CustomerRemarks(0), "", "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List", "CustomRemark2", "Delete", CustomerRemarks(1), "", "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List", "CustomRemark3", "Delete", CustomerRemarks(2), "", "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List", "CustomRemark4", "Delete", CustomerRemarks(3), "", "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List", "CustomRemark5", "Delete", CustomerRemarks(4), "", "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List", "RefId", "Delete", RefId, "", "Accepted")

                For Each row As Data.DataRow In objTableAliases.Rows
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List Alias", "Name", "Delete", row("Aliases"), "", "Accepted")
                Next

                For Each row As Data.DataRow In objTableAddresses.Rows
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List Address", "Address", "Delete", row("Addresses"), "", "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List Address", "AddressTypeId", "Delete", row("AddressType"), "", "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List Address", "IsLocalAddress", "Delete", row("IsLocalAddress"), "", "Accepted")
                Next

                For Each row As Data.DataRow In objTableIDNos.Rows
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Verification List ID Number", "Name", "Delete", row("IDNos"), "", "Accepted")
                Next

                oSQLTrans.Commit()
            End Using
            'End Using
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub
#End Region

    ''' <summary>
    ''' get user id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetListId() As String
        Get
            Return Me.Request.Params("VerificationListId")
        End Get
    End Property

    ''' <summary>
    ''' cancel handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "VerificationListView.aspx"
        Me.Response.Redirect("VerificationListView.aspx", False)
    End Sub

    Private Function CheckInApproval() As Boolean
        Using AccessApproval As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Master_ApprovalTableAdapter
            Dim count As Int32 = AccessApproval.CountVerificationList_MasterApproval(Me.LabelVerificationListID.Text)

            'Jika count > 0 berarti VerificationListID tsb dlm status Pending Approval
            If count > 0 Then
                Return True
            Else 'Jika count = 0 berarti VerificationListID tsb tdk dlm status Pending Approval
                Return False
            End If
        End Using
    End Function

    ''' <summary>
    ''' update handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim oSQLTrans As SqlTransaction = Nothing

        Try
            If Me.CheckInApproval = False Then
                Using AccessVerificationList As New AMLDAL.AMLDataSetTableAdapters.VerificationList_MasterTableAdapter
                    Dim VerificationListId As Int32 = Session("VerificationListId")

                    Dim objTable As Data.DataTable = AccessVerificationList.GetVerificationListMasterDataByListID(VerificationListId)

                    'Jika objTable.Rows.Count = 0 berarti Verification List tsb sdh tdk ada lg dlm tabel VerificationList_Master
                    If objTable.Rows.Count = 0 Then
                        Throw New Exception("Cannot delete Verification List because it is no longer in the database.")
                    Else
                        'Jika User yg login adalah Super User
                        If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                            Me.InsertAuditTrail()
                            Me.DeleteVerificationListBySU()
                            Me.LblSuccess.Text = "The Verification List has been deleted."
                            Me.LblSuccess.Visible = True
                        Else 'Jika User yg login bukan Super User
                            Dim DateEntered As Nullable(Of DateTime)
                            If Session("DateEntered") <> "01-Jan-1900" Then
                                DateEntered = CDate(Session("DateEntered"))
                            End If

                            Dim DateUpdated As Nullable(Of DateTime)
                            If Session("DateUpdated") <> "01-Jan-1900" Then
                                DateUpdated = CDate(Session("DateUpdated"))
                            End If

                            Dim DateOfData As Nullable(Of DateTime)
                            If Session("DateOfData") <> "01-Jan-1900" Then
                                DateOfData = CDate(Session("DateOfData"))
                            End If

                            Dim DisplayName As String = Session("DisplayName")

                            Dim DateOfBirth As Nullable(Of DateTime)
                            If Session("DateOfBirth") <> "01-Jan-1900" Then
                                DateOfBirth = CDate(Session("DateOfBirth"))
                            End If

                            Dim VerificationListTypeId As String = Session("VerificationListTypeId")
                            Dim VerificationListCategoryId As Int32 = Session("VerificationListCategoryId")
                            Dim RefId As String = Session("RefId")

                            Dim objTableCustomRemarks As Data.DataTable = CType(Session("CustomRemarksDatatable_Delete"), DataTable)
                            Dim CustomerRemarks As String() = New String(4) {"", "", "", "", ""}
                            Dim i As Integer = 0

                            For Each row As Data.DataRow In objTableCustomRemarks.Rows
                                CustomerRemarks(i) = CType(row("CustomRemarks"), String)
                                i += 1
                            Next

                            'Dim counter As Int32 = 13 + objTableAliases.Rows.Count + (3 * objTableAddresses.Rows.Count) + objTableIDNos.Rows.Count

                            'Using TranScope As New Transactions.TransactionScope
                            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Master_PendingApprovalTableAdapter
                                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessPending, Data.IsolationLevel.ReadUncommitted)
                                Using AccessApproval As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Master_ApprovalTableAdapter
                                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessApproval, oSQLTrans)

                                    'Catat kegiatan penghapusan Verification List utk VerificationListID tsb dlm tabel VerificationList_Master_PendingApproval
                                    Dim PendingApprovalID As Int64 = AccessPending.InsertVerificationList_Master_PendingApproval(Sahassa.AML.Commonly.SessionUserId, Now, "Verification List Delete", 3)
                                    AccessApproval.Insert(PendingApprovalID, 3, VerificationListId, DateEntered, DateUpdated, DateOfData, DisplayName, DateOfBirth, VerificationListTypeId, VerificationListCategoryId, CustomerRemarks(0), CustomerRemarks(1), CustomerRemarks(2), CustomerRemarks(3), CustomerRemarks(4), Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, RefId, Nothing, Session("BirthPlace"), Nothing, Nothing, Nothing)

                                    'Catat kegiatan penghapusan Alias utk VerificationListID tsb dlm tabel VerificationList_Alias_PendingApproval
                                    Using AccessApprovalAliases As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Alias_ApprovalTableAdapter
                                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessApprovalAliases, oSQLTrans)
                                        Using AccessAlias As New AMLDAL.AMLDataSetTableAdapters.VerificationList_AliasTableAdapter
                                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAlias, oSQLTrans)
                                            Using AliasTable As AMLDAL.AMLDataSet.VerificationList_AliasDataTable = AccessAlias.GetVerificationListAliasByVerificationListID(Me.GetListId)
                                                'Sahassa.AML.TableAdapterHelper.SetTransaction(AliasTable, oSQLTrans)
                                                For Each AliasRow As AMLDAL.AMLDataSet.VerificationList_AliasRow In AliasTable.Rows
                                                    AccessApprovalAliases.Insert(PendingApprovalID, 3, AliasRow.VerificationList_Alias_ID, AliasRow.VerificationListId, AliasRow.Name, Nothing, Nothing, Nothing)
                                                Next
                                            End Using
                                        End Using
                                    End Using


                                    'Catat kegiatan penghapusan Address utk VerificationListID tsb dlm tabel VerificationList_Address_PendingApproval
                                    Using AccessApprovalAddresses As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Address_ApprovalTableAdapter
                                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessApprovalAddresses, oSQLTrans)
                                        Using AccessAddress As New AMLDAL.AMLDataSetTableAdapters.VerificationList_AddressTableAdapter
                                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAddress, oSQLTrans)
                                            Using AddressTable As AMLDAL.AMLDataSet.VerificationList_AddressDataTable = AccessAddress.GetVerificationListAddressByVerificationListID(Me.GetListId)
                                                'Sahassa.AML.TableAdapterHelper.SetTransaction(AddressTable, oSQLTrans)
                                                For Each AddressRow As AMLDAL.AMLDataSet.VerificationList_AddressRow In AddressTable.Rows
                                                    AccessApprovalAddresses.Insert(PendingApprovalID, 3, AddressRow.VerificationList_Address_Id, AddressRow.VerificationListId, AddressRow.Address, AddressRow.AddressTypeId, AddressRow.IsLocalAddress, Nothing, Nothing, Nothing, Nothing, True)
                                                Next
                                            End Using
                                        End Using
                                    End Using

                                    'Catat kegiatan penghapusan ID Number utk VerificationListID tsb dlm tabel VerificationList_IDNumber_PendingApproval
                                    Using AccessApprovalIDNos As New AMLDAL.AMLDataSetTableAdapters.VerificationList_IDNumber_ApprovalTableAdapter
                                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessApprovalIDNos, oSQLTrans)
                                        Using AccessIDNo As New AMLDAL.AMLDataSetTableAdapters.VerificationList_IDNumberTableAdapter
                                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessIDNo, oSQLTrans)
                                            Using IDNoTable As AMLDAL.AMLDataSet.VerificationList_IDNumberDataTable = AccessIDNo.GetVerificationIDNumberByVerificationListID(Me.GetListId)
                                                'Sahassa.AML.TableAdapterHelper.SetTransaction(IDNoTable, oSQLTrans)
                                                For Each IDNoRow As AMLDAL.AMLDataSet.VerificationList_IDNumberRow In IDNoTable.Rows
                                                    AccessApprovalIDNos.Insert(PendingApprovalID, 3, IDNoRow.VerificationList_IDNumber_ID, IDNoRow.VerificationListId, IDNoRow.IDNumber, Nothing, Nothing, Nothing)
                                                Next
                                            End Using
                                        End Using
                                    End Using

                                    oSQLTrans.Commit()

                                    Dim MessagePendingID As Integer = 81003 'MessagePendingID 81003 = Verification List Delete 

                                    Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & DisplayName

                                    Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & DisplayName, False)
                                End Using
                            End Using
                            'End Using
                        End If
                    End If
                End Using
            Else 'Jika Me.CheckInApproval = True berarti VerificationListID tsb dlm status PendingApproval dan tidak boleh dihapus
                Throw New Exception("Cannot delete the following Verification List : " & Me.LabelVerificationListID.Text & " because it is currently waiting for approval.")
            End If
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub

    ''' <summary>
    ''' filledit data
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillEditData()
        Using AccessList As New AMLDAL.AMLDataSetTableAdapters.VerificationList_MasterTableAdapter
            Using TableList As AMLDAL.AMLDataSet.VerificationList_MasterDataTable = AccessList.GetVerificationListMasterDataByListID(Me.GetListId)
                If TableList.Rows.Count > 0 Then
                    Dim i As Integer = 0
                    Dim TableRowList As AMLDAL.AMLDataSet.VerificationList_MasterRow = TableList.Rows(0)

                    Session("VerificationListId") = TableRowList.VerificationListId
                    Me.LabelVerificationListID.Text = Session("VerificationListId")

                    Dim DateEntered As String = String.Format("{0:dd-MMM-yyyy}", TableRowList.DateEntered)
                    Session("DateEntered") = DateEntered

                    Dim DateUpdated As String = String.Format("{0:dd-MMM-yyyy}", TableRowList.DateUpdated)
                    Session("DateUpdated") = DateUpdated

                    Dim DateOfData As String = String.Format("{0:dd-MMM-yyyy}", TableRowList.DateOfData)
                    Session("DateOfData") = DateOfData
                    If DateOfData = "01-Jan-1900" Then
                        Me.LabelDateOfData.Text = "N/A"
                    Else
                        Me.LabelDateOfData.Text = DateOfData
                    End If
                    Session("BirthPlace") = TableRowList.BirthPlace

                    Session("DisplayName") = TableRowList.DisplayName

                    Dim DOB As String = String.Format("{0:dd-MMM-yyyy}", TableRowList.DateOfBirth)
                    Session("DateOfBirth") = DOB
                    If DOB = "01-Jan-1900" Then
                        Me.LabelDOB.Text = "N/A"
                    Else
                        Me.LabelDOB.Text = DOB
                    End If

                    Session("VerificationListTypeId") = TableRowList.VerificationListTypeId
                    Using AccessListType As New AMLDAL.AMLDataSetTableAdapters.VerificationListTypeTableAdapter
                        Me.LabelVerificationListType.Text = AccessListType.GetListTypeNameByListTypeID(Session("VerificationListTypeId"))
                    End Using

                    Session("VerificationListCategoryId") = TableRowList.VerificationListCategoryId
                    Using AccessCategory As New AMLDAL.AMLDataSetTableAdapters.VerificationListCategoryTableAdapter
                        Me.LabelCategory.Text = AccessCategory.GetCategoryNameByCategoryID(Session("VerificationListCategoryId"))
                    End Using

                    Session("RefId") = TableRowList.RefId

                    '==========================================

                    'Tarik semua alias utk VerificationListID tsb
                    Dim myDt As New DataTable()
                    myDt = CreateDataTableAliases()
                    i = 1
                    Using AccessAlias As New AMLDAL.AMLDataSetTableAdapters.VerificationList_AliasTableAdapter
                        Using AliasTable As AMLDAL.AMLDataSet.VerificationList_AliasDataTable = AccessAlias.GetVerificationListAliasByVerificationListID(Me.GetListId)
                            For Each AliasRow As AMLDAL.AMLDataSet.VerificationList_AliasRow In AliasTable.Rows
                                AddAliasesToTable(i, AliasRow.Name, myDt)
                                i += 1
                            Next
                        End Using
                    End Using
                    Session("AliasesDatatable_Delete") = myDt

                    If myDt.Rows.Count > 0 Then
                        Me.GridViewAliases.DataSource = myDt
                        Me.GridViewAliases.DataBind()
                    Else
                        Me.GridViewAliasesRow.Visible = False
                        Me.SpacerAliases.Visible = False
                    End If

                    '==========================================
                    'Tarik semua address utk VerificationListID tsb
                    Dim myDt2 As New DataTable()
                    myDt2 = CreateDataTableAddresses()
                    i = 1
                    Using AccessAddress As New AMLDAL.AMLDataSetTableAdapters.VerificationList_AddressTableAdapter
                        Using AddressTable As AMLDAL.AMLDataSet.VerificationList_AddressDataTable = AccessAddress.GetVerificationListAddressByVerificationListID(Me.GetListId)
                            For Each AddressRow As AMLDAL.AMLDataSet.VerificationList_AddressRow In AddressTable.Rows
                                'AddAliasesToTable(AliasRow.VerificationList_Alias_ID, AliasRow.Name, "(Alias " & i & ")", myDt2)
                                Dim AddressTypeLabel As String

                                Using AccessAddressType As New AMLDAL.AMLDataSetTableAdapters.AddressTypeTableAdapter
                                    AddressTypeLabel = AccessAddressType.GetAddressTypeDescriptionByAddressTypeID(AddressRow.AddressTypeId)
                                End Using

                                AddAddressesToTable(i, AddressRow.Address, AddressRow.AddressTypeId, "(" & AddressTypeLabel & ")", AddressRow.IsLocalAddress, myDt2)
                                i += 1
                            Next
                        End Using
                    End Using
                    Session("AddressesDatatable_Delete") = myDt2

                    If myDt2.Rows.Count > 0 Then
                        Me.GridViewAddresses.DataSource = myDt2
                        Me.GridViewAddresses.DataBind()
                    Else
                        Me.GridViewAddressesRow.Visible = False
                        Me.SpacerAddresses.Visible = False
                    End If

                    '==========================================

                    'Tarik semua id number utk VerificationListID tsb
                    Dim myDt3 As New DataTable()
                    myDt3 = CreateDataTableIDNos()
                    i = 1
                    Using AccessIDNo As New AMLDAL.AMLDataSetTableAdapters.VerificationList_IDNumberTableAdapter
                        Using IDNoTable As AMLDAL.AMLDataSet.VerificationList_IDNumberDataTable = AccessIDNo.GetVerificationIDNumberByVerificationListID(Me.GetListId)
                            For Each IDNoRow As AMLDAL.AMLDataSet.VerificationList_IDNumberRow In IDNoTable.Rows
                                AddIDNosToTable(i, IDNoRow.IDNumber, myDt3)
                                i += 1
                            Next
                        End Using
                    End Using
                    Session("IDNosDatatable_Delete") = myDt3

                    If myDt3.Rows.Count > 0 Then
                        Me.GridViewIDNos.DataSource = myDt3
                        Me.GridViewIDNos.DataBind()
                    Else
                        Me.GridViewIDNosRow.Visible = False
                        Me.SpacerIDNos.Visible = False
                    End If

                    '============================================
                    Dim myDt4 As New DataTable()
                    myDt4 = CreateDataTableCustomRemarks()

                    i = 1

                    If TableRowList.CustomRemark1 <> "" Then
                        AddCustomRemarksToTable(i, TableRowList.CustomRemark1, myDt4)
                        i += 1
                    End If

                    If TableRowList.CustomRemark2 <> "" Then
                        AddCustomRemarksToTable(i, TableRowList.CustomRemark2, myDt4)
                        i += 1
                    End If

                    If TableRowList.CustomRemark3 <> "" Then
                        AddCustomRemarksToTable(i, TableRowList.CustomRemark3, myDt4)
                        i += 1
                    End If

                    If TableRowList.CustomRemark4 <> "" Then
                        AddCustomRemarksToTable(i, TableRowList.CustomRemark4, myDt4)
                        i += 1
                    End If

                    If TableRowList.CustomRemark5 <> "" Then
                        AddCustomRemarksToTable(i, TableRowList.CustomRemark5, myDt4)
                        i += 1
                    End If

                    Session("CustomRemarksDatatable_Delete") = myDt4

                    If myDt4.Rows.Count > 0 Then
                        Me.GridViewCustomRemarks.DataSource = myDt4
                        Me.GridViewCustomRemarks.DataBind()
                    Else
                        Me.GridViewCustomRemarksRow.Visible = False
                        Me.SpacerCustomRemarks.Visible = False
                    End If
                End If
            End Using
        End Using
    End Sub

    ''' <summary>
    ''' page load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.FillEditData()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Function CreateDataTableCustomRemarks() As DataTable
        Dim CustomRemarksDatatable_Delete As DataTable = New DataTable()

        Dim CustomRemarksDataColumn As DataColumn

        CustomRemarksDataColumn = New DataColumn()
        CustomRemarksDataColumn.DataType = Type.GetType("System.String")
        CustomRemarksDataColumn.ColumnName = "id"
        CustomRemarksDatatable_Delete.Columns.Add(CustomRemarksDataColumn)

        CustomRemarksDataColumn = New DataColumn()
        CustomRemarksDataColumn.DataType = Type.GetType("System.String")
        CustomRemarksDataColumn.ColumnName = "CustomRemarks"
        CustomRemarksDatatable_Delete.Columns.Add(CustomRemarksDataColumn)

        Return CustomRemarksDatatable_Delete
    End Function

    Private Sub AddCustomRemarksToTable(ByVal id As String, ByVal CustomRemarks As String, ByRef myTable As DataTable)
        Dim row As DataRow

        row = myTable.NewRow()

        row("id") = id
        row("CustomRemarks") = CustomRemarks

        myTable.Rows.Add(row)
    End Sub

    Private Function CreateDataTableAliases() As DataTable
        Dim AliasesDatatable_Delete As DataTable = New DataTable()

        Dim AliasesDataColumn As DataColumn

        AliasesDataColumn = New DataColumn()
        AliasesDataColumn.DataType = Type.GetType("System.String")
        AliasesDataColumn.ColumnName = "id"
        AliasesDatatable_Delete.Columns.Add(AliasesDataColumn)

        AliasesDataColumn = New DataColumn()
        AliasesDataColumn.DataType = Type.GetType("System.String")
        AliasesDataColumn.ColumnName = "Aliases"
        AliasesDatatable_Delete.Columns.Add(AliasesDataColumn)

        Return AliasesDatatable_Delete
    End Function

    Private Sub AddAliasesToTable(ByVal id As String, ByVal aliases As String, ByRef myTable As DataTable)
        Dim row As DataRow

        row = myTable.NewRow()

        row("id") = id
        row("Aliases") = aliases

        myTable.Rows.Add(row)

    End Sub

    Private Function CreateDataTableAddresses() As DataTable
        Dim AddressesDatatable_Delete As DataTable = New DataTable()

        Dim AddressesDataColumn As DataColumn

        AddressesDataColumn = New DataColumn()
        AddressesDataColumn.DataType = Type.GetType("System.String")
        AddressesDataColumn.ColumnName = "id"
        AddressesDatatable_Delete.Columns.Add(AddressesDataColumn)

        AddressesDataColumn = New DataColumn()
        AddressesDataColumn.DataType = Type.GetType("System.String")
        AddressesDataColumn.ColumnName = "Addresses"
        AddressesDatatable_Delete.Columns.Add(AddressesDataColumn)

        AddressesDataColumn = New DataColumn()
        AddressesDataColumn.DataType = Type.GetType("System.Int16")
        AddressesDataColumn.ColumnName = "AddressType"
        AddressesDatatable_Delete.Columns.Add(AddressesDataColumn)

        AddressesDataColumn = New DataColumn()
        AddressesDataColumn.DataType = Type.GetType("System.String")
        AddressesDataColumn.ColumnName = "AddressTypeLabel"
        AddressesDatatable_Delete.Columns.Add(AddressesDataColumn)

        AddressesDataColumn = New DataColumn()
        AddressesDataColumn.DataType = Type.GetType("System.Boolean")
        AddressesDataColumn.ColumnName = "IsLocalAddress"
        AddressesDatatable_Delete.Columns.Add(AddressesDataColumn)

        Return AddressesDatatable_Delete
    End Function

    Private Sub AddAddressesToTable(ByVal id As String, ByVal addresses As String, ByVal addresstype As Int16, ByVal AddressTypeLabel As String, ByVal islocaladdress As Boolean, ByRef myTable As DataTable)
        Dim row As DataRow

        row = myTable.NewRow()

        row("id") = id
        row("Addresses") = addresses
        row("AddressType") = addresstype
        row("AddressTypeLabel") = AddressTypeLabel
        row("IsLocalAddress") = islocaladdress

        myTable.Rows.Add(row)

    End Sub

    Private Function CreateDataTableIDNos() As DataTable
        Dim IDNosDatatable_Delete As DataTable = New DataTable()

        Dim IDNosDataColumn As DataColumn

        IDNosDataColumn = New DataColumn()
        IDNosDataColumn.DataType = Type.GetType("System.String")
        IDNosDataColumn.ColumnName = "id"
        IDNosDatatable_Delete.Columns.Add(IDNosDataColumn)

        IDNosDataColumn = New DataColumn()
        IDNosDataColumn.DataType = Type.GetType("System.String")
        IDNosDataColumn.ColumnName = "IDNos"
        IDNosDatatable_Delete.Columns.Add(IDNosDataColumn)

        Return IDNosDatatable_Delete
    End Function

    Private Sub AddIDNosToTable(ByVal id As String, ByVal idnos As String, ByRef myTable As DataTable)
        Dim row As DataRow

        row = myTable.NewRow()

        row("id") = id
        row("IDNos") = idnos

        myTable.Rows.Add(row)

    End Sub

End Class