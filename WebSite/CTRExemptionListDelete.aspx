<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="CTRExemptionListDelete.aspx.vb" Inherits="CTRExemptionListDelete" title="CTR Exemption List - Delete" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
	<table id="title"  border="2" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr>
            <td bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>CTR Exemption List - Delete
                </strong>
                <asp:Label ID="LblSucces" runat="server" CssClass="validationok" Visible="False"
                    Width="100%"></asp:Label></td>
        </tr>
       <tr class="formText">
            <td bgcolor="#ffffff" style="width: 500px; border-top-style: none; border-right-style: none;
                border-left-style: none; height: 24px; border-bottom-style: none">
                &nbsp;<asp:Image ID="Image2" runat="server" ImageUrl="~/Images/validationsign_animate.gif" />
                &nbsp;<asp:Label ID="Label1" runat="server" Text="The following Data will be deleted :"></asp:Label>
            </td>
        </tr>
    </table>
    
    
	<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2" height="72">      
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
            </td>
            <td bgcolor="#ffffff" style="height: 24px" width="20%">
                CIF No</td>
            <td bgcolor="#ffffff" style="height: 24px" width="5">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px" width="80%">
                <asp:TextBox ID="TxtCIFNo" runat="server" CssClass="textBox" MaxLength="15" ReadOnly="True"
                    TextMode="SingleLine"></asp:TextBox></td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" style="width: 24px; height: 24px">
            </td>
            <td bgcolor="#ffffff" style="height: 24px" width="20%">
                Account No</td>
            <td bgcolor="#ffffff" style="height: 24px" width="5">
                :</td>
            <td bgcolor="#ffffff" style="height: 24px" width="80%">
                <asp:textbox id="TxtAccountNo" runat="server" CssClass="textBox" MaxLength="15" TextMode="SingleLine"  ReadOnly="True"></asp:textbox>
                <asp:Label ID="LblCTRExemptionListID" runat="server" Visible="false" ></asp:Label></td>
        </tr>
		<tr class="formText">
			<td bgColor="#ffffff" style="width: 24px; height: 24px"></td>
			<td width="20%" bgColor="#ffffff" style="height: 24px">
                Name</td>
			<td width="5" bgColor="#ffffff" style="height: 24px">:</td>
			<td width="80%" bgColor="#ffffff" style="height: 24px"><asp:textbox id="TxtAccountName" runat="server" CssClass="textBox" MaxLength="20" ReadOnly="True"></asp:textbox>
                </td>
		</tr>
		<tr class="formText">
			<td bgColor="#ffffff" style="width: 24px; height: 24px;" rowspan="6">&nbsp;</td>
			<td bgColor="#ffffff" rowspan="6" style="height: 24px">
                Description<br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" rowspan="6" style="height: 24px">
                :<br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" rowspan="6" style="height: 24px">
                <asp:textbox id="TxtDescription" runat="server" CssClass="textBox" MaxLength="255" Width="200px" Height="63px" TextMode="MultiLine" ReadOnly="True"></asp:textbox>
                <asp:textbox id="TxtAddedBy" runat="server" CssClass="textBox" MaxLength="255" Width="200px" Height="63px" TextMode=SingleLine  Visible=false ReadOnly="True"></asp:textbox></td>
		</tr>
		<tr class="formText">
		</tr>
		<tr class="formText">
		</tr>
		<tr class="formText">
		</tr>
		<tr class="formText">
		</tr>
		<tr class="formText">
		</tr>

		<tr class="formText" bgColor="#dddddd" height="30">
			<td style="width: 24px"><IMG height="15" src="images/arrow.gif" width="15"></td>
			<td colSpan="3" style="height: 9px">
				<table cellSpacing="0" cellPadding="3" border="0">
					<tr>
						<td><asp:imagebutton id="ImageSave" runat="server" CausesValidation="True" SkinID="DeleteButton"></asp:imagebutton></td>
						<td><asp:imagebutton id="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton"></asp:imagebutton></td>
					</tr>
				</table><br />
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
		</tr>
	</table>    
	
</asp:Content>