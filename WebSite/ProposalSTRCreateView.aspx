<%@ Page Language="VB" MasterPageFile="~/masterpage.master" AutoEventWireup="false" CodeFile="ProposalSTRCreateView.aspx.vb" Inherits="ProposalSTRCreateView" title="Proposal STR Create View" Culture="id-ID" UICulture ="id-ID" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
    &nbsp;<table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
        height="72" style="border-top-style: none; border-right-style: none; border-left-style: none;
        border-bottom-style: none" width="100%">
        <tr>
            <td bgcolor="#ffffff" colspan="3" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Proposal STR - Create
                    <hr />
                </strong>
            </td>
        </tr>
    </table>
    <asp:Panel ID="Panel1" runat="server" DefaultButton="ImageButtonSearch" Width="100%">
        <table bgcolor="#dddddd" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
            width="100%">
            <tr id="searchbox">
                <td bgcolor="#ffffff" valign="top" width="98%">
                    <table border="0" cellpadding="0" cellspacing="4" width="100%">
                        <tr>
                            <td nowrap="nowrap" valign="middle">
                                <ajax:ajaxpanel id="AjaxPanel14" runat="server">
                                    <table style="width: 100%; height: 100%">
                                        <tr>
                                            <td nowrap style="height: 26px">
                                                Case ID</td>
                                            <td nowrap style="height: 26px">
                                                :</td>
                                            <td nowrap style="height: 26px">
                                                <asp:TextBox ID="txtCaseID" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox></td>
                                            <td>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                            <td nowrap style="height: 26px">
                                                Created Date<asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtCreatedDate1"
                                                    ControlToValidate="txtCreatedDate2" Display="Dynamic" ErrorMessage="Created Date Last Must Greater or Equal  than Created Date First"
                                                    Operator="GreaterThanEqual" Type="Date" >*</asp:CompareValidator></td>
                                            <td nowrap style="height: 26px">
                                                :</td>
                                            <td nowrap style="height: 26px; width: 332px;">
                                                <asp:TextBox ID="txtCreatedDate1" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox>
                                                <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtCreatedDate1"
                                                    Display="Dynamic" ErrorMessage="Created Date First must between 1-1-1753 and 31-12-9999"
                                                    MaximumValue="31-12-9999" MinimumValue="1-1-1753" Type="Date">*</asp:RangeValidator>
                                                <input id="popupCreatedFirst" title="Click to show calendar" style="border-right: #ffffff 0px solid;
                                                    border-top: #ffffff 0px solid; font-size: 11px; border-left: #ffffff 0px solid;
                                                    border-bottom: #ffffff 0px solid; height: 17px; background-image: url(Script/Calendar/cal.gif);
                                                    width: 16px;" type="button" name="popupCreatedFirst" runat="server">
                                                to
                                                <asp:TextBox ID="txtCreatedDate2" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox>
                                                <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="txtCreatedDate2"
                                                    Display="Dynamic" ErrorMessage="Created Date Last must between 1-1-1753 and 31-12-9999"
                                                    MaximumValue="31-12-9999" MinimumValue="1-1-1753" Type="Date">*</asp:RangeValidator>
                                                <input id="popupCreatedLast" title="Click to show calendar" style="border-right: #ffffff 0px solid;
                                                    border-top: #ffffff 0px solid; font-size: 11px; border-left: #ffffff 0px solid;
                                                    border-bottom: #ffffff 0px solid; height: 17px; background-image: url(Script/Calendar/cal.gif);
                                                    width: 16px;" type="button" name="popupCreatedLast" runat="server">
                                            </td>
                                            <td>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td nowrap style="height: 26px">
                                                Case Description</td>
                                            <td nowrap style="height: 26px">
                                                :</td>
                                            <td nowrap style="height: 26px">
                                                <asp:TextBox ID="txtDescription" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox></td>
                                            <td>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                            <td nowrap style="height: 26px">
                                                Last Updated Date<asp:CompareValidator ID="CompareValidator2" runat="server" ControlToCompare="txtLastUpdatedDate1"
                                                    ControlToValidate="txtLastUpdatedDate2" Display="Dynamic" ErrorMessage="Last Updated Date Last Must Greater or Equal than  Last Update Date First"
                                                    Operator="GreaterThanEqual" Type="Date">*</asp:CompareValidator></td>
                                            <td nowrap style="height: 26px">
                                                :</td>
                                            <td nowrap style="height: 26px; width: 332px;">
                                                <asp:TextBox ID="txtLastUpdatedDate1" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox>
                                                <asp:RangeValidator ID="RangeValidator3" runat="server" ControlToValidate="txtLastUpdatedDate1"
                                                    Display="Dynamic" ErrorMessage="Last Update Date First must between 1-1-1753 and 31-12-9999"
                                                    MaximumValue="31-12-9999" MinimumValue="1-1-1753" Type="Date">*</asp:RangeValidator>
                                                <input id="popupLastUpdatedFirst" title="Click to show calendar" style="border-right: #ffffff 0px solid;
                                                    border-top: #ffffff 0px solid; font-size: 11px; border-left: #ffffff 0px solid;
                                                    border-bottom: #ffffff 0px solid; height: 17px; background-image: url(Script/Calendar/cal.gif);
                                                    width: 16px;" type="button" name="popupLastUpdatedFirst" runat="server">
                                                to<asp:TextBox ID="txtLastUpdatedDate2" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox>
                                                <asp:RangeValidator ID="RangeValidator4" runat="server" ControlToValidate="txtLastUpdatedDate2"
                                                    Display="Dynamic" ErrorMessage="Last Update Date Last must between 1-1-1753 and 31-12-9999"
                                                    MaximumValue="31-12-9999" MinimumValue="1-1-1753" Type="Date">*</asp:RangeValidator>
                                                <input id="popupLastUpdatedLast" title="Click to show calendar" style="border-right: #ffffff 0px solid;
                                                    border-top: #ffffff 0px solid; font-size: 11px; border-left: #ffffff 0px solid;
                                                    border-bottom: #ffffff 0px solid; height: 17px; background-image: url(Script/Calendar/cal.gif);
                                                    width: 16px;" type="button" name="popupLastUpdatedLast" runat="server">
                                            </td>
                                            <td>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td nowrap="nowrap" style="height: 26px">
                                                Case Status</td>
                                            <td nowrap="nowrap" style="height: 26px">
                                                :</td>
                                            <td nowrap="nowrap" style="height: 26px">
                                                <asp:TextBox ID="txtCaseStatus" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox></td>
                                            <td>
                                            </td>
                                            <td nowrap="nowrap" style="height: 26px">
                                                PIC</td>
                                            <td nowrap="nowrap" style="height: 26px">
                                                :</td>
                                            <td nowrap="nowrap" style="height: 26px; width: 332px;">
                                                <asp:TextBox ID="txtPIC" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox></td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td nowrap="nowrap" style="height: 26px">
                                                Workflow Step</td>
                                            <td nowrap="nowrap" style="height: 26px">
                                                :</td>
                                            <td nowrap="nowrap" style="height: 26px">
                                                <asp:TextBox ID="txtWorkFlowStep" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox></td>
                                            <td>
                                            </td>
                                            <td nowrap="nowrap" style="height: 26px">
                                                Has Open Issue</td>
                                            <td nowrap="nowrap" style="height: 26px">
                                                :</td>
                                            <td nowrap="nowrap" style="height: 26px; width: 332px;">
                                                <asp:TextBox ID="txtHasOpenIssue" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox></td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td nowrap="nowrap" style="height: 26px">
                                                Account Owner Name</td>
                                            <td nowrap="nowrap" style="height: 26px">
                                                :</td>
                                            <td nowrap="nowrap" style="height: 26px">
                                                <asp:TextBox ID="txtAccountOwner" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox></td>
                                            <td>
                                            </td>
                                            <td nowrap="nowrap" style="height: 26px">
                                                Aging</td>
                                            <td nowrap="nowrap" style="height: 26px">
                                                :</td>
                                            <td nowrap="nowrap" style="height: 26px; width: 332px;">
                                                <asp:TextBox ID="txtAging" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox></td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td nowrap="nowrap" style="height: 26px">
                                                Last Proposed Action</td>
                                            <td nowrap="nowrap" style="height: 26px">
                                                :</td>
                                            <td nowrap="nowrap" style="height: 26px">
                                                <asp:TextBox ID="txtLastProposedAction" runat="server" CssClass="searcheditbox" TabIndex="2"></asp:TextBox></td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" height="10">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <asp:ImageButton ID="ImageButtonSearch" TabIndex="3" runat="server" SkinID="SearchButton"
                                                    ImageUrl="~/Images/button/search.gif"></asp:ImageButton>
                                                <asp:ImageButton ID="ImageClearSearch" runat="server" ImageUrl="~/Images/button/clearsearch.gif"  CausesValidation ="false"/></td>
                                        </tr>
                                    </table>
                                    &nbsp;&nbsp;&nbsp;
                                </ajax:ajaxpanel>
                            </td>
                            <td valign="middle" width="99%">
                                &nbsp;</td>
                        </tr>
                    </table>
                    <ajax:ajaxpanel id="AjaxPanel15" runat="server">
                        <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></ajax:ajaxpanel>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <table bgcolor="#dddddd" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
        width="100%">
        <tr>
            <td bgcolor="#ffffff">
                <ajax:ajaxpanel id="AjaxPanel4" runat="server">
                    <asp:DataGrid ID="GridMSUserView" runat="server" AutoGenerateColumns="False" Font-Size="XX-Small"
                        BackColor="White" CellPadding="4" BorderWidth="1px" BorderStyle="None" AllowPaging="True"
                        Width="100%" GridLines="Vertical" AllowSorting="True" BorderColor="#DEDFDE" ForeColor="Black">
                        <FooterStyle BackColor="#CCCC99"></FooterStyle>
                        <SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#CE5D5A"></SelectedItemStyle>
                        <AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
                        <ItemStyle BackColor="#F7F7DE"></ItemStyle>
                        <HeaderStyle Font-Size="XX-Small" Font-Bold="True" ForeColor="White" BackColor="#6B696B">
                        </HeaderStyle>
                        <Columns>
                            <asp:TemplateColumn>
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBoxExporttoExcel" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="PK_CaseManagementID" SortExpression="CaseManagement.PK_CaseManagementID  desc"
                                HeaderText="Case ID"></asp:BoundColumn>
                            <asp:BoundColumn DataField="CaseDescription" HeaderText="Case Description"
                                SortExpression="CaseManagement.CaseDescription  desc">
                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" Wrap="False" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CaseStatusDescription" HeaderText="Status"
                                SortExpression="CaseStatus.CaseStatusDescription  desc"></asp:BoundColumn>
                            <asp:BoundColumn DataField="WorkflowStep" SortExpression="CaseManagement.WorkflowStep  desc"
                                HeaderText="Workflow Step"></asp:BoundColumn>
                            <asp:BoundColumn DataField="CreatedDate" HeaderText="Created Date"
                                SortExpression="CaseManagement.CreatedDate  desc" DataFormatString="{0:dd-MM-yyyy}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="LastUpdated" HeaderText="Last Updated Date"
                                SortExpression="CaseManagement.LastUpdated  desc" DataFormatString="{0:dd-MM-yyyy}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="AccountOwnerName" HeaderText="Account Owner Name"
                                SortExpression="AccountOwner.AccountOwnerName desc"></asp:BoundColumn>
                            <asp:BoundColumn DataField="PIC" HeaderText="PIC" SortExpression="CaseManagement.PIC  desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="HasOpenIssue" HeaderText="HasOpenIssue"
                                SortExpression="casemanagement.HasOpenIssue desc"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Aging" HeaderText="Aging" SortExpression="casemanagement.Aging  desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="ProposedAction" HeaderText="Last Proposed Action"
                                SortExpression="CaseManagementProposedAction.ProposedAction desc"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Detail">
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkDetailCase" runat="server" CausesValidation="false" CommandName="Detail"  Text="Detail"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:EditCommandColumn EditText="Create Proposal STR" HeaderText="Create Proposal STR"></asp:EditCommandColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Right" ForeColor="Black" BackColor="#F7F7DE"
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                </ajax:ajaxpanel>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="background-color: #ffffff">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td nowrap="nowrap">
                            <ajax:ajaxpanel id="AjaxPanel5" runat="server">
                                &nbsp;<asp:CheckBox ID="CheckBoxSelectAll" runat="server" Text="Select All" AutoPostBack="True" />
                                &nbsp;
                            </ajax:ajaxpanel>
                        </td>
                        <td width="99%">
                            &nbsp;
                            <asp:LinkButton ID="LinkButtonExportExcel" runat="server">Export 
		        to Excel</asp:LinkButton>&nbsp;&nbsp;<asp:LinkButton id="LnkBtnExportAllToExcel" runat="server">Export All to Excel</asp:LinkButton></td>
                        <td align="right" nowrap="nowrap">
                            &nbsp;&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor="#ffffff">
                <table id="Table3" bgcolor="#ffffff" border="2" bordercolor="#ffffff" cellpadding="2"
                    cellspacing="1" width="100%">
                    <tr align="center" bgcolor="#dddddd" class="regtext">
                        <td align="left" bgcolor="#ffffff" valign="top" width="50%">
                            Page&nbsp;<ajax:ajaxpanel id="AjaxPanel6" runat="server">
                                <asp:Label ID="PageCurrentPage" runat="server" CssClass="regtext">0</asp:Label>
                                &nbsp;of&nbsp;
                                <asp:Label ID="PageTotalPages" runat="server" CssClass="regtext">0</asp:Label>
                            </ajax:ajaxpanel>
                        </td>
                        <td align="right" bgcolor="#ffffff" valign="top" width="50%">
                            Total Records&nbsp;
                            <ajax:ajaxpanel id="AjaxPanel7" runat="server">
                                <asp:Label ID="PageTotalRows" runat="server">0</asp:Label>
                            </ajax:ajaxpanel>
                        </td>
                    </tr>
                </table>
                <table id="Table4" bgcolor="#ffffff" border="2" bordercolor="#ffffff" cellpadding="2"
                    cellspacing="1" width="100%">
                    <tr bgcolor="#ffffff">
                        <td align="left" class="regtext" colspan="11" height="7" valign="middle">
                            <hr color="#f40101" noshade="noshade" size="1" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="63">
                            Go to page</td>
                        <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="5">
                            <font face="Verdana, Arial, Helvetica, sans-serif" size="1">
                                <ajax:ajaxpanel id="AjaxPanel8" runat="server">
                                    <asp:TextBox ID="TextGoToPage" runat="server" Width="38px" CssClass="searcheditbox"></asp:TextBox>
                                </ajax:ajaxpanel>
                            </font>
                        </td>
                        <td align="left" bgcolor="#ffffff" class="regtext" valign="middle">
                            <ajax:ajaxpanel id="AjaxPanel9" runat="server">
                                <asp:ImageButton ID="ImageButtonGo" runat="server" SkinID="GoButton"></asp:ImageButton>
                            </ajax:ajaxpanel>
                        </td>
                        <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="6">
                            <img height="5" src="images/first.gif" width="6" />
                        </td>
                        <td align="right" bgcolor="#ffffff" class="regtext" valign="middle" width="6">
                            <ajax:ajaxpanel id="AjaxPanel10" runat="server">
                                <asp:LinkButton ID="LinkButtonFirst" runat="server" CssClass="regtext" CommandName="First"
                                    OnCommand="PageNavigate">First</asp:LinkButton>
                            </ajax:ajaxpanel>
                        </td>
                        <td align="right" bgcolor="#ffffff" class="regtext" valign="middle" width="6">
                            <img height="5" src="images/prev.gif" width="6" /></td>
                        <td align="right" bgcolor="#ffffff" class="regtext" valign="middle" width="14">
                            <ajax:ajaxpanel id="AjaxPanel11" runat="server">
                                <asp:LinkButton ID="LinkButtonPrevious" runat="server" CssClass="regtext" CommandName="Prev"
                                    OnCommand="PageNavigate">Previous</asp:LinkButton>
                            </ajax:ajaxpanel>
                        </td>
                        <td align="right" bgcolor="#ffffff" class="regtext" valign="middle" width="60">
                            <ajax:ajaxpanel id="AjaxPanel12" runat="server">
                                <a class="pageNav" href="#">
                                    <asp:LinkButton ID="LinkButtonNext" runat="server" CssClass="regtext" CommandName="Next"
                                        OnCommand="PageNavigate">Next</asp:LinkButton>
                                </a>
                            </ajax:ajaxpanel>
                        </td>
                        <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="6">
                            <img height="5" src="images/next.gif" width="6" /></td>
                        <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="25">
                            <ajax:ajaxpanel id="AjaxPanel13" runat="server">
                                <asp:LinkButton ID="LinkButtonLast" runat="server" CssClass="regtext" CommandName="Last"
                                    OnCommand="PageNavigate">Last</asp:LinkButton>
                            </ajax:ajaxpanel>
                        </td>
                        <td align="left" bgcolor="#ffffff" class="regtext" valign="middle" width="6">
                            <img height="5" src="images/last.gif" width="6" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

