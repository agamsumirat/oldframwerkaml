﻿Option Explicit On
Option Strict On

Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports AMLBLL

Partial Class TransactionPenghasilanApprovalDetail
    Inherits Parent

    Public ReadOnly Property PKTransactionPenghasilanApprovalID() As Integer
        Get
            If Session("TransactionPenghasilanApprovalDetail.PK") Is Nothing Then
                Dim StrTmppkTransactionPenghasilanApprovalId As String

                StrTmppkTransactionPenghasilanApprovalId = Request.Params("PKTransactionPenghasilanApprovalID")
                If Integer.TryParse(StrTmppkTransactionPenghasilanApprovalId, 0) Then
                    Session("TransactionPenghasilanApprovalDetail.PK") = StrTmppkTransactionPenghasilanApprovalId
                Else
                    Throw New SahassaException("Parameter PKTransactionPenghasilanApprovalID is not valid.")
                End If

                Return CInt(Session("TransactionPenghasilanApprovalDetail.PK"))
            Else
                Return CInt(Session("TransactionPenghasilanApprovalDetail.PK"))
            End If
        End Get
    End Property

    Public ReadOnly Property ObjTransactionPenghasilanApproval() As TList(Of TransactionPenghasilan_Approval)
        Get
            Dim PkApprovalid As Integer
            Try
                If Session("TransactionPenghasilanApprovalDetail.ObjTransactionPenghasilanApproval") Is Nothing Then
                    PkApprovalid = PKTransactionPenghasilanApprovalID
                    Session("TransactionPenghasilanApprovalDetail.ObjTransactionPenghasilanApproval") = DataRepository.TransactionPenghasilan_ApprovalProvider.GetPaged("PK_TransactionPenghasilan_ApprovalID = " & PkApprovalid, "", 0, Integer.MaxValue, 0)
                End If
                Return CType(Session("TransactionPenghasilanApprovalDetail.ObjTransactionPenghasilanApproval"), TList(Of TransactionPenghasilan_Approval))
            Finally
            End Try
        End Get
    End Property

    Private Sub LoadDataApproval()
        If ObjTransactionPenghasilanApproval.Count > 0 Then
            If ObjTransactionPenghasilanApproval(0).FK_ModeID.GetValueOrDefault(0) = 2 Then
                Me.LblAction.Text = "Edit"
            End If

            Dim strRequestBy As String = ""
            Using objRequestBy As User = SahassaNettier.Data.DataRepository.UserProvider.GetByPkUserID(ObjTransactionPenghasilanApproval(0).FK_MsUserID.GetValueOrDefault(0))
                If Not objRequestBy Is Nothing Then
                    strRequestBy = objRequestBy.UserID & " ( " & objRequestBy.UserName & " ) "
                End If
            End Using

            LblRequestBy.Text = strRequestBy
            LblRequestDate.Text = ObjTransactionPenghasilanApproval(0).CreatedDate.GetValueOrDefault().ToString("dd-MM-yyyy")

            If ObjTransactionPenghasilanApproval(0).FK_ModeID.GetValueOrDefault(0) = 2 Then
                'Old

                Me.GrdVwTransactionPenghasilanOld.DataSource = DataRepository.TransactionPenghasilanProvider.GetPaged("", "Tier", 0, Integer.MaxValue, 0)
                Me.GrdVwTransactionPenghasilanOld.DataBind()

                'New
                Me.GrdVwTransactionPenghasilanNew.DataSource = DataRepository.TransactionPenghasilan_ApprovalDetailProvider.GetPaged("", "Tier", 0, Integer.MaxValue, 0)
                Me.GrdVwTransactionPenghasilanNew.DataBind()
            End If
        Else
            'kalau ternyata sudah di approve /reject dan memakai tombol back, maka redirect ke viewapproval
            Response.Redirect("TransactionPenghasilan_Approval.aspx", False)
        End If
    End Sub

    Private Sub ClearSession()
        Session("TransactionPenghasilanApprovalDetail.PK") = Nothing
        Session("TransactionPenghasilanApprovalDetail.ObjTransactionPenghasilanApproval") = Nothing
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                ClearSession()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using

                LoadDataApproval()
            End If
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImgBtnAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAccept.Click
        Try
            If ObjTransactionPenghasilanApproval.Count > 0 Then
                Using ObjTransactionPenghasilanBll As New TransactionPenghasilanBLL
                    If ObjTransactionPenghasilanBll.AcceptEdit Then
                        Response.Redirect("TransactionPenghasilan_Approval.aspx", False)
                    End If
                End Using
            Else
                Response.Redirect("TransactionPenghasilan_Approval.aspx", False)
            End If

        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBtnReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnReject.Click
        Try
            If ObjTransactionPenghasilanApproval.Count > 0 Then
                Using ObjTransactionPenghasilanBll As New TransactionPenghasilanBLL
                    If ObjTransactionPenghasilanBll.RejectEdit Then
                        Response.Redirect("TransactionPenghasilan_Approval.aspx", False)
                    End If
                End Using
            Else
                Response.Redirect("TransactionPenghasilan_Approval.aspx", False)
            End If
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBackAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBackAdd.Click
        Try
            Response.Redirect("TransactionPenghasilan_Approval.aspx", False)
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    'Protected Sub CvalHandleErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles CvalHandleErr.PreRender
    '    If CvalHandleErr.IsValid = False Then
    '        Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
    '    End If
    'End Sub

    'Protected Sub CvalPageErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles CvalPageErr.PreRender
    '    If CvalPageErr.IsValid = False Then
    '        Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
    '    End If
    'End Sub
End Class