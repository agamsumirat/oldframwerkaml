<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="CustomerVerificationJudgementWebAPIView.aspx.vb" Inherits="CustomerVerificationJudgementWebAPIView" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="a" ContentPlaceHolderID="cpContent" runat="server">
     <table id="title"  border="2" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr>

            <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Customer Verification Judgement Web API
                </strong>
            </td>
        </tr>
    </table>
    <table id="SearchBar" runat="server" borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2">
        <tr>
            <td align="left" bgColor="#eeeeee"><IMG height="15" src="images/arrow.gif" width="15"></td>
				<td vAlign="top" width="98%" bgColor="#ffffff"><table cellSpacing="4" cellPadding="0" width="100%" border="0">
						<tr>
							<td class="Regtext" noWrap>Search By :
							</td>
							<td vAlign="middle" noWrap><ajax:ajaxpanel id="AjaxPanel1" runat="server">
                                <asp:dropdownlist id="ComboSearch" tabIndex="1" runat="server" Width="120px" CssClass="searcheditcbo" ></asp:dropdownlist>&nbsp; 
                                <asp:textbox id="TextSearch" tabIndex="2" runat="server" CssClass="searcheditbox"></asp:textbox>
                                </ajax:ajaxpanel></td>
							<td vAlign="middle" width="99%"><ajax:ajaxpanel id="AjaxPanel2" runat="server">
									<asp:imagebutton id="ImageButtonSearch" tabIndex="3" runat="server" SkinID="SearchButton"></asp:imagebutton></ajax:ajaxpanel>
								</td>
						</tr>
					</table><ajax:ajaxpanel id="AjaxPanel3" runat="server">
                    <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></ajax:ajaxpanel></td>
			</tr>
	</table>
	<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2">
		<tr>
			<td bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel4" runat="server">
					<asp:datagrid id="GridMSUserView" runat="server" AutoGenerateColumns="False"
						Font-Size="XX-Small" BackColor="White" CellPadding="4" BorderWidth="1px" BorderStyle="None"
						AllowPaging="True" width="100%" GridLines="Vertical" AllowSorting="True" BorderColor="#DEDFDE"
						ForeColor="Black" >
						<FooterStyle BackColor="#CCCC99"></FooterStyle>
						<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#CE5D5A"></SelectedItemStyle>
						<AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
						<ItemStyle BackColor="#F7F7DE"></ItemStyle>
						<HeaderStyle Font-Size="XX-Small" Font-Bold="True" ForeColor="White" BackColor="#6B696B"></HeaderStyle>
						<Columns>
                            <asp:TemplateColumn>
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBoxExporttoExcel" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateColumn>						
                            <asp:BoundColumn DataField="CIFNo" HeaderText="CIF No" SortExpression="CIFNo desc"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Name" HeaderText="Name" SortExpression="Name desc"></asp:BoundColumn>
                            <asp:BoundColumn DataField="DOB" HeaderText="Date of Birth" DataFormatString="{0:dd-MMM-yyyy}" SortExpression="DOB desc"></asp:BoundColumn>
                            <%--<asp:BoundColumn DataField="RequestDate" HeaderText="Request Date" SortExpression="RequestDate desc"></asp:BoundColumn>--%>
                            <%--<asp:BoundColumn DataField="AccountOwnerID" HeaderText="AccountOwnerID" SortExpression="AccountOwnerID  desc" Visible="False"></asp:BoundColumn>--%>
                            
                            
                            <asp:EditCommandColumn EditText="Detail"></asp:EditCommandColumn>
						</Columns>
						<PagerStyle Visible="False" HorizontalAlign="Right" ForeColor="Black" BackColor="#F7F7DE" Mode="NumericPages"></PagerStyle>
					</asp:datagrid></ajax:ajaxpanel>
				</td>
		</tr>
		<tr>
		    <td style="background-color:#ffffff">
		    <table cellpadding="0" cellspacing="0" border="0" width="100%">
		        <tr>
			<td nowrap><ajax:ajaxpanel id="AjaxPanel5" runat="server">&nbsp;<asp:CheckBox ID="CheckBoxSelectAll" runat="server" Text="Select All" AutoPostBack="True" />
			  &nbsp; </ajax:ajaxpanel> </td>
			 <td width="99%">&nbsp;&nbsp;<asp:LinkButton ID="LinkButtonExportExcel" runat="server">Export 
		        to Excel</asp:LinkButton></td>
			<td align="right" nowrap>&nbsp;&nbsp;</td>
		    </tr>
      </table>
                </td>
		</tr>
		<tr>
			<td bgColor="#ffffff">
				<table id="Table3" borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#ffffff"
					border="2">
					<tr class="regtext" align="center" bgColor="#dddddd">
						<td vAlign="top" align="left" width="50%" bgColor="#ffffff">Page&nbsp;<ajax:ajaxpanel id="AjaxPanel6" runat="server"><asp:label id="PageCurrentPage" runat="server" CssClass="regtext">0</asp:label>&nbsp;of&nbsp;
							<asp:label id="PageTotalPages" runat="server" CssClass="regtext">0</asp:label></ajax:ajaxpanel></td>
						<td vAlign="top" align="right" width="50%" bgColor="#ffffff">Total Records&nbsp;
							<ajax:ajaxpanel id="AjaxPanel7" runat="server"><asp:label id="PageTotalRows" runat="server">0</asp:label></ajax:ajaxpanel></td>
					</tr>
				</table>
				<table id="Table4" borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#ffffff"
					border="2">
					<tr bgColor="#ffffff">
						<td class="regtext" vAlign="middle" align="left" colSpan="11" height="7">
							<hr color="#f40101" noShade size="1">
						</td>
					</tr>
					<tr>
						<td class="regtext" vAlign="middle" align="left" width="63" bgColor="#ffffff">Go to 
							page</td>
						<td class="regtext" vAlign="middle" align="left" width="5" bgColor="#ffffff"><font face="Verdana, Arial, Helvetica, sans-serif" size="1">
									<ajax:ajaxpanel id="AjaxPanel8" runat="server"><asp:textbox id="TextGoToPage" runat="server" Width="38px" CssClass="searcheditbox"></asp:textbox></ajax:ajaxpanel>
								</font></td>
						<td class="regtext" vAlign="middle" align="left" bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel9" runat="server">
								<asp:imagebutton id="ImageButtonGo" runat="server" SkinID="GoButton"></asp:imagebutton></ajax:ajaxpanel>
							</td>
						<td class="regtext" vAlign="middle" align="left" width="6" bgColor="#ffffff"><IMG height="5" src="images/first.gif" width="6">
						</td>
						<td class="regtext" vAlign="middle" align="right" width="6" bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel10" runat="server">
								<asp:linkbutton id="LinkButtonFirst" runat="server" CssClass="regtext" CommandName="First" OnCommand="PageNavigate">First</asp:linkbutton></ajax:ajaxpanel>
							</td>
						<td class="regtext" vAlign="middle" align="right" width="6" bgColor="#ffffff"><IMG height="5" src="images/prev.gif" width="6"></td>
						<td class="regtext" vAlign="middle" align="right" width="14" bgColor="#ffffff">
						<ajax:ajaxpanel id="AjaxPanel11" runat="server">
								<asp:linkbutton id="LinkButtonPrevious" runat="server" CssClass="regtext" CommandName="Prev" OnCommand="PageNavigate">Previous</asp:linkbutton></ajax:ajaxpanel>
							</td>
						<td class="regtext" vAlign="middle" align="right" width="60" bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel12" runat="server"><A class="pageNav" href="#">
									<asp:linkbutton id="LinkButtonNext" runat="server" CssClass="regtext" CommandName="Next" OnCommand="PageNavigate">Next</asp:linkbutton></A></ajax:ajaxpanel></td>
						<td class="regtext" vAlign="middle" align="left" width="6" bgColor="#ffffff"><IMG height="5" src="images/next.gif" width="6"></td>
						<td class="regtext" vAlign="middle" align="left" width="25" bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel13" runat="server">
								<asp:linkbutton id="LinkButtonLast" runat="server" CssClass="regtext" CommandName="Last" OnCommand="PageNavigate">Last</asp:linkbutton></ajax:ajaxpanel>
							</td>
						<td class="regtext" vAlign="middle" align="left" width="6" bgColor="#ffffff"><IMG height="5" src="images/last.gif" width="6"></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</asp:Content>
