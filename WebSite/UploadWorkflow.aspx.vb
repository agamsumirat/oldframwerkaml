
Partial Class UploadWorkflow
    Inherits Parent

    Protected Sub LnkDownloadTemplate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LnkDownloadTemplate.Click

        Try

            Dim fileContents As Byte()
            fileContents = My.Computer.FileSystem.ReadAllBytes(Server.MapPath("~\template\WorkflowTemplate.xls"))


            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=TemplateWorkFlow.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Me.EnableViewState = False
            Response.ContentType = "application/vnd.xls"
            Response.BinaryWrite(fileContents)
            Response.End()

        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImgUpload_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgUpload.Click
        Try

            If FileUpload1.HasFile Then

                Dim strTempfile As String = System.IO.Path.GetTempFileName
                Dim strxlsfile As String = IO.Path.ChangeExtension(strTempfile, ".xls")
                FileUpload1.SaveAs(strxlsfile)
                Using objwt As New AMLBLL.WorkFlowTemplateBll
                    objwt.ProcessExcel(strxlsfile)
                    MultiView1.ActiveViewIndex = 1
                    If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                        LblMessage.Text = "Data Workflow Saved."
                    Else
                        LblMessage.Text = "Data Workflow Saved into Pending Approval."
                    End If
                End Using

            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBack.Click

        Try

            MultiView1.ActiveViewIndex = 0
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub
End Class
