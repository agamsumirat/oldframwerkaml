Imports AMLBLL.CDDLNPBLL

Partial Class PopUpPotentialCustomerVerification
    Inherits Parent

    Public Property FK_AML_Screening_Customer_Request_Id() As String
        Get
            Return Session("FK_AML_Screening_Customer_Request_Id")
        End Get
        Set(ByVal value As String)
            Session("FK_AML_Screening_Customer_Request_Id") = value
        End Set
    End Property

    Protected Sub ImageSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSearch.Click
        Try
            
            If Page.IsValid Then
                Dim dob As Nullable(Of Date) = Nothing

                If String.IsNullOrEmpty(TxtName.Text.Trim) Then
                    Throw New Exception("Alias is required. At least one alias name.")
                End If

                If TxtDOB.Text.Trim.Length > 0 Then
                    dob = Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", TxtDOB.Text.Trim)
                End If

                FK_AML_Screening_Customer_Request_Id = Nothing
                FK_AML_Screening_Customer_Request_Id = EKABLL.ScreeningBLL.PotentialScreeningSatuan(TxtName.Text.Trim, dob, TxtNationality.Text.Trim).ToString()

                'Dim PotentialCustomerVerification_SearchId As Integer
                'PotentialCustomerVerification_SearchId = ProcessPotentialCustomerVerification()
                'Session("DOB") = Me.TextDOB.Text.Trim
                If Not String.IsNullOrEmpty(FK_AML_Screening_Customer_Request_Id) Then
                    Sahassa.AML.Commonly.SessionIntendedPage = "PopUpPotentialCustomerVerificationResult.aspx?ScreeningCustomerRequestId=" & FK_AML_Screening_Customer_Request_Id
                    Me.Response.Redirect(Sahassa.AML.Commonly.SessionIntendedPage)
                End If

            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Me.IsPostBack Then
            Me.popUp.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TxtDOB.ClientID & "'), 'dd-mmm-yyyy')")
            Me.popUp.Style.Add("display", "")

            Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)                
            End Using
        End If

    End Sub
End Class
