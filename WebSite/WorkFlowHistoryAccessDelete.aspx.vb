﻿Imports SahassaNettier.Entities
Imports AMLBLL
Partial Class WorkFlowHistoryAccessDelete
    Inherits Parent

    Public ReadOnly Property objPkEditID() As Integer
        Get
            Dim strtemp As String = Request.Params("PKEditID")
            Dim intresult As Integer
            If Integer.TryParse(strtemp, intresult) Then
                Return intresult
            Else
                Throw New Exception("PKEdit Must Numeric")
            End If
        End Get
    End Property

    Sub LoadDAta()
        Using objvwWorkflowHistory As VList(Of vw_WorkflowHistoryAccess) = WorkFlowHistoryAccessBLL.GetVlistvw_WorkflowHistoryAccess(vw_WorkflowHistoryAccessColumn.PK_WorkflowHistoryAccess_ID.ToString & "=" & Me.objPkEditID, "", 0, Integer.MaxValue, 0)
            If objvwWorkflowHistory.Count > 0 Then
                LblHistoryID.Text = objvwWorkflowHistory(0).PK_WorkflowHistoryAccess_ID
                LblWorkflowStep.Text = objvwWorkflowHistory(0).WorkflowStep
                LblAccessViewOther.Text = objvwWorkflowHistory(0).AccessViewOtherWorkflow
            End If
        End Using
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try

            If Not IsPostBack Then

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    '    Transcope.Complete()
                End Using

                LoadDAta()
                'End Using
            End If
            
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageUpdate_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImageUpdate.Click
        Try
            If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                WorkFlowHistoryAccessBLL.DeleteBySU(Me.objPkEditID)
                LblSuccess.Text = "Data Workflow History Access Deleted"
                LblSuccess.Visible = True
            Else
                If Not WorkFlowHistoryAccessBLL.IsAlreadyExistInApproval(LblWorkflowStep.Text) Then
                    WorkFlowHistoryAccessBLL.SaveDeleteApproval(Me.objPkEditID)
                    LblSuccess.Text = "Data Workflow History Access Saved into Pending Approval"
                    LblSuccess.Visible = True
                Else
                    Throw New Exception("Data Workflow History Access Already Saved into Pending Approval.")
                End If
                
            End If


        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageCancel_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Try
            Response.Redirect("WorkFlowHistoryAccessView.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class
