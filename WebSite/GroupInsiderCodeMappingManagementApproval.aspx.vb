Partial Class GroupInsiderCodeMappingManagementApproval
    Inherits Parent
#Region " Property "
    ''' <summary>
    ''' selected item store
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("GroupInsiderCodeMappingManagementApprovalViewSelected") Is Nothing, New ArrayList, Session("GroupInsiderCodeMappingManagementApprovalViewSelected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("GroupInsiderCodeMappingManagementApprovalViewSelected") = value
        End Set
    End Property
    ''' <summary>
    ''' setnget search field
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetFieldSearch() As String
        Get
            Return IIf(Session("GroupInsiderCodeMappingManagementApprovalViewFieldSearch") Is Nothing, "", Session("GroupInsiderCodeMappingManagementApprovalViewFieldSearch"))
        End Get
        Set(ByVal Value As String)
            Session("GroupInsiderCodeMappingManagementApprovalViewFieldSearch") = Value
        End Set
    End Property
    ''' <summary>
    ''' search value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetValueSearch() As String
        Get
            Return IIf(Session("GroupInsiderCodeMappingManagementApprovalViewValueSearch") Is Nothing, "", Session("GroupInsiderCodeMappingManagementApprovalViewValueSearch"))
        End Get
        Set(ByVal Value As String)
            Session("GroupInsiderCodeMappingManagementApprovalViewValueSearch") = Value
        End Set
    End Property
    ''' <summary>
    ''' sort expresion
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("GroupInsiderCodeMappingManagementApprovalViewSort") Is Nothing, "PendingApprovalUserID  asc", Session("GroupInsiderCodeMappingManagementApprovalViewSort"))
        End Get
        Set(ByVal Value As String)
            Session("GroupInsiderCodeMappingManagementApprovalViewSort") = Value
        End Set
    End Property
    ''' <summary>
    ''' current page index
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("GroupInsiderCodeMappingManagementApprovalViewCurrentPage") Is Nothing, 0, Session("GroupInsiderCodeMappingManagementApprovalViewCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("GroupInsiderCodeMappingManagementApprovalViewCurrentPage") = Value
        End Set
    End Property
    ''' <summary>
    ''' total pages
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    ''' <summary>
    ''' row total
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("GroupInsiderCodeMappingManagementApprovalViewRowTotal") Is Nothing, 0, Session("GroupInsiderCodeMappingManagementApprovalViewRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("GroupInsiderCodeMappingManagementApprovalViewRowTotal") = Value
        End Set
    End Property
    ''' <summary>
    ''' save bind table
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetBindTable() As Data.DataTable
        Get
            Return IIf(Session("GroupInsiderCodeMappingManagementApprovalViewData") Is Nothing, New AMLDAL.AMLDataSet.GroupInsiderCodeMapping_PendingApprovalDataTable, Session("GroupInsiderCodeMappingManagementApprovalViewData"))
        End Get
        Set(ByVal value As Data.DataTable)
            Session("GroupInsiderCodeMappingManagementApprovalViewData") = value
        End Set
    End Property
#End Region

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' fill search
    ''' </summary>
    ''' <remarks>
    ''' Group Name
    ''' PendingApprovalUserID
    ''' PendingApprovalEntryDate
    ''' PendingApprovalModeID
    ''' </remarks>
    ''' <history>
    ''' Modification of similar function created on 05/06/2006 by [Ariwibawa]
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub FillSearch()
        Me.ComboSearch.Items.Add(New ListItem("...", ""))
        Me.ComboSearch.Items.Add(New ListItem("Group Name", "GroupName Like '%-=Search=-%'"))
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' grid edit command handler
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    '''     [Hendry]    07/06/2007 Modified 
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridMSUserView.EditCommand
        Dim GroupId As Int32
        Dim GroupName As String

        Try
            GroupId = e.Item.Cells(1).Text
            GroupName = e.Item.Cells(2).Text

            Sahassa.AML.Commonly.SessionIntendedPage = "GroupInsiderCodeMappingManagementApprovalDetail.aspx?GroupId=" & GroupId & "&GroupName=" & GroupName

            MagicAjax.AjaxCallHelper.Redirect("GroupInsiderCodeMappingManagementApprovalDetail.aspx?GroupId=" & GroupId & "&GroupName=" & GroupName)

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub ClearThisPageSessions()
        Session("GroupInsiderCodeMappingManagementApprovalViewSelected") = Nothing
        Session("GroupInsiderCodeMappingManagementApprovalViewFieldSearch") = Nothing
        Session("GroupInsiderCodeMappingManagementApprovalViewValueSearch") = Nothing
        Session("GroupInsiderCodeMappingManagementApprovalViewSort") = Nothing
        Session("GroupInsiderCodeMappingManagementApprovalViewCurrentPage") = Nothing
        Session("GroupInsiderCodeMappingManagementApprovalViewRowTotal") = Nothing
        Session("GroupInsiderCodeMappingManagementApprovalViewData") = Nothing
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' onclick="popUpCalendar(this, document.getElementById('TextSearch'), 'dd-mmm-yyyy')" 
    ''' </remarks>
    ''' <history>
    ''' Modification of similar function created on 05/06/2006 by [Ariwibawa]
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not Me.IsPostBack Then
                Me.ClearThisPageSessions()

                Me.ComboSearch.Attributes.Add("onchange", "javascript:CheckNoPopUp('" & Me.TextSearch.ClientID & "');")

                Using AccessGroupInsiderCodeMappingApproval As New AMLDAL.AMLDataSetTableAdapters.GroupInsiderCodeMapping_PendingApprovalTableAdapter
                    Me.SetnGetBindTable = AccessGroupInsiderCodeMappingApproval.GetGroupInsiderCodeMappingViewData(Sahassa.AML.Commonly.SessionUserId)
                End Using
                Me.FillSearch()
                Me.GridMSUserView.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' page navigate button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
            Me.CollectSelected()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' collect sub
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = gridRow.FindControl("CheckBoxExporttoExcel")
                Dim GroupId As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
                If ArrTarget.Contains(GroupId) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(GroupId)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(GroupId)
                End If
                Me.SetnGetSelectedItem = ArrTarget
            End If
        Next
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' bind grid
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Sub BindGrid()
        Try
            Dim Rows() As AMLDAL.AMLDataSet.GroupInsiderCodeMapping_PendingApprovalRow = Me.SetnGetBindTable.Select(Me.SetnGetFieldSearch.Replace("-=Search=-", Me.SetnGetValueSearch.Replace("'", "''")), Me.SetnGetSort)
            Me.GridMSUserView.DataSource = Rows
            Me.SetnGetRowTotal = Rows.Length
            Me.GridMSUserView.CurrentPageIndex = Me.SetnGetCurrentPage
            Me.GridMSUserView.VirtualItemCount = Me.SetnGetRowTotal
            Me.GridMSUserView.DataBind()
        Catch
            Throw
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' set navigate info
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.ComboSearch.SelectedValue = Me.SetnGetFieldSearch
            Me.TextSearch.Text = Me.SetnGetValueSearch
        Catch
            Throw
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' change sort expression
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridMSUserView.SortCommand
        Dim GridUser As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' image button search
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Try
            Me.CollectSelected()
            Me.SetnGetFieldSearch = Me.ComboSearch.SelectedValue
            If Me.ComboSearch.SelectedIndex = 0 Then
                Me.TextSearch.Text = ""
            End If
            Me.SetnGetValueSearch = Me.TextSearch.Text
            Me.SetnGetCurrentPage = 0
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get item bound
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridMSUserView.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Dim chkBox As CheckBox = e.Item.FindControl("CheckBoxExporttoExcel")
                chkBox.Checked = Me.SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' go button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	14/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            Me.CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) And (CInt(Me.TextGoToPage.Text) > 0) Then
                If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                    Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                Else
                    Throw New Exception("Page number must be less than or equal to the total page count.")
                End If
            Else
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' select all
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        'For Each gridRow As DataGridItem In Me.GridMSUserView.Items
        '    If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
        '        Dim GroupId As String = gridRow.Cells(1).Text
        '        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        '        If Me.SetnGetSelectedItem.Contains(GroupId) Then
        '            If Me.CheckBoxSelectAll.Checked Then
        '                ArrTarget.Add(GroupId)
        '            Else
        '                ArrTarget.Remove(GroupId)
        '            End If
        '        Else
        '            If Me.CheckBoxSelectAll.Checked Then
        '                ArrTarget.Add(GroupId)
        '            End If
        '        End If
        '        Me.SetnGetSelectedItem = ArrTarget
        '    End If
        'Next

        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(1).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItem.Contains(StrKey) = False Then
                        ArrTarget.Add(StrKey)
                    End If
                Else
                    If Me.SetnGetSelectedItem.Contains(StrKey) Then
                        ArrTarget.Remove(StrKey)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItem = ArrTarget
    End Sub

    ''' <summary>
    ''' prerender event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' bind selected item
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub BindSelected()
        Try
            Dim Rows As New ArrayList
            For Each IdPk As Int64 In Me.SetnGetSelectedItem
                Dim rowData() As AMLDAL.AMLDataSet.GroupInsiderCodeMapping_PendingApprovalRow = Me.SetnGetBindTable.Select("PendingApprovalID = " & IdPk)
                If rowData.Length > 0 Then
                    Rows.Add(rowData(0))
                End If
            Next
            Me.GridMSUserView.DataSource = Rows
            Me.GridMSUserView.AllowPaging = False
            Me.GridMSUserView.DataBind()

            'Sembunyikan kolom ke 0, 1, 7, 8 & 9 agar tidak ikut diekspor ke excel
            Me.GridMSUserView.Columns(0).Visible = False
            Me.GridMSUserView.Columns(1).Visible = False
            Me.GridMSUserView.Columns(7).Visible = False
            Me.GridMSUserView.Columns(8).Visible = False
            Me.GridMSUserView.Columns(9).Visible = False
        Catch
            Throw
        End Try
    End Sub

    ''' <summary>
    ''' clear all control except control
    ''' </summary>
    ''' <param name="control">excluded control</param>
    ''' <remarks></remarks>
    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub 'ClearControls

    ''' <summary>
    ''' export button 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Me.CollectSelected()
            Me.ExportToExcel()
            'Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"

            'Me.BindSelected()
            'Response.Clear()
            'Response.AddHeader("content-disposition", "attachment;filename=GroupInsiderCodeMappingManagementApproval.xls")
            'Response.Charset = ""
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            'Response.ContentType = "application/vnd.xls"
            'Me.EnableViewState = False
            'Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            'Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            'ClearControls(GridMSUserView)
            'GridMSUserView.RenderControl(htmlWrite)
            'Response.Write(strStyle)
            'Response.Write(stringWrite.ToString())
            'Response.End()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Public Sub ExportToExcel()
        Dim range As String = ""
        Dim xlApp As Excel.ApplicationClass
        Dim xlWorkbooks As Excel.Workbooks
        Dim xlWorkbook As Excel.Workbook
        Dim xlWorkSheets As Excel.Sheets
        Dim xlWorkSheet As Excel.Worksheet
        Dim xlRange As Excel.Range
        Dim xlCells As Excel.Range
        Dim xlFont As Excel.Font

        Try
            xlApp = New Excel.Application
            xlApp.DisplayAlerts = False
            xlApp.UserControl = False
            xlApp.Visible = False
            xlWorkbooks = xlApp.Workbooks
            xlWorkbook = xlWorkbooks.Add
            xlWorkSheets = xlWorkbook.Worksheets

            Dim ArrCollectGroupInsider As ArrayList = Me.SetnGetSelectedItem

            Try
                xlWorkSheet = xlWorkSheets.Add
                xlWorkSheet.Name = "GroupInsiderApproval"
                xlCells = xlWorkSheet.Cells

                range = "A1:C1"
                xlRange = xlWorkSheet.Range(range)
                xlRange.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter
                xlRange.Interior.Color = RGB(192, 0, 0)
                xlFont = xlRange.Font
                xlFont.Color = RGB(255, 255, 255)
                xlRange.Merge()
                xlRange.Value = "Group Insider Code Mapping Approval"
                NAR(xlFont)
                NAR(xlRange)

                ' Create Header Alias
                Dim HeaderUserWorkingUnit() As String = {"Group Name", "Added", "Deleted"}
                For i As Integer = 0 To 2
                    xlCells = xlWorkSheet.Cells
                    xlRange = xlCells(2, i + 1)
                    xlRange.Value = HeaderUserWorkingUnit(i)
                    xlFont = xlRange.Font
                    xlFont.Bold = True
                    NAR(xlFont)
                    NAR(xlRange)
                Next

                Dim IntCounter As Integer = 3

                Using AccessGroupInsider As New AMLDAL.GroupInsiderCodeMappingTableAdapters.SelectFilterGroupInsider_ApprovalTableAdapter
                    For i As Integer = 0 To ArrCollectGroupInsider.Count - 1
                        Using dtGroup As Data.DataTable = AccessGroupInsider.GetData(CInt(ArrCollectGroupInsider(i)))
                            If dtGroup.Rows.Count > 0 Then
                                Dim GroupInsiderAdded As String = ""
                                Dim GroupInsiderDeleted As String = ""
                                Dim GroupName As String = ""
                                For Each RowGroup As AMLDAL.GroupInsiderCodeMapping.SelectFilterGroupInsider_ApprovalRow In dtGroup.Rows
                                    If RowGroup.ModeID = 1 Then
                                        GroupInsiderAdded &= RowGroup.InsiderCodeId & ","
                                    ElseIf RowGroup.ModeID = 3 Then
                                        GroupInsiderDeleted &= RowGroup.InsiderCodeId & ","
                                    End If                                    
                                    GroupName = RowGroup.GroupName
                                Next
                                If GroupInsiderAdded <> "" Then
                                    GroupInsiderAdded = GroupInsiderAdded.Substring(0, GroupInsiderAdded.Length - 1)
                                End If
                                If GroupInsiderDeleted <> "" Then
                                    GroupInsiderDeleted = GroupInsiderDeleted.Substring(0, GroupInsiderDeleted.Length - 1)
                                End If
                                Dim StrValueGroupInsider() As String = {GroupName, GroupInsiderAdded, GroupInsiderDeleted}
                                For j As Integer = 0 To 2
                                    xlCells = xlWorkSheet.Cells
                                    xlRange = xlCells(IntCounter, j + 1)
                                    xlRange.Value = StrValueGroupInsider(j)
                                    NAR(xlRange)
                                Next
                                IntCounter += 1
                            End If
                        End Using
                    Next
                End Using


                xlWorkSheet.Columns.AutoFit()
            Catch
                Throw
            Finally
                NAR(xlWorkSheet)
            End Try

            xlWorkbook.SaveAs(Me.Server.MapPath("Temp") & "\" & Sahassa.AML.Commonly.SessionUserId & "GroupInsiderCodeMappingApproval.xls")


            NAR(xlRange)
            NAR(xlWorkSheet)
            NAR(xlWorkbook)
            NAR(xlWorkbooks)
            If Not (xlApp Is Nothing) Then
                xlApp.Quit()
            End If
            NAR(xlApp)
            'GC cleanup 
            System.GC.Collect()
            System.GC.WaitForPendingFinalizers()
            Me.Response.Redirect("Temp/" & Sahassa.AML.Commonly.SessionUserId & "GroupInsiderCodeMappingApproval.xls", False)
        Catch
            Throw
        Finally
            NAR(xlRange)
            NAR(xlWorkSheet)
            NAR(xlWorkbook)
            NAR(xlWorkbooks)
            If Not (xlApp Is Nothing) Then
                xlApp.Quit()
            End If
            NAR(xlApp)
            'GC cleanup 
            System.GC.Collect()
            System.GC.WaitForPendingFinalizers()
        End Try
    End Sub

    ''' <summary>
    ''' NAR
    ''' </summary>
    ''' <param name="obj"></param>
    ''' <remarks></remarks>
    Private Sub NAR(ByRef obj As Object)
        Dim intReferenceCount As Integer
        If Not obj Is Nothing Then
            Try
                Do
                    intReferenceCount = System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
                Loop While intReferenceCount > 0
            Catch
            Finally
                obj = Nothing
            End Try
        End If
    End Sub
End Class