Option Explicit On
Option Strict On

Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports AMLBLL

Partial Class ResikoNasabahExistingApprovalDetail
    Inherits Parent

    Public ReadOnly Property PKResikoNasabahExistingApprovalID() As Integer
        Get
            If Session("ResikoNasabahExistingApprovalDetail.PK") Is Nothing Then
                Dim StrTmppkResikoNasabahExistingApprovalId As String

                StrTmppkResikoNasabahExistingApprovalId = Request.Params("PKResikoNasabahExistingApprovalID")
                If Integer.TryParse(StrTmppkResikoNasabahExistingApprovalId, 0) Then
                    Session("ResikoNasabahExistingApprovalDetail.PK") = StrTmppkResikoNasabahExistingApprovalId
                Else
                    Throw New SahassaException("Parameter PKResikoNasabahExistingApprovalID is not valid.")
                End If

                Return CInt(Session("ResikoNasabahExistingApprovalDetail.PK"))
            Else
                Return CInt(Session("ResikoNasabahExistingApprovalDetail.PK"))
            End If
        End Get
    End Property

    Public ReadOnly Property ObjResikoNasabahExistingApproval() As TList(Of ResikoNasabahExisting_Approval)
        Get
            Dim PkApprovalid As Integer
            Try
                If Session("ResikoNasabahExistingApprovalDetail.ObjResikoNasabahExistingApproval") Is Nothing Then
                    PkApprovalid = PKResikoNasabahExistingApprovalID
                    Session("ResikoNasabahExistingApprovalDetail.ObjResikoNasabahExistingApproval") = DataRepository.ResikoNasabahExisting_ApprovalProvider.GetPaged("PK_ResikoNasabahExisting_ApprovalID = " & PkApprovalid, "", 0, Integer.MaxValue, 0)
                End If
                Return CType(Session("ResikoNasabahExistingApprovalDetail.ObjResikoNasabahExistingApproval"), TList(Of ResikoNasabahExisting_Approval))
            Finally
            End Try
        End Get
    End Property

    Private Sub LoadDataApproval()
        If ObjResikoNasabahExistingApproval.Count > 0 Then
            If ObjResikoNasabahExistingApproval(0).FK_ModeID.GetValueOrDefault(0) = 2 Then
                Me.LblAction.Text = "Edit"
            End If

            Dim strRequestBy As String = ""
            Using objRequestBy As User = SahassaNettier.Data.DataRepository.UserProvider.GetByPkUserID(ObjResikoNasabahExistingApproval(0).FK_MsUserID.GetValueOrDefault(0))
                If Not objRequestBy Is Nothing Then
                    strRequestBy = objRequestBy.UserID & " ( " & objRequestBy.UserName & " ) "
                End If
            End Using

            LblRequestBy.Text = strRequestBy
            LblRequestDate.Text = ObjResikoNasabahExistingApproval(0).CreatedDate.GetValueOrDefault().ToString("dd-MM-yyyy")

            If ObjResikoNasabahExistingApproval(0).FK_ModeID.GetValueOrDefault(0) = 2 Then
                'Old
                Me.GrdVwResikoNasabahExistingOld.DataSource = DataRepository.ResikoNasabahExistingProvider.GetPaged("", "PK_ResikoNasabahExisting_ID", 0, Integer.MaxValue, 0)
                Me.GrdVwResikoNasabahExistingOld.DataBind()

                'New
                Me.GrdVwResikoNasabahExistingNew.DataSource = DataRepository.ResikoNasabahExisting_ApprovalDetailProvider.GetPaged("", "PK_ResikoNasabahExisting_ApprovalDetail_ID", 0, Integer.MaxValue, 0)
                Me.GrdVwResikoNasabahExistingNew.DataBind()
            End If
        Else
            'kalau ternyata sudah di approve /reject dan memakai tombol back, maka redirect ke viewapproval
            Response.Redirect("ResikoNasabahExistingApproval.aspx", False)
        End If
    End Sub

    Private Sub ClearSession()
        Session("ResikoNasabahExistingApprovalDetail.PK") = Nothing
        Session("ResikoNasabahExistingApprovalDetail.ObjResikoNasabahExistingApproval") = Nothing
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                ClearSession()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using

                LoadDataApproval()
            End If
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImgBtnAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAccept.Click
        Try
            If ObjResikoNasabahExistingApproval.Count > 0 Then
                Using ObjResikoNasabahExistingBll As New ResikoNasabahExistingBLL
                    If ObjResikoNasabahExistingBll.AcceptEdit Then
                        Response.Redirect("ResikoNasabahExistingApproval.aspx", False)
                    End If
                End Using
            Else
                Response.Redirect("ResikoNasabahExistingApproval.aspx", False)
            End If

        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBtnReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnReject.Click
        Try
            If ObjResikoNasabahExistingApproval.Count > 0 Then
                Using ObjResikoNasabahExistingBll As New ResikoNasabahExistingBLL
                    If ObjResikoNasabahExistingBll.RejectEdit Then
                        Response.Redirect("ResikoNasabahExistingApproval.aspx", False)
                    End If
                End Using
            Else
                Response.Redirect("ResikoNasabahExistingApproval.aspx", False)
            End If
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBackAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBackAdd.Click
        Try
            Response.Redirect("ResikoNasabahExistingApproval.aspx", False)
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    'Protected Sub CvalHandleErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles CvalHandleErr.PreRender
    '    If CvalHandleErr.IsValid = False Then
    '        Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
    '    End If
    'End Sub

    'Protected Sub CvalPageErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles CvalPageErr.PreRender
    '    If CvalPageErr.IsValid = False Then
    '        Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
    '    End If
    'End Sub
End Class