<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AccountInformation.aspx.vb"
    Inherits="AccountInformation" MasterPageFile="~/masterpage.master" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
        height="72" style="border-top-style: none; border-right-style: none; border-left-style: none;
        border-bottom-style: none" width="100%">
        <tr>
            <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Account Information - View&nbsp; </strong>
            </td>
        </tr>
    </table>
    <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
        border="2">
        <tr>
            <td align="left" bgcolor="#eeeeee">
                <img height="15" src="images/arrow.gif" width="15"></td>
            <td valign="top" width="98%" bgcolor="#ffffff">
                <ajax:AjaxPanel ID="AjaxPanel3" runat="server">
                    <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></ajax:AjaxPanel>
                <ajax:AjaxPanel ID="AjaxPanel14" runat="server">
                    <table id="TblSimpleSearch" runat="server" cellspacing="4" cellpadding="0" width="100%"
                        border="0">
                        <tr>
                            <td class="Regtext" nowrap>
                                Search By :
                            </td>
                            <td valign="middle" nowrap>
                                <ajax:AjaxPanel ID="AjaxPanel1" runat="server">
                                    <asp:DropDownList ID="ComboSearch" TabIndex="1" runat="server" Width="120px" CssClass="searcheditcbo">
                                    </asp:DropDownList>&nbsp;
                                    <asp:TextBox ID="TextSearch" TabIndex="2" runat="server" CssClass="searcheditbox"></asp:TextBox>
                                    <input id="popUp" runat="server" name="popUpCalc" style="border-right: #ffffff 0px solid;
                                        border-top: #ffffff 0px solid; font-size: 11px; border-left: #ffffff 0px solid;
                                        border-bottom: #ffffff 0px solid; height: 17px; background-image: url(Script/Calendar/cal.gif);
                                        width: 16px;" title="Click to show calendar" type="button" /></ajax:AjaxPanel></td>
                            <td valign="middle" width="99%">
                                <ajax:AjaxPanel ID="AjaxPanel2" runat="server">
                                    <asp:ImageButton ID="ImageButtonSearch" TabIndex="3" runat="server" SkinID="SearchButton">
                                    </asp:ImageButton></ajax:AjaxPanel>
                            </td>
                        </tr>
                    </table>
                </ajax:AjaxPanel>
                <ajax:AjaxPanel ID="AjaxPanel15" runat="server">
                    <table id="TblAdvanceSearch" runat="server" style="width: 100%" align="left">
                        <tr align="left">
                            <td valign="top" width="1%" align="left">
                                <ajax:AjaxPanel ID="AjaxPanel16" runat="server">
                                    <asp:Panel ID="PanelAndOr" runat="server" Visible="False">
                                        <asp:DropDownList ID="CboAndOR" runat="server" CssClass="combobox">
                                            <asp:ListItem>AND</asp:ListItem>
                                            <asp:ListItem>OR</asp:ListItem>
                                        </asp:DropDownList></asp:Panel>
                                    &nbsp;</ajax:AjaxPanel>
                            </td>
                            <td valign="top" width="7%" align="left">
                                <ajax:AjaxPanel ID="AjaxPanel17" runat="server">
                                    <asp:DropDownList ID="CboFilter" runat="server" CssClass="combobox" AutoPostBack="True"
                                        Visible="False">
                                    </asp:DropDownList>&nbsp;
                                </ajax:AjaxPanel>
                            </td>
                            <td valign="top" width="3%" align="left">
                                <ajax:AjaxPanel ID="AjaxPanel18" runat="server">
                                    <asp:DropDownList ID="cboOperator" runat="server" CssClass="combobox" Visible="False"
                                        AutoPostBack="True">
                                    </asp:DropDownList>&nbsp;
                                </ajax:AjaxPanel>
                            </td>
                            <td valign="top" width="5%" align="left">
                                <ajax:AjaxPanel ID="AjaxPanel19" runat="server">
                                    <asp:Panel ID="PanelAwal" runat="server" Visible="False">
                                        <asp:TextBox ID="TxtFieldAwal" runat="server" CssClass="textbox" Width="100px"></asp:TextBox><input
                                            id="popUp1" runat="server" name="popUpCalc1" style="border-right: #ffffff 0px solid;
                                            border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
                                            border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
                                            height: 17px" title="Click to show calendar" type="button" /></asp:Panel>
                                    &nbsp;
                                </ajax:AjaxPanel>
                            </td>
                            <td valign="top" width="2%" align="left">
                                <ajax:AjaxPanel ID="AjaxPanel20" runat="server">
                                    <asp:Panel ID="PanelAnd" runat="server" Visible="False">
                                        <asp:Label ID="LblAnd" runat="server" Text="And"></asp:Label></asp:Panel>
                                    &nbsp;
                                </ajax:AjaxPanel>
                            </td>
                            <td valign="top" width="5%" align="left">
                                <ajax:AjaxPanel ID="AjaxPanel21" runat="server">
                                    <asp:Panel ID="PanelAkhir" runat="server" Visible="False">
                                        <asp:TextBox ID="TxtFieldAkhir" runat="server" CssClass="textbox" Width="100px"></asp:TextBox><input
                                            id="popUp2" runat="server" name="popUpCalc2" style="border-right: #ffffff 0px solid;
                                            border-top: #ffffff 0px solid; font-size: 11px; background-image: url(Script/Calendar/cal.gif);
                                            border-left: #ffffff 0px solid; width: 16px; border-bottom: #ffffff 0px solid;
                                            height: 17px" title="Click to show calendar" type="button" /></asp:Panel>
                                    &nbsp;
                                </ajax:AjaxPanel>
                            </td>
                            <td valign="top" width="99%" align="left">
                                <ajax:AjaxPanel ID="AjaxPanel22" runat="server">
                                    <asp:ImageButton ID="ImageAddFilter" runat="server" ImageUrl="~/images/button/add.gif"
                                        Visible="False" />
                                </ajax:AjaxPanel>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" width="100%" align="left" colspan="7">
                                <ajax:AjaxPanel ID="AjaxPanel23" runat="server">
                                    <asp:GridView ID="GridViewList" runat="server" AllowSorting="True" BackColor="White"
                                        BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black"
                                        EnableTheming="False" DataKeyNames="pk" AutoGenerateColumns="false">
                                        <RowStyle BackColor="#F7F7DE" VerticalAlign="Top" Wrap="False" />
                                        <FooterStyle BackColor="#CCCC99" />
                                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                        <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                        <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                                        <AlternatingRowStyle BackColor="White" />
                                        <Columns>
                                            <asp:BoundField DataField="AndOr" HeaderText="And Or" />
                                            <asp:BoundField DataField="columnName" HeaderText="ColumnName" />
                                            <asp:BoundField DataField="Operator" HeaderText="Operator" />
                                            <asp:BoundField DataField="nilai" HeaderText="Value" />
                                            <asp:CommandField ShowDeleteButton="True" />
                                        </Columns>
                                    </asp:GridView>
                                </ajax:AjaxPanel>
                            </td>
                        </tr>
                        <tr align="left">
                            <td valign="middle" width="99%" colspan="7">
                                <asp:ImageButton ID="ImgAdvanceSearch" TabIndex="3" runat="server" SkinID="SearchButton">
                                </asp:ImageButton>
                            </td>
                        </tr>
                    </table>
                </ajax:AjaxPanel>
            </td>
        </tr>
    </table>
    <ajax:AjaxPanel ID="AjaxPanel24" runat="server">
        <table id="TblSearchType" runat="server" visible="true" cellspacing="4" cellpadding="0"
            width="100%" border="0">
            <tr>
                <td class="Regtext" colspan="3" nowrap>
                    <asp:LinkButton ID="LnkSearchType" runat="server" Visible="true">Advance Search</asp:LinkButton>
                </td>
            </tr>
        </table>
    </ajax:AjaxPanel>
    <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
        border="2">
        <tr>
            <td bgcolor="#ffffff">
                <ajax:AjaxPanel ID="AjaxPanel4" runat="server">
                    <asp:DataGrid ID="GridAccountInformation" runat="server" AutoGenerateColumns="False"
                        Font-Size="XX-Small" BackColor="White" CellPadding="4" BorderWidth="1px" BorderStyle="None"
                        AllowPaging="True" Width="100%" GridLines="Vertical" AllowSorting="True" BorderColor="#DEDFDE"
                        ForeColor="Black">
                        <FooterStyle BackColor="#CCCC99"></FooterStyle>
                        <SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#CE5D5A"></SelectedItemStyle>
                        <AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
                        <ItemStyle BackColor="#F7F7DE"></ItemStyle>
                        <HeaderStyle Font-Size="XX-Small" Font-Bold="True" ForeColor="White" BackColor="#6B696B">
                        </HeaderStyle>
                        <Columns>
                            <asp:TemplateColumn>
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBoxExporttoExcel" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="Source" Visible="False"></asp:BoundColumn>
                            <asp:BoundColumn DataField="AccountOwner" HeaderText="Account Owner" SortExpression="(CAST(AllAccount_WebTempTable.AccountOwnerId AS VARCHAR) + ' - ' + AllAccount_WebTempTable.AccountOwnerName) desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CIFNo" SortExpression="CIFNo desc" HeaderText="CIF No"></asp:BoundColumn>
                            <asp:BoundColumn DataField="AccountNo" HeaderText="Account Number" SortExpression="AccountNo desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="AccountName" HeaderText="Name" SortExpression="AccountName desc">
                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" Wrap="False" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="AccountTypeDescription" HeaderText="Account Type" SortExpression="AccountTypeDescription desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="OpeningDate" HeaderText="Opening Date" SortExpression="OpeningDate desc"
                                DataFormatString="{0:dd-MMM-yyyy}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ProductDescription" HeaderText="Product" SortExpression="ProductDescription desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="AccountStatusDescription" SortExpression="AccountStatusDescription desc"
                                HeaderText="Status"></asp:BoundColumn>
                            <asp:EditCommandColumn EditText="Detail"></asp:EditCommandColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Right" ForeColor="Black" BackColor="#F7F7DE"
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                </ajax:AjaxPanel>
            </td>
        </tr>
        <tr>
            <td style="background-color: #ffffff">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td nowrap>
                            <ajax:AjaxPanel ID="AjaxPanel5" runat="server">
                                &nbsp;<asp:CheckBox ID="CheckBoxSelectAll" runat="server" Text="Select All" AutoPostBack="True" />
                                &nbsp;
                            </ajax:AjaxPanel>
                        </td>
                        <td width="99%">
                            &nbsp;&nbsp;<asp:LinkButton ID="LinkButtonExportExcel" runat="server">Export 
		        to Excel</asp:LinkButton>
                            <asp:LinkButton ID="LnkBtnExportAllToExcel" runat="server">Export All to Excel</asp:LinkButton></td>
                        <td align="right" nowrap>
                            &nbsp;&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor="#ffffff">
                <table id="Table3" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                    bgcolor="#ffffff" border="2">
                    <tr class="regtext" align="center" bgcolor="#dddddd">
                        <td valign="top" align="left" width="50%" bgcolor="#ffffff">
                            Page&nbsp;<ajax:AjaxPanel ID="AjaxPanel6" runat="server">
                                <asp:Label ID="PageCurrentPage" runat="server" CssClass="regtext">0</asp:Label>
                                &nbsp;of&nbsp;
                                <asp:Label ID="PageTotalPages" runat="server" CssClass="regtext">0</asp:Label>
                            </ajax:AjaxPanel>
                        </td>
                        <td valign="top" align="right" width="50%" bgcolor="#ffffff">
                            Total Records&nbsp;
                            <ajax:AjaxPanel ID="AjaxPanel7" runat="server">
                                <asp:Label ID="PageTotalRows" runat="server">0</asp:Label>
                            </ajax:AjaxPanel>
                        </td>
                    </tr>
                </table>
                <table id="Table4" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                    bgcolor="#ffffff" border="2">
                    <tr bgcolor="#ffffff">
                        <td class="regtext" valign="middle" align="left" colspan="11" height="7">
                            <hr color="#f40101" noshade size="1">
                        </td>
                    </tr>
                    <tr>
                        <td class="regtext" valign="middle" align="left" width="63" bgcolor="#ffffff">
                            Go to page</td>
                        <td class="regtext" valign="middle" align="left" width="5" bgcolor="#ffffff">
                            <font face="Verdana, Arial, Helvetica, sans-serif" size="1">
                                <ajax:AjaxPanel ID="AjaxPanel8" runat="server">
                                    <asp:TextBox ID="TextGoToPage" runat="server" Width="38px" CssClass="searcheditbox"></asp:TextBox>
                                </ajax:AjaxPanel>
                            </font>
                        </td>
                        <td class="regtext" valign="middle" align="left" bgcolor="#ffffff">
                            <ajax:AjaxPanel ID="AjaxPanel9" runat="server">
                                <asp:ImageButton ID="ImageButtonGo" runat="server" SkinID="GoButton"></asp:ImageButton>
                            </ajax:AjaxPanel>
                        </td>
                        <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                            <img height="5" src="images/first.gif" width="6">
                        </td>
                        <td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff">
                            <ajax:AjaxPanel ID="AjaxPanel10" runat="server">
                                <asp:LinkButton ID="LinkButtonFirst" runat="server" CssClass="regtext" CommandName="First"
                                    OnCommand="PageNavigate">First</asp:LinkButton>
                            </ajax:AjaxPanel>
                        </td>
                        <td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff">
                            <img height="5" src="images/prev.gif" width="6"></td>
                        <td class="regtext" valign="middle" align="right" width="14" bgcolor="#ffffff">
                            <ajax:AjaxPanel ID="AjaxPanel11" runat="server">
                                <asp:LinkButton ID="LinkButtonPrevious" runat="server" CssClass="regtext" CommandName="Prev"
                                    OnCommand="PageNavigate">Previous</asp:LinkButton>
                            </ajax:AjaxPanel>
                        </td>
                        <td class="regtext" valign="middle" align="right" width="60" bgcolor="#ffffff">
                            <ajax:AjaxPanel ID="AjaxPanel12" runat="server">
                                <a class="pageNav" href="#">
                                    <asp:LinkButton ID="LinkButtonNext" runat="server" CssClass="regtext" CommandName="Next"
                                        OnCommand="PageNavigate">Next</asp:LinkButton>
                                </a>
                            </ajax:AjaxPanel>
                        </td>
                        <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                            <img height="5" src="images/next.gif" width="6"></td>
                        <td class="regtext" valign="middle" align="left" width="25" bgcolor="#ffffff">
                            <ajax:AjaxPanel ID="AjaxPanel13" runat="server">
                                <asp:LinkButton ID="LinkButtonLast" runat="server" CssClass="regtext" CommandName="Last"
                                    OnCommand="PageNavigate">Last</asp:LinkButton>
                            </ajax:AjaxPanel>
                        </td>
                        <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                            <img height="5" src="images/last.gif" width="6"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
