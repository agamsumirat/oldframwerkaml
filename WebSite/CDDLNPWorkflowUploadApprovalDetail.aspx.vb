Imports CDDLNPNettier.Data
Imports CDDLNPNettier.Entities
Imports AMLBLL
Imports System.Collections.Generic
Imports System.IO

Partial Class CDDLNPWorkflowUploadApprovalDetail
    Inherits Parent
    Private BindGridFromExcel As Boolean = False

#Region " Property "
    Private ReadOnly Property GetPK_CDDLNP_WorkflowUpload_Approval_Id() As String
        Get
            Return IIf(Request.QueryString("ID") = String.Empty, 0, Request.QueryString("ID"))
        End Get
    End Property

    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("CDDLNPWorkflowUpload.Sort") Is Nothing, "PK_CDDLNP_WorkflowUpload_ApprovalDetail_id  asc", Session("CDDLNPWorkflowUpload.Sort"))
        End Get
        Set(ByVal Value As String)
            Session("CDDLNPWorkflowUpload.Sort") = Value
        End Set
    End Property
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("CDDLNPWorkflowUpload.CurrentPage") Is Nothing, 0, Session("CDDLNPWorkflowUpload.CurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("CDDLNPWorkflowUpload.CurrentPage") = Value
        End Set
    End Property
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("CDDLNPWorkflowUpload.RowTotal") Is Nothing, 0, Session("CDDLNPWorkflowUpload.RowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("CDDLNPWorkflowUpload.RowTotal") = Value
        End Set
    End Property
    Private Function SetnGetBindTable() As TList(Of CDDLNP_WorkflowUpload_ApprovalDetail)
        Return DataRepository.CDDLNP_WorkflowUpload_ApprovalDetailProvider.GetPaged("FK_CDDLNP_WorkflowUpload_Approval_Id = " & Me.GetPK_CDDLNP_WorkflowUpload_Approval_Id, SetnGetSort, SetnGetCurrentPage, Sahassa.AML.Commonly.GetDisplayedTotalRow, SetnGetRowTotal)
    End Function
    Private Function SetnGetBindTableUserReassignment() As TList(Of CDDLNP_UserAssignment)
        Return DataRepository.CDDLNP_UserAssignmentProvider.GetPaged("FK_CDDLNP_WorkflowUpload_Approval_Id = " & Me.GetPK_CDDLNP_WorkflowUpload_Approval_Id, String.Empty, 0, Int32.MaxValue, 0)
    End Function

#End Region

    Private Sub ClearThisPageSessions()
        Me.SetnGetSort = Nothing
        Me.SetnGetCurrentPage = Nothing
        Me.SetnGetRowTotal = Nothing
    End Sub

    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknown Commandname " & e.CommandName)
            End Select            
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
        Catch
            Throw
        End Try
    End Sub

    Protected Sub GridViewCDDLNP_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridViewCDDLNP.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                If BindGridFromExcel = True Then
                    e.Item.Cells(2).Text = CStr((e.Item.ItemIndex + 1))
                Else
                    e.Item.Cells(2).Text = CStr((e.Item.ItemIndex + 1) + (Me.SetnGetCurrentPage * Sahassa.AML.Commonly.GetDisplayedTotalRow))
                End If
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub GridViewCDDLNP_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridViewCDDLNP.SortCommand
        Dim GridUser As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub BindGrid()
        Me.GridViewCDDLNP.DataSource = Me.SetnGetBindTable
        Me.GridViewCDDLNP.VirtualItemCount = Me.SetnGetRowTotal
        Me.GridViewCDDLNP.DataBind()

        Me.GridReassignment.DataSource = SetnGetBindTableUserReassignment()
        Me.GridReassignment.DataBind()

        Me.rowCDDLNPReassignment.Visible = (SetnGetBindTableUserReassignment.Count > 0)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                Using objApproval As VList(Of vw_CDDLNP_WorkflowUpload_Approval) = DataRepository.vw_CDDLNP_WorkflowUpload_ApprovalProvider.GetPaged("PK_CDDLNP_WorkflowUpload_Approval_Id = " & Me.GetPK_CDDLNP_WorkflowUpload_Approval_Id, String.Empty, 0, Int32.MaxValue, 0)
                    If objApproval.Count > 0 Then
                        lblRequestedBy.Text = objApproval(0).UserName
                        lblRequestedDate.Text = objApproval(0).RequestedDate.GetValueOrDefault.ToString("dd-MMM-yyyy hh:mm")

                        Me.ClearThisPageSessions()

                        Me.GridViewCDDLNP.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow

                        Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                            Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                            AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                        End Using
                    Else
                        Me.Response.Redirect("CDDLNPWorkflowUploadApproval.aspx", False)
                    End If
                End Using
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Me.Response.Redirect("CDDLNPWorkflowUploadApproval.aspx", False)
    End Sub

    Protected Sub ImageReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click
        Dim objTransManager As New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)
        Dim objApprovalDetail As New TList(Of CDDLNP_WorkflowUpload_ApprovalDetail)
        Dim objUserAssignment As New TList(Of CDDLNP_UserAssignment)

        Try
            objTransManager.BeginTransaction()

            objApprovalDetail = DataRepository.CDDLNP_WorkflowUpload_ApprovalDetailProvider.GetPaged("FK_CDDLNP_WorkflowUpload_Approval_Id = " & Me.GetPK_CDDLNP_WorkflowUpload_Approval_Id, String.Empty, 0, Int32.MaxValue, 0)
            objUserAssignment = DataRepository.CDDLNP_UserAssignmentProvider.GetPaged(objTransManager, "FK_CDDLNP_WorkflowUpload_Approval_Id = " & Me.GetPK_CDDLNP_WorkflowUpload_Approval_Id, String.Empty, 0, Int32.MaxValue, 0)
            'Delete Approval
            DataRepository.CDDLNP_WorkflowUpload_ApprovalProvider.Delete(objTransManager, Me.GetPK_CDDLNP_WorkflowUpload_Approval_Id)
            'Delete Approval Detail
            DataRepository.CDDLNP_WorkflowUpload_ApprovalDetailProvider.Delete(objTransManager, objApprovalDetail)
            'Delete User Assignment
            DataRepository.CDDLNP_UserAssignmentProvider.Delete(objTransManager, objUserAssignment)

            objTransManager.Commit()

            Me.Response.Redirect("CDDLNPMessage.aspx?ID=10352", False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not objTransManager Is Nothing Then
                objTransManager.Dispose()
                objTransManager = Nothing
            End If
            If Not objApprovalDetail Is Nothing Then
                objApprovalDetail.Dispose()
                objApprovalDetail = Nothing
            End If
        End Try
    End Sub

    Protected Sub ImageAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click
        Dim objTransManager As New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)
        Dim objApprovalDetail As New TList(Of CDDLNP_WorkflowUpload_ApprovalDetail)
        Dim objUserAssignment As New TList(Of CDDLNP_UserAssignment)
        Dim objData As New TList(Of CDDLNP_WorkflowUpload)

        Try
            objTransManager.BeginTransaction()

            objApprovalDetail = DataRepository.CDDLNP_WorkflowUpload_ApprovalDetailProvider.GetPaged(objTransManager, "FK_CDDLNP_WorkflowUpload_Approval_Id = " & Me.GetPK_CDDLNP_WorkflowUpload_Approval_Id, String.Empty, 0, Int32.MaxValue, 0)
            objUserAssignment = DataRepository.CDDLNP_UserAssignmentProvider.GetPaged(objTransManager, "FK_CDDLNP_WorkflowUpload_Approval_Id = " & Me.GetPK_CDDLNP_WorkflowUpload_Approval_Id, String.Empty, 0, Int32.MaxValue, 0)

            For Each rowApprovalDetail As CDDLNP_WorkflowUpload_ApprovalDetail In objApprovalDetail
                objData.Add(CDDLNP_WorkflowUpload.CreateCDDLNP_WorkflowUpload( _
                rowApprovalDetail.AccountOwnerId, _
                rowApprovalDetail.List_RM & ";", _
                rowApprovalDetail.List_TeamHead & ";", _
                rowApprovalDetail.List_GroupHead & ";", _
                rowApprovalDetail.List_HeadOff & ";"))
            Next

            'Update data-data CDDNLP yang user-nya resign untuk digantikan dengan user baru
            For Each rowUserAssignment As CDDLNP_UserAssignment In objUserAssignment

                Using objCDDLNP As TList(Of CDDLNP) = DataRepository.CDDLNPProvider.GetPaged(objTransManager, "CreatedBy = '" & rowUserAssignment.Old_RM & "'", String.Empty, 0, Int32.MaxValue, 0)
                    For Each rowCDDLNP As CDDLNP In objCDDLNP
                        rowCDDLNP.CreatedBy = rowUserAssignment.New_RM
                    Next
                    DataRepository.CDDLNPProvider.Save(objTransManager, objCDDLNP)
                End Using

                Using objCDDLNPApproval As TList(Of CDDLNP_Approval) = DataRepository.CDDLNP_ApprovalProvider.GetPaged(objTransManager, "RequestedBy = '" & rowUserAssignment.Old_RM & "'", String.Empty, 0, Int32.MaxValue, 0)
                    For Each rowCDDLNPApproval As CDDLNP_Approval In objCDDLNPApproval
                        rowCDDLNPApproval.RequestedBy = rowUserAssignment.New_RM
                    Next
                    DataRepository.CDDLNP_ApprovalProvider.Save(objTransManager, objCDDLNPApproval)
                End Using

                rowUserAssignment.AssignDate = Now
            Next
            DataRepository.CDDLNP_UserAssignmentProvider.Save(objTransManager, objUserAssignment)

            'Truncate Table
            DataRepository.Provider.ExecuteNonQuery(objTransManager, Data.CommandType.Text, "TRUNCATE TABLE CDDLNP_WorkflowUpload")
            'Insert Data
            DataRepository.CDDLNP_WorkflowUploadProvider.Save(objTransManager, objData)
            'Delete Approval
            DataRepository.CDDLNP_WorkflowUpload_ApprovalProvider.Delete(objTransManager, Me.GetPK_CDDLNP_WorkflowUpload_Approval_Id)
            'Delete Approval Detail
            DataRepository.CDDLNP_WorkflowUpload_ApprovalDetailProvider.Delete(objTransManager, DataRepository.CDDLNP_WorkflowUpload_ApprovalDetailProvider.GetPaged("FK_CDDLNP_WorkflowUpload_Approval_Id = " & Me.GetPK_CDDLNP_WorkflowUpload_Approval_Id, String.Empty, 0, Int32.MaxValue, 0))

            objTransManager.Commit()

            Me.Response.Redirect("CDDLNPMessage.aspx?ID=10353", False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not objTransManager Is Nothing Then
                objTransManager.Dispose()
                objTransManager = Nothing
            End If
            If Not objApprovalDetail Is Nothing Then
                objApprovalDetail.Dispose()
                objApprovalDetail = Nothing
            End If
            If Not objData Is Nothing Then
                objData.Dispose()
                objData = Nothing
            End If
        End Try
    End Sub
End Class