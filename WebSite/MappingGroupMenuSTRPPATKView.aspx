<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="MappingGroupMenuSTRPPATKView.aspx.vb" Inherits="MappingGroupMenuSTRPPATKView" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
<table id="title"  border="2" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr>

            <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Mapping Group Menu STRPPATK &nbsp;- VIEW&nbsp;
                    <hr />
                </strong>
            </td>
        </tr>
    </table>
    <TABLE borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2">
        <TR>
            <TD align="left" bgColor="#eeeeee"><IMG height="15" src="images/arrow.gif" width="15"></TD>
				<TD vAlign="top" width="98%" bgColor="#ffffff"><TABLE cellSpacing="4" cellPadding="0" width="100%" border="0">
						<TR>
							<TD class="Regtext" noWrap>Search By :</TD>
							<TD  noWrap><ajax:ajaxpanel id="AjaxPanel1" runat="server">
                                <TABLE cellSpacing="1" cellPadding="2" width="100%"
					border="0" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
                                    <tr>
                                        <TD style="width: 15%; height: 22px; background-color: #fff7e6" nowrap="noWrap">
                                            <asp:Label id="subject" runat="server" meta:resourcekey="LblGroupMenuNameResource1" Text="GroupMenu"></asp:Label>
                                        </td>
                                        <td nowrap="nowrap" style="width: 367px; height: 22px; background-color: #fff7e6"><asp:TextBox id="TxtGroupName" tabIndex=2 runat="server" CssClass="searcheditbox" Width="200px" meta:resourcekey="TxtGroupMenuNameResource1" MaxLength="100"></asp:TextBox> </td>
                                    </tr>
                                    
                                </table>
                            </ajax:ajaxpanel></TD>
							<TD vAlign="middle" width="99%"><ajax:ajaxpanel id="AjaxPanel2" runat="server">
									<asp:imagebutton id="ImageButtonSearch" tabIndex="3" runat="server" SkinID="SearchButton"></asp:imagebutton>
                                <asp:ImageButton ID="ImageButtonSearchCancel" runat="server"
                                    TabIndex="3" ImageUrl="~/Images/button/clearsearch.gif" /></ajax:ajaxpanel>
								</TD>
						</TR>
					</TABLE><ajax:ajaxpanel id="AjaxPanel3" runat="server">
                    <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></ajax:ajaxpanel></TD>
			</TR>
	</TABLE>
	
<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2">
  <tr> 
    <td bgColor="#ffffff">
        <ajax:AjaxPanel ID="AjaxPanel14"
                runat="server" Width="672px"><asp:DataGrid ID="GridGMSTRPPATK" runat="server" AllowPaging="True"
                    AllowSorting="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#DEDFDE"
                    BorderStyle="None" BorderWidth="1px" CellPadding="4" Font-Size="XX-Small" ForeColor="Black"
                    GridLines="Vertical" Width="100%" AllowCustomPaging="True">
                    <FooterStyle BackColor="#CCCC99" />
                    <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages"
                        Visible="False" />
                    <AlternatingItemStyle BackColor="White" />
                    <ItemStyle BackColor="#F7F7DE" />
                    <Columns>
                        <asp:BoundColumn HeaderText="No">
                            <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                Font-Underline="False" ForeColor="#FFFFFF" />
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="PK_MappingGroupMenuSTRPPATK_ID" Visible="False">
                            <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                Font-Underline="False" Wrap="False" />
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="GroupName" HeaderText="Group Menu" SortExpression="GroupName desc">
                        </asp:BoundColumn>
                        <asp:EditCommandColumn EditText="Edit" Visible="False"></asp:EditCommandColumn>
                        <asp:ButtonColumn CommandName="Delete" Text="Delete"></asp:ButtonColumn>
                    </Columns>
                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" Font-Size="XX-Small" ForeColor="White" />
                </asp:DataGrid>
            </ajax:AjaxPanel>
    </td>
  </tr>
  <tr> 
    <td style="background-color:#ffffff"><table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td nowrap>
                &nbsp;</td>
			 <td width="99%">&nbsp;&nbsp;</td>
			<td align="right" nowrap><asp:LinkButton ID="LinkButtonAddNew" runat="server">Add 
	        New</asp:LinkButton>&nbsp;&nbsp;</td>
		</tr>
      </table></td>
  </tr>
  <tr> 
    <td bgColor="#ffffff"> <TABLE id="Table3" borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#ffffff"
					border="2">
        <TR class="regtext" align="center" bgColor="#dddddd"> 
          <TD vAlign="top" align="left" width="50%" bgColor="#ffffff">Page&nbsp;<ajax:ajaxpanel id="AjaxPanel6" runat="server"> 
            <asp:label id="PageCurrentPage" runat="server" CssClass="regtext">0</asp:label>
            &nbsp;of&nbsp; 
            <asp:label id="PageTotalPages" runat="server" CssClass="regtext">0</asp:label>
            </ajax:ajaxpanel></TD>
          <TD vAlign="top" align="right" width="50%" bgColor="#ffffff">Total Records&nbsp; 
            <ajax:ajaxpanel id="AjaxPanel7" runat="server"> 
            <asp:label id="PageTotalRows" runat="server">0</asp:label>
            </ajax:ajaxpanel></TD>
        </TR>
      </TABLE>
      <TABLE id="Table4" borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#ffffff"
					border="2">
        <TR bgColor="#ffffff"> 
          <TD class="regtext" vAlign="middle" align="left" colSpan="11" height="7"> 
            <HR color="#f40101" noShade SIZE="1"> </TD>
        </TR>
        <TR> 
          <TD class="regtext" vAlign="middle" align="left" width="63" bgColor="#ffffff">Go 
            to page</TD>
          <TD class="regtext" vAlign="middle" align="left" width="5" bgColor="#ffffff"><FONT face="Verdana, Arial, Helvetica, sans-serif" size="1"> 
            <ajax:ajaxpanel id="AjaxPanel8" runat="server"> 
            <asp:textbox id="TextGoToPage" runat="server" Width="38px" CssClass="searcheditbox"></asp:textbox>
            </ajax:ajaxpanel> </FONT></TD>
          <TD class="regtext" vAlign="middle" align="left" bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel9" runat="server"> 
            <asp:imagebutton id="ImageButtonGo" runat="server" SkinID="GoButton"></asp:imagebutton>
            </ajax:ajaxpanel> </TD>
          <TD class="regtext" vAlign="middle" align="left" width="6" bgColor="#ffffff"><IMG height="5" src="images/first.gif" width="6"> 
          </TD>
          <TD class="regtext" vAlign="middle" align="right" width="6" bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel10" runat="server"> 
            <asp:linkbutton id="LinkButtonFirst" runat="server" CssClass="regtext" CommandName="First" OnCommand="PageNavigate">First</asp:linkbutton>
            </ajax:ajaxpanel> </TD>
          <TD class="regtext" vAlign="middle" align="right" width="6" bgColor="#ffffff"><IMG height="5" src="images/prev.gif" width="6"></TD>
          <TD class="regtext" vAlign="middle" align="right" width="14" bgColor="#ffffff"> 
            <ajax:ajaxpanel id="AjaxPanel11" runat="server"> 
            <asp:linkbutton id="LinkButtonPrevious" runat="server" CssClass="regtext" CommandName="Prev" OnCommand="PageNavigate">Previous</asp:linkbutton>
            </ajax:ajaxpanel> </TD>
          <TD class="regtext" vAlign="middle" align="right" width="60" bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel12" runat="server"><A class="pageNav" href="#"> 
            <asp:linkbutton id="LinkButtonNext" runat="server" CssClass="regtext" CommandName="Next" OnCommand="PageNavigate">Next</asp:linkbutton>
            </A></ajax:ajaxpanel></TD>
          <TD class="regtext" vAlign="middle" align="left" width="6" bgColor="#ffffff"><IMG height="5" src="images/next.gif" width="6"></TD>
          <TD class="regtext" vAlign="middle" align="left" width="25" bgColor="#ffffff"><ajax:ajaxpanel id="AjaxPanel13" runat="server"> 
            <asp:linkbutton id="LinkButtonLast" runat="server" CssClass="regtext" CommandName="Last" OnCommand="PageNavigate">Last</asp:linkbutton>
            </ajax:ajaxpanel> </TD>
          <TD class="regtext" vAlign="middle" align="left" width="6" bgColor="#ffffff"><IMG height="5" src="images/last.gif" width="6"></TD>
        </TR>
      </TABLE></td>
  </tr>
</table>

</asp:Content>

