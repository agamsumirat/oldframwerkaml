Imports System.Data.SqlClient
Partial Class PeerGroupDelete
    Inherits Parent

    Private ReadOnly Property PeerGroupID() As Integer
        Get
            Dim temp As String
            temp = Request.Params("PeerGroupID")
            If Not IsNumeric(temp) OrElse temp = "" Then
                Throw New Exception("Peer Group ID not valid")
            Else
                Return temp
            End If
        End Get
    End Property
    Public ReadOnly Property oRowPeerGroup() As AMLDAL.PeerGroup.PeerGroupRow
        Get
            Using adapter As New AMLDAL.PeerGroupTableAdapters.PeerGroupTableAdapter
                If adapter.GetPeerGroupByPK(Me.PeerGroupID).Rows.Count > 0 Then
                    Return adapter.GetPeerGroupByPK(Me.PeerGroupID).Rows(0)
                Else
                    Return Nothing
                End If

            End Using
        End Get
    End Property
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "PeerGroupView.aspx"

        Me.Response.Redirect("PeerGroupView.aspx", False)

    End Sub
#Region "Delete PeerGroup Rules"
    Private Sub DeletePeerGroupBySU()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try

            'Using oTransScope As New Transactions.TransactionScope
            Using adapter As New AMLDAL.PeerGroupTableAdapters.PeerGroupTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(adapter, Data.IsolationLevel.ReadUncommitted)
                adapter.DeletePeerGroupByPK(Me.PeerGroupID)

                InsertAuditTrailDelete(oSQLTrans)
                oSQLTrans.Commit()
                Response.Redirect("PeerGroupView.aspx", False)
            End Using


        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Rollback()
            End If
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub
    Private Sub InsertAuditTrailDelete(ByRef oSQLTrans As SqlTransaction)
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(4)

            If Not Me.oRowPeerGroup Is Nothing Then
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Peer Group", "Peer Group Name", "Delete", "", Me.oRowPeerGroup.PeerGroupName, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Peer Group", "Description", "Delete", "", Me.oRowPeerGroup.PeerGroupDescription, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Peer Group", "Enabled", "Delete", "", Me.oRowPeerGroup.Enabled, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Peer Group", "Expression", "Delete", "", Me.oRowPeerGroup.Expression, "Accepted")


                End Using
            End If
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' menyimpan data risk fact ke pending approval
    ''' </summary>
    ''' <remarks>
    ''' Step
    ''' 1.insert header
    ''' 2.insert detail
    ''' </remarks>
    Private Sub InsertDataPeerGroupToPendingApproval()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try

            Dim intHeader As Integer

            Using adapter As New AMLDAL.PeerGroupTableAdapters.PeerGroupPendingApprovalTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(adapter, Data.IsolationLevel.ReadUncommitted)
                intHeader = adapter.InsertPeerGroupPendingApproval(oRowPeerGroup.PeerGroupName, Sahassa.AML.Commonly.SessionUserId, 3, "Peer Group Delete", Now)


                Using adapter1 As New AMLDAL.PeerGroupTableAdapters.PeerGroupApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(adapter1, oSQLTrans)
                    adapter1.Insert(intHeader, oRowPeerGroup.PeerGroupId, oRowPeerGroup.PeerGroupName, oRowPeerGroup.PeerGroupDescription, oRowPeerGroup.Enabled, oRowPeerGroup.Expression, oRowPeerGroup.CreatedDate, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
                End Using
                oSQLTrans.Commit()

            End Using
            Dim MessagePendingID As Integer = 81603 'MessagePendingID 81603 = Peer Group Delete

            Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & oRowPeerGroup.PeerGroupName
            Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & oRowPeerGroup.PeerGroupName, False)
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Rollback()
            End If
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub
#End Region
    ''' <summary>
    ''' 1.cek data riskfact Masih ada ngak di table
    ''' 2.cek data riskfactngak boleh ada di table approval
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function DataValidToDelete() As Boolean
        Try
            'Step 1
            If oRowPeerGroup Is Nothing Then
                Try
                    Throw New Exception("Cannot delete Peer Group Name because that Peer Group name does not exist in the database anymore. ")
                Catch ex As Exception
                    cvalPageError.IsValid = False
                    cvalPageError.ErrorMessage = ex.Message
                    Return False
                End Try
            End If
            'Step 2
            Using adapter As New AMLDAL.PeerGroupTableAdapters.PeerGroupPendingApprovalTableAdapter
                If adapter.CountPeerGroupPendingApprovalByUniqueKey(oRowPeerGroup.PeerGroupName) > 0 Then
                    Try
                        Throw New Exception("Cannot delete the following Peer Group : " & oRowPeerGroup.PeerGroupName & " Name because it is currently waiting for approval.")
                    Catch ex As Exception
                        cvalPageError.IsValid = False
                        cvalPageError.ErrorMessage = ex.Message
                        Return False
                    End Try
                End If
            End Using

            Return True
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub LoadData()
        Try
            If Not oRowPeerGroup Is Nothing Then
                Me.LabelPeerGroupName.Text = oRowPeerGroup.PeerGroupName
                Me.LabelPeerGroupDescription.Text = oRowPeerGroup.PeerGroupDescription
                Me.LabelEnabled.Text = oRowPeerGroup.Enabled.ToString
                Me.LabelExpression.Text = oRowPeerGroup.Expression
            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' Delete 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click

        Try
            If Page.IsValid Then
                If DataValidToDelete Then
                    If Sahassa.AML.Commonly.SessionUserId.ToLower = "superuser" Then
                        DeletePeerGroupBySU()
                    Else
                        InsertDataPeerGroupToPendingApproval()
                    End If
                End If
            End If
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                LoadData()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)


                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
            End If
        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class
