﻿Imports SahassaNettier.Entities
Imports SahassaNettier.Data
Imports AMLBLL

Partial Class OutlierToleransiApprovalDetailView
    Inherits Parent

    ReadOnly Property GetPk_OutlierToleransiApproval As Long
        Get
            If IsNumeric(Request.Params("OutlierToleransiApprovalID")) Then
                If Not IsNothing(Session("OutlierToleransiApprovalDetail.PK")) Then
                    Return CLng(Session("OutlierToleransiApprovalDetail.PK"))
                Else
                    Session("OutlierToleransiApprovalDetail.PK") = Request.Params("OutlierToleransiApprovalID")
                    Return CLng(Session("OutlierToleransiApprovalDetail.PK"))
                End If
            End If
            Return 0
        End Get
    End Property

    Sub clearSession()
        Session("OutlierToleransiApprovalDetail.PK") = Nothing
    End Sub

    Sub LoadDataAll()
        Using objOutlierToleransiApproval As OutlierToleransiApproval = OutlierToleransiBLL.getOutlierToleransiApprovalByPK(GetPk_OutlierToleransiApproval)
            If Not IsNothing(objOutlierToleransiApproval) Then
                If objOutlierToleransiApproval.FK_ModeID.GetValueOrDefault(0) = Sahassa.AML.Commonly.TypeMode.Add Then

                    LblAction.Text = "Add"
                ElseIf objOutlierToleransiApproval.FK_ModeID.GetValueOrDefault(0) = Sahassa.AML.Commonly.TypeMode.Edit Then
                    LblAction.Text = "Edit"
                ElseIf objOutlierToleransiApproval.FK_ModeID.GetValueOrDefault(0) = Sahassa.AML.Commonly.TypeMode.Delete Then
                    LblAction.Text = "Delete"
                ElseIf objOutlierToleransiApproval.FK_ModeID.GetValueOrDefault(0) = Sahassa.AML.Commonly.TypeMode.Activation Then
                    LblAction.Text = "Activation"
                End If
            End If
            Using objUser As User = UserBLL.GetUserByPkUserID(objOutlierToleransiApproval.FK_MsUserID)
                If Not IsNothing(objUser) Then
                    LblRequestBy.Text = objUser.UserName
                End If
            End Using
            LblRequestDate.Text = CDate(objOutlierToleransiApproval.CreatedDate).ToString("dd-MM-yyyy")


            Using objOutlierToleransiApprovalDetail As OutlierToleransiApprovalDetail = OutlierToleransiBLL.getOutlierToleransiApprovalDetailByPKApproval(GetPk_OutlierToleransiApproval)
                If LblAction.Text = "Add" Then
                    LoadNewData()
                ElseIf LblAction.Text = "Edit" Then
                    LoadNewData()
                    LoadOldData(objOutlierToleransiApprovalDetail.PK_OutlierToleransi_ID)
                    compareDataByContainer(PanelNew, PanelOld)
                ElseIf LblAction.Text = "Delete" Then
                    LoadOldData(objOutlierToleransiApprovalDetail.PK_OutlierToleransi_ID)
                ElseIf LblAction.Text = "Activation" Then
                    LoadNewData()
                    LoadOldData(objOutlierToleransiApprovalDetail.PK_OutlierToleransi_ID)
                    compareDataByContainer(PanelNew, PanelOld)
                End If
            End Using


        End Using

    End Sub

    Sub SelectUser(ByVal IntUserid As Long)
        haccountowner.Value = IntUserid
        LblAccountOwner.Text = AMLBLL.SuspiciusPersonBLL.GetAccountOwnerIDbyPk(IntUserid)
    End Sub

    Sub SelectUserOld(ByVal IntUserid As Long)
        old_haccountowner.Value = IntUserid
        old_lblAccountOwner.Text = AMLBLL.SuspiciusPersonBLL.GetAccountOwnerIDbyPk(IntUserid)
    End Sub

    Sub LoadNewData()
        Using objOutlierToleransiApprovalDetail As OutlierToleransiApprovalDetail = OutlierToleransiBLL.getOutlierToleransiApprovalDetailByPKApproval(GetPk_OutlierToleransiApproval)
            If Not IsNothing(objOutlierToleransiApprovalDetail) Then

                haccountowner.Value = objOutlierToleransiApprovalDetail.AccountOwnerID.ToString
                'LblAccountOwner.Text = objOutlierToleransi..ToString
                txtSegment.Text = objOutlierToleransiApprovalDetail.Segment
                SelectUser(objOutlierToleransiApprovalDetail.AccountOwnerID)
                txtToleransiDebet.Text = objOutlierToleransiApprovalDetail.ToleransiDebet.ToString
                txtToleransiKredit.Text = objOutlierToleransiApprovalDetail.ToleransiKredit.ToString
                If CBool(objOutlierToleransiApprovalDetail.Activation) Then
                    lblActiveOrInactive.Text = "Activated"
                Else
                    lblActiveOrInactive.Text = "Deactivated"
                End If
                PanelNew.Visible = True
   
            End If

        End Using
    End Sub

    Sub LoadOldData(ByVal PK_OutlierToleransi_ID As Long)
        Using objOutlierToleransi As OutlierToleransi = OutlierToleransiBLL.getOutlierToleransiByPk(PK_OutlierToleransi_ID)
            If Not IsNothing(objOutlierToleransi) Then
                old_haccountowner.Value = objOutlierToleransi.AccountOwnerID.ToString
                'LblAccountOwner.Text = objOutlierToleransi..ToString
                old_txtSegment.Text = objOutlierToleransi.Segment
                SelectUserOld(objOutlierToleransi.AccountOwnerID)
                old_txtToleransiDebet.Text = objOutlierToleransi.ToleransiDebet.ToString
                old_txtToleransiKredit.Text = objOutlierToleransi.ToleransiKredit.ToString
                If CBool(objOutlierToleransi.Activation) Then
                    old_lblActiveOrInactive.Text = "Activated"
                Else
                    old_lblActiveOrInactive.Text = "Deactivated"
                End If
                PanelOld.Visible = True
            End If
        End Using
    End Sub


    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                clearSession()
                mtvApproval.ActiveViewIndex = 0

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using

                LoadDataAll()
            End If
        Catch ex As Exception
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImgBtnAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAccept.Click
        Try
            If LblAction.Text = "Add" Then
                If OutlierToleransiBLL.acceptAddOrDelete(GetPk_OutlierToleransiApproval, "Add") Then
                    mtvApproval.ActiveViewIndex = 1
                    lblMessage.Text = "Adding Data Approved"
                    AjaxPanel1.Visible = False
                End If
            ElseIf LblAction.Text = "Edit" Then
                If OutlierToleransiBLL.acceptEditOrActivate(GetPk_OutlierToleransiApproval, "Edit") Then
                    mtvApproval.ActiveViewIndex = 1
                    lblMessage.Text = "Editing Data Approved"
                    AjaxPanel1.Visible = False
                End If
            ElseIf LblAction.Text = "Delete" Then
                If OutlierToleransiBLL.acceptAddOrDelete(GetPk_OutlierToleransiApproval, "Delete") Then
                    mtvApproval.ActiveViewIndex = 1
                    lblMessage.Text = "Deleting Data Approved"
                    AjaxPanel1.Visible = False
                End If
            ElseIf LblAction.Text = "Activation" Then
                If OutlierToleransiBLL.acceptEditOrActivate(GetPk_OutlierToleransiApproval, "Activation") Then
                    mtvApproval.ActiveViewIndex = 1
                    lblMessage.Text = "Activation Data Approved"
                    AjaxPanel1.Visible = False
                End If
            End If
        Catch ex As Exception
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub ImgBtnReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnReject.Click
        Try
            If LblAction.Text = "Add" Then
                If OutlierToleransiBLL.rejectAddOrDelete(GetPk_OutlierToleransiApproval, "Add") Then
                    mtvApproval.ActiveViewIndex = 1
                    lblMessage.Text = "Adding Data Rejeceted"
                    AjaxPanel1.Visible = False
                End If
            ElseIf LblAction.Text = "Edit" Then
                If OutlierToleransiBLL.rejectEditOrActivate(GetPk_OutlierToleransiApproval, "Edit") Then
                    mtvApproval.ActiveViewIndex = 1
                    lblMessage.Text = "Editing Data Rejeceted"
                    AjaxPanel1.Visible = False
                End If
            ElseIf LblAction.Text = "Delete" Then
                If OutlierToleransiBLL.rejectAddOrDelete(GetPk_OutlierToleransiApproval, "Delete") Then
                    mtvApproval.ActiveViewIndex = 1
                    lblMessage.Text = "Deleting Data Rejeceted"
                    AjaxPanel1.Visible = False
                End If
            ElseIf LblAction.Text = "Activation" Then
                If OutlierToleransiBLL.rejectEditOrActivate(GetPk_OutlierToleransiApproval, "Activation") Then
                    mtvApproval.ActiveViewIndex = 1
                    lblMessage.Text = "Activation Data Rejeceted"
                    AjaxPanel1.Visible = False
                End If
            End If
        Catch ex As Exception
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImgBackAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBackAdd.Click
        Try
            Response.Redirect("OutlierToleransiApproval.aspx")
        Catch ex As Exception
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub btnOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnOK.Click
        Try
            Response.Redirect("OutlierToleransiApproval.aspx")
        Catch ex As Exception
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Sub compareDataByContainer(ByRef containerNew As Control, ByRef containerOld As Control)
        Dim index As Integer = 0
        For Each item As Control In containerNew.Controls
            If TypeOf (item) Is Label Then

                If CType(item, Label).Text.ToLower <> "old value" And CType(item, Label).Text.ToLower <> "new value" Then
                    If CType(item, Label).Text <> CType(containerOld.Controls(index), Label).Text Then
                        CType(item, Label).ForeColor = Drawing.Color.Red
                        CType(containerOld.Controls(index), Label).ForeColor = Drawing.Color.Red
                    End If
                End If
            End If
            index = index + 1
        Next
    End Sub
End Class
