Imports System.Windows.Forms
Imports System.Data
Imports System.Data.SqlClient
Imports Sahassa.AML.TableAdapterHelper
Imports System.Text.RegularExpressions


Partial Class NewsAdd
    Inherits Parent

    ''' <summary>
    ''' cancel handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "NewsView.aspx"

        Me.Response.Redirect("NewsView.aspx", False)
    End Sub

    Private Sub CheckInput()
        If Me.TextNewsTitleCK.Text = "" Then
            Throw New Exception("News Title is required.")
        End If
        If Me.TextNewsSummary.Text = "" Then
            Throw New Exception("News Summary is required.")
        End If
        If Me.TextNewsContent.Text = "" Then
            Throw New Exception("News Content is required.")
        End If

    End Sub


    ''' <summary>
    ''' update handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim oSQLTrans As SQLTransaction = Nothing
        Page.Validate()

        If Page.IsValid Then
            Try
                Me.CheckInput()

                Dim NewsTitle As String = Trim(Me.TextNewsTitleCK.Text)
                lblJudul.Text = Trim(Regex.Replace(NewsTitle, "<.*?>", ""))
                'Periksa apakah News Title tersebut sudah ada dalam tabel News atau belum
                Using AccessNews As New AMLDAL.AMLDataSetTableAdapters.NewsTableAdapter
                    Dim counter As Int32 = AccessNews.CountMatchingNews(NewsTitle)

                    'Counter = 0 berarti News Title tersebut belum pernah ada dalam tabel News
                    If counter = 0 Then
                        'Periksa apakah News Title tersebut sudah ada dalam tabel News_Approval atau belum
                        Using AccessNewsApproval As New AMLDAL.AMLDataSetTableAdapters.News_ApprovalTableAdapter
                            oSQLTrans = BeginTransaction(AccessNewsApproval, IsolationLevel.ReadUncommitted)
                            counter = AccessNewsApproval.CountNewsApprovalByNewsTitle(NewsTitle)

                            'Counter = 0 berarti News tersebut statusnya tidak dalam pending approval dan boleh ditambahkan dlm tabel News_Approval
                            If counter = 0 Then
                                Dim NewsSummary As String
                                If Me.TextNewsSummary.Text.Length <= 255 Then
                                    NewsSummary = Me.TextNewsSummary.Text
                                Else
                                    NewsSummary = Me.TextNewsSummary.Text.Substring(0, 255)
                                End If

                                Dim NewsContent As String
                                If Me.TextNewsContent.Text.Length <= 8000 Then
                                    NewsContent = Me.TextNewsContent.Text
                                Else
                                    NewsContent = Me.TextNewsContent.Text.Substring(0, 8000)
                                End If

                                Dim NewsStartDate As DateTime = CDate(Me.TextStartDate.Text)
                                Dim NewsEndDate As DateTime = CDate(Me.TextEndDate.Text)

                                If DateDiff(DateInterval.Day, NewsStartDate, NewsEndDate) < 0 Then
                                    Throw New Exception("Start Date must be less than or equal to End Date.")
                                End If


                                Dim NewsPendingApprovalID As Int64

                                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                                    Me.InsertNewsBySU()
                                    Me.InsertAuditTrail()
                                    Me.LblSuccess.Visible = True
                                    Me.LblSuccess.Text = "Success to Insert News."
                                Else
                                    'Using TransScope As New Transactions.TransactionScope()
                                    Using AccessNewsPendingApproval As New AMLDAL.AMLDataSetTableAdapters.News_PendingApprovalTableAdapter
                                        SetTransaction(AccessNewsPendingApproval, oSQLTrans)
                                        'Tambahkan ke dalam tabel News_PendingApproval dengan ModeID = 1 (Add) 
                                        NewsPendingApprovalID = AccessNewsPendingApproval.InsertNews_PendingApproval(Sahassa.AML.Commonly.SessionUserId, Now, "News Add", 1)

                                        'Tambahkan ke dalam tabel News_Approval dengan ModeID = 1 (Add) 
                                        AccessNewsApproval.Insert(NewsPendingApprovalID, 1, Nothing, NewsTitle, NewsSummary, NewsContent, NewsStartDate, NewsEndDate, Now, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)

                                        oSQLTrans.Commit()
                                        'TransScope.Complete()
                                    End Using
                                    'End Using

                                    'Penambahan Attach
                                    Using adapter As New AMLDAL.AMLDataSetTableAdapters.NewsAttachmentTableAdapter
                                        adapter.UpdateNewsAttachmentFk_NewsApprovalIdbyUserId(Sahassa.AML.Commonly.SessionPkUserId, NewsPendingApprovalID)
                                    End Using
                                    '------------------

                                    Dim MessagePendingID As Integer = 8101 'MessagePendingID 8101 = News Add 

                                    Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & lblJudul.Text.Trim
                                    Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & lblJudul.Text.Trim, False)

                                    'Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID '& "&Identifier=" & NewsTitle
                                    'Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID, False)

                                End If

                              

                            Else
                                Throw New Exception("Cannot add the following News: '" & NewsTitle & "' because it is currently waiting for approval.")
                            End If
                        End Using
                    Else
                        Throw New Exception("Cannot add the following News: '" & NewsTitle & "' because that News Title already exists in the database.")
                    End If

                End Using
            Catch ex As Exception
                If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
                Me.cvalPageError.IsValid = False
                Me.cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            Finally
                If Not oSQLTrans Is Nothing Then
                    oSQLTrans.Dispose()
                    oSQLTrans = Nothing
                End If
            End Try
        End If
    End Sub

#Region "Insert News By SU dan Audit Trail"

    ''' <summary>
    '''  insert news
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub InsertNewsBySU()
        Try
            Dim NewsSummary As String
            If Me.TextNewsSummary.Text.Length <= 255 Then
                NewsSummary = Me.TextNewsSummary.Text
            Else
                NewsSummary = Me.TextNewsSummary.Text.Substring(0, 255)
            End If

            Dim NewsContent As String
            If Me.TextNewsContent.Text.Length <= 8000 Then
                NewsContent = Me.TextNewsContent.Text
            Else
                NewsContent = Me.TextNewsContent.Text.Substring(0, 8000)
            End If

            Using AccessNews As New AMLDAL.AMLDataSetTableAdapters.NewsTableAdapter
                AccessNews.Insert(Me.TextNewsTitleCK.Text, NewsSummary, NewsContent, CDate(Me.TextStartDate.Text), CDate(Me.TextEndDate.Text), Now)
            End Using
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' audit trail
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub InsertAuditTrail()
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(6)

            Dim NewsSummary As String
            If Me.TextNewsSummary.Text.Length <= 255 Then
                NewsSummary = Me.TextNewsSummary.Text
            Else
                NewsSummary = Me.TextNewsSummary.Text.Substring(0, 255)
            End If

            Dim NewsContent As String
            If Me.TextNewsContent.Text.Length <= 8000 Then
                NewsContent = Me.TextNewsContent.Text
            Else
                NewsContent = Me.TextNewsContent.Text.Substring(0, 8000)
            End If

            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "News", "NewsTitle", "Add", "", Me.TextNewsTitleCK.Text, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "News", "NewsSummary", "Add", "", NewsSummary, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "News", "NewsContent", "Add", "", NewsContent, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "News", "NewsStartDate", "Add", "", Me.TextStartDate.Text, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "News", "NewsEndDate", "Add", "", Me.TextEndDate.Text, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "News", "NewsCreatedDate", "Add", "", Now, "Accepted")
            End Using
        Catch
            Throw
        End Try
    End Sub
#End Region

    ''' <summary>
    ''' page load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                'Tambahkan event handler OnClick yang akan menampilkan kalender javascript
                Me.cmdStartDatePicker.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TextStartDate.ClientID & "'), 'dd-mmm-yyyy')")
                Me.cmdEndDatePicker.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TextEndDate.ClientID & "'), 'dd-mmm-yyyy')")

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                    '        Transcope.Complete()
                End Using
                'End Using

                Using adapter As New AMLDAL.AMLDataSetTableAdapters.NewsAttachmentTableAdapter
                    adapter.DeleteNewsAttachmentbyUserId(Sahassa.AML.Commonly.SessionPkUserId)
                End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    'Penambahan Attachment.
    Protected Sub ButtonAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        Try

            If FileUpload1.HasFile Then
                Using adapter As New AMLDAL.AMLDataSetTableAdapters.NewsAttachmentTableAdapter
                    adapter.Insert(0, FileUpload1.FileName, FileUpload1.FileBytes, Sahassa.AML.Commonly.SessionPkUserId, FileUpload1.PostedFile.ContentType)
                End Using
                BindListAttachment()
            End If

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Private Sub BindListAttachment()
        Try
            Using adapter As New AMLDAL.AMLDataSetTableAdapters.NewsAttachmentTableAdapter

                Me.ListAttachment.DataSource = adapter.GetNewsAttachmentByUserIdCreateFirst(Sahassa.AML.Commonly.SessionPkUserId)
                'Me.ListAttachment.DataSource = adapter.GetData()
                Me.ListAttachment.DataTextField = "filename"
                Me.ListAttachment.DataValueField = "PK_NewsAttachment"
                Me.ListAttachment.DataBind()
            End Using
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Sub
    Protected Sub ButtonDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonDelete.Click
        Try
            If ListAttachment.SelectedIndex <> -1 Then
                Using adapter As New AMLDAL.AMLDataSetTableAdapters.NewsAttachmentTableAdapter
                    adapter.DeleteNewsAttachmentbyPkId(ListAttachment.SelectedValue)
                End Using
                BindListAttachment()
            End If

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class