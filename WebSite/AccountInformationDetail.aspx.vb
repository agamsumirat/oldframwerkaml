Imports System
Imports System.Collections
Imports WebChart
Imports System.Data.SqlClient
Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports System.Data

Partial Class AccountInformationDetail
    Inherits Parent

    Public Property ObjAllAccountAllinfo() As AllAccount_AllInfo
        Get
            If Session("AccountInformationDetail.ObjAllAccountAllinfo") Is Nothing Then
                Session("AccountInformationDetail.ObjAllAccountAllinfo") = AMLBLL.AccountInformationBLL.GetAllAccount_AllInfoByPk(Me.GetAccountNo)
                Return CType(Session("AccountInformationDetail.ObjAllAccountAllinfo"), AllAccount_AllInfo)
            Else
                Return CType(Session("AccountInformationDetail.ObjAllAccountAllinfo"), AllAccount_AllInfo)
            End If
        End Get
        Set(value As AllAccount_AllInfo)
            Session("AccountInformationDetail.ObjAllAccountAllinfo") = value
        End Set
    End Property

    Public Property ObjAllAccountAllinfo_JIVE() As AllAccount_AllInfo_JIVE
        Get
            If Session("AccountInformationDetail.ObjAllAccountAllinfo_JIVE") Is Nothing Then
                Using objAllAccountAllInfo_JiveList As TList(Of AllAccount_AllInfo_JIVE) = DataRepository.AllAccount_AllInfo_JIVEProvider.GetPaged("AccountNo = '" & GetAccountNo & "'", "", 0, 1, 0)
                    If objAllAccountAllInfo_JiveList.Count > 0 Then
                        Session("AccountInformationDetail.ObjAllAccountAllinfo_JIVE") = objAllAccountAllInfo_JiveList(0)
                    End If
                End Using

                Return CType(Session("AccountInformationDetail.ObjAllAccountAllinfo_JIVE"), AllAccount_AllInfo_JIVE)
            Else
                Return CType(Session("AccountInformationDetail.ObjAllAccountAllinfo_JIVE"), AllAccount_AllInfo_JIVE)
            End If
        End Get
        Set(ByVal value As AllAccount_AllInfo_JIVE)
            Session("AccountInformationDetail.ObjAllAccountAllinfo_JIVE") = value
        End Set
    End Property

    Public ReadOnly Property GetAccountNo() As String
        Get
            Return Me.Page.Request.Params.Get("AccountNo")
        End Get
    End Property

    Public ReadOnly Property GetSource() As String
        Get
            If Me.Page.Request.Params.Get("source") Is Nothing Then
                Return ""
            Else
                Return Me.Page.Request.Params.Get("source")
            End If

        End Get
    End Property

    Public ReadOnly Property GetStartDate() As String
        Get
            Return DateTime.Now.ToString("yyyy-MM-dd")
        End Get
    End Property

    Public ReadOnly Property GetEndDate() As String
        Get
            Return DateTime.Now.ToString("yyyy-MM-dd")
        End Get
    End Property

#Region "Set Session"
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("TransDetailPageSelected") Is Nothing, New ArrayList, Session("TransDetailPageSelected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("TransDetailPageSelected") = value
        End Set
    End Property
    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("TransDetailPageSort") Is Nothing, "TransactionDetail.TransactionDetailId  asc", Session("TransDetailPageSort"))
        End Get
        Set(ByVal Value As String)
            Session("TransDetailPageSort") = Value
        End Set
    End Property
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("TransDetailCurrentPage") Is Nothing, 0, Session("TransDetailCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("TransDetailCurrentPage") = Value
        End Set
    End Property

    Private Property SetnGetCurrentPageGraph() As Int32
        Get
            Return IIf(Session("TransDetailCurrentPageGraph") Is Nothing, 0, Session("TransDetailCurrentPageGraph"))
        End Get
        Set(ByVal Value As Int32)
            Session("TransDetailCurrentPageGraph") = Value
        End Set
    End Property

    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property



    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("TransDetailRowTotal") Is Nothing, 0, Session("TransDetailRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("TransDetailRowTotal") = Value
        End Set
    End Property
    Private Property SetAndGetSearchingCriteria() As String
        Get
            Return IIf(Session("TransDetailSearchCriteria") Is Nothing, "", Session("TransDetailSearchCriteria"))
        End Get
        Set(ByVal Value As String)
            Session("TransDetailSearchCriteria") = Value
        End Set
    End Property

    Private Function SetAllSearchingCriteria() As String
        Dim StrSearch As String = " TransactionDetail.AccountNo  = '" & Me.GetAccountNo & "'"
        Try


            If IsDate(Me.TxtDate1.Text) And IsDate(Me.TxtDate2.Text) Then
                StrSearch = StrSearch & " And TransactionDetail.TransactionDate Between '" & Me.TxtDate1.Text.Replace("'", "''") & "' And '" & Me.TxtDate2.Text.Replace("'", "''") & "'"
            End If


            If Me.HfAuxTrxCode.Value <> "" Then
                Dim filtersearch As String = HfAuxTrxCode.Value
                Dim arrFilter() As String = filtersearch.Split(";")
                Dim filtersearchfinal As String = ""
                For Each Item As String In arrFilter
                    filtersearchfinal += "'" & Item & "',"
                Next

                If filtersearchfinal.Length > 0 Then
                    filtersearchfinal = filtersearchfinal.Substring(0, filtersearchfinal.Length - 1)

                End If
                StrSearch = StrSearch & " And (TransactionDetail.AuxiliaryTransactionCode in (" & filtersearchfinal & "))"
            End If

            If Me.DebitOrCreditDropDownList.SelectedValue.ToString() <> "All" Then
                StrSearch = StrSearch & " And DebitORCredit = '" & Me.DebitOrCreditDropDownList.SelectedValue.ToString() & "'"
            End If

            If Me.TxtAmount1.Text <> "" Then
                If Not Double.TryParse(Me.TxtAmount1.Text, 0) Then

                    Throw New Exception("Transaction Amount must be numeric value")
                End If
            End If

            If Me.txtAmount2.Text <> "" Then
                If Not Double.TryParse(Me.txtAmount2.Text, 0) Then
                    Throw New Exception("Transaction Amount must be numeric value")
                End If
            End If

            If Double.TryParse(Me.TxtAmount1.Text, 0) And Double.TryParse(Me.txtAmount2.Text, 0) Then
                StrSearch = StrSearch & " And TransactionLocalEquivalent Between " & Me.TxtAmount1.Text & " And " & Me.txtAmount2.Text
            End If

            If Me.TransactionRemarkTextBox.Text <> "" Then
                StrSearch = StrSearch & " And TransactionRemark like '%" & Me.TransactionRemarkTextBox.Text.Replace("'", "''") & "%' "
            End If

            If Me.MemoRemarkTextBox.Text <> "" Then
                StrSearch = StrSearch & " And MemoRemark like '%" & Me.MemoRemarkTextBox.Text.Replace("'", "''") & "%' "
            End If

            Return StrSearch
        Catch
            Throw
            Return ""
        End Try
    End Function
    Private Property SetnGetBindTable() As Data.DataTable
        Get
            Return IIf(Session("TransDetailViewData") Is Nothing, Sahassa.AML.Commonly.GetDatasetPaging(Me.TablesRelated, Me.Pk, Me.SetnGetSort, Me.SetnGetCurrentPage + 1, Sahassa.AML.Commonly.GetDisplayedTotalRow, Me.Fields, Me.SetAndGetSearchingCriteria, ""), Session("TransDetailViewData"))
        End Get
        Set(ByVal value As Data.DataTable)
            Session("TransDetailViewData") = value
        End Set
    End Property

#End Region



    Private Sub ClearThisPageSessions()

        ObjAllAccountAllinfo = Nothing
        Session("AccountInformationDetail.ObjAllAccountAllinfo_JIVE") = Nothing
        Session("TransDetailPageSelected") = Nothing
        Session("TransDetailPageSort") = Nothing
        Session("TransDetailCurrentPage") = Nothing
        Session("TransDetailRowTotal") = Nothing
        Session("TransDetailSearchCriteria") = Nothing
        Session("TransDetailViewData") = Nothing
    End Sub

    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs) Handles LinkButtonFirst.Command
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select
            If MultiView1.ActiveViewIndex = 3 Then ' Transaction Detail
                Me.BindGrid()
                Me.SetInfoNavigate()
                'Me.ChartByDate()
            End If
            'Me.BindGrid()
            'Me.SetInfoNavigate()
            'Me.ChartByDate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
#Region "Set Property For Table Bind Grid"
    Private ReadOnly Property TablesRelated() As String
        Get
            Dim StrQuery As New Text.StringBuilder
            StrQuery.Append("TransactionDetail INNER JOIN TransactionChannelType  ")
            StrQuery.Append("ON TransactionDetail.TransactionChannelType = TransactionChannelType.PK_TransactionChannelType ")
            StrQuery.Append("LEFT JOIN JHCOUN ON TransactionDetail.CountryCode=JHCOUN.JHCCOC ")
            StrQuery.Append("left JOIN vw_AuxTransactionCode ON TransactionDetail.AuxiliaryTransactionCode=vw_AuxTransactionCode.TransactionCode ")
            StrQuery.Append("INNER JOIN AccountOwner ON TransactionDetail.AccountOwnerId=AccountOwner.AccountOwnerId ")

            Return StrQuery.ToString()
        End Get
    End Property
    Private ReadOnly Property Pk() As String
        Get
            Return "TransactionDetail.TransactionDetailId"
        End Get
    End Property
    Private ReadOnly Property Fields() As String
        Get
            Dim StrQuery As New Text.StringBuilder
            StrQuery.Append("TransactionDetail.TransactionDetailId, TransactionDetail.TransactionDate, ")
            StrQuery.Append("CAST(TransactionDetail.AccountOwnerId AS VARCHAR)  + ' - ' + AccountOwner.AccountOwnerName AS AccountOwner, ")
            StrQuery.Append("TransactionChannelType.TransactionChannelTypeName, TransactionDetail.CountryCode + ' - ' + ISNULL(JHCOUN.JHCNAM,'') AS Country, ")
            StrQuery.Append("TransactionDetail.BankCode, TransactionDetail.BankName, ")
            'StrQuery.Append("TransactionDetail.CIFNo, TransactionDetail.AccountNo, TransactionDetail.AccountName, ")
            StrQuery.Append("TransactionDetail.AuxiliaryTransactionCode + ' - ' + vw_AuxTransactionCode.TransactionDescription AS AuxiliaryTransactionCode, ")
            StrQuery.Append("TransactionDetail.TransactionAmount, TransactionDetail.CurrencyType, ")
            StrQuery.Append("TransactionDetail.DebitORCredit, TransactionDetail.MemoRemark, ")
            StrQuery.Append("TransactionDetail.TransactionRemark, TransactionDetail.TransactionExchangeRate, ")
            StrQuery.Append("TransactionDetail.TransactionLocalEquivalent, TransactionDetail.TransactionTicketNo, ")
            StrQuery.Append("TransactionDetail.UserId,TransactionDetail.AccountNoLawan  AS AccountLawan,TransactionDetail.AccountNameLawan  AS AccountNameLawan")

            'StrQuery.Append("TransactionDetail.UserId,dbo.ufn_GetAccountLawan(TransactionDetail.TransactionTicketNo,TransactionDetail.DebitORCredit,TransactionDetail.TransactionDate) as AccountLawan,dbo.ufn_GetAccountNameByAccountNo( TransactionDetail.TransactionTicketNo,TransactionDetail.DebitORCredit,TransactionDetail.TransactionDate) as AccountNameLawan")

            Return StrQuery.ToString()
        End Get
    End Property
#End Region

#Region "Bind Grid View"
    Public Sub BindGrid()
        Me.SetnGetBindTable = Sahassa.AML.Commonly.GetDatasetPaging(Me.TablesRelated, Me.Pk, Me.SetnGetSort, Me.SetnGetCurrentPage + 1, Sahassa.AML.Commonly.GetDisplayedTotalRow, Me.Fields, Me.SetAndGetSearchingCriteria, "")
        Me.GridTransDetail.DataSource = Me.SetnGetBindTable
        Me.SetnGetRowTotal = Sahassa.AML.Commonly.GetTableCount(Me.TablesRelated, Me.SetAndGetSearchingCriteria)
        Me.GridTransDetail.CurrentPageIndex = Me.SetnGetCurrentPage
        Me.GridTransDetail.VirtualItemCount = Me.SetnGetRowTotal
        Me.GridTransDetail.DataBind()
        If SetnGetRowTotal > 0 Then
            LabelNoRecordFound.Visible = False
        Else
            LabelNoRecordFound.Visible = True
        End If
    End Sub
#End Region

#Region "Paging Event"
    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
        Catch
            Throw
        End Try
    End Sub

    Protected Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            Me.CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be a positive number greater than zero.")
                End If
            Else
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If
            BindGrid()
            SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
#End Region
#Region "Sorting Event"

    Protected Sub GridTransDetail_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridTransDetail.SortCommand
        Dim GridUser As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort

            Me.BindGrid()
            Me.SetInfoNavigate()
            'Me.ChartByDate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
#End Region
#Region "Excel Export Event"
    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In Me.GridTransDetail.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = gridRow.FindControl("CheckBoxExporttoExcel")
                Dim RETBaselPkId As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
                If ArrTarget.Contains(RETBaselPkId) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(RETBaselPkId)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(RETBaselPkId)
                End If
                Me.SetnGetSelectedItem = ArrTarget
            End If
        Next
    End Sub
    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub 'ClearControls

    Sub BindAll()
        Me.SetnGetBindTable = Sahassa.AML.Commonly.GetDatasetPaging(Me.TablesRelated, Me.Pk, Me.SetnGetSort, 0, Integer.MaxValue, Me.Fields, Me.SetAndGetSearchingCriteria, "")
        Me.GridTransDetail.DataSource = Me.SetnGetBindTable
        Me.SetnGetRowTotal = Sahassa.AML.Commonly.GetTableCount(Me.TablesRelated, Me.SetAndGetSearchingCriteria)
        Me.GridTransDetail.AllowPaging = False
        Me.GridTransDetail.DataBind()
        Me.GridTransDetail.Columns(0).Visible = False
        Me.GridTransDetail.Columns(1).Visible = False
    End Sub

    Private Sub BindSelected()
        Dim Rows As New ArrayList
        Dim oTableTransaction As New Data.DataTable
        Dim oRowTransaction As Data.DataRow

        oTableTransaction.Columns.Add("TransactionDetailId", GetType(Int64))
        oTableTransaction.Columns.Add("TransactionDate", GetType(String))
        oTableTransaction.Columns.Add("AccountOwner", GetType(String))
        oTableTransaction.Columns.Add("TransactionChannelTypeName", GetType(String))
        oTableTransaction.Columns.Add("Country", GetType(String))
        oTableTransaction.Columns.Add("BankCode", GetType(String))
        oTableTransaction.Columns.Add("BankName", GetType(String))
        oTableTransaction.Columns.Add("AuxiliaryTransactionCode", GetType(String))
        oTableTransaction.Columns.Add("TransactionAmount", GetType(String))
        oTableTransaction.Columns.Add("CurrencyType", GetType(String))
        oTableTransaction.Columns.Add("DebitORCredit", GetType(String))
        oTableTransaction.Columns.Add("MemoRemark", GetType(String))
        oTableTransaction.Columns.Add("TransactionRemark", GetType(String))
        oTableTransaction.Columns.Add("TransactionExchangeRate", GetType(String))
        oTableTransaction.Columns.Add("TransactionLocalEquivalent", GetType(String))
        oTableTransaction.Columns.Add("TransactionTicketNo", GetType(String))
        oTableTransaction.Columns.Add("UserId", GetType(String))
        oTableTransaction.Columns.Add("AccountLawan", GetType(String))
        oTableTransaction.Columns.Add("AccountNameLawan", GetType(String))
        For Each IdPk As Int64 In Me.SetnGetSelectedItem
            Dim rowData() As Data.DataRow = Me.SetnGetBindTable.Select("TransactionDetailId = " & IdPk & "")
            If rowData.Length > 0 Then
                oRowTransaction = oTableTransaction.NewRow
                oRowTransaction("TransactionDetailId") = rowData(0)("TransactionDetailId")
                oRowTransaction("TransactionDate") = rowData(0)("TransactionDate")
                oRowTransaction("AccountOwner") = rowData(0)("AccountOwner")
                oRowTransaction("TransactionChannelTypeName") = rowData(0)("TransactionChannelTypeName")
                oRowTransaction("Country") = rowData(0)("Country")
                oRowTransaction("BankCode") = rowData(0)("BankCode")
                oRowTransaction("BankName") = rowData(0)("BankName")
                oRowTransaction("AuxiliaryTransactionCode") = rowData(0)("AuxiliaryTransactionCode")
                oRowTransaction("TransactionAmount") = rowData(0)("TransactionAmount")
                oRowTransaction("CurrencyType") = rowData(0)("CurrencyType")
                oRowTransaction("DebitORCredit") = rowData(0)("DebitORCredit")
                oRowTransaction("MemoRemark") = rowData(0)("MemoRemark")
                oRowTransaction("TransactionRemark") = rowData(0)("TransactionRemark")
                oRowTransaction("TransactionExchangeRate") = rowData(0)("TransactionExchangeRate")
                oRowTransaction("TransactionLocalEquivalent") = rowData(0)("TransactionLocalEquivalent")
                oRowTransaction("TransactionTicketNo") = rowData(0)("TransactionTicketNo")
                oRowTransaction("UserId") = rowData(0)("UserId")
                oRowTransaction("AccountLawan") = rowData(0)("AccountLawan")
                oRowTransaction("AccountNameLawan") = rowData(0)("AccountNameLawan")
                oTableTransaction.Rows.Add(oRowTransaction)
            End If
        Next
        Me.GridTransDetail.DataSource = oTableTransaction
        Me.GridTransDetail.AllowPaging = False
        Me.GridTransDetail.DataBind()

        'Sembunyikan kolom agar tidak ikut diekspor ke excel
        Me.GridTransDetail.Columns(0).Visible = False
        Me.GridTransDetail.Columns(1).Visible = False
    End Sub

    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Me.CollectSelected()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelected()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=AccountInformationTransaction.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridTransDetail)
            GridTransDetail.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch tex As Threading.ThreadAbortException
            ' ignore
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        Try
            Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
            For Each gridRow As DataGridItem In Me.GridTransDetail.Items
                If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                    Dim StrKey As String = gridRow.Cells(1).Text
                    If Me.CheckBoxSelectAll.Checked Then
                        If Me.SetnGetSelectedItem.Contains(StrKey) = False Then
                            ArrTarget.Add(StrKey)
                        End If
                    Else
                        If Me.SetnGetSelectedItem.Contains(StrKey) Then
                            ArrTarget.Remove(StrKey)
                        End If
                    End If
                End If
            Next
            Me.SetnGetSelectedItem = ArrTarget
            Me.BindGrid()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
#End Region

#Region "Searching Event"
    Protected Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Try
            Me.CollectSelected()
            Me.SetAndGetSearchingCriteria = Me.SetAllSearchingCriteria
            Me.SetnGetCurrentPage = 0
            If MultiView1.ActiveViewIndex = 3 Then ' Transaction Detail
                Me.BindGrid()
                Me.SetInfoNavigate()
                'Me.ChartByDate()
            End If
            'Me.BindGrid()
            'Me.SetInfoNavigate()
            'Me.ChartByDate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
#End Region

    Protected Sub GridTransDetail_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridTransDetail.ItemDataBound
        'bikin indexing
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Dim chkBox As CheckBox = e.Item.FindControl("CheckBoxExporttoExcel")
                chkBox.Checked = Me.SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)
                'e.Item.Cells(1).Text = e.Item.ItemIndex + 1
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Menu1_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs) Handles Menu1.MenuItemClick
        MultiView1.ActiveViewIndex = Int32.Parse(e.Item.Value)
    End Sub

    Private Function LoadCustomerSubTypeInformation(ByVal StrCIFNo As String) As Boolean
        Using CustomerInformationAdapter As New AMLDAL.CustomerInformationTableAdapters.SelectCDD_CIF_MasterFilterTableAdapter
            Using CustomerInformationTable As New AMLDAL.CustomerInformation.SelectCDD_CIF_MasterFilterDataTable
                CustomerInformationAdapter.Fill(CustomerInformationTable, StrCIFNo)
                If Not CustomerInformationTable Is Nothing Then
                    If CustomerInformationTable.Rows.Count > 0 Then
                        Dim CustomerInformationTableRow As AMLDAL.CustomerInformation.SelectCDD_CIF_MasterFilterRow
                        CustomerInformationTableRow = CustomerInformationTable.Rows(0)
                        If Not CustomerInformationTableRow Is Nothing Then
                            If Not CustomerInformationTableRow.IsCustomerTypeNull Then
                                Me.CustomerTypeLabel.Text = CustomerInformationTableRow.CustomerType
                            End If
                            If Not CustomerInformationTableRow.IsCustomerSubClassNull Then
                                Me.CustomerSubTypeLabel.Text = CustomerInformationTableRow.CustomerSubClass
                            End If
                            If Not CustomerInformationTableRow.IsBusinessTypeNull Then
                                Me.BusinessTypeLabel.Text = CustomerInformationTableRow.BusinessType
                            End If
                            If Not CustomerInformationTableRow.IsIndustryCodeNull Then
                                Me.InternalIndustryCodeLabel.Text = CustomerInformationTableRow.IndustryCode
                            End If
                        End If
                    End If
                End If
            End Using
        End Using
        Return True
    End Function


    Private Function LoadSBU(ByVal StrAccountNo As String, ByVal StrCIFNo As String, ByVal StrOffCode As String) As Boolean
        Dim reader As Data.SqlClient.SqlDataReader
        Dim i As Integer
        Dim str As String
        str = "select CompCode, convert(varchar,CostCenter), SBUCode, SBUDesc, SUBSBUCode, SUBSBUDesc from AccountInformationSBU where CIFNO = @CIFNO and  ACCTNO = @ACCTNO  "
        Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommand(str)
            objCommand.Parameters.Add(New SqlParameter("@CIFNO", StrCIFNo))
            objCommand.Parameters.Add(New SqlParameter("@ACCTNO", StrAccountNo))
            reader = DataRepository.Provider.ExecuteReader(objCommand)
            If reader.HasRows Then
                If reader.Read Then
                    If reader.IsDBNull(0) = False Then
                        Me.lblCompCode.Text = reader.GetString(0)
                    End If

                    If reader.IsDBNull(1) = False Then
                        Me.lblRCCode.Text = reader.GetString(1)
                    End If

                    If reader.IsDBNull(2) = False Then
                        Me.lblSBU.Text = reader.GetString(2)
                    End If
                    If reader.IsDBNull(3) = False Then
                        Me.lblSBU.Text = Me.lblSBU.Text + " - " + reader.GetString(3)
                    End If
                    If reader.IsDBNull(4) = False Then
                        Me.lblSubSBU.Text = reader.GetString(4)
                    End If

                    If reader.IsDBNull(5) = False Then
                        Me.lblSubSBU.Text = Me.lblSubSBU.Text + " - " + reader.GetString(5)
                    End If
                End If
            End If
        End Using

        If GetSource.ToLower <> "jive" Then
            str = "SELECT SSONAM FROM SSOFFR WHERE  SSOOFF = @OffCode  "
            Using objCommand As SqlCommand = Sahassa.AML.Commonly.GetSQLCommand(str)
                objCommand.Parameters.Add(New SqlParameter("@OffCode", StrOffCode))
                reader = DataRepository.Provider.ExecuteReader(objCommand)
                If reader.HasRows Then
                    If reader.Read Then
                        If reader.IsDBNull(0) = False Then
                            Me.LblAccountOfficer.Text = StrOffCode + " - " + reader.GetString(0)
                        End If
                    End If
                End If
            End Using
        End If
        Return True
    End Function





    Private Sub LoadAccountMessage()
        Try


            Using TableAdapterSelectAccountMessage As New AMLDAL.AccountInformationTableAdapters.SelectAccountMessageTableAdapter
                Using dtAccountMessage As Data.DataTable = TableAdapterSelectAccountMessage.GetData(Me.GetAccountNo)
                    If dtAccountMessage.Rows.Count > 0 Then
                        Dim Rows As AMLDAL.AccountInformation.SelectAccountMessageRow = dtAccountMessage.Rows(0)
                        If Rows.IsAccountMessageNull Then
                            Me.LblMessage.Text = ""
                        Else
                            Me.LblMessage.Text = "Message: "
                            Lblmessageisi.Text = Rows.AccountMessage
                        End If

                    Else
                        Me.LblMessage.Text = ""
                    End If
                End Using
            End Using
        Catch
            Throw
        End Try
    End Sub

    Private Sub LoadDetailGeneral()
        Try
            If GetSource.ToLower = "jive" Then
                LblAccountOfficer.Text = ObjAllAccountAllinfo_JIVE.OfficerCode
                LblLineOfBusiness.Text = ObjAllAccountAllinfo_JIVE.LineOfBusiness

                Using ObjAllAccount_WebTempTable_JIVE As TList(Of AllAccount_WebTempTable_JIVE) = DataRepository.AllAccount_WebTempTable_JIVEProvider.GetPaged("AccountNo='" & GetAccountNo & "'", "", 0, 1, 0)
                    If ObjAllAccount_WebTempTable_JIVE.Count > 0 Then
                        Me.LblAccountNumber.Text = ObjAllAccount_WebTempTable_JIVE(0).AccountNo

                        If Not ObjAllAccount_WebTempTable_JIVE(0).AccountName Is Nothing Then
                            Me.LblName.Text = ObjAllAccount_WebTempTable_JIVE(0).AccountName
                        End If

                        If Not ObjAllAccount_WebTempTable_JIVE(0).AccountOwnerName Is Nothing Then
                            Me.LblAccountOwner.Text = ObjAllAccount_WebTempTable_JIVE(0).AccountOwnerId.ToString() & " - " & ObjAllAccount_WebTempTable_JIVE(0).AccountOwnerName
                        End If


                        If Not ObjAllAccount_WebTempTable_JIVE(0).CIFNO Is Nothing Then
                            'Me.LblCIFNo.Text = Rows.CIFNO
                            Me.LnkCIFNo.Text = ObjAllAccount_WebTempTable_JIVE(0).CIFNO
                            Me.LnkCIFNo.NavigateUrl = "./CustomerInformationDetail.aspx?CIFNo=" & ObjAllAccount_WebTempTable_JIVE(0).CIFNO
                            Me.LoadCustomerSubTypeInformation(ObjAllAccount_WebTempTable_JIVE(0).CIFNO)
                        End If

                        If Not ObjAllAccount_WebTempTable_JIVE(0).AccountTypeDescription Is Nothing Then
                            Me.LblAccountType.Text = ObjAllAccount_WebTempTable_JIVE(0).AccountTypeDescription
                        End If

                        If Not ObjAllAccount_WebTempTable_JIVE(0).OpeningDate.GetValueOrDefault = "01/01/0001" Then
                            Me.LblOpeningDate.Text = CDate(ObjAllAccount_WebTempTable_JIVE(0).OpeningDate).ToString("dd-MMM-yyyy")
                        End If

                        If Not ObjAllAccount_WebTempTable_JIVE(0).CurrencyType Is Nothing Then
                            Me.LblCurrency.Text = ObjAllAccount_WebTempTable_JIVE(0).CurrencyType
                        End If

                        If Not ObjAllAccount_WebTempTable_JIVE(0).ProductDescription Is Nothing Then
                            Me.LblProduct.Text = ObjAllAccount_WebTempTable_JIVE(0).ProductDescription
                        End If
                        If Not ObjAllAccount_WebTempTable_JIVE(0).AccountStatusDescription Is Nothing Then
                            Me.LblStatus.Text = ObjAllAccount_WebTempTable_JIVE(0).AccountStatusDescription
                        End If

                        Using TblAdapterStatusDate As New AMLDAL.AccountInformationTableAdapters.AccountInformationGetStatusDateTableAdapter_JIVE
                            Using TblStatusDate As AMLDAL.AccountInformation.AccountInformationGetStatusDate_JIVEDataTable = TblAdapterStatusDate.GetData(Me.GetAccountNo)
                                If TblStatusDate.Rows.Count > 0 Then
                                    Dim RowStatusDate As AMLDAL.AccountInformation.AccountInformationGetStatusDateRow
                                    RowStatusDate = TblStatusDate.Rows(0)
                                    If Not RowStatusDate.IsStatusDateNull Then
                                        If RowStatusDate.StatusDate = "1900-01-01" Then
                                            Me.LblStatusDate.Text = ""
                                        Else
                                            Me.LblStatusDate.Text = RowStatusDate.StatusDate.ToString("dd-MMM-yyyy")
                                        End If
                                    End If
                                End If
                            End Using
                        End Using
                        Me.TdGeneralAvailable.Visible = True
                        Me.TdGeneralNotAvailable.Visible = False

                    Else
                        Me.TdGeneralAvailable.Visible = False
                        Me.TdGeneralNotAvailable.Visible = True
                    End If
                End Using


            Else
                LblAccountOfficer.Text = AMLBLL.AccountInformationBLL.GetAccountOfficerByAccontNo(Me.GetAccountNo)
                LblLineOfBusiness.Text = AMLBLL.AccountInformationBLL.GetLineOfBusinessByAccountNO(Me.GetAccountNo)

                Using AccessAccountInformation As New AMLDAL.AccountInformationTableAdapters.SelectAllAccount_WebTempTableByAccountNoTableAdapter
                    Using dtDetailGeneral As Data.DataTable = AccessAccountInformation.GetAllAccount_WebTempTableByAccountNo(Me.GetAccountNo)
                        If dtDetailGeneral.Rows.Count > 0 Then
                            Dim Rows As AMLDAL.AccountInformation.SelectAllAccount_WebTempTableByAccountNoRow = dtDetailGeneral.Rows(0)
                            Me.LblAccountNumber.Text = Rows.AccountNo
                            If Not Rows.IsAccountNameNull Then
                                Me.LblName.Text = Rows.AccountName
                            End If
                            If Not Rows.IsAccountOwnerNameNull Then
                                Me.LblAccountOwner.Text = Rows.AccountOwnerId.ToString() & " - " & Rows.AccountOwnerName
                            End If
                            If Not Rows.IsCIFNONull Then
                                'Me.LblCIFNo.Text = Rows.CIFNO
                                Me.LnkCIFNo.Text = Rows.CIFNO
                                Me.LnkCIFNo.NavigateUrl = "./CustomerInformationDetail.aspx?CIFNo=" & Rows.CIFNO
                                Me.LoadCustomerSubTypeInformation(Rows.CIFNO)
                                Me.LoadSBU(Rows.AccountNo, Rows.CIFNO, LblAccountOfficer.Text)
                            End If
                            If Not Rows.IsAccountTypeDescriptionNull Then
                                Me.LblAccountType.Text = Rows.AccountTypeDescription
                            End If
                            If Not Rows.IsOpeningDateNull Then
                                Me.LblOpeningDate.Text = Rows.OpeningDate.ToString("dd-MMM-yyyy")
                            End If
                            If Not Rows.IsCurrencyTypeNull Then
                                Me.LblCurrency.Text = Rows.CurrencyType
                            End If
                            If Not Rows.IsProductDescriptionNull Then
                                Me.LblProduct.Text = Rows.ProductDescription
                            End If
                            If Not Rows.IsAccountStatusDescriptionNull Then
                                Me.LblStatus.Text = Rows.AccountStatusDescription
                            End If
                            Using TblAdapterStatusDate As New AMLDAL.AccountInformationTableAdapters.AccountInformationGetStatusDateTableAdapter
                                Using TblStatusDate As AMLDAL.AccountInformation.AccountInformationGetStatusDateDataTable = TblAdapterStatusDate.GetData(Me.GetAccountNo)
                                    If TblStatusDate.Rows.Count > 0 Then
                                        Dim RowStatusDate As AMLDAL.AccountInformation.AccountInformationGetStatusDateRow
                                        RowStatusDate = TblStatusDate.Rows(0)
                                        If Not RowStatusDate.IsStatusDateNull Then
                                            If RowStatusDate.StatusDate = "1900-01-01" Then
                                                Me.LblStatusDate.Text = ""
                                            Else
                                                Me.LblStatusDate.Text = RowStatusDate.StatusDate.ToString("dd-MMM-yyyy")
                                            End If
                                        End If
                                    End If
                                End Using
                            End Using
                            Me.TdGeneralAvailable.Visible = True
                            Me.TdGeneralNotAvailable.Visible = False
                        Else
                            Me.TdGeneralAvailable.Visible = False
                            Me.TdGeneralNotAvailable.Visible = True
                        End If
                    End Using
                End Using
            End If
        Catch
            Throw
        End Try
    End Sub

    ''' <summary>
    ''' Load Detail KYC Information
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub LoadDetailKYCInformation()
        Try
            If GetSource.ToLower = "jive" Then
                Using AccessAccountInformation As New AMLDAL.AccountInformationTableAdapters.SelectDetailKYCInformation_JIVETableAdapter
                    Using dtDetailKYC As Data.DataTable = AccessAccountInformation.GetData(Me.LblAccountNumber.Text)
                        If dtDetailKYC.Rows.Count > 0 Then
                            Dim RowKYC As AMLDAL.AccountInformation.SelectDetailKYCInformation_JIVERow = dtDetailKYC.Rows(0)
                            ' set visibility
                            Me.TdKYCAvailable.Visible = True
                            Me.TdKYCNotAvailable.Visible = False
                            If Not RowKYC.IsNominalTransactionNormalLowNull Then
                                If Not RowKYC.IsNominalTransactionNormalHighNull Then
                                    Me.LblNominalTransactionNormal.Text = Format(RowKYC.NominalTransactionNormalLow, "#,#.00") & " - " & Format(RowKYC.NominalTransactionNormalHigh, "#,#.00")
                                End If
                            End If
                            If Not RowKYC.IsTujuanPenggunaanDanaNull Then
                                Me.LblTujuanPenggunaanDana.Text = RowKYC.TujuanPenggunaanDana
                            End If
                            If Not RowKYC.IsHubunganDgBankNull Then
                                Me.LblHubDgBank.Text = RowKYC.HubunganDgBank
                            End If
                            LblLastUpdatedCIF.Text = AMLBLL.AccountInformationBLL.getLastModifiedCIF(LnkCIFNo.Text)
                        Else
                            Me.TdKYCAvailable.Visible = False
                            Me.TdKYCNotAvailable.Visible = True
                        End If
                    End Using
                End Using
            Else
                Using AccessAccountInformation As New AMLDAL.AccountInformationTableAdapters.SelectDetailKYCInformationTableAdapter
                    Using dtDetailKYC As Data.DataTable = AccessAccountInformation.GetData(Convert.ToDecimal(Me.LblAccountNumber.Text))
                        If dtDetailKYC.Rows.Count > 0 Then
                            Dim RowKYC As AMLDAL.AccountInformation.SelectDetailKYCInformationRow = dtDetailKYC.Rows(0)
                            ' set visibility
                            Me.TdKYCAvailable.Visible = True
                            Me.TdKYCNotAvailable.Visible = False
                            If Not RowKYC.IsNominalTransactionNormalLowNull Then
                                If Not RowKYC.IsNominalTransactionNormalHighNull Then
                                    Me.LblNominalTransactionNormal.Text = Format(RowKYC.NominalTransactionNormalLow, "#,#.00") & " - " & Format(RowKYC.NominalTransactionNormalHigh, "#,#.00")
                                End If
                            End If
                            If Not RowKYC.IsTujuanPenggunaanDanaNull Then
                                Me.LblTujuanPenggunaanDana.Text = RowKYC.TujuanPenggunaanDana
                            End If
                            If Not RowKYC.IsHubunganDgBankNull Then
                                Me.LblHubDgBank.Text = RowKYC.HubunganDgBank
                            End If
                            LblLastUpdatedCIF.Text = AMLBLL.AccountInformationBLL.getLastModifiedCIF(LnkCIFNo.Text)
                        Else
                            Me.TdKYCAvailable.Visible = False
                            Me.TdKYCNotAvailable.Visible = True
                        End If
                    End Using
                End Using
            End If
        Catch
            Throw
        End Try
    End Sub

    Private Sub FinancialInformationAvailable()
        Try
            If Me.GetSource = "jive" Then
                Using AccessAccountInformation As New AMLDAL.AccountInformationTableAdapters.SelectAccountFinancialInfoByAccountNo_JIVETableAdapter
                    Using dtDetailFinancialInfo As Data.DataTable = AccessAccountInformation.GetAccountFinancialInfoByAccountNo(Me.GetAccountNo)
                        If dtDetailFinancialInfo.Rows.Count > 0 Then
                            Dim RowFinancialInfo As AMLDAL.AccountInformation.SelectAccountFinancialInfoByAccountNo_JIVERow = dtDetailFinancialInfo.Rows(0)
                            ' set visibility
                            Me.TdFinancialInformationAvailable.Visible = True
                            Me.TdFinancialInformationNotAvailable.Visible = False

                            If Not RowFinancialInfo.IsCurrentBalanceNull Then
                                Me.LblCurBalance.Text = Format(RowFinancialInfo.CurrentBalance, "#,#.00")
                            End If

                            If Not RowFinancialInfo.IsTransactionYesterdayNumberOfDebitNull Then
                                Me.LblYesterdayDebit.Text = Format(RowFinancialInfo.TransactionYesterdayNumberOfDebit, "#,#")
                            End If
                            If Not RowFinancialInfo.IsTransactionYesterdayNumberOfCreditNull Then
                                Me.LblYesterdayCredit.Text = Format(RowFinancialInfo.TransactionYesterdayNumberOfCredit, "#,#")
                            End If
                            If Not RowFinancialInfo.IsTransactionYesterdayAverageDebitAmountNull Then
                                Me.LblYesterdayAvgDebit.Text = Format(RowFinancialInfo.TransactionYesterdayAverageDebitAmount, "#,#.00")
                            End If
                            If Not RowFinancialInfo.IsTransactionYesterdayAverageCreditAmountNull Then
                                Me.LblYesterdayAvgCredit.Text = Format(RowFinancialInfo.TransactionYesterdayAverageCreditAmount, "#,#.00")
                            End If
                            If Not RowFinancialInfo.IsTransactionYesterdayTotalDebitAmountNull Then
                                Me.LblYesterdayTotalDebit.Text = Format(RowFinancialInfo.TransactionYesterdayTotalDebitAmount, "#,#.00")
                            End If
                            If Not RowFinancialInfo.IsTransactionYesterdayTotalCreditAmountNull Then
                                Me.LblYesterdayTotalCredit.Text = Format(RowFinancialInfo.TransactionYesterdayTotalCreditAmount, "#,#.00")
                            End If

                            If Not RowFinancialInfo.IsTransactionLastWeekNumberOfDebitNull Then
                                Me.LblLastWeekDebit.Text = Format(RowFinancialInfo.TransactionLastWeekNumberOfDebit, "#,#")
                            End If
                            If Not RowFinancialInfo.IsTransactionLastWeekNumberOfCreditNull Then
                                Me.LblLastWeekCredit.Text = Format(RowFinancialInfo.TransactionLastWeekNumberOfCredit, "#,#")
                            End If
                            If Not RowFinancialInfo.IsTransactionLastWeekAverageDebitAmountNull Then
                                Me.LblLastWeekAvgDebit.Text = Format(RowFinancialInfo.TransactionLastWeekAverageDebitAmount, "#,#.00")
                            End If
                            If Not RowFinancialInfo.IsTransactionLastWeekAverageCreditAmountNull Then
                                Me.LblLastWeekAvgCredit.Text = Format(RowFinancialInfo.TransactionLastWeekAverageCreditAmount, "#,#.00")
                            End If
                            If Not RowFinancialInfo.IsTransactionLastWeekTotalDebitAmountNull Then
                                Me.LblLastWeekTotalDebit.Text = Format(RowFinancialInfo.TransactionLastWeekTotalDebitAmount, "#,#.00")
                            End If
                            If Not RowFinancialInfo.IsTransactionLastWeekTotalCreditAmountNull Then
                                Me.LblLastWeekTotalCredit.Text = Format(RowFinancialInfo.TransactionLastWeekTotalCreditAmount, "#,#.00")
                            End If

                            If Not RowFinancialInfo.IsTransactionLastMonthNumberOfDebitNull Then
                                Me.LblLastMonthDebit.Text = Format(RowFinancialInfo.TransactionLastMonthNumberOfDebit, "#,#")
                            End If
                            If Not RowFinancialInfo.IsTransactionLastMonthNumberOfCreditNull Then
                                Me.LblLastMonthCredit.Text = Format(RowFinancialInfo.TransactionLastMonthNumberOfCredit, "#,#")
                            End If
                            If Not RowFinancialInfo.IsTransactionLastMonthAverageDebitAmountNull Then
                                Me.LblLastMonthAvgDebit.Text = Format(RowFinancialInfo.TransactionLastMonthAverageDebitAmount, "#,#.00")
                            End If
                            If Not RowFinancialInfo.IsTransactionLastMonthAverageCreditAmountNull Then
                                Me.LblLastMonthAvgCredit.Text = Format(RowFinancialInfo.TransactionLastMonthAverageCreditAmount, "#,#.00")
                            End If
                            If Not RowFinancialInfo.IsTransactionLastMonthTotalDebitAmountNull Then
                                Me.LblLastMonthTotalDebit.Text = Format(RowFinancialInfo.TransactionLastMonthTotalDebitAmount, "#,#.00")
                            End If
                            If Not RowFinancialInfo.IsTransactionLastMonthTotalCreditAmountNull Then
                                Me.LblLastMonthTotalCredit.Text = Format(RowFinancialInfo.TransactionLastMonthTotalCreditAmount, "#,#.00")
                            End If

                            If Not RowFinancialInfo.IsTransactionLast3MonthNumberOfDebitNull Then
                                Me.LblLast3MonthDebit.Text = Format(RowFinancialInfo.TransactionLast3MonthNumberOfDebit, "#,#")
                            End If
                            If Not RowFinancialInfo.IsTransactionLast3MonthNumberOfCreditNull Then
                                Me.LblLast3MonthCredit.Text = Format(RowFinancialInfo.TransactionLast3MonthNumberOfCredit, "#,#")
                            End If
                            If Not RowFinancialInfo.IsTransactionLast3MonthAverageDebitAmountNull Then
                                Me.LblLast3MonthAvgDebit.Text = Format(RowFinancialInfo.TransactionLast3MonthAverageDebitAmount, "#,#.00")
                            End If
                            If Not RowFinancialInfo.IsTransactionLast3MonthAverageCreditAmountNull Then
                                Me.LblLast3MonthAvgCredit.Text = Format(RowFinancialInfo.TransactionLast3MonthAverageCreditAmount, "#,#.00")
                            End If
                            If Not RowFinancialInfo.IsTransactionLast3MonthTotalDebitAmountNull Then
                                Me.LblLast3MonthTotalDebit.Text = Format(RowFinancialInfo.TransactionLast3MonthTotalDebitAmount, "#,#.00")
                            End If
                            If Not RowFinancialInfo.IsTransactionLast3MonthTotalCreditAmountNull Then
                                Me.LblLast3MonthTotalCredit.Text = Format(RowFinancialInfo.TransactionLast3MonthTotalCreditAmount, "#,#.00")
                            End If

                            If Not RowFinancialInfo.IsTransactionLast6MonthNumberOfDebitNull Then
                                Me.LblLast6MonthDebit.Text = Format(RowFinancialInfo.TransactionLast6MonthNumberOfDebit, "#,#")
                            End If
                            If Not RowFinancialInfo.IsTransactionLast6MonthNumberOfCreditNull Then
                                Me.LblLast6MonthCredit.Text = Format(RowFinancialInfo.TransactionLast6MonthNumberOfCredit, "#,#")
                            End If
                            If Not RowFinancialInfo.IsTransactionLast6MonthAverageDebitAmountNull Then
                                Me.LblLast6MonthAvgDebit.Text = Format(RowFinancialInfo.TransactionLast6MonthAverageDebitAmount, "#,#.00")
                            End If
                            If Not RowFinancialInfo.IsTransactionLast6MonthAverageCreditAmountNull Then
                                Me.LblLast6MonthAvgCredit.Text = Format(RowFinancialInfo.TransactionLast6MonthAverageCreditAmount, "#,#.00")
                            End If
                            If Not RowFinancialInfo.IsTransactionLast6MonthTotalDebitAmountNull Then
                                Me.LblLast6MonthTotalDebit.Text = Format(RowFinancialInfo.TransactionLast6MonthTotalDebitAmount, "#,#.00")
                            End If
                            If Not RowFinancialInfo.IsTransactionLast6MonthTotalCreditAmountNull Then
                                Me.LblLast6MonthTotalCredit.Text = Format(RowFinancialInfo.TransactionLast6MonthTotalCreditAmount, "#,#.00")
                            End If

                            If Not RowFinancialInfo.IsTransactionLast1YearNumberOfDebitNull Then
                                Me.LblLast1YearDebit.Text = Format(RowFinancialInfo.TransactionLast1YearNumberOfDebit, "#,#")
                            End If
                            If Not RowFinancialInfo.IsTransactionLast1YearNumberOfCreditNull Then
                                Me.LblLast1YearCredit.Text = Format(RowFinancialInfo.TransactionLast1YearNumberOfCredit, "#,#")
                            End If
                            If Not RowFinancialInfo.IsTransactionLast1YearAverageDebitAmountNull Then
                                Me.LblLast1YearAvgDebit.Text = Format(RowFinancialInfo.TransactionLast1YearAverageDebitAmount, "#,#.00")
                            End If
                            If Not RowFinancialInfo.IsTransactionLast1YearAverageCreditAmountNull Then
                                Me.LblLast1YearAvgCredit.Text = Format(RowFinancialInfo.TransactionLast1YearAverageCreditAmount, "#,#.00")
                            End If
                            If Not RowFinancialInfo.IsTransactionLast1YearTotalDebitAmountNull Then
                                Me.LblLast1YearTotalDebit.Text = Format(RowFinancialInfo.TransactionLast1YearTotalDebitAmount, "#,#.00")
                            End If
                            If Not RowFinancialInfo.IsTransactionLast1YearTotalCreditAmountNull Then
                                Me.LblLast1YearTotalCredit.Text = Format(RowFinancialInfo.TransactionLast1YearTotalCreditAmount, "#,#.00")
                            End If

                            If Not RowFinancialInfo.IsTransactionLast2YearNumberOfDebitNull Then
                                Me.LblLast2YearDebit.Text = Format(RowFinancialInfo.TransactionLast2YearNumberOfDebit, "#,#")
                            End If
                            If Not RowFinancialInfo.IsTransactionLast2YearNumberOfCreditNull Then
                                Me.LblLast2YearCredit.Text = Format(RowFinancialInfo.TransactionLast2YearNumberOfCredit, "#,#")
                            End If
                            If Not RowFinancialInfo.IsTransactionLast2YearAverageDebitAmountNull Then
                                Me.LblLast2YearAvgDebit.Text = Format(RowFinancialInfo.TransactionLast2YearAverageDebitAmount, "#,#.00")
                            End If
                            If Not RowFinancialInfo.IsTransactionLast2YearAverageCreditAmountNull Then
                                Me.LblLast2YearAvgCredit.Text = Format(RowFinancialInfo.TransactionLast2YearAverageCreditAmount, "#,#.00")
                            End If
                            If Not RowFinancialInfo.IsTransactionLast2YearTotalDebitAmountNull Then
                                Me.LblLast2YearTotalDebit.Text = Format(RowFinancialInfo.TransactionLast2YearTotalDebitAmount, "#,#.00")
                            End If
                            If Not RowFinancialInfo.IsTransactionLast2YearTotalCreditAmountNull Then
                                Me.LblLast2YearTotalCredit.Text = Format(RowFinancialInfo.TransactionLast2YearTotalCreditAmount, "#,#.00")
                            End If

                        Else
                            Me.TdFinancialInformationAvailable.Visible = False
                            Me.TdFinancialInformationNotAvailable.Visible = True
                        End If
                    End Using
                End Using
            Else
                Using AccessAccountInformation As New AMLDAL.AccountInformationTableAdapters.SelectAccountFinancialInfoByAccountNoTableAdapter
                    Using dtDetailFinancialInfo As Data.DataTable = AccessAccountInformation.GetAccountFinancialInfoByAccountNo(Me.GetAccountNo)
                        If dtDetailFinancialInfo.Rows.Count > 0 Then
                            Dim RowFinancialInfo As AMLDAL.AccountInformation.SelectAccountFinancialInfoByAccountNoRow = dtDetailFinancialInfo.Rows(0)
                            ' set visibility
                            Me.TdFinancialInformationAvailable.Visible = True
                            Me.TdFinancialInformationNotAvailable.Visible = False

                            If Not RowFinancialInfo.IsCurrentBalanceNull Then
                                Me.LblCurBalance.Text = Format(RowFinancialInfo.CurrentBalance, "#,#.00")
                            End If

                            If Not RowFinancialInfo.IsTransactionYesterdayNumberOfDebitNull Then
                                Me.LblYesterdayDebit.Text = Format(RowFinancialInfo.TransactionYesterdayNumberOfDebit, "#,#")
                            End If
                            If Not RowFinancialInfo.IsTransactionYesterdayNumberOfCreditNull Then
                                Me.LblYesterdayCredit.Text = Format(RowFinancialInfo.TransactionYesterdayNumberOfCredit, "#,#")
                            End If
                            If Not RowFinancialInfo.IsTransactionYesterdayAverageDebitAmountNull Then
                                Me.LblYesterdayAvgDebit.Text = Format(RowFinancialInfo.TransactionYesterdayAverageDebitAmount, "#,#.00")
                            End If
                            If Not RowFinancialInfo.IsTransactionYesterdayAverageCreditAmountNull Then
                                Me.LblYesterdayAvgCredit.Text = Format(RowFinancialInfo.TransactionYesterdayAverageCreditAmount, "#,#.00")
                            End If
                            If Not RowFinancialInfo.IsTransactionYesterdayTotalDebitAmountNull Then
                                Me.LblYesterdayTotalDebit.Text = Format(RowFinancialInfo.TransactionYesterdayTotalDebitAmount, "#,#.00")
                            End If
                            If Not RowFinancialInfo.IsTransactionYesterdayTotalCreditAmountNull Then
                                Me.LblYesterdayTotalCredit.Text = Format(RowFinancialInfo.TransactionYesterdayTotalCreditAmount, "#,#.00")
                            End If

                            If Not RowFinancialInfo.IsTransactionLastWeekNumberOfDebitNull Then
                                Me.LblLastWeekDebit.Text = Format(RowFinancialInfo.TransactionLastWeekNumberOfDebit, "#,#")
                            End If
                            If Not RowFinancialInfo.IsTransactionLastWeekNumberOfCreditNull Then
                                Me.LblLastWeekCredit.Text = Format(RowFinancialInfo.TransactionLastWeekNumberOfCredit, "#,#")
                            End If
                            If Not RowFinancialInfo.IsTransactionLastWeekAverageDebitAmountNull Then
                                Me.LblLastWeekAvgDebit.Text = Format(RowFinancialInfo.TransactionLastWeekAverageDebitAmount, "#,#.00")
                            End If
                            If Not RowFinancialInfo.IsTransactionLastWeekAverageCreditAmountNull Then
                                Me.LblLastWeekAvgCredit.Text = Format(RowFinancialInfo.TransactionLastWeekAverageCreditAmount, "#,#.00")
                            End If
                            If Not RowFinancialInfo.IsTransactionLastWeekTotalDebitAmountNull Then
                                Me.LblLastWeekTotalDebit.Text = Format(RowFinancialInfo.TransactionLastWeekTotalDebitAmount, "#,#.00")
                            End If
                            If Not RowFinancialInfo.IsTransactionLastWeekTotalCreditAmountNull Then
                                Me.LblLastWeekTotalCredit.Text = Format(RowFinancialInfo.TransactionLastWeekTotalCreditAmount, "#,#.00")
                            End If

                            If Not RowFinancialInfo.IsTransactionLastMonthNumberOfDebitNull Then
                                Me.LblLastMonthDebit.Text = Format(RowFinancialInfo.TransactionLastMonthNumberOfDebit, "#,#")
                            End If
                            If Not RowFinancialInfo.IsTransactionLastMonthNumberOfCreditNull Then
                                Me.LblLastMonthCredit.Text = Format(RowFinancialInfo.TransactionLastMonthNumberOfCredit, "#,#")
                            End If
                            If Not RowFinancialInfo.IsTransactionLastMonthAverageDebitAmountNull Then
                                Me.LblLastMonthAvgDebit.Text = Format(RowFinancialInfo.TransactionLastMonthAverageDebitAmount, "#,#.00")
                            End If
                            If Not RowFinancialInfo.IsTransactionLastMonthAverageCreditAmountNull Then
                                Me.LblLastMonthAvgCredit.Text = Format(RowFinancialInfo.TransactionLastMonthAverageCreditAmount, "#,#.00")
                            End If
                            If Not RowFinancialInfo.IsTransactionLastMonthTotalDebitAmountNull Then
                                Me.LblLastMonthTotalDebit.Text = Format(RowFinancialInfo.TransactionLastMonthTotalDebitAmount, "#,#.00")
                            End If
                            If Not RowFinancialInfo.IsTransactionLastMonthTotalCreditAmountNull Then
                                Me.LblLastMonthTotalCredit.Text = Format(RowFinancialInfo.TransactionLastMonthTotalCreditAmount, "#,#.00")
                            End If

                            If Not RowFinancialInfo.IsTransactionLast3MonthNumberOfDebitNull Then
                                Me.LblLast3MonthDebit.Text = Format(RowFinancialInfo.TransactionLast3MonthNumberOfDebit, "#,#")
                            End If
                            If Not RowFinancialInfo.IsTransactionLast3MonthNumberOfCreditNull Then
                                Me.LblLast3MonthCredit.Text = Format(RowFinancialInfo.TransactionLast3MonthNumberOfCredit, "#,#")
                            End If
                            If Not RowFinancialInfo.IsTransactionLast3MonthAverageDebitAmountNull Then
                                Me.LblLast3MonthAvgDebit.Text = Format(RowFinancialInfo.TransactionLast3MonthAverageDebitAmount, "#,#.00")
                            End If
                            If Not RowFinancialInfo.IsTransactionLast3MonthAverageCreditAmountNull Then
                                Me.LblLast3MonthAvgCredit.Text = Format(RowFinancialInfo.TransactionLast3MonthAverageCreditAmount, "#,#.00")
                            End If
                            If Not RowFinancialInfo.IsTransactionLast3MonthTotalDebitAmountNull Then
                                Me.LblLast3MonthTotalDebit.Text = Format(RowFinancialInfo.TransactionLast3MonthTotalDebitAmount, "#,#.00")
                            End If
                            If Not RowFinancialInfo.IsTransactionLast3MonthTotalCreditAmountNull Then
                                Me.LblLast3MonthTotalCredit.Text = Format(RowFinancialInfo.TransactionLast3MonthTotalCreditAmount, "#,#.00")
                            End If

                            If Not RowFinancialInfo.IsTransactionLast6MonthNumberOfDebitNull Then
                                Me.LblLast6MonthDebit.Text = Format(RowFinancialInfo.TransactionLast6MonthNumberOfDebit, "#,#")
                            End If
                            If Not RowFinancialInfo.IsTransactionLast6MonthNumberOfCreditNull Then
                                Me.LblLast6MonthCredit.Text = Format(RowFinancialInfo.TransactionLast6MonthNumberOfCredit, "#,#")
                            End If
                            If Not RowFinancialInfo.IsTransactionLast6MonthAverageDebitAmountNull Then
                                Me.LblLast6MonthAvgDebit.Text = Format(RowFinancialInfo.TransactionLast6MonthAverageDebitAmount, "#,#.00")
                            End If
                            If Not RowFinancialInfo.IsTransactionLast6MonthAverageCreditAmountNull Then
                                Me.LblLast6MonthAvgCredit.Text = Format(RowFinancialInfo.TransactionLast6MonthAverageCreditAmount, "#,#.00")
                            End If
                            If Not RowFinancialInfo.IsTransactionLast6MonthTotalDebitAmountNull Then
                                Me.LblLast6MonthTotalDebit.Text = Format(RowFinancialInfo.TransactionLast6MonthTotalDebitAmount, "#,#.00")
                            End If
                            If Not RowFinancialInfo.IsTransactionLast6MonthTotalCreditAmountNull Then
                                Me.LblLast6MonthTotalCredit.Text = Format(RowFinancialInfo.TransactionLast6MonthTotalCreditAmount, "#,#.00")
                            End If

                            If Not RowFinancialInfo.IsTransactionLast1YearNumberOfDebitNull Then
                                Me.LblLast1YearDebit.Text = Format(RowFinancialInfo.TransactionLast1YearNumberOfDebit, "#,#")
                            End If
                            If Not RowFinancialInfo.IsTransactionLast1YearNumberOfCreditNull Then
                                Me.LblLast1YearCredit.Text = Format(RowFinancialInfo.TransactionLast1YearNumberOfCredit, "#,#")
                            End If
                            If Not RowFinancialInfo.IsTransactionLast1YearAverageDebitAmountNull Then
                                Me.LblLast1YearAvgDebit.Text = Format(RowFinancialInfo.TransactionLast1YearAverageDebitAmount, "#,#.00")
                            End If
                            If Not RowFinancialInfo.IsTransactionLast1YearAverageCreditAmountNull Then
                                Me.LblLast1YearAvgCredit.Text = Format(RowFinancialInfo.TransactionLast1YearAverageCreditAmount, "#,#.00")
                            End If
                            If Not RowFinancialInfo.IsTransactionLast1YearTotalDebitAmountNull Then
                                Me.LblLast1YearTotalDebit.Text = Format(RowFinancialInfo.TransactionLast1YearTotalDebitAmount, "#,#.00")
                            End If
                            If Not RowFinancialInfo.IsTransactionLast1YearTotalCreditAmountNull Then
                                Me.LblLast1YearTotalCredit.Text = Format(RowFinancialInfo.TransactionLast1YearTotalCreditAmount, "#,#.00")
                            End If

                            If Not RowFinancialInfo.IsTransactionLast2YearNumberOfDebitNull Then
                                Me.LblLast2YearDebit.Text = Format(RowFinancialInfo.TransactionLast2YearNumberOfDebit, "#,#")
                            End If
                            If Not RowFinancialInfo.IsTransactionLast2YearNumberOfCreditNull Then
                                Me.LblLast2YearCredit.Text = Format(RowFinancialInfo.TransactionLast2YearNumberOfCredit, "#,#")
                            End If
                            If Not RowFinancialInfo.IsTransactionLast2YearAverageDebitAmountNull Then
                                Me.LblLast2YearAvgDebit.Text = Format(RowFinancialInfo.TransactionLast2YearAverageDebitAmount, "#,#.00")
                            End If
                            If Not RowFinancialInfo.IsTransactionLast2YearAverageCreditAmountNull Then
                                Me.LblLast2YearAvgCredit.Text = Format(RowFinancialInfo.TransactionLast2YearAverageCreditAmount, "#,#.00")
                            End If
                            If Not RowFinancialInfo.IsTransactionLast2YearTotalDebitAmountNull Then
                                Me.LblLast2YearTotalDebit.Text = Format(RowFinancialInfo.TransactionLast2YearTotalDebitAmount, "#,#.00")
                            End If
                            If Not RowFinancialInfo.IsTransactionLast2YearTotalCreditAmountNull Then
                                Me.LblLast2YearTotalCredit.Text = Format(RowFinancialInfo.TransactionLast2YearTotalCreditAmount, "#,#.00")
                            End If

                        Else
                            Me.TdFinancialInformationAvailable.Visible = False
                            Me.TdFinancialInformationNotAvailable.Visible = True
                        End If
                    End Using
                End Using
            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Function IsValidDataAccess(ByVal StrAccountNo As String) As Boolean
        Dim StrTables As String
        Dim StrFilter As String
        If Not StrAccountNo Is Nothing Then
            If GetSource.ToLower = "jive" Then
                StrTables = "AllAccount_WebTempTable_JIVE INNER JOIN CFMAST ON AllAccount_WebTempTable_JIVE.CIFNo=CFMAST.CFCIF#"
                StrFilter = "AllAccount_WebTempTable_JIVE.AccountNo='" & StrAccountNo.Replace("'", "''") & "' AND (CFMAST.CFINSC IN (SELECT InsiderCodeId FROM GroupInsiderCodeMapping WHERE GroupId= (SELECT UserGroupId FROM [User] WHERE UserId = '" & Sahassa.AML.Commonly.SessionUserId & "')) OR CFMAST.CFINSC='' OR CFMAST.CFINSC IS NULL)"
                If Sahassa.AML.Commonly.GetTableCount(StrTables, StrFilter) > 0 Then
                    Return True
                End If
            Else
                StrTables = "AllAccount_WebTempTable INNER JOIN CFMAST ON AllAccount_WebTempTable.CIFNo=CFMAST.CFCIF#"
                StrFilter = "AllAccount_WebTempTable.AccountNo='" & StrAccountNo.Replace("'", "''") & "' AND (CFMAST.CFINSC IN (SELECT InsiderCodeId FROM GroupInsiderCodeMapping WHERE GroupId= (SELECT UserGroupId FROM [User] WHERE UserId = '" & Sahassa.AML.Commonly.SessionUserId & "')) OR CFMAST.CFINSC='' OR CFMAST.CFINSC IS NULL)"
                If Sahassa.AML.Commonly.GetTableCount(StrTables, StrFilter) > 0 Then
                    Return True
                End If
            End If
        End If
    End Function

    Protected Sub SelectAuxTrxCodeMultiple(ByVal strVal As String)
        Try
            If (strVal.Trim <> String.Empty) Then
                HfAuxTrxCode.Value = strVal

                Dim tmpStr As String = String.Empty
                Dim al As New ArrayList
                Dim i As Integer

                al.AddRange(strVal.Split(";"c))

                For i = 0 To al.Count - 1
                    Dim ObjList_vw_AuxTransactionCode As VList(Of vw_AuxTransactionCode) = DataRepository.vw_AuxTransactionCodeProvider.GetPaged(vw_AuxTransactionCodeColumn.TransactionCode.ToString & "='" & al(i).ToString & "'", "", 0, Integer.MaxValue, 0)
                    If Not ObjList_vw_AuxTransactionCode Is Nothing Then
                        If ObjList_vw_AuxTransactionCode.Count > 0 Then

                            tmpStr = tmpStr & ObjList_vw_AuxTransactionCode(0).TransactionCode & " " & ObjList_vw_AuxTransactionCode(0).TransactionDescription & ";"
                        End If
                    End If
                Next

                TxtAuxTrxCode.Text = tmpStr
            Else
                HfAuxTrxCode.Value = String.Empty
                TxtAuxTrxCode.Text = String.Empty
            End If

            PickerAuxTrxCodeMultiple1.CloseData()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    ''' <summary>
    ''' Page Load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            AddHandler PickerAuxTrxCodeMultiple1.SelectAuxTrxCodeMultiple, AddressOf SelectAuxTrxCodeMultiple
            If IsValidDataAccess(Me.GetAccountNo) Then
                If Not IsPostBack Then
                    ClearThisPageSessions()

                    If GetSource.ToLower = "jive" Then

                    Else
                        If Not IsNothing(ObjAllAccountAllinfo) Then
                            If Me.ObjAllAccountAllinfo.AccountType.ToLower = "c" Then
                                Menu1.Items.Add(New MenuItem("Credit Card Information", "9"))
                            ElseIf Me.ObjAllAccountAllinfo.AccountType.ToLower = "l" Then
                                Menu1.Items.Add(New MenuItem("Loan Information", "8"))
                            End If
                        End If
                    End If


                    Me.LoadAccountMessage()
                    Me.LoadDetailGeneral()
                    Me.popUp1.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TxtDate1.ClientID & "'), 'dd-mmm-yyyy')")
                    Me.PopUp2.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TxtDate2.ClientID & "'), 'dd-mmm-yyyy')")
                End If
            Else
                Me.Menu1.Visible = False
                Me.MultiView1.Visible = False
                Throw New Exception("Account not found or you do not have access to this account data.")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    ''' <summary>
    ''' Tab KYC Information Active
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub TabKYCInformation_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabKYCInformation.Activate
        Try
            Me.LoadDetailKYCInformation()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    'Protected Sub ImageBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click
    '    Me.Response.Redirect("AccountInformation.aspx", False)
    'End Sub

    Protected Sub TabFinancialInformation_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabFinancialInformation.Activate
        Try
            Me.FinancialInformationAvailable()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub TabTransactionDetail_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabTransactionDetail.Activate
        Try
            Me.GridTransDetail.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow
            Me.SetAndGetSearchingCriteria = Me.SetAllSearchingCriteria

            'Me.BindGrid()
            'Me.SetInfoNavigate()
            'Me.ChartByDate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Private Sub BindPeerGroup()
        Try
            Using adapter As New AMLDAL.PeerGroupAnalysisTableAdapters.PeerGroupTableAdapter
                cboPeerGroupName.DataSource = adapter.GetData
                cboPeerGroupName.DataTextField = "PeerGroupName"
                cboPeerGroupName.DataValueField = "PeerGroupId"
                cboPeerGroupName.DataBind()
            End Using
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Sub
    Private Sub BindCharacteristic()
        Try
            Using adapter As New AMLDAL.PeerGroupAnalysisTableAdapters.PeerGroupCharacteristicTableAdapter
                cboPeerGroupChar.DataSource = adapter.GetData
                cboPeerGroupChar.DataTextField = "PeerGroupCharacteristicName"
                cboPeerGroupChar.DataValueField = "PK_PeerGroupCharacteristicId"
                cboPeerGroupChar.DataBind()
            End Using
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Sub
    Protected Sub TabPeerGroupAnalysis_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabPeerGroupAnalysis.Activate
        Try
            BindPeerGroup()
            BindCharacteristic()
            ChartControlPeerGroup.Visible = False
            ChartControlAccount.Visible = False
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
    Private Function GenerateDatasetPeerGroupAll(ByVal orowPeerGroupAll As AMLDAL.PeerGroupAnalysis.PeerGroupStatisticDataRow) As System.Data.DataTable

        Dim dt As New System.Data.DataTable
        Dim dr As System.Data.DataRow
        Try
            dt.Columns.Add(New System.Data.DataColumn("Tanggal", System.Type.GetType("System.DateTime")))
            dt.Columns.Add(New System.Data.DataColumn("Char1", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New System.Data.DataColumn("Char2", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New System.Data.DataColumn("Char3", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New System.Data.DataColumn("Char4", System.Type.GetType("System.Decimal")))



            If Not orowPeerGroupAll.IsStatisticDate1Null Then
                dr = dt.NewRow()
                dr("tanggal") = orowPeerGroupAll.StatisticDate1

                If Not orowPeerGroupAll.IsValue11Null Then
                    dr("Char1") = orowPeerGroupAll.Value11
                Else
                    dr("Char1") = 0
                End If
                If Not orowPeerGroupAll.IsValue12Null Then
                    dr("Char2") = orowPeerGroupAll.Value12
                Else
                    dr("Char2") = 0
                End If
                If Not orowPeerGroupAll.IsValue13Null Then
                    dr("Char3") = orowPeerGroupAll.Value13
                Else
                    dr("Char3") = 0
                End If
                If Not orowPeerGroupAll.IsValue14Null Then
                    dr("Char4") = orowPeerGroupAll.Value14
                Else
                    dr("Char4") = 0
                End If
                dt.Rows.Add(dr)
            End If


            If Not orowPeerGroupAll.IsStatisticDate2Null Then
                dr = dt.NewRow()
                dr("tanggal") = orowPeerGroupAll.StatisticDate2

                If Not orowPeerGroupAll.IsValue21Null Then
                    dr("Char1") = orowPeerGroupAll.Value21
                Else
                    dr("Char1") = 0
                End If
                If Not orowPeerGroupAll.IsValue22Null Then
                    dr("Char2") = orowPeerGroupAll.Value22
                Else
                    dr("Char2") = 0
                End If
                If Not orowPeerGroupAll.IsValue23Null Then
                    dr("Char3") = orowPeerGroupAll.Value23
                Else
                    dr("Char3") = 0
                End If
                If Not orowPeerGroupAll.IsValue24Null Then
                    dr("Char4") = orowPeerGroupAll.Value24
                Else
                    dr("Char4") = 0
                End If
                dt.Rows.Add(dr)
            End If

            If Not orowPeerGroupAll.IsStatisticDate3Null Then
                dr = dt.NewRow()
                dr("tanggal") = orowPeerGroupAll.StatisticDate3

                If Not orowPeerGroupAll.IsValue31Null Then
                    dr("Char1") = orowPeerGroupAll.Value31
                Else
                    dr("Char1") = 0
                End If
                If Not orowPeerGroupAll.IsValue32Null Then
                    dr("Char2") = orowPeerGroupAll.Value32
                Else
                    dr("Char2") = 0
                End If
                If Not orowPeerGroupAll.IsValue33Null Then
                    dr("Char3") = orowPeerGroupAll.Value33
                Else
                    dr("Char3") = 0
                End If
                If Not orowPeerGroupAll.IsValue34Null Then
                    dr("Char4") = orowPeerGroupAll.Value34
                Else
                    dr("Char4") = 0
                End If
                dt.Rows.Add(dr)
            End If

            'If Not orowPeerGroupAll.IsStatisticDate4Null Then
            '    dr = dt.NewRow()
            '    dr("tanggal") = orowPeerGroupAll.StatisticDate4

            '    If Not orowPeerGroupAll.IsValue41Null Then
            '        dr("Char1") = orowPeerGroupAll.Value41
            '    End If
            '    If Not orowPeerGroupAll.IsValue42Null Then
            '        dr("Char2") = orowPeerGroupAll.Value42
            '    End If
            '    If Not orowPeerGroupAll.IsValue43Null Then
            '        dr("Char3") = orowPeerGroupAll.Value43
            '    End If
            '    If Not orowPeerGroupAll.IsValue44Null Then
            '        dr("Char4") = orowPeerGroupAll.Value44
            '    End If
            '    dt.Rows.Add(dr)
            'End If

            If Not orowPeerGroupAll.IsStatisticDate4Null Then
                dr = dt.NewRow()
                dr("tanggal") = orowPeerGroupAll.StatisticDate4

                If Not orowPeerGroupAll.IsValue41Null Then
                    dr("Char1") = orowPeerGroupAll.Value41
                Else
                    dr("Char4") = 0
                End If
                If Not orowPeerGroupAll.IsValue42Null Then
                    dr("Char2") = orowPeerGroupAll.Value42
                Else
                    dr("Char4") = 0
                End If
                If Not orowPeerGroupAll.IsValue43Null Then
                    dr("Char3") = orowPeerGroupAll.Value43
                Else
                    dr("Char4") = 0
                End If
                If Not orowPeerGroupAll.IsValue44Null Then
                    dr("Char4") = orowPeerGroupAll.Value44
                Else
                    dr("Char4") = 0
                End If
                dt.Rows.Add(dr)
            End If

            If Not orowPeerGroupAll.IsStatisticDate5Null Then
                dr = dt.NewRow()
                dr("tanggal") = orowPeerGroupAll.StatisticDate5

                If Not orowPeerGroupAll.IsValue51Null Then
                    dr("Char1") = orowPeerGroupAll.Value51
                Else
                    dr("Char1") = 0
                End If
                If Not orowPeerGroupAll.IsValue52Null Then
                    dr("Char2") = orowPeerGroupAll.Value52
                Else
                    dr("Char2") = 0
                End If
                If Not orowPeerGroupAll.IsValue53Null Then
                    dr("Char3") = orowPeerGroupAll.Value53
                Else
                    dr("Char3") = 0
                End If
                If Not orowPeerGroupAll.IsValue54Null Then
                    dr("Char4") = orowPeerGroupAll.Value54
                Else
                    dr("Char4") = 0
                End If
                dt.Rows.Add(dr)
            End If

            If Not orowPeerGroupAll.IsStatisticDate6Null Then
                dr = dt.NewRow()
                dr("tanggal") = orowPeerGroupAll.StatisticDate6

                If Not orowPeerGroupAll.IsValue61Null Then
                    dr("Char1") = orowPeerGroupAll.Value61
                Else
                    dr("Char1") = 0
                End If
                If Not orowPeerGroupAll.IsValue62Null Then
                    dr("Char2") = orowPeerGroupAll.Value62
                Else
                    dr("Char2") = 0
                End If
                If Not orowPeerGroupAll.IsValue63Null Then
                    dr("Char3") = orowPeerGroupAll.Value63
                Else
                    dr("Char3") = 0
                End If
                If Not orowPeerGroupAll.IsValue64Null Then
                    dr("Char4") = orowPeerGroupAll.Value64
                Else
                    dr("Char4") = 0
                End If
                dt.Rows.Add(dr)
            End If

            Return dt
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Function
    Private Function GenerateDatasetAccount(ByVal orowaccount As AMLDAL.PeerGroupAnalysis.PeerGroupAccountDataRow) As System.Data.DataTable

        Dim dt As New System.Data.DataTable
        Dim dr As System.Data.DataRow
        Try
            dt.Columns.Add(New System.Data.DataColumn("Tanggal", System.Type.GetType("System.DateTime")))
            dt.Columns.Add(New System.Data.DataColumn("Char1", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New System.Data.DataColumn("Char2", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New System.Data.DataColumn("Char3", System.Type.GetType("System.Decimal")))
            dt.Columns.Add(New System.Data.DataColumn("Char4", System.Type.GetType("System.Decimal")))



            If Not orowaccount.IsStatisticDate1Null Then
                dr = dt.NewRow()
                dr("tanggal") = orowaccount.StatisticDate1

                If Not orowaccount.IsValue11Null Then
                    dr("Char1") = orowaccount.Value11
                Else
                    dr("Char1") = 0
                End If
                If Not orowaccount.IsValue12Null Then
                    dr("Char2") = orowaccount.Value12
                Else
                    dr("Char2") = 0
                End If
                If Not orowaccount.IsValue13Null Then
                    dr("Char3") = orowaccount.Value13
                Else
                    dr("Char3") = 0
                End If
                If Not orowaccount.IsValue14Null Then
                    dr("Char4") = orowaccount.Value14
                Else
                    dr("Char4") = 0
                End If
                dt.Rows.Add(dr)
            End If


            If Not orowaccount.IsStatisticDate2Null Then
                dr = dt.NewRow()
                dr("tanggal") = orowaccount.StatisticDate2

                If Not orowaccount.IsValue21Null Then
                    dr("Char1") = orowaccount.Value21
                Else
                    dr("Char1") = 0
                End If
                If Not orowaccount.IsValue22Null Then
                    dr("Char2") = orowaccount.Value22
                Else
                    dr("Char2") = 0
                End If
                If Not orowaccount.IsValue23Null Then
                    dr("Char3") = orowaccount.Value23
                Else
                    dr("Char3") = 0
                End If
                If Not orowaccount.IsValue24Null Then
                    dr("Char4") = orowaccount.Value24
                Else
                    dr("Char4") = 0
                End If
                dt.Rows.Add(dr)
            End If

            If Not orowaccount.IsStatisticDate3Null Then
                dr = dt.NewRow()
                dr("tanggal") = orowaccount.StatisticDate3

                If Not orowaccount.IsValue31Null Then
                    dr("Char1") = orowaccount.Value31
                Else
                    dr("Char1") = 0
                End If
                If Not orowaccount.IsValue32Null Then
                    dr("Char2") = orowaccount.Value32
                Else
                    dr("Char2") = 0
                End If
                If Not orowaccount.IsValue33Null Then
                    dr("Char3") = orowaccount.Value33
                Else
                    dr("Char3") = 0
                End If
                If Not orowaccount.IsValue34Null Then
                    dr("Char4") = orowaccount.Value34
                Else
                    dr("Char4") = 0
                End If
                dt.Rows.Add(dr)
            End If

            'If Not orowaccount.IsStatisticDate4Null Then
            '    dr = dt.NewRow()
            '    dr("tanggal") = orowaccount.StatisticDate4

            '    If Not orowaccount.IsValue41Null Then
            '        dr("Char1") = orowaccount.Value41
            '    End If
            '    If Not orowaccount.IsValue42Null Then
            '        dr("Char2") = orowaccount.Value42
            '    End If
            '    If Not orowaccount.IsValue43Null Then
            '        dr("Char3") = orowaccount.Value43
            '    End If
            '    If Not orowaccount.IsValue44Null Then
            '        dr("Char4") = orowaccount.Value44
            '    End If
            '    dt.Rows.Add(dr)
            'End If

            If Not orowaccount.IsStatisticDate4Null Then
                dr = dt.NewRow()
                dr("tanggal") = orowaccount.StatisticDate4

                If Not orowaccount.IsValue41Null Then
                    dr("Char1") = orowaccount.Value41
                Else
                    dr("Char1") = 0
                End If
                If Not orowaccount.IsValue42Null Then
                    dr("Char2") = orowaccount.Value42
                Else
                    dr("Char2") = 0
                End If
                If Not orowaccount.IsValue43Null Then
                    dr("Char3") = orowaccount.Value43
                Else
                    dr("Char3") = 0
                End If
                If Not orowaccount.IsValue44Null Then
                    dr("Char4") = orowaccount.Value44
                Else
                    dr("Char4") = 0
                End If
                dt.Rows.Add(dr)
            End If

            If Not orowaccount.IsStatisticDate5Null Then
                dr = dt.NewRow()
                dr("tanggal") = orowaccount.StatisticDate5

                If Not orowaccount.IsValue51Null Then
                    dr("Char1") = orowaccount.Value51
                Else
                    dr("Char1") = 0
                End If
                If Not orowaccount.IsValue52Null Then
                    dr("Char2") = orowaccount.Value52
                Else
                    dr("Char2") = 0
                End If
                If Not orowaccount.IsValue53Null Then
                    dr("Char3") = orowaccount.Value53
                Else
                    dr("Char3") = 0
                End If
                If Not orowaccount.IsValue54Null Then
                    dr("Char4") = orowaccount.Value54
                Else
                    dr("Char4") = 0
                End If
                dt.Rows.Add(dr)
            End If

            If Not orowaccount.IsStatisticDate6Null Then
                dr = dt.NewRow()
                dr("tanggal") = orowaccount.StatisticDate6

                If Not orowaccount.IsValue61Null Then
                    dr("Char1") = orowaccount.Value61
                Else
                    dr("Char1") = 0
                End If
                If Not orowaccount.IsValue62Null Then
                    dr("Char2") = orowaccount.Value62
                Else
                    dr("Char2") = 0
                End If
                If Not orowaccount.IsValue63Null Then
                    dr("Char3") = orowaccount.Value63
                Else
                    dr("Char3") = 0
                End If
                If Not orowaccount.IsValue64Null Then
                    dr("Char4") = orowaccount.Value64
                Else
                    dr("Char4") = 0
                End If
                dt.Rows.Add(dr)
            End If

            Return dt
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Function

    Private Function GetDoubleValue(ByVal Value As Object) As Double
        Dim DblReturn As Double
        DblReturn = 0
        If Not IsDBNull(Value) Then
            DblReturn = CType(Value, Double)
        End If
        Return DblReturn
    End Function

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        'Dim oChartArea As WebChart.AreaChart
        'Dim oChartAreaAll As WebChart.AreaChart
        Dim oChartArea As WebChart.ColumnChart
        Dim oChartAreaAll As WebChart.ColumnChart
        Dim DblMaxValue As Double = 0
        Try
            ChartControlAccount.Visible = True
            ChartControlPeerGroup.Visible = True

            Using adapterAccount As New AMLDAL.PeerGroupAnalysisTableAdapters.PeerGroupAccountDataTableAdapter
                Using otable As AMLDAL.PeerGroupAnalysis.PeerGroupAccountDataDataTable = adapterAccount.GetDataByAccountAndPeerGroup(Me.GetAccountNo, cboPeerGroupName.SelectedValue)
                    If otable.Rows.Count > 0 Then
                        Dim orowAccount As AMLDAL.PeerGroupAnalysis.PeerGroupAccountDataRow = otable.Rows(0)
                        Dim dt As System.Data.DataTable = GenerateDatasetAccount(orowAccount)
                        Dim dr() As System.Data.DataRow = dt.Select("", "tanggal asc")
                        If dr.Length > 0 Then
                            oChartArea = ChartControlAccount.Charts(0)
                            oChartArea.DataLabels.Visible = True
                            oChartArea.Fill.Color = Drawing.Color.Blue
                            For i As Integer = 0 To dr.Length - 1

                                Select Case cboPeerGroupChar.SelectedValue
                                    Case EnCharacteristic.TransactionVolume
                                        oChartArea.Data.Add(New WebChart.ChartPoint(Format(dr(i)("Tanggal"), "MMM yyyy"), dr(i)("Char1")))
                                        'oChartArea.Data.Add(New WebChart.ChartPoint(Format(dr(i)("Tanggal"), "MMM yyyy"), (CType(dr(i)("Char1"), Decimal) / 1000).ToString("#,#.00")))


                                        If DblMaxValue < Me.GetDoubleValue(dr(i)("Char1")) Then
                                            DblMaxValue = Me.GetDoubleValue(dr(i)("Char1"))
                                        End If

                                    Case EnCharacteristic.TransactionValue
                                        oChartArea.Data.Add(New WebChart.ChartPoint(Format(dr(i)("Tanggal"), "MMM yyyy"), dr(i)("Char2")))
                                        'oChartArea.Data.Add(New WebChart.ChartPoint(Format(dr(i)("Tanggal"), "MMM yyyy"), (CType(dr(i)("Char2"), Decimal) / 1000).ToString("#,#.00")))
                                        If DblMaxValue < Me.GetDoubleValue(dr(i)("Char2")) Then
                                            DblMaxValue = Me.GetDoubleValue(dr(i)("Char2"))
                                        End If
                                    Case EnCharacteristic.TransactionVelocity
                                        oChartArea.Data.Add(New WebChart.ChartPoint(Format(dr(i)("Tanggal"), "MMM yyyy"), dr(i)("Char3")))
                                        'oChartArea.Data.Add(New WebChart.ChartPoint(Format(dr(i)("Tanggal"), "MMM yyyy"), (CType(dr(i)("Char3"), Decimal) / 1000).ToString("#,#.00")))
                                        If DblMaxValue < Me.GetDoubleValue(dr(i)("Char3")) Then
                                            DblMaxValue = Me.GetDoubleValue(dr(i)("Char3"))
                                        End If
                                    Case EnCharacteristic.TransactionNormalDeviation
                                        oChartArea.Data.Add(New WebChart.ChartPoint(Format(dr(i)("Tanggal"), "MMM yyyy"), dr(i)("Char4")))
                                        'oChartArea.Data.Add(New WebChart.ChartPoint(Format(dr(i)("Tanggal"), "MMM yyyy"), (CType(dr(i)("Char4"), Decimal) / 1000).ToString("#,#.00")))
                                        If DblMaxValue < Me.GetDoubleValue(dr(i)("Char4")) Then
                                            DblMaxValue = Me.GetDoubleValue(dr(i)("Char4"))
                                        End If
                                End Select

                            Next
                            ChartControlAccount.DefaultImageUrl = Server.MapPath("~/Images/nodata.GIF")
                        End If
                    End If
                End Using
            End Using

            Using adapterAll As New AMLDAL.PeerGroupAnalysisTableAdapters.PeerGroupStatisticDataTableAdapter
                Using otable As AMLDAL.PeerGroupAnalysis.PeerGroupStatisticDataDataTable = adapterAll.GetDataByPeerGroup(cboPeerGroupName.SelectedValue)
                    If otable.Rows.Count > 0 Then
                        Dim orowAll As AMLDAL.PeerGroupAnalysis.PeerGroupStatisticDataRow = otable.Rows(0)
                        Dim dt As System.Data.DataTable = GenerateDatasetPeerGroupAll(orowAll)
                        Dim dr() As System.Data.DataRow = dt.Select("", "tanggal asc")
                        If dr.Length > 0 Then
                            oChartAreaAll = ChartControlPeerGroup.Charts(0)
                            oChartAreaAll.DataLabels.Visible = True
                            oChartAreaAll.Fill.Color = Drawing.Color.Blue
                            For i As Integer = 0 To dr.Length - 1

                                Select Case cboPeerGroupChar.SelectedValue
                                    Case EnCharacteristic.TransactionVolume
                                        oChartAreaAll.Data.Add(New WebChart.ChartPoint(Format(dr(i)("Tanggal"), "MMM yyyy"), dr(i)("Char1")))
                                        If DblMaxValue < Me.GetDoubleValue(dr(i)("Char1")) Then
                                            DblMaxValue = Me.GetDoubleValue(dr(i)("Char1"))
                                        End If
                                    Case EnCharacteristic.TransactionValue
                                        oChartAreaAll.Data.Add(New WebChart.ChartPoint(Format(dr(i)("Tanggal"), "MMM yyyy"), dr(i)("Char2")))
                                        If DblMaxValue < Me.GetDoubleValue(dr(i)("Char2")) Then
                                            DblMaxValue = Me.GetDoubleValue(dr(i)("Char2"))
                                        End If
                                    Case EnCharacteristic.TransactionVelocity
                                        oChartAreaAll.Data.Add(New WebChart.ChartPoint(Format(dr(i)("Tanggal"), "MMM yyyy"), dr(i)("Char3")))
                                        If DblMaxValue < Me.GetDoubleValue(dr(i)("Char3")) Then
                                            DblMaxValue = Me.GetDoubleValue(dr(i)("Char3"))
                                        End If
                                    Case EnCharacteristic.TransactionNormalDeviation
                                        oChartAreaAll.Data.Add(New WebChart.ChartPoint(Format(dr(i)("Tanggal"), "MMM yyyy"), dr(i)("Char4")))
                                        If DblMaxValue < Me.GetDoubleValue(dr(i)("Char4")) Then
                                            DblMaxValue = Me.GetDoubleValue(dr(i)("Char4"))
                                        End If
                                End Select

                            Next
                            ChartControlPeerGroup.DefaultImageUrl = Server.MapPath("~/Images/nodata.GIF")
                        End If
                    End If
                End Using
            End Using
            ChartControlAccount.YCustomEnd = DblMaxValue * 1.5
            ChartControlPeerGroup.YCustomEnd = DblMaxValue * 1.5
            ChartControlAccount.RedrawChart()
            ChartControlPeerGroup.RedrawChart()
        Catch ex As Exception
            LogError(ex)
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
    Public Enum EnCharacteristic
        TransactionVolume = 1
        TransactionValue
        TransactionVelocity
        TransactionNormalDeviation
    End Enum

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

        Try
            If Not Page.ClientScript.IsClientScriptIncludeRegistered("idpopup1") Then
                Page.ClientScript.RegisterClientScriptInclude(Me.GetType(), "idpopup1", ResolveClientUrl("script/popupdrag.js"))
            End If

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub TabGraphicalTransaction_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabGraphicalTransaction.Activate
        Session("TransactionGraphCommand") = Nothing
        Session("GraphicalTransactionAccountNo") = Me.GetAccountNo
        Dim ProcessDatePeriod As Integer
        ProcessDatePeriod = System.Configuration.ConfigurationManager.AppSettings("ProcessDatePeriod")
        Session("GraphicalTransactionStartDate") = DateTime.Now().AddDays(ProcessDatePeriod).ToString("yyyy-MM-dd")
        Session("GraphicalTransactionEndDate") = DateTime.Now().AddDays(ProcessDatePeriod).ToString("yyyy-MM-dd")
    End Sub

    ''' <summary>
    ''' Load Detail Joint Account
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub LoadDetailJointAccount()
        Try
            Using TaJointAccount As New AMLDAL.AccountInformationTableAdapters.AccountInformationDetailJointAccountTableAdapter
                Dim DtJointAccount() As Data.DataRow = TaJointAccount.GetData(Me.LblAccountNumber.Text).Select("CIFNO <> '" & LnkCIFNo.Text & "'")

                If DtJointAccount.Length > 0 Then
                    Me.GridJointAccount.DataSource = DtJointAccount
                    Me.GridJointAccount.DataBind()
                    Me.TdJointAccountAvailable.Visible = True
                    Me.TdJointAccountNotAvailable.Visible = False
                Else
                    Me.TdJointAccountAvailable.Visible = False
                    Me.TdJointAccountNotAvailable.Visible = True
                End If
            End Using

        Catch
            Throw
        End Try
    End Sub

    Protected Sub TabJointAccount_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabJointAccount.Activate
        Try
            Me.LoadDetailJointAccount()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Function GenerateCIFLink(ByVal strCIFNo As String) As String
        If strCIFNo = "" Then
            Return "Customer Data Not Valid"
        Else
            Return String.Format("<a href=""{0}"">{1}</a> ", ResolveUrl("~\CustomerInformationDetail.aspx?CIFNo=" & strCIFNo), strCIFNo)
        End If
    End Function

    'Protected Sub Page_SaveStateComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.SaveStateComplete
    '    Try
    '        Dim a(,) As String = {{"Modus Amount", "1", "2"}, {"Modus Freq", "3", "2"}, {"Modus Freq/Day/Period", "5", "2"}}
    '        'Dim b(,) As String = {{"Modus", "1", "6"}}
    '        WebControlsProcsClass.ApplyGridViewHeader(GridViewFinancialModus, a)
    '        'WebControlsProcsClass.ApplyGridViewHeader(GridViewFinancialModus, b)
    '    Catch ex As Exception

    '    End Try
    'End Sub

    Protected Sub StatisticFinancialModus_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles StatisticFinancialModus.Activate

        Try
            GridViewFinancialModus.DataSource = AMLBLL.AccountInformationBLL.GetStatisticMOdusDataset(Me.GetAccountNo)
            GridViewFinancialModus.DataBind()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub lnkTransPeriod_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try
            Dim objLinkButton As LinkButton = CType(sender, LinkButton)
            Dim strperiod As String = objLinkButton.CommandArgument
            'AMLBLL.AccountInformationBLL.GetDetailStatisticModus(Me.GetAccountNo, strperiod)

            Page.ClientScript.RegisterStartupScript(Me.GetType(), "idpopup2", "popUpDrag('" & CType(sender, LinkButton).ClientID & "','divBrowsePopUpModus');", True)
            PopUpStatisticModus1.InitData(Me.GetAccountNo, strperiod)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub GridViewFinancialModus_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridViewFinancialModus.RowCreated
        Try
            If e.Row.RowType = DataControlRowType.Header Then
                Dim objgridviewheader As GridView = CType(sender, GridView)

                Dim objgridviewrow0 As GridViewRow = New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)

                Dim tc1 As New TableCell
                tc1.Text = "D"
                tc1.Font.Bold = True
                tc1.ForeColor = Drawing.Color.White
                tc1.HorizontalAlign = HorizontalAlign.Center
                tc1.BackColor = Drawing.Color.Black

                objgridviewrow0.Cells.Add(tc1)

                tc1 = New TableCell
                tc1.Text = "C"
                tc1.Font.Bold = True
                tc1.ForeColor = Drawing.Color.White
                tc1.HorizontalAlign = HorizontalAlign.Center
                tc1.BackColor = Drawing.Color.Black
                objgridviewrow0.Cells.Add(tc1)

                'tc1 = New TableCell
                'tc1.Text = "D"
                'tc1.Font.Bold = True
                'tc1.ForeColor = Drawing.Color.White
                'tc1.HorizontalAlign = HorizontalAlign.Center
                'tc1.BackColor = Drawing.Color.Black
                'objgridviewrow0.Cells.Add(tc1)

                'tc1 = New TableCell
                'tc1.Text = "C"
                'tc1.Font.Bold = True
                'tc1.ForeColor = Drawing.Color.White
                'tc1.HorizontalAlign = HorizontalAlign.Center
                'tc1.BackColor = Drawing.Color.Black
                'objgridviewrow0.Cells.Add(tc1)

                'tc1 = New TableCell
                'tc1.Text = "D"
                'tc1.Font.Bold = True
                'tc1.ForeColor = Drawing.Color.White
                'tc1.HorizontalAlign = HorizontalAlign.Center
                'tc1.BackColor = Drawing.Color.Black
                'objgridviewrow0.Cells.Add(tc1)

                'tc1 = New TableCell
                'tc1.Text = "C"
                'tc1.Font.Bold = True
                'tc1.ForeColor = Drawing.Color.White
                'tc1.HorizontalAlign = HorizontalAlign.Center
                'tc1.BackColor = Drawing.Color.Black
                'objgridviewrow0.Cells.Add(tc1)

                objgridviewheader.Controls(0).Controls.AddAt(0, objgridviewrow0)


                Dim objgridviewrow As GridViewRow = New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)



                'tc1 = New TableCell
                'tc1.Text = "Amount"
                'tc1.ColumnSpan = 2
                'tc1.Font.Bold = True
                'tc1.ForeColor = Drawing.Color.White
                'tc1.HorizontalAlign = HorizontalAlign.Center
                'tc1.BackColor = Drawing.Color.Black
                'objgridviewrow.Cells.Add(tc1)

                'tc1 = New TableCell
                'tc1.Text = "#Freq"
                'tc1.ColumnSpan = 2
                'tc1.Font.Bold = True
                'tc1.ForeColor = Drawing.Color.White
                'tc1.HorizontalAlign = HorizontalAlign.Center
                'tc1.BackColor = Drawing.Color.Black
                'objgridviewrow.Cells.Add(tc1)


                'tc1 = New TableCell
                'tc1.Text = "Freq/Day/Period"
                'tc1.ColumnSpan = 2
                'tc1.Font.Bold = True
                'tc1.ForeColor = Drawing.Color.White
                'tc1.BackColor = Drawing.Color.Black
                'tc1.HorizontalAlign = HorizontalAlign.Center

                'objgridviewrow.Cells.Add(tc1)
                'objgridviewheader.Controls(0).Controls.AddAt(0, objgridviewrow)

                Dim objgridviewrow1 As GridViewRow = New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)

                Dim tc3 As New TableCell
                tc3.Text = "Transaction Period"
                tc3.RowSpan = 2
                tc3.Font.Bold = True
                tc3.ForeColor = Drawing.Color.White
                tc3.HorizontalAlign = HorizontalAlign.Center
                tc3.BackColor = Drawing.Color.Black
                objgridviewrow1.Cells.Add(tc3)

                Dim tc2 As New TableCell
                tc2.Text = "Transaction Profile"
                tc2.ColumnSpan = 2
                tc2.Font.Bold = True
                tc2.BackColor = Drawing.Color.Black
                tc2.ForeColor = Drawing.Color.White
                tc2.HorizontalAlign = HorizontalAlign.Center
                objgridviewrow1.Cells.Add(tc2)
                objgridviewheader.Controls(0).Controls.AddAt(0, objgridviewrow1)


            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub BtnBrowseAuxiliaryTrxCode_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnBrowseAuxiliaryTrxCode.Click
        Try
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "idpopupBrowseAuxiliaryTrxCode", "popUpDrag('" & CType(sender, Button).ClientID & "','divBrowseAuxTrxCodeMultiple');", True)
            PickerAuxTrxCodeMultiple1.InitData()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgClearSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClearSearch.Click

        Try

            TxtDate1.Text = ""
            TxtDate2.Text = ""

            DebitOrCreditDropDownList.SelectedValue = "All"
            TxtAmount1.Text = ""
            txtAmount2.Text = ""
            TransactionRemarkTextBox.Text = ""
            MemoRemarkTextBox.Text = ""

            TxtAuxTrxCode.Text = ""
            HfAuxTrxCode.Value = ""
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub LnkBtnExportAllToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LnkBtnExportAllToExcel.Click
        Try
            Me.BindAll()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=AccountInformationTransaction.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridTransDetail)
            GridTransDetail.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub TabLoanInformation_Activate(sender As Object, e As System.EventArgs) Handles TabLoanInformation.Activate
        Try
            Using objTAccountLoanCollateral As TList(Of AccountLoanCollateral) = AMLBLL.AccountInformationBLL.GetTlistAccountLoanCollateral(AccountLoanCollateralColumn.AcocuntNo.ToString & "='" & Me.GetAccountNo & "'", "", 0, Integer.MaxValue, 0)
                GrdCollateral.DataSource = objTAccountLoanCollateral
                GrdCollateral.DataBind()
            End Using
            Using objAccountLoanDetail As TList(Of AccountLoanDetail) = AMLBLL.AccountInformationBLL.GetTlistAccountLoanDetail(AccountLoanDetailColumn.AccountNo.ToString & "='" & Me.GetAccountNo & "'", "", 0, Integer.MaxValue, 0)
                If objAccountLoanDetail.Count > 0 Then
                    LblPlafon.Text = objAccountLoanDetail(0).Plafon.GetValueOrDefault(0).ToString("#,##0.00")
                    LblJenisPinjaman.Text = objAccountLoanDetail(0).JenisPinjaman
                    If objAccountLoanDetail(0).Maturity_Date.HasValue Then
                        LblMaturityDate.Text = objAccountLoanDetail(0).Maturity_Date.GetValueOrDefault.ToString("dd-MMM-yyyy")
                    End If
                    If objAccountLoanDetail(0).Closing_Date.HasValue Then
                        LblClosingDate.Text = objAccountLoanDetail(0).Closing_Date.GetValueOrDefault.ToString("dd-MMM-yyyy")
                    End If
                    If objAccountLoanDetail(0).Opening_Date.HasValue Then
                        LblOpenDate.Text = objAccountLoanDetail(0).Opening_Date.GetValueOrDefault.ToString("dd-MMM-yyyy")
                    End If

                End If
            End Using
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Sub LoadDetailCreditCard()
        Using objAccountCreditCardDetail As TList(Of AccountCreditCardDetail) = AMLBLL.AccountInformationBLL.GetTlistAccountCreditCardDetail(AccountCreditCardDetailColumn.AccountNo.ToString & "='" & Me.GetAccountNo & "'", "", 0, Integer.MaxValue, 0)
            If objAccountCreditCardDetail.Count > 0 Then
                LblLimitCreditCard.Text = Format(objAccountCreditCardDetail(0).LimitCreditCard, "#,###,##")
                LblOUtStanding.Text = Format(ObjAllAccountAllinfo.CurrentBalance.GetValueOrDefault(0), "#,###,##")
            End If
        End Using
    End Sub

    Protected Sub TabCreditCardInformation_Activate(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabCreditCardInformation.Activate
        Try
            LoadDetailCreditCard()
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgPrint.Click

        Try
            Response.Redirect("PrintAccount.aspx?AccountNo=" & Me.GetAccountNo & "&CIFNO=" & ObjAllAccountAllinfo.CIFNO & "", False)
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub lnkcounterpartacc_Click(sender As Object, e As System.EventArgs)
        Try
            'LblCounterPartAcc()
            Dim objdatagridview As DataGridItem = CType(sender, LinkButton).NamingContainer

            Dim strtransdate As String = GridTransDetail.Items(objdatagridview.ItemIndex).Cells(2).Text

            Dim strdebetcredit As String = GridTransDetail.Items(objdatagridview.ItemIndex).Cells(11).Text
            Dim strtiketno As String = GridTransDetail.Items(objdatagridview.ItemIndex).Cells(16).Text.Replace("&nbsp;", "")
            Dim objlblcounterpartaccc As Label = CType(objdatagridview.FindControl("LblCounterPartAcc"), Label)
            Dim dtransdate As DateTime = Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", strtransdate)

            Dim strquery As String = "select dbo.ufn_GetAccountLawan('" & strtiketno & "','" & strdebetcredit & "','" & dtransdate.ToString("yyyy-MM-dd") & "') "

            objlblcounterpartaccc.Text = DataRepository.Provider.ExecuteScalar(Data.CommandType.Text, strquery).ToString

            If objlblcounterpartaccc.Text = "" Then
                objlblcounterpartaccc.Text = "There No Account Part"

            End If
            CType(sender, LinkButton).Visible = False

        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub MultiView1_ActiveViewChanged(sender As Object, e As System.EventArgs) Handles MultiView1.ActiveViewChanged
        Try
            If MultiView1.ActiveViewIndex = 3 Then ' Transaction Detail
                Me.BindGrid()
                Me.SetInfoNavigate()
                'Me.ChartByDate()
            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub lnkcounterpartName_Click(sender As Object, e As System.EventArgs)
        Try
            'LblCounterPartAcc()
            Dim objdatagridview As DataGridItem = CType(sender, LinkButton).NamingContainer

            Dim strtransdate As String = GridTransDetail.Items(objdatagridview.ItemIndex).Cells(2).Text

            Dim strdebetcredit As String = GridTransDetail.Items(objdatagridview.ItemIndex).Cells(11).Text
            Dim strtiketno As String = GridTransDetail.Items(objdatagridview.ItemIndex).Cells(16).Text.Replace("&nbsp;", "")
            Dim objlblcounterpartaccc As Label = CType(objdatagridview.FindControl("LblCounterPartName"), Label)
            Dim dtransdate As DateTime = Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", strtransdate)

            Dim strquery As String = "select dbo.ufn_GetAccountNameByAccountNo('" & strtiketno & "','" & strdebetcredit & "','" & dtransdate.ToString("yyyy-MM-dd") & "') "

            objlblcounterpartaccc.Text = DataRepository.Provider.ExecuteScalar(Data.CommandType.Text, strquery).ToString

            If objlblcounterpartaccc.Text = "" Then
                objlblcounterpartaccc.Text = "There No Account Part Name"

            End If
            CType(sender, LinkButton).Visible = False


        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
End Class '2441


