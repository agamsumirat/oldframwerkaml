<%@ Page Language="VB" MasterPageFile="~/masterpage.master" AutoEventWireup="false" CodeFile="RulesBasicAdd.aspx.vb" Inherits="RulesBasicAdd" title="Basic Rules - Add" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
    <table width="100%">
        <tr>
            <td colspan="3" class="maintitle">
                Basic Rules - Add<asp:Label ID="LblSucces" runat="server" CssClass="validationok"
                    Visible="False" Width="100%"></asp:Label></td>
        </tr>
        <tr>
            <td style="height: 26px">
                Basic Rule Name</td>
            <td style="height: 26px">
                :</td>
            <td style="width: 604px; height: 26px;">
                <asp:TextBox ID="BasicRulesNameTextBox" runat="server" Width="217px" MaxLength="50"></asp:TextBox>
                <asp:RequiredFieldValidator ID="BasicRuleNameRequiredFieldValidator" runat="server"
                    ControlToValidate="BasicRulesNameTextBox" ErrorMessage="Basic Rule Name is required">*</asp:RequiredFieldValidator><strong><span
                        style="color: #ff0000">*</span></strong></td>
        </tr>
        <tr>
            <td style="height: 75px">
                Description</td>
            <td style="height: 75px">
                :</td>
            <td style="width: 604px; height: 75px;" valign="top">
                <asp:TextBox ID="BasicRulesDescriptionTextBox" runat="server" Height="65px" TextMode="MultiLine"
                    Width="218px" MaxLength="255"></asp:TextBox>
                <asp:RequiredFieldValidator ID="BasicRulesDescriptionRequiredFieldValidator" runat="server" ControlToValidate="BasicRulesDescriptionTextBox"
                    ErrorMessage="Basic Rule Description is required">*</asp:RequiredFieldValidator><strong><span style="color: #ff0000">*</span></strong></td>
        </tr>
        <tr>
            <td>
                </td>
            <td>
                </td>
            <td style="width: 604px">
                <asp:DropDownList ID="AlertToDropDownList" runat="server" Visible="false">
                    <asp:ListItem Value="1">Source Account Owner Branch</asp:ListItem>
                    <asp:ListItem Value="2">Beneficiary Account Owner Branch</asp:ListItem>
                    <asp:ListItem Value="3">First Transaction Account Owner Branch</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td>
                Enabled this rule</td>
            <td style="height: 41px">
            </td>
            <td style="width: 604px; height: 41px;">
                <asp:CheckBox ID="RuleEnabledCheckBox" runat="server" Checked="True" /></td>
        </tr>
        <tr valign="top">
            <td colspan="3">
                <asp:Menu ID="Menu1" runat="server" Orientation="Horizontal" CssClass="tabs">
                    <Items>
                        <asp:MenuItem Text="Customer Data" Value="0" Selected="True"></asp:MenuItem>
                        <asp:MenuItem Text="Transaction" Value="1"></asp:MenuItem>
                    </Items>
                    <StaticMenuItemStyle CssClass="tab" />
                    <StaticSelectedStyle CssClass="selectedTab" />
                </asp:Menu>
                <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                    <asp:View ID="CustomerDataView" runat="server">
                            <asp:Panel ID="SourceCustomerPanel" runat="server" GroupingText="Customer" Height="250px" Width="100%">
                            <table width="100%">
                                <tr>
                                    <td style="width: 107px; height: 24px">
                                        Customer Of</td>
                                    <td style="width: 4px; height: 24px">
                                        :</td>
                                    <td style="width: 240px; height: 24px">
                                        <asp:DropDownList ID="SourceCustomerOfDropDownList" runat="server">
                                            <asp:ListItem>All</asp:ListItem>
                                            <asp:ListItem>Bank Niaga</asp:ListItem>
                                            <asp:ListItem>Non Bank Niaga</asp:ListItem>
                                        </asp:DropDownList></td>
                                    <td style="width: 135px; height: 24px">
                                        Customer Sub Type</td>
                                    <td style="height: 24px; width: 12px;">
                                        :</td>
                                    <td style="width: 117px; height: 24px">
                                        <asp:DropDownList ID="SourceCustomerSubTypeDropDownList" runat="server" AppendDataBoundItems="True">
                                            <asp:ListItem>All</asp:ListItem>
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td style="width: 107px; height: 24px">
                                        Customer Type</td>
                                    <td style="width: 4px; height: 24px;">
                                        :</td>
                                    <td style="width: 240px; height: 24px">
                                        <asp:DropDownList ID="SourceCustomerTypeDropDownList" runat="server">
                                            <asp:ListItem>All</asp:ListItem>
                                            <asp:ListItem Value="A">Personal</asp:ListItem>
                                            <asp:ListItem Value="B">Company</asp:ListItem>
                                        </asp:DropDownList></td>
                                    <td style="width: 135px; height: 24px">
                                        Business Type</td>
                                    <td style="height: 24px; width: 12px;">
                                        :</td>
                                    <td style="width: 117px; height: 24px">
                                        <asp:DropDownList ID="SourceBusinessTypeDropDownList" runat="server" AppendDataBoundItems="True">
                                            <asp:ListItem>All</asp:ListItem>
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td style="width: 107px; height: 22px" valign="top">
                                        Negative List</td>
                                    <td style="width: 4px; height: 22px;" valign="top">
                                        :</td>
                                    <td style="width: 240px; height: 22px" valign="top">
                                        &nbsp;<div style="overflow: auto; width: 100%; height: 100px; border-right: black 1px solid; border-top: black 1px solid; border-left: black 1px solid; border-bottom: black 1px solid;">
                                        <asp:CheckBoxList ID="SourceNegativeListCheckBoxList" runat="server" RepeatLayout="Flow">
                                        </asp:CheckBoxList></div>
                                    </td>
                                    <td style="width: 135px; height: 22px">
                                        Internal Industry Code</td>
                                    <td style="height: 22px; width: 12px;">
                                        :</td>
                                    <td style="width: 117px; height: 22px">
                                        <asp:DropDownList ID="SourceInternalIndustryCodeDropDownList" runat="server" AppendDataBoundItems="True">
                                            <asp:ListItem>All</asp:ListItem>
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td style="width: 107px; height: 22px">
                                        High Risk Business</td>
                                    <td style="width: 4px; height: 22px">
                                        :</td>
                                    <td style="width: 240px; height: 22px">
                                        <asp:CheckBox ID="SourceHighRiskBusinessCheckBox" runat="server"
                                            Width="171px" /></td>
                                    <td style="width: 135px; height: 22px">
                                    </td>
                                    <td style="width: 12px; height: 22px">
                                    </td>
                                    <td style="width: 117px; height: 22px">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 107px">
                                        High Risk Country</td>
                                    <td style="width: 4px">
                                        :</td>
                                    <td style="width: 240px">
                                        <asp:CheckBox ID="SourceHighRiskCountryCheckBox" runat="server"
                                            Width="140px" /></td>
                                    <td style="width: 135px">
                                    </td>
                                    <td style="width: 12px">
                                    </td>
                                    <td style="width: 117px">
                                    </td>
                                </tr>
                            </table>
                            </asp:Panel>
                        <asp:Panel ID="SourceAccountPanel" runat="server" GroupingText="Account" Height="112px" Width="100%">
                            <table width="100%">
                                <tr>
                                    <td style="width: 71px; height: 15px">
                                        Opened within</td>
                                    <td style="width: 12px; height: 15px">
                                        :
                                    </td>
                                    <td style="width: 213px; height: 15px">
                                        <asp:TextBox ID="SourceOpenedWithinTextBox" runat="server"></asp:TextBox><asp:DropDownList
                                            ID="SourceOpenedWithinTypeDropDownList" runat="server">
                                            <asp:ListItem Value="1">Day(s)</asp:ListItem>
                                            <asp:ListItem Value="2">Month(s)</asp:ListItem>
                                            <asp:ListItem Value="3">Year(s)</asp:ListItem>
                                        </asp:DropDownList></td>
                                    <td style="height: 15px; width: 80px;">
                                        Segmentation</td>
                                    <td style="height: 15px; width: 9px;">
                                        :</td>
                                    <td style="width: 82px; height: 15px"><asp:DropDownList ID="SourceSegmentationDropDownList" runat="server">
                                        <asp:ListItem>All</asp:ListItem>
                                        <asp:ListItem>Individual (Except BNPC &amp; PBG)</asp:ListItem>
                                        <asp:ListItem>Affluent</asp:ListItem>
                                        <asp:ListItem>Business</asp:ListItem>
                                        <asp:ListItem>Corporate</asp:ListItem>
                                        <asp:ListItem>UKM</asp:ListItem>
                                        <asp:ListItem>Financial Institution</asp:ListItem>
                                    </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td style="width: 71px; height: 15px">
                                        Account Type</td>
                                    <td style="width: 12px; height: 15px">
                                        :</td>
                                    <td style="width: 213px; height: 15px">
                                        <asp:CheckBoxList ID="SourceAccountTypeCheckBoxList" runat="server">
                                        </asp:CheckBoxList></td>
                                    <td style="height: 15px; width: 80px;">
                                        Status</td>
                                    <td style="height: 15px; width: 9px;">
                                        :</td>
                                    <td style="width: 82px; height: 15px">
                                        <asp:DropDownList ID="SourceAccountStatusDropDownList" runat="server" AppendDataBoundItems="True">
                                            <asp:ListItem Value="A">All</asp:ListItem>
                                        </asp:DropDownList></td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </asp:View>
                    <asp:View ID="TransactionView" runat="server">
                        <table width="100%">
                            <tr>
                                <td>
                                    Transaction Type</td>
                                <td>
                                    :</td>
                                <td style="width: 307px">
                                    <asp:DropDownList ID="TransactionTypeDropDownList" runat="server" AppendDataBoundItems="True">
                                        <asp:ListItem Value="0">All</asp:ListItem>
                                    </asp:DropDownList></td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Auxiliary Transaction Code</td>
                                <td>
                                    :</td>
                                <td style="width: 307px">
                                    <asp:DropDownList ID="AuxiliaryTransactionCodeDropDownList" runat="server">
                                        <asp:ListItem>IN</asp:ListItem>
                                        <asp:ListItem>NOT IN</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:TextBox ID="AuxiliaryTransactionCodeTextBox" runat="server"></asp:TextBox>
                                    <br />
                                    Separated by comma (,) if more than 1 value</td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Transaction Amount</td>
                                <td>
                                    :</td>
                                <td style="width: 307px">
                                    <ajax:AjaxPanel ID="AjaxPanel3" runat="server" Width="283px">
                                        <asp:DropDownList ID="TransactionAmountFromDropDownList" runat="server" AutoPostBack="True">
                                            <asp:ListItem Value="0">Unspecified</asp:ListItem>
                                            <asp:ListItem Value="1">Value</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox ID="TransactionAmountFromTextBox" runat="server" Visible="False"></asp:TextBox></ajax:AjaxPanel></td>
                                <td>
                                    <ajax:AjaxPanel ID="AjaxPanel4" runat="server" Width="273px">
                                        <asp:DropDownList ID="TransactionAmountUntilDropDownList" runat="server" AutoPostBack="True">
                                            <asp:ListItem Value="0">Unspecified</asp:ListItem>
                                            <asp:ListItem Value="1">Value</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox ID="TransactionAmountUntilTextBox" runat="server" Visible="False"></asp:TextBox></ajax:AjaxPanel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td style="width: 307px">
                                    <asp:CheckBox ID="TransactionAmountGEKYCTransactionNormalCheckBox" runat="server" Text="Amount >= Nominal Transaction Normal" /></td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 24px">
                                    Credit/Debit</td>
                                <td style="height: 24px">
                                    :</td>
                                <td style="width: 307px; height: 24px;">
                                    <asp:DropDownList ID="TransactionDebitORCreditDropDownList" runat="server">
                                        <asp:ListItem>All</asp:ListItem>
                                        <asp:ListItem Value="C">Credit</asp:ListItem>
                                        <asp:ListItem Value="D">Debit</asp:ListItem>
                                    </asp:DropDownList></td>
                                <td style="height: 24px">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Within</td>
                                <td>
                                    :</td>
                                <td style="width: 307px">
                                    <ajax:AjaxPanel ID="AjaxPanel1" runat="server" Width="100%">
                                        <asp:CheckBox ID="TransactionFrequencyCheckBox" runat="server" AutoPostBack="True"
                                            Text="Transaction" />
                                        <asp:TextBox ID="TransactionFrequencyTextBox" runat="server" Enabled="False"></asp:TextBox>
                                        time(s)</ajax:AjaxPanel></td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td style="width: 307px">
                                    <ajax:AjaxPanel ID="AjaxPanel2" runat="server" Width="100%">
                                        <asp:CheckBox ID="TransactionPeriodCheckBox" runat="server" AutoPostBack="True" Text="Period" />
                                        <asp:TextBox ID="TransactionPeriodTextBox" runat="server" Enabled="False"></asp:TextBox>
                                        <asp:DropDownList ID="TransactionPeriodTypeDropDownList" runat="server" Enabled="False">
                                            <asp:ListItem Value="1">Day(s)</asp:ListItem>
                                            <asp:ListItem Value="2">Month(s)</asp:ListItem>
                                            <asp:ListItem Value="3">Year(s)</asp:ListItem>
                                        </asp:DropDownList></ajax:AjaxPanel></td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 29px">
                                    Country Code</td>
                                <td style="height: 29px">
                                    :</td>
                                <td style="width: 307px; height: 29px;">
                                    <asp:DropDownList ID="TransactionCountryCodeDropDownList" runat="server">
                                        <asp:ListItem>IN</asp:ListItem>
                                        <asp:ListItem>NOT IN</asp:ListItem>
                                    </asp:DropDownList><asp:TextBox ID="TransactionCountryCodeTextBox" runat="server"></asp:TextBox><br />
                                    Separated by comma (,) if more than 1 value</td>
                                <td style="height: 29px">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    1 CIF &amp; Related Source Fund</td>
                                <td>
                                </td>
                                <td style="width: 307px">
                                    <asp:CheckBox ID="TransactionRelatedCIFCheckBox" runat="server" /></td>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                </asp:MultiView>
            </td>
        </tr>
		<tr class="formText" bgColor="#dddddd" height="30">
			<td colspan="3"><table cellSpacing="0" cellPadding="3" border="0">
					<tr>
					    <td><IMG height="15" src="images/arrow.gif" width="15"></td>
						<td><asp:imagebutton id="ImageSave" runat="server" CausesValidation="False" SkinID="SaveButton"></asp:imagebutton></td>
						<td style="width: 35px"><asp:imagebutton id="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton"></asp:imagebutton></td>
					</tr>
				</table>
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator>
            </td>      
        </tr>
    </table>
</asp:Content>

