﻿Imports CDDLNPNettier.Data
Imports CDDLNPNettier.Entities
Imports AMLBLL

Partial Class CDDLNPMappingQuestionEdit
    Inherits Parent

    Private ReadOnly Property GetPK_SettingCDDLNP_ID() As Int32
        Get
            Return Request.QueryString("ID")
        End Get
    End Property

    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("CDDLNPMappingQuestionEdit.CurrentPage") Is Nothing, 0, Session("CDDLNPMappingQuestionEdit.CurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("CDDLNPMappingQuestionEdit.CurrentPage") = Value
        End Set
    End Property

    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetBindTable.Count)
        End Get
    End Property

    Private Property SetnGetBindTable() As TList(Of MappingSettingCDDBagianQuestion_ApprovalDetail)
        Get
            Return CType(Session("MappingSettingCDDBagianQuestion_ApprovalDetail.Table"), TList(Of MappingSettingCDDBagianQuestion_ApprovalDetail))
        End Get
        Set(ByVal value As TList(Of MappingSettingCDDBagianQuestion_ApprovalDetail))
            Session("MappingSettingCDDBagianQuestion_ApprovalDetail.Table") = value
        End Set
    End Property

    Protected Sub CvalPageErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles cvalPageError.PreRender
        If cvalPageError.IsValid = False Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
        End If
    End Sub

    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknown Commandname " & e.CommandName)
            End Select            
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub

    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetBindTable.Count
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
        Catch
            Throw
        End Try
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not Page.IsPostBack Then
                Using objSetting As VList(Of vw_MappingSettingCDDBagian) = DataRepository.vw_MappingSettingCDDBagianProvider.GetPaged("FK_SettingCDDLNP_ID = " & Me.GetPK_SettingCDDLNP_ID, String.Empty, 0, Int32.MaxValue, 0)
                    If objSetting.Count > 0 Then
                        lblCDDType.Text = objSetting(0).CDD_Type
                        lblCDDNasabahType.Text = objSetting(0).CDD_NasabahType_ID

                        ddlCDDBagian.Items.Clear()
                        For Each objCDDBagian As vw_MappingSettingCDDBagian In objSetting
                            ddlCDDBagian.Items.Add(New ListItem(objCDDBagian.CDDBagianName, objCDDBagian.PK_MappingSettingCDDBagian_ID))
                        Next

                        'Add Data Mapping ke temporary table
                        Me.SetnGetCurrentPage = 0
                        SetnGetBindTable = New TList(Of MappingSettingCDDBagianQuestion_ApprovalDetail)
                        For Each objQuestion As vw_CDDLNP_Form In DataRepository.vw_CDDLNP_FormProvider.GetPaged("FK_SettingCDDLNP_ID = " & Me.GetPK_SettingCDDLNP_ID, "PK_CDD_Bagian_ID, PK_CDD_Question_ID", 0, Int32.MaxValue, 0)
                            SetnGetBindTable.Add(MappingSettingCDDBagianQuestion_ApprovalDetail.CreateMappingSettingCDDBagianQuestion_ApprovalDetail(Nothing, objQuestion.FK_MappingSettingCDDBagian_ID, objQuestion.PK_CDD_Question_ID))
                        Next

                        Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                        Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                            Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                            AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                        End Using

                    Else 'Handle jika ID tidak ditemukan
                        Me.Response.Redirect("CDDLNPMessage.aspx?ID=404", False)
                    End If
                End Using
            End If
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub

    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim objTransManager As New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)
        Dim objApproval As New MappingSettingCDDBagianQuestion_Approval

        Try
            objTransManager.BeginTransaction()

            If DataRepository.MappingSettingCDDBagianQuestion_ApprovalProvider.GetTotalItems(objTransManager, "FK_SettingCDDLNP_ID=" & Me.GetPK_SettingCDDLNP_ID, 0) > 0 Then
                Throw New Exception("CDD LNP Mapping Question for " & lblCDDType.Text & " " & lblCDDNasabahType.Text & " is currently waiting for approval.")
            End If

            objApproval.RequestedBy = Sahassa.AML.Commonly.SessionPkUserId
            objApproval.RequestedDate = Date.Now
            objApproval.FK_MsMode_Id = 2
            objApproval.IsUpload = False
            objApproval.FK_SettingCDDLNP_ID = Me.GetPK_SettingCDDLNP_ID
            DataRepository.MappingSettingCDDBagianQuestion_ApprovalProvider.Save(objTransManager, objApproval)


            For i As Int32 = 0 To SetnGetBindTable.Count - 1
                SetnGetBindTable(i).FK_MappingSettingCDDBagianQuestion_Approval_Id = objApproval.PK_MappingSettingCDDBagianQuestion_Approval_Id
            Next
            DataRepository.MappingSettingCDDBagianQuestion_ApprovalDetailProvider.Save(objTransManager, SetnGetBindTable)


            objTransManager.Commit()

            Me.Response.Redirect("CDDLNPMessage.aspx?ID=10021", False)
        Catch ex As Exception
            objTransManager.Rollback()
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        Finally
            If Not objTransManager Is Nothing Then
                objTransManager.Dispose()
                objTransManager = Nothing
            End If
            If Not objApproval Is Nothing Then
                objApproval.Dispose()
                objApproval = Nothing
            End If
        End Try
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Me.Response.Redirect("CDDLNPMappingQuestionView.aspx")
    End Sub

    Protected Sub ImageAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAdd.Click
        Try
            If hfQuestionID.Value = String.Empty Then
                Throw New Exception("Question belum dipilih")
            Else
                SetnGetBindTable.Add(New MappingSettingCDDBagianQuestion_ApprovalDetail)
                SetnGetBindTable(SetnGetBindTable.Count - 1).FK_MappingSettingCDDBagian_ID = ddlCDDBagian.SelectedValue
                SetnGetBindTable(SetnGetBindTable.Count - 1).FK_CDD_Question_ID = hfQuestionID.Value

                ddlCDDBagian.Focus()
                hfQuestionID.Value = String.Empty
                txtParentQuestion.Text = String.Empty
            End If
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub

    Protected Sub GridViewCDDQuestionDetail_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridViewCDDQuestionDetail.ItemCommand
        Try
            Select Case e.CommandName.ToLower
                Case "delete"
                    SetnGetBindTable.RemoveAt((Me.SetnGetCurrentPage * Sahassa.AML.Commonly.GetDisplayedTotalRow) + e.Item.ItemIndex)

            End Select
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub

    Protected Sub GridViewCDDQuestionDetail_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridViewCDDQuestionDetail.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then

                Using objBagian As VList(Of vw_MappingSettingCDDBagian) = DataRepository.vw_MappingSettingCDDBagianProvider.GetPaged("PK_MappingSettingCDDBagian_ID = " & e.Item.Cells(0).Text, String.Empty, 0, Int32.MaxValue, 0)
                    If objBagian.Count > 0 Then
                        e.Item.Cells(1).Text = objBagian(0).CDDBagianName
                    End If
                End Using


                Using objQuestion As CDD_Question = DataRepository.CDD_QuestionProvider.GetByPK_CDD_Question_ID(e.Item.Cells(2).Text)
                    If objQuestion IsNot Nothing Then
                        e.Item.Cells(3).Text = objQuestion.CDD_Question

                        If objQuestion.IsRequired.GetValueOrDefault Then
                            Dim LnkDelete As LinkButton = e.Item.FindControl("LnkDelete")
                            LnkDelete.Visible = False
                        End If
                    End If
                End Using

            End If
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            GridViewCDDQuestionDetail.DataSource = SetnGetBindTable.GetRange(Me.SetnGetCurrentPage * Sahassa.AML.Commonly.GetDisplayedTotalRow, Sahassa.AML.Commonly.GetDisplayedTotalRow)

            'Buat filter Question yang sudah dipilih, tidak bisa dipilih lagi
            Dim strQuestionID As String = "0,"
            For Each objQuestion As MappingSettingCDDBagianQuestion_ApprovalDetail In SetnGetBindTable
                strQuestionID &= objQuestion.FK_CDD_Question_ID.ToString & ","
            Next
            Session("strPK_CDD_Question_ID") = strQuestionID.Remove(strQuestionID.LastIndexOf(","), 1)

            GridViewCDDQuestionDetail.DataBind()
            Me.SetInfoNavigate()
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try
    End Sub

    Protected Sub ImageBrowse_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBrowse.Click
        Try
            If Session("PopupCDDQuestion.PK_CDD_Question_ID") IsNot Nothing Then
                txtParentQuestion.Text = Session("PopupCDDQuestion.CDD_Question")
                hfQuestionID.Value = Session("PopupCDDQuestion.PK_CDD_Question_ID")
            End If
        Catch ex As Exception
            cvalPageError.ErrorMessage = ex.Message
            cvalPageError.IsValid = False
        End Try

    End Sub
End Class