Option Strict On
Option Explicit On

Imports System.IO
Imports System.Collections.Generic
Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports AMLBLL.ReportControlGeneratorBLL
Imports System.Data.SqlClient
Imports AMLBLL
Imports Ionic.Zip
Imports Ionic.Zlib


Partial Class CTRReportControlGenerator
    Inherits Parent

    Sub LoadReportType()
        rblReportType.Items.Clear()
        rblReportType.DataSource = ReportControlGeneratorBLL.GetTlistReportType("", "", 0, Integer.MaxValue, 0)
        rblReportType.DataTextField = ReportTypeColumn.ReportTypeDesc.ToString
        rblReportType.DataValueField = ReportTypeColumn.ReportType.ToString
        rblReportType.DataBind()
        rblReportType.Items(0).Selected = True
    End Sub
    Sub LoadReportFormat()
        rblReportFormat.Items.Clear()
        rblReportFormat.DataSource = ReportControlGeneratorBLL.GetTlistReportFormat("", "", 0, Integer.MaxValue, 0)
        rblReportFormat.DataTextField = ReportFormatColumn.ReportFormatName.ToString
        rblReportFormat.DataValueField = ReportFormatColumn.ReportFormat.ToString
        rblReportFormat.DataBind()
        rblReportFormat.Items(0).Selected = True
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using

                Me.popUpStartDate.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.textDate.ClientID & "'), 'dd-mmm-yyyy')")
                LoadReportType()
                LoadReportFormat()

            End If
        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Function IsDataValid() As Boolean
        Dim strErrorMessage As New StringBuilder

        If Not Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", textDate.Text) Then

            strErrorMessage.Append("Transaction Date Must dd-MMM-yyyy </br>")
        End If
        If rblReportType.SelectedValue = "" Then
            strErrorMessage.Append("Please Select Report Type </br>")
        End If
        If rblReportFormat.SelectedValue = "" Then
            strErrorMessage.Append("Please Select Report Format </br>")
        End If

        If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", textDate.Text) And rblReportType.SelectedValue <> "" And rblReportFormat.SelectedValue <> "" Then
            Dim dTransactiondate As DateTime = Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", textDate.Text.Trim)

            If ReportControlGeneratorBLL.IsHasRecord(dTransactiondate, CInt(rblReportType.SelectedValue), CInt(rblReportFormat.SelectedValue)) = 0 Then
                strErrorMessage.Append(" There is no Record For " & rblReportType.SelectedItem.Text & " in date " & dTransactiondate.ToString("dd-MMM-yyyy") & " </br>")
            End If
            If ReportControlGeneratorBLL.IsHasRecordInPending(dTransactiondate, CInt(rblReportType.SelectedValue), CInt(rblReportFormat.SelectedValue)) > 0 Then
                strErrorMessage.Append(" Can't Generate Report. There is Record in Pending Approval For " & rblReportType.SelectedItem.Text & " in date " & dTransactiondate.ToString("dd-MMM-yyyy") & " </br>")
            End If


            If ReportControlGeneratorBLL.IsAlreadyExistInApproval(dTransactiondate, CInt(rblReportType.SelectedValue), CInt(rblReportFormat.SelectedValue), Sahassa.AML.Commonly.SessionUserId) > 0 Then
                strErrorMessage.Append("Report For Choose Date already Saved Into Pending Approval </br>")
            End If
        End If
        If strErrorMessage.ToString.Trim.Length > 0 Then
            Throw New Exception(strErrorMessage.ToString)
        Else
            Return True
        End If

    End Function

    Protected Sub btnGenerate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGenerate.Click
        Try

            If IsDataValid() Then
                Dim dTransactiondate As DateTime = Sahassa.AML.Commonly.ConvertToDate("dd-MMM-yyyy", textDate.Text.Trim)

                Dim intpk As Long = ReportControlGeneratorBLL.SaveRequestPendingApproval(Server.MapPath("~"), dTransactiondate, CInt(rblReportType.SelectedValue), CInt(rblReportFormat.SelectedValue), Sahassa.AML.Commonly.SessionUserId)

                AMLBLL.ReportControlGeneratorBLL.AcceptGenerate(intpk)

                Try
                    Using objListOfgenratectr As ListOfGeneratedFileCTR = DataRepository.ListOfGeneratedFileCTRProvider.GetByPK_ListOfGeneratedFileCTR_ID(intpk)
                        If Not objListOfgenratectr Is Nothing Then
                            If objListOfgenratectr.FK_ReportFormat_ID.GetValueOrDefault(0) = 2 Then
                                objListOfgenratectr.FK_MsStatusUploadPPATK_ID = 3
                                DataRepository.ListOfGeneratedFileCTRProvider.Save(objListOfgenratectr)
                            End If

                            Response.Clear()
                            Response.ClearHeaders()
                            Response.AddHeader("content-disposition", "attachment; filename=" & objListOfgenratectr.FileName)
                            Response.Charset = ""
                            Response.AddHeader("cache-control", "max-age=0")
                            Me.EnableViewState = False
                            Response.ContentType = "application/octet-stream"
                            Response.BinaryWrite(objListOfgenratectr.IsiFile)
                            Response.End()
                        End If
                    End Using



                Catch ex As Exception

                End Try




                MultiView1.ActiveViewIndex = 1
                LblMessage.Text = "Request Generate Report Data Saved CTRInto Pending Approval."
            End If

        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        Try
            MultiView1.ActiveViewIndex = 0
        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class