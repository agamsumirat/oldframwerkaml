#Region "Imports..."
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Excel
Imports Sahassa.AML.Commonly
Imports Sahassanettier.Data
Imports Sahassanettier.Entities
Imports System
Imports System.Collections.Generic
Imports System.Data
Imports System.IO
Imports AMLBLL.DataType
#End Region

Partial Class MsProvince_upload
    Inherits Parent

#Region "Structure..."

    Structure GrouP_MsProvince_success
        Dim MsProvince As MsProvince_ApprovalDetail
        Dim L_MsProvinceNCBS As TList(Of MsProvinceNCBS_approvalDetail)

    End Structure

#End Region

#Region "Property umum"
    Private Property GroupTableSuccessData() As List(Of GrouP_MsProvince_success)
        Get
            Return Session("GroupTableSuccessData")
        End Get
        Set(ByVal value As List(Of GrouP_MsProvince_success))
            Session("GroupTableSuccessData") = value
        End Set
    End Property

    Private Property GroupTableAnomalyData() As List(Of AMLBLL.MsProvinceBll.DT_Group_MsProvince)
        Get
            Return Session("GroupTableAnomalyData")
        End Get
        Set(ByVal value As List(Of AMLBLL.MsProvinceBll.DT_Group_MsProvince))
            Session("GroupTableAnomalyData") = value
        End Set
    End Property

    Public ReadOnly Property GET_PKUserID() As Integer
        Get
            Return SessionPkUserId
        End Get
    End Property

    Public ReadOnly Property GET_UserName() As String
        Get
            'Return SessionNIK
            Return SessionUserId
        End Get
    End Property

    Private Property SetnGetDataTableForSuccessData() As TList(Of MsProvince_ApprovalDetail)
        Get
            Return CType(IIf(Session("MsProvince_upload.TableSuccessUpload") Is Nothing, Nothing, Session("MsProvince_upload.TableSuccessUpload")), TList(Of MsProvince_ApprovalDetail))
        End Get
        Set(ByVal value As TList(Of MsProvince_ApprovalDetail))
            Session("MsProvince_upload.TableSuccessUpload") = value
        End Set
    End Property

    Private Property SetnGetDataTableForAnomalyData() As System.Data.DataTable
        Get
            Return CType(IIf(Session("MsProvince_upload.TableAnomalyUpload") Is Nothing, Nothing, Session("MsProvince_upload.TableAnomalyUpload")), System.Data.DataTable)
        End Get
        Set(ByVal value As System.Data.DataTable)
            Session("MsProvince_upload.TableAnomalyUpload") = value
        End Set
    End Property

    Private Property DSexcelTemp() As Data.DataSet
        Get
            Return CType(IIf(Session("MsProvince_upload.DSexcelTemp") Is Nothing, Nothing, Session("MsProvince_upload.DSexcelTemp")), DataSet)
        End Get
        Set(ByVal value As Data.DataSet)
            Session("MsProvince_upload.DSexcelTemp") = value
        End Set
    End Property

#End Region

#Region "Property Tabel Temporary"
    Private Property DT_AllMsProvince() As Data.DataTable
        Get
            Return CType(IIf(Session("MsProvince_upload.DT_MsProvince") Is Nothing, Nothing, Session("MsProvince_upload.DT_MsProvince")), System.Data.DataTable)
        End Get
        Set(ByVal value As Data.DataTable)
            Session("MsProvince_upload.DT_MsProvince") = value
        End Set
    End Property

    Private Property DT_AllMsProvinceNCBS() As Data.DataTable
        Get
            Return CType(IIf(Session("MsProvince_upload.DT_MsProvinceNCBS") Is Nothing, Nothing, Session("MsProvince_upload.DT_MsProvinceNCBS")), System.Data.DataTable)
        End Get
        Set(ByVal value As Data.DataTable)
            Session("MsProvince_upload.DT_MsProvinceNCBS") = value
        End Set
    End Property

#End Region

#Region "Property Table Sukses Data"

    Private Property TableSuccessMsProvince() As TList(Of MsProvince_ApprovalDetail)
        Get
            Return Session("TableSuccessMsProvince")
        End Get
        Set(ByVal value As TList(Of MsProvince_ApprovalDetail))
            Session("TableSuccessMsProvince") = value
        End Set
    End Property

    Private Property TableSuccessMsProvinceNCBS() As TList(Of MsProvinceNCBS)
        Get
            Return Session("TableSuccessMsProvinceNCBS")
        End Get
        Set(ByVal value As TList(Of MsProvinceNCBS))
            Session("TableSuccessMsProvinceNCBS") = value
        End Set
    End Property


#End Region

#Region "Property Table Anomaly Data"

    Private Property TableAnomalyMsProvince() As System.Data.DataTable
        Get
            Return Session("TableAnomalyMsProvince")
        End Get
        Set(ByVal value As System.Data.DataTable)
            Session("TableAnomalyMsProvince") = value
        End Set
    End Property

    Private Property TableAnomalyMsProvinceNCBS() As System.Data.DataTable
        Get
            Return Session("TableAnomalyMsProvinceNCBS")
        End Get
        Set(ByVal value As System.Data.DataTable)
            Session("TableAnomalyMsProvinceNCBS") = value
        End Set
    End Property

#End Region

#Region "validasi.."

    Private Function ValidateInput(ByVal MsProvince As AMLBLL.MsProvinceBll.DT_Group_MsProvince) As List(Of String)
        Dim temp As New List(Of String)
        Dim msgError As New StringBuilder
        Dim errorline As New StringBuilder
        Try
            'validasi
            ValidateMsProvince(MsProvince.MsProvince, msgError, errorline)
            If msgError.ToString = "" Then
                For Each ci As DataRow In MsProvince.L_MsProvinceNCBS.Rows
                    'validasi NCBS
                    ValidateMsProvinceNCBS(ci, msgError, errorline)
                Next
            End If
            temp.Add(msgError.ToString)
            temp.Add(errorline.ToString)
        Catch ex As Exception
            msgError.Append(ex.Message)
            temp.Add(msgError.ToString)
            temp.Add(errorline.ToString)
        End Try
        Return temp
    End Function

    Private Shared Sub ValidateMsProvinceNCBS(ByVal DTR As DataRow, ByVal msgError As StringBuilder, ByVal errorLine As StringBuilder)
        Try
            '======================== Validasi textbox :  IdProvinceNCBS canot Null (#)====================================================
            If ObjectAntiNull(DTR("IdProvinceBank")) = False Then
                msgError.AppendLine(" &bull; IdProvinceBank must be filled <BR/>")
                errorLine.AppendLine(" &bull; MsProvinceBank : Line " & DTR("NO") & "<BR/>")
            End If

            '======================== Validasi textbox :  Nama canot Null (#)====================================================
            If ObjectAntiNull(DTR("Nama")) = False Then
                msgError.AppendLine(" &bull; ProvinceNameBank must be filled <BR/>")
                errorLine.AppendLine(" &bull; MsProvinceBank : Line " & DTR("NO") & "<BR/>")
            End If

        Catch ex As Exception
            msgError.AppendLine(" &bull;" & ex.Message & "<BR/>")
            errorLine.AppendLine(" &bull;MsProvinceBank : Line " & DTR("NO") & "<BR/>")
        End Try
    End Sub

    Private Shared Sub ValidateMsProvince(ByVal DTR As DataRow, ByVal msgError As StringBuilder, ByVal errorline As StringBuilder)
        Try

            Using checkBlocking As TList(Of MsProvince_ApprovalDetail) = DataRepository.MsProvince_ApprovalDetailProvider.GetPaged("IdProvince=" & DTR("IdProvince"), "", 0, 1, Nothing)
                If checkBlocking.Count > 0 Then
                    msgError.AppendLine(" &bull;MsProvince is waiting for approval <BR/>")
                    errorline.AppendLine(" &bull;MsProvince : Line " & DTR("NO") & "<BR/>")
                End If
            End Using

            '======================== Validasi textbox :  IdProvince   canot Null (#)====================================================
            If ObjectAntiNull(DTR("IdProvince")) = False Then
                msgError.AppendLine(" &bull; ID must be filled <BR/>")
                errorline.AppendLine(" &bull; MsProvince : Line " & DTR("NO") & "<BR/>")
            End If
            If ObjectAntiNull(DTR("IdProvince")) Then
                If objectNumOnly(DTR("IdProvince")) = False Then
                    msgError.AppendLine(" &bull; ID must be Numeric <BR/>")
                    errorline.AppendLine(" &bull; MsProvince : Line " & DTR("NO") & "<BR/>")
                End If
            End If
            '======================== Validasi textbox :  Code   canot Null (#)====================================================
            If ObjectAntiNull(DTR("Code")) = False Then
                msgError.AppendLine(" &bull; Code must be filled <BR/>")
                errorline.AppendLine(" &bull; MsProvince : Line " & DTR("NO") & "<BR/>")
            End If
            '======================== Validasi textbox :  Nama   canot Null (#)====================================================
            If ObjectAntiNull(DTR("Nama")) = False Then
                msgError.AppendLine(" &bull; Nama must be filled <BR/>")
                errorline.AppendLine(" &bull; MsProvince : Line " & DTR("NO") & "<BR/>")
            End If
            '======================== Validasi textbox :  Description   canot Null (#)====================================================
            If ObjectAntiNull(DTR("ProvinceDescription")) = False Then
                msgError.AppendLine(" &bull; Description must be filled <BR/>")
                errorline.AppendLine(" &bull; MsProvince : Line " & DTR("NO") & "<BR/>")
            End If

        Catch ex As Exception
            msgError.AppendLine(" &bull;" & ex.Message & "<BR/>")
            errorline.AppendLine(" &bull;MsProvince  : Line " & DTR("NO") & "<BR/>")
            LogError(ex)
        End Try
    End Sub

#End Region

#Region "Upload File"

    Public Function SaveFileImportToSave() As String
        Dim filePath As String
        Dim FileNameToGetData As String = ""
        Try
            If FileUploadKPI.PostedFile.ContentLength = 0 Then
                Return FileNameToGetData
            End If

            If Not FileUploadKPI.HasFile Then
                Return FileNameToGetData
            End If

            filePath = Path.Combine(Server.MapPath("Upload"), FileUploadKPI.FileName)

            FileNameToGetData = FileUploadKPI.FileName
            If System.IO.File.Exists(filePath) Then
                Dim bexist As Boolean = True
                Dim i As Integer = 1
                While bexist
                    filePath = Path.Combine(Server.MapPath("Upload"), FileUploadKPI.FileName.Split(CChar("."))(0) & "-" & i.ToString & "." & FileUploadKPI.FileName.Split(CChar("."))(1))
                    FileNameToGetData = FileUploadKPI.FileName.Split(CChar("."))(0) & "-" & i.ToString & "." & Me.FileUploadKPI.FileName.Split(CChar("."))(1)
                    If Not System.IO.File.Exists(filePath) Then
                        bexist = False
                        Exit While
                    End If
                    i = i + 1
                End While
            End If
            Me.FileUploadKPI.PostedFile.SaveAs(filePath)
            Session("Filepath") = filePath
            Return FileNameToGetData
        Catch
            Throw
        End Try
    End Function

    ReadOnly Property FilePath() As String
        Get
            Return Session("FilePath")
        End Get
    End Property

    Private Sub ProcessExcelFile(ByVal FullPathFileNameToExportExcel As String)

        Using stream As FileStream = File.Open(FullPathFileNameToExportExcel, FileMode.Open, FileAccess.Read)
            Using excelReader As IExcelDataReader = ExcelReaderFactory.CreateBinaryReader(stream)
                excelReader.IsFirstRowAsColumnNames = True
                DSexcelTemp = excelReader.AsDataSet()
                If DSexcelTemp.Tables("MsProvince").Columns(0).ColumnName Like "*Column*" Then
                    DSexcelTemp.Tables("MsProvince").Rows.Clear()
                End If

            End Using
        End Using

    End Sub


#End Region

#Region "Function..."

    Function getErrorNo(ByVal Id As String, ByVal nama As String) As String
        Dim temp As String = ""
        Try
            Dim DV As New DataView(DT_AllMsProvinceNCBS)
            'jika Null maka harus is null bukan kosong disini belum di handle
            DV.RowFilter = "IdProvince = '" & Id & "' and Nama = '" & nama & "'"

            Dim DT As System.Data.DataTable = DV.ToTable
            If DT.Rows.Count > 0 Then
                temp = Safe(DT.Rows(0)("NO"))
            End If
        Catch ex As Exception
            temp = ex.Message
        End Try

        Return temp
    End Function
    ''' <summary>
    ''' Merubah excel ke Object Databale sekaligus memecahnya
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ImportExcelTempToDTproperty()
        Dim DT As New System.Data.DataTable
        'DT_MsProvince = DSexcelTemp.Tables("MsProvince")
        DT = DSexcelTemp.Tables("MsProvince")
        Dim DV As New DataView(DT)
        DT_AllMsProvince = DV.ToTable(True, "IdProvince", "Code", "Nama", "ProvinceDescription", "activation")
        DT_AllMsProvinceNCBS = DSexcelTemp.Tables("MsProvince")
        DT_AllMsProvince.Columns.Add("NO")
        For Each i As DataRow In DT_AllMsProvince.Rows
            Dim PK As String = Safe(i("IdProvince"))
            Dim nama As String = Safe(i("Nama"))
            i("NO") = getErrorNo(PK, nama)
        Next
    End Sub

    Private Sub ClearThisPageSessions()
        DT_AllMsProvince = Nothing
        DT_AllMsProvinceNCBS = Nothing
        GroupTableSuccessData = Nothing
        GroupTableAnomalyData = Nothing
        DSexcelTemp = Nothing
        SetnGetDataTableForAnomalyData = Nothing
        SetnGetDataTableForSuccessData = Nothing
    End Sub


    Function ConvertDTtoMsProvinceNCBS(ByVal DT As System.Data.DataTable) As TList(Of MsProvinceNCBS_approvalDetail)
        Dim temp As New TList(Of MsProvinceNCBS_approvalDetail)
        For Each DTR As DataRow In DT.Rows
            Using MsProvinceNCBS As New MsProvinceNCBS_approvalDetail
                With MsProvinceNCBS
                    FillOrNothing(.Pk_MsProvinceNCBS_Id, DTR("IdProvinceBank"), True, oInt)
                    FillOrNothing(.MsProvinceNCBS_Name, DTR("ProvinceNameBank"), True, oString)
                    FillOrNothing(.MsProvinceNCBS_IsActive, 1, True, oBit)
                    FillOrNothing(.MsProvinceNCBS_CreatedBy, GET_PKUserID, True, oString)
                    FillOrNothing(.MsProvinceNCBS_CreatedDate, Now, True, oDate)
                    temp.Add(MsProvinceNCBS)
                End With
            End Using

        Next

        Return temp
    End Function



    Function ConvertDTtoMsProvince(ByVal DTR As DataRow) As MsProvince_ApprovalDetail
        Dim temp As New MsProvince_ApprovalDetail
        Using MsProvince As New MsProvince_ApprovalDetail()
            With MsProvince
                FillOrNothing(.IdProvince, DTR("IdProvince"), True, oInt)
                FillOrNothing(.Code, DTR("Code"), True, Ovarchar)
                FillOrNothing(.Nama, DTR("Nama"), True, Ovarchar)
                FillOrNothing(.Description, DTR("ProvinceDescription"), True, Ovarchar)

                FillOrNothing(.Activation, DTR("Activation"), True, oBit)
                FillOrNothing(.CreatedBy, GET_UserName, True, oString)
                FillOrNothing(.CreatedDate, Now, True, oDate)
            End With
            temp = MsProvince
        End Using

        Return temp
    End Function

    Function ConvertDTtoMsProvince(ByVal DT As System.Data.DataTable) As TList(Of MsProvince_ApprovalDetail)
        Dim temp As New TList(Of MsProvince_ApprovalDetail)
        For Each DTR As DataRow In DT.Rows
            Using MsProvince As New MsProvince_ApprovalDetail()
                With MsProvince
                    FillOrNothing(.IdProvince, DTR("IdProvince"), True, oInt)
                    FillOrNothing(.Code, DTR("Code"), True, Ovarchar)
                    FillOrNothing(.Nama, DTR("Nama"), True, Ovarchar)
                    FillOrNothing(.Description, DTR("ProvinceDescription"), True, Ovarchar)

                    FillOrNothing(.Activation, DTR("Activation"), True, oBit)
                    FillOrNothing(.CreatedBy, GET_UserName, True, oString)
                    FillOrNothing(.CreatedDate, Now, True, oDate)
                    temp.Add(MsProvince)
                End With
            End Using
        Next
        Return temp
    End Function

    Private Sub ChangeMultiView(ByVal index As Integer)
        MultiViewKPIOvertimeUpload.ActiveViewIndex = index
        If index = 0 Then
            Imgbtnsave.Visible = False
            Imgbtncancel.Visible = False
        ElseIf index = 1 Then
            Imgbtncancel.Visible = True
        ElseIf index = 2 Then
            Imgbtncancel.Visible = False
            Imgbtnsave.Visible = False
        End If
    End Sub

    Private Function GenerateGroupingMsProvince() As List(Of AMLBLL.MsProvinceBll.DT_Group_MsProvince)
        Dim Ltemp As New List(Of AMLBLL.MsProvinceBll.DT_Group_MsProvince)
        For i As Integer = 0 To DT_AllMsProvince.Rows.Count - 1
            Dim MsProvince As New AMLBLL.MsProvinceBll.DT_Group_MsProvince
            With MsProvince
                .MsProvince = DT_AllMsProvince.Rows(i)
                .L_MsProvinceNCBS = DT_AllMsProvinceNCBS.Clone

                'Insert MsProvinceNCBS
                Dim MsProvince_ID As String = Safe(.MsProvince("IdProvince"))
                Dim MsProvince_nama As String = Safe(.MsProvince("Nama"))
                Dim MsProvince_Activation As String = Safe(.MsProvince("Activation"))
                If DT_AllMsProvinceNCBS.Rows.Count > 0 Then
                    Using DV As New DataView(DT_AllMsProvinceNCBS)
                        DV.RowFilter = "IdProvince='" & MsProvince_ID & "' and " & _
                                                  "Nama='" & MsProvince_nama & "' and " & _
                                                  "activation='" & MsProvince_Activation & "'"
                        If DV.Count > 0 Then
                            Dim temp As System.Data.DataTable = DV.ToTable
                            For Each H As DataRow In temp.Rows
                                .L_MsProvinceNCBS.ImportRow(H)
                            Next
                        End If
                    End Using
                End If




            End With
            Ltemp.Add(MsProvince)
        Next
        Return Ltemp
    End Function

    Public Sub BindGrid()
        lblValueFailedData.Text = SetnGetDataTableForAnomalyData.Rows.Count
        GridAnomalyList.DataSource = SetnGetDataTableForAnomalyData
        GridAnomalyList.DataBind()
        lblValueSuccessData.Text = SetnGetDataTableForSuccessData.Count
        GridSuccessList.DataSource = SetnGetDataTableForSuccessData
        lblValueuploadeddata.Text = DT_AllMsProvince.Rows.Count
        GridSuccessList.DataBind()
    End Sub

#End Region

#Region "events..."


    Protected Sub ImgBtnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imgbtncancel.Click
        ClearThisPageSessions()
        ChangeMultiView(0)
        LinkButtonExportExcel.Visible = True
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Try
            If IsPostBack Then
                Return
            End If

            ClearThisPageSessions()
            SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
            ChangeMultiView(0)
        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub ImgBtnAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAdd.Click
        ChangeMultiView(0)
    End Sub

    Protected Sub GridAnomalyList_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles GridAnomalyList.PageIndexChanged
        GridAnomalyList.CurrentPageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub GridSuccessList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridSuccessList.ItemDataBound
        Try
            Dim CollCount As Integer = e.Item.Cells.Count - 1
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then

                'Show mapping item
                Dim StringMaping As New StringBuilder
                Dim DV As New DataView(DT_AllMsProvinceNCBS)
                Dim DT As New System.Data.DataTable
                DV.RowFilter = "IdProvince='" & e.Item.Cells(0).Text & "' and " & "Nama='" & e.Item.Cells(2).Text & "'"
                DT = DV.ToTable
                If DT.Rows.Count > 0 Then
                    Dim countField As Integer = 7
                    Dim lengthField As Integer = 0
                    For Each idx As DataRow In DT.Rows
                        With idx
                            If lengthField > countField Then
                                StringMaping.AppendLine(idx("ProvinceNameBank") & "(" & idx("IdProvinceBank") & ");")
                                lengthField = 0
                            Else
                                StringMaping.AppendLine(idx("ProvinceNameBank") & "(" & idx("IdProvinceBank") & ");")
                                lengthField += 1
                            End If

                        End With
                    Next
                Else
                    StringMaping.Append("-")
                End If
                e.Item.Cells(CollCount).Text = StringMaping.ToString

            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub GridSuccessList_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles GridSuccessList.PageIndexChanged
        Dim GridTarget As DataGrid = CType(source, DataGrid)
        Try
            GridSuccessList.CurrentPageIndex = e.NewPageIndex
            BindGrid()
        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgSaveSuccessData_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imgbtnsave.Click
        Try
            'File belum diupload
            If DT_AllMsProvince Is Nothing Then
                Throw New Exception("Load data failed")
            End If

            'Save MsProvince Approval (Header)
            Dim KeyApp As String
            Using Aproval As New MsProvince_Approval()
                With Aproval
                    .Fk_MsUser_Id = GET_PKUserID
                    .RequestedDate = Date.Now
                    .Fk_MsMode_Id = 2 'Edit
                    .IsUpload = True
                End With
                DataRepository.MsProvince_ApprovalProvider.Save(Aproval)
                'ambil key approval
                KeyApp = Aproval.Pk_MsProvince_Approval_Id
            End Using

            For k As Integer = 0 To GroupTableSuccessData.Count - 1

                Dim MsProvince As GrouP_MsProvince_success = GroupTableSuccessData(k)


                With MsProvince
                    'save MsProvince approval detil
                    .MsProvince.Fk_MsProvince_Approval_Id = KeyApp
                    DataRepository.MsProvince_ApprovalDetailProvider.Save(.MsProvince)
                    'ambil key MsProvince approval detil
                    Dim IdMsProvince As String = .MsProvince.IdProvince
                    Dim L_MappingMsProvinceNCBSPPATK_Approval_Detail As New TList(Of MappingMsProvinceNCBSPPATK_Approval_Detail)
                    'save mapping dan NCBS
                    For Each OMsProvinceNCBS_ApprovalDetail As MsProvinceNCBS_approvalDetail In .L_MsProvinceNCBS
                        OMsProvinceNCBS_ApprovalDetail.PK_MsProvince_Approval_Id = KeyApp
                        Using OMappingMsProvinceNCBSPPATK_Approval_Detail As New MappingMsProvinceNCBSPPATK_Approval_Detail
                            With OMappingMsProvinceNCBSPPATK_Approval_Detail
                                FillOrNothing(.IDProvinceNCBS, OMsProvinceNCBS_ApprovalDetail.Pk_MsProvinceNCBS_Id, True, oInt)
                                FillOrNothing(.IDProvince, IdMsProvince, True, oInt)
                                FillOrNothing(.PK_MsProvince_Approval_Id, KeyApp, True, oInt)
                                FillOrNothing(.Nama, OMsProvinceNCBS_ApprovalDetail.MsProvinceNCBS_Name, True, oString)
                            End With
                            L_MappingMsProvinceNCBSPPATK_Approval_Detail.Add(OMappingMsProvinceNCBSPPATK_Approval_Detail)
                        End Using
                    Next
                    'save maping dan NCBS
                    DataRepository.MappingMsProvinceNCBSPPATK_Approval_DetailProvider.Save(L_MappingMsProvinceNCBSPPATK_Approval_Detail)
                    DataRepository.MsProvinceNCBS_approvalDetailProvider.Save(.L_MsProvinceNCBS)

                End With
            Next

            LblConfirmation.Text = "MsProvince data has edited (" & SetnGetDataTableForSuccessData.Count & " Rows / MsProvince)"
            ChangeMultiView(2)

        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgUpload_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgUpload.Click
        Dim CombinedFileName As String
        GroupTableSuccessData = New List(Of GrouP_MsProvince_success)
        GroupTableAnomalyData = New List(Of AMLBLL.MsProvinceBll.DT_Group_MsProvince)
        Try
            'validasi File
            If FileUploadKPI.FileName = "" Then
                Throw New Exception("Data required")
            End If

            If Path.GetExtension(FileUploadKPI.FileName) <> ".xls" Then
                Throw New Exception("Invalid File Upload Extension")
            End If

            CombinedFileName = SaveFileImportToSave()
            If String.Compare(CombinedFileName, "", False) <> 0 Then
                Try
                    ProcessExcelFile(FilePath)
                Catch ex As Exception
                    Throw New Exception("Invalid File Format")
                End Try
            Else
                Throw New Exception("Invalid File Format")
            End If

            'Import from Excell to property Global
            ImportExcelTempToDTproperty()

            Dim LG_MsProvince As List(Of AMLBLL.MsProvinceBll.DT_Group_MsProvince) = GenerateGroupingMsProvince()
            'tambahkan kolom untuk tampilan di grid anomaly
            DT_AllMsProvince.Columns.Add("Description")
            DT_AllMsProvince.Columns.Add("errorline")

            'Membuat tempory untuk yang anomali dan yang sukses
            Dim DTnormal As System.Data.DataTable = DT_AllMsProvince.Clone
            Dim DTAnomaly As System.Data.DataTable = DT_AllMsProvince.Clone

            'proses pemilahan data anomaly dan data sukses melalui validasi 
            For i As Integer = 0 To LG_MsProvince.Count - 1
                Dim Lerror As List(Of String) = ValidateInput(LG_MsProvince(i))
                Dim MSG As String = Lerror(0)
                Dim LINE As String = Lerror(1)
                If MSG = "" Then
                    DTnormal.ImportRow(LG_MsProvince(i).MsProvince)
                    Dim GroupMsProvince As New GrouP_MsProvince_success
                    With GroupMsProvince
                        'konversi dari datatable ke Object Nettier
                        .MsProvince = ConvertDTtoMsProvince(LG_MsProvince(i).MsProvince)
                        .L_MsProvinceNCBS = ConvertDTtoMsProvinceNCBS(LG_MsProvince(i).L_MsProvinceNCBS)
                    End With
                    'masukan ke group sukses
                    GroupTableSuccessData.Add(GroupMsProvince)
                Else
                    DT_AllMsProvince.Rows(i)("Description") = MSG
                    DT_AllMsProvince.Rows(i)("errorline") = LINE
                    DTAnomaly.ImportRow(DT_AllMsProvince.Rows(i))
                    GroupTableAnomalyData.Add(LG_MsProvince(i))
                End If

            Next

            SetnGetDataTableForSuccessData = ConvertDTtoMsProvince(DTnormal)
            SetnGetDataTableForAnomalyData = DTAnomaly

            If DTAnomaly.Rows.Count > 0 Then
                Imgbtnsave.Visible = False
                Imgbtncancel.Visible = True
            Else
                Imgbtnsave.Visible = True
                Imgbtncancel.Visible = True
            End If

            BindGrid()
            If GridAnomalyList.Items.Count < 1 Then
                LinkButtonExportExcel.Visible = False
            Else
                LinkButtonExportExcel.Visible = True
            End If
            ChangeMultiView(1)
        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

#End Region

    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Dim Exp As New AMLBLL.MsProvinceBll
            Dim FolderPath As String = Server.MapPath("FolderTemplate")

            Dim buffer As Byte() = Exp.ExportDTtoExcel(GroupTableAnomalyData, FolderPath)
            Response.AddHeader("Content-type", "application/vnd.ms-excel")
            Response.AddHeader("Content-Disposition", "attachment; filename=MsProvinceUpload.xls")
            Response.BinaryWrite(buffer)
            Response.Flush()
            Response.End()

        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

        ' Verifies that the control is rendered 

    End Sub

    Protected Sub GridSuccessList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridSuccessList.SelectedIndexChanged

    End Sub
End Class





