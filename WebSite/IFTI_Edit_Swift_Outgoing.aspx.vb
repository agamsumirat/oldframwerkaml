#Region "Imports..."
Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports System.Data
Imports System.Collections.Generic
Imports amlbll
Imports amlbll.ValidateBLL
Imports amlbll.DataType
Imports Sahassa.AML.Commonly
#End Region
Partial Class IFTI_Edit_Swift_Outgoing
    Inherits Parent
    Private BindGridFromExcel As Boolean = False
#Region "properties..."
    ReadOnly Property getIFTIPK() As Integer
        Get
            If Session("IFTIEdit.IFTIPK") = Nothing Then
                Session("IFTIEdit.IFTIPK") = CInt(Request.Params("ID"))
            End If
            Return Session("IFTIEdit.IFTIPK")
        End Get
    End Property
    Public Property getIFTIBeneficiaryTempPK() As Integer
        Get
            Return IIf(Session("IFTIEdit.IFTIBeneficiaryTempPK") Is Nothing, Nothing, Session("IFTIEdit.IFTIBeneficiaryTempPK"))
        End Get
        Set(ByVal value As Integer)
            Session("IFTIEdit.IFTIBeneficiaryTempPK") = value
        End Set
       
    End Property
    'Public Property ListObjBeneficiary() As TList(Of IFTI_Beneficiary)
    '    Get
    '        Return Session("ListObjBeneficiary.data")
    '    End Get
    '    Set(ByVal value As TList(Of IFTI_Beneficiary))
    '        Session("ListObjBeneficiary.data") = value
    '    End Set
    'End Property
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("IFTIViewSelected") Is Nothing, New ArrayList, Session("IFTIViewSelected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("IFTIViewSelected") = value
        End Set
    End Property
    Private Property SetnGetSenderAccount() As String
        Get
            Return IIf(Session("IFTIEdit.SenderAccount") Is Nothing, "", Session("IFTIEdit.SenderAccount"))
        End Get
        Set(ByVal Value As String)
            Session("IFTIEdit.SenderAccount") = Value
        End Set
    End Property
    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("IFTIEditSort") Is Nothing, "Pk_IFTI_Id  asc", Session("IFTIEditSort"))
        End Get
        Set(ByVal Value As String)
            Session("IFTIEditSort") = Value
        End Set
    End Property

    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("IFTIEditCurrentPage") Is Nothing, 0, Session("IFTIEditCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("IFTIEditCurrentPage") = Value
        End Set
    End Property

    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property

    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("IFTIEditRowTotal") Is Nothing, 0, Session("IFTIEditRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("IFTIEditRowTotal") = Value
        End Set
    End Property
    'ReadOnly Property getSenderAccount() As Integer
    '    Get
    '        If Session("IFTIEdit.SenderAccount") = Nothing Then
    '            Session("IFTIEdit.SenderAccount") = CInt(Request.Params("ID"))
    '        End If
    '        Return Session("IFTIEdit.SenderAccount")
    '    End Get
    'End Property
    Private Property SetnGetCurrentPageReceiver() As Integer
        Get
            Dim result As Integer = 0
            If Not (Session("IFTIEdit.CurrentPageReceiver") Is Nothing) Then
                result = CType(Session("IFTIEdit.CurrentPageReceiver"), Integer)
            End If

            Return result
        End Get
        Set(ByVal Value As Integer)
            Session("IFTIEdit.CurrentPageReceiver") = Value
        End Set
    End Property
    Private Property ReceiverDataTable() As Data.DataTable
        Get
            Dim dt As Data.DataTable = DataRepository.IFTI_BeneficiaryProvider.GetPaged(IFTI_BeneficiaryColumn.FK_IFTI_ID.ToString & " = " & getIFTIPK, "", 0, Integer.MaxValue, 0).ToDataSet(False).Tables(0)
            dt = CType(IIf(Session("IFTIEdit.Receiver") Is Nothing, dt, Session("IFTIEdit.Receiver")), Data.DataTable)
            Session("IFTIEdit.Receiver") = dt
            Return dt
        End Get
        Set(ByVal value As Data.DataTable)
            Session("IFTIEdit.Receiver") = value
        End Set
    End Property

#End Region
#Region "Saving..."
    Sub SaveSwiftOut()
        Try

            Using OTrans As TransactionManager = New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)

                Try
                    OTrans.BeginTransaction()
                    Page.Validate("handle")

                    ValidasiControl()
                    'cekPengirim

                    Dim pjkbank As Integer = CboSwiftOutPengirim.SelectedValue
                    Dim tipepengirim As Integer
                    Dim tipenasabah As Integer
                    Dim senderType As Integer
                   
                    If pjkbank = 1 Then
                        If Rb_IdenPengirimNas_TipePengirim.SelectedIndex = -1 Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Tipe Pengirim harus diisi  ")
                        tipepengirim = Rb_IdenPengirimNas_TipePengirim.SelectedValue
                        If tipepengirim = 1 Then
                            
                            If Rb_IdenPengirimNas_TipeNasabah.SelectedIndex = -1 Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Tipe Nasabah harus diisi  ")
                            tipenasabah = CInt(Rb_IdenPengirimNas_TipeNasabah.SelectedValue)
                            If tipenasabah = 1 Then
                                senderType = 1
                            Else
                                senderType = 2
                            End If
                        Else
                            senderType = 3
                        End If
                    Else
                        If Rb_IdenPengirimNas_TipeNasabah.SelectedIndex = -1 Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Tipe Nasabah harus diisi  ")
                        tipenasabah = CInt(Rb_IdenPengirimNas_TipeNasabah.SelectedValue)
                        If tipenasabah = 1 Then
                            senderType = 4
                        Else
                            senderType = 5
                        End If
                    End If

                    Select Case (senderType)
                        Case 1
                            'pengirimNasabahIndividu
                            'cekvalidasi
                            validasiSender1()
                        Case 2
                            'pengirimNasabahKorporasi
                            'cekvalidasi
                            validasiSender2()
                        Case 3
                            'PengirimNonNasabah
                            'cekvalidasi
                            validasiSender3()
                        Case 4
                            'PenererusIndividu
                            'cekvalidasi
                            validasiSender4()
                        Case 5
                            'penerusKorporasi
                            'cekValidasi
                            validasiSender5()
                    End Select

                    'cekBOwner
                    If Rb_BOwnerNas_ApaMelibatkan.SelectedValue = 1 Then

                        If BenfOwnerHubunganPemilikDana.Text.Length < 2 Then Throw New Exception("Identitas Beneficial Owner : Hubungan pemilik dana harus diisi minimal 3 karakter!")
                        If Rb_BOwnerNas_TipePengirim.SelectedIndex <> -1 Then
                            Dim BOwnerID As Integer = Rb_BOwnerNas_TipePengirim.SelectedValue
                            Select Case (BOwnerID)
                                Case 1
                                    'Nasabah
                                    Me.validationBOwnerNasabah()
                                Case 2
                                    'NonNasabah
                                    Me.validationBOwnerNonNasabah()
                            End Select
                        Else
                            Throw New Exception("Identitas Beneficial Owner : Tipe pengirim harus diisi!")
                        End If
                        
                    End If

                    'cek receiver
                    ValidateReceiverSaveAll()
                    'cekTransaksi
                    validasiTransaksi()


                    If Page.IsValid Then
                        'parameter value

                        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
                        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
                        Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
                        Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)
                        Dim ParameterBidangUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BidangUsaha)
                        ' =========== Insert Header Approval
                        Dim KeyHeaderApproval As Integer
                        Using objIftiApproval As New IFTI_Approval
                            With objIftiApproval
                                .RequestedBy = Sahassa.AML.Commonly.SessionPkUserId
                                .RequestedDate = Now()
                                .FK_MsMode_Id = 2
                                .Fk_IFTI_Type_ID = 1
                                ' .IsUpload = 0
                                'FillOrNothing(.FK_MsMode_Id, 2, True, oInt)
                                'FillOrNothing(.RequestedBy, Sahassa.aml.Commonly.SessionPkUserId, True, oInt)
                                'FillOrNothing(.RequestedDate, Date.Now)
                                'FillOrNothing(.IsUpload, False)
                            End With
                            DataRepository.IFTI_ApprovalProvider.Save(OTrans, objIftiApproval)
                            KeyHeaderApproval = objIftiApproval.PK_IFTI_Approval_Id
                        End Using

                        '============ Insert Detail Approval
                        Using objIfti_ApprovalDetail As New IFTI_Approval_Detail()
                            Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)

                                With objIfti_ApprovalDetail
                                    'FK
                                    .PK_IFTI_ID_Old = objIfti.PK_IFTI_ID
                                    .PK_IFTI_ID = objIfti.PK_IFTI_ID
                                    .FK_IFTI_Approval_Id = KeyHeaderApproval
                                    .FK_IFTI_Type_ID_Old = objIfti.FK_IFTI_Type_ID
                                    .FK_IFTI_Type_ID = 1

                                    'ValidasiControl()
                                    'umum
                                    '--old--
                                    FillOrNothing(.LTDLNNo_Old, objIfti.LTDLNNo)
                                    FillOrNothing(.LTDLNNoKoreksi_Old, objIfti.LTDLNNoKoreksi)
                                    FillOrNothing(.TanggalLaporan_Old, objIfti.TanggalLaporan)
                                    FillOrNothing(.NamaPJKBankPelapor_Old, objIfti.NamaPJKBankPelapor)
                                    FillOrNothing(.NamaPejabatPJKBankPelapor_Old, objIfti.NamaPejabatPJKBankPelapor)
                                    FillOrNothing(.JenisLaporan_Old, objIfti.JenisLaporan)
                                    '--new--
                                    FillOrNothing(.LTDLNNo, Me.TxtUmum_LTDLN.Text, False, Ovarchar)
                                    FillOrNothing(.LTDLNNoKoreksi, Me.TxtUmum_LTDLNKoreksi.Text, False, Ovarchar)
                                    FillOrNothing(.TanggalLaporan, Me.TxtUmum_TanggalLaporan.Text, False, oDate)
                                    FillOrNothing(.NamaPJKBankPelapor, Me.TxtUmum_PJKBankPelapor.Text, False, Ovarchar)
                                    FillOrNothing(.NamaPejabatPJKBankPelapor, Me.TxtUmum_NamaPejabatPJKBankPelapor.Text, False, Ovarchar)
                                    FillOrNothing(.JenisLaporan, Me.RbUmum_JenisLaporan.SelectedValue, False, oInt)

                                    'cekPengirimOld
                                    Dim senderold As Integer = objIfti.Sender_FK_IFTI_NasabahType_ID
                                    .Sender_PJKBank_Type_Old = objIfti.Sender_FK_PJKBank_Type

                                    Select Case (senderold)
                                        Case 1
                                            'pengirimNasabahIndividu

                                            FillOrNothing(.Sender_FK_IFTI_NasabahType_ID_Old, objIfti.Sender_FK_IFTI_NasabahType_ID)
                                            FillOrNothing(.Sender_Nasabah_INDV_NoRekening_Old, objIfti.Sender_Nasabah_INDV_NoRekening)
                                            FillOrNothing(.Sender_Nasabah_INDV_NamaLengkap_Old, objIfti.Sender_Nasabah_INDV_NamaLengkap)
                                            FillOrNothing(.Sender_Nasabah_INDV_TanggalLahir_Old, objIfti.Sender_Nasabah_INDV_TanggalLahir)
                                            FillOrNothing(.Sender_Nasabah_INDV_KewargaNegaraan_Old, objIfti.Sender_Nasabah_INDV_KewargaNegaraan)
                                            If getStringFieldValue(.Sender_Nasabah_INDV_KewargaNegaraan_Old) = "2" Then
                                                FillOrNothing(.Sender_Nasabah_INDV_Negara_Old, objIfti.Sender_Nasabah_INDV_Negara)
                                                FillOrNothing(.Sender_Nasabah_INDV_NegaraLainnya_Old, objIfti.Sender_Nasabah_INDV_NegaraLainnya)
                                            End If
                                            FillOrNothing(.Sender_Nasabah_INDV_Pekerjaan_Old, objIfti.Sender_Nasabah_INDV_Pekerjaan)
                                            FillOrNothing(.Sender_Nasabah_INDV_PekerjaanLainnya_Old, objIfti.Sender_Nasabah_INDV_PekerjaanLainnya)
                                            FillOrNothing(.Sender_Nasabah_INDV_DOM_Alamat_Old, objIfti.Sender_Nasabah_INDV_ID_Alamat)
                                            FillOrNothing(.Sender_Nasabah_INDV_DOM_KotaKab_Old, objIfti.Sender_Nasabah_INDV_ID_KotaKab)
                                            FillOrNothing(.Sender_Nasabah_INDV_DOM_KotaKabLainnya_Old, objIfti.Sender_Nasabah_INDV_ID_KotaKabLainnya)
                                            FillOrNothing(.Sender_Nasabah_INDV_DOM_Propinsi_Old, objIfti.Sender_Nasabah_INDV_DOM_Propinsi)
                                            FillOrNothing(.Sender_Nasabah_INDV_DOM_PropinsiLainnya_Old, objIfti.Sender_Nasabah_INDV_DOM_PropinsiLainnya)
                                            FillOrNothing(.Sender_Nasabah_INDV_ID_Alamat_Old, objIfti.Sender_Nasabah_INDV_ID_Alamat)
                                            FillOrNothing(.Sender_Nasabah_INDV_ID_KotaKab_Old, objIfti.Sender_Nasabah_INDV_ID_KotaKab)
                                            FillOrNothing(.Sender_Nasabah_INDV_ID_KotaKabLainnya_Old, objIfti.Sender_Nasabah_INDV_ID_KotaKabLainnya)
                                            FillOrNothing(.Sender_Nasabah_INDV_ID_Propinsi_Old, objIfti.Sender_Nasabah_INDV_ID_Propinsi)
                                            FillOrNothing(.Sender_Nasabah_INDV_ID_PropinsiLainnya_Old, objIfti.Sender_Nasabah_INDV_ID_PropinsiLainnya)
                                            FillOrNothing(.Sender_Nasabah_INDV_FK_IFTI_IDType_Old, objIfti.Sender_Nasabah_INDV_FK_IFTI_IDType)
                                            FillOrNothing(.Sender_Nasabah_INDV_NomorID_Old, objIfti.Sender_Nasabah_INDV_NomorID)

                                        Case 2
                                            'pengirimNasabahKorporasi
                                        
                                            FillOrNothing(.Sender_FK_IFTI_NasabahType_ID_Old, objIfti.Sender_FK_IFTI_NasabahType_ID)
                                            FillOrNothing(.Sender_Nasabah_CORP_NoRekening_Old, objIfti.Sender_Nasabah_CORP_NoRekening)
                                            FillOrNothing(.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id_Old, objIfti.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id)
                                            FillOrNothing(.Sender_Nasabah_CORP_BentukBadanUsahaLainnya_Old, objIfti.Sender_Nasabah_CORP_BentukBadanUsahaLainnya)
                                            FillOrNothing(.Sender_Nasabah_CORP_NamaKorporasi_Old, objIfti.Sender_Nasabah_CORP_NamaKorporasi)
                                            FillOrNothing(.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id_Old, objIfti.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id)
                                            FillOrNothing(.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id_Old, objIfti.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id)
                                            FillOrNothing(.Sender_Nasabah_CORP_BidangUsahaLainnya_Old, objIfti.Sender_Nasabah_CORP_BidangUsahaLainnya)
                                            FillOrNothing(.Sender_Nasabah_CORP_AlamatLengkap_Old, objIfti.Sender_Nasabah_CORP_AlamatLengkap)
                                            FillOrNothing(.Sender_Nasabah_COR_KotaKab_Old, objIfti.Sender_Nasabah_CORP_KotaKab)
                                            FillOrNothing(.Sender_Nasabah_COR_KotaKabLainnya_Old, objIfti.Sender_Nasabah_CORP_KotaKabLainnya)
                                            FillOrNothing(.Sender_Nasabah_CORP_Propinsi_Old, objIfti.Sender_Nasabah_CORP_Propinsi)
                                            FillOrNothing(.Sender_Nasabah_CORP_PropinsiLainnya_Old, objIfti.Sender_Nasabah_CORP_PropinsiLainnya)
                                            FillOrNothing(.Sender_Nasabah_CORP_LN_AlamatLengkap_Old, objIfti.Sender_Nasabah_CORP_LN_AlamatLengkap)
                                            FillOrNothing(.Sender_Nasabah_CORP_Alamat_KantorCabangPengirim_Old, objIfti.Sender_Nasabah_CORP_Alamat_KantorCabangPengirim)

                                        Case 3
                                            'PengirimNonNasabah
                                            FillOrNothing(.Sender_FK_IFTI_NasabahType_ID_Old, objIfti.Sender_FK_IFTI_NasabahType_ID)
                                            FillOrNothing(.FK_IFTI_NonNasabahNominalType_ID_Old, objIfti.FK_IFTI_NonNasabahNominalType_ID)
                                            FillOrNothing(.Sender_NonNasabah_NamaLengkap_Old, objIfti.Sender_NonNasabah_NamaLengkap)
                                            FillOrNothing(.Sender_NonNasabah_TanggalLahir_Old, objIfti.Sender_NonNasabah_TanggalLahir)
                                            FillOrNothing(.Sender_NonNasabah_ID_Alamat_Old, objIfti.Sender_NonNasabah_ID_Alamat)
                                            FillOrNothing(.Sender_NonNasabah_ID_KotaKab_Old, objIfti.Sender_NonNasabah_ID_KotaKab)
                                            FillOrNothing(.Sender_NonNasabah_ID_KotaKabLainnya_Old, objIfti.Sender_NonNasabah_ID_KotaKabLainnya)
                                            FillOrNothing(.Sender_NonNasabah_ID_Propinsi_Old, objIfti.Sender_NonNasabah_ID_Propinsi)
                                            FillOrNothing(.Sender_NonNasabah_ID_PropinsiLainnya_Old, objIfti.Sender_NonNasabah_ID_PropinsiLainnya)
                                            FillOrNothing(.Sender_NonNasabah_NoTelp_Old, objIfti.Sender_Nasabah_INDV_NoTelp)
                                            FillOrNothing(.Sender_NonNasabah_FK_IFTI_IDType_Old, objIfti.Sender_Nasabah_INDV_FK_IFTI_IDType)
                                            FillOrNothing(.Sender_NonNasabah_NomorID_Old, objIfti.Sender_NonNasabah_NomorID)

                                        Case 4
                                            'PenererusIndividu

                                            FillOrNothing(.Sender_FK_IFTI_NasabahType_ID_Old, objIfti.Sender_FK_IFTI_NasabahType_ID)
                                            FillOrNothing(.Sender_Nasabah_INDV_NoRekening_Old, objIfti.Sender_Nasabah_INDV_NoRekening)
                                            FillOrNothing(.Sender_Nasabah_INDV_NamaLengkap_Old, objIfti.Sender_Nasabah_INDV_NamaLengkap)
                                            FillOrNothing(.Sender_Nasabah_INDV_TanggalLahir_Old, objIfti.Sender_Nasabah_INDV_TanggalLahir)
                                            FillOrNothing(.Sender_Nasabah_INDV_NamaBank_Old, objIfti.Sender_Nasabah_INDV_NamaBank)
                                            FillOrNothing(.Sender_Nasabah_INDV_KewargaNegaraan_Old, objIfti.Sender_Nasabah_INDV_KewargaNegaraan)
                                            FillOrNothing(.Sender_Nasabah_INDV_Negara_Old, objIfti.Sender_Nasabah_INDV_Negara)
                                            FillOrNothing(.Sender_Nasabah_INDV_NegaraLainnya_Old, objIfti.Sender_Nasabah_INDV_NegaraLainnya)
                                            FillOrNothing(.Sender_Nasabah_INDV_Pekerjaan_Old, objIfti.Sender_Nasabah_INDV_Pekerjaan)
                                            FillOrNothing(.Sender_Nasabah_INDV_PekerjaanLainnya_Old, objIfti.Sender_Nasabah_INDV_PekerjaanLainnya)
                                            FillOrNothing(.Sender_Nasabah_INDV_DOM_Alamat_Old, objIfti.Sender_Nasabah_INDV_ID_Alamat)
                                            FillOrNothing(.Sender_Nasabah_INDV_DOM_KotaKab_Old, objIfti.Sender_Nasabah_INDV_ID_KotaKab)
                                            FillOrNothing(.Sender_Nasabah_INDV_DOM_KotaKabLainnya_Old, objIfti.Sender_Nasabah_INDV_ID_KotaKabLainnya)
                                            FillOrNothing(.Sender_Nasabah_INDV_DOM_Propinsi_Old, objIfti.Sender_Nasabah_INDV_ID_Propinsi)
                                            FillOrNothing(.Sender_Nasabah_INDV_DOM_PropinsiLainnya_Old, objIfti.Sender_Nasabah_INDV_ID_PropinsiLainnya)
                                            FillOrNothing(.Sender_Nasabah_INDV_ID_Alamat_Old, objIfti.Sender_Nasabah_INDV_ID_Alamat)
                                            FillOrNothing(.Sender_Nasabah_INDV_ID_KotaKab_Old, objIfti.Sender_Nasabah_INDV_ID_KotaKab)
                                            FillOrNothing(.Sender_Nasabah_INDV_ID_KotaKabLainnya_Old, objIfti.Sender_Nasabah_INDV_ID_KotaKabLainnya)
                                            FillOrNothing(.Sender_Nasabah_INDV_ID_Propinsi_Old, objIfti.Sender_Nasabah_INDV_ID_Propinsi)
                                            FillOrNothing(.Sender_Nasabah_INDV_ID_PropinsiLainnya_Old, objIfti.Sender_Nasabah_INDV_ID_PropinsiLainnya)
                                            FillOrNothing(.Sender_Nasabah_INDV_FK_IFTI_IDType_Old, objIfti.Sender_Nasabah_INDV_FK_IFTI_IDType)
                                            FillOrNothing(.Sender_Nasabah_INDV_NomorID_Old, objIfti.Sender_Nasabah_INDV_NomorID)

                                        Case 5
                                            'penerusKorporasi
                                            
                                            FillOrNothing(.Sender_FK_IFTI_NasabahType_ID_Old, objIfti.Sender_FK_IFTI_NasabahType_ID)
                                            FillOrNothing(.Sender_Nasabah_CORP_NoRekening_Old, objIfti.Sender_Nasabah_CORP_NoRekening)
                                            FillOrNothing(.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id_Old, objIfti.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id)
                                            FillOrNothing(.Sender_Nasabah_CORP_BentukBadanUsahaLainnya_Old, objIfti.Sender_Nasabah_CORP_BentukBadanUsahaLainnya)
                                            FillOrNothing(.Sender_Nasabah_CORP_NamaBank_Old, objIfti.Sender_Nasabah_CORP_NamaBank)
                                            FillOrNothing(.Sender_Nasabah_CORP_NamaKorporasi_Old, objIfti.Sender_Nasabah_CORP_NamaKorporasi)
                                            FillOrNothing(.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id_Old, objIfti.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id)
                                            FillOrNothing(.Sender_Nasabah_CORP_AlamatLengkap_Old, objIfti.Sender_Nasabah_CORP_AlamatLengkap)
                                            FillOrNothing(.Sender_Nasabah_COR_KotaKab_Old, objIfti.Sender_Nasabah_CORP_KotaKab)
                                            FillOrNothing(.Sender_Nasabah_COR_KotaKabLainnya_Old, objIfti.Sender_Nasabah_CORP_KotaKabLainnya)
                                            FillOrNothing(.Sender_Nasabah_CORP_Propinsi_Old, objIfti.Sender_Nasabah_CORP_Propinsi)
                                            FillOrNothing(.Sender_Nasabah_CORP_PropinsiLainnya_Old, objIfti.Sender_Nasabah_CORP_PropinsiLainnya)
                                            FillOrNothing(.Sender_Nasabah_CORP_LN_AlamatLengkap_Old, objIfti.Sender_Nasabah_CORP_LN_AlamatLengkap)
                                            FillOrNothing(.Sender_Nasabah_CORP_Alamat_KantorCabangPengirim_Old, objIfti.Sender_Nasabah_CORP_Alamat_KantorCabangPengirim)

                                    End Select
                                    'cekPengirimNew
                                    .Sender_PJKBank_Type = Me.CboSwiftOutPengirim.SelectedValue
                                    Select Case (senderType)
                                        Case 1
                                            'pengirimNasabahIndividu
                                            
                                            FillOrNothing(.Sender_FK_IFTI_NasabahType_ID, senderType, False, oInt)
                                            FillOrNothing(.Sender_Nasabah_INDV_NoRekening, Me.TxtIdenPengirimNas_account.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_Nasabah_INDV_NamaLengkap, Me.TxtIdenPengirimNas_Ind_NamaLengkap.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_Nasabah_INDV_TanggalLahir, Me.TxtIdenPengirimNas_Ind_tanggalLahir.Text, False, oDate)
                                            FillOrNothing(.Sender_Nasabah_INDV_KewargaNegaraan, Me.Rb_IdenPengirimNas_Ind_kewarganegaraan.SelectedValue, False, oInt)
                                            If Me.Rb_IdenPengirimNas_Ind_kewarganegaraan.SelectedValue = "2" Then
                                                FillOrNothing(.Sender_Nasabah_INDV_Negara, Me.hfIdenPengirimNas_Ind_negara.Value, False, oInt)
                                                If hfIdenPengirimNas_Ind_negara.Value = ParameterNegara.MsSystemParameter_Value Then
                                                    FillOrNothing(.Sender_Nasabah_INDV_NegaraLainnya, Me.TxtIdenPengirimNas_Ind_negaralain.Text, False, Ovarchar)
                                                Else
                                                    .Sender_Nasabah_INDV_NegaraLainnya = Nothing
                                                End If
                                            Else
                                                FillOrNothing(.Sender_Nasabah_INDV_Negara, Nothing, False, oInt)
                                                FillOrNothing(.Sender_Nasabah_INDV_NegaraLainnya, Nothing, False, Ovarchar)
                                            End If
                                            FillOrNothing(.Sender_Nasabah_INDV_Pekerjaan, Me.hfIdenPengirimNas_Ind_pekerjaan.Value, False, oInt)
                                            If Me.hfIdenPengirimNas_Ind_pekerjaan.Value = ParameterPekerjaan.MsSystemParameter_Value Then
                                                FillOrNothing(.Sender_Nasabah_INDV_PekerjaanLainnya, Me.TxtIdenPengirimNas_Ind_Pekerjaanlain.Text, False, Ovarchar)
                                            Else
                                                .Sender_Nasabah_INDV_PekerjaanLainnya = Nothing
                                            End If
                                            FillOrNothing(.Sender_Nasabah_INDV_DOM_Alamat, Me.TxtIdenPengirimNas_Ind_Alamat.Text, False, Ovarchar)
                                            If ObjectAntiNull(Me.TxtIdenPengirimNas_Ind_Kota.Text) = True Then
                                                FillOrNothing(.Sender_Nasabah_INDV_DOM_KotaKab, Me.hfIdenPengirimNas_Ind_Kota_dom.Value, False, oInt)
                                            Else
                                                .Sender_Nasabah_INDV_DOM_KotaKab = Nothing
                                            End If
                                            FillOrNothing(.Sender_Nasabah_INDV_DOM_KotaKabLainnya, Me.TxtIdenPengirimNas_Ind_kotalain.Text, False, Ovarchar)
                                            If ObjectAntiNull(Me.TxtIdenPengirimNas_Ind_provinsi.Text) = True Then
                                                FillOrNothing(.Sender_Nasabah_INDV_DOM_Propinsi, Me.hfIdenPengirimNas_Ind_provinsi_dom.Value, False, oInt)
                                            Else
                                                .Sender_Nasabah_INDV_DOM_Propinsi = Nothing
                                            End If
                                            FillOrNothing(.Sender_Nasabah_INDV_DOM_PropinsiLainnya, Me.TxtIdenPengirimNas_Ind_provinsiLain.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_Nasabah_INDV_ID_Alamat, Me.TxtIdenPengirimNas_Ind_alamatIdentitas.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_Nasabah_INDV_ID_KotaKab, Me.hfIdenPengirimNas_Ind_kotaIdentitas.Value, False, oInt)
                                            If Me.hfIdenPengirimNas_Ind_kotaIdentitas.Value = ParameterKota.MsSystemParameter_Value Then
                                                FillOrNothing(.Sender_Nasabah_INDV_ID_KotaKabLainnya, Me.TxtIdenPengirimNas_Ind_KotaLainIden.Text, False, Ovarchar)
                                            Else
                                                .Sender_Nasabah_INDV_ID_KotaKabLainnya = Nothing
                                            End If
                                            FillOrNothing(.Sender_Nasabah_INDV_ID_Propinsi, Me.hfIdenPengirimNas_Ind_ProvinsiIden.Value, False, oInt)
                                            If Me.hfIdenPengirimNas_Ind_ProvinsiIden.Value = ParameterProvinsi.MsSystemParameter_Value Then
                                                FillOrNothing(.Sender_Nasabah_INDV_ID_PropinsiLainnya, Me.TxtIdenPengirimNas_Ind_ProvinsilainIden.Text, False, Ovarchar)
                                            Else
                                                .Sender_Nasabah_INDV_ID_PropinsiLainnya = Nothing
                                            End If
                                            FillOrNothing(.Sender_Nasabah_INDV_FK_IFTI_IDType, Me.cboPengirimNasInd_jenisidentitas.SelectedValue, False, oInt)
                                            FillOrNothing(.Sender_Nasabah_INDV_NomorID, Me.TxtIdenPengirimNas_Ind_noIdentitas.Text, False, Ovarchar)

                                        Case 2
                                            'pengirimNasabahKorporasi
                                            FillOrNothing(.Sender_FK_IFTI_NasabahType_ID, senderType, False, oInt)
                                            FillOrNothing(.Sender_Nasabah_CORP_NoRekening, Me.TxtIdenPengirimNas_account.Text, False, Ovarchar)
                                            If CBOIdenPengirimNas_Corp_BentukBadanUsaha.SelectedIndex > 0 Then
                                                FillOrNothing(.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id, Me.CBOIdenPengirimNas_Corp_BentukBadanUsaha.SelectedValue, False, oInt)
                                            Else
                                                FillOrNothing(.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id, Nothing)
                                            End If
                                            FillOrNothing(.Sender_Nasabah_CORP_BentukBadanUsahaLainnya, Me.TxtIdenPengirimNas_Corp_BentukBadanUsahaLain.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_Nasabah_CORP_NamaKorporasi, Me.TxtIdenPengirimNas_Corp_NamaKorp.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id, Me.hfIdenPengirimNas_Corp_BidangUsahaKorp.Value, False, oInt)
                                            If Me.hfIdenPengirimNas_Corp_BidangUsahaKorp.Value = ParameterBidangUsaha.MsSystemParameter_Value Then
                                                FillOrNothing(.Sender_Nasabah_CORP_BidangUsahaLainnya, Me.TxtIdenPengirimNas_Corp_BidangUsahaKorpLainnya.Text, False, Ovarchar)
                                            Else
                                                .Sender_Nasabah_CORP_BidangUsahaLainnya = Nothing
                                            End If
                                            FillOrNothing(.Sender_Nasabah_CORP_AlamatLengkap, Me.TxtIdenPengirimNas_Corp_alamatkorp.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_Nasabah_COR_KotaKab, Me.hfIdenPengirimNas_Corp_kotakorp.Value, False, oInt)
                                            If Me.hfIdenPengirimNas_Corp_kotakorp.Value = ParameterKota.MsSystemParameter_Value Then
                                                FillOrNothing(.Sender_Nasabah_COR_KotaKabLainnya, Me.TxtIdenPengirimNas_Ind_KotaLainIden.Text, False, Ovarchar)
                                            Else
                                                .Sender_Nasabah_COR_KotaKabLainnya = Nothing
                                            End If
                                            FillOrNothing(.Sender_Nasabah_CORP_Propinsi, Me.hfIdenPengirimNas_Corp_provKorp.Value, False, oInt)
                                            If Me.hfIdenPengirimNas_Corp_provKorp.Value = ParameterProvinsi.MsSystemParameter_Value Then
                                                FillOrNothing(.Sender_Nasabah_CORP_PropinsiLainnya, Me.TxtIdenPengirimNas_Corp_provKorpLain.Text, False, Ovarchar)
                                            Else
                                                .Sender_Nasabah_CORP_PropinsiLainnya = Nothing
                                            End If
                                            FillOrNothing(.Sender_Nasabah_CORP_LN_AlamatLengkap, Me.TxtIdenPengirimNas_Corp_AlamatLuar.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_Nasabah_CORP_Alamat_KantorCabangPengirim, Me.TxtIdenPengirimNas_Corp_AlamatAsal.Text, False, Ovarchar)

                                        Case 3
                                            'PengirimNonNasabah
                                            FillOrNothing(.Sender_FK_IFTI_NasabahType_ID, senderType, False, oInt)
                                            FillOrNothing(.FK_IFTI_NonNasabahNominalType_ID, Me.RbPengirimNonNasabah_100Juta.SelectedValue, False, oInt)
                                            FillOrNothing(.Sender_NonNasabah_NamaLengkap, Me.TxtPengirimNonNasabah_nama.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_NonNasabah_TanggalLahir, Me.TxtPengirimNonNasabah_TanggalLahir.Text, False, oDate)
                                            FillOrNothing(.Sender_NonNasabah_ID_Alamat, Me.TxtPengirimNonNasabah_alamatiden.Text, False, Ovarchar)
                                            If ObjectAntiNull(TxtPengirimNonNasabah_kotaIden.Text) = True Then
                                                FillOrNothing(.Sender_NonNasabah_ID_KotaKab, Me.hfPengirimNonNasabah_kotaIden.Value, False, oInt)
                                            Else
                                                .Sender_NonNasabah_ID_KotaKab = Nothing
                                            End If
                                            FillOrNothing(.Sender_NonNasabah_ID_KotaKabLainnya, Me.TxtPengirimNonNasabah_kotaIden.Text, False, Ovarchar)
                                            If ObjectAntiNull(TxtPengirimNonNasabah_ProvIden.Text) = True Then
                                                FillOrNothing(.Sender_NonNasabah_ID_Propinsi, Me.hfPengirimNonNasabah_ProvIden.Value, False, oInt)
                                            Else
                                                FillOrNothing(.Sender_NonNasabah_ID_Propinsi, Nothing)
                                            End If
                                            FillOrNothing(.Sender_NonNasabah_ID_PropinsiLainnya, Me.hfPengirimNonNasabah_ProvIden, False, oInt)
                                            FillOrNothing(.Sender_NonNasabah_FK_IFTI_IDType, Me.CboPengirimNonNasabah_JenisDokumen.SelectedValue, False, oInt)
                                            FillOrNothing(.Sender_NonNasabah_NomorID, Me.TxtPengirimNonNasabah_NomorIden.Text, False, Ovarchar)

                                        Case 4
                                            'PenererusIndividu
                                            FillOrNothing(.Sender_FK_IFTI_NasabahType_ID, senderType, False, oInt)
                                            FillOrNothing(.Sender_Nasabah_INDV_NoRekening, Me.TxtPengirimPenerus_Rekening.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_Nasabah_INDV_NamaLengkap, Me.TxtPengirimPenerus_Ind_nama.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_Nasabah_INDV_TanggalLahir, Me.TxtPengirimPenerus_Ind_TanggalLahir.Text, False, oDate)
                                            FillOrNothing(.Sender_Nasabah_INDV_NamaBank, Me.TxtPengirimPenerus_Ind_namaBank.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_Nasabah_INDV_KewargaNegaraan, getStringFieldValue(Me.RBPengirimPenerus_Ind_Kewarganegaraan.SelectedValue), False, oInt)
                                            If Me.RbPenerimaNasabah_IND_Warganegara.SelectedValue = "2" Then
                                                FillOrNothing(.Sender_Nasabah_INDV_Negara, Me.hfIdenPengirimNas_Ind_negara.Value, False, oInt)
                                                FillOrNothing(.Sender_Nasabah_INDV_NegaraLainnya, Me.TxtPengirimPenerus_IND_negaralain.Text, False, Ovarchar)
                                            End If
                                            If ObjectAntiNull(TxtPengirimPenerus_IND_pekerjaan.Text) = True Then
                                                FillOrNothing(.Sender_Nasabah_INDV_Pekerjaan, Me.hfPengirimPenerus_IND_pekerjaan.Value, False, oInt)
                                            Else
                                                FillOrNothing(.Sender_Nasabah_INDV_Pekerjaan, Nothing)
                                            End If
                                            FillOrNothing(.Sender_Nasabah_INDV_PekerjaanLainnya, Me.TxtPengirimPenerus_IND_pekerjaanLain.Text, Ovarchar)
                                            FillOrNothing(.Sender_Nasabah_INDV_DOM_Alamat, Me.TxtPengirimPenerus_IND_alamatDom.Text, False, Ovarchar)
                                            If ObjectAntiNull(Me.TxtPengirimPenerus_IND_kotaDom.Text) = True Then
                                                FillOrNothing(.Sender_Nasabah_INDV_DOM_KotaKab, Me.hfIdenPengirimNas_Ind_Kota_dom.Value, False, oInt)
                                            Else
                                                FillOrNothing(.Sender_Nasabah_INDV_DOM_KotaKab, Nothing)
                                            End If
                                            FillOrNothing(.Sender_Nasabah_INDV_DOM_KotaKabLainnya, Me.TxtPengirimPenerus_IND_kotaLainDom.Text, False, Ovarchar)
                                            If ObjectAntiNull(Me.TxtPengirimPenerus_IND_ProvDom.Text) = True Then
                                                FillOrNothing(.Sender_Nasabah_INDV_DOM_Propinsi, Me.hfPengirimPenerus_IND_ProvDom.Value, False, oInt)
                                            Else
                                                FillOrNothing(.Sender_Nasabah_INDV_DOM_Propinsi, Nothing)
                                            End If
                                            FillOrNothing(.Sender_Nasabah_INDV_DOM_PropinsiLainnya, Me.TxtPengirimPenerus_IND_ProvLainIden.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_Nasabah_INDV_ID_Alamat, Me.TxtPengirimPenerus_IND_alamatIden.Text, False, Ovarchar)
                                            If ObjectAntiNull(Me.TxtPengirimPenerus_IND_kotaIden.Text) = True Then
                                                FillOrNothing(.Sender_Nasabah_INDV_ID_KotaKab, Me.hfPengirimPenerus_IND_kotaIden.Value, False, oInt)
                                            Else
                                                FillOrNothing(.Sender_Nasabah_INDV_ID_KotaKab, Nothing)
                                            End If
                                            FillOrNothing(.Sender_Nasabah_INDV_ID_KotaKabLainnya, Me.TxtPengirimPenerus_IND_KotaLainIden.Text, False, Ovarchar)
                                            If ObjectAntiNull(Me.TxtPengirimPenerus_IND_ProvIden.Text) = True Then
                                                FillOrNothing(.Sender_Nasabah_INDV_ID_Propinsi, Me.hfIdenPengirimNas_Ind_ProvinsiIden.Value, False, oInt)
                                            Else
                                                FillOrNothing(.Sender_Nasabah_INDV_ID_Propinsi, Nothing)
                                            End If
                                            FillOrNothing(.Sender_Nasabah_INDV_ID_PropinsiLainnya, Me.TxtPengirimPenerus_IND_ProvLainIden.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_Nasabah_INDV_FK_IFTI_IDType, Me.CBoPengirimPenerus_IND_jenisIdentitas.SelectedValue, False, oInt)
                                            FillOrNothing(.Sender_Nasabah_INDV_NomorID, Me.TxtPengirimPenerus_IND_nomoridentitas.Text, False, Ovarchar)

                                        Case 5
                                            'penerusKorporasi

                                            FillOrNothing(.Sender_FK_IFTI_NasabahType_ID, senderType, False, oInt)
                                            FillOrNothing(.Sender_Nasabah_CORP_NoRekening, Me.TxtPengirimPenerus_Rekening.Text, False, Ovarchar)
                                            If CBOPengirimPenerus_Korp_BentukBadanUsaha.SelectedIndex > 0 Then
                                                FillOrNothing(.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id, Me.CBOPengirimPenerus_Korp_BentukBadanUsaha.SelectedValue, False, oInt)
                                            Else
                                                FillOrNothing(.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id, Nothing)
                                            End If
                                            FillOrNothing(.Sender_Nasabah_CORP_BentukBadanUsahaLainnya, Me.txtPengirimPenerus_Korp_BentukBadanUsahaLain.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_Nasabah_CORP_NamaBank, Me.txtPengirimPenerus_Korp_namabank.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_Nasabah_CORP_NamaKorporasi, Me.txtPengirimPenerus_Korp_NamaKorp.Text, False, Ovarchar)
                                            If ObjectAntiNull(txtPengirimPenerus_Korp_BidangUsahaKorp.Text) = True Then
                                                FillOrNothing(.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id, Me.hfPengirimPenerus_Korp_BidangUsahaKorp.Value, False, oInt)
                                            Else
                                                FillOrNothing(.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id, Nothing)
                                            End If
                                            FillOrNothing(.Sender_Nasabah_CORP_AlamatLengkap, Me.txtPengirimPenerus_Korp_alamatkorp.Text, False, Ovarchar)
                                            If ObjectAntiNull(Me.txtPengirimPenerus_Korp_kotakorp.Text) = True Then
                                                FillOrNothing(.Sender_Nasabah_COR_KotaKab, Me.hfPengirimPenerus_Korp_kotakorp.Value, False, oInt)
                                            Else
                                                FillOrNothing(.Sender_Nasabah_COR_KotaKab, Nothing)
                                            End If
                                            FillOrNothing(.Sender_Nasabah_COR_KotaKabLainnya, Me.txtPengirimPenerus_Korp_kotalainnyakorp.Text, False, Ovarchar)
                                            If ObjectAntiNull(Me.txtPengirimPenerus_Korp_ProvinsiKorp.Text) = True Then
                                                FillOrNothing(.Sender_Nasabah_CORP_Propinsi, Me.hfPengirimPenerus_Korp_ProvinsiKorp.Value, False, oInt)
                                            Else
                                                FillOrNothing(.Sender_Nasabah_CORP_Propinsi, Nothing)
                                            End If
                                            FillOrNothing(.Sender_Nasabah_CORP_PropinsiLainnya, Me.TxtIdenPengirimNas_Corp_provKorpLain.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_Nasabah_CORP_LN_AlamatLengkap, Me.txtPengirimPenerus_Korp_AlamatLuar.Text, False, Ovarchar)
                                            FillOrNothing(.Sender_Nasabah_CORP_Alamat_KantorCabangPengirim, Me.txtPengirimPenerus_Korp_AlamatAsal.Text, False, Ovarchar)

                                    End Select

                                    'cekBOwner
                                    If Rb_BOwnerNas_ApaMelibatkan.SelectedValue = 1 Then
                                        FillOrNothing(.BeneficialOwner_Keterlibatan_Old, objIfti.FK_IFTI_KeterlibatanBeneficialOwner_ID)
                                        FillOrNothing(.BeneficialOwner_Keterlibatan, Me.Rb_BOwnerNas_ApaMelibatkan.Text, False, oInt)
                                        FillOrNothing(.BeneficialOwner_HubunganDenganPemilikDana_Old, objIfti.BeneficialOwner_HubunganDenganPemilikDana)
                                        FillOrNothing(.BeneficialOwner_HubunganDenganPemilikDana, Me.BenfOwnerHubunganPemilikDana.Text, False, Ovarchar)
                                        Dim BOwnerID As Integer = Rb_BOwnerNas_TipePengirim.SelectedValue
                                        Select Case (BOwnerID)
                                            Case 1
                                                'Nasabah
                                                FillOrNothing(.FK_IFTI_BeneficialOwnerType_ID, 1, False, oInt)

                                                FillOrNothing(.BeneficialOwner_NoRekening_Old, objIfti.BeneficialOwner_NoRekening)
                                                FillOrNothing(.BeneficialOwner_NoRekening, Me.BenfOwnerNasabah_rekening.Text, False, Ovarchar)
                                                FillOrNothing(.BeneficialOwner_NamaLengkap_Old, objIfti.BeneficialOwner_NamaLengkap)
                                                FillOrNothing(.BeneficialOwner_NamaLengkap, Me.BenfOwnerNasabah_Nama.Text, False, Ovarchar)
                                                FillOrNothing(.BeneficialOwner_TanggalLahir_Old, objIfti.BeneficialOwner_TanggalLahir)
                                                FillOrNothing(.BeneficialOwner_TanggalLahir, Me.BenfOwnerNasabah_tanggalLahir, False, oDate)
                                                FillOrNothing(.BeneficialOwner_ID_Alamat_Old, objIfti.BeneficialOwner_ID_Alamat)
                                                FillOrNothing(.BeneficialOwner_ID_Alamat, Me.BenfOwnerNasabah_AlamatIden.Text, False, Ovarchar)
                                                FillOrNothing(.BeneficialOwner_ID_KotaKab, Me.hfBenfOwnerNasabah_KotaIden.Value, False, oInt)
                                                FillOrNothing(.BeneficialOwner_ID_KotaKab_Old, objIfti.BeneficialOwner_ID_KotaKab)
                                                FillOrNothing(.BeneficialOwner_ID_KotaKabLainnya_Old, objIfti.BeneficialOwner_ID_KotaKabLainnya)
                                                If Me.hfBenfOwnerNasabah_KotaIden.Value = ParameterKota.MsSystemParameter_Value Then
                                                    FillOrNothing(.BeneficialOwner_ID_KotaKabLainnya, Me.BenfOwnerNasabah_kotaLainIden.Text, False, Ovarchar)
                                                Else
                                                    FillOrNothing(.BeneficialOwner_ID_KotaKabLainnya, Nothing)
                                                End If

                                                FillOrNothing(.BeneficialOwner_ID_Propinsi_Old, objIfti.BeneficialOwner_ID_Propinsi)
                                                FillOrNothing(.BeneficialOwner_ID_Propinsi, Me.hfBenfOwnerNasabah_ProvinsiIden.Value, False, oInt)
                                                FillOrNothing(.BeneficialOwner_ID_PropinsiLainnya_Old, objIfti.BeneficialOwner_ID_PropinsiLainnya)
                                                If ObjectAntiNull(Me.BenfOwnerNasabah_ProvinsiIden.Text) = True Then
                                                    FillOrNothing(.BeneficialOwner_ID_PropinsiLainnya, Me.BenfOwnerNasabah_ProvinsiLainIden.Text, False, Ovarchar)
                                                Else
                                                    FillOrNothing(.BeneficialOwner_ID_PropinsiLainnya, Nothing)
                                                End If
                                                FillOrNothing(.BeneficialOwner_FK_IFTI_IDType_Old, objIfti.BeneficialOwner_FK_IFTI_IDType)
                                                FillOrNothing(.BeneficialOwner_FK_IFTI_IDType, Me.CBOBenfOwnerNasabah_JenisDokumen.SelectedValue, False, oInt)
                                                FillOrNothing(.BeneficialOwner_NomorID_Old, objIfti.BeneficialOwner_NomorID)
                                                FillOrNothing(.BeneficialOwner_NomorID, Me.BenfOwnerNasabah_NomorIden.Text, False, Ovarchar)

                                            Case 2
                                                'NonNasabah
                                                FillOrNothing(.FK_IFTI_BeneficialOwnerType_ID, 2, False, oInt)

                                                FillOrNothing(.BeneficialOwner_NamaLengkap_Old, objIfti.BeneficialOwner_NamaLengkap)
                                                FillOrNothing(.BeneficialOwner_NamaLengkap, Me.BenfOwnerNonNasabah_Nama.Text, False, Ovarchar)
                                                FillOrNothing(.BeneficialOwner_TanggalLahir_Old, objIfti.BeneficialOwner_TanggalLahir)
                                                FillOrNothing(.BeneficialOwner_TanggalLahir, Me.BenfOwnerNonNasabah_TanggalLahir, False, oDate)
                                                FillOrNothing(.BeneficialOwner_ID_Alamat_Old, objIfti.BeneficialOwner_ID_Alamat)
                                                FillOrNothing(.BeneficialOwner_ID_Alamat, Me.BenfOwnerNonNasabah_AlamatIden.Text, False, Ovarchar)
                                                FillOrNothing(.BeneficialOwner_ID_KotaKab_Old, objIfti.BeneficialOwner_ID_KotaKab)
                                                FillOrNothing(.BeneficialOwner_ID_KotaKabLainnya_Old, objIfti.BeneficialOwner_ID_KotaKabLainnya)
                                                FillOrNothing(.BeneficialOwner_ID_KotaKab, Me.hfBenfOwnerNonNasabah_KotaIden.Value, False, oInt)
                                                If Me.hfBenfOwnerNonNasabah_KotaIden.Value = ParameterKota.MsSystemParameter_Value Then
                                                    FillOrNothing(.BeneficialOwner_ID_KotaKabLainnya, Me.BenfOwnerNonNasabah_KotaLainIden.Text, False, Ovarchar)
                                                Else
                                                    FillOrNothing(.BeneficialOwner_ID_KotaKabLainnya, Nothing)
                                                End If

                                                FillOrNothing(.BeneficialOwner_ID_Propinsi_Old, objIfti.BeneficialOwner_ID_Propinsi)
                                                FillOrNothing(.BeneficialOwner_ID_PropinsiLainnya_Old, objIfti.BeneficialOwner_ID_PropinsiLainnya)
                                                FillOrNothing(.BeneficialOwner_ID_Propinsi, Me.hfBenfOwnerNonNasabah_ProvinsiIden.Value, False, oInt)
                                                If ObjectAntiNull(BenfOwnerNonNasabah_ProvinsiIden.Text) = True Then
                                                    FillOrNothing(.BeneficialOwner_ID_PropinsiLainnya, Me.BenfOwnerNonNasabah_ProvinsiLainIden.Text, False, Ovarchar)
                                                Else
                                                    FillOrNothing(.BeneficialOwner_ID_PropinsiLainnya, Nothing)
                                                End If


                                                FillOrNothing(.BeneficialOwner_FK_IFTI_IDType_Old, objIfti.BeneficialOwner_FK_IFTI_IDType)
                                                FillOrNothing(.BeneficialOwner_FK_IFTI_IDType, Me.CboPengirimNonNasabah_JenisDokumen.SelectedValue, False, oInt)
                                                FillOrNothing(.BeneficialOwner_NomorID_Old, objIfti.BeneficialOwner_NomorID)
                                                FillOrNothing(.BeneficialOwner_NomorID, Me.BenfOwnerNonNasabah_NomorIdentitas.Text, False, Ovarchar)
                                        End Select

                                    End If

                                    'cekPenerima
                                    'Using objReceiver As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("FK_IFTI_ID=" & getIFTIPK, "", 0, Integer.MaxValue, 0)
                                    '    Dim KeyBeneficiary As Integer = objReceiver(0).PK_IFTI_Beneficiary_ID
                                    '    Dim TipePenerima As Integer = objReceiver(0).FK_IFTI_NasabahType_ID
                                    '    Using objReceiverAppDetail As New IFTI_Approval_Beneficiary '= DataRepository.IFTI_Approval_BeneficiaryProvider.GetPaged("FK_IFTI_Approval_Id=" & KeyHeaderApproval, "", 0, Integer.MaxValue, 0)
                                    '        With objReceiverAppDetail
                                    '            Select Case (TipePenerima)
                                    '                Case 1
                                    '                    validasiReceiverInd()
                                    '                    .FK_IFTI_NasabahType_ID = 1
                                    '                    .FK_IFTI_ID = objIfti.PK_IFTI_ID
                                    '                    .FK_IFTI_Approval_Id = KeyHeaderApproval
                                    '                    .FK_IFTI_Beneficiary_ID = KeyBeneficiary

                                    '                    FillOrNothing(.Beneficiary_Nasabah_INDV_NoRekening, Me.txtPenerima_rekening.Text)

                                    '                    FillOrNothing(.Beneficiary_Nasabah_INDV_NamaLengkap, Me.txtPenerimaNasabah_IND_nama.Text, True, Ovarchar)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_INDV_TanggalLahir, Me.txtPenerimaNasabah_IND_TanggalLahir.Text, True, oDate)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_INDV_KewargaNegaraan, Me.RbPenerimaNasabah_IND_Warganegara.SelectedValue, True, oInt)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_INDV_Negara, Me.hfPenerimaNasabah_IND_negara.Value, True, oInt)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_INDV_NegaraLainnya, Me.txtPenerimaNasabah_IND_negaraLain.Text, True, Ovarchar)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Alamat_Iden, Me.txtPenerimaNasabah_IND_alamatIden.Text, True, Ovarchar)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_NegaraBagian, Me.txtPenerimaNasabah_IND_negaraBagian.Text, True, Ovarchar)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Negara, Me.hfPenerimaNasabah_IND_negaraIden.Value, True, oInt)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_NegaraLainnya, Me.txtPenerimaNasabah_IND_negaraLainIden.Text, True, Ovarchar)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_INDV_FK_IFTI_IDType, Me.CBOPenerimaNasabah_IND_JenisIden.SelectedValue, True, oInt)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_INDV_NomorID, Me.txtPenerimaNasabah_IND_NomorIden.Text, True, Ovarchar)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan, Me.txtPenerimaNasabah_IND_NilaiTransaksiKeuangan.Text, True, oDecimal)
                                    '                Case 2
                                    '                    validasiReceiverKorp()
                                    '                    .FK_IFTI_NasabahType_ID = 2
                                    '                    .FK_IFTI_ID = objIfti.PK_IFTI_ID
                                    '                    .FK_IFTI_Approval_Id = KeyHeaderApproval
                                    '                    .FK_IFTI_Beneficiary_ID = KeyBeneficiary
                                    '                    FillOrNothing(.Beneficiary_Nasabah_CORP_NoRekening, Me.txtPenerima_rekening.Text)

                                    '                    FillOrNothing(.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id, CBOPenerimaNasabah_Korp_BentukBadanUsahaLain.SelectedValue, True, oInt)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya, Me.txtPenerimaNasabah_Korp_BentukBadanUsahaLainnya.Text, True, Ovarchar)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_CORP_NamaKorporasi, Me.txtPenerimaNasabah_Korp_namaKorp.Text, True, Ovarchar)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id, Me.hfPenerimaNasabah_Korp_BidangUsahaKorp.Value, True, oInt)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_CORP_BidangUsahaLainnya, Me.txtPenerimaNasabah_Korp_BentukBadanUsahaLainnya.Text, True, Ovarchar)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_CORP_AlamatLengkap, Me.txtPenerimaNasabah_Korp_AlamatKorp.Text, TipePenerima, Ovarchar)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_CORP_ID_NegaraBagian, Me.txtPenerimaNasabah_Korp_negaraKota.Text, True, oInt)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_CORP_ID_Negara, Me.hfPenerimaNasabah_Korp_NegaraKorp.Value, True, oInt)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_CORP_ID_NegaraLainnya, Me.txtPenerimaNasabah_Korp_NegaraLainnyaKorp.Text, True, Ovarchar)
                                    '                    FillOrNothing(.Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan, Me.txtPenerimaNasabah_Korp_NilaiTransaksiKeuangan.Text, True, oDecimal)
                                    '                Case 3
                                    '                    validasiReceiverNonNasabah()
                                    '                    .FK_IFTI_NasabahType_ID = 3
                                    '                    .FK_IFTI_ID = objIfti.PK_IFTI_ID
                                    '                    .FK_IFTI_Approval_Id = KeyHeaderApproval
                                    '                    .FK_IFTI_Beneficiary_ID = KeyBeneficiary
                                    '                    FillOrNothing(.Beneficiary_NonNasabah_NoRekening, Me.txtPenerima_rekening.Text)

                                    '                    FillOrNothing(.Beneficiary_NonNasabah_NamaBank, Me.txtPenerimaNonNasabah_namabank.Text, True, Ovarchar)
                                    '                    FillOrNothing(.Beneficiary_NonNasabah_NamaLengkap, Me.txtPenerimaNonNasabah_Nama.Text, True, Ovarchar)
                                    '                    FillOrNothing(.Beneficiary_NonNasabah_ID_Alamat, Me.txtPenerimaNonNasabah_Alamat.Text, True, Ovarchar)
                                    '                    FillOrNothing(.Beneficiary_NonNasabah_ID_Negara, Me.hfPenerimaNonNasabah_Negara.Value, True, oInt)
                                    '                    FillOrNothing(.Beneficiary_NonNasabah_ID_NegaraLainnya, Me.txtPenerimaNonNasabah_negaraLain.Text, True, Ovarchar)
                                    '                    FillOrNothing(.Beneficiary_NonNasabah_NilaiTransaksikeuangan, Me.txtPenerimaNonNasabah_nilaiTransaksiKeuangan.Text, True, oDecimal)
                                    '            End Select
                                    '        End With
                                    '        'save beneficiary
                                    '        DataRepository.IFTI_Approval_BeneficiaryProvider.Save(OTrans, objReceiverAppDetail)
                                    '    End Using
                                    'End Using
                                    'Using objReceiver As TList(Of IFTI_Approval_Beneficiary) = DataRepository.IFTI_Approval_BeneficiaryProvider.GetPaged("FK_IFTI_ID=" & getIFTIPK, "", 0, Integer.MaxValue, 0)
                                    'penerima

                                    Dim result As ArrayList
                                    Dim i As Integer
                                    Dim dt As Data.DataTable = ReceiverDataTable
                                    Dim rowCount As Integer = dt.Rows.Count

                                    If (rowCount > 0) Then
                                        result = New ArrayList
                                    End If

                                    For i = 0 To rowCount - 1
                                        Dim objReceiverAppDetail As New IFTI_Approval_Beneficiary
                                        Dim KeyBeneficiary As Integer
                                        If ObjectAntiNull(dt.Rows(i)("PK_IFTI_Beneficiary_ID")) Then
                                            KeyBeneficiary = dt.Rows(i)("PK_IFTI_Beneficiary_ID")
                                        End If
                                        '= dt.Rows(i)("PK_IFTI_Beneficiary_ID")

                                        Dim TipePenerima As Integer = dt.Rows(i)("FK_IFTI_NasabahType_ID")
                                        With objReceiverAppDetail
                                            '.ChronologyDate = CDate(dt.Rows(i)("ChronologyDate").ToString)
                                            '.Description = dt.Rows(i)("Description").ToString
                                            '.ReportedBy = dt.Rows(i)("ReportedBy").ToString()
                                            '.ReportedWorkUnit = dt.Rows(i)("ReportedWorkUnit").ToString()
                                            '.OwnershipIssue = txtChronologyOwnershipIssue.Text
                                            '.OwnershipIssueUnit = txtChronologyOwnershipIssueUnit.Text

                                            Select Case (TipePenerima)
                                                Case 1
                                                    'validasiReceiverInd()
                                                    .FK_IFTI_NasabahType_ID = 1
                                                    .FK_IFTI_ID = objIfti.PK_IFTI_ID
                                                    .FK_IFTI_Approval_Id = KeyHeaderApproval
                                                    If ObjectAntiNull(dt.Rows(i)("PK_IFTI_Beneficiary_ID")) Then
                                                        .FK_IFTI_Beneficiary_ID = KeyBeneficiary
                                                    End If

                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_NoRekening, dt.Rows(i)("Beneficiary_Nasabah_INDV_NoRekening").ToString)

                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_NamaLengkap, dt.Rows(i)("Beneficiary_Nasabah_INDV_NamaLengkap").ToString, False, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_TanggalLahir, dt.Rows(i)("Beneficiary_Nasabah_INDV_TanggalLahir").ToString, False, oDate)
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_KewargaNegaraan, dt.Rows(i)("Beneficiary_Nasabah_INDV_KewargaNegaraan"), False, oInt)
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_Negara, dt.Rows(i)("Beneficiary_Nasabah_INDV_Negara"), False, oInt)
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_NegaraLainnya, dt.Rows(i)("Beneficiary_Nasabah_INDV_NegaraLainnya").ToString, False, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Alamat_Iden, dt.Rows(i)("Beneficiary_Nasabah_INDV_ID_Alamat_Iden").ToString, False, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_NegaraBagian, dt.Rows(i)("Beneficiary_Nasabah_INDV_ID_NegaraBagian"), False, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Negara, dt.Rows(i)("Beneficiary_Nasabah_INDV_ID_Negara"), False, oInt)
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_NegaraLainnya, dt.Rows(i)("Beneficiary_Nasabah_INDV_ID_NegaraLainnya"), False, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_FK_IFTI_IDType, dt.Rows(i)("Beneficiary_Nasabah_INDV_FK_IFTI_IDType"), False, oInt)
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_NomorID, dt.Rows(i)("Beneficiary_Nasabah_INDV_NomorID"), False, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan, dt.Rows(i)("Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan"), False, oDecimal)
                                                Case 2
                                                    'validasiReceiverKorp()
                                                    .FK_IFTI_NasabahType_ID = 2
                                                    .FK_IFTI_ID = objIfti.PK_IFTI_ID
                                                    .FK_IFTI_Approval_Id = KeyHeaderApproval

                                                    If ObjectAntiNull(dt.Rows(i)("PK_IFTI_Beneficiary_ID")) Then
                                                        .FK_IFTI_Beneficiary_ID = KeyBeneficiary
                                                    End If
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_NoRekening, Me.txtPenerima_rekening.Text)

                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id, dt.Rows(i)("Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id"), False, oInt)
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya, dt.Rows(i)("Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya"), False, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_NamaKorporasi, dt.Rows(i)("Beneficiary_Nasabah_CORP_NamaKorporasi"), False, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id, dt.Rows(i)("Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id"), False, oInt)
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_BidangUsahaLainnya, dt.Rows(i)("Beneficiary_Nasabah_CORP_BidangUsahaLainnya"), False, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_AlamatLengkap, dt.Rows(i)("Beneficiary_Nasabah_CORP_AlamatLengkap"), TipePenerima, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_ID_NegaraBagian, dt.Rows(i)("Beneficiary_Nasabah_CORP_ID_NegaraBagian"), False, oInt)
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_ID_Negara, dt.Rows(i)("Beneficiary_Nasabah_CORP_ID_Negara"), False, oInt)
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_ID_NegaraLainnya, dt.Rows(i)("Beneficiary_Nasabah_CORP_ID_NegaraLainnya"), False, Ovarchar)
                                                    FillOrNothing(.Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan, dt.Rows(i)("Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan"), False, oDecimal)
                                                Case 3
                                                    'validasiReceiverNonNasabah()
                                                    .FK_IFTI_NasabahType_ID = 3
                                                    .FK_IFTI_ID = objIfti.PK_IFTI_ID
                                                    .FK_IFTI_Approval_Id = KeyHeaderApproval

                                                    If ObjectAntiNull(dt.Rows(i)("PK_IFTI_Beneficiary_ID")) Then
                                                        .FK_IFTI_Beneficiary_ID = KeyBeneficiary
                                                    End If
                                                    FillOrNothing(.Beneficiary_NonNasabah_NoRekening, dt.Rows(i)("Beneficiary_NonNasabah_NoRekening"))

                                                    FillOrNothing(.Beneficiary_NonNasabah_NamaBank, dt.Rows(i)("Beneficiary_NonNasabah_NamaBank"), False, Ovarchar)
                                                    FillOrNothing(.Beneficiary_NonNasabah_NamaLengkap, dt.Rows(i)("Beneficiary_NonNasabah_NamaLengkap"), False, Ovarchar)
                                                    FillOrNothing(.Beneficiary_NonNasabah_ID_Alamat, dt.Rows(i)("Beneficiary_NonNasabah_ID_Alamat"), False, Ovarchar)
                                                    FillOrNothing(.Beneficiary_NonNasabah_ID_Negara, dt.Rows(i)("Beneficiary_NonNasabah_ID_Negara"), False, oInt)
                                                    FillOrNothing(.Beneficiary_NonNasabah_ID_NegaraLainnya, dt.Rows(i)("Beneficiary_NonNasabah_ID_NegaraLainnya"), False, Ovarchar)
                                                    FillOrNothing(.Beneficiary_NonNasabah_NilaiTransaksikeuangan, dt.Rows(i)("Beneficiary_NonNasabah_NilaiTransaksikeuangan"), False, oDecimal)
                                            End Select
                                        End With
                                        'result.Add(objReceiverAppDetail)
                                        'save beneficiary
                                        DataRepository.IFTI_Approval_BeneficiaryProvider.Save(OTrans, objReceiverAppDetail)
                                    Next


                                    'End Using

                                    'cekTransaksi
                                    'validasiTransaksi()
                                    '----old----
                                    FillOrNothing(.TanggalTransaksi_Old, objIfti.TanggalTransaksi)
                                    FillOrNothing(.TimeIndication_Old, objIfti.TimeIndication)
                                    FillOrNothing(.SenderReference_Old, objIfti.SenderReference)
                                    FillOrNothing(.BankOperationCode_Old, objIfti.BankOperationCode)
                                    FillOrNothing(.InstructionCode_Old, objIfti.InstructionCode)
                                    FillOrNothing(.KantorCabangPenyelengaraPengirimAsal_Old, objIfti.KantorCabangPenyelengaraPengirimAsal)
                                    FillOrNothing(.TransactionCode_Old, objIfti.TransactionCode)
                                    FillOrNothing(.ValueDate_TanggalTransaksi_Old, objIfti.ValueDate_TanggalTransaksi)
                                    FillOrNothing(.ValueDate_NilaiTransaksi_Old, objIfti.ValueDate_NilaiTransaksi)
                                    FillOrNothing(.ValueDate_FK_Currency_ID_Old, objIfti.ValueDate_FK_Currency_ID)
                                    FillOrNothing(.ValueDate_CurrencyLainnya_Old, objIfti.ValueDate_CurrencyLainnya)
                                    FillOrNothing(.ValueDate_NilaiTransaksiIDR_Old, objIfti.ValueDate_NilaiTransaksiIDR)
                                    FillOrNothing(.Instructed_Currency_Old, objIfti.Instructed_Currency)
                                    FillOrNothing(.Instructed_CurrencyLainnya_Old, objIfti.Instructed_CurrencyLainnya)
                                    FillOrNothing(.Instructed_Amount_Old, objIfti.Instructed_Amount)
                                    FillOrNothing(.ExchangeRate_Old, objIfti.ExchangeRate)
                                    FillOrNothing(.SendingInstitution_Old, objIfti.SendingInstitution)
                                    FillOrNothing(.TujuanTransaksi_Old, objIfti.TujuanTransaksi)
                                    FillOrNothing(.SumberPenggunaanDana_Old, objIfti.SumberPenggunaanDana)

                                    '-----new---
                                    FillOrNothing(.TanggalTransaksi, Me.Transaksi_tanggal.Text, False, oDate)
                                    FillOrNothing(.TimeIndication, Me.Transaksi_waktutransaksi.Text, False, oDate)
                                    FillOrNothing(.SenderReference, Me.Transaksi_Sender.Text, False, Ovarchar)
                                    FillOrNothing(.BankOperationCode, Me.Transaksi_BankOperationCode.Text, False, Ovarchar)
                                    FillOrNothing(.InstructionCode, Me.Transaksi_InstructionCode.Text, False, Ovarchar)
                                    FillOrNothing(.KantorCabangPenyelengaraPengirimAsal, Me.Transaksi_kantorCabangPengirim.Text, False, Ovarchar)
                                    FillOrNothing(.TransactionCode, Me.Transaksi_kodeTipeTransaksi.Text, False, Ovarchar)
                                    FillOrNothing(.ValueDate_TanggalTransaksi, Me.Transaksi_ValueTanggalTransaksi.Text, False, oDate)
                                    FillOrNothing(.ValueDate_NilaiTransaksi, Me.Transaksi_nilaitransaksi.Text, False, oDecimal)
                                    If Me.hfTransaksi_MataUangTransaksi.Value <> "" Then
                                        Using objCurrency As TList(Of MsCurrency) = DataRepository.MsCurrencyProvider.GetPaged(MsCurrencyColumn.Code.ToString & _
                                                                                                               " like '%" & Me.Transaksi_MataUangTransaksi.Text & "%'", "", 0, Integer.MaxValue, 0)
                                            If objCurrency.Count > 0 Then
                                                FillOrNothing(.ValueDate_FK_Currency_ID, objCurrency(0).IdCurrency, False, oInt)
                                            End If

                                        End Using
                                    End If

                                    'FillOrNothing(.ValueDate_FK_Currency_ID, Me.hfTransaksi_MataUangTransaksi.Value, false,oInt)
                                    FillOrNothing(.ValueDate_CurrencyLainnya, Me.Transaksi_MataUangTransaksiLain.Text, False, Ovarchar)
                                    FillOrNothing(.ValueDate_NilaiTransaksiIDR, Me.Transaksi_AmountdalamRupiah.Text, False, oDecimal)
                                    If Me.hfTransaksi_currency.Value <> "" Then
                                        Using objCurrency2 As TList(Of MsCurrency) = DataRepository.MsCurrencyProvider.GetPaged(MsCurrencyColumn.Code.ToString & _
                                                                                                                                      " like '%" & Me.Transaksi_currency.Text & "%'", "", 0, Integer.MaxValue, 0)
                                            If objCurrency2.Count > 0 Then
                                                FillOrNothing(.Instructed_Currency, objCurrency2(0).IdCurrency, False, oInt)
                                            End If

                                        End Using
                                    End If

                                    'FillOrNothing(.Instructed_Currency, Me.Transaksi_currency.Text, false,oInt)
                                    FillOrNothing(.Instructed_CurrencyLainnya, Me.Transaksi_currencyLain.Text, False, oInt)
                                    FillOrNothing(.Instructed_Amount, Me.Transaksi_instructedAmount.Text, False, oDecimal)
                                    FillOrNothing(.ExchangeRate, Me.Transaksi_nilaiTukar.Text, False, oDecimal)
                                    FillOrNothing(.SendingInstitution, Me.Transaksi_sendingInstitution.Text, False, Ovarchar)
                                    FillOrNothing(.TujuanTransaksi, Me.Transaksi_TujuanTransaksi.Text, False, Ovarchar)
                                    FillOrNothing(.SumberPenggunaanDana, Me.Transaksi_SumberPenggunaanDana.Text, False, Ovarchar)

                                    'cekInformasiLain
                                    '----old---
                                    FillOrNothing(.InformationAbout_SenderCorrespondent_Old, objIfti.InformationAbout_SenderCorrespondent)
                                    FillOrNothing(.InformationAbout_ReceiverCorrespondent_Old, objIfti.InformationAbout_ReceiverCorrespondent)
                                    FillOrNothing(.InformationAbout_Thirdreimbursementinstitution_Old, objIfti.InformationAbout_Thirdreimbursementinstitution)
                                    FillOrNothing(.InformationAbout_IntermediaryInstitution_Old, objIfti.InformationAbout_IntermediaryInstitution)
                                    FillOrNothing(.RemittanceInformation_Old, objIfti.RemittanceInformation)
                                    FillOrNothing(.SendertoReceiverInformation_Old, objIfti.SendertoReceiverInformation)
                                    FillOrNothing(.RegulatoryReporting_Old, objIfti.RegulatoryReporting)
                                    FillOrNothing(.EnvelopeContents_Old, objIfti.EnvelopeContents)

                                    '---new-----
                                    FillOrNothing(.InformationAbout_SenderCorrespondent, Me.InformasiLainnya_Sender.Text, False, Ovarchar)
                                    FillOrNothing(.InformationAbout_ReceiverCorrespondent, Me.InformasiLainnya_receiver.Text, False, Ovarchar)
                                    FillOrNothing(.InformationAbout_Thirdreimbursementinstitution, Me.InformasiLainnya_thirdReimbursement.Text, False, Ovarchar)
                                    FillOrNothing(.InformationAbout_IntermediaryInstitution, Me.InformasiLainnya_intermediary.Text, False, Ovarchar)
                                    FillOrNothing(.RemittanceInformation, Me.InformasiLainnya_Remittance.Text, False, Ovarchar)
                                    FillOrNothing(.SendertoReceiverInformation, Me.InformasiLainnya_SenderToReceiver.Text, False, Ovarchar)
                                    FillOrNothing(.RegulatoryReporting, Me.InformasiLainnya_RegulatoryReport.Text, False, Ovarchar)
                                    FillOrNothing(.EnvelopeContents, Me.InformasiLainnya_EnvelopeContents.Text, False, Ovarchar)

                                End With

                                DataRepository.IFTI_Approval_DetailProvider.Save(OTrans, objIfti_ApprovalDetail)
                            End Using
                            ''Send Email

                            ImageButtonSave.Visible = False
                            ImageButtonCancel.Visible = False
                            lblMsg.Text = "Data has been edited and waiting for approval"
                            MultiViewNasabahTypeAll.ActiveViewIndex = 1
                        End Using
                    End If
                    OTrans.Commit()
                Catch ex As Exception
                    OTrans.Rollback()
                    Throw
                End Try

            End Using

        Catch ex As Exception
            'LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub
    'Sub SaveSwiftInco()
    '    Try
    '        Page.Validate("handle")
    '        If Page.IsValid Then
    '            'ValidasiControl()

    '            ' =========== Insert Header Approval
    '            Dim KeyHeaderApproval As Integer
    '            Using objIftiApproval As New IFTI_Approval
    '                With objIftiApproval
    '                    FillOrNothing(.FK_MsMode_Id, 2, True, oInt)
    '                    FillOrNothing(.RequestedBy, SessionPkUserId)
    '                    FillOrNothing(.RequestedDate, Date.Now)
    '                    'FillOrNothing(.IsUpload, False)
    '                End With
    '                DataRepository.IFTI_ApprovalProvider.Save(objIftiApproval)
    '                KeyHeaderApproval = objIftiApproval.PK_IFTI_Approval_Id
    '            End Using

    '            '============ Insert Detail Approval
    '            Using objIfti_ApprovalDetail As New IFTI_Approval_Detail()
    '                Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)

    '                    Dim senderType As Integer = objIfti.Sender_FK_IFTI_NasabahType_ID
    '                    With objIfti_ApprovalDetail
    '                        'FK
    '                        .PK_IFTI_ID_Old = objIfti.PK_IFTI_ID
    '                        .PK_IFTI_ID = objIfti.PK_IFTI_ID
    '                        .FK_IFTI_Approval_Id = KeyHeaderApproval
    '                        .FK_IFTI_Type_ID_Old = objIfti.FK_IFTI_Type_ID
    '                        .FK_IFTI_Type_ID = 2
    '                        'umum
    '                        '--old--
    '                        FillOrNothing(.LTDLNNo_Old, objIfti.LTDLNNo, True, oInt)
    '                        FillOrNothing(.LTDLNNoKoreksi_Old, objIfti.LTDLNNoKoreksi, True, oInt)
    '                        FillOrNothing(.TanggalLaporan_Old, objIfti.TanggalLaporan, True, oDate)
    '                        FillOrNothing(.NamaPJKBankPelapor_Old, objIfti.NamaPJKBankPelapor, True, Ovarchar)
    '                        FillOrNothing(.NamaPejabatPJKBankPelapor_Old, objIfti.NamaPejabatPJKBankPelapor, True, Ovarchar)
    '                        FillOrNothing(.JenisLaporan_Old, objIfti.JenisLaporan, True, oInt)
    '                        '--new--
    '                        FillOrNothing(.LTDLNNo, Me.SwiftInUmum_LTDN.text, True, oInt)
    '                        FillOrNothing(.LTDLNNoKoreksi, Me.SwiftInUmum_LtdnKoreksi.Text, True, oInt)
    '                        FillOrNothing(.TanggalLaporan, Me.SwiftInUmum_TanggalLaporan.Text, True, oDate)
    '                        FillOrNothing(.NamaPJKBankPelapor, Me.SwiftInUmum_NamaPJKBank.Text, True, Ovarchar)
    '                        FillOrNothing(.NamaPejabatPJKBankPelapor, Me.SwiftInUmum_NamaPejabatPJKBank.Text, True, Ovarchar)
    '                        FillOrNothing(.JenisLaporan, Me.RbSwiftInUmum_JenisLaporan.SelectedValue, True, oInt)
    '                        'cekPengirim
    '                        Select Case (senderType)
    '                            Case 1
    '                                'pengirimNasabahIndividu
    '                                'cekvalidasi
    '                                FillOrNothing(.Sender_FK_IFTI_NasabahType_ID_Old, objIfti.Sender_FK_IFTI_NasabahType_ID)
    '                                FillOrNothing(.Sender_FK_IFTI_NasabahType_ID, senderType, True, Ovarchar)

    '                                FillOrNothing(.Sender_Nasabah_INDV_NoRekening_Old, objIfti.Sender_Nasabah_INDV_NoRekening)
    '                                FillOrNothing(.Sender_Nasabah_INDV_NoRekening, Me.txtSwiftInPengirim_rekening.Text, True, Ovarchar)

    '                                FillOrNothing(.Sender_Nasabah_INDV_NamaLengkap_Old, objIfti.Sender_Nasabah_INDV_NamaLengkap, )
    '                                FillOrNothing(.Sender_Nasabah_INDV_NamaLengkap, Me.txtSwiftInPengirimNasabah_IND_nama.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_Nasabah_INDV_TanggalLahir_Old, objIfti.Sender_Nasabah_INDV_TanggalLahir)
    '                                FillOrNothing(.Sender_Nasabah_INDV_TanggalLahir, Me.txtSwiftInPengirimNasabah_IND_TanggalLahir.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_Nasabah_INDV_KewargaNegaraan_Old, objIfti.Sender_Nasabah_INDV_KewargaNegaraan)
    '                                FillOrNothing(.Sender_Nasabah_INDV_KewargaNegaraan, Me.RbSwiftInPengirimNasabah_IND_Warganegara.SelectedValue, True, oInt)
    '                                FillOrNothing(.Sender_Nasabah_INDV_Negara_Old, objIfti.Sender_Nasabah_INDV_Negara)
    '                                FillOrNothing(.Sender_Nasabah_INDV_NegaraLainnya_Old, objIfti.Sender_Nasabah_INDV_NegaraLainnya)
    '                                If Me.RbPenerimaNasabah_IND_Warganegara.SelectedValue = "2" Then
    '                                    FillOrNothing(.Sender_Nasabah_INDV_Negara, Me.hfSwiftInPengirimNasabah_IND_negara.Value, True, oInt)
    '                                    FillOrNothing(.Sender_Nasabah_INDV_NegaraLainnya, Me.txtSwiftInPengirimNasabah_IND_negaraLainIden.Text, True, Ovarchar)
    '                                End If
    '                                FillOrNothing(.Sender_Nasabah_INDV_ID_Alamat_Old, objIfti.Sender_Nasabah_INDV_ID_Alamat)
    '                                FillOrNothing(.Sender_Nasabah_INDV_ID_Alamat, Me.txtSwiftInPengirimNasabah_IND_alamatIden.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_Nasabah_INDV_ID_NegaraKota_Old, objIfti.Sender_Nasabah_INDV_ID_NegaraKota)
    '                                FillOrNothing(.Sender_Nasabah_INDV_ID_NegaraKota, Me.hfSwiftInPengirimNasabah_IND_negaraBagian.Value, True, oInt)
    '                                FillOrNothing(.Sender_Nasabah_INDV_ID_Negara_Old, objIfti.Sender_Nasabah_INDV_ID_Negara)
    '                                FillOrNothing(.Sender_Nasabah_INDV_ID_Negara, Me.hfSwiftInPengirimNasabah_IND_negara.Value, True, oInt)
    '                                FillOrNothing(.Sender_Nasabah_INDV_NegaraLainnya_Old, objIfti.Sender_Nasabah_INDV_NegaraLainnya)
    '                                FillOrNothing(.Sender_Nasabah_INDV_NegaraLainnya, Me.txtSwiftInPengirimNasabah_IND_negaraLainIden.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_Nasabah_INDV_NoTelp_Old, objIfti.Sender_Nasabah_INDV_NoTelp)
    '                                FillOrNothing(.Sender_Nasabah_INDV_NoTelp, Me.txtSwiftInPengirimNasabah_IND_NoTelp.Text, True, Ovarchar)
    '                               FillOrNothing(.)
    '                            Case 2
    '                                'pengirimNasabahKorporasi
    '                                'cekvalidasi
    '                                FillOrNothing(.Sender_FK_IFTI_NasabahType_ID_Old, objIfti.Sender_FK_IFTI_NasabahType_ID)
    '                                FillOrNothing(.Sender_FK_IFTI_NasabahType_ID, senderType, True, Ovarchar)

    '                                FillOrNothing(.Sender_Nasabah_CORP_NoRekening_Old, objIfti.Sender_Nasabah_CORP_NoRekening)
    '                                FillOrNothing(.Sender_Nasabah_CORP_NoRekening, Me.txtSwiftInPengirim_rekening.Text, True, Ovarchar)

    '                                FillOrNothing(.Sender_Nasabah_CORP_NamaKorporasi_Old, objIfti.Sender_Nasabah_CORP_NamaKorporasi)
    '                                FillOrNothing(.Sender_Nasabah_CORP_NamaKorporasi, Me.txtSwiftInPengirimNasabah_Korp_namaKorp.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_Nasabah_CORP_AlamatLengkap_Old, objIfti.Sender_Nasabah_CORP_AlamatLengkap)
    '                                FillOrNothing(.Sender_Nasabah_CORP_AlamatLengkap, Me.txtSwiftInPengirimNasabah_Korp_AlamatKorp.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_Nasabah_CORP_NegaraKota_Old, objIfti.Sender_Nasabah_CORP_NegaraKota)
    '                                FillOrNothing(.Sender_Nasabah_CORP_NegaraKota, Me.hfSwiftInPengirimNasabah_Korp_Kota.Value, True, Ovarchar)
    '                                FillOrNothing(.Sender_Nasabah_CORP_Negara_Old, objIfti.Sender_Nasabah_CORP_Negara)
    '                                FillOrNothing(.Sender_Nasabah_CORP_Negara, Me.txtSwiftInPengirimNasabah_Korp_NegaraKorp.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_Nasabah_CORP_NegaraLainnya_Old, objIfti.Sender_Nasabah_CORP_NegaraLainnya)
    '                                FillOrNothing(.Sender_Nasabah_CORP_NegaraLainnya, Me.txtSwiftInPengirimNasabah_Korp_NegaraLainnyaKorp.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_Nasabah_CORP_NoTelp_Old, objIfti.Sender_Nasabah_CORP_NoTelp)
    '                                FillOrNothing(.Sender_Nasabah_CORP_NoTelp, Me.txtSwiftInPengirimNasabah_Korp_NoTelp.Text, True, Ovarchar)

    '                            Case 3
    '                                'PengirimNonNasabah
    '                                'cekvalidasi
    '                                FillOrNothing(.Sender_FK_IFTI_NasabahType_ID_Old, objIfti.Sender_FK_IFTI_NasabahType_ID)
    '                                FillOrNothing(.Sender_FK_IFTI_NasabahType_ID, senderType, True, Ovarchar)

    '                                FillOrNothing(.Sender_NonNasabah_NoRekening_Old, objIfti.Sender_NonNasabah_NoRekening)
    '                                FillOrNothing(.Sender_NonNasabah_NoRekening, Me.txtSwiftInPengirimNonNasabah_rekening.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_NonNasabah_NamaLengkap_Old, objIfti.Sender_NonNasabah_NamaLengkap)
    '                                FillOrNothing(.Sender_NonNasabah_NamaLengkap, Me.txtSwiftInPengirimNonNasabah_Nama.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_NonNasabah_NamaBank_Old, objIfti.Sender_NonNasabah_NamaBank)
    '                                FillOrNothing(.Sender_NonNasabah_NamaBank, Me.txtSwiftInPengirimNonNasabah_namabank.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_NonNasabah_TanggalLahir_Old, objIfti.Sender_NonNasabah_TanggalLahir)
    '                                FillOrNothing(.Sender_NonNasabah_TanggalLahir, Me.txtSwiftInPengirimNonNasabah_TanggalLahir.Text, True, oDate)
    '                                FillOrNothing(.Sender_NonNasabah_ID_Alamat_Old, objIfti.Sender_NonNasabah_ID_Alamat)
    '                                FillOrNothing(.Sender_NonNasabah_ID_Alamat, Me.txtSwiftInPengirimNonNasabah_Alamat.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_NonNasabah_ID_NegaraBagian_Old, objIfti.Sender_NonNasabah_ID_NegaraBagian)
    '                                FillOrNothing(.Sender_NonNasabah_ID_NegaraBagian, Me.hfSwiftInPengirimNonNasabah_Kota.Value, True, Ovarchar)
    '                                FillOrNothing(.Sender_NonNasabah_ID_Negara_Old, objIfti.Sender_NonNasabah_ID_Negara)
    '                                FillOrNothing(.Sender_NonNasabah_ID_Negara, Me.txtSwiftInPengirimNonNasabah_Negara.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_NonNasabah_ID_NegaraLainnya_Old, objIfti.Sender_NonNasabah_ID_NegaraLainnya)
    '                                FillOrNothing(.Sender_NonNasabah_ID_NegaraLainnya, Me.txtSwiftInPengirimNonNasabah_negaraLain.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_NonNasabah_NoTelp_Old, objIfti.Sender_NonNasabah_NoTelp)
    '                                FillOrNothing(.Sender_NonNasabah_NoTelp, Me.txtSwiftInPengirimNonNasabah_NoTelp.Text, True, Ovarchar)

    '                        End Select


    '                        'cekPenerima
    '                        Using objReceiver As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("FK_IFTI_ID=" & getIFTIPK, "", 0, Integer.MaxValue, 0)
    '                            Dim TipePenerima As Integer = objReceiver(0).FK_IFTI_NasabahType_ID
    '                            Select Case (TipePenerima)
    '                                Case 1
    '                                    'FillOrNothing(.)
    '                                Case 2
    '                                Case 3
    '                                    'saveappDetailPenerima
    '                            End Select


    '                        End Using

    '                        'cekTransaksi
    '                        '----old----
    '                        FillOrNothing(.TanggalTransaksi_Old, objIfti.TanggalTransaksi)
    '                        FillOrNothing(.TimeIndication_Old, objIfti.TimeIndication)
    '                        FillOrNothing(.SenderReference_Old, objIfti.SenderReference)
    '                        FillOrNothing(.RelatedReference_Old, objIfti.RelatedReference)
    '                        FillOrNothing(.BankOperationCode_Old, objIfti.BankOperationCode)
    '                        FillOrNothing(.InstructionCode_Old, objIfti.InstructionCode)
    '                        FillOrNothing(.KantorCabangPenyelengaraPengirimAsal_Old, objIfti.KantorCabangPenyelengaraPengirimAsal)
    '                        FillOrNothing(.TransactionCode, objIfti.TransactionCode)
    '                        FillOrNothing(.ValueDate_TanggalTransaksi_Old, objIfti.ValueDate_TanggalTransaksi)
    '                        FillOrNothing(.ValueDate_NilaiTransaksi_Old, objIfti.ValueDate_NilaiTransaksi)
    '                        FillOrNothing(.ValueDate_FK_Currency_ID_Old, objIfti.ValueDate_FK_Currency_ID)
    '                        FillOrNothing(.ValueDate_NilaiTransaksiIDR_Old, objIfti.ValueDate_NilaiTransaksiIDR)
    '                        FillOrNothing(.Instructed_Currency_Old, objIfti.Instructed_Currency)
    '                        FillOrNothing(.Instructed_Amount_Old, objIfti.Instructed_Amount)
    '                        FillOrNothing(.ExchangeRate_Old, objIfti.ExchangeRate)
    '                        FillOrNothing(.SendingInstitution_Old, objIfti.SendingInstitution)
    '                        FillOrNothing(.BeneficiaryInstitution_Old, objIfti.BeneficiaryInstitution)
    '                        FillOrNothing(.TujuanTransaksi_Old, objIfti.BeneficiaryInstitution)
    '                        FillOrNothing(.SumberPenggunaanDana_Old, objIfti.SumberPenggunaanDana)

    '                        '-----new---
    '                        FillOrNothing(.TanggalTransaksi, Me.Transaksi_SwiftIntanggal0.Text, True, oDate)
    '                        FillOrNothing(.TimeIndication, Me.Transaksi_SwiftInwaktutransaksi0.Text, True, oDate)
    '                        FillOrNothing(.SenderReference, Me.Transaksi_SwiftInSender0.Text, True, Ovarchar)
    '                        FillOrNothing(.RelatedReference, Me.Transaksi_SwiftInrelated0.Text, True, Ovarchar)
    '                        FillOrNothing(.BankOperationCode, Me.Transaksi_SwiftInBankOperationCode0.Text, True, Ovarchar)
    '                        FillOrNothing(.InstructionCode, Me.Transaksi_SwiftInInstructionCode0.Text, True, Ovarchar)
    '                        FillOrNothing(.KantorCabangPenyelengaraPengirimAsal, Me.Transaksi_SwiftInkantorCabangPengirim0.Text, True, Ovarchar)
    '                        FillOrNothing(.TransactionCode, Me.Transaksi_SwiftInkodeTipeTransaksi0.Text, True, Ovarchar)
    '                        FillOrNothing(.ValueDate_TanggalTransaksi, Me.Transaksi_SwiftInValueTanggalTransaksi0.Text, True, oDate)
    '                        FillOrNothing(.ValueDate_NilaiTransaksi, Me.cboTransaksi_SwiftInnilaitransaksi0.SelectedValue, True, oDecimal)
    '                        FillOrNothing(.ValueDate_FK_Currency_ID_Old, Me.hfTransaksi_SwiftIncurrency.Value, True, oInt)
    '                        FillOrNothing(.ValueDate_NilaiTransaksiIDR, Me.Transaksi_SwiftInAmountdalamRupiah0.Text, True, oDecimal)
    '                        FillOrNothing(.Instructed_Currency, Me.Transaksi_SwiftIncurrency0.Text, True, oInt)
    '                        FillOrNothing(.Instructed_Amount, Me.Transaksi_SwiftIninstructedAmount0.Text, True, oDecimal)
    '                        FillOrNothing(.ExchangeRate, Me.Transaksi_SwiftInnilaiTukar0.Text, True, oDecimal)
    '                        FillOrNothing(.SendingInstitution, Me.Transaksi_SwiftInsendingInstitution0.Text, True, Ovarchar)
    '                        FillOrNothing(.BeneficiaryInstitution, Me.Transaksi_SwiftInBeneficiaryInstitution0.Text, True, Ovarchar)
    '                        FillOrNothing(.TujuanTransaksi, Me.Transaksi_SwiftInTujuanTransaksi0.Text, True, Ovarchar)
    '                        FillOrNothing(.SumberPenggunaanDana, Me.Transaksi_SwiftInSumberPenggunaanDana0.Text, True, Ovarchar)

    '                        'cekInformasiLain
    '                        '----old---
    '                        FillOrNothing(.InformationAbout_SenderCorrespondent_Old, objIfti.InformationAbout_SenderCorrespondent)
    '                        FillOrNothing(.InformationAbout_ReceiverCorrespondent_Old, objIfti.InformationAbout_ReceiverCorrespondent)
    '                        FillOrNothing(.InformationAbout_Thirdreimbursementinstitution_Old, objIfti.InformationAbout_Thirdreimbursementinstitution)
    '                        FillOrNothing(.InformationAbout_IntermediaryInstitution_Old, objIfti.InformationAbout_IntermediaryInstitution)
    '                        FillOrNothing(.RemittanceInformation_Old, objIfti.RemittanceInformation)
    '                        FillOrNothing(.SendertoReceiverInformation_Old, objIfti.SendertoReceiverInformation)
    '                        FillOrNothing(.RegulatoryReporting_Old, objIfti.RegulatoryReporting)
    '                        FillOrNothing(.EnvelopeContents_Old, objIfti.EnvelopeContents)

    '                        '---new-----
    '                        FillOrNothing(.InformationAbout_SenderCorrespondent, Me.InformasiLainnya_SwiftInSender.Text, True, Ovarchar)
    '                        FillOrNothing(.InformationAbout_ReceiverCorrespondent, Me.InformasiLainnya_SwiftInreceiver.Text, True, Ovarchar)
    '                        FillOrNothing(.InformationAbout_Thirdreimbursementinstitution, Me.InformasiLainnya_SwiftInthirdReimbursement.Text, True, Ovarchar)
    '                        FillOrNothing(.InformationAbout_IntermediaryInstitution, Me.InformasiLainnya_SwiftInintermediary.Text, True, Ovarchar)
    '                        FillOrNothing(.RemittanceInformation, Me.InformasiLainnya_SwiftInRemittance.Text, True, Ovarchar)
    '                        FillOrNothing(.SendertoReceiverInformation, Me.InformasiLainnya_SwiftInSenderToReceiver.Text, True, Ovarchar)
    '                        FillOrNothing(.RegulatoryReporting, Me.InformasiLainnya_SwiftInRegulatoryReport.Text, True, Ovarchar)
    '                        FillOrNothing(.EnvelopeContents, Me.InformasiLainnya_SwiftInEnvelopeContents.Text, True, Ovarchar)

    '                        'saving
    '                    End With

    '                    DataRepository.IFTI_Approval_DetailProvider.Save(objIfti_ApprovalDetail)
    '                End Using
    '                ''Send Email
    '                'SendEmail("Ms Bentuk Badan Usaha Edit (User Id : " & SessionNIK & "-" & SessionGroupMenuName & ")", _
    '                '          "Ms Bentuk Badan Usaha", SessionIsBankWide, SessionFkGroupApprovalId, "", "MsBentukBidangUsaha_Approval_view.aspx")
    '                'ImgBtnSave.Visible = False
    '                'ImgBackAdd.Visible = False
    '                'LblConfirmation.Text = "Data has been edited and waiting for approval"
    '                'MtvMsUser.ActiveViewIndex = 1
    '            End Using
    '        End If
    '    Catch ex As Exception
    '        'LogError(ex)
    '        cvalPageErr.IsValid = False
    '        cvalPageErr.ErrorMessage = ex.Message
    '    End Try
    'End Sub
    'Sub SaveNonSwiftOut()
    '    Try
    '        Page.Validate("handle")
    '        If Page.IsValid Then
    '            'ValidasiControl()

    '            ' =========== Insert Header Approval
    '            Dim KeyHeaderApproval As Integer
    '            Using objIftiApproval As New IFTI_Approval
    '                With objIftiApproval
    '                    FillOrNothing(.FK_MsMode_Id, 2, True, oInt)
    '                    FillOrNothing(.RequestedBy, SessionPkUserId)
    '                    FillOrNothing(.RequestedDate, Date.Now)
    '                    'FillOrNothing(.IsUpload, False)
    '                End With
    '                DataRepository.IFTI_ApprovalProvider.Save(objIftiApproval)
    '                KeyHeaderApproval = objIftiApproval.PK_IFTI_Approval_Id
    '            End Using

    '            '============ Insert Detail Approval
    '            Using objIfti_ApprovalDetail As New IFTI_Approval_Detail()
    '                Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)

    '                    Dim senderType As Integer = objIfti.Sender_FK_IFTI_NasabahType_ID
    '                    With objIfti_ApprovalDetail
    '                        'FK
    '                        .PK_IFTI_ID_Old = objIfti.PK_IFTI_ID
    '                        .PK_IFTI_ID = objIfti.PK_IFTI_ID
    '                        .FK_IFTI_Approval_Id = KeyHeaderApproval
    '                        .FK_IFTI_Type_ID_Old = objIfti.FK_IFTI_Type_ID
    '                        .FK_IFTI_Type_ID = 3
    '                        'umum
    '                        '--old--
    '                        FillOrNothing(.LTDLNNo_Old, objIfti.LTDLNNo, True, oInt)
    '                        FillOrNothing(.LTDLNNoKoreksi_Old, objIfti.LTDLNNoKoreksi, True, oInt)
    '                        FillOrNothing(.TanggalLaporan_Old, objIfti.TanggalLaporan, True, oDate)
    '                        FillOrNothing(.NamaPJKBankPelapor_Old, objIfti.NamaPJKBankPelapor, True, Ovarchar)
    '                        FillOrNothing(.NamaPejabatPJKBankPelapor_Old, objIfti.NamaPejabatPJKBankPelapor, True, Ovarchar)
    '                        FillOrNothing(.JenisLaporan_Old, objIfti.JenisLaporan, True, oInt)
    '                        '--new--
    '                        FillOrNothing(.LTDLNNo, Me.TxtUmum_NonSwiftOutLTDLN, True, oInt)
    '                        FillOrNothing(.LTDLNNoKoreksi, Me.TxtUmum_NonSwiftOutLTDLNKoreksi.Text, True, oInt)
    '                        FillOrNothing(.TanggalLaporan, Me.TxtUmum_NonSwiftOutPJKBankPelapor.Text, True, oDate)
    '                        FillOrNothing(.NamaPJKBankPelapor, Me.TxtUmum_NonSwiftOutPJKBankPelapor.Text, True, Ovarchar)
    '                        FillOrNothing(.NamaPejabatPJKBankPelapor, Me.TxtUmum_NonSwiftOutNamaPejabatPJKBankPelapor.Text, True, Ovarchar)
    '                        FillOrNothing(.JenisLaporan, Me.Rb_Umum_NonSwiftOut_jenislaporan.SelectedValue, True, oInt)
    '                        'cekPengirim
    '                        Select Case (senderType)
    '                            Case 1
    '                                'pengirimNasabahIndividu
    '                                'cekvalidasi
    '                                FillOrNothing(.Sender_FK_IFTI_NasabahType_ID_Old, objIfti.Sender_FK_IFTI_NasabahType_ID)
    '                                FillOrNothing(.Sender_FK_IFTI_NasabahType_ID, senderType, True, Ovarchar)

    '                                FillOrNothing(.Sender_Nasabah_INDV_NoRekening_Old, objIfti.Sender_Nasabah_INDV_NoRekening)
    '                                FillOrNothing(.Sender_Nasabah_INDV_NoRekening, Me.TxtIdenNonSwiftOutPengirim_Norekening.Text, True, Ovarchar)

    '                                FillOrNothing(.Sender_Nasabah_INDV_NamaLengkap_Old, objIfti.Sender_Nasabah_INDV_NamaLengkap)
    '                                FillOrNothing(.Sender_Nasabah_INDV_NamaLengkap, Me.TxtIdenNonSwiftOutPengirimNas_Ind_NamaLengkap.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_Nasabah_INDV_TanggalLahir_Old, objIfti.Sender_Nasabah_INDV_TanggalLahir)
    '                                FillOrNothing(.Sender_Nasabah_INDV_TanggalLahir, Me.TxtIdenNonSwiftOutPengirimNas_Ind_TanggalLahir.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_Nasabah_INDV_KewargaNegaraan_Old, objIfti.Sender_Nasabah_INDV_KewargaNegaraan)
    '                                FillOrNothing(.Sender_Nasabah_INDV_KewargaNegaraan, Me.RbIdenNonSwiftOutPengirimNas_Ind_Kewarganegaraan.SelectedValue, True, oInt)
    '                                FillOrNothing(.Sender_Nasabah_INDV_Negara_Old, objIfti.Sender_Nasabah_INDV_Negara)
    '                                FillOrNothing(.Sender_Nasabah_INDV_NegaraLainnya_Old, objIfti.Sender_Nasabah_INDV_NegaraLainnya)
    '                                If Me.RbPenerimaNasabah_IND_Warganegara.SelectedValue = "2" Then
    '                                    FillOrNothing(.Sender_Nasabah_INDV_Negara, Me.hfIdenNonSwiftOutPengirimNas_Ind_Negara.Value, True, oInt)
    '                                    FillOrNothing(.Sender_Nasabah_INDV_NegaraLainnya, Me.TxtIdenNonSwiftOutPengirimNas_Ind_NegaraLain.Text, True, Ovarchar)
    '                                End If
    '                                FillOrNothing(.Sender_Nasabah_INDV_Pekerjaan_Old, objIfti.Sender_Nasabah_INDV_Pekerjaan)
    '                                FillOrNothing(.Sender_Nasabah_INDV_Pekerjaan, Me.hfIdenNonSwiftOutPengirimNas_Ind_pekerjaan.Value, True, oInt)
    '                                FillOrNothing(.Sender_Nasabah_INDV_PekerjaanLainnya_Old, objIfti.Sender_Nasabah_INDV_PekerjaanLainnya)
    '                                FillOrNothing(.Sender_Nasabah_INDV_PekerjaanLainnya, Me.TxtIdenNonSwiftOutPengirimNas_Ind_Pekerjaanlain.Text, Ovarchar)
    '                                FillOrNothing(.Sender_Nasabah_INDV_DOM_Alamat_Old, objIfti.Sender_Nasabah_INDV_ID_Alamat)
    '                                FillOrNothing(.Sender_Nasabah_INDV_DOM_Alamat, Me.TxtIdenNonSwiftOutPengirimNas_Ind_Alamat.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_Nasabah_INDV_DOM_KotaKab_Old, objIfti.Sender_Nasabah_INDV_ID_KotaKab)
    '                                FillOrNothing(.Sender_Nasabah_INDV_DOM_KotaKab, Me.hfIdenNonSwiftOutPengirimNas_Ind_Kota_dom.Value, True, oInt)
    '                                FillOrNothing(.Sender_Nasabah_INDV_DOM_KotaKabLainnya_Old, objIfti.Sender_Nasabah_INDV_ID_KotaKabLainnya)
    '                                FillOrNothing(.Sender_Nasabah_INDV_DOM_KotaKabLainnya, Me.TxtIdenNonSwiftOutPengirimNas_Ind_KotaLainIden.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_Nasabah_INDV_DOM_Propinsi_Old, objIfti.Sender_Nasabah_INDV_ID_Propinsi)
    '                                FillOrNothing(.Sender_Nasabah_INDV_DOM_Propinsi, Me.hfIdenNonSwiftOutPengirimNas_Ind_provinsi_dom.Value, True, oInt)
    '                                FillOrNothing(.Sender_Nasabah_INDV_DOM_PropinsiLainnya_Old, objIfti.Sender_Nasabah_INDV_ID_PropinsiLainnya)
    '                                FillOrNothing(.Sender_Nasabah_INDV_DOM_PropinsiLainnya, Me.TxtIdenNonSwiftOutPengirimNas_Ind_provinsiLain.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_Nasabah_INDV_ID_Alamat_Old, objIfti.Sender_Nasabah_INDV_ID_Alamat)
    '                                FillOrNothing(.Sender_Nasabah_INDV_ID_Alamat, Me.TxtIdenNonSwiftOutPengirimNas_Ind_alamatIdentitas.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_Nasabah_INDV_ID_KotaKab_Old, objIfti.Sender_Nasabah_INDV_ID_KotaKab)
    '                                FillOrNothing(.Sender_Nasabah_INDV_ID_KotaKab, Me.hfIdenNonSwiftOutPengirimNas_Ind_kotaIdentitas.Value, True, oInt)
    '                                FillOrNothing(.Sender_Nasabah_INDV_ID_KotaKabLainnya_Old, objIfti.Sender_Nasabah_INDV_ID_KotaKabLainnya)
    '                                FillOrNothing(.Sender_Nasabah_INDV_ID_KotaKabLainnya, Me.TxtIdenNonSwiftOutPengirimNas_Ind_kotalain.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_Nasabah_INDV_ID_Propinsi_Old, objIfti.Sender_Nasabah_INDV_ID_Propinsi)
    '                                FillOrNothing(.Sender_Nasabah_INDV_ID_Propinsi, Me.hfIdenNonSwiftOutPengirimNas_Ind_ProvinsiIden.Value, True, oInt)
    '                                FillOrNothing(.Sender_Nasabah_INDV_ID_PropinsiLainnya_Old, objIfti.Sender_Nasabah_INDV_ID_PropinsiLainnya)
    '                                FillOrNothing(.Sender_Nasabah_INDV_ID_PropinsiLainnya, Me.TxtIdenNonSwiftOutPengirimNas_Ind_ProvinsilainIden.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_Nasabah_INDV_FK_IFTI_IDType_Old, objIfti.Sender_Nasabah_INDV_FK_IFTI_IDType)
    '                                FillOrNothing(.Sender_Nasabah_INDV_FK_IFTI_IDType, Me.cboNonSwiftOutPengirimNasInd_jenisidentitas.SelectedValue, True, oInt)
    '                                FillOrNothing(.Sender_Nasabah_INDV_NomorID_Old, objIfti.Sender_Nasabah_INDV_NomorID)
    '                                FillOrNothing(.Sender_Nasabah_INDV_NomorID, Me.TxtIdenNonSwiftOutPengirimNas_Ind_noIdentitas.Text, True, Ovarchar)

    '                            Case 2
    '                                'pengirimNasabahKorporasi
    '                                'cekvalidasi
    '                                FillOrNothing(.Sender_FK_IFTI_NasabahType_ID_Old, objIfti.Sender_FK_IFTI_NasabahType_ID)
    '                                FillOrNothing(.Sender_FK_IFTI_NasabahType_ID, senderType, True, Ovarchar)

    '                                FillOrNothing(.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id_Old, objIfti.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id)
    '                                'FillOrNothing(.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id, Me.TxtIdenPengirimNas_Corp_BentukBadanUsaha.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_Nasabah_CORP_BentukBadanUsahaLainnya_Old, objIfti.Sender_Nasabah_CORP_BentukBadanUsahaLainnya)
    '                                FillOrNothing(.Sender_Nasabah_CORP_BentukBadanUsahaLainnya, Me.TxtIdenNonSwiftOutPengirimNas_Corp_BentukBadanUsahaLain.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_Nasabah_CORP_NamaKorporasi_Old, objIfti.Sender_Nasabah_CORP_NamaKorporasi)
    '                                FillOrNothing(.Sender_Nasabah_CORP_NamaKorporasi, Me.TxtIdenNonSwiftOutPengirimNas_Corp_NamaKorp.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id_Old, objIfti.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id)
    '                                FillOrNothing(.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id, Me.hfIdenNonSwiftOutPengirimNas_Corp_BidangUsahaKorp.Value, True, oInt)
    '                                FillOrNothing(.Sender_Nasabah_CORP_AlamatLengkap_Old, objIfti.Sender_Nasabah_CORP_AlamatLengkap)
    '                                FillOrNothing(.Sender_Nasabah_CORP_AlamatLengkap, Me.TxtIdenNonSwiftOutPengirimNas_Corp_alamatkorp.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_Nasabah_COR_KotaKab_Old, objIfti.Sender_Nasabah_CORP_KotaKab)
    '                                FillOrNothing(.Sender_Nasabah_COR_KotaKab, Me.hfIdenNonSwiftOutPengirimNas_Corp_kotakorp.Value, True, oInt)
    '                                FillOrNothing(.Sender_Nasabah_COR_KotaKabLainnya_Old, objIfti.Sender_Nasabah_CORP_KotaKabLainnya)
    '                                FillOrNothing(.Sender_Nasabah_COR_KotaKabLainnya, Me.TxtIdenNonSwiftOutPengirimNas_Ind_KotaLainIden.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_Nasabah_CORP_Propinsi_Old, objIfti.Sender_Nasabah_CORP_Propinsi)
    '                                FillOrNothing(.Sender_Nasabah_CORP_Propinsi, Me.hfIdenNonSwiftOutPengirimNas_Corp_provKorp.Value, True, oInt)
    '                                FillOrNothing(.Sender_Nasabah_CORP_PropinsiLainnya_Old, objIfti.Sender_Nasabah_CORP_PropinsiLainnya)
    '                                FillOrNothing(.Sender_Nasabah_CORP_PropinsiLainnya, Me.TxtIdenNonSwiftOutPengirimNas_Corp_provKorpLain.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_Nasabah_CORP_LN_AlamatLengkap_Old, objIfti.Sender_Nasabah_CORP_LN_AlamatLengkap)
    '                                FillOrNothing(.Sender_Nasabah_CORP_LN_AlamatLengkap, Me.TxtIdenNonSwiftOutPengirimNas_Corp_AlamatLuar.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_Nasabah_CORP_Alamat_KantorCabangPengirim_Old, objIfti.Sender_Nasabah_CORP_Alamat_KantorCabangPengirim)
    '                                FillOrNothing(.Sender_Nasabah_CORP_Alamat_KantorCabangPengirim, Me.TxtIdenNonSwiftOutPengirimNas_Corp_AlamatAsal.Text, True, Ovarchar)

    '                            Case 3
    '                                'PengirimNonNasabah
    '                                'cekvalidasi
    '                                FillOrNothing(.Sender_FK_IFTI_NasabahType_ID_Old, objIfti.Sender_FK_IFTI_NasabahType_ID)
    '                                FillOrNothing(.Sender_FK_IFTI_NasabahType_ID, senderType, True, Ovarchar)

    '                                FillOrNothing(.FK_IFTI_NonNasabahNominalType_ID_Old, objIfti.FK_IFTI_NonNasabahNominalType_ID)
    '                                FillOrNothing(.FK_IFTI_NonNasabahNominalType_ID, Me.RbPengirimNonNasabah_100Juta.SelectedValue, True, oInt)
    '                                FillOrNothing(.Sender_NonNasabah_NamaLengkap_Old, objIfti.Sender_NonNasabah_NamaLengkap)
    '                                FillOrNothing(.Sender_NonNasabah_NamaLengkap, Me.TxtPengirimNonNasabah_nama.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_NonNasabah_TanggalLahir_Old, objIfti.Sender_NonNasabah_TanggalLahir)
    '                                FillOrNothing(.Sender_NonNasabah_TanggalLahir, Me.TxtPengirimNonNasabah_TanggalLahir.Text, True, oDate)
    '                                FillOrNothing(.Sender_NonNasabah_ID_Alamat_Old, objIfti.Sender_NonNasabah_ID_Alamat)
    '                                FillOrNothing(.Sender_NonNasabah_ID_Alamat, Me.TxtPengirimNonNasabah_alamatiden.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_NonNasabah_ID_KotaKab_Old, objIfti.Sender_NonNasabah_ID_KotaKab)
    '                                FillOrNothing(.Sender_NonNasabah_ID_KotaKab, Me.hfPengirimNonNasabah_kotaIden.Value, True, oInt)
    '                                FillOrNothing(.Sender_NonNasabah_ID_KotaKabLainnya_Old, objIfti.Sender_NonNasabah_ID_KotaKabLainnya)
    '                                FillOrNothing(.Sender_NonNasabah_ID_KotaKabLainnya, Me.TxtPengirimNonNasabah_kotaIden.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_NonNasabah_ID_Propinsi_Old, objIfti.Sender_NonNasabah_ID_Propinsi)
    '                                FillOrNothing(.Sender_NonNasabah_ID_Propinsi, Me.hfPengirimNonNasabah_ProvIden.Value, True, oInt)
    '                                FillOrNothing(.Sender_NonNasabah_ID_PropinsiLainnya_Old, objIfti.Sender_NonNasabah_ID_PropinsiLainnya)
    '                                FillOrNothing(.Sender_NonNasabah_ID_PropinsiLainnya, Me.hfPengirimNonNasabah_ProvIden, False, oInt)
    '                                FillOrNothing(.Sender_NonNasabah_FK_IFTI_IDType_Old, objIfti.Sender_Nasabah_INDV_FK_IFTI_IDType)
    '                                FillOrNothing(.Sender_NonNasabah_FK_IFTI_IDType, Me.CboPengirimNonNasabah_JenisDokumen.SelectedValue, True, oInt)
    '                                FillOrNothing(.Sender_NonNasabah_NomorID_Old, objIfti.Sender_NonNasabah_NomorID)
    '                                FillOrNothing(.Sender_NonNasabah_NomorID, Me.TxtPengirimNonNasabah_NomorIden.Text, True, Ovarchar)

    '                        End Select
    '                        'cekBOwner
    '                        If Not IsNothing(objIfti.FK_IFTI_BeneficialOwnerType_ID) Then
    '                            FillOrNothing(.BeneficialOwner_HubunganDenganPemilikDana_Old, objIfti.BeneficialOwner_HubunganDenganPemilikDana)
    '                            FillOrNothing(.BeneficialOwner_HubunganDenganPemilikDana, Me.BenfOwnerNonSwiftOutHubunganPemilikDana.Text, True, Ovarchar)
    '                            Dim BOwnerID As Integer = objIfti.FK_IFTI_BeneficialOwnerType_ID
    '                            Select Case (BOwnerID)
    '                                Case 1
    '                                    'Nasabah
    '                                    FillOrNothing(.BeneficialOwner_NoRekening_Old, objIfti.BeneficialOwner_NoRekening)
    '                                    FillOrNothing(.BeneficialOwner_NoRekening, Me.BenfOwnerNonSwiftOutNasabah_rekening.Text, True, Ovarchar)
    '                                    FillOrNothing(.BeneficialOwner_NamaLengkap_Old, objIfti.BeneficialOwner_NamaLengkap)
    '                                    FillOrNothing(.BeneficialOwner_NamaLengkap, Me.BenfOwnerNonSwiftOutNasabah_Nama.Text, True, Ovarchar)
    '                                    FillOrNothing(.BeneficialOwner_TanggalLahir_Old, objIfti.BeneficialOwner_TanggalLahir)
    '                                    FillOrNothing(.BeneficialOwner_TanggalLahir, Me.BenfOwnerNonSwiftOutNasabah_tanggalLahir, True, oDate)
    '                                    FillOrNothing(.BeneficialOwner_ID_Alamat_Old, objIfti.BeneficialOwner_ID_Alamat)
    '                                    FillOrNothing(.BeneficialOwner_ID_Alamat, Me.BenfOwnerNonSwiftOutNasabah_AlamatIden.Text, True, Ovarchar)
    '                                    FillOrNothing(.BeneficialOwner_ID_KotaKab, Me.hfBenfOwnerNonSwiftOutNasabah_KotaIden.Value, True, oInt)
    '                                    FillOrNothing(.BeneficialOwner_ID_KotaKab_Old, objIfti.BeneficialOwner_ID_KotaKab)
    '                                    FillOrNothing(.BeneficialOwner_ID_KotaKabLainnya_Old, objIfti.BeneficialOwner_ID_KotaKabLainnya)
    '                                    FillOrNothing(.BeneficialOwner_ID_KotaKabLainnya, Me.BenfOwnerNonSwiftOutNasabah_kotaLainIden.Text, True, Ovarchar)
    '                                    FillOrNothing(.BeneficialOwner_ID_Propinsi_Old, objIfti.BeneficialOwner_ID_Propinsi)
    '                                    FillOrNothing(.BeneficialOwner_ID_Propinsi, Me.BenfOwnerNonSwiftOutNasabah_ProvinsiIden.Text, True, oInt)
    '                                    FillOrNothing(.BeneficialOwner_ID_PropinsiLainnya_Old, objIfti.BeneficialOwner_ID_PropinsiLainnya)
    '                                    FillOrNothing(.BeneficialOwner_ID_PropinsiLainnya, Me.BenfOwnerNonSwiftOutNasabah_ProvinsiLainIden.Text, True, Ovarchar)
    '                                    FillOrNothing(.BeneficialOwner_FK_IFTI_IDType_Old, objIfti.BeneficialOwner_FK_IFTI_IDType)
    '                                    FillOrNothing(.BeneficialOwner_FK_IFTI_IDType, Me.CBOBenfOwnerNonSwiftOutNasabah_JenisDokumen.SelectedValue, True, oInt)
    '                                    FillOrNothing(.BeneficialOwner_NomorID_Old, objIfti.BeneficialOwner_NomorID)
    '                                    FillOrNothing(.BeneficialOwner_NomorID, Me.BenfOwnerNonSwiftOutNasabah_NomorIden.Text, True, Ovarchar)

    '                                Case 2
    '                                    'NonNasabah
    '                                    FillOrNothing(.BeneficialOwner_NamaLengkap_Old, objIfti.BeneficialOwner_NamaLengkap)
    '                                    FillOrNothing(.BeneficialOwner_NamaLengkap, Me.BenfOwnerNonSwiftOutNonNasabah_Nama.Text, True, Ovarchar)
    '                                    FillOrNothing(.BeneficialOwner_TanggalLahir_Old, objIfti.BeneficialOwner_TanggalLahir)
    '                                    FillOrNothing(.BeneficialOwner_TanggalLahir, Me.BenfOwnerNonSwiftOutNonNasabah_TanggalLahir, True, oDate)
    '                                    FillOrNothing(.BeneficialOwner_ID_Alamat_Old, objIfti.BeneficialOwner_ID_Alamat)
    '                                    FillOrNothing(.BeneficialOwner_ID_Alamat, Me.BenfOwnerNonSwiftOutNonNasabah_AlamatIden.Text, True, Ovarchar)
    '                                    FillOrNothing(.BeneficialOwner_ID_KotaKab, Me.hfBenfOwnerNonSwiftOutNonNasabah_KotaIden.Value, True, oInt)
    '                                    FillOrNothing(.BeneficialOwner_ID_KotaKab_Old, objIfti.BeneficialOwner_ID_KotaKab)
    '                                    FillOrNothing(.BeneficialOwner_ID_KotaKabLainnya_Old, objIfti.BeneficialOwner_ID_KotaKabLainnya)
    '                                    FillOrNothing(.BeneficialOwner_ID_KotaKabLainnya, Me.BenfOwnerNonSwiftOutNonNasabah_KotaLainIden.Text, True, Ovarchar)
    '                                    FillOrNothing(.BeneficialOwner_ID_Propinsi_Old, objIfti.BeneficialOwner_ID_Propinsi)
    '                                    FillOrNothing(.BeneficialOwner_ID_Propinsi, Me.BenfOwnerNonSwiftOutNonNasabah_ProvinsiIden.Text, True, oInt)
    '                                    FillOrNothing(.BeneficialOwner_ID_PropinsiLainnya_Old, objIfti.BeneficialOwner_ID_PropinsiLainnya)
    '                                    FillOrNothing(.BeneficialOwner_ID_PropinsiLainnya, Me.BenfOwnerNonSwiftOutNonNasabah_ProvinsiLainIden.Text, True, Ovarchar)
    '                                    FillOrNothing(.BeneficialOwner_FK_IFTI_IDType_Old, objIfti.BeneficialOwner_FK_IFTI_IDType)
    '                                    FillOrNothing(.BeneficialOwner_FK_IFTI_IDType, Me.CBOBenfOwnerNonSwiftOutNonNasabah_JenisDokumen.SelectedValue, True, oInt)
    '                                    FillOrNothing(.BeneficialOwner_NomorID_Old, objIfti.BeneficialOwner_NomorID)
    '                                    FillOrNothing(.BeneficialOwner_NomorID, Me.BenfOwnerNonSwiftOutNonNasabah_NomorIdentitas.Text, True, Ovarchar)
    '                            End Select

    '                        End If

    '                        'cekPenerima
    '                        Using objReceiver As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("FK_IFTI_ID=" & getIFTIPK, "", 0, Integer.MaxValue, 0)
    '                            Dim TipePenerima As Integer = objReceiver(0).FK_IFTI_NasabahType_ID
    '                            Select Case (TipePenerima)
    '                                Case 1
    '                                    'FillOrNothing(.)
    '                                Case 2
    '                                Case 3
    '                                    'saveappDetailPenerima
    '                            End Select


    '                        End Using

    '                        'cekTransaksi
    '                        '----old----
    '                        FillOrNothing(.TanggalTransaksi_Old, objIfti.TanggalTransaksi)
    '                        FillOrNothing(.TimeIndication_Old, objIfti.TimeIndication)
    '                        FillOrNothing(.SenderReference_Old, objIfti.SenderReference)
    '                        FillOrNothing(.RelatedReference_Old, objIfti.RelatedReference)
    '                        FillOrNothing(.BankOperationCode_Old, objIfti.BankOperationCode)
    '                        FillOrNothing(.InstructionCode_Old, objIfti.InstructionCode)
    '                        FillOrNothing(.KantorCabangPenyelengaraPengirimAsal_Old, objIfti.KantorCabangPenyelengaraPengirimAsal)
    '                        FillOrNothing(.TransactionCode, objIfti.TransactionCode)
    '                        FillOrNothing(.ValueDate_TanggalTransaksi_Old, objIfti.ValueDate_TanggalTransaksi)
    '                        FillOrNothing(.ValueDate_NilaiTransaksi_Old, objIfti.ValueDate_NilaiTransaksi)
    '                        FillOrNothing(.ValueDate_FK_Currency_ID_Old, objIfti.ValueDate_FK_Currency_ID)
    '                        FillOrNothing(.ValueDate_NilaiTransaksiIDR_Old, objIfti.ValueDate_NilaiTransaksiIDR)
    '                        FillOrNothing(.Instructed_Currency_Old, objIfti.Instructed_Currency)
    '                        FillOrNothing(.Instructed_Amount_Old, objIfti.Instructed_Amount)
    '                        FillOrNothing(.ExchangeRate_Old, objIfti.ExchangeRate)
    '                        FillOrNothing(.SendingInstitution_Old, objIfti.SendingInstitution)
    '                        FillOrNothing(.BeneficiaryInstitution_Old, objIfti.BeneficiaryInstitution)
    '                        FillOrNothing(.TujuanTransaksi_Old, objIfti.BeneficiaryInstitution)
    '                        FillOrNothing(.SumberPenggunaanDana_Old, objIfti.SumberPenggunaanDana)

    '                        '-----new---
    '                        FillOrNothing(.TanggalTransaksi, Me.Transaksi_NonSwiftOut_tanggal.Text, True, oDate)
    '                        FillOrNothing(.KantorCabangPenyelengaraPengirimAsal, Me.Transaksi_NonSwiftOut_kantorCabangPengirim.Text, True, Ovarchar)
    '                        FillOrNothing(.ValueDate_TanggalTransaksi, Me.Transaksi_NonSwiftOut_ValueTanggalTransaksi.Text, True, oDate)
    '                        FillOrNothing(.ValueDate_NilaiTransaksi, Me.cboTransaksi_NonSwiftOut_nilaitransaksi.SelectedValue, True, oDecimal)
    '                        FillOrNothing(.ValueDate_FK_Currency_ID_Old, Me.Transaksi_NonSwiftOut_currency.Text, True, oInt)
    '                        FillOrNothing(.ValueDate_NilaiTransaksiIDR, Me.Transaksi_NonSwiftOut_AmountdalamRupiah.Text, True, oDecimal)
    '                        FillOrNothing(.Instructed_Currency, Me.Transaksi_NonSwiftOut_currency.Text, True, oInt)
    '                        FillOrNothing(.Instructed_Amount, Me.Transaksi_NonSwiftOut_instructedAmount.Text, True, oDecimal)
    '                        FillOrNothing(.TujuanTransaksi, Me.Transaksi_NonSwiftOut_TujuanTransaksi.Text, True, Ovarchar)
    '                        FillOrNothing(.SumberPenggunaanDana, Me.Transaksi_NonSwiftOut_SumberPenggunaanDana.Text, True, Ovarchar)

    '                        'saving
    '                    End With

    '                    DataRepository.IFTI_Approval_DetailProvider.Save(objIfti_ApprovalDetail)
    '                End Using
    '                ''Send Email
    '                'SendEmail("Ms Bentuk Badan Usaha Edit (User Id : " & SessionNIK & "-" & SessionGroupMenuName & ")", _
    '                '          "Ms Bentuk Badan Usaha", SessionIsBankWide, SessionFkGroupApprovalId, "", "MsBentukBidangUsaha_Approval_view.aspx")
    '                'ImgBtnSave.Visible = False
    '                'ImgBackAdd.Visible = False
    '                'LblConfirmation.Text = "Data has been edited and waiting for approval"
    '                'MtvMsUser.ActiveViewIndex = 1
    '            End Using
    '        End If
    '    Catch ex As Exception
    '        'LogError(ex)
    '        cvalPageErr.IsValid = False
    '        cvalPageErr.ErrorMessage = ex.Message
    '    End Try
    'End Sub
    'Sub SaveNonSwiftInco()
    '    Try
    '        Page.Validate("handle")
    '        If Page.IsValid Then
    '            'ValidasiControl()

    '            ' =========== Insert Header Approval
    '            Dim KeyHeaderApproval As Integer
    '            Using objIftiApproval As New IFTI_Approval
    '                With objIftiApproval
    '                    FillOrNothing(.FK_MsMode_Id, 2, True, oInt)
    '                    FillOrNothing(.RequestedBy, SessionPkUserId)
    '                    FillOrNothing(.RequestedDate, Date.Now)
    '                    'FillOrNothing(.IsUpload, False)
    '                End With
    '                DataRepository.IFTI_ApprovalProvider.Save(objIftiApproval)
    '                KeyHeaderApproval = objIftiApproval.PK_IFTI_Approval_Id
    '            End Using

    '            '============ Insert Detail Approval
    '            Using objIfti_ApprovalDetail As New IFTI_Approval_Detail()
    '                Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)

    '                    Dim senderType As Integer = objIfti.Sender_FK_IFTI_NasabahType_ID
    '                    With objIfti_ApprovalDetail
    '                        'FK
    '                        .PK_IFTI_ID_Old = objIfti.PK_IFTI_ID
    '                        .PK_IFTI_ID = objIfti.PK_IFTI_ID
    '                        .FK_IFTI_Approval_Id = KeyHeaderApproval
    '                        .FK_IFTI_Type_ID_Old = objIfti.FK_IFTI_Type_ID
    '                        .FK_IFTI_Type_ID = 4
    '                        'umum
    '                        '--old--
    '                        FillOrNothing(.LTDLNNo_Old, objIfti.LTDLNNo, True, oInt)
    '                        FillOrNothing(.LTDLNNoKoreksi_Old, objIfti.LTDLNNoKoreksi, True, oInt)
    '                        FillOrNothing(.TanggalLaporan_Old, objIfti.TanggalLaporan, True, oDate)
    '                        FillOrNothing(.NamaPJKBankPelapor_Old, objIfti.NamaPJKBankPelapor, True, Ovarchar)
    '                        FillOrNothing(.NamaPejabatPJKBankPelapor_Old, objIfti.NamaPejabatPJKBankPelapor, True, Ovarchar)
    '                        FillOrNothing(.JenisLaporan_Old, objIfti.JenisLaporan, True, oInt)
    '                        '--new--
    '                        FillOrNothing(.LTDLNNo, Me.NonSwiftInUmum_LTDN, True, oInt)
    '                        FillOrNothing(.LTDLNNoKoreksi, Me.NonSwiftInUmum_LtdnKoreksi.Text, True, oInt)
    '                        FillOrNothing(.TanggalLaporan, Me.NonSwiftInUmum_TanggalLaporan.Text, True, oDate)
    '                        FillOrNothing(.NamaPJKBankPelapor, Me.NonSwiftInUmum_NamaPJKBank.Text, True, Ovarchar)
    '                        FillOrNothing(.NamaPejabatPJKBankPelapor, Me.NonSwiftInUmum_NamaPejabatPJKBank.Text, True, Ovarchar)
    '                        FillOrNothing(.JenisLaporan, Me.RbNonSwiftInUmum_JenisLaporan.SelectedValue, True, oInt)
    '                        'cekPengirim
    '                        Select Case (senderType)
    '                            Case 1
    '                                'pengirimNasabahIndividu
    '                                'cekvalidasi
    '                                FillOrNothing(.Sender_FK_IFTI_NasabahType_ID_Old, objIfti.Sender_FK_IFTI_NasabahType_ID)
    '                                FillOrNothing(.Sender_FK_IFTI_NasabahType_ID, senderType, True, Ovarchar)

    '                                FillOrNothing(.Sender_Nasabah_INDV_NoRekening_Old, objIfti.Sender_Nasabah_INDV_NoRekening)
    '                                FillOrNothing(.Sender_Nasabah_INDV_NoRekening, Me.txtNonSwiftInPengirimNasabah_IND_Rekening.Text, True, Ovarchar)

    '                                FillOrNothing(.Sender_Nasabah_INDV_NamaLengkap_Old, objIfti.Sender_Nasabah_INDV_NamaLengkap, )
    '                                FillOrNothing(.Sender_Nasabah_INDV_NamaLengkap, Me.TxtNonSwiftIn_pengirimPerorangan_NamaLengkap.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_Nasabah_INDV_TanggalLahir_Old, objIfti.Sender_Nasabah_INDV_TanggalLahir)
    '                                FillOrNothing(.Sender_Nasabah_INDV_TanggalLahir, Me.TxtNonSwiftIn_pengirimPerorangan_TglLahir.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_Nasabah_INDV_KewargaNegaraan_Old, objIfti.Sender_Nasabah_INDV_KewargaNegaraan)
    '                                FillOrNothing(.Sender_Nasabah_INDV_KewargaNegaraan, Me.RbNonSwiftIn_pengirimPerorangan_Kewarganegaraan.SelectedValue, True, oInt)
    '                                FillOrNothing(.Sender_Nasabah_INDV_Negara_Old, objIfti.Sender_Nasabah_INDV_Negara)
    '                                FillOrNothing(.Sender_Nasabah_INDV_NegaraLainnya_Old, objIfti.Sender_Nasabah_INDV_NegaraLainnya)
    '                                If Me.RbPenerimaNasabah_IND_Warganegara.SelectedValue = "2" Then
    '                                    FillOrNothing(.Sender_Nasabah_INDV_Negara, Me.hfNonSwiftInPeroranganPengirimNas_Ind_Negara.Value, True, oInt)
    '                                    FillOrNothing(.Sender_Nasabah_INDV_NegaraLainnya, Me.TxtNonSwiftIn_pengirimPerorangan_NegaraLainnya.Text, True, Ovarchar)
    '                                End If
    '                                FillOrNothing(.Sender_Nasabah_INDV_ID_Alamat_Old, objIfti.Sender_Nasabah_INDV_ID_Alamat)
    '                                FillOrNothing(.Sender_Nasabah_INDV_ID_Alamat, Me.TxtIdenNonSwiftOutPengirimNas_Ind_alamatIdentitas.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_Nasabah_INDV_ID_NegaraKota_Old, objIfti.Sender_Nasabah_INDV_ID_NegaraKota)
    '                                FillOrNothing(.Sender_Nasabah_INDV_ID_NegaraKota, Me.hfNonSwiftIn_pengirimPeroranganDom_NegaraBagian.Value, True, oInt)
    '                                FillOrNothing(.Sender_Nasabah_INDV_ID_Negara_Old, objIfti.Sender_Nasabah_INDV_ID_Negara)
    '                                FillOrNothing(.Sender_Nasabah_INDV_ID_Negara, Me.hfNonSwiftIn_pengirimPeroranganDom_Negara.Value, True, oInt)
    '                                FillOrNothing(.Sender_Nasabah_INDV_ID_NegaraLainnya_Old, objIfti.Sender_Nasabah_INDV_ID_NegaraLainnya)
    '                                FillOrNothing(.Sender_Nasabah_INDV_ID_NegaraLainnya, Me.TxtNonSwiftIn_pengirimPeroranganDom_NegaraLainnya.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_Nasabah_INDV_NoTelp_Old, objIfti.Sender_Nasabah_INDV_NoTelp)
    '                                FillOrNothing(.Sender_Nasabah_INDV_NoTelp, Me.TxtNonSwiftIn_pengirimPerorangan_NoTelp.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_Nasabah_INDV_FK_IFTI_IDType_Old, objIfti.Sender_Nasabah_INDV_FK_IFTI_IDType)
    '                                FillOrNothing(.Sender_Nasabah_INDV_FK_IFTI_IDType, Me.cbo_NonSwiftInpengirimNas_Ind_jenisidentitas.SelectedValue, True, oInt)
    '                                FillOrNothing(.Sender_Nasabah_INDV_NomorID_Old, objIfti.Sender_Nasabah_INDV_NomorID)
    '                                FillOrNothing(.Sender_Nasabah_INDV_NomorID, Me.TxtNonSwiftIndenPengirimaNas_Ind_noIdentitas.Text, True, Ovarchar)

    '                            Case 2
    '                                'pengirimNasabahKorporasi
    '                                'cekvalidasi
    '                                FillOrNothing(.Sender_FK_IFTI_NasabahType_ID_Old, objIfti.Sender_FK_IFTI_NasabahType_ID)
    '                                FillOrNothing(.Sender_FK_IFTI_NasabahType_ID, senderType, True, Ovarchar)

    '                                FillOrNothing(.Sender_Nasabah_CORP_NoRekening_Old, objIfti.Sender_Nasabah_CORP_NoRekening)
    '                                FillOrNothing(.Sender_Nasabah_CORP_NoRekening, Me.txtNonSwiftInPengirimNasabah_IND_Rekening.Text, True, Ovarchar)

    '                                FillOrNothing(.Sender_Nasabah_CORP_NamaKorporasi_Old, objIfti.Sender_Nasabah_CORP_NamaKorporasi)
    '                                FillOrNothing(.Sender_Nasabah_CORP_NamaKorporasi, Me.TxtNonSwiftIn_nasabah_Korporasi_NamaKorporasi.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_Nasabah_CORP_AlamatLengkap_Old, objIfti.Sender_Nasabah_CORP_AlamatLengkap)
    '                                FillOrNothing(.Sender_Nasabah_CORP_AlamatLengkap, Me.TxtNonSwiftIn_nasabah_Korporasi_alamat.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_Nasabah_CORP_NegaraKota_Old, objIfti.Sender_Nasabah_CORP_NegaraKota)
    '                                FillOrNothing(.Sender_Nasabah_CORP_NegaraKota, Me.TxtNonSwiftIn_nasabah_Korporasi_kota.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_Nasabah_CORP_Negara_Old, objIfti.Sender_Nasabah_CORP_NegaraKota)
    '                                FillOrNothing(.Sender_Nasabah_CORP_Negara, Me.hfNonSwiftInPengirimNonNasabah_Korp_Negara.Value, True, oInt)
    '                                FillOrNothing(.Sender_Nasabah_CORP_NegaraLainnya_Old, objIfti.Sender_Nasabah_CORP_NegaraLainnya)
    '                                FillOrNothing(.Sender_Nasabah_CORP_NegaraLainnya, Me.TxtNonSwiftIn_nasabah_Korporasi_NegaraLainnya.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_Nasabah_CORP_NoTelp_Old, objIfti.Sender_Nasabah_CORP_NoTelp)
    '                                FillOrNothing(.Sender_Nasabah_CORP_NoTelp, Me.TxtNonSwiftIn_nasabah_Korporasi_NoTelp.Text, True, Ovarchar)
    '                            Case 3
    '                                'PengirimNonNasabah
    '                                'cekvalidasi
    '                                FillOrNothing(.Sender_FK_IFTI_NasabahType_ID_Old, objIfti.Sender_FK_IFTI_NasabahType_ID)
    '                                FillOrNothing(.Sender_FK_IFTI_NasabahType_ID, senderType, True, Ovarchar)

    '                                FillOrNothing(.Sender_NonNasabah_NoRekening_Old, objIfti.Sender_NonNasabah_NoRekening)
    '                                FillOrNothing(.Sender_NonNasabah_NoRekening, Me.TxtNonSwiftInPengirimNonNasabah_NomorRek.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_NonNasabah_NamaLengkap_Old, objIfti.Sender_NonNasabah_NamaLengkap)
    '                                FillOrNothing(.Sender_NonNasabah_NamaLengkap, Me.TxtNonSwiftInPengirimNonNasabah_NamaLengkap.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_NonNasabah_NamaBank_Old, objIfti.Sender_NonNasabah_NamaBank)
    '                                FillOrNothing(.Sender_NonNasabah_NamaBank, Me.TxtNonSwiftInPengirimNonNasabah_NamaBank.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_NonNasabah_TanggalLahir_Old, objIfti.Sender_NonNasabah_TanggalLahir)
    '                                FillOrNothing(.Sender_NonNasabah_TanggalLahir, Me.TxtNonSwiftInPengirimNonNasabah_TanggalLhr.Text, True, oDate)
    '                                FillOrNothing(.Sender_NonNasabah_ID_Alamat_Old, objIfti.Sender_NonNasabah_ID_Alamat)
    '                                FillOrNothing(.Sender_NonNasabah_ID_Alamat, Me.TxtNonSwiftInPengirimNonNasabah_Alamat.Text, True, Ovarchar)

    '                                FillOrNothing(.Sender_NonNasabah_ID_NegaraBagian_Old, objIfti.Sender_NonNasabah_ID_NegaraBagian)
    '                                FillOrNothing(.Sender_NonNasabah_ID_NegaraBagian, Me.TxtNonSwiftIn_nonnasabah_Korporasi_kta.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_NonNasabah_ID_Negara_Old, objIfti.Sender_NonNasabah_ID_Negara)
    '                                FillOrNothing(.Sender_NonNasabah_ID_Negara, Me.hfNonSwiftInPengirimNonNasabah_Korp_Negara.Value, True, oInt)
    '                                FillOrNothing(.Sender_NonNasabah_ID_Negara_Old, objIfti.Sender_NonNasabah_ID_Negara)
    '                                FillOrNothing(.Sender_NonNasabah_ID_Negara, Me.TxtNonSwiftIn_nasabah_Korporasi_NegaraLainnya.Text, True, Ovarchar)
    '                                FillOrNothing(.Sender_NonNasabah_NoTelp_Old, objIfti.Sender_NonNasabah_NoTelp)
    '                                FillOrNothing(.Sender_NonNasabah_NoTelp, Me.TxtNonSwiftIn_nasabah_Korporasi_NoTelp.Text, True, Ovarchar)

    '                        End Select


    '                        'cekPenerima
    '                        Using objReceiver As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("FK_IFTI_ID=" & getIFTIPK, "", 0, Integer.MaxValue, 0)
    '                            Dim TipePenerima As Integer = objReceiver(0).FK_IFTI_NasabahType_ID
    '                            Select Case (TipePenerima)
    '                                Case 1
    '                                    'FillOrNothing(.)
    '                                Case 2
    '                                Case 3
    '                                    'saveappDetailPenerima
    '                            End Select


    '                        End Using

    '                        'cekTransaksi
    '                        '----old----
    '                        FillOrNothing(.TanggalTransaksi_Old, objIfti.TanggalTransaksi)
    '                        FillOrNothing(.KantorCabangPenyelengaraPengirimAsal_Old, objIfti.KantorCabangPenyelengaraPengirimAsal)
    '                        FillOrNothing(.ValueDate_TanggalTransaksi_Old, objIfti.ValueDate_TanggalTransaksi)
    '                        FillOrNothing(.ValueDate_NilaiTransaksi_Old, objIfti.ValueDate_NilaiTransaksi)
    '                        FillOrNothing(.ValueDate_FK_Currency_ID_Old, objIfti.ValueDate_FK_Currency_ID)
    '                        FillOrNothing(.ValueDate_NilaiTransaksiIDR_Old, objIfti.ValueDate_NilaiTransaksiIDR)
    '                        FillOrNothing(.Instructed_Currency_Old, objIfti.Instructed_Currency)
    '                        FillOrNothing(.Instructed_Amount_Old, objIfti.Instructed_Amount)
    '                        FillOrNothing(.TujuanTransaksi_Old, objIfti.BeneficiaryInstitution)
    '                        FillOrNothing(.SumberPenggunaanDana_Old, objIfti.SumberPenggunaanDana)

    '                        '-----new---
    '                        FillOrNothing(.TanggalTransaksi, Me.TxtTransaksi_NonSwiftIntanggal0.Text, True, oDate)
    '                        FillOrNothing(.KantorCabangPenyelengaraPengirimAsal, Me.TxtTransaksi_NonSwiftInkantorCabangPengirim.Text, True, Ovarchar)
    '                        FillOrNothing(.ValueDate_TanggalTransaksi, Me.TxtTransaksi_NonSwiftInValueTanggalTransaksi.Text, True, oDate)
    '                        FillOrNothing(.ValueDate_NilaiTransaksi, Me.cboTransaksi_NonSwiftInnilaitransaksi0.SelectedValue, True, oDecimal)
    '                        FillOrNothing(.ValueDate_FK_Currency_ID_Old, Me.hfTransaksi_NonSwiftIncurrency.Value, True, oInt)
    '                        FillOrNothing(.ValueDate_NilaiTransaksiIDR, Me.TxtTransaksi_NonSwiftInAmountdalamRupiah.Text, True, oDecimal)
    '                        FillOrNothing(.Instructed_Currency, Me.TxtTransaksi_NonSwiftIncurrency.Text, True, oInt)
    '                        FillOrNothing(.Instructed_Amount, Me.TxtTransaksi_NonSwiftIninstructedAmount.Text, True, oDecimal)
    '                        FillOrNothing(.TujuanTransaksi, Me.TxtTransaksi_NonSwiftInTujuanTransaksi.Text, True, Ovarchar)
    '                        FillOrNothing(.SumberPenggunaanDana, Me.TxtTransaksi_NonSwiftInSumberPenggunaanDana.Text, True, Ovarchar)
    '                        'saving
    '                    End With

    '                    DataRepository.IFTI_Approval_DetailProvider.Save(objIfti_ApprovalDetail)
    '                End Using
    '                ''Send Email
    '                'SendEmail("Ms Bentuk Badan Usaha Edit (User Id : " & SessionNIK & "-" & SessionGroupMenuName & ")", _
    '                '          "Ms Bentuk Badan Usaha", SessionIsBankWide, SessionFkGroupApprovalId, "", "MsBentukBidangUsaha_Approval_view.aspx")
    '                'ImgBtnSave.Visible = False
    '                'ImgBackAdd.Visible = False
    '                'LblConfirmation.Text = "Data has been edited and waiting for approval"
    '                'MtvMsUser.ActiveViewIndex = 1
    '            End Using
    '        End If
    '    Catch ex As Exception
    '        'LogError(ex)
    '        cvalPageErr.IsValid = False
    '        cvalPageErr.ErrorMessage = ex.Message
    '    End Try
    '    'End Sub
#End Region
    '#Region "SaveDirect"
    '    Sub SaveSwiftOutDirect()
    '        Try

    '            Using OTrans As TransactionManager = New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)
    '                Try
    '                    OTrans.BeginTransaction()
    '                    Page.Validate("handle")
    '                    If Page.IsValid Then
    '                        '============ Insert Detail Approval
    '                        Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)

    '                            Dim senderType As Integer = objIfti.Sender_FK_IFTI_NasabahType_ID
    '                            With objIfti
    '                                'FK
    '                                .PK_IFTI_ID = objIfti.PK_IFTI_ID
    '                                .FK_IFTI_Type_ID = 1

    '                                ValidasiControl()
    '                                'umum
    '                                   '--new--
    '                                FillOrNothing(.LTDLNNo, Me.TxtUmum_LTDLN.Text, False, oInt)
    '                                FillOrNothing(.LTDLNNoKoreksi, Me.TxtUmum_LTDLNKoreksi.Text, False, oInt)
    '                                FillOrNothing(.TanggalLaporan, Me.TxtUmum_PJKBankPelapor.Text, False, oDate)
    '                                FillOrNothing(.NamaPJKBankPelapor, Me.TxtUmum_PJKBankPelapor.Text, False, Ovarchar)
    '                                FillOrNothing(.NamaPejabatPJKBankPelapor, Me.TxtUmum_NamaPejabatPJKBankPelapor.Text, False, Ovarchar)
    '                                FillOrNothing(.JenisLaporan, Me.RbUmum_JenisLaporan.SelectedValue, False, oInt)
    '                                'cekPengirim
    '                                Select Case (senderType)
    '                                    Case 1
    '                                        'pengirimNasabahIndividu
    '                                        'cekvalidasi
    '                                        validasiSender1()

    '                                        FillOrNothing(.Sender_FK_IFTI_NasabahType_ID, senderType, False, oInt)

    '                                        FillOrNothing(.Sender_Nasabah_INDV_NoRekening, Me.TxtIdenPengirimNas_account.Text, False, Ovarchar)
    '                                        FillOrNothing(.Sender_Nasabah_INDV_NamaLengkap, Me.TxtIdenPengirimNas_Ind_NamaLengkap.Text, False, Ovarchar)
    '                                        FillOrNothing(.Sender_Nasabah_INDV_TanggalLahir, Me.TxtIdenPengirimNas_Ind_tanggalLahir.Text, False, oDate)
    '                                        FillOrNothing(.Sender_Nasabah_INDV_KewargaNegaraan, Me.Rb_IdenPengirimNas_Ind_kewarganegaraan.SelectedValue, False, oInt)
    '                                        If Me.RbPenerimaNasabah_IND_Warganegara.SelectedValue = "2" Then
    '                                            FillOrNothing(.Sender_Nasabah_INDV_Negara, Me.hfIdenPengirimNas_Ind_negara.Value, False, oInt)
    '                                            FillOrNothing(.Sender_Nasabah_INDV_NegaraLainnya, Me.TxtIdenPengirimNas_Ind_negaralain.Text, False, Ovarchar)
    '                                        End If
    '                                        FillOrNothing(.Sender_Nasabah_INDV_Pekerjaan, Me.hfIdenPengirimNas_Ind_pekerjaan.Value, False, oInt)
    '                                        FillOrNothing(.Sender_Nasabah_INDV_PekerjaanLainnya, Me.TxtIdenPengirimNas_Ind_Pekerjaanlain.Text, Ovarchar)
    '                                        FillOrNothing(.Sender_Nasabah_INDV_DOM_Alamat, Me.TxtIdenPengirimNas_Ind_Alamat.Text, False, Ovarchar)
    '                                        FillOrNothing(.Sender_Nasabah_INDV_DOM_KotaKab, Me.hfIdenPengirimNas_Ind_Kota_dom.Value, False, oInt)
    '                                        FillOrNothing(.Sender_Nasabah_INDV_DOM_KotaKabLainnya, Me.TxtIdenPengirimNas_Ind_KotaLainIden.Text, False, Ovarchar)
    '                                        FillOrNothing(.Sender_Nasabah_INDV_DOM_Propinsi, Me.hfIdenPengirimNas_Ind_provinsi_dom.Value, False, oInt)
    '                                        FillOrNothing(.Sender_Nasabah_INDV_DOM_PropinsiLainnya, Me.TxtIdenPengirimNas_Ind_provinsiLain.Text, False, Ovarchar)
    '                                        FillOrNothing(.Sender_Nasabah_INDV_ID_Alamat, Me.TxtIdenPengirimNas_Ind_alamatIdentitas.Text, False, Ovarchar)
    '                                        FillOrNothing(.Sender_Nasabah_INDV_ID_KotaKab, Me.hfIdenPengirimNas_Ind_kotaIdentitas.Value, False, oInt)
    '                                        FillOrNothing(.Sender_Nasabah_INDV_ID_KotaKabLainnya, Me.TxtIdenPengirimNas_Ind_kotalain.Text, False, Ovarchar)
    '                                        FillOrNothing(.Sender_Nasabah_INDV_ID_Propinsi, Me.hfIdenPengirimNas_Ind_ProvinsiIden.Value, False, oInt)
    '                                        FillOrNothing(.Sender_Nasabah_INDV_ID_PropinsiLainnya, Me.TxtIdenPengirimNas_Ind_ProvinsilainIden.Text, False, Ovarchar)
    '                                        FillOrNothing(.Sender_Nasabah_INDV_FK_IFTI_IDType, Me.cboPengirimNasInd_jenisidentitas.SelectedValue, False, oInt)
    '                                        FillOrNothing(.Sender_Nasabah_INDV_NomorID, Me.TxtIdenPengirimNas_Ind_noIdentitas.Text, False, Ovarchar)

    '                                    Case 2
    '                                        'pengirimNasabahKorporasi
    '                                        'cekvalidasi
    '                                        validasiSender2()
    '                                        FillOrNothing(.Sender_FK_IFTI_NasabahType_ID, senderType, False, oInt)

    '                                        FillOrNothing(.Sender_Nasabah_CORP_NoRekening, Me.TxtIdenPengirimNas_account.Text, False, Ovarchar)

    '                                        FillOrNothing(.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id, Me.CBOIdenPengirimNas_Corp_BentukBadanUsaha.SelectedValue, False, oInt)
    '                                        FillOrNothing(.Sender_Nasabah_CORP_BentukBadanUsahaLainnya, Me.TxtIdenPengirimNas_Corp_BentukBadanUsahaLain.Text, False, Ovarchar)
    '                                        FillOrNothing(.Sender_Nasabah_CORP_NamaKorporasi, Me.TxtIdenPengirimNas_Corp_NamaKorp.Text, False, Ovarchar)
    '                                        FillOrNothing(.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id, Me.hfIdenPengirimNas_Corp_BidangUsahaKorp.Value, False, oInt)
    '                                        FillOrNothing(.Sender_Nasabah_CORP_BidangUsahaLainnya, Me.TxtIdenPengirimNas_Corp_BidangUsahaKorpLainnya.Text, False, Ovarchar)
    '                                        FillOrNothing(.Sender_Nasabah_CORP_AlamatLengkap, Me.TxtIdenPengirimNas_Corp_alamatkorp.Text, False, Ovarchar)
    '                                        FillOrNothing(.Sender_Nasabah_CORP_KotaKab, Me.hfIdenPengirimNas_Corp_kotakorp.Value, False, oInt)
    '                                        FillOrNothing(.Sender_Nasabah_CORP_KotaKabLainnya, Me.TxtIdenPengirimNas_Ind_KotaLainIden.Text, False, Ovarchar)
    '                                        FillOrNothing(.Sender_Nasabah_CORP_Propinsi, Me.hfIdenPengirimNas_Corp_provKorp.Value, False, oInt)
    '                                        FillOrNothing(.Sender_Nasabah_CORP_PropinsiLainnya, Me.TxtIdenPengirimNas_Corp_provKorpLain.Text, False, Ovarchar)
    '                                        FillOrNothing(.Sender_Nasabah_CORP_LN_AlamatLengkap, Me.TxtIdenPengirimNas_Corp_AlamatLuar.Text, False, Ovarchar)
    '                                        FillOrNothing(.Sender_Nasabah_CORP_Alamat_KantorCabangPengirim, Me.TxtIdenPengirimNas_Corp_AlamatAsal.Text, False, Ovarchar)

    '                                    Case 3
    '                                        'PengirimNonNasabah
    '                                        'cekvalidasi
    '                                        validasiSender3()
    '                                        FillOrNothing(.Sender_FK_IFTI_NasabahType_ID, senderType, False, oInt)

    '                                        FillOrNothing(.FK_IFTI_NonNasabahNominalType_ID, Me.RbPengirimNonNasabah_100Juta.SelectedValue, False, oInt)
    '                                        FillOrNothing(.Sender_NonNasabah_NamaLengkap, Me.TxtPengirimNonNasabah_nama.Text, False, Ovarchar)
    '                                        FillOrNothing(.Sender_NonNasabah_TanggalLahir, Me.TxtPengirimNonNasabah_TanggalLahir.Text, False, oDate)
    '                                        FillOrNothing(.Sender_NonNasabah_ID_Alamat, Me.TxtPengirimNonNasabah_alamatiden.Text, False, Ovarchar)
    '                                        FillOrNothing(.Sender_NonNasabah_ID_KotaKab, Me.hfPengirimNonNasabah_kotaIden.Value, False, oInt)
    '                                        FillOrNothing(.Sender_NonNasabah_ID_KotaKabLainnya, Me.TxtPengirimNonNasabah_kotaIden.Text, False, Ovarchar)
    '                                        FillOrNothing(.Sender_NonNasabah_ID_Propinsi, Me.hfPengirimNonNasabah_ProvIden.Value, False, oInt)
    '                                        FillOrNothing(.Sender_NonNasabah_ID_PropinsiLainnya, Me.hfPengirimNonNasabah_ProvIden, False, oInt)
    '                                        FillOrNothing(.Sender_NonNasabah_FK_IFTI_IDType, Me.CboPengirimNonNasabah_JenisDokumen.SelectedValue, False, oInt)
    '                                        FillOrNothing(.Sender_NonNasabah_NomorID, Me.TxtPengirimNonNasabah_NomorIden.Text, False, Ovarchar)

    '                                    Case 4
    '                                        'PenererusIndividu
    '                                        'cekvalidasi
    '                                        validasiSender4()

    '                                        FillOrNothing(.Sender_FK_IFTI_NasabahType_ID, senderType, False, oInt)

    '                                        FillOrNothing(.Sender_Nasabah_INDV_NoRekening, Me.TxtPengirimPenerus_Rekening.Text, False, Ovarchar)
    '                                        FillOrNothing(.Sender_Nasabah_INDV_NamaLengkap, Me.TxtPengirimPenerus_Ind_nama.Text, False, Ovarchar)
    '                                        FillOrNothing(.Sender_Nasabah_INDV_TanggalLahir, Me.TxtPengirimPenerus_Ind_TanggalLahir.Text, False, oDate)
    '                                        FillOrNothing(.Sender_Nasabah_INDV_NamaBank, Me.TxtPengirimPenerus_Ind_namaBank.Text, False, Ovarchar)
    '                                        FillOrNothing(.Sender_Nasabah_INDV_KewargaNegaraan, Me.RBPengirimPenerus_Ind_Kewarganegaraan.SelectedValue, False, oInt)
    '                                        If Me.RbPenerimaNasabah_IND_Warganegara.SelectedValue = "2" Then
    '                                            FillOrNothing(.Sender_Nasabah_INDV_Negara, Me.hfIdenPengirimNas_Ind_negara.Value, False, oInt)
    '                                            FillOrNothing(.Sender_Nasabah_INDV_NegaraLainnya, Me.TxtPengirimPenerus_IND_negaralain.Text, False, Ovarchar)
    '                                        End If
    '                                        FillOrNothing(.Sender_Nasabah_INDV_Pekerjaan, Me.hfPengirimPenerus_IND_pekerjaan.Value, False, oInt)
    '                                        FillOrNothing(.Sender_Nasabah_INDV_PekerjaanLainnya, Me.TxtPengirimPenerus_IND_pekerjaanLain.Text, Ovarchar)
    '                                        FillOrNothing(.Sender_Nasabah_INDV_DOM_Alamat, Me.TxtPengirimPenerus_IND_alamatDom.Text, False, Ovarchar)
    '                                        FillOrNothing(.Sender_Nasabah_INDV_DOM_KotaKab, Me.hfIdenPengirimNas_Ind_Kota_dom.Value, False, oInt)
    '                                        FillOrNothing(.Sender_Nasabah_INDV_DOM_KotaKabLainnya, Me.TxtPengirimPenerus_IND_kotaLainDom, False, Ovarchar)
    '                                        FillOrNothing(.Sender_Nasabah_INDV_DOM_Propinsi, Me.hfIdenPengirimNas_Ind_Kota_dom.Value, False, oInt)
    '                                        FillOrNothing(.Sender_Nasabah_INDV_DOM_PropinsiLainnya, Me.TxtPengirimPenerus_IND_ProvLainIden.Text, False, Ovarchar)
    '                                        FillOrNothing(.Sender_Nasabah_INDV_ID_Alamat, Me.TxtPengirimPenerus_IND_alamatIden.Text, False, Ovarchar)
    '                                        FillOrNothing(.Sender_Nasabah_INDV_ID_KotaKab, Me.hfPengirimPenerus_IND_kotaIden.Value, False, oInt)
    '                                        FillOrNothing(.Sender_Nasabah_INDV_ID_KotaKabLainnya, Me.TxtPengirimPenerus_IND_KotaLainIden.Text, False, Ovarchar)
    '                                        FillOrNothing(.Sender_Nasabah_INDV_ID_Propinsi, Me.hfIdenPengirimNas_Ind_ProvinsiIden.Value, False, oInt)
    '                                        FillOrNothing(.Sender_Nasabah_INDV_ID_PropinsiLainnya, Me.TxtPengirimPenerus_IND_ProvLainIden.Text, False, Ovarchar)
    '                                        FillOrNothing(.Sender_Nasabah_INDV_FK_IFTI_IDType, Me.CBoPengirimPenerus_IND_jenisIdentitas.SelectedValue, False, oInt)
    '                                        FillOrNothing(.Sender_Nasabah_INDV_NomorID, Me.TxtPengirimPenerus_IND_nomoridentitas.Text, False, Ovarchar)

    '                                    Case 5
    '                                        'penerusKorporasi
    '                                        'cekValidasi
    '                                        validasiSender5()
    '                                        FillOrNothing(.Sender_FK_IFTI_NasabahType_ID, senderType, False, oInt)

    '                                        FillOrNothing(.Sender_Nasabah_CORP_NoRekening, Me.TxtPengirimPenerus_Rekening.Text, False, Ovarchar)

    '                                        FillOrNothing(.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id, Me.CBOPengirimPenerus_Korp_BentukBadanUsaha.SelectedValue, False, oInt)
    '                                        FillOrNothing(.Sender_Nasabah_CORP_BentukBadanUsahaLainnya, Me.txtPengirimPenerus_Korp_BentukBadanUsahaLain.Text, False, Ovarchar)
    '                                        FillOrNothing(.Sender_Nasabah_CORP_NamaBank, Me.txtPengirimPenerus_Korp_namabank.Text, False, Ovarchar)
    '                                        FillOrNothing(.Sender_Nasabah_CORP_NamaKorporasi, Me.txtPengirimPenerus_Korp_NamaKorp.Text, False, Ovarchar)
    '                                        FillOrNothing(.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id, Me.hfPengirimPenerus_Korp_BidangUsahaKorp.Value, False, oInt)
    '                                        FillOrNothing(.Sender_Nasabah_CORP_AlamatLengkap, Me.txtPengirimPenerus_Korp_alamatkorp.Text, False, Ovarchar)
    '                                        FillOrNothing(.Sender_Nasabah_CORP_KotaKab, Me.hfPengirimPenerus_IND_kotaDom.Value, False, oInt)
    '                                        FillOrNothing(.Sender_Nasabah_CORP_KotaKabLainnya, Me.txtPengirimPenerus_Korp_kotalainnyakorp.Text, False, Ovarchar)
    '                                        FillOrNothing(.Sender_Nasabah_CORP_Propinsi, Me.hfPengirimPenerus_IND_ProvDom.Value, False, oInt)
    '                                        FillOrNothing(.Sender_Nasabah_CORP_PropinsiLainnya, Me.TxtIdenPengirimNas_Corp_provKorpLain.Text, False, Ovarchar)
    '                                        FillOrNothing(.Sender_Nasabah_CORP_LN_AlamatLengkap, Me.txtPengirimPenerus_Korp_AlamatLuar.Text, False, Ovarchar)
    '                                        FillOrNothing(.Sender_Nasabah_CORP_Alamat_KantorCabangPengirim, Me.txtPengirimPenerus_Korp_AlamatAsal.Text, False, Ovarchar)

    '                                End Select

    '                                'cekBOwner
    '                                If Not IsNothing(objIfti.FK_IFTI_BeneficialOwnerType_ID) Then
    '                                    FillOrNothing(.BeneficialOwner_HubunganDenganPemilikDana, Me.BenfOwnerHubunganPemilikDana.Text, False, Ovarchar)
    '                                    Dim BOwnerID As Integer = objIfti.FK_IFTI_BeneficialOwnerType_ID
    '                                    Select Case (BOwnerID)
    '                                        Case 1
    '                                            'Nasabah
    '                                            FillOrNothing(.FK_IFTI_BeneficialOwnerType_ID, 1, False, oInt)

    '                                            FillOrNothing(.BeneficialOwner_NoRekening, Me.BenfOwnerNasabah_rekening.Text, False, Ovarchar)
    '                                            FillOrNothing(.BeneficialOwner_NamaLengkap, Me.BenfOwnerNasabah_Nama.Text, False, Ovarchar)
    '                                            FillOrNothing(.BeneficialOwner_TanggalLahir, Me.BenfOwnerNasabah_tanggalLahir, False, oDate)
    '                                            FillOrNothing(.BeneficialOwner_ID_Alamat, Me.BenfOwnerNasabah_AlamatIden.Text, False, Ovarchar)
    '                                            FillOrNothing(.BeneficialOwner_ID_KotaKab, Me.hfBenfOwnerNasabah_KotaIden.Value, False, oInt)
    '                                            FillOrNothing(.BeneficialOwner_ID_KotaKabLainnya, Me.BenfOwnerNasabah_kotaLainIden.Text, False, Ovarchar)
    '                                            FillOrNothing(.BeneficialOwner_ID_Propinsi, Me.BenfOwnerNasabah_ProvinsiIden.Text, False, oInt)
    '                                            FillOrNothing(.BeneficialOwner_ID_PropinsiLainnya, Me.BenfOwnerNasabah_ProvinsiLainIden.Text, False, Ovarchar)
    '                                            FillOrNothing(.BeneficialOwner_FK_IFTI_IDType, Me.CBOBenfOwnerNasabah_JenisDokumen.SelectedValue, False, oInt)
    '                                            FillOrNothing(.BeneficialOwner_NomorID, Me.BenfOwnerNasabah_NomorIden.Text, False, Ovarchar)

    '                                        Case 2
    '                                            'NonNasabah
    '                                            FillOrNothing(.FK_IFTI_BeneficialOwnerType_ID, 2, False, oInt)

    '                                            FillOrNothing(.BeneficialOwner_NamaLengkap, Me.BenfOwnerNonNasabah_Nama.Text, False, Ovarchar)
    '                                            FillOrNothing(.BeneficialOwner_TanggalLahir, Me.BenfOwnerNonNasabah_TanggalLahir, False, oDate)
    '                                            FillOrNothing(.BeneficialOwner_ID_Alamat, Me.BenfOwnerNonNasabah_AlamatIden.Text, False, Ovarchar)
    '                                            FillOrNothing(.BeneficialOwner_ID_KotaKab, Me.hfBenfOwnerNonNasabah_KotaIden.Value, False, oInt)
    '                                            FillOrNothing(.BeneficialOwner_ID_KotaKabLainnya, Me.BenfOwnerNonNasabah_KotaLainIden.Text, False, Ovarchar)
    '                                            FillOrNothing(.BeneficialOwner_ID_Propinsi, Me.BenfOwnerNonNasabah_ProvinsiIden.Text, False, oInt)
    '                                            FillOrNothing(.BeneficialOwner_ID_PropinsiLainnya, Me.BenfOwnerNonNasabah_ProvinsiLainIden.Text, False, Ovarchar)
    '                                            FillOrNothing(.BeneficialOwner_FK_IFTI_IDType, Me.CboPengirimNonNasabah_JenisDokumen.SelectedValue, False, oInt)
    '                                            FillOrNothing(.BeneficialOwner_NomorID, Me.BenfOwnerNonNasabah_NomorIdentitas.Text, False, Ovarchar)
    '                                    End Select

    '                                End If

    '                                'cekPenerima
    '                                Using objReceiver As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("FK_IFTI_ID=" & getIFTIPK, "", 0, Integer.MaxValue, 0)
    '                                    Dim KeyBeneficiary As Integer = objReceiver(0).PK_IFTI_Beneficiary_ID
    '                                    Dim TipePenerima As Integer = objReceiver(0).FK_IFTI_NasabahType_ID
    '                                    With objReceiver(0)
    '                                        Select Case (TipePenerima)
    '                                            Case 1
    '                                                validasiReceiverInd()
    '                                                .FK_IFTI_NasabahType_ID = 1
    '                                                .FK_IFTI_ID = objIfti.PK_IFTI_ID

    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_NoRekening, Me.txtPenerima_rekening.Text)

    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_NamaLengkap, Me.txtPenerimaNasabah_IND_nama.Text, False, Ovarchar)
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_TanggalLahir, Me.txtPenerimaNasabah_IND_TanggalLahir.Text, False, oDate)
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_KewargaNegaraan, Me.RbPenerimaNasabah_IND_Warganegara.SelectedValue, False, oInt)
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_Negara, Me.hfPenerimaNasabah_IND_negara.Value, False, oInt)
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_NegaraLainnya, Me.txtPenerimaNasabah_IND_negaraLain.Text, False, Ovarchar)
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Alamat_Iden, Me.txtPenerimaNasabah_IND_alamatIden.Text, False, Ovarchar)
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_ID_NegaraBagian, Me.txtPenerimaNasabah_IND_negaraBagian.Text, False, Ovarchar)
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Negara, Me.hfPenerimaNasabah_IND_negaraIden.Value, False, oInt)
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_ID_NegaraLainnya, Me.txtPenerimaNasabah_IND_negaraLainIden.Text, False, Ovarchar)
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_FK_IFTI_IDType, Me.CBOPenerimaNasabah_IND_JenisIden.SelectedValue, False, oInt)
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_NomorID, Me.txtPenerimaNasabah_IND_NomorIden.Text, False, Ovarchar)
    '                                                FillOrNothing(.Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan, Me.txtPenerimaNasabah_IND_NilaiTransaksiKeuangan.Text, False, oDecimal)
    '                                            Case 2
    '                                                validasiReceiverKorp()
    '                                                .FK_IFTI_NasabahType_ID = 2
    '                                                .FK_IFTI_ID = objIfti.PK_IFTI_ID
    '                                                FillOrNothing(.Beneficiary_Nasabah_CORP_NoRekening, Me.txtPenerima_rekening.Text)

    '                                                FillOrNothing(.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id, CBOPenerimaNasabah_Korp_BentukBadanUsahaLain.SelectedValue, False, oInt)
    '                                                FillOrNothing(.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya, Me.txtPenerimaNasabah_Korp_BentukBadanUsahaLainnya.Text, False, Ovarchar)
    '                                                FillOrNothing(.Beneficiary_Nasabah_CORP_NamaKorporasi, Me.txtPenerimaNasabah_Korp_namaKorp.Text, False, Ovarchar)
    '                                                FillOrNothing(.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id, Me.hfPenerimaNasabah_Korp_BidangUsahaKorp.Value, False, oInt)
    '                                                FillOrNothing(.Beneficiary_Nasabah_CORP_BidangUsahaLainnya, Me.txtPenerimaNasabah_Korp_BentukBadanUsahaLainnya.Text, False, Ovarchar)
    '                                                FillOrNothing(.Beneficiary_Nasabah_CORP_AlamatLengkap, Me.txtPenerimaNasabah_Korp_AlamatKorp.Text, TipePenerima, Ovarchar)
    '                                                FillOrNothing(.Beneficiary_Nasabah_CORP_ID_NegaraBagian, Me.txtPenerimaNasabah_Korp_negaraKota.Text, False, oInt)
    '                                                FillOrNothing(.Beneficiary_Nasabah_CORP_ID_Negara, Me.hfPenerimaNasabah_Korp_NegaraKorp.Value, False, oInt)
    '                                                FillOrNothing(.Beneficiary_Nasabah_CORP_ID_NegaraLainnya, Me.txtPenerimaNasabah_Korp_NegaraLainnyaKorp.Text, False, Ovarchar)
    '                                                FillOrNothing(.Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan, Me.txtPenerimaNasabah_Korp_NilaiTransaksiKeuangan.Text, False, oDecimal)
    '                                            Case 3
    '                                                validasiReceiverNonNasabah()
    '                                                .FK_IFTI_NasabahType_ID = 3
    '                                                .FK_IFTI_ID = objIfti.PK_IFTI_ID

    '                                                FillOrNothing(.Beneficiary_NonNasabah_NoRekening, Me.txtPenerima_rekening.Text)

    '                                                FillOrNothing(.Beneficiary_NonNasabah_NamaBank, Me.txtPenerimaNonNasabah_namabank.Text, False, Ovarchar)
    '                                                FillOrNothing(.Beneficiary_NonNasabah_NamaLengkap, Me.txtPenerimaNonNasabah_Nama.Text, False, Ovarchar)
    '                                                FillOrNothing(.Beneficiary_NonNasabah_ID_Alamat, Me.txtPenerimaNonNasabah_Alamat.Text, False, Ovarchar)
    '                                                FillOrNothing(.Beneficiary_NonNasabah_ID_Negara, Me.hfPenerimaNonNasabah_Negara.Value, False, oInt)
    '                                                FillOrNothing(.Beneficiary_NonNasabah_ID_NegaraLainnya, Me.txtPenerimaNonNasabah_negaraLain.Text, False, Ovarchar)
    '                                                FillOrNothing(.Beneficiary_NonNasabah_NilaiTransaksikeuangan, Me.txtPenerimaNonNasabah_nilaiTransaksiKeuangan.Text, False, oDecimal)
    '                                        End Select
    '                                    End With
    '                                    'save beneficiary
    '                                    DataRepository.IFTI_BeneficiaryProvider.Save(OTrans, objReceiver(0))
    '                                End Using

    '                                'cekTransaksi
    '                                validasiTransaksi()

    '                                '-----new---
    '                                FillOrNothing(.TanggalTransaksi, Me.Transaksi_tanggal.Text, False, oDate)
    '                                FillOrNothing(.TimeIndication, Me.Transaksi_waktutransaksi.Text, False, oDate)
    '                                FillOrNothing(.SenderReference, Me.Transaksi_Sender.Text, False, Ovarchar)
    '                                FillOrNothing(.BankOperationCode, Me.Transaksi_BankOperationCode.Text, False, Ovarchar)
    '                                FillOrNothing(.InstructionCode, Me.Transaksi_InstructionCode.Text, False, Ovarchar)
    '                                FillOrNothing(.KantorCabangPenyelengaraPengirimAsal, Me.Transaksi_kantorCabangPengirim.Text, False, Ovarchar)
    '                                FillOrNothing(.TransactionCode, Me.Transaksi_kodeTipeTransaksi.Text, False, Ovarchar)
    '                                FillOrNothing(.ValueDate_TanggalTransaksi, Me.Transaksi_ValueTanggalTransaksi.Text, False, oDate)
    '                                FillOrNothing(.ValueDate_NilaiTransaksi, Me.Transaksi_nilaitransaksi.Text, False, oDecimal)
    '                                Using objCurrency As TList(Of MsCurrency) = DataRepository.MsCurrencyProvider.GetPaged(MsCurrencyColumn.Code.ToString & _
    '                                                                              " like '%" & Me.hfTransaksi_MataUangTransaksi.Value.ToString & "%'", "", 0, Integer.MaxValue, 0)

    '                                    FillOrNothing(.ValueDate_FK_Currency_ID, objCurrency(0).Pk_MsCurrency_Id, False, oInt)
    '                                End Using
    '                                FillOrNothing(.ValueDate_NilaiTransaksiIDR, Me.Transaksi_AmountdalamRupiah.Text, False, oDecimal)
    '                                Using objCurrency As TList(Of MsCurrency) = DataRepository.MsCurrencyProvider.GetPaged(MsCurrencyColumn.Code.ToString & _
    '                                                                              " like '%" & Me.hfTransaksi_currency.Value.ToString & "%'", "", 0, Integer.MaxValue, 0)

    '                                    FillOrNothing(.Instructed_Currency, objCurrency(0).Pk_MsCurrency_Id, False, oInt)
    '                                End Using

    '                                FillOrNothing(.Instructed_Amount, Me.Transaksi_instructedAmount.Text, False, oDecimal)
    '                                FillOrNothing(.ExchangeRate, Me.Transaksi_nilaiTukar.Text, False, oDecimal)
    '                                FillOrNothing(.SendingInstitution, Me.Transaksi_sendingInstitution.Text, False, Ovarchar)
    '                                FillOrNothing(.TujuanTransaksi, Me.Transaksi_TujuanTransaksi.Text, False, Ovarchar)
    '                                FillOrNothing(.SumberPenggunaanDana, Me.Transaksi_SumberPenggunaanDana.Text, False, Ovarchar)

    '                                'cekInformasiLain

    '                                '---new-----
    '                                FillOrNothing(.InformationAbout_SenderCorrespondent, Me.InformasiLainnya_Sender.Text, False, Ovarchar)
    '                                FillOrNothing(.InformationAbout_ReceiverCorrespondent, Me.InformasiLainnya_receiver.Text, False, Ovarchar)
    '                                FillOrNothing(.InformationAbout_Thirdreimbursementinstitution, Me.InformasiLainnya_thirdReimbursement.Text, False, Ovarchar)
    '                                FillOrNothing(.InformationAbout_IntermediaryInstitution, Me.InformasiLainnya_intermediary.Text, False, Ovarchar)
    '                                FillOrNothing(.RemittanceInformation, Me.InformasiLainnya_Remittance.Text, False, Ovarchar)
    '                                FillOrNothing(.SendertoReceiverInformation, Me.InformasiLainnya_SenderToReceiver.Text, False, Ovarchar)
    '                                FillOrNothing(.RegulatoryReporting, Me.InformasiLainnya_RegulatoryReport.Text, False, Ovarchar)
    '                                FillOrNothing(.EnvelopeContents, Me.InformasiLainnya_EnvelopeContents.Text, False, Ovarchar)

    '                            End With

    '                            DataRepository.IFTIProvider.Save(OTrans, objIfti)
    '                        End Using

    '                        ImageButtonSave.Visible = False
    '                        ImageButtonCancel.Visible = False
    '                        lblMsg.Text = "Data has been edited"
    '                        MultiViewNasabahTypeAll.ActiveViewIndex = 1

    '                    End If
    '                    OTrans.Commit()
    '                Catch ex As Exception
    '                    OTrans.Rollback()
    '                    Throw
    '                End Try

    '            End Using

    '        Catch ex As Exception
    '            'LogError(ex)
    '            cvalPageErr.IsValid = False
    '            cvalPageErr.ErrorMessage = ex.Message
    '        End Try

    '    End Sub

    '#End Region
#Region "Function..."

    'Sub copyBeneficiary()
    '    Try
    '        Using OTrans As TransactionManager = New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)
    '            Try
    '                OTrans.BeginTransaction()
    '                Using objbeneficiary As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("FK_IFTI_ID=" & getIFTIPK, "", 0, Integer.MaxValue, 0)
    '                    If objbeneficiary.Count > 0 Then
    '                        Dim objBeneficiaryTemp1 As TList(Of IFTI_Beneficiary_Temporary) = DataRepository.IFTI_Beneficiary_TemporaryProvider.GetAll
    '                        DataRepository.IFTI_Beneficiary_TemporaryProvider.Delete(objBeneficiaryTemp1)
    '                        For Each objSingleBeneficiary As IFTI_Beneficiary In objbeneficiary
    '                            ' For i As Integer = 0 To objbeneficiary.Count - 1
    '                            Dim objBeneficiaryTemp As IFTI_Beneficiary_Temporary = New IFTI_Beneficiary_Temporary
    '                            With objBeneficiaryTemp
    '                                '.PK_IFTI_Beneficiary_Temporary_ID = i + 1
    '                                .FK_IFTI_ID = objSingleBeneficiary.FK_IFTI_ID
    '                                .FK_IFTI_NasabahType_ID = objSingleBeneficiary.FK_IFTI_NasabahType_ID
    '                                .Beneficiary_Nasabah_CORP_AlamatLengkap = objSingleBeneficiary.Beneficiary_Nasabah_CORP_AlamatLengkap
    '                                .Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya = objSingleBeneficiary.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya
    '                                .Beneficiary_Nasabah_CORP_BidangUsahaLainnya = objSingleBeneficiary.Beneficiary_Nasabah_CORP_BidangUsahaLainnya
    '                                .Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id = objSingleBeneficiary.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id
    '                                .Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id = objSingleBeneficiary.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id
    '                                .Beneficiary_Nasabah_CORP_ID_KotaKab = objSingleBeneficiary.Beneficiary_Nasabah_CORP_ID_KotaKab
    '                                .Beneficiary_Nasabah_CORP_ID_KotaKabLainnya = objSingleBeneficiary.Beneficiary_Nasabah_CORP_ID_KotaKabLainnya
    '                                .Beneficiary_Nasabah_CORP_ID_Negara = objSingleBeneficiary.Beneficiary_Nasabah_CORP_ID_Negara
    '                                .Beneficiary_Nasabah_CORP_ID_NegaraBagian = objSingleBeneficiary.Beneficiary_Nasabah_CORP_ID_NegaraBagian
    '                                .Beneficiary_Nasabah_CORP_ID_NegaraLainnya = objSingleBeneficiary.Beneficiary_Nasabah_CORP_ID_NegaraLainnya
    '                                .Beneficiary_Nasabah_CORP_ID_Propinsi = objSingleBeneficiary.Beneficiary_Nasabah_CORP_ID_Propinsi
    '                                .Beneficiary_Nasabah_CORP_ID_PropinsiLainnya = objSingleBeneficiary.Beneficiary_Nasabah_CORP_ID_PropinsiLainnya
    '                                .Beneficiary_Nasabah_CORP_NamaKorporasi = objSingleBeneficiary.Beneficiary_Nasabah_CORP_NamaKorporasi
    '                                .Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan = objSingleBeneficiary.Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan
    '                                .Beneficiary_Nasabah_CORP_NoRekening = objSingleBeneficiary.Beneficiary_Nasabah_CORP_NoRekening
    '                                .Beneficiary_Nasabah_CORP_NoTelp = objSingleBeneficiary.Beneficiary_Nasabah_CORP_NoTelp
    '                                .Beneficiary_Nasabah_INDV_FK_IFTI_IDType = objSingleBeneficiary.Beneficiary_Nasabah_INDV_FK_IFTI_IDType
    '                                .Beneficiary_Nasabah_INDV_ID_Alamat_Dom = objSingleBeneficiary.Beneficiary_Nasabah_INDV_ID_Alamat_Dom
    '                                .Beneficiary_Nasabah_INDV_ID_Alamat_Iden = objSingleBeneficiary.Beneficiary_Nasabah_INDV_ID_Alamat_Iden
    '                                .Beneficiary_Nasabah_INDV_ID_KotaKab_Dom = objSingleBeneficiary.Beneficiary_Nasabah_INDV_ID_KotaKab_Dom
    '                                .Beneficiary_Nasabah_INDV_ID_KotaKab_Iden = objSingleBeneficiary.Beneficiary_Nasabah_INDV_ID_KotaKab_Iden
    '                                .Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom = objSingleBeneficiary.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Dom
    '                                .Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden = objSingleBeneficiary.Beneficiary_Nasabah_INDV_ID_KotaKabLainnya_Iden
    '                                .Beneficiary_Nasabah_INDV_ID_Negara = objSingleBeneficiary.Beneficiary_Nasabah_INDV_ID_Negara
    '                                .Beneficiary_Nasabah_INDV_ID_NegaraBagian = objSingleBeneficiary.Beneficiary_Nasabah_INDV_ID_NegaraBagian
    '                                .Beneficiary_Nasabah_INDV_ID_NegaraLainnya = objSingleBeneficiary.Beneficiary_Nasabah_INDV_ID_NegaraLainnya
    '                                .Beneficiary_Nasabah_INDV_ID_Propinsi_Dom = objSingleBeneficiary.Beneficiary_Nasabah_INDV_ID_Propinsi_Dom
    '                                .Beneficiary_Nasabah_INDV_ID_Propinsi_Iden = objSingleBeneficiary.Beneficiary_Nasabah_INDV_ID_Propinsi_Dom
    '                                .Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom = objSingleBeneficiary.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Dom
    '                                .Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden = objSingleBeneficiary.Beneficiary_Nasabah_INDV_ID_PropinsiLainnya_Iden
    '                                .Beneficiary_Nasabah_INDV_KewargaNegaraan = objSingleBeneficiary.Beneficiary_Nasabah_INDV_KewargaNegaraan
    '                                .Beneficiary_Nasabah_INDV_NamaBank = objSingleBeneficiary.Beneficiary_Nasabah_INDV_NamaBank
    '                                .Beneficiary_Nasabah_INDV_NamaLengkap = objSingleBeneficiary.Beneficiary_Nasabah_INDV_NamaLengkap
    '                                .Beneficiary_Nasabah_INDV_Negara = objSingleBeneficiary.Beneficiary_Nasabah_INDV_Negara
    '                                .Beneficiary_Nasabah_INDV_NegaraLainnya = objSingleBeneficiary.Beneficiary_Nasabah_INDV_NegaraLainnya
    '                                .Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan = objSingleBeneficiary.Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan
    '                                .Beneficiary_Nasabah_INDV_NomorID = objSingleBeneficiary.Beneficiary_Nasabah_INDV_NomorID
    '                                .Beneficiary_Nasabah_INDV_NoRekening = objSingleBeneficiary.Beneficiary_Nasabah_INDV_NoRekening
    '                                .Beneficiary_Nasabah_INDV_NoTelp = objSingleBeneficiary.Beneficiary_Nasabah_INDV_NoTelp
    '                                .Beneficiary_Nasabah_INDV_Pekerjaan = objSingleBeneficiary.Beneficiary_Nasabah_INDV_Pekerjaan
    '                                .Beneficiary_Nasabah_INDV_PekerjaanLainnya = objSingleBeneficiary.Beneficiary_Nasabah_INDV_PekerjaanLainnya
    '                                .Beneficiary_Nasabah_INDV_TanggalLahir = objSingleBeneficiary.Beneficiary_Nasabah_INDV_TanggalLahir
    '                                .Beneficiary_NonNasabah_FK_IFTI_IDType = objSingleBeneficiary.Beneficiary_NonNasabah_FK_IFTI_IDType
    '                                .Beneficiary_NonNasabah_ID_Alamat = objSingleBeneficiary.Beneficiary_NonNasabah_ID_Alamat
    '                                .Beneficiary_NonNasabah_ID_Negara = objSingleBeneficiary.Beneficiary_NonNasabah_ID_Negara
    '                                .Beneficiary_NonNasabah_ID_NegaraBagian = objSingleBeneficiary.Beneficiary_NonNasabah_ID_NegaraBagian
    '                                .Beneficiary_NonNasabah_ID_NegaraLainnya = objSingleBeneficiary.Beneficiary_NonNasabah_ID_NegaraLainnya
    '                                .Beneficiary_NonNasabah_KodeRahasia = objSingleBeneficiary.Beneficiary_NonNasabah_KodeRahasia
    '                                .Beneficiary_NonNasabah_NamaBank = objSingleBeneficiary.Beneficiary_NonNasabah_NamaBank
    '                                .Beneficiary_NonNasabah_NamaLengkap = objSingleBeneficiary.Beneficiary_NonNasabah_NamaLengkap
    '                                .Beneficiary_NonNasabah_NilaiTransaksikeuangan = objSingleBeneficiary.Beneficiary_NonNasabah_NilaiTransaksikeuangan
    '                                .Beneficiary_NonNasabah_NomorID = objSingleBeneficiary.Beneficiary_NonNasabah_NomorID
    '                                .Beneficiary_NonNasabah_NoRekening = objSingleBeneficiary.Beneficiary_NonNasabah_NoRekening
    '                                .Beneficiary_NonNasabah_NoTelp = objSingleBeneficiary.Beneficiary_NonNasabah_NoTelp
    '                                .Beneficiary_NonNasabah_TanggalLahir = objSingleBeneficiary.Beneficiary_NonNasabah_NoTelp
    '                            End With
    '                            'DataRepository.IFTI_Beneficiary_TemporaryProvider.Delete(objBeneficiaryTemp)
    '                            DataRepository.IFTI_Beneficiary_TemporaryProvider.Save(objBeneficiaryTemp)
    '                            'objbeneficiarytemporary.add()
    '                        Next
    '                        'DataRepository.IFTI_Beneficiary_TemporaryProvider.Save(objBeneficiaryTemp)
    '                    End If

    '                End Using

    '                OTrans.Commit()
    '            Catch ex As Exception
    '                OTrans.Rollback()
    '                Throw
    '            End Try

    '        End Using

    '    Catch ex As Exception
    '        'LogError(ex)
    '        cvalPageErr.IsValid = False
    '        cvalPageErr.ErrorMessage = ex.Message
    '    End Try
    'End Sub
    'Private Sub bindgrid()
    '    'ValidateBLL.ButtonAccess2(GridDataView, "IFTI_Approval_view.aspx")

    '    Dim objBeneficiary As TList(Of IFTI_Beneficiary_Temporary) = DataRepository.IFTI_Beneficiary_TemporaryProvider.GetAll 'GetPaged("FK_IFTI_ID=" & getIFTIPK, "", 0, Integer.MaxValue, 0)
    '    SetnGetRowTotal = objBeneficiary.Count
    '    If SetnGetCurrentPage > GetPageTotal - 1 And GetPageTotal - 1 > 0 Then
    '        SetnGetCurrentPage = GetPageTotal - 1
    '    End If
    '    GridDataView.DataSource = objBeneficiary
    '    GridDataView.CurrentPageIndex = SetnGetCurrentPage
    '    GridDataView.VirtualItemCount = SetnGetRowTotal
    '    GridDataView.DataBind()
    '    'If SetnGetRowTotal > 0 Then
    '    '    LabelNoRecordFound.Visible = False
    '    'Else
    '    '    LabelNoRecordFound.Visible = True
    '    'End If
    'End Sub
    Private Sub SetCOntrolLoad()
        'bind JenisID
        Using objJenisId As TList(Of IFTI_IDType) = DataRepository.IFTI_IDTypeProvider.GetAll
            If objJenisId.Count > 0 Then
                Me.cboPengirimNasInd_jenisidentitas.Items.Clear()
                cboPengirimNasInd_jenisidentitas.Items.Add("-Select-")
                ' cek(lagi)
                Me.CBoPengirimPenerus_IND_jenisIdentitas.Items.Clear()
                CBoPengirimPenerus_IND_jenisIdentitas.Items.Add("-Select-")

                Me.CBOBenfOwnerNasabah_JenisDokumen.Items.Clear()
                CBOBenfOwnerNasabah_JenisDokumen.Items.Add("-Select-")

                Me.CboPengirimNonNasabah_JenisDokumen.Items.Clear()
                CboPengirimNonNasabah_JenisDokumen.Items.Add("-Select-")

                Me.CBOBenfOwnerNonNasabah_JenisDokumen.Items.Clear()
                Me.CBOBenfOwnerNonNasabah_JenisDokumen.Items.Add("-Select-")

                'cek lagi
                Me.CBOPenerimaNasabah_IND_JenisIden.Items.Clear()
                Me.CBOPenerimaNasabah_IND_JenisIden.Items.Add("-Select-")

                'cek lagi
                For i As Integer = 0 To objJenisId.Count - 1
                    cboPengirimNasInd_jenisidentitas.Items.Add(New ListItem(objJenisId(i).IDType, objJenisId(i).PK_IFTI_IDType.ToString))
                    CBoPengirimPenerus_IND_jenisIdentitas.Items.Add(New ListItem(objJenisId(i).IDType, objJenisId(i).PK_IFTI_IDType.ToString))
                    CBOBenfOwnerNasabah_JenisDokumen.Items.Add(New ListItem(objJenisId(i).IDType, objJenisId(i).PK_IFTI_IDType.ToString))
                    CboPengirimNonNasabah_JenisDokumen.Items.Add(New ListItem(objJenisId(i).IDType, objJenisId(i).PK_IFTI_IDType.ToString))
                    CBOPenerimaNasabah_IND_JenisIden.Items.Add(New ListItem(objJenisId(i).IDType, objJenisId(i).PK_IFTI_IDType.ToString))
                    CBOBenfOwnerNonNasabah_JenisDokumen.Items.Add(New ListItem(objJenisId(i).IDType, objJenisId(i).PK_IFTI_IDType.ToString))
                Next
            End If

            'bindgrid popUpCalender
            Me.popUpTransactionDateBegin.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TxtUmum_TanggalLaporan.ClientID & "'), 'dd-mmm-yyyy')")
            Me.popUpTransactionDateBegin.Style.Add("display", "")
            Me.popUpTanggalLahirPengirimNasabah_Ind.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TxtIdenPengirimNas_Ind_tanggalLahir.ClientID & "'), 'dd-mmm-yyyy')")
            Me.popUpTanggalLahirPengirimNasabah_Ind.Style.Add("display", "")
            Me.popUpTanggalLahirPengirimNonNasabah.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TxtPengirimNonNasabah_TanggalLahir.ClientID & "'), 'dd-mmm-yyyy')")
            Me.popUpTanggalLahirPengirimNonNasabah.Style.Add("display", "")
            Me.popUpTangglaLahirPengirimPenerus_Ind.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TxtPengirimPenerus_Ind_TanggalLahir.ClientID & "'), 'dd-mmm-yyyy')")
            Me.popUpTangglaLahirPengirimPenerus_Ind.Style.Add("display", "")
            Me.popUpTanggalLahirBOwnerNasabah.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.BenfOwnerNasabah_tanggalLahir.ClientID & "'), 'dd-mmm-yyyy')")
            Me.popUpTanggalLahirBOwnerNasabah.Style.Add("display", "")
            Me.popUpTanggalLahirBownerNonNasabah.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.BenfOwnerNonNasabah_TanggalLahir.ClientID & "'), 'dd-mmm-yyyy')")
            Me.popUpTanggalLahirBownerNonNasabah.Style.Add("display", "")
            Me.popUpTanggalLahirPenerimaNasabah_Ind.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.txtPenerimaNasabah_IND_TanggalLahir.ClientID & "'), 'dd-mmm-yyyy')")
            Me.popUpTanggalLahirPenerimaNasabah_Ind.Style.Add("display", "")
            Me.popUpTanggalTransaksi_Transaksi.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.Transaksi_tanggal.ClientID & "'), 'dd-mmm-yyyy')")
            Me.popUpTanggalTransaksi_Transaksi.Style.Add("display", "")
            Me.popUpTanggalTransaksi_ValueDate.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.Transaksi_ValueTanggalTransaksi.ClientID & "'), 'dd-mmm-yyyy')")
            Me.popUpTanggalTransaksi_ValueDate.Style.Add("display", "")

        End Using
        'Bind MsBentukBadanUsaha
        Using objBentukBadanUsaha As TList(Of MsBentukBidangUsaha) = DataRepository.MsBentukBidangUsahaProvider.GetAll
            If objBentukBadanUsaha.Count > 0 Then
                CBOIdenPengirimNas_Corp_BentukBadanUsaha.Items.Clear()
                CBOIdenPengirimNas_Corp_BentukBadanUsaha.Items.Add("-Select-")

                CBOPengirimPenerus_Korp_BentukBadanUsaha.Items.Clear()
                CBOPengirimPenerus_Korp_BentukBadanUsaha.Items.Add("-Select-")

                CBOPenerimaNasabah_Korp_BentukBadanUsahaLain.Items.Clear()
                CBOPenerimaNasabah_Korp_BentukBadanUsahaLain.Items.Add("-Select-")

                For i As Integer = 0 To objBentukBadanUsaha.Count - 1
                    CBOIdenPengirimNas_Corp_BentukBadanUsaha.Items.Add(New ListItem(objBentukBadanUsaha(i).BentukBidangUsaha, objBentukBadanUsaha(i).IdBentukBidangUsaha.ToString))
                    CBOPengirimPenerus_Korp_BentukBadanUsaha.Items.Add(New ListItem(objBentukBadanUsaha(i).BentukBidangUsaha, objBentukBadanUsaha(i).IdBentukBidangUsaha.ToString))
                    CBOPenerimaNasabah_Korp_BentukBadanUsahaLain.Items.Add(New ListItem(objBentukBadanUsaha(i).BentukBidangUsaha, objBentukBadanUsaha(i).IdBentukBidangUsaha.ToString))
                Next
            End If
        End Using

    End Sub
    Sub clearSession()
        Session("IFTIEdit.IFTIPK") = Nothing
        Session("IFTIEdit.IFTIBeneficiaryTempPK") = Nothing
        ReceiverDataTable = Nothing
    End Sub

#Region "SwiftOut..."
#Region "SwiftOutSender..."
#Region "SwiftOutSenderType..."
    Sub FieldSwiftOutSenderCase1()
        'PengirimNasabahIndividu

        Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetPaged(MsKotaKabColumn.Activation.ToString & "=1", MsKotaKabColumn.NamaKotaKab.ToString, 0, Integer.MaxValue, 0)
        Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
        Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetPaged(MsProvinceColumn.activation.ToString & "=1", MsProvinceColumn.Nama.ToString, 0, Integer.MaxValue, 0)
        Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi
        Dim objNegara As TList(Of MsNegara) = DataRepository.MsNegaraProvider.GetPaged(MsNegaraColumn.Activation.ToString & "=1", MsNegaraColumn.NamaNegara.ToString, 0, Integer.MaxValue, 0)
        Dim objSnegara As MsNegara 'Penampung hasil search negara yang pke picker
        Dim objPekerjaan As TList(Of MsPekerjaan) = DataRepository.MsPekerjaanProvider.GetPaged(MsPekerjaanColumn.Activation.ToString & "=1", MsPekerjaanColumn.NamaPekerjaan.ToString, 0, Integer.MaxValue, 0)
        Dim objSPekerjaan As MsPekerjaan 'Penampung hasil search pekerjaan
        'parameter value

        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
        Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
        Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)


        Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)
            SetnGetSenderAccount = objIfti.Sender_Nasabah_INDV_NoRekening
            Me.TxtIdenPengirimNas_account.Text = SetnGetSenderAccount

            Me.TxtIdenPengirimNas_Ind_NamaLengkap.Text = objIfti.Sender_Nasabah_INDV_NamaLengkap

            TxtIdenPengirimNas_Ind_tanggalLahir.Text = FormatDate(objIfti.Sender_Nasabah_INDV_TanggalLahir)
            If Not IsNothing(objIfti.Sender_Nasabah_INDV_KewargaNegaraan) Then
                Me.Rb_IdenPengirimNas_Ind_kewarganegaraan.SelectedValue = objIfti.Sender_Nasabah_INDV_KewargaNegaraan
            End If
            If Not IsNothing(objIfti.Sender_Nasabah_INDV_KewargaNegaraan) Then
                If objIfti.Sender_Nasabah_INDV_KewargaNegaraan.ToString = "2" Then
                    Me.PengirimIndNegara.Visible = True
                    'Me.PengirimIndNegaraLain.Visible = True
                    objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, objIfti.Sender_Nasabah_INDV_Negara)
                    If Not IsNothing(objSnegara) Then
                        TxtIdenPengirimNas_Ind_negara.Text = Safe(objSnegara.NamaNegara)
                        hfIdenPengirimNas_Ind_negara.Value = Safe(objSnegara.IDNegara)
                        If hfIdenPengirimNas_Ind_negara.Value = ParameterNegara.MsSystemParameter_Value Then
                            Me.TxtIdenPengirimNas_Ind_negaralain.Text = objIfti.Sender_Nasabah_INDV_NegaraLainnya
                            Me.PengirimIndNegaraLain.Visible = True
                        End If
                    End If
                    'Me.TxtIdenPengirimNas_Ind_negara.Text = objIfti.Sender_Nasabah_INDV_Negara

                End If

            End If
            objSPekerjaan = objPekerjaan.Find(MsPekerjaanColumn.IDPekerjaan, objIfti.Sender_Nasabah_INDV_Pekerjaan)
            If Not IsNothing(objSPekerjaan) Then
                TxtIdenPengirimNas_Ind_pekerjaan.Text = Safe(objSPekerjaan.NamaPekerjaan)
                hfIdenPengirimNas_Ind_pekerjaan.Value = Safe(objSPekerjaan.IDPekerjaan)
                If hfIdenPengirimNas_Ind_pekerjaan.Value = ParameterPekerjaan.MsSystemParameter_Value Then
                    Me.TxtIdenPengirimNas_Ind_Pekerjaanlain.Text = objIfti.Sender_Nasabah_INDV_PekerjaanLainnya
                    Me.TrIdenPengirimNas_Ind_Pekerjaanlain.Visible = True
                End If
            End If


            Me.TxtIdenPengirimNas_Ind_Alamat.Text = objIfti.Sender_Nasabah_INDV_DOM_Alamat
            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objIfti.Sender_Nasabah_INDV_DOM_KotaKab)
            If Not IsNothing(objSkotaKab) Then
                Me.TxtIdenPengirimNas_Ind_Kota.Text = Safe(objSkotaKab.NamaKotaKab)
                hfIdenPengirimNas_Ind_Kota_dom.Value = Safe(objSkotaKab.IDKotaKab)

                If hfIdenPengirimNas_Ind_Kota_dom.Value = ParameterKota.MsSystemParameter_Value Then
                    Me.TxtIdenPengirimNas_Ind_kotalain.Text = objIfti.Sender_Nasabah_INDV_DOM_KotaKabLainnya
                    Me.TrIdenPengirimNas_Ind_kotalain.Visible = True
                End If
            End If


            objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(SafeNumIndex(objIfti.Sender_Nasabah_INDV_DOM_Propinsi))
            If Not IsNothing(objSProvinsi) Then
                TxtIdenPengirimNas_Ind_provinsi.Text = Safe(objSProvinsi.Nama)
                hfIdenPengirimNas_Ind_provinsi_dom.Value = Safe(objSProvinsi.IdProvince)

                If hfIdenPengirimNas_Ind_provinsi_dom.Value = ParameterProvinsi.MsSystemParameter_Value Then
                    Me.TxtIdenPengirimNas_Ind_provinsiLain.Text = objIfti.Sender_Nasabah_INDV_DOM_PropinsiLainnya
                    Me.TrIdenPengirimNas_Ind_provinsiLain.Visible = True
                End If

            End If

            Me.TxtIdenPengirimNas_Ind_alamatIdentitas.Text = objIfti.Sender_Nasabah_INDV_ID_Alamat

            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objIfti.Sender_Nasabah_INDV_ID_KotaKab)
            If Not IsNothing(objSkotaKab) Then
                Me.TxtIdenPengirimNas_Ind_kotaIdentitas.Text = Safe(objSkotaKab.NamaKotaKab)
                hfIdenPengirimNas_Ind_kotaIdentitas.Value = Safe(objSkotaKab.IDKotaKab)

                If hfIdenPengirimNas_Ind_kotaIdentitas.Value = ParameterKota.MsSystemParameter_Value Then
                    Me.TxtIdenPengirimNas_Ind_KotaLainIden.Text = objIfti.Sender_Nasabah_INDV_ID_KotaKabLainnya
                    Me.TrIdenPengirimNas_Ind_KotaLainIden.Visible = True
                End If
            End If


            objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(SafeNumIndex(objIfti.Sender_Nasabah_INDV_ID_Propinsi))
            If Not IsNothing(objSProvinsi) Then
                Me.TxtIdenPengirimNas_Ind_ProvinsiIden.Text = Safe(objSProvinsi.Nama)
                hfIdenPengirimNas_Ind_ProvinsiIden.Value = Safe(objSProvinsi.IdProvince)

                If hfIdenPengirimNas_Ind_ProvinsiIden.Value = ParameterProvinsi.MsSystemParameter_Value Then
                    Me.TxtIdenPengirimNas_Ind_ProvinsilainIden.Text = objIfti.Sender_Nasabah_INDV_ID_PropinsiLainnya
                    Me.TrIdenPengirimNas_Ind_ProvinsilainIden.Visible = True
                End If
            End If


            Me.cboPengirimNasInd_jenisidentitas.SelectedValue = objIfti.Sender_Nasabah_INDV_FK_IFTI_IDType
            Me.TxtIdenPengirimNas_Ind_noIdentitas.Text = objIfti.Sender_Nasabah_INDV_NomorID
        End Using



    End Sub
    Sub FieldSwiftOutSenderCase2()
        'PengirimNasabahKorporasi

        Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetPaged(MsKotaKabColumn.Activation.ToString & "=1", MsKotaKabColumn.NamaKotaKab.ToString, 0, Integer.MaxValue, 0)
        Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
        Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetPaged(MsProvinceColumn.activation.ToString & "=1", MsProvinceColumn.Nama.ToString, 0, Integer.MaxValue, 0)
        Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi

        'parameter value

        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
        Dim ParameterBidangUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BidangUsaha)

        Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)
            SetnGetSenderAccount = objIfti.Sender_Nasabah_CORP_NoRekening
            Me.TxtIdenPengirimNas_account.Text = SetnGetSenderAccount
            If ObjectAntiNull(objIfti.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id) = True Then
                Using objBentukBadanUsaha As MsBentukBidangUsaha = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(objIfti.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault(0))
                    If Not IsNothing(objBentukBadanUsaha) Then
                        Me.CBOIdenPengirimNas_Corp_BentukBadanUsaha.SelectedValue = objBentukBadanUsaha.IdBentukBidangUsaha
                    End If

                End Using
            Else
                Me.CBOIdenPengirimNas_Corp_BentukBadanUsaha.SelectedIndex = 0
            End If

            Me.TxtIdenPengirimNas_Corp_BentukBadanUsahaLain.Text = objIfti.Sender_Nasabah_CORP_BentukBadanUsahaLainnya
            Me.TxtIdenPengirimNas_Corp_NamaKorp.Text = objIfti.Sender_Nasabah_CORP_NamaKorporasi
            Using objBidangUsaha As MsBidangUsaha = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(objIfti.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id.GetValueOrDefault(0))
                If Not IsNothing(objBidangUsaha) Then
                    Me.TxtIdenPengirimNas_Corp_BidangUsahaKorp.Text = objBidangUsaha.NamaBidangUsaha
                    hfIdenPengirimNas_Corp_BidangUsahaKorp.Value = objBidangUsaha.IdBidangUsaha
                    If hfIdenPengirimNas_Corp_BidangUsahaKorp.Value = ParameterBidangUsaha.MsSystemParameter_Value Then
                        Me.TxtIdenPengirimNas_Corp_BidangUsahaKorpLainnya.Text = objIfti.Sender_Nasabah_CORP_BidangUsahaLainnya
                        Me.TrIdenPengirimNas_Corp_BidangUsahaKorpLainnya.Visible = True
                    End If
                End If


            End Using


            Me.TxtIdenPengirimNas_Corp_alamatkorp.Text = objIfti.Sender_Nasabah_CORP_AlamatLengkap
            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objIfti.Sender_Nasabah_CORP_KotaKab)
            If Not IsNothing(objSkotaKab) Then
                Me.TxtIdenPengirimNas_Corp_kotakorp.Text = Safe(objSkotaKab.NamaKotaKab)
                hfIdenPengirimNas_Corp_kotakorp.Value = Safe(objSkotaKab.IDKotaKab)

                If hfIdenPengirimNas_Corp_kotakorp.Value = ParameterKota.MsSystemParameter_Value Then
                    Me.TxtIdenPengirimNas_Corp_kotakorplain.Text = objIfti.Sender_Nasabah_CORP_KotaKabLainnya
                    Me.TrIdenPengirimNas_Corp_kotakorplain.Visible = True
                End If
            End If


            objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(SafeNumIndex(objIfti.Sender_Nasabah_CORP_Propinsi))
            If Not IsNothing(objSProvinsi) Then
                Me.TxtIdenPengirimNas_Corp_provKorp.Text = Safe(objSProvinsi.Nama)
                hfIdenPengirimNas_Corp_provKorp.Value = Safe(objSProvinsi.IdProvince)
                If hfIdenPengirimNas_Corp_provKorp.Value = ParameterProvinsi.MsSystemParameter_Value Then
                    Me.TxtIdenPengirimNas_Corp_provKorpLain.Text = objIfti.Sender_Nasabah_CORP_PropinsiLainnya
                    Me.TrIdenPengirimNas_Corp_provKorpLain.Visible = True
                End If
            End If


            Me.TxtIdenPengirimNas_Corp_AlamatAsal.Text = objIfti.Sender_Nasabah_CORP_Alamat_KantorCabangPengirim
            Me.TxtIdenPengirimNas_Corp_AlamatLuar.Text = objIfti.Sender_Nasabah_CORP_LN_AlamatLengkap
        End Using
    End Sub
    Sub FieldSwiftOutSenderCase3()
        'PengirimNonNasabah

        Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetPaged(MsKotaKabColumn.Activation.ToString & "=1", MsKotaKabColumn.NamaKotaKab.ToString, 0, Integer.MaxValue, 0)
        Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
        Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetPaged(MsProvinceColumn.activation.ToString & "=1", MsProvinceColumn.Nama.ToString, 0, Integer.MaxValue, 0)
        Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi
        'parameter value

        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)


        Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)
            If ObjectAntiNull(objIfti.FK_IFTI_NonNasabahNominalType_ID) = True Then
                Me.RbPengirimNonNasabah_100Juta.SelectedValue = objIfti.FK_IFTI_NonNasabahNominalType_ID
            Else
                Me.RbPengirimNonNasabah_100Juta.SelectedIndex = 0
            End If
            'Me.RbPengirimNonNasabah_100Juta.SelectedValue = objIfti.FK_IFTI_NonNasabahNominalType_ID
            If RbPengirimNonNasabah_100Juta.SelectedValue = 2 Then
                jenisdokumen_nonNasabah_mand.Visible = True
                Me.NomorIdentitas_nonNasabah_mand.Visible = True
            Else
                jenisdokumen_nonNasabah_mand.Visible = False
                Me.NomorIdentitas_nonNasabah_mand.Visible = False
            End If
            Me.TxtPengirimNonNasabah_nama.Text = objIfti.Sender_NonNasabah_NamaLengkap
            Me.TxtPengirimNonNasabah_TanggalLahir.Text = FormatDate(objIfti.Sender_NonNasabah_TanggalLahir)
            Me.TxtPengirimNonNasabah_alamatiden.Text = objIfti.Sender_NonNasabah_ID_Alamat
            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objIfti.Sender_NonNasabah_ID_KotaKab)
            If Not IsNothing(objSkotaKab) Then
                Me.TxtPengirimNonNasabah_kotaIden.Text = Safe(objSkotaKab.NamaKotaKab)
                hfPengirimNonNasabah_kotaIden.Value = Safe(objSkotaKab.IDKotaKab)

                If hfPengirimNonNasabah_kotaIden.Value = ParameterKota.MsSystemParameter_Value Then
                    Me.TxtPengirimNonNasabah_KoaLainIden.Text = objIfti.Sender_NonNasabah_ID_KotaKabLainnya
                    Me.TrPengirimNonNasabah_KoaLainIden.Visible = True
                End If

            End If

            objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(SafeNumIndex(objIfti.Sender_NonNasabah_ID_Propinsi))
            If Not IsNothing(objSProvinsi) Then
                Me.TxtPengirimNonNasabah_ProvIden.Text = Safe(objSProvinsi.Nama)
                hfPengirimNonNasabah_ProvIden.Value = Safe(objSProvinsi.IdProvince)

                If hfPengirimNonNasabah_ProvIden.Value = ParameterProvinsi.MsSystemParameter_Value Then
                    Me.TxtPengirimNonNasabah_ProvLainIden.Text = objIfti.Sender_NonNasabah_ID_PropinsiLainnya
                    Me.TrPengirimNonNasabah_ProvLainIden.Visible = True
                End If
            End If

            Me.CboPengirimNonNasabah_JenisDokumen.SelectedValue = objIfti.Sender_NonNasabah_FK_IFTI_IDType.GetValueOrDefault(0)


            Me.TxtPengirimNonNasabah_NomorIden.Text = objIfti.Sender_NonNasabah_NomorID
        End Using
    End Sub
    Sub FieldSwiftOutSenderCase4()
        'PenyelenggaraPenerusIndividu

        'Ambil data buat cari PK Negara, Provinsi, KotaKab, dkk (ini variable yang bakal dpke berkali2)
        Dim i As Integer = 0 'buat iterasi
        Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetPaged(MsKotaKabColumn.Activation.ToString & "=1", MsKotaKabColumn.NamaKotaKab.ToString, 0, Integer.MaxValue, 0)
        Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
        Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetPaged(MsProvinceColumn.activation.ToString & "=1", MsProvinceColumn.Nama.ToString, 0, Integer.MaxValue, 0)
        Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi
        Dim objPekerjaan As TList(Of MsPekerjaan) = DataRepository.MsPekerjaanProvider.GetPaged(MsPekerjaanColumn.Activation.ToString & "=1", MsPekerjaanColumn.NamaPekerjaan.ToString, 0, Integer.MaxValue, 0)
        Dim objSPekerjaan As MsPekerjaan 'Penampung hasil search pekerjaan
        Dim objNegara As TList(Of MsNegara) = DataRepository.MsNegaraProvider.GetPaged(MsNegaraColumn.Activation.ToString & "=1", MsNegaraColumn.NamaNegara.ToString, 0, Integer.MaxValue, 0)
        Dim objSnegara As MsNegara 'Penampung hasil search negara yang pke picker
        'parameter value

        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
        Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
        Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)


        Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)
            SetnGetSenderAccount = objIfti.Sender_Nasabah_INDV_NoRekening
            Me.TxtPengirimPenerus_Rekening.Text = SetnGetSenderAccount
            Me.TxtPengirimPenerus_Ind_namaBank.Text = objIfti.Sender_Nasabah_INDV_NamaBank
            Me.TxtPengirimPenerus_Ind_nama.Text = objIfti.Sender_Nasabah_INDV_NamaLengkap
            If Not IsNothing(objIfti.Sender_Nasabah_INDV_TanggalLahir) Then
                Me.TxtPengirimPenerus_Ind_TanggalLahir.Text = objIfti.Sender_Nasabah_INDV_TanggalLahir
            End If
            If Not IsNothing(objIfti.Sender_Nasabah_INDV_KewargaNegaraan) Then
                Me.RBPengirimPenerus_Ind_Kewarganegaraan.SelectedValue = objIfti.Sender_Nasabah_INDV_KewargaNegaraan
                If objIfti.Sender_Nasabah_INDV_KewargaNegaraan = "2" Then
                    Me.PengirimPenerusIndNegara.Visible = True
                    'Me.PengirimPenerusIndNegaraLain.Visible = True
                    objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, objIfti.Sender_Nasabah_INDV_Negara)
                    If Not IsNothing(objSnegara) Then
                        TxtPengirimPenerus_IND_negara.Text = Safe(objSnegara.NamaNegara)
                        hfPengirimPenerus_IND_negara.Value = Safe(objSnegara.IDNegara)

                        If hfPengirimPenerus_IND_negara.Value = ParameterNegara.MsSystemParameter_Value Then
                            Me.TxtPengirimPenerus_IND_negaralain.Text = objIfti.Sender_Nasabah_INDV_NegaraLainnya
                            Me.PengirimPenerusIndNegaraLain.Visible = True
                        End If
                    End If
                    'Me.TxtPengirimPenerus_IND_negara.Text = objIfti.Sender_Nasabah_INDV_Negara


                End If
            End If
            objSPekerjaan = objPekerjaan.Find(MsPekerjaanColumn.IDPekerjaan, objIfti.Sender_Nasabah_INDV_Pekerjaan)
            If Not IsNothing(objSPekerjaan) Then
                TxtPengirimPenerus_IND_pekerjaan.Text = Safe(objSPekerjaan.NamaPekerjaan)
                hfPengirimPenerus_IND_pekerjaan.Value = Safe(objSPekerjaan.IDPekerjaan)

                If hfPengirimPenerus_IND_pekerjaan.Value = ParameterPekerjaan.MsSystemParameter_Value Then
                    Me.TxtPengirimPenerus_IND_pekerjaanLain.Text = objIfti.Sender_Nasabah_INDV_PekerjaanLainnya
                    Me.TrPengirimPenerus_IND_pekerjaanLain.Visible = True
                End If
            End If

            Me.TxtPengirimPenerus_IND_alamatDom.Text = objIfti.Sender_Nasabah_INDV_DOM_Alamat
            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objIfti.Sender_Nasabah_INDV_DOM_KotaKab)
            If Not IsNothing(objSkotaKab) Then
                TxtPengirimPenerus_IND_kotaDom.Text = Safe(objSkotaKab.NamaKotaKab)
                hfPengirimPenerus_IND_kotaDom.Value = Safe(objSkotaKab.IDKotaKab)

                If hfPengirimPenerus_IND_kotaDom.Value = ParameterKota.MsSystemParameter_Value Then
                    Me.TxtPengirimPenerus_IND_kotaLainDom.Text = objIfti.Sender_Nasabah_INDV_DOM_KotaKabLainnya
                    Me.TrPengirimPenerus_IND_kotaLainDom.Visible = True
                End If
            End If


            objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(SafeNumIndex(objIfti.Sender_Nasabah_INDV_DOM_Propinsi))
            If Not IsNothing(objSProvinsi) Then
                TxtPengirimPenerus_IND_ProvDom.Text = Safe(objSProvinsi.Nama)
                hfPengirimPenerus_IND_ProvDom.Value = Safe(objSProvinsi.IdProvince)
                If hfPengirimPenerus_IND_ProvDom.Value = ParameterProvinsi.MsSystemParameter_Value Then
                    Me.TxtPengirimPenerus_IND_ProvLainDom.Text = objIfti.Sender_Nasabah_INDV_DOM_PropinsiLainnya
                    Me.TrPengirimPenerus_IND_ProvLainDom.Visible = True
                End If
            End If


            Me.TxtPengirimPenerus_IND_alamatIden.Text = objIfti.Sender_Nasabah_INDV_ID_Alamat
            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objIfti.Sender_Nasabah_INDV_ID_KotaKab)
            If Not IsNothing(objSkotaKab) Then
                Me.TxtPengirimPenerus_IND_kotaIden.Text = objIfti.Sender_Nasabah_INDV_ID_KotaKab
                hfPengirimPenerus_IND_kotaIden.Value = Safe(objSkotaKab.IDKotaKab)

                If hfPengirimPenerus_IND_kotaIden.Value = ParameterKota.MsSystemParameter_Value Then
                    Me.TxtPengirimPenerus_IND_KotaLainIden.Text = objIfti.Sender_Nasabah_INDV_ID_KotaKabLainnya
                    Me.TrPengirimPenerus_IND_KotaLainIden.Visible = True
                End If

            End If

            objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(SafeNumIndex(objIfti.Sender_Nasabah_INDV_ID_Propinsi))
            If Not IsNothing(objSProvinsi) Then
                Me.TxtPengirimPenerus_IND_ProvIden.Text = Safe(objSProvinsi.Nama)
                hfPengirimPenerus_IND_ProvIden.Value = Safe(objSProvinsi.IdProvince)

                If hfPengirimPenerus_IND_ProvIden.Value = ParameterProvinsi.MsSystemParameter_Value Then
                    Me.TxtPengirimPenerus_IND_ProvLainIden.Text = objIfti.Sender_Nasabah_INDV_ID_PropinsiLainnya
                    Me.TrPengirimPenerus_IND_ProvLainIden.Visible = True
                End If
            End If

            If ObjectAntiNull(objIfti.Sender_Nasabah_INDV_FK_IFTI_IDType) = True Then
                Me.CBoPengirimPenerus_IND_jenisIdentitas.SelectedValue = objIfti.Sender_Nasabah_INDV_FK_IFTI_IDType
            Else
                Me.CBoPengirimPenerus_IND_jenisIdentitas.SelectedIndex = 0
            End If

            Me.TxtPengirimPenerus_IND_nomoridentitas.Text = objIfti.Sender_Nasabah_INDV_NomorID
        End Using

    End Sub
    Sub FieldSwiftOutSenderCase5()
        'PenyelenggaraPenerusKorporasi

        Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetPaged(MsKotaKabColumn.Activation.ToString & "=1", MsKotaKabColumn.NamaKotaKab.ToString, 0, Integer.MaxValue, 0)
        Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
        Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetPaged(MsProvinceColumn.activation.ToString & "=1", MsProvinceColumn.Nama.ToString, 0, Integer.MaxValue, 0)
        Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi
        'parameter value

        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
        Dim ParameterBidangUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BidangUsaha)

        Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)
            Me.TxtPengirimPenerus_Rekening.Text = objIfti.Sender_Nasabah_CORP_NoRekening
            Me.txtPengirimPenerus_Korp_namabank.Text = objIfti.Sender_Nasabah_CORP_NamaBank
            Using objBentukBadanUsaha As MsBentukBidangUsaha = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(objIfti.Sender_Nasabah_CORP_FK_MsBentukBadanUsaha_Id.GetValueOrDefault(0))
                If Not IsNothing(objBentukBadanUsaha) Then
                    Me.CBOPengirimPenerus_Korp_BentukBadanUsaha.SelectedValue = objBentukBadanUsaha.IdBentukBidangUsaha
                End If

            End Using
            Me.txtPengirimPenerus_Korp_BentukBadanUsahaLain.Text = objIfti.Sender_Nasabah_CORP_BentukBadanUsahaLainnya
            Me.txtPengirimPenerus_Korp_NamaKorp.Text = objIfti.Sender_Nasabah_CORP_NamaKorporasi
            Using objBidangUsaha As MsBidangUsaha = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(objIfti.Sender_Nasabah_CORP_FK_MsBidangUsaha_Id.GetValueOrDefault(0))
                If Not IsNothing(objBidangUsaha) Then
                    Me.txtPengirimPenerus_Korp_BidangUsahaKorp.Text = objBidangUsaha.NamaBidangUsaha
                    Me.hfPengirimPenerus_Korp_BidangUsahaKorp.Value = objBidangUsaha.IdBidangUsaha

                    If Me.hfPengirimPenerus_Korp_BidangUsahaKorp.Value = ParameterBidangUsaha.MsSystemParameter_Value Then
                        Me.txtPengirimPenerus_Korp_BidangUsahaKorplain.Text = objIfti.Sender_Nasabah_CORP_BidangUsahaLainnya
                        TrPengirimPenerus_Korp_BidangUsahaKorplain.Visible = True
                    End If
                End If

            End Using
            Me.txtPengirimPenerus_Korp_alamatkorp.Text = objIfti.Sender_Nasabah_CORP_AlamatLengkap
            objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objIfti.Sender_Nasabah_CORP_KotaKab)
            If Not IsNothing(objSkotaKab) Then
                Me.txtPengirimPenerus_Korp_kotakorp.Text = Safe(objSkotaKab.NamaKotaKab)
                hfPengirimPenerus_Korp_kotakorp.Value = Safe(objSkotaKab.IDKotaKab)

                If hfPengirimPenerus_Korp_kotakorp.Value = ParameterKota.MsSystemParameter_Value Then
                    Me.txtPengirimPenerus_Korp_kotalainnyakorp.Text = objIfti.Sender_Nasabah_CORP_KotaKabLainnya
                    Me.TrtxtPengirimPenerus_Korp_kotalainnyakorp.Visible = True
                End If
            End If


            objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(SafeNumIndex(objIfti.Sender_Nasabah_CORP_Propinsi))
            If Not IsNothing(objSProvinsi) Then
                Me.txtPengirimPenerus_Korp_ProvinsiKorp.Text = Safe(objSProvinsi.Nama)
                hfPengirimPenerus_Korp_ProvinsiKorp.Value = Safe(objSProvinsi.IdProvince)

                If hfPengirimPenerus_Korp_ProvinsiKorp.Value = ParameterProvinsi.MsSystemParameter_Value Then
                    Me.txtPengirimPenerus_Korp_provinsiLainnyaKorp.Text = objIfti.Sender_Nasabah_CORP_PropinsiLainnya
                    Me.TrPengirimPenerus_Korp_provinsiLainnyaKorp.Visible = True
                End If
            End If


            Me.txtPengirimPenerus_Korp_AlamatAsal.Text = objIfti.Sender_Nasabah_CORP_Alamat_KantorCabangPengirim
            Me.txtPengirimPenerus_Korp_AlamatLuar.Text = objIfti.Sender_Nasabah_CORP_LN_AlamatLengkap
        End Using
    End Sub
#End Region

#End Region
#Region "SwiftOutReceiver..."
    Sub FieldSwiftOutPenerimaNasabahPerorangan(ByVal PK_IFTI_Beneficiary_ID As Integer)
        'PenerimaNasabahPerorangan

        Dim objNegara As TList(Of MsNegara) = DataRepository.MsNegaraProvider.GetPaged(MsNegaraColumn.Activation.ToString & "=1", MsNegaraColumn.NamaNegara.ToString, 0, Integer.MaxValue, 0)
        Dim objSnegara As MsNegara 'Penampung hasil search negara yang pke picker

        'parameter value

        Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)

        'Using objIfti As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("PK_IFTI_Beneficiary_ID=" & PK_IFTI_Beneficiary_ID, "", 0, 1, 0)
        '    Me.txtPenerima_rekening.Text = objIfti(0).Beneficiary_Nasabah_INDV_NoRekening
        '    Me.txtPenerimaNasabah_IND_nama.Text = objIfti(0).Beneficiary_Nasabah_INDV_NamaLengkap
        '    Me.txtPenerimaNasabah_IND_TanggalLahir.Text = FormatDate(objIfti(0).Beneficiary_Nasabah_INDV_TanggalLahir)
        '    If Not IsNothing(objIfti(0).Beneficiary_Nasabah_INDV_KewargaNegaraan) Then
        '        Me.RbPenerimaNasabah_IND_Warganegara.SelectedValue = objIfti(0).Beneficiary_Nasabah_INDV_KewargaNegaraan
        '        If objIfti(0).Beneficiary_Nasabah_INDV_KewargaNegaraan = "2" Then
        '            Me.PenerimaIndNegara.Visible = True
        '            Me.PenerimaIndNegaraLain.Visible = True
        '            objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, objIfti(0).Beneficiary_Nasabah_INDV_Negara)
        '            If Not IsNothing(objSnegara) Then
        '                txtPenerimaNasabah_IND_negara.Text = Safe(objSnegara.NamaNegara)
        '                hfPenerimaNasabah_IND_negara.Value = Safe(objSnegara.IDNegara)
        '            End If
        '            Me.txtPenerimaNasabah_IND_negaraLain.Text = objIfti(0).Beneficiary_Nasabah_INDV_NegaraLainnya
        '        End If
        '    End If

        '    Me.txtPenerimaNasabah_IND_alamatIden.Text = objIfti(0).Beneficiary_Nasabah_INDV_ID_Alamat_Iden
        '    Me.txtPenerimaNasabah_IND_negaraBagian.Text = objIfti(0).Beneficiary_Nasabah_INDV_ID_NegaraBagian
        '    objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, objIfti(0).Beneficiary_Nasabah_INDV_ID_Negara)
        '    If Not IsNothing(objSnegara) Then
        '        txtPenerimaNasabah_IND_negaraIden.Text = Safe(objSnegara.NamaNegara)
        '        hfPenerimaNasabah_IND_negaraIden.Value = Safe(objSnegara.IDNegara)
        '    End If
        '    Me.txtPenerimaNasabah_IND_negaraLainIden.Text = objIfti(0).Beneficiary_Nasabah_INDV_ID_NegaraLainnya
        '    Me.CBOPenerimaNasabah_IND_JenisIden.SelectedValue = objIfti(0).Beneficiary_Nasabah_INDV_FK_IFTI_IDType
        '    Me.txtPenerimaNasabah_IND_NomorIden.Text = objIfti(0).Beneficiary_Nasabah_INDV_NomorID
        '    Me.txtPenerimaNasabah_IND_NilaiTransaksiKeuangan.Text = objIfti(0).Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan
        'End Using
        Dim dr As Data.DataRow = ReceiverDataTable.Rows(CInt(getIFTIBeneficiaryTempPK - 1))
        'Using objIfti As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("PK_IFTI_Beneficiary_ID=" & PK_IFTI_Beneficiary_ID, "", 0, 1, 0)
        dr("FK_IFTI_NasabahType_ID") = 1
        Me.txtPenerima_rekening.Text = dr("Beneficiary_Nasabah_INDV_NoRekening").ToString
        Me.txtPenerimaNasabah_IND_nama.Text = dr("Beneficiary_Nasabah_INDV_NamaLengkap").ToString
        Me.txtPenerimaNasabah_IND_TanggalLahir.Text = FormatDate(dr("Beneficiary_Nasabah_INDV_TanggalLahir"))
        If Not IsNothing(dr("Beneficiary_Nasabah_INDV_KewargaNegaraan")) Then
            Me.RbPenerimaNasabah_IND_Warganegara.SelectedValue = CInt(dr("Beneficiary_Nasabah_INDV_KewargaNegaraan"))
            If dr("Beneficiary_Nasabah_INDV_KewargaNegaraan") = "2" Then
                Me.PenerimaIndNegara.Visible = True
                ' Me.PenerimaIndNegaraLain.Visible = True
                objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, (dr("Beneficiary_Nasabah_INDV_Negara")))
                If Not IsNothing(objSnegara) Then
                    txtPenerimaNasabah_IND_negara.Text = Safe(objSnegara.NamaNegara)
                    hfPenerimaNasabah_IND_negara.Value = Safe(objSnegara.IDNegara)

                    If hfPenerimaNasabah_IND_negara.Value = ParameterNegara.MsSystemParameter_Value Then
                        Me.txtPenerimaNasabah_IND_negaraLain.Text = dr("Beneficiary_Nasabah_INDV_NegaraLainnya").ToString
                        Me.PenerimaIndNegaraLain.Visible = True
                    End If
                End If

            End If
        End If

        Me.txtPenerimaNasabah_IND_alamatIden.Text = dr("Beneficiary_Nasabah_INDV_ID_Alamat_Iden").ToString
        Me.txtPenerimaNasabah_IND_negaraBagian.Text = dr("Beneficiary_Nasabah_INDV_ID_NegaraBagian").ToString
        objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, (dr("Beneficiary_Nasabah_INDV_ID_Negara")))
        If Not IsNothing(objSnegara) Then
            txtPenerimaNasabah_IND_negaraIden.Text = Safe(objSnegara.NamaNegara)
            hfPenerimaNasabah_IND_negaraIden.Value = Safe(objSnegara.IDNegara)

            If hfPenerimaNasabah_IND_negaraIden.Value = ParameterNegara.MsSystemParameter_Value Then
                Me.txtPenerimaNasabah_IND_negaraLainIden.Text = dr("Beneficiary_Nasabah_INDV_ID_NegaraLainnya").ToString
                Me.txtPenerimaNasabah_IND_negaraLainIden.Visible = True
            End If
        End If


        If ObjectAntiNull(dr("Beneficiary_Nasabah_INDV_FK_IFTI_IDType")) = True Then
            Me.CBOPenerimaNasabah_IND_JenisIden.SelectedValue = CInt(dr("Beneficiary_Nasabah_INDV_FK_IFTI_IDType"))
        Else
            Me.CBOPenerimaNasabah_IND_JenisIden.SelectedIndex = 0
        End If

        Me.txtPenerimaNasabah_IND_NomorIden.Text = dr("Beneficiary_Nasabah_INDV_NomorID").ToString
        Me.txtPenerimaNasabah_IND_NilaiTransaksiKeuangan.Text = dr("Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan").ToString
        'End Using
    End Sub
    Sub FieldSwiftOutPenerimaNasabahKorporasi(ByVal PK_IFTI_Beneficiary_ID As Integer)
        'PenerimaNasabahKorporasi
        Dim objNegara As TList(Of MsNegara) = DataRepository.MsNegaraProvider.GetPaged(MsNegaraColumn.Activation.ToString & "=1", MsNegaraColumn.NamaNegara.ToString, 0, Integer.MaxValue, 0)
        Dim objSnegara As MsNegara 'Penampung hasil search negara yang pke picker
        'parameter value

        Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
        Dim ParameterBidangUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BidangUsaha)
        'Using objIfti As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("PK_IFTI_Beneficiary_ID=" & PK_IFTI_Beneficiary_ID, "", 0, 1, 0)
        '    Me.txtPenerima_rekening.Text = objIfti(0).Beneficiary_Nasabah_CORP_NoRekening
        '    Using objBadanUsaha As MsBentukBidangUsaha = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(objIfti(0).Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id)
        '        If Not IsNothing(objBadanUsaha) Then
        '            Me.CBOPenerimaNasabah_Korp_BentukBadanUsahaLain.SelectedValue = objBadanUsaha.BentukBidangUsaha
        '        End If
        '    End Using
        '    Me.txtPenerimaNasabah_Korp_BentukBadanUsahaLainnya.Text = objIfti(0).Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya
        '    Me.txtPenerimaNasabah_Korp_namaKorp.Text = objIfti(0).Beneficiary_Nasabah_CORP_NamaKorporasi
        '    Using objBidangUsaha As MsBidangUsaha = DataRepository.MsBidangUsahaProvider.GetByBidangUsahaId(objIfti(0).Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id)
        '        If Not IsNothing(objBidangUsaha) Then
        '            Me.txtPenerimaNasabah_Korp_BidangUsahaKorp.Text = objBidangUsaha.NamaBidangUsaha
        '            hfPenerimaNasabah_Korp_BidangUsahaKorp.Value = objBidangUsaha.BidangUsahaId
        '        End If
        '    End Using
        '    Me.txtPenerimaNasabah_Korp_AlamatKorp.Text = objIfti(0).Beneficiary_Nasabah_CORP_AlamatLengkap
        '    Me.txtPenerimaNasabah_Korp_negaraKota.Text = objIfti(0).Beneficiary_Nasabah_CORP_ID_NegaraBagian
        '    objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, objIfti(0).Beneficiary_Nasabah_CORP_ID_Negara)
        '    If Not IsNothing(objSnegara) Then
        '        txtPenerimaNasabah_Korp_NegaraKorp.Text = Safe(objSnegara.NamaNegara)
        '        hfPenerimaNasabah_Korp_NegaraKorp.Value = Safe(objSnegara.IDNegara)
        '    End If
        '    Me.txtPenerimaNasabah_Korp_NegaraLainnyaKorp.Text = objIfti(0).Beneficiary_Nasabah_CORP_ID_NegaraLainnya
        '    Me.txtPenerimaNasabah_Korp_NilaiTransaksiKeuangan.Text = objIfti(0).Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan
        'End Using
        Dim dr As Data.DataRow = ReceiverDataTable.Rows(PK_IFTI_Beneficiary_ID - 1)

        Me.txtPenerima_rekening.Text = dr("Beneficiary_Nasabah_CORP_NoRekening").ToString
        If ObjectAntiNull(dr("Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id")) = True Then
            Using objBadanUsaha As MsBentukBidangUsaha = DataRepository.MsBentukBidangUsahaProvider.GetByIdBentukBidangUsaha(dr("Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id"))
                If Not IsNothing(objBadanUsaha) Then
                    Me.CBOPenerimaNasabah_Korp_BentukBadanUsahaLain.SelectedValue = objBadanUsaha.IdBentukBidangUsaha
                End If
            End Using
        Else
            Me.CBOPenerimaNasabah_Korp_BentukBadanUsahaLain.SelectedIndex = 0
        End If

        Me.txtPenerimaNasabah_Korp_BentukBadanUsahaLainnya.Text = dr("Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya").ToString
        Me.txtPenerimaNasabah_Korp_namaKorp.Text = dr("Beneficiary_Nasabah_CORP_NamaKorporasi").ToString
        If ObjectAntiNull(dr("Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id")) = True Then
            Using objBidangUsaha As MsBidangUsaha = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(dr("Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id"))
                If Not IsNothing(objBidangUsaha) Then
                    Me.txtPenerimaNasabah_Korp_BidangUsahaKorp.Text = objBidangUsaha.NamaBidangUsaha
                    hfPenerimaNasabah_Korp_BidangUsahaKorp.Value = objBidangUsaha.IdBidangUsaha

                    If hfPenerimaNasabah_Korp_BidangUsahaKorp.Value = ParameterBidangUsaha.MsSystemParameter_Value Then
                        Me.txtPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Text = dr("Beneficiary_Nasabah_CORP_BidangUsahaLainnya").ToString
                        Me.TrPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Visible = True
                    End If
                End If
            End Using
        End If


        Me.txtPenerimaNasabah_Korp_AlamatKorp.Text = dr("Beneficiary_Nasabah_CORP_AlamatLengkap").ToString
        Me.txtPenerimaNasabah_Korp_negaraKota.Text = dr("Beneficiary_Nasabah_CORP_ID_NegaraBagian").ToString
        objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, dr("Beneficiary_Nasabah_CORP_ID_Negara"))
        If Not IsNothing(objSnegara) Then
            txtPenerimaNasabah_Korp_NegaraKorp.Text = Safe(objSnegara.NamaNegara)
            hfPenerimaNasabah_Korp_NegaraKorp.Value = Safe(objSnegara.IDNegara)

            If hfPenerimaNasabah_Korp_NegaraKorp.Value = ParameterNegara.MsSystemParameter_Value Then
                Me.txtPenerimaNasabah_Korp_NegaraLainnyaKorp.Text = dr("Beneficiary_Nasabah_CORP_ID_NegaraLainnya").ToString
                Me.TrPenerimaNasabah_Korp_NegaraLainnyaKorp.Visible = True
            End If
        End If


        Me.txtPenerimaNasabah_Korp_NilaiTransaksiKeuangan.Text = dr("Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan").ToString

    End Sub
    Sub FieldSwiftOutPenerimaNonNasabah(ByVal PK_IFTI_Beneficiary_ID As Integer)
        'PenerimaNonNasabah

        Dim objNegara As TList(Of MsNegara) = DataRepository.MsNegaraProvider.GetAll
        Dim objSnegara As MsNegara 'Penampung hasil search negara yang pke picker
        'parameter value

        Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)

        'Using objIfti As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("PK_IFTI_Beneficiary_ID=" & PK_IFTI_Beneficiary_ID, "", 0, 1, 0)

        '    Dim dr As Data.DataRow = ReceiverDataTable.Rows(getIFTIBeneficiaryTempPK - 1)
        '    getIFTIBeneficiaryTempPK = (e.Item.ItemIndex + (SetnGetCurrentPageReceiver * SahassaCommonly.Commonly.GetDisplayedTotalRow)).ToString

        '    Me.txtPenerima_rekening.Text = objIfti(0).Beneficiary_NonNasabah_NoRekening
        '    Me.txtPenerimaNonNasabah_namabank.Text = objIfti(0).Beneficiary_NonNasabah_NamaBank
        '    Me.txtPenerimaNonNasabah_Nama.Text = objIfti(0).Beneficiary_NonNasabah_NamaLengkap
        '    Me.txtPenerimaNonNasabah_Alamat.Text = objIfti(0).Beneficiary_NonNasabah_ID_Alamat
        '    objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, objIfti(0).Beneficiary_NonNasabah_ID_Negara)
        '    If Not IsNothing(objSnegara) Then
        '        txtPenerimaNonNasabah_Negara.Text = Safe(objSnegara.NamaNegara)
        '        hfPenerimaNonNasabah_Negara.Value = Safe(objSnegara.IDNegara)
        '    End If
        '    Me.txtPenerimaNonNasabah_negaraLain.Text = objIfti(0).Beneficiary_Nasabah_CORP_ID_NegaraLainnya
        '    If Not IsNothing(objIfti(0).Beneficiary_NonNasabah_NilaiTransaksikeuangan) Then
        '        Me.txtPenerimaNonNasabah_nilaiTransaksiKeuangan.Text = objIfti(0).Beneficiary_NonNasabah_NilaiTransaksikeuangan
        '    End If
        'End Using


        Dim dr As Data.DataRow = ReceiverDataTable.Rows(PK_IFTI_Beneficiary_ID - 1)

        Me.txtPenerima_rekening.Text = dr("Beneficiary_NonNasabah_NoRekening").ToString
        Me.txtPenerimaNonNasabah_namabank.Text = dr("Beneficiary_NonNasabah_NamaBank").ToString
        Me.txtPenerimaNonNasabah_Nama.Text = dr("Beneficiary_NonNasabah_NamaLengkap").ToString
        Me.txtPenerimaNonNasabah_Alamat.Text = dr("Beneficiary_NonNasabah_ID_Alamat").ToString
        objSnegara = objNegara.Find(MsNegaraColumn.IDNegara, dr("Beneficiary_NonNasabah_ID_Negara"))
        If Not IsNothing(objSnegara) Then
            txtPenerimaNonNasabah_Negara.Text = Safe(objSnegara.NamaNegara)
            hfPenerimaNonNasabah_Negara.Value = Safe(objSnegara.IDNegara)

            If hfPenerimaNonNasabah_Negara.Value = ParameterNegara.MsSystemParameter_Value Then
                Me.txtPenerimaNonNasabah_negaraLain.Text = dr("Beneficiary_NonNasabah_ID_NegaraLainnya").ToString
                Me.TrPenerimaNonNasabah_negaraLain.Visible = True
            End If
        End If


        If Not IsNothing(dr("Beneficiary_NonNasabah_NilaiTransaksikeuangan")) Then
            Me.txtPenerimaNonNasabah_nilaiTransaksiKeuangan.Text = dr("Beneficiary_NonNasabah_NilaiTransaksikeuangan").ToString
        End If


    End Sub
#End Region
#Region "SwiftOutBOwner..."
    Sub FieldSwiftOutBOwnerNasabah()
        'BeneficialOwnerNasabah
        Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetPaged(MsKotaKabColumn.Activation.ToString & "=1", MsKotaKabColumn.NamaKotaKab.ToString, 0, Integer.MaxValue, 0)
        Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
        Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetPaged(MsProvinceColumn.activation.ToString & "=1", MsProvinceColumn.Nama.ToString, 0, Integer.MaxValue, 0)
        Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi
        'parameter value

        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)

        Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)
            SafeDefaultValue = ""
            If Not IsNothing(objIfti) Then
                Me.BenfOwnerNasabah_rekening.Text = objIfti.BeneficialOwner_NoRekening
                Me.BenfOwnerNasabah_Nama.Text = objIfti.BeneficialOwner_NamaLengkap
                Me.BenfOwnerNasabah_tanggalLahir.Text = FormatDate(objIfti.BeneficialOwner_TanggalLahir)
                Me.BenfOwnerNasabah_AlamatIden.Text = objIfti.BeneficialOwner_ID_Alamat
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objIfti.BeneficialOwner_ID_KotaKab)
                If Not IsNothing(objSkotaKab) Then
                    Me.BenfOwnerNasabah_KotaIden.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfBenfOwnerNasabah_KotaIden.Value = Safe(objSkotaKab.IDKotaKab)

                    If hfBenfOwnerNasabah_KotaIden.Value = ParameterKota.MsSystemParameter_Value Then
                        Me.BenfOwnerNasabah_kotaLainIden.Text = objIfti.BeneficialOwner_ID_KotaKabLainnya
                        Me.TrBenfOwnerNasabah_kotaLainIden.Visible = True
                    End If
                End If


                objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(SafeNumIndex(objIfti.BeneficialOwner_ID_Propinsi))
                If Not IsNothing(objSProvinsi) Then
                    Me.BenfOwnerNasabah_ProvinsiIden.Text = Safe(objSProvinsi.Nama)
                    hfBenfOwnerNasabah_ProvinsiIden.Value = Safe(objSProvinsi.IdProvince)

                    If hfBenfOwnerNasabah_ProvinsiIden.Value = ParameterProvinsi.MsSystemParameter_Value Then
                        Me.BenfOwnerNasabah_ProvinsiLainIden.Text = objIfti.BeneficialOwner_ID_PropinsiLainnya
                        Me.TrBenfOwnerNasabah_ProvinsiLainIden.Visible = True
                    End If
                End If


                If Not IsNothing(objIfti.BeneficialOwner_FK_IFTI_IDType) Then
                    Me.CBOBenfOwnerNasabah_JenisDokumen.SelectedValue = objIfti.BeneficialOwner_FK_IFTI_IDType
                Else
                    Me.CBOBenfOwnerNasabah_JenisDokumen.SelectedIndex = 0
                End If

                Me.BenfOwnerNasabah_NomorIden.Text = objIfti.BeneficialOwner_NomorID
            End If
        End Using


    End Sub
    Sub FieldSwiftOutBOwnerNonNasabah()
        'BeneficialOwnerNonNasabah
        Dim objKotaKab As TList(Of MsKotaKab) = DataRepository.MsKotaKabProvider.GetPaged(MsKotaKabColumn.Activation.ToString & "=1", MsKotaKabColumn.NamaKotaKab.ToString, 0, Integer.MaxValue, 0)
        Dim objSkotaKab As MsKotaKab 'Penampung hasil search kotakab nanti
        Dim objProvinsi As TList(Of MsProvince) = DataRepository.MsProvinceProvider.GetAll
        Dim objSProvinsi As MsProvince 'Penampung hasil search provinsi
        'parameter value

        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)

        Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)
            SafeDefaultValue = ""
            If Not IsNothing(objIfti) Then
                Me.BenfOwnerNonNasabah_Nama.Text = objIfti.BeneficialOwner_NamaLengkap
                If Not IsNothing(objIfti.BeneficialOwner_TanggalLahir) Then
                    Me.BenfOwnerNonNasabah_TanggalLahir.Text = objIfti.BeneficialOwner_TanggalLahir
                End If
                Me.BenfOwnerNonNasabah_AlamatIden.Text = objIfti.BeneficialOwner_ID_Alamat
                objSkotaKab = objKotaKab.Find(MsKotaKabColumn.IDKotaKab, objIfti.BeneficialOwner_ID_KotaKab)
                If Not IsNothing(objSkotaKab) Then
                    Me.BenfOwnerNonNasabah_KotaIden.Text = Safe(objSkotaKab.NamaKotaKab)
                    hfBenfOwnerNonNasabah_KotaIden.Value = Safe(objSkotaKab.IDKotaKab)

                    If hfBenfOwnerNonNasabah_KotaIden.Value = ParameterKota.MsSystemParameter_Value Then
                        Me.BenfOwnerNonNasabah_KotaLainIden.Text = objIfti.BeneficialOwner_ID_KotaKabLainnya
                        Me.TrBenfOwnerNonNasabah_KotaLainIden.Visible = True
                    End If
                End If
                objSProvinsi = DataRepository.MsProvinceProvider.GetByIdProvince(SafeNumIndex(objIfti.BeneficialOwner_ID_Propinsi))
                If Not IsNothing(objSProvinsi) Then
                    Me.BenfOwnerNonNasabah_ProvinsiIden.Text = Safe(objSProvinsi.Nama)
                    hfBenfOwnerNonNasabah_ProvinsiIden.Value = Safe(objSProvinsi.IdProvince)
                    If hfBenfOwnerNonNasabah_ProvinsiIden.Value = ParameterProvinsi.MsSystemParameter_Value Then
                        Me.BenfOwnerNonNasabah_ProvinsiLainIden.Text = objIfti.BeneficialOwner_ID_PropinsiLainnya
                        Me.TrBenfOwnerNonNasabah_ProvinsiLainIden.Visible = True
                    End If
                End If


                If Not IsNothing(objIfti.BeneficialOwner_FK_IFTI_IDType) Then
                    Me.CBOBenfOwnerNonNasabah_JenisDokumen.SelectedValue = objIfti.BeneficialOwner_FK_IFTI_IDType
                End If
                Me.BenfOwnerNonNasabah_NomorIdentitas.Text = objIfti.BeneficialOwner_NomorID
            End If
        End Using
    End Sub
#End Region
#Region "SwiftOutValidation..."
#Region "Validation Sender"
    Sub ValidasiControl()
        If ObjectAntiNull(TxtUmum_TanggalLaporan.Text) = False Then Throw New Exception("Tanggal Laporan harus diisi  ")
        If RbUmum_JenisLaporan.SelectedValue <> "1" And ObjectAntiNull(TxtUmum_LTDLNKoreksi.Text) = False Then Throw New Exception("No. LTDLN Koreksi harus diisi  ")
        If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", TxtUmum_TanggalLaporan.Text) = False Then
            Throw New Exception("Tanggal Laporan tidak valid")
        End If
        If ObjectAntiNull(TxtUmum_PJKBankPelapor.Text) = False Or TxtUmum_PJKBankPelapor.Text.Length < 2 Then Throw New Exception("Nama PJK Bank Pelapor harus diisi minimal 3 karakter! ")
        If ObjectAntiNull(TxtUmum_NamaPejabatPJKBankPelapor.Text) = False Or TxtUmum_NamaPejabatPJKBankPelapor.Text.Length < 2 Then Throw New Exception("Nama Pejabat PJK Bank Pelapor harus diisi minimal 3 karakter!")
        If RbUmum_JenisLaporan.SelectedIndex = -1 Then Throw New Exception("Jenis Laporan harus dipilih  ")
    End Sub
    Sub validasiSender1()
        'senderIndividu
        'parameter value

        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
        Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
        Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)


        If CboSwiftOutPengirim.SelectedIndex = -1 Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Tipe PJK Bank harus diisi  ")
        If Rb_IdenPengirimNas_TipePengirim.SelectedIndex = -1 Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Tipe Pengirim harus diisi  ")
        If Rb_IdenPengirimNas_TipeNasabah.SelectedIndex = -1 Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Tipe Nasabah harus diisi  ")
        If ObjectAntiNull(TxtIdenPengirimNas_account.Text) = False Or TxtIdenPengirimNas_account.Text.Length < 2 Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: No Rekening harus diisi minimal 3 karakter!")

        If ObjectAntiNull(TxtIdenPengirimNas_Ind_NamaLengkap.Text) = False Or TxtIdenPengirimNas_Ind_NamaLengkap.Text.Length < 2 Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Nama Lengkap harus diisi minimal 3 karakter! ")
        If ObjectAntiNull(TxtIdenPengirimNas_Ind_tanggalLahir.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Tanggal Lahir harus diisi  ")
        If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", TxtIdenPengirimNas_Ind_tanggalLahir.Text) = False Then
            Throw New Exception("Identitas Pengirim Nasabah Perorangan: Tanggal Lahir tidak valid")
        End If
        If Rb_IdenPengirimNas_Ind_kewarganegaraan.SelectedValue = "2" Then
            If ObjectAntiNull(TxtIdenPengirimNas_Ind_negara.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Negara Kewarganegaraan harus diisi  ")
            If hfIdenPengirimNas_Ind_negara.Value = ParameterNegara.MsSystemParameter_Value Then
                If ObjectAntiNull(TxtIdenPengirimNas_Ind_negaralain.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Negara Lain Kewarganegaraan harus diisi  ")
            End If
            'If ObjectAntiNull(TxtIdenPengirimNas_Ind_negara.Text) = False And ObjectAntiNull(TxtIdenPengirimNas_Ind_negaralain.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Negara Kewarganegaraan harus diisi  ")
            'If ObjectAntiNull(TxtIdenPengirimNas_Ind_negara.Text) = True And ObjectAntiNull(TxtIdenPengirimNas_Ind_negaralain.Text) = True Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Negara Kewarganegaraan harus diisi salah satu saja  ")
        End If
        If ObjectAntiNull(TxtIdenPengirimNas_Ind_pekerjaan.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Pekerjaan harus diisi  ")
        If hfIdenPengirimNas_Ind_pekerjaan.Value = ParameterPekerjaan.MsSystemParameter_Value Then
            If ObjectAntiNull(TxtIdenPengirimNas_Ind_Pekerjaanlain.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Pekerjaan Lain harus diisi  ")
        End If

        'If ObjectAntiNull(TxtIdenPengirimNas_Ind_pekerjaan.Text) = True And ObjectAntiNull(TxtIdenPengirimNas_Ind_Pekerjaanlain.Text) = True Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Pekerjaan harus diisi salah satu saja  ")

        If cboPengirimNasInd_jenisidentitas.SelectedIndex = -1 Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Jenis Dokumen Identitas harus dipilih ")
        'alamat domisili
        'If ObjectAntiNull(hfIdenPengirimNas_Ind_Kota_dom.Value) = True Then
        '    If hfIdenPengirimNas_Ind_Kota_dom.Value = ParameterKota.MsSystemParameter_Value Then
        '        If ObjectAntiNull(Me.TxtIdenPengirimNas_Ind_kotalain.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Kota/Kabupaten Domisili Lain harus diisi  ")
        '    End If
        'End If
        'If ObjectAntiNull(hfIdenPengirimNas_Ind_provinsi_dom.Value) = True Then
        '    If hfIdenPengirimNas_Ind_provinsi_dom.Value = ParameterProvinsi.MsSystemParameter_Value Then
        '        If ObjectAntiNull(Me.TxtIdenPengirimNas_Ind_provinsiLain.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Provinsi Domisili Lain harus diisi  ")
        '    End If
        'End If


        'alamat identitas
        If ObjectAntiNull(TxtIdenPengirimNas_Ind_alamatIdentitas.Text) = False Or TxtIdenPengirimNas_Ind_alamatIdentitas.Text.Length < 2 Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Alamat Identitas harus diisi minimal 3 karakter! ")
        If ObjectAntiNull(TxtIdenPengirimNas_Ind_kotaIdentitas.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Kota/Kabupaten Identitas harus diisi  ")
        'If ObjectAntiNull(TxtIdenPengirimNas_Ind_kotaIdentitas.Text) = True And ObjectAntiNull(TxtIdenPengirimNas_Ind_KotaLainIden.Text) = True Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Kota/Kabupaten harus diisi salah satu saja ")
        If hfIdenPengirimNas_Ind_kotaIdentitas.Value = ParameterKota.MsSystemParameter_Value Then
            If ObjectAntiNull(TxtIdenPengirimNas_Ind_KotaLainIden.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Kota/Kabupaten Lain Identitas harus diisi  ")
        End If
        If ObjectAntiNull(TxtIdenPengirimNas_Ind_ProvinsiIden.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Provinsi Identitas harus diisi  ")
        'If ObjectAntiNull(TxtIdenPengirimNas_Ind_ProvinsiIden.Text) = True And ObjectAntiNull(TxtIdenPengirimNas_Ind_ProvinsilainIden.Text) = True Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Provinsi harus diisi salah satu saja ")
        If hfIdenPengirimNas_Ind_ProvinsiIden.Value = ParameterProvinsi.MsSystemParameter_Value Then
            If ObjectAntiNull(TxtIdenPengirimNas_Ind_ProvinsilainIden.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Provinsi Lain Identitas harus diisi  ")
        End If
        'If ObjectAntiNull(TxtIdenPengirimNas_Ind_ProvinsiIden.Text) = True And ObjectAntiNull(TxtIdenPengirimNas_Ind_ProvinsilainIden.Text) = True Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Nomor Identitas tidak valid atau kurang dari 3 digit! ")
        If ObjectAntiNull(TxtIdenPengirimNas_Ind_noIdentitas.Text) = False Or TxtIdenPengirimNas_Ind_noIdentitas.Text.Length < 2 Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Nomor Identitas harus diisi lebih dari 2 karakter!  ")
        If TxtIdenPengirimNas_Ind_noIdentitas.Text <> "" Then
            If Me.cboPengirimNasInd_jenisidentitas.SelectedValue = 1 Or Me.cboPengirimNasInd_jenisidentitas.SelectedValue = 1 = 2 Then
                If Not Regex.Match(TxtIdenPengirimNas_Ind_noIdentitas.Text.Trim(), "^[0-9]*$").Success Then
                    Throw New Exception("Nomor Identitas hanya boleh menggunakan angka!")
                End If
            Else
                If Not Regex.Match(TxtIdenPengirimNas_Ind_noIdentitas.Text.Trim(), "^[a-zA-Z0-9]*$").Success Then
                    Throw New Exception("Nomor Identitas hanya boleh menggunakan huruf dan angka!")
                End If
            End If
        End If
    End Sub
    Sub validasiSender2()
        'senderKorp
        'parameter value

        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
        Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
        Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)
        Dim ParameterBidangUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BidangUsaha)
        Dim ParameterBentukBadanUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BentukBadanUsaha)

        If CboSwiftOutPengirim.SelectedIndex = -1 Then Throw New Exception("Identitas Pengirim Nasabah korporasi: Tipe PJK Bank harus diisi  ")
        If Rb_IdenPengirimNas_TipePengirim.SelectedIndex = -1 Then Throw New Exception("Identitas Pengirim Nasabah korporasi: Tipe Pengirim harus diisi  ")
        If Rb_IdenPengirimNas_TipeNasabah.SelectedIndex = -1 Then Throw New Exception("Identitas Pengirim Nasabah korporasi: Tipe Nasabah harus diisi  ")
        If ObjectAntiNull(TxtIdenPengirimNas_account.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah korporasi: No Rekening harus diisi  ")
        'bentuk badan usaha
        'If ObjectAntiNull(CBOIdenPengirimNas_Corp_BentukBadanUsaha.SelectedValue) = True Then
        '    If CBOIdenPengirimNas_Corp_BentukBadanUsaha.SelectedValue = ParameterBidangUsaha.MsSystemParameter_Value Then
        '        If ObjectAntiNull(Me.TxtIdenPengirimNas_Corp_BentukBadanUsahaLain.Text) = False Then Throw New Exception(" Identitas Pengirim Nasabah korporasi: Bentuk Usaha Lain harus diisi  ")
        '    End If
        'End If

        If ObjectAntiNull(TxtIdenPengirimNas_Corp_NamaKorp.Text) = False Or TxtIdenPengirimNas_Corp_NamaKorp.Text.Length < 2 Then Throw New Exception("Identitas Pengirim Nasabah korporasi:Nama Korporasi harus diisi minimal 3 karakter! ")
        If ObjectAntiNull(TxtIdenPengirimNas_Corp_BidangUsahaKorp.Text) = False Then Throw New Exception("Bidang Usaha Korporasi harus diisi  ")
        '.bidang usaha korporasi lain
        'If hfIdenPengirimNas_Corp_BidangUsahaKorp.Value = ParameterBidangUsaha.MsSystemParameter_Value Then
        '    If ObjectAntiNull(Me.TxtIdenPengirimNas_Corp_BidangUsahaKorpLainnya.Text) = False Then Throw New Exception("Bidang Usaha Korporasi Lain harus diisi  ")
        'End If
        'If ObjectAntiNull(TxtIdenPengirimNas_Corp_BidangUsahaKorp.Text) = True And ObjectAntiNull(Me.TxtIdenPengirimNas_Corp_BidangUsahaKorpLainnya.Text) = True Then Throw New Exception("Identitas Pengirim Nasabah korporasi: Bidang Usaha Korporasi harus diisi salah satu saja")
        'alamat
        If ObjectAntiNull(TxtIdenPengirimNas_Corp_alamatkorp.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah korporasi: Alamat Identitas harus diisi  ")
        If ObjectAntiNull(TxtIdenPengirimNas_Corp_kotakorp.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah korporasi: Kota/Kabupaten harus diisi  ")
        If hfIdenPengirimNas_Corp_kotakorp.Value = ParameterKota.MsSystemParameter_Value Then
            If ObjectAntiNull(TxtIdenPengirimNas_Corp_kotakorplain.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah korporasi: Kota/Kabupaten Lain harus diisi  ")
        End If
        'If ObjectAntiNull(TxtIdenPengirimNas_Corp_kotakorp.Text) = True And ObjectAntiNull(TxtIdenPengirimNas_Corp_kotakorplain.Text) = True Then Throw New Exception("Identitas Pengirim Nasabah korporasi: Kota/Kabupaten harus diisi salah satu saja ")
        If ObjectAntiNull(TxtIdenPengirimNas_Corp_provKorp.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah korporasi: Provinsi harus diisi  ")
        If hfIdenPengirimNas_Corp_provKorp.Value = ParameterProvinsi.MsSystemParameter_Value Then
            If ObjectAntiNull(TxtIdenPengirimNas_Corp_provKorpLain.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah korporasi: Provinsi Lain harus diisi  ")
        End If
        'If ObjectAntiNull(TxtIdenPengirimNas_Corp_provKorp.Text) = True And ObjectAntiNull(TxtIdenPengirimNas_Corp_provKorpLain.Text) = True Then Throw New Exception("Identitas Pengirim Nasabah korporasi: Provinsi harus diisi salah satu saja  ")
    End Sub
    Sub validasiSender3()
        'senderNOnNasabah
        'parameter value

        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
        Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
        Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)


        If CboSwiftOutPengirim.SelectedIndex = -1 Then Throw New Exception("Identitas Pengirim Non Nasabah: Tipe PJK Bank harus diisi  ")
        If Rb_IdenPengirimNas_TipePengirim.SelectedIndex = -1 Then Throw New Exception("Identitas Pengirim Non Nasabah: Tipe Pengirim harus diisi  ")

        If RbPengirimNonNasabah_100Juta.SelectedIndex = -1 Then Throw New Exception("Identitas Pengirim Non Nasabah: Jumlah Transaksi harus diisi  ")
        If ObjectAntiNull(TxtPengirimNonNasabah_nama.Text) = False Or TxtPengirimNonNasabah_nama.Text.Length < 2 Then Throw New Exception("Identitas Pengirim Non Nasabah: Nama Lengkap harus diisi minimal 3 karakter! ")
        If ObjectAntiNull(TxtPengirimNonNasabah_alamatiden.Text) = False Then Throw New Exception("Identitas Pengirim Non Nasabah: Alamat harus diisi  ")
        'If ObjectAntiNull(TxtPengirimNonNasabah_kotaIden.Text) = True And ObjectAntiNull(Me.TxtPengirimNonNasabah_KoaLainIden.Text) = True Then Throw New Exception("Identitas Pengirim Non Nasabah: Kota/Kabupaten harus diisi salah satu saja ")
        'If ObjectAntiNull(TxtPengirimNonNasabah_ProvIden.Text) = True And ObjectAntiNull(TxtPengirimNonNasabah_ProvLainIden.Text) = True Then Throw New Exception("Identitas Pengirim Non Nasabah: Provinsi harus diisi salah satu saja  ")
        'kota dan provinsi  optional
        '------------
        'If ObjectAntiNull(hfPengirimNonNasabah_kotaIden.Value) = True Then
        '    If hfPengirimNonNasabah_kotaIden.Value = ParameterKota.MsSystemParameter_Value Then
        '        If ObjectAntiNull(TxtPengirimNonNasabah_KoaLainIden.Text) = False Then Throw New Exception("Identitas Pengirim Non Nasabah : Kota/Kabupaten Lain harus diisi  ")
        '    End If
        'End If

        ''If ObjectAntiNull(TxtIdenPengirimNas_Corp_kotakorp.Text) = True And ObjectAntiNull(TxtIdenPengirimNas_Corp_kotakorplain.Text) = True Then Throw New Exception("Identitas Pengirim Nasabah korporasi: Kota/Kabupaten harus diisi salah satu saja ")
        'If ObjectAntiNull(hfPengirimNonNasabah_ProvIden.Value) = True Then
        '    If hfPengirimNonNasabah_ProvIden.Value = ParameterProvinsi.MsSystemParameter_Value Then
        '        If ObjectAntiNull(TxtPengirimNonNasabah_ProvLainIden.Text) = False Then Throw New Exception("Identitas Pengirim Non Nasabah : Provinsi Lain harus diisi  ")
        '    End If
        'End If
        '---------------
        If TxtPengirimNonNasabah_NomorIden.Text <> "" Then
            If Me.CboPengirimNonNasabah_JenisDokumen.SelectedValue = 1 Or Me.CboPengirimNonNasabah_JenisDokumen.SelectedValue = 2 Then
                If Not Regex.Match(TxtPengirimNonNasabah_NomorIden.Text.Trim(), "^[0-9]*$").Success Then
                    Throw New Exception("Nomor Identitas hanya boleh menggunakan angka!")
                End If
            Else
                If Not Regex.Match(TxtPengirimNonNasabah_NomorIden.Text.Trim(), "^[a-zA-Z0-9]*$").Success Then
                    Throw New Exception("Nomor Identitas hanya boleh menggunakan huruf dan angka!")
                End If
            End If
        End If
        If RbPengirimNonNasabah_100Juta.SelectedValue = 2 Then
            If CboPengirimNonNasabah_JenisDokumen.SelectedIndex = -1 Then Throw New Exception("Identitas Pengirim Non Nasabah: Jenis Dokumen Identitas harus dipilih ")
            If ObjectAntiNull(TxtPengirimNonNasabah_NomorIden.Text) = False Then Throw New Exception("Identitas Pengirim Non Nasabah: Nomor Identitas harus diisi  ")
        End If

    End Sub
    Sub validasiSender4()
        'sender penerus Individu
        'parameter value

        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
        Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
        Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)


        If CboSwiftOutPengirim.SelectedIndex = -1 Then Throw New Exception("Identitas Pengirim Perorangan: Tipe PJK Bank harus diisi  ")
        If Rb_IdenPengirimNas_TipeNasabah.SelectedIndex = -1 Then Throw New Exception("Identitas Pengirim Perorangan: Tipe Nasabah harus diisi  ")
        If ObjectAntiNull(TxtPengirimPenerus_Rekening.Text) = False Or TxtPengirimPenerus_Rekening.Text.Length < 2 Then Throw New Exception("Identitas Pengirim  Perorangan: Nomor rekening harus diisi minimal 3 karakter! ")
        If ObjectAntiNull(TxtPengirimPenerus_Ind_namaBank.Text) = False Or TxtPengirimPenerus_Ind_namaBank.Text.Length < 2 Then Throw New Exception("Identitas Pengirim Perorangan: Nama Bank harus diisi minimal 3 karakter! ")
        If ObjectAntiNull(TxtPengirimPenerus_Ind_nama.Text) = False Then Throw New Exception("Identitas Pengirim  Perorangan: Nama Lengkap harus diisi  ")

        'If RBPengirimPenerus_Ind_Kewarganegaraan.SelectedValue = "2" Then
        '    If ObjectAntiNull(TxtPengirimPenerus_IND_negara.Text) = False And ObjectAntiNull(TxtPengirimPenerus_IND_negaralain.Text) = False Then Throw New Exception("Identitas Pengirim Perorangan: Negara Kewarganegaraan harus diisi  ")
        '    'If ObjectAntiNull(TxtPengirimPenerus_IND_negara.Text) = True And ObjectAntiNull(TxtPengirimPenerus_IND_negaralain.Text) = True Then Throw New Exception("Identitas Pengirim Perorangan: Negara Kewarganegaraan harus diisi salah satu saja  ")
        'End If
        If RBPengirimPenerus_Ind_Kewarganegaraan.SelectedValue = "2" Then
            If hfPengirimPenerus_IND_negara.Value = ParameterNegara.MsSystemParameter_Value Then
                If ObjectAntiNull(Me.TxtPengirimPenerus_IND_negaralain.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Negara Lain Kewarganegaraan harus diisi  ")
            End If
        End If
        'pekerjaan opt
        'If ObjectAntiNull(hfPengirimPenerus_IND_pekerjaan.Value) = True Then
        '    If hfPengirimPenerus_IND_pekerjaan.Value = ParameterPekerjaan.MsSystemParameter_Value Then
        '        If ObjectAntiNull(TxtPengirimPenerus_IND_pekerjaanLain.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Pekerjaan Lain harus diisi  ")
        '    End If
        'End If

        'alamat domisili
        'opt
        '----------------------------------------
        'If ObjectAntiNull(hfPengirimPenerus_IND_kotaDom.Value) = True Then
        '    If hfPengirimPenerus_IND_kotaDom.Value = ParameterKota.MsSystemParameter_Value Then
        '        If ObjectAntiNull(TxtPengirimPenerus_IND_kotaLainDom.Text) = False Then Throw New Exception("Identitas Pengirim Pengirim Perorangan : Kota/Kabupaten Domisili Lain harus diisi  ")
        '    End If
        'End If

        'If ObjectAntiNull(hfPengirimPenerus_IND_ProvDom.Value) = True Then
        '    If hfPengirimPenerus_IND_ProvDom.Value = ParameterProvinsi.MsSystemParameter_Value Then
        '        If ObjectAntiNull(TxtPengirimPenerus_IND_ProvLainDom.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Provinsi Domisili Lain harus diisi  ")
        '    End If
        'End If

        ''alamat identitas
        'If ObjectAntiNull(TxtPengirimPenerus_IND_kotaIden.Text) = True And ObjectAntiNull(TxtPengirimPenerus_IND_KotaLainIden.Text) = True Then Throw New Exception("Identitas Pengirim Perorangan: Kota/Kabupaten harus diisi salah satu saja ")
        'If ObjectAntiNull(TxtPengirimPenerus_IND_ProvIden.Text) = True And ObjectAntiNull(TxtPengirimPenerus_IND_ProvLainIden.Text) = True Then Throw New Exception("Identitas Pengirim Perorangan: Provinsi harus diisi salah satu saja ")
        'If ObjectAntiNull(hfPengirimPenerus_IND_kotaIden.Value) = True Then
        '    If hfPengirimPenerus_IND_kotaIden.Value = ParameterKota.MsSystemParameter_Value Then
        '        If ObjectAntiNull(TxtPengirimPenerus_IND_KotaLainIden.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah Perorangan : Kota/Kabupaten Lain harus diisi  ")
        '    End If
        'End If
        ''If ObjectAntiNull(TxtIdenPengirimNas_Corp_kotakorp.Text) = True And ObjectAntiNull(TxtIdenPengirimNas_Corp_kotakorplain.Text) = True Then Throw New Exception("Identitas Pengirim Nasabah korporasi: Kota/Kabupaten harus diisi salah satu saja ")
        'If ObjectAntiNull(hfPengirimPenerus_IND_ProvIden.Value) = True Then
        '    If hfPengirimPenerus_IND_ProvIden.Value = ParameterProvinsi.MsSystemParameter_Value Then
        '        If ObjectAntiNull(TxtPengirimPenerus_IND_ProvLainIden.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah Perorangan: Provinsi Lain harus diisi  ")
        '    End If
        'End If
        '-----------------------------------
        If ObjectAntiNull(TxtPengirimPenerus_IND_nomoridentitas.Text) = True And TxtPengirimPenerus_IND_nomoridentitas.Text.Length < 2 Then Throw New Exception("Identitas Pengirim Perorangan: Nomor Identitas kurang dari 2 digit!")
        If TxtPengirimPenerus_IND_nomoridentitas.Text <> "" Then
            If Me.CBoPengirimPenerus_IND_jenisIdentitas.SelectedValue = 1 Or Me.CBoPengirimPenerus_IND_jenisIdentitas.SelectedValue = 2 Then
                If Not Regex.Match(TxtPengirimPenerus_IND_nomoridentitas.Text.Trim(), "^[0-9]*$").Success Then
                    Throw New Exception("Nomor Identitas hanya boleh menggunakan angka!")
                End If
            Else
                If Not Regex.Match(TxtPengirimPenerus_IND_nomoridentitas.Text.Trim(), "^[a-zA-Z0-9]*$").Success Then
                    Throw New Exception("Nomor Identitas hanya boleh menggunakan huruf dan angka!")
                End If
            End If
        End If
    End Sub
    Sub validasiSender5()
        'sender penerus korporasi
        'parameter value

        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
        Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
        Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)
        Dim ParameterBidangUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BidangUsaha)
        Dim ParameterBentukBadanUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BentukBadanUsaha)


        If CboSwiftOutPengirim.SelectedIndex = -1 Then Throw New Exception("Identitas Pengirim  korporasi: Tipe PJK Bank harus diisi  ")
        If Rb_IdenPengirimNas_TipeNasabah.SelectedIndex = -1 Then Throw New Exception("Identitas Pengirim  korporasi: Tipe Nasabah harus diisi  ")
        If ObjectAntiNull(Me.TxtPengirimPenerus_Rekening.Text) = False Or TxtPengirimPenerus_Rekening.Text.Length < 2 Then Throw New Exception("Identitas Pengirim  korporasi: No Rekening harus diisi minimal 3 karakter! ")
        If ObjectAntiNull(txtPengirimPenerus_Korp_namabank.Text) = False Then Throw New Exception("Identitas Pengirim  korporasi: Nama Bank harus diisi  ")

        If ObjectAntiNull(txtPengirimPenerus_Korp_NamaKorp.Text) = False Or txtPengirimPenerus_Korp_NamaKorp.Text.Length < 2 Then Throw New Exception("Identitas Pengirim  korporasi: Nama Korporasi harus diisi minimal 3 karakter!  ")
        'If ObjectAntiNull(txtPengirimPenerus_Korp_BidangUsahaKorp.Text) = True And ObjectAntiNull(Me.txtPengirimPenerus_Korp_BidangUsahaKorplain.Text) = True Then Throw New Exception("Identitas Pengirim  korporasi: Bidang Usaha Korporasi harus diisi salah satu saja")

        'optional
        'If ObjectAntiNull(CBOPengirimPenerus_Korp_BentukBadanUsaha.SelectedValue) = True Then
        '    If CBOPengirimPenerus_Korp_BentukBadanUsaha.SelectedValue = ParameterBidangUsaha.MsSystemParameter_Value Then
        '        If ObjectAntiNull(Me.txtPengirimPenerus_Korp_BentukBadanUsahaLain.Text) = False Then Throw New Exception(" Identitas Pengirim Nasabah korporasi: Bentuk Usaha Lain harus diisi  ")
        '    End If
        'End If
        'If ObjectAntiNull(hfPengirimPenerus_Korp_BidangUsahaKorp.Value) = True Then
        '    If hfPengirimPenerus_Korp_BidangUsahaKorp.Value = ParameterBidangUsaha.MsSystemParameter_Value Then
        '        If ObjectAntiNull(Me.txtPengirimPenerus_Korp_BidangUsahaKorplain.Text) = False Then Throw New Exception("Bidang Usaha Korporasi harus diisi  ")
        '    End If
        'End If


        'If ObjectAntiNull(TxtIdenPengirimNas_Corp_BidangUsahaKorp.Text) = True And ObjectAntiNull(Me.TxtIdenPengirimNas_Corp_BidangUsahaKorpLainnya.Text) = True Then Throw New Exception("Identitas Pengirim Nasabah korporasi: Bidang Usaha Korporasi harus diisi salah satu saja")
        'alamat
        'If ObjectAntiNull(TxtIdenPengirimNas_Corp_alamatkorp.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah korporasi: Alamat Identitas harus diisi  ")
        'If ObjectAntiNull(hfPengirimPenerus_Korp_kotakorp.Value) = True Then
        '    If hfPengirimPenerus_Korp_kotakorp.Value = ParameterKota.MsSystemParameter_Value Then
        '        If ObjectAntiNull(txtPengirimPenerus_Korp_kotalainnyakorp.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah korporasi: Kota/Kabupaten Lain harus diisi  ")
        '    End If
        'End If

        ''If ObjectAntiNull(TxtIdenPengirimNas_Corp_kotakorp.Text) = True And ObjectAntiNull(TxtIdenPengirimNas_Corp_kotakorplain.Text) = True Then Throw New Exception("Identitas Pengirim Nasabah korporasi: Kota/Kabupaten harus diisi salah satu saja ")
        'If ObjectAntiNull(hfPengirimPenerus_Korp_ProvinsiKorp.Value) = True Then
        '    If hfPengirimPenerus_Korp_ProvinsiKorp.Value = ParameterProvinsi.MsSystemParameter_Value Then
        '        If ObjectAntiNull(txtPengirimPenerus_Korp_provinsiLainnyaKorp.Text) = False Then Throw New Exception("Identitas Pengirim Nasabah korporasi: Provinsi Lain harus diisi  ")
        '    End If
        'End If



    End Sub
#End Region
#Region "Validation Receiver"
    Function IsValidDecimal(ByVal intPrecision As Integer, ByVal intScale As Integer, ByVal strDatatoValidate As String) As Boolean

        Return Regex.IsMatch(strDatatoValidate, "^[\d]{1," & intPrecision & "}(\.[\d]{1," & intScale & "})?$")

    End Function
    Private Function ValidateReceiver() As Boolean
        Dim result As Boolean = True
        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
        Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
        Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)
        If RBPenerimaNasabah_TipePengirim.SelectedIndex = -1 Then
            result = False
            Throw New Exception("Identitas Penerima :Tipe Pengirim harus diisi  ")
        End If
        If RBPenerimaNasabah_TipePengirim.SelectedValue = 1 Then
            If Rb_IdenPenerimaNas_TipeNasabah.SelectedIndex = -1 Then
                result = False
                Throw New Exception("Identitas Penerima : Tipe Nasabah harus diisi  ")
            End If
        End If

        If RBPenerimaNasabah_TipePengirim.SelectedValue = 1 Then
            ' fj xf k
            If Rb_IdenPenerimaNas_TipeNasabah.SelectedValue = 1 Then
                'validasiReceiverInd()
                If ObjectAntiNull(txtPenerima_rekening.Text) = False Then
                    result = False
                    Throw New Exception("Identitas Penerima Nasabah Perorangan: Nomor rekening harus diisi  ")
                End If

                If ObjectAntiNull(txtPenerimaNasabah_IND_nama.Text) = False Or txtPenerimaNasabah_IND_nama.Text.Length < 2 Then
                    result = False
                    Throw New Exception("Identitas Penerima Nasabah Perorangan: Nama Lengkap harus diisi minimal 3 karakter! ")
                End If

                If RbPenerimaNasabah_IND_Warganegara.SelectedValue = "2" Then
                    If ObjectAntiNull(txtPenerimaNasabah_IND_negara.Text) = False Then
                        result = False
                        Throw New Exception("Identitas Penerima Nasabah Perorangan: Negara Kewarganegaraan harus diisi  ")
                    End If
                    If Me.hfPenerimaNasabah_IND_negaraIden.Value = ParameterNegara.MsSystemParameter_Value Then
                        If ObjectAntiNull(txtPenerimaNasabah_IND_negaraLain.Text) = False Then
                            result = False
                            Throw New Exception("Identitas Penerima Nasabah Perorangan: Negara Lain Kewarganegaraan harus diisi  ")
                        End If
                    End If
                End If

                If ObjectAntiNull(txtPenerimaNasabah_IND_alamatIden.Text) = False Or txtPenerimaNasabah_IND_alamatIden.Text.Length < 2 Then
                    result = False
                    Throw New Exception("Identitas Penerima Nasabah Perorangan: Alamat Identitas harus diisi minimal 3 karakter! ")
                End If
                'If ObjectAntiNull(txtPenerimaNasabah_IND_negaraIden.Text) = False And ObjectAntiNull(txtPenerimaNasabah_IND_negaraLainIden.Text) = False Then
                '    result = False
                '    Throw New Exception("Identitas Penerima Nasabah Perorangan: Negara harus diisi  ")
                'End If
                If ObjectAntiNull(txtPenerimaNasabah_IND_negaraIden.Text) = False Then
                    result = False
                    Throw New Exception("Identitas Penerima Nasabah Perorangan: Negara harus diisi  ")
                End If
                If Me.hfPenerimaNasabah_IND_negaraIden.Value = ParameterNegara.MsSystemParameter_Value Then
                    If ObjectAntiNull(txtPenerimaNasabah_IND_negaraLainIden.Text) = False Then
                        result = False
                        Throw New Exception("Identitas Penerima Nasabah Perorangan: Negara Lain harus diisi  ")
                    End If
                End If
                'If ObjectAntiNull(txtPenerimaNasabah_IND_negaraIden.Text) = True And ObjectAntiNull(txtPenerimaNasabah_IND_negaraLainIden.Text) = True Then
                '    result = False
                '    Throw New Exception("Identitas Penerima Nasabah Perorangan: Negara harus diisi salah satu saja ")
                'End If
                'If ObjectAntiNull(txtPenerimaNasabah_IND_NomorIden.Text) = True And Not IsNothing(txtPenerimaNasabah_IND_NomorIden.Text) Or txtPenerimaNasabah_IND_NomorIden.Text.Length < 2 Then
                '    result = False
                '    Throw New Exception("Identitas Penerima Nasabah Perorangan: Nomor identitas tidak valid! ")
                'End If
                If ReceiverDataTable.Rows.Count > 1 Then
                    If ObjectAntiNull(txtPenerimaNasabah_IND_NilaiTransaksiKeuangan.Text) = False Then
                        result = False
                        Throw New Exception("Identitas Penerima Nasabah Perorangan: Nilai Transaksi harus diisi apabila penerima lebih dari 1  ")
                    End If
                End If
                If ObjectAntiNull(txtPenerimaNasabah_IND_NilaiTransaksiKeuangan.Text) = True Then
                    If Not IsValidDecimal(15, 2, txtPenerimaNasabah_IND_NilaiTransaksiKeuangan.Text) Then
                        result = False
                        Throw New Exception("Identitas Penerima Nasabah Perorangan: Nilai Transaksi tidak valid!  ")
                    End If
                End If
            Else
                'validasiReceiverKorp()
                If ObjectAntiNull(txtPenerima_rekening.Text) = False Then
                    result = False
                    Throw New Exception("Identitas Penerima Nasabah Korporasi: Nomor rekening harus diisi  ")
                End If
                If ObjectAntiNull(txtPenerimaNasabah_Korp_namaKorp.Text) = False Or txtPenerimaNasabah_Korp_namaKorp.Text.Length < 2 Then
                    result = False
                    Throw New Exception("Identitas Penerima Nasabah Korporasi: Nama Korporasi harus diisi minimal 3 karakter! ")
                End If
                'If ObjectAntiNull(txtPenerimaNasabah_Korp_BidangUsahaKorp.Text) = True And ObjectAntiNull(txtPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Text) = True Then
                '    result = False
                '    Throw New Exception("Identitas Penerima Nasabah Korporasi: Bidang Usaha harus diisi salah satu saja ")
                'End If

                If ObjectAntiNull(txtPenerimaNasabah_Korp_AlamatKorp.Text) = False Or txtPenerimaNasabah_Korp_AlamatKorp.Text.Length < 2 Then
                    result = False
                    Throw New Exception("Identitas Penerima Nasabah Korporasi: Alamat Identitas harus diisi  ")
                End If
                If ObjectAntiNull(txtPenerimaNasabah_Korp_NegaraKorp.Text) = False Then
                    result = False
                    Throw New Exception("Identitas Penerima Nasabah Korporasi: Negara harus diisi  ")
                End If
                If Me.hfPenerimaNasabah_Korp_NegaraKorp.Value = ParameterNegara.MsSystemParameter_Value Then
                    If ObjectAntiNull(txtPenerimaNasabah_Korp_NegaraLainnyaKorp.Text) = False Then
                        result = False
                        Throw New Exception("Identitas Penerima Nasabah Korporasi: Negara Lain harus diisi  ")
                    End If
                End If
                'If ObjectAntiNull(txtPenerimaNasabah_Korp_NegaraKorp.Text) = True And ObjectAntiNull(txtPenerimaNasabah_Korp_NegaraLainnyaKorp.Text) = True Then
                '    result = False
                '    Throw New Exception("Identitas Penerima Nasabah Korporasi: Negara harus diisi salah satu saja ")
                'End If

                If ReceiverDataTable.Rows.Count > 1 Then
                    If ObjectAntiNull(txtPenerimaNasabah_Korp_NilaiTransaksiKeuangan.Text) = False Then
                        result = False
                        Throw New Exception("Identitas Penerima Nasabah Korporasi: Nilai Transaksi harus diisi apabila penerima lebih dari 1  ")
                    End If
                End If
                If ObjectAntiNull(txtPenerimaNasabah_Korp_NilaiTransaksiKeuangan.Text) = True Then
                    If Not IsValidDecimal(15, 2, txtPenerimaNasabah_Korp_NilaiTransaksiKeuangan.Text) Then
                        result = False
                        Throw New Exception("Identitas Penerima Nasabah Korporasi: Nilai Transaksi tidak valid!  ")
                    End If
                End If
            End If

        Else
            'validasiReceiverNonNasabah()
            If ObjectAntiNull(txtPenerima_rekening.Text) = False Then
                'result = False
                'Throw New Exception("Identitas Penerima Non Nasabah: Nomor rekening harus diisi  ")
                If ObjectAntiNull(Me.txtPenerimaNonNasabah_namabank.Text) = False Then
                    result = False
                    Throw New Exception("Identitas Penerima Non Nasabah: Nama Bank harus diisi apabila no rekening kosong!  ")
                End If
                If ObjectAntiNull(Me.txtPenerimaNonNasabah_Alamat.Text) = False Then
                    result = False
                    Throw New Exception("Identitas Penerima Non Nasabah: Alamat Identitas harus diisi apabila no rekening kosong! ")
                End If
            End If
            If ObjectAntiNull(txtPenerimaNonNasabah_Nama.Text) = False Or txtPenerimaNonNasabah_Nama.Text.Length < 2 Then
                result = False
                Throw New Exception("Identitas Penerima Non Nasabah: Nama Lengkap harus diisi minimal 3 karakter! ")
            End If

            If ObjectAntiNull(txtPenerimaNonNasabah_Negara.Text) = False Then
                result = False
                Throw New Exception("Identitas Penerima Non Nasabah: Negara harus diisi  ")
            End If
            If Me.hfPenerimaNonNasabah_Negara.Value = ParameterNegara.MsSystemParameter_Value Then
                If ObjectAntiNull(txtPenerimaNonNasabah_negaraLain.Text) = False Then
                    result = False
                    Throw New Exception("Identitas Penerima Non Nasabah: Negara Lain harus diisi  ")
                End If
            End If
            'If ObjectAntiNull(txtPenerimaNonNasabah_Negara.Text) = True And ObjectAntiNull(txtPenerimaNonNasabah_negaraLain.Text) = True Then
            '    result = False
            '    Throw New Exception("Identitas Penerima Non Nasabah: Negara harus diisi salah satu saja ")
            'End If
            If ReceiverDataTable.Rows.Count > 1 Then
                If ObjectAntiNull(txtPenerimaNonNasabah_nilaiTransaksiKeuangan.Text) = False Then
                    result = False
                    Throw New Exception("Identitas Penerima Non Nasabah: Nilai Transaksi harus diisi apabila penerima lebih dari 1  ")
                End If
            End If
            If ObjectAntiNull(txtPenerimaNonNasabah_nilaiTransaksiKeuangan.Text) = True Then
                If Not IsValidDecimal(15, 2, txtPenerimaNonNasabah_nilaiTransaksiKeuangan.Text) Then
                    result = False
                    Throw New Exception("Identitas Penerima Non Nasabah: Nilai Transaksi tidak valid!  ")
                End If
            End If

        End If
        Return result
    End Function
    'Sub validasiReceiverInd()
    '    'penerimaIndividu
    '    'parameter value

    '    Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
    '    Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
    '    Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
    '    Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)

    '    If ObjectAntiNull(txtPenerima_rekening.Text) = False Then Throw New Exception("Identitas Penerima Nasabah Perorangan: Nomor rekening harus diisi  ")
    '    If RBPenerimaNasabah_TipePengirim.SelectedIndex = -1 Then Throw New Exception("Identitas Penerima Nasabah Perorangan: Tipe Pengirim harus diisi  ")
    '    If Rb_IdenPenerimaNas_TipeNasabah.SelectedIndex = -1 Then Throw New Exception("Identitas Penerima Nasabah Perorangan: Tipe Nasabah harus diisi  ")

    '    If ObjectAntiNull(txtPenerimaNasabah_IND_nama.Text) = False Then Throw New Exception("Identitas Penerima Nasabah Perorangan: Nama Lengkap harus diisi  ")
    '    If RbPenerimaNasabah_IND_Warganegara.SelectedValue = "2" Then
    '        If ObjectAntiNull(txtPenerimaNasabah_IND_negara.Text) = False Then Throw New Exception("Identitas Penerima Nasabah Perorangan: Negara Kewarganegaraan harus diisi  ")
    '        If hfPenerimaNasabah_IND_negara.Value = ParameterNegara.MsSystemParameter_Value Then
    '            If ObjectAntiNull(txtPenerimaNasabah_IND_negaraLain.Text) = False Then Throw New Exception("Identitas Penerima Nasabah Perorangan: Negara Kewarganegaraan harus diisi  ")
    '        End If
    '        'If ObjectAntiNull(txtPenerimaNasabah_IND_negara.Text) = True And ObjectAntiNull(txtPenerimaNasabah_IND_negaraLain.Text) = True Then Throw New Exception("Identitas Penerima Nasabah Perorangan: Negara Kewarganegaraan harus diisi salah satu saja  ")
    '    End If

    '    If ObjectAntiNull(txtPenerimaNasabah_IND_alamatIden.Text) = False Then Throw New Exception("Identitas Penerima Nasabah Perorangan: Alamat Identitas harus diisi  ")
    '    If ObjectAntiNull(txtPenerimaNasabah_IND_negaraIden.Text) = False Then Throw New Exception("Identitas Penerima Nasabah Perorangan: Negara harus diisi  ")
    '    If Me.hfPenerimaNasabah_IND_negaraIden.Value = ParameterNegara.MsSystemParameter_Value Then
    '        If ObjectAntiNull(txtPenerimaNasabah_IND_negaraLainIden.Text) = False Then Throw New Exception("Identitas Penerima Nasabah Perorangan: Negara harus diisi  ")
    '    End If
    '    'If ObjectAntiNull(txtPenerimaNasabah_IND_negaraIden.Text) = True And ObjectAntiNull(txtPenerimaNasabah_IND_negaraLainIden.Text) = True Then Throw New Exception("Identitas Penerima Nasabah Perorangan: Negara harus diisi salah satu saja ")
    'End Sub
    'Sub validasiReceiverKorp()
    '    'pennerimaKorporasi
    '    'parameter value

    '    Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
    '    Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
    '    Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
    '    Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)

    '    If ObjectAntiNull(txtPenerima_rekening.Text) = False Then Throw New Exception("Identitas Penerima Nasabah Korporasi: Nomor rekening harus diisi  ")
    '    If RBPenerimaNasabah_TipePengirim.SelectedIndex = -1 Then Throw New Exception("Identitas Penerima Nasabah Korporasi: Tipe Pengirim harus diisi  ")
    '    If Rb_IdenPenerimaNas_TipeNasabah.SelectedIndex = -1 Then Throw New Exception("Identitas Penerima Nasabah Korporasi: Tipe Nasabah harus diisi  ")

    '    If ObjectAntiNull(txtPenerimaNasabah_Korp_namaKorp.Text) = False Then Throw New Exception("Identitas Penerima Nasabah Korporasi: Nama Korporasi harus diisi  ")
    '    If ObjectAntiNull(txtPenerimaNasabah_Korp_BidangUsahaKorp.Text) = True And ObjectAntiNull(txtPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Text) = True Then Throw New Exception("Identitas Penerima Nasabah Korporasi: Bidang Usaha harus diisi salah satu saja ")
    '    If ObjectAntiNull(txtPenerimaNasabah_Korp_AlamatKorp.Text) = False Then Throw New Exception("Identitas Penerima Nasabah Korporasi: Alamat Identitas harus diisi  ")
    '    If ObjectAntiNull(txtPenerimaNasabah_Korp_NegaraKorp.Text) = False And ObjectAntiNull(txtPenerimaNasabah_IND_negaraLainIden.Text) = False Then Throw New Exception("Identitas Penerima Nasabah Korporasi: Negara harus diisi  ")
    '    If ObjectAntiNull(txtPenerimaNasabah_Korp_NegaraKorp.Text) = True And ObjectAntiNull(txtPenerimaNasabah_IND_negaraLainIden.Text) = True Then Throw New Exception("Identitas Penerima Nasabah Korporasi: Negara harus diisi salah satu saja ")
    'End Sub
    'Sub validasiReceiverNonNasabah()
    '    'penerimaNOnNasabah
    '    'parameter value

    '    Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
    '    Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
    '    Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
    '    Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)

    '    If ObjectAntiNull(txtPenerima_rekening.Text) = False Then Throw New Exception("Identitas Penerima Non Nasabah: Nomor rekening harus diisi  ")
    '    If RBPenerimaNasabah_TipePengirim.SelectedIndex = -1 Then Throw New Exception("Identitas Penerima Non Nasabah: Tipe Pengirim harus diisi  ")

    '    If ObjectAntiNull(txtPenerimaNonNasabah_Nama.Text) = False Then Throw New Exception("Identitas Penerima Non Nasabah: Nama Lengkap harus diisi  ")
    '    If ObjectAntiNull(txtPenerimaNasabah_Korp_AlamatKorp.Text) = False Then Throw New Exception("Identitas Penerima Non Nasabah: Alamat Identitas harus diisi  ")
    '    If ObjectAntiNull(txtPenerimaNonNasabah_Negara.Text) = False And ObjectAntiNull(txtPenerimaNasabah_IND_negaraLainIden.Text) = False Then Throw New Exception("Identitas Penerima Non Nasabah: Negara harus diisi  ")
    '    If ObjectAntiNull(txtPenerimaNonNasabah_Negara.Text) = True And ObjectAntiNull(txtPenerimaNasabah_IND_negaraLainIden.Text) = True Then Throw New Exception("Identitas Penerima Non Nasabah: Negara harus diisi salah satu saja ")
    'End Sub
    Private Function ValidateReceiverSaveAll() As Boolean

        Dim result As ArrayList
        Dim i As Integer
        Dim dt As Data.DataTable = ReceiverDataTable
        Dim rowCount As Integer = dt.Rows.Count

        If (rowCount > 0) Then
            result = New ArrayList
        End If

        For i = 0 To rowCount - 1
            Dim KeyBeneficiary As Integer
            If ObjectAntiNull(dt.Rows(i)("PK_IFTI_Beneficiary_ID")) Then
                KeyBeneficiary = dt.Rows(i)("PK_IFTI_Beneficiary_ID")
            End If
            '= dt.Rows(i)("PK_IFTI_Beneficiary_ID")

            Dim TipePenerima As Integer = dt.Rows(i)("FK_IFTI_NasabahType_ID")


            'Dim resultbool As Boolean = True
            Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
            Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
            Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
            Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)
            Select Case (TipePenerima)
                Case 1
                    'validasiReceiverInd()
                    If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_INDV_NoRekening")) = False Then
                        'resultbool = False
                        Throw New Exception("Identitas Penerima " & i + 1 & " Nasabah Perorangan: Nomor rekening harus diisi  ")
                    End If
                    If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_INDV_NamaLengkap")) = False Or dt.Rows(i)("Beneficiary_Nasabah_INDV_NamaLengkap").Length < 2 Then
                        ''resultbool = False
                        Throw New Exception("Identitas Penerima " & i + 1 & " Nasabah Perorangan: Nama Lengkap harus diisi minimal 3 karakter! ")
                    End If

                    If dt.Rows(i)("Beneficiary_Nasabah_INDV_KewargaNegaraan") = 2 Then
                        If dt.Rows(i)("Beneficiary_Nasabah_INDV_Negara") = False Then
                            'resultbool = False
                            Throw New Exception("Identitas Penerima " & i + 1 & " Nasabah Perorangan: Negara Kewarganegaraan harus diisi  ")
                        End If
                        If dt.Rows(i)("Beneficiary_Nasabah_INDV_Negara") = ParameterNegara.MsSystemParameter_Value Then
                            If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_INDV_NegaraLainnya")) = False Then
                                'resultbool = False
                                Throw New Exception("Identitas Penerima " & i + 1 & " Nasabah Perorangan: Negara Lain Kewarganegaraan harus diisi  ")
                            End If
                        End If
                    End If

                    If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_INDV_ID_Alamat_Iden")) = False Or dt.Rows(i)("Beneficiary_Nasabah_INDV_ID_Alamat_Iden").Length < 2 Then
                        'resultbool = False
                        Throw New Exception("Identitas Penerima " & i + 1 & " Nasabah Perorangan: Alamat Identitas harus diisi minimal 3 karakter! ")
                    End If
                    If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_INDV_ID_Negara")) = False Then
                        'resultbool = False
                        Throw New Exception("Identitas Penerima " & i + 1 & " Nasabah Perorangan: Negara harus diisi  ")
                    End If
                    If dt.Rows(i)("Beneficiary_Nasabah_INDV_ID_Negara") = ParameterNegara.MsSystemParameter_Value Then
                        If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_INDV_ID_NegaraLainnya")) = False Then
                            'resultbool = False
                            Throw New Exception("Identitas Penerima " & i + 1 & " Nasabah Perorangan: Negara Lain harus diisi  ")
                        End If
                    End If
                    If ReceiverDataTable.Rows.Count > 1 Then
                        If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan")) = False Then
                            'resultbool = False
                            Throw New Exception("Identitas Penerima " & i + 1 & " Nasabah Perorangan: Nilai Transaksi harus diisi apabila penerima lebih dari 1  ")
                        End If
                    End If
                    If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan")) = True Then
                        If Not IsValidDecimal(15, 2, dt.Rows(i)("Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan")) Then
                            'resultbool = False
                            Throw New Exception("Identitas Penerima " & i + 1 & " Nasabah Perorangan: Nilai Transaksi tidak valid!  ")
                        End If
                    End If
                Case 2
                    'validasiReceiverKorp()
                    If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_CORP_NoRekening")) = False Then
                        'resultbool = False
                        Throw New Exception("Identitas Penerima " & i + 1 & " Nasabah Korporasi: Nomor rekening harus diisi  ")
                    End If

                    If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_CORP_NamaKorporasi")) = False Or dt.Rows(i)("Beneficiary_Nasabah_CORP_NamaKorporasi").Length < 2 Then
                        'resultbool = False
                        Throw New Exception("Identitas Penerima " & i + 1 & " Nasabah Korporasi: Nama Korporasi harus diisi minimal 3 karakter! ")
                    End If

                    If ObjectAntiNull(dt.Rows(i)("Sender_Nasabah_CORP_AlamatLengkap")) = False Or dt.Rows(i)("Sender_Nasabah_CORP_AlamatLengkap").Length < 2 Then
                        'resultbool = False
                        Throw New Exception("Identitas Penerima " & i + 1 & " Nasabah Korporasi: Alamat Identitas harus diisi  ")
                    End If
                    If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_CORP_ID_Negara")) = False Then
                        'resultbool = False
                        Throw New Exception("Identitas Penerima " & i + 1 & " Nasabah Korporasi: Negara harus diisi  ")
                    End If
                    If dt.Rows(i)("Beneficiary_Nasabah_CORP_ID_Negara") = ParameterNegara.MsSystemParameter_Value Then
                        If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_CORP_ID_NegaraLainnya")) = False Then
                            'resultbool = False
                            Throw New Exception("Identitas Penerima " & i + 1 & " Nasabah Korporasi: Negara Lain harus diisi  ")
                        End If
                    End If
                    If ReceiverDataTable.Rows.Count > 1 Then
                        If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan")) = False Then
                            'resultbool = False
                            Throw New Exception("Identitas Penerima " & i + 1 & " Nasabah Korporasi: Nilai Transaksi harus diisi apabila penerima lebih dari 1  ")
                        End If
                    End If
                    If ObjectAntiNull(dt.Rows(i)("Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan")) = True Then
                        If Not IsValidDecimal(15, 2, dt.Rows(i)("Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan")) Then
                            'resultbool = False
                            Throw New Exception("Identitas Penerima " & i + 1 & " Nasabah Korporasi: Nilai Transaksi tidak valid!  ")
                        End If
                    End If
                Case 3
                    'validasiReceiverNonNasabah()
                    If ObjectAntiNull(dt.Rows(i)("Beneficiary_NonNasabah_NoRekening")) = False Then
                        ''resultbool = False
                        'Throw New Exception("Identitas Penerima Non Nasabah: Nomor rekening harus diisi  ")
                        If ObjectAntiNull(dt.Rows(i)("Beneficiary_NonNasabah_NamaBank")) = False Then
                            'resultbool = False
                            Throw New Exception("Identitas Penerima " & i + 1 & " Non Nasabah: Nama Bank harus diisi apabila no rekening kosong!  ")
                        End If
                        If ObjectAntiNull(dt.Rows(i)("Beneficiary_NonNasabah_ID_Alamat")) = False Then
                            'resultbool = False
                            Throw New Exception("Identitas Penerima " & i + 1 & " Non Nasabah: Alamat Identitas harus diisi apabila no rekening kosong! ")
                        End If
                    End If
                    If ObjectAntiNull(dt.Rows(i)("Beneficiary_NonNasabah_NamaLengkap")) = False Or dt.Rows(i)("Beneficiary_NonNasabah_NamaLengkap").Length < 2 Then
                        'resultbool = False
                        Throw New Exception("Identitas Penerima " & i + 1 & " Non Nasabah: Nama Lengkap harus diisi minimal 3 karakter! ")
                    End If

                    If ObjectAntiNull(dt.Rows(i)("Beneficiary_NonNasabah_ID_Negara")) = False Then
                        'resultbool = False
                        Throw New Exception("Identitas Penerima " & i + 1 & " Non Nasabah: Negara harus diisi  ")
                    End If
                    If dt.Rows(i)("Beneficiary_NonNasabah_ID_Negara") = ParameterNegara.MsSystemParameter_Value Then
                        If ObjectAntiNull(dt.Rows(i)("Beneficiary_NonNasabah_ID_NegaraLainnya")) = False Then
                            'resultbool = False
                            Throw New Exception("Identitas Penerima " & i + 1 & " Non Nasabah: Negara Lain harus diisi  ")
                        End If
                    End If

                    If ReceiverDataTable.Rows.Count > 1 Then
                        If ObjectAntiNull(dt.Rows(i)("Beneficiary_NonNasabah_NilaiTransaksikeuangan")) = False Then
                            'resultbool = False
                            Throw New Exception("Identitas Penerima " & i + 1 & " Non Nasabah: Nilai Transaksi harus diisi apabila penerima lebih dari 1  ")
                        End If
                    End If
                    If ObjectAntiNull(dt.Rows(i)("Beneficiary_NonNasabah_NilaiTransaksikeuangan")) = True Then
                        If Not IsValidDecimal(15, 2, dt.Rows(i)("Beneficiary_NonNasabah_NilaiTransaksikeuangan")) Then
                            'resultbool = False
                            Throw New Exception("Identitas Penerima " & i + 1 & " Non Nasabah: Nilai Transaksi tidak valid!  ")
                        End If
                    End If
            End Select
        Next

    End Function
#End Region
    Sub validasiTransaksi()
        Dim ParameterMataUang As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.MataUang)
        If ObjectAntiNull(Transaksi_tanggal.Text) = False Then Throw New Exception("Tanggal Transaksi harus diisi  ")
        If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", Transaksi_tanggal.Text) = False Then
            Throw New Exception("Tanggal Transaksi tidak valid")
        End If
        If ObjectAntiNull(Transaksi_Sender.Text) = False Or Transaksi_Sender.Text.Length < 2 Then Throw New Exception("Sender's Reference harus diisi  ")
        If ObjectAntiNull(Transaksi_BankOperationCode.Text) = False Then Throw New Exception("Bank Operation Code harus diisi  ")
        'If ReceiverDataTable.Rows.Count > 0 Then
        '    Dim i As Integer
        '    Dim rows As Integer = ReceiverDataTable.Rows.Count
        '    Dim Penerima As Integer
        '    Dim counter As Integer
        '    For i = 0 To rows - 1
        '        Penerima = ReceiverDataTable.Rows(i)("sender_PJKBank_Type")
        '        If Penerima = 2 Then
        '            counter += 1
        '        End If
        '    Next
        '    If counter > 0 Then
        '        If ObjectAntiNull(Me.Transaksi_kantorCabangPengirim.Text) = False Then Throw New Exception("Kantor Cabang Penyelenggara harus diisi!  ")
        '    End If
        'End If

        ' Dim Penerima As Integer = CboSwiftOutPengirim.SelectedValue
        If CboSwiftOutPengirim.SelectedValue = 2 Then
            If ObjectAntiNull(Me.Transaksi_kantorCabangPengirim.Text) = False Then Throw New Exception("Kantor Cabang Penyelenggara harus diisi!  ")
        End If

        If ObjectAntiNull(Transaksi_ValueTanggalTransaksi.Text) = False Then Throw New Exception("Tanggal Transaksi(value date) harus diisi  ")
        If IsDateValid("dd-MMM-yyyy", Transaksi_ValueTanggalTransaksi.Text) = False Then
            Throw New Exception("Tanggal Transaksi tidak valid")
        End If
        If ObjectAntiNull(Transaksi_nilaitransaksi.Text) = False Then Throw New Exception("Nilai Transaksi harus diisi  ")
        If Not IsNumeric(Transaksi_nilaitransaksi.Text) Then Throw New Exception("Nilai Transaksi harus diisi angka ")
        If ObjectAntiNull(Transaksi_MataUangTransaksi.Text) = False Then Throw New Exception("Mata Uang Transaksi harus diisi  ")


        If Me.hfTransaksi_MataUangTransaksi.Value = ParameterMataUang.MsSystemParameter_Value Then
            If ObjectAntiNull(Me.Transaksi_MataUangTransaksiLain.Text) = False Then Throw New Exception("Mata Uang Lain Transaksi harus diisi  ")
        End If
        'If ObjectAntiNull(Transaksi_MataUangTransaksi.Text) = True And ObjectAntiNull(Transaksi_MataUangTransaksiLain.Text) = True Then Throw New Exception("Mata Uang Transaksi harus diisi salah satu saja! ")
        If ObjectAntiNull(Transaksi_AmountdalamRupiah.Text) = False Then Throw New Exception("Nilai Transaksi Keuangan yang dikirim dalam rupiah  harus diisi  ")
        If Not IsNumeric(Transaksi_AmountdalamRupiah.Text) Then Throw New Exception("Nilai Transaksi Keuangan yang dikirim dalam rupiah  harus diisi angka ")
        'If ObjectAntiNull(Transaksi_currency.Text) = False Then Throw New Exception("Mata Uang Transaksi harus diisi  ")
        'If ObjectAntiNull(Transaksi_currency.Text) = True And ObjectAntiNull(Transaksi_currencyLain.Text) = True Then Throw New Exception("Mata Uang Transaksi harus diisi salah satu saja! ")
        If ObjectAntiNull(Transaksi_instructedAmount.Text) = True Then
            If Not IsNumeric(Transaksi_instructedAmount.Text) Then Throw New Exception("Instructed Amaount harus diisi angka ")
        End If

        If Me.Rb_IdenPengirimNas_TipeNasabah.SelectedIndex <> -1 Then
            If Me.Rb_IdenPengirimNas_TipeNasabah.SelectedValue = 1 Then
                If Rb_IdenPengirimNas_TipePengirim.SelectedIndex <> -1 Then
                    If Me.Rb_IdenPengirimNas_TipePengirim.SelectedValue = 1 And CDbl(Transaksi_AmountdalamRupiah.Text) > 100000000.0 And ObjectAntiNull(Transaksi_SumberPenggunaanDana.Text) = False Then Throw New Exception("Sumber Penggunaan Dana harus diisi! ")
                End If
            End If
        End If


        If ObjectAntiNull(Transaksi_nilaiTukar.Text) = True Then
            If Not IsNumeric(Transaksi_nilaiTukar.Text) Then Throw New Exception(" Nilai Tukar harus diisi angka ")
        End If
        If CboSwiftOutPengirim.SelectedIndex = -1 Then Throw New Exception("Tipe PJK Bank harus diisi  ")
        'If ObjectAntiNull(Transaksi_SumberPenggunaanDana.Text) = False And RbPengirimNonNasabah_100Juta.SelectedValue = 2 And CInt(Transaksi_AmountdalamRupiah.Text) > 100000000 Then Throw New Exception("Amount dalam rupiah harus diisi  ")
    End Sub
    Sub validationBOwnerNasabah()
        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.MataUang)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
        Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
        Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)

        If ObjectAntiNull(BenfOwnerNasabah_rekening.Text) = False Or BenfOwnerNasabah_rekening.Text.Length < 2 Then Throw New Exception("Identitas Beneficial Owner Nasabah: No Rekening harus diisi minimal 3 karakter!  ")
        If ObjectAntiNull(BenfOwnerNasabah_Nama.Text) = False Or BenfOwnerNasabah_Nama.Text.Length < 2 Then Throw New Exception("Identitas Beneficial Owner Nasabah: Nama Lengkap harus diisi minimal 3 karakter! ")
        If ObjectAntiNull(BenfOwnerNasabah_tanggalLahir.Text) = False Then Throw New Exception("Identitas Beneficial Owner Nasabah: Tanggal Lahir harus diisi  ")
        If Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", BenfOwnerNasabah_tanggalLahir.Text) = False Then
            Throw New Exception("Identitas Beneficial Owner Nasabah: Tanggal Lahir tidak valid")
        End If
        If ObjectAntiNull(BenfOwnerNasabah_AlamatIden.Text) = False Then Throw New Exception("Identitas Beneficial Owner Nasabah: Alamat harus diisi  ")
        If ObjectAntiNull(BenfOwnerNasabah_KotaIden.Text) = False Then Throw New Exception("Identitas Beneficial Owner Nasabah: Kota/Kabupaten harus diisi! ")
        If hfBenfOwnerNasabah_KotaIden.Value = ParameterKota.MsSystemParameter_Value Then
            If ObjectAntiNull(BenfOwnerNasabah_kotaLainIden.Text) = False Then Throw New Exception("Identitas Beneficial Owner Nasabah: Kota/Kabupaten Lain harus diisi! ")
        End If
        If ObjectAntiNull(BenfOwnerNasabah_ProvinsiIden.Text) = False Then Throw New Exception("Identitas Beneficial Owner Nasabah: Provinsi harus diisi! ")
        If hfBenfOwnerNasabah_ProvinsiIden.Value = ParameterProvinsi.MsSystemParameter_Value Then
            If ObjectAntiNull(BenfOwnerNasabah_ProvinsiLainIden.Text) = False Then Throw New Exception("Identitas Beneficial Owner Nasabah: Provinsi Lain harus diisi! ")
        End If
        'If ObjectAntiNull(BenfOwnerNasabah_KotaIden.Text) = True And ObjectAntiNull(BenfOwnerNasabah_kotaLainIden.Text) = True Then Throw New Exception("Identitas Beneficial Owner Nasabah: Kota/Kabupaten harus diisi salah satu saja ")
        'If ObjectAntiNull(BenfOwnerNasabah_ProvinsiIden.Text) = True And ObjectAntiNull(BenfOwnerNasabah_ProvinsiLainIden.Text) = True Then Throw New Exception("Identitas Beneficial Owner Nasabah: Provinsi harus diisi salah satu saja ")
        If CBOBenfOwnerNasabah_JenisDokumen.SelectedIndex = 0 Then Throw New Exception("Identitas Beneficial Owner Nasabah: Jenis Dokumen identitas harus dipilih! ")
        If BenfOwnerNasabah_NomorIden.Text.Length < 2 Then Throw New Exception("Identitas Beneficial Owner Nasabah: Nomor Identitas harus diisi lebih dari 2 karakter! ")
        If BenfOwnerNasabah_NomorIden.Text <> "" Then
            If Me.CBOBenfOwnerNasabah_JenisDokumen.SelectedValue = 1 Or Me.CBOBenfOwnerNasabah_JenisDokumen.SelectedValue = 2 Then
                If Not Regex.Match(BenfOwnerNasabah_NomorIden.Text.Trim(), "^[0-9]*$").Success Then
                    Throw New Exception("Nomor Identitas hanya boleh menggunakan angka!")
                End If
            Else
                If Not Regex.Match(BenfOwnerNasabah_NomorIden.Text.Trim(), "^[a-zA-Z0-9]*$").Success Then
                    Throw New Exception("Nomor Identitas hanya boleh menggunakan huruf dan angka!")
                End If
            End If
        End If
    End Sub
    Sub validationBOwnerNonNasabah()
        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
        Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
        Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)

        If ObjectAntiNull(BenfOwnerNonNasabah_Nama.Text) = False Or BenfOwnerNonNasabah_Nama.Text.Length < 2 Then Throw New Exception("Identitas Beneficial Owner No Nasabah: Nama Lengkap harus diisi!")
        If ObjectAntiNull(BenfOwnerNonNasabah_TanggalLahir.Text) = True And Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", BenfOwnerNonNasabah_TanggalLahir.Text) = False Then
            Throw New Exception("Identitas Beneficial Owner Non Nasabah: Tanggal Lahir tidak valid")
        End If

        If ObjectAntiNull(BenfOwnerNonNasabah_AlamatIden.Text) = False Or BenfOwnerNonNasabah_AlamatIden.Text.Length < 2 Then Throw New Exception("Identitas Beneficial Owner No Nasabah: Alamat harus diisi!")
        If ObjectAntiNull(BenfOwnerNonNasabah_KotaIden.Text) = False Then Throw New Exception("Identitas Beneficial Owner Non Nasabah: Kota/Kabupaten harus diisi! ")
        If hfBenfOwnerNonNasabah_KotaIden.Value = ParameterKota.MsSystemParameter_Value Then
            If ObjectAntiNull(BenfOwnerNonNasabah_KotaLainIden.Text) = False Then Throw New Exception("Identitas Beneficial Owner Non Nasabah: Kota/Kabupaten Lain harus diisi! ")
        End If
        'If ObjectAntiNull(BenfOwnerNasabah_KotaIden.Text) = True And ObjectAntiNull(BenfOwnerNasabah_kotaLainIden.Text) = True Then Throw New Exception("Identitas Beneficial Owner Non Nasabah: Kota/Kabupaten harus diisi salah satu saja ")
        'If ObjectAntiNull(BenfOwnerNasabah_ProvinsiIden.Text) = True And ObjectAntiNull(BenfOwnerNasabah_ProvinsiLainIden.Text) = True Then Throw New Exception("Identitas Beneficial Owner Non Nasabah: Provinsi harus diisi salah satu saja ")
        If ObjectAntiNull(BenfOwnerNonNasabah_ProvinsiIden.Text) = False Then Throw New Exception("Identitas Beneficial Owner Non Nasabah: Provinsi harus diisi! ")
        If hfBenfOwnerNonNasabah_ProvinsiIden.Value = ParameterProvinsi.MsSystemParameter_Value Then
            If ObjectAntiNull(BenfOwnerNonNasabah_ProvinsiLainIden.Text) = False Then Throw New Exception("Identitas Beneficial Owner Non Nasabah: Provinsi Lain harus diisi! ")
        End If
        If ObjectAntiNull(BenfOwnerNonNasabah_NomorIdentitas.Text) = False And Me.CBOBenfOwnerNonNasabah_JenisDokumen.SelectedIndex = -1 Then Throw New Exception("Identitas Beneficial Owner Non Nasabah: Nomor Identitas harus diisi lebih dari 2 karakter apabila jenis identitas diisi! ")
        If ObjectAntiNull(BenfOwnerNonNasabah_NomorIdentitas.Text) = True And BenfOwnerNonNasabah_NomorIdentitas.Text.Length < 2 Then Throw New Exception("Identitas Beneficial Owner Non Nasabah: Nomor Identitas harus diisi lebih dari 2 karakter! ")
        If BenfOwnerNonNasabah_NomorIdentitas.Text <> "" Then
            If Me.CBOBenfOwnerNonNasabah_JenisDokumen.SelectedIndex > 0 Then
                If Me.CBOBenfOwnerNonNasabah_JenisDokumen.SelectedValue = 1 Or Me.CBOBenfOwnerNonNasabah_JenisDokumen.SelectedValue = 2 Then
                    If Not Regex.Match(BenfOwnerNonNasabah_NomorIdentitas.Text.Trim(), "^[0-9]*$").Success Then
                        Throw New Exception("Nomor Identitas hanya boleh menggunakan angka!")
                    End If
                Else
                    If Not Regex.Match(BenfOwnerNonNasabah_NomorIdentitas.Text.Trim(), "^[a-zA-Z0-9]*$").Success Then
                        Throw New Exception("Nomor Identitas hanya boleh menggunakan huruf dan angka!")
                    End If
                End If
            Else
                Throw New Exception("Jenis Dokumen Beneficial Owner Non Nasabah harus diisi!")
            End If
        End If
    End Sub
#End Region
#End Region
#Region "TransactionType..."
    Sub TransactionSwiftOut()
        Using objIfti As IFTI = DataRepository.IFTIProvider.GetByPK_IFTI_ID(getIFTIPK)
            SafeDefaultValue = ""
            If Not IsNothing(objIfti) Then
                Me.MultiViewNasabahTypeAll.ActiveViewIndex = 0
                'umum
                Me.TxtUmum_LTDLN.Text = Safe(objIfti.LTDLNNo)
                Me.TxtUmum_LTDLNKoreksi.Text = Safe(objIfti.LTDLNNoKoreksi)
                Me.TxtUmum_TanggalLaporan.Text = FormatDate(Safe(objIfti.TanggalLaporan))
                Me.TxtUmum_PJKBankPelapor.Text = Safe(objIfti.NamaPJKBankPelapor)
                Me.TxtUmum_NamaPejabatPJKBankPelapor.Text = Safe(objIfti.NamaPejabatPJKBankPelapor)
                Me.RbUmum_JenisLaporan.SelectedValue = Safe(objIfti.JenisLaporan)

                'Identitas Pengirim

                'cekTipe Penyelenggara
                Dim senderType As Integer = objIfti.Sender_FK_IFTI_NasabahType_ID.GetValueOrDefault(1)
                Me.CboSwiftOutPengirim.SelectedValue = objIfti.Sender_FK_PJKBank_Type
                Select Case (senderType)
                    Case 1
                        'pengirimNasabahIndividu
                        Me.CboSwiftOutPengirim.SelectedValue = 1
                        Rb_IdenPengirimNas_TipePengirim.SelectedValue = 1
                        Rb_IdenPengirimNas_TipeNasabah.SelectedValue = 1
                        Me.trTipePengirim.Visible = True
                        Me.trTipeNasabah.Visible = True
                        Me.MultiViewSwiftOUtIdentitasPengirim.ActiveViewIndex = 0
                        Me.MultiViewSwiftOutJenisNasabah.ActiveViewIndex = 0
                        Me.MultiViewPengirimNasabah.ActiveViewIndex = 0
                        FieldSwiftOutSenderCase1()
                    Case 2
                        'pengirimNasabahKorp
                        Me.CboSwiftOutPengirim.SelectedValue = 1
                        Rb_IdenPengirimNas_TipePengirim.SelectedValue = 1
                        Rb_IdenPengirimNas_TipeNasabah.SelectedValue = 2
                        Me.trTipePengirim.Visible = True
                        Me.trTipeNasabah.Visible = True
                        Me.MultiViewSwiftOUtIdentitasPengirim.ActiveViewIndex = 0
                        Me.MultiViewSwiftOutJenisNasabah.ActiveViewIndex = 0
                        Me.MultiViewPengirimNasabah.ActiveViewIndex = 1
                        FieldSwiftOutSenderCase2()
                    Case 3
                        'PengirimNonNasabah
                        Me.CboSwiftOutPengirim.SelectedValue = 1
                        Rb_IdenPengirimNas_TipePengirim.SelectedValue = 2
                        Me.trTipePengirim.Visible = True
                        Me.MultiViewSwiftOUtIdentitasPengirim.ActiveViewIndex = 0
                        Me.MultiViewSwiftOutJenisNasabah.ActiveViewIndex = 1
                        FieldSwiftOutSenderCase3()
                    Case 4
                        'PenyelenggaraIndividu
                        Me.CboSwiftOutPengirim.SelectedValue = 2
                        Rb_IdenPengirimNas_TipeNasabah.SelectedValue = 1
                        Me.trTipePengirim.Visible = False
                        Me.trTipeNasabah.Visible = True
                        Me.MultiViewSwiftOUtIdentitasPengirim.ActiveViewIndex = 1
                        Me.MultiViewPengirimPenerus.ActiveViewIndex = 0
                        FieldSwiftOutSenderCase4()
                    Case 5
                        'PenyelenggaraKorp
                        Me.CboSwiftOutPengirim.SelectedValue = 2
                        Rb_IdenPengirimNas_TipeNasabah.SelectedValue = 2
                        Me.MultiViewSwiftOUtIdentitasPengirim.ActiveViewIndex = 1
                        Me.MultiViewPengirimPenerus.ActiveViewIndex = 1
                        Me.trTipePengirim.Visible = False
                        Me.trTipeNasabah.Visible = True
                        'Me.trSwiftOutPenerimaTipeNasabah.Visible = True
                        FieldSwiftOutSenderCase5()
                End Select

                'Beneficiary Owner
                'check if having Beneficiary Owner
                'check if FK_IFTI_BeneficialOwnerType_ID not nothing
                Rb_BOwnerNas_ApaMelibatkan.SelectedValue = objIfti.FK_IFTI_KeterlibatanBeneficialOwner_ID.GetValueOrDefault(2)
                If Rb_BOwnerNas_ApaMelibatkan.SelectedValue = 1 Then
                    Me.trBenfOwnerHubungan.Visible = True
                    Me.trBenfOwnerTipePengirim.Visible = True
                    Me.BenfOwnerHubunganPemilikDana.Text = objIfti.BeneficialOwner_HubunganDenganPemilikDana
                    Me.Rb_BOwnerNas_TipePengirim.SelectedValue = objIfti.FK_IFTI_BeneficialOwnerType_ID
                    Dim BOwnerID As Integer = objIfti.FK_IFTI_BeneficialOwnerType_ID
                    Select Case (BOwnerID)
                        Case 1
                            MultiViewSwiftOutBOwner.ActiveViewIndex = 0
                            FieldSwiftOutBOwnerNasabah()
                        Case 2
                            MultiViewSwiftOutBOwner.ActiveViewIndex = 1
                            FieldSwiftOutBOwnerNonNasabah()
                    End Select
                End If



                'Transaksi
                Me.Transaksi_tanggal.Text = FormatDate(objIfti.TanggalTransaksi)
                Me.Transaksi_waktutransaksi.Text = objIfti.TimeIndication
                Me.Transaksi_Sender.Text = objIfti.SenderReference
                Me.Transaksi_BankOperationCode.Text = objIfti.BankOperationCode
                Me.Transaksi_InstructionCode.Text = objIfti.InstructionCode
                Me.Transaksi_kantorCabangPengirim.Text = objIfti.KantorCabangPenyelengaraPengirimAsal
                Me.Transaksi_kodeTipeTransaksi.Text = objIfti.TransactionCode
                Me.Transaksi_ValueTanggalTransaksi.Text = FormatDate(objIfti.ValueDate_TanggalTransaksi)
                Me.Transaksi_nilaitransaksi.Text = objIfti.ValueDate_NilaiTransaksi
                'Using objCurrency As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(objIfti.ValueDate_FK_Currency_ID)
                '    If Not IsNothing(objCurrency) Then
                '        Me.Transaksi_MataUangTransaksi.Text = objCurrency.MsCurrency_Code
                '        hfTransaksi_MataUangTransaksi.Value = objCurrency.Pk_MsCurrency_Id
                '    End If
                'End Using
                'Dim objCurrency As TList(Of MsCurrency) = DataRepository.MsCurrencyProvider.GetPaged(MsCurrencyColumn.Pk_MsCurrency_Id.ToString & _
                '                                                             " like '%" & objIfti.ValueDate_FK_Currency_ID.ToString & "%'", "", 0, Integer.MaxValue, 0)
                'Me.Transaksi_MataUangTransaksi.Text = objCurrency(0).CurrencyCode & "-" & objCurrency(0).CurrencyName
                'hfTransaksi_MataUangTransaksi.Value = objCurrency(0).Pk_MsCurrency_Id
                Using objCurrency As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(objIfti.ValueDate_FK_Currency_ID.GetValueOrDefault(0))
                    If Not IsNothing(objCurrency) Then
                        Me.Transaksi_MataUangTransaksi.Text = objCurrency.Code
                        hfTransaksi_MataUangTransaksi.Value = objCurrency.IdCurrency

                    End If
                End Using
                Me.Transaksi_MataUangTransaksiLain.Text = objIfti.ValueDate_CurrencyLainnya
                Me.Transaksi_AmountdalamRupiah.Text = objIfti.ValueDate_NilaiTransaksiIDR
                'Using objCurrency2 As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(objIfti.Instructed_Currency)
                '    If Not IsNothing(objCurrency) Then
                '        Me.Transaksi_currency.Text = objCurrency2.MsCurrency_Code
                '        hfTransaksi_currency.Value = objCurrency2.Pk_MsCurrency_Id
                '    End If
                'End Using
                '               Dim objCurrency2 As TList(Of MsCurrency) = DataRepository.MsCurrencyProvider.GetPaged(MsCurrencyColumn.Pk_MsCurrency_Id.ToString & _
                '" like '%" & objIfti.Instructed_Currency.ToString & "%'", "", 0, Integer.MaxValue, 0)
                '               Me.Transaksi_currency.Text = objCurrency2(0).CurrencyCode & "-" & objCurrency(0).CurrencyName
                '               hfTransaksi_currency.Value = objCurrency2(0).Pk_MsCurrency_Id
                Using objCurrency As MsCurrency = DataRepository.MsCurrencyProvider.GetByIdCurrency(objIfti.Instructed_Currency.GetValueOrDefault(0))
                    If Not IsNothing(objCurrency) Then
                        Me.Transaksi_currency.Text = objCurrency.Code
                        hfTransaksi_currency.Value = objCurrency.IdCurrency
                    End If
                End Using
                Me.Transaksi_currencyLain.Text = objIfti.Instructed_CurrencyLainnya
                If Not IsNothing(objIfti.Instructed_Amount) Then
                    Me.Transaksi_instructedAmount.Text = objIfti.Instructed_Amount
                End If
                If Not IsNothing(objIfti.ExchangeRate) Then
                    Me.Transaksi_nilaiTukar.Text = objIfti.ExchangeRate
                End If


                Me.Transaksi_sendingInstitution.Text = objIfti.SendingInstitution
                Me.Transaksi_TujuanTransaksi.Text = objIfti.TujuanTransaksi
                Me.Transaksi_SumberPenggunaanDana.Text = objIfti.SumberPenggunaanDana


                'Informasi Lainnya

                Me.InformasiLainnya_Sender.Text = objIfti.InformationAbout_SenderCorrespondent
                InformasiLainnya_receiver.Text = objIfti.InformationAbout_ReceiverCorrespondent
                InformasiLainnya_thirdReimbursement.Text = objIfti.InformationAbout_Thirdreimbursementinstitution
                InformasiLainnya_intermediary.Text = objIfti.InformationAbout_IntermediaryInstitution
                InformasiLainnya_Remittance.Text = objIfti.RemittanceInformation
                InformasiLainnya_SenderToReceiver.Text = objIfti.SendertoReceiverInformation
                InformasiLainnya_RegulatoryReport.Text = objIfti.RegulatoryReporting
                InformasiLainnya_EnvelopeContents.Text = objIfti.EnvelopeContents
            End If
        End Using

    End Sub
#End Region

#End Region
#Region "Receiver"
    Private Sub LoadReceiver()
        Me.GridDataView.DataSource = ReceiverDataTable
        GridDataView.DataBind()

        'lnkBtnDeleteAllChronology.Enabled = False
        'If (ReceiverDataTable.Rows.Count > 0) Then
        '    lnkBtnDeleteAllChronology.Enabled = True
        'End If

        'lblTotalChronologyValue.Text = ReceiverDataTable.Rows.Count.ToString
    End Sub

    Private Sub AddReceiver()
        Try
            If ValidateReceiver() Then
                Dim dr As Data.DataRow = Nothing
                Dim tipepenerima As Integer = RBPenerimaNasabah_TipePengirim.SelectedValue
                Dim tipenasabah As Integer
                Dim nasabahtype As Integer

                dr = ReceiverDataTable.NewRow
                dr("FK_IFTI_ID") = getIFTIPK

                If tipepenerima = 1 Then
                    tipenasabah = Rb_IdenPenerimaNas_TipeNasabah.SelectedValue
                    If tipenasabah = 1 Then
                        nasabahtype = 1
                        dr("FK_IFTI_NasabahType_ID") = nasabahtype

                        dr("Beneficiary_Nasabah_INDV_NoRekening") = txtPenerima_rekening.Text
                        dr("Beneficiary_Nasabah_INDV_NamaLengkap") = txtPenerimaNasabah_IND_nama.Text
                        If txtPenerimaNasabah_IND_TanggalLahir.Text.Trim <> "" Then
                            dr("Beneficiary_Nasabah_INDV_TanggalLahir") = txtPenerimaNasabah_IND_TanggalLahir.Text.Trim() 'Sahassa.aml.Commonly.ConvertToDate("dd-MM-yyyy", txtPenerimaNasabah_IND_TanggalLahir.Text.Trim())
                        End If
                        dr("Beneficiary_Nasabah_INDV_KewargaNegaraan") = RbPenerimaNasabah_IND_Warganegara.SelectedValue
                        If RbPenerimaNasabah_IND_Warganegara.SelectedValue = 2 Then
                            If txtPenerimaNasabah_IND_negara.Text <> "" Then
                                dr("Beneficiary_Nasabah_INDV_Negara") = hfPenerimaNasabah_IND_negara.Value
                            Else
                                dr("Beneficiary_Nasabah_INDV_Negara") = DBNull.Value
                            End If

                            dr("Beneficiary_Nasabah_INDV_NegaraLainnya") = txtPenerimaNasabah_IND_negaraLain.Text
                        End If

                        dr("Beneficiary_Nasabah_INDV_ID_Alamat_Iden") = txtPenerimaNasabah_IND_alamatIden.Text
                        dr("Beneficiary_Nasabah_INDV_ID_NegaraBagian") = txtPenerimaNasabah_IND_negaraBagian.Text
                        If txtPenerimaNasabah_IND_negaraIden.Text <> "" Then
                            dr("Beneficiary_Nasabah_INDV_ID_Negara") = hfPenerimaNasabah_IND_negaraIden.Value
                        End If

                        dr("Beneficiary_Nasabah_INDV_ID_NegaraLainnya") = txtPenerimaNasabah_IND_negaraLainIden.Text
                        If CBOPenerimaNasabah_IND_JenisIden.SelectedIndex <> 0 Then
                            dr("Beneficiary_Nasabah_INDV_FK_IFTI_IDType") = CBOPenerimaNasabah_IND_JenisIden.SelectedValue
                        End If
                        dr("Beneficiary_Nasabah_INDV_NomorID") = txtPenerimaNasabah_IND_NomorIden.Text
                        If txtPenerimaNasabah_IND_NilaiTransaksiKeuangan.Text <> "" Then
                            dr("Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan") = txtPenerimaNasabah_IND_NilaiTransaksiKeuangan.Text
                        End If


                        ReceiverDataTable.Rows.Add(dr)
                        txtPenerima_rekening.Text = ""
                        txtPenerimaNasabah_IND_nama.Text = ""
                        txtPenerimaNasabah_IND_TanggalLahir.Text = ""
                        RbPenerimaNasabah_IND_Warganegara.SelectedIndex = 0
                        Me.txtPenerimaNasabah_IND_negara.Text = ""
                        hfPenerimaNasabah_IND_negara.Value = ""
                        txtPenerimaNasabah_IND_negaraLain.Text = ""
                        txtPenerimaNasabah_IND_alamatIden.Text = ""
                        txtPenerimaNasabah_IND_negaraBagian.Text = ""
                        hfPenerimaNasabah_IND_negaraIden.Value = ""
                        txtPenerimaNasabah_IND_negaraIden.Text = ""
                        txtPenerimaNasabah_IND_negaraLainIden.Text = ""
                        CBOPenerimaNasabah_IND_JenisIden.SelectedIndex = 0
                        txtPenerimaNasabah_IND_NomorIden.Text = ""
                        txtPenerimaNasabah_IND_NilaiTransaksiKeuangan.Text = ""
                    Else
                        nasabahtype = 2
                        dr("FK_IFTI_NasabahType_ID") = nasabahtype

                        dr("Beneficiary_Nasabah_corp_NoRekening") = txtPenerima_rekening.Text

                        dr("Beneficiary_Nasabah_CORP_NamaKorporasi") = txtPenerimaNasabah_Korp_namaKorp.Text
                        dr("Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya") = txtPenerimaNasabah_Korp_BentukBadanUsahaLainnya.Text
                        If (CBOPenerimaNasabah_Korp_BentukBadanUsahaLain.SelectedIndex <> 0) Then
                            dr("Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id") = CInt(CBOPenerimaNasabah_Korp_BentukBadanUsahaLain.SelectedValue)
                        End If

                        If ObjectAntiNull(txtPenerimaNasabah_Korp_BidangUsahaKorp.Text) = True Then
                            dr("Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id") = hfPenerimaNasabah_Korp_BidangUsahaKorp.Value
                        End If

                        dr("Beneficiary_Nasabah_CORP_BidangUsahaLainnya") = txtPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Text
                        dr("Beneficiary_Nasabah_CORP_AlamatLengkap") = txtPenerimaNasabah_Korp_AlamatKorp.Text
                        If txtPenerimaNasabah_Korp_NegaraKorp.Text <> "" Then
                            dr("Beneficiary_Nasabah_CORP_ID_Negara") = hfPenerimaNasabah_Korp_NegaraKorp.Value
                        End If

                        dr("Beneficiary_Nasabah_CORP_ID_NegaraBagian") = txtPenerimaNasabah_Korp_negaraKota.Text
                        dr("Beneficiary_Nasabah_CORP_ID_NegaraLainnya") = txtPenerimaNasabah_Korp_NegaraLainnyaKorp.Text
                        If txtPenerimaNasabah_Korp_NilaiTransaksiKeuangan.Text <> "" Then
                            dr("Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan") = txtPenerimaNasabah_Korp_NilaiTransaksiKeuangan.Text
                        End If


                        ReceiverDataTable.Rows.Add(dr)

                        txtPenerima_rekening.Text = ""

                        txtPenerimaNasabah_Korp_namaKorp.Text = ""
                        txtPenerimaNasabah_Korp_BentukBadanUsahaLainnya.Text = ""
                        CBOPenerimaNasabah_Korp_BentukBadanUsahaLain.SelectedIndex = 0
                        hfPenerimaNasabah_Korp_BidangUsahaKorp.Value = Nothing
                        txtPenerimaNasabah_Korp_BidangUsahaKorp.Text = ""
                        txtPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Text = ""
                        txtPenerimaNasabah_Korp_AlamatKorp.Text = ""
                        hfPenerimaNasabah_Korp_NegaraKorp.Value = Nothing
                        txtPenerimaNasabah_Korp_NegaraKorp.Text = ""
                        txtPenerimaNasabah_Korp_negaraKota.Text = ""
                        txtPenerimaNasabah_Korp_NegaraLainnyaKorp.Text = ""
                        txtPenerimaNasabah_Korp_NilaiTransaksiKeuangan.Text = ""

                    End If
                ElseIf tipepenerima = 2 Then
                    nasabahtype = 3
                    dr("FK_IFTI_NasabahType_ID") = nasabahtype

                    dr("Beneficiary_NonNasabah_NoRekening") = txtPenerima_rekening.Text

                    dr("Beneficiary_NonNasabah_NamaBank") = txtPenerimaNonNasabah_namabank.Text
                    dr("Beneficiary_NonNasabah_NamaLengkap") = txtPenerimaNonNasabah_Nama.Text
                    dr("Beneficiary_NonNasabah_ID_Alamat") = txtPenerimaNonNasabah_Alamat.Text
                    If txtPenerimaNonNasabah_Negara.Text <> "" Then
                        dr("Beneficiary_NonNasabah_ID_Negara") = hfPenerimaNonNasabah_Negara.Value
                    End If
                    dr("Beneficiary_NonNasabah_ID_NegaraLainnya") = txtPenerimaNonNasabah_negaraLain.Text
                    If txtPenerimaNonNasabah_nilaiTransaksiKeuangan.Text <> "" Then
                        dr("Beneficiary_NonNasabah_NilaiTransaksikeuangan") = txtPenerimaNonNasabah_nilaiTransaksiKeuangan.Text
                    End If


                    ReceiverDataTable.Rows.Add(dr)
                    txtPenerima_rekening.Text = ""

                    txtPenerimaNonNasabah_namabank.Text = ""
                    txtPenerimaNonNasabah_Nama.Text = ""
                    txtPenerimaNonNasabah_Alamat.Text = ""
                    hfPenerimaNonNasabah_Negara.Value = ""
                    txtPenerimaNonNasabah_Negara.Text = ""
                    txtPenerimaNonNasabah_negaraLain.Text = ""
                    txtPenerimaNonNasabah_nilaiTransaksiKeuangan.Text = ""

                End If

                LoadReceiver()
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    Private Sub EditReceiver(ByVal index As Integer)
        Try
            If ValidateReceiver() Then
                Dim dr As Data.DataRow = Nothing

                dr = ReceiverDataTable.Rows(index)
                dr.BeginEdit()

                Dim tipepenerima As Integer = RBPenerimaNasabah_TipePengirim.SelectedValue

                Dim tipenasabah As Integer

                Dim nasabahtype As Integer

                dr("FK_IFTI_ID") = getIFTIPK

                If tipepenerima = 1 Then
                    tipenasabah = Rb_IdenPenerimaNas_TipeNasabah.SelectedValue
                    If tipenasabah = 1 Then
                        nasabahtype = 1
                        dr("FK_IFTI_NasabahType_ID") = nasabahtype

                        dr("Beneficiary_Nasabah_INDV_NoRekening") = txtPenerima_rekening.Text
                        dr("Beneficiary_Nasabah_INDV_NamaLengkap") = txtPenerimaNasabah_IND_nama.Text
                        If txtPenerimaNasabah_IND_TanggalLahir.Text.Trim() <> "" Then
                            dr("Beneficiary_Nasabah_INDV_TanggalLahir") = txtPenerimaNasabah_IND_TanggalLahir.Text.Trim() 'Sahassa.aml.Commonly.ConvertToDate("dd-MM-yyyy", txtPenerimaNasabah_IND_TanggalLahir.Text.Trim())
                        Else
                            dr("Beneficiary_Nasabah_INDV_TanggalLahir") = DBNull.Value
                        End If
                        If RbPenerimaNasabah_IND_Warganegara.SelectedIndex <> 0 Then
                            dr("Beneficiary_Nasabah_INDV_KewargaNegaraan") = RbPenerimaNasabah_IND_Warganegara.SelectedValue
                        End If

                        If RbPenerimaNasabah_IND_Warganegara.SelectedValue = 2 Then
                            dr("Beneficiary_Nasabah_INDV_Negara") = hfPenerimaNasabah_IND_negara.Value
                            dr("Beneficiary_Nasabah_INDV_NegaraLainnya") = txtPenerimaNasabah_IND_negaraLain.Text
                        Else
                            dr("Beneficiary_Nasabah_INDV_Negara") = DBNull.Value
                            dr("Beneficiary_Nasabah_INDV_NegaraLainnya") = ""
                        End If
                        dr("Beneficiary_Nasabah_INDV_NegaraLainnya") = txtPenerimaNasabah_IND_negaraLain.Text
                        dr("Beneficiary_Nasabah_INDV_ID_Alamat_Iden") = txtPenerimaNasabah_IND_alamatIden.Text
                        dr("Beneficiary_Nasabah_INDV_ID_NegaraBagian") = txtPenerimaNasabah_IND_negaraBagian.Text
                        dr("Beneficiary_Nasabah_INDV_ID_Negara") = hfPenerimaNasabah_IND_negaraIden.Value
                        dr("Beneficiary_Nasabah_INDV_ID_NegaraLainnya") = txtPenerimaNasabah_IND_negaraLainIden.Text
                        If CBOPenerimaNasabah_IND_JenisIden.SelectedIndex <> 0 Then
                            dr("Beneficiary_Nasabah_INDV_FK_IFTI_IDType") = CBOPenerimaNasabah_IND_JenisIden.SelectedValue
                        Else
                            dr("Beneficiary_Nasabah_INDV_FK_IFTI_IDType") = DBNull.Value
                        End If

                        dr("Beneficiary_Nasabah_INDV_NomorID") = txtPenerimaNasabah_IND_NomorIden.Text
                        If txtPenerimaNasabah_IND_NilaiTransaksiKeuangan.Text <> "" Then
                            dr("Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan") = txtPenerimaNasabah_IND_NilaiTransaksiKeuangan.Text
                        End If


                        ReceiverDataTable.AcceptChanges()
                        txtPenerima_rekening.Text = ""
                        txtPenerimaNasabah_IND_nama.Text = ""
                        txtPenerimaNasabah_IND_TanggalLahir.Text = ""
                        RbPenerimaNasabah_IND_Warganegara.SelectedIndex = 0
                        hfPenerimaNasabah_IND_negara.Value = ""
                        txtPenerimaNasabah_IND_negara.Text = ""
                        txtPenerimaNasabah_IND_negaraLain.Text = ""
                        txtPenerimaNasabah_IND_alamatIden.Text = ""
                        txtPenerimaNasabah_IND_negaraBagian.Text = ""
                        hfPenerimaNasabah_IND_negaraIden.Value = ""
                        txtPenerimaNasabah_IND_negaraIden.Text = ""
                        txtPenerimaNasabah_IND_negaraLainIden.Text = ""
                        CBOPenerimaNasabah_IND_JenisIden.SelectedIndex = 0
                        txtPenerimaNasabah_IND_NomorIden.Text = ""
                        txtPenerimaNasabah_IND_NilaiTransaksiKeuangan.Text = ""
                        Me.ImageButton_savePenerimaInd.Visible = False
                        Me.ImageButton_AddPenerimaInd.Visible = True
                    Else
                        nasabahtype = 2
                        dr("FK_IFTI_NasabahType_ID") = nasabahtype

                        dr("Beneficiary_Nasabah_INDV_NoRekening") = txtPenerima_rekening.Text

                        dr("Beneficiary_Nasabah_CORP_NamaKorporasi") = txtPenerimaNasabah_Korp_namaKorp.Text
                        dr("Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya") = txtPenerimaNasabah_Korp_BentukBadanUsahaLainnya.Text
                        If CBOPenerimaNasabah_Korp_BentukBadanUsahaLain.SelectedIndex > 0 Then
                            dr("Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id") = CBOPenerimaNasabah_Korp_BentukBadanUsahaLain.SelectedValue
                        End If
                        If ObjectAntiNull(hfPenerimaNasabah_Korp_BidangUsahaKorp.Value) = True Then
                            dr("Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id") = hfPenerimaNasabah_Korp_BidangUsahaKorp.Value
                        Else
                            dr("Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id") = DBNull.Value
                        End If
                        dr("Beneficiary_Nasabah_CORP_BidangUsahaLainnya") = txtPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Text
                        dr("Beneficiary_Nasabah_CORP_AlamatLengkap") = txtPenerimaNasabah_Korp_AlamatKorp.Text
                        If ObjectAntiNull(hfPenerimaNasabah_Korp_NegaraKorp.Value) = True Then
                            dr("Beneficiary_Nasabah_CORP_ID_Negara") = hfPenerimaNasabah_Korp_NegaraKorp.Value
                        Else
                            dr("Beneficiary_Nasabah_CORP_ID_Negara") = DBNull.Value
                        End If

                        dr("Beneficiary_Nasabah_CORP_ID_NegaraBagian") = txtPenerimaNasabah_Korp_negaraKota.Text
                        dr("Beneficiary_Nasabah_CORP_ID_NegaraLainnya") = txtPenerimaNasabah_Korp_NegaraLainnyaKorp.Text
                        If txtPenerimaNasabah_Korp_NilaiTransaksiKeuangan.Text <> "" Then
                            dr("Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan") = txtPenerimaNasabah_Korp_NilaiTransaksiKeuangan.Text
                        End If


                        ReceiverDataTable.AcceptChanges()

                        txtPenerima_rekening.Text = ""

                        txtPenerimaNasabah_Korp_namaKorp.Text = ""
                        txtPenerimaNasabah_Korp_BentukBadanUsahaLainnya.Text = ""
                        CBOPenerimaNasabah_Korp_BentukBadanUsahaLain.SelectedIndex = 0
                        hfPenerimaNasabah_Korp_BidangUsahaKorp.Value = ""
                        txtPenerimaNasabah_Korp_BidangUsahaKorp.Text = ""
                        txtPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Text = ""
                        txtPenerimaNasabah_Korp_AlamatKorp.Text = ""
                        hfPenerimaNasabah_Korp_NegaraKorp.Value = ""
                        txtPenerimaNasabah_Korp_NegaraKorp.Text = ""
                        txtPenerimaNasabah_Korp_negaraKota.Text = ""
                        txtPenerimaNasabah_Korp_NegaraLainnyaKorp.Text = ""
                        txtPenerimaNasabah_Korp_NilaiTransaksiKeuangan.Text = ""
                        Me.ImageButton_savePenerimaKorp.Visible = False
                        Me.ImageButton_AddPenerimaKorp.Visible = True
                    End If
                ElseIf tipepenerima = 2 Then
                    nasabahtype = 3
                    dr("FK_IFTI_NasabahType_ID") = nasabahtype

                    dr("Beneficiary_Nasabah_INDV_NoRekening") = txtPenerima_rekening.Text

                    dr("Beneficiary_NonNasabah_NamaBank") = txtPenerimaNonNasabah_namabank.Text
                    dr("Beneficiary_NonNasabah_NamaLengkap") = txtPenerimaNonNasabah_Nama.Text
                    dr("Beneficiary_NonNasabah_ID_Alamat") = txtPenerimaNonNasabah_Alamat.Text
                    If ObjectAntiNull(txtPenerimaNonNasabah_Negara.Text) = True Then
                        dr("Beneficiary_NonNasabah_ID_Negara") = hfPenerimaNonNasabah_Negara.Value
                    Else
                        dr("Beneficiary_NonNasabah_ID_Negara") = DBNull.Value
                    End If

                    dr("Beneficiary_NonNasabah_ID_NegaraLainnya") = txtPenerimaNonNasabah_negaraLain.Text
                    If txtPenerimaNonNasabah_nilaiTransaksiKeuangan.Text <> "" Then
                        dr("Beneficiary_NonNasabah_NilaiTransaksikeuangan") = txtPenerimaNonNasabah_nilaiTransaksiKeuangan.Text
                    End If


                    ReceiverDataTable.AcceptChanges()
                    txtPenerima_rekening.Text = ""

                    txtPenerimaNonNasabah_namabank.Text = ""
                    txtPenerimaNonNasabah_Nama.Text = ""
                    txtPenerimaNonNasabah_Alamat.Text = ""
                    hfPenerimaNonNasabah_Negara.Value = ""
                    txtPenerimaNonNasabah_Negara.Text = ""
                    txtPenerimaNonNasabah_negaraLain.Text = ""
                    txtPenerimaNonNasabah_nilaiTransaksiKeuangan.Text = ""
                    Me.ImageButton_savePenerimaNonNasabah.Visible = False
                    Me.ImageButton_AddPenerimaNonNasabah.Visible = True
                End If

                ReceiverDataTable.AcceptChanges()

                GridDataView.Enabled = True
                getIFTIBeneficiaryTempPK = Nothing
                LoadReceiver()
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
#End Region
#Region "Event"
#Region "Browse"
    Protected Sub imgButtonPengirimNasnegara_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButtonPengirimNasnegara.Click
        Try
            If Session("PickerNegara.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
                TxtIdenPengirimNas_Ind_negara.Text = strData(1)
                Me.hfIdenPengirimNas_Ind_negara.Value = strData(0)
                Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
                If Me.hfIdenPengirimNas_Ind_negara.Value = ParameterNegara.MsSystemParameter_Value Then
                    Me.PengirimIndNegaraLain.Visible = True
                Else
                    Me.PengirimIndNegaraLain.Visible = False
                End If
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButton_PengirimPenerus_IND_Negara_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_PengirimPenerus_IND_Negara.Click
        Try
            If Session("PickerNegara.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
                TxtPengirimPenerus_IND_negara.Text = strData(1)
                hfPengirimPenerus_IND_negara.Value = strData(0)
                Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
                If hfPengirimPenerus_IND_negara.Value = ParameterNegara.MsSystemParameter_Value Then
                    Me.PengirimPenerusIndNegaraLain.Visible = True
                Else
                    Me.PengirimPenerusIndNegaraLain.Visible = False
                End If
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub


    Protected Sub imgButton_PenerimaNas_IND_negara_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_PenerimaNasabah_IND_negara.Click
        Try
            If Session("PickerNegara.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
                txtPenerimaNasabah_IND_negara.Text = strData(1)
                hfPenerimaNasabah_IND_negara.Value = strData(0)
                Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
                If hfPenerimaNasabah_IND_negara.Value = ParameterNegara.MsSystemParameter_Value Then
                    Me.PenerimaIndNegaraLain.Visible = True
                Else
                    Me.PenerimaIndNegaraLain.Visible = False
                End If
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub


    'Protected Sub imgButton_PenerimaNasabah_IND_negaraIden_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_PenerimaNasabah_IND_negaraIden.Click
    '    Try
    '        If Session("PickerNegara.Data") IsNot Nothing Then
    '            Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
    '            txtPenerimaNasabah_IND_negaraIden.Text = strData(1)
    '            hfPenerimaNasabah_IND_negaraIden.Value = strData(0)
    '        End If
    '    Catch ex As Exception
    '        'LogError(ex)
    '        Me.cvalPageErr.IsValid = False
    '        Me.cvalPageErr.ErrorMessage = ex.Message
    '    End Try
    'End Sub

    Protected Sub ImageButton27_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton27.Click
        Try
            If Session("PickerNegara.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
                txtPenerimaNonNasabah_Negara.Text = strData(1)
                hfPenerimaNonNasabah_Negara.Value = strData(0)
                Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
                If hfPenerimaNonNasabah_Negara.Value = ParameterNegara.MsSystemParameter_Value Then
                    Me.TrPenerimaNonNasabah_negaraLain.Visible = True
                Else
                    Me.TrPenerimaNonNasabah_negaraLain.Visible = False
                End If
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButtonPengirimNasKotaDom_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButtonPengirimNasKotaDom.Click
        Try
            If Session("PickerKotaKab.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
                TxtIdenPengirimNas_Ind_Kota.Text = strData(1)
                hfIdenPengirimNas_Ind_Kota_dom.Value = strData(0)

                hfIdenPengirimNas_Ind_provinsi_dom.Value = IFTIBLL.GetProvinceIDByKotaKab(strData(0))
                TxtIdenPengirimNas_Ind_provinsi.Text = IFTIBLL.GetProvincetextByKotaKab(strData(0))


                Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
                If hfIdenPengirimNas_Ind_Kota_dom.Value = ParameterKota.MsSystemParameter_Value Then
                    Me.TrIdenPengirimNas_Ind_kotalain.Visible = True

                Else
                    Me.TrIdenPengirimNas_Ind_kotalain.Visible = False
                End If
                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.KabupatenKota)
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButtonPengirimNasKotaIden_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButtonPengirimNasKotaIden.Click
        Try
            If Session("PickerKotaKab.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
                TxtIdenPengirimNas_Ind_kotaIdentitas.Text = strData(1)
                hfIdenPengirimNas_Ind_kotaIdentitas.Value = strData(0)

                hfIdenPengirimNas_Ind_ProvinsiIden.Value = IFTIBLL.GetProvinceIDByKotaKab(strData(0))
                TxtIdenPengirimNas_Ind_ProvinsiIden.Text = IFTIBLL.GetProvincetextByKotaKab(strData(0))


                Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
                If hfIdenPengirimNas_Ind_kotaIdentitas.Value = ParameterKota.MsSystemParameter_Value Then
                    Me.TrIdenPengirimNas_Ind_kotalain.Visible = True
                Else
                    Me.TrIdenPengirimNas_Ind_kotalain.Visible = False
                End If
                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.KabupatenKota)
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub imgButtonPengirimNasKorp_kotaLengkap_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButtonPengirimNasKorp_kotaLengkap.Click

        Try
            If Session("PickerKotaKab.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
                TxtIdenPengirimNas_Corp_kotakorp.Text = strData(1)
                hfIdenPengirimNas_Corp_kotakorp.Value = strData(0)

                hfIdenPengirimNas_Corp_provKorp.Value = IFTIBLL.GetProvinceIDByKotaKab(strData(0))
                TxtIdenPengirimNas_Corp_provKorp.Text = IFTIBLL.GetProvincetextByKotaKab(strData(0))


                Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
                If hfIdenPengirimNas_Corp_kotakorp.Value = ParameterKota.MsSystemParameter_Value Then
                    Me.TrIdenPengirimNas_Corp_kotakorplain.Visible = True
                Else
                    Me.TrIdenPengirimNas_Corp_kotakorplain.Visible = False
                End If
                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.KabupatenKota)
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub


    Protected Sub imgButton_PengirimNonNas_Kota_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_PengirimNonNas_Kota.Click

        Try
            If Session("PickerKotaKab.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
                TxtPengirimNonNasabah_kotaIden.Text = strData(1)
                hfPengirimNonNasabah_kotaIden.Value = strData(0)

                hfPengirimNonNasabah_ProvIden.Value = IFTIBLL.GetProvinceIDByKotaKab(strData(0))
                TxtPengirimNonNasabah_ProvIden.Text = IFTIBLL.GetProvincetextByKotaKab(strData(0))


                Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
                If hfPengirimNonNasabah_kotaIden.Value = ParameterKota.MsSystemParameter_Value Then
                    Me.TrPengirimNonNasabah_KoaLainIden.Visible = True
                Else
                    Me.TrPengirimNonNasabah_KoaLainIden.Visible = False
                End If
                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.KabupatenKota)
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButton_PengirimPenerus_IND_KotaDom_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_PengirimPenerus_IND_KotaDom.Click
        Try
            If Session("PickerKotaKab.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
                TxtPengirimPenerus_IND_kotaDom.Text = strData(1)
                hfPengirimPenerus_IND_kotaDom.Value = strData(0)


                hfPengirimPenerus_IND_ProvDom.Value = IFTIBLL.GetProvinceIDByKotaKab(strData(0))
                TxtPengirimPenerus_IND_ProvDom.Text = IFTIBLL.GetProvincetextByKotaKab(strData(0))


                Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
                If hfPengirimPenerus_IND_kotaDom.Value = ParameterKota.MsSystemParameter_Value Then
                    Me.TrPengirimPenerus_IND_kotaLainDom.Visible = True
                Else
                    Me.TrPengirimPenerus_IND_kotaLainDom.Visible = False
                End If
                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.KabupatenKota)
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub


    Protected Sub imgButton_PengirimPenerus_IND_KotaIden_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_PengirimPenerus_IND_KotaIden.Click

        Try
            If Session("PickerKotaKab.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
                TxtPengirimPenerus_IND_kotaIden.Text = strData(1)
                hfPengirimPenerus_IND_kotaIden.Value = strData(0)

                hfPengirimPenerus_IND_ProvIden.Value = IFTIBLL.GetProvinceIDByKotaKab(strData(0))
                TxtPengirimPenerus_IND_ProvIden.Text = IFTIBLL.GetProvincetextByKotaKab(strData(0))


                Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
                If hfPengirimPenerus_IND_kotaIden.Value = ParameterKota.MsSystemParameter_Value Then
                    Me.TrPengirimPenerus_IND_kotaLainDom.Visible = True
                Else
                    Me.TrPengirimPenerus_IND_kotaLainDom.Visible = False
                End If
                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.KabupatenKota)
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButton_PengirimPenerus_Korp_KotaLengkap_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_PengirimPenerus_Korp_KotaLengkap.Click
        Try
            If Session("PickerKotaKab.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
                txtPengirimPenerus_Korp_kotakorp.Text = strData(1)
                hfPengirimPenerus_Korp_kotakorp.Value = strData(0)

                hfPengirimPenerus_Korp_ProvinsiKorp.Value = IFTIBLL.GetProvinceIDByKotaKab(strData(0))
                txtPengirimPenerus_Korp_ProvinsiKorp.Text = IFTIBLL.GetProvincetextByKotaKab(strData(0))


                Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
                If hfPengirimPenerus_Korp_kotakorp.Value = ParameterKota.MsSystemParameter_Value Then
                    Me.TrtxtPengirimPenerus_Korp_kotalainnyakorp.Visible = True
                Else
                    Me.TrtxtPengirimPenerus_Korp_kotalainnyakorp.Visible = False
                End If
                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.KabupatenKota)
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButton_BOwnerNas_KotaIden_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_BOwnerNas_KotaIden.Click

        Try
            If Session("PickerKotaKab.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
                BenfOwnerNasabah_KotaIden.Text = strData(1)
                hfBenfOwnerNasabah_KotaIden.Value = strData(0)

                hfBenfOwnerNasabah_ProvinsiIden.Value = IFTIBLL.GetProvinceIDByKotaKab(strData(0))
                BenfOwnerNasabah_ProvinsiIden.Text = IFTIBLL.GetProvincetextByKotaKab(strData(0))


                Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
                If hfBenfOwnerNasabah_KotaIden.Value = ParameterKota.MsSystemParameter_Value Then
                    Me.TrBenfOwnerNasabah_kotaLainIden.Visible = True
                Else
                    Me.TrBenfOwnerNasabah_kotaLainIden.Visible = False
                End If
                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.KabupatenKota)
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButton_BOwnerNonNas_kota_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_BOwnerNonNas_kota.Click

        Try
            If Session("PickerKotaKab.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerKotaKab.Data")).Split(";")
                BenfOwnerNonNasabah_KotaIden.Text = strData(1)
                hfBenfOwnerNonNasabah_KotaIden.Value = strData(0)

                hfBenfOwnerNonNasabah_ProvinsiIden.Value = IFTIBLL.GetProvinceIDByKotaKab(strData(0))
                BenfOwnerNonNasabah_ProvinsiIden.Text = IFTIBLL.GetProvincetextByKotaKab(strData(0))


                Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
                If hfBenfOwnerNonNasabah_KotaIden.Value = ParameterKota.MsSystemParameter_Value Then
                    Me.TrBenfOwnerNonNasabah_KotaLainIden.Visible = True
                Else
                    Me.TrBenfOwnerNonNasabah_KotaLainIden.Visible = False
                End If
                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.KabupatenKota)
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButtonPengirimNasProvinsiDom_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButtonPengirimNasProvinsiDom.Click
        Try
            If Session("PickerProvinsi.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                TxtIdenPengirimNas_Ind_provinsi.Text = strData(1)
                hfIdenPengirimNas_Ind_provinsi_dom.Value = strData(0)
                Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
                If hfIdenPengirimNas_Ind_provinsi_dom.Value = ParameterProvinsi.MsSystemParameter_Value Then
                    Me.TrIdenPengirimNas_Ind_provinsiLain.Visible = True
                Else
                    Me.TrIdenPengirimNas_Ind_provinsiLain.Visible = False
                End If
                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.Propinsi)
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButtonPengirimNasProvinsiIden_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButtonPengirimNasProvinsiIden.Click

        Try
            If Session("PickerProvinsi.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                TxtIdenPengirimNas_Ind_ProvinsiIden.Text = strData(1)
                hfIdenPengirimNas_Ind_ProvinsiIden.Value = strData(0)
                Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
                If hfIdenPengirimNas_Ind_ProvinsiIden.Value = ParameterProvinsi.MsSystemParameter_Value Then
                    Me.TrIdenPengirimNas_Ind_ProvinsilainIden.Visible = True
                Else
                    Me.TrIdenPengirimNas_Ind_ProvinsilainIden.Visible = False
                End If
                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.Propinsi)
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButtonPengirimNasKorp_ProvinsiLengkap_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButtonPengirimNasKorp_ProvinsiLengkap.Click

        Try
            If Session("PickerProvinsi.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                TxtIdenPengirimNas_Corp_provKorp.Text = strData(1)
                hfIdenPengirimNas_Corp_provKorp.Value = strData(0)
                Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
                If hfIdenPengirimNas_Corp_provKorp.Value = ParameterProvinsi.MsSystemParameter_Value Then
                    Me.TrIdenPengirimNas_Corp_provKorpLain.Visible = True
                Else
                    Me.TrIdenPengirimNas_Corp_provKorpLain.Visible = False
                End If
                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.Propinsi)
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub imgButton_PengirimNonNas__Provinsi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_PengirimNonNas__Provinsi.Click

        Try
            If Session("PickerProvinsi.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                TxtPengirimNonNasabah_ProvIden.Text = strData(1)
                hfPengirimNonNasabah_ProvIden.Value = strData(0)
                Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
                If hfPengirimNonNasabah_ProvIden.Value = ParameterProvinsi.MsSystemParameter_Value Then
                    Me.TrPengirimNonNasabah_ProvLainIden.Visible = True
                Else
                    Me.TrPengirimNonNasabah_ProvLainIden.Visible = False
                End If
                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.Propinsi)
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButton_PengirimPenerus_IND_ProvinsiDom_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_PengirimPenerus_IND_ProvinsiDom.Click

        Try
            If Session("PickerProvinsi.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                TxtPengirimPenerus_IND_ProvDom.Text = strData(1)
                hfPengirimPenerus_IND_ProvDom.Value = strData(0)
                Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
                If hfPengirimPenerus_IND_ProvDom.Value = ParameterProvinsi.MsSystemParameter_Value Then
                    Me.TrPengirimPenerus_IND_ProvLainDom.Visible = True
                Else
                    Me.TrPengirimPenerus_IND_ProvLainDom.Visible = False
                End If
                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.Propinsi)
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButton_PengirimPenerus_IND_ProvinsiIden_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_PengirimPenerus_IND_ProvinsiIden.Click
        Try
            If Session("PickerProvinsi.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                TxtPengirimPenerus_IND_ProvIden.Text = strData(1)
                hfPengirimPenerus_IND_ProvIden.Value = strData(0)
                Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
                If hfPengirimPenerus_IND_ProvIden.Value = ParameterProvinsi.MsSystemParameter_Value Then
                    Me.TrPengirimPenerus_IND_ProvLainIden.Visible = True
                Else
                    Me.TrPengirimPenerus_IND_ProvLainIden.Visible = False
                End If
                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.Propinsi)
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButton_PengirimPenerus_Korp_ProvinsiLengkap_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_PengirimPenerus_Korp_ProvinsiLengkap.Click

        Try
            If Session("PickerProvinsi.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                txtPengirimPenerus_Korp_ProvinsiKorp.Text = strData(1)
                hfPengirimPenerus_Korp_ProvinsiKorp.Value = strData(0)
                Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
                If hfPengirimPenerus_Korp_ProvinsiKorp.Value = ParameterProvinsi.MsSystemParameter_Value Then
                    Me.TrPengirimPenerus_Korp_provinsiLainnyaKorp.Visible = True
                Else
                    Me.TrPengirimPenerus_Korp_provinsiLainnyaKorp.Visible = False
                End If
                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.Propinsi)
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButton_BOwnerNas_ProvinsiIden_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_BOwnerNas_ProvinsiIden.Click

        Try
            If Session("PickerProvinsi.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                BenfOwnerNasabah_ProvinsiIden.Text = strData(1)
                hfBenfOwnerNasabah_ProvinsiIden.Value = strData(0)
                Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
                If hfBenfOwnerNasabah_ProvinsiIden.Value = ParameterProvinsi.MsSystemParameter_Value Then
                    Me.TrBenfOwnerNasabah_ProvinsiLainIden.Visible = True
                Else
                    Me.TrBenfOwnerNasabah_ProvinsiLainIden.Visible = False
                End If
                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.Propinsi)
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButton_BOwnerNonNas_Provinsi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_BOwnerNonNas_Provinsi.Click

        Try
            If Session("PickerProvinsi.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerProvinsi.Data")).Split(";")
                BenfOwnerNonNasabah_ProvinsiIden.Text = strData(1)
                hfBenfOwnerNonNasabah_ProvinsiIden.Value = strData(0)
                Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)
                If hfBenfOwnerNonNasabah_ProvinsiIden.Value = ParameterProvinsi.MsSystemParameter_Value Then
                    Me.TrBenfOwnerNonNasabah_ProvinsiLainIden.Visible = True
                Else
                    Me.TrBenfOwnerNonNasabah_ProvinsiLainIden.Visible = False
                End If
                'FillNextLevelAlamatIndividuIdentitas(AddressLevel.Propinsi)
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButtonPengirimNasPekerjaan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButtonPengirimNasPekerjaan.Click
        Try
            If Session("PickerPekerjaan.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerPekerjaan.Data")).Split(";")
                TxtIdenPengirimNas_Ind_pekerjaan.Text = strData(1)
                hfIdenPengirimNas_Ind_pekerjaan.Value = strData(0)
                Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)
                If hfIdenPengirimNas_Ind_pekerjaan.Value = ParameterPekerjaan.MsSystemParameter_Value Then
                    Me.TrIdenPengirimNas_Ind_Pekerjaanlain.Visible = True
                Else
                    Me.TrIdenPengirimNas_Ind_Pekerjaanlain.Visible = False
                End If
            End If
        Catch ex As Exception
            ' LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButton_PengirimPenerus_IND_Pekerjaan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_PengirimPenerus_IND_Pekerjaan.Click

        Try
            If Session("PickerPekerjaan.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerPekerjaan.Data")).Split(";")
                TxtPengirimPenerus_IND_pekerjaan.Text = strData(1)
                hfPengirimPenerus_IND_pekerjaan.Value = strData(0)
                Dim ParameterPekerjaan As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Pekerjaan)
                If hfPengirimPenerus_IND_pekerjaan.Value = ParameterPekerjaan.MsSystemParameter_Value Then
                    Me.TrPengirimPenerus_IND_pekerjaanLain.Visible = True
                Else
                    Me.TrPengirimPenerus_IND_pekerjaanLain.Visible = False
                End If
            End If
        Catch ex As Exception
            ' LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButton_PenerimaNasabah_IND_negaraIden_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_PenerimaNasabah_IND_negaraIden.Click
        Try
            If Session("PickerNegara.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
                txtPenerimaNasabah_IND_negaraIden.Text = strData(1)
                hfPenerimaNasabah_IND_negaraIden.Value = strData(0)
                Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
                If hfPenerimaNasabah_IND_negaraIden.Value = ParameterNegara.MsSystemParameter_Value Then
                    Me.TrPenerimaNasabah_IND_negaraLainIden.Visible = True
                Else
                    Me.TrPenerimaNasabah_IND_negaraLainIden.Visible = False
                End If
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    Protected Sub imgButtonPengirimNasCorp_BidangUsaha_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButtonPengirimNasCorp_BidangUsaha.Click
        Try
            If Session("PickerBidangUsaha.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerBidangUsaha.Data")).Split(";")
                TxtIdenPengirimNas_Corp_BidangUsahaKorp.Text = strData(1)
                hfIdenPengirimNas_Corp_BidangUsahaKorp.Value = strData(0)
                Dim ParameterBidangUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BidangUsaha)
                If hfIdenPengirimNas_Corp_BidangUsahaKorp.Value = ParameterBidangUsaha.MsSystemParameter_Value Then
                    Me.TrIdenPengirimNas_Corp_BidangUsahaKorpLainnya.Visible = True
                Else
                    Me.TrIdenPengirimNas_Corp_BidangUsahaKorpLainnya.Visible = False
                End If
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButton_PengirimPenerus_Korp_bidangUsaha_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_PengirimPenerus_Korp_bidangUsaha.Click

        Try
            If Session("PickerBidangUsaha.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerBidangUsaha.Data")).Split(";")
                txtPengirimPenerus_Korp_BidangUsahaKorp.Text = strData(1)
                hfPengirimPenerus_Korp_BidangUsahaKorp.Value = strData(0)
                Dim ParameterBidangUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BidangUsaha)
                If hfPengirimPenerus_Korp_BidangUsahaKorp.Value = ParameterBidangUsaha.MsSystemParameter_Value Then
                    Me.TrPengirimPenerus_Korp_BidangUsahaKorplain.Visible = True

                Else
                    Me.TrPengirimPenerus_Korp_BidangUsahaKorplain.Visible = False
                End If
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgButton_PenerimaNas_Korp_bidangUsaha_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_PenerimaNas_Korp_bidangUsaha.Click
        Try
            If Session("PickerBidangUsaha.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerBidangUsaha.Data")).Split(";")
                txtPenerimaNasabah_Korp_BidangUsahaKorp.Text = strData(1)
                hfPenerimaNasabah_Korp_BidangUsahaKorp.Value = strData(0)
                Dim ParameterBidangUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BidangUsaha)
                If hfPenerimaNasabah_Korp_BidangUsahaKorp.Value = ParameterBidangUsaha.MsSystemParameter_Value Then
                    Me.TrPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Visible = True

                Else
                    Me.TrPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Visible = True
                End If
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    Protected Sub imgButton_PenerimaNasabah_Korp_NegaraKorp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgButton_PenerimaNasabah_Korp_NegaraKorp.Click
        Try
            If Session("PickerNegara.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerNegara.Data")).Split(";")
                txtPenerimaNasabah_Korp_NegaraKorp.Text = strData(1)
                Me.hfPenerimaNasabah_Korp_NegaraKorp.Value = strData(0)
                Dim ParameterNegara As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Negara)
                If Me.hfPenerimaNasabah_Korp_NegaraKorp.Value = ParameterNegara.MsSystemParameter_Value Then
                    Me.TrPenerimaNasabah_Korp_NegaraLainnyaKorp.Visible = True
                Else
                    Me.TrPenerimaNasabah_Korp_NegaraLainnyaKorp.Visible = False
                End If
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    Protected Sub ImageButton_Transaksi_currency_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton_Transaksi_currency.Click
        Try
            If Session("PickerMataUang.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerMataUang.Data")).Split(";")
                Dim ParameterMataUang As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.MataUang)
                'Transaksi_currency.Text = strData(0) & " - " & strData(1)
                'hfTransaksi_currency.Value = strData(0)
                Using objMatauang As TList(Of MsCurrency) = DataRepository.MsCurrencyProvider.GetPaged("code = '" & strData(0).Trim & "'", "", 0, 1, 0)
                    If objMatauang.Count > 0 Then
                        Transaksi_currency.Text = objMatauang(0).Code
                        hfTransaksi_currency.Value = objMatauang(0).IdCurrency
                        If hfTransaksi_currency.Value = ParameterMataUang.MsSystemParameter_Value Then
                            Me.TrcurrencyLain.Visible = True
                        Else
                            Me.TrcurrencyLain.Visible = False
                        End If
                    End If
                End Using
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImageButton_Transaksi_MataUangTransaksi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton_Transaksi_MataUangTransaksi.Click
        Try
            If Session("PickerMataUang.Data") IsNot Nothing Then
                Dim strData() As String = CStr(Session("PickerMataUang.Data")).Split(";")
                Dim ParameterMataUang As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.MataUang)
                'Transaksi_MataUangTransaksi.Text = strData(0) & " - " & strData(1)
                'hfTransaksi_MataUangTransaksi.Value = strData(0)
                Using objMatauang As TList(Of MsCurrency) = DataRepository.MsCurrencyProvider.GetPaged("Code = '" & strData(0).Trim & "'", "", 0, 1, 0)
                    If objMatauang.Count > 0 Then
                        Transaksi_MataUangTransaksi.Text = objMatauang(0).Code
                        hfTransaksi_MataUangTransaksi.Value = objMatauang(0).IdCurrency
                        If hfTransaksi_MataUangTransaksi.Value = ParameterMataUang.MsSystemParameter_Value Then
                            Me.TrMataUangTransaksiLain.Visible = True
                        Else
                            Me.TrMataUangTransaksiLain.Visible = False
                        End If
                    End If
                End Using
            End If
        Catch ex As Exception
            'LogError(ex)
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

#End Region
#Region "Radio Button"
    Protected Sub RBPenerimaNasabah_TipePengirim_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RBPenerimaNasabah_TipePengirim.SelectedIndexChanged
        'MultiViewPenerimaNasabah.ActiveViewIndex = CInt(Me.RBPenerimaNasabah_TipePengirim.SelectedValue) - 1
        If RBPenerimaNasabah_TipePengirim.SelectedValue = "1" Then
            Me.trSwiftOutPenerimaTipeNasabah.Visible = True

            Rb_IdenPenerimaNas_TipeNasabah.SelectedValue = 1

            Me.MultiViewSwiftOutIdenPenerima.ActiveViewIndex = 0
            MultiViewPenerimaNasabah.ActiveViewIndex = 0
            RbPenerimaNasabah_IND_Warganegara.SelectedValue = 1
            ' Me.MultiViewPenerimaNasabah.ActiveViewIndex = 0
        Else
            Me.trSwiftOutPenerimaTipeNasabah.Visible = False
            Me.MultiViewSwiftOutIdenPenerima.ActiveViewIndex = 1
        End If
    End Sub
    Protected Sub Rb_IdenPengirimNas_TipePengirim_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Rb_IdenPengirimNas_TipePengirim.SelectedIndexChanged
        If Me.Rb_IdenPengirimNas_TipePengirim.SelectedValue = 1 Then
            Me.trTipeNasabah.Visible = True
            Rb_IdenPengirimNas_TipeNasabah.SelectedValue = 1

            Me.MultiViewSwiftOUtIdentitasPengirim.ActiveViewIndex = 0
            Me.MultiViewSwiftOutJenisNasabah.ActiveViewIndex = 0
            Me.MultiViewPengirimNasabah.ActiveViewIndex = 0
            Rb_IdenPengirimNas_Ind_kewarganegaraan.SelectedIndex = 0
        Else
            Me.trTipeNasabah.Visible = False
            Me.MultiViewSwiftOUtIdentitasPengirim.ActiveViewIndex = 0
            Me.MultiViewSwiftOutJenisNasabah.ActiveViewIndex = 1
        End If
    End Sub
    Protected Sub Rb_IdenPengirimNas_TipeNasabah_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Rb_IdenPengirimNas_TipeNasabah.SelectedIndexChanged
        If Me.Rb_IdenPengirimNas_TipeNasabah.SelectedValue = 1 Then
            Me.MultiViewSwiftOUtIdentitasPengirim.ActiveViewIndex = 0
            Me.MultiViewSwiftOutJenisNasabah.ActiveViewIndex = 0
            Me.MultiViewPengirimNasabah.ActiveViewIndex = 0
            Rb_IdenPengirimNas_Ind_kewarganegaraan.SelectedIndex = 0
        Else
            Me.MultiViewSwiftOUtIdentitasPengirim.ActiveViewIndex = 0
            Me.MultiViewSwiftOutJenisNasabah.ActiveViewIndex = 0
            Me.MultiViewPengirimNasabah.ActiveViewIndex = 1
        End If
        If Me.CboSwiftOutPengirim.SelectedValue = 2 Then
            If Me.Rb_IdenPengirimNas_TipeNasabah.SelectedValue = 1 Then
                Me.MultiViewPengirimPenerus.ActiveViewIndex = 0
                Me.MultiViewSwiftOUtIdentitasPengirim.ActiveViewIndex = 1
                Rb_IdenPengirimNas_Ind_kewarganegaraan.SelectedIndex = 0
            Else
                Me.MultiViewPengirimPenerus.ActiveViewIndex = 1
                Me.MultiViewSwiftOUtIdentitasPengirim.ActiveViewIndex = 1
            End If
        End If
    End Sub

    Protected Sub Rb_BOwnerNas_TipePengirim_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Rb_BOwnerNas_TipePengirim.SelectedIndexChanged
        If Rb_BOwnerNas_TipePengirim.SelectedValue = 1 Then
            Me.MultiViewSwiftOutBOwner.ActiveViewIndex = 0
        Else
            Me.MultiViewSwiftOutBOwner.ActiveViewIndex = 1
        End If
    End Sub

    Protected Sub Rb_IdenPengirimNas_TipeNasabah1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Rb_IdenPenerimaNas_TipeNasabah.SelectedIndexChanged
        If Rb_IdenPenerimaNas_TipeNasabah.SelectedValue = 1 Then
            Me.MultiViewSwiftOutIdenPenerima.ActiveViewIndex = 0
            MultiViewPenerimaNasabah.ActiveViewIndex = 0
            RbPenerimaNasabah_IND_Warganegara.SelectedValue = 1
            If Me.GridDataView.Enabled = False Then
                Me.ImageButton_savePenerimaInd.Visible = True
                Me.ImageButton_AddPenerimaInd.Visible = False
            End If
        Else
            Me.MultiViewSwiftOutIdenPenerima.ActiveViewIndex = 0
            MultiViewPenerimaNasabah.ActiveViewIndex = 1
            If Me.GridDataView.Enabled = False Then
                Me.ImageButton_savePenerimaKorp.Visible = True
                Me.ImageButton_AddPenerimaKorp.Visible = False
            End If
        End If
    End Sub
    Protected Sub RbPenerimaNasabah_IND_Warganegara_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RbPenerimaNasabah_IND_Warganegara.SelectedIndexChanged
        If RbPenerimaNasabah_IND_Warganegara.SelectedValue = 1 Then
            Me.PenerimaIndNegara.Visible = False
            Me.PenerimaIndNegaraLain.Visible = False
        Else
            Me.PenerimaIndNegara.Visible = True
            'Me.PenerimaIndNegaraLain.Visible = True
        End If
    End Sub

    Protected Sub RBPengirimPenerus_Ind_Kewarganegaraan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RBPengirimPenerus_Ind_Kewarganegaraan.SelectedIndexChanged
        If RBPengirimPenerus_Ind_Kewarganegaraan.SelectedValue = 1 Then
            Me.PengirimPenerusIndNegara.Visible = False
            Me.PengirimPenerusIndNegaraLain.Visible = False
        Else
            Me.PengirimPenerusIndNegara.Visible = True
            'Me.PengirimPenerusIndNegaraLain.Visible = True
        End If
    End Sub

    Protected Sub Rb_IdenPengirimNas_Ind_kewarganegaraan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Rb_IdenPengirimNas_Ind_kewarganegaraan.SelectedIndexChanged
        If Rb_IdenPengirimNas_Ind_kewarganegaraan.SelectedValue = 1 Then
            Me.PengirimIndNegara.Visible = False
            Me.PengirimIndNegaraLain.Visible = False
        Else
            Me.PengirimIndNegara.Visible = True
            'Me.PengirimIndNegaraLain.Visible = True
        End If
    End Sub

    'Protected Sub Rb_IdenPengirimNas_TipePengirim_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Rb_IdenNasabahNas_TipePengirim.SelectedIndexChanged
    '    ' Me.MultiViewPengirimNasabah.ActiveViewIndex = CInt(Me.Rb_IdenPengirimNas_TipePengirim.SelectedValue) - 1
    '    If Rb_IdenNasabahNas_TipePengirim.SelectedValue = "1" Then
    '        Me.MultiViewPengirimNasabah.ActiveViewIndex = 0
    '    Else
    '        Me.MultiViewPengirimNasabah.ActiveViewIndex = 1
    '    End If
    'End Sub

    'Protected Sub RbPengirimPenerus_TipePengirim_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RbPengirimPenerus_TipePengirim.SelectedIndexChanged
    '    'Me.MultiViewPengirimPenerus.ActiveViewIndex = CInt(Me.RBPenerimaNasabah_TipePengirim.SelectedValue) - 1
    '    If RbPengirimPenerus_TipePengirim.SelectedValue = "1" Then
    '        Me.MultiViewPengirimPenerus.ActiveViewIndex = 0
    '    Else
    '        Me.MultiViewPengirimPenerus.ActiveViewIndex = 1
    '    End If
    'End Sub


#End Region
#Region "combobox"
    Protected Sub CboSwiftOutPengirim_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CboSwiftOutPengirim.SelectedIndexChanged
        If Me.CboSwiftOutPengirim.SelectedValue = 1 Then
            Me.trTipePengirim.Visible = True
            Me.trTipeNasabah.Visible = True

            Rb_IdenPengirimNas_TipePengirim.SelectedValue = 1
            Rb_IdenPengirimNas_TipeNasabah.SelectedValue = 1

            Me.MultiViewSwiftOUtIdentitasPengirim.ActiveViewIndex = 0
            Me.MultiViewSwiftOutJenisNasabah.ActiveViewIndex = 0
            Me.MultiViewPengirimNasabah.ActiveViewIndex = 0
            Rb_IdenPengirimNas_Ind_kewarganegaraan.SelectedIndex = 0

        Else
            Me.trTipePengirim.Visible = False
            Me.trTipeNasabah.Visible = True

            Rb_IdenPengirimNas_TipeNasabah.SelectedValue = 1

            Me.MultiViewPengirimPenerus.ActiveViewIndex = 0
            Me.MultiViewSwiftOUtIdentitasPengirim.ActiveViewIndex = 1
            Rb_IdenPengirimNas_Ind_kewarganegaraan.SelectedIndex = 0

        End If
    End Sub


    'Protected Sub CboSwiftOutPengirim_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CboSwiftOutPengirim.TextChanged
    '    If Me.CboSwiftOutPengirim.SelectedValue = 1 Then
    '        Me.trTipePengirim.Visible = True
    '        Me.trTipeNasabah.Visible = False
    '    Else
    '        Me.trTipePengirim.Visible = False
    '        Me.trTipeNasabah.Visible = True
    '    End If
    'End Sub

#End Region
#Region "Button for receiver"
    Protected Sub ImageButton_savePenerimaInd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton_savePenerimaInd.Click
        'cekPenerima
        EditReceiver(getIFTIBeneficiaryTempPK - 1)
        'Beneficiary radiobutton
        RBPenerimaNasabah_TipePengirim.Enabled = True

        'Using objReceiverTemp As TList(Of IFTI_Beneficiary_Temporary) = DataRepository.IFTI_Beneficiary_TemporaryProvider.GetPaged("PK_IFTI_Beneficiary_Temporary_Id=" & getIFTIBeneficiaryTempPK, "", 0, Integer.MaxValue, 0)
        '    If objReceiverTemp.Count > 0 Then
        '        ' Dim KeyBeneficiary As Integer = objReceiverTemp(0).PK_IFTI_Beneficiary_ID
        '        Dim TipePenerima As Integer = objReceiverTemp(0).FK_IFTI_NasabahType_ID
        '        With objReceiverTemp(0)
        '            Select Case (TipePenerima)
        '                Case 1
        '                    validasiReceiverInd()
        '                    '.FK_IFTI_NasabahType_ID = 1
        '                    ''.FK_IFTI_ID = objIfti.PK_IFTI_ID
        '                    '.FK_IFTI_Approval_Id = objReceiverTemp(0).fk_ifti_app
        '                    ''.FK_IFTI_Beneficiary_ID = KeyBeneficiary
        '                    FillOrNothing(.Beneficiary_Nasabah_INDV_NoRekening, Me.txtPenerima_rekening.Text)

        '                    FillOrNothing(.Beneficiary_Nasabah_INDV_NamaLengkap, Me.txtPenerimaNasabah_IND_nama.Text, True, Ovarchar)
        '                    FillOrNothing(.Beneficiary_Nasabah_INDV_TanggalLahir, Me.txtPenerimaNasabah_IND_TanggalLahir.Text, True, oDate)
        '                    FillOrNothing(.Beneficiary_Nasabah_INDV_KewargaNegaraan, Me.RbPenerimaNasabah_IND_Warganegara.SelectedValue, True, oInt)
        '                    FillOrNothing(.Beneficiary_Nasabah_INDV_Negara, Me.hfPenerimaNasabah_IND_negara.Value, True, oInt)
        '                    FillOrNothing(.Beneficiary_Nasabah_INDV_NegaraLainnya, Me.txtPenerimaNasabah_IND_negaraLain.Text, True, Ovarchar)
        '                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Alamat_Iden, Me.txtPenerimaNasabah_IND_alamatIden.Text, True, Ovarchar)
        '                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_NegaraBagian, Me.txtPenerimaNasabah_IND_negaraBagian.Text, True, Ovarchar)
        '                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_Negara, Me.hfPenerimaNasabah_IND_negaraIden.Value, True, oInt)
        '                    FillOrNothing(.Beneficiary_Nasabah_INDV_ID_NegaraLainnya, Me.txtPenerimaNasabah_IND_negaraLainIden.Text, True, Ovarchar)
        '                    FillOrNothing(.Beneficiary_Nasabah_INDV_FK_IFTI_IDType, Me.CBOPenerimaNasabah_IND_JenisIden.SelectedValue, True, oInt)
        '                    FillOrNothing(.Beneficiary_Nasabah_INDV_NomorID, Me.txtPenerimaNasabah_IND_NomorIden.Text, True, Ovarchar)
        '                    FillOrNothing(.Beneficiary_Nasabah_INDV_NilaiTransaksiKeuangan, Me.txtPenerimaNasabah_IND_NilaiTransaksiKeuangan.Text, True, oDecimal)
        '                Case 2
        '                    validasiReceiverKorp()
        '                    '.FK_IFTI_NasabahType_ID = 2
        '                    '.FK_IFTI_ID = objIfti.PK_IFTI_ID
        '                    '.FK_IFTI_Approval_Id = KeyHeaderApproval
        '                    ''.FK_IFTI_Beneficiary_ID = KeyBeneficiary
        '                    FillOrNothing(.Beneficiary_Nasabah_CORP_NoRekening, Me.txtPenerima_rekening.Text)

        '                    FillOrNothing(.Beneficiary_Nasabah_CORP_FK_MsBentukBadanUsaha_Id, CBOPenerimaNasabah_Korp_BentukBadanUsahaLain.SelectedValue, True, oInt)
        '                    FillOrNothing(.Beneficiary_Nasabah_CORP_BentukBadanUsahaLainnya, Me.txtPenerimaNasabah_Korp_BentukBadanUsahaLainnya.Text, True, Ovarchar)
        '                    FillOrNothing(.Beneficiary_Nasabah_CORP_NamaKorporasi, Me.txtPenerimaNasabah_Korp_namaKorp.Text, True, Ovarchar)
        '                    FillOrNothing(.Beneficiary_Nasabah_CORP_FK_MsBidangUsaha_Id, Me.hfPenerimaNasabah_Korp_BidangUsahaKorp.Value, True, oInt)
        '                    FillOrNothing(.Beneficiary_Nasabah_CORP_BidangUsahaLainnya, Me.txtPenerimaNasabah_Korp_BentukBadanUsahaLainnya.Text, True, Ovarchar)
        '                    FillOrNothing(.Beneficiary_Nasabah_CORP_AlamatLengkap, Me.txtPenerimaNasabah_Korp_AlamatKorp.Text, TipePenerima, Ovarchar)
        '                    FillOrNothing(.Beneficiary_Nasabah_CORP_ID_NegaraBagian, Me.txtPenerimaNasabah_Korp_negaraKota.Text, True, oInt)
        '                    FillOrNothing(.Beneficiary_Nasabah_CORP_ID_Negara, Me.hfPenerimaNasabah_Korp_NegaraKorp.Value, True, oInt)
        '                    FillOrNothing(.Beneficiary_Nasabah_CORP_ID_NegaraLainnya, Me.txtPenerimaNasabah_Korp_NegaraLainnyaKorp.Text, True, Ovarchar)
        '                    FillOrNothing(.Beneficiary_Nasabah_CORP_NilaiTransaksiKeuangan, Me.txtPenerimaNasabah_Korp_NilaiTransaksiKeuangan.Text, True, oDecimal)
        '                Case 3
        '                    validasiReceiverNonNasabah()
        '                    '.FK_IFTI_NasabahType_ID = 3
        '                    '.FK_IFTI_ID = objIfti.PK_IFTI_ID
        '                    '.FK_IFTI_Approval_Id = KeyHeaderApproval
        '                    '.FK_IFTI_Beneficiary_ID = KeyBeneficiary
        '                    FillOrNothing(.Beneficiary_NonNasabah_NoRekening, Me.txtPenerima_rekening.Text)

        '                    FillOrNothing(.Beneficiary_NonNasabah_NamaBank, Me.txtPenerimaNonNasabah_namabank.Text, True, Ovarchar)
        '                    FillOrNothing(.Beneficiary_NonNasabah_NamaLengkap, Me.txtPenerimaNonNasabah_Nama.Text, True, Ovarchar)
        '                    FillOrNothing(.Beneficiary_NonNasabah_ID_Alamat, Me.txtPenerimaNonNasabah_Alamat.Text, True, Ovarchar)
        '                    FillOrNothing(.Beneficiary_NonNasabah_ID_Negara, Me.hfPenerimaNonNasabah_Negara.Value, True, oInt)
        '                    FillOrNothing(.Beneficiary_NonNasabah_ID_NegaraLainnya, Me.txtPenerimaNonNasabah_negaraLain.Text, True, Ovarchar)
        '                    FillOrNothing(.Beneficiary_NonNasabah_NilaiTransaksikeuangan, Me.txtPenerimaNonNasabah_nilaiTransaksiKeuangan.Text, True, oDecimal)
        '            End Select
        '        End With
        '        'save beneficiary
        '        DataRepository.IFTI_Beneficiary_TemporaryProvider.Save(objReceiverTemp(0))
        '    End If
        'End Using
        ' Me.tableTipePenerima.Visible = False
    End Sub

    Protected Sub ImageButton_savePenerimaKorp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton_savePenerimaKorp.Click
        EditReceiver(getIFTIBeneficiaryTempPK - 1)
        'Beneficiary radiobutton
        RBPenerimaNasabah_TipePengirim.Enabled = True
    End Sub

    Protected Sub ImageButton_savePenerimaNonNasabah_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton_savePenerimaNonNasabah.Click
        EditReceiver(getIFTIBeneficiaryTempPK - 1)
        'Beneficiary radiobutton
        RBPenerimaNasabah_TipePengirim.Enabled = True
    End Sub

    Protected Sub ImageButton_CancelPenerimaNonNasabah_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton_CancelPenerimaNonNasabah.Click
        LoadReceiver()
        txtPenerima_rekening.Text = ""

        'Beneficiary radiobutton
        RBPenerimaNasabah_TipePengirim.Enabled = True

        txtPenerimaNonNasabah_namabank.Text = ""
        txtPenerimaNonNasabah_Nama.Text = ""
        txtPenerimaNonNasabah_Alamat.Text = ""
        hfPenerimaNonNasabah_Negara.Value = ""
        txtPenerimaNonNasabah_Negara.Text = ""
        txtPenerimaNonNasabah_negaraLain.Text = ""
        txtPenerimaNonNasabah_nilaiTransaksiKeuangan.Text = ""
        GridDataView.Enabled = True
        Me.ImageButton_savePenerimaNonNasabah.Visible = False
        Me.ImageButton_AddPenerimaNonNasabah.Visible = True
    End Sub

    Protected Sub ImageButton_CancelPenerimaKorp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton_CancelPenerimaKorp.Click
        LoadReceiver()
        txtPenerima_rekening.Text = ""

        'Beneficiary radiobutton
        RBPenerimaNasabah_TipePengirim.Enabled = True

        txtPenerimaNasabah_Korp_namaKorp.Text = ""
        txtPenerimaNasabah_Korp_BentukBadanUsahaLainnya.Text = ""
        CBOPenerimaNasabah_Korp_BentukBadanUsahaLain.SelectedIndex = 0
        hfPenerimaNasabah_Korp_BidangUsahaKorp.Value = ""
        txtPenerimaNasabah_Korp_BidangUsahaKorp.Text = ""
        txtPenerimaNasabah_Korp_BidangUsahaKorpLainnya.Text = ""
        txtPenerimaNasabah_Korp_AlamatKorp.Text = ""
        hfPenerimaNasabah_Korp_NegaraKorp.Value = ""
        txtPenerimaNasabah_Korp_NegaraKorp.Text = ""
        txtPenerimaNasabah_Korp_negaraKota.Text = ""
        txtPenerimaNasabah_Korp_NegaraLainnyaKorp.Text = ""
        txtPenerimaNasabah_Korp_NilaiTransaksiKeuangan.Text = ""
        GridDataView.Enabled = True
        Me.ImageButton_savePenerimaKorp.Visible = False
        Me.ImageButton_AddPenerimaKorp.Visible = True
    End Sub

    Protected Sub ImageButton_CancelPenerimaInd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton_CancelPenerimaInd.Click
        LoadReceiver()

        'Beneficiary radiobutton
        RBPenerimaNasabah_TipePengirim.Enabled = True

        txtPenerima_rekening.Text = ""
        txtPenerimaNasabah_IND_nama.Text = ""
        txtPenerimaNasabah_IND_TanggalLahir.Text = ""
        RbPenerimaNasabah_IND_Warganegara.SelectedIndex = 0
        hfPenerimaNasabah_IND_negara.Value = ""
        txtPenerimaNasabah_IND_negara.Text = ""
        txtPenerimaNasabah_IND_negaraLain.Text = ""
        txtPenerimaNasabah_IND_alamatIden.Text = ""
        txtPenerimaNasabah_IND_negaraBagian.Text = ""
        hfPenerimaNasabah_IND_negaraIden.Value = ""
        txtPenerimaNasabah_IND_negaraIden.Text = ""
        txtPenerimaNasabah_IND_negaraLainIden.Text = ""
        CBOPenerimaNasabah_IND_JenisIden.SelectedIndex = 0
        txtPenerimaNasabah_IND_NomorIden.Text = ""
        txtPenerimaNasabah_IND_NilaiTransaksiKeuangan.Text = ""
        GridDataView.Enabled = True
        Me.ImageButton_savePenerimaInd.Visible = False
        Me.ImageButton_AddPenerimaInd.Visible = True
    End Sub

    Protected Sub ImageButton_AddPenerimaNonNasabah_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton_AddPenerimaNonNasabah.Click
        AddReceiver()
        Me.GridDataView.Enabled = True
    End Sub

    Protected Sub ImageButton_AddPenerimaKorp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton_AddPenerimaKorp.Click
        AddReceiver()
        Me.GridDataView.Enabled = True
    End Sub

    Protected Sub ImageButton_AddPenerimaInd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton_AddPenerimaInd.Click
        AddReceiver()
        Me.GridDataView.Enabled = True
    End Sub
#End Region
#Region "Checkbox"
    Protected Sub CheckPengirimNas_Ind_alamat_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckPengirimNas_Ind_alamat.CheckedChanged
        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)

        If CheckPengirimNas_Ind_alamat.Checked = True Then
            Me.TxtIdenPengirimNas_Ind_alamatIdentitas.Text = Me.TxtIdenPengirimNas_Ind_Alamat.Text
            Me.TxtIdenPengirimNas_Ind_kotaIdentitas.Text = Me.TxtIdenPengirimNas_Ind_Kota.Text
            Me.hfIdenPengirimNas_Ind_kotaIdentitas.Value = Me.hfIdenPengirimNas_Ind_Kota_dom.Value
            If hfIdenPengirimNas_Ind_kotaIdentitas.Value = ParameterKota.MsSystemParameter_Value Then
                Me.TxtIdenPengirimNas_Ind_KotaLainIden.Text = Me.TxtIdenPengirimNas_Ind_kotalain.Text
                Me.TrIdenPengirimNas_Ind_KotaLainIden.Visible = True
            Else
                Me.TrIdenPengirimNas_Ind_KotaLainIden.Visible = False
            End If

            Me.TxtIdenPengirimNas_Ind_ProvinsiIden.Text = Me.TxtIdenPengirimNas_Ind_provinsi.Text
            Me.hfIdenPengirimNas_Ind_ProvinsiIden.Value = Me.hfIdenPengirimNas_Ind_provinsi_dom.Value
            If hfIdenPengirimNas_Ind_ProvinsiIden.Value = ParameterProvinsi.MsSystemParameter_Value Then
                Me.TxtIdenPengirimNas_Ind_ProvinsilainIden.Text = Me.TxtIdenPengirimNas_Ind_provinsiLain.Text
                Me.TrIdenPengirimNas_Ind_ProvinsilainIden.Visible = True
            Else
                Me.TrIdenPengirimNas_Ind_ProvinsilainIden.Visible = False
            End If

            ''.Text = Me.TxtIdenPengirimNas_Ind_kodeposProvDom.Text
        Else
            Me.TxtIdenPengirimNas_Ind_alamatIdentitas.Text = ""
            Me.TxtIdenPengirimNas_Ind_kotaIdentitas.Text = ""
            Me.TrIdenPengirimNas_Ind_KotaLainIden.Visible = False
            Me.TxtIdenPengirimNas_Ind_KotaLainIden.Text = ""
            Me.TxtIdenPengirimNas_Ind_ProvinsiIden.Text = ""
            Me.TrIdenPengirimNas_Ind_ProvinsilainIden.Visible = False
            Me.TxtIdenPengirimNas_Ind_ProvinsilainIden.Text = ""
            'Me.TxtIdenPengirimNas_Ind_KodeposProvIden.Text = ""
        End If
    End Sub

    Protected Sub CheckBoxPengirimPenerus_IND_samaIden_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxPengirimPenerus_IND_samaIden.CheckedChanged
        Dim ParameterKota As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.KotaKabupaten)
        Dim ParameterProvinsi As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.Propinsi)

        If CheckBoxPengirimPenerus_IND_samaIden.Checked = True Then
            Me.TxtPengirimPenerus_IND_alamatIden.Text = Me.TxtPengirimPenerus_IND_alamatDom.Text
            Me.TxtPengirimPenerus_IND_kotaIden.Text = TxtPengirimPenerus_IND_kotaDom.Text
            hfPengirimPenerus_IND_kotaIden.Value = hfPengirimPenerus_IND_kotaDom.Value
            If hfPengirimPenerus_IND_kotaIden.Value = ParameterKota.MsSystemParameter_Value Then
                Me.TxtPengirimPenerus_IND_KotaLainIden.Text = Me.TxtPengirimPenerus_IND_kotaLainDom.Text
                Me.TrPengirimPenerus_IND_KotaLainIden.Visible = True
            Else
                Me.TrPengirimPenerus_IND_KotaLainIden.Visible = False
            End If

            Me.TxtPengirimPenerus_IND_ProvIden.Text = TxtPengirimPenerus_IND_ProvDom.Text
            hfPengirimPenerus_IND_ProvIden.Value = hfPengirimPenerus_IND_ProvDom.Value
            If hfPengirimPenerus_IND_ProvIden.Value = ParameterProvinsi.MsSystemParameter_Value Then
                Me.TxtPengirimPenerus_IND_ProvLainIden.Text = Me.TxtPengirimPenerus_IND_ProvLainDom.Text
                Me.TrPengirimPenerus_IND_ProvLainIden.Visible = True
            Else
                Me.TrPengirimPenerus_IND_ProvLainIden.Visible = False
            End If

        Else
            Me.TxtPengirimPenerus_IND_alamatIden.Text = ""
            Me.TxtPengirimPenerus_IND_kotaIden.Text = ""
            Me.TxtPengirimPenerus_IND_KotaLainIden.Text = ""
            Me.TrPengirimPenerus_IND_KotaLainIden.Visible = False
            Me.TxtPengirimPenerus_IND_ProvIden.Text = ""
            Me.TxtPengirimPenerus_IND_ProvLainIden.Text = ""
            Me.TrPengirimPenerus_IND_ProvLainIden.Visible = False
        End If
    End Sub

#End Region
    Protected Sub ImageButtonCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonCancel.Click
        Response.Redirect("IFTIView.aspx")
    End Sub

    Protected Sub ImageButtonSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSave.Click
        'If SessionPkUserId = 1 Then
        '    SaveSwiftOutDirect()
        'Else
        SaveSwiftOut()
        'End If

    End Sub


    Protected Sub cvalPageErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles cvalPageErr.PreRender
        If cvalPageErr.IsValid = False Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
        End If
    End Sub

    Protected Sub imgOKMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgOKMsg.Click
        Response.Redirect("IFTIView.aspx")
    End Sub


    Protected Sub GridDataView_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridDataView.DeleteCommand
        If e.Item.ItemType <> ListItemType.Header And e.Item.ItemType <> ListItemType.Footer Then
            Select Case e.CommandName
                Case "delete"
                    '' if it's an autogenerated delete-LinkButton: '
                    'Dim LnkBtnDelete As LinkButton = DirectCast(.Cells(0).Controls(0), LinkButton)
                    'LnkBtnDelete.OnClientClick = "return confirm('Are you certain you want to delete?');"
                    'Dim response As MsgBoxStyle

                    'response = MsgBoxStyle.YesNo.


                    'If response = vbYes Then
                    '    'delete record
                    'Else
                    '    dsvsdv()
                    'End If

                    If Page.IsPostBack Then
                        ReceiverDataTable.Rows.RemoveAt(e.Item.ItemIndex + (SetnGetCurrentPageReceiver * GetDisplayedTotalRow))

                        LoadReceiver()
                    End If

            End Select
        End If
    End Sub


    Protected Sub GridDataView_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridDataView.EditCommand
        Dim PKIFTIBeneficiary As Integer
        Try
            PKIFTIBeneficiary = CInt(e.Item.Cells(4).Text)
            'getIFTIBeneficiaryTempPK = PKIFTIBeneficiary

            Me.tableTipePenerima.Visible = True

            ''Identitas Penerima
            'Using objIftiBeneficiary As TList(Of IFTI_Beneficiary) = DataRepository.IFTI_BeneficiaryProvider.GetPaged("PK_IFTI_Beneficiary_ID = " & PKIFTIBeneficiary, "", 0, Integer.MaxValue, 0)
            '    If objIftiBeneficiary.Count > 0 Then
            '        'Me.txtPenerima_rekening.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_NoRekening

            '        ' cek ReceiverType
            '        Dim receiverType As Integer = objIftiBeneficiary(0).FK_IFTI_NasabahType_ID
            '        Select Case (receiverType)
            '            Case 1
            '                Me.RBPenerimaNasabah_TipePengirim.SelectedValue = 1
            '                Me.trSwiftOutPenerimaTipeNasabah.Visible = True
            '                Me.Rb_IdenPenerimaNas_TipeNasabah.SelectedValue = 1
            '                Me.MultiViewSwiftOutIdenPenerima.ActiveViewIndex = 0
            '                Me.MultiViewPenerimaNasabah.ActiveViewIndex = 0
            '                FieldSwiftOutPenerimaNasabahPerorangan(PKIFTIBeneficiary)
            '                ''enable true
            '                'RBPenerimaNasabah_TipePengirim.Enabled = True
            '                'Rb_IdenPenerimaNas_TipeNasabah.Enabled = True
            '                'Me.txtPenerima_rekening.Enabled = True
            '                'Me.txtPenerimaNasabah_IND_nama.Enabled = True
            '                'Me.txtPenerimaNasabah_IND_TanggalLahir.Enabled = True
            '                'Me.RbPenerimaNasabah_IND_Warganegara.Enabled = True
            '                'txtPenerimaNasabah_IND_negara.Enabled = True
            '                'Me.txtPenerimaNasabah_IND_negaraLain.Enabled = True
            '                'Me.txtPenerimaNasabah_IND_alamatIden.Enabled = True
            '                'Me.txtPenerimaNasabah_IND_negaraBagian.Enabled = True
            '                'txtPenerimaNasabah_IND_negaraIden.Enabled = True
            '                'Me.txtPenerimaNasabah_IND_negaraLainIden.Enabled = True
            '                'Me.CBOPenerimaNasabah_IND_JenisIden.Enabled = True
            '                'Me.txtPenerimaNasabah_IND_NomorIden.Enabled = True
            '                'Me.txtPenerimaNasabah_IND_NilaiTransaksiKeuangan.Enabled = True
            '                'popUpTanggalLahirPenerimaNasabah_Ind.Visible = True
            '                'imgButton_PenerimaNasabah_IND_negara.Visible = True
            '                'imgButton_PenerimaNasabah_IND_negaraIden.Visible = True
            '                'ImageButton_savePenerimaInd.Visible = True
            '            Case 2
            '                Me.RBPenerimaNasabah_TipePengirim.SelectedValue = 1
            '                Me.Rb_IdenPenerimaNas_TipeNasabah.SelectedValue = 2
            '                Me.trSwiftOutPenerimaTipeNasabah.Visible = True
            '                Me.MultiViewSwiftOutIdenPenerima.ActiveViewIndex = 0
            '                Me.MultiViewPenerimaNasabah.ActiveViewIndex = 1
            '                FieldSwiftOutPenerimaNasabahKorporasi(PKIFTIBeneficiary)
            '                ''enable true
            '                'RBPenerimaNasabah_TipePengirim.Enabled = True
            '                'Rb_IdenPenerimaNas_TipeNasabah.Enabled = True
            '                'Me.txtPenerima_rekening.Enabled = True
            '                'Me.CBOPenerimaNasabah_Korp_BentukBadanUsahaLain.Enabled = True
            '                'Me.txtPenerimaNasabah_Korp_BentukBadanUsahaLainnya.Enabled = True
            '                'Me.txtPenerimaNasabah_Korp_namaKorp.Enabled = True
            '                'Me.txtPenerimaNasabah_Korp_BidangUsahaKorp.Enabled = True
            '                'Me.txtPenerimaNasabah_Korp_AlamatKorp.Enabled = True
            '                'Me.txtPenerimaNasabah_Korp_negaraKota.Enabled = True
            '                'txtPenerimaNasabah_Korp_NegaraKorp.Enabled = True
            '                'Me.txtPenerimaNasabah_Korp_NegaraLainnyaKorp.Enabled = True
            '                'Me.txtPenerimaNasabah_Korp_NilaiTransaksiKeuangan.Enabled = True

            '            Case 3
            '                Me.RBPenerimaNasabah_TipePengirim.SelectedValue = 1
            '                Me.trSwiftOutPenerimaTipeNasabah.Visible = False
            '                Me.MultiViewSwiftOutIdenPenerima.ActiveViewIndex = 1
            '                Me.trSwiftOutPenerimaTipeNasabah.Visible = False
            '                FieldSwiftOutPenerimaNonNasabah(PKIFTIBeneficiary)
            '        End Select
            '    End If

            'End Using
            Dim dr As Data.DataRow = ReceiverDataTable.Rows(e.Item.ItemIndex + (SetnGetCurrentPageReceiver * Sahassa.AML.Commonly.GetDisplayedTotalRow))
            getIFTIBeneficiaryTempPK = (e.Item.ItemIndex + (SetnGetCurrentPageReceiver * Sahassa.AML.Commonly.GetDisplayedTotalRow)) + 1


            'Me.txtPenerima_rekening.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_NoRekening

            ' cek ReceiverType
            Dim receiverType As Integer = dr("FK_IFTI_NasabahType_ID")
            RBPenerimaNasabah_TipePengirim.Enabled = False
            Select Case (receiverType)
                Case 1
                    Me.RBPenerimaNasabah_TipePengirim.SelectedValue = 1
                    Me.trSwiftOutPenerimaTipeNasabah.Visible = True
                    Me.Rb_IdenPenerimaNas_TipeNasabah.SelectedValue = 1
                    Me.MultiViewSwiftOutIdenPenerima.ActiveViewIndex = 0
                    Me.MultiViewPenerimaNasabah.ActiveViewIndex = 0
                    FieldSwiftOutPenerimaNasabahPerorangan(PKIFTIBeneficiary)
                    Me.ImageButton_savePenerimaInd.Visible = True
                    Me.ImageButton_AddPenerimaInd.Visible = False
                Case 2
                    Me.RBPenerimaNasabah_TipePengirim.SelectedValue = 1
                    Me.Rb_IdenPenerimaNas_TipeNasabah.SelectedValue = 2
                    Me.trSwiftOutPenerimaTipeNasabah.Visible = True
                    Me.MultiViewSwiftOutIdenPenerima.ActiveViewIndex = 0
                    Me.MultiViewPenerimaNasabah.ActiveViewIndex = 1
                    FieldSwiftOutPenerimaNasabahKorporasi(PKIFTIBeneficiary)
                    Me.ImageButton_savePenerimaKorp.Visible = True
                    Me.ImageButton_AddPenerimaKorp.Visible = False
                Case 3
                    Me.RBPenerimaNasabah_TipePengirim.SelectedValue = 2
                    Me.trSwiftOutPenerimaTipeNasabah.Visible = False
                    Me.MultiViewSwiftOutIdenPenerima.ActiveViewIndex = 1
                    'Me.trSwiftOutPenerimaTipeNasabah.Visible = False
                    FieldSwiftOutPenerimaNonNasabah(PKIFTIBeneficiary)
                    ImageButton_savePenerimaNonNasabah.Visible = True
                    ImageButton_AddPenerimaNonNasabah.Visible = False
            End Select


            GridDataView.Enabled = False
        Catch ex As Exception
            'LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    'Protected Sub GridDataView_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridDataView.ItemCommand
    '    If e.CommandName.ToLower = "detail" Then
    '        Dim PKIFTIBeneficiary As Integer
    '        Try
    '            PKIFTIBeneficiary = CInt(e.Item.Cells(1).Text)
    '            getIFTIBeneficiaryTempPK = PKIFTIBeneficiary
    '            Me.tableTipePenerima.Visible = True

    '            'Identitas Penerima
    '            Using objIftiBeneficiary As TList(Of IFTI_Beneficiary_Temporary) = DataRepository.IFTI_Beneficiary_TemporaryProvider.GetPaged("PK_IFTI_Beneficiary_Temporary_ID = " & PKIFTIBeneficiary, "", 0, Integer.MaxValue, 0)
    '                If objIftiBeneficiary.Count > 0 Then
    '                    'Me.txtPenerima_rekening.Text = objIftiBeneficiary(0).Beneficiary_Nasabah_CORP_NoRekening

    '                    ' cek ReceiverType
    '                    Dim receiverType As Integer = objIftiBeneficiary(0).FK_IFTI_NasabahType_ID
    '                    Select Case (receiverType)
    '                        Case 1
    '                            Me.RBPenerimaNasabah_TipePengirim.SelectedValue = 1
    '                            Me.trSwiftOutPenerimaTipeNasabah.Visible = True
    '                            Me.Rb_IdenPenerimaNas_TipeNasabah.SelectedValue = 1
    '                            Me.MultiViewSwiftOutIdenPenerima.ActiveViewIndex = 0
    '                            Me.MultiViewPenerimaNasabah.ActiveViewIndex = 0
    '                            FieldSwiftOutPenerimaNasabahPerorangan(PKIFTIBeneficiary)
    '                            'enable false
    '                            RBPenerimaNasabah_TipePengirim.Enabled = False
    '                            Rb_IdenPenerimaNas_TipeNasabah.Enabled = False
    '                            Me.txtPenerima_rekening.Enabled = False
    '                            Me.txtPenerimaNasabah_IND_nama.Enabled = False
    '                            Me.txtPenerimaNasabah_IND_TanggalLahir.Enabled = False
    '                            Me.RbPenerimaNasabah_IND_Warganegara.Enabled = False
    '                            txtPenerimaNasabah_IND_negara.Enabled = False
    '                            Me.txtPenerimaNasabah_IND_negaraLain.Enabled = False
    '                            Me.txtPenerimaNasabah_IND_alamatIden.Enabled = False
    '                            Me.txtPenerimaNasabah_IND_negaraBagian.Enabled = False
    '                            txtPenerimaNasabah_IND_negaraIden.Enabled = False
    '                            Me.txtPenerimaNasabah_IND_negaraLainIden.Enabled = False
    '                            Me.CBOPenerimaNasabah_IND_JenisIden.Enabled = False
    '                            Me.txtPenerimaNasabah_IND_NomorIden.Enabled = False
    '                            Me.txtPenerimaNasabah_IND_NilaiTransaksiKeuangan.Enabled = False
    '                            popUpTanggalLahirPenerimaNasabah_Ind.Visible = False
    '                            imgButton_PenerimaNasabah_IND_negara.Visible = False
    '                            imgButton_PenerimaNasabah_IND_negaraIden.Visible = False
    '                            ImageButton_savePenerimaInd.Visible = False
    '                        Case 2
    '                            Me.RBPenerimaNasabah_TipePengirim.SelectedValue = 1
    '                            Me.Rb_IdenPenerimaNas_TipeNasabah.SelectedValue = 2
    '                            Me.trSwiftOutPenerimaTipeNasabah.Visible = True
    '                            Me.MultiViewSwiftOutIdenPenerima.ActiveViewIndex = 0
    '                            Me.MultiViewPenerimaNasabah.ActiveViewIndex = 1
    '                            FieldSwiftOutPenerimaNasabahKorporasi(PKIFTIBeneficiary)
    '                        Case 3
    '                            Me.RBPenerimaNasabah_TipePengirim.SelectedValue = 1
    '                            Me.trSwiftOutPenerimaTipeNasabah.Visible = False
    '                            Me.MultiViewSwiftOutIdenPenerima.ActiveViewIndex = 1
    '                            Me.trSwiftOutPenerimaTipeNasabah.Visible = False
    '                            FieldSwiftOutPenerimaNonNasabah(PKIFTIBeneficiary)
    '                    End Select
    '                End If

    '            End Using
    '        Catch ex As Exception
    '            'LogError(ex)
    '            cvalPageErr.IsValid = False
    '            cvalPageErr.ErrorMessage = ex.Message
    '        End Try
    '    End If
    'End Sub

    Protected Sub GridDataView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridDataView.ItemDataBound
        Try
            Dim CollCount As Integer = e.Item.Cells.Count - 1
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Dim chkBox As CheckBox = CType(e.Item.FindControl("CheckBoxExporttoExcel"), CheckBox)
                chkBox.Checked = SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)
                If BindGridFromExcel = True Then
                    e.Item.Cells(4).Text = CStr((e.Item.ItemIndex + 1))
                Else
                    e.Item.Cells(4).Text = CStr((e.Item.ItemIndex + 1) + (SetnGetCurrentPage * GetDisplayedTotalRow))
                End If
                'Dim nasabahType As Integer = e.Item.Cells(3).Text

                'Select Case (nasabahType)
                '    Case 1
                '        GridDataView.Columns(9).Visible = False
                '        GridDataView.Columns(10).Visible = False
                '        GridDataView.Columns(11).Visible = False
                '        GridDataView.Columns(12).Visible = False
                '        GridDataView.Columns(13).Visible = False
                '        GridDataView.Columns(14).Visible = False
                '        GridDataView.Columns(15).Visible = False
                '        GridDataView.Columns(16).Visible = False
                '        GridDataView.Columns(17).Visible = False
                '    Case 2
                '        GridDataView.Columns(5).Visible = False
                '        GridDataView.Columns(6).Visible = False
                '        GridDataView.Columns(7).Visible = False
                '        GridDataView.Columns(8).Visible = False
                '        GridDataView.Columns(13).Visible = False
                '        GridDataView.Columns(14).Visible = False
                '        GridDataView.Columns(15).Visible = False
                '        GridDataView.Columns(16).Visible = False
                '        GridDataView.Columns(17).Visible = False

                '        'e.Item.Cells(5).Visible = False
                '        'e.Item.Cells(6).Visible = False
                '        'e.Item.Cells(7).Visible = False
                '        'e.Item.Cells(8).Visible = False

                '        'e.Item.Cells(13).Visible = False
                '        'e.Item.Cells(14).Visible = False
                '        'e.Item.Cells(15).Visible = False
                '        'e.Item.Cells(16).Visible = False
                '        'e.Item.Cells(17).Visible = False
                '    Case 3
                '        GridDataView.Columns(9).Visible = False
                '        GridDataView.Columns(10).Visible = False
                '        GridDataView.Columns(11).Visible = False
                '        GridDataView.Columns(12).Visible = False
                '        GridDataView.Columns(5).Visible = False
                '        GridDataView.Columns(6).Visible = False
                '        GridDataView.Columns(7).Visible = False
                '        GridDataView.Columns(8).Visible = False
                'End Select
                'Using CekIFTI_ApprovalDetail As TList(Of IFTI_Approval_Detail) = DataRepository.IFTI_Approval_DetailProvider.GetPaged("PK_IFTI_Approval_Detail_Id = '" & CInt(e.Item.Cells(1).Text) & "'", "", 0, Integer.MaxValue, 0)
                '    If CekIFTI_ApprovalDetail.Count > 0 Then
                '        e.Item.Cells(CollCount - 1).Enabled = False
                '    End If
                'End Using
            End If
        Catch ex As Exception
            'LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                clearSession()
                AuditTrailBLL.InsertAuditTrailUserAccess(Sahassa.AML.Commonly.SessionCurrentPage)
                SetCOntrolLoad()
                TransactionSwiftOut()
                clearSession()
                'If MultiView1.ActiveViewIndex = 0 Then
                '    ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('IdTerlapor','Img1','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
                '    ClientScript.RegisterStartupScript(Me.GetType, System.Guid.NewGuid.ToString, "  hidePanel('Transaksi','Img2','Images/search-bar-minimize.gif','images/search-bar-maximize.gif');", True)
                'End If
                Me.MultiViewNasabahTypeAll.ActiveViewIndex = 0
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            'LogError(ex)
        End Try
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'bindgrid()
        Me.LoadReceiver()
    End Sub
#End Region
    Protected Sub Rb_BOwnerNas_ApaMelibatkan_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Rb_BOwnerNas_ApaMelibatkan.SelectedIndexChanged
        If Rb_BOwnerNas_ApaMelibatkan.SelectedValue = 1 Then
            Me.trBenfOwnerHubungan.Visible = True
            Me.trBenfOwnerTipePengirim.Visible = True
            MultiViewSwiftOutBOwner.Visible = True
        Else
            Me.trBenfOwnerTipePengirim.Visible = False
            Me.trBenfOwnerHubungan.Visible = False
            MultiViewSwiftOutBOwner.Visible = False
        End If
    End Sub

    Protected Sub RbPengirimNonNasabah_100Juta_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RbPengirimNonNasabah_100Juta.SelectedIndexChanged
        If RbPengirimNonNasabah_100Juta.SelectedValue = 2 Then
            jenisdokumen_nonNasabah_mand.Visible = True
            Me.NomorIdentitas_nonNasabah_mand.Visible = True
        Else
            jenisdokumen_nonNasabah_mand.Visible = False
            Me.NomorIdentitas_nonNasabah_mand.Visible = False
        End If
    End Sub

    Protected Sub RMIdenPengirimNas_Ind_negara_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMIdenPengirimNas_Ind_negara.Click
        TxtIdenPengirimNas_Ind_negara.Text = ""
        hfIdenPengirimNas_Ind_negara.Value = Nothing
    End Sub

    Protected Sub RMPengirimNasPekerjaan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMPengirimNasPekerjaan.Click
        TxtIdenPengirimNas_Ind_pekerjaan.Text = ""
        hfIdenPengirimNas_Ind_pekerjaan.Value = Nothing
    End Sub

    Protected Sub RMPengirimNasKotaDom_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMPengirimNasKotaDom.Click
        TxtIdenPengirimNas_Ind_Kota.Text = ""
        hfIdenPengirimNas_Ind_Kota_dom.Value = Nothing
    End Sub

    Protected Sub RMPengirimNasProvinsiDom_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMPengirimNasProvinsiDom.Click
        TxtIdenPengirimNas_Ind_provinsi.Text = ""
        hfIdenPengirimNas_Ind_provinsi_dom.Value = Nothing
    End Sub

    Protected Sub RMPengirimNasKotaIden_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMPengirimNasKotaIden.Click
        TxtIdenPengirimNas_Ind_kotaIdentitas.Text = ""
        hfIdenPengirimNas_Ind_kotaIdentitas.Value = Nothing
    End Sub

    Protected Sub RMPengirimNasProvinsiIden_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMPengirimNasProvinsiIden.Click
        TxtIdenPengirimNas_Ind_ProvinsiIden.Text = ""
        hfIdenPengirimNas_Ind_ProvinsiIden.Value = Nothing
    End Sub

    Protected Sub RMPengirimNasCorp_BidangUsaha_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMPengirimNasCorp_BidangUsaha.Click
        TxtIdenPengirimNas_Corp_BidangUsahaKorp.Text = ""
        hfIdenPengirimNas_Corp_BidangUsahaKorp.Value = Nothing
    End Sub

    Protected Sub RMPengirimNasKorp_kotaLengkap_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMPengirimNasKorp_kotaLengkap.Click
        TxtIdenPengirimNas_Corp_kotakorp.Text = ""
        hfIdenPengirimNas_Corp_kotakorp.Value = Nothing
    End Sub

    Protected Sub RMPengirimNasKorp_ProvinsiLengkap_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMPengirimNasKorp_ProvinsiLengkap.Click
        TxtIdenPengirimNas_Corp_provKorp.Text = ""
        hfIdenPengirimNas_Corp_provKorp.Value = Nothing
    End Sub

    Protected Sub RMPengirimNonNas_Kota_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMPengirimNonNas_Kota.Click
        TxtPengirimNonNasabah_kotaIden.Text = ""
        hfPengirimNonNasabah_kotaIden.Value = Nothing
    End Sub

    Protected Sub RMPengirimNonNas__Provinsi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMPengirimNonNas__Provinsi.Click
        TxtPengirimNonNasabah_ProvIden.Text = ""
        hfPengirimNonNasabah_ProvIden.Value = Nothing
    End Sub

    Protected Sub RMPengirimPenerus_IND_Negara_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMPengirimPenerus_IND_Negara.Click
        TxtPengirimPenerus_IND_negara.Text = ""
        hfPengirimPenerus_IND_negara.Value = Nothing
    End Sub

    Protected Sub RMPengirimPenerus_IND_Pekerjaan_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMPengirimPenerus_IND_Pekerjaan.Click
        TxtPengirimPenerus_IND_pekerjaan.Text = ""
        hfPengirimPenerus_IND_pekerjaan.Value = Nothing
    End Sub

    Protected Sub RMPengirimPenerus_IND_KotaDom_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMPengirimPenerus_IND_KotaDom.Click
        TxtPengirimPenerus_IND_kotaDom.Text = ""
        hfPengirimPenerus_IND_kotaDom.Value = Nothing
    End Sub

    Protected Sub RMPengirimPenerus_IND_ProvinsiDom_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMPengirimPenerus_IND_ProvinsiDom.Click
        TxtPengirimPenerus_IND_ProvDom.Text = ""
        hfPengirimPenerus_IND_ProvDom.Value = Nothing
    End Sub

    Protected Sub RmPengirimPenerus_IND_KotaIden_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RmPengirimPenerus_IND_KotaIden.Click
        TxtPengirimPenerus_IND_kotaIden.Text = ""
        hfPengirimPenerus_IND_kotaIden.Value = Nothing
    End Sub

    Protected Sub RMPengirimPenerus_IND_ProvinsiIden_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMPengirimPenerus_IND_ProvinsiIden.Click
        TxtPengirimPenerus_IND_ProvIden.Text = ""
        hfPengirimPenerus_IND_ProvIden.Value = Nothing
    End Sub

    Protected Sub RMPengirimPenerus_Korp_bidangUsaha_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMPengirimPenerus_Korp_bidangUsaha.Click
        txtPengirimPenerus_Korp_BidangUsahaKorp.Text = ""
        hfPengirimPenerus_Korp_BidangUsahaKorp.Value = Nothing
    End Sub

    Protected Sub RMPengirimPenerus_Korp_KotaLengkap_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMPengirimPenerus_Korp_KotaLengkap.Click
        txtPengirimPenerus_Korp_kotakorp.Text = ""
        hfPengirimPenerus_Korp_kotakorp.Value = Nothing
    End Sub

    Protected Sub RMPengirimPenerus_Korp_ProvinsiLengkap_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMPengirimPenerus_Korp_ProvinsiLengkap.Click
        txtPengirimPenerus_Korp_ProvinsiKorp.Text = ""
        hfPengirimPenerus_Korp_ProvinsiKorp.Value = Nothing
    End Sub

    Protected Sub RMBOwnerNas_KotaIden_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMBOwnerNas_KotaIden.Click
        BenfOwnerNasabah_KotaIden.Text = ""
        hfBenfOwnerNasabah_KotaIden.Value = Nothing
    End Sub

    Protected Sub RMBOwnerNas_ProvinsiIden_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMBOwnerNas_ProvinsiIden.Click
        BenfOwnerNasabah_ProvinsiIden.Text = ""
        hfBenfOwnerNasabah_ProvinsiIden.Value = Nothing
    End Sub

    Protected Sub RMBOwnerNonNas_kota_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMBOwnerNonNas_kota.Click
        hfBenfOwnerNonNasabah_KotaIden.Value = Nothing
        BenfOwnerNonNasabah_KotaIden.Text = ""

    End Sub

    Protected Sub RMBOwnerNonNas_Provinsi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMBOwnerNonNas_Provinsi.Click
        BenfOwnerNonNasabah_ProvinsiIden.Text = ""
        hfBenfOwnerNonNasabah_ProvinsiIden.Value = Nothing
    End Sub

    Protected Sub RMPenerimaNasabah_IND_negara_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMPenerimaNasabah_IND_negara.Click
        txtPenerimaNasabah_IND_negara.Text = ""
        hfPenerimaNasabah_IND_negara.Value = Nothing

    End Sub

    Protected Sub RMPenerimaNasabah_IND_negaraIden_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMPenerimaNasabah_IND_negaraIden.Click
        txtPenerimaNasabah_IND_negaraIden.Text = ""
        hfPenerimaNasabah_IND_negaraIden.Value = Nothing
    End Sub

    Protected Sub RMPenerimaNas_Korp_bidangUsaha_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMPenerimaNas_Korp_bidangUsaha.Click
        txtPenerimaNasabah_Korp_BidangUsahaKorp.Text = ""
        hfPenerimaNasabah_Korp_BidangUsahaKorp.Value = Nothing
    End Sub

    Protected Sub RMPenerimaNasabah_Korp_NegaraKorp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMPenerimaNasabah_Korp_NegaraKorp.Click
        txtPenerimaNasabah_Korp_NegaraKorp.Text = ""
        hfPenerimaNasabah_Korp_NegaraKorp.Value = Nothing
    End Sub

    Protected Sub RMPenerimaNonNasabah_Negara_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMPenerimaNonNasabah_Negara.Click
        txtPenerimaNonNasabah_Negara.Text = ""
        hfPenerimaNonNasabah_Negara.Value = Nothing
    End Sub

    Protected Sub RMTransaksi_MataUangTransaksi_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMTransaksi_MataUangTransaksi.Click
        Transaksi_MataUangTransaksi.Text = ""
        hfTransaksi_MataUangTransaksi.Value = Nothing
    End Sub

    Protected Sub RMTransaksi_currency_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles RMTransaksi_currency.Click
        Transaksi_currency.Text = ""
        hfTransaksi_currency.Value = Nothing
    End Sub

    Protected Sub CBOIdenPengirimNas_Corp_BentukBadanUsaha_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CBOIdenPengirimNas_Corp_BentukBadanUsaha.SelectedIndexChanged
        Dim ParameterBentukUsaha As MsSystemParameter = DataRepository.MsSystemParameterProvider.GetByPk_MsSystemParameter_Id(AMLBLL.MsSystemParameterBll.IFTI.BentukBadanUsaha)
        If CBOIdenPengirimNas_Corp_BentukBadanUsaha.SelectedValue = ParameterBentukUsaha.MsSystemParameter_Value Then
            Me.TrIdenPengirimNas_Corp_BentukBadanUsahaLain.Visible = True
        Else
            Me.TrIdenPengirimNas_Corp_BentukBadanUsahaLain.Visible = False
        End If
    End Sub

  
End Class
