Imports SahassaNettier.Entities
Imports SahassaNettier.Data
Imports Sahassa.AML
Imports System.Data
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports EKABLL
Partial Class _Default
    Inherits Parent


    Private currentToastr As EKABLL.toastr

    Public ReadOnly Property toastrTitle As String
        Get
            If currentToastr Is Nothing Then Return String.Empty Else Return currentToastr.toastTitle
        End Get
    End Property
    Public ReadOnly Property toastrText As String
        Get
            If currentToastr Is Nothing Then Return String.Empty Else Return currentToastr.toastText
        End Get
    End Property
    Public ReadOnly Property toastrType As String
        Get
            If currentToastr Is Nothing Then Return String.Empty Else Return currentToastr.toastType
        End Get
    End Property
    Public ReadOnly Property toastrPosition As String
        Get
            If currentToastr Is Nothing Then Return String.Empty Else Return currentToastr.toastPosition
        End Get
    End Property

    Public Sub SetToast()







        Dim res As DataTable = EKABLL.ScreeningBLL.GetUserAuditInfo(Sahassa.AML.Commonly.SessionUserId)


        Dim strLastChangePassword As String
        Dim strLastLoginSucess As String
        Dim strLastLoginFail As String

        Dim objsystemparameterLoginSucess As DataTable = EKABLL.ScreeningBLL.GetSystemParameterByPK(9006)
        Dim objsystemparameterLoginFail As DataTable = EKABLL.ScreeningBLL.GetSystemParameterByPK(9007)
        Dim objsystemparameterChangePassword As DataTable = EKABLL.ScreeningBLL.GetSystemParameterByPK(9008)

        'Dim objsystemparameterLoginSucess As Data.DataTable = EKABLL.ScreeningBLL.GetSystemParameterByPK(9001)
        'Dim objsystemparameterLoginFail As NawaDAL.SystemParameterNewFramework = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(9007)
        'Dim objsystemparameterChangePassword As NawaDAL.SystemParameterNewFramework = NawaBLL.SystemParameterBLL.GetSystemParameterByPk(9008)

        strLastLoginSucess = "Last Login Sucess in to AML System in $Date$"
        strLastLoginFail = "Last Login Fail in to AML System in $Date$"
        strLastChangePassword = "Your Password will be expired in $Date$"

        If Not objsystemparameterLoginSucess Is Nothing Then
            strLastLoginSucess = objsystemparameterLoginSucess.Rows(0)(0)
            Dim objdatelastloginsucess As DateTime
            'Dim objdt As DataTable = EKABLL.ScreeningBLL.GetUserAuditInfo(NawaBLL.Common.SessionCurrentUser.UserID)
            Dim objdt As DataTable = EKABLL.ScreeningBLL.GetUserAuditInfo(Sahassa.AML.Commonly.SessionUserId)
            If Not objdt Is Nothing Then

                If Not objdt.Rows(0).Item("LoginSucess") Is DBNull.Value Then
                    objdatelastloginsucess = Convert.ToDateTime(objdt.Rows(0).Item("LoginSucess"))
                End If

            End If
            strLastLoginSucess = strLastLoginSucess.Replace("$Date$", objdatelastloginsucess.ToString("dd-MMM-yyyy HH:mm:ss"))
        End If

        If Not objsystemparameterLoginFail Is Nothing Then
            strLastLoginFail = objsystemparameterLoginFail.Rows(0)(0)
            Dim objdatelastloginfail As DateTime
            Dim objdt As DataTable = EKABLL.ScreeningBLL.GetUserAuditInfo(Sahassa.AML.Commonly.SessionUserId)
            If Not objdt Is Nothing Then
                If Not objdt.Rows(0).Item("LoginFail") Is DBNull.Value Then
                    objdatelastloginfail = Convert.ToDateTime(objdt.Rows(0).Item("LoginFail"))
                End If

            End If
            If objdatelastloginfail = DateTime.MinValue Then
                strLastLoginFail = ""
            Else
                strLastLoginFail = strLastLoginFail.Replace("$Date$", objdatelastloginfail.ToString("dd-MMM-yyyy HH:mm:ss"))
            End If

        End If

        Dim intagepassword As Integer = 30
        If Not objsystemparameterChangePassword Is Nothing Then
            strLastChangePassword = objsystemparameterChangePassword.Rows(0)(0)


            Dim objpasswordexpiredparam As DataTable = EKABLL.ScreeningBLL.GetSystemParameterByPK(1000)

            If Not objpasswordexpiredparam Is Nothing Then
                intagepassword = objpasswordexpiredparam.Rows(0)(0)
            End If

            Dim objdatelastChangePassword As DateTime

            Dim objdt As DataTable = EKABLL.ScreeningBLL.GetUserAuditInfo(Sahassa.AML.Commonly.SessionUserId)
            If Not objdt Is Nothing Then

                If Not objdt.Rows(0).Item("LastChangePassword") Is DBNull.Value Then
                    objdatelastChangePassword = Convert.ToDateTime(objdt.Rows(0).Item("LastChangePassword"))
                    objdatelastChangePassword = DateAdd(DateInterval.Day, intagepassword, objdatelastChangePassword)
                End If





            End If
            strLastChangePassword = strLastChangePassword.Replace("$Date$", objdatelastChangePassword.ToString("dd-MMM-yyyy"))
        End If
        Dim objsettingparamintervalpassword As DataTable = EKABLL.ScreeningBLL.GetSystemParameterByPK(1011)
        Dim strsettingintervalpassword As String = 7
        Dim bShowPasswordExpiredMessage As Boolean = False
        If Not objsettingparamintervalpassword Is Nothing Then
            strsettingintervalpassword = objsettingparamintervalpassword.Rows(0)(0)

            Dim agechangepassword As Integer

            Dim objdt As DataTable = EKABLL.ScreeningBLL.GetUserAuditInfo(Sahassa.AML.Commonly.SessionUserId)
            If Not objdt Is Nothing Then

                If Not objdt.Rows(0).Item("LastChangePassword") Is DBNull.Value Then
                    agechangepassword = intagepassword - DateDiff(DateInterval.Day, objdt.Rows(0).Item("LastChangePassword"), Now)
                End If
            End If


            Dim arrdata() As String = strsettingintervalpassword.Split(",")

            For Each item As String In arrdata
                If item = agechangepassword Then
                    bShowPasswordExpiredMessage = True
                    Exit For
                End If
            Next

            'Dim objresult As String = arrdata.Where(Function(x) x.ToString = agechangepassword).FirstOrDefault

            'If Not objresult Is Nothing Then
            '    bShowPasswordExpiredMessage = True
            'End If

        End If


        Dim strInfo As String = ""


        If bShowPasswordExpiredMessage Then
            strInfo += strLastChangePassword & "</br></br>"

        End If


        strInfo += strLastLoginSucess & "</br></br>"
        strInfo += strLastLoginFail & "</br></br>"
        'strInfo += strLastChangePassword & "</br></br>"



        'Dim builder As New StringBuilder()
        'builder.Append("1. Windows pop up Testing")
        'builder.Append("<br/>")
        'builder.Append("2. Windows pop up Testing 2")

        currentToastr = toastr.GetToast
        'currentToastr = toastr.GetToast1
        'toastr.SetToast(Now.ToString("F"), toastr.ToastPositions.toast_bottom_center)
        toastr.SetToast(strInfo.ToString(), toastr.ToastTypes.info, toastr.ToastPositions.toast_bottom_center)
        'toastr.SetToast1("Windows pop up Testing ke 2 ", toastr.ToastTypes.info, toastr.ToastPositions.toast_top_center)
        'Response.Redirect("Toast2.aspx")
        currentToastr = toastr.GetToast
        'currentToastr = toastr.GetToast1



    End Sub


    Private Sub GetUserAccessMenu()
        Me.SetnGetUserMenu = New List(Of String)
        Using objcmd As New System.Data.SqlClient.SqlCommand("usp_GetUserMenuAccess")
            objcmd.CommandType = System.Data.CommandType.StoredProcedure
            objcmd.CommandTimeout = System.Configuration.ConfigurationManager.AppSettings("SQLCommandTimeout")
            objcmd.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UserId", Sahassa.AML.Commonly.SessionUserId))

            Using dsMenuAccess As DataSet = DataRepository.Provider.ExecuteDataSet(objcmd)
                If dsMenuAccess.Tables.Count > 0 Then
                    For Each drMenuAccess As DataRow In dsMenuAccess.Tables(0).Rows
                        If drMenuAccess("MenuHyperlink").ToString <> "" Then
                            SetnGetUserMenu.Add(drMenuAccess("MenuHyperlink").ToString())
                        End If
                    Next
                End If
            End Using
        End Using
    End Sub

    Private Property SetnGetUserMenu() As List(Of String)
        Get
            Return IIf(Session("Default.MenuAccess") Is Nothing, New List(Of String), Session("Default.MenuAccess"))
        End Get
        Set(ByVal value As List(Of String))
            Session("Default.MenuAccess") = value
        End Set
    End Property

    Private _strMenuHyperlink As String
    Private _currentDataTable As DataTable
    Private _dataTableIndex As Integer = 0
    Private Function FindMenuAccess(ByVal drMenuAccess As String) As Boolean
        If drMenuAccess.ToLower = _strMenuHyperlink.ToLower Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub NewTaskListRepeater_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles NewTaskListRepeater.ItemCommand

        'If e.CommandName = "ShowMoreTaskList" Then
        '    Try
        '        Dim urlPage As String = e.CommandArgument.ToString()

        '        Commonly.SessionIntendedPage = urlPage
        '        'Response.Redirect(urlPage, False)

        '    Catch ex As Exception
        '        Me.cvalPageError.IsValid = False
        '        Me.cvalPageError.ErrorMessage = ex.Message
        '        LogError(ex)
        '    End Try
        'End If

    End Sub
    
    Protected Sub TaskListRepeater_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles TaskListRepeater.ItemCommand
        If e.CommandName = "ShowMoreTaskList" Then
            Try
                'Ambil nilai TaskListID
                Dim TaskListID As Integer = Convert.ToInt32(e.CommandArgument)

                'Redirect ke halaman detail News dgn NewsID tersebut
                Select Case TaskListID
                    Case 1
                        Sahassa.AML.Commonly.SessionIntendedPage = "NewsManagementApproval.aspx"
                        Me.Response.Redirect("NewsManagementApproval.aspx", False)
                    Case 2
                        Sahassa.AML.Commonly.SessionIntendedPage = "GroupManagementApproval.aspx"
                        Me.Response.Redirect("GroupManagementApproval.aspx", False)
                    Case 3
                        Sahassa.AML.Commonly.SessionIntendedPage = "UserManagementApproval.aspx"
                        Me.Response.Redirect("UserManagementApproval.aspx", False)
                    Case 4
                        Sahassa.AML.Commonly.SessionIntendedPage = "LevelTypeManagementApproval.aspx"
                        Me.Response.Redirect("LevelTypeManagementApproval.aspx", False)
                    Case 5
                        Sahassa.AML.Commonly.SessionIntendedPage = "WorkingUnitManagementApproval.aspx"
                        Me.Response.Redirect("WorkingUnitManagementApproval.aspx", False)
                    Case 6
                        Sahassa.AML.Commonly.SessionIntendedPage = "GroupInsiderCodeMappingManagementApproval.aspx"
                        Me.Response.Redirect("GroupInsiderCodeMappingManagementApproval.aspx", False)
                    Case 7
                        Sahassa.AML.Commonly.SessionIntendedPage = "MenuApproval.aspx"
                        Me.Response.Redirect("MenuApproval.aspx", False)

                    Case 8
                        Sahassa.AML.Commonly.SessionIntendedPage = "LoginParameterManagementApproval.aspx"
                        Me.Response.Redirect("LoginParameterManagementApproval.aspx", False)
                    Case 9
                        Sahassa.AML.Commonly.SessionIntendedPage = "SuspectParameterManagementApproval.aspx"
                        Me.Response.Redirect("SuspectParameterManagementApproval.aspx", False)
                    Case 10
                        Sahassa.AML.Commonly.SessionIntendedPage = "PotentialCustomerVerificationParameterManagementApproval.aspx"
                        Me.Response.Redirect("PotentialCustomerVerificationParameterManagementApproval.aspx", False)
                    Case 11
                        Sahassa.AML.Commonly.SessionIntendedPage = "DataUpdatingParameterManagementApproval.aspx"
                        Me.Response.Redirect("DataUpdatingParameterManagementApproval.aspx", False)
                    Case 12
                        Sahassa.AML.Commonly.SessionIntendedPage = "DataUpdatingEmailTemplateManagementApproval.aspx"
                        Me.Response.Redirect("DataUpdatingEmailTemplateManagementApproval.aspx", False)
                    Case 13  'CaseManagementWorkflowSettings
                        'do nothing`
                    Case 14 'ProposalSTRWorkflowSettings
                        'do nothing
                    Case 15
                        Sahassa.AML.Commonly.SessionIntendedPage = "UserWorkingUnitAssignmentManagementApproval.aspx"
                        Me.Response.Redirect("UserWorkingUnitAssignmentManagementApproval.aspx", False)
                    Case 16
                        Sahassa.AML.Commonly.SessionIntendedPage = "AccountOwnerWorkingUnitMappingManagementApproval.aspx"
                        Me.Response.Redirect("AccountOwnerWorkingUnitMappingManagementApproval.aspx", False)
                    Case 17
                        Sahassa.AML.Commonly.SessionIntendedPage = "AuditTrailParameterManagementApproval.aspx"
                        Me.Response.Redirect("AuditTrailParameterManagementApproval.aspx", False)
                    Case 18
                        Sahassa.AML.Commonly.SessionIntendedPage = "VerificationListCategoryManagementApproval.aspx"
                        Me.Response.Redirect("VerificationListCategoryManagementApproval.aspx", False)
                    Case 19
                        Sahassa.AML.Commonly.SessionIntendedPage = "VerificationListManagementApproval.aspx"
                        Me.Response.Redirect("VerificationListManagementApproval.aspx", False)
                    Case 20
                        Sahassa.AML.Commonly.SessionIntendedPage = "CustomerVerificationJudgementView.aspx"
                        Me.Response.Redirect("CustomerVerificationJudgementView.aspx", False)
                    Case 21
                        Sahassa.AML.Commonly.SessionIntendedPage = "CustomerVerificationJudgementApproval.aspx"
                        Me.Response.Redirect("CustomerVerificationJudgementApproval.aspx", False)
                    Case 22 'PEP Upload Approval
                        Sahassa.AML.Commonly.SessionIntendedPage = "VerificationListUploadPEPApproval.aspx"
                        Me.Response.Redirect("VerificationListUploadPEPApproval.aspx", False)
                    Case 23 'SDN Upload Approval
                        Sahassa.AML.Commonly.SessionIntendedPage = "VerificationListUploadOFACSDNApproval.aspx"
                        Me.Response.Redirect("VerificationListUploadOFACSDNApproval.aspx", False)
                    Case 24 'Data Updating
                        Sahassa.AML.Commonly.SessionIntendedPage = "DataUpdatingView.aspx"
                        Me.Response.Redirect("DataUpdatingView.aspx", False)
                    Case 25 'Audit Trail Maintenance Approval
                        Sahassa.AML.Commonly.SessionIntendedPage = "AuditTrailMaintenanceApproval.aspx"
                        Me.Response.Redirect("AuditTrailMaintenanceApproval.aspx", False)
                    Case 26 'Audit Trail User Access Maintenance Approval
                        Sahassa.AML.Commonly.SessionIntendedPage = "AuditTrailUserAccessMaintenanceApproval.aspx"
                        Me.Response.Redirect("AuditTrailUserAccessMaintenanceApproval.aspx", False)
                    Case 27 'Case Management Workflow Approval
                        Sahassa.AML.Commonly.SessionIntendedPage = "CaseManagementWorkflowApproval.aspx"
                        Me.Response.Redirect("CaseManagementWorkflowApproval.aspx", False)
                    Case 28 'Case CTR Exemption Approval
                        Sahassa.AML.Commonly.SessionIntendedPage = "CTRExemptionApproval.aspx"
                        Me.Response.Redirect("CTRExemptionApproval.aspx", False)
                    Case 29 'Case Proposal STR Workflow Setting Approval
                        Sahassa.AML.Commonly.SessionIntendedPage = "ProposalSTRWorkflowSettingsApproval.aspx"
                        Me.Response.Redirect("ProposalSTRWorkflowSettingsApproval.aspx", False)
                    Case 30 'Case Risk Rating Approval
                        Sahassa.AML.Commonly.SessionIntendedPage = "RiskRatingManagementApproval.aspx"
                        Me.Response.Redirect("RiskRatingManagementApproval.aspx", False)
                    Case 31 'Case Aux TRansaction Code Parameter
                        Sahassa.AML.Commonly.SessionIntendedPage = "AuxTransactionCodeParameterApproval.aspx"
                        Me.Response.Redirect("AuxTransactionCodeParameterApproval.aspx", False)
                    Case 32 'Case Rules Basic
                        Sahassa.AML.Commonly.SessionIntendedPage = "RulesBasicApproval.aspx"
                        Me.Response.Redirect("RulesBasicApproval.aspx", False)
                    Case 33 'Case Rules Advanced
                        Sahassa.AML.Commonly.SessionIntendedPage = "AdvancedRulesApproval.aspx"
                        Me.Response.Redirect("AdvancedRulesApproval.aspx", False)
                    Case 34 'Case Peer Group
                        Sahassa.AML.Commonly.SessionIntendedPage = "PeerGroupApproval.aspx"
                        Me.Response.Redirect("PeerGroupApproval.aspx", False)
                    Case 35 'Case Risk Fact
                        Sahassa.AML.Commonly.SessionIntendedPage = "RiskFactApproval.aspx"
                        Me.Response.Redirect("RiskFactApproval.aspx", False)
                    Case 36 'Case Branch Type Mapping
                        Sahassa.AML.Commonly.SessionIntendedPage = "BranchTypeMappingApproval.aspx"
                        Me.Response.Redirect("BranchTypeMappingApproval.aspx", False)
                    Case 37 'Case CTR Parameter
                        Sahassa.AML.Commonly.SessionIntendedPage = "CTRParameterApproval.aspx"
                        Me.Response.Redirect("CTRParameterApproval.aspx", False)
                    Case 38 'Case Transaction Type Auxiliary Transaction Code Mapping
                        Sahassa.AML.Commonly.SessionIntendedPage = "TransactionTypeAuxiliaryMappingApproval.aspx"
                        Me.Response.Redirect("TransactionTypeAuxiliaryMappingApproval.aspx", False)
                    Case 39 'Case Management
                        Sahassa.AML.Commonly.SessionIntendedPage = "CaseManagementView.aspx"
                        Me.Response.Redirect("CaseManagementView.aspx", False)
                    Case 40 'Case Management Has Open Issues
                        Sahassa.AML.Commonly.SessionIntendedPage = "CaseManagementView.aspx"
                        Me.Response.Redirect("CaseManagementView.aspx", False)
                    Case 41 'CTR
                        Sahassa.AML.Commonly.SessionIntendedPage = "CashTransactionReport.aspx"
                        Me.Response.Redirect("CashTransactionReport.aspx", False)
                    Case 42 'Proposal STR Need Response
                        Sahassa.AML.Commonly.SessionIntendedPage = "ProposalSTRResponseView.aspx"
                        Me.Response.Redirect("ProposalSTRResponseView.aspx", False)
                    Case 43 'transaction amount
                        Sahassa.AML.Commonly.SessionIntendedPage = "TransactionAmountApproval.aspx"
                        Me.Response.Redirect("TransactionAmountApproval.aspx", False)
                    Case 44 'transaction frequency
                        Sahassa.AML.Commonly.SessionIntendedPage = "TransactionFrequencyApproval.aspx"
                        Me.Response.Redirect("TransactionFrequencyApproval.aspx", False)
                    Case 45 'sytem parameter
                        Sahassa.AML.Commonly.SessionIntendedPage = "SystemParameterApproval.aspx"
                        Me.Response.Redirect("SystemParameterApproval.aspx", False)

                    Case 46 'resikonasabahbaru
                        Sahassa.AML.Commonly.SessionIntendedPage = "ResikoNasabahBaruApproval.aspx"
                        Me.Response.Redirect("ResikoNasabahBaruApproval.aspx", False)
                    Case 47 'resikonasabahexisting
                        Sahassa.AML.Commonly.SessionIntendedPage = "ResikoNasabahExistingApproval.aspx"
                        Me.Response.Redirect("ResikoNasabahExistingApproval.aspx", False)
                    Case 48
                        Sahassa.AML.Commonly.SessionIntendedPage = "SuspiciusPersonApproval.aspx"
                        Me.Response.Redirect("SuspiciusPersonApproval.aspx", False)
                    Case 49
                        Sahassa.AML.Commonly.SessionIntendedPage = "YearIntervalCDDLowRiskApproval.aspx"
                        Me.Response.Redirect("YearIntervalCDDLowRiskApproval.aspx", False)

                    Case 50
                        Sahassa.AML.Commonly.SessionIntendedPage = "UploadWorkFlowApproval.aspx"
                        Me.Response.Redirect("UploadWorkFlowApproval.aspx", False)


                    Case 51
                        Sahassa.AML.Commonly.SessionIntendedPage = "VerificationListUploadUNListApproval.aspx"
                        Me.Response.Redirect("VerificationListUploadUNListApproval.aspx", False)
                    Case 52
                        Sahassa.AML.Commonly.SessionIntendedPage = "YearIntervalCDDHighRiskApproval.aspx"
                        Me.Response.Redirect("YearIntervalCDDHighRiskApproval.aspx", False)

                    Case 53 'Case Management
                        Sahassa.AML.Commonly.SessionIntendedPage = "CaseManagementView.aspx"
                        Me.Response.Redirect("CaseManagementView.aspx?CaseDescription=Artificial STR", False)

                    Case 54 'Case Management
                        Sahassa.AML.Commonly.SessionIntendedPage = "VerificationListCorrectionView.aspx"
                        Me.Response.Redirect("VerificationListCorrectionView.aspx", False)
                    Case 55 'Case Management
                        Sahassa.AML.Commonly.SessionIntendedPage = "DataUpdatingAlertApproval.aspx"
                        Me.Response.Redirect("DataUpdatingAlertApproval.aspx", False)
                    Case 56 'WorkflowHistoryAccess
                        Sahassa.AML.Commonly.SessionIntendedPage = "WorkFlowHistoryAccessApproval.aspx"
                        Me.Response.Redirect("WorkFlowHistoryAccessApproval.aspx", False)
                    Case 57 'WorkflowHistoryAccess
                        Sahassa.AML.Commonly.SessionIntendedPage = "TransactionPenghasilan_Approval.aspx"
                        Me.Response.Redirect("TransactionPenghasilan_Approval.aspx", False)
                    Case 58 'WorkflowHistoryAccess
                        Sahassa.AML.Commonly.SessionIntendedPage = "TransactionNTNApproval.aspx"
                        Me.Response.Redirect("TransactionNTNApproval.aspx", False)
                    Case 59 'uploadhiportaccount
                        Sahassa.AML.Commonly.SessionIntendedPage = "UploadHiportAccountApproval.aspx"
                        Me.Response.Redirect("UploadHiportAccountApproval.aspx", False)
                    Case 60 'insidercode
                        Sahassa.AML.Commonly.SessionIntendedPage = "InsiderCodeApproval.aspx"
                        Me.Response.Redirect("InsiderCodeApproval.aspx", False)
                    Case 61 'VIPCode
                        Sahassa.AML.Commonly.SessionIntendedPage = "VIPCodeApproval.aspx"
                        Me.Response.Redirect("VIPCodeApproval.aspx", False)
                    Case 62 'OutlierToleransi
                        Sahassa.AML.Commonly.SessionIntendedPage = "OutlierToleransiApproval.aspx"
                        Me.Response.Redirect("OutlierToleransiApproval.aspx", False)
                    Case 63 'Segment
                        Sahassa.AML.Commonly.SessionIntendedPage = "SegmentApproval.aspx"
                        Me.Response.Redirect("SegmentApproval.aspx", False)

                    Case 64 'MappingGroupMenuHiportJive
                        Sahassa.AML.Commonly.SessionIntendedPage = "MappingGroupMenuHiportJiveApproval.aspx"
                        Me.Response.Redirect("MappingGroupMenuHiportJiveApproval.aspx", False)
                    Case 65 'MappingGroupMenuSTRPPATK
                        Sahassa.AML.Commonly.SessionIntendedPage = "MappingGroupMenuSTRPPATKApproval.aspx"
                        Me.Response.Redirect("MappingGroupMenuSTRPPATKApproval.aspx", False)
                    Case 66 'MappingReportBuilderWorkingUnit
                        Sahassa.AML.Commonly.SessionIntendedPage = "MappingReportBuilderWorkingUnitApproval.aspx"
                        Me.Response.Redirect("MappingReportBuilderWorkingUnitApproval.aspx", False)
                    Case 67 'MappingUserIdHiportJive
                        Sahassa.AML.Commonly.SessionIntendedPage = "MappingUserIdHiportJiveApproval.aspx"
                        Me.Response.Redirect("MappingUserIdHiportJiveApproval.aspx", False)
                    Case 68 '   QuestionaireSTR
                        Sahassa.AML.Commonly.SessionIntendedPage = "QuestionaireSTRApproval.aspx"
                        Me.Response.Redirect("QuestionaireSTRApproval.aspx", False)

                    Case 73
                        Sahassa.AML.Commonly.SessionIntendedPage = "MsBentukBidangUsaha_Approval_view.aspx"
                        Me.Response.Redirect("MsBentukBidangUsaha_Approval_view.aspx")
                    Case 74
                        Sahassa.AML.Commonly.SessionIntendedPage = "MsBidangUsaha_Approval_view.aspx"
                        Me.Response.Redirect("MsBidangUsaha_Approval_view.aspx")
                    Case 75
                        Sahassa.AML.Commonly.SessionIntendedPage = "MsCurrency_Approval_view.aspx"
                        Me.Response.Redirect("MsCurrency_Approval_view.aspx")
                    Case 76
                        Sahassa.AML.Commonly.SessionIntendedPage = "MsKecamatan_Approval_view.aspx"
                        Me.Response.Redirect("MsKecamatan_Approval_view.aspx")
                    Case 77
                        Sahassa.AML.Commonly.SessionIntendedPage = "MsKelurahan_Approval_view.aspx"
                        Me.Response.Redirect("MsKelurahan_Approval_view.aspx")
                    Case 78
                        Sahassa.AML.Commonly.SessionIntendedPage = "MsKepemilikan_Approval_view.aspx"
                        Me.Response.Redirect("MsKepemilikan_Approval_view.aspx")
                    Case 79
                        Sahassa.AML.Commonly.SessionIntendedPage = "MsKotaKab_Approval_view.aspx"
                        Me.Response.Redirect("MsKotaKab_Approval_view.aspx")
                    Case 80
                        Sahassa.AML.Commonly.SessionIntendedPage = "MsNegara_Approval_view.aspx"
                        Me.Response.Redirect("MsNegara_Approval_view.aspx")
                    Case 81
                        Sahassa.AML.Commonly.SessionIntendedPage = "MsPekerjaan_Approval_view.aspx"
                        Me.Response.Redirect("MsPekerjaan_Approval_view.aspx")
                    Case 82
                        Sahassa.AML.Commonly.SessionIntendedPage = "MsProfession_Approval_view.aspx"
                        Me.Response.Redirect("MsProfession_Approval_view.aspx")
                    Case 83
                        Sahassa.AML.Commonly.SessionIntendedPage = "MsProvince_Approval_view.aspx"
                        Me.Response.Redirect("MsProvince_Approval_view.aspx")
                    Case 84
                        Sahassa.AML.Commonly.SessionIntendedPage = "LTKT_APPROVAL_View.aspx"
                        Me.Response.Redirect("LTKT_APPROVAL_View.aspx")
                    Case 85
                        Sahassa.AML.Commonly.SessionIntendedPage = "WIC_AprovalView.aspx"
                        Me.Response.Redirect("WIC_AprovalView.aspx")
                    Case 86
                        Sahassa.AML.Commonly.SessionIntendedPage = "MsBentukBidangUsaha_UploadApproval_view.aspx"
                        Me.Response.Redirect("MsBentukBidangUsaha_UploadApproval_view.aspx")
                    Case 87
                        Sahassa.AML.Commonly.SessionIntendedPage = "MsBidangUsaha_UploadApproval_view.aspx"
                        Me.Response.Redirect("MsBidangUsaha_UploadApproval_view.aspx")
                    Case 88
                        Sahassa.AML.Commonly.SessionIntendedPage = "MsCurrency_UploadApproval_view.aspx"
                        Me.Response.Redirect("MsCurrency_UploadApproval_view.aspx")
                    Case 89
                        Sahassa.AML.Commonly.SessionIntendedPage = "MsKecamatan_UploadApproval_view.aspx"
                        Me.Response.Redirect("MsKecamatan_UploadApproval_view.aspx")
                    Case 90
                        Sahassa.AML.Commonly.SessionIntendedPage = "MsKelurahan_UploadApproval_view.aspx"
                        Me.Response.Redirect("MsKelurahan_UploadApproval_view.aspx")
                    Case 91
                        Sahassa.AML.Commonly.SessionIntendedPage = "MsKepemilikan_UploadApproval_view.aspx"
                        Me.Response.Redirect("MsKepemilikan_UploadApproval_view.aspx")
                    Case 92
                        Sahassa.AML.Commonly.SessionIntendedPage = "MsKotaKab_UploadApproval_view.aspx"
                        Me.Response.Redirect("MsKotaKab_UploadApproval_view.aspx")
                    Case 93
                        Sahassa.AML.Commonly.SessionIntendedPage = "MsNegara_UploadApproval_view.aspx"
                        Me.Response.Redirect("MsNegara_UploadApproval_view.aspx")
                    Case 94
                        Sahassa.AML.Commonly.SessionIntendedPage = "MsPekerjaan_UploadApproval_view.aspx"
                        Me.Response.Redirect("MsPekerjaan_UploadApproval_view.aspx")
                    Case 95
                        Sahassa.AML.Commonly.SessionIntendedPage = "MsProvince_UploadApproval_view.aspx"
                        Me.Response.Redirect("MsProvince_UploadApproval_view.aspx")
                    Case 96
                        Sahassa.AML.Commonly.SessionIntendedPage = "CTRReportControlGeneratorApproval.aspx"
                        Me.Response.Redirect("CTRReportControlGeneratorApproval.aspx")

                    Case 97
                        Sahassa.AML.Commonly.SessionIntendedPage = "ListofGeneratedCTRFile.aspx"
                        Me.Response.Redirect("ListofGeneratedCTRFile.aspx", False)

                    Case 98
                        Sahassa.AML.Commonly.SessionIntendedPage = "WICView.aspx"
                        Me.Response.Redirect("WICView.aspx", False)

                    Case 99
                        Sahassa.AML.Commonly.SessionIntendedPage = "LTKTVIew.aspx"
                        Me.Response.Redirect("LTKTVIew.aspx", False)

                    Case 101
                        Sahassa.AML.Commonly.SessionIntendedPage = "LTKTAPPROVAL.aspx"
                        Me.Response.Redirect("LTKTAPPROVAL.aspx")
                    Case 102
                        Sahassa.AML.Commonly.SessionIntendedPage = "WICAPPROVAL.aspx"
                        Me.Response.Redirect("WICAPPROVAL.aspx")
                    Case 103
                        Sahassa.AML.Commonly.SessionIntendedPage = "IFTI_Approvals.aspx"
                        Me.Response.Redirect("IFTI_Approvals.aspx")
                    Case 104
                        Sahassa.AML.Commonly.SessionIntendedPage = "WUUpload_Approval.aspx"
                        Me.Response.Redirect("WUUpload_Approval.aspx")
                    Case 105 'need approval
                        Sahassa.AML.Commonly.SessionIntendedPage = "ListofGeneratedIFTIFile.aspx"
                        Me.Response.Redirect("ListofGeneratedIFTIFile.aspx")

                    Case 106
                        Sahassa.AML.Commonly.SessionIntendedPage = "CDDLNPMappingQuestionApproval.aspx"
                        Me.Response.Redirect("CDDLNPMappingQuestionApproval.aspx")
                    Case 107
                        Sahassa.AML.Commonly.SessionIntendedPage = "CDDLNPWorkflowParameterApproval.aspx"
                        Me.Response.Redirect("CDDLNPWorkflowParameterApproval.aspx")
                    Case 108
                        Sahassa.AML.Commonly.SessionIntendedPage = "CDDLNPWorkflowUploadApproval.aspx"
                        Me.Response.Redirect("CDDLNPWorkflowUploadApproval.aspx")
                    Case 109
                        Sahassa.AML.Commonly.SessionIntendedPage = "CDDLNPQuestionApproval.aspx"
                        Me.Response.Redirect("CDDLNPQuestionApproval.aspx")
                    Case 110
                        Sahassa.AML.Commonly.SessionIntendedPage = "CDDRiskFactQuestionApproval.aspx"
                        Me.Response.Redirect("CDDRiskFactQuestionApproval.aspx")
                    Case 111
                        Sahassa.AML.Commonly.SessionIntendedPage = "CDDLNPApproval.aspx"
                        Me.Response.Redirect("CDDLNPApproval.aspx")
                    Case 112
                        Sahassa.AML.Commonly.SessionIntendedPage = "UploadURSCustody_Approval.aspx"
                        Me.Response.Redirect("UploadURSCustody_Approval.aspx")
                    Case 113
                        Sahassa.AML.Commonly.SessionIntendedPage = "CDDLNPHeaderApproval.aspx"
                        Me.Response.Redirect("CDDLNPHeaderApproval.aspx")
                    Case 114
                        Sahassa.AML.Commonly.SessionIntendedPage = "Bancassurance_Upload_Approval.aspx"
                        Me.Response.Redirect("Bancassurance_Upload_Approval.aspx")
                    Case 115
                        Sahassa.AML.Commonly.SessionIntendedPage = "UploadViewBlackListAML_Approval.aspx"
                        Me.Response.Redirect("UploadViewBlackListAML_Approval.aspx")
                    Case 116
                        Sahassa.AML.Commonly.SessionIntendedPage = "StrukturOrganisasi_Upload_Approval.aspx"
                        Me.Response.Redirect("StrukturOrganisasi_Upload_Approval.aspx")
                    Case 117
                        Sahassa.AML.Commonly.SessionIntendedPage = "CustomerVerificationJudgementWebAPIView.aspx"
                        Me.Response.Redirect("CustomerVerificationJudgementWebAPIView.aspx")
                    Case Else
                        Throw New Exception("Unknown TaskListID.")
                End Select
            Catch ex As Exception
                Me.cvalPageError.IsValid = False
                Me.cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            End Try
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                If SetnGetUserMenu.Count = 0 Then
                    Me.GetUserAccessMenu()
                End If

                GetTaskList()

                '===================================================================

                Dim myDt As New Data.DataTable()
                myDt = CreateDataTableTaskList()
                Me.FillTaskLists(myDt)

                Me.TaskListRepeater.DataSource = myDt
                Me.TaskListRepeater.DataBind()
                Me.TaskListRepeater.Visible = True

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using

                If System.Web.HttpContext.Current.Session("toastr") Is Nothing Then
                    SetToast()

                End If

            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub GetTaskList()

        'Dim currentDataTable As DataTable = CreateTaskListStructure()
        _currentDataTable = CreateTaskListStructure()
        Dim lstCustomTaskList As List(Of EKABLL.CustomTaskListData) = EKABLL.CustomTaskListBLL.GetAllCustomTaskListModule(Sahassa.AML.Commonly.SessionUserId)
        'Dim totalData As Integer

        If lstCustomTaskList.Count = 0 Then Return
        
        For Each builderData As EKABLL.CustomTaskListData In lstCustomTaskList

            AddNewData(_currentDataTable, builderData)
        Next

        With NewTaskListRepeater
            '.DataSource = currentDataTable
            .DataSource = _currentDataTable
            .DataBind()
            .Visible = True
        End With

    End Sub

    Private Sub AddNewData(dataTableParam As DataTable, customTaskListDataParam As EKABLL.CustomTaskListData)

        Dim newRow As DataRow = dataTableParam.NewRow()

        newRow("TaskName") = customTaskListDataParam.Name
        newRow("ID") = customTaskListDataParam.CustomTaskListID
        'newRow("TaskName") = customTaskListDataParam.Name.Replace("{TotalTask}", totalDataParam.ToString())
        'newRow("TaskURL") = "window.open('showwatchlist.aspx?uid=" & 1 & "', 'ca', 'width=500,height=550,left=500,top=500,resizable=yes,scrollbars=yes')"

        dataTableParam.Rows.Add(newRow)

    End Sub

    Private Sub NewTaskListRepeater_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles NewTaskListRepeater.ItemDataBound
        
        If (e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem) Then
            Dim linkButton As LinkButton = CType(e.Item.FindControl("ShowMoreTaskList"), LinkButton)

            linkButton.OnClientClick = "window.open('ShowTaskList.aspx?uid=" & _currentDataTable.Rows(_dataTableIndex).Item("ID").ToString() & "', 'ca', 'width=500,height=550,left=500,top=500,resizable=yes,scrollbars=yes')"

        End If

        _dataTableIndex += 1

    End Sub

    'Private Function GetCurrentUserTotalTaskList() As DataTable

    '    Dim currentUserName As String = Commonly.SessionUserId
    '    Dim totalTask As Integer 

    '    totalTask = Commonly.GetTableCount("Lap_Bulanan", "WHERE CustomerName LIKE '%" & currentUserName & "%'")

    'End Function

    Private Function CreateTaskListStructure() As Data.DataTable

        Dim currentDataTable As New DataTable()
        Dim currentColumn As DataColumn

        currentColumn = New DataColumn()
        currentColumn.DataType = GetType(STring)
        currentColumn.ColumnName = "TaskName"
        currentDataTable.Columns.Add(currentColumn)

        currentColumn = New DataColumn()
        currentColumn.DataType = GetType(String)
        currentColumn.ColumnName = "TaskURL"
        currentDataTable.Columns.Add(currentColumn)

        currentColumn = New DataColumn()
        currentColumn.DataType = GetType(String)
        currentColumn.ColumnName = "ID"
        currentDataTable.Columns.Add(currentColumn)

        Return currentDataTable

    End Function

    Private Sub FillTaskLists(ByRef mydt As Data.DataTable)
        Dim MatchCount As Int32 = 0
        Dim CurrentUserLoginID As String = Sahassa.AML.Commonly.SessionUserId
        Dim IntTotalMenu As Integer = 0

        Try

            _strMenuHyperlink = "NewsManagementApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using AccessNewsPending As New AMLDAL.AMLDataSetTableAdapters.News_PendingApprovalTableAdapter
                    MatchCount = AccessNewsPending.CountNewsPendingTaskList(CurrentUserLoginID)
                    If MatchCount <> 0 Then
                        AddTaskListsToTable(1, MatchCount & " News Approval Required.", mydt)
                    End If
                End Using
            End If

            _strMenuHyperlink = "GroupManagementApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using AccessGroupPending As New AMLDAL.AMLDataSetTableAdapters.Group_PendingApprovalTableAdapter
                    MatchCount = AccessGroupPending.CountGroupPendingTaskList(CurrentUserLoginID)
                    If MatchCount <> 0 Then
                        AddTaskListsToTable(2, MatchCount & " Group Approval Required.", mydt)
                    End If
                End Using
            End If

            _strMenuHyperlink = "UserManagementApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using AccessUserPending As New AMLDAL.AMLDataSetTableAdapters.User_PendingApprovalTableAdapter
                    MatchCount = AccessUserPending.CountUserPendingTaskList(CurrentUserLoginID)
                    If MatchCount <> 0 Then
                        AddTaskListsToTable(3, MatchCount & " User Approval Required.", mydt)
                    End If
                End Using
            End If

            _strMenuHyperlink = "LevelTypeManagementApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using AccessLevelTypePending As New AMLDAL.AMLDataSetTableAdapters.LevelType_PendingApprovalTableAdapter
                    MatchCount = AccessLevelTypePending.CountLevelTypePendingTaskList(CurrentUserLoginID)
                    If MatchCount <> 0 Then
                        AddTaskListsToTable(4, MatchCount & " Level Type Approval Required.", mydt)
                    End If
                End Using
            End If

            _strMenuHyperlink = "WorkingUnitManagementApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using AccessWorkingUnitPending As New AMLDAL.AMLDataSetTableAdapters.WorkingUnit_PendingApprovalTableAdapter
                    MatchCount = AccessWorkingUnitPending.CountWorkingUnitPendingTaskList(CurrentUserLoginID)
                    If MatchCount <> 0 Then
                        AddTaskListsToTable(5, MatchCount & " Working Unit Approval Required.", mydt)
                    End If
                End Using
            End If

            _strMenuHyperlink = "GroupInsiderCodeMappingManagementApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using AccessGroupInsiderCodeMappingPending As New AMLDAL.AMLDataSetTableAdapters.GroupInsiderCodeMapping_PendingApprovalTableAdapter
                    MatchCount = AccessGroupInsiderCodeMappingPending.CountGroupInsiderCodeMappingPendingTaskList(CurrentUserLoginID)
                    If MatchCount <> 0 Then
                        AddTaskListsToTable(6, MatchCount & " Group Insider Code Mapping Approval Required.", mydt)
                    End If
                End Using
            End If

            _strMenuHyperlink = "MenuApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using AccessMenuPending As New AMLDAL.AMLDataSetTableAdapters.Menu_PendingApprovalTableAdapter
                    MatchCount = AccessMenuPending.CountMenuPendingTaskList(CurrentUserLoginID)
                    If MatchCount <> 0 Then
                        AddTaskListsToTable(7, MatchCount & " Menu Approval Required.", mydt)
                    End If
                End Using
            End If


            Using AccessParameter As New AMLDAL.AMLDataSetTableAdapters.Parameters_PendingApprovalTableAdapter
                Dim j As Integer = 8
                For i As Integer = 1 To 10
                    MatchCount = 0
                    Select Case i
                        Case 1
                            _strMenuHyperlink = "LoginParameterManagementApproval.aspx"
                            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                                MatchCount = AccessParameter.CountLoginParametersPendingTaskList(CurrentUserLoginID)
                                If MatchCount <> 0 Then
                                    AddTaskListsToTable(j, MatchCount & " Login Parameter Approval Required.", mydt)
                                End If
                            End If

                        Case 2
                            _strMenuHyperlink = "SuspectParameterManagementApproval.aspx"
                            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                                MatchCount = AccessParameter.CountSuspectParameterPendingTaskList(CurrentUserLoginID)
                                If MatchCount <> 0 Then
                                    AddTaskListsToTable(j, MatchCount & " Suspect Parameter Approval Required.", mydt)
                                End If
                            End If

                        Case 3
                            _strMenuHyperlink = "PotentialCustomerVerificationParameterManagementApproval.aspx"
                            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                                MatchCount = AccessParameter.CountPotentialCustomerVerificationParameterPendingTaskList(CurrentUserLoginID)
                                If MatchCount <> 0 Then
                                    AddTaskListsToTable(j, MatchCount & " CustomerSreeningParameter Approval Required.", mydt)
                                End If
                            End If

                        Case 4
                            _strMenuHyperlink = "DataUpdatingParameterManagementApproval.aspx"
                            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                                MatchCount = AccessParameter.CountDataUpdatingParameterPendingTaskList(CurrentUserLoginID)
                                If MatchCount <> 0 Then
                                    AddTaskListsToTable(j, MatchCount & " DataUpdatingParameter Approval Required.", mydt)
                                End If
                            End If

                        Case 5
                            _strMenuHyperlink = "DataUpdatingEmailTemplateManagementApproval.aspx"
                            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                                MatchCount = AccessParameter.CountDataUpdatingEmailTemplatePendingTaskList(CurrentUserLoginID)
                                If MatchCount <> 0 Then
                                    AddTaskListsToTable(j, MatchCount & " DataUpdatingEmailTemplate Approval Required.", mydt)
                                End If
                            End If

                        'Case 6
                            ' NOT USED --> JOHAN 25 FEB 2011
                            'label = "  "
                            'If MatchCount <> 0 Then
                            '    AddTaskListsToTable(j, MatchCount & " CaseManagementWorkflowSettings Approval Required.", mydt)
                            'End If
                        'Case 7
                            ' NOT USED --> JOHAN 25 FEB 2011    
                            'label = "  "
                            'If MatchCount <> 0 Then
                            '    AddTaskListsToTable(j, MatchCount & " ProposalSTRWorkflowSettings Approval Required.", mydt)
                            'End If
                        Case 8
                            _strMenuHyperlink = "UserWorkingUnitAssignmentManagementApproval.aspx"
                            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                                MatchCount = AccessParameter.CountUserWorkingUnitAssignmentPendingTaskList(CurrentUserLoginID)
                                If MatchCount <> 0 Then
                                    AddTaskListsToTable(j, MatchCount & " UserWorkingUnitAssignment Approval Required.", mydt)
                                End If
                            End If

                        Case 9
                            _strMenuHyperlink = "AccountOwnerWorkingUnitMappingManagementApproval.aspx"
                            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                                MatchCount = AccessParameter.CountAccountOwnerWorkingUnitMappingPendingTaskList(CurrentUserLoginID)
                                If MatchCount <> 0 Then
                                    AddTaskListsToTable(j, MatchCount & " AccountOwnerWorkingUnitMapping Approval Required.", mydt)
                                End If
                            End If

                        Case 10
                            _strMenuHyperlink = "AuditTrailParameterManagementApproval.aspx"
                            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                                MatchCount = AccessParameter.CountAuditTrailParameterPendingTaskList(CurrentUserLoginID)
                                If MatchCount <> 0 Then
                                    AddTaskListsToTable(j, MatchCount & "AuditTrailParameter Approval Required.", mydt)
                                End If
                            End If

                        Case Else
                            'do nothing
                    End Select

                    j += 1

                Next
            End Using

            _strMenuHyperlink = "VerificationListCategoryManagementApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using AccessVerificationListCategoryPending As New AMLDAL.AMLDataSetTableAdapters.VerificationListCategory_PendingApprovalTableAdapter
                    MatchCount = AccessVerificationListCategoryPending.CountVerificationListCategoryPendingTaskList(CurrentUserLoginID)
                    If MatchCount <> 0 Then
                        AddTaskListsToTable(18, MatchCount & " Verification List Category Approval Required.", mydt)
                    End If
                End Using
            End If

            _strMenuHyperlink = "VerificationListManagementApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using AccessVerificationListPending As New AMLDAL.AMLDataSetTableAdapters.VerificationList_Master_PendingApprovalTableAdapter
                    MatchCount = AccessVerificationListPending.CountVerificationListPendingTaskList(CurrentUserLoginID)
                    If MatchCount <> 0 Then
                        AddTaskListsToTable(19, MatchCount & " Verification List Approval Required.", mydt)
                    End If
                End Using
            End If

            _strMenuHyperlink = "CustomerVerificationJudgementView.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then

                'Using objdata As EkaDataNettier.Entities.TList(Of EkaDataNettier.Entities.AML_Screening_Customer_Judgement) = EkaDataNettier.Data.DataRepository.AML_Screening_Customer_JudgementProvider.GetPaged(" JudgementResult IS  NULL AND AccountOwnerID IN (SELECT AccountOwnerId FROM AccountOwnerUserMapping WHERE  userid ='" & Commonly.SessionUserId & "')", "", 0, Integer.MaxValue, 0)
                Dim objdata As Integer = EKABLL.ScreeningBLL.GetcountJudgement(Sahassa.AML.Commonly.SessionUserId)

                If objdata > 0 Then
                    AddTaskListsToTable(20, "You have " & objdata & " Customer Verification Judgement Required.", mydt)
                End If


                'End Using


            End If

            _strMenuHyperlink = "CustomerVerificationJudgementWebAPIView.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Dim objdata As Integer = EKABLL.ScreeningBLL.GetCountWebAPIScreeningUnjudged(Sahassa.AML.Commonly.SessionUserId)

                If objdata > 0 Then
                    AddTaskListsToTable(117, "You have " & objdata & " data Screening Judgement WebAPI that need judged", mydt)
                End If
            End If

            _strMenuHyperlink = "VerificationListCorrectionView.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                MatchCount = AMLBLL.VerificationListBLL.getCountVerificationListMasterCorrection(CurrentUserLoginID)
                If MatchCount <> 0 Then
                    AddTaskListsToTable(54, MatchCount & " Customer Verification Correction Required.", mydt)
                End If
            End If

            _strMenuHyperlink = "VerificationListUploadPEPApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using AccessPEPUploadPending As New AMLDAL.PEPTableAdapters.CountPEPUploadPendingTaskListTableAdapter
                    Dim objTable As Data.DataTable = AccessPEPUploadPending.GetData(CurrentUserLoginID)
                    Dim objRow As AMLDAL.PEP.CountPEPUploadPendingTaskListRow = objTable.Rows(0)
                    MatchCount = objRow.MatchCount
                    If MatchCount <> 0 Then
                        AddTaskListsToTable(22, MatchCount & " Verification List Upload PEP List Approval Required.", mydt)
                    End If
                End Using
            End If

            _strMenuHyperlink = "VerificationListUploadOFACSDNApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using AccessSDNUploadPending As New AMLDAL.SDNTableAdapters.CountSDNUploadPendingTaskListTableAdapter
                    Dim objTable As Data.DataTable = AccessSDNUploadPending.GetData(CurrentUserLoginID)
                    Dim objRow As AMLDAL.SDN.CountSDNUploadPendingTaskListRow = objTable.Rows(0)
                    MatchCount = objRow.MatchCount
                    If MatchCount <> 0 Then
                        AddTaskListsToTable(23, MatchCount & " Verification List OFAC SDN List Approval Required.", mydt)
                    End If
                End Using
            End If

            'Data Updating PendingTaskList
            MatchCount = 0

            _strMenuHyperlink = "DataUpdatingView.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using TADataUpdatingTaskList As New AMLDAL.PendingTaskListTableAdapters.CountDataUpdatingTaskListTableAdapter
                    Dim objtable As Data.DataTable = TADataUpdatingTaskList.GetData(CurrentUserLoginID)
                    Dim objRow As AMLDAL.PendingTaskList.CountDataUpdatingTaskListRow = objtable.Rows(0)
                    MatchCount = objRow.MatchCount
                    If MatchCount <> 0 Then
                        AddTaskListsToTable(24, MatchCount & " Customers'EDD to be Updated", mydt)
                    End If
                End Using
            End If

            _strMenuHyperlink = "AuditTrailMaintenanceApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using AccessAuditTrailMaintenancePending As New AMLDAL.AMLDataSetTableAdapters.CountAuditTrailMaintenancePendingTaskListTableAdapter()
                    Dim objtable As Data.DataTable = AccessAuditTrailMaintenancePending.GetData(CurrentUserLoginID)
                    Dim objRow As AMLDAL.AMLDataSet.CountAuditTrailMaintenancePendingTaskListRow = objtable.Rows(0)
                    MatchCount = objRow.MatchCount
                    If MatchCount <> 0 Then
                        AddTaskListsToTable(25, MatchCount & " Audit Trail Maintenance Approval Required.", mydt)
                    End If
                End Using
            End If

            _strMenuHyperlink = "AuditTrailUserAccessMaintenanceApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using AccessAuditTrailUserAccessMaintenancePending As New AMLDAL.AMLDataSetTableAdapters.CountAuditTrailUserAccessMaintenancePendingTaskListTableAdapter()
                    Dim objtable As Data.DataTable = AccessAuditTrailUserAccessMaintenancePending.GetData(CurrentUserLoginID)
                    Dim objRow As AMLDAL.AMLDataSet.CountAuditTrailUserAccessMaintenancePendingTaskListRow = objtable.Rows(0)
                    MatchCount = objRow.MatchCount
                    If MatchCount <> 0 Then
                        AddTaskListsToTable(26, MatchCount & " Audit Trail User Access Maintenance Approval Required.", mydt)
                    End If
                End Using
            End If

            _strMenuHyperlink = "CaseManagementWorkflowApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using AccessCaseManagementWorkFlow As New AMLDAL.PendingTaskListTableAdapters.CountCaseManagementWorkflowApprovalTaskListTableAdapter
                    Dim objtable As Data.DataTable = AccessCaseManagementWorkFlow.GetData(CurrentUserLoginID)
                    Dim objRow As AMLDAL.PendingTaskList.CountCaseManagementWorkflowApprovalTaskListRow = objtable.Rows(0)
                    MatchCount = objRow.MatchCount
                    If MatchCount <> 0 Then
                        AddTaskListsToTable(27, MatchCount & " Case Management Workflow Approval Required.", mydt)
                    End If
                End Using
            End If

            _strMenuHyperlink = "CTRExemptionApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using AccessCTRExemtion As New AMLDAL.PendingTaskListTableAdapters.CountCTRExemptionListApprovalTaskListTableAdapter
                    Dim objtable As Data.DataTable = AccessCTRExemtion.GetData(CurrentUserLoginID)
                    Dim objRow As AMLDAL.PendingTaskList.CountCTRExemptionListApprovalTaskListRow = objtable.Rows(0)
                    MatchCount = objRow.MatchCount
                    If MatchCount <> 0 Then
                        AddTaskListsToTable(28, MatchCount & " CTR Exemption Approval Required.", mydt)
                    End If
                End Using
            End If

            _strMenuHyperlink = "ProposalSTRWorkflowSettingsApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using AccessProposalCTRWorkFlow As New AMLDAL.PendingTaskListTableAdapters.CountProposalSTRWorkflowGeneralApprovalTaskListTableAdapter
                    Dim objtable As Data.DataTable = AccessProposalCTRWorkFlow.GetData(CurrentUserLoginID)
                    Dim objRow As AMLDAL.PendingTaskList.CountProposalSTRWorkflowGeneralApprovalTaskListRow = objtable.Rows(0)
                    MatchCount = objRow.MatchCount
                    If MatchCount <> 0 Then
                        AddTaskListsToTable(29, MatchCount & " Proposal STR Workflow Setting Approval Required.", mydt)
                    End If
                End Using
            End If

            _strMenuHyperlink = "RiskRatingManagementApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using AccessRiskRating As New AMLDAL.PendingTaskListTableAdapters.CountRiskRatingApprovalTaskListTableAdapter
                    Dim objtable As Data.DataTable = AccessRiskRating.CountRiskRatingApprovalTaskList(CurrentUserLoginID)
                    Dim objRow As AMLDAL.PendingTaskList.CountRiskRatingApprovalTaskListRow = objtable.Rows(0)
                    MatchCount = objRow.MatchCount
                    If MatchCount <> 0 Then
                        AddTaskListsToTable(30, MatchCount & " Risk Rating Approval Required.", mydt)
                    End If
                End Using
            End If

            _strMenuHyperlink = "AuxTransactionCodeParameterApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using AccessAuxTransCode As New AMLDAL.PendingTaskListTableAdapters.CountAuxTransactionCodeParameterPendingApprovalTaskListTableAdapter
                    Dim objtable As Data.DataTable = AccessAuxTransCode.GetData(CurrentUserLoginID)
                    Dim objRow As AMLDAL.PendingTaskList.CountAuxTransactionCodeParameterPendingApprovalTaskListRow = objtable.Rows(0)
                    MatchCount = objRow.MatchCount
                    If MatchCount <> 0 Then
                        AddTaskListsToTable(31, MatchCount & " Aux Transaction Code Parameter Approval Required.", mydt)
                    End If
                End Using
            End If

            _strMenuHyperlink = "RulesBasicApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using AccessRulesBasic As New AMLDAL.PendingTaskListTableAdapters.CountRulesBasicPendingApprovalTaskListTableAdapter
                    Dim objtable As Data.DataTable = AccessRulesBasic.GetData(CurrentUserLoginID)
                    Dim objRow As AMLDAL.PendingTaskList.CountRulesBasicPendingApprovalTaskListRow = objtable.Rows(0)
                    MatchCount = objRow.MatchCount
                    If MatchCount <> 0 Then
                        AddTaskListsToTable(32, MatchCount & " Rules Basic Approval Required.", mydt)
                    End If
                End Using
            End If

            _strMenuHyperlink = "AdvancedRulesApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using AccessRulesAdvanced As New AMLDAL.PendingTaskListTableAdapters.CountRulesAdvancedPendingApprovalTaskListTableAdapter
                    Dim objtable As Data.DataTable = AccessRulesAdvanced.GetData(CurrentUserLoginID)
                    Dim objRow As AMLDAL.PendingTaskList.CountRulesAdvancedPendingApprovalTaskListRow = objtable.Rows(0)
                    MatchCount = objRow.MatchCount
                    If MatchCount <> 0 Then
                        AddTaskListsToTable(33, MatchCount & " Rules Advanced Approval Required.", mydt)
                    End If
                End Using
            End If

            _strMenuHyperlink = "PeerGroupApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using AccessPeerGroup As New AMLDAL.PendingTaskListTableAdapters.CountPeerGroupPendingApprovalTaskListTableAdapter
                    Dim objtable As Data.DataTable = AccessPeerGroup.GetData(CurrentUserLoginID)
                    Dim objRow As AMLDAL.PendingTaskList.CountPeerGroupPendingApprovalTaskListRow = objtable.Rows(0)
                    MatchCount = objRow.MatchCount
                    If MatchCount <> 0 Then
                        AddTaskListsToTable(34, MatchCount & " Peer Group Approval Required.", mydt)
                    End If
                End Using
            End If

            _strMenuHyperlink = "RiskFactApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using AccessRiskFact As New AMLDAL.PendingTaskListTableAdapters.CountRiskFactPendingApprovalTaskListTableAdapter
                    Dim objtable As Data.DataTable = AccessRiskFact.GetData(CurrentUserLoginID)
                    Dim objRow As AMLDAL.PendingTaskList.CountRiskFactPendingApprovalTaskListRow = objtable.Rows(0)
                    MatchCount = objRow.MatchCount
                    If MatchCount <> 0 Then
                        AddTaskListsToTable(35, MatchCount & " Risk Fact Approval Required.", mydt)
                    End If
                End Using
            End If

            _strMenuHyperlink = "BranchTypeMappingApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using AccessBranchType As New AMLDAL.PendingTaskListTableAdapters.CountBranchTypeMappingPendingApprovalTaskListTableAdapter
                    Dim objtable As Data.DataTable = AccessBranchType.GetData(CurrentUserLoginID)
                    Dim objRow As AMLDAL.PendingTaskList.CountBranchTypeMappingPendingApprovalTaskListRow = objtable.Rows(0)
                    MatchCount = objRow.MatchCount
                    If MatchCount <> 0 Then
                        AddTaskListsToTable(36, MatchCount & " Branch Type Mapping Approval Required.", mydt)
                    End If
                End Using
            End If

            _strMenuHyperlink = "CTRParameterApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using AccessCtrPara As New AMLDAL.PendingTaskListTableAdapters.CountCTRParameter_PendingApprovalTaskListTableAdapter
                    Dim objtable As Data.DataTable = AccessCtrPara.GetData(CurrentUserLoginID)
                    Dim objRow As AMLDAL.PendingTaskList.CountCTRParameter_PendingApprovalTaskListRow = objtable.Rows(0)
                    MatchCount = objRow.MatchCount
                    If MatchCount <> 0 Then
                        AddTaskListsToTable(37, MatchCount & " CTR Parameter Approval Required.", mydt)
                    End If
                End Using
            End If

            _strMenuHyperlink = "TransactionTypeAuxiliaryMappingApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using AccessTransactionTypeAuxiliaryMapping As New AMLDAL.PendingTaskListTableAdapters.CountTransactionTypeAuxiliaryMappingApprovalTaskListTableAdapter
                    Dim objtable As Data.DataTable = AccessTransactionTypeAuxiliaryMapping.GetData(CurrentUserLoginID)
                    Dim objRow As AMLDAL.PendingTaskList.CountTransactionTypeAuxiliaryMappingApprovalTaskListRow = objtable.Rows(0)
                    MatchCount = objRow.MatchCount
                    If MatchCount <> 0 Then
                        AddTaskListsToTable(38, MatchCount & " Transaction Type Auxiliary Transaction Code Mapping Approval Required.", mydt)
                    End If
                End Using
            End If

            _strMenuHyperlink = "CaseManagementView.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using CaseManagementTaskListAdapter As New AMLDAL.PendingTaskListTableAdapters.CountCaseManagementTaskListTableAdapter
                    Dim objtable As Data.DataTable = CaseManagementTaskListAdapter.GetData(CurrentUserLoginID)
                    If Not objtable Is Nothing Then
                        If objtable.Rows.Count > 0 Then
                            Dim objRow As AMLDAL.PendingTaskList.CountCaseManagementTaskListRow = objtable.Rows(0)
                            MatchCount = objRow.MatchCount
                            If MatchCount <> 0 Then
                                AddTaskListsToTable(39, MatchCount & " Alert Cases to be Investigated", mydt)
                            End If
                        End If
                    End If
                End Using
            End If

            _strMenuHyperlink = "CaseManagementView.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using CaseManagementArtificialSTRTaskListAdapter As New AMLDAL.PendingTaskListTableAdapters.CountCaseManagementArtificialSTRTaskListTableAdapter
                    Dim objtable As Data.DataTable = CaseManagementArtificialSTRTaskListAdapter.GetData(CurrentUserLoginID)
                    If Not objtable Is Nothing Then
                        If objtable.Rows.Count > 0 Then
                            Dim objRow As AMLDAL.PendingTaskList.CountCaseManagementArtificialSTRTaskListRow = objtable.Rows(0)
                            MatchCount = objRow.MatchCount
                            If MatchCount <> 0 Then
                                AddTaskListsToTable(53, MatchCount & " Alert Artificial STR Cases to be Investigated", mydt)
                            End If
                        End If
                    End If
                End Using
            End If

            _strMenuHyperlink = "CaseManagementView.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using CaseManagementIssueTaskListAdapter As New AMLDAL.PendingTaskListTableAdapters.CountCaseManagementIssueTaskListTableAdapter
                    Dim objtable As Data.DataTable = CaseManagementIssueTaskListAdapter.GetData(CurrentUserLoginID)
                    If Not objtable Is Nothing Then
                        If objtable.Rows.Count > 0 Then
                            Dim objRow As AMLDAL.PendingTaskList.CountCaseManagementIssueTaskListRow = objtable.Rows(0)
                            MatchCount = objRow.MatchCount
                            If MatchCount <> 0 Then
                                AddTaskListsToTable(40, "You have " & MatchCount & " open issues Case", mydt)
                            End If
                        End If
                    End If
                End Using
            End If

            _strMenuHyperlink = "CashTransactionReport.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using CTRTaskListAdapter As New AMLDAL.PendingTaskListTableAdapters.CountCTRTaskListTableAdapter
                    Dim objtable As Data.DataTable = CTRTaskListAdapter.GetData(CurrentUserLoginID)
                    If Not objtable Is Nothing Then
                        If objtable.Rows.Count > 0 Then
                            Dim objRow As AMLDAL.PendingTaskList.CountCTRTaskListRow = objtable.Rows(0)
                            MatchCount = objRow.MatchCount
                            If MatchCount <> 0 Then
                                AddTaskListsToTable(41, "You have " & MatchCount & " CTR that not been reported.", mydt)
                            End If
                        End If
                    End If
                End Using
            End If

            'Case 42 'Proposal STR Need Response
            _strMenuHyperlink = "ProposalSTRResponseView.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using CountProposalSTRNeedResponseTaskListAdapter As New AMLDAL.PendingTaskListTableAdapters.CountProposalSTRNeedResponseTaskListTableAdapter
                    Dim objtable As Data.DataTable = CountProposalSTRNeedResponseTaskListAdapter.GetData(CurrentUserLoginID)
                    If Not objtable Is Nothing Then
                        If objtable.Rows.Count > 0 Then
                            Dim objRow As AMLDAL.PendingTaskList.CountProposalSTRNeedResponseTaskListRow = objtable.Rows(0)
                            MatchCount = objRow.MatchCount
                            If MatchCount <> 0 Then
                                AddTaskListsToTable(42, "You have " & MatchCount & " Proposal STR that need response.", mydt)
                            End If
                        End If
                    End If
                End Using
            End If


            'case 43 
            _strMenuHyperlink = "TransactionAmountApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using CountTransactionAmountPendingTaskListAdapter As New AMLDAL.PendingTaskListTableAdapters.CountTransactionAmountPendingTaskListTableAdapter
                    Dim objtable As Data.DataTable = CountTransactionAmountPendingTaskListAdapter.GetData(Sahassa.AML.Commonly.SessionPkUserId)
                    If Not objtable Is Nothing Then
                        If objtable.Rows.Count > 0 Then
                            Dim objRow As AMLDAL.PendingTaskList.CountTransactionAmountPendingTaskListRow = objtable.Rows(0)
                            MatchCount = objRow.MatchCount
                            If MatchCount <> 0 Then
                                AddTaskListsToTable(43, "You have " & MatchCount & " Transaction Amount that need response.", mydt)
                            End If
                        End If
                    End If
                End Using
            End If

            'case 44
            _strMenuHyperlink = "TransactionFrequencyApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using CountTransactionFrequencyPendingTaskListAdapter As New AMLDAL.PendingTaskListTableAdapters.CountTransactionFrequencyPendingTaskListTableAdapter
                    Dim objtable As Data.DataTable = CountTransactionFrequencyPendingTaskListAdapter.GetData(Sahassa.AML.Commonly.SessionPkUserId)
                    If Not objtable Is Nothing Then
                        If objtable.Rows.Count > 0 Then
                            Dim objRow As AMLDAL.PendingTaskList.CountTransactionFrequencyPendingTaskListRow = objtable.Rows(0)
                            MatchCount = objRow.MatchCount
                            If MatchCount <> 0 Then
                                AddTaskListsToTable(44, "You have " & MatchCount & " Transaction Frequency that need response.", mydt)
                            End If
                        End If
                    End If
                End Using
            End If

            'case 45
            _strMenuHyperlink = "SystemParameterApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using CountSystemParameterPendingTaskListAdapter As New AMLDAL.PendingTaskListTableAdapters.CountSystemParameterPendingTaskListTableAdapter
                    Dim objtable As Data.DataTable = CountSystemParameterPendingTaskListAdapter.GetData(Sahassa.AML.Commonly.SessionPkUserId)
                    If Not objtable Is Nothing Then
                        If objtable.Rows.Count > 0 Then
                            Dim objRow As AMLDAL.PendingTaskList.CountSystemParameterPendingTaskListRow = objtable.Rows(0)
                            MatchCount = objRow.MatchCount
                            If MatchCount <> 0 Then
                                AddTaskListsToTable(45, "You have " & MatchCount & " System Parameter that need response.", mydt)
                            End If
                        End If
                    End If
                End Using
            End If

            'case 46
            _strMenuHyperlink = "ResikoNasabahBaruApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using CountResikoNasabahBaruPendingTaskListAdapter As New AMLDAL.PendingTaskListTableAdapters.CountResikoNasabahBaruPendingTaskListTableAdapter
                    Dim objtable As Data.DataTable = CountResikoNasabahBaruPendingTaskListAdapter.GetData(Sahassa.AML.Commonly.SessionPkUserId)
                    If Not objtable Is Nothing Then
                        If objtable.Rows.Count > 0 Then
                            Dim objRow As AMLDAL.PendingTaskList.CountResikoNasabahBaruPendingTaskListRow = objtable.Rows(0)
                            MatchCount = objRow.MatchCount
                            If MatchCount <> 0 Then
                                AddTaskListsToTable(46, "You have " & MatchCount & " Resiko Nasabah Baru that need response.", mydt)
                            End If
                        End If
                    End If
                End Using
            End If

            'case 47
            _strMenuHyperlink = "ResikoNasabahExistingApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using CountResikoNasabahExistingPendingTaskListAdapter As New AMLDAL.PendingTaskListTableAdapters.CountResikoNasabahExistingPendingTaskListTableAdapter
                    Dim objtable As Data.DataTable = CountResikoNasabahExistingPendingTaskListAdapter.GetData(Sahassa.AML.Commonly.SessionPkUserId)
                    If Not objtable Is Nothing Then
                        If objtable.Rows.Count > 0 Then
                            Dim objRow As AMLDAL.PendingTaskList.CountResikoNasabahExistingPendingTaskListRow = objtable.Rows(0)
                            MatchCount = objRow.MatchCount
                            If MatchCount <> 0 Then
                                AddTaskListsToTable(47, "You have " & MatchCount & " Resiko Nasabah Existing that need response.", mydt)
                            End If
                        End If
                    End If
                End Using
            End If

            'case 48
            _strMenuHyperlink = "SuspiciuspersonApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using CountSuspiciusPersonPendingTaskListTableAdapter As New AMLDAL.PendingTaskListTableAdapters.CountSuspiciusPersonPendingTaskListTableAdapter
                    Dim objtable As Data.DataTable = CountSuspiciusPersonPendingTaskListTableAdapter.GetData(Sahassa.AML.Commonly.SessionUserId)
                    If Not objtable Is Nothing Then
                        If objtable.Rows.Count > 0 Then
                            Dim objRow As AMLDAL.PendingTaskList.CountSuspiciusPersonPendingTaskListRow = objtable.Rows(0)
                            MatchCount = objRow.MatchCount
                            If MatchCount <> 0 Then
                                AddTaskListsToTable(48, "You have " & MatchCount & " Custom Case that need response.", mydt)
                            End If
                        End If
                    End If
                End Using
            End If

            'case 48
            _strMenuHyperlink = "YearIntervalCDDLowRiskApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using CountmaxntnPendingTaskListTableAdapter As New AMLDAL.PendingTaskListTableAdapters.CountYearIntervalLowRiskPendingTaskListTableAdapter
                    Dim objtable As Data.DataTable = CountmaxntnPendingTaskListTableAdapter.GetData(Sahassa.AML.Commonly.SessionPkUserId)
                    If Not objtable Is Nothing Then
                        If objtable.Rows.Count > 0 Then
                            Dim objRow As AMLDAL.PendingTaskList.CountYearIntervalLowRiskPendingTaskListRow = objtable.Rows(0)
                            MatchCount = objRow.MatchCount
                            If MatchCount <> 0 Then
                                AddTaskListsToTable(49, "You have " & MatchCount & " Year Internal EDD Low Risk  that need response.", mydt)
                            End If
                        End If
                    End If
                End Using
            End If

            _strMenuHyperlink = "UploadWorkFlowApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using CountuploadworkflowPendingTaskListTableAdapter As New AMLDAL.PendingTaskListTableAdapters.CountWorkFlowUploadPendingTaskListTableAdapter
                    Dim objtable As Data.DataTable = CountuploadworkflowPendingTaskListTableAdapter.GetData(Sahassa.AML.Commonly.SessionUserId)
                    If Not objtable Is Nothing Then
                        If objtable.Rows.Count > 0 Then
                            Dim objRow As AMLDAL.PendingTaskList.CountWorkFlowUploadPendingTaskListRow = objtable.Rows(0)
                            MatchCount = objRow.MatchCount
                            If MatchCount <> 0 Then
                                AddTaskListsToTable(50, "You have " & MatchCount & " Upload Workflow that need response.", mydt)
                            End If
                        End If
                    End If
                End Using
            End If

            _strMenuHyperlink = "VerificationListUploadUNListApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using CountunlistPendingTaskListTableAdapter As New AMLDAL.PendingTaskListTableAdapters.CountUnListPendingTaskListTableAdapter
                    Dim objtable As Data.DataTable = CountunlistPendingTaskListTableAdapter.GetData(Sahassa.AML.Commonly.SessionUserId)
                    If Not objtable Is Nothing Then
                        If objtable.Rows.Count > 0 Then
                            Dim objRow As AMLDAL.PendingTaskList.CountUnListPendingTaskListRow = objtable.Rows(0)
                            MatchCount = objRow.MatchCount
                            If MatchCount <> 0 Then
                                AddTaskListsToTable(51, "You have " & MatchCount & " Unlist that need response.", mydt)
                            End If
                        End If
                    End If
                End Using
            End If

            _strMenuHyperlink = "YearIntervalCDDHighRiskApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using CountmaxntnPendingTaskListTableAdapter As New AMLDAL.PendingTaskListTableAdapters.CountYearIntervalHighRiskPendingTaskListTableAdapter
                    Dim objtable As Data.DataTable = CountmaxntnPendingTaskListTableAdapter.GetData(Sahassa.AML.Commonly.SessionPkUserId)
                    If Not objtable Is Nothing Then
                        If objtable.Rows.Count > 0 Then
                            Dim objRow As AMLDAL.PendingTaskList.CountYearIntervalHighRiskPendingTaskListRow = objtable.Rows(0)
                            MatchCount = objRow.MatchCount
                            If MatchCount <> 0 Then
                                AddTaskListsToTable(52, "You have " & MatchCount & " Year Internal EDD High Risk  that need response.", mydt)
                            End If
                        End If
                    End If
                End Using
            End If

            _strMenuHyperlink = "DataUpdatingAlertApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using CountDataUpdatingParameterAlertPendingTaskListTableAdapter As New AMLDAL.PendingTaskListTableAdapters.CountDataUpdatingAlertPendingTaskListTableAdapter
                    Dim objtable As Data.DataTable = CountDataUpdatingParameterAlertPendingTaskListTableAdapter.GetData(Sahassa.AML.Commonly.SessionUserId)
                    If Not objtable Is Nothing Then
                        If objtable.Rows.Count > 0 Then
                            Dim objRow As AMLDAL.PendingTaskList.CountDataUpdatingAlertPendingTaskListRow = objtable.Rows(0)
                            MatchCount = objRow.MatchCount
                            If MatchCount <> 0 Then
                                AddTaskListsToTable(55, "You have " & MatchCount & " Data Updating Parameter Alert that need response.", mydt)
                            End If
                        End If
                    End If
                End Using
            End If

            _strMenuHyperlink = "WorkFlowHistoryAccessApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using CountWorkflowHistoryAccessPendingTaskListTableAdapter As New AMLDAL.PendingTaskListTableAdapters.CountWorkflowHistoryAccessPendingTaskListTableAdapter
                    Dim objtable As Data.DataTable = CountWorkflowHistoryAccessPendingTaskListTableAdapter.GetData(Sahassa.AML.Commonly.SessionUserId)
                    If Not objtable Is Nothing Then
                        If objtable.Rows.Count > 0 Then
                            Dim objRow As AMLDAL.PendingTaskList.CountWorkflowHistoryAccessPendingTaskListRow = objtable.Rows(0)
                            MatchCount = objRow.MatchCount
                            If MatchCount <> 0 Then
                                AddTaskListsToTable(56, "You have " & MatchCount & " Workflow History Access  Parameter that need response.", mydt)
                            End If
                        End If
                    End If
                End Using
            End If

            _strMenuHyperlink = "TransactionPenghasilan_Approval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using CountTransactionPenghasilanPendingTaskListTableAdapter As New AMLDAL.PendingTaskListTableAdapters.CountTransactionPenghasilanAccessPendingTaskListTableAdapter
                    Dim objtable As Data.DataTable = CountTransactionPenghasilanPendingTaskListTableAdapter.GetData(Sahassa.AML.Commonly.SessionUserId)
                    If Not objtable Is Nothing Then
                        If objtable.Rows.Count > 0 Then
                            Dim objRow As AMLDAL.PendingTaskList.CountTransactionPenghasilanAccessPendingTaskListRow = objtable.Rows(0)
                            MatchCount = objRow.MatchCount
                            If MatchCount <> 0 Then
                                AddTaskListsToTable(57, "You have " & MatchCount & " Transaction Penghasilan Parameter that need response.", mydt)
                            End If
                        End If
                    End If
                End Using
            End If

            _strMenuHyperlink = "TransactionntnApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using CountTransactionntnPendingTaskListTableAdapter As New AMLDAL.PendingTaskListTableAdapters.CountTransactionntnAccessPendingTaskListTableAdapter
                    Dim objtable As Data.DataTable = CountTransactionntnPendingTaskListTableAdapter.GetData(Sahassa.AML.Commonly.SessionUserId)
                    If Not objtable Is Nothing Then
                        If objtable.Rows.Count > 0 Then
                            Dim objRow As AMLDAL.PendingTaskList.CountTransactionntnAccessPendingTaskListRow = objtable.Rows(0)
                            MatchCount = objRow.MatchCount
                            If MatchCount <> 0 Then
                                AddTaskListsToTable(58, "You have " & MatchCount & " Transaction ntn Parameter that need response.", mydt)
                            End If
                        End If
                    End If
                End Using
            End If

            _strMenuHyperlink = "UploadHiportAccountApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using CountUploadHiportPendingTaskListTableAdapter As New AMLDAL.PendingTaskListTableAdapters.CountUploadHiportPendingApprovalTaskListTableAdapter
                    Dim objtable As Data.DataTable = CountUploadHiportPendingTaskListTableAdapter.GetData(Sahassa.AML.Commonly.SessionUserId)
                    If Not objtable Is Nothing Then
                        If objtable.Rows.Count > 0 Then
                            Dim objRow As AMLDAL.PendingTaskList.CountUploadHiportPendingApprovalTaskListRow = objtable.Rows(0)
                            MatchCount = objRow.MatchCount
                            If MatchCount <> 0 Then
                                AddTaskListsToTable(59, "You have " & MatchCount & " Upload Hiport Account that need response.", mydt)
                            End If
                        End If
                    End If
                End Using
            End If

            _strMenuHyperlink = "InsiderCodeApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using CountInsiderCodePendingTaskListTableAdapter As New AMLDAL.PendingTaskListTableAdapters.CountInsiderCodePendingTaskListTableAdapter
                    Dim objtable As Data.DataTable = CountInsiderCodePendingTaskListTableAdapter.GetData(Sahassa.AML.Commonly.SessionUserId)
                    If Not objtable Is Nothing Then
                        If objtable.Rows.Count > 0 Then
                            Dim objRow As AMLDAL.PendingTaskList.CountInsiderCodePendingTaskListRow = objtable.Rows(0)
                            MatchCount = objRow.MatchCount
                            If MatchCount <> 0 Then
                                AddTaskListsToTable(60, "You have " & MatchCount & " Insider Code that need response.", mydt)
                            End If
                        End If
                    End If
                End Using
            End If

            _strMenuHyperlink = "VIPCodeApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using CountVIPCodePendingTaskListTableAdapter As New AMLDAL.PendingTaskListTableAdapters.CountVIPCODEPendingTaskListTableAdapter
                    Dim objtable As Data.DataTable = CountVIPCodePendingTaskListTableAdapter.GetData(Sahassa.AML.Commonly.SessionUserId)
                    If Not objtable Is Nothing Then
                        If objtable.Rows.Count > 0 Then
                            Dim objRow As AMLDAL.PendingTaskList.CountVIPCODEPendingTaskListRow = objtable.Rows(0)
                            MatchCount = objRow.MatchCount
                            If MatchCount <> 0 Then
                                AddTaskListsToTable(61, "You have " & MatchCount & " VIP Code that need response.", mydt)
                            End If
                        End If
                    End If
                End Using
            End If

            _strMenuHyperlink = "OutlierToleransiApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using CountOutlierToleransiPendingTaskListTableAdapter As New AMLDAL.PendingTaskListTableAdapters.CountOutlierToleransiPendingTaskListTableAdapter
                    Dim objtable As Data.DataTable = CountOutlierToleransiPendingTaskListTableAdapter.GetData(Sahassa.AML.Commonly.SessionUserId)
                    If Not objtable Is Nothing Then
                        If objtable.Rows.Count > 0 Then
                            Dim objRow As AMLDAL.PendingTaskList.CountOutlierToleransiPendingTaskListRow = objtable.Rows(0)
                            MatchCount = objRow.MatchCount
                            If MatchCount <> 0 Then
                                AddTaskListsToTable(62, "You have " & MatchCount & " Outlier Toleransi that need response.", mydt)
                            End If
                        End If
                    End If
                End Using
            End If

            _strMenuHyperlink = "SegmentApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using CountSegmentPendingTaskListTableAdapter As New AMLDAL.PendingTaskListTableAdapters.CountSegmentPendingTaskListTableAdapter
                    Dim objtable As Data.DataTable = CountSegmentPendingTaskListTableAdapter.GetData(Sahassa.AML.Commonly.SessionUserId)
                    If Not objtable Is Nothing Then
                        If objtable.Rows.Count > 0 Then
                            Dim objRow As AMLDAL.PendingTaskList.CountSegmentPendingTaskListRow = objtable.Rows(0)
                            MatchCount = objRow.MatchCount
                            If MatchCount <> 0 Then
                                AddTaskListsToTable(63, "You have " & MatchCount & " Segment that need response.", mydt)
                            End If
                        End If
                    End If
                End Using
            End If

            _strMenuHyperlink = "MappingGroupMenuHiportJiveApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using CountMappingGroupMenuHiportJivePendingTaskListTableAdapter As New AMLDAL.PendingTaskListTableAdapters.CountMappingGroupMenuHiportJivePendingTaskListTableAdapter
                    Dim objtable As Data.DataTable = CountMappingGroupMenuHiportJivePendingTaskListTableAdapter.GetData(Sahassa.AML.Commonly.SessionUserId)
                    If Not objtable Is Nothing Then
                        If objtable.Rows.Count > 0 Then
                            Dim objRow As AMLDAL.PendingTaskList.CountMappingGroupMenuHiportJivePendingTaskListRow = objtable.Rows(0)
                            MatchCount = objRow.MatchCount
                            If MatchCount <> 0 Then
                                AddTaskListsToTable(64, "You have " & MatchCount & " Mapping Group Menu Hiport Jive that need response.", mydt)
                            End If
                        End If
                    End If
                End Using
            End If

            _strMenuHyperlink = "MappingGroupMenuSTRPPATKApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using CountMappingGroupMenuSTRPPATKPendingTaskListTableAdapter As New AMLDAL.PendingTaskListTableAdapters.CountMappingGroupMenuSTRPPATKPendingTaskListTableAdapter
                    Dim objtable As Data.DataTable = CountMappingGroupMenuSTRPPATKPendingTaskListTableAdapter.GetData(Sahassa.AML.Commonly.SessionUserId)
                    If Not objtable Is Nothing Then
                        If objtable.Rows.Count > 0 Then
                            Dim objRow As AMLDAL.PendingTaskList.CountMappingGroupMenuSTRPPATKPendingTaskListRow = objtable.Rows(0)
                            MatchCount = objRow.MatchCount
                            If MatchCount <> 0 Then
                                AddTaskListsToTable(65, "You have " & MatchCount & " Mapping Group Menu STR PPATK that need response.", mydt)
                            End If
                        End If
                    End If
                End Using
            End If

            _strMenuHyperlink = "MappingReportBuilderWorkingUnitApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using CountMappingReportBuilderWorkingUnitPendingTaskListTableAdapter As New AMLDAL.PendingTaskListTableAdapters.CountMappingReportBuilderWorkingUnitPendingTaskListTableAdapter
                    Dim objtable As Data.DataTable = CountMappingReportBuilderWorkingUnitPendingTaskListTableAdapter.GetData(Sahassa.AML.Commonly.SessionUserId)
                    If Not objtable Is Nothing Then
                        If objtable.Rows.Count > 0 Then
                            Dim objRow As AMLDAL.PendingTaskList.CountMappingReportBuilderWorkingUnitPendingTaskListRow = objtable.Rows(0)
                            MatchCount = objRow.MatchCount
                            If MatchCount <> 0 Then
                                AddTaskListsToTable(66, "You have " & MatchCount & " Mapping Report Builder Working Unit that need response.", mydt)
                            End If
                        End If
                    End If
                End Using
            End If

            _strMenuHyperlink = "MappingUserIdHiportJiveApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using CountMappingUserIdHiportJivePendingTaskListTableAdapter As New AMLDAL.PendingTaskListTableAdapters.CountMappingUserIdHiportJivePendingTaskListTableAdapter
                    Dim objtable As Data.DataTable = CountMappingUserIdHiportJivePendingTaskListTableAdapter.GetData(Sahassa.AML.Commonly.SessionUserId)
                    If Not objtable Is Nothing Then
                        If objtable.Rows.Count > 0 Then
                            Dim objRow As AMLDAL.PendingTaskList.CountMappingUserIdHiportJivePendingTaskListRow = objtable.Rows(0)
                            MatchCount = objRow.MatchCount
                            If MatchCount <> 0 Then
                                AddTaskListsToTable(67, "You have " & MatchCount & " Mapping UserId Hiport Jive that need response.", mydt)
                            End If
                        End If
                    End If
                End Using
            End If

            _strMenuHyperlink = "QuestionaireSTRApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using CountQuestionaireSTRPendingTaskListTableAdapter As New AMLDAL.PendingTaskListTableAdapters.CountMsQuestionaireSTRPendingTaskListTableAdapter
                    Dim objtable As Data.DataTable = CountQuestionaireSTRPendingTaskListTableAdapter.GetData(Sahassa.AML.Commonly.SessionUserId)
                    If Not objtable Is Nothing Then
                        If objtable.Rows.Count > 0 Then
                            Dim objRow As AMLDAL.PendingTaskList.CountMsQuestionaireSTRPendingTaskListRow = objtable.Rows(0)
                            MatchCount = objRow.MatchCount
                            If MatchCount <> 0 Then
                                AddTaskListsToTable(68, "You have " & MatchCount & " MsQuestionaire that need response.", mydt)
                            End If
                        End If
                    End If
                End Using
            End If

            '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
            'case 73, MsBentukBidangUsaha_Approval
            _strMenuHyperlink = "MsBentukBidangUsaha_Approval_view.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using objdata As TList(Of MsBentukBidangUsaha_Approval) = DataRepository.MsBentukBidangUsaha_ApprovalProvider.GetPaged("IsUpload = 0 AND RequestedBy <> '" & Commonly.SessionPkUserId & "' and RequestedBy IN (SELECT x.pkUserID FROM UserWorkingUnitAssignment uwua INNER JOIN  [USER] x ON x.UserID=uwua.UserID WHERE uwua.WorkingUnitID IN (SELECT uwua2.WorkingUnitID FROM UserWorkingUnitAssignment uwua2 WHERE uwua2.UserID= '" & Commonly.SessionUserId & "'))", "", 0, Integer.MaxValue, 0)
                    If objdata.Count > 0 Then
                        AddTaskListsToTable(73, "You have " & objdata.Count & " Master Bentuk Badan Usaha Approval Required.", mydt)
                    End If
                End Using
            End If

            'case 74, MsBidangUsaha_Approval_view.aspx
            _strMenuHyperlink = "MsBidangUsaha_Approval_view.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using objdata As TList(Of MsBidangUsaha_Approval) = DataRepository.MsBidangUsaha_ApprovalProvider.GetPaged("IsUpload = 0 AND RequestedBy <> '" & Commonly.SessionPkUserId & "' and RequestedBy IN (SELECT x.pkUserID FROM UserWorkingUnitAssignment uwua INNER JOIN  [USER] x ON x.UserID=uwua.UserID WHERE uwua.WorkingUnitID IN (SELECT uwua2.WorkingUnitID FROM UserWorkingUnitAssignment uwua2 WHERE uwua2.UserID= '" & Commonly.SessionUserId & "'))", "", 0, Integer.MaxValue, 0)
                    If objdata.Count > 0 Then
                        AddTaskListsToTable(74, "You have " & objdata.Count & " Master Bidang Usaha Approval Required.", mydt)
                    End If
                End Using
            End If

            'case 75, MsCurrency_Approval_view.aspx
            _strMenuHyperlink = "MsCurrency_Approval_view.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using objdata As TList(Of MsCurrency_Approval) = DataRepository.MsCurrency_ApprovalProvider.GetPaged("IsUpload = 0 AND RequestedBy <> '" & Commonly.SessionPkUserId & "' and RequestedBy IN (SELECT x.pkUserID FROM UserWorkingUnitAssignment uwua INNER JOIN  [USER] x ON x.UserID=uwua.UserID WHERE uwua.WorkingUnitID IN (SELECT uwua2.WorkingUnitID FROM UserWorkingUnitAssignment uwua2 WHERE uwua2.UserID= '" & Commonly.SessionUserId & "'))", "", 0, Integer.MaxValue, 0)
                    If objdata.Count > 0 Then
                        AddTaskListsToTable(75, "You have " & objdata.Count & " Master Currency Approval Required.", mydt)
                    End If
                End Using
            End If

            'case 76, MsKecamatan_Approval_view
            _strMenuHyperlink = "MsKecamatan_Approval_view.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using objdata As TList(Of MsKecamatan_Approval) = DataRepository.MsKecamatan_ApprovalProvider.GetPaged("IsUpload = 0 AND RequestedBy <> '" & Commonly.SessionPkUserId & "' and RequestedBy IN (SELECT x.pkUserID FROM UserWorkingUnitAssignment uwua INNER JOIN  [USER] x ON x.UserID=uwua.UserID WHERE uwua.WorkingUnitID IN (SELECT uwua2.WorkingUnitID FROM UserWorkingUnitAssignment uwua2 WHERE uwua2.UserID= '" & Commonly.SessionUserId & "'))", "", 0, Integer.MaxValue, 0)
                    If objdata.Count > 0 Then
                        AddTaskListsToTable(76, "You have " & objdata.Count & " Master Kecamatan Approval Required.", mydt)
                    End If
                End Using
            End If

            'case 77, MsKelurahan_Approval_view.aspx
            _strMenuHyperlink = "MsKelurahan_Approval_view.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using objdata As TList(Of MsKelurahan_Approval) = DataRepository.MsKelurahan_ApprovalProvider.GetPaged("IsUpload = 0 AND RequestedBy <> '" & Commonly.SessionPkUserId & "' and RequestedBy IN (SELECT x.pkUserID FROM UserWorkingUnitAssignment uwua INNER JOIN  [USER] x ON x.UserID=uwua.UserID WHERE uwua.WorkingUnitID IN (SELECT uwua2.WorkingUnitID FROM UserWorkingUnitAssignment uwua2 WHERE uwua2.UserID= '" & Commonly.SessionUserId & "'))", "", 0, Integer.MaxValue, 0)
                    If objdata.Count > 0 Then
                        AddTaskListsToTable(77, "You have " & objdata.Count & " Master Kelurahan Approval Required.", mydt)
                    End If
                End Using
            End If

            'case 78, MsKepemilikan_Approval_view.aspx
            _strMenuHyperlink = "MsKepemilikan_Approval_view.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using objdata As TList(Of MsKepemilikan_Approval) = DataRepository.MsKepemilikan_ApprovalProvider.GetPaged("IsUpload = 0 AND RequestedBy <> '" & Commonly.SessionPkUserId & "' and RequestedBy IN (SELECT x.pkUserID FROM UserWorkingUnitAssignment uwua INNER JOIN  [USER] x ON x.UserID=uwua.UserID WHERE uwua.WorkingUnitID IN (SELECT uwua2.WorkingUnitID FROM UserWorkingUnitAssignment uwua2 WHERE uwua2.UserID= '" & Commonly.SessionUserId & "'))", "", 0, Integer.MaxValue, 0)
                    If objdata.Count > 0 Then
                        AddTaskListsToTable(78, "You have " & objdata.Count & " Master Kepemilikan Approval Required.", mydt)
                    End If
                End Using
            End If

            'case 79, MsKotaKab_Approval_view.aspx
            _strMenuHyperlink = "MsKotaKab_Approval_view.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using objdata As TList(Of MsKotaKab_Approval) = DataRepository.MsKotaKab_ApprovalProvider.GetPaged("IsUpload = 0 AND RequestedBy <> '" & Commonly.SessionPkUserId & "' and RequestedBy IN (SELECT x.pkUserID FROM UserWorkingUnitAssignment uwua INNER JOIN  [USER] x ON x.UserID=uwua.UserID WHERE uwua.WorkingUnitID IN (SELECT uwua2.WorkingUnitID FROM UserWorkingUnitAssignment uwua2 WHERE uwua2.UserID= '" & Commonly.SessionUserId & "'))", "", 0, Integer.MaxValue, 0)
                    If objdata.Count > 0 Then
                        AddTaskListsToTable(79, "You have " & objdata.Count & " Master KotaKab Approval Required.", mydt)
                    End If
                End Using
            End If

            'case 80, MsNegara_Approval_view.aspx
            _strMenuHyperlink = "MsNegara_Approval_view.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using objdata As TList(Of MsNegara_Approval) = DataRepository.MsNegara_ApprovalProvider.GetPaged("IsUpload = 0 AND RequestedBy <> '" & Commonly.SessionPkUserId & "' and RequestedBy IN (SELECT x.pkUserID FROM UserWorkingUnitAssignment uwua INNER JOIN  [USER] x ON x.UserID=uwua.UserID WHERE uwua.WorkingUnitID IN (SELECT uwua2.WorkingUnitID FROM UserWorkingUnitAssignment uwua2 WHERE uwua2.UserID= '" & Commonly.SessionUserId & "'))", "", 0, Integer.MaxValue, 0)
                    If objdata.Count > 0 Then
                        AddTaskListsToTable(80, "You have " & objdata.Count & " Master Negara Approval Required.", mydt)
                    End If
                End Using
            End If

            'case 81, MsPekerjaan_Approval_view.aspx
            _strMenuHyperlink = "MsPekerjaan_Approval_view.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using objdata As TList(Of MsPekerjaan_Approval) = DataRepository.MsPekerjaan_ApprovalProvider.GetPaged("IsUpload = 0 AND RequestedBy <> '" & Commonly.SessionPkUserId & "' and RequestedBy IN (SELECT x.pkUserID FROM UserWorkingUnitAssignment uwua INNER JOIN  [USER] x ON x.UserID=uwua.UserID WHERE uwua.WorkingUnitID IN (SELECT uwua2.WorkingUnitID FROM UserWorkingUnitAssignment uwua2 WHERE uwua2.UserID= '" & Commonly.SessionUserId & "'))", "", 0, Integer.MaxValue, 0)
                    If objdata.Count > 0 Then
                        AddTaskListsToTable(81, "You have " & objdata.Count & " Master Pekerjaan Approval Required.", mydt)
                    End If
                End Using
            End If

            'case 82, MsProfession_Approval_view.aspx

            'case 83, MsProvince_Approval_view.aspx
            _strMenuHyperlink = "MsProvince_Approval_view.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using objdata As TList(Of MsProvince_Approval) = DataRepository.MsProvince_ApprovalProvider.GetPaged("IsUpload = 0 AND Fk_MsUser_Id <> '" & Commonly.SessionPkUserId & "' and Fk_MsUser_Id IN (SELECT x.pkUserID FROM UserWorkingUnitAssignment uwua INNER JOIN  [USER] x ON x.UserID=uwua.UserID WHERE uwua.WorkingUnitID IN (SELECT uwua2.WorkingUnitID FROM UserWorkingUnitAssignment uwua2 WHERE uwua2.UserID= '" & Commonly.SessionUserId & "'))", "", 0, Integer.MaxValue, 0)
                    If objdata.Count > 0 Then
                        AddTaskListsToTable(83, "You have " & objdata.Count & " Master Province Approval Required.", mydt)
                    End If
                End Using
            End If

            'case 84, LTKT_APPROVAL_View.aspx
            _strMenuHyperlink = "LTKT_APPROVAL_View.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using objdata As TList(Of LTKT_Approval) = DataRepository.LTKT_ApprovalProvider.GetPaged(" IsUpload=1 and  RequestedBy <> '" & Commonly.SessionPkUserId & "' and RequestedBy IN (SELECT x.pkUserID FROM UserWorkingUnitAssignment uwua INNER JOIN  [USER] x ON x.UserID=uwua.UserID WHERE uwua.WorkingUnitID IN (SELECT uwua2.WorkingUnitID FROM UserWorkingUnitAssignment uwua2 WHERE uwua2.UserID= '" & Commonly.SessionUserId & "'))", "", 0, Integer.MaxValue, 0)
                    If objdata.Count > 0 Then
                        AddTaskListsToTable(84, "You have " & objdata.Count & " LTKT Upload List Approval Required.", mydt)
                    End If
                End Using
            End If

            'case 85, WIC_AprovalView.aspx
            _strMenuHyperlink = "WIC_AprovalView.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using objdata As TList(Of WIC_Approval) = DataRepository.WIC_ApprovalProvider.GetPaged("RequestedBy <> '" & Commonly.SessionPkUserId & "' and RequestedBy IN (SELECT x.pkUserID FROM UserWorkingUnitAssignment uwua INNER JOIN  [USER] x ON x.UserID=uwua.UserID WHERE uwua.WorkingUnitID IN (SELECT uwua2.WorkingUnitID FROM UserWorkingUnitAssignment uwua2 WHERE uwua2.UserID= '" & Commonly.SessionUserId & "'))", "", 0, Integer.MaxValue, 0)
                    If objdata.Count > 0 Then
                        AddTaskListsToTable(85, "You have " & objdata.Count & " WIC List Upload Approval Required.", mydt)
                    End If
                End Using
            End If

            'Master Upload
            'case 86, MsBentukBidangUsaha_Approval
            _strMenuHyperlink = "MsBentukBidangUsaha_UploadApproval_view.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using objdata As TList(Of MsBentukBidangUsaha_Approval) = DataRepository.MsBentukBidangUsaha_ApprovalProvider.GetPaged("IsUpload = 1 AND RequestedBy <> '" & Commonly.SessionPkUserId & "' and RequestedBy IN (SELECT x.pkUserID FROM UserWorkingUnitAssignment uwua INNER JOIN  [USER] x ON x.UserID=uwua.UserID WHERE uwua.WorkingUnitID IN (SELECT uwua2.WorkingUnitID FROM UserWorkingUnitAssignment uwua2 WHERE uwua2.UserID= '" & Commonly.SessionUserId & "'))", "", 0, Integer.MaxValue, 0)
                    If objdata.Count > 0 Then
                        AddTaskListsToTable(86, "You have " & objdata.Count & " Master Bentuk Badan Usaha Upload Approval Required.", mydt)
                    End If
                End Using
            End If

            'case 87, MsBidangUsaha_Approval_view.aspx
            _strMenuHyperlink = "MsBidangUsaha_UploadApproval_view.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using objdata As TList(Of MsBidangUsaha_Approval) = DataRepository.MsBidangUsaha_ApprovalProvider.GetPaged("IsUpload = 1 AND RequestedBy <> '" & Commonly.SessionPkUserId & "' and RequestedBy IN (SELECT x.pkUserID FROM UserWorkingUnitAssignment uwua INNER JOIN  [USER] x ON x.UserID=uwua.UserID WHERE uwua.WorkingUnitID IN (SELECT uwua2.WorkingUnitID FROM UserWorkingUnitAssignment uwua2 WHERE uwua2.UserID= '" & Commonly.SessionUserId & "'))", "", 0, Integer.MaxValue, 0)
                    If objdata.Count > 0 Then
                        AddTaskListsToTable(87, "You have " & objdata.Count & " Master Bidang Usaha Upload Approval Required.", mydt)
                    End If
                End Using
            End If

            'case 88, MsCurrency_Approval_view.aspx
            _strMenuHyperlink = "MsCurrency_UploadApproval_view.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using objdata As TList(Of MsCurrency_Approval) = DataRepository.MsCurrency_ApprovalProvider.GetPaged("IsUpload = 1 AND RequestedBy <> '" & Commonly.SessionPkUserId & "' and RequestedBy IN (SELECT x.pkUserID FROM UserWorkingUnitAssignment uwua INNER JOIN  [USER] x ON x.UserID=uwua.UserID WHERE uwua.WorkingUnitID IN (SELECT uwua2.WorkingUnitID FROM UserWorkingUnitAssignment uwua2 WHERE uwua2.UserID= '" & Commonly.SessionUserId & "'))", "", 0, Integer.MaxValue, 0)
                    If objdata.Count > 0 Then
                        AddTaskListsToTable(88, "You have " & objdata.Count & " Master Currency Upload Approval Required.", mydt)
                    End If
                End Using
            End If

            'case 89, MsKecamatan_Approval_view
            _strMenuHyperlink = "MsKecamatan_UploadApproval_view.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using objdata As TList(Of MsKecamatan_Approval) = DataRepository.MsKecamatan_ApprovalProvider.GetPaged("IsUpload = 1 AND RequestedBy <> '" & Commonly.SessionPkUserId & "' and RequestedBy IN (SELECT x.pkUserID FROM UserWorkingUnitAssignment uwua INNER JOIN  [USER] x ON x.UserID=uwua.UserID WHERE uwua.WorkingUnitID IN (SELECT uwua2.WorkingUnitID FROM UserWorkingUnitAssignment uwua2 WHERE uwua2.UserID= '" & Commonly.SessionUserId & "'))", "", 0, Integer.MaxValue, 0)
                    If objdata.Count > 0 Then
                        AddTaskListsToTable(89, "You have " & objdata.Count & " Master Kecamatan Upload Approval Required.", mydt)
                    End If
                End Using
            End If

            'case 90, MsKelurahan_Approval_view.aspx
            _strMenuHyperlink = "MsKelurahan_UploadApproval_view.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using objdata As TList(Of MsKelurahan_Approval) = DataRepository.MsKelurahan_ApprovalProvider.GetPaged("IsUpload = 1 AND RequestedBy <> '" & Commonly.SessionPkUserId & "' and RequestedBy IN (SELECT x.pkUserID FROM UserWorkingUnitAssignment uwua INNER JOIN  [USER] x ON x.UserID=uwua.UserID WHERE uwua.WorkingUnitID IN (SELECT uwua2.WorkingUnitID FROM UserWorkingUnitAssignment uwua2 WHERE uwua2.UserID= '" & Commonly.SessionUserId & "'))", "", 0, Integer.MaxValue, 0)
                    If objdata.Count > 0 Then
                        AddTaskListsToTable(90, "You have " & objdata.Count & " Master Kelurahan Upload Approval Required.", mydt)
                    End If
                End Using
            End If

            'case 91, MsKepemilikan_Approval_view.aspx
            _strMenuHyperlink = "MsKepemilikan_UploadApproval_view.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using objdata As TList(Of MsKepemilikan_Approval) = DataRepository.MsKepemilikan_ApprovalProvider.GetPaged("IsUpload = 1 AND RequestedBy <> '" & Commonly.SessionPkUserId & "' and RequestedBy IN (SELECT x.pkUserID FROM UserWorkingUnitAssignment uwua INNER JOIN  [USER] x ON x.UserID=uwua.UserID WHERE uwua.WorkingUnitID IN (SELECT uwua2.WorkingUnitID FROM UserWorkingUnitAssignment uwua2 WHERE uwua2.UserID= '" & Commonly.SessionUserId & "'))", "", 0, Integer.MaxValue, 0)
                    If objdata.Count > 0 Then
                        AddTaskListsToTable(91, "You have " & objdata.Count & " Master Kepemilikan Upload Approval Required.", mydt)
                    End If
                End Using
            End If

            'case 92, MsKotaKab_Approval_view.aspx
            _strMenuHyperlink = "MsKotaKab_UploadApproval_view.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using objdata As TList(Of MsKotaKab_Approval) = DataRepository.MsKotaKab_ApprovalProvider.GetPaged("IsUpload = 1 AND RequestedBy <> '" & Commonly.SessionPkUserId & "' and RequestedBy IN (SELECT x.pkUserID FROM UserWorkingUnitAssignment uwua INNER JOIN  [USER] x ON x.UserID=uwua.UserID WHERE uwua.WorkingUnitID IN (SELECT uwua2.WorkingUnitID FROM UserWorkingUnitAssignment uwua2 WHERE uwua2.UserID= '" & Commonly.SessionUserId & "'))", "", 0, Integer.MaxValue, 0)
                    If objdata.Count > 0 Then
                        AddTaskListsToTable(92, "You have " & objdata.Count & " Master KotaKab Upload Approval Required.", mydt)
                    End If
                End Using
            End If

            'case 93, MsNegara_Approval_view.aspx
            _strMenuHyperlink = "MsNegara_UploadApproval_view.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using objdata As TList(Of MsNegara_Approval) = DataRepository.MsNegara_ApprovalProvider.GetPaged("IsUpload = 1 AND RequestedBy <> '" & Commonly.SessionPkUserId & "' and RequestedBy IN (SELECT x.pkUserID FROM UserWorkingUnitAssignment uwua INNER JOIN  [USER] x ON x.UserID=uwua.UserID WHERE uwua.WorkingUnitID IN (SELECT uwua2.WorkingUnitID FROM UserWorkingUnitAssignment uwua2 WHERE uwua2.UserID= '" & Commonly.SessionUserId & "'))", "", 0, Integer.MaxValue, 0)
                    If objdata.Count > 0 Then
                        AddTaskListsToTable(93, "You have " & objdata.Count & " Master Negara Upload Approval Required.", mydt)
                    End If
                End Using
            End If

            'case 94, MsPekerjaan_Approval_view.aspx
            _strMenuHyperlink = "MsPekerjaan_UploadApproval_view.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using objdata As TList(Of MsPekerjaan_Approval) = DataRepository.MsPekerjaan_ApprovalProvider.GetPaged("IsUpload = 1 AND RequestedBy <> '" & Commonly.SessionPkUserId & "' and RequestedBy IN (SELECT x.pkUserID FROM UserWorkingUnitAssignment uwua INNER JOIN  [USER] x ON x.UserID=uwua.UserID WHERE uwua.WorkingUnitID IN (SELECT uwua2.WorkingUnitID FROM UserWorkingUnitAssignment uwua2 WHERE uwua2.UserID= '" & Commonly.SessionUserId & "'))", "", 0, Integer.MaxValue, 0)
                    If objdata.Count > 0 Then
                        AddTaskListsToTable(94, "You have " & objdata.Count & " Master Pekerjaan Upload Approval Required.", mydt)
                    End If
                End Using
            End If

            'case 95, MsProvince_Approval_view.aspx
            _strMenuHyperlink = "MsProvince_UploadApproval_view.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using objdata As TList(Of MsProvince_Approval) = DataRepository.MsProvince_ApprovalProvider.GetPaged("IsUpload = 1 AND Fk_MsUser_Id <> '" & Commonly.SessionPkUserId & "' and Fk_MsUser_Id IN (SELECT x.pkUserID FROM UserWorkingUnitAssignment uwua INNER JOIN  [USER] x ON x.UserID=uwua.UserID WHERE uwua.WorkingUnitID IN (SELECT uwua2.WorkingUnitID FROM UserWorkingUnitAssignment uwua2 WHERE uwua2.UserID= '" & Commonly.SessionUserId & "'))", "", 0, Integer.MaxValue, 0)
                    If objdata.Count > 0 Then
                        AddTaskListsToTable(95, "You have " & objdata.Count & " Master Province Upload Approval Required.", mydt)
                    End If
                End Using
            End If

            'case 96, CTRReportControlGeneratorApproval.aspx
            _strMenuHyperlink = "CTRReportControlGeneratorApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using objdata As TList(Of ListOfGeneratedFileCTR) = DataRepository.ListOfGeneratedFileCTRProvider.GetPaged(" UserIDProposed <> '" & Commonly.SessionUserId & "' and StatusReportApproval=1 and UserIDProposed IN (SELECT UserId FROM   UserWorkingUnitAssignment WHERE  WorkingUnitId IN (SELECT WorkingUnitId FROM UserWorkingUnitAssignment WHERE UserId = '" & Commonly.SessionUserId & "'))", "", 0, Integer.MaxValue, 0)
                    If objdata.Count > 0 Then
                        AddTaskListsToTable(96, "You have " & objdata.Count & " CTR Report Control Generator Approval Required.", mydt)
                    End If
                End Using
            End If

            'case 97, ListofGeneratedCTRFile.aspx.aspx
            _strMenuHyperlink = "ListofGeneratedCTRFile.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using objdata As TList(Of ListOfGeneratedFileCTR) = DataRepository.ListOfGeneratedFileCTRProvider.GetPaged(" UserIDProposed = '" & Commonly.SessionUserId & "' and StatusReportApproval=2 and FK_MsStatusUploadPPATK_ID=2", "", 0, Integer.MaxValue, 0)
                    If objdata.Count > 0 Then
                        AddTaskListsToTable(97, "You have " & objdata.Count & " CTR Report Control Generator Already Approved.", mydt)
                    End If
                End Using
            End If

            'case 101, LTKT_APPROVAL.aspx
            _strMenuHyperlink = "LTKTAPPROVAL.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using objdata As TList(Of LTKT_Approval) = DataRepository.LTKT_ApprovalProvider.GetPaged(" IsUpload=0 and  RequestedBy <> '" & Commonly.SessionPkUserId & "' and RequestedBy IN (SELECT x.pkUserID FROM UserWorkingUnitAssignment uwua INNER JOIN  [USER] x ON x.UserID=uwua.UserID WHERE uwua.WorkingUnitID IN (SELECT uwua2.WorkingUnitID FROM UserWorkingUnitAssignment uwua2 WHERE uwua2.UserID= '" & Commonly.SessionUserId & "'))", "", 0, Integer.MaxValue, 0)
                    If objdata.Count > 0 Then
                        AddTaskListsToTable(101, "You have " & objdata.Count & " LTKT List Approval Required.", mydt)
                    End If
                End Using
            End If

            'case 102, WIC_APPROVAL.aspx
            _strMenuHyperlink = "WICAPPROVAL.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using objdata As TList(Of WIC_Approval) = DataRepository.WIC_ApprovalProvider.GetPaged(" IsUpload=0 and  RequestedBy <> '" & Commonly.SessionPkUserId & "' and RequestedBy IN (SELECT x.pkUserID FROM UserWorkingUnitAssignment uwua INNER JOIN  [USER] x ON x.UserID=uwua.UserID WHERE uwua.WorkingUnitID IN (SELECT uwua2.WorkingUnitID FROM UserWorkingUnitAssignment uwua2 WHERE uwua2.UserID= '" & Commonly.SessionUserId & "'))", "", 0, Integer.MaxValue, 0)
                    If objdata.Count > 0 Then
                        AddTaskListsToTable(102, "You have " & objdata.Count & " WIC List Approval Required.", mydt)
                    End If
                End Using
            End If

            _strMenuHyperlink = "IFTI_Approvals.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using objdata As TList(Of IFTI_Approval) = DataRepository.IFTI_ApprovalProvider.GetPaged(" RequestedBy <>" & Commonly.SessionPkUserId, "", 0, Integer.MaxValue, 0)
                    If objdata.Count > 0 Then
                        AddTaskListsToTable(103, "You have " & objdata.Count & " IFTI Approval Approval Required.", mydt)
                    End If
                End Using
            End If

            _strMenuHyperlink = "WUUpload_Approval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using objdata As TList(Of WU_Approval) = DataRepository.WU_ApprovalProvider.GetPaged(" RequestedBy <>" & Commonly.SessionPkUserId, "", 0, Integer.MaxValue, 0)
                    If objdata.Count > 0 Then
                        AddTaskListsToTable(104, "You have " & objdata.Count & " WU Approval Approval Required.", mydt)
                    End If
                End Using
            End If

            _strMenuHyperlink = "ListofGeneratedIFTIFile.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using objdata As TList(Of ListOfGeneratedIFTI) = DataRepository.ListOfGeneratedIFTIProvider.GetPaged(" IsConfirmed=0 ", "", 0, Integer.MaxValue, 0)
                    If objdata.Count > 0 Then
                        AddTaskListsToTable(105, "You have " & objdata.Count & " List OF IFTI Generated Approval Required.", mydt)
                    End If
                End Using
            End If

            _strMenuHyperlink = "CDDLNPMappingQuestionApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using objdata As CDDLNPNettier.Entities.TList(Of CDDLNPNettier.Entities.MappingSettingCDDBagianQuestion_Approval) = CDDLNPNettier.Data.DataRepository.MappingSettingCDDBagianQuestion_ApprovalProvider.GetPaged(" RequestedBy <> '" & Commonly.SessionPkUserId & "' and RequestedBy IN (SELECT x.pkUserID FROM UserWorkingUnitAssignment uwua INNER JOIN  [USER] x ON x.UserID=uwua.UserID WHERE uwua.WorkingUnitID IN (SELECT uwua2.WorkingUnitID FROM UserWorkingUnitAssignment uwua2 WHERE uwua2.UserID= '" & Commonly.SessionUserId & "'))", "", 0, Integer.MaxValue, 0)
                    If objdata.Count > 0 Then
                        AddTaskListsToTable(106, "You have " & objdata.Count & " CDDLNP Mapping Question Approval Required.", mydt)
                    End If
                End Using
            End If

            _strMenuHyperlink = "CDDLNPWorkflowParameterApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using objdata As CDDLNPNettier.Entities.TList(Of CDDLNPNettier.Entities.CDDLNP_WorkflowParameter_Approval) = CDDLNPNettier.Data.DataRepository.CDDLNP_WorkflowParameter_ApprovalProvider.GetPaged(" RequestedBy <> '" & Commonly.SessionPkUserId & "' and RequestedBy IN (SELECT x.pkUserID FROM UserWorkingUnitAssignment uwua INNER JOIN  [USER] x ON x.UserID=uwua.UserID WHERE uwua.WorkingUnitID IN (SELECT uwua2.WorkingUnitID FROM UserWorkingUnitAssignment uwua2 WHERE uwua2.UserID= '" & Commonly.SessionUserId & "'))", "", 0, Integer.MaxValue, 0)
                    If objdata.Count > 0 Then
                        AddTaskListsToTable(107, "You have " & objdata.Count & " CDDLNP Workflow Parameter Approval Required.", mydt)
                    End If
                End Using
            End If

            _strMenuHyperlink = "CDDLNPWorkflowUploadApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using objdata As CDDLNPNettier.Entities.TList(Of CDDLNPNettier.Entities.CDDLNP_WorkflowUpload_Approval) = CDDLNPNettier.Data.DataRepository.CDDLNP_WorkflowUpload_ApprovalProvider.GetPaged(" RequestedBy <> '" & Commonly.SessionPkUserId & "' and RequestedBy IN (SELECT x.pkUserID FROM UserWorkingUnitAssignment uwua INNER JOIN  [USER] x ON x.UserID=uwua.UserID WHERE uwua.WorkingUnitID IN (SELECT uwua2.WorkingUnitID FROM UserWorkingUnitAssignment uwua2 WHERE uwua2.UserID= '" & Commonly.SessionUserId & "'))", "", 0, Integer.MaxValue, 0)
                    If objdata.Count > 0 Then
                        AddTaskListsToTable(108, "You have " & objdata.Count & " CDDLNP Workflow Upload Approval Required.", mydt)
                    End If
                End Using
            End If

            _strMenuHyperlink = "CDDLNPQuestionApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using objdata As CDDLNPNettier.Entities.TList(Of CDDLNPNettier.Entities.CDD_Question_Approval) = CDDLNPNettier.Data.DataRepository.CDD_Question_ApprovalProvider.GetPaged(" RequestedBy <> '" & Commonly.SessionPkUserId & "' and RequestedBy IN (SELECT x.pkUserID FROM UserWorkingUnitAssignment uwua INNER JOIN  [USER] x ON x.UserID=uwua.UserID WHERE uwua.WorkingUnitID IN (SELECT uwua2.WorkingUnitID FROM UserWorkingUnitAssignment uwua2 WHERE uwua2.UserID= '" & Commonly.SessionUserId & "'))", "", 0, Integer.MaxValue, 0)
                    If objdata.Count > 0 Then
                        AddTaskListsToTable(109, "You have " & objdata.Count & " CDDLNP Question Approval Required.", mydt)
                    End If
                End Using
            End If

            _strMenuHyperlink = "CDDRiskFactQuestionApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using objdata As CDDLNPNettier.Entities.TList(Of CDDLNPNettier.Entities.CDD_RiskFactQuestion_Approval) = CDDLNPNettier.Data.DataRepository.CDD_RiskFactQuestion_ApprovalProvider.GetPaged(" RequestedBy <> '" & Commonly.SessionPkUserId & "' and RequestedBy IN (SELECT x.pkUserID FROM UserWorkingUnitAssignment uwua INNER JOIN  [USER] x ON x.UserID=uwua.UserID WHERE uwua.WorkingUnitID IN (SELECT uwua2.WorkingUnitID FROM UserWorkingUnitAssignment uwua2 WHERE uwua2.UserID= '" & Commonly.SessionUserId & "'))", "", 0, Integer.MaxValue, 0)
                    If objdata.Count > 0 Then
                        AddTaskListsToTable(110, "You have " & objdata.Count & " CDDLNP Risk Fact Question Approval Required.", mydt)
                    End If
                End Using
            End If

            _strMenuHyperlink = "CDDLNPApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Dim strWhereClause As String = String.Empty
                Using objCDDLNP As New AMLBLL.CDDLNPBLL
                    strWhereClause = "LastApprovalBy IN (" & objCDDLNP.GetChildUser & ") AND LastApprovalStatus = 2 AND RequestedBy IN (" & objCDDLNP.GetRMUser() & ")"
                End Using
                Using objdata As CDDLNPNettier.Entities.VList(Of CDDLNPNettier.Entities.vw_CDDLNP_Approval) = CDDLNPNettier.Data.DataRepository.vw_CDDLNP_ApprovalProvider.GetPaged(strWhereClause, String.Empty, 0, Integer.MaxValue, 0)
                    If objdata.Count > 0 Then
                        AddTaskListsToTable(111, "**** You have " & objdata.Count & " CDDLNP Form Approval Required. ****", mydt)
                    End If
                End Using
            End If

            _strMenuHyperlink = "UploadURSCustody_Approval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using objdata As CDDLNPNettier.Entities.VList(Of CDDLNPNettier.Entities.vw_AccountUSRCustody_Approval) = CDDLNPNettier.Data.DataRepository.vw_AccountUSRCustody_ApprovalProvider.GetPaged("  UserID <> '" & Sahassa.AML.Commonly.SessionUserId & "' AND UserID IN (SELECT u.UserID FROM [User] u WHERE u.UserID IN (select UserId from UserWorkingUnitAssignment Where WorkingUnitId in (select WorkingUnitId from UserWorkingUnitAssignment where UserId = '" & Sahassa.AML.Commonly.SessionUserId & "')))", "", 0, Integer.MaxValue, 0)
                    If objdata.Count > 0 Then
                        AddTaskListsToTable(112, "You have " & objdata.Count & " URS Account Approval Required.", mydt)
                    End If
                End Using
            End If

            _strMenuHyperlink = "CDDLNPHeaderApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using objdata As CDDLNPNettier.Entities.VList(Of CDDLNPNettier.Entities.vw_CDD_Bagian_Approval) = CDDLNPNettier.Data.DataRepository.vw_CDD_Bagian_ApprovalProvider.GetPaged(" RequestedBy <> '" & Commonly.SessionPkUserId & "' and RequestedBy IN (SELECT x.pkUserID FROM UserWorkingUnitAssignment uwua INNER JOIN  [USER] x ON x.UserID=uwua.UserID WHERE uwua.WorkingUnitID IN (SELECT uwua2.WorkingUnitID FROM UserWorkingUnitAssignment uwua2 WHERE uwua2.UserID= '" & Commonly.SessionUserId & "'))", "", 0, Integer.MaxValue, 0)
                    If objdata.Count > 0 Then
                        AddTaskListsToTable(113, "You have " & objdata.Count & " CDDLNP Header Approval Required.", mydt)
                    End If
                End Using
            End If

            _strMenuHyperlink = "Bancassurance_Upload_Approval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using objdata As CDDLNPNettier.Entities.TList(Of CDDLNPNettier.Entities.Bancassurance_Approval) = CDDLNPNettier.Data.DataRepository.Bancassurance_ApprovalProvider.GetPaged(" RequestedBy <>" & Commonly.SessionPkUserId, "", 0, Integer.MaxValue, 0)
                    If objdata.Count > 0 Then
                        AddTaskListsToTable(114, "You have " & objdata.Count & " Bancassurance_Approval Required.", mydt)
                    End If
                End Using
            End If

            _strMenuHyperlink = "UploadViewBlackListAML_Approval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using objdata As CDDLNPNettier.Entities.TList(Of CDDLNPNettier.Entities.UploadBlacklistAml_Approval) = CDDLNPNettier.Data.DataRepository.UploadBlacklistAml_ApprovalProvider.GetPaged(" RequestedBy <>" & Commonly.SessionPkUserId, "", 0, Integer.MaxValue, 0)
                    If objdata.Count > 0 Then
                        AddTaskListsToTable(115, "You have " & objdata.Count & " UploadBlacklistAml_Approval Required.", mydt)
                    End If
                End Using
            End If

            _strMenuHyperlink = "StrukturOrganisasi_Upload_Approval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using objdata As CDDLNPNettier.Entities.TList(Of CDDLNPNettier.Entities.StrukturOrganisasi_Approval) = CDDLNPNettier.Data.DataRepository.StrukturOrganisasi_ApprovalProvider.GetPaged(" RequestedBy <>" & Commonly.SessionPkUserId, "", 0, Integer.MaxValue, 0)
                    If objdata.Count > 0 Then
                        AddTaskListsToTable(116, "You have " & objdata.Count & " StrukturOrganisasi_Approval Required.", mydt)
                    End If
                End Using
            End If

            _strMenuHyperlink = "CustomerTicketNumberView.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Dim objCmdSelect As New Data.SqlClient.SqlCommand("usp_GetCountTaskListCTRPelaku")
                objCmdSelect.CommandType = CommandType.StoredProcedure
                objCmdSelect.Parameters.Add("@UserId", SqlDbType.VarChar)
                objCmdSelect.Parameters("@UserId").Value = Sahassa.AML.Commonly.SessionUserId.Replace("'", "''")
                MatchCount = DataRepository.Provider.ExecuteScalar(objCmdSelect)
                If MatchCount <> 0 Then
                    Me.LnkCustomerTicketTransaction.Text = "You have " & MatchCount & " Transaction Ticket that need to be filled."
                End If
            End If
            _strMenuHyperlink = "CustomerVerificationJudgementApproval.aspx"
            If Me.SetnGetUserMenu.Find(AddressOf FindMenuAccess) IsNot Nothing Then
                Using objdata As EkaDataNettier.Entities.TList(Of EkaDataNettier.Entities.AML_Screening_Customer_Judgement) = EkaDataNettier.Data.DataRepository.AML_Screening_Customer_JudgementProvider.GetPaged(" JudgementResult IS  not NULL AND AccountOwnerID IN (SELECT AccountOwnerId FROM AccountOwnerUserMapping WHERE  userid ='" & Commonly.SessionUserId & "') and JudgementBy<>'" & Commonly.SessionUserId & "' and JudgementResult IS  not NULL   ", "", 0, Integer.MaxValue, 0)

                    If objdata.Count > 0 Then
                        AddTaskListsToTable(21, "You have " & objdata.Count & " Customer Verification Judgement Approval Required.", mydt)
                    End If


                End Using
            End If



        Catch
            Throw
        End Try
    End Sub

    Private Function GetChildWorkingUnitHierarchy(ByVal WorkingUnitId As Integer) As AMLDAL.AMLDataSet.WorkingUnitDataTable
        Dim WorkingUnitAdapter As New AMLDAL.AMLDataSetTableAdapters.WorkingUnitTableAdapter()
        Dim WorkingUnitTable As New AMLDAL.AMLDataSet.WorkingUnitDataTable
        Dim WorkingUnitChildTable As New AMLDAL.AMLDataSet.WorkingUnitDataTable
        Dim WorkingUnitReturnTable As New AMLDAL.AMLDataSet.WorkingUnitDataTable
        Dim WorkingUnitRow As AMLDAL.AMLDataSet.WorkingUnitRow

        WorkingUnitChildTable = WorkingUnitAdapter.GetWorkingUnitChildrenByWorkingUnitID(WorkingUnitId)
        For Each WorkingUnitRow In WorkingUnitChildTable.Rows
            WorkingUnitReturnTable = GetChildWorkingUnitHierarchy(WorkingUnitRow.WorkingUnitID)
            If WorkingUnitReturnTable.Rows.Count = 0 Then
                WorkingUnitTable.ImportRow(WorkingUnitRow)
            Else
                WorkingUnitTable.Merge(WorkingUnitReturnTable)
            End If
        Next
        Return WorkingUnitTable
    End Function



    Private Sub AddTaskListsToTable(ByVal id As Int32, ByVal TaskListLabel As String, ByRef myTable As Data.DataTable)
        Dim row As Data.DataRow

        row = myTable.NewRow()

        row("TaskListID") = id
        row("TaskListLabel") = TaskListLabel

        myTable.Rows.Add(row)
    End Sub

    Private Function CreateDataTableTaskList() As Data.DataTable
        Dim TaskListDataTable As Data.DataTable = New Data.DataTable()

        Dim TaskListDataColumn As Data.DataColumn

        TaskListDataColumn = New Data.DataColumn()
        TaskListDataColumn.DataType = Type.GetType("System.Int32")
        TaskListDataColumn.ColumnName = "TaskListID"
        TaskListDataTable.Columns.Add(TaskListDataColumn)

        TaskListDataColumn = New Data.DataColumn()
        TaskListDataColumn.DataType = Type.GetType("System.String")
        TaskListDataColumn.ColumnName = "TaskListLabel"
        TaskListDataTable.Columns.Add(TaskListDataColumn)

        Return TaskListDataTable
    End Function

    Protected Sub LnkCustomerTicketTransaction_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LnkCustomerTicketTransaction.Click
        Me.Response.Redirect("CustomerTicketNumberView.aspx", False)
    End Sub

End Class '2034