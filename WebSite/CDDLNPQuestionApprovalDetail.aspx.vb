Imports CDDLNPNettier.Data
Imports CDDLNPNettier.Entities

Partial Class CDDLNPQuestionApprovalDetail
    Inherits Parent

#Region " Property "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get confirm type
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Devina Ariyanti]	27/2/2014	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property GetPK_CDDLQuestionApp_ID() As Int32
        Get
            Return Request.QueryString("ID")
        End Get
    End Property

    Private ReadOnly Property GetPK_CDDQuestion_ID() As Int32
        Get
            Using adapter As TList(Of CDD_Question_ApprovalDetail) = DataRepository.CDD_Question_ApprovalDetailProvider.GetPaged(CDD_Question_ApprovalDetailColumn.FK_CDD_Question_Approval_Id.ToString & " = " & GetPK_CDDLQuestionApp_ID, "", 0, Integer.MaxValue, 0)

                If adapter.Count = 0 Then
                    Throw New Exception("Approval ID not exist  or already deleted")
                Else
                    Return adapter(0).PK_CDD_Question_ID
                End If
            End Using
        End Get
    End Property

    Private ReadOnly Property GetCDDLNPQuestionNEW() As TList(Of CDD_Question_ApprovalDetail)
        Get
            Session("CDDLNPQuestion.NEW") = DataRepository.CDD_Question_ApprovalDetailProvider.GetPaged("FK_CDD_Question_Approval_Id = " & GetPK_CDDLQuestionApp_ID, "", 0, Integer.MaxValue, 0)
            Return Session("CDDLNPQuestion.NEW")
        End Get
    End Property
    Private ReadOnly Property GetCDDLNPQuestionOLD() As TList(Of CDD_Question)
        Get
            Session("CDDLNPQuestion.OLD") = DataRepository.CDD_QuestionProvider.GetPaged("PK_CDD_Question_ID = " & GetPK_CDDQuestion_ID, "", 0, Integer.MaxValue, 0)
            Return Session("CDDLNPQuestion.OLD")
        End Get
    End Property
    Private ReadOnly Property GetCDDLNPQuestionDetailNEW() As TList(Of CDD_QuestionDetail_ApprovalDetail)
        Get
            Session("CDDLNPQuestionDetail.NEW") = DataRepository.CDD_QuestionDetail_ApprovalDetailProvider.GetPaged("FK_CDD_Question_Approval_Id = " & GetPK_CDDLQuestionApp_ID, "", 0, Integer.MaxValue, 0)
            Return Session("CDDLNPQuestionDetail.NEW")
        End Get
    End Property
    Private ReadOnly Property GetCDDLNPQuestionDetailOLD() As TList(Of CDD_QuestionDetail)
        Get
            Session("CDDLNPQuestionDetail.OLD") = DataRepository.CDD_QuestionDetailProvider.GetPaged("FK_CDD_Question_ID = " & GetPK_CDDQuestion_ID, "", 0, Integer.MaxValue, 0)
            Return Session("CDDLNPQuestionDetail.OLD")
        End Get
    End Property
#End Region

    Private Sub AbandonSession()
        Session("CDDLNPQuestion.NEW") = Nothing
        Session("CDDLNPQuestion.OLD") = Nothing
        Session("CDDLNPQuestionDetail.NEW") = Nothing
        Session("CDDLNPQuestionDetail.OLD") = Nothing
    End Sub

    Private Sub LoadDataCDDLNPQuestionApprovalEdit()
        Using objQuestionApp As VList(Of CDDLNPNettier.Entities.vw_CDD_Question_Approval) = DataRepository.vw_CDD_Question_ApprovalProvider.GetPaged(vw_CDD_Question_ApprovalColumn.PK_CDD_Question_Approval_Id.ToString & " = " & GetPK_CDDLQuestionApp_ID, "", 0, Integer.MaxValue, 0)
            If Not objQuestionApp Is Nothing Then
                Me.lblRequestedBy.Text = objQuestionApp(0).UserName
                Me.lblRequestedDate.Text = objQuestionApp(0).RequestedDate.GetValueOrDefault.ToString("dd-MMM-yyyy HH:mm")
                Me.lblAction.Text = objQuestionApp(0).Mode
            End If
        End Using

        If GetCDDLNPQuestionOLD.Count = 0 Then
            Me.lblOldQuestion.Text = "-"
            Me.lblOldQuestionType.Text = "-"
            Me.lblOldParentQuestion.Text = "-"
            Me.lblOldIsRequired.Text = "-"
            Me.lblOldQuestionSequence.Text = "-"
            Me.gridOldQuestionDetail.DataSource = New TList(Of CDD_QuestionDetail)
            Me.gridOldQuestionDetail.DataBind()
        Else
            Me.lblOldQuestion.Text = GetCDDLNPQuestionOLD(0).CDD_Question
            Using objQuestionType As CDD_QuestionType = DataRepository.CDD_QuestionTypeProvider.GetByPK_CDD_QuestionType_ID(GetCDDLNPQuestionOLD(0).FK_CDD_QuestionType_ID)
                If objQuestionType Is Nothing Then
                    Me.lblOldQuestionType.Text = "-"
                Else
                    Me.lblOldQuestionType.Text = objQuestionType.QuestionTypeName
                End If
            End Using
            Using objQuestion As CDD_Question = DataRepository.CDD_QuestionProvider.GetByPK_CDD_Question_ID(GetCDDLNPQuestionOLD(0).FK_Parent_Question_ID.GetValueOrDefault(0))
                If objQuestion Is Nothing Then
                    Me.lblOldParentQuestion.Text = "-"
                Else
                    Me.lblOldParentQuestion.Text = objQuestion.CDD_Question.Replace("</br>", vbCrLf)
                End If
            End Using
            Me.lblOldIsRequired.Text = GetCDDLNPQuestionOLD(0).IsRequired
            Me.lblOldQuestionSequence.Text = GetCDDLNPQuestionOLD(0).SortNo
            Me.gridOldQuestionDetail.DataSource = GetCDDLNPQuestionDetailOLD
            Me.gridOldQuestionDetail.DataBind()
        End If

        If GetCDDLNPQuestionNEW.Count = 0 Then
            Me.lblNewQuestion.Text = "-"
            Me.lblNewQuestionType.Text = "-"
            Me.lblNewParentQuestion.Text = "-"
            Me.lblNewIsRequired.Text = "-"
            Me.lblNewQuestionSequence.Text = "-"
        Else
            Me.lblNewQuestion.Text = GetCDDLNPQuestionNEW(0).CDD_Question
            Using objQuestionType As CDD_QuestionType = DataRepository.CDD_QuestionTypeProvider.GetByPK_CDD_QuestionType_ID(GetCDDLNPQuestionNEW(0).FK_CDD_QuestionType_ID)
                If objQuestionType Is Nothing Then
                    Me.lblNewQuestionType.Text = "-"
                Else
                    Me.lblNewQuestionType.Text = objQuestionType.QuestionTypeName
                End If
            End Using
            Using objQuestion As CDD_Question = DataRepository.CDD_QuestionProvider.GetByPK_CDD_Question_ID(GetCDDLNPQuestionNEW(0).FK_Parent_Question_ID.GetValueOrDefault(0))
                If objQuestion Is Nothing Then
                    Me.lblNewParentQuestion.Text = "-"
                Else
                    Me.lblNewParentQuestion.Text = objQuestion.CDD_Question.Replace("</br>", vbCrLf)
                End If
            End Using
            Me.lblNewIsRequired.Text = GetCDDLNPQuestionNEW(0).IsRequired
            Me.lblNewQuestionSequence.Text = GetCDDLNPQuestionNEW(0).SortNo
        End If

        If GetCDDLNPQuestionDetailNEW.Count = 0 Then
            Me.gridNewQuestionDetail.DataSource = New TList(Of CDD_QuestionDetail_ApprovalDetail)
            Me.gridNewQuestionDetail.DataBind()
        Else
            Me.gridNewQuestionDetail.DataSource = GetCDDLNPQuestionDetailNEW
            Me.gridNewQuestionDetail.DataBind()
        End If

    End Sub

#Region " Accept "
    Private Sub AcceptDataCDDLNPQuestionAdd()
        Dim objTransManager As New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)
        Dim objCDDQuestion As New CDD_Question
        Dim objCDDQuestionDetail As New TList(Of CDD_QuestionDetail)

        Try
            objTransManager.BeginTransaction()
          
            If Not GetCDDLNPQuestionNEW Is Nothing Then

                'Save Question
                objCDDQuestion.FK_Parent_Question_ID = GetCDDLNPQuestionNEW(0).FK_Parent_Question_ID
                objCDDQuestion.FK_CDD_QuestionType_ID = GetCDDLNPQuestionNEW(0).FK_CDD_QuestionType_ID
                objCDDQuestion.CDD_Question = GetCDDLNPQuestionNEW(0).CDD_Question
                objCDDQuestion.IsRequired = GetCDDLNPQuestionNEW(0).IsRequired
                objCDDQuestion.SortNo = GetCDDLNPQuestionNEW(0).SortNo.GetValueOrDefault(9999)

                Dim intSortNo As Int32 = objCDDQuestion.SortNo
                Using tblCDDQuestion As TList(Of CDD_Question) = DataRepository.CDD_QuestionProvider.GetPaged(objTransManager, "SortNo >= " & intSortNo, "SortNo asc", 0, Int32.MaxValue, 0)
                    If intSortNo = 9999 Then
                        tblCDDQuestion.Add(objCDDQuestion)
                    End If

                    For Each objSort As CDD_Question In tblCDDQuestion
                        intSortNo += 1
                        objSort.SortNo = intSortNo
                    Next

                    If intSortNo <> 9999 Then
                        tblCDDQuestion.Insert(0, objCDDQuestion)
                    End If

                    DataRepository.CDD_QuestionProvider.Save(objTransManager, tblCDDQuestion)
                End Using

                
                'Save Question Detail
                For Each row As CDD_QuestionDetail_ApprovalDetail In GetCDDLNPQuestionDetailNEW
                    objCDDQuestionDetail.Add(New CDD_QuestionDetail)
                    objCDDQuestionDetail(objCDDQuestionDetail.Count - 1).FK_CDD_Question_ID = objCDDQuestion.PK_CDD_Question_ID
                    objCDDQuestionDetail(objCDDQuestionDetail.Count - 1).QuestionDetail = row.QuestionDetail
                    objCDDQuestionDetail(objCDDQuestionDetail.Count - 1).IsNeedValidation = row.IsNeedValidation
                    objCDDQuestionDetail(objCDDQuestionDetail.Count - 1).FK_CDD_VAlidation_ID = row.FK_CDD_VAlidation_ID
                Next
                DataRepository.CDD_QuestionDetailProvider.Save(objTransManager, objCDDQuestionDetail)

                'Delete Approval
                DataRepository.CDD_Question_ApprovalProvider.Delete(objTransManager, GetPK_CDDLQuestionApp_ID)
                DataRepository.CDD_Question_ApprovalDetailProvider.Delete(objTransManager, GetCDDLNPQuestionNEW)
                DataRepository.CDD_QuestionDetail_ApprovalDetailProvider.Delete(objTransManager, GetCDDLNPQuestionDetailNEW)

                objTransManager.Commit()
            End If
        Catch ex As Exception
            If Not objTransManager Is Nothing Then
                objTransManager.Rollback()
            End If
        Finally
            If Not objTransManager Is Nothing Then
                objTransManager.Dispose()
                objTransManager = Nothing
            End If
        End Try
    End Sub

    Private Sub AcceptDataCDDLNPQuestionEdit()
        Dim objTransManager As New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)
        Dim objCDDQuestion As New CDD_Question
        Dim objCDDQuestionDetail As New TList(Of CDD_QuestionDetail)

        Try
            objTransManager.BeginTransaction()

            If Not GetCDDLNPQuestionNEW Is Nothing Then
                'Save question
                objCDDQuestion = DataRepository.CDD_QuestionProvider.GetByPK_CDD_Question_ID(objTransManager, GetPK_CDDQuestion_ID)
                objCDDQuestion.FK_Parent_Question_ID = GetCDDLNPQuestionNEW(0).FK_Parent_Question_ID
                objCDDQuestion.FK_CDD_QuestionType_ID = GetCDDLNPQuestionNEW(0).FK_CDD_QuestionType_ID
                objCDDQuestion.CDD_Question = GetCDDLNPQuestionNEW(0).CDD_Question
                objCDDQuestion.SortNo = GetCDDLNPQuestionNEW(0).SortNo.GetValueOrDefault(9999)
                objCDDQuestion.IsRequired = GetCDDLNPQuestionNEW(0).IsRequired

                Dim intSortNo As Int32 = 0
                Using tblCDDQuestion As TList(Of CDD_Question) = DataRepository.CDD_QuestionProvider.GetPaged(objTransManager, "PK_CDD_Question_ID <> " & GetPK_CDDQuestion_ID, "SortNo asc", 0, Int32.MaxValue, 0)
                    For Each objSort As CDD_Question In tblCDDQuestion
                        intSortNo += 1
                        objSort.SortNo = intSortNo
                    Next
                    DataRepository.CDD_QuestionProvider.Save(objTransManager, tblCDDQuestion)
                End Using

                intSortNo = objCDDQuestion.SortNo
                Using tblCDDQuestion As TList(Of CDD_Question) = DataRepository.CDD_QuestionProvider.GetPaged(objTransManager, "SortNo >= " & intSortNo & " AND PK_CDD_Question_ID <> " & GetPK_CDDQuestion_ID, "SortNo asc", 0, Int32.MaxValue, 0)
                    If intSortNo = 9999 Then
                        tblCDDQuestion.Add(objCDDQuestion)
                    End If

                    For Each objSort As CDD_Question In tblCDDQuestion
                        intSortNo += 1
                        objSort.SortNo = intSortNo
                    Next

                    If intSortNo <> 9999 Then
                        tblCDDQuestion.Insert(0, objCDDQuestion)
                    End If

                    DataRepository.CDD_QuestionProvider.Save(objTransManager, tblCDDQuestion)
                End Using

                'Delete old question detail
                DataRepository.CDD_QuestionDetailProvider.Delete(objTransManager, GetCDDLNPQuestionDetailOLD)

                'Save new question detail
                For Each row As CDD_QuestionDetail_ApprovalDetail In GetCDDLNPQuestionDetailNEW
                    objCDDQuestionDetail.Add(New CDD_QuestionDetail)
                    objCDDQuestionDetail(objCDDQuestionDetail.Count - 1).FK_CDD_Question_ID = objCDDQuestion.PK_CDD_Question_ID
                    objCDDQuestionDetail(objCDDQuestionDetail.Count - 1).QuestionDetail = row.QuestionDetail
                    objCDDQuestionDetail(objCDDQuestionDetail.Count - 1).IsNeedValidation = row.IsNeedValidation
                    objCDDQuestionDetail(objCDDQuestionDetail.Count - 1).FK_CDD_VAlidation_ID = row.FK_CDD_VAlidation_ID
                Next
                DataRepository.CDD_QuestionDetailProvider.Save(objTransManager, objCDDQuestionDetail)

                'Delete approval
                DataRepository.CDD_Question_ApprovalProvider.Delete(objTransManager, GetPK_CDDLQuestionApp_ID)
                DataRepository.CDD_Question_ApprovalDetailProvider.Delete(objTransManager, GetCDDLNPQuestionNEW)
                DataRepository.CDD_QuestionDetail_ApprovalDetailProvider.Delete(objTransManager, GetCDDLNPQuestionDetailNEW)
            End If

            objTransManager.Commit()
        Catch ex As Exception
            If Not objTransManager Is Nothing Then
                objTransManager.Rollback()
            End If
        Finally
            If Not objTransManager Is Nothing Then
                objTransManager.Dispose()
                objTransManager = Nothing
            End If
        End Try
    End Sub

    Private Sub AcceptDataCDDLNPQuestionDelete()
        Dim objTransManager As New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)
        Try
            objTransManager.BeginTransaction()

            If Not GetCDDLNPQuestionNEW Is Nothing Then
                DataRepository.CDD_QuestionProvider.Delete(objTransManager, GetPK_CDDQuestion_ID)
                DataRepository.CDD_QuestionDetailProvider.Delete(objTransManager, GetCDDLNPQuestionDetailOLD)

                DataRepository.CDD_Question_ApprovalProvider.Delete(objTransManager, GetPK_CDDLQuestionApp_ID)
                DataRepository.CDD_Question_ApprovalDetailProvider.Delete(objTransManager, GetCDDLNPQuestionNEW)
                DataRepository.CDD_QuestionDetail_ApprovalDetailProvider.Delete(objTransManager, GetCDDLNPQuestionDetailNEW)
            End If

            objTransManager.Commit()
        Catch ex As Exception
            If Not objTransManager Is Nothing Then
                objTransManager.Rollback()
            End If
        Finally
            If Not objTransManager Is Nothing Then
                objTransManager.Dispose()
                objTransManager = Nothing
            End If
        End Try
    End Sub
#End Region

#Region " Reject "
    Private Sub RejectDataCDDLNPQuestion()
        Dim objTransManager As New TransactionManager(DataRepository.ConnectionStrings("netTiersConnectionString").ConnectionString)
        Try
            objTransManager.BeginTransaction()

            If Not GetCDDLNPQuestionNEW Is Nothing Then
                DataRepository.CDD_Question_ApprovalProvider.Delete(objTransManager, GetPK_CDDLQuestionApp_ID)
                DataRepository.CDD_Question_ApprovalDetailProvider.Delete(objTransManager, GetCDDLNPQuestionNEW)
                DataRepository.CDD_QuestionDetail_ApprovalDetailProvider.Delete(objTransManager, GetCDDLNPQuestionDetailNEW)
            End If

            objTransManager.Commit()
        Catch ex As Exception
            If Not objTransManager Is Nothing Then
                objTransManager.Rollback()
            End If
        Finally
            If Not objTransManager Is Nothing Then
                objTransManager.Dispose()
                objTransManager = Nothing
            End If
        End Try
    End Sub
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.ImageAccept.Attributes.Add("onclick", "javascript: if(confirm('Do you want to accept this activity ?')== false) return false;")
                Me.ImageReject.Attributes.Add("onclick", "javascript: if(confirm('Do you want to reject this activity ?')== false) return false;")
                Using objQuestionApp As VList(Of CDDLNPNettier.Entities.vw_CDD_Question_Approval) = DataRepository.vw_CDD_Question_ApprovalProvider.GetPaged(vw_CDD_Question_ApprovalColumn.PK_CDD_Question_Approval_Id.ToString & " = " & GetPK_CDDLQuestionApp_ID, "", 0, Integer.MaxValue, 0)
                    If Not objQuestionApp Is Nothing Then
                        Me.LoadDataCDDLNPQuestionApprovalEdit()
                    End If
                End Using

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click
        Try
            Using objQuestionApp As VList(Of CDDLNPNettier.Entities.vw_CDD_Question_Approval) = DataRepository.vw_CDD_Question_ApprovalProvider.GetPaged(vw_CDD_Question_ApprovalColumn.PK_CDD_Question_Approval_Id.ToString & " = " & GetPK_CDDLQuestionApp_ID, "", 0, Integer.MaxValue, 0)
                If Not objQuestionApp Is Nothing Then
                    Dim mode As Integer = objQuestionApp(0).FK_MsMode_Id
                    Select Case mode
                        Case TypeConfirm.Add
                            Me.AcceptDataCDDLNPQuestionAdd()
                        Case TypeConfirm.Edit
                            Me.AcceptDataCDDLNPQuestionEdit()
                        Case TypeConfirm.Delete
                            Me.AcceptDataCDDLNPQuestionDelete()
                        Case Else
                            Throw New Exception("Type not supported type:" & mode)
                    End Select

                    AbandonSession()
                    Sahassa.AML.Commonly.SessionIntendedPage = "CDDLNPMessage.aspx?ID=101" & mode & "3"
                    Me.Response.Redirect(Sahassa.AML.Commonly.SessionIntendedPage, False)
                End If
            End Using
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click
        Try
            Using objQuestionApp As VList(Of CDDLNPNettier.Entities.vw_CDD_Question_Approval) = DataRepository.vw_CDD_Question_ApprovalProvider.GetPaged(vw_CDD_Question_ApprovalColumn.PK_CDD_Question_Approval_Id.ToString & " = " & GetPK_CDDLQuestionApp_ID, "", 0, Integer.MaxValue, 0)
                If Not objQuestionApp Is Nothing Then
                    Dim mode As Integer = objQuestionApp(0).FK_MsMode_Id
                    
                    Me.RejectDataCDDLNPQuestion()

                    AbandonSession()
                    Sahassa.AML.Commonly.SessionIntendedPage = "CDDLNPMessage.aspx?ID=101" & mode & "2"
                    Me.Response.Redirect(Sahassa.AML.Commonly.SessionIntendedPage, False)
                End If
            End Using
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        AbandonSession()
        Sahassa.AML.Commonly.SessionIntendedPage = "CDDLNPQuestionApproval.aspx"
        Me.Response.Redirect("CDDLNPQuestionApproval.aspx", False)
    End Sub

    Public Enum TypeConfirm
        Add = 1
        Edit
        Delete
    End Enum

    Protected Sub gridQuestionDetail_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles gridNewQuestionDetail.ItemDataBound, gridOldQuestionDetail.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Using objValidation As CDD_VAlidation = DataRepository.CDD_VAlidationProvider.GetByPK_CDD_VAlidation_ID(e.Item.Cells(1).Text)
                    If objValidation IsNot Nothing Then
                        e.Item.Cells(2).Text = objValidation.CDD_VAlidationName
                    End If
                End Using
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class '677