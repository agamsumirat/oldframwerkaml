<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="ListofGeneratedCTRFile.aspx.vb" Inherits="ListofGeneratedCTRFile" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
    <script src="Script/popcalendar.js"></script>
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td>
                <img src="Images/blank.gif" width="5" height="1" /></td>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td></td><td width="99%" bgcolor="#FFFFFF">
                            <img src="Images/blank.gif" width="1" height="1" /></td>
                        <td bgcolor="#FFFFFF">
                            <img src="Images/blank.gif" width="1" height="1" /></td>
                            
                    </tr>
                </table>
            </td>
            <td>
                <img src="Images/blank.gif" width="5" height="1" /></td>
        </tr>
        <tr>
            <td>
            </td>
            <td id="tdcontent" height="99%" valign="top" bgcolor="#FFFFFF">
                <div id="divcontent" class="divcontent">
                    <table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td>
                                <img src="Images/blank.gif" width="5" height="100%" /></td>
                            <td class="divcontentinside" bgcolor="#FFFFFF" width="100%">
                                <ajax:ajaxpanel  ID="a" runat="server">
                                </ajax:ajaxpanel  >
                               <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
                                                style="border-top-style: none; border-right-style: none; border-left-style: none;
                                                border-bottom-style: none" width="100%">
                                                <tr>
                                                    <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                                                        border-right-style: none; border-left-style: none; border-bottom-style: none;
                                                        font-family: Tahoma">
                                                        <img src="Images/dot_title.gif" width="17" height="17">&nbsp;<b><asp:Label ID="Label1"
                                                            runat="server" Text="List of Generated CTR File"></asp:Label></b><hr />
                                                    </td>
                                                </tr>
                                            </table>
                                <table border="0" cellpadding="0" cellspacing="0" style="background-color: White;
                                                border-color: White;" width="100%">
                                                <tr>
                                                    <td>
                                                        <table align="left" bordercolor="#ffffff" cellspacing="1" cellpadding="2" 
                                                            bgcolor="#dddddd" border="2" width="100%">
                                                            <tr>
                                                                <td background="Images/search-bar-background.gif" valign="middle" width="100%" align="left"
                                                                    style="height: 6px">
                                                                    <table cellpadding="0" cellspacing="0" border="0">
                                                                        <tr>
                                                                            <td class="formtext">
                                                                                <asp:Label ID="Label9" runat="server" Text="Searching Criteria" Font-Bold="True"></asp:Label>&nbsp;</td>
                                                                            <td>
                                                                                <a href="#" onclick="javascript:ShowHidePanel('SearchBox','searchimage','Images/search-bar-minimize.gif','images/search-bar-maximize.gif')"
                                                                                    title="click to minimize or maximize">
                                                                                    <img id="searchimage" src="Images/search-bar-minimize.gif" border="0" height="12px"
                                                                                        width="12px"></a></td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr id="SearchBox">
                                                                <td valign="top" bgcolor="#ffffff">
                                                                    <ajax:AjaxPanel ID="AjaxPanel1" runat="server" Width="100%">
                                                                        <table cellspacing="1" cellpadding="2" width="100%" border="0" style="border-top-style: none;
                                                                            border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                                            <tr class="formText">
                                                                                <td width="17%" bgcolor="#FFF7E6">
                                                                                    <asp:Label ID="Label20" runat="server" Text="Transaction Date"></asp:Label></td>
                                                                                <td width="1%" bgcolor="#ffffff">
                                                                                    :</td>
                                                                                <td width="80%" bgcolor="#ffffff">
                                                                                   <asp:TextBox ID="txtFilterTransactionDate" TabIndex="2" runat="server" CssClass="searcheditbox"
                                                                                        Width="100px"></asp:TextBox><input id="popUpTransactionDate"
                                                                                            title="Click to show calendar" style="border-right: #ffffff 0px solid; border-top: #ffffff 0px solid;
                                                                                            font-size: 11px; border-left: #ffffff 0px solid; border-bottom: #ffffff 0px solid;
                                                                                            height: 17px; background-image: url(Script/Calendar/cal.gif); width: 16px;" type="button"
                                                                                            onclick="popUpCalendar(this, frmBasicCTRWeb.txtFilterTransactionDate, 'dd-mmm-yyyy')" runat="server">
                                                                                </td>
                                                                            </tr>
                                                                            <tr class="formText">
                                                                                <td width="15%" bgcolor="#FFF7E6" valign="top">
                                                                                    <asp:Label ID="Label28" runat="server" Text="Report Type"></asp:Label></td>
                                                                                <td width="1%" bgcolor="#ffffff">
                                                                                    :</td>
                                                                                <td width="30%" bgcolor="#ffffff">
                                                                                    <ajax:AjaxPanel ID="AjaxPanel33" runat="server">
                                                                                        <asp:RadioButtonList ID="RdbReportType" runat="server" RepeatColumns="3">
                                                                                           
                                                                                        </asp:RadioButtonList>
                                                                                    </ajax:AjaxPanel>
                                                                                </td>
                                                                              
                                                                            </tr>
                                                                             <tr class="formText">
                                                                                <td width="15%" bgcolor="#FFF7E6" valign="top">
                                                                                    <asp:Label ID="Label8" runat="server" Text="Status Uploaded to PPATK"></asp:Label></td>
                                                                                <td width="1%" bgcolor="#ffffff">
                                                                                    :</td>
                                                                                <td width="30%" bgcolor="#ffffff">
                                                                                    <ajax:AjaxPanel ID="AjaxPanel26" runat="server">
                                                                                        <asp:RadioButtonList ID="RdbStatusUploadToPPATK" runat="server" RepeatColumns="4">
                                                                                            
                                                                                        </asp:RadioButtonList>
                                                                                    </ajax:AjaxPanel>
                                                                                </td>
                                                                              
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="7">
                                                                                    <ajax:AjaxPanel ID="AjaxPanel3" runat="server">
                                                                                        <asp:ImageButton ID="BtnSearch" TabIndex="3" runat="server" ImageUrl="~/Images/button/Search.gif"
                                                                                            CausesValidation="False"></asp:ImageButton>&nbsp;&nbsp;<asp:ImageButton ID="ImageClearSearch"
                                                                                                runat="server" ImageUrl="~/Images/button/ClearSearch.gif" CausesValidation="False" /><asp:CustomValidator
                                                                                                    ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></ajax:AjaxPanel></td>
                                                                            </tr>
                                                                        </table>
                                                                    </ajax:AjaxPanel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                </table>
                                                 <table border="0" cellpadding="0" cellspacing="0" style="background-color: White;
                                                border-color: White;" width="100%">
                                                <tr>
                                                    <td>
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table style="width: 100%; height: 100%">
                                                            <tr>
                                                                <td>
                                                                    <ajax:AjaxPanel ID="AjaxPanel4" runat="server" Width="1300px">
                                                                        <asp:DataGrid ID="DtGridListofGeneratedFileView" runat="server" SkinID="gridview"
                                                                            AutoGenerateColumns="False" Font-Size="XX-Small" CellPadding="4"
                                                                            AllowPaging="True" Width="100%" GridLines="Vertical" AllowSorting="True" ForeColor="Black"
                                                                            Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                            Font-Underline="False" HorizontalAlign="Left" BorderWidth="1px" 
                                                                            BackColor="White" BorderColor="#DEDFDE" BorderStyle="None">
                                                                            <AlternatingItemStyle BackColor="White" />
                                                                            <Columns>
                                                                                <asp:TemplateColumn>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="CheckBoxExporttoExcel" runat="server" />
                                                                                    </ItemTemplate>
                                                                                    <HeaderStyle Width="1%" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:BoundColumn DataField="PK_ListOfGeneratedFileCTR_ID" Visible="False">
                                                                                    <HeaderStyle Width="0%" />
                                                                                </asp:BoundColumn>
                                                                                <asp:BoundColumn HeaderText="No.">
                                                                                    <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" 
                                                                                        Font-Strikeout="False" Font-Underline="False" ForeColor="White" 
                                                                                        HorizontalAlign="Center" VerticalAlign="Top" Width="2%" Wrap="False" />
                                                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                        VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="FileName" Visible="False">
                                                                                    <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" 
                                                                                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                        VerticalAlign="Top" Width="0%" Wrap="False" />
                                                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                        VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="CreatedDate" DataFormatString="{0:dd-MMM-yyyy}" 
                                                                                    HeaderText="Request Date" SortExpression="CreatedDate  desc">
                                                                                </asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="TransactionDate" 
                                                                                    DataFormatString="{0:dd-MMM-yyyy}" HeaderText="Transaction Date" 
                                                                                    SortExpression="TransactionDate desc">
                                                                                    <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" 
                                                                                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                        VerticalAlign="Top" Width="5%" Wrap="False" />
                                                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                        VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="TotalCIF" HeaderText="Total #CIF" 
                                                                                    SortExpression="TotalCIF desc">
                                                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                        VerticalAlign="Top" Width="5%" Wrap="False" />
                                                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                        VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="TotalWIC" HeaderText="Total #WIC" 
                                                                                    SortExpression="TotalWIC desc">
                                                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                        VerticalAlign="Top" Width="5%" Wrap="False" />
                                                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                        VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="ReportTypeDesc" HeaderText="Report Type" 
                                                                                    SortExpression="ReportTypeDesc desc">
                                                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                        VerticalAlign="Top" Width="5%" Wrap="False" />
                                                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                        VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="ReportFormatName" HeaderText="Report Format" 
                                                                                    SortExpression="ReportFormatName  desc">
                                                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                        VerticalAlign="Top" Width="5%" Wrap="False" />
                                                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                        VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="MsStatusUploadPPATK_Name" 
                                                                                    HeaderText="Status Uploaded to PPATK" 
                                                                                    SortExpression="MsStatusUploadPPATK_Name  desc">
                                                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                        VerticalAlign="Middle" Width="7%" Wrap="False" />
                                                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                        VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="StatusReportApprovalDesc" 
                                                                                    HeaderText="Status Approval" SortExpression="StatusReportApprovalDesc  desc">
                                                                                </asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="UserIDApproved" HeaderText="Confirmed By" 
                                                                                    SortExpression="UserIDApproved  desc">
                                                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" 
                                                                                        VerticalAlign="Top" Width="5%" Wrap="False" />
                                                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" 
                                                                                        VerticalAlign="Top" Wrap="False" />
                                                                                </asp:BoundColumn>
                                                                                <asp:TemplateColumn>
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="LinkGenerate" runat="server" 
                                                                                            CommandArgument='<%# Eval("PK_ListOfGeneratedFileCTR_ID") %>' 
                                                                                            OnClick="LinkGenerate_Click" ajaxcall="none" Text="Download"></asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                        VerticalAlign="Middle" Width="3%" Wrap="False" />
                                                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" 
                                                                                        VerticalAlign="Top" Wrap="False" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn>
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="LnkUploadRefNo" runat="server" 
                                                                                           CommandArgument='<%# Eval("PK_ListOfGeneratedFileCTR_ID") %>'  onclick="LnkUploadRefNo_Click">Upload Reff No.</asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                      <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                        VerticalAlign="Middle" Width="3%" Wrap="False" />
                                                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" 
                                                                                        VerticalAlign="Top" Wrap="False" />
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn>
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="LinkListofTransaction" runat="server" 
                                                                                            CommandArgument='<%# Eval("PK_ListOfGeneratedFileCTR_ID") %>' 
                                                                                            Text="List of Transaction" onclick="LinkListofTransaction_Click"></asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                    <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                        VerticalAlign="Top" Width="3%" Wrap="False" />
                                                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                                                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                                                        VerticalAlign="Top" Wrap="False" />
                                                                                </asp:TemplateColumn>
                                                                               
                                                                               
                                                                            </Columns>
                                                                            <FooterStyle BackColor="#CCCC99" />
                                                                            <HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#6B696B" />
                                                                            <ItemStyle BackColor="#F7F7DE" BorderColor="#CC9966" BorderStyle="Solid" />
                                                                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" 
                                                                                Mode="NumericPages" Visible="False" />
                                                                            <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                                        </asp:DataGrid></ajax:AjaxPanel></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="background-color: #ffffff; height: 5px;">
                                                                    &nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="background-color: #ffffff">
                                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                        <tr>
                                                                            <td nowrap>
                                                                                <ajax:AjaxPanel ID="AjaxPanel2" runat="server" meta:resourcekey="AjaxPanel5Resource1">
                                                                                    &nbsp;<asp:CheckBox ID="CheckBoxSelectAll" runat="server" Text="Select All" AutoPostBack="True" />
                                                                                    &nbsp; &nbsp;
                                                                                </ajax:AjaxPanel>
                                                                                <asp:LinkButton ID="LinkButtonExportExcel" runat="server" Text="Export Selected Data to Excel"
                                                                                    ajaxcall="none" OnClientClick="aspnetForm.encoding = 'multipart/form-data';"></asp:LinkButton>&nbsp;&nbsp;&nbsp;<asp:LinkButton
                                                                                        ID="lnkExportAllData" runat="server" Text="Export All to Excel" ajaxcall="none"
                                                                                        OnClientClick="aspnetForm.encoding = 'multipart/form-data';"></asp:LinkButton></td>
                                                                            <td width="100%">
                                                                                &nbsp;&nbsp;</td>
                                                                            <td align="right" nowrap>
                                                                                &nbsp;&nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td bgcolor="#ffffff" colspan="3">
                                                                                <table id="Table3" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                                                    bgcolor="#ffffff" border="2">
                                                                                    <tr class="regtext" align="center" bgcolor="#dddddd">
                                                                                        <td valign="top" align="left" width="50%" bgcolor="#ffffff">
                                                                                            Page&nbsp;<ajax:AjaxPanel ID="AjaxPanel6" runat="server"><asp:Label ID="PageCurrentPage"
                                                                                                runat="server" CssClass="regtext" Text="0"></asp:Label>&nbsp;of&nbsp;<asp:Label ID="PageTotalPages"
                                                                                                    runat="server" CssClass="regtext" Text="0"></asp:Label></ajax:AjaxPanel></td>
                                                                                        <td valign="top" align="right" width="50%" bgcolor="#ffffff">
                                                                                            Total Records&nbsp;<ajax:AjaxPanel ID="AjaxPanel7" runat="server"><asp:Label ID="PageTotalRows"
                                                                                                runat="server" Text="0"></asp:Label></ajax:AjaxPanel></td>
                                                                                    </tr>
                                                                                </table>
                                                                                <table id="Table4" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                                                    bgcolor="#ffffff" border="2">
                                                                                    <tr bgcolor="#ffffff">
                                                                                        <td class="regtext" valign="middle" align="left" colspan="11" style="height: 4px">
                                                                                            <hr>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="regtext" valign="middle" align="left" width="63" bgcolor="#ffffff">
                                                                                            Go to page</td>
                                                                                        <td class="regtext" valign="middle" align="left" width="5" bgcolor="#ffffff">
                                                                                            <font face="Verdana, Arial, Helvetica, sans-serif" size="1">
                                                                                                <ajax:AjaxPanel ID="AjaxPanel8" runat="server">
                                                                                                    <asp:TextBox ID="TextGoToPage" runat="server" Width="38px" CssClass="searcheditbox"></asp:TextBox></ajax:AjaxPanel></font></td>
                                                                                        <td class="regtext" valign="middle" align="left" bgcolor="#ffffff">
                                                                                            <ajax:AjaxPanel ID="AjaxPanel9" runat="server">
                                                                                                <asp:ImageButton ID="ImageButtonGo" runat="server" ImageUrl="~/Images/button/go.gif">
                                                                                                </asp:ImageButton></ajax:AjaxPanel></td>
                                                                                        <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                                                                                            <img height="5" src="images/first.gif" width="6"></td>
                                                                                        <td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff">
                                                                                            <ajax:AjaxPanel ID="AjaxPanel10" runat="server">
                                                                                                <asp:LinkButton ID="LinkButtonFirst" runat="server" CssClass="regtext" CommandName="First"
                                                                                                    OnCommand="PageNavigate" Text="First"></asp:LinkButton></ajax:AjaxPanel></td>
                                                                                        <td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff">
                                                                                            <img height="5" src="images/prev.gif" width="6"></td>
                                                                                        <td class="regtext" valign="middle" align="right" width="14" bgcolor="#ffffff">
                                                                                            <ajax:AjaxPanel ID="AjaxPanel11" runat="server">
                                                                                                <asp:LinkButton ID="LinkButtonPrevious" runat="server" CssClass="regtext" CommandName="Prev"
                                                                                                    OnCommand="PageNavigate" Text="Previous"></asp:LinkButton></ajax:AjaxPanel></td>
                                                                                        <td class="regtext" valign="middle" align="right" width="60" bgcolor="#ffffff">
                                                                                            <ajax:AjaxPanel ID="AjaxPanel12" runat="server">
                                                                                                <a class="pageNav" href="#">
                                                                                                    <asp:LinkButton ID="LinkButtonNext" runat="server" CssClass="regtext" CommandName="Next"
                                                                                                        OnCommand="PageNavigate" Text="Next"></asp:LinkButton></a></ajax:AjaxPanel></td>
                                                                                        <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                                                                                            <img height="5" src="images/next.gif" width="6"></td>
                                                                                        <td class="regtext" valign="middle" align="left" width="25" bgcolor="#ffffff">
                                                                                            <ajax:AjaxPanel ID="AjaxPanel13" runat="server">
                                                                                                <asp:LinkButton ID="LinkButtonLast" runat="server" CssClass="regtext" CommandName="Last"
                                                                                                    OnCommand="PageNavigate" Text="Last"></asp:LinkButton></ajax:AjaxPanel></td>
                                                                                        <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff">
                                                                                            <img height="5" src="images/last.gif" width="6"></td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                              </td>
                        </tr>
                    </table>
                </div>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td background="Images/button-bground.gif" align="left" valign="middle">
                            <img src="Images/blank.gif" width="5" height="1" /></td>
                        <td background="Images/button-bground.gif" align="left" valign="middle">
                            <img src="images/arrow.gif" width="15" height="15" /></td>
                        <td width="99%" background="Images/button-bground.gif">
                            <img src="Images/blank.gif" width="1" height="1" /></td>
                        <td>
                            </td>
                    </tr>
                </table>
            </td>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>
