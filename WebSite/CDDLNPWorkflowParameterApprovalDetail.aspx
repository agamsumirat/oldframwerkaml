﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="CDDLNPWorkflowParameterApprovalDetail.aspx.vb" Inherits="CDDLNPWorkflowParameterApprovalDetail" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
        height="72" style="border-top-style: none; border-right-style: none; border-left-style: none;
        border-bottom-style: none" width="100%">
        <tr>
            <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>
                    <img height="17" src="Images/dot_title.gif" width="17" />
                    <asp:Label ID="lblHeader" runat="server" Text="EDD Group Workflow Parameter Approval"></asp:Label>&nbsp;
                    <hr />
                </strong>
            </td>
        </tr>
    </table>
    <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
        border="2" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none">
        <tr class="formText" height="20">
            <td bgcolor="#ffffff" width="5px">
                &nbsp;</td>
            <td bgcolor="#ffffff" width="20%">
                Requested By</td>
            <td bgcolor="#ffffff" width="5px">
                :</td>
            <td bgcolor="#ffffff" colspan="2" width="80%">
                <asp:Label ID="lblRequestedBy" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText" height="20">
            <td bgcolor="#ffffff">
                &nbsp;</td>
            <td bgcolor="#ffffff">
                Requested Date</td>
            <td bgcolor="#ffffff">
                :</td>
            <td bgcolor="#ffffff" colspan="2">
                <asp:Label ID="lblRequestedDate" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText" height="20">
            <td bgcolor="#ffffff">
            </td>
            <td bgcolor="#ffffff">
                Action</td>
            <td bgcolor="#ffffff">
                :</td>
            <td bgcolor="#ffffff" colspan="2">
                <asp:Label ID="lblAction" runat="server"></asp:Label></td>
        </tr>
        <tr class="formText" height="20px">
            <td bgcolor="#ffffff" colspan="5">
                &nbsp;</td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" width="5px">
                &nbsp;</td>
            <td bgcolor="#ffffff" colspan="4" width="50%">
                <table bordercolor="white" cellspacing="1" cellpadding="2" width="100%" bgcolor="#ffffff"
                    border="2" height="72" style="border-top-style: none; border-right-style: none;
                    border-left-style: none; border-bottom-style: none">
                    <tr>
                        <td>
                            <table bordercolor="#dddddd" cellspacing="1" cellpadding="2" width="100%" bgcolor="#ffffff"
                                border="2" style="border-top-style: none; border-right-style: none; border-left-style: none;
                                border-bottom-style: none">
                                <tr>
                                    <td width="20%">
                                    </td>
                                    <td width="5">
                                    </td>
                                    <td width="40%">
                                        <strong>Old Data</strong></td>
                                    <td width="40%">
                                        <strong>New Data</strong></td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        Level
                                    </td>
                                    <td width="5px">
                                        :
                                    </td>
                                    <td width="40%">
                                        <asp:Label ID="lblOldLevel" runat="server"></asp:Label></td>
                                    <td width="40%">
                                        <asp:Label ID="lblNewLevel" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td width="20%">
                                        Group Workflow
                                    </td>
                                    <td width="5px">
                                        :
                                    </td>
                                    <td width="40%">
                                        <asp:Label ID="lblOldGroupWorkflow" runat="server"></asp:Label></td>
                                    <td width="40%">
                                        <asp:Label ID="lblNewGroupWorkflow" runat="server"></asp:Label></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="formText" bgcolor="#dddddd" height="30">
            <td style="width: 5px">
                <img height="15" src="images/arrow.gif" width="15"></td>
            <td colspan="6" style="height: 9px">
                <table cellspacing="0" cellpadding="3" border="0">
                    <tr>
                        <td>
                            <ajax:AjaxPanel ID="AjaxPanel5" runat="server">
                                <asp:ImageButton ID="ImageAccept" runat="server" CausesValidation="True" ImageUrl="~/Images/Button/Accept.gif" /></ajax:AjaxPanel></td>
                        <td>
                            <ajax:AjaxPanel ID="AjaxPanel7" runat="server">
                                <asp:ImageButton ID="ImageReject" runat="server" CausesValidation="True" ImageUrl="~/Images/Button/Reject.gif">
                                </asp:ImageButton></ajax:AjaxPanel></td>
                        <td>
                            <ajax:AjaxPanel ID="AjaxPanel8" runat="server">
                                <asp:ImageButton ID="ImageCancel" runat="server" ImageUrl="~/Images/Button/Cancel.gif">
                                </asp:ImageButton></ajax:AjaxPanel></td>
                    </tr>
                </table>
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
        </tr>
    </table>
</asp:Content>
