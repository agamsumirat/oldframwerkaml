#Region "Imports..."
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Excel
Imports Sahassa.AML.Commonly
Imports Sahassanettier.Data
Imports Sahassanettier.Entities
Imports Sahassanettier.Data.SqlClient
Imports System
Imports System.Collections.Generic
Imports System.Data
Imports System.IO
Imports AMLBLL.DataType
#End Region

Partial Class MsPekerjaan_upload
    Inherits Parent

#Region "Structure..."

    Structure GrouP_MsPekerjaan_success
        Dim MsPekerjaan As MsPekerjaan_ApprovalDetail
        Dim L_msPekerjaanNCBS As TList(Of MsPekerjaanNCBS_ApprovalDetail)

    End Structure

#End Region

#Region "Property umum"
    Private Property GroupTableSuccessData() As List(Of GrouP_MsPekerjaan_success)
        Get
            Return Session("GroupTableSuccessData")
        End Get
        Set(ByVal value As List(Of GrouP_MsPekerjaan_success))
            Session("GroupTableSuccessData") = value
        End Set
    End Property

    Private Property GroupTableAnomalyData() As List(Of AMLBLL.MsPekerjaanBll.DT_Group_MsPekerjaan)
        Get

            Return Session("GroupTableAnomalyData")
        End Get
        Set(ByVal value As List(Of AMLBLL.MsPekerjaanBll.DT_Group_MsPekerjaan))
            Session("GroupTableAnomalyData") = value
        End Set
    End Property

    Public ReadOnly Property GET_PKUserID() As Integer
        Get
            Return SessionPkUserId
        End Get
    End Property

    Public ReadOnly Property GET_UserName() As String
        Get
            Return SessionUserId
            'Return SessionNIK
        End Get
    End Property

    Private Property SetnGetDataTableForSuccessData() As TList(Of MsPekerjaan_ApprovalDetail)
        Get
            Return CType(IIf(Session("MsPekerjaan_upload.TableSuccessUpload") Is Nothing, Nothing, Session("MsPekerjaan_upload.TableSuccessUpload")), TList(Of MsPekerjaan_ApprovalDetail))
        End Get
        Set(ByVal value As TList(Of MsPekerjaan_ApprovalDetail))
            Session("MsPekerjaan_upload.TableSuccessUpload") = value
        End Set
    End Property

    Private Property SetnGetDataTableForAnomalyData() As System.Data.DataTable
        Get
            Return CType(IIf(Session("MsPekerjaan_upload.TableAnomalyUpload") Is Nothing, Nothing, Session("MsPekerjaan_upload.TableAnomalyUpload")), System.Data.DataTable)
        End Get
        Set(ByVal value As System.Data.DataTable)
            Session("MsPekerjaan_upload.TableAnomalyUpload") = value
        End Set
    End Property

    Private Property DSexcelTemp() As Data.DataSet
        Get
            Return CType(IIf(Session("MsPekerjaan_upload.DSexcelTemp") Is Nothing, Nothing, Session("MsPekerjaan_upload.DSexcelTemp")), DataSet)
        End Get
        Set(ByVal value As Data.DataSet)
            Session("MsPekerjaan_upload.DSexcelTemp") = value
        End Set
    End Property

#End Region

#Region "Property Tabel Temporary"
    Private Property DT_AllMsPekerjaan() As Data.DataTable
        Get
            Return CType(IIf(Session("MsPekerjaan_upload.DT_MsPekerjaan") Is Nothing, Nothing, Session("MsPekerjaan_upload.DT_MsPekerjaan")), System.Data.DataTable)
        End Get
        Set(ByVal value As Data.DataTable)
            Session("MsPekerjaan_upload.DT_MsPekerjaan") = value
        End Set
    End Property

    Private Property DT_AllmsPekerjaanNCBS() As Data.DataTable
        Get
            Return CType(IIf(Session("MsPekerjaan_upload.DT_msPekerjaanNCBS") Is Nothing, Nothing, Session("MsPekerjaan_upload.DT_msPekerjaanNCBS")), System.Data.DataTable)
        End Get
        Set(ByVal value As Data.DataTable)
            Session("MsPekerjaan_upload.DT_msPekerjaanNCBS") = value
        End Set
    End Property

#End Region

#Region "Property Table Sukses Data"

    Private Property TableSuccessMsPekerjaan() As TList(Of MsPekerjaan_ApprovalDetail)
        Get
            Return Session("TableSuccessMsPekerjaan")
        End Get
        Set(ByVal value As TList(Of MsPekerjaan_ApprovalDetail))
            Session("TableSuccessMsPekerjaan") = value
        End Set
    End Property

    Private Property TableSuccessmsPekerjaanNCBS() As TList(Of msPekerjaanNCBS)
        Get
            Return Session("TableSuccessmsPekerjaanNCBS")
        End Get
        Set(ByVal value As TList(Of msPekerjaanNCBS))
            Session("TableSuccessmsPekerjaanNCBS") = value
        End Set
    End Property


#End Region

#Region "Property Table Anomaly Data"

    Private Property TableAnomalyMsPekerjaan() As System.Data.DataTable
        Get
            Return Session("TableAnomalyMsPekerjaan")
        End Get
        Set(ByVal value As System.Data.DataTable)
            Session("TableAnomalyMsPekerjaan") = value
        End Set
    End Property

    Private Property TableAnomalymsPekerjaanNCBS() As System.Data.DataTable
        Get
            Return Session("TableAnomalymsPekerjaanNCBS")
        End Get
        Set(ByVal value As System.Data.DataTable)
            Session("TableAnomalymsPekerjaanNCBS") = value
        End Set
    End Property

#End Region

#Region "validasi.."

    Private Function ValidateInput(ByVal MsPekerjaan As AMLBLL.MsPekerjaanBll.DT_Group_MsPekerjaan) As List(Of String)
        Dim temp As New List(Of String)
        Dim msgError As New StringBuilder
        Dim errorline As New StringBuilder
        Try
            'validasi
            ValidateMsPekerjaan(MsPekerjaan.MsPekerjaan, msgError, errorline)
            If msgError.ToString = "" Then
                For Each ci As DataRow In MsPekerjaan.L_msPekerjaanNCBS.Rows
                    'validasi NCBS
                    ValidatemsPekerjaanNCBS(ci, msgError, errorline)
                Next
            End If
            temp.Add(msgError.ToString)
            temp.Add(errorline.ToString)
        Catch ex As Exception
            msgError.Append(ex.Message)
            temp.Add(msgError.ToString)
            temp.Add(errorline.ToString)
        End Try
        Return temp
    End Function

    Private Shared Sub ValidatemsPekerjaanNCBS(ByVal DTR As DataRow, ByVal msgError As StringBuilder, ByVal errorLine As StringBuilder)
        Try

            '======================== Validasi textbox :  IDPekerjaanNCBS canot Null (#)====================================================
            If ObjectAntiNull(DTR("IDPekerjaanBank")) = False Then
                msgError.AppendLine(" &bull; IDPekerjaanBank must be filled <BR/>")
                errorLine.AppendLine(" &bull; msPekerjaanBank : Line " & DTR("NO") & "<BR/>")
            End If

            '======================== Validasi textbox :  NamaPekerjaan canot Null (#)====================================================
            If ObjectAntiNull(DTR("NamaPekerjaanBank")) = False Then
                msgError.AppendLine(" &bull; NamaPekerjaanBank must be filled <BR/>")
                errorLine.AppendLine(" &bull; msPekerjaanBank : Line " & DTR("NO") & "<BR/>")
            End If

        Catch ex As Exception
            msgError.AppendLine(" &bull;" & ex.Message & "<BR/>")
            errorLine.AppendLine(" &bull;msPekerjaanBank : Line " & DTR("NO") & "<BR/>")
        End Try
    End Sub

    Private Shared Sub ValidateMsPekerjaan(ByVal DTR As DataRow, ByVal msgError As StringBuilder, ByVal errorline As StringBuilder)
        Try

            Using checkBlocking As TList(Of MsPekerjaan_ApprovalDetail) = DataRepository.MsPekerjaan_ApprovalDetailProvider.GetPaged("IDPekerjaan=" & DTR("IDPekerjaan"), "", 0, 1, Nothing)
                If checkBlocking.Count > 0 Then
                    msgError.AppendLine(" &bull;MsPekerjaan is waiting for approval <BR/>")
                    errorline.AppendLine(" &bull;MsPekerjaan : Line " & DTR("NO") & "<BR/>")
                End If
            End Using

            '======================== Validasi textbox :  IDPekerjaan   canot Null (#)====================================================
            If ObjectAntiNull(DTR("IDPekerjaan")) = False Then
                msgError.AppendLine(" &bull; ID must be filled <BR/>")
                errorline.AppendLine(" &bull; MsPekerjaan : Line " & DTR("NO") & "<BR/>")
            End If
            If ObjectAntiNull(DTR("IDPekerjaan")) Then
                If objectNumOnly(DTR("IDPekerjaan")) = False Then
                    msgError.AppendLine(" &bull; ID must be Numeric <BR/>")
                    errorline.AppendLine(" &bull; MsPekerjaan : Line " & DTR("NO") & "<BR/>")
                End If
            End If
            '======================== Validasi textbox :  NamaPekerjaan   canot Null (#)====================================================
            If ObjectAntiNull(DTR("NamaPekerjaan")) = False Then
                msgError.AppendLine(" &bull; Nama must be filled <BR/>")
                errorline.AppendLine(" &bull; MsPekerjaan : Line " & DTR("NO") & "<BR/>")
            End If

        Catch ex As Exception
            msgError.AppendLine(" &bull;" & ex.Message & "<BR/>")
            errorline.AppendLine(" &bull;MsPekerjaan  : Line " & DTR("NO") & "<BR/>")
            LogError(ex)
        End Try
    End Sub

#End Region

#Region "Upload File"

    Public Function SaveFileImportToSave() As String
        Dim filePath As String
        Dim FileNameToGetData As String = ""
        Try
            If FileUploadKPI.PostedFile.ContentLength = 0 Then
                Return FileNameToGetData
            End If

            If Not FileUploadKPI.HasFile Then
                Return FileNameToGetData
            End If

            filePath = Path.Combine(Server.MapPath("Upload"), FileUploadKPI.FileName)

            FileNameToGetData = FileUploadKPI.FileName
            If System.IO.File.Exists(filePath) Then
                Dim bexist As Boolean = True
                Dim i As Integer = 1
                While bexist
                    filePath = Path.Combine(Server.MapPath("Upload"), FileUploadKPI.FileName.Split(CChar("."))(0) & "-" & i.ToString & "." & FileUploadKPI.FileName.Split(CChar("."))(1))
                    FileNameToGetData = FileUploadKPI.FileName.Split(CChar("."))(0) & "-" & i.ToString & "." & Me.FileUploadKPI.FileName.Split(CChar("."))(1)
                    If Not System.IO.File.Exists(filePath) Then
                        bexist = False
                        Exit While
                    End If
                    i = i + 1
                End While
            End If
            Me.FileUploadKPI.PostedFile.SaveAs(filePath)
            Session("Filepath") = filePath
            Return FileNameToGetData
        Catch
            Throw
        End Try
    End Function

    ReadOnly Property FilePath() As String
        Get
            Return Session("FilePath")
        End Get
    End Property

    Private Sub ProcessExcelFile(ByVal FullPathFileNameToExportExcel As String)

        Using stream As FileStream = File.Open(FullPathFileNameToExportExcel, FileMode.Open, FileAccess.Read)
            Using excelReader As IExcelDataReader = ExcelReaderFactory.CreateBinaryReader(stream)
                excelReader.IsFirstRowAsColumnNames = True
                DSexcelTemp = excelReader.AsDataSet()
                If DSexcelTemp.Tables("MsPekerjaan").Columns(0).ColumnName Like "*Column*" Then
                    DSexcelTemp.Tables("MsPekerjaan").Rows.Clear()
                End If

            End Using
        End Using

    End Sub


#End Region

#Region "Function..."

    Function getErrorNo(ByVal Id As String, ByVal nama As String) As String
        Dim temp As String = ""
        Try
            Dim DV As New DataView(DT_AllmsPekerjaanNCBS)
            'jika Null maka harus is null bukan kosong disini belum di handle
            DV.RowFilter = "IDPekerjaan = '" & Id & "' and NamaPekerjaan = '" & nama & "'"

            Dim DT As System.Data.DataTable = DV.ToTable
            If DT.Rows.Count > 0 Then
                temp = Safe(DT.Rows(0)("NO"))
            End If
        Catch ex As Exception
            temp = ex.Message
        End Try

        Return temp
    End Function
    ''' <summary>
    ''' Merubah excel ke Object Databale sekaligus memecahnya
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ImportExcelTempToDTproperty()
        Dim DT As New System.Data.DataTable
        'DT_MsPekerjaan = DSexcelTemp.Tables("MsPekerjaan")
        DT = DSexcelTemp.Tables("MsPekerjaan")
        Dim DV As New DataView(DT)
        DT_AllMsPekerjaan = DV.ToTable(True, "IDPekerjaan", "NamaPekerjaan", "activation")
        DT_AllmsPekerjaanNCBS = DSexcelTemp.Tables("MsPekerjaan")
        DT_AllMsPekerjaan.Columns.Add("NO")
        For Each i As DataRow In DT_AllMsPekerjaan.Rows
            Dim PK As String = Safe(i("IDPekerjaan"))
            Dim nama As String = Safe(i("NamaPekerjaan"))
            i("NO") = getErrorNo(PK, nama)
        Next
    End Sub

    Private Sub ClearThisPageSessions()
        DT_AllMsPekerjaan = Nothing
        DT_AllmsPekerjaanNCBS = Nothing
        GroupTableSuccessData = Nothing
        GroupTableAnomalyData = Nothing
        DSexcelTemp = Nothing
        SetnGetDataTableForAnomalyData = Nothing
        SetnGetDataTableForSuccessData = Nothing
    End Sub


    Function ConvertDTtomsPekerjaanNCBS(ByVal DT As System.Data.DataTable) As TList(Of msPekerjaanNCBS_ApprovalDetail)
        Dim temp As New TList(Of msPekerjaanNCBS_ApprovalDetail)
        For Each DTR As DataRow In DT.Rows
            Using msPekerjaanNCBS As New msPekerjaanNCBS_ApprovalDetail
                With msPekerjaanNCBS
                    FillOrNothing(.IDPekerjaanNCBS, DTR("IDPekerjaanBank"), True, oInt)
                    FillOrNothing(.NamaPekerjaan, DTR("NamaPekerjaanBank"), True, oString)
                    FillOrNothing(.Activation, 1, True, oBit)
                    FillOrNothing(.CreatedBy, GET_UserName, True, oString)
                    FillOrNothing(.CreatedDate, Now, True, oDate)
                    temp.Add(msPekerjaanNCBS)
                End With
            End Using

        Next

        Return temp
    End Function



    Function ConvertDTtoMsPekerjaan(ByVal DTR As DataRow) As MsPekerjaan_ApprovalDetail
        Dim temp As New MsPekerjaan_ApprovalDetail
        Using MsPekerjaan As New MsPekerjaan_ApprovalDetail()
            With MsPekerjaan
                FillOrNothing(.IDPekerjaan, DTR("IDPekerjaan"), True, oInt)
                FillOrNothing(.NamaPekerjaan, DTR("NamaPekerjaan"), True, Ovarchar)

                FillOrNothing(.Activation, DTR("Activation"), True, oBit)
                FillOrNothing(.CreatedBy, GET_UserName, True, oString)
                FillOrNothing(.CreatedDate, Now, True, oDate)
            End With
            temp = MsPekerjaan
        End Using

        Return temp
    End Function

    Function ConvertDTtoMsPekerjaan(ByVal DT As System.Data.DataTable) As TList(Of MsPekerjaan_ApprovalDetail)
        Dim temp As New TList(Of MsPekerjaan_ApprovalDetail)
        For Each DTR As DataRow In DT.Rows
            Using MsPekerjaan As New MsPekerjaan_ApprovalDetail()
                With MsPekerjaan
                    FillOrNothing(.IDPekerjaan, DTR("IDPekerjaan"), True, oInt)
                    FillOrNothing(.NamaPekerjaan, DTR("NamaPekerjaan"), True, Ovarchar)

                    FillOrNothing(.Activation, DTR("Activation"), True, oBit)
                    FillOrNothing(.CreatedBy, GET_UserName, True, oString)
                    FillOrNothing(.CreatedDate, Now, True, oDate)
                    temp.Add(MsPekerjaan)
                End With
            End Using
        Next
        Return temp
    End Function

    Private Sub ChangeMultiView(ByVal index As Integer)
        MultiViewKPIOvertimeUpload.ActiveViewIndex = index
        If index = 0 Then
            Imgbtnsave.Visible = False
            Imgbtncancel.Visible = False
        ElseIf index = 1 Then
            Imgbtncancel.Visible = True
        ElseIf index = 2 Then
            Imgbtncancel.Visible = False
            Imgbtnsave.Visible = False
        End If
    End Sub

    Private Function GenerateGroupingMsPekerjaan() As List(Of AMLBLL.MsPekerjaanBll.DT_Group_MsPekerjaan)
        Dim Ltemp As New List(Of AMLBLL.MsPekerjaanBll.DT_Group_MsPekerjaan)
        For i As Integer = 0 To DT_AllMsPekerjaan.Rows.Count - 1
            Dim MsPekerjaan As New AMLBLL.MsPekerjaanBll.DT_Group_MsPekerjaan
            With MsPekerjaan
                .MsPekerjaan = DT_AllMsPekerjaan.Rows(i)
                .L_msPekerjaanNCBS = DT_AllmsPekerjaanNCBS.Clone

                'Insert msPekerjaanNCBS
                Dim MsPekerjaan_ID As String = Safe(.MsPekerjaan("IDPekerjaan"))
                Dim MsPekerjaan_nama As String = Safe(.MsPekerjaan("NamaPekerjaan"))
                Dim MsPekerjaan_Activation As String = Safe(.MsPekerjaan("Activation"))
                If DT_AllmsPekerjaanNCBS.Rows.Count > 0 Then
                    Using DV As New DataView(DT_AllmsPekerjaanNCBS)
                        DV.RowFilter = "IDPekerjaan='" & MsPekerjaan_ID & "' and " & _
                                                  "NamaPekerjaan='" & MsPekerjaan_nama & "' and " & _
                                                  "activation='" & MsPekerjaan_Activation & "'"
                        If DV.Count > 0 Then
                            Dim temp As System.Data.DataTable = DV.ToTable
                            For Each H As DataRow In temp.Rows
                                .L_msPekerjaanNCBS.ImportRow(H)
                            Next
                        End If
                    End Using
                End If




            End With
            Ltemp.Add(MsPekerjaan)
        Next
        Return Ltemp
    End Function

    Public Sub BindGrid()
        lblValueFailedData.Text = SetnGetDataTableForAnomalyData.Rows.Count
        GridAnomalyList.DataSource = SetnGetDataTableForAnomalyData
        GridAnomalyList.DataBind()
        lblValueSuccessData.Text = SetnGetDataTableForSuccessData.Count
        GridSuccessList.DataSource = SetnGetDataTableForSuccessData
        lblValueuploadeddata.Text = DT_AllMsPekerjaan.Rows.Count
        GridSuccessList.DataBind()
    End Sub

#End Region

#Region "events..."


    Protected Sub ImgBtnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imgbtncancel.Click
        ClearThisPageSessions()
        ChangeMultiView(0)
        LinkButtonExportExcel.Visible = True
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Try
            If IsPostBack Then
                Return
            End If

            ClearThisPageSessions()
            SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
            ChangeMultiView(0)
        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub ImgBtnAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAdd.Click
        ChangeMultiView(0)
    End Sub

    Protected Sub GridAnomalyList_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles GridAnomalyList.PageIndexChanged
        GridAnomalyList.CurrentPageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub GridSuccessList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridSuccessList.ItemDataBound
        Try
            Dim CollCount As Integer = e.Item.Cells.Count - 1
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then

                'Show mapping item
                Dim StringMaping As New StringBuilder
                Dim DV As New DataView(DT_AllmsPekerjaanNCBS)
                Dim DT As New System.Data.DataTable
                DV.RowFilter = "IDPekerjaan='" & e.Item.Cells(0).Text & "' and " & "NamaPekerjaan='" & e.Item.Cells(1).Text & "'"
                DT = DV.ToTable
                If DT.Rows.Count > 0 Then
                    Dim countField As Integer = 7
                    Dim lengthField As Integer = 0
                    For Each idx As DataRow In DT.Rows
                        With idx
                            If lengthField > countField Then
                                StringMaping.AppendLine(idx("NamaPekerjaanBank") & "(" & idx("IDPekerjaanBank") & ");")
                                lengthField = 0
                            Else
                                StringMaping.AppendLine(idx("NamaPekerjaanBank") & "(" & idx("IDPekerjaanBank") & ");")
                                lengthField += 1
                            End If

                        End With
                    Next
                Else
                    StringMaping.Append("-")
                End If
                e.Item.Cells(CollCount).Text = StringMaping.ToString

            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub GridSuccessList_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles GridSuccessList.PageIndexChanged
        Dim GridTarget As DataGrid = CType(source, DataGrid)
        Try
            GridSuccessList.CurrentPageIndex = e.NewPageIndex
            BindGrid()
        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgSaveSuccessData_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imgbtnsave.Click
        Try
            'File belum diupload
            If DT_AllMsPekerjaan Is Nothing Then
                Throw New Exception("Load data failed")
            End If

            'Save MsPekerjaan Approval (Header)
            Dim KeyApp As String
            Using Aproval As New MsPekerjaan_Approval()
                With Aproval
                    .RequestedBy = GET_PKUserID
                    .RequestedDate = Date.Now
                    .FK_MsMode_Id = 2 'Edit
                    .IsUpload = True
                End With
                DataRepository.MsPekerjaan_ApprovalProvider.Save(Aproval)
                'ambil key approval
                KeyApp = Aproval.PK_MsPekerjaan_Approval_Id
            End Using

            For k As Integer = 0 To GroupTableSuccessData.Count - 1

                Dim MsPekerjaan As GrouP_MsPekerjaan_success = GroupTableSuccessData(k)


                With MsPekerjaan
                    'save MsPekerjaan approval detil
                    .MsPekerjaan.FK_MsPekerjaan_Approval_Id = KeyApp
                    DataRepository.MsPekerjaan_ApprovalDetailProvider.Save(.MsPekerjaan)
                    'ambil key MsPekerjaan approval detil
                    Dim IdMsPekerjaan As String = .MsPekerjaan.IDPekerjaan
                    Dim L_MappingMsPekerjaanNCBSPPATK_Approval_Detail As New TList(Of MappingMsPekerjaanNCBSPPATK_Approval_Detail)
                    'save mapping dan NCBS
                    For Each OmsPekerjaanNCBS_ApprovalDetail As MsPekerjaanNCBS_ApprovalDetail In .L_msPekerjaanNCBS
                        OmsPekerjaanNCBS_ApprovalDetail.FK_msPekerjaanNCBS_Approval_Id = KeyApp
                        Using OMappingMsPekerjaanNCBSPPATK_Approval_Detail As New MappingMsPekerjaanNCBSPPATK_Approval_Detail
                            With OMappingMsPekerjaanNCBSPPATK_Approval_Detail
                                FillOrNothing(.IdPekerjaanNCBS, OmsPekerjaanNCBS_ApprovalDetail.IDPekerjaanNCBS, True, oInt)
                                FillOrNothing(.IdPekerjaan, IdMsPekerjaan, True, oInt)
                                FillOrNothing(.PK_MsPekerjaan_approval_id, KeyApp, True, oInt)
                                FillOrNothing(.Nama, OmsPekerjaanNCBS_ApprovalDetail.NamaPekerjaan, True, oString)
                            End With
                            L_MappingMsPekerjaanNCBSPPATK_Approval_Detail.Add(OMappingMsPekerjaanNCBSPPATK_Approval_Detail)
                        End Using
                    Next
                    'save maping dan NCBS
                    DataRepository.MappingMsPekerjaanNCBSPPATK_Approval_DetailProvider.Save(L_MappingMsPekerjaanNCBSPPATK_Approval_Detail)
                    DataRepository.MsPekerjaanNCBS_ApprovalDetailProvider.Save(.L_msPekerjaanNCBS)

                End With
            Next

            LblConfirmation.Text = "MsPekerjaan data has edited (" & SetnGetDataTableForSuccessData.Count & " Rows / MsPekerjaan)"
            ChangeMultiView(2)

        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgUpload_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgUpload.Click
        Dim CombinedFileName As String
        GroupTableSuccessData = New List(Of GrouP_MsPekerjaan_success)
        GroupTableAnomalyData = New List(Of AMLBLL.MsPekerjaanBll.DT_Group_MsPekerjaan)
        Try
            'validasi File
            If FileUploadKPI.FileName = "" Then
                Throw New Exception("Data required")
            End If

            If Path.GetExtension(FileUploadKPI.FileName) <> ".xls" Then
                Throw New Exception("Invalid File Upload Extension")
            End If

            CombinedFileName = SaveFileImportToSave()
            If String.Compare(CombinedFileName, "", False) <> 0 Then
                Try
                    ProcessExcelFile(FilePath)
                Catch ex As Exception
                    Throw New Exception("Invalid File Format")
                End Try
            Else
                Throw New Exception("Invalid File Format")
            End If

            'Import from Excell to property Global
            ImportExcelTempToDTproperty()

            Dim LG_MsPekerjaan As List(Of AMLBLL.MsPekerjaanBll.DT_Group_MsPekerjaan) = GenerateGroupingMsPekerjaan()
            'tambahkan kolom untuk tampilan di grid anomaly
            DT_AllMsPekerjaan.Columns.Add("Description")
            DT_AllMsPekerjaan.Columns.Add("errorline")

            'Membuat tempory untuk yang anomali dan yang sukses
            Dim DTnormal As System.Data.DataTable = DT_AllMsPekerjaan.Clone
            Dim DTAnomaly As System.Data.DataTable = DT_AllMsPekerjaan.Clone

            'proses pemilahan data anomaly dan data sukses melalui validasi 
            For i As Integer = 0 To LG_MsPekerjaan.Count - 1
                Dim Lerror As List(Of String) = ValidateInput(LG_MsPekerjaan(i))
                Dim MSG As String = Lerror(0)
                Dim LINE As String = Lerror(1)
                If MSG = "" Then
                    DTnormal.ImportRow(LG_MsPekerjaan(i).MsPekerjaan)
                    Dim GroupMsPekerjaan As New GrouP_MsPekerjaan_success
                    With GroupMsPekerjaan
                        'konversi dari datatable ke Object Nettier
                        .MsPekerjaan = ConvertDTtoMsPekerjaan(LG_MsPekerjaan(i).MsPekerjaan)
                        .L_msPekerjaanNCBS = ConvertDTtomsPekerjaanNCBS(LG_MsPekerjaan(i).L_msPekerjaanNCBS)
                    End With
                    'masukan ke group sukses
                    GroupTableSuccessData.Add(GroupMsPekerjaan)
                Else
                    DT_AllMsPekerjaan.Rows(i)("Description") = MSG
                    DT_AllMsPekerjaan.Rows(i)("errorline") = LINE
                    DTAnomaly.ImportRow(DT_AllMsPekerjaan.Rows(i))
                    GroupTableAnomalyData.Add(LG_MsPekerjaan(i))
                End If

            Next

            SetnGetDataTableForSuccessData = ConvertDTtoMsPekerjaan(DTnormal)
            SetnGetDataTableForAnomalyData = DTAnomaly

            If DTAnomaly.Rows.Count > 0 Then
                Imgbtnsave.Visible = False
                Imgbtncancel.Visible = True
            Else
                Imgbtnsave.Visible = True
                Imgbtncancel.Visible = True
            End If

            BindGrid()
            If GridAnomalyList.Items.Count < 1 Then
                LinkButtonExportExcel.Visible = False
            Else
                LinkButtonExportExcel.Visible = True
            End If
            ChangeMultiView(1)
        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

#End Region

    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Dim Exp As New AMLBLL.MsPekerjaanBll
            Dim FolderPath As String = Server.MapPath("FolderTemplate")

            Dim buffer As Byte() = Exp.ExportDTtoExcel(GroupTableAnomalyData, FolderPath)
            Response.AddHeader("Content-type", "application/vnd.ms-excel")
            Response.AddHeader("Content-Disposition", "attachment; filename=MsPekerjaanUpload.xls")
            Response.BinaryWrite(buffer)
            Response.Flush()
            Response.End()

        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

        ' Verifies that the control is rendered 

    End Sub

    Protected Sub GridSuccessList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridSuccessList.SelectedIndexChanged

    End Sub
End Class
