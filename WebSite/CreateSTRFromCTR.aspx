﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="CreateSTRFromCTR.aspx.vb" Inherits="CreateSTRFromCTR" %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
    <table id="title"  border="2" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr>

            <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>New Case
                </strong>
            </td>
        </tr>
    </table>
    <TABLE id="SearchBar" runat="server" borderColor="#ffffff" cellSpacing="0" cellPadding="0" width="99%" border="1">
        <TR>
            <TD align="left" bgColor="#eeeeee"><IMG height="15" src="images/arrow.gif" width="15"></TD>
		    <TD vAlign="top" width="100%" >
		        <TABLE  borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd" border="2">
						<TR>
							<TD class="Regtext" noWrap bgcolor="White">Account Owner :
							</TD>
							<TD vAlign="middle" noWrap width="100%"  bgcolor="White">
							<ajax:ajaxpanel id="AjaxPanel14" runat="server">
                                <asp:dropdownlist id="ListAccountOwner" tabIndex="1" runat="server" Width="400px" CssClass="searcheditcbo" AutoPostBack="true" ></asp:dropdownlist>&nbsp; 
                            </ajax:ajaxpanel>
                                <ajax:AjaxPanel ID="AjaxPanel2" runat="server">
                                    <asp:dropdownlist id="AccountOwnerDropdownlist" tabIndex="1" runat="server" Width="400px" CssClass="searcheditcbo" AutoPostBack="true" >
                                    </asp:DropDownList></ajax:AjaxPanel>&nbsp;
                            </TD>
						</TR>
                        	<TR>
							<TD class="Regtext" noWrap bgcolor="White">VIP Code :</TD>
							<TD vAlign="middle" noWrap width="100%"  bgcolor="White">
							<ajax:ajaxpanel id="AjaxPanel17" runat="server">
                                <asp:dropdownlist id="CboVIPCode" tabIndex="1" runat="server" Width="400px" 
                                    CssClass="searcheditcbo" AutoPostBack="true" ></asp:dropdownlist>&nbsp; 
                            </ajax:ajaxpanel>
                            </TD>
						</TR>
						<TR>
							<TD class="Regtext" noWrap bgcolor="White">Insider Code :</TD>
							<TD vAlign="middle" noWrap width="100%"  bgcolor="White">
							<ajax:ajaxpanel id="AjaxPanel18" runat="server">
                                <asp:dropdownlist id="CboInsidercode" tabIndex="1" runat="server" Width="400px" 
                                    CssClass="searcheditcbo" AutoPostBack="true" ></asp:dropdownlist>&nbsp; 
                            </ajax:ajaxpanel>
                            </TD>
						</TR>
						<TR>
							<TD class="Regtext" noWrap bgcolor="White">Segment :</TD>
							<TD vAlign="middle" noWrap width="100%"  bgcolor="White">
							<ajax:ajaxpanel id="AjaxPanel19" runat="server">
                                <asp:dropdownlist id="CboSegment" tabIndex="1" runat="server" Width="400px" 
                                    CssClass="searcheditcbo" AutoPostBack="true" ></asp:dropdownlist>&nbsp; 
                            </ajax:ajaxpanel>
                            </TD>
						</TR>
						<TR>
							<TD class="Regtext" noWrap bgcolor="White">SBU :</TD>
							<TD vAlign="middle" noWrap width="100%"  bgcolor="White">
							<ajax:ajaxpanel id="AjaxPanel20" runat="server">
                                <asp:dropdownlist id="cbosbu" tabIndex="1" runat="server" Width="400px" 
                                    CssClass="searcheditcbo" AutoPostBack="true" ></asp:dropdownlist>&nbsp; 
                            </ajax:ajaxpanel>
                            </TD>
						</TR>
						<TR>
							<TD class="Regtext" noWrap bgcolor="White">SUB SBU :</TD>
							<TD vAlign="middle" noWrap width="100%"  bgcolor="White">
							<ajax:ajaxpanel id="AjaxPanel23" runat="server">
                                <asp:dropdownlist id="cbosubsbu" tabIndex="1" runat="server" Width="400px" 
                                    CssClass="searcheditcbo" AutoPostBack="true" ></asp:dropdownlist>&nbsp; 
                            </ajax:ajaxpanel>
                            </TD>
						</TR>
						<TR>
							<TD class="Regtext" noWrap bgcolor="White">RM :</TD>
							<TD vAlign="middle" noWrap width="100%"  bgcolor="White">
							<ajax:ajaxpanel id="AjaxPanel22" runat="server">
                                <asp:dropdownlist id="CboRM" tabIndex="1" runat="server" Width="400px" 
                                    CssClass="searcheditcbo" AutoPostBack="true" ></asp:dropdownlist>&nbsp; 
                            </ajax:ajaxpanel>
                            </TD>
						</TR>
						<TR>
							<TD class="Regtext" noWrap bgcolor="White">PIC :
												</TD>
							<TD vAlign="middle" noWrap width="100%"  bgcolor="White">
							<ajax:ajaxpanel id="AjaxPanel16" runat="server">
                                <asp:Label ID="LabelPIC" runat="server" Text=""></asp:Label>&nbsp;
                            </ajax:ajaxpanel>
                            </TD>
						</TR>
						<TR>
							<TD class="Regtext" noWrap bgcolor="White">Description :
							</TD>
							<TD vAlign="middle" noWrap width="100%"  bgcolor="White">
							<ajax:ajaxpanel id="AjaxPanel1" runat="server">
                                <asp:textbox id="TextDescription" tabIndex="2" runat="server" CssClass="searcheditbox" TextMode="MultiLine" MaxLength="255" Width="300px" Height="50px"></asp:textbox>
                            </ajax:ajaxpanel>
                            </TD>
						</TR>
				</TABLE><ajax:ajaxpanel id="AjaxPanel3" runat="server">
                    <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></ajax:ajaxpanel>
            </TD>
	    </TR>
	</TABLE>
	<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="99%" bgColor="#dddddd" border="2">
		<tr>
		    <td bgcolor="White">
                <asp:Label ID="Label1" runat="server" Text="Related Transaction" Font-Bold="True" Font-Size="Small"></asp:Label>		    
		    </td>
		</tr>
		<tr>
			<td bgColor="#ffffff">
                <ajax:ajaxpanel id="AjaxPanel4" runat="server">
<%--					<asp:datagrid id="GridMSUserView" runat="server" AutoGenerateColumns="False"
						Font-Size="XX-Small" BackColor="White" CellPadding="4" BorderWidth="1px" BorderStyle="None"
						AllowPaging="True" width="100%" GridLines="Vertical" AllowSorting="True" BorderColor="#DEDFDE"
						ForeColor="Black">
						<FooterStyle BackColor="#CCCC99"></FooterStyle>
						<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#CE5D5A"></SelectedItemStyle>
						<AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
						<ItemStyle BackColor="#F7F7DE"></ItemStyle>
						<HeaderStyle Font-Size="XX-Small" Font-Bold="True" ForeColor="White" BackColor="#6B696B"></HeaderStyle>
						<Columns>
                            <asp:BoundColumn DataField="TransactionDetailId" HeaderText="TransactionDetailId" Visible="False" >
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="TransactionDate" HeaderText="Transaction Date" SortExpression="TransactionDate desc" >
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="TransactionChannelType" HeaderText="Transaction Channel Type"
                                SortExpression="TransactionChannelType desc"></asp:BoundColumn>
                            <asp:BoundColumn DataField="CountryCode" HeaderText="Country Code" SortExpression="CountryCode desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="BankCode" HeaderText="Bank Code" SortExpression="BankCode desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="BankName" HeaderText="Bank Name" SortExpression="BankName desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="AccountOwnerId" HeaderText="AccountOwnerId" SortExpression="AccountOwnerId desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="AccountOwnerName" HeaderText="AccountOwnerName" SortExpression="AccountOwnerName desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CIFNo" HeaderText="CIF No" SortExpression="CIFNo desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="AccountNo" HeaderText="Account Number" SortExpression="AccountNo Desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="AccountName" HeaderText="Account Name" SortExpression="AccountName desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="AuxiliaryTransactionCode" HeaderText="Auxiliary Transaction Code" SortExpression="AuxiliaryTransactionCode desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="TransactionAmount" HeaderText="Transaction Amount" SortExpression="TransactionAmount desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CurrencyType" HeaderText="CurrencyType" SortExpression="CurrencyType desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="DebitOrCredit" HeaderText="Debit / Credit" SortExpression="DebitOrCredit desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="MemoRemark" HeaderText="Memo Remark" SortExpression="MemoRemark desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="TransactionRemark" HeaderText="Transaction Remark" SortExpression="TransactionRemark desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="TransactionExchangeRate" HeaderText="TransactionExchangeRate"
                                SortExpression="TransactionExchangeRate desc"></asp:BoundColumn>
                            <asp:BoundColumn DataField="TransactionLocalEquivalent" HeaderText="TransactionLocalEquivalent"
                                SortExpression="TransactionLocalEquivalent desc"></asp:BoundColumn>
                            <asp:BoundColumn DataField="TransactionTicketNo" HeaderText="Transaction Ticket #" SortExpression="TransactionTicketNo desc">
                            </asp:BoundColumn>
						</Columns>
						<PagerStyle Visible="False" HorizontalAlign="Right" ForeColor="Black" BackColor="#F7F7DE" Mode="NumericPages"></PagerStyle>
					</asp:datagrid>--%>
						<asp:datagrid id="GridMSUserView" runat="server" AutoGenerateColumns="False"
						Font-Size="XX-Small" BackColor="White" CellPadding="4" BorderWidth="1px" BorderStyle="None"
						AllowPaging="True" width="100%" GridLines="Vertical" AllowSorting="True" BorderColor="#DEDFDE"
						ForeColor="Black">
						<FooterStyle BackColor="#CCCC99"></FooterStyle>
						<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#CE5D5A"></SelectedItemStyle>
						<AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
						<ItemStyle BackColor="#F7F7DE"></ItemStyle>
						<HeaderStyle Font-Size="XX-Small" Font-Bold="True" ForeColor="White" BackColor="#6B696B"></HeaderStyle>
						<Columns>
                            <asp:BoundColumn DataField="TransactionDetailId" HeaderText="TransactionDetailId" Visible="False" >
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="TransactionDate" HeaderText="Transaction Date" SortExpression="TransactionDate desc" DataFormatString="{0:dd-MMM-yyyy}">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="AccountOwnerId" HeaderText="AccountOwnerId" SortExpression="CTRTransaction.AccountOwnerId desc" Visible="false">
                            </asp:BoundColumn>                            
                            <asp:BoundColumn DataField="AccountOwner" HeaderText="AccountOwner" SortExpression="(CAST(CTRTransaction.AccountOwnerId AS VARCHAR)  + ' - ' + AccountOwner.AccountOwnerName) desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="TransactionChannelTypeName" HeaderText="TransactionChannelType" SortExpression="TransactionChannelTypeName desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Country" HeaderText="Country" SortExpression="CTRTransaction.CountryCode + ' - ' + ISNULL(JHCOUN.JHCNAM,'') desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="BankCode" HeaderText="BankCode" SortExpression="BankCode desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="BankName" HeaderText="Bank Name" SortExpression="BankName desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CIFNo" HeaderText="CIF No" SortExpression="CIFNo desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="AccountNo" HeaderText="Account Number" SortExpression="AccountNo Desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="AccountName" HeaderText="Account Name" SortExpression="AccountName desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="AuxiliaryTransactionCode" HeaderText="AuxiliaryTransactionCode"
                                SortExpression="CTRTransaction.AuxiliaryTransactionCode + ' - ' + vw_AuxTransactionCode.TransactionDescription desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="TransactionAmount" HeaderText="Transaction Amount" SortExpression="TransactionAmount desc" DataFormatString="{0:#,#.00}">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CurrencyType" HeaderText="CurrencyType" SortExpression="CurrencyType desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="DebitOrCredit" HeaderText="Debit / Credit" SortExpression="DebitOrCredit desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="MemoRemark" HeaderText="Memo Remark" SortExpression="MemoRemark desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="TransactionRemark" HeaderText="Transaction Remark" SortExpression="TransactionRemark desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="TransactionExchangeRate" DataFormatString="{0:#,#.00}"
                                HeaderText="TransactionExchangeRate" SortExpression="TransactionExchangeRate desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="TransactionLocalEquivalent" DataFormatString="{0:#,#.00}"
                                HeaderText="TransactionLocalEquivalent" SortExpression="TransactionLocalEquivalent desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="TransactionTicketNo" HeaderText="Transaction Ticket #" SortExpression="TransactionTicketNo desc">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="UserId" HeaderText="UserId" SortExpression="UserId desc">
                            </asp:BoundColumn>
						</Columns>
						<PagerStyle Visible="False" HorizontalAlign="Right" ForeColor="Black" BackColor="#F7F7DE" Mode="NumericPages"></PagerStyle>
					</asp:datagrid>				
				</ajax:ajaxpanel>
			</td>
		</tr>
		<tr>
		    <td style="background-color:#ffffff"><table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td nowrap>
			<ajax:ajaxpanel id="AjaxPanel5" runat="server">
                <asp:imagebutton id="ImageButtonSave" tabIndex="3" runat="server" SkinID="SaveButton"></asp:imagebutton>
			  &nbsp; 
			</ajax:ajaxpanel> 
			</td>
			 <td width="99%">
                <asp:imagebutton id="ImageButtonCancel" tabIndex="3" runat="server" SkinID="CancelButton"></asp:imagebutton>			    
		    </td>
			<td align="right" nowrap>
			&nbsp;&nbsp;</td>
		</tr>
      </table></td>
		</tr>		
	</table>
</asp:Content>