<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="LoginParameter.aspx.vb" Inherits="LoginParameter" title="Login Parameter" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
    <table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
	    border="2">
        <tr>
            <td bgcolor="#ffffff" colspan="5" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Login Parameter
                    <hr />
                </strong>
                <asp:Label ID="LblSucces"  CssClass="validationok" width="94%" runat="server"  Visible="False"></asp:Label></td>
        </tr>
        <tr>
        <td colspan="5" bgColor="#ffffff" style="height: 19px">Password</td>
        </tr>
	    <tr class="formText">
            <td bgcolor="#ffffff">
                <asp:CheckBox ID="CbPassExpiration" runat="server" AutoPostBack="True"
					meta:resourcekey="CheckBoxResource1" />
            </td>
		    <td bgColor="#ffffff" style="width: 25%">Password Expiration Period</td>
		    <td bgColor="#ffffff" style="width: 11px">:</td>
		    <td width="80%" bgColor="#ffffff"><asp:textbox id="TextPasswordPeriod" 
                    runat="server" CssClass="textBox" MaxLength="20" Width="50px" Enabled="False"></asp:textbox>&nbsp;Day(s)</td>
	    </tr>
	    <tr class="formText">
            <td bgcolor="#ffffff">
                <asp:CheckBox ID="CbMinimumLenght" runat="server" AutoPostBack="True"
					meta:resourcekey="CheckBoxResource2" />
            </td>
		    <td bgcolor="#ffffff">
                Minimum Password Length</td>
		    <td bgcolor="#ffffff" style="width: 11px">:</td>
		    <td bgcolor="#ffffff">
			    <asp:TextBox id="TextPasswordMin" runat="server" Width="50px" 
                    CssClass="textBox" Enabled="False"></asp:TextBox>&nbsp;Char(s)</td>
	    </tr>
	    <tr class="formText">
            <td bgcolor="#ffffff">
                <asp:CheckBox ID="CbPassRecycle" runat="server" AutoPostBack="True"
					meta:resourcekey="CheckBoxResource3" />
            </td>
		    <td bgcolor="#ffffff" style="width: 743px;">Password Recycle</td>
		    <td bgcolor="#ffffff" style="width: 11px;">:</td>
		    <td bgcolor="#ffffff">
			    <asp:TextBox id="TextPasswordRecycle" runat="server" Width="50px" 
                    CssClass="textBox" Enabled="False"></asp:TextBox>&nbsp;Times</td>
	    </tr>
        <tr class="formText">
            <td bgcolor="#ffffff">
                <asp:CheckBox ID="CbPasswordChar" runat="server" AutoPostBack="True"
					meta:resourcekey="CheckBoxResource4" />
            </td>
            <td bgcolor="#ffffff" style="width: 743px;" colspan=3> Password Char</td>
        </tr>
            <tr class="formText">
            <td bgcolor="#ffffff">
                <asp:CheckBox ID="CbPassCombination" runat="server" AutoPostBack="True"
					meta:resourcekey="CheckBoxResource5" />
            </td>
            <td bgcolor="#ffffff" style="width: 743px;" colspan=3> Password Combination</td>
        </tr>
        <tr class="formText">
            <td bgcolor="#ffffff">
                <asp:CheckBox ID="CbFirstLogin" runat="server" AutoPostBack="True"
					meta:resourcekey="CheckBoxResource6" />
            </td>
            <td bgcolor="#ffffff" style="width: 743px;" colspan=3>Change Password on First Login</td>
        </tr>
        <tr>
            <td colspan="5" bgColor="#ffffff" style="height: 19px"> Account
            </td>
        </tr>
	    <tr class="formText">
            <td bgcolor="#ffffff">
                <asp:CheckBox ID="CbLockUnused" runat="server" AutoPostBack="True"
					meta:resourcekey="CheckBoxResource7" />
            </td>
		    <td bgcolor="#ffffff" style="width: 743px">Lock Unused account after</td>
		    <td bgcolor="#ffffff" style="width: 11px">:</td>
		    <td bgcolor="#ffffff">
			    <asp:TextBox id="TextBoxLockUnused" runat="server" Width="50px" 
                    CssClass="textBox" Enabled="False"></asp:TextBox>&nbsp;Day(s)</td>
	    </tr>
        <tr class="formText">
            <td bgcolor="#ffffff">
                <asp:CheckBox ID="CbAccountLock" runat="server" AutoPostBack="True"
					meta:resourcekey="CheckBoxResource8" />
            </td>
            <td bgcolor="#ffffff" style="width: 743px;">
                Account Lockout</td>
            <td bgcolor="#ffffff" style="height: 32px; width: 11px;">
                :</td>
            <td bgcolor="#ffffff" style="height: 32px">
                <asp:TextBox ID="TextBoxAccountLockout" runat="server" CssClass="textBox" 
                    Width="50px" Enabled="False"></asp:TextBox>
                Times</td>
        </tr>
	    <tr class="formText" bgColor="#dddddd" height="30">
		    <td style="width: 273px"><IMG height="15" src="images/arrow.gif" width="15"></td>
		    <td colSpan="4">
			    <table cellSpacing="0" cellPadding="3" border="0">
				    <tr>
					    <td style="height: 36px"><asp:imagebutton id="ImageSave" runat="server" SkinID="SaveButton" CausesValidation="False"></asp:imagebutton></td>
					    <td style="height: 36px"><asp:imagebutton id="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton"></asp:imagebutton></td>
				    </tr>
			    </table>
                <asp:CustomValidator ID="cvalPageErr" runat="server" Display="None"></asp:CustomValidator></td>
	    </tr>
    </table>
	<script language="javascript">
//document.getElementById('<%=GetFocusID %>').focus();
	</script>
</asp:Content>