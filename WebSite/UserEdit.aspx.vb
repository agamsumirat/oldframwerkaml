Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Partial Class UserEdit
    Inherits Parent

    ''' <summary>
    ''' get user id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetPkUserId() As String
        Get
            Return Me.Request.Params("PkUserID")
        End Get
    End Property

    ''' <summary>
    ''' get id for focues
    ''' </summary>
    ''' <value></value>
    ''' <returns>element control id</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property GetFocusID() As String
        Get
            Return Me.TextUserId.ClientID
        End Get
    End Property

    ''' <summary>
    ''' cancel handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "UserView.aspx"

        Me.Response.Redirect("UserView.aspx", False)
    End Sub

#Region " Insert User dan Audit trail"
    Private Sub UpdateUserBySU(ByRef oSQLTrans As SqlTransaction)
        Try
            Dim UserID As String = Trim(Me.TextUserId.Text)
            Dim UserName As String = Me.TextUserName.Text

            Dim Salt As String ' = Guid.NewGuid.ToString
            Dim UserPassword As String

            'Periksa apakah user melakukan perubahan pada TextBoxPassword atau tidak
            'Bila tidak ada perubahan maka gunakan nilai password lama yg telah dienkripsi
            If Trim(Me.TextPassword.Text).Length = 0 Then
                Salt = Session("UserPasswordSalt_Old")
                UserPassword = Session("UserPassword_Old")
            Else 'Bila user melakukan perubahan pada TextBoxPassword maka lakukan enkripsi pada entry TextBoxPassword tsb
                If Me.TextPassword.Text.Length < Session("MinimumPasswordLength") Then
                    Throw New Exception("Password must be at least " & Session("MinimumPasswordLength") & " characters long.")
                End If

                Salt = Session("UserPasswordSalt_Old")
                UserPassword = Sahassa.AML.Commonly.Encrypt(Me.TextPassword.Text, Salt)

                Dim PasswordRecycleCount As Int32 = 3
                Using AccessLoginParameter As New AMLDAL.AMLDataSetTableAdapters.LoginParameterTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessLoginParameter, oSQLTrans)
                    Using LoginParamterTable As AMLDAL.AMLDataSet.LoginParameterDataTable = AccessLoginParameter.GetData
                        'Sahassa.AML.TableAdapterHelper.SetTransaction(LoginParamterTable, oSQLTrans)

                        If LoginParamterTable.Rows.Count <> 0 Then
                            Dim LoginParameterRow As AMLDAL.AMLDataSet.LoginParameterRow = LoginParamterTable.Rows(0)
                            PasswordRecycleCount = LoginParameterRow.PasswordRecycleCount
                        End If
                    End Using
                End Using
                If Not Me.CheckIfPasswordIsOK(Me.TextUserId.Text, UserPassword, PasswordRecycleCount) Then
                    Throw New Exception("Cannot change password for the following User : " & Me.TextUserId.Text & " because the new password violates the Password Recycle Count rule.")
                End If
            End If

            Dim EmailAddr As String = Me.TextboxEmailAddr.Text
            Dim Hp As String = Me.TextboxMobilePhone.Text
            Dim GroupID As Int32 = Me.DropdownlistGroup.SelectedValue
            Dim TellerID As String = Me.TextboxTellerID.Text

            Dim UserLastChangedPassword_Old As DateTime = Session("UserLastChangedPassword_Old")
            Dim UserIPAddress_Old As String = Session("UserIPAddress_Old")
            Dim UserInUsed_Old As Boolean = Session("UserInUsed_Old")
            Dim UserIsDisabled_Old As Boolean = Session("UserIsDisabled_Old")
            Dim UserLastLogin_Old As DateTime = Session("UserLastLogin_Old")
            Dim UserWorkingUnitMappingStatus_Old As Boolean = Session("UserWorkingUnitMappingStatus_Old")
            Dim UserCreatedDate_Old As DateTime = Session("UserCreatedDate_Old")
            Dim isCreatedBySU_Old As Boolean = Session("isCreatedBySU_Old")


            Using AccessUser As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessUser, oSQLTrans)
                AccessUser.UpdateUser(Me.GetPkUserId, UserID, UserName, EmailAddr, Hp, UserPassword, Salt, UserLastChangedPassword_Old, UserIPAddress_Old, UserInUsed_Old, UserCreatedDate_Old, UserIsDisabled_Old, GroupID, UserLastLogin_Old, UserWorkingUnitMappingStatus_Old, isCreatedBySU_Old, TellerID)
            End Using
        Catch
            Throw
        End Try
    End Sub

    Private Sub InsertAuditTrail(ByRef oSQLTrans As SqlTransaction)
        Try
            Dim UserID As String = Trim(Me.TextUserId.Text)
            Dim UserName As String = Me.TextUserName.Text
            Dim Salt As String = Guid.NewGuid.ToString
            Dim UserPassword As String = Sahassa.AML.Commonly.Encrypt(Me.TextPassword.Text, Salt)
            Dim EmailAddr As String = Me.TextboxEmailAddr.Text
            Dim Hp As String = Me.TextboxMobilePhone.Text
            Dim GroupID As Int32 = Me.DropdownlistGroup.SelectedValue
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(14)
            'Using TransScope As New Transactions.TransactionScope
            Using AccessUser As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessUser, Data.IsolationLevel.ReadUncommitted)
                Using dt As Data.DataTable = AccessUser.GetData()
                    Dim RowData() As AMLDAL.AMLDataSet.UserRow = dt.Select("pkUserId='" & CInt(Me.GetPkUserId) & "'")
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)

                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserID", "Edit", RowData(0).UserID, UserID, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserName", "Edit", RowData(0).UserName, UserName, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserEmailAddress", "Edit", RowData(0).UserEmailAddress, EmailAddr, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserMobilePhone", "Edit", RowData(0).UserMobilePhone, Hp, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserPassword", "Edit", RowData(0).UserPassword, UserPassword, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserPasswordSalt", "Edit", RowData(0).UserPasswordSalt, Salt, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserLastChangedPassword", "Edit", RowData(0).UserLastChangedPassword.ToString("dd-MMMM-yyyy HH:mm"), "", "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserIPAddress", "Edit", RowData(0).UserIPAddress, "", "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserInUsed", "Edit", RowData(0).UserInUsed, "False", "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserCreatedDate", "Edit", RowData(0).UserCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), RowData(0).UserCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserIsDisabled", "Edit", RowData(0).UserIsDisabled, "False", "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserGroupId", "Edit", RowData(0).UserGroupId, RowData(0).UserGroupId, "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "UserLastLogin", "Edit", RowData(0).UserLastLogin.ToString("dd-MMMM-yyyy HH:mm"), "", "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "isCreatedBySU", "Edit", RowData(0).isCreatedBySU, "True", "Accepted")
                        AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User", "TellerID", "Edit", RowData(0).TellerID, "True", "Accepted")
                    End Using
                End Using
            End Using
            'End Using
        Catch
            Throw
        End Try
    End Sub
#End Region

    Private Function CheckIfPasswordIsOK(ByVal UserId As String, ByVal EncryptedPassword As String, ByVal PasswordRecycleCount As Int32) As Boolean
        Try
            Dim connectionString As String = WebConfigurationManager.ConnectionStrings("AMLConnectionString").ConnectionString
            Dim con As New SqlConnection(connectionString)
            Dim QueryString As String
            'QueryString = String.Format("select count(*) from (select top {0} HistoryPasswordUserPassword from HistoryPassword order by HistoryPasswordEntryDate desc)a where HistoryPasswordUserPassword = '{1}'", PasswordRecycleCount, EncryptedPassword)
            QueryString = String.Format("select count(*) from (select top {0} HistoryPasswordUserPassword from HistoryPassword where HistoryPasswordUserId='{1}' order by HistoryPasswordEntryDate desc)a where HistoryPasswordUserPassword = '{2}'", PasswordRecycleCount, UserId, EncryptedPassword)


            Dim cmd As New SqlCommand()
            Dim count As Int32

            cmd.Connection = con
            cmd.CommandType = CommandType.Text
            cmd.CommandText = QueryString

            con.Open()
            count = CType(cmd.ExecuteScalar(), Int32)
            con.Close()

            If count > 0 Then
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Function


    ''' <summary>
    ''' update handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim oSQLTrans As SqlTransaction = Nothing
        Page.Validate()

        If Page.IsValid Then
            Try
                Dim UserID As String = Trim(Me.TextUserId.Text)

                'Jika User tidak melakukan perubahan UserID
                If UserID = Session("UserID_Old") Then
                    GoTo Add
                Else 'Jika User melakukan perubahan UserIDnya 
                    Dim counter As Int32
                    'Periksa apakah UserID yg baru tsb sdh ada dalam tabel User atau belum
                    Using AccessUser As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                        counter = AccessUser.CountMatchingUser(UserID)
                    End Using

                    'Counter = 0 berarti UserID tersebut belum pernah ada dalam tabel User
                    If counter = 0 Then
Add:
                        'Pemeriksaan thd UserID yg lama sdh dilakukan dalam Form User View, jd tdk perlu dilakukan lg di sini
                        'Periksa apakah UserID yg baru tsb dalam status pending approval atau tidak
                        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.User_ApprovalTableAdapter
                            counter = AccessPending.CountUserApprovalByUserID(UserID)

                            'Counter = 0 berarti UserID yg baru tsb tidak dalam status pending approval
                            If counter = 0 Then
                                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                                    'Using TransScope As New Transactions.TransactionScope
                                    Me.InsertAuditTrail(oSQLTrans)
                                    Me.UpdateUserBySU(oSQLTrans)

                                    oSQLTrans.Commit()

                                    Me.LblSuccess.Text = "Success to Insert User."
                                    Me.LblSuccess.Visible = True
                                    'End Using
                                Else
                                    'Buat variabel untuk menampung nilai-nilai baru
                                    Dim UserName As String = Me.TextUserName.Text
                                    Dim Salt As String

                                    Dim UserPassword As String

                                    'Periksa apakah user melakukan perubahan pada TextBoxPassword atau tidak
                                    'Bila tidak ada perubahan maka gunakan nilai password lama yg telah dienkripsi
                                    If Trim(Me.TextPassword.Text).Length = 0 Then
                                        Salt = Session("UserPasswordSalt_Old")
                                        UserPassword = Session("UserPassword_Old")
                                        'Me.RegularExpressionValidatorPassword.Enabled = False
                                    Else 'Bila user melakukan perubahan pada TextBoxPassword maka lakukan enkripsi pada entry TextBoxPassword tsb
                                        'Me.RegularExpressionValidatorPassword.Enabled = True

                                        If Me.TextPassword.Text.Length < Session("MinimumPasswordLength") Then
                                            Throw New Exception("Password must be at least " & Session("MinimumPasswordLength") & " characters long.")
                                        End If

                                        Salt = Session("UserPasswordSalt_Old")
                                        UserPassword = Sahassa.AML.Commonly.Encrypt(Me.TextPassword.Text, Salt)

                                        Dim PasswordRecycleCount As Int32 = 3
                                        Using AccessLoginParameter As New AMLDAL.AMLDataSetTableAdapters.LoginParameterTableAdapter
                                            Using LoginParamterTable As AMLDAL.AMLDataSet.LoginParameterDataTable = AccessLoginParameter.GetData

                                                If LoginParamterTable.Rows.Count <> 0 Then
                                                    Dim LoginParameterRow As AMLDAL.AMLDataSet.LoginParameterRow = LoginParamterTable.Rows(0)
                                                    PasswordRecycleCount = LoginParameterRow.PasswordRecycleCount
                                                End If
                                            End Using
                                        End Using
                                        If Not Me.CheckIfPasswordIsOK(Me.TextUserId.Text, UserPassword, PasswordRecycleCount) Then
                                            Throw New Exception("Cannot change password for the following User : " & Me.TextUserId.Text & " because the new password violates the Password Recycle Count rule.")
                                        End If

                                    End If

                                    Dim EmailAddr As String = Me.TextboxEmailAddr.Text
                                    Dim Hp As String = Me.TextboxMobilePhone.Text
                                    Dim Group As Int32 = Me.DropdownlistGroup.SelectedValue
                                    Dim TellerID As String = Me.TextboxTellerID.Text

                                    'Buat variabel untuk menampung nilai-nilai lama
                                    Dim PkUserId_Old As Int64 = Session("PkUserId_Old")
                                    Dim UserID_Old As String = Session("UserID_Old")
                                    Dim UserName_Old As String = Session("UserName_Old")
                                    Dim Salt_Old As String = Session("UserPasswordSalt_Old")
                                    Dim UserPassword_Old As String = Session("UserPassword_Old")
                                    Dim EmailAddr_Old As String = Session("UserEmailAddress_Old")
                                    Dim Hp_Old As String = Session("UserMobilePhone_Old")
                                    Dim Group_Old As Int32 = Session("UserGroupId_Old")
                                    Dim UserLastChangedPassword_Old As DateTime = Session("UserLastChangedPassword_Old")
                                    Dim UserIPAddress_Old As String = Session("UserIPAddress_Old")
                                    Dim UserInUsed_Old As Boolean = Session("UserInUsed_Old")
                                    Dim UserIsDisabled_Old As Boolean = Session("UserIsDisabled_Old")
                                    Dim UserLastLogin_Old As DateTime = Session("UserLastLogin_Old")
                                    Dim UserWorkingUnitMappingStatus_Old As Boolean = Session("UserWorkingUnitMappingStatus_Old")
                                    Dim UserCreatedDate_Old As DateTime = Session("UserCreatedDate_Old")
                                    Dim isCreatedBySU_Old As Boolean = Session("isCreatedBySU_Old")
                                    Dim TellerID_Old As String = Session("UserTellerID_Old")

                                    'variabel untuk menampung nilai identity dari tabel NewsPendingApprovalID
                                    Dim UserPendingApprovalID As Int64

                                    'Using TransScope As New Transactions.TransactionScope
                                    Using AccessUserPending As New AMLDAL.AMLDataSetTableAdapters.User_PendingApprovalTableAdapter
                                        oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessUserPending, IsolationLevel.ReadUncommitted)
                                        'Tambahkan ke dalam tabel User_PendingApproval dengan ModeID = 2 (Edit) 
                                        UserPendingApprovalID = AccessUserPending.InsertUser_PendingApproval(Sahassa.AML.Commonly.SessionUserId, Now, "User Edit", 2)
                                    End Using

                                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, oSQLTrans)
                                    'Tambahkan ke dalam tabel User_Approval dengan ModeID = 2 (Edit) 
                                    AccessPending.Insert(UserPendingApprovalID, 2, UserID, EmailAddr, Hp, UserPassword, Salt, Now, "", UserInUsed_Old, UserCreatedDate_Old, UserIsDisabled_Old, Group, UserLastLogin_Old, UserID_Old, EmailAddr_Old, Hp_Old, UserPassword_Old, Salt_Old, UserLastChangedPassword_Old, UserIPAddress_Old, UserInUsed_Old, UserCreatedDate_Old, UserIsDisabled_Old, Group_Old, UserLastLogin_Old, UserWorkingUnitMappingStatus_Old, UserWorkingUnitMappingStatus_Old, isCreatedBySU_Old, isCreatedBySU_Old, PkUserId_Old, UserName, PkUserId_Old, UserName_Old, TellerID, TellerID_Old)

                                    oSQLTrans.Commit()
                                    'End Using

                                    Dim MessagePendingID As Integer = 8302 'MessagePendingID 8302 = User Edit 

                                    Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & UserID_Old

                                    Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & UserID_Old, False)
                                End If
                            Else  'Counter != 0 berarti UserID tersebut dalam status pending approval
                                Throw New Exception("Cannot edit the following User: '" & UserID & "' because it is currently waiting for approval")
                            End If
                        End Using
                    Else 'Counter != 0 berarti UserID tersebut sudah ada dalam tabel User
                        Throw New Exception("Cannot change to the following User ID: '" & UserID & "' because that UserID already exists in the database.")
                    End If
                End If
            Catch ex As Exception
                If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
                Me.cvalPageError.IsValid = False
                Me.cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            Finally
                If Not oSQLTrans Is Nothing Then
                    oSQLTrans.Dispose()
                    oSQLTrans = Nothing
                End If
            End Try

        End If
    End Sub

    ''' <summary>
    ''' fill group
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillGroup()
        Using AccessGroup As New AMLDAL.AMLDataSetTableAdapters.GroupTableAdapter
            Me.DropdownlistGroup.DataSource = AccessGroup.GetData
            Me.DropdownlistGroup.DataTextField = "GroupName"
            Me.DropdownlistGroup.DataValueField = "GroupID"
            Me.DropdownlistGroup.DataBind()
        End Using

        Try
            'Hapus SuperUser Group dari webcontrol DropDownListGroup
            For Each item As ListItem In Me.DropdownlistGroup.Items
                If item.Text = "SuperUser" Then
                    Me.DropdownlistGroup.Items.Remove(item)
                End If
            Next
        Catch ex As Exception

        End Try
    End Sub

    ''' <summary>
    ''' filledit data
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillEditData()
        Using AccessUser As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
            Using TableUser As AMLDAL.AMLDataSet.UserDataTable = AccessUser.GetDataByUserID(Me.GetPkUserId)
                If TableUser.Rows.Count > 0 Then
                    Dim TableRowUser As AMLDAL.AMLDataSet.UserRow = TableUser.Rows(0)

                    Session("PkUserId_Old") = TableRowUser.pkUserID

                    Session("UserID_Old") = TableRowUser.UserID
                    Me.TextUserId.Text = Session("UserID_Old")

                    Session("UserName_Old") = TableRowUser.UserName
                    Me.TextUserName.Text = Session("UserName_Old")

                    Session("UserEmailAddress_Old") = TableRowUser.UserEmailAddress
                    Me.TextboxEmailAddr.Text = Session("UserEmailAddress_Old")

                    Session("UserMobilePhone_Old") = TableRowUser.UserMobilePhone
                    Me.TextboxMobilePhone.Text = Session("UserMobilePhone_Old")

                    Session("UserPassword_Old") = TableRowUser.UserPassword

                    Session("UserPasswordSalt_Old") = TableRowUser.UserPasswordSalt
                    Session("UserLastChangedPassword_Old") = TableRowUser.UserLastChangedPassword
                    Session("UserIPAddress_Old") = TableRowUser.UserIPAddress
                    Session("UserInUsed_Old") = CType(TableRowUser.UserInUsed, Boolean)
                    Session("UserIsDisabled_Old") = CType(TableRowUser.UserIsDisabled, Boolean)

                    Session("UserGroupId_Old") = TableRowUser.UserGroupId
                    Me.DropdownlistGroup.SelectedValue = Session("UserGroupId_Old")

                    Session("UserLastLogin_Old") = TableRowUser.UserLastLogin
                    Try
                        Session("UserWorkingUnitMappingStatus_Old") = CType(TableRowUser.UserWorkingUnitMappingStatus, Boolean)
                    Catch ex As Exception
                        Session("UserWorkingUnitMappingStatus_Old") = False
                    End Try

                    Session("UserCreatedDate_Old") = TableRowUser.UserCreatedDate
                    Session("isCreatedBySU_Old") = TableRowUser.isCreatedBySU

                    Session("UserTellerID_Old") = TableRowUser.TellerID
                    Me.TextboxTellerID.Text = Session("UserTellerID_Old")
                End If
            End Using
        End Using
    End Sub

    ''' <summary>
    ''' page load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.FillGroup()
                Me.FillEditData()

                Using AccessLoginParameter As New AMLDAL.AMLDataSetTableAdapters.LoginParameterTableAdapter
                    Dim objTable As Data.DataTable = AccessLoginParameter.GetData
                    If objTable.Rows.Count > 0 Then
                        Dim objRow As AMLDAL.AMLDataSet.LoginParameterRow = objTable.Rows(0)
                        Session("MinimumPasswordLength") = objRow.MinimumPasswordLength
                    Else
                        Session("MinimumPasswordLength") = 6
                    End If

                    Me.TextPassword.MaxLength = CInt(Session("MinimumPasswordLength")) * 2
                    Me.TextBoxRetype.MaxLength = CInt(Session("MinimumPasswordLength")) * 2
                End Using

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class
