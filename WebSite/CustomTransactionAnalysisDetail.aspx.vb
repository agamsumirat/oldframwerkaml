Imports System.Data
Imports System.Data.SqlClient

Partial Class CustomTransactionAnalysisDetail
    Inherits Parent

#Region " Property "
    Private Property SetnGetSelectedItemFilter() As ArrayList
        Get
            Return IIf(Session("CustomTransactionAnalysisDetailSelected") Is Nothing, New ArrayList, Session("CustomTransactionAnalysisDetailSelected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("CustomTransactionAnalysisDetailSelected") = value
        End Set
    End Property
    Private Property SetnGetFieldSearch() As String
        Get
            Return IIf(Session("CustomTransactionAnalysisDetailFieldSearch") Is Nothing, "", Session("CustomTransactionAnalysisDetailFieldSearch"))
        End Get
        Set(ByVal Value As String)
            Session("CustomTransactionAnalysisDetailFieldSearch") = Value
        End Set
    End Property
    Private Property SetnGetValueSearch() As String
        Get
            Return IIf(Session("CustomTransactionAnalysisDetailValueSearch") Is Nothing, "", Session("CustomTransactionAnalysisDetailValueSearch"))
        End Get
        Set(ByVal Value As String)
            Session("CustomTransactionAnalysisDetailValueSearch") = Value
        End Set
    End Property
    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("CustomTransactionAnalysisDetailSort") Is Nothing, "TransactionDetail.TransactionDetailId  asc", Session("CustomTransactionAnalysisDetailSort"))
        End Get
        Set(ByVal Value As String)
            Session("CustomTransactionAnalysisDetailSort") = Value
        End Set
    End Property
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("CustomTransactionAnalysisDetailCurrentPage") Is Nothing, 0, Session("CustomTransactionAnalysisDetailCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("CustomTransactionAnalysisDetailCurrentPage") = Value
        End Set
    End Property
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("CustomTransactionAnalysisDetailRowTotal") Is Nothing, 0, Session("CustomTransactionAnalysisDetailRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("CustomTransactionAnalysisDetailRowTotal") = Value
        End Set
    End Property
    Private Property SetnGetBindTable() As Data.DataTable
        Get
            Return IIf(Session("CustomTransactionAnalysisDetailData") Is Nothing, Sahassa.AML.Commonly.GetDatasetPaging(Me.Tables, Me.PK, Me.SetnGetSort, Me.SetnGetCurrentPage + 1, Me.PageSize, Me.Fields, Me.SetnGetValueSearch, ""), Session("CustomTransactionAnalysisDetailData"))
        End Get
        Set(ByVal value As Data.DataTable)
            Session("CustomTransactionAnalysisDetailData") = value
        End Set
    End Property

    Private ReadOnly Property Tables() As String
        Get
            Dim StrQuery As New StringBuilder
            StrQuery.Append("TransactionDetail INNER JOIN CFMAST ON TransactionDetail.CIFNo=CFMAST.CFCIF# ")
            StrQuery.Append("INNER JOIN TransactionChannelType ON TransactionDetail.TransactionChannelType = TransactionChannelType.PK_TransactionChannelType ")
            StrQuery.Append("LEFT JOIN JHCOUN ON TransactionDetail.CountryCode=JHCOUN.JHCCOC ")
            StrQuery.Append("INNER JOIN vw_AuxTransactionCode ON TransactionDetail.AuxiliaryTransactionCode=vw_AuxTransactionCode.TransactionCode ")
            StrQuery.Append("INNER JOIN AccountOwner ON TransactionDetail.AccountOwnerId=AccountOwner.AccountOwnerId ")
            Return StrQuery.ToString()
        End Get
    End Property

    Private ReadOnly Property PK() As String
        Get
            'Return "CDD_CIF_Master.CIFNo"
            Return "TransactionDetail.TransactionDetailId"
        End Get
    End Property

    Private ReadOnly Property Fields() As String
        Get
            'Return "CDD_CIF_Master.CIFNo, CDD_CIF_Master.Name, CDD_CIF_Master.DateOfBirth, CDD_CIF_Master.AccountOwnerId, CDD_CIF_Master.OpeningDate, WorkingUnit.WorkingUnitName, CASE WHEN NOT CustomerInListId IS NULL THEN 'TRUE' ELSE 'FALSE' END AS IsCustomerInList"
            'Return "TransactionDetailId, TransactionDate, CAST(TransactionDetail.AccountOwnerId AS VARCHAR) + ' - ' + AccountOwner.AccountOwnerName AS AccountOwner, CountryCode, BankName, CIFNo, AccountNo, AccountName, DebitOrCredit, TransactionAmount, TransactionRemark, MemoRemark, TransactionTicketNo"
            Dim StrQuery As New StringBuilder
            StrQuery.Append("TransactionDetail.TransactionDetailId, TransactionDetail.TransactionDate, ")
            StrQuery.Append("CAST(TransactionDetail.AccountOwnerId AS VARCHAR)  + ' - ' + AccountOwner.AccountOwnerName AS AccountOwner, ")
            StrQuery.Append("TransactionChannelType.TransactionChannelTypeName, TransactionDetail.CountryCode + ' - ' + ISNULL(JHCOUN.JHCNAM,'') AS Country, ")
            StrQuery.Append("TransactionDetail.BankCode, TransactionDetail.BankName, ")
            StrQuery.Append("TransactionDetail.CIFNo, TransactionDetail.AccountNo, TransactionDetail.AccountName, ")
            StrQuery.Append("TransactionDetail.AuxiliaryTransactionCode + ' - ' + vw_AuxTransactionCode.TransactionDescription AS AuxiliaryTransactionCode, ")
            StrQuery.Append("TransactionDetail.TransactionAmount, TransactionDetail.CurrencyType, ")
            StrQuery.Append("TransactionDetail.DebitORCredit, TransactionDetail.MemoRemark, ")
            StrQuery.Append("TransactionDetail.TransactionRemark, TransactionDetail.TransactionExchangeRate, ")
            StrQuery.Append("TransactionDetail.TransactionLocalEquivalent, TransactionDetail.TransactionTicketNo, ")
            StrQuery.Append("TransactionDetail.UserId")
            Return StrQuery.ToString()
        End Get
    End Property

    Public Property PageSize() As Int16
        Get
            Return IIf(Session("PageSize") IsNot Nothing, Session("PageSize"), 10)
        End Get
        Set(ByVal value As Int16)
            Session("PageSize") = value
        End Set
    End Property
#End Region

    Private Sub FillSearch()
        Try
            Me.ComboSearch.Items.Add(New ListItem("...", ""))
            Me.ComboSearch.Items.Add(New ListItem("Transaction Date", "TransactionDate"))
            Me.ComboSearch.Items.Add(New ListItem("Account Owner", "CAST(TransactionDetail.AccountOwnerId AS VARCHAR)  + ' - ' + AccountOwner.AccountOwnerName"))
            Me.ComboSearch.Items.Add(New ListItem("TransactionChannelTypeName", "TransactionChannelType.TransactionChannelTypeName"))
            Me.ComboSearch.Items.Add(New ListItem("Country", "TransactionDetail.CountryCode + ' - ' + ISNULL(JHCOUN.JHCNAM,'')"))
            Me.ComboSearch.Items.Add(New ListItem("Bank Code", "BankCode"))
            Me.ComboSearch.Items.Add(New ListItem("Bank Name", "BankName"))
            Me.ComboSearch.Items.Add(New ListItem("CIFNo", "CIFNo"))
            Me.ComboSearch.Items.Add(New ListItem("Account No", "AccountNo"))
            Me.ComboSearch.Items.Add(New ListItem("Account Name", "AccountName"))
            Me.ComboSearch.Items.Add(New ListItem("AuxiliaryTransactionCode", "TransactionDetail.AuxiliaryTransactionCode + ' - ' + vw_AuxTransactionCode.TransactionDescription"))
            Me.ComboSearch.Items.Add(New ListItem("Transaction Amount", "TransactionAmount"))
            Me.ComboSearch.Items.Add(New ListItem("CurrencyType", "CurrencyType"))
            Me.ComboSearch.Items.Add(New ListItem("Debit / Credit", "DebitORCredit"))
            Me.ComboSearch.Items.Add(New ListItem("Memo Remark", "MemoRemark"))
            Me.ComboSearch.Items.Add(New ListItem("Transaction Remark", "TransactionRemark"))
            Me.ComboSearch.Items.Add(New ListItem("Transaction Exchange Rate", "TransactionExchangeRate"))
            Me.ComboSearch.Items.Add(New ListItem("Transaction Local Equivalent", "TransactionLocalEquivalent"))
            Me.ComboSearch.Items.Add(New ListItem("Transaction Ticket No", "TransactionTicketNo"))
            Me.ComboSearch.Items.Add(New ListItem("UserId", "UserId"))

            'Me.ComboSearch.Items.Add(New ListItem("Transaction Date", "TransactionDate Like '%-=Search=-%'"))
            'Me.ComboSearch.Items.Add(New ListItem("Transaction Code", "TransactionCode Like '%-=Search=-%'"))
            'Me.ComboSearch.Items.Add(New ListItem("Country Name", "CountryCode like '%-=Search=-%'"))
            'Me.ComboSearch.Items.Add(New ListItem("Bank Name", "BankName Like '%-=Search=-%'"))
            'Me.ComboSearch.Items.Add(New ListItem("CIFNo", "CIFNo like '%-=Search=-%'"))
            'Me.ComboSearch.Items.Add(New ListItem("Account No", "AccountNo like '%-=Search=-%'"))
            'Me.ComboSearch.Items.Add(New ListItem("Account Name", "AccountName LIKE '%-=Search=-%'"))
            'Me.ComboSearch.Items.Add(New ListItem("Transaction Amount", "TransactionAmount Like '%-=Search=-%'"))
            'Me.ComboSearch.Items.Add(New ListItem("Transaction Remark", "TransactionRemark Like '%-=Search=-%'"))
            'Me.ComboSearch.Items.Add(New ListItem("Memo Remark", "MemoRemark like '%-=Search=-%'"))
            'Me.ComboSearch.Items.Add(New ListItem("Transaction Ticket No", "TransactionTicketNo Like '%-=Search=-%'"))
        Catch
            Throw
        End Try
    End Sub

    Private Sub ClearThisPageSessions()
        Session("CustomTransactionAnalysisDetailSelected") = Nothing
        Session("CustomTransactionAnalysisDetailFieldSearch") = Nothing
        Session("CustomTransactionAnalysisDetailValueSearch") = Nothing
        Session("CustomTransactionAnalysisDetailSort") = Nothing
        Session("CustomTransactionAnalysisDetailCurrentPage") = Nothing
        Session("CustomTransactionAnalysisDetailRowTotal") = Nothing
        Session("CustomTransactionAnalysisDetailData") = Nothing
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim Trans As SqlTransaction = Nothing
        Try
            If Page.IsPostBack = False Then
                Me.ClearThisPageSessions()
                Me.ComboSearch.Attributes.Add("onchange", "javascript:Check(this, 1, '" & Me.TextSearch.ClientID & "', '" & Me.popUp.ClientID & "');")
                Me.popUp.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TextSearch.ClientID & "'), 'dd-mmm-yyyy')")
                Me.popUp.Style.Add("display", "none")
                Me.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow
                Me.FillSearch()
                Me.GridMSUserView.PageSize = Me.PageSize
                Me.SetnGetValueSearch = Session("CustomTransactionSearchCriteria")

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Trans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAudit)
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                    Trans.Commit()
                End Using
            End If
        Catch ex As Exception
            If Not Trans Is Nothing Then Trans.Rollback()
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not Trans Is Nothing Then
                Trans.Dispose()
            End If
        End Try
    End Sub

    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknown Commandname " & e.CommandName)
            End Select
            Me.CollectSelected()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Public Sub BindGrid()
        Try
            'Me.SetnGetValueSearch = ""
            Me.SetnGetBindTable = Sahassa.AML.Commonly.GetDatasetPaging(Me.Tables, Me.PK, Me.SetnGetSort, Me.SetnGetCurrentPage + 1, Me.PageSize, Me.Fields, Me.SetnGetValueSearch, "")
            Me.GridMSUserView.DataSource = Me.SetnGetBindTable
            Dim TotalRow As Int64
            'Using AccessAccountInformation As New AMLDAL.AccountInformationTableAdapters.GetCountAccountInformationByFilterTableAdapter
            '    Dim Filter As String = Me.SetnGetFieldSearch.Replace("-=Search=-", Me.SetnGetValueSearch.Replace("'", "''"))
            '    If Filter <> "" Then
            '        TotalRow = AccessAccountInformation.Fill("WHERE " & Filter)
            '    Else
            '        TotalRow = AccessAccountInformation.Fill(Filter)
            '    End If
            '    Me.SetnGetRowTotal = TotalRow
            'End Using

            TotalRow = Sahassa.AML.Commonly.GetTableCount(Me.Tables, Me.SetnGetValueSearch)
            Me.SetnGetRowTotal = TotalRow

            Me.GridMSUserView.VirtualItemCount = Me.SetnGetRowTotal
            Me.GridMSUserView.DataBind()
        Catch
            Throw
        End Try
    End Sub

    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.ComboSearch.SelectedValue = Me.SetnGetFieldSearch
            'Me.TextSearch.Text = Me.SetnGetValueSearch
        Catch
            Throw
        End Try
    End Sub

    Private Sub GridA_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridMSUserView.SortCommand
        Dim GridUser As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Try
            Me.CollectSelected()
            Me.SetnGetFieldSearch = Me.ComboSearch.SelectedValue
            If Me.ComboSearch.SelectedIndex = 0 Then
                Me.TextSearch.Text = ""
                Me.SetnGetValueSearch = Session("CustomTransactionSearchCriteria")
            ElseIf Me.ComboSearch.SelectedIndex = 1 Then
                Dim DateSearch As DateTime
                Try
                    DateSearch = DateTime.Parse(Me.TextSearch.Text, New System.Globalization.CultureInfo("id-ID"))
                Catch ex As ArgumentOutOfRangeException
                    Throw New Exception("Input character cannot be convert to datetime.")
                Catch ex As FormatException
                    Throw New Exception("Unknown format date")
                End Try
                '    Me.SetnGetValueSearch = DateSearch.ToString("dd-MMM-yyyy")
                Me.SetnGetValueSearch = Session("CustomTransactionSearchCriteria") & " and " & Me.ComboSearch.SelectedValue & " = '" & Me.TextSearch.Text & "' "
            Else
                Me.SetnGetValueSearch = Session("CustomTransactionSearchCriteria") & " and " & Me.ComboSearch.SelectedValue & " like '%" & Me.TextSearch.Text & "%' "
            End If
            Me.SetnGetCurrentPage = 0
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Private Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            Me.CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be a positive number greater than zero.")
                End If
            Else
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

#Region "Excel"
    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = gridRow.FindControl("CheckBoxExporttoExcel")
                Dim CIFNo As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = Me.SetnGetSelectedItemFilter
                If ArrTarget.Contains(CIFNo) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(CIFNo)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(CIFNo)
                End If
                Me.SetnGetSelectedItemFilter = ArrTarget
            End If
        Next
    End Sub

    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub 'ClearControls

    Private Sub BindSelected()
        Try
            'Dim Rows As New ArrayList
            'Dim oTableAccountInformation As New Data.DataTable
            'Dim oRowAccountInformation As Data.DataRow
            ''Me.ComboSearch.Items.Add(New ListItem("Transaction Date", "TransactionDate"))
            ''Me.ComboSearch.Items.Add(New ListItem("Account Owner", "CAST(TransactionDetail.AccountOwnerId AS VARCHAR)  + ' - ' + AccountOwner.AccountOwnerName"))
            ''Me.ComboSearch.Items.Add(New ListItem("TransactionChannelTypeName", "TransactionChannelType.TransactionChannelTypeName"))
            ''Me.ComboSearch.Items.Add(New ListItem("Country", "TransactionDetail.CountryCode + ' - ' + ISNULL(JHCOUN.JHCNAM,'')"))
            ''Me.ComboSearch.Items.Add(New ListItem("Bank Code", "BankCode"))
            ''Me.ComboSearch.Items.Add(New ListItem("Bank Name", "BankName"))
            ''Me.ComboSearch.Items.Add(New ListItem("CIFNo", "CIFNo"))
            ''Me.ComboSearch.Items.Add(New ListItem("Account No", "AccountNo"))
            ''Me.ComboSearch.Items.Add(New ListItem("Account Name", "AccountName"))
            ''Me.ComboSearch.Items.Add(New ListItem("AuxiliaryTransactionCode", "TransactionDetail.AuxiliaryTransactionCode + ' - ' + TLTX.TLTXDS"))
            ''Me.ComboSearch.Items.Add(New ListItem("Transaction Amount", "TransactionAmount"))
            ''Me.ComboSearch.Items.Add(New ListItem("CurrencyType", "CurrencyType"))
            ''Me.ComboSearch.Items.Add(New ListItem("Debit / Credit", "DebitORCredit"))
            ''Me.ComboSearch.Items.Add(New ListItem("Memo Remark", "MemoRemark"))
            ''Me.ComboSearch.Items.Add(New ListItem("Transaction Remark", "TransactionRemark"))
            ''Me.ComboSearch.Items.Add(New ListItem("Transaction Exchange Rate", "TransactionExchangeRate"))
            ''Me.ComboSearch.Items.Add(New ListItem("Transaction Local Equivalent", "TransactionLocalEquivalent"))
            ''Me.ComboSearch.Items.Add(New ListItem("Transaction Ticket No", "TransactionTicketNo"))
            ''Me.ComboSearch.Items.Add(New ListItem("UserId", "UserId"))

            'oTableAccountInformation.Columns.Add("TransactionDetailId", GetType(String))
            'oTableAccountInformation.Columns.Add("TransactionDate", GetType(String))
            'oTableAccountInformation.Columns.Add("AccountOwner", GetType(String))
            'oTableAccountInformation.Columns.Add("TransactionChannelTypeName", GetType(String))
            'oTableAccountInformation.Columns.Add("Country", GetType(String))
            'oTableAccountInformation.Columns.Add("BankCode", GetType(String))
            'oTableAccountInformation.Columns.Add("BankName", GetType(String))
            'oTableAccountInformation.Columns.Add("CIFNo", GetType(String))
            'oTableAccountInformation.Columns.Add("AccountNo", GetType(String))
            'oTableAccountInformation.Columns.Add("AccountName", GetType(String))
            'oTableAccountInformation.Columns.Add("AuxiliaryTransactionCode", GetType(String))
            'oTableAccountInformation.Columns.Add("TransactionAmount", GetType(String))
            'oTableAccountInformation.Columns.Add("CurrencyType", GetType(String))
            'oTableAccountInformation.Columns.Add("DebitOrCredit", GetType(String))
            'oTableAccountInformation.Columns.Add("MemoRemark", GetType(String))
            'oTableAccountInformation.Columns.Add("TransactionRemark", GetType(String))
            'oTableAccountInformation.Columns.Add("TransactionExchangeRate", GetType(String))
            'oTableAccountInformation.Columns.Add("TransactionLocalEquivalent", GetType(String))
            'oTableAccountInformation.Columns.Add("TransactionTicketNo", GetType(String))
            'oTableAccountInformation.Columns.Add("UserId", GetType(String))
            'For Each IdPk As String In Me.SetnGetSelectedItemFilter
            '    Dim rowData() As Data.DataRow = Me.SetnGetBindTable.Select("TransactionDetailId = '" & IdPk & "'")
            '    If rowData.Length > 0 Then
            '        oRowAccountInformation = oTableAccountInformation.NewRow
            '        oRowAccountInformation("TransactionDetailId") = rowData(0)("TransactionDetailId")
            '        oRowAccountInformation("TransactionDate") = rowData(0)("TransactionDate")
            '        oRowAccountInformation("AccountOwner") = rowData(0)("AccountOwner")
            '        oRowAccountInformation("TransactionChannelTypeName") = rowData(0)("TransactionChannelTypeName")
            '        oRowAccountInformation("Country") = rowData(0)("Country")
            '        oRowAccountInformation("BankCode") = rowData(0)("BankCode")
            '        oRowAccountInformation("BankName") = rowData(0)("BankName")
            '        oRowAccountInformation("CIFNo") = rowData(0)("CIFNo")
            '        oRowAccountInformation("AccountNo") = rowData(0)("AccountNo")
            '        oRowAccountInformation("AccountName") = rowData(0)("AccountName")
            '        oRowAccountInformation("AuxiliaryTransactionCode") = rowData(0)("AuxiliaryTransactionCode")
            '        oRowAccountInformation("TransactionAmount") = rowData(0)("TransactionAmount")
            '        oRowAccountInformation("CurrencyType") = rowData(0)("CurrencyType")
            '        oRowAccountInformation("DebitOrCredit") = rowData(0)("DebitOrCredit")
            '        oRowAccountInformation("MemoRemark") = rowData(0)("MemoRemark")
            '        oRowAccountInformation("TransactionRemark") = rowData(0)("TransactionRemark")
            '        oRowAccountInformation("TransactionExchangeRate") = rowData(0)("TransactionExchangeRate")
            '        oRowAccountInformation("TransactionLocalEquivalent") = rowData(0)("TransactionLocalEquivalent")
            '        oRowAccountInformation("TransactionTicketNo") = rowData(0)("TransactionTicketNo")
            '        oRowAccountInformation("UserId") = rowData(0)("UserId")
            '        oTableAccountInformation.Rows.Add(oRowAccountInformation)
            '    End If
            'Next
            'Me.GridMSUserView.DataSource = oTableAccountInformation
            'Me.GridMSUserView.AllowPaging = False
            'Me.GridMSUserView.DataBind()

            ''Sembunyikan kolom ke 0,1,4 & 5 agar tidak ikut diekspor ke excel
            'Me.GridMSUserView.Columns(0).Visible = False
            'Me.GridMSUserView.Columns(1).Visible = False

            Dim listPK As New System.Collections.Generic.List(Of String)
            listPK.Add("'0'")
            For Each IdPk As String In Me.SetnGetSelectedItemFilter
                listPK.Add("'" & IdPk.ToString & "'")
            Next

            Me.GridMSUserView.DataSource = Sahassa.AML.Commonly.GetDatasetPaging(Me.Tables, Me.PK, Me.SetnGetSort, 0, Int32.MaxValue, Me.Fields, "TransactionDetailId IN (" & String.Join(",", listPK.ToArray) & ")", "")
            Me.GridMSUserView.AllowPaging = False
            Me.GridMSUserView.DataBind()

            'Sembunyikan kolom ke 0,1,4 & 5 agar tidak ikut diekspor ke excel
            Me.GridMSUserView.Columns(0).Visible = False
            Me.GridMSUserView.Columns(1).Visible = False
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Me.CollectSelected()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelected()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=Custom Transaction Detail.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridMSUserView)
            GridMSUserView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
#End Region

    Private Sub GridMSUserView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridMSUserView.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Dim chkBox As CheckBox = e.Item.FindControl("CheckBoxExporttoExcel")
                chkBox.Checked = Me.SetnGetSelectedItemFilter.Contains(e.Item.Cells(1).Text)
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        'For Each gridRow As DataGridItem In Me.GridMSUserView.Items
        '    If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
        '        Dim AccountID As String = gridRow.Cells(1).Text
        '        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItemFilter
        '        If Me.SetnGetSelectedItemFilter.Contains(AccountID) Then
        '            If Me.CheckBoxSelectAll.Checked Then
        '                ArrTarget.Add(AccountID)
        '            Else
        '                ArrTarget.Remove(AccountID)
        '            End If
        '        Else
        '            If Me.CheckBoxSelectAll.Checked Then
        '                ArrTarget.Add(AccountID)
        '            End If
        '        End If
        '        Me.SetnGetSelectedItemFilter = ArrTarget
        '    End If
        'Next
        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItemFilter
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(1).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItemFilter.Contains(StrKey) = False Then
                        ArrTarget.Add(StrKey)
                    End If
                Else
                    If Me.SetnGetSelectedItemFilter.Contains(StrKey) Then
                        ArrTarget.Remove(StrKey)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItemFilter = ArrTarget
    End Sub

    Protected Sub LinkAddNewCase_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkAddNewCase.Click
        Me.CollectSelected()
        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItemFilter

        Sahassa.AML.Commonly.SessionIntendedPage = "CreateNewCase.aspx"
        Response.Redirect("CreateNewCase.aspx", False)
    End Sub

    Private Sub BindExportAll()
        Try
            Me.GridMSUserView.DataSource = Sahassa.AML.Commonly.GetDatasetPaging(Me.Tables, Me.PK, Me.SetnGetSort, 0, Int32.MaxValue, Me.Fields, Me.SetnGetValueSearch, "")
            Me.GridMSUserView.AllowPaging = False
            Me.GridMSUserView.DataBind()

            'Sembunyikan kolom ke 0,1,4 & 5 agar tidak ikut diekspor ke excel
            Me.GridMSUserView.Columns(0).Visible = False
            Me.GridMSUserView.Columns(1).Visible = False
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub LnkBtnExportAllToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LnkBtnExportAllToExcel.Click
        Try
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindExportAll()
            Response.Clear()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=Custom Transaction Detail.xls")
            Response.Charset = ""
            Response.AddHeader("cache-control", "max-age=0")
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridMSUserView)
            GridMSUserView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class