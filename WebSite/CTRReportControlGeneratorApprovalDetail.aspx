﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="CTRReportControlGeneratorApprovalDetail.aspx.vb" Inherits="CTRReportControlGeneratorApprovalDetail" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">
    <table cellspacing="4" cellpadding="0" width="100%">
        <tr>
            <td valign="bottom" align="left">
                <img height="15" src="images/dot_title.gif" width="15"></td>
            <td class="maintitle" valign="bottom" width="99%">
                <asp:Label ID="LabelTitle" runat="server"></asp:Label></td>
        </tr>
    </table>

    <table  cellpadding="1" cellspacing="4" width="100%">
        <tr>
            <td colspan="3">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </td>
        
        </tr>
        <tr>
            <td style=" width:50px">
                &nbsp;</td>
            <td style=" width:30%">
                <asp:Label ID="Label1" runat="server" Text="Requested Date"></asp:Label>
            </td>
            <td>
                <asp:Label ID="LblRequestDate" runat="server"></asp:Label>
            </td>
            </tr>
        <tr>
            <td style=" width:50px">
                &nbsp;</td>
            <td>
                <asp:Label ID="Label2" runat="server" Text="CTR Date"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblCTRDate" runat="server"></asp:Label>
            </td>
            </tr>
        <tr>
            <td style=" width:50px">
                &nbsp;</td>
            <td>
                Proposed User</td>
            <td>
                <asp:Label ID="LblUserid" runat="server"></asp:Label>
            </td>
            </tr>
        <tr>
            <td style=" width:50px">
                &nbsp;</td>
            <td>
                <asp:Label ID="Label3" runat="server" Text="Report Type"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblReportType" runat="server"></asp:Label>
            </td>
            </tr>
        <tr>
            <td style=" width:50px">
                &nbsp;</td>
            <td>
                <asp:Label ID="Label4" runat="server" Text="Report Format"></asp:Label>
            </td>
            <td>
                <asp:Label ID="LblReportFormat" runat="server"></asp:Label>
            </td>
            </tr>
        <tr>
            <td style=" width:50px">
                &nbsp;</td>
            <td>
                <asp:Label ID="Label5" runat="server" Text="Total CIF"></asp:Label>
            </td>
            <td>
                <asp:Label ID="LblTotalCIF" runat="server"></asp:Label>
            </td>
            </tr>
        <tr>
            <td style=" width:50px">
                &nbsp;</td>
            <td>
                <asp:Label ID="lblwic" runat="server" Text="Total WIC"></asp:Label>
            </td>
            <td>
                <asp:Label ID="LblTotalWIC" runat="server"></asp:Label>
            </td>
            </tr>
        <tr>
            <td style=" width:50px">
                &nbsp;</td>
            <td>
                <asp:Label ID="LblFile" runat="server" Text="File"></asp:Label>
            </td>
            <td>
                <asp:LinkButton ID="LnkFile" runat="server"></asp:LinkButton>
            </td>
            </tr>
        <tr>
            <td style=" width:50px">     
                <asp:CustomValidator ID="CvalPageErr" runat="server" 
                    ErrorMessage="" Display="Dynamic" Visible="False"></asp:CustomValidator>
            </td>
            <td colspan="2">
                <asp:ImageButton ID="ImgAccept" runat="server" 
                    ImageUrl="~/Images/button/accept.gif" /> 
                &nbsp; 
                <asp:ImageButton ID="ImgReject" runat="server" 
                    ImageUrl="~/Images/button/reject.gif" />  
                &nbsp;  
                <asp:ImageButton ID="ImgBack" runat="server" 
                    ImageUrl="~/Images/button/back.gif" />
            </td>
            </tr>
    </table>
</asp:Content>
