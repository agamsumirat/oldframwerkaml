Imports System.Data
Imports System.Data.SqlClient
Imports AML2015Nettier.Data
Imports AML2015Nettier.Entities
Partial Class CTRExemptionApproval
    Inherits Parent

#Region " Property "
    ''' <summary>
    ''' selected item store
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("MsCTRExemptionListSelected") Is Nothing, New ArrayList, Session("MsCTRExemptionListSelected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("MsCTRExemptionListSelected") = value
        End Set
    End Property
    ''' <summary>
    ''' setnget search field
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetFieldSearch() As String
        Get
            Return IIf(Session("MsCTRExemptionListFieldSearch") Is Nothing, "", Session("MsCTRExemptionListFieldSearch"))
        End Get
        Set(ByVal Value As String)
            Session("MsCTRExemptionListFieldSearch") = Value
        End Set
    End Property
    ''' <summary>
    ''' search value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetValueSearch() As String
        Get
            Return IIf(Session("MsCTRExemptionListValueSearch") Is Nothing, "", Session("MsCTRExemptionListValueSearch"))
        End Get
        Set(ByVal Value As String)
            Session("MsCTRExemptionListValueSearch") = Value
        End Set
    End Property
    ''' <summary>
    ''' sort expresion
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("MsCTRExemptionListSort") Is Nothing, "CTRPendingApproval_ModeID asc", Session("MsCTRExemptionListSort"))
        End Get
        Set(ByVal Value As String)
            Session("MsCTRExemptionListSort") = Value
        End Set
    End Property
    ''' <summary>
    ''' current page index
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("MsCTRExemptionListCurrentPage") Is Nothing, 0, Session("MsCTRExemptionListCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("MsCTRExemptionListCurrentPage") = Value
        End Set
    End Property
    ''' <summary>
    ''' total pages
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    ''' <summary>
    ''' row total
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("MsCTRExemptionListRowTotal") Is Nothing, 0, Session("MsCTRExemptionListRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("MsCTRExemptionListRowTotal") = Value
        End Set
    End Property
    ''' <summary>
    ''' save bind table
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    'Private Property SetnGetBindTable() As Data.DataTable
    '    Get
    '        Return IIf(Session("MsCTRExemptionListData") Is Nothing, New AMLDAL.CTRExemptionList.CTRPending_SelectCommandDataTable, Session("MsCTRExemptionListData"))
    '    End Get
    '    Set(ByVal value As Data.DataTable)
    '        Session("MsCTRExemptionListData") = value
    '    End Set
    'End Property
    Private Property SetnGetBindTable() As TList(Of CTRExemptionList_PendingApproval)
        Get
            Dim strWhereClause(-1) As String
            Dim strAllWhereClause As String = ""
            If Me.SetnGetCurrentPage > Me.GetPageTotal - 1 And Me.GetPageTotal - 1 > 0 Then
                Me.SetnGetCurrentPage = Me.GetPageTotal - 1
            End If


            If SetnGetFieldSearch.Trim.Length > 0 Then
                ReDim Preserve strWhereClause(strWhereClause.Length)
                strWhereClause(strWhereClause.Length - 1) = Me.SetnGetFieldSearch.Replace("-=Search=-", Me.SetnGetValueSearch.Replace("'", "''"))
            End If
            strAllWhereClause = String.Join(" and ", strWhereClause)
            If strAllWhereClause = "" Then
                strAllWhereClause += "(CTRPendingApproval_CreatedBy <> '" & Sahassa.AML.Commonly.SessionUserId & "') AND (CTRPendingApproval_CreatedBy IN (SELECT UserID FROM UserWorkingUnitAssignment  WHERE (WorkingUnitID IN ((SELECT WorkingUnitID FROM UserWorkingUnitAssignment AS UserWorkingUnitAssignment_1 WHERE      (UserID = '" & Sahassa.AML.Commonly.SessionUserId & "'))))))"
            Else
                strAllWhereClause += " and (CTRPendingApproval_CreatedBy <> '" & Sahassa.AML.Commonly.SessionUserId & "') AND (CTRPendingApproval_CreatedBy IN (SELECT UserID FROM UserWorkingUnitAssignment  WHERE (WorkingUnitID IN ((SELECT WorkingUnitID FROM UserWorkingUnitAssignment AS UserWorkingUnitAssignment_1 WHERE      (UserID = '" & Sahassa.AML.Commonly.SessionUserId & "'))))))"
            End If

            Session("MsCTRExemptionListData") = AMLBLL.CTRExemptionListBLL.GetTlistCTRExemptionList_PendingApproval(strAllWhereClause, SetnGetSort, SetnGetCurrentPage, Sahassa.AML.Commonly.GetDisplayedTotalRow, Me.SetnGetRowTotal)
            Return CType(Session("MsCTRExemptionListData"), TList(Of CTRExemptionList_PendingApproval))

        End Get
        Set(ByVal value As TList(Of CTRExemptionList_PendingApproval))

        End Set
    End Property

#End Region

    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknow Commandname " & e.CommandName)
            End Select            
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub FillSearch()
        Try
            Me.ComboSearch.Items.Add(New ListItem("...", ""))
            Me.ComboSearch.Items.Add(New ListItem("Created By", "CTRPendingApproval_CreatedBy Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Entry Date", "CTRPendingApproval_CreatedDate >= '-=Search=- 00:00' AND CTRPendingApproval_CreatedDate <= '-=Search=- 23:59'"))
            Me.ComboSearch.Items.Add(New ListItem("Account Number", "CTRPendingApproval_AccountNo Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("CIF No", "CTRPendingApproval_CIFNo Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Action", "CTRPendingApproval_Category Like '%-=Search=-%'"))
        Catch
            Throw
        End Try
    End Sub

    Private Sub ClearThisPageSessions()
        Session("MsCTRExemptionListSelected") = Nothing
        Session("MsCTRExemptionListFieldSearch") = Nothing
        Session("MsCTRExemptionListValueSearch") = Nothing
        Session("MsCTRExemptionListSort") = Nothing
        Session("MsCTRExemptionListCurrentPage") = Nothing
        Session("MsCTRExemptionListRowTotal") = Nothing
        Session("MsCTRExemptionListData") = Nothing
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.ClearThisPageSessions()

                
                Me.ComboSearch.Attributes.Add("onchange", "javascript:Check(this, 2, '" & Me.TextSearch.ClientID & "', '" & Me.popUp.ClientID & "');")
                Me.popUp.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TextSearch.ClientID & "'), 'dd-mmm-yyyy')")
                Me.popUp.Style.Add("display", "none")
                Me.FillSearch()
                Me.GridMSUserView.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

  
    Public Sub BindGrid()
        Dim strWhereClause As String = Me.SetnGetFieldSearch.Replace("-=Search=-", Me.SetnGetValueSearch.Replace("'", "''"))
        'If strWhereClause.Contains("CreatedDate BETWEEN") Then
        '    strWhereClause = strWhereClause.Replace(" BETWEEN ", " >= ").Replace(" AND ", " AND CreatedDate <= ")
        'End If
        'Dim Rows() As AMLDAL.CTRExemptionList.CTRExemptionListRow = Me.SetnGetBindTable.Select(strWhereClause, Me.SetnGetSort)

        Me.GridMSUserView.DataSource = SetnGetBindTable
        Me.SetnGetRowTotal = Me.SetnGetRowTotal
        Me.GridMSUserView.CurrentPageIndex = Me.SetnGetCurrentPage
        Me.GridMSUserView.AllowCustomPaging = True
        Me.GridMSUserView.VirtualItemCount = Me.SetnGetRowTotal
        Me.GridMSUserView.DataBind()
    End Sub

    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.ComboSearch.SelectedValue = Me.SetnGetFieldSearch
            Me.TextSearch.Text = Me.SetnGetValueSearch
        Catch
            Throw
        End Try
    End Sub

    Private Sub GridMSUserView_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridMSUserView.SortCommand
        Dim GridUser As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub GridMSUserView_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridMSUserView.EditCommand
        Dim TypeConfirm As Sahassa.AML.Commonly.TypeConfirm
        Dim PendingID As Long
        Try
            PendingID = e.Item.Cells(0).Text

            If e.Item.Cells(5).Text.IndexOf("Add") > -1 Then
                TypeConfirm = Sahassa.AML.Commonly.TypeConfirm.GroupAdd
            End If
            If e.Item.Cells(5).Text.IndexOf("Edit") > -1 Then
                TypeConfirm = Sahassa.AML.Commonly.TypeConfirm.GroupEdit
            End If
            If e.Item.Cells(5).Text.IndexOf("Delete") > -1 Then
                TypeConfirm = Sahassa.AML.Commonly.TypeConfirm.GroupDelete
            End If

            Sahassa.AML.Commonly.SessionIntendedPage = "CTRExemptionApprovalDetail.aspx?PendingID=" & PendingID & "&TypeConfirm=" & TypeConfirm.ToString("D")

            MagicAjax.AjaxCallHelper.Redirect("CTRExemptionApprovalDetail.aspx?PendingID=" & PendingID & "&TypeConfirm=" & TypeConfirm.ToString("D"))
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Public Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Try        
            Me.SetnGetFieldSearch = Me.ComboSearch.SelectedValue
            If Me.ComboSearch.SelectedIndex = 0 Then
                Me.TextSearch.Text = ""
                Me.SetnGetValueSearch = Me.TextSearch.Text
            ElseIf Me.ComboSearch.SelectedIndex = 2 Then
                Dim DateSearch As DateTime
                Try
                    DateSearch = DateTime.Parse(Me.TextSearch.Text, New System.Globalization.CultureInfo("id-ID"))
                Catch ex As ArgumentOutOfRangeException
                    Throw New Exception("Input character cannot be convert to datetime.")
                Catch ex As FormatException
                    Throw New Exception("Unknown format date")
                End Try
                Me.SetnGetValueSearch = DateSearch.ToString("dd-MMM-yyyy")
            Else
                Me.SetnGetValueSearch = Me.TextSearch.Text
            End If
            Me.SetnGetCurrentPage = 0

        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Function IsDeleted(ByVal strRiskRatingID As String) As Boolean
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.RiskRating_ApprovalTableAdapter
            Dim count As Int32 = AccessPending.CountRiskRatingApproval(strRiskRatingID)
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        End Using
    End Function

    Private Sub GridMSUserView_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridMSUserView.DeleteCommand
        Dim strRiskRatingID As String = e.Item.Cells(1).Text

        Try
            If Me.IsDeleted(strRiskRatingID) = False Then
                Me.Response.Redirect("CTRExemptionListDelete.aspx?CTRExemptionID=" & strRiskRatingID, False)
            Else
                Throw New Exception("Cannot delete the following CTR Exemption: '" & e.Item.Cells(2).Text & "' because it is currently waiting for approval")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try            
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be less than or equal to the total page count.")
                End If
            Else
                Throw New Exception("Page number must be less than or equal to the total page count.")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
End Class