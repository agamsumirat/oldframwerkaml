#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports Sahassa.AML
#End Region

Partial Class MsBidangUsaha_Delete
    Inherits Parent

#Region "Function"

    Sub ChangeMultiView(ByVal index As Integer)
        MtvMsUser.ActiveViewIndex = index
    End Sub

    Private Sub BindListMapping()
        LBMapping.DataSource = Listmaping
        LBMapping.DataTextField = "nama"
        LBMapping.DataValueField = "IDBidangUsahaNCBS"
        LBMapping.DataBind()
    End Sub


#End Region

#Region "events..."

    Public ReadOnly Property parID() As String
        Get
            Return Request.Item("ID")
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
                Listmaping = New TList(Of MappingBidangUsahaNCBS)
                LoadData()
            Catch ex As Exception
                LogError(ex)
                CvalPageErr.IsValid = False
                CvalPageErr.ErrorMessage = ex.Message
            End Try
        End If

    End Sub

    Private Sub LoadData()
        Dim ObjMsBidangUsaha As MsBidangUsaha = DataRepository.MsBidangUsahaProvider.GetPaged(MsBidangUsahaColumn.IdBidangUsaha.ToString & "=" & parID, "", 0, 1, Nothing)(0)
        If ObjMsBidangUsaha Is Nothing Then Throw New Exception("Data Not Found")
        With ObjMsBidangUsaha
            SafeDefaultValue = "-"
            lblIdBidangUsaha.Text = .IdBidangUsaha
            lblNamaBidangUsaha.Text = Safe(.NamaBidangUsaha)

            'other info
            Dim Omsuser As TList(Of User)
            Omsuser = DataRepository.UserProvider.GetByUserID(.CreatedBy)
            If Omsuser.Count > 0 Then
                lblCreatedBy.Text = Omsuser(0).UserName
            End If
            lblCreatedDate.Text = FormatDate(.CreatedDate)
            Omsuser = DataRepository.UserProvider.GetByUserID(.LastUpdatedBy)
            If Omsuser.Count > 0 Then
                lblUpdatedby.Text = Omsuser(0).UserName
            End If
            lblUpdatedDate.Text = FormatDate(.LastUpdatedDate)
            lblActivation.Text = SafeActiveInactive(.Activation)
            Dim L_objMappingBidangUsahaNCBS As TList(Of MappingBidangUsahaNCBS)
            L_objMappingBidangUsahaNCBS = DataRepository.MappingBidangUsahaNCBSProvider.GetPaged(MappingBidangUsahaNCBSColumn.BidangUsahaId.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)

            Listmaping.AddRange(L_objMappingBidangUsahaNCBS)

        End With
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            BindListMapping()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

#End Region

#Region "Property..."

    ''' <summary>
    ''' Menyimpan Item untuk mapping sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Listmaping() As TList(Of MappingBidangUsahaNCBS)
        Get
            Return Session("Listmaping.data")
        End Get
        Set(ByVal value As TList(Of MappingBidangUsahaNCBS))
            Session("Listmaping.data") = value
        End Set
    End Property

#End Region

#Region "events..."
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Response.Redirect("MsBidangUsaha_View.aspx")
    End Sub

    Protected Sub imgOk_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgOk.Click
        Try
            Page.Validate("handle")
            If Page.IsValid Then

                ' =========== Insert Header Approval
                Dim KeyHeaderApproval As Integer
                Using ObjMsBidangUsaha_Approval As New MsBidangUsaha_Approval
                    With ObjMsBidangUsaha_Approval
                        .FK_MsMode_Id = 3
                        FillOrNothing(.RequestedBy, SessionPkUserId)
                        FillOrNothing(.RequestedDate, Date.Now)
                        .IsUpload = False
                    End With
                    DataRepository.MsBidangUsaha_ApprovalProvider.Save(ObjMsBidangUsaha_Approval)
                    KeyHeaderApproval = ObjMsBidangUsaha_Approval.PK_MsBidangUsaha_Approval_Id
                End Using
                '============ Insert Detail Approval
                Using objMsBidangUsaha_ApprovalDetail As New MsBidangUsaha_ApprovalDetail()
                    With objMsBidangUsaha_ApprovalDetail
                        Dim ObjMsBidangUsaha As MsBidangUsaha = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(parID)
                        FillOrNothing(.IdBidangUsaha, lblIdBidangUsaha.Text, True, oInt)
                        FillOrNothing(.NamaBidangUsaha, lblNamaBidangUsaha.Text, True, Ovarchar)

                        FillOrNothing(.IdBidangUsaha, ObjMsBidangUsaha.IdBidangUsaha)
                        FillOrNothing(.Activation, ObjMsBidangUsaha.Activation)
                        FillOrNothing(.CreatedDate, ObjMsBidangUsaha.CreatedDate)
                        FillOrNothing(.CreatedBy, ObjMsBidangUsaha.CreatedBy)
                        FillOrNothing(.FK_MsBidangUsaha_Approval_Id, KeyHeaderApproval)
                    End With
                    DataRepository.MsBidangUsaha_ApprovalDetailProvider.Save(objMsBidangUsaha_ApprovalDetail)
                    Dim keyHeaderApprovalDetail As Integer = objMsBidangUsaha_ApprovalDetail.PK_MsBidangUsaha_ApprovalDetail_Id

                    '========= Insert mapping item 
                    Dim LobjMappingBidangUsahaNCBS_Approval_Detail_Approval_Detail As New TList(Of MappingBidangUsahaNCBS_Approval_Detail)

                    Dim L_ObjMappingBidangUsahaNCBS As TList(Of MappingBidangUsahaNCBS) = DataRepository.MappingBidangUsahaNCBSProvider.GetPaged(MappingBidangUsahaNCBSColumn.BidangUsahaId.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)

                    For Each objMappingBidangUsahaNCBS As MappingBidangUsahaNCBS In L_ObjMappingBidangUsahaNCBS
                        Dim objMappingBidangUsahaNCBS_Approval_Detail_Approval_Detail As New MappingBidangUsahaNCBS_Approval_Detail
                        With objMappingBidangUsahaNCBS_Approval_Detail_Approval_Detail
                            FillOrNothing(.BidangUsahaId, objMappingBidangUsahaNCBS.BidangUsahaId)
                            FillOrNothing(.IDBidangUsahaNCBS, objMappingBidangUsahaNCBS.IDBidangUsahaNCBS)
                            FillOrNothing(.PK_MsBidangUsaha_Approval_id, KeyHeaderApproval)
                            FillOrNothing(.PK_MappingBidangUsahaNCBS_Id, objMappingBidangUsahaNCBS.PK_MappingBidangUsahaNCBS_Id)
                            .NAMA = objMappingBidangUsahaNCBS.Nama
                            LobjMappingBidangUsahaNCBS_Approval_Detail_Approval_Detail.Add(objMappingBidangUsahaNCBS_Approval_Detail_Approval_Detail)
                        End With
                        DataRepository.MappingBidangUsahaNCBS_Approval_DetailProvider.Save(LobjMappingBidangUsahaNCBS_Approval_Detail_Approval_Detail)
                    Next
                    'session Maping item Clear
                    Listmaping.Clear()
                    Me.imgOk.Visible = False
                    Me.ImageCancel.Visible = False
                    LblConfirmation.Text = "Data has been Delete and waiting for approval"
                    MtvMsUser.ActiveViewIndex = 1
                End Using
            End If
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImgBtnAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAdd.Click
        Response.Redirect("MsBidangUsaha_View.aspx")
    End Sub

#End Region

End Class



