Imports System.Data.SqlClient
Imports Sahassa.AML.TableAdapterHelper
Partial Class BranchTypeMappingApprovalDetail
    Inherits Parent
    Private ReadOnly Property PKPendingID() As Long
        Get
            Dim temp As String
            temp = Request.Params("PKPendingID")
            If temp = "" Or Not IsNumeric(temp) Then
                Throw New Exception("Pk Pending Id is not valid")
            Else
                Return temp
            End If
        End Get
    End Property
    Private _orowBranchTypeMappingApprovalDetail As AMLDAL.MappingBranch.BranchTypeMappingApprovalRow
    Private ReadOnly Property orowBranchTypeMappingApprovalDetail() As AMLDAL.MappingBranch.BranchTypeMappingApprovalRow
        Get
            If _orowBranchTypeMappingApprovalDetail Is Nothing Then
                Using adapter As New AMLDAL.MappingBranchTableAdapters.BranchTypeMappingApprovalTableAdapter
                    Using otable As AMLDAL.MappingBranch.BranchTypeMappingApprovalDataTable = adapter.GetDataByFK(Me.PKPendingID)
                        If otable.Rows.Count > 0 Then
                            _orowBranchTypeMappingApprovalDetail = otable.Rows(0)
                            Return _orowBranchTypeMappingApprovalDetail
                        Else
                            _orowBranchTypeMappingApprovalDetail = Nothing
                            Return _orowBranchTypeMappingApprovalDetail
                        End If
                    End Using
                End Using
            Else
                Return _orowBranchTypeMappingApprovalDetail
            End If
        End Get
    End Property
    Private _orowBranchTypeMappingPendingApproval As AMLDAL.MappingBranch.BranchTypeMapping_PendingApprovalRow
    Private ReadOnly Property orowBranchTypeMappingPendingApproval() As AMLDAL.MappingBranch.BranchTypeMapping_PendingApprovalRow
        Get
            If _orowBranchTypeMappingPendingApproval Is Nothing Then
                Using adapter As New AMLDAL.MappingBranchTableAdapters.BranchTypeMapping_PendingApprovalTableAdapter
                    Using otable As AMLDAL.MappingBranch.BranchTypeMapping_PendingApprovalDataTable = adapter.GetDataByBranchTypeMappingPendingApproval(Me.PKPendingID)
                        If otable.Rows.Count > 0 Then
                            _orowBranchTypeMappingPendingApproval = otable.Rows(0)
                            Return _orowBranchTypeMappingPendingApproval
                        Else
                            _orowBranchTypeMappingPendingApproval = Nothing
                            Return _orowBranchTypeMappingPendingApproval
                        End If
                    End Using
                End Using
            Else
                Return _orowBranchTypeMappingPendingApproval
            End If
        End Get
    End Property


    Private Sub loadData()
        Try
            If Not orowBranchTypeMappingPendingApproval Is Nothing Then
                Select Case orowBranchTypeMappingPendingApproval.FK_MsModeID
                    Case Sahassa.AML.Commonly.TypeMode.Add
                        PanelAdd.Visible = True
                        PanelEditNew.Visible = False
                        PanelEditOld.Visible = False
                        PanelDelete.Visible = False
                        If Not orowBranchTypeMappingApprovalDetail Is Nothing Then
                            If Not orowBranchTypeMappingApprovalDetail.IsBranchIdNull Then

                                LabelBranchNameAdd.Text = GetDataBranch(orowBranchTypeMappingApprovalDetail.BranchId)

                            End If
                            If Not orowBranchTypeMappingApprovalDetail.IsBranchTypeIdNull Then
                                LabelBranchTypeAdd.Text = GetDataBranchType(orowBranchTypeMappingApprovalDetail.BranchTypeId)
                            End If
                        End If


                    Case Sahassa.AML.Commonly.TypeMode.Edit
                            PanelAdd.Visible = False
                            PanelEditNew.Visible = True
                            PanelEditOld.Visible = True
                        PanelDelete.Visible = False
                        If Not orowBranchTypeMappingApprovalDetail Is Nothing Then
                            If Not orowBranchTypeMappingApprovalDetail.IsBranchIdNull Then
                                LabelBranchNameEditNew.Text = GetDataBranch(orowBranchTypeMappingApprovalDetail.BranchId)
                            End If
                            If Not orowBranchTypeMappingApprovalDetail.IsBranchId_OldNull Then
                                LabelBranchNameEditOld.Text = GetDataBranch(orowBranchTypeMappingApprovalDetail.BranchId_Old)
                            End If
                            If Not orowBranchTypeMappingApprovalDetail.IsBranchTypeIdNull Then
                                LabelBranchTypeEditNew.Text = GetDataBranchType(orowBranchTypeMappingApprovalDetail.BranchTypeId)
                            End If
                            If Not orowBranchTypeMappingApprovalDetail.IsBranchTypeId_OldNull Then
                                LabelBranchTypeEditOld.Text = GetDataBranchType(orowBranchTypeMappingApprovalDetail.BranchTypeId_Old)
                            End If
                        End If
                    Case Sahassa.AML.Commonly.TypeMode.Delete
                        PanelAdd.Visible = False
                        PanelEditNew.Visible = False
                        PanelEditOld.Visible = False
                        PanelDelete.Visible = True
                        If Not orowBranchTypeMappingApprovalDetail Is Nothing Then
                            If Not orowBranchTypeMappingApprovalDetail.IsBranchIdNull Then
                                LabelBranchnameDelete.Text = GetDataBranch(orowBranchTypeMappingApprovalDetail.BranchId)
                            End If
                            If Not orowBranchTypeMappingApprovalDetail.IsBranchTypeIdNull Then
                                LabelBranchTypeDelete.Text = GetDataBranchType(orowBranchTypeMappingApprovalDetail.BranchTypeId)
                            End If
                        End If
                End Select

            End If
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Sub
    Private Function GetDataBranchType(ByVal branchtypeid As Long) As String
        Try
            Using adapter As New AMLDAL.MappingBranchTableAdapters.BranchTypeTableAdapter
                Using otable As AMLDAL.MappingBranch.BranchTypeDataTable = adapter.GetDataByPK(branchtypeid)
                    If otable.Rows.Count > 0 Then
                        Dim orow As AMLDAL.MappingBranch.BranchTypeRow = otable.Rows(0)
                        If Not orow.IsBranchTypeNameNull Then
                            Return orow.BranchTypeName
                        Else
                            Return ""
                        End If
                    Else
                        Return ""
                    End If

                End Using

            End Using
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Function
    Private Function GetDataBranch(ByVal branchid As Long) As String
        Try
            Using adapter As New AMLDAL.MappingBranchTableAdapters.JHDATATableAdapter
                Using otable As AMLDAL.MappingBranch.JHDATADataTable = adapter.GetDataByPK(branchid)
                    If otable.Rows.Count > 0 Then
                        Dim orow As AMLDAL.MappingBranch.JHDATARow = otable.Rows(0)
                        If Not orow.IsJDNAMENull Then
                            Return orow.JDNAME
                        Else
                            Return ""
                        End If
                    Else
                        Return ""
                    End If
                End Using
            End Using
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                Me.ImageAccept.Attributes.Add("onclick", "javascript: if(confirm('Do you want to accept this activity ?')== false) return false;")
                Me.ImageReject.Attributes.Add("onclick", "javascript: if(confirm('Do you want to reject this activity ?')== false) return false;")
            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            If Not Page.IsPostBack Then
                LabelTitle.Text = "Activity :" & orowBranchTypeMappingPendingApproval.Category & " Branch Type Mapping"
                loadData()
            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    Private Sub InsertAuditTrail(ByRef otrans As SqlTransaction, ByVal oType As Sahassa.AML.Commonly.TypeMode)
        Try
            Select Case oType
                Case Sahassa.AML.Commonly.TypeMode.Add
                    Sahassa.AML.AuditTrailAlert.AuditTrailChecking(2)
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                        SetTransaction(AccessAudit, otrans)
                        AccessAudit.Insert(Now, orowBranchTypeMappingPendingApproval.UserID, Sahassa.AML.Commonly.SessionUserId, "Branch Type Mapping", "BranchID", "Add", "", orowBranchTypeMappingApprovalDetail.BranchId, "Accepted")
                        AccessAudit.Insert(Now, orowBranchTypeMappingPendingApproval.UserID, Sahassa.AML.Commonly.SessionUserId, "Branch Type Mapping", "BranchType", "Add", "", GetDataBranchType(orowBranchTypeMappingApprovalDetail.BranchTypeId), "Accepted")
                    End Using
                Case Sahassa.AML.Commonly.TypeMode.Edit
                    Sahassa.AML.AuditTrailAlert.AuditTrailChecking(2)
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                        SetTransaction(AccessAudit, otrans)
                        AccessAudit.Insert(Now, orowBranchTypeMappingPendingApproval.UserID, Sahassa.AML.Commonly.SessionUserId, "Branch Type Mapping", "BranchID", "Edit", orowBranchTypeMappingApprovalDetail.BranchId_Old, orowBranchTypeMappingApprovalDetail.BranchId, "Accepted")
                        AccessAudit.Insert(Now, orowBranchTypeMappingPendingApproval.UserID, Sahassa.AML.Commonly.SessionUserId, "Branch Type Mapping", "BranchType", "Edit", GetDataBranchType(orowBranchTypeMappingApprovalDetail.BranchTypeId_Old), GetDataBranchType(orowBranchTypeMappingApprovalDetail.BranchTypeId), "Accepted")
                    End Using
                Case Sahassa.AML.Commonly.TypeMode.Delete
                    Sahassa.AML.AuditTrailAlert.AuditTrailChecking(2)
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                        SetTransaction(AccessAudit, otrans)
                        AccessAudit.Insert(Now, orowBranchTypeMappingPendingApproval.UserID, Sahassa.AML.Commonly.SessionUserId, "Branch Type Mapping", "BranchID", "Delete", "", orowBranchTypeMappingApprovalDetail.BranchId, "Accepted")
                        AccessAudit.Insert(Now, orowBranchTypeMappingPendingApproval.UserID, Sahassa.AML.Commonly.SessionUserId, "Branch Type Mapping", "BranchType", "Delete", "", GetDataBranchType(orowBranchTypeMappingApprovalDetail.BranchTypeId), "Accepted")
                    End Using
            End Select
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try

    End Sub
    Private Sub InsertAuditTrailReject(ByRef otrans As SqlTransaction, ByVal oType As Sahassa.AML.Commonly.TypeMode)
        Try
            Select Case oType
                Case Sahassa.AML.Commonly.TypeMode.Add
                    Sahassa.AML.AuditTrailAlert.AuditTrailChecking(2)
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                        otrans = BeginTransaction(AccessAudit)
                        AccessAudit.Insert(Now, orowBranchTypeMappingPendingApproval.UserID, Sahassa.AML.Commonly.SessionUserId, "Branch Type Mapping", "BranchID", "Add", "", orowBranchTypeMappingApprovalDetail.BranchId, "Rejected")
                        AccessAudit.Insert(Now, orowBranchTypeMappingPendingApproval.UserID, Sahassa.AML.Commonly.SessionUserId, "Branch Type Mapping", "BranchType", "Add", "", GetDataBranchType(orowBranchTypeMappingApprovalDetail.BranchTypeId), "Rejected")
                    End Using
                Case Sahassa.AML.Commonly.TypeMode.Edit
                    Sahassa.AML.AuditTrailAlert.AuditTrailChecking(2)
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                        otrans = BeginTransaction(AccessAudit)
                        AccessAudit.Insert(Now, orowBranchTypeMappingPendingApproval.UserID, Sahassa.AML.Commonly.SessionUserId, "Branch Type Mapping", "BranchID", "Edit", orowBranchTypeMappingApprovalDetail.BranchId_Old, orowBranchTypeMappingApprovalDetail.BranchId, "Rejected")
                        AccessAudit.Insert(Now, orowBranchTypeMappingPendingApproval.UserID, Sahassa.AML.Commonly.SessionUserId, "Branch Type Mapping", "BranchType", "Edit", GetDataBranchType(orowBranchTypeMappingApprovalDetail.BranchTypeId_Old), GetDataBranchType(orowBranchTypeMappingApprovalDetail.BranchTypeId), "Rejected")
                    End Using
                Case Sahassa.AML.Commonly.TypeMode.Delete
                    Sahassa.AML.AuditTrailAlert.AuditTrailChecking(2)
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                        otrans = BeginTransaction(AccessAudit)
                        AccessAudit.Insert(Now, orowBranchTypeMappingPendingApproval.UserID, Sahassa.AML.Commonly.SessionUserId, "Branch Type Mapping", "BranchID", "Delete", "", orowBranchTypeMappingApprovalDetail.BranchId, "Rejected")
                        AccessAudit.Insert(Now, orowBranchTypeMappingPendingApproval.UserID, Sahassa.AML.Commonly.SessionUserId, "Branch Type Mapping", "BranchType", "Delete", "", GetDataBranchType(orowBranchTypeMappingApprovalDetail.BranchTypeId), "Rejected")
                    End Using
            End Select
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try

    End Sub
    Private Sub DeletePending(ByRef otrans As SqlTransaction)
        Try
            'Delete detail
            Using adapter As New AMLDAL.MappingBranchTableAdapters.BranchTypeMappingApprovalTableAdapter
                SetTransaction(adapter, otrans)
                adapter.DeleteByFKPendingApprovalID(Me.PKPendingID)
            End Using

            'Delete Master
            Using adapter As New AMLDAL.MappingBranchTableAdapters.BranchTypeMapping_PendingApprovalTableAdapter
                SetTransaction(adapter, otrans)
                adapter.DeleteByPK(Me.PKPendingID)
            End Using
        Catch ex As Exception
            LogError(ex)
            Throw
        End Try
    End Sub
    Private Sub AcceptAdd()
        Dim oTrans As SqlTransaction = Nothing
        Try
            'insert ke branchtypemapping
            Using adapter As New AMLDAL.MappingBranchTableAdapters.BranchTypeMappingTableAdapter
                oTrans = BeginTransaction(adapter)
                If Not orowBranchTypeMappingApprovalDetail Is Nothing Then
                    adapter.Insert(orowBranchTypeMappingApprovalDetail.BranchTypeId, orowBranchTypeMappingApprovalDetail.BranchId)
                End If

            End Using
            'insert audittrail
            InsertAuditTrail(oTrans, Sahassa.AML.Commonly.TypeMode.Add)
            'deletePending
            DeletePending(oTrans)
            oTrans.Commit()
        Catch ex As Exception
            If Not oTrans Is Nothing Then
                oTrans.Rollback()
            End If
            LogError(ex)
            Throw
        Finally
            If Not oTrans Is Nothing Then
                oTrans.Dispose()
            End If
        End Try
    End Sub
    Private Sub AcceptEdit()
        Dim oTrans As SqlTransaction = Nothing
        Try
            'Edit ke branchtypemapping
            Using adapter As New AMLDAL.MappingBranchTableAdapters.BranchTypeMappingTableAdapter
                oTrans = BeginTransaction(adapter)
                If Not orowBranchTypeMappingApprovalDetail Is Nothing Then
                    adapter.UpdateBranchTypeMappingByPk(orowBranchTypeMappingApprovalDetail.BranchTypeId, orowBranchTypeMappingApprovalDetail.BranchId, orowBranchTypeMappingApprovalDetail.BranchTypeMappingId)
                End If

            End Using
            'insert audittrail
            InsertAuditTrail(oTrans, Sahassa.AML.Commonly.TypeMode.Edit)
            'deletePending
            DeletePending(oTrans)
            oTrans.Commit()
        Catch ex As Exception
            If Not oTrans Is Nothing Then
                oTrans.Rollback()
            End If
            LogError(ex)
            Throw
        Finally
            If Not oTrans Is Nothing Then
                oTrans.Dispose()
            End If
        End Try
    End Sub
    Private Sub AcceptDelete()
        Dim oTrans As SqlTransaction = Nothing
        Try
            'Edit ke branchtypemapping
            Using adapter As New AMLDAL.MappingBranchTableAdapters.BranchTypeMappingTableAdapter
                oTrans = BeginTransaction(adapter)
                If Not orowBranchTypeMappingApprovalDetail Is Nothing Then
                    adapter.DeleteBranchTypeMappingByPK(orowBranchTypeMappingApprovalDetail.BranchTypeMappingId)
                End If
            End Using
            'insert audittrail
            InsertAuditTrail(oTrans, Sahassa.AML.Commonly.TypeMode.Delete)
            'deletePending
            DeletePending(oTrans)
            oTrans.Commit()
        Catch ex As Exception
            If Not oTrans Is Nothing Then
                oTrans.Rollback()
            End If
            LogError(ex)
            Throw
        Finally
            If Not oTrans Is Nothing Then
                oTrans.Dispose()
            End If
        End Try
    End Sub
    Protected Sub ImageBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click
        Response.Redirect("BranchTypeMappingApproval.aspx", False)
    End Sub
    Protected Sub ImageAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click
        Try
            If Not orowBranchTypeMappingPendingApproval Is Nothing Then
                Select Case orowBranchTypeMappingPendingApproval.FK_MsModeID
                    Case Sahassa.AML.Commonly.TypeMode.Add
                        AcceptAdd()
                    Case Sahassa.AML.Commonly.TypeMode.Edit
                        AcceptEdit()
                    Case Sahassa.AML.Commonly.TypeMode.Delete
                        AcceptDelete()
                End Select
                Response.Redirect("BranchTypeMappingApproval.aspx", False)
            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub RejectAdd()
        Dim oTrans As SqlTransaction = Nothing
        Try
            'insert audittrail
            InsertAuditTrailReject(oTrans, Sahassa.AML.Commonly.TypeMode.Add)
            'deletePending
            DeletePending(oTrans)
            oTrans.Commit()
        Catch ex As Exception
            If Not oTrans Is Nothing Then
                oTrans.Rollback()
            End If
            LogError(ex)
            Throw
        Finally
            If Not oTrans Is Nothing Then
                oTrans.Dispose()
            End If

        End Try
    End Sub
    Private Sub RejectEdit()
        Dim oTrans As SqlTransaction = Nothing
        Try
            'insert audittrail
            InsertAuditTrailReject(oTrans, Sahassa.AML.Commonly.TypeMode.Edit)
            'deletePending
            DeletePending(oTrans)
            oTrans.Commit()
        Catch ex As Exception
            If Not oTrans Is Nothing Then
                oTrans.Rollback()
            End If
            LogError(ex)
            Throw
        Finally
            If Not oTrans Is Nothing Then
                oTrans.Dispose()
            End If

        End Try
    End Sub
    Private Sub RejectDelete()
        Dim oTrans As SqlTransaction = Nothing
        Try
            'insert audittrail
            InsertAuditTrailReject(oTrans, Sahassa.AML.Commonly.TypeMode.Delete)
            'deletePending
            DeletePending(oTrans)
            oTrans.Commit()
        Catch ex As Exception
            If Not oTrans Is Nothing Then
                oTrans.Rollback()
            End If
            LogError(ex)
            Throw
        Finally
            If Not oTrans Is Nothing Then
                oTrans.Dispose()
            End If

        End Try
    End Sub
    Protected Sub ImageReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click
        Try
            If Not orowBranchTypeMappingPendingApproval Is Nothing Then
                Select Case orowBranchTypeMappingPendingApproval.FK_MsModeID
                    Case Sahassa.AML.Commonly.TypeMode.Add
                        RejectAdd()
                    Case Sahassa.AML.Commonly.TypeMode.Edit
                        RejectEdit()
                    Case Sahassa.AML.Commonly.TypeMode.Delete
                        RejectDelete()
                End Select
                Response.Redirect("BranchTypeMappingApproval.aspx", False)
            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
End Class
