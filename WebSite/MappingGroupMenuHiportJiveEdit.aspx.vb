Option Strict On
Option Explicit On
Imports amlbll
Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports SahassaNettier.Services
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Imports System.IO
Imports System.Collections.Generic

Partial Class MappingGroupMenuHiportJiveEdit
    Inherits Parent

    Public ReadOnly Property PK_MappingGroupMenuHIPORTJIVE_ID() As Integer
        Get
            Dim strTemp As String = Request.Params("PK_MappingGroupMenuHIPORTJIVE_ID")
            Dim intResult As Integer
            If Integer.TryParse(strTemp, intResult) Then
                Return intResult
            Else
                cvalPageError.IsValid = False
                cvalPageError.ErrorMessage = "Parameter PK_MappingGroupMenuHIPORTJIVE_ID is invalid."

            End If
        End Get
    End Property

    Public ReadOnly Property Objvw_MappingGroupMenuHIPORTJIVE() As VList(Of vw_MappingGroupMenuHiportJive)
        Get
            If Session("Objvw_MappingGroupMenuHIPORTJIVE.MappingGroupMenuHIPORTJIVEEdit") Is Nothing Then

                Session("Objvw_MappingGroupMenuHIPORTJIVE.MappingGroupMenuHIPORTJIVEEdit") = DataRepository.vw_MappingGroupMenuHiportJiveProvider.GetPaged("PK_MappingGroupMenuHIPORTJIVE_ID = '" & PK_MappingGroupMenuHIPORTJIVE_ID & "'", "", 0, Integer.MaxValue, 0)

            End If
            Return CType(Session("Objvw_MappingGroupMenuHIPORTJIVE.MappingGroupMenuHIPORTJIVEEdit"), VList(Of vw_MappingGroupMenuHiportJive))
        End Get
    End Property

    Public Property SetnGetMode() As String
        Get
            If Not Session("MappingGroupMenuHIPORTJIVEEdit.Mode") Is Nothing Then
                Return CStr(Session("MappingGroupMenuHIPORTJIVEEdit.Mode"))
            Else
                Return ""

            End If
        End Get
        Set(ByVal value As String)
            Session("MappingGroupMenuHIPORTJIVEEdit.Mode") = value
        End Set
    End Property

    Private Sub LoadMode()
        Session("MappingGroupMenuHIPORTJIVEEdit.Mode") = Nothing
        If Objvw_MappingGroupMenuHIPORTJIVE.Count > 0 Then

            CboGroupMenu.Items.Clear()
            Using ObjHIPORTJIVEBll As New AMLBLL.MappingGroupMenuHiportJiveBLL
                CboGroupMenu.AppendDataBoundItems = True
                CboGroupMenu.DataSource = ObjHIPORTJIVEBll.GeModeDataEdit
                CboGroupMenu.DataTextField = GroupColumn.GroupName.ToString
                CboGroupMenu.DataValueField = GroupColumn.GroupID.ToString
                CboGroupMenu.DataBind()
                CboGroupMenu.Items.Insert(0, New ListItem("...", ""))
                'CboGroupMenu.Items.Insert(1, New ListItem(Objvw_MappingGroupMenuHIPORTJIVE(0).GroupName, CStr(Objvw_MappingGroupMenuHIPORTJIVE(0).FK_Group_ID)))
            End Using
        Else
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = MappingGroupMenuHiportJiveBLL.DATA_NOTVALID
        End If

    End Sub

    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim oTrans As SqlTransaction = Nothing
        Try

            Using ObjList As MappingGroupMenuHiportJive = DataRepository.MappingGroupMenuHiportJiveProvider.GetByPK_MappingGroupMenuHiportJive_ID(PK_MappingGroupMenuHIPORTJIVE_ID)
                If ObjList Is Nothing Then
                    Throw New AMLBLL.SahassaException(MappingGroupMenuHiportJiveEnum.DATA_NOTVALID)
                End If
            End Using

            Dim GroupID As Integer
            If CboGroupMenu.Text = "" Then
                GroupID = 0
            Else
                GroupID = CInt(CboGroupMenu.Text)
            End If

            Dim GroupName As String = ""
            If GroupID > 0 Then

                AMLBLL.MappingGroupMenuHiportJiveBLL.IsDataValidEditApproval(CStr(GroupID))

                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                    Using Objlist As TList(Of MappingGroupMenuHiportJive) = DataRepository.MappingGroupMenuHiportJiveProvider.GetPaged("FK_Group_ID = '" & GroupID & "'", "", 0, Integer.MaxValue, 0)
                        If Objlist.Count > 0 Then
                            Sahassa.AML.Commonly.SessionIntendedPage = "MappingGroupMenuHIPORTJIVEView.aspx"
                            Me.Response.Redirect("MappingGroupMenuHIPORTJIVEView.aspx", False)
                        Else
                            Using ObjMappingGroupMenuHIPORTJIVENEW As New MappingGroupMenuHiportJive
                                Using ObjMappingGroupMenuHIPORTJIVE As MappingGroupMenuHiportJive = DataRepository.MappingGroupMenuHiportJiveProvider.GetByPK_MappingGroupMenuHiportJive_ID(PK_MappingGroupMenuHIPORTJIVE_ID)
                                    Using ObjGroup As Group = DataRepository.GroupProvider.GetByGroupID(CInt(ObjMappingGroupMenuHIPORTJIVE.FK_Group_ID))
                                        Using ObjGroup1 As Group = DataRepository.GroupProvider.GetByGroupID(CInt(CboGroupMenu.Text))
                                            AMLBLL.MappingGroupMenuHiportJiveBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "MappingGroupMenuHIPORTJIVE", "GroupName", "Edit", ObjGroup.GroupName, ObjGroup1.GroupName, "Acc")
                                        End Using
                                    End Using
                                    DataRepository.MappingGroupMenuHiportJiveProvider.Delete(ObjMappingGroupMenuHIPORTJIVE)
                                End Using
                                ObjMappingGroupMenuHIPORTJIVENEW.FK_Group_ID = GroupID
                                DataRepository.MappingGroupMenuHiportJiveProvider.Save(ObjMappingGroupMenuHIPORTJIVENEW)
                                Sahassa.AML.Commonly.SessionIntendedPage = "MappingGroupMenuHIPORTJIVEView.aspx"
                                Me.Response.Redirect("MappingGroupMenuHIPORTJIVEView.aspx", False)
                            End Using
                        End If
                    End Using
                Else
                    Using ObjMappingGroupMenuHIPORTJIVE_Approval As New MappingGroupMenuHiportJive_Approval

                        Using objgroup As TList(Of Group) = DataRepository.GroupProvider.GetPaged("GroupID = '" & GroupID & "'", "", 0, Integer.MaxValue, 0)
                            GroupName = objgroup(0).GroupName

                            ObjMappingGroupMenuHIPORTJIVE_Approval.FK_MsUserID = Sahassa.AML.Commonly.SessionPkUserId
                            ObjMappingGroupMenuHIPORTJIVE_Approval.GroupMenu = GroupName
                            ObjMappingGroupMenuHIPORTJIVE_Approval.FK_ModeID = CInt(Sahassa.AML.Commonly.TypeMode.Edit)
                            ObjMappingGroupMenuHIPORTJIVE_Approval.CreatedDate = Now
                            DataRepository.MappingGroupMenuHiportJive_ApprovalProvider.Save(ObjMappingGroupMenuHIPORTJIVE_Approval)

                            Using ObjMappingGroupMenuHIPORTJIVE As MappingGroupMenuHiportJive = DataRepository.MappingGroupMenuHiportJiveProvider.GetByPK_MappingGroupMenuHiportJive_ID(PK_MappingGroupMenuHIPORTJIVE_ID)
                                Using ObjMappingGroupMenuHIPORTJIVE_approvalDetail As New MappingGroupMenuHiportJive_ApprovalDetail
                                    ObjMappingGroupMenuHIPORTJIVE_approvalDetail.FK_MappingGroupMenuHiportJive_Approval_ID = ObjMappingGroupMenuHIPORTJIVE_Approval.PK_MappingGroupMenuHiportJive_Approval_ID
                                    ObjMappingGroupMenuHIPORTJIVE_approvalDetail.PK_MappingGroupMenuHiportJive_ID = PK_MappingGroupMenuHIPORTJIVE_ID
                                    ObjMappingGroupMenuHIPORTJIVE_approvalDetail.FK_Group_ID = GroupID
                                    ObjMappingGroupMenuHIPORTJIVE_approvalDetail.PK_MappingGroupMenuHiportJive_ID_Old = PK_MappingGroupMenuHIPORTJIVE_ID
                                    ObjMappingGroupMenuHIPORTJIVE_approvalDetail.FK_Group_ID_Old = ObjMappingGroupMenuHIPORTJIVE.FK_Group_ID
                                    'ObjMappingGroupMenuHIPORTJIVE_approvalDetail.PK_MappingGroupMenuHIPORTJIVE_ID_Old = ObjMappingGroupMenuHIPORTJIVE.PK_MappingGroupMenuHIPORTJIVE_ID
                                    Using ObjGroup2 As Group = DataRepository.GroupProvider.GetByGroupID(CInt(ObjMappingGroupMenuHIPORTJIVE.FK_Group_ID))
                                        Using ObjGroup1 As Group = DataRepository.GroupProvider.GetByGroupID(CInt(CboGroupMenu.Text))
                                            AMLBLL.MappingGroupMenuHiportJiveBLL.AddAuditTrail(Sahassa.AML.Commonly.SessionUserId, "MappingGroupMenuHIPORTJIVE", "GroupName", "Edit", ObjGroup2.GroupName, ObjGroup1.GroupName, "PendingApproval")
                                        End Using
                                    End Using
                                    DataRepository.MappingGroupMenuHiportJive_ApprovalDetailProvider.Save(ObjMappingGroupMenuHIPORTJIVE_approvalDetail)
                                End Using
                            End Using
                        End Using
                        'Sahassa.AML.Commonly.SessionIntendedPage = "MappingGroupMenuHIPORTJIVEView.aspx"
                        'Me.Response.Redirect("MappingGroupMenuHIPORTJIVEView.aspx", False)
                        Using ObjGroup As TList(Of Group) = DataRepository.GroupProvider.GetPaged("GroupID = '" & GroupID & "'", "", 0, Integer.MaxValue, 0)
                            Me.LblSuccess.Text = "Data saved successfully and waiting for Approval."
                            Me.LblSuccess.Visible = True
                            LoadMode()
                        End Using

                    End Using
                End If
                MultiView1.ActiveViewIndex = 1
            Else
                Me.LblSuccess.Text = "Please select Group Menu"
                Me.LblSuccess.Visible = True
            End If

        Catch ex As Exception
            If Not oTrans Is Nothing Then oTrans.Rollback()
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not oTrans Is Nothing Then
                oTrans.Dispose()
                oTrans = Nothing
            End If
        End Try
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "MappingGroupMenuHIPORTJIVEView.aspx"
            Me.Response.Redirect("MappingGroupMenuHIPORTJIVEView.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Session("Objvw_MappingGroupMenuHIPORTJIVE.MappingGroupMenuHIPORTJIVEEdit") = Nothing
            'Using Transcope As New Transactions.TransactionScope
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                '    Transcope.Complete()
            End Using
            If Not Page.IsPostBack Then
                MultiView1.ActiveViewIndex = 0
                LoadMode()
            End If
            'End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub btnOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnOK.Click
        Try
            Sahassa.AML.Commonly.SessionIntendedPage = "MappingGroupMenuHIPORTJIVEView.aspx"
            Me.Response.Redirect("MappingGroupMenuHIPORTJIVEView.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
End Class

