<%@ Page Language="VB" MasterPageFile="~/masterpage.master" AutoEventWireup="false" CodeFile="CashTransactionReportUpdateConfirmation.aspx.vb" Inherits="CashTransactionReportUpdateConfirmation" title="Update Confirmation Number" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
<table id="title"  border="2" cellpadding="2" cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr>

            <td colspan="3" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>CTR Update Confirmation Number
                </strong>
            </td>
        </tr>
    </table>
    <br />
<table>
    <tr>
    <td>
        <asp:Label ID="label_PKCTRID" runat="server"   Width="150px">PKCTR ID:</asp:Label>
    </td>
    <td style="width: 100%">
        <asp:TextBox ID="textbox_PKCTRID" runat="server" Enabled="false" BorderStyle="none" Width="100%"></asp:TextBox>
    </td>
    </tr>
    
        <tr>
        <td>
            <asp:Label ID="label_CTRDescription" runat="server"   Width="150px">CTR Description:</asp:Label>
        </td>
        <td>
            <asp:TextBox ID="textbox_CTRDescription" runat="server" Enabled="false" BorderStyle="none" Width="100%"></asp:TextBox>
        </td>
        </tr>
    
        <tr>
        <td>
            <asp:Label ID="label_CreatedDate" runat="server"   Width="150px">Created Date:</asp:Label>
        </td>
        <td style="width: 100%">
            <asp:TextBox ID="textbox_CreatedDate" runat="server" Enabled="false" BorderStyle="none" Width="100%"></asp:TextBox>
        </td>
        </tr>
        
    <tr>
    <td>
        <asp:Label ID="label_LastUpdated" runat="server"   Width="150px">Last Updated:</asp:Label>
    </td>
    <td style="width: 100%">
        <asp:TextBox ID="textbox_LastUpdated" runat="server" Enabled="false" BorderStyle="none" Width="100%"></asp:TextBox>
    </td>
    </tr>
    
        <tr>
        <td>
            <asp:Label ID="label_FKAccountOwnerName" runat="server"   Width="150px">Account Owner Name:</asp:Label>
        </td>
        <td style="width: 100%">
            <asp:TextBox ID="textbox_FKAccountOwnerName" runat="server" Enabled="false" BorderStyle="none" Width="100%"></asp:TextBox>
        </td>
        </tr>
        
    <tr>
    <td>
        <asp:Label ID="label_pic" runat="server"   Width="150px">PIC:</asp:Label>
    </td>
    <td style="width: 100%">
        <asp:TextBox ID="textbox_pic" runat="server" Enabled="false" BorderStyle="none" Width="100%"></asp:TextBox>
    </td>
    </tr>
    
        <tr>
        <td>
            <asp:Label ID="label_ReportedDate" runat="server"   Width="150px">Reported Date:</asp:Label>
        </td>
        <td style="width: 100%">
            <asp:TextBox ID="textbox_ReportedDate" runat="server" Enabled="false" BorderStyle="none" Width="100%"></asp:TextBox>
        </td>
        </tr>
        
    <tr>
    <td>
        <asp:Label ID="label_ReportedBy" runat="server"   Width="150px">Reported By:</asp:Label>
    </td>
    <td style="width: 100%">
        <asp:TextBox ID="textbox_ReportedBy" runat="server" Enabled="False" BorderStyle="none" Width="25%"></asp:TextBox>
    </td>
    </tr>
    
            <tr>
            <td>
                <asp:Label ID="label_PPATKConfirmationNo" runat="server"   Width="150px">PPATK Confirmation No:</asp:Label>
            </td>
            <td style="width: 100%">
                <asp:TextBox ID="textbox_PPATKConfirmationNo" runat="server" BorderColor="Black" BorderWidth="1px"  Width="25%"></asp:TextBox>
            </td>
            </tr>
            
    <tr>
    <td style="height: 14px">
    </td>
    <td style="width: 100%; height: 14px;">
    </td>
    </tr>
    
        <tr>
        <td align="right">
            <asp:Button ID="button_submit" runat="server" text="Submit"/>
        </td>
        <td align="left" style="width: 100%">
            <asp:Button ID="button_cancel" runat="server" Text="Cancel" />
        </td>
        </tr>
</table>

</asp:Content>
