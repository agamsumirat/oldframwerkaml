<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="MappingUserIdHiportJiveApprovalDetail.aspx.vb" Inherits="MappingUserIdHiportJiveApprovalDetail"  %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">

<ajax:AjaxPanel runat="server" ID="Ajax1" Width="971px">
<table cellSpacing="4" cellPadding="0" width="100%">
		<tr>
			<td vAlign="bottom" align="left"><IMG height="15" src="images/dot_title.gif" width="15"></td>
			<td class="maintitle" vAlign="bottom" width="99%"><asp:label id="LabelTitle" runat="server"></asp:label></td>
		</tr>
	</table>
    <table style="width: 100%">
        <tr>
            <td style="width: 200px; height: 15px;" valign="top">
                <asp:Label ID="LblMode" runat="server" Text="Mode"></asp:Label></td>
            <td style="width: 1px; height: 15px;" valign="top">
                :</td>
            <td valign="top" style="height: 15px">
                <asp:Label ID="LblAction" runat="server"></asp:Label></td>
        </tr>
    </table>
    <table style="width: 100%">
        <tr>
            <td valign="top">
                <asp:Panel ID="PanelOld" runat="server" GroupingText="Old Value" Width="100%">
                    <table style="width: 100%">
                        
                        <tr>
                            <td style="width: 20%" valign="top">
                                <asp:Label ID="Label14" runat="server" Text="User ID Old"></asp:Label></td>
                            <td style="width: 1px" valign="top">
                                :</td>
                            <td style="width: 80%" valign="top">
                                <asp:Label ID="LblFK_User_ID_Old" runat="server"></asp:Label></td>
                        </tr>
                         <tr>
                            <td style="width: 20%" valign="top">
                                <asp:Label ID="Label1" runat="server" Text="Kode AO OLd"></asp:Label></td>
                            <td style="width: 1px" valign="top">
                                :</td>
                            <td style="width: 80%" valign="top">
                                <asp:Label ID="KodeAO_OLD" runat="server"></asp:Label></td>
                        </tr>
                        
                     </table>
                </asp:Panel>
            </td>
            <td valign="top">
                <asp:Panel ID="PanelNew" runat="server" GroupingText="New Value" Width="100%">
                    <table style="width: 100%">
                       
                        <tr>
                            <td style="width: 20%" valign="top">
                                <asp:Label ID="Label16" runat="server" Text="User ID New"></asp:Label></td>
                            <td style="width: 1px" valign="top">
                                :</td>
                            <td style="width: 80%" valign="top">
                                <asp:Label ID="LblFK_User_ID_New" runat="server"></asp:Label></td>
                        </tr>
                         <tr>
                            <td style="width: 20%; height: 15px;" valign="top">
                                <asp:Label ID="Label3" runat="server" Text="Kode AO New"></asp:Label></td>
                            <td style="width: 1px; height: 15px;" valign="top">
                                :</td>
                            <td style="width: 80%; height: 15px;" valign="top">
                                <asp:Label ID="KodeAO_New" runat="server"></asp:Label></td>
                        </tr>
                       
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td colspan="2" valign="top">
                <asp:ImageButton ID="ImageAccept" runat="server" CausesValidation="False" SkinID="AcceptButton" /><asp:ImageButton
                    ID="ImageReject" runat="server" CausesValidation="False" SkinID="RejectButton" /><asp:ImageButton
                        ID="ImageBack" runat="server" CausesValidation="False" SkinID="BackButton" /><asp:CustomValidator
                            ID="cvalPageError" runat="server" Display="none"></asp:CustomValidator></td>
        </tr>
    </table>
  </ajax:AjaxPanel>
</asp:Content>





