<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="GroupInsiderCodeMapping.aspx.vb" Inherits="GroupInsiderCodeMapping" title="Group Insider Code Mapping" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
    <table id="title"  border="2" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr>
            <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Group Insider Code Mapping&nbsp;
                    <hr />
                </strong>
                <asp:Label ID="LblSuccess" runat="server" CssClass="validationok" Visible="False"
                    Width="100%"></asp:Label></td>
        </tr>
    </table>
    	<TABLE borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2">
        <TR>
            <td bgcolor="#ffffff" colspan="3" nowrap="nowrap" valign="middle">
                <ajax:ajaxpanel id="AjaxPanel1" runat="server" Width="346px">
                    Group Name :
                                <asp:dropdownlist id="DropdownlistGroup" tabIndex="1" runat="server" Width="224px" CssClass="searcheditcbo" AutoPostBack="True"></asp:dropdownlist>&nbsp;
                    <br />
                    <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></ajax:ajaxpanel>
                                &nbsp;</td>
			</TR>
	</TABLE>
	
<table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
		border="2">
  <tr> 
    <td bgColor="#ffffff">
        By default each Group has access to regular customers' account in the bank.
        <br />
        However, if you wish to give a certain Group access to the additional Insider Code
        then choose from the following list.<br />
        &nbsp;<br />
        <ajax:ajaxpanel id="AjaxPanel5" runat="server" Width="265px">
   <%-- <asp:Panel ID="PanelGroupInsiderCodeMapping" runat="server" GroupingText="Select Insider Code for the chosen Group :"
            Width="125px" height="50px" Wrap="False">      --%>  Select Insider Code for the chosen Group :<br />
        <br />
            <asp:datagrid id="GridMSUserView" runat="server" AutoGenerateColumns="False"
						Font-Size="XX-Small" BackColor="White" CellPadding="4" BorderWidth="0px" BorderStyle="None" width="100%" GridLines="None" BorderColor="White"
						ForeColor="Black" ShowHeader="False">
                    <FooterStyle BackColor="#CCCC99" />
                    <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Mode="NumericPages"
                        Visible="False" />
                    <AlternatingItemStyle BackColor="White" />
                    <ItemStyle BackColor="White" />
                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" Font-Size="XX-Small" ForeColor="White" />
                    <Columns>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:CheckBox ID="CheckBoxGroupInsiderCodeMapping" runat="server" />
                            </ItemTemplate>
                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                Font-Underline="False" HorizontalAlign="Left" />
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="InsiderCodeId" Visible="False"></asp:BoundColumn>
                        <asp:BoundColumn DataField="InsiderCodeName" Visible="False"></asp:BoundColumn>
                        <asp:BoundColumn DataField="InsiderCodeIDAndName" HeaderText="InsiderCodeID And Name">
                            <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                    Font-Underline="False" Wrap="False" ForeColor="White" />
                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                Font-Underline="False" HorizontalAlign="Left" />
                        </asp:BoundColumn>
                    </Columns>
                </asp:DataGrid><%-- </asp:Panel>--%></ajax:AjaxPanel></td>
  </tr>
  <tr> 
    <td style="background-color:#ffffff"><table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td nowrap colspan="3"><ajax:ajaxpanel id="AjaxPanel6" runat="server">&nbsp;<asp:CheckBox ID="CheckBoxSelectAll" runat="server" Text=" Select All" AutoPostBack="True" />
			  &nbsp; </ajax:ajaxpanel> &nbsp; &nbsp;&nbsp;</td>
		</tr>
      </table></td>
  </tr>
  <tr> 
      <td bgColor="#ffffff">   
       <table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
	    border="2">
        	 <tr class="formText" bgColor="#dddddd" height="30">
		    <td width="15"><IMG height="15" src="images/arrow.gif" width="15"></td>
		    <td colSpan="3">
			    <table cellSpacing="0" cellPadding="3" border="0">
				    <tr>
					    <td><asp:imagebutton id="SaveButton" runat="server" CausesValidation="True" SkinID="SaveButton"></asp:imagebutton></td>
					    <td><asp:imagebutton id="CancelButton" runat="server" CausesValidation="False" SkinID="CancelButton"></asp:imagebutton></td>
				    </tr>
			    </table>
                </td>
	    </tr>
    </table>
	<script language="javascript">
	    document.getElementById('<%=GetFocusID %>').focus();
	</script>
</asp:Content>