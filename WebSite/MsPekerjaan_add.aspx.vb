#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports System.Collections.Generic
#End Region

Partial Class MsPekerjaan_add
	Inherits Parent



#Region "Function"

	''' <summary>
	''' Validasi men-handle seluruh syarat2 data valid
	''' data yang lolos dari validasi dipastikan lolos ke database 
	''' karena menggunakan FillorNothing
	''' </summary>
	''' <remarks></remarks>
	Private Sub ValidasiControl()
	
	  '======================== Validasi textbox :  ID canot Null ====================================================
 If ObjectAntiNull(txtIDPekerjaan.Text) = False Then Throw New Exception("ID  must be filled  ")
        If Not IsNumeric(Me.txtIDPekerjaan.Text) Then
            Throw New Exception("ID must be in number format")
        End If
  '======================== Validasi textbox :  Nama canot Null ====================================================
 If ObjectAntiNull(txtNamaPekerjaan.Text) = False Then Throw New Exception("Nama  must be filled  ")


		If Not DataRepository.MsPekerjaanProvider.GetByIDPekerjaan(txtIDPekerjaan.Text) Is Nothing Then
			Throw New Exception("ID already exist in the database")
		End If

		If DataRepository.MsPekerjaan_ApprovalDetailProvider.GetPaged(MsPekerjaan_ApprovalDetailColumn.IDPekerjaan.ToString & "=" & txtIDPekerjaan.Text, "", 0, 1, Nothing).Count > 0 Then
			Throw New Exception("Data exist in List Waiting Approval")
		End If

	   
		'If LBMapping.Items.Count = 0 Then Throw New Exception("data doesnt have list Mapping")
	End Sub

#End Region

#Region "events..."
    Protected Sub ImgBack_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImgBack.Click
        Response.Redirect("MsPekerjaan_View.aspx")
    End Sub
	Protected Sub imgBrowse_click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles imgBrowse.Click
		Try
			If Not Get_DataFromPicker Is Nothing Then
				Listmaping.AddRange(Get_DataFromPicker)
			End If
		Catch ex As Exception
			LogError(ex)
			Me.CvalPageErr.IsValid = False
			Me.CvalPageErr.ErrorMessage = ex.Message
		End Try
	End Sub

	Protected Sub imgDeleteItem_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles imgDeleteItem.Click
		Try
			If LBMapping.SelectedIndex < 0 Then
				Throw New Exception("No data Selected")
			End If
			Listmaping.RemoveAt(LBMapping.SelectedIndex)

		Catch ex As Exception
			LogError(ex)
			CvalPageErr.IsValid = False
			CvalPageErr.ErrorMessage = ex.Message
		End Try

	End Sub

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Not Page.IsPostBack Then
			Try
                'AuditTrailBLL.InsertAuditTrailUserAccess(Sahassa.CommonCTRWeb.Commonly.SessionCurrentPage)
                Listmaping = New TList(Of MappingMsPekerjaanNCBSPPATK)
			Catch ex As Exception
				LogError(ex)
				CvalPageErr.IsValid = False
				CvalPageErr.ErrorMessage = ex.Message
			End Try
		End If

	End Sub

	

	Protected Sub ImgBtnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnSave.Click
		Try
			Page.Validate("handle")
			If Page.IsValid Then
				ValidasiControl()
				'create audittrailtype
                'Dim objAuditTrailType As MsAuditTrailEntityType = DataRepository.MsAuditTrailEntityTypeProvider.GetByPk_MsAuditTrailEntityTypeId(18)
				' =========== Insert Header Approval
				Dim KeyHeaderApproval As Integer
                Using ObjMsNamaPekerjaan_Approval As New MsPekerjaan_Approval
                    With ObjMsNamaPekerjaan_Approval
                        FillOrNothing(.FK_MsMode_Id, 1, True, oInt)
                        FillOrNothing(.RequestedBy, SessionPkUserId)
                        FillOrNothing(.RequestedDate, Date.Now)
                        .IsUpload = False
                    End With
                    DataRepository.MsPekerjaan_ApprovalProvider.Save(ObjMsNamaPekerjaan_Approval)
                    KeyHeaderApproval = ObjMsNamaPekerjaan_Approval.PK_MsPekerjaan_Approval_Id
                End Using
				'============ Insert Detail Approval
				Using objMsPekerjaan_ApprovalDetail As New MsPekerjaan_ApprovalDetail()
					With objMsPekerjaan_ApprovalDetail
						FillOrNothing(.IDPekerjaan, txtIDPekerjaan.Text,true,Oint)
FillOrNothing(.NamaPekerjaan, txtNamaPekerjaan.Text,true,Ovarchar)

                        FillOrNothing(.Activation, 1, True, oBit)
                       	FillOrNothing(.CreatedDate, Date.Now)
						FillOrNothing(.CreatedBy, SessionUserId)
                        FillOrNothing(.FK_MsPekerjaan_Approval_Id, KeyHeaderApproval)
					End With
                    DataRepository.MsPekerjaan_ApprovalDetailProvider.Save(objMsPekerjaan_ApprovalDetail)
                    Dim keyHeaderApprovalDetail As Integer = objMsPekerjaan_ApprovalDetail.PK_MsPekerjaan_ApprovalDetail_Id
					'Insert auditrail
                    'AuditTrailBLL.InsertAuditTrail(AuditTrailOperation.Add, objAuditTrailType, Now(), Sahassa.CommonCTRWeb.Commonly.SessionNIK, Sahassa.CommonCTRWeb.Commonly.SessionStaffName, "Request for approval inserted  data MsNamaPekerjaan", objMsNamaPekerjaan_ApprovalDetail, Nothing)
					'========= Insert mapping item 
                    Dim LobjMappingMsNamaPekerjaanPPATK_Approval_Detail As New TList(Of MappingMsPekerjaanNCBSPPATK_Approval_Detail)
                    For Each objMappingMsNamaPekerjaanPPATK As MappingMsPekerjaanNCBSPPATK In Listmaping
                        Dim Mapping As New MappingMsPekerjaanNCBSPPATK_Approval_Detail
                        With Mapping
                            FillOrNothing(.IdPekerjaan, objMsPekerjaan_ApprovalDetail.IDPekerjaan)
                            FillOrNothing(.IDPekerjaanNCBS, objMappingMsNamaPekerjaanPPATK.IdPekerjaanNCBS)
                            FillOrNothing(.PK_MsPekerjaan_approval_id, KeyHeaderApproval)
                            FillOrNothing(.PK_MappingMsPekerjaanNCBSPPATK_Approval_detail_ID, keyHeaderApprovalDetail)
                            FillOrNothing(.Nama, objMappingMsNamaPekerjaanPPATK.Nama)
                            LobjMappingMsNamaPekerjaanPPATK_Approval_Detail.Add(Mapping)
                        End With
                    Next
                    DataRepository.MappingMsPekerjaanNCBSPPATK_Approval_DetailProvider.Save(LobjMappingMsNamaPekerjaanPPATK_Approval_Detail)
					'session Maping item Clear
					Listmaping.Clear()
					LblConfirmation.Text = "MsPekerjaan data is waiting approval for Insert"
					MtvMsUser.ActiveViewIndex = 1
					ImgBtnSave.Visible = False
                    ImgBackAdd.Visible = False
				End Using
			End If
		Catch ex As Exception
			LogError(ex)
			CvalPageErr.IsValid = False
			CvalPageErr.ErrorMessage = ex.Message
		End Try

	End Sub

	Protected Sub ImgBtnAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAdd.Click
		Try
			ClearAllControl(VwAdd)
			MtvMsUser.ActiveViewIndex = 0
            ImgBtnSave.Visible = True
            ImgBackAdd.Visible = True
		Catch ex As Exception
			LogError(ex)
			CvalPageErr.IsValid = False
			CvalPageErr.ErrorMessage = ex.Message
		End Try
	End Sub

	Protected Sub ImgBackAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBackAdd.Click
		Response.Redirect("MsPekerjaan_View.aspx")
	End Sub

	Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
		Try
			BindListMapping()
		Catch ex As Exception
			LogError(ex)
			CvalPageErr.IsValid = False
			CvalPageErr.ErrorMessage = ex.Message
		End Try
	End Sub

    Private Sub BindListMapping()
        LBMapping.DataSource = ListMappingDisplay
        LBMapping.DataBind()
    End Sub

#End Region

#Region "Property..."
    ''' <summary>
    ''' Mengambil Data dari Picker
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Get_DataFromPicker As TList(Of MappingMsPekerjaanNCBSPPATK)
        Get
            Dim Temp As New TList(Of MappingMsPekerjaanNCBSPPATK)
            Dim L_msPekerjaanNCBS As TList(Of msPekerjaanNCBS)
            Try
                If Session("PickermsPekerjaanNCBS.Data") Is Nothing Then
                    Throw New Exception
                End If
                L_msPekerjaanNCBS = Session("PickermsPekerjaanNCBS.Data")
                For Each i As msPekerjaanNCBS In L_msPekerjaanNCBS
                    Dim Tempmapping As New MappingMsPekerjaanNCBSPPATK
                    With Tempmapping
                        .IDPekerjaanNCBS = i.IDPekerjaanNCBS
                        .Nama = i.NamaPekerjaan
                    End With
                    Temp.Add(Tempmapping)
                Next
            Catch ex As Exception
                L_msPekerjaanNCBS = Nothing
            End Try
            Return Temp
        End Get
    End Property
    ''' <summary>
    ''' Menyimpan Item untuk mapping sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Listmaping As TList(Of MappingMsPekerjaanNCBSPPATK)
        Get
            Return Session("Listmaping.data")
        End Get
        Set(value As TList(Of MappingMsPekerjaanNCBSPPATK))
            Session("Listmaping.data") = value
        End Set
    End Property
    ''' <summary>
    ''' List yang tampil di ListBox
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplay() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingMsPekerjaanNCBSPPATK In Listmaping.FindAllDistinct("IDPekerjaanNCBS")
                Temp.Add(i.IDPekerjaanNCBS.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property

#End Region

End Class



