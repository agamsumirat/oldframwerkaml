<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PopupWindow.aspx.vb" Inherits="PopupWindow" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Select Users</title>
    <link href="theme/aml.css" rel="stylesheet" type="text/css">
      <script language="javascript">
function mainValues(clientId)
{
    var lsString;    
    lsString=window.document.getElementById(clientId).value;
    
    window.opener.parent.form1(clientId).value=lsString;    
    window.close();
}
</script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <%--<ajax:AjaxPanel ID="PanelUserID" runat="server" Width="300px">--%>
            <table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="300" bgColor="#dddddd"
		border="2" height="72" style="border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none">
                <tr class="formText">
                    <td bgcolor="#ffffff" colspan="5">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="validation"
                            HeaderText="There were errors on the page:" Width="85%" />
                <asp:CustomValidator ID="cvalPageError" runat="server" Display="None"></asp:CustomValidator></td>
                </tr>
                <tr class="formText">
                <td colspan="1">
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/arrow.gif" /></td>
                    <td bgColor="#dddddd" colspan="4" style="font-size: 20px" valign="middle" >
                        PICK USERID</td>
                </tr>
		<tr class="formText">
            <td bgcolor="#ffffff" colspan="5" style="height: 24px">
            <asp:GridView ID="GridViewUserID" runat="server" AutoGenerateColumns="False" CellPadding="4"
                ForeColor="#333333" GridLines="None" Width="250px">
                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <%--<asp:CheckBox ID="CheckBox1" runat="server" OnCheckedChanged="CheckBox1_CheckedChanged" AutoPostBack="true" />--%>
                            <asp:CheckBox ID="CheckBox1" runat="server" />
                        </ItemTemplate>
                        <ItemStyle Width="2%" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="UserID" HeaderText="UserID" SortExpression="UserID"/>
                </Columns>
                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <AlternatingRowStyle BackColor="White" />
            </asp:GridView>
            
		</tr>
		<tr class="formText" bgColor="#dddddd" height="30">
			<td width="2%"><IMG height="15" src="images/arrow.gif" width="15"></td>
			<td colspan="4">
                <asp:ImageButton ID="ImageButtonSend" runat="server" SkinID="UpdateButton" />&nbsp;
            </td>
		</tr>
		</table>
		<br />
        <br />
        <br />
            <br />
            <br />
            <br />
        <br />
        <asp:TextBox ID="TxtTampung" runat="server" TextMode="SingleLine" Visible="true" BorderColor="White" BorderStyle="None" ForeColor="White"></asp:TextBox>       
        <script>
        </script>
      <%--</ajax:AjaxPanel>--%>
      &nbsp;&nbsp;<br />
        
        
    </div>
    </form>
</body>
</html>
