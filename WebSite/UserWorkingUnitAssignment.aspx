<%@ Page Language="VB" MasterPageFile="~/masterpage.master" AutoEventWireup="false" CodeFile="UserWorkingUnitAssignment.aspx.vb" Inherits="UserWorkingUnitAssignment" title="User Working Unit Assignment" %>
<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
   <ajax:ajaxpanel id="AjaxPanel1" runat="server" Width="99%">   
    <table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="99%" bgColor="#dddddd"
	    border="2">
        <tr>
        
            <td bgcolor="#ffffff"  style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none" colspan="5">
                <strong>User Working Unit Assignment
                    <hr />
                </strong>
                <asp:Label ID="LblSucces" runat="server" CssClass="validationok" Visible="False"
                    Width="93%"></asp:Label></td>
        </tr>
        <tr class="formText">
		    <td width="5" bgColor="#ffffff" height="24">
                <br />
            </td>
		    <td bgColor="#ffffff" colspan="2">
                User ID</td>
            <td bgcolor="#ffffff" colspan="1">
                :</td>
		    <td width="80%" bgColor="#ffffff">
                <asp:Label ID="LabelUserID" runat="server" Text="LabelUserID"></asp:Label>&nbsp;</td>
	    </tr>
	    <tr class="formText">
		    <td bgcolor="#ffffff" height="24">
                <br />
            </td>
		    <td bgcolor="#ffffff" colspan="2">
                Working Unit</td>
            <td bgcolor="#ffffff" colspan="1">
                :</td>
		    <td bgcolor="#ffffff">
                <asp:DropDownList ID="DropDownListWorkingUnit" runat="server" Width="215px" CssClass="combobox">
                </asp:DropDownList>&nbsp;</td>
	    </tr>
	    <tr class="formText">
		    <td bgcolor="#ffffff" style="height: 32px">
                &nbsp;</td>
		    <td bgcolor="#ffffff" style="height: 32px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;" colspan="2">
                </td>
            <td bgcolor="#ffffff" colspan="1" style="height: 32px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;">
            </td>
		    <td bgcolor="#ffffff" style="height: 32px">
                <asp:Button ID="ButtonAdd" runat="server" Text="Add V" CssClass="button" />&nbsp;</td>
	    </tr>
        <tr class="formText">
            <td bgcolor="#ffffff" height="24">
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" colspan="2">
                List of Working Unit<br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff" colspan="1">
                :<br />
                <br />
                <br />
                <br />
            </td>
            <td bgcolor="#ffffff">
                <asp:ListBox ID="ListBoxWorkingUnits" runat="server" Width="215px" SelectionMode="Multiple" CssClass="textbox"></asp:ListBox>&nbsp;<strong><span style="color: #ff0000">
                    <asp:Button ID="ButtonRemove" runat="server" Text="Remove" CssClass="button" /></span></strong></td>
        </tr>
	    <tr class="formText" bgColor="#dddddd" height="30">
		    <td width="15"><IMG height="15" src="images/arrow.gif" width="15"></td>
		    <td colSpan="4">
			    <table cellSpacing="0" cellPadding="3" border="0">
				    <tr>
					    <td><asp:imagebutton id="ImageSave" runat="server" CausesValidation="True" SkinID="SaveButton"></asp:imagebutton></td>
					    <td><asp:imagebutton id="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton"></asp:imagebutton></td>
				    </tr>
			    </table>
                <span style="color: #ff0000"></span>
                <asp:CustomValidator ID="cvalPageErr" runat="server" Display="None" Visible="False" Width="1px"></asp:CustomValidator></td>
	    </tr>
    </table>
    </ajax:ajaxpanel>
	<script language="javascript">
	    document.getElementById('<%=GetFocusID %>').focus();
	</script>
</asp:Content>