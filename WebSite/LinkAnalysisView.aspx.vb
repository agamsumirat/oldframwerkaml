Imports System.Runtime.InteropServices
Imports System.Drawing
Partial Class LinkAnalysisView
    Inherits System.Web.UI.Page

    Private m_ErrorMessage As String

    Private Property ErrorMessage() As String
        Get
            Return m_ErrorMessage
        End Get
        Set(ByVal value As String)
            m_ErrorMessage = value
        End Set
    End Property

    Private Property CIFNodeCollection() As ArrayList
        Get
            Return IIf(Session("LinkAnalysisCIFNodeCollection") Is Nothing, New ArrayList, Session("LinkAnalysisCIFNodeCollection"))
        End Get
        Set(ByVal value As ArrayList)
            Session("LinkAnalysisCIFNodeCollection") = value
        End Set
    End Property

    Private Property CIFRelationshipNodeCollection() As ArrayList
        Get
            Return IIf(Session("LinkAnalysisCIFRelationshipNodeCollection") Is Nothing, New ArrayList, Session("LinkAnalysisCIFRelationshipNodeCollection"))
        End Get
        Set(ByVal value As ArrayList)
            Session("LinkAnalysisCIFRelationshipNodeCollection") = value
        End Set
    End Property

    Private Function ClearPageSession() As Boolean
        Session("LinkAnalysisCIFNodeCollection") = Nothing
        Session("LinkAnalysisCIFRelationshipNodeCollection") = Nothing
        Return True
    End Function
    'Protected Function BuildGraphLanguage(ByVal CollCIFNo As Hashtable) As String
    '    Dim strCommand As String = ""
    '    Dim StrCIFNo As String
    '    If Not CollCIFNo Is Nothing Then
    '        If CollCIFNo.Count > 0 Then
    '            Dim CIFRelationAdapter As New AMLDAL.LinkAnalysisTableAdapters.LinkAnalysisGetCustomerRelationshipTableAdapter

    '            Try
    '                strCommand = "Digraph LinkAnalysis {"
    '                For Each StrCIFNo In CollCIFNo.Keys
    '                    ' Get CIF Relationship Info
    '                    Dim DtCIFRelation As New AMLDAL.LinkAnalysis.LinkAnalysisGetCustomerRelationshipDataTable
    '                    Dim DrCIFRelation As AMLDAL.LinkAnalysis.LinkAnalysisGetCustomerRelationshipRow
    '                    CIFRelationAdapter.FillByCIFNo(DtCIFRelation, StrCIFNo.Replace("'", "''"))
    '                    ' untuk setiap relasinya tampilkan
    '                    strCommand = strCommand + " " & StrCIFNo & " [label=""" & StrCIFNo & "\n" & CollCIFNo(StrCIFNo) & """ URL=""javascript:ShowMenu('" & StrCIFNo & "')""];"
    '                    For Each DrCIFRelation In DtCIFRelation.Rows
    '                        strCommand = strCommand + " " & DrCIFRelation.CIFRelationNo & " [label=""" & DrCIFRelation.CIFRelationNo & "\n" & IIf(IsDBNull(DrCIFRelation.CIFRelationName), "", DrCIFRelation.CIFRelationName) & """ URL=""javascript:ShowMenu('" & DrCIFRelation.CIFRelationNo & "')""];"
    '                        strCommand = strCommand + " " & StrCIFNo & " -> " & DrCIFRelation.CIFRelationNo & " [label=""" & DrCIFRelation.RelationTypeName & """ URL=""javascript:ShowMenu('" & DrCIFRelation.CIFRelationNo & "')""];"
    '                    Next
    '                Next
    '                strCommand = strCommand + " }"
    '            Catch ex As Exception
    '                Throw ex
    '            Finally
    '                If Not CIFRelationAdapter Is Nothing Then
    '                    CIFRelationAdapter.Dispose()
    '                    CIFRelationAdapter = Nothing
    '                End If
    '            End Try
    '        End If
    '    End If
    '    Return strCommand
    'End Function

    'Protected Function BuildGraphLanguage(ByVal StrCIFNo As String, ByVal PreviousGraphCommand As String, ByVal ExpandLink As Boolean) As String
    '    Dim strCommand As String = ""
    '    Dim StrCIFName As String = ""
    '    Dim CIFRelationAdapter As New AMLDAL.LinkAnalysisTableAdapters.LinkAnalysisGetCustomerRelationshipTableAdapter
    '    Try
    '        Using GetCustomerNameAdapter As New AMLDAL.LinkAnalysisTableAdapters.LinkAnalysisGetCustomerNameByCIFNoTableAdapter
    '            Using GetCustomerNameTable As New AMLDAL.LinkAnalysis.LinkAnalysisGetCustomerNameByCIFNoDataTable
    '                GetCustomerNameAdapter.Fill(GetCustomerNameTable, StrCIFNo)
    '                If Not GetCustomerNameTable Is Nothing Then
    '                    If GetCustomerNameTable.Rows.Count > 0 Then
    '                        Dim GetCustomerNameTableRow As AMLDAL.LinkAnalysis.LinkAnalysisGetCustomerNameByCIFNoRow
    '                        GetCustomerNameTableRow = GetCustomerNameTable.Rows(0)
    '                        If Not GetCustomerNameTableRow.IsCustomerNameNull Then
    '                            StrCIFName = GetCustomerNameTableRow.CustomerName
    '                        End If
    '                    End If
    '                End If
    '            End Using
    '        End Using
    '        'StrCIFName=
    '        If ExpandLink Then
    '            strCommand = PreviousGraphCommand
    '            If strCommand <> "" Then
    '                strCommand = strCommand.Remove(strCommand.Length - 1)
    '                ' Get CIF Relationship Info
    '                Dim DtCIFRelation As New AMLDAL.LinkAnalysis.LinkAnalysisGetCustomerRelationshipDataTable
    '                Dim DrCIFRelation As AMLDAL.LinkAnalysis.LinkAnalysisGetCustomerRelationshipRow
    '                CIFRelationAdapter.FillByCIFNo(DtCIFRelation, StrCIFNo.Replace("'", "''"))
    '                ' untuk setiap relasinya tampilkan
    '                'strCommand = strCommand + " " & StrCIFNo & " [label=""" & StrCIFNo & "\n" & StrCIFName & """ URL=""javascript:ShowMenu('" & StrCIFNo & "')""];"
    '                For Each DrCIFRelation In DtCIFRelation.Rows
    '                    strCommand = strCommand + " {rank=same; " & DrCIFRelation.CIFRelationNo & " [label=""" & DrCIFRelation.CIFRelationNo & "\n" & IIf(DrCIFRelation.IsCIFRelationNameNull, "", DrCIFRelation.CIFRelationName) & """ URL=""javascript:ShowMenu('" & DrCIFRelation.CIFRelationNo & "')""];}"
    '                    strCommand = strCommand + " " & StrCIFNo & " -> " & DrCIFRelation.CIFRelationNo & " [label=""" & DrCIFRelation.RelationTypeName & """ URL=""javascript:ShowMenu('" & DrCIFRelation.CIFRelationNo & "')""];"
    '                Next
    '                strCommand = strCommand + " }"
    '            End If
    '        Else
    '            strCommand = "Digraph LinkAnalysis {"
    '            strCommand = strCommand & "  ranksep=1.0; size = ""7.5,7.5""; "
    '            strCommand = strCommand & "  node [shape=box,color=blue,fillcolor=""#9afd84"",style=""filled""];"

    '            ' Get CIF Relationship Info
    '            Dim DtCIFRelation As New AMLDAL.LinkAnalysis.LinkAnalysisGetCustomerRelationshipDataTable
    '            Dim DrCIFRelation As AMLDAL.LinkAnalysis.LinkAnalysisGetCustomerRelationshipRow
    '            CIFRelationAdapter.FillByCIFNo(DtCIFRelation, StrCIFNo.Replace("'", "''"))
    '            ' untuk setiap relasinya tampilkan
    '            strCommand = strCommand + " " & StrCIFNo & " [label=""" & StrCIFNo & "\n" & StrCIFName & """ URL=""javascript:ShowMenu('" & StrCIFNo & "')""];"

    '            For Each DrCIFRelation In DtCIFRelation.Rows
    '                strCommand = strCommand + " " & DrCIFRelation.CIFRelationNo & " [label=""" & DrCIFRelation.CIFRelationNo & "\n" & IIf(DrCIFRelation.IsCIFRelationNameNull, "", DrCIFRelation.CIFRelationName) & """ URL=""javascript:ShowMenu('" & DrCIFRelation.CIFRelationNo & "')""];"
    '                strCommand = strCommand + " " & StrCIFNo & " -> " & DrCIFRelation.CIFRelationNo & " [label=""" & DrCIFRelation.RelationTypeName & """ URL=""javascript:ShowMenu('" & DrCIFRelation.CIFRelationNo & "')""];"
    '            Next
    '            strCommand = strCommand + " }"
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    Finally
    '        If Not CIFRelationAdapter Is Nothing Then
    '            CIFRelationAdapter.Dispose()
    '            CIFRelationAdapter = Nothing
    '        End If
    '    End Try

    '    Return strCommand
    'End Function

    Private Function IsCIFInCollection(ByVal StrCIFNo As String) As Boolean
        Dim Counter As Integer
        Dim InstanceCIFNode As CIFNode
        Dim ArrCIFNode As ArrayList
        ArrCIFNode = Me.CIFNodeCollection
        For Counter = 0 To ArrCIFNode.Count - 1
            InstanceCIFNode = ArrCIFNode.Item(Counter)
            If InstanceCIFNode.CIFNo.ToLower() = StrCIFNo.ToLower() Then
                Return True
            End If
        Next
    End Function

    Private Function IsCIFRelationshipInCollection(ByVal StrCIFNo As String, ByVal StrRelationshipType As String, ByVal StrCIFRelationNo As String) As Boolean
        Dim Counter As Integer
        Dim InstanceCIFRelationshipNode As CIFRelationshipNode
        Dim ArrCIFRelationshipNode As ArrayList
        ArrCIFRelationshipNode = Me.CIFRelationshipNodeCollection
        For Counter = 0 To ArrCIFRelationshipNode.Count - 1
            InstanceCIFRelationshipNode = ArrCIFRelationshipNode.Item(Counter)
            If InstanceCIFRelationshipNode.CIFNo.ToLower() = StrCIFNo.ToLower() Then
                If InstanceCIFRelationshipNode.CIFRelationNo.ToLower() = StrCIFRelationNo.ToLower() Then
                    If InstanceCIFRelationshipNode.RelationshipType.ToLower() = StrRelationshipType.ToLower() Then
                        Return True
                    End If
                End If
            End If
        Next
    End Function

    Protected Function BuildGraphLanguage(ByVal StrCIFNo As String, ByRef IsDataExists As Boolean) As String
        Dim strCommand As String = ""
        Dim StrCIFName As String = ""
        Try
            IsDataExists = False
            Using CIFRelationAdapter As New AMLDAL.LinkAnalysisTableAdapters.LinkAnalysisGetCustomerRelationshipTableAdapter
                Using CIFRelationTable As New AMLDAL.LinkAnalysis.LinkAnalysisGetCustomerRelationshipDataTable
                    'CIFRelationAdapter.FillByCIFNo(CIFRelationTable, StrCIFNo.Replace("'", "''"))

                    If GetFromAmount.Trim.Length > 0 And GetToAmount.Trim.Length > 0 Then
                        CIFRelationAdapter.FillByCIFRelationNo(CIFRelationTable, StrCIFNo.Replace("'", "''"), GetFromAmount, GetToAmount)
                    Else
                        CIFRelationAdapter.FillByCIFRelationNo(CIFRelationTable, StrCIFNo.Replace("'", "''"), Nothing, Nothing)
                    End If

                    If Not CIFRelationTable Is Nothing Then
                        If CIFRelationTable.Rows.Count > 0 Then
                            IsDataExists = True
                            Dim CIFRelationTableRow As AMLDAL.LinkAnalysis.LinkAnalysisGetCustomerRelationshipRow
                            Dim InstanceCIFNode As CIFNode
                            Dim InstanceCIFRelationshipNode As CIFRelationshipNode
                            Dim ArrCIFNode As ArrayList
                            Dim ArrCIFRelationshipNode As ArrayList
                            Dim DisplayedNodeCount As Integer
                            ArrCIFNode = Me.CIFNodeCollection
                            ArrCIFRelationshipNode = Me.CIFRelationshipNodeCollection
                            DisplayedNodeCount = 0
                            For Each CIFRelationTableRow In CIFRelationTable.Rows
                                ' Add CIF to CIF Node Collection
                                InstanceCIFNode = Nothing
                                InstanceCIFNode = New CIFNode
                                If Not CIFRelationTableRow.IsCIFNoNull Then
                                    InstanceCIFNode.CIFNo = CIFRelationTableRow.CIFNo
                                End If
                                If Not CIFRelationTableRow.IsCustomerNameNull Then
                                    InstanceCIFNode.CIFName = CIFRelationTableRow.CustomerName.Trim()
                                End If
                                If Not Me.CIFNodeCollection Is Nothing Then
                                    If Not Me.IsCIFInCollection(InstanceCIFNode.CIFNo) Then
                                        ArrCIFNode.Add(InstanceCIFNode)
                                    End If
                                Else
                                    ArrCIFNode.Add(InstanceCIFNode)
                                End If
                                ' Add CIFRelation to CIF Node Collection
                                InstanceCIFNode = Nothing
                                InstanceCIFNode = New CIFNode
                                If Not CIFRelationTableRow.IsCIFRelationNoNull Then
                                    InstanceCIFNode.CIFNo = CIFRelationTableRow.CIFRelationNo
                                End If
                                If Not CIFRelationTableRow.IsCIFRelationNameNull Then
                                    InstanceCIFNode.CIFName = CIFRelationTableRow.CIFRelationName.Trim()
                                End If
                                If Not ArrCIFNode Is Nothing Then
                                    If Not Me.IsCIFInCollection(InstanceCIFNode.CIFNo) Then
                                        ArrCIFNode.Add(InstanceCIFNode)
                                    End If
                                Else
                                    ArrCIFNode.Add(InstanceCIFNode)
                                End If
                                ' Add Relationship to CIFRelationshipCollection
                                InstanceCIFRelationshipNode = Nothing
                                InstanceCIFRelationshipNode = New CIFRelationshipNode
                                If Not CIFRelationTableRow.IsCIFNoNull Then
                                    InstanceCIFRelationshipNode.CIFNo = CIFRelationTableRow.CIFNo
                                End If
                                If Not CIFRelationTableRow.IsCIFRelationNoNull Then
                                    InstanceCIFRelationshipNode.CIFRelationNo = CIFRelationTableRow.CIFRelationNo
                                End If
                                If Not CIFRelationTableRow.IsRelationTypeNull Then
                                    InstanceCIFRelationshipNode.RelationshipType = CIFRelationTableRow.RelationType.Trim()
                                End If
                                If Not CIFRelationTableRow.IsRelationTypeNameNull Then
                                    InstanceCIFRelationshipNode.RelationshipTypeName = CIFRelationTableRow.RelationTypeName.Trim()
                                End If
                                If Not ArrCIFRelationshipNode Is Nothing Then
                                    If Not Me.IsCIFRelationshipInCollection(InstanceCIFRelationshipNode.CIFNo, InstanceCIFRelationshipNode.RelationshipType, InstanceCIFRelationshipNode.CIFRelationNo) Then
                                        If Not Me.IsCIFRelationshipInCollection(InstanceCIFRelationshipNode.CIFRelationNo, InstanceCIFRelationshipNode.RelationshipType, InstanceCIFRelationshipNode.CIFNo) Then
                                            ArrCIFRelationshipNode.Add(InstanceCIFRelationshipNode)
                                        End If
                                    End If
                                Else
                                    ArrCIFRelationshipNode.Add(InstanceCIFRelationshipNode)
                                End If
                                If Not ArrCIFNode Is Nothing Then
                                    DisplayedNodeCount = System.Configuration.ConfigurationManager.AppSettings("DisplayedNodeCount")
                                    If ArrCIFNode.Count > DisplayedNodeCount Then
                                        Me.ErrorMessage = "Displayed Node more than 100 node, other node will not shown."
                                        Exit For
                                    End If
                                End If
                            Next
                            Me.CIFNodeCollection = ArrCIFNode
                            Me.CIFRelationshipNodeCollection = ArrCIFRelationshipNode
                        End If
                    End If
                End Using
            End Using
            strCommand = "Digraph LinkAnalysis {"
            strCommand = strCommand & "  ranksep=1.0; size = ""7.5,7.5""; "
            strCommand = strCommand & "  node [shape=box,color=blue,fillcolor=""#9afd84"",style=""filled""];"

            ' Draw CIF Node
            If Not Me.CIFNodeCollection Is Nothing Then
                If Me.CIFNodeCollection.Count > 0 Then
                    Dim Counter As Integer
                    Dim ArrCIFNode As ArrayList
                    Dim InstanceCIFNode As New CIFNode
                    ArrCIFNode = Me.CIFNodeCollection

                    'Dim intcountermax As Integer
                    'If ArrCIFNode.Count > 30 Then
                    '    intcountermax = 30
                    'Else
                    '    intcountermax = ArrCIFNode.Count - 1
                    'End If
                    For Counter = 0 To ArrCIFNode.Count - 1
                        InstanceCIFNode = ArrCIFNode(Counter)
                        strCommand = strCommand + " " & InstanceCIFNode.CIFNo & " [label=""" & InstanceCIFNode.CIFNo & "\n" & InstanceCIFNode.CIFName & """ URL=""javascript:ShowMenu('" & InstanceCIFNode.CIFNo & "')""];"
                    Next
                End If
            End If
            ' Draw Relationship Node
            If Not Me.CIFRelationshipNodeCollection Is Nothing Then
                If Me.CIFRelationshipNodeCollection.Count > 0 Then
                    Dim Counter As Integer
                    Dim ArrCIFRelationshipNode As ArrayList
                    Dim InstanceCIFRelationshipNode As CIFRelationshipNode
                    ArrCIFRelationshipNode = Me.CIFRelationshipNodeCollection
                    'Dim incounter As Integer
                    'If ArrCIFRelationshipNode.Count > 26 Then
                    '    incounter = 26
                    'Else
                    '    incounter = ArrCIFRelationshipNode.Count - 1
                    'End If
                    For Counter = 0 To ArrCIFRelationshipNode.Count - 1
                        InstanceCIFRelationshipNode = ArrCIFRelationshipNode(Counter)
                        'strCommand = strCommand + " " & InstanceCIFRelationshipNode.CIFNo & " -> " & InstanceCIFRelationshipNode.CIFRelationNo & " [label=""" & InstanceCIFRelationshipNode.RelationshipTypeName & """ URL=""javascript:ShowMenu('" & InstanceCIFRelationshipNode.CIFRelationNo & "')""];"
                        strCommand = strCommand + " " & InstanceCIFRelationshipNode.CIFRelationNo & " -> " & InstanceCIFRelationshipNode.CIFNo & " [label=""" & InstanceCIFRelationshipNode.RelationshipTypeName.Trim() & """ URL=""javascript:ShowMenu('" & InstanceCIFRelationshipNode.CIFRelationNo & "')""];"
                    Next
                End If
            End If
            strCommand = strCommand + " }"
            Return strCommand
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'Protected Function ShowLink(ByVal CollCIFNo As Hashtable, ByVal IntLevelDepth As Byte) As Boolean
    '    Dim strCommand As String
    '    'Create DOT Object 
    '    Dim dot As New WINGRAPHVIZLib.DOT

    '    'strCommand = "Digraph LinkAnalysis {"
    '    'strCommand = strCommand + " A -> B [label=""B"" URL=""./LinkAnalysis?ID=123""];"
    '    'strCommand = strCommand + " A -> B [label=""B"" URL=""./LinkAnalysis?ID=123""];"
    '    'strCommand = strCommand + " A -> C [label=""C"" URL=""./LinkAnalysis?ID=234""];"
    '    'strCommand = strCommand + " A -> D;"
    '    'strCommand = strCommand + " B -> E;"
    '    'strCommand = strCommand + " }"
    '    strCommand = BuildGraphLanguage(CollCIFNo)
    '    Session("LinkAnalysisGraphCommand") = strCommand

    '    Dim Img As New WINGRAPHVIZLib.BinaryImage
    '    Dim iStream As ComTypes.IStream = Nothing

    '    Img = dot.ToGIF(strCommand)

    '    Img.Dump(CType(iStream, WINGRAPHVIZLib.IStream))
    '    Response.ContentType = "image/gif"

    '    Sahassa.AML.ComStream.CreateStreamOnHGlobal(0, False, iStream)
    '    Img.Dump(CType(iStream, WINGRAPHVIZLib.IStream))

    '    Dim cs As Sahassa.AML.ComStream = New Sahassa.AML.ComStream(iStream)
    '    Dim bmp As New Bitmap(cs)

    '    'Return

    '    bmp.Save(Response.OutputStream, Imaging.ImageFormat.Gif)

    '    'Release Resource                               
    '    Img = Nothing
    '    iStream = Nothing
    '    dot = Nothing
    'End Function

    Private ReadOnly Property GetCIFNo() As String
        Get
            Return IIf(Me.Request.Params.Get("CIFNo") Is Nothing, Session("LinkAnalysisCIFNo"), Me.Request.Params.Get("CIFNo"))
        End Get
    End Property
    Private ReadOnly Property GetFromAmount() As String
        Get
            If Session("LinkAnalysisFromAmount") Is Nothing Then
                Return ""
            Else
                Return Session("LinkAnalysisFromAmount")
            End If
        End Get
    End Property
    Private ReadOnly Property GetToAmount() As String
        Get
            If Session("LinkAnalysisToAmount") Is Nothing Then
                Return ""
            Else
                Return Session("LinkAnalysisToAmount")
            End If

        End Get
    End Property

    Private ReadOnly Property GetExpandLink() As Boolean
        Get
            Return IIf(Me.Request.Params.Get("ExpandLink") Is Nothing, False, Me.Request.Params.Get("ExpandLink"))
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Dim dot As New WINGRAPHVIZLib.DOT
        Dim StrCIFNo As String
        Dim ExpandLink As Boolean
        Dim IsDataExists As Boolean
        Try
            If Not Page.IsPostBack Then
                Me.LinkAnalysisImage.Visible = False
                StrCIFNo = Me.GetCIFNo
                ExpandLink = Me.GetExpandLink
                If Not ExpandLink Then
                    Me.ClearPageSession()
                End If
                If StrCIFNo <> "" Then
                    Dim strCommand As String

                    strCommand = Session("LinkAnalysisGraphCommand")
                    'strCommand = BuildGraphLanguage(StrCIFNo, strCommand, ExpandLink)
                    strCommand = BuildGraphLanguage(StrCIFNo, IsDataExists)
                    If IsDataExists Then
                        If Not strCommand Is Nothing Then

                            Session("LinkAnalysisGraphCommand") = strCommand
                            ''Generate Client-side image map(CMAP)
                            'Dim strCMAP As String

                            'strCMAP = dot.ToCMAP(strCommand)

                            ''Return
                            'strCMAP = strCMAP.Replace("href", "href=""#"" onclick")
                            'strCMAP = strCMAP.Replace("\n", " - ")
                            'Me.LinkAnalysisMap.InnerHtml = strCMAP
                            ''Response.Write(strCMAP)

                            ''Release Resource                                               
                            'Me.LinkAnalysisImage.Src = "LinkAnalysis.aspx"

                            'dot = Nothing

                            If Me.ErrorMessage <> "" Then
                                Me.cvalPageError.IsValid = False
                                Me.cvalPageError.ErrorMessage = Me.ErrorMessage
                                Me.ErrorMessage = ""
                            End If
                        Else
                            Me.LinkAnalysisImage.Visible = True
                        End If
                    Else
                        Me.LinkAnalysisImage.Visible = True
                    End If
                End If
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            'Finally
            '    If Not dot Is Nothing Then
            '        dot = Nothing
            '    End If
        End Try
    End Sub

End Class

Public Class CIFRelationshipNode
    Public CIFNo As String
    Public CIFRelationNo As String
    Public RelationshipType As String
    Public RelationshipTypeName As String
End Class

Public Class CIFNode
    Public CIFNo As String
    Public CIFName As String
End Class
