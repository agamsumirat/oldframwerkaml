Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper
Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports System
Imports AMLBLL
Imports AMLBLL.ValidateBLL

Public Class LoginParameter
    Inherits Parent
    
#Region " Property "
    ''' <summary>
    ''' get focus id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property GetFocusID() As String
        Get
            Return Me.TextPasswordPeriod.ClientID
        End Get
    End Property
#End Region

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' cek approved
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	09/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Function CekApprove() As Boolean
        Using AccessParameters As New TList(Of LoginParameter_Approval)
            Dim count As Int32 = AccessParameters.Count
            If count > 0 Then
                Return False
            Else
                Return True
            End If
        End Using
    End Function

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get data login parameter
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	03/07/2006	Created
    '''     [Stabbian] 17/09/2013 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------

    Private Sub LoadData()
        Dim oParameter As TList(Of SahassaNettier.Entities.LoginParameter) = DataRepository.LoginParameterProvider.GetAll()
        Dim o As SahassaNettier.Entities.LoginParameter
        Try
            For Each o In oParameter
                '"umurpassword"
                If Not CInt(o.PasswordExpiredPeriod) = 0 Then
                    TextPasswordPeriod.Text = o.PasswordExpiredPeriod
                    CbPassExpiration.Checked = True
                    TextPasswordPeriod.Enabled = CbPassExpiration.Checked
                End If
                '"panjangpassword"
                If Not CInt(o.MinimumPasswordLength) = 0 Then
                    TextPasswordMin.Text = o.MinimumPasswordLength
                    CbMinimumLenght.Checked = True
                    TextPasswordMin.Enabled = CbMinimumLenght.Checked
                End If
                '"PasswordRecycleCount"
                If Not CInt(o.PasswordRecycleCount) = 0 Then
                    TextPasswordRecycle.Text = o.PasswordRecycleCount
                    CbPassRecycle.Checked = True
                    TextPasswordRecycle.Enabled = CbPassRecycle.Checked
                End If
                'passwordchar
                If Not CBool(o.PasswordChar) = False Then
                    CbPasswordChar.Checked = True
                End If
                'passwordcombination
                If Not CBool(o.PasswordCombination) = False Then
                    CbPassCombination.Checked = True
                End If
                'changepasswordonfirstlogin
                If Not CBool(o.ChangePasswordLogin) = False Then
                    CbFirstLogin.Checked = True
                End If
                'accountlockout
                If Not CInt(o.AccountLockout) = 0 Then
                    TextBoxAccountLockout.Text = o.AccountLockout
                    CbAccountLock.Checked = True
                    TextBoxAccountLockout.Enabled = CbAccountLock.Checked
                End If
                'inactiveperiod
                If Not CInt(o.LockUserAfterNDays) = 0 Then
                    TextBoxLockUnused.Text = o.LockUserAfterNDays
                    CbLockUnused.Checked = True
                    TextBoxLockUnused.Enabled = CbLockUnused.Checked
                End If
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load page handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	09/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load
        Try
            If Me.CekApprove() Then
                If Not Me.IsPostBack Then
                    Me.LoadData()
                    tampilan()
                    Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                    'Using Transcope As New Transactions.TransactionScope
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                        Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                        AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                        'Transcope.Complete()
                    End Using
                    'End Using
                End If
            Else
                Throw New Exception("Cannot update Login Parameter because it is currently waiting for approval")
            End If

        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' cancel event handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	09/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "Default.aspx"
        Me.Response.Redirect("Default.aspx", False)
    End Sub
    ''' <summary>
    ''' Insert Audit Trail SU
    ''' </summary>
    ''' <param name="mode"></param>
    ''' <param name="Action"></param>
    ''' <remarks></remarks>
    Private Sub InsertAuditTrailSU(ByVal mode As String, ByVal Action As String, ByRef oSQLTrans As SqlTransaction)

        Try
            Dim PasswordExpiredPeriod As Int32 = Me.TextPasswordPeriod.Text
            Dim MinimumPasswordLength As Int32 = Me.TextPasswordMin.Text
            Dim PasswordRecycleCount As Int32 = Me.TextPasswordRecycle.Text
            Dim PasswordChar As Byte = CbPasswordChar.Checked
            Dim PasswordCombination As Byte = CbPassCombination.Checked
            Dim ChangePasswordLogin As Byte = CbFirstLogin.Checked
            Dim LockUserAfterNDays As Int32 = Me.TextBoxLockUnused.Text
            Dim AccountLockout As Int32 = Me.TextBoxAccountLockout.Text

            Dim oldData As SahassaNettier.Entities.LoginParameter = DataRepository.LoginParameterProvider.GetPaged("PK_LoginParameter_ID = 1", "", 0, Integer.MaxValue, 0)(0)
            'Using TransScope As New Transactions.TransactionScope
            Using AccessLoginParameter As New TList(Of SahassaNettier.Entities.LoginParameter)
                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessLoginParameter, oSQLTrans)

                Dim dt As TList(Of SahassaNettier.Entities.LoginParameter) = AccessLoginParameter
                If dt.Count > 0 Then
                    Sahassa.AML.AuditTrailAlert.AuditTrailChecking(7)
                    Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                        If mode.ToLower = "add" Then
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - LoginParameter", "PasswordExpiredPeriod", mode, "", PasswordExpiredPeriod, Action)
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - LoginParameter", "MinimumPasswordLength", mode, "", MinimumPasswordLength, Action)
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - LoginParameter", "PasswordRecycleCount", mode, "", PasswordRecycleCount, Action)
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - LoginParameter", "PasswordChar", mode, "", PasswordChar, Action)
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - LoginParameter", "PasswordCombination", mode, "", PasswordCombination, Action)
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - LoginParameter", "ChangePasswordLogin", mode, "", ChangePasswordLogin, Action)
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - LoginParameter", "LockUserAfterNDays", mode, "", LockUserAfterNDays, Action)
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - LoginParameter", "AccountLockout", mode, "", AccountLockout, Action)
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - LoginParameter", "CreatedDate", mode, "", Now.ToString("dd-MMMM-yyyy HH:mm"), Action)
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - LoginParameter", "LastUpdateDate", mode, "", Now.ToString("dd-MMMM-yyyy HH:mm"), Action)
                        Else
                            Dim PasswordExpiredPeriod_Old As Int32 = oldData.PasswordExpiredPeriod
                            Dim MinimumPasswordLength_Old As Int32 = oldData.MinimumPasswordLength
                            Dim PasswordRecycleCount_Old As Int32 = oldData.PasswordRecycleCount
                            Dim PasswordChar_Old As Byte = oldData.PasswordChar
                            Dim PasswordCombination_Old As Byte = oldData.PasswordCombination
                            Dim ChangePasswordLogin_Old As Byte = oldData.ChangePasswordLogin
                            Dim LockUserAfterNDays_Old As Int32 = oldData.LockUserAfterNDays
                            Dim AccountLockout_Old As Int32 = oldData.AccountLockout
                            Dim CreatedDate_Old As DateTime = oldData.CreatedDate
                            Dim LastUpdate_Old As DateTime = oldData.LastUpdateDate

                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - LoginParameter", "PasswordExpiredPeriod", mode, PasswordExpiredPeriod_Old, PasswordExpiredPeriod, Action)
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - LoginParameter", "MinimumPasswordLength", mode, MinimumPasswordLength_Old, MinimumPasswordLength, Action)
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - LoginParameter", "PasswordRecycleCount", mode, PasswordRecycleCount_Old, PasswordRecycleCount, Action)
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - LoginParameter", "PasswordChar", mode, PasswordChar_Old, PasswordChar, Action)
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - LoginParameter", "PasswordCombination", mode, PasswordCombination_Old, PasswordCombination, Action)
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - LoginParameter", "ChangePasswordLogin", mode, ChangePasswordLogin_Old, ChangePasswordLogin, Action)
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - LoginParameter", "LockUserAfterNDays", mode, LockUserAfterNDays_Old, LockUserAfterNDays, Action)
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - LoginParameter", "AccountLockout", mode, AccountLockout_Old, AccountLockout, Action)
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - LoginParameter", "CreatedDate", mode, CreatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), Now, Action)
                            AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Parameters - LoginParameter", "LastUpdateDate", mode, LastUpdate_Old.ToString("dd-MMMM-yyyy HH:mm"), Now, Action)
                        End If
                    End Using

                End If
            End Using
            'End Using
        Catch
            Throw
        End Try
    End Sub
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' save handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>    
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry] 14/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageSave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            If cekValidasi() Then
                If Me.CekApprove() Then
                    Page.Validate()
                    If Page.IsValid Then

                        Using ParametersPending As New Parameters_PendingApproval
                            'oSQLTrans = BeginTransaction(ParametersPending, IsolationLevel.ReadUncommitted)
                            'Perika apakah sudah ada entry dalam tabel LoginParameter, bila belum ada, maka buat entry baru dlm tabel LoginParameter,
                            'tapi bila entry sudah ada dalam tabel LoginParameter, maka update entry tsb
                            Using AccessLoginParameter As New SahassaNettier.Entities.LoginParameter
                                'SetTransaction(AccessLoginParameter, oSQLTrans)
                                Using TableDataUpdatingEmailTemplate As TList(Of SahassaNettier.Entities.LoginParameter) = DataRepository.LoginParameterProvider.GetAll()
                                    Dim LoginParameterInsert As New SahassaNettier.Entities.LoginParameter
                                    Using LoginParameterApproval As New LoginParameter_Approval
                                        'SetTransaction(AccessLoginParameter, oSQLTrans)
                                        'Buat variabel untuk menampung nilai-nilai baru

                                        Dim ModeID As Int16
                                        Dim LoginParameterApprovalID As Int64

                                        If Sahassa.AML.Commonly.SessionPkUserId = 1 Then ' Cek Super User
                                            'Using TranScope As New Transactions.TransactionScope
                                            If TableDataUpdatingEmailTemplate.Count > 0 Then ' Jika Sudah Ada
                                                ' EDIT
                                                Dim dataparameters As SahassaNettier.Entities.LoginParameter = DataRepository.LoginParameterProvider.GetPaged("PK_LoginParameter_ID = 1", "", 0, Integer.MaxValue, 0)(0)
                                                Me.InsertAuditTrailSU("Edit", "Accept", oSQLTrans)
                                                With dataparameters
                                                    .PasswordExpiredPeriod = CInt(IIf(TextPasswordPeriod.Text <> Nothing, TextPasswordPeriod.Text, 0))
                                                    .MinimumPasswordLength = CInt(IIf(TextPasswordMin.Text <> Nothing, TextPasswordMin.Text, 0))
                                                    .PasswordRecycleCount = CInt(IIf(TextPasswordRecycle.Text <> Nothing, TextPasswordRecycle.Text, 0))
                                                    .PasswordChar = CbPasswordChar.Checked.ToString
                                                    .PasswordCombination = CbPassCombination.Checked.ToString
                                                    .ChangePasswordLogin = CbFirstLogin.Checked.ToString
                                                    .LockUserAfterNDays = CInt(IIf(TextBoxLockUnused.Text <> Nothing, TextBoxLockUnused.Text, 0))
                                                    .AccountLockout = CInt(IIf(TextBoxAccountLockout.Text <> Nothing, TextBoxAccountLockout.Text, 0))
                                                    .CreatedDate = Now
                                                    .LastUpdateDate = Now
                                                End With
                                                If DataRepository.LoginParameterProvider.Insert(dataparameters) > 0 Then
                                                    Me.LblSucces.Visible = True
                                                    Me.LblSucces.Text = "Login Parameter Succeed to Save."
                                                    Me.InsertAuditTrailSU("Edit", "Accept", oSQLTrans)
                                                    'TranScope.Complete()
                                                Else
                                                    Throw New Exception("Failed to save Login Parameter.")
                                                End If
                                            Else ' Row Blon Ada
                                                With LoginParameterInsert
                                                    .PasswordExpiredPeriod = CInt(IIf(TextPasswordPeriod.Text <> Nothing, TextPasswordPeriod.Text, 0))
                                                    .MinimumPasswordLength = CInt(IIf(TextPasswordMin.Text <> Nothing, TextPasswordMin.Text, 0))
                                                    .PasswordRecycleCount = CInt(IIf(TextPasswordRecycle.Text <> Nothing, TextPasswordRecycle.Text, 0))
                                                    .PasswordChar = CbPasswordChar.Checked.ToString
                                                    .PasswordCombination = CbPassCombination.Checked.ToString
                                                    .ChangePasswordLogin = CbFirstLogin.Checked.ToString
                                                    .LockUserAfterNDays = CInt(IIf(TextBoxLockUnused.Text <> Nothing, TextBoxLockUnused.Text, 0))
                                                    .AccountLockout = CInt(IIf(TextBoxAccountLockout.Text <> Nothing, TextBoxAccountLockout.Text, 0))
                                                    .CreatedDate = Now
                                                    .LastUpdateDate = Now
                                                End With
                                                If DataRepository.LoginParameterProvider.Insert(LoginParameterInsert) > 0 Then
                                                    Me.LblSucces.Visible = True
                                                    Me.LblSucces.Text = "Login Parameter Succeed to Save."
                                                    Me.InsertAuditTrailSU("Add", "Accept", oSQLTrans)
                                                    'TranScope.Complete()
                                                Else
                                                    Throw New Exception("Failed to save Login Parameter.")
                                                End If
                                            End If
                                            'End Using
                                        Else ' Bukan Super User
                                            'Using TranScope As New Transactions.TransactionScope
                                            'Bila tabel LoginParameter tidak kosong, maka update entry tsb & set ModeID = 2 (Edit)
                                            If TableDataUpdatingEmailTemplate.Count > 0 Then
                                                'Buat variabel untuk menampung nilai-nilai lama
                                                ModeID = 2
                                                Dim dataparameters As SahassaNettier.Entities.LoginParameter = DataRepository.LoginParameterProvider.GetPaged("PK_LoginParameter_ID = 1", "", 0, Integer.MaxValue, 0)(0)
                                                With LoginParameterApproval
                                                    .PasswordExpiredPeriod = CInt(IIf(TextPasswordPeriod.Text <> Nothing, TextPasswordPeriod.Text, 0))
                                                    .MinimumPasswordLength = CInt(IIf(TextPasswordMin.Text <> Nothing, TextPasswordMin.Text, 0))
                                                    .PasswordRecycleCount = CInt(IIf(TextPasswordRecycle.Text <> Nothing, TextPasswordRecycle.Text, 0))
                                                    .PasswordChar = CbPasswordChar.Checked.ToString
                                                    .PasswordCombination = CbPassCombination.Checked.ToString
                                                    .ChangePasswordLogin = CbFirstLogin.Checked.ToString
                                                    .LockUserAfterNDays = CInt(IIf(TextBoxLockUnused.Text <> Nothing, TextBoxLockUnused.Text, 0))
                                                    .AccountLockout = CInt(IIf(TextBoxAccountLockout.Text <> Nothing, TextBoxAccountLockout.Text, 0))
                                                    .CreatedDate = Now
                                                    .LastUpdateDate = Now
                                                    .PasswordExpiredPeriod_Old = dataparameters.PasswordExpiredPeriod
                                                    .MinimumPasswordLength_Old = dataparameters.MinimumPasswordLength
                                                    .PasswordRecycleCount_Old = dataparameters.PasswordRecycleCount
                                                    .PasswordChar_Old = dataparameters.PasswordChar
                                                    .PasswordCombination_Old = dataparameters.PasswordCombination
                                                    .ChangePasswordLogin_Old = dataparameters.ChangePasswordLogin
                                                    .LockUserAfterNDays_Old = dataparameters.LockUserAfterNDays
                                                    .AccountLockout_Old = dataparameters.AccountLockout
                                                    .CreatedDate_Old = dataparameters.CreatedDate
                                                    .LastUpdateDate_Old = dataparameters.LastUpdateDate
                                                End With
                                                DataRepository.LoginParameter_ApprovalProvider.Insert(LoginParameterApproval)
                                            Else 'Bila tabel LoginParameter kosong, maka buat entry baru & set ModeID = 1 (Add)
                                                ModeID = 1
                                                With LoginParameterApproval
                                                    .PasswordExpiredPeriod = CInt(IIf(TextPasswordPeriod.Text <> Nothing, TextPasswordPeriod.Text, 0))
                                                    .MinimumPasswordLength = CInt(IIf(TextPasswordMin.Text <> Nothing, TextPasswordMin.Text, 0))
                                                    .PasswordRecycleCount = CInt(IIf(TextPasswordRecycle.Text <> Nothing, TextPasswordRecycle.Text, 0))
                                                    .PasswordChar = CbPasswordChar.Checked.ToString
                                                    .PasswordCombination = CbPassCombination.Checked.ToString
                                                    .ChangePasswordLogin = CbFirstLogin.Checked.ToString
                                                    .LockUserAfterNDays = CInt(IIf(TextBoxLockUnused.Text <> Nothing, TextBoxLockUnused.Text, 0))
                                                    .AccountLockout = CInt(IIf(TextBoxAccountLockout.Text <> Nothing, TextBoxAccountLockout.Text, 0))
                                                    .CreatedDate = Now
                                                    .LastUpdateDate = Now
                                                    .PasswordExpiredPeriod_Old = Nothing
                                                    .MinimumPasswordLength_Old = Nothing
                                                    .PasswordRecycleCount_Old = Nothing
                                                    .PasswordChar_Old = Nothing
                                                    .PasswordCombination_Old = Nothing
                                                    .ChangePasswordLogin_Old = Nothing
                                                    .LockUserAfterNDays_Old = Nothing
                                                    .AccountLockout_Old = Nothing
                                                    .CreatedDate_Old = Nothing
                                                    .LastUpdateDate_Old = Nothing
                                                End With
                                                DataRepository.LoginParameter_ApprovalProvider.Insert(LoginParameterApproval)
                                            End If

                                            'ParametersPendingApprovalID adalah primary key dari tabel Parameters_PendingApproval yang sifatnya identity dan baru dibentuk setelah row baru dibuat
                                            Dim parameterpendingapp As New Parameters_PendingApproval
                                            With parameterpendingapp
                                                .Parameters_PendingApprovalUserID = Sahassa.AML.Commonly.SessionUserId
                                                .Parameters_PendingApprovalEntryDate = Now
                                                .Parameters_PendingApprovalModeID = ModeID
                                            End With
                                            DataRepository.Parameters_PendingApprovalProvider.Insert(parameterpendingapp)

                                            Using ParametersApproval As New Parameters_Approval
                                                'SetTransaction(ParametersApproval, oSQLTrans)
                                                'ParameterItemID 1 = LoginParameter
                                                With ParametersApproval
                                                    .Parameters_PendingApprovalID = parameterpendingapp.Parameters_PendingApprovalID
                                                    .ModeID = ModeID
                                                    .ParameterItemID = 1
                                                    .ParameterItemApprovalID = LoginParameterApproval.ApprovalID
                                                End With
                                                DataRepository.Parameters_ApprovalProvider.Insert(ParametersApproval)
                                            End Using

                                            If ModeID = 1 Then
                                                Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=8811"

                                                Me.Response.Redirect("MessagePending.aspx?MessagePendingID=8811", False)
                                            ElseIf ModeID = 2 Then
                                                Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=8812"

                                                Me.Response.Redirect("MessagePending.aspx?MessagePendingID=8812", False)
                                            End If
                                        End If
                                    End Using
                                End Using
                            End Using

                            oSQLTrans.Commit()
                        End Using
                        'End Using
                    End If
                Else
                    Throw New Exception("Cannot update Login Parameter because it is currently waiting for approval")
                End If
            End If

        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub
    Private Function cekValidasi() As Boolean
        Dim i As Integer = 6
        If TextBoxAccountLockout.Enabled = True Then
            If TextBoxAccountLockout.Text.Trim = "" Then
                Throw New Exception("Account Lockout cannot be blank")
            Else
                If objectNumOnly(TextBoxAccountLockout.Text) = False Then
                    Throw New Exception("Account Lockout must be a numeric value [0-9]")
                End If
            End If
        End If

        If TextPasswordPeriod.Enabled = True Then
            If TextPasswordPeriod.Text.Trim = "" Then
                Throw New Exception("Password Expiration Period cannot be blank")
            Else
                If objectNumOnly(TextPasswordPeriod.Text) = False Then
                    Throw New Exception("Password Expiration Period must be a numeric value [0-9]")
                End If
            End If
        End If

        If TextPasswordMin.Enabled = True Then
            If TextPasswordMin.Text = "" Then
                Throw New Exception("Minimum Password Length cannot be blank")
            Else
                If objectNumOnly(TextPasswordMin.Text) = False Then
                    Throw New Exception("Password Expiration Period must be a numeric value [0-9]")
                Else
                    If CInt(TextPasswordMin.Text) < i Then
                        Throw New Exception("Value Must Greater Than 6")
                    End If
                End If
            End If
        End If

        If TextPasswordRecycle.Enabled = True Then
            If TextPasswordRecycle.Text = "" Then
                Throw New Exception("Password Recycle cannot be blank")
            Else
                If objectNumOnly(TextPasswordRecycle.Text) = False Then
                    Throw New Exception("Password Recycle must be a numeric value [0-9]")
                End If
            End If
        End If

        If TextBoxLockUnused.Enabled = True Then
            If TextBoxLockUnused.Text = "" Then
                Throw New Exception("Lock Unused Account cannot be blank")
            Else
                If objectNumOnly(TextBoxLockUnused.Text) = False Then
                    Throw New Exception("Lock Unused Account must be a numeric value [0-9]")
                End If
            End If
        End If

        Return True
    End Function
    Private Sub tampilan()
        If CbAccountLock.Checked = False Then
            If CbPassExpiration.Checked = False Then
                If CbMinimumLenght.Checked = False Then
                    If CbPassRecycle.Checked = False Then
                        If CbLockUnused.Checked = False Then
                            ImageSave.Enabled = False
                        End If
                    End If
                End If
            End If
        End If
    End Sub
    Protected Sub CbAccountLock_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CbAccountLock.CheckedChanged
        If CbAccountLock.Checked = True Then
            ImageSave.Enabled = True
            TextBoxAccountLockout.Enabled = True
        Else
            TextBoxAccountLockout.Enabled = False
            TextBoxAccountLockout.Text = Nothing
        End If
    End Sub

    Protected Sub CbPassExpiration_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CbPassExpiration.CheckedChanged
        If CbPassExpiration.Checked = True Then
            TextPasswordPeriod.Enabled = True
            ImageSave.Enabled = True
        Else
            TextPasswordPeriod.Enabled = False
            TextPasswordPeriod.Text = Nothing
        End If
    End Sub

    Protected Sub CbMinimumLenght_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CbMinimumLenght.CheckedChanged
        If CbMinimumLenght.Checked = True Then
            TextPasswordMin.Enabled = True
            ImageSave.Enabled = True
        Else
            TextPasswordMin.Enabled = False
            TextPasswordMin.Text = Nothing
        End If
    End Sub

    Protected Sub CbPassRecycle_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CbPassRecycle.CheckedChanged
        If CbPassRecycle.Checked = True Then
            TextPasswordRecycle.Enabled = True
            ImageSave.Enabled = True
        Else
            TextPasswordRecycle.Enabled = False
            TextPasswordRecycle.Text = Nothing
        End If
    End Sub

    Protected Sub CbLockUnused_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CbLockUnused.CheckedChanged
        If CbLockUnused.Checked = True Then
            TextBoxLockUnused.Enabled = True
            ImageSave.Enabled = True
        Else
            TextBoxLockUnused.Enabled = False
            TextBoxLockUnused.Text = Nothing
        End If
    End Sub
End Class