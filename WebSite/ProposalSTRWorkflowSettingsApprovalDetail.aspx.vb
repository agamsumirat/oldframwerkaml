Imports system.Data
Imports System.Data.SqlClient

Partial Class ProposalSTRWorkflowSettingsApprovalDetail
    Inherits Parent

#Region "Property"
    
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetMode() As String
        Get
            Return Me.Page.Request.Params.Item("Mode").Trim
        End Get
    End Property
    ''' <summary>
    ''' SetnGetGeneral_Old
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetGeneral_Old() As Data.DataTable
        Get
            Return Session("GeneralSTR_Old")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("GeneralSTR_Old") = value
        End Set
    End Property
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetGeneral_New() As AMLDAL.ProposalSTRWorkflowSettings.SelectAllProposalSTRWorkflowGeneralApprovalDataTable
        Get
            Return Session("GeneralSTR_New")
        End Get
        Set(ByVal value As AMLDAL.ProposalSTRWorkflowSettings.SelectAllProposalSTRWorkflowGeneralApprovalDataTable)
            Session("GeneralSTR_New") = value
        End Set
    End Property

    Private Property SetnGetEmailTemplate_Old() As Data.DataTable
        Get
            Return Session("EmailTemplateSTR_Old")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("EmailTemplateSTR_Old") = value
        End Set
    End Property


    Private Property SetnGetEmailTemplate_New() As Data.DataTable
        Get
            Return Session("EmailTemplateSTR_New")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("EmailTemplateSTR_New") = value
        End Set
    End Property

    Private Property SetnGetUserIDCreated() As String
        Get
            Return Session("UserIDCreatedSTR")
        End Get
        Set(ByVal value As String)
            Session("UserIDCreatedSTR") = value
        End Set
    End Property

    Private Property SetnGetCreatedDate() As DateTime
        Get
            Return Session("CreatedDateSTR")
        End Get
        Set(ByVal value As DateTime)
            Session("CreatedDateSTR") = value
        End Set
    End Property
#End Region

#Region "Reset All Session"
    Private Sub ResetAllSession()
        Me.SetnGetEmailTemplate_New = Nothing
        Me.SetnGetEmailTemplate_Old = Nothing
        Me.SetnGetGeneral_New = Nothing
        Me.SetnGetGeneral_Old = Nothing
        Me.SetnGetUserIDCreated = Nothing
        Me.SetnGetCreatedDate = Nothing        
    End Sub
#End Region

#Region "Load"
    ''' <summary>
    ''' Page Load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try            
            If Not Me.IsPostBack Then
                Me.ImageAccept.Attributes.Add("onclick", "javascript: if(confirm('Do you want to accept this activity ?')== false) return false;")
                Me.ImageReject.Attributes.Add("onclick", "javascript: if(confirm('Do you want to reject this activity ?')== false) return false;")

                Using AccessProposalSTRApproval As New AMLDAL.ProposalSTRWorkflowSettingsTableAdapters.SelectAllProposalSTRWorkflowGeneralApprovalTableAdapter
                    Dim DtApproval As Data.DataTable = AccessProposalSTRApproval.GetData()
                    If DtApproval.Rows.Count > 0 Then
                        Dim RowApproval As AMLDAL.ProposalSTRWorkflowSettings.SelectAllProposalSTRWorkflowGeneralApprovalRow = DtApproval.Rows(0)
                        Me.SetnGetUserIDCreated = RowApproval.UserId_Approval
                        Me.SetnGetCreatedDate = RowApproval.CreatedDate_Approval
                    End If
                End Using

                Using oAccessEmail_New As New AMLDAL.ProposalSTRWorkflowSettingsTableAdapters.SelectAllProposalSTRWorkflowEmailTemplateApprovalTableAdapter
                    Me.SetnGetEmailTemplate_New = oAccessEmail_New.GetData()
                End Using

                If Me.GetMode.ToLower = "add" Then
                    Me.MultiViewApprovalDetail.ActiveViewIndex = 0
                    ShowDetailProposalWorkflowAdd()
                Else
                    Me.MultiViewApprovalDetail.ActiveViewIndex = 1
                    Me.ShowDetailCaseManagementWorkflowEdit()

                    Using oAccessEmail_Old As New AMLDAL.ProposalSTRWorkflowSettingsTableAdapters.SelectAllProposalSTRWorkflowEmailTemplateTableAdapter
                        Me.SetnGetEmailTemplate_New = oAccessEmail_Old.GetData()
                    End Using
                End If
            End If
            Me.ResetEmail()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ShowDetailCaseManagementWorkflowEdit()
        Try

            ' Get Data New Value
            Using oAcessSTRGeneralApproval_New As New AMLDAL.ProposalSTRWorkflowSettingsTableAdapters.SelectAllProposalSTRWorkflowGeneralApprovalTableAdapter
                Dim DtApprovalDetail_General_New As AMLDAL.ProposalSTRWorkflowSettings.SelectAllProposalSTRWorkflowGeneralApprovalDataTable = oAcessSTRGeneralApproval_New.GetData()
                Me.GridViewEdit_New.DataSource = DtApprovalDetail_General_New
                Me.GridViewEdit_New.DataBind()

                Me.SetnGetGeneral_New = DtApprovalDetail_General_New

                For Each Rows As GridViewRow In Me.GridViewEdit_New.Rows
                    Dim GridViewApprovers_New As GridView = Rows.Cells(3).Controls(1) '
                    Dim GridViewDueDate_New As GridView = Rows.Cells(4).Controls(1) '
                    Dim GridViewNotifyOthers_New As GridView = Rows.Cells(5).Controls(1) '
                    Dim SerialNo As Integer = Rows.Cells(0).Text

                    Using oAccessSTRApprovalDetail_New As New AMLDAL.ProposalSTRWorkflowSettingsTableAdapters.SelectAllProposalSTRWorkflowDetailApproval_BySerialNoTableAdapter
                        Dim DtApprovalDetail_DetailCMW_New As AMLDAL.ProposalSTRWorkflowSettings.SelectAllProposalSTRWorkflowDetailApproval_BySerialNoDataTable = oAccessSTRApprovalDetail_New.GetData(SerialNo)
                        GridViewApprovers_New.DataSource = DtApprovalDetail_DetailCMW_New
                        GridViewApprovers_New.DataBind()
                        GridViewDueDate_New.DataSource = DtApprovalDetail_DetailCMW_New
                        GridViewDueDate_New.DataBind()
                        GridViewNotifyOthers_New.DataSource = DtApprovalDetail_DetailCMW_New
                        GridViewNotifyOthers_New.DataBind()
                    End Using
                Next
            End Using

            ' Get Data Old Value
            Using oAcessSTRApprovalDetail_General_Old As New AMLDAL.ProposalSTRWorkflowSettingsTableAdapters.SelectAllProposalSTRWorkflowGeneralTableAdapter
                Dim DtApprovalDetail_General_Old As AMLDAL.ProposalSTRWorkflowSettings.SelectAllProposalSTRWorkflowGeneralDataTable = oAcessSTRApprovalDetail_General_Old.GetData()

                Me.GridViewEdit_Old.DataSource = DtApprovalDetail_General_Old
                Me.GridViewEdit_Old.DataBind()

                For Each Rows As GridViewRow In Me.GridViewEdit_Old.Rows
                    Dim GridViewApprovers_Old As GridView = Rows.Cells(3).Controls(1) '
                    Dim GridViewDueDate_Old As GridView = Rows.Cells(4).Controls(1) '
                    Dim GridViewNotifyOthers_Old As GridView = Rows.Cells(5).Controls(1) '
                    Dim SerialNo_Old As Integer = Rows.Cells(0).Text

                    Using oAccessSTRApproval_Detail_Old As New AMLDAL.ProposalSTRWorkflowSettingsTableAdapters.SelectAllProposalSTRWorkflowDetail_BySerialNoTableAdapter
                        Dim DtApprovalDetail_DetailCMW_Old As AMLDAL.ProposalSTRWorkflowSettings.SelectAllProposalSTRWorkflowDetail_BySerialNoDataTable = oAccessSTRApproval_Detail_Old.GetData(SerialNo_Old)
                        GridViewApprovers_Old.DataSource = DtApprovalDetail_DetailCMW_Old
                        GridViewApprovers_Old.DataBind()
                        GridViewDueDate_Old.DataSource = DtApprovalDetail_DetailCMW_Old
                        GridViewDueDate_Old.DataBind()
                        GridViewNotifyOthers_Old.DataSource = DtApprovalDetail_DetailCMW_Old
                        GridViewNotifyOthers_Old.DataBind()
                    End Using
                Next
            End Using
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' ShowDetailCaseManagementWorkflowAdd
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ShowDetailProposalWorkflowAdd()
        Try
            Using oAcessProposalSTRApproval_General As New AMLDAL.ProposalSTRWorkflowSettingsTableAdapters.SelectAllProposalSTRWorkflowGeneralApprovalTableAdapter
                Dim DtApproval_General As AMLDAL.ProposalSTRWorkflowSettings.SelectAllProposalSTRWorkflowGeneralApprovalDataTable = oAcessProposalSTRApproval_General.GetData()
                Me.GridViewAddApproval_General.DataSource = DtApproval_General
                Me.GridViewAddApproval_General.DataBind()

                ' Set General Ke Data Table
                Me.SetnGetGeneral_New = DtApproval_General

                For Each Rows As GridViewRow In Me.GridViewAddApproval_General.Rows
                    Dim GridViewApprovers As GridView = Rows.Cells(3).Controls(1) '
                    Dim GridViewDueDate As GridView = Rows.Cells(4).Controls(1) '
                    Dim GridViewNotifyOthers As GridView = Rows.Cells(5).Controls(1) '
                    Dim SerialNo As Integer = Rows.Cells(0).Text

                    Using oAccessProposalSTRDetail_Approval As New AMLDAL.ProposalSTRWorkflowSettingsTableAdapters.SelectAllProposalSTRWorkflowDetailApproval_BySerialNoTableAdapter
                        Dim DtProposalSTRDetail_Approval As AMLDAL.ProposalSTRWorkflowSettings.SelectAllProposalSTRWorkflowDetailApproval_BySerialNoDataTable = oAccessProposalSTRDetail_Approval.GetData(SerialNo)
                        GridViewApprovers.DataSource = DtProposalSTRDetail_Approval
                        GridViewApprovers.DataBind()
                        GridViewDueDate.DataSource = DtProposalSTRDetail_Approval
                        GridViewDueDate.DataBind()
                        GridViewNotifyOthers.DataSource = DtProposalSTRDetail_Approval
                        GridViewNotifyOthers.DataBind()
                    End Using
                Next
            End Using
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' Row DataBound Approvers Add
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub GridViewApprovers_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                e.Row.Cells(0).Text = Me.GetUserIdByPk(e.Row.Cells(0).Text)
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
    ''' <summary>
    ''' Row DataBound Notify Others Add
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub GridViewNotifyOthers_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                e.Row.Cells(0).Text = Me.GetUserIdByPk(e.Row.Cells(0).Text)
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub
    ''' <summary>
    ''' Get UserId By Pk
    ''' </summary>
    ''' <param name="PkUserID"></param>
    ''' <returns></returns>iex
    ''' <remarks></remarks>
    Public Function GetUserIdByPk(ByVal PkUserID As String) As String
        Try
            Dim StrUserID As String = Nothing
            If PkUserID <> "" Then
                Dim oArrPkUserId As String() = PkUserID.Split("|")
                Using oAccessUserID As New AMLDAL.CaseManagementWorkflowTableAdapters.GetUserIDByPKTableAdapter
                    For i As Integer = 0 To oArrPkUserId.Length - 1
                        If i = oArrPkUserId.Length - 1 Then
                            StrUserID &= Convert.ToString(oAccessUserID.Fill(Convert.ToInt32(oArrPkUserId(i))))
                        Else
                            StrUserID &= Convert.ToString(oAccessUserID.Fill(Convert.ToInt32(oArrPkUserId(i)))) & ";"
                        End If
                    Next
                End Using
            End If
            Return StrUserID
        Catch
            Throw
        End Try
    End Function
#End Region

#Region "Reset Email"
    Public Sub ResetEmail()
        Me.LblSubjectAdd.Text = ""
        Me.LblSubjectEdit_New.Text = ""
        Me.LblSubjectEdit_Old.Text = ""
        Me.TxtBodyAdd.Text = ""
        Me.TxtBodyEdit_New.Text = ""
        Me.TxtBodyEdit_Old.Text = ""
    End Sub
#End Region

#Region "Accept, Reject dan Cancel"
    ''' <summary>
    ''' Accept 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click
        Try
            Me.SaveAccept()
            Me.AuditTrail("Accepted")
            Me.Response.Redirect("ProposalSTRWorkflowSettingsApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        Finally
            Me.ResetAllSession()
        End Try
    End Sub
    ''' <summary>
    ''' Save Accept
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub SaveAccept()
        Dim Trans As SqlTransaction

        Try
            'Using oTrans As New Transactions.TransactionScope
            If Me.GetMode = "Edit" Then
                Using oAccessEmailTemplate_Old As New AMLDAL.ProposalSTRWorkflowSettingsTableAdapters.SelectAllProposalSTRWorkflowEmailTemplateTableAdapter
                    SetnGetEmailTemplate_Old = oAccessEmailTemplate_Old.GetData()
                End Using
                Using oAccessDeleteSTR As New AMLDAL.ProposalSTRWorkflowSettingsTableAdapters.TransTableAdapterTableAdapter
                    Trans = Sahassa.AML.TableAdapterHelper.BeginTransaction(oAccessDeleteSTR)

                    oAccessDeleteSTR.DeleteAllProposalSTRWorkflow()
                End Using
            End If

            Using oAccessSTRApproval As New AMLDAL.ProposalSTRWorkflowSettingsTableAdapters.SelectAllProposalSTRWorkflowGeneralApprovalTableAdapter
                Dim DtSTRApproval As Data.DataTable = oAccessSTRApproval.GetData()

                If DtSTRApproval.Rows.Count > 0 Then
                    Using oAccessDML_STR As New AMLDAL.ProposalSTRWorkflowSettingsTableAdapters.TransTableAdapterTableAdapter
                        If Me.GetMode = "Edit" Then
                            Sahassa.AML.TableAdapterHelper.SetTransaction(oAccessDML_STR, Trans)
                        Else
                            Trans = Sahassa.AML.TableAdapterHelper.BeginTransaction(oAccessDML_STR)
                        End If
                        ' Insert General
                        For Each RowSTRApproval As AMLDAL.ProposalSTRWorkflowSettings.SelectAllProposalSTRWorkflowGeneralApprovalRow In DtSTRApproval.Rows
                            oAccessDML_STR.InsertProposalSTRWorkflowGeneral(RowSTRApproval.SerialNo_Approval, RowSTRApproval.WorkflowStep_Approval, RowSTRApproval.Paralel_Approval, RowSTRApproval.CreatedDate_Approval, Now)
                        Next

                        ' Get Detail dan Save
                        Using oAccessApprovalDetail As New AMLDAL.ProposalSTRWorkflowSettingsTableAdapters.SelectAllProposalSTRWorkflowSettingsDetail_ApprovalTableAdapter
                            Dim DtApprovalDetail As AMLDAL.ProposalSTRWorkflowSettings.SelectAllProposalSTRWorkflowSettingsDetail_ApprovalDataTable = oAccessApprovalDetail.GetData()
                            If DtApprovalDetail.Rows.Count > 0 Then
                                For Each RowsDetail As AMLDAL.ProposalSTRWorkflowSettings.SelectAllProposalSTRWorkflowSettingsDetail_ApprovalRow In DtApprovalDetail.Rows
                                    oAccessDML_STR.InsertProposalSTRWorkflowDetail(RowsDetail.SerialNoDetail_Approval, RowsDetail.ApproversDetail_Approval, RowsDetail.DueDateDetail_Approval, RowsDetail.NotifyOthersDetail_Approval)
                                Next
                            End If
                        End Using

                        ' Get Email Template dan Save 
                        Using oAccessApprovalEmailTemplate As New AMLDAL.ProposalSTRWorkflowSettingsTableAdapters.SelectAllProposalSTRWorkflowEmailTemplateApprovalTableAdapter
                            Dim DtApprovalEmailTemplate As AMLDAL.ProposalSTRWorkflowSettings.SelectAllProposalSTRWorkflowEmailTemplateApprovalDataTable = oAccessApprovalEmailTemplate.GetData()
                            If DtApprovalEmailTemplate.Rows.Count > 0 Then
                                Me.SetnGetEmailTemplate_New = DtApprovalEmailTemplate
                                For Each RowsEmailTemplate As AMLDAL.ProposalSTRWorkflowSettings.SelectAllProposalSTRWorkflowEmailTemplateApprovalRow In DtApprovalEmailTemplate.Rows
                                    oAccessDML_STR.InsertProposalSTRWorkflowEmailTemplate(RowsEmailTemplate.SerialNo_Approval, RowsEmailTemplate.Subject_EmailTemplate_Approval, RowsEmailTemplate.Body_EmailTemplate_Approval)
                                Next
                            End If
                        End Using
                    End Using
                End If
            End Using

            ' Delete Approval
            DeleteSTRApproval(Trans, 2)
            Trans.Commit()
            'oTrans.Complete()
            'End Using
        Catch
            If Not Trans Is Nothing Then Trans.Rollback()
            Throw
        Finally
            If Not Trans Is Nothing Then
                Trans.Dispose()
            End If
        End Try
    End Sub
    ''' <summary>
    ''' Delete Approval
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub DeleteSTRApproval(ByRef TransRef As SqlTransaction, ByVal Mode As Int16)
        Using oAccessDeleteSTRApproval As New AMLDAL.ProposalSTRWorkflowSettingsTableAdapters.TransTableAdapterTableAdapter
            If Mode = 1 Then
                TransRef = Sahassa.AML.TableAdapterHelper.BeginTransaction(oAccessDeleteSTRApproval)
            Else
                Sahassa.AML.TableAdapterHelper.SetTransaction(oAccessDeleteSTRApproval, TransRef)
            End If
            oAccessDeleteSTRApproval.DeleteAllProposalSTRWorkflowApproval()
        End Using
    End Sub
    ''' <summary>
    ''' Reject
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click
        Dim Trans As SqlTransaction = Nothing
        Try
            Me.AuditTrail("Rejected")
            ' Delete All Approval
            'Using oTrans As New Transactions.TransactionScope
            DeleteSTRApproval(Trans, 1)
            'oTrans.Complete()
            'End Using
            If Not Trans Is Nothing Then Trans.Commit()
            Me.Response.Redirect("ProposalSTRWorkflowSettingsApproval.aspx", False)
        Catch ex As Exception
            If Not Trans Is Nothing Then Trans.Rollback()
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        Finally
            If Not Trans Is Nothing Then
                Trans.Dispose()
                Trans = Nothing
            End If
            Me.ResetAllSession()
        End Try
    End Sub
    ''' <summary>
    ''' Cancel
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Me.Response.Redirect("ProposalSTRWorkflowSettingsApproval.aspx", False)
        Me.ResetAllSession()
    End Sub
#End Region

#Region "Hide Multiview Email Template"

    Protected Sub LinkHideEmailTemplateAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkHideEmailTemplateAdd.Click
        Me.MultiViewEmailTemplate.ActiveViewIndex = -1
    End Sub

    Protected Sub LinkHideEmailTemplateEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkHideEmailTemplateEdit.Click
        Me.MultiViewEmailTemplate.ActiveViewIndex = -1
    End Sub

#End Region

#Region "Show Email Template"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub LinkButtonEmailTemplateOld_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.ShowEmailTemplate(sender)
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub LinkButtonEmailTemplateNew_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.ShowEmailTemplate(sender)
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <remarks></remarks>
    Private Sub ShowEmailTemplate(ByVal sender As Object)
        Try
            Me.MultiViewEmailTemplate.ActiveViewIndex = 1

            Dim oLinkEmailTemplate As LinkButton = sender
            Dim oGridViewRow As GridViewRow = CType(oLinkEmailTemplate.NamingContainer, GridViewRow)
            Dim IntSerialNo As Integer = oGridViewRow.Cells(0).Text

            Using oAccessEmailTemplate_New As New AMLDAL.ProposalSTRWorkflowSettingsTableAdapters.SelectAllProposalSTRWorkflowEmailTemplateApproval_BySerialNoTableAdapter
                Using Dt_New As Data.DataTable = oAccessEmailTemplate_New.GetData(IntSerialNo)
                    If Dt_New.Rows.Count > 0 Then
                        Dim Row As AMLDAL.ProposalSTRWorkflowSettings.SelectAllProposalSTRWorkflowEmailTemplateApproval_BySerialNoRow = Dt_New.Rows(0)
                        Me.LblSubjectEdit_New.Text = Row.Subject_EmailTemplate_Approval
                        Me.TxtBodyEdit_New.Text = Row.Body_EmailTemplate_Approval
                    End If
                End Using
            End Using

            Using oAccessEmailTemplate_Old As New AMLDAL.ProposalSTRWorkflowSettingsTableAdapters.SelectAllProposalSTRWorkflowEmailTemplate_BySerialNoTableAdapter
                Using Dt_Old As Data.DataTable = oAccessEmailTemplate_Old.GetData(IntSerialNo)
                    If Dt_Old.Rows.Count > 0 Then
                        Dim Row As AMLDAL.ProposalSTRWorkflowSettings.SelectAllProposalSTRWorkflowEmailTemplate_BySerialNoRow = Dt_Old.Rows(0)
                        Me.LblSubjectEdit_Old.Text = Row.Subject_EmailTemplate
                        Me.TxtBodyEdit_Old.Text = Row.Body_EmailTemplate
                    End If
                End Using
            End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

    ''' <summary>
    ''' Link Email Add
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub LinkEmail_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim BtnShowEmailTemplate_Add As LinkButton = sender
            Dim GridViewRow_Add As GridViewRow = BtnShowEmailTemplate_Add.NamingContainer
            Dim IntSerial As Integer = GridViewRow_Add.Cells(0).Text
            Using oAccessEmailAddressApproval_Add As New AMLDAL.ProposalSTRWorkflowSettingsTableAdapters.SelectAllProposalSTRWorkflowEmailTemplateApproval_BySerialNoTableAdapter
                Dim DtEmail As Data.DataTable = oAccessEmailAddressApproval_Add.GetData(IntSerial)
                Me.MultiViewEmailTemplate.ActiveViewIndex = 0 ' untuk add
                Me.LblSubjectAdd.Text = DtEmail.Rows(0)(1).ToString
                Me.TxtBodyAdd.Text = DtEmail.Rows(0)(2).ToString
            End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
        End Try
    End Sub

#End Region

#Region "Insert AuditTrail"
    ''' <summary>
    ''' AuditTrail
    ''' </summary>
    ''' <param name="Action"></param>
    ''' <remarks></remarks>
    Private Sub AuditTrail(ByVal Action As String)
        Try
            'Using oTrans As New Transactions.TransactionScope
            If Me.GetMode = "Add" Then
                Sahassa.AML.AuditTrailAlert.CheckMailAlert(10 * Me.GridViewAddApproval_General.Rows.Count)
                Me.InsertIntoAuditTrail(Me.GridViewAddApproval_General, Me.SetnGetUserIDCreated, Me.SetnGetCreatedDate, Action, "Add")
            Else ' Edit
                Sahassa.AML.AuditTrailAlert.CheckMailAlert(10 * (Me.GridViewEdit_Old.Rows.Count + Me.GridViewEdit_New.Rows.Count))
                Me.InsertIntoAuditTrail(Me.GridViewEdit_Old, Me.SetnGetUserIDCreated, Me.SetnGetCreatedDate, Action, "Delete")
                Me.InsertIntoAuditTrail(Me.GridViewEdit_New, Me.SetnGetUserIDCreated, Me.SetnGetCreatedDate, Action, "Add")
            End If

            'oTrans.Complete()
            'End Using
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' Insert Into Audit Trail
    ''' </summary>
    ''' <param name="o_gridview"></param>
    ''' <param name="UserIDCreated"></param>
    ''' <param name="CreatedDate"></param>
    ''' <param name="Action"></param>
    ''' <param name="Mode"></param>
    ''' <remarks></remarks>
    Private Sub InsertIntoAuditTrail(ByVal o_gridview As GridView, ByVal UserIDCreated As String, ByVal CreatedDate As DateTime, ByVal Action As String, ByVal Mode As String)
        Try
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                For Each RowGrid_Old As GridViewRow In o_gridview.Rows
                    Dim SerialNo As String = RowGrid_Old.Cells(0).Text
                    Dim WorkflowStep As String = RowGrid_Old.Cells(1).Text
                    Dim Paralel As String = RowGrid_Old.Cells(2).Text

                    Dim GridViewApprovers As GridView = RowGrid_Old.Cells(3).Controls(1)
                    Dim GridViewDueDate As GridView = RowGrid_Old.Cells(4).Controls(1)
                    Dim GridViewNotifyOthers As GridView = RowGrid_Old.Cells(5).Controls(1)

                    Dim StrTempApprovers As String = ""
                    Dim StrTempDueDate As String = ""
                    Dim StrTempNotify As String = ""
                    For i As Integer = 0 To GridViewApprovers.Rows.Count - 1
                        Dim RowApprovers As GridViewRow = GridViewApprovers.Rows(i)
                        Dim RowDueDate As GridViewRow = GridViewDueDate.Rows(i)
                        Dim RowNotifyOthers As GridViewRow = GridViewNotifyOthers.Rows(i)

                        If i = GridViewApprovers.Rows.Count - 1 Then
                            StrTempApprovers &= RowApprovers.Cells(0).Text
                            StrTempDueDate &= RowDueDate.Cells(0).Text
                            StrTempNotify &= RowNotifyOthers.Cells(0).Text
                        Else
                            StrTempApprovers &= RowApprovers.Cells(0).Text & " | "
                            StrTempDueDate &= RowDueDate.Cells(0).Text & " | "
                            StrTempNotify &= RowNotifyOthers.Cells(0).Text & " | "
                        End If
                    Next

                    Dim RowEmail() As Data.DataRow

                    If Me.GetMode = "Delete" Then
                        RowEmail = Me.SetnGetEmailTemplate_Old.Select("SerialNo=" & SerialNo)
                    Else
                        RowEmail = Me.SetnGetEmailTemplate_New.Select("SerialNo_Approval=" & SerialNo)
                    End If


                    Dim Subject As String = ""
                    Dim Body As String = ""
                    If RowEmail.Length > 0 Then
                        Subject = RowEmail(0)(1).ToString
                        Body = RowEmail(0)(2).ToString
                    End If

                    AccessAudit.Insert(Now, UserIDCreated, Sahassa.AML.Commonly.SessionUserId, "Parameter - ProposalSTRWorkflowSettings", "SerialNo", Mode, "", SerialNo, Action)
                    AccessAudit.Insert(Now, UserIDCreated, Sahassa.AML.Commonly.SessionUserId, "Parameter - ProposalSTRWorkflowSetting", "Workflow Step", Mode, "", WorkflowStep, Action)
                    AccessAudit.Insert(Now, UserIDCreated, Sahassa.AML.Commonly.SessionUserId, "Parameter - ProposalSTRWorkflowSettings", "Paralel", Mode, "", Paralel, Action)
                    AccessAudit.Insert(Now, UserIDCreated, Sahassa.AML.Commonly.SessionUserId, "Parameter - ProposalSTRWorkflowSettings", "Approvers", Mode, "", StrTempApprovers, Action)
                    AccessAudit.Insert(Now, UserIDCreated, Sahassa.AML.Commonly.SessionUserId, "Parameter - ProposalSTRWorkflowSettings", "DueDate", Mode, "", StrTempDueDate, Action)
                    AccessAudit.Insert(Now, UserIDCreated, Sahassa.AML.Commonly.SessionUserId, "Parameter - ProposalSTRWorkflowSettings", "NotifyOthers", Mode, "", StrTempNotify, Action)
                    AccessAudit.Insert(Now, UserIDCreated, Sahassa.AML.Commonly.SessionUserId, "Parameter - ProposalSTRWorkflowSettings", "SubjectEmailTemplate", Mode, "", Subject, Action)
                    AccessAudit.Insert(Now, UserIDCreated, Sahassa.AML.Commonly.SessionUserId, "Parameter - ProposalSTRWorkflowSettings", "BodyEmailTemplate", Mode, "", Body, Action)
                    AccessAudit.Insert(Now, UserIDCreated, Sahassa.AML.Commonly.SessionUserId, "Parameter - ProposalSTRWorkflowSettings", "CreatedDate", Mode, "", CreatedDate, Action)
                    AccessAudit.Insert(Now, UserIDCreated, Sahassa.AML.Commonly.SessionUserId, "Parameter - ProposalSTRWorkflowSettings", "LastUpdatedDate", Mode, "", Now, Action)
                Next
            End Using
        Catch
            Throw
        End Try
    End Sub
#End Region
End Class
