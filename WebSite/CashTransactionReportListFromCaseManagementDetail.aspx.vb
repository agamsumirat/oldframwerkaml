﻿Imports System
Imports System.IO

Partial Class CashTransactionReportListFromCaseManagementDetail
    Inherits Parent

#Region " Property "
    '''' <summary>
    '''' selected item store
    '''' </summary>
    '''' <value></value>
    '''' <returns></returns>
    '''' <remarks></remarks>
    'Private Property SetnGetSelectedItemFilter() As ArrayList
    '    Get
    '        Return IIf(Session("CustomerInformationViewSelected") Is Nothing, New ArrayList, Session("CustomerInformationViewSelected"))
    '    End Get
    '    Set(ByVal value As ArrayList)
    '        Session("CustomerInformationViewSelected") = value
    '    End Set
    'End Property

    ''' <summary>
    ''' selected item store
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSelectedItem() As ArrayList
        Get
            Return IIf(Session("CashTransactionReportSelected") Is Nothing, New ArrayList, Session("CashTransactionReportSelected"))
        End Get
        Set(ByVal value As ArrayList)
            Session("CashTransactionReportSelected") = value
        End Set
    End Property
    ''' <summary>
    ''' setnget search field
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetFieldSearch() As String
        Get
            Return IIf(Session("CashTransactionReportFieldSearch") Is Nothing, "", Session("CashTransactionReportFieldSearch"))
        End Get
        Set(ByVal Value As String)
            Session("CashTransactionReportFieldSearch") = Value
        End Set
    End Property
    ''' <summary>
    ''' search value
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetValueSearch() As String
        Get
            Return IIf(Session("CashTransactionReportValueSearch") Is Nothing, "", Session("CashTransactionReportValueSearch"))
        End Get
        Set(ByVal Value As String)
            Session("CashTransactionReportValueSearch") = Value
        End Set
    End Property
    ''' <summary>
    ''' sort expresion
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetSort() As String
        Get
            Return IIf(Session("CashTransactionReportSort") Is Nothing, "PK_CTRID  asc", Session("CashTransactionReportSort"))
        End Get
        Set(ByVal Value As String)
            Session("CashTransactionReportSort") = Value
        End Set
    End Property
    ''' <summary>
    ''' current page index
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetCurrentPage() As Int32
        Get
            Return IIf(Session("CashTransactionReportCurrentPage") Is Nothing, 0, Session("CashTransactionReportCurrentPage"))
        End Get
        Set(ByVal Value As Int32)
            Session("CashTransactionReportCurrentPage") = Value
        End Set
    End Property
    ''' <summary>
    ''' total pages
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetPageTotal() As Int32
        Get
            Return Sahassa.AML.Commonly.iTotalPages(Me.SetnGetRowTotal)
        End Get
    End Property
    ''' <summary>
    ''' row total
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetRowTotal() As Int32
        Get
            Return IIf(Session("CashTransactionReportRowTotal") Is Nothing, 0, Session("CashTransactionReportRowTotal"))
        End Get
        Set(ByVal Value As Int32)
            Session("CashTransactionReportRowTotal") = Value
        End Set
    End Property
    ''' <summary>
    ''' save bind table
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property SetnGetBindTable() As Data.DataTable
        Get
            Return IIf(Session("CashTransactionReportData") Is Nothing, Sahassa.AML.Commonly.GetDatasetPaging(Me.Tables, Me.PK, Me.SetnGetSort, Me.SetnGetCurrentPage + 1, Me.PageSize, Me.Fields, Me.SetnGetFieldSearch.Replace("-=Search=-", Me.SetnGetValueSearch.Replace("'", "''")), ""), Session("CashTransactionReportData"))
        End Get
        Set(ByVal value As Data.DataTable)
            Session("CashTransactionReportData") = value
        End Set
    End Property

    Private ReadOnly Property Tables() As String
        Get
            Return "CTR INNER JOIN AccountOwner ON CTR.FK_AccountOwnerID = AccountOwner.AccountOwnerId"
        End Get
    End Property

    ''' <summary>
    ''' PK
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property PK() As String
        Get
            Return "CTR.PK_CTRID"
        End Get
    End Property

    ''' <summary>
    ''' Fields
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property Fields() As String
        Get
            Return "CAST(CTR.FK_AccountOwnerId AS VARCHAR) + ' - ' + AccountOwner.AccountOwnerName AS AccountOwner, CTR.PK_CTRID, CTR.CTRDescription, CTR.CreatedDate, CTR.LastUpdated, CTR.PIC, CTR.ReportedBy, CTR.ReportedDate, CTR.PPATKConfirmationNo, CTR.Aging, CTR.CIFNo, CTR.CustomerName, CTR.TotalTransactionValue"
        End Get
    End Property

    Public Property PageSize() As Int16
        Get
            Return IIf(Session("PageSize") IsNot Nothing, Session("PageSize"), 10)
        End Get
        Set(ByVal value As Int16)
            Session("PageSize") = value
        End Set
    End Property
#End Region

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' fill search
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub FillSearch()
        Try
            Me.ComboSearch.Items.Add(New ListItem("...", ""))
            Me.ComboSearch.Items.Add(New ListItem("Case Number", "CTR.PK_CTRID Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Account Owner", " CAST(CTR.FK_AccountOwnerId AS VARCHAR) + ' - ' + AccountOwner.AccountOwnerName Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("CIFNo", "CTR.CIFNo Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Customer Name", "CTR.CustomerName Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("TotalTransactionValue", "CTR.TotalTransactionValue Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Case Description", "CTR.CTRDescription Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Created Date", "CTR.CreatedDate >= '-=Search=- 00:00' AND CTR.CreatedDate <= '-=Search=- 23:59'"))
            Me.ComboSearch.Items.Add(New ListItem("Last Updated", "CTR.LastUpdated >= '-=Search=- 00:00' AND CTR.LastUpdated <= '-=Search=- 23:59'"))
            Me.ComboSearch.Items.Add(New ListItem("PIC", "CTR.PIC Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Reported By", "CTR.ReportedBy Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Reported Date", "CTR.ReportedDate >= '-=Search=- 00:00' AND CTR.ReportedDate <= '-=Search=- 23:59'"))
            Me.ComboSearch.Items.Add(New ListItem("PPATK Confirmation Number", "CTR.PPATKConfirmationNo Like '%-=Search=-%'"))
            Me.ComboSearch.Items.Add(New ListItem("Aging", "Aging LIKE '-=Search=-'"))
            'Me.ComboSearch.Items.Add(New ListItem("Aging", "CASE CTR.IsReportedToRegulator WHEN 1 THEN (DATEDIFF(DAY, CTR.CreatedDate, CTR.ReportedDate)) ELSE (DATEDIFF(DAY, CTR.CreatedDate, GETDATE())) END = '-=Search=-'"))
        Catch
            Throw
        End Try
    End Sub

    Private Sub ClearThisPageSessions()
        Session("CashTransactionReportSelected") = Nothing
        Session("CashTransactionReportFieldSearch") = Nothing
        Session("CashTransactionReportValueSearch") = Nothing
        Session("CashTransactionReportSort") = Nothing
        Session("CashTransactionReportCurrentPage") = Nothing
        Session("CashTransactionReportRowTotal") = Nothing
        Session("CashTransactionReportData") = Nothing
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not Me.IsPostBack Then
                Me.ClearThisPageSessions()
                Me.ComboSearch.Attributes.Add("onchange", "javascript:CheckNoPopUp('" & Me.TextSearch.ClientID & "');")
                Me.ComboSearch.Attributes.Add("onchange", "javascript:Check3Calendar(this,'" & Me.TextSearch.ClientID & "', '" & Me.popUp.ClientID & "',7,8,11);")
                Me.popUp.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TextSearch.ClientID & "'), 'dd-mmm-yyyy')")
                Me.popUp.Style.Add("display", "none")

                Me.FillSearch()
                Me.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow
                Me.GridMSUserView.PageSize = Sahassa.AML.Commonly.GetDisplayedTotalRow

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' page navigate button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Sub PageNavigate(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            Select Case e.CommandName
                Case "First" : Me.SetnGetCurrentPage = 0
                Case "Prev" : Me.SetnGetCurrentPage -= 1
                Case "Next" : Me.SetnGetCurrentPage += 1
                Case "Last" : Me.SetnGetCurrentPage = Me.GetPageTotal - 1
                Case Else : Throw New Exception("Unknown Commandname " & e.CommandName)
            End Select
            Me.CollectSelected()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' bind grid
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Sub BindGrid()
        Try
            Dim whereClause As String = ""
            If Me.SetnGetFieldSearch.Replace("-=Search=-", Me.SetnGetValueSearch.Replace("'", "''")).Trim.Length > 0 Then
                whereClause = Me.SetnGetFieldSearch.Replace("-=Search=-", Me.SetnGetValueSearch.Replace("'", "''")) & " And CTR.CIFNo='" & Request.Params("CIFNo") & "' And CTR.CreatedDate Between '" & Request.Params("Start") & "' And '" & Request.Params("End") & "' "
            Else
                whereClause = "CTR.CIFNo='" & Request.Params("CIFNo") & "' And CTR.CreatedDate Between '" & Request.Params("Start") & "' And '" & Request.Params("End") & "' "
            End If
            Me.SetnGetBindTable = Sahassa.AML.Commonly.GetDatasetPaging(Me.Tables, Me.PK, Me.SetnGetSort, Me.SetnGetCurrentPage + 1, Me.PageSize, Me.Fields, whereClause, "")
            Me.GridMSUserView.DataSource = Me.SetnGetBindTable
            Me.SetnGetRowTotal = Sahassa.AML.Commonly.GetTableCount(Me.Tables, Me.SetnGetFieldSearch.Replace("-=Search=-", Me.SetnGetValueSearch.Replace("'", "''")))
            'Using AccessCustomerInformation As New AMLDAL.CTRReportTableAdapters.pCTRRecordCounterTableAdapter
            '    Dim Filter As String = Me.SetnGetFieldSearch.Replace("-=Search=-", Me.SetnGetValueSearch.Replace("'", "''"))
            '    Dim TotalRow As Int64
            '    If Filter <> "" Then
            '        Using DataTableInformation As AMLDAL.CTRReport.pCTRRecordCounterDataTable = AccessCustomerInformation.GetCTRRowCounter("WHERE " & Filter)
            '            TotalRow = DataTableInformation.Rows(0).Item(0)
            '        End Using
            '    Else
            '        Using DataTableInformation As AMLDAL.CTRReport.pCTRRecordCounterDataTable = AccessCustomerInformation.GetCTRRowCounter(Filter)
            '            TotalRow = DataTableInformation.Rows(0).Item(0)
            '        End Using
            '    End If
            '    Me.SetnGetRowTotal = TotalRow
            'End Using
            Me.GridMSUserView.VirtualItemCount = Me.SetnGetRowTotal
            Me.GridMSUserView.DataBind()
        Catch
            Throw
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' set navigate info
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub SetInfoNavigate()
        Try
            Me.PageCurrentPage.Text = Me.SetnGetCurrentPage + 1
            Me.PageTotalPages.Text = Me.GetPageTotal
            Me.PageTotalRows.Text = Me.SetnGetRowTotal
            Me.LinkButtonNext.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonLast.Enabled = (Not Me.SetnGetCurrentPage + 1 = Me.GetPageTotal) AndAlso Me.GetPageTotal <> 0
            Me.LinkButtonFirst.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.LinkButtonPrevious.Enabled = Not Me.SetnGetCurrentPage = 0
            Me.ComboSearch.SelectedValue = Me.SetnGetFieldSearch
            Me.TextSearch.Text = Me.SetnGetValueSearch
        Catch
            Throw
        End Try
    End Sub

    Protected Sub GridMSUserView_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridMSUserView.DeleteCommand
        Response.Redirect("CashTransactionReportPrint.aspx?PK_CTRID=" & e.Item.Cells(1).Text.Trim & "&CIFNo=" & e.Item.Cells(3).Text.Trim, False)
    End Sub

    Protected Sub GridMSUserView_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridMSUserView.EditCommand
        Response.Redirect("CashTransactionReportDetail.aspx?PK_CTRID=" & e.Item.Cells(1).Text.Trim & "&AccountOwner=" & e.Item.Cells(2).Text.Trim & "&CIFNo=" & e.Item.Cells(3).Text.Trim & "&CustomerName=" & e.Item.Cells(4).Text.Trim & "&TotalTransactionValue=" & e.Item.Cells(5).Text.Trim & "&CTRDescription=" & e.Item.Cells(6).Text.Trim, False)
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' change sort expression
    ''' </summary>
    ''' <param name="source"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles GridMSUserView.SortCommand
        Dim GridUser As DataGrid = source
        Try
            Me.SetnGetSort = Sahassa.AML.Commonly.ChangeSortCommand(e.SortExpression)
            GridUser.Columns(Sahassa.AML.Commonly.IndexSort(GridUser, e.SortExpression)).SortExpression = Me.SetnGetSort
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' image button search
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageButtonSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonSearch.Click
        Try
            'Me.SetnGetFieldSearch = Me.ComboSearch.SelectedValue
            'If Me.ComboSearch.SelectedIndex = 0 Then
            '    Me.TextSearch.Text = ""
            'End If
            'Me.SetnGetValueSearch = Me.TextSearch.Text
            'Me.SetnGetCurrentPage = 0
            Me.CollectSelected()
            Me.SetnGetFieldSearch = Me.ComboSearch.SelectedValue
            If Me.ComboSearch.SelectedIndex = 0 Then
                Me.TextSearch.Text = ""
                Me.SetnGetValueSearch = Me.TextSearch.Text
            ElseIf Me.ComboSearch.SelectedIndex = 7 Or Me.ComboSearch.SelectedIndex = 8 Or Me.ComboSearch.SelectedIndex = 11 Then
                Dim DateSearch As DateTime
                Try
                    DateSearch = DateTime.Parse(Me.TextSearch.Text, New System.Globalization.CultureInfo("id-ID"))
                Catch ex As ArgumentOutOfRangeException
                    Throw New Exception("Input character cannot be convert to datetime.")
                Catch ex As FormatException
                    Throw New Exception("Unknown format date")
                End Try
                Me.SetnGetValueSearch = DateSearch.ToString("yyyy-MM-dd")
            Else
                Me.SetnGetValueSearch = Me.TextSearch.Text
            End If
            Me.SetnGetCurrentPage = 0
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    '''' -----------------------------------------------------------------------------
    '''' <summary>
    '''' cek if groupID already edited
    '''' </summary>
    '''' <param name="strgroupID"></param>
    '''' <returns></returns>
    '''' <remarks>
    '''' </remarks>
    '''' <history>
    '''' 	[Ariwibawa]	05/06/2006	Created
    '''' </history>
    '''' -----------------------------------------------------------------------------
    'Private Function IsPendingApproval(ByVal strgroupID As String) As Boolean
    '    Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.Group_ApprovalTableAdapter
    '        Dim count As Int32 = AccessPending.CountGroupApproval(strgroupID)
    '        If count > 0 Then
    '            Return True
    '        Else
    '            Return False
    '        End If
    '    End Using
    'End Function

    '''' <summary>
    '''' get row user
    '''' </summary>
    '''' <param name="strgroupID"></param>
    '''' <returns></returns>
    '''' <remarks></remarks>
    'Private Function GetRowGroup(ByVal strgroupID As String) As AMLDAL.AMLDataSet.GroupRow
    '    Using AccessGroup As New AMLDAL.AMLDataSetTableAdapters.GroupTableAdapter
    '        Return AccessGroup.GetDataByGroupID(strgroupID).Rows(0)
    '    End Using
    'End Function

    '''' -----------------------------------------------------------------------------
    '''' <summary>
    '''' cek if groupID is being used by a user
    '''' </summary>
    '''' <param name="strgroupID"></param>
    '''' <returns></returns>
    '''' <remarks>
    '''' </remarks>
    '''' <history>
    '''' 	[Ariwibawa]	05/06/2006	Created
    '''' </history>
    '''' -----------------------------------------------------------------------------
    'Private Function IsBeingUsedByUser(ByVal strgroupID As String) As Boolean
    '    Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.GroupTableAdapter
    '        Dim count As Int32 = AccessPending.CountGroupUsedByUser(strgroupID)
    '        If count > 0 Then
    '            Return True
    '        Else
    '            Return False
    '        End If
    '    End Using
    'End Function

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' go button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	14/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub ImageButtonGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButtonGo.Click
        Try
            Me.CollectSelected()
            If IsNumeric(Me.TextGoToPage.Text) Then
                If (CInt(Me.TextGoToPage.Text) > 0) Then
                    If (CInt(Me.TextGoToPage.Text) <= CInt(PageTotalPages.Text)) Then
                        Me.SetnGetCurrentPage = Int32.Parse(Me.TextGoToPage.Text) - 1
                    Else
                        Throw New Exception("Page number must be less than or equal to the total page count.")
                    End If
                Else
                    Throw New Exception("Page number must be a positive number greater than zero.")
                End If
            Else
                Throw New Exception("Page number must be a positive number greater than zero.")
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    ''' <summary>
    ''' collect sub
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CollectSelected()
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim chkBox As CheckBox = gridRow.FindControl("CheckBoxExporttoExcel")
                Dim groupID As String = gridRow.Cells(1).Text
                Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
                If ArrTarget.Contains(groupID) Then
                    If Not chkBox.Checked Then
                        ArrTarget.Remove(groupID)
                    End If
                Else
                    If chkBox.Checked Then ArrTarget.Add(groupID)
                End If
                Me.SetnGetSelectedItem = ArrTarget
            End If
        Next
    End Sub

    ''' <summary>
    ''' prerender event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.BindGrid()
            Me.SetInfoNavigate()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    ''' <summary>
    ''' clear all control except control
    ''' </summary>
    ''' <param name="control">excluded control</param>
    ''' <remarks></remarks>
    Private Sub ClearControls(ByVal control As Control)
        Dim i As Integer
        For i = control.Controls.Count - 1 To 0 Step -1
            ClearControls(control.Controls(i))
        Next i
        If Not TypeOf control Is TableCell Then
            If Not (control.GetType().GetProperty("SelectedItem") Is Nothing) Then
                Dim literal As New LiteralControl
                control.Parent.Controls.Add(literal)
                Try
                    literal.Text = CStr(control.GetType().GetProperty("SelectedItem").GetValue(control, Nothing))
                Catch
                End Try
                control.Parent.Controls.Remove(control)
            Else
                If Not (control.GetType().GetProperty("Text") Is Nothing) Then
                    Dim literal As New LiteralControl
                    control.Parent.Controls.Add(literal)
                    literal.Text = CStr(control.GetType().GetProperty("Text").GetValue(control, Nothing))
                    control.Parent.Controls.Remove(control)
                End If
            End If
        End If
    End Sub 'ClearControls

    ''' <summary>
    ''' bind selected item
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub BindSelected()
        Try
            Dim Rows As New ArrayList
            Dim oTableCTR As New Data.DataTable
            Dim oRowCTR As Data.DataRow
            oTableCTR.Columns.Add("PK_CTRID", GetType(String))
            oTableCTR.Columns.Add("AccountOwner", GetType(String))
            oTableCTR.Columns.Add("CIFNo", GetType(String))
            oTableCTR.Columns.Add("CustomerName", GetType(String))
            oTableCTR.Columns.Add("TotalTransactionValue", GetType(String))
            oTableCTR.Columns.Add("CTRDescription", GetType(String))
            oTableCTR.Columns.Add("CreatedDate", GetType(String))
            oTableCTR.Columns.Add("LastUpdated", GetType(String))
            oTableCTR.Columns.Add("PIC", GetType(String))
            oTableCTR.Columns.Add("ReportedBy", GetType(String))
            oTableCTR.Columns.Add("ReportedDate", GetType(String))
            oTableCTR.Columns.Add("PPATKConfirmationNo", GetType(String))
            oTableCTR.Columns.Add("Aging", GetType(Integer))

            For Each IdPk As String In Me.SetnGetSelectedItem
                Dim rowData() As Data.DataRow = Me.SetnGetBindTable.Select("PK_CTRID = '" & IdPk & "'")
                If rowData.Length > 0 Then
                    oRowCTR = oTableCTR.NewRow
                    oRowCTR("PK_CTRID") = rowData(0)("PK_CTRID")
                    oRowCTR("AccountOwner") = rowData(0)("AccountOwner")
                    oRowCTR("CIFNo") = rowData(0)("CIFNo")
                    oRowCTR("CustomerName") = rowData(0)("CustomerName")
                    oRowCTR("TotalTransactionValue") = rowData(0)("TotalTransactionValue")
                    oRowCTR("CTRDescription") = rowData(0)("CTRDescription")
                    oRowCTR("CreatedDate") = rowData(0)("CreatedDate")
                    oRowCTR("LastUpdated") = rowData(0)("LastUpdated")
                    oRowCTR("PIC") = rowData(0)("PIC")
                    oRowCTR("ReportedBy") = rowData(0)("ReportedBy")
                    oRowCTR("ReportedDate") = rowData(0)("ReportedDate")
                    oRowCTR("PPATKConfirmationNo") = rowData(0)("PPATKConfirmationNo")
                    oRowCTR("Aging") = rowData(0)("Aging")
                    oTableCTR.Rows.Add(oRowCTR)
                End If
            Next
            Me.GridMSUserView.DataSource = oTableCTR
            Me.GridMSUserView.AllowPaging = False
            Me.GridMSUserView.DataBind()

            'Sembunyikan kolom ke 0,14 agar tidak ikut diekspor ke excel
            Me.GridMSUserView.Columns(0).Visible = False
            Me.GridMSUserView.Columns(14).Visible = False
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' export button 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Me.CollectSelected()
            Dim strStyle As String = "<style>.text { mso-number-format:\@; } </style>"
            Me.BindSelected()
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=CashTransactionReport.xls")
            Response.Charset = ""
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            Me.EnableViewState = False
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            ClearControls(GridMSUserView)
            GridMSUserView.RenderControl(htmlWrite)
            Response.Write(strStyle)
            Response.Write(stringWrite.ToString())
            Response.End()
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get item bound
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	05/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub GridMSUserView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridMSUserView.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then
                Dim chkBox As CheckBox = e.Item.FindControl("CheckBoxExporttoExcel")
                chkBox.Checked = Me.SetnGetSelectedItem.Contains(e.Item.Cells(1).Text)

            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' select all
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub CheckBoxSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBoxSelectAll.CheckedChanged
        'For Each gridRow As DataGridItem In Me.GridMSUserView.Items
        '    If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
        '        Dim GroupID As String = gridRow.Cells(1).Text
        '        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        '        If Me.SetnGetSelectedItem.Contains(GroupID) Then
        '            If Me.CheckBoxSelectAll.Checked Then
        '                ArrTarget.Add(GroupID)
        '            Else
        '                ArrTarget.Remove(GroupID)
        '            End If
        '        Else
        '            If Me.CheckBoxSelectAll.Checked Then
        '                ArrTarget.Add(GroupID)
        '            End If
        '        End If
        '        Me.SetnGetSelectedItem = ArrTarget
        '    End If
        'Next

        Dim ArrTarget As ArrayList = Me.SetnGetSelectedItem
        For Each gridRow As DataGridItem In Me.GridMSUserView.Items
            If gridRow.ItemType = ListItemType.Item OrElse gridRow.ItemType = ListItemType.AlternatingItem Then
                Dim StrKey As String = gridRow.Cells(1).Text
                If Me.CheckBoxSelectAll.Checked Then
                    If Me.SetnGetSelectedItem.Contains(StrKey) = False Then
                        ArrTarget.Add(StrKey)
                    End If
                Else
                    If Me.SetnGetSelectedItem.Contains(StrKey) Then
                        ArrTarget.Remove(StrKey)
                    End If
                End If
            End If
        Next
        Me.SetnGetSelectedItem = ArrTarget
    End Sub

End Class
