<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="AuditTrailPotensialScreeningCustomerDetail.aspx.vb" Inherits="AuditTrailPotensialScreeningCustomerDetail"
     %>

<%@ Register Assembly="MagicAjax" Namespace="MagicAjax.UI.Controls" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" runat="Server">

    <script src="script/popcalendar.js"></script>

    <span defaultbutton="ImgBtnSearch">
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tr>
                <td>
                </td>
                <td id="tdcontent" height="99%" valign="top" bgcolor="#FFFFFF">
                    <div id="divcontent" class="divcontent">
                        <table id="tblpenampung" cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tr>
                                <td>
                                    <img src="Images/blank.gif" width="20" height="100%" /></td>
                                <td class="divcontentinside" bgcolor="#FFFFFF">
                                    <ajax:AjaxPanel ID="AjaxPanel5" runat="server" Width="100%">
                                        <table id="title" border="2" bordercolor="#ffffff" cellpadding="2" cellspacing="1"
                                            height="72" style="border-top-style: none; border-right-style: none; border-left-style: none;
                                            border-bottom-style: none" bgcolor="#dddddd" width="100%">
                                            <tr bgcolor="#ffffff">
                                                <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                                                    border-right-style: none; border-left-style: none; border-bottom-style: none">
                                                    <strong>
                                                        <img src="Images/dot_title.gif" width="17" height="17">
                                                        <asp:Label ID="Label7" runat="server" Text="Audit Trail Potensial Screening Customer Detail"></asp:Label>
                                                        <hr />
                                                    </strong>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<ajax:AjaxPanel ID="AjxMessage" runat="server">
                                                        <asp:Label ID="LblMessage" background="images/validationbground.gif" runat="server"
                                                            CssClass="validationok" Width="100%"></asp:Label>
                                                    </ajax:AjaxPanel>
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:Panel ID="Panel1" runat="server" Width="100%">
                                            <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
                                                border="2">
                                                <tr id="searchbox">
                                                    <td colspan="2" valign="top" width="50%" bgcolor="#ffffff" style="height: 200px">
                                                        <table cellspacing="4" cellpadding="0" width="50%" border="0">
                                                            <tr bgcolor="#ffffff">
                                                                <td colspan="3" bgcolor="#ffffff" style="font-size: 18px; border-top-style: none;
                                                                    border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                                                                </td>
                                                            </tr>
                                                            <tr bgcolor="#ffffff">
                                                                <td style="width: 29%; height: 18px">
                                                                    <asp:Label ID="Label2" runat="server" Text="User ID"></asp:Label></td>
                                                                <td style="width: 1px">
                                                                    :</td>
                                                                <td style="width: 90%">
                                                                    <asp:Label ID="LblUserID" runat="server"></asp:Label></td>
                                                            </tr>
                                                            <tr bgcolor="#ffffff">
                                                                <td style="width: 29%; height: 18px">
                                                                    <asp:Label ID="Label3" runat="server" Text="User Name"></asp:Label></td>
                                                                <td style="width: 1px">
                                                                    :</td>
                                                                <td style="width: 90%">
                                                                    <asp:Label ID="LblUserName" runat="server"></asp:Label></td>
                                                            </tr>
                                                            <tr bgcolor="#ffffff">
                                                                <td style="width: 29%; height: 18px">
                                                                    <asp:Label ID="LblMode" runat="server" Text="Screening Date"></asp:Label></td>
                                                                <td style="width: 1px">
                                                                    :</td>
                                                                <td style="width: 90%; height: 20px;">
                                                                    <asp:Label ID="LblScreeningDate" runat="server"></asp:Label></td>
                                                            </tr>
                                                            <tr bgcolor="#ffffff">
                                                                <td style="width: 29%; height: 18px">
                                                                    <asp:Label ID="Label11" runat="server" Text="Searching Data"></asp:Label></td>
                                                            </tr>
                                                            <tr bgcolor="#ffffff">
                                                                <td style="width: 29%; height: 18px">
                                                                    <asp:Label ID="Label4" runat="server" Text="Name"></asp:Label></td>
                                                                <td style="width: 1px">
                                                                    :</td>
                                                                <td style="width: 90%">
                                                                    <asp:Label ID="LblNama" runat="server"></asp:Label></td>
                                                            </tr>
                                                            <tr bgcolor="#ffffff">
                                                                <td style="width: 29%; height: 18px">
                                                                    <asp:Label ID="Label6" runat="server" Text="Date Of Birth"></asp:Label></td>
                                                                <td style="width: 1px">
                                                                    :</td>
                                                                <td style="width: 90%">
                                                                    <asp:Label ID="LblDateOfBirth" runat="server"></asp:Label></td>
                                                            </tr>
                                                            <tr bgcolor="#ffffff">
                                                                <td style="width: 29%; height: 15px">
                                                                    Nationality</td>
                                                                <td style="width: 1px; height: 15px;">
                                                                    :</td>
                                                                <td style="width: 90%; height: 15px;">
                                                                    <asp:Label ID="LblNationality" runat="server"></asp:Label></td>
                                                            </tr>
                                                        </table>
                                                        <ajax:AjaxPanel ID="AjaxPanel3" runat="server">
                                                            <asp:CustomValidator ID="CvalPageErr" runat="server" Display="None"></asp:CustomValidator></ajax:AjaxPanel>
                                                        <ajax:AjaxPanel ID="Ajaxpanel2" runat="server">
                                                            <asp:CustomValidator ID="CvalHandleErr" runat="server" ValidationGroup="handle" Display="None"></asp:CustomValidator></ajax:AjaxPanel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <table bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%" bgcolor="#dddddd"
                                            border="2">
                                            <tr>
                                                <td bgcolor="#ffffff">
                                                    &nbsp;<ajax:AjaxPanel ID="AjaxPanel14" runat="server"><asp:DataGrid ID="GridAuditTrailPotensialScreeningCustomer"
                                                        runat="server"  AllowSorting="True" AutoGenerateColumns="False"
                                                        BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px"
                                                        CellPadding="4" Font-Size="XX-Small" ForeColor="Black" GridLines="Vertical" Width="100%">
                                                        <FooterStyle BackColor="#CCCC99" />
                                                        <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                                                        <AlternatingItemStyle BackColor="White" />
                                                        <ItemStyle BackColor="#F7F7DE" />
                                                        <HeaderStyle BackColor="#6B696B" Font-Bold="True" Font-Size="XX-Small" ForeColor="White" />
                                                        <Columns>
                                                            <asp:TemplateColumn>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="CheckBoxExporttoExcel" runat="server" />
                                                                </ItemTemplate>
                                                                <HeaderStyle Width="10px" />
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="PK_AML_Screening_Customer_Result_Id"
                                                                Visible="False"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="AliasName_Lookup" HeaderText="Result Display Name" SortExpression="AliasName_Lookup desc">
                                                                <HeaderStyle Width="24%" />
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="PercentMatch" HeaderText="Percent Match" SortExpression="PercentMatch desc">
                                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                    Font-Underline="False" Width="40%" Wrap="False" />
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="dob_lookup" HeaderText="Date Of Birth" SortExpression="dob_lookup desc" DataFormatString="{0:dd-MM-yyyy}">
                                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                    Font-Underline="False" Wrap="False" />
                                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                    Font-Underline="False" Width="24%" Wrap="False" />
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="Nationality_lookup" HeaderText="Nationality" SortExpression="Nationality Desc">
                                                            </asp:BoundColumn>
                                                            
                                                            <asp:BoundColumn DataField="ListTypename" HeaderText="List Type" SortExpression="ListTypename Desc">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="categoryname" HeaderText="List Category" SortExpression="categoryname Desc">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="CustomRemark1" HeaderText="Custom Remarks 1" SortExpression="CustomRemark1 Desc">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="CustomRemark2" HeaderText="Custom Remarks 2" SortExpression="CustomRemark2 Desc">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="CustomRemark3" HeaderText="Custom Remarks 3" SortExpression="CustomRemark3 Desc">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="CustomRemark4" HeaderText="Custom Remarks 4" SortExpression="CustomRemark4 Desc">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="CustomRemark5" HeaderText="Custom Remarks 5" SortExpression="CustomRemark5 Desc">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="DateOfData" HeaderText="Date Of Data" SortExpression="DateOfData Desc" DataFormatString="{0:dd-MM-yyyy HH:mm}">
                                                            </asp:BoundColumn>
                                                        </Columns>
                                                    </asp:DataGrid>
                                                        <asp:Label ID="LabelNoRecordFound" runat="server" CssClass="text" Text="No record match with your criteria"
                                                            Visible="False"></asp:Label></ajax:AjaxPanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="background-color: #ffffff">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td nowrap>
                                                                <ajax:AjaxPanel ID="AjaxPanel6" runat="server">
                                                                    &nbsp;<asp:CheckBox ID="CheckBoxSelectAll" runat="server" Text="Select All" AutoPostBack="True" />
                                                                    &nbsp;
                                                                </ajax:AjaxPanel>
                                                            </td>
                                                            <td width="99%">
                                                                &nbsp;&nbsp;<asp:LinkButton ID="LinkButtonExportExcel" ajaxcall="none" OnClientClick="aspnetForm.encoding = 'multipart/form-data';"
                                                                    runat="server">Export 
		        to Excel</asp:LinkButton>&nbsp;
                                                                <asp:LinkButton ID="lnkExportAllData" runat="server" ajaxcall="none" OnClientClick="aspnetForm.encoding = 'multipart/form-data';"
                                                                    Text="Export All to Excel"></asp:LinkButton></td>
                                                            <td align="right" nowrap>
                                                                &nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#ffffff" style="height: 257px">
                                                    <table id="Table3" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                        bgcolor="#ffffff" border="2">
                                                        <tr class="regtext" align="center" bgcolor="#dddddd">
                                                            <td valign="top" align="left" width="50%" bgcolor="#ffffff">
                                                                Page&nbsp;<ajax:AjaxPanel ID="AjaxPanel7" runat="server"><asp:Label ID="PageCurrentPage"
                                                                    runat="server" CssClass="regtext">0</asp:Label>&nbsp;of&nbsp;
                                                                    <asp:Label ID="PageTotalPages" runat="server" CssClass="regtext">0</asp:Label></ajax:AjaxPanel></td>
                                                            <td valign="top" align="right" width="50%" bgcolor="#ffffff">
                                                                Total Records&nbsp;
                                                                <ajax:AjaxPanel ID="AjaxPanel8" runat="server">
                                                                    <asp:Label ID="PageTotalRows" runat="server">0</asp:Label></ajax:AjaxPanel></td>
                                                        </tr>
                                                    </table>
                                                    <table id="Table4" bordercolor="#ffffff" cellspacing="1" cellpadding="2" width="100%"
                                                        bgcolor="#ffffff" border="2">
                                                        <tr bgcolor="#ffffff">
                                                            <td class="regtext" valign="middle" align="left" colspan="11" style="height: 7px">
                                                                <hr color="#f40101" noshade size="1">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="regtext" valign="middle" align="left" width="63" bgcolor="#ffffff" style="height: 14px">
                                                                Go to page</td>
                                                            <td class="regtext" valign="middle" align="left" width="5" bgcolor="#ffffff" style="height: 14px">
                                                                <font face="Verdana, Arial, Helvetica, sans-serif" size="1">
                                                                    <ajax:AjaxPanel ID="AjaxPanel9" runat="server">
                                                                        <asp:TextBox ID="TextGoToPage" runat="server" Width="38px" CssClass="searcheditbox"></asp:TextBox></ajax:AjaxPanel>
                                                                </font>
                                                            </td>
                                                            <td class="regtext" valign="middle" align="left" bgcolor="#ffffff" style="height: 14px">
                                                                <ajax:AjaxPanel ID="AjaxPanel10" runat="server">
                                                                    <asp:ImageButton ID="ImageButtonGo" runat="server" SkinID="GoButton" ImageUrl="~/Images/button/go.gif">
                                                                    </asp:ImageButton></ajax:AjaxPanel>
                                                            </td>
                                                            <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff" style="height: 14px">
                                                                <img height="5" src="images/first.gif" width="6">
                                                            </td>
                                                            <td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff" style="height: 14px">
                                                                <ajax:AjaxPanel ID="AjaxPanel11" runat="server">
                                                                    <asp:LinkButton ID="LinkButtonFirst" runat="server" CssClass="regtext" CommandName="First"
                                                                        OnCommand="PageNavigate">First</asp:LinkButton></ajax:AjaxPanel>
                                                            </td>
                                                            <td class="regtext" valign="middle" align="right" width="6" bgcolor="#ffffff" style="height: 14px">
                                                                <img height="5" src="images/prev.gif" width="6"></td>
                                                            <td class="regtext" valign="middle" align="right" width="14" bgcolor="#ffffff" style="height: 14px">
                                                                <ajax:AjaxPanel ID="AjaxPanel12" runat="server">
                                                                    <asp:LinkButton ID="LinkButtonPrevious" runat="server" CssClass="regtext" CommandName="Prev"
                                                                        OnCommand="PageNavigate">Previous</asp:LinkButton></ajax:AjaxPanel>
                                                            </td>
                                                            <td class="regtext" valign="middle" align="right" width="60" bgcolor="#ffffff" style="height: 14px">
                                                                <ajax:AjaxPanel ID="AjaxPanel13" runat="server">
                                                                    <a class="pageNav" href="#">
                                                                        <asp:LinkButton ID="LinkButtonNext" runat="server" CssClass="regtext" CommandName="Next"
                                                                            OnCommand="PageNavigate">Next</asp:LinkButton></a></ajax:AjaxPanel></td>
                                                            <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff" style="height: 14px">
                                                                <img height="5" src="images/next.gif" width="6"></td>
                                                            <td class="regtext" valign="middle" align="left" width="25" bgcolor="#ffffff" style="height: 14px">
                                                                <ajax:AjaxPanel ID="AjaxPanel16" runat="server">
                                                                    <asp:LinkButton ID="LinkButtonLast" runat="server" CssClass="regtext" CommandName="Last"
                                                                        OnCommand="PageNavigate">Last</asp:LinkButton></ajax:AjaxPanel>
                                                            </td>
                                                            <td class="regtext" valign="middle" align="left" width="6" bgcolor="#ffffff" style="height: 14px">
                                                                <img height="5" src="images/last.gif" width="6"></td>
                                                        </tr>
                                                    </table>
                                                    <br />
                                                    <br />
                                                    <br />
                                                    <br />
                                                   
                                                    <asp:ImageButton ID="ImageCancel" runat="server" CausesValidation="False" SkinID="BackButton" /></td>
                                                    
                                            </tr>
                                        </table>
                                    </ajax:AjaxPanel>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
</asp:Content>
