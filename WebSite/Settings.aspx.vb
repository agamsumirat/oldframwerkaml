
Partial Class Settings
    Inherits Parent

    ''' <summary>
    ''' get user id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetPkUserId() As Int64
        Get
            Return Sahassa.AML.Commonly.SessionPkUserId
        End Get
    End Property

    ''' <summary>
    ''' get id for focues
    ''' </summary>
    ''' <value></value>
    ''' <returns>element control id</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property GetFocusID() As String
        Get
            Return Me.TextUserName.ClientID
        End Get
    End Property

    ''' <summary>
    ''' cancel handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "default.aspx"

        Me.Response.Redirect("default.aspx", False)
    End Sub

    Private Sub InsertAuditTrailUserSettings(ByVal mode As String, ByVal NewRecord As String, ByVal OldRecord As String)
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(3)
            Using AcessAuditTrail As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                AcessAuditTrail.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User Settings", "Record Per Page", mode, OldRecord, NewRecord, "")
                AcessAuditTrail.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User Settings", "Created Date", mode, CDate(Session("CreatedDate")).ToString("dd MMMM yyyy hh:mm:ss"), CDate(Session("CreatedDate")).ToString("dd MMMM yyyy hh:mm:ss"), "")
                AcessAuditTrail.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "User Settings", "Updated Date", mode, CDate(Session("UpdatedDate")).ToString("dd MMMM yyyy hh:mm:ss"), Now.ToString("dd MMMM yyyy hh:mm:ss"), "")
            End Using
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' update handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Page.Validate()

        If Page.IsValid Then
            Try
                If Me.DropDownListPagingLimit.SelectedValue = -1 And Me.TextBoxPagingLimit.Text.Trim.Length = 0 Then
                    Throw New Exception("Display page per record is required.")
                Else

                    If Me.DropDownListPagingLimit.SelectedValue = -1 Then
                        'Sahassa.AML.Commonly.SessionPagingLimit = Me.TextBoxPagingLimit.Text
                        Using AccessInsertSettings As New AMLDAL.UserSettingsTableAdapters.QueriesSettings
                            If Session("IsExist") = "Yes" Then
                                AccessInsertSettings.UpdateUserSettings(Sahassa.AML.Commonly.SessionUserId, CInt(Me.TextBoxPagingLimit.Text), Now)
                                Me.InsertAuditTrailUserSettings("Edit", Me.TextBoxPagingLimit.Text, Session("Record_Old"))
                                Me.LblSuccess.Visible = True
                                Me.LblSuccess.Text = "Success to Update User Settings."
                            Else
                                AccessInsertSettings.InsertUserSettings(Sahassa.AML.Commonly.SessionUserId, CInt(Me.TextBoxPagingLimit.Text))
                                Me.InsertAuditTrailUserSettings("Add", Me.TextBoxPagingLimit.Text, "")
                                Me.LblSuccess.Visible = True
                                Me.LblSuccess.Text = "Success to Insert User Settings."
                                Session("IsExist") = "Yes"
                            End If
                            Sahassa.AML.Commonly.SessionPagingLimit = CInt(Me.TextBoxPagingLimit.Text)
                        End Using
                    Else
                        Using AccessInsertSettings As New AMLDAL.UserSettingsTableAdapters.QueriesSettings
                            If Session("IsExist") = "Yes" Then
                                AccessInsertSettings.UpdateUserSettings(Sahassa.AML.Commonly.SessionUserId, CInt(Me.DropDownListPagingLimit.SelectedItem.Text), Now)
                                Me.InsertAuditTrailUserSettings("Edit", Me.DropDownListPagingLimit.SelectedItem.Text, Session("Record_Old"))
                                Me.LblSuccess.Visible = True
                                Me.LblSuccess.Text = "Success to update User Settings."
                            Else
                                AccessInsertSettings.InsertUserSettings(Sahassa.AML.Commonly.SessionUserId, CInt(Me.DropDownListPagingLimit.SelectedItem.Text))
                                Me.InsertAuditTrailUserSettings("Add", Me.DropDownListPagingLimit.SelectedItem.Text, "")
                                Me.LblSuccess.Visible = True
                                Me.LblSuccess.Text = "Success to Insert User Settings."
                                Session("IsExist") = "Yes"
                            End If
                            Sahassa.AML.Commonly.SessionPagingLimit = CInt(Me.DropDownListPagingLimit.SelectedItem.Text)
                        End Using
                        'Sahassa.AML.Commonly.SessionPagingLimit = Me.DropDownListPagingLimit.SelectedValue
                    End If

                    If (ViewState("UserName_Old") = Me.TextUserName.Text) AndAlso (ViewState("UserEmailAddress_Old") = Me.TextboxEmailAddr.Text) AndAlso (ViewState("UserMobilePhone_Old") = Me.TextboxMobilePhone.Text) Then
                        'do nothing krn tdk ada perubahan pd User Information
                    Else
                        Using AccessUser As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
                            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                                AccessAudit.Insert(Now, Me.LabelUserId.Text, Me.LabelUserId.Text, "User", "UserName", "Edit", ViewState("UserName_Old"), Me.TextUserName.Text, "Accepted")
                                AccessAudit.Insert(Now, Me.LabelUserId.Text, Me.LabelUserId.Text, "User", "UserEmailAddress", "Edit", ViewState("UserEmailAddress_Old"), Me.TextboxEmailAddr.Text, "Accepted")
                                AccessAudit.Insert(Now, Me.LabelUserId.Text, Me.LabelUserId.Text, "User", "UserMobilePhone", "Edit", ViewState("UserMobilePhone_Old"), Me.TextboxMobilePhone.Text, "Accepted")
                            End Using
                            AccessUser.UpdateUserInformationSettings(Me.GetPkUserId, Me.TextUserName.Text, Me.TextboxEmailAddr.Text, Me.TextboxMobilePhone.Text)
                        End Using
                    End If
                End If
            Catch ex As Exception
                Me.cvalPageError.IsValid = False
                Me.cvalPageError.ErrorMessage = ex.Message
                LogError(ex)
            End Try
        End If
    End Sub

    ''' <summary>
    ''' filledit data
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub FillEditData()
        Using AccessUser As New AMLDAL.AMLDataSetTableAdapters.UserTableAdapter
            Using TableUser As AMLDAL.AMLDataSet.UserDataTable = AccessUser.GetDataByUserID(Me.GetPkUserId)
                If TableUser.Rows.Count > 0 Then
                    Dim TableRowUser As AMLDAL.AMLDataSet.UserRow = TableUser.Rows(0)
                    ViewState("UserID_Old") = TableRowUser.UserID
                    Me.LabelUserId.Text = ViewState("UserID_Old")

                    ViewState("UserName_Old") = TableRowUser.UserName
                    Me.TextUserName.Text = ViewState("UserName_Old")

                    ViewState("UserEmailAddress_Old") = TableRowUser.UserEmailAddress
                    Me.TextboxEmailAddr.Text = ViewState("UserEmailAddress_Old")

                    ViewState("UserMobilePhone_Old") = TableRowUser.UserMobilePhone
                    Me.TextboxMobilePhone.Text = ViewState("UserMobilePhone_Old")
                End If
            End Using
        End Using
    End Sub

    Public Sub LoadDataEditSettings(ByVal Record As Integer)
        Try
            Dim booldrop As Boolean = True
            For Each item As ListItem In Me.DropDownListPagingLimit.Items
                If item.Value = -1 Then
                    Exit For
                Else
                    If item.Text = Record Then
                        Me.pilih1.Visible = True
                        Me.DropDownListPagingLimit.SelectedValue = item.Value
                        Me.pilih2.Visible = False
                        booldrop = False
                        Exit For
                    End If
                End If
                
            Next
            If booldrop Then
                Me.pilih2.Visible = True
                Me.pilih1.Visible = True
                Me.DropDownListPagingLimit.SelectedValue = -1
                Me.TextBoxPagingLimit.Text = Record
            End If
        Catch
            Throw
        End Try
    End Sub

   
    ''' <summary>
    ''' page load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.FillEditData()

                Using AccessUserSettings As New AMLDAL.UserSettingsTableAdapters.SelectUserSettingsFilterTableAdapter
                    Using dtUser As Data.DataTable = AccessUserSettings.GetData(Sahassa.AML.Commonly.SessionUserId)
                        If dtUser.Rows.Count > 0 Then
                            ' edit
                            Dim RowUser As AMLDAL.UserSettings.SelectUserSettingsFilterRow = dtUser.Rows(0)
                            Dim Record As Integer = CInt(RowUser.RecordPerPage)
                            Session("CreatedDate") = RowUser.CreatedDate
                            Session("UpdatedDate") = RowUser.UpdatedDate
                            Session("Record_Old") = RowUser.RecordPerPage
                            Me.LoadDataEditSettings(Record)
                            Session("IsExist") = "Yes"
                        Else
                            ' add
                            Me.pilih1.Visible = True
                            Me.pilih2.Visible = False
                            Session("IsExist") = "No"
                        End If
                    End Using
                End Using

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub DropDownListPagingLimit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownListPagingLimit.SelectedIndexChanged
        If Me.DropDownListPagingLimit.SelectedValue = -1 Then
            Me.pilih2.Visible = True
            Me.TextBoxPagingLimit.Text = ""
        Else
            Me.pilih1.Visible = True
            Me.pilih2.Visible = False
            'Sahassa.AML.Commonly.SessionPagingLimit = Me.DropDownListPagingLimit.SelectedValue
        End If
    End Sub

    Protected Sub LinkButtonChangePassword_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonChangePassword.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "ChangePassword.aspx"

        Me.Response.Redirect("ChangePassword.aspx", False)
    End Sub
End Class