<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="PrintAccount.aspx.vb" Inherits="PrintAccount"  %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
    <rsweb:ReportViewer ID="reportViewer_content" runat="server" width="100%" height="800px" Font-Names="Verdana" Font-Size="8pt">
    <LocalReport ReportPath="ReportAccount.rdlc" EnableExternalImages="True">
        <DataSources>
            <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="AccountInformation_SelectAllAccount_WebTempTableByAccountNo" />
            <rsweb:ReportDataSource DataSourceId="ObjectDataSource2" Name="CustomerInformation_SelectCDD_CIF_MasterFilter" />
            <rsweb:ReportDataSource DataSourceId="ObjectDataSource3" Name="AccountInformation_AccountInformationGetStatusDate" />
            <rsweb:ReportDataSource DataSourceId="ObjectDataSource4" Name="AccountInformation_SelectAllAccount_AllInfoByAccountNo" />
            <rsweb:ReportDataSource DataSourceId="ObjectDataSource5" Name="AccountInformation_SelectAccountFinancialInfoByAccountNo" />
            <rsweb:ReportDataSource DataSourceId="ObjectDataSource6" Name="AccountInformation_SelectDetailKYCInformation" />
            
        </DataSources>

        </LocalReport>
    </rsweb:ReportViewer>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetAllAccount_WebTempTableByAccountNo" TypeName="AMLDAL.AccountInformationTableAdapters.SelectAllAccount_WebTempTableByAccountNoTableAdapter">
        <SelectParameters>
            <asp:QueryStringParameter Name="AccountNo" QueryStringField="AccountNo" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <br />
    <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="AMLDAL.CustomerInformationTableAdapters.SelectCDD_CIF_MasterFilterTableAdapter">
        <SelectParameters>
            <asp:QueryStringParameter Name="CIFNo" QueryStringField="CIFNo" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource3" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetData" TypeName="AMLDAL.AccountInformationTableAdapters.AccountInformationGetStatusDateTableAdapter">
        <SelectParameters>
            <asp:QueryStringParameter Name="AccountNo" QueryStringField="AccountNo" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataSource4" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetSelectAllAccount_AllInfoByAccountNo" TypeName="AMLDAL.AccountInformationTableAdapters.SelectAllAccount_AllInfoByAccountNoTableAdapter">
        <SelectParameters>
            <asp:QueryStringParameter Name="AccountNo" QueryStringField="AccountNo" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource><asp:ObjectDataSource ID="ObjectDataSource5" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetAccountFinancialInfoByAccountNo" TypeName="AMLDAL.AccountInformationTableAdapters.SelectAccountFinancialInfoByAccountNoTableAdapter">
        <SelectParameters>
            <asp:QueryStringParameter Name="AccountNo" QueryStringField="AccountNo" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource><asp:ObjectDataSource ID="ObjectDataSource6" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetData" TypeName="AMLDAL.AccountInformationTableAdapters.SelectDetailKYCInformationTableAdapter">
        <SelectParameters>
            <asp:QueryStringParameter Name="ACCTNO" QueryStringField="AccountNo" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <br />
    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/button/back.gif" />
    
</asp:Content>

