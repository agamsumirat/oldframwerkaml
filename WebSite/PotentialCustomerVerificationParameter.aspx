<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="PotentialCustomerVerificationParameter.aspx.vb" Inherits="PotentialCustomerVerificationParameter" title="Potential Customer Verification Parameter" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
    <table borderColor="#ffffff" cellSpacing="1" cellPadding="2" bgColor="#dddddd"
	    border="2" style="width: 99%">
        <tr>
            <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong> Customer Screening Parameter Settings
                    <hr />
                </strong>
                <asp:Label ID="LblSucces" runat="server" CssClass="validationok"  Visible="False" Width="90%"></asp:Label></td>
        </tr>
	    <tr class="formText">
		    <td width="5" bgColor="#ffffff" height="24"><asp:requiredfieldvalidator id="RequiredFieldValidator2" runat="server" ErrorMessage="Minimum Result Percentage cannot be blank"
				    Display="Dynamic" ControlToValidate="TextMinimumResultPercentage">*</asp:requiredfieldvalidator><br />
                <asp:RangeValidator ID="RangeValidatorMinimumResultPercentage" runat="server" ControlToValidate="TextMinimumResultPercentage"
                    Display="Dynamic" ErrorMessage="Minimum Result Percentage must be numeric value between 0 and 100"
                    MaximumValue="100" MinimumValue="0" Type="Integer">*</asp:RangeValidator>
            </td>
		    <td width="20%" bgColor="#ffffff">
                Minimum Result Percentage</td>
		    <td width="5" bgColor="#ffffff">:</td>
		    <td bgColor="#ffffff" style="width: 80%"><asp:textbox id="TextMinimumResultPercentage" runat="server" CssClass="textBox" MaxLength="20" Width="50px"></asp:textbox>&nbsp;%</td>
	    </tr>
	    <tr class="formText" bgColor="#dddddd" height="30">
		    <td width="15"><IMG height="15" src="images/arrow.gif" width="15"></td>
		    <td colSpan="3">
			    <table cellSpacing="0" cellPadding="3" border="0">
				    <tr>
					    <td><asp:imagebutton id="ImageSave" runat="server" CausesValidation="True" SkinID="SaveButton"></asp:imagebutton></td>
					    <td><asp:imagebutton id="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton"></asp:imagebutton></td>
				    </tr>
			    </table>
                <asp:CustomValidator ID="cvalPageErr" runat="server" Display="None"></asp:CustomValidator></td>
	    </tr>
    </table>
	<script language="javascript">
	    document.getElementById('<%=GetFocusID %>').focus();
	</script>
</asp:Content>