Imports sahassa.AML.Commonly
Imports SahassaNettier.Entities
Partial Class SuspiciusPersonEdit
    Inherits Parent



    Public ReadOnly Property PKSuspiciusPersonID() As Integer
        Get
            Dim temp As String = Request.Params("PKSuspiciusPersonID")
            Dim intresult As Integer
            If Integer.TryParse(temp, intresult) Then
                Return intresult
            Else
                Throw New Exception("PKSuspiciusPersonId must numeric")
            End If
        End Get
    End Property

    Public ReadOnly Property ObjSuspiciusPersonEdit() As SuspiciusPerson
        Get
            If Session("SuspiciusPersonEdit.ObjSuspiciusPersonEdit") Is Nothing Then
                Session("SuspiciusPersonEdit.ObjSuspiciusPersonEdit") = AMLBLL.SuspiciusPersonBLL.GetSuspiciusPersonByPk(Me.PKSuspiciusPersonID)
            End If
            Return Session("SuspiciusPersonEdit.ObjSuspiciusPersonEdit")
        End Get
    End Property
    Public ReadOnly Property GetFocusID() As String
        Get
            Return Me.TxtName.ClientID
        End Get
    End Property

    Sub ClearSession()
        Session("SuspiciusPersonEdit.ObjSuspiciusPersonEdit") = Nothing
    End Sub
    Sub LoadDataSuspiciusPerson()
        If Me.ObjSuspiciusPersonEdit Is Nothing Then
            Throw New Exception("Data Custom Alert not Exist or Already Deleted.")
        Else
            With ObjSuspiciusPersonEdit
                txtCIFNo.Text = .CIFNo
                TxtName.Text = .Nama
                TxtTglLahir.Text = .TanggalLahir.GetValueOrDefault(GetDefaultDate).ToString("dd-MMM-yyyy")
                TxtTempatLahir.Text = .TempatLahir
                TxtNoIdentitas.Text = .NoIdentitas
                TxtAlamat.Text = .Alamat
                TxtKecamatanKelurahan.Text = .KecamatanKeluarahan
                TxtNoTelp.Text = .NoTelp
                TxtPekerjaan.Text = .Pekerjaan
                TxtAlamatTempatKerja.Text = .AlamatTempatKerja
                TxtNPWP.Text = .NPWP
                TxtDescription.Text = .Description
                lblCreatedBy.Text = .UseridCreator

            End With


        End If
    End Sub

    Sub SelectCIF(ByVal PK_TempSuspiciusPerson_ID As Integer)
        Using objSuspiciusCIF As TempCIFSuspicius = AMLBLL.SuspiciusPersonBLL.GetTempCIF(PK_TempSuspiciusPerson_ID)
            If Not objSuspiciusCIF Is Nothing Then
                TxtName.Text = objSuspiciusCIF.CustomerName
                TxtTglLahir.Text = objSuspiciusCIF.Tanggallahir.GetValueOrDefault(GetDefaultDate).ToString("dd-MMM-yyyy")
                TxtNoIdentitas.Text = objSuspiciusCIF.NoIdentitas
                TxtAlamat.Text = objSuspiciusCIF.Alamat
                TxtNoTelp.Text = objSuspiciusCIF.NoTelp
                TxtNPWP.Text = objSuspiciusCIF.NPWP
            End If
        End Using
    End Sub

    Sub Selectwic(ByVal pkwicid As Integer)
        Using objSuspiciusCIF As tempWIC = AMLBLL.SuspiciusPersonBLL.GetTempwic(pkwicid)
            If Not objSuspiciusCIF Is Nothing Then
                TxtName.Text = objSuspiciusCIF.CustomerName
                TxtTglLahir.Text = objSuspiciusCIF.TglLahir.GetValueOrDefault(GetDefaultDate).ToString("dd-MMM-yyyy")
                TxtNoIdentitas.Text = objSuspiciusCIF.NoIdentitas
                TxtAlamat.Text = objSuspiciusCIF.Alamat
                TxtNPWP.Text = objSuspiciusCIF.NPWP
            End If
        End Using
    End Sub

    Protected Sub Browse_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Browse.Click

        Try


            If CboSourceData.SelectedIndex = 1 Then
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "idpopup2", "popUpDrag('" & CType(sender, ImageButton).ClientID & "','divBrowseUser');", True)

                PopUpCIF1.initData()
            ElseIf CboSourceData.SelectedIndex = 2 Then
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "idpopup2", "popUpDrag('" & CType(sender, ImageButton).ClientID & "','divBrowseUserWic');", True)

                PopUpWIC1.initData()
            End If

        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try


    End Sub

    Protected Sub CboSourceData_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CboSourceData.SelectedIndexChanged

        Try

            If CboSourceData.SelectedIndex = 0 Then
                Browse.Visible = False
            ElseIf CboSourceData.SelectedIndex = 1 Then
                'cif
                Browse.Visible = True
            ElseIf CboSourceData.SelectedIndex = 2 Then
                'wic
                Browse.Visible = True
            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

        Try


            If Not Page.ClientScript.IsClientScriptIncludeRegistered("idpopup1") Then
                Page.ClientScript.RegisterClientScriptInclude(Me.GetType(), "idpopup1", ResolveClientUrl("script/popupdrag.js"))
            End If



        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub
    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            AddHandler PopUpCIF1.SelectCIF, AddressOf SelectCIF
            AddHandler PopUpWIC1.Selectwic, AddressOf Selectwic

            If Not Me.IsPostBack Then
                ClearSession()
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                End Using
                Me.PopUpTglLahir.Attributes.Add("onclick", "javascript:popUpCalendar(this, document.getElementById('" & Me.TxtTglLahir.ClientID & "'), 'dd-mmm-yyyy')")
                LoadDataSuspiciusPerson()
            End If
            

        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click

        Try

            Response.Redirect("SuspiciusPersonView.aspx", False)
        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub

    Function IsDataValidEdit() As Boolean

        Dim strErrorMessage As New StringBuilder

        If TxtName.Text.Trim.Length = 0 Then
            strErrorMessage.Append("Nama is required </br>")
        End If

        If Not Sahassa.AML.Commonly.IsDateValid("dd-MMM-yyyy", TxtTglLahir.Text.Trim) Then
            strErrorMessage.Append("Birth Date must dd-MMM-yyyy")
        End If
        If TxtNoIdentitas.Text.Trim.Length = 0 Then
            strErrorMessage.Append("Identity No is required </br>")
        End If

        If TxtAlamat.Text.Trim.Length = 0 Then
            strErrorMessage.Append("Address is required </br>")
        End If


        If TxtDescription.Text.Trim.Length = 0 Then
            strErrorMessage.Append("Alert Reason is required </br>")
        End If




        If strErrorMessage.ToString.Trim.Length > 0 Then
            Throw New Exception(strErrorMessage.ToString)
        Else
            Return True
        End If
    End Function

    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click

        Try
            If IsDataValidEdit Then
                If SessionPkUserId = 1 Then
                    'save diret
                    With ObjSuspiciusPersonEdit

                        .Nama = TxtName.Text.Trim
                        .CIFNo = txtCIFNo.Text
                        .TanggalLahir = ConvertToDate("dd-MMM-yyyy", TxtTglLahir.Text.Trim)
                        .TempatLahir = TxtTempatLahir.Text.Trim
                        .NoIdentitas = TxtNoIdentitas.Text.Trim
                        .Alamat = TxtAlamat.Text.Trim
                        .KecamatanKeluarahan = TxtKecamatanKelurahan.Text.Trim
                        .NoTelp = TxtNoTelp.Text.Trim
                        .Pekerjaan = TxtPekerjaan.Text.Trim
                        .AlamatTempatKerja = TxtAlamatTempatKerja.Text.Trim
                        .NPWP = TxtNPWP.Text.Trim
                        .Description = TxtDescription.Text.Trim
                        .CreatedDate = Now
                        .LastUpdateDate = Now
                        .UseridCreator = Sahassa.AML.Commonly.SessionUserId
                    End With
                    AMLBLL.SuspiciusPersonBLL.SaveEditDirect(ObjSuspiciusPersonEdit)
                    Response.Redirect("SuspiciusPersonView.aspx", False)
                Else
                    'save approval
                    Using objNewSuspiciusApprovaldetail As New Suspicius_ApprovalDetail
                        With objNewSuspiciusApprovaldetail

                            .FK_Suspicius_Approval = Nothing
                            .ModeID = 1
                            .PK_SuspiciusPerson_ID = Me.PKSuspiciusPersonID
                            .CIFNo = txtCIFNo.Text
                            .Nama = TxtName.Text.Trim
                            .TanggalLahir = ConvertToDate("dd-MMM-yyyy", TxtTglLahir.Text.Trim)
                            .TempatLahir = TxtTempatLahir.Text.Trim
                            .NoIdentitas = TxtNoIdentitas.Text.Trim
                            .Alamat = TxtAlamat.Text.Trim
                            .KecamatanKeluarahan = TxtKecamatanKelurahan.Text.Trim
                            .NoTelp = TxtNoTelp.Text.Trim
                            .Pekerjaan = TxtPekerjaan.Text.Trim
                            .AlamatTempatKerja = TxtAlamatTempatKerja.Text.Trim
                            .NPWP = TxtNPWP.Text.Trim
                            .Description = TxtDescription.Text.Trim
                            .CreatedDate = Now
                            .LastUpdateDate = Now
                            .UseridCreator = Sahassa.AML.Commonly.SessionUserId

                            .PK_SuspiciusPerson_ID_old = Me.PKSuspiciusPersonID
                            .CIFNo_Old = ObjSuspiciusPersonEdit.CIFNo
                            .Nama_old = ObjSuspiciusPersonEdit.Nama
                            .TanggalLahir_old = ObjSuspiciusPersonEdit.TanggalLahir
                            .TempatLahir_old = ObjSuspiciusPersonEdit.TempatLahir
                            .NoIdentitas_old = ObjSuspiciusPersonEdit.NoIdentitas
                            .Alamat_old = ObjSuspiciusPersonEdit.Alamat
                            .KecamatanKeluarahan_old = ObjSuspiciusPersonEdit.KecamatanKeluarahan
                            .NoTelp_old = ObjSuspiciusPersonEdit.NoTelp
                            .Pekerjaan_old = ObjSuspiciusPersonEdit.Pekerjaan
                            .AlamatTempatKerja_old = ObjSuspiciusPersonEdit.AlamatTempatKerja
                            .NPWP_old = ObjSuspiciusPersonEdit.NPWP
                            .Description_old = ObjSuspiciusPersonEdit.Description
                            .CreatedDate_old = ObjSuspiciusPersonEdit.CreatedDate
                            .LastUpdateDate_old = ObjSuspiciusPersonEdit.LastUpdateDate
                            .UseridCreator_old = ObjSuspiciusPersonEdit.UseridCreator




                        End With
                        AMLBLL.SuspiciusPersonBLL.SaveEditApproval(objNewSuspiciusApprovaldetail, ObjSuspiciusPersonEdit)
                        Dim MessagePendingID As Integer = 89802 'MessagePendingID 8201 = Group Add 

                        Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & TxtName.Text.Trim

                        Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & TxtName.Text.Trim, False)

                    End Using
                End If
            End If

        Catch ex As Exception
            LogError(ex)
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
        End Try

    End Sub
End Class
