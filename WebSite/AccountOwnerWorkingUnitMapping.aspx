<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="AccountOwnerWorkingUnitMapping.aspx.vb" Inherits="AccountOwnerWorkingUnitMapping" title="Account Owner Working Unit Mapping" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
    <table id="title"  border="2" bordercolor="#ffffff" cellpadding="2"
        cellspacing="1" height="72" style="border-top-style: none; border-right-style: none;
        border-left-style: none; border-bottom-style: none" width="100%">
        <tr>
            <td bgcolor="#ffffff" colspan="4" style="font-size: 18px; border-top-style: none;
                border-right-style: none; border-left-style: none; height: 24px; border-bottom-style: none">
                <strong>Account Owner Working Unit Mapping&nbsp;
                    <hr />
                </strong>
                <asp:Label ID="LblSuccess" runat="server" CssClass="validationok" Visible="False"
                    Width="100%"></asp:Label>
            </td>
        </tr>      
    </table>	
    <table borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%" bgColor="#dddddd"
	    border="2">      
	    <tr class="formText">
		    <td width="5" bgColor="#ffffff" height="24">
                <br />
            </td>
		    <td bgColor="#ffffff" colspan="2">
                Account Owner</td>
            <td bgcolor="#ffffff" colspan="1">
                :</td>
		    <td width="80%" bgColor="#ffffff">
                <asp:Label ID="LabelAccountOwnerName" runat="server"></asp:Label>&nbsp;</td>
	    </tr>
	    <tr class="formText">
		    <td bgcolor="#ffffff" height="24">
                <br />
            </td>
		    <td bgcolor="#ffffff" colspan="2">
                Working Unit</td>
            <td bgcolor="#ffffff" colspan="1">
                :</td>
		    <td bgcolor="#ffffff">
                <asp:DropDownList ID="DropDownListWorkingUnit" runat="server" Width="215px">
                </asp:DropDownList>&nbsp;</td>
	    </tr>
	    <tr class="formText" bgColor="#dddddd" height="30">
		    <td width="15"><IMG height="15" src="images/arrow.gif" width="15"></td>
		    <td colSpan="4">
			    <table cellSpacing="0" cellPadding="3" border="0">
				    <tr>
					    <td><asp:imagebutton id="ImageSave" runat="server" CausesValidation="True" SkinID="SaveButton"></asp:imagebutton></td>
					    <td><asp:imagebutton id="ImageCancel" runat="server" CausesValidation="False" SkinID="CancelButton"></asp:imagebutton></td>
				    </tr>
			    </table>
                <span style="color: #ff0000"></span>
                <asp:CustomValidator ID="cvalPageErr" runat="server" Display="None" Visible="False" Width="1px"></asp:CustomValidator></td>
	    </tr>
    </table>
	<script language="javascript">
	    document.getElementById('<%=GetFocusID %>').focus();
	</script>
</asp:Content>