Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper

Imports System.IO
Imports System

Partial Class FileSecurity
    Inherits Parent
    Private Shared ArrayListExcludeFile As New System.Collections.ArrayList

    ''' <summary>
    ''' Fill Group 
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub FillMsGroup()
        Try
            Using AccessGroup As New AMLDAL.AMLDataSetTableAdapters.GroupTableAdapter
                Using dtGroup As AMLDAL.AMLDataSet.GroupDataTable = AccessGroup.GetData
                    Me.Combofk_MsGroup_id.DataSource = dtGroup '.Select("GroupID <> 1") 'DataRepository.MsGroupProvider.GetPaged("pk_MsGroup_id <> 1", "pk_MsGroup_id", 0, Int16.MaxValue, 100)
                    Me.Combofk_MsGroup_id.DataTextField = "GroupName"
                    Me.Combofk_MsGroup_id.DataValueField = "GroupID"
                    Me.Combofk_MsGroup_id.DataBind()
                End Using
            End Using
        Catch
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' Fill Selected File
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub FillSelectedFile()
        Try
            Me.ListSelectedFile.Items.Clear()
            Dim flag As Boolean = 0
            Using AccessFileSecurity As New AMLDAL.AMLDataSetTableAdapters.FileSecurityTableAdapter
                For Each RowFileSecurity As AMLDAL.AMLDataSet.FileSecurityRow In AccessFileSecurity.GetData.Select("FileSecurityGroupId=" & Me.Combofk_MsGroup_id.SelectedValue)
                    Me.ListSelectedFile.Items.Add(New ListItem(Split(RowFileSecurity.FileSecurityFileName), RowFileSecurity.FileSecurityFileName))
                    flag = 1
                Next
                If flag = 0 Then
                    Me.ListSelectedFile.Items.Clear()
                End If
            End Using
        Catch
            Throw
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     Untuk menghilangkan File yang telah ada di Avalaible File dengan File yang 
    '''     ada di dalam SHS_Menu dengan GroupId = 0
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AddExcludeFile()
        Dim AMLMenu As Sahassa.AML.AMLMenu
        Dim DatasetExclude As Data.DataSet
        Dim i As Integer
        Try
            AMLMenu = New Sahassa.AML.AMLMenu
            DatasetExclude = AMLMenu.GetAllExcludeFile()
            If DatasetExclude.Tables.Count > 0 AndAlso DatasetExclude.Tables(0).Rows.Count > 0 Then
                For i = 0 To DatasetExclude.Tables(0).Rows.Count - 1
                    Dim StrName As String = DatasetExclude.Tables(0).Rows(i).Item("MenuHyperlink")
                    If StrName.EndsWith("/") Then
                        StrName = StrName.Substring(0, StrName.Length - 1)
                    End If
                    ArrayListExcludeFile.Add(StrName)
                Next
            End If
            ArrayListExcludeFile.Add("ForceChangePassword.aspx")
            ArrayListExcludeFile.Add("NoWorkingUnitAssignedRequest.aspx")
            ArrayListExcludeFile.Add("DisplayAllNews.aspx")
            ArrayListExcludeFile.Add("DisplayNews.aspx")
        Catch
            Throw
        Finally
            AMLMenu = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' FillAvailableFile()
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub FillAvailableFile()
        Dim StrItem, StrItemInside, StrTemp As String
        Dim ObjArraylist As New ArrayList
        Try
            Me.ListAvailableFile.Items.Clear()
            For Each StrItem In Directory.GetDirectories(Server.MapPath("~/"))
                Dim dirInfo As New DirectoryInfo(StrItem)
                If Not (dirInfo.Name().Substring(0, 1).ToLower = "_" Or dirInfo.Name().ToLower = "aspnet_client" Or dirInfo.Name().ToLower = "include" Or dirInfo.Name().ToLower = "bin" Or dirInfo.Name().ToLower = "theme" Or dirInfo.Name().ToLower = "script" Or dirInfo.Name().ToLower = "app_code" Or dirInfo.Name().ToLower = "app_data" Or dirInfo.Name().ToLower = "app_localresources" Or dirInfo.Name().ToLower = "app_themes" Or dirInfo.Name().ToLower = "images") Or ArrayListExcludeFile.Contains(dirInfo.Name()) Then
                    For Each StrItemInside In Directory.GetFileSystemEntries(Server.MapPath("~/"), "*.aspx")
                        If Path.GetFileName(Server.MapPath("~/")) = "" Then
                            StrTemp = dirInfo.Name()
                        Else
                            StrTemp = dirInfo.Name() & "/" & Path.GetFileName(Server.MapPath("~/"))
                        End If
                        ListAvailableFile.Items.Add(New ListItem(StrTemp, StrTemp))
                    Next
                End If
            Next

            For Each StrItem In Directory.GetFileSystemEntries(Server.MapPath("~/"), "*.aspx")
                StrTemp = Path.GetFileName(StrItem)
                If Not (ArrayListExcludeFile.Contains(StrTemp)) Then
                    If StrTemp.EndsWith("/") Then
                        StrTemp = StrTemp.Substring(0, StrTemp.Length - 1)
                    End If
                    ObjArraylist.Add(StrTemp.Trim)
                End If
            Next

            Dim flag As Integer = 0
            Dim arrTmp As New ArrayList
            For i As Integer = 0 To ListSelectedFile.Items.Count - 1
                For j As Integer = 0 To ObjArraylist.Count - 1
                    If ListSelectedFile.Items(i).Value = ObjArraylist(j) Then
                        flag = 1
                    End If
                Next
                If flag = 1 Then
                    arrTmp.Add(ListSelectedFile.Items(i).Value)
                    flag = 0
                End If
            Next

            For k As Integer = 0 To arrTmp.Count - 1
                ObjArraylist.Remove(arrTmp(k))
            Next
            ObjArraylist.Sort()
            ListAvailableFile.Items.Clear()
            For i As Integer = 0 To ObjArraylist.Count - 1
                ListAvailableFile.Items.Add(New ListItem(Split(ObjArraylist(i).ToString), ObjArraylist(i).ToString))
            Next
        Catch
            Throw
        Finally
            ObjArraylist.Clear()
            ObjArraylist = Nothing
        End Try
    End Sub
    ''' <summary>
    ''' Split
    ''' </summary>
    ''' <param name="FileName"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Split(ByVal FileName As String) As String
        Dim OriFilename As String = FileName
        Dim RemoveExt As String = OriFilename.Substring(0, OriFilename.LastIndexOf("."))
        Dim r As Regex = New Regex("[A-Z]")
        Dim GroupColl As MatchCollection = r.Matches(RemoveExt)
        Dim last, curr As Integer
        curr = 0
        last = 0
        Dim jadi As String = ""
        For Each i As Match In GroupColl
            curr = i.Index
            If curr <> last Then
                jadi = jadi + RemoveExt.Substring(last, curr - last) & " "
            End If
            last = curr
        Next
        jadi += RemoveExt.Substring(last, RemoveExt.Length - last)
        Return jadi
    End Function

    ''' <summary>
    ''' Load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.lblSuccess.Visible = False
            If Not Page.IsPostBack Then
                FillMsGroup()
                AddExcludeFile()
                FillSelectedFile()
                FillAvailableFile()
            End If
            Me.Page.GetPostBackEventReference(Me)

            Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

            'Using Transcope As New Transactions.TransactionScope
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                'Transcope.Complete()
            End Using
            'End Using
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' select change
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Combofk_MsGroup_id_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Combofk_MsGroup_id.SelectedIndexChanged
        Try
            Me.FillSelectedFile()
            Me.FillAvailableFile()
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' Add File Security
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAdd.Click
        Dim ObjListItem As ListItem
        Dim ObjArrayRemove As New ArrayList
        Dim i As Integer
        Dim j As Integer = 0
        Try
            For Each ObjListItem In ListAvailableFile.Items
                If ObjListItem.Selected Then
                    If Not ListSelectedFile.Items.Contains(ObjListItem) Then
                        ListSelectedFile.Items.Add(ListAvailableFile.Items(j))
                        ObjArrayRemove.Add(ListAvailableFile.Items(j))
                    End If
                End If
                j += 1
            Next
            For i = 0 To ObjArrayRemove.Count - 1
                ListAvailableFile.Items.Remove(ObjArrayRemove(i))
            Next
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            ObjArrayRemove = Nothing
            ObjListItem = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Remove FileSecurity
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageRemove_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageRemove.Click
        If ListSelectedFile.Items.Count > 0 Then
            Dim ObjItem As ListItem
            Dim ObjArrayRemove As New ArrayList
            Dim i As Integer
            Try
                For Each ObjItem In ListSelectedFile.Items
                    If ObjItem.Selected Then
                        ObjArrayRemove.Add(ObjItem)
                    End If
                Next
                For i = 0 To ObjArrayRemove.Count - 1
                    ListSelectedFile.Items.Remove(ObjArrayRemove(i))
                    ListAvailableFile.Items.Add(ObjArrayRemove(i))
                Next
            Catch ex As Exception
                Me.cvalPageErr.IsValid = False
                Me.cvalPageErr.ErrorMessage = ex.Message
                LogError(ex)
            Finally
                ObjItem = Nothing
                ObjArrayRemove = Nothing
            End Try
        End If
    End Sub

    ''' <summary>
    ''' Save File Security
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            If Page.IsValid Then
                '1. cek approval ya dulu
                Using AccessFileSecurityApproval As New AMLDAL.AMLDataSetTableAdapters.CountFileSecurityApprovalByGroupIDTableAdapter
                    Dim JmlFileApproval As Integer = AccessFileSecurityApproval.GetData(Me.Combofk_MsGroup_id.SelectedValue).Rows(0).Item("Jml")

                    If JmlFileApproval > 0 Then
                        Throw New Exception("Access Authority For Group " & Combofk_MsGroup_id.SelectedItem.Text.Trim & " Already In Pending Approval.")
                    Else
                        ' jika super user 
                        If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                            Using AccessCountFileSecurity As New AMLDAL.AMLDataSetTableAdapters.CountFileSecurityByGroupIDTableAdapter
                                Dim CountFile As Integer = AccessCountFileSecurity.GetData(Me.Combofk_MsGroup_id.SelectedValue).Rows(0).Item("Jml")

                                If CountFile > 0 Then ' edit
                                    Sahassa.AML.AuditTrailAlert.AuditTrailChecking(2 * (CountFile + ListSelectedFile.Items.Count))
                                    Using AcessGetFileSecurity As New AMLDAL.AMLDataSetTableAdapters.FileSecurityTableAdapter
                                        'Using TransScope As New Transactions.TransactionScope
                                        Using AccessAuditTrail As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                                            oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAuditTrail, IsolationLevel.ReadUncommitted)
                                            For Each Row As AMLDAL.AMLDataSet.FileSecurityRow In AcessGetFileSecurity.GetData.Select("FileSecurityGroupId=" & Me.Combofk_MsGroup_id.SelectedValue)
                                                ' insert audit trail for access authority 
                                                AccessAuditTrail.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Access Authority", "GroupName", "Edit", Combofk_MsGroup_id.SelectedItem.Text.Trim, "", "Accepted")
                                                AccessAuditTrail.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Access Authority", "FileName", "Edit", Row.FileSecurityFileName, "", "Accepted")
                                                ' Delete file security        
                                                AcessGetFileSecurity.DeleteFileSecurity(Row.FileSecurityID)
                                            Next
                                        End Using

                                        Using AccessAuditTrail As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAuditTrail, oSQLTrans)
                                            For i As Integer = 0 To ListSelectedFile.Items.Count - 1
                                                ' Insert file security
                                                AcessGetFileSecurity.Insert(CInt(Me.Combofk_MsGroup_id.SelectedValue), ListSelectedFile.Items(i).Value)
                                                ' insert audit trail for access authority

                                                AccessAuditTrail.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Access Authority", "GroupName", "Edit", "", Combofk_MsGroup_id.SelectedItem.Text.Trim, "Accepted")
                                                AccessAuditTrail.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Access Authority", "FileName", "Edit", "", ListSelectedFile.Items(i).ToString, "Accepted")

                                            Next
                                        End Using

                                        oSQLTrans.Commit()
                                        'TransScope.Complete()

                                        Me.lblSuccess.Visible = True
                                        Me.lblSuccess.Text = "Access Authority For Group " & Combofk_MsGroup_id.SelectedItem.Text & " Are Completed Save."
                                    End Using
                                    'End Using
                                Else 'add
                                    Sahassa.AML.AuditTrailAlert.AuditTrailChecking(2 * ListSelectedFile.Items.Count)

                                    'Using TransScope As New Transactions.TransactionScope

                                    Using AccessFileSecurity As New AMLDAL.AMLDataSetTableAdapters.FileSecurityTableAdapter
                                        oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessFileSecurity, IsolationLevel.ReadUncommitted)
                                        For i As Integer = 0 To ListSelectedFile.Items.Count - 1
                                            AccessFileSecurity.Insert(Me.Combofk_MsGroup_id.SelectedValue, Me.ListSelectedFile.Items(i).Value)

                                            'ObjFileSecurity.Add(Combofk_MsGroup_id.SelectedValue, ListSelectedFile.Items(i).ToString)
                                            'DataRepository.FileSecurityProvider.Save(ObjTransaction, ObjFileSecurity)
                                            Using AccessAuditTrail As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAuditTrail, oSQLTrans)

                                                AccessAuditTrail.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Access Authority", "GroupName", "Add", "", Combofk_MsGroup_id.SelectedItem.Text.Trim, "Accepted")
                                                AccessAuditTrail.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Access Authority", "FileName", "Add", "", ListSelectedFile.Items(i).ToString, "Accepted")
                                            End Using
                                            'ObjAudit_MenuManagement.Add(Commonly.SessionUserId, Commonly.SessionUserId, Now, "Access Authority", "Add", "GroupName", Nothing, Combofk_MsGroup_id.SelectedItem.Text.Trim, "Accepted")
                                            'ObjAudit_MenuManagement.Add(Commonly.SessionUserId, Commonly.SessionUserId, Now, "Access Authority", "Add", "FileName", Nothing, ListSelectedFile.Items(i).ToString, "Accepted")
                                        Next
                                    End Using

                                    oSQLTrans.Commit()
                                    'TransScope.Complete()

                                    Me.lblSuccess.Visible = True
                                    Me.lblSuccess.Text = "Access Authority For Group " & Combofk_MsGroup_id.SelectedItem.Text & " Are Completed Save."
                                    'End Using
                                End If
                            End Using
                        Else ' jika bukan super user 
                            ' cek group jika sudah ada d filesecurity
                            Using AccessFileSecurity As New AMLDAL.AMLDataSetTableAdapters.FileSecurityTableAdapter
                                Using AccessCountFile As New AMLDAL.AMLDataSetTableAdapters.CountFileSecurityByGroupIDTableAdapter
                                    Dim CountFileSecurity As Integer = CInt(AccessCountFile.GetData(Me.Combofk_MsGroup_id.SelectedValue).Rows(0)("Jml"))
                                    Using AccessInsertFileSecurityApproval As New AMLDAL.AMLDataSetTableAdapters.FileSecurity_ApprovalTableAdapter
                                        If CountFileSecurity > 0 Then ' Edit
                                            ''Using TransScope As New Transactions.TransactionScope
                                            ' save Menu Pending Approval
                                            Dim IdMenuPendingApproval As Integer
                                            Using AccessMenuPendingApproval As New AMLDAL.AMLDataSetTableAdapters.InsertMenuPendingApprovalTableAdapter
                                                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessMenuPendingApproval, IsolationLevel.ReadUncommitted)
                                                IdMenuPendingApproval = AccessMenuPendingApproval.Insert1(Sahassa.AML.Commonly.SessionUserId, Now, "Access Authority", "", Sahassa.AML.Commonly.TypeMode.Edit)
                                            End Using

                                            ' save Old File Security ke approval 
                                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessInsertFileSecurityApproval, oSQLTrans)
                                            For Each RowFileSecurityOld As AMLDAL.AMLDataSet.FileSecurityRow In AccessFileSecurity.GetData.Select("FileSecurityGroupId=" & Me.Combofk_MsGroup_id.SelectedValue)
                                                AccessInsertFileSecurityApproval.Insert(IdMenuPendingApproval, RowFileSecurityOld.FileSecurityID, RowFileSecurityOld.FileSecurityGroupId, RowFileSecurityOld.FileSecurityFileName, 1, Sahassa.AML.Commonly.TypeMode.Edit)
                                            Next

                                            'Save New File Security ke Approval 
                                            If Me.ListSelectedFile.Items.Count <> 0 Then
                                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessInsertFileSecurityApproval, oSQLTrans)
                                                For i As Integer = 0 To Me.ListSelectedFile.Items.Count - 1
                                                    'For Each RowID As AMLDAL.AMLDataSet.FileSecurityRow In AccessFileSecurity.GetData.Select("FileSecurityFileName='" & Me.ListSelectedFile.Items(i).Value & "'")
                                                    '    Dim IDFileSecurity As Integer = CInt(RowID.FileSecurityID)
                                                    'Next
                                                    AccessInsertFileSecurityApproval.Insert(IdMenuPendingApproval, 0, Me.Combofk_MsGroup_id.SelectedValue, Me.ListSelectedFile.Items(i).Value, 2, Sahassa.AML.Commonly.TypeMode.Edit)
                                                Next
                                            Else
                                                'ObjFileSecurity_Approval.Add(ObjMenu_PendingApproval.Pk_Menu_PendingApproval_Id, ObjFileSecurity.Item(0).Pk_FileSecurity_id, Combofk_MsGroup_id.SelectedValue, "", 2, EditMode.Edit)
                                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessInsertFileSecurityApproval, oSQLTrans)
                                                AccessInsertFileSecurityApproval.Insert(IdMenuPendingApproval, 0, Me.Combofk_MsGroup_id.SelectedValue, "", 2, Sahassa.AML.Commonly.TypeMode.Edit)
                                            End If

                                            oSQLTrans.Commit()
                                            ''TransScope.Complete()

                                            Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=8712&Identifier=" & Combofk_MsGroup_id.SelectedItem.Text

                                            Me.Response.Redirect("MessagePending.aspx?MessagePendingID=8712&Identifier=" & Combofk_MsGroup_id.SelectedItem.Text, False)
                                            ''End Using
                                        Else 'Add
                                            ''Using TransScope As New Transactions.TransactionScope
                                            ' save Menu Pending Approval
                                            Dim IdMenuPendingApproval As Integer
                                            Using AccessMenuPendingApproval As New AMLDAL.AMLDataSetTableAdapters.InsertMenuPendingApprovalTableAdapter
                                                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessMenuPendingApproval, IsolationLevel.ReadUncommitted)
                                                IdMenuPendingApproval = Integer.Parse(AccessMenuPendingApproval.Insert1(Sahassa.AML.Commonly.SessionUserId, Now, "Access Authority", "", CInt(Sahassa.AML.Commonly.TypeMode.Add)))
                                            End Using

                                            ' save New FileSecurity
                                            Sahassa.AML.TableAdapterHelper.SetTransaction(AccessInsertFileSecurityApproval, oSQLTrans)
                                            For i As Integer = 0 To ListSelectedFile.Items.Count - 1
                                                AccessInsertFileSecurityApproval.Insert(IdMenuPendingApproval, Nothing, CInt(Combofk_MsGroup_id.SelectedValue), ListSelectedFile.Items(i).Value, 2, CInt(Sahassa.AML.Commonly.TypeMode.Add))
                                            Next

                                            oSQLTrans.Commit()
                                            ''TransScope.Complete()

                                            Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=8711&Identifier=" & Combofk_MsGroup_id.SelectedItem.Text

                                            Me.Response.Redirect("MessagePending.aspx?MessagePendingID=8711&Identifier=" & Combofk_MsGroup_id.SelectedItem.Text, False)
                                            ''End Using
                                        End If
                                    End Using
                                End Using
                            End Using
                        End If
                    End If
                End Using
            End If
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Rollback()
            End If

            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Me.Response.Redirect("Default.aspx", False)
    End Sub
End Class
