Option Explicit On
Option Strict On

Imports SahassaNettier.Data
Imports SahassaNettier.Entities
Imports AMLBLL

Partial Class YearIntervalCDDLowRiskApprovalDetail
    Inherits Parent

    Public ReadOnly Property PKSystemParameterApprovalID() As Integer
        Get
            If Session("YearIntervalCDDLowRiskApprovalDetail.PK") Is Nothing Then
                Dim StrTmppkSystemParameterApprovalId As String

                StrTmppkSystemParameterApprovalId = Request.Params("PKSystemParameterApprovalID")
                If Integer.TryParse(StrTmppkSystemParameterApprovalId, 0) Then
                    Session("YearIntervalCDDLowRiskApprovalDetail.PK") = StrTmppkSystemParameterApprovalId
                Else
                    Throw New SahassaException("Parameter PKSystemParameterApprovalID is not valid.")
                End If

                Return CInt(Session("YearIntervalCDDLowRiskApprovalDetail.PK"))
            Else
                Return CInt(Session("YearIntervalCDDLowRiskApprovalDetail.PK"))
            End If
        End Get
    End Property

    Public ReadOnly Property ObjSystemParameterApproval() As TList(Of SystemParameter_Approval)
        Get
            Dim PkApprovalid As Integer
            Try
                If Session("YearIntervalCDDLowRiskApprovalDetail.ObjSystemParameterApproval") Is Nothing Then
                    PkApprovalid = PKSystemParameterApprovalID
                    Session("YearIntervalCDDLowRiskApprovalDetail.ObjSystemParameterApproval") = DataRepository.SystemParameter_ApprovalProvider.GetPaged("PK_SystemParameter_ApprovalID = " & PkApprovalid, "", 0, Integer.MaxValue, 0)
                End If
                Return CType(Session("YearIntervalCDDLowRiskApprovalDetail.ObjSystemParameterApproval"), TList(Of SystemParameter_Approval))
            Finally
            End Try
        End Get
    End Property

    Private Sub LoadDataApproval()
        If ObjSystemParameterApproval.Count > 0 Then
            If ObjSystemParameterApproval(0).FK_ModeID.GetValueOrDefault(0) = 2 Then
                Me.LblAction.Text = "Edit"
            End If

            Dim strRequestBy As String = ""
            Using objRequestBy As User = SahassaNettier.Data.DataRepository.UserProvider.GetBypkUserID(ObjSystemParameterApproval(0).FK_MsUserID.GetValueOrDefault(0))
                If Not objRequestBy Is Nothing Then
                    strRequestBy = objRequestBy.UserID & " ( " & objRequestBy.UserName & " ) "
                End If
            End Using

            LblRequestBy.Text = strRequestBy
            LblRequestDate.Text = ObjSystemParameterApproval(0).CreatedDate.GetValueOrDefault().ToString("dd-MM-yyyy")

            For Each ObjSystemParameter As SystemParameter In DataRepository.SystemParameterProvider.GetPaged("", "", 0, Integer.MaxValue, 0)
                With ObjSystemParameter
                    Select Case .PK_SystemParameter_ID
                        Case 12 'Modus Amount yang Diambil
                            Me.LblModusAmountOld.Text = .ParameterValue
                    End Select
                End With
            Next

            For Each ObjSystemParameter_ApprovalDetail As SystemParameter_ApprovalDetail In DataRepository.SystemParameter_ApprovalDetailProvider.GetPaged("", "", 0, Integer.MaxValue, 0)
                With ObjSystemParameter_ApprovalDetail
                    Select Case .FK_SystemParameter_ID
                        Case 12 'Modus Amount yang Diambil
                            Me.LblModusAmountNew.Text = .ParameterValue

                    End Select
                End With
            Next

        Else
            'kalau ternyata sudah di approve /reject dan memakai tombol back, maka redirect ke viewapproval
            Response.Redirect("YearIntervalCDDLowRiskApproval.aspx", False)
        End If
    End Sub

    Private Sub ClearSession()
        Session("YearIntervalCDDLowRiskApprovalDetail.PK") = Nothing
        Session("YearIntervalCDDLowRiskApprovalDetail.ObjSystemParameterApproval") = Nothing
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                ClearSession()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using

                LoadDataApproval()
            End If
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImgBtnAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAccept.Click
        Try
            If ObjSystemParameterApproval.Count > 0 Then
                Using ObjSystemParameterBll As New SystemParameterBLL
                    If ObjSystemParameterBll.AcceptLowRisk(Me.PKSystemParameterApprovalID) Then
                        Response.Redirect("YearIntervalCDDLowRiskApproval.aspx", False)
                    End If
                End Using
            Else
                Response.Redirect("YearIntervalCDDLowRiskApproval.aspx", False)
            End If

        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBtnReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnReject.Click
        Try
            If ObjSystemParameterApproval.Count > 0 Then
                Using ObjSystemParameterBll As New SystemParameterBLL
                    If ObjSystemParameterBll.RejectLowRiskEdit(Me.PKSystemParameterApprovalID) Then
                        Response.Redirect("YearIntervalCDDLowRiskApproval.aspx", False)
                    End If
                End Using
            Else
                Response.Redirect("YearIntervalCDDLowRiskApproval.aspx", False)
            End If
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBackAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBackAdd.Click
        Try
            Response.Redirect("YearIntervalCDDLowRiskApproval.aspx", False)
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    'Protected Sub CvalHandleErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles CvalHandleErr.PreRender
    '    If CvalHandleErr.IsValid = False Then
    '        Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
    '    End If
    'End Sub

    'Protected Sub CvalPageErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles CvalPageErr.PreRender
    '    If CvalPageErr.IsValid = False Then
    '        Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
    '    End If
    'End Sub
End Class
