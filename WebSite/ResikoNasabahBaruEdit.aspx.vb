Option Explicit On
Option Strict On
Imports AMLBLL
Imports SahassaNettier.Entities
Imports SahassaNettier.Data
Imports SahassaNettier.Services
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports Sahassa.AML.Commonly

Partial Class ResikoNasabahBaruEdit
    Inherits Parent

    Public Property ObjResikoNasabahBaru() As TList(Of ResikoNasabahBaru)
        Get
            If Session("ResikoNasabahBaruEdit.ObjResikoNasabahBaru") Is Nothing Then
                Using objreturn As TList(Of ResikoNasabahBaru) = DataRepository.ResikoNasabahBaruProvider.GetPaged("", "", 0, Integer.MaxValue, 0)
                    Session("ResikoNasabahBaruEdit.ObjResikoNasabahBaru") = objreturn
                End Using
            End If
            Return CType(Session("ResikoNasabahBaruEdit.ObjResikoNasabahBaru"), TList(Of ResikoNasabahBaru))
        End Get
        Set(ByVal value As TList(Of ResikoNasabahBaru))
            Session("ResikoNasabahBaruEdit.ObjResikoNasabahBaru") = value
        End Set
    End Property

    Public Property PkResikoNasabahBaruEditId() As Integer
        Get
            If Session("ResikoNasabahBaruEdit.PkResikoNasabahBaruEditId") Is Nothing Then
                Session("ResikoNasabahBaruEdit.PkResikoNasabahBaruEditId") = 0
            End If
            Return CType(Session("ResikoNasabahBaruEdit.PkResikoNasabahBaruEditId"), Integer)
        End Get
        Set(ByVal value As Integer)
            Session("ResikoNasabahBaruEdit.PkResikoNasabahBaruEditId") = value
        End Set
    End Property

    Public Property ObjEditResikoNasabahBaru() As ResikoNasabahBaru
        Get
            Return CType(Session("ObjEditResikoNasabahBaru.ObjResikoNasabahBaru"), ResikoNasabahBaru)
        End Get
        Set(ByVal value As ResikoNasabahBaru)
            Session("ObjEditResikoNasabahBaru.ObjResikoNasabahBaru") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                'Add Event Calender
                ClearSession()

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using

                BindAmount()
            End If
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message

        End Try
    End Sub

    Sub ClearSession()
        Session("ResikoNasabahBaruEdit.ConfigApproval") = Nothing
        Session("ResikoNasabahBaruEdit.objResikoNasabahBaru") = Nothing
        Session("objeditResikoNasabahBaru.objResikoNasabahBaru") = Nothing
        Session("objappResikoNasabahBaru.objResikoNasabahBaru") = Nothing

        Session("ResikoNasabahBaruEdit.PkResikoNasabahBaruEditId") = Nothing
    End Sub

    Public Sub IsDataValid(ByVal StrStatistikTransaksi As String, ByVal StrIdentitas As String, ByVal StrTipologi As String, ByVal StrRiskScoringAkhir As String)
        'Validasi Harus diisi
        If StrStatistikTransaksi = "" Then
            Throw New Exception("Statistik Transaksi harus diisi.")
        End If

        If StrIdentitas = "" Then
            Throw New Exception("Identitas harus diisi.")
        End If

        If StrTipologi = "" Then
            Throw New Exception("Tipologi harus diisi.")
        End If

        If StrRiskScoringAkhir = "" Then
            Throw New Exception("Risk Scoring Akhir harus diisi.")
        Else
            'risk scoring akhir harus H
            If StrRiskScoringAkhir <> "H" Then
                Throw New Exception("Risk Scoring Akhir harus bernilai H.")
            End If
        End If

        'Tidak boleh sama dengan kombinasi yang sudah ada
        For Each ObjResikoNasabahBaruItem As ResikoNasabahBaru In Me.ObjResikoNasabahBaru
            With ObjResikoNasabahBaruItem
                If Me.PkResikoNasabahBaruEditId <> .PK_ResikoNasabahBaru_ID Then
                    If StrStatistikTransaksi = .StatistikTransaksi Then
                        If StrIdentitas = .Identitas Then
                            If StrTipologi = .Tipologi Then
                                If StrRiskScoringAkhir = .RiskScoringAkhir Then
                                    Throw New Exception("Kombinasi sudah ada.")
                                End If
                            End If
                        End If
                    End If
                End If
            End With
        Next
    End Sub

    Private Sub SaveEdit()
        Try
            Using ObjResikoNasabahBaruBll As New ResikoNasabahBaruBLL
                If ObjResikoNasabahBaruBll.SaveDirectly(Me.ObjResikoNasabahBaru) Then
                    LblConfirmation.Text = "Success to Edit Resiko Nasabah Baru."
                    MtvResikoNasabahBaru.ActiveViewIndex = 1
                End If
            End Using
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub SaveToApproval()
        Try
            Using ObjResikoNasabahBaruBll As New ResikoNasabahBaruBLL
                If ObjResikoNasabahBaruBll.SaveToApproval(ObjResikoNasabahBaru) Then
                    LblConfirmation.Text = "Resiko Nasabah Baru parameter has been changed and it is currently waiting for approval."
                    MtvResikoNasabahBaru.ActiveViewIndex = 1
                End If
            End Using
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBtnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnSave.Click
        Try
            'cek apakah sudah ada di pending approval
            Dim IntCount As Integer = 0
            DataRepository.ResikoNasabahBaru_ApprovalProvider.GetPaged("", "", 0, 1, IntCount)

            If IntCount = 0 Then
                If Sahassa.AML.Commonly.SessionPkUserId = 1 Then
                    SaveEdit()
                Else
                    SaveToApproval()
                End If
            Else
                Throw New Exception("Cannot edit Resiko Nasabah Baru because it is currently waiting for approval.")
            End If
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBackAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBackAdd.Click
        Try
            Response.Redirect("Default.aspx", False)
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBack.Click
        Try
            Response.Redirect("Default.aspx", False)
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub ClearControl()
        Me.CboStatistikTransaksi.SelectedIndex = 0
        Me.CboIdentitas.SelectedIndex = 0
        Me.CboTipologi.SelectedIndex = 0

    End Sub

    Protected Sub ImgBtnAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAdd.Click
        Try
            ClearControl()
            MtvResikoNasabahBaru.ActiveViewIndex = 0
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    'Protected Sub CvalHandleErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles CvalHandleErr.PreRender
    '    If CvalHandleErr.IsValid = False Then
    '        Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
    '    End If
    'End Sub

    'Protected Sub CvalPageErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles CvalPageErr.PreRender
    '    If CvalPageErr.IsValid = False Then
    '        Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
    '    End If
    'End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            If MtvResikoNasabahBaru.ActiveViewIndex = 0 Then
                ImgBtnSave.Visible = True
                ImgBackAdd.Visible = True
            Else
                ImgBtnSave.Visible = False
                ImgBackAdd.Visible = False
            End If
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Function BindAmount() As Boolean
        Me.GrdVwResikoNasabahBaru.DataSource = Me.ObjResikoNasabahBaru
        Me.GrdVwResikoNasabahBaru.DataBind()
    End Function

    Protected Sub GrdVwResikoNasabahBaru_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GrdVwResikoNasabahBaru.RowDataBound
        Dim PKMsAction As String = Nothing
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim objData As ResikoNasabahBaru = CType(e.Row.DataItem, ResikoNasabahBaru)
                Dim lnkButtonRemove As LinkButton = CType(e.Row.FindControl("lnkButtonRemove"), LinkButton)
                lnkButtonRemove.CommandName = "Remove"
                lnkButtonRemove.CommandArgument = objData.PK_ResikoNasabahBaru_ID.ToString

                Dim LinkBtnEdit As LinkButton = CType(e.Row.FindControl("LinkBtnEdit"), LinkButton)
                LinkBtnEdit.CommandName = "EditData"
                LinkBtnEdit.CommandArgument = objData.PK_ResikoNasabahBaru_ID.ToString
            End If
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImageAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAdd.Click
        Try
            If ObjEditResikoNasabahBaru Is Nothing Then
                Me.IsDataValid(Me.CboStatistikTransaksi.SelectedValue.ToString, Me.CboIdentitas.SelectedValue.ToString, Me.CboTipologi.SelectedValue.ToString, LblResikoAkhir.Text)

                Dim ObjResikoNasabahBaruItem As New ResikoNasabahBaru

                Dim generator As New Random
                Dim randomValue As Integer
                randomValue = generator.Next(1, Integer.MaxValue)
                While Me.ObjResikoNasabahBaru.FindAll(ResikoNasabahBaruColumn.PK_ResikoNasabahBaru_ID, randomValue).Count = 1
                    randomValue = generator.Next(1, Integer.MaxValue)
                End While

                With ObjResikoNasabahBaruItem
                    .PK_ResikoNasabahBaru_ID = randomValue

                    .StatistikTransaksi = Me.CboStatistikTransaksi.SelectedValue.ToString
                    .Identitas = Me.CboIdentitas.SelectedValue.ToString
                    .Tipologi = Me.CboTipologi.SelectedValue.ToString
                    .RiskScoringAkhir = LblResikoAkhir.Text
                End With

                ObjResikoNasabahBaru.Add(ObjResikoNasabahBaruItem)

                BindAmount()
                ClearControl()
            Else
                Me.IsDataValid(Me.CboStatistikTransaksi.SelectedValue.ToString, Me.CboIdentitas.SelectedValue.ToString, Me.CboTipologi.SelectedValue.ToString, LblResikoAkhir.Text)

                Using ObjEditResikoNasabahBaru As ResikoNasabahBaru = Me.ObjResikoNasabahBaru.Find(SahassaNettier.Entities.ResikoNasabahBaruColumn.PK_ResikoNasabahBaru_ID, Me.PkResikoNasabahBaruEditId)
                    If Not ObjEditResikoNasabahBaru Is Nothing Then
                        With ObjEditResikoNasabahBaru
                            .StatistikTransaksi = Me.CboStatistikTransaksi.SelectedValue.ToString
                            .Identitas = Me.CboIdentitas.SelectedValue.ToString
                            .Tipologi = Me.CboTipologi.SelectedValue.ToString
                            .RiskScoringAkhir = LblResikoAkhir.Text
                        End With
                    End If
                End Using

                BindAmount()
                ClearControl()
                ObjEditResikoNasabahBaru = Nothing
                ImgBtnCancel.Visible = False
            End If

        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub GrdVwResikoNasabahBaru_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdVwResikoNasabahBaru.RowCommand
        If e.CommandName = "Remove" Then

            Dim idx As Integer = CInt(e.CommandArgument)
            Using ObjDeleted As ResikoNasabahBaru = ObjResikoNasabahBaru.Find(ResikoNasabahBaruColumn.PK_ResikoNasabahBaru_ID, idx)
                If Not ObjDeleted Is Nothing Then
                    If Not ObjEditResikoNasabahBaru Is Nothing Then
                        If ObjDeleted.PK_ResikoNasabahBaru_ID = Me.PkResikoNasabahBaruEditId Then
                            ImgBtnCancel_Click(ImgBtnCancel, Nothing)
                        End If
                    End If

                    ObjResikoNasabahBaru.Remove(ObjDeleted)
                End If
                BindAmount()
            End Using
        ElseIf e.CommandName = "EditData" Then
            Dim idx As Integer = CInt(e.CommandArgument)

            Using ObjEdit As ResikoNasabahBaru = ObjResikoNasabahBaru.Find(ResikoNasabahBaruColumn.PK_ResikoNasabahBaru_ID, idx)
                If Not ObjEdit Is Nothing Then
                    ObjEditResikoNasabahBaru = ObjEdit
                    Me.PkResikoNasabahBaruEditId = ObjEdit.PK_ResikoNasabahBaru_ID
                    LoadEditData()
                End If
            End Using
        End If
    End Sub

    Sub LoadEditData()
        If Not Me.ObjEditResikoNasabahBaru Is Nothing Then
            With Me.ObjEditResikoNasabahBaru
                Me.CboStatistikTransaksi.SelectedValue = .StatistikTransaksi
                Me.CboIdentitas.SelectedValue = .Identitas
                Me.CboTipologi.SelectedValue = .Tipologi

            End With
        End If

        ImgBtnCancel.Visible = True
    End Sub

    Protected Sub ImgBtnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnCancel.Click
        Try
            ClearControl()
            ObjEditResikoNasabahBaru = Nothing
            Me.PkResikoNasabahBaruEditId = Nothing
            ImgBtnCancel.Visible = False
        Catch sex As SahassaException
            CvalHandleErr.IsValid = False
            CvalHandleErr.ErrorMessage = sex.Message
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message

        End Try
    End Sub
End Class