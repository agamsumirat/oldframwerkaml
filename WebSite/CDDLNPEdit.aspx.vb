﻿Imports CDDLNPNettier.Data
Imports CDDLNPNettier.Entities
Imports AMLBLL
Imports AMLBLL.CDDLNPBLL
Imports System.Data

Partial Class CDDLNPEdit
    Inherits Parent

    Private ReadOnly Property GetPK_CDDLNP_ID() As Int32
        Get
            Return Request.QueryString("CID")
        End Get
    End Property

    Private ReadOnly Property GetFK_CDD_Type_ID() As Int32
        Get
            Return Request.QueryString("TID")
        End Get
    End Property

    Private ReadOnly Property GetFK_CDD_NasabahType_ID() As Int32
        Get
            Return Request.QueryString("NTID")
        End Get
    End Property

    Private Property ScreeningCustomerPK As Int64
        Get
            Return Session("ScreeningCustomerPK")
        End Get
        Set
            Session("ScreeningCustomerPK") = Value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not Page.IsPostBack Then
                'Audit Trail Access Page
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using

                Select Case GetFK_CDD_Type_ID
                    Case 1, 3 : ImageHistory.Visible = False
                    Case 2, 4 : ImageHistory.Visible = True
                End Select

                'Set Attachment Nasabah
                Using objAttachment As TList(Of CDDLNP_Attachment) = DataRepository.CDDLNP_AttachmentProvider.GetPaged("FK_CDDLNP_ID = " & Me.GetPK_CDDLNP_ID, String.Empty, 0, Int32.MaxValue, 0)
                    If objAttachment.Count > 0 Then
                        SessionCDDLNP_Attachment = objAttachment
                    Else
                        SessionCDDLNP_Attachment = New TList(Of CDDLNP_Attachment)
                    End If
                End Using

                'Reset Session
                SessionCDDLNPAuditTrailId = 0
                SessionCDDLNP_Attachment = New TList(Of CDDLNP_Attachment)

                ScreeningCustomerPK = -1
            End If

            Using tblCDDLNP As VList(Of vw_CDDLNP_View) = DataRepository.vw_CDDLNP_ViewProvider.GetPaged("PK_CDDLNP_ID = " & Me.GetPK_CDDLNP_ID & " AND CreatedBy = '" & Sahassa.AML.Commonly.SessionUserId & "'", String.Empty, 0, 1, 0)
                If tblCDDLNP.Count > 0 Then
                    'Set Screening Nasabah
                    If Not Page.IsPostBack Then
                        SessionCDDLNPAuditTrailId = tblCDDLNP(0).FK_AuditTrail_Potensial_Screening_Customer_ID.GetValueOrDefault(0)
                    End If

                    'Set Customer Type
                    SessionCustomerType = tblCDDLNP(0).CDD_NasabahType_ID

                    'Set CDDLNP Risk Fact Question
                    SessionCDDLNP_RiskFactQuestionDetail = DataRepository.CDDLNP_RiskFactQuestionDetailProvider.GetPaged("FK_CDDLNP_ID = " & Me.GetPK_CDDLNP_ID, String.Empty, 0, Int32.MaxValue, 0)

                    'Set CDD Type
                    Using objCDDType As CDD_Type = DataRepository.CDD_TypeProvider.GetByPK_CDD_Type_ID(Me.GetFK_CDD_Type_ID)
                        If objCDDType IsNot Nothing Then

                            If SessionReferenceID = 0 Then
                                lblHeader.Text = "CDD " & objCDDType.CDD_Type
                            Else
                                lblHeader.Text = "CDD for Beneficial Owner"
                                ClientScript.RegisterStartupScript(Page.GetType(), "BO", "alert('Mohon perbaiki form BO atau pemilik sumber dana berikut jika ada revisi; Jika tidak lanjutkan ke tombol save di bawah');", True)
                            End If

                            SessionDefaultScore = objCDDType.CDD_DefaultScore
                        End If
                    End Using

                Else
                    Using objCDDLNP As New CDDLNPBLL
                        Me.Response.Redirect(objCDDLNP.GetCDDLNPUrl(Me.GetFK_CDD_Type_ID))
                    End Using
                End If
            End Using

            Using objCDDLNP As New CDDLNPBLL
                objCDDLNP.ConstructFormEdit(Me.TableCategory, Me.GetFK_CDD_Type_ID, Me.GetFK_CDD_NasabahType_ID, Me.GetPK_CDDLNP_ID)
                objCDDLNP.LoadAnswer(Me.TableCategory, Me.GetPK_CDDLNP_ID)
            End Using
        Catch ex As Exception
            cvalPageErr.ErrorMessage = ex.Message
            cvalPageErr.IsValid = False
        End Try
    End Sub

    Protected Sub ImageSavePropose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSavePropose.Click
        Try
            Dim isExist As Boolean = False
            For Each objWorkflow As CDDLNP_WorkflowUpload In DataRepository.CDDLNP_WorkflowUploadProvider.GetPaged("List_RM LIKE '%" & Sahassa.AML.Commonly.SessionUserId & "%'", String.Empty, 0, Int32.MaxValue, 0)
                For Each objUserId As String In objWorkflow.List_RM.Split(";")
                    If String.Compare(Sahassa.AML.Commonly.SessionUserId, objUserId, StringComparison.OrdinalIgnoreCase) = 0 Then
                        isExist = True
                    End If
                Next
            Next

            If Not isExist Then
                Throw New Exception("Your user ID has not been assigned for workflow approval. Please contact your administrator.")
            End If

            Using objCDDLNP As New CDDLNPBLL
                If objCDDLNP.IsRequired(Me.TableCategory, Me.GetPK_CDDLNP_ID) Then
                    If objCDDLNP.IsNeedFillBO(Me.TableCategory) Then
                        SessionCDDLNPIsCompleted = False
                        objCDDLNP.CDDLNPEdit(Me.TableCategory, Me.GetFK_CDD_Type_ID, Me.GetFK_CDD_NasabahType_ID, Me.GetPK_CDDLNP_ID, SessionReferenceID)
                        Using objParent As TList(Of CDDLNP) = DataRepository.CDDLNPProvider.GetPaged("FK_RefParent_ID = " & SessionReferenceID, String.Empty, 0, 1, 0)
                            If objParent.Count > 0 Then
                                Me.Response.Redirect("CDDLNPEdit.aspx?CID=" & objParent(0).PK_CDDLNP_ID & "&TID=" & Me.GetFK_CDD_Type_ID & "&NTID=2")
                            Else
                                Me.Response.Redirect("CDDLNPAdd.aspx?TID=" & Me.GetFK_CDD_Type_ID & "&NTID=2")
                            End If
                        End Using

                    Else
                        SessionCDDLNPIsCompleted = True
                        objCDDLNP.CDDLNPEdit(Me.TableCategory, Me.GetFK_CDD_Type_ID, Me.GetFK_CDD_NasabahType_ID, Me.GetPK_CDDLNP_ID, SessionReferenceID)
                        objCDDLNP.UpdateAMLRating(SessionReferenceID)
                        CDDLNPEmailSend.CDDLNPEmailSend(SessionReferenceID)
                        SessionReferenceID = 0
                        ClientScript.RegisterStartupScript(Page.GetType(), "Success", "alert('CDD is submitted for approval.');window.location='" & objCDDLNP.GetCDDLNPUrl(Me.GetFK_CDD_Type_ID) & "';", True)
                    End If
                End If
            End Using
        Catch ex As Exception
            cvalPageErr.ErrorMessage = ex.Message
            cvalPageErr.IsValid = False
        End Try
    End Sub

    Protected Sub ImageSaveDraft_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSaveDraft.Click
        Try
            Using objCDDLNP As New CDDLNPBLL
                SessionCDDLNPIsCompleted = False
                objCDDLNP.CDDLNPEdit(Me.TableCategory, Me.GetFK_CDD_Type_ID, Me.GetFK_CDD_NasabahType_ID, Me.GetPK_CDDLNP_ID, SessionReferenceID)
                SessionReferenceID = 0
                ClientScript.RegisterStartupScript(Page.GetType(), "Success", "alert('CDD is saved as draft.');window.location='" & objCDDLNP.GetCDDLNPUrl(Me.GetFK_CDD_Type_ID) & "';", True)
            End Using
        Catch ex As Exception
            cvalPageErr.ErrorMessage = ex.Message
            cvalPageErr.IsValid = False
        End Try
    End Sub

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Try
            Using objCDDLNP As New CDDLNPBLL
                SessionReferenceID = 0
                Me.Response.Redirect(objCDDLNP.GetCDDLNPUrl(Me.GetFK_CDD_Type_ID))
            End Using
        Catch ex As Exception
            cvalPageErr.ErrorMessage = ex.Message
            cvalPageErr.IsValid = False
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Dim l As LinkButton = CType(TableCategory.FindControl("lnkSuspect"), LinkButton)
            Dim totalSuspect As Long = EKABLL.CDDBLL.GetTotalSuspectScreening(ScreeningCustomerPK)

            If totalSuspect < 0 Then
                l.Visible = False
            Else
                l.Visible = True
                l.Text = totalSuspect.ToString() & " suspect(s)"
                SessionCDDLNPAuditTrailCount = totalSuspect.ToString()
            End If

            'Using objAudit As SahassaNettier.Entities.AuditTrail_Potensial_Screening_Customer = SahassaNettier.Data.DataRepository.AuditTrail_Potensial_Screening_CustomerProvider.GetByPK_AuditTrail_Potensial_Screening_Customer_ID(SessionCDDLNPAuditTrailId)
            '    If objAudit Is Nothing Then
            '        l.Visible = False
            '    Else
            '        l.Visible = True
            '        l.Text = objAudit.SuspectMatch.ToString & " suspect(s)"
            '        SessionCDDLNPAuditTrailCount = objAudit.SuspectMatch.ToString
            '    End If
            'End Using

            Using objCDDLNP As New CDDLNPBLL
                Dim lblAMLResult As Label = CType(TableCategory.FindControl("lblAMLResult"), Label)
                SessionRiskRating = objCDDLNP.CalculateRating(Me.TableCategory, Me.GetPK_CDDLNP_ID)
                lblAMLResult.ForeColor = Drawing.Color.FromName(objCDDLNP.GetRatingColor(SessionRiskRating))
                lblAMLResult.Text = SessionRiskRating
                objCDDLNP.HideUncheck(Me.TableCategory)
            End Using
        Catch ex As Exception
            cvalPageErr.ErrorMessage = ex.Message
            cvalPageErr.IsValid = False
        End Try
    End Sub

    Protected Sub CvalPageErr_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles cvalPageErr.PreRender
        If cvalPageErr.IsValid = False Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "gototop" & Guid.NewGuid.ToString, "f_scrollTop()", True)
        End If
    End Sub

    Protected Sub ImageHistory_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageHistory.Click
        Try
            Dim strCIF As String = String.Empty
            Using objCDDLNP As New CDDLNPBLL
                strCIF = objCDDLNP.getCIF(Me.TableCategory)
            End Using

            If DataRepository.CDDLNPProvider.GetTotalItems("CIF = '" & strCIF & "'", 0) = 0 Then
                Throw New Exception("No history was found for CIF : '" & strCIF & "'. Please fill below CIF correctly to view history.")
            End If

            Me.Controls.Add(New LiteralControl("<script>popupWindow = window.open('CDDLNPHistory.aspx?ID=" & strCIF & "','mustunique','scrollbars=1,directories=0,height=600,width=800,location=0,menubar=0,resizable=1,status=0,toolbar=0');popupWindow.focus();</script> "))
        Catch ex As Exception
            cvalPageErr.ErrorMessage = ex.Message
            cvalPageErr.IsValid = False
        End Try
    End Sub
End Class