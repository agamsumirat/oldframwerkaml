Imports System.Data.SqlClient
Imports Sahassa.AML.TableAdapterHelper
Partial Class PeerGroupEdit
    Inherits Parent
    Public ReadOnly Property PeerGroupID() As String
        Get
            Dim temp As String

            temp = Request.Params("PeerGroupID")
            If Not IsNumeric(temp) Then
                Try
                    Throw New Exception("Peer Group ID not valid")
                Catch ex As Exception
                    Return ""
                End Try
            Else
                Return temp
            End If
        End Get
    End Property
    Public Property oPeerGroup() As AMLDAL.PeerGroup.PeerGroupRow
        Get
            If Not Session("RowPeerGroup") Is Nothing Then

                Return Session("RowPeerGroup")
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As AMLDAL.PeerGroup.PeerGroupRow)
            Session("RowPeerGroup") = value
        End Set
    End Property
    Public ReadOnly Property oQueryBuilder() As Sahassa.AML.QueryBuilder
        Get
            If Session("PeerGroupQueryBuilderEdit") Is Nothing Then
                Return New Sahassa.AML.QueryBuilder
            Else
                Return Session("PeerGroupQueryBuilderEdit")
            End If
        End Get

    End Property
    Protected Sub Menu1_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs) Handles Menu1.MenuItemClick
        MultiView1.ActiveViewIndex = Int32.Parse(e.Item.Value)
        Tablestest1.ActiveIndex = MultiView1.ActiveViewIndex

        WhereClausesTest1.ActiveIndex = MultiView1.ActiveViewIndex
        GroupBy1.ActiveIndex = MultiView1.ActiveViewIndex
        HavingCondition1.ActiveIndex = MultiView1.ActiveViewIndex


    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)


            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

            End Using
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Not IsPostBack Then

            LoadData()
            SetSession()
        End If
    End Sub
    Private Sub InsertAuditTrail(ByRef osqltrans As SqlTransaction)
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(3)

            Dim PeerGroupDescription As String
            If Me.TextDescription.Text.Trim.Length <= 255 Then
                PeerGroupDescription = Me.TextDescription.Text.Trim
            Else
                PeerGroupDescription = Me.TextDescription.Text.Substring(0, 255)
            End If
            If Not oPeerGroup Is Nothing Then


                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                    SetTransaction(AccessAudit, osqltrans)
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Peer Group", "Peer Group Name", "Edit", oPeerGroup.PeerGroupName.Trim, Me.TextPeerGroupName.Text.Trim, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Peer Group", "Description", "Edit", oPeerGroup.PeerGroupDescription.Trim, PeerGroupDescription, "Accepted")
                    AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "Peer Group", "Enabled", "Edit", oPeerGroup.Enabled, chkEnabled.Checked, "Accepted")

                End Using
            End If
        Catch
            Throw
        End Try
    End Sub
    Private Sub LoadData()
        Try
            'Get Data Peer Group by Peer Group
            Using adapter As New AMLDAL.PeerGroupTableAdapters.PeerGroupTableAdapter
                For Each orows As AMLDAL.PeerGroup.PeerGroupRow In adapter.GetPeerGroupByPK(Me.PeerGroupID).Rows
                    oQueryBuilder.SQlQuery = orows.Expression
                    oQueryBuilder.SetttingDataset()
                    Me.TextPeerGroupName.Text = orows.PeerGroupName
                    Me.TextDescription.Text = orows.PeerGroupDescription
                    Me.chkEnabled.Checked = orows.Enabled
                    Me.oPeerGroup = orows
                Next
            End Using
            'load Data

        Catch ex As Exception
            cvalPageError.IsValid = False
            cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Protected Sub CustomValidator1_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles CustomValidator1.ServerValidate
        If oQueryBuilder.SQlQuery = "" Then
            args.IsValid = False

            CustomValidator1.ErrorMessage = "Please configure sql expression first!"

        Else
            Try
                oQueryBuilder.ParseSQLQuery()
                args.IsValid = True
                CustomValidator1.ErrorMessage = ""
            Catch ex As Exception
                CustomValidator1.ErrorMessage = "There is an error in sql expression.Please configure sql expression again !"
                args.IsValid = False
            End Try



        End If
    End Sub

    Protected Sub ImageSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageSave.Click
        Try
            If Page.IsValid AndAlso IsDataValid() Then
                'jika superuser maka 
                If Sahassa.AML.Commonly.SessionUserId.ToLower = "superuser" Then
                    UpdatePeerGroupBySU()
                Else
                    UpdateAdvaceRulesToPendingApproval()
                End If
            End If
        Catch ex As Exception
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        End Try


    End Sub
    Private Sub SetSession()
        Tablestest1.OQueryBuilder = Me.oQueryBuilder
        Fields1.OQueryBuilder = Me.oQueryBuilder
        WhereClausesTest1.OQueryBuilder = Me.oQueryBuilder
        GroupBy1.OQueryBuilder = Me.oQueryBuilder
        HavingCondition1.OQueryBuilder = Me.oQueryBuilder
    End Sub
    Private Sub AbandonSession()
        Session("PeerGroupQueryBuilderEdit") = Nothing
        Tablestest1.OQueryBuilder = Nothing
        Fields1.OQueryBuilder = Nothing
        WhereClausesTest1.OQueryBuilder = Nothing
        GroupBy1.OQueryBuilder = Nothing
        HavingCondition1.OQueryBuilder = Nothing
        SQLExpression1.OQueryBuilder = Nothing
    End Sub
    ''' <summary>
    ''' Update data AdvanceRule yang dilakukan oleh superuser
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub UpdatePeerGroupBySU()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            Using adapter As New AMLDAL.PeerGroupTableAdapters.PeerGroupTableAdapter
                oSQLTrans = BeginTransaction(adapter)
                adapter.UpdatePeerGroupByPK(TextPeerGroupName.Text.Trim, Me.TextDescription.Text.Trim, chkEnabled.Checked, oQueryBuilder.SQlQuery, Now, oPeerGroup.PeerGroupId)
            End Using
            AbandonSession()
            InsertAuditTrail(oSQLTrans)
            SetSession()
            oSQLTrans.Commit()
            Response.Redirect("PeerGroupView.aspx", False)
        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Rollback()
            End If
            LogError(ex)
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub
    ''' <summary>
    ''' update data yang dilakukan user selain superuser
    ''' </summary>

    Private Sub UpdateAdvaceRulesToPendingApproval()
        'insert ke header 
        'insert ke detail
        Dim intHeader As Integer
        Dim oSQLTrans As SqlTransaction = Nothing
        Try


            Using adapter As New AMLDAL.PeerGroupTableAdapters.PeerGroupPendingApprovalTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(adapter)
                intHeader = adapter.InsertPeerGroupPendingApproval(oPeerGroup.PeerGroupName, Sahassa.AML.Commonly.SessionUserId, 2, "Peer Group Edit", Now)
            End Using

            'insert to detail
            Using adapter As New AMLDAL.PeerGroupTableAdapters.PeerGroupApprovalTableAdapter
                Sahassa.AML.TableAdapterHelper.SetTransaction(adapter, oSQLTrans)
                adapter.Insert(intHeader, Me.PeerGroupID, TextPeerGroupName.Text.Trim, TextDescription.Text.Trim, chkEnabled.Checked, SQLExpression1.OQueryBuilder.SQlQuery, oPeerGroup.CreatedDate, oPeerGroup.PeerGroupId, oPeerGroup.PeerGroupName, oPeerGroup.PeerGroupDescription, oPeerGroup.Enabled, oPeerGroup.Expression, oPeerGroup.CreatedDate)
            End Using
            oSQLTrans.Commit()
            Dim MessagePendingID As Integer = 81602 'MessagePendingID 81602 = Peer Group Edit

            Sahassa.AML.Commonly.SessionIntendedPage = "MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & TextPeerGroupName.Text.Trim
            Me.Response.Redirect("MessagePending.aspx?MessagePendingID=" & MessagePendingID & "&Identifier=" & TextPeerGroupName.Text.Trim, False)


        Catch ex As Exception
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Me.cvalPageError.IsValid = False
            Me.cvalPageError.ErrorMessage = ex.Message
            LogError(ex)
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub

    Private Function IsDataValid() As Boolean
        Try
            'cek rules name sudah ada belum di peer group selain data yang sedang di edit
            Using adapter As New AMLDAL.PeerGroupTableAdapters.PeerGroupTableAdapter

                Dim intJmlPeerGroup As Nullable(Of Integer) = adapter.CountPeerGroupExceptOriginal(TextPeerGroupName.Text.Trim, oPeerGroup.PeerGroupName)
                If intJmlPeerGroup.GetValueOrDefault(0) > 0 Then

                    cvalPageError.IsValid = False
                    cvalPageError.ErrorMessage = "Peer group name already exits in peer group"
                    Return False
                End If
            End Using
            'cek apakah rules name lama ada di table approval
            Using Adapter As New AMLDAL.PeerGroupTableAdapters.PeerGroupPendingApprovalTableAdapter
                Dim intJmlApproval As Integer = Adapter.CountPeerGroupPendingApprovalByUniqueKey(oPeerGroup.PeerGroupName)
                If intJmlApproval > 0 Then

                    cvalPageError.IsValid = False
                    cvalPageError.ErrorMessage = "Peer Group already exits in Peer Group Pending Approval."
                    Return False
                End If
            End Using
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Response.Redirect("PeerGroupView.aspx", False)
    End Sub
End Class
