#Region "Imports..."
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Excel
Imports Sahassa.AML.Commonly
Imports Sahassanettier.Data
Imports Sahassanettier.Entities
Imports Sahassanettier.Data.SqlClient
Imports System
Imports System.Collections.Generic
Imports System.Data
Imports System.IO
Imports AMLBLL.DataType
#End Region

Partial Class MsKepemilikan_upload
	Inherits Parent

#Region "Structure..."

	Structure GrouP_MsKepemilikan_success
		Dim MsKepemilikan As MsKepemilikan_ApprovalDetail
        Dim L_MsKepemilikanNCBS_ApprovalDetail As TList(Of MsKepemilikanNCBS_ApprovalDetail)
 
	End Structure

#End Region

#Region "Property umum"
	Private Property GroupTableSuccessData() As List(Of GrouP_MsKepemilikan_success)
		Get
			Return Session("GroupTableSuccessData")
		End Get
		Set(ByVal value As List(Of GrouP_MsKepemilikan_success))
			Session("GroupTableSuccessData") = value
		End Set
	End Property

    Private Property GroupTableAnomalyData() As List(Of AMLBLL.MsKepemilikanBll.DT_Group_MsKepemilikan)
        Get
            Return Session("GroupTableAnomalyData")
        End Get
        Set(ByVal value As List(Of AMLBLL.MsKepemilikanBll.DT_Group_MsKepemilikan))
            Session("GroupTableAnomalyData") = value
        End Set
    End Property

    Public ReadOnly Property GET_PKUserID() As Integer
        Get
            Return SessionPkUserId
        End Get
    End Property

    Public ReadOnly Property GET_UserName() As String
        Get
            Return SessionUserId
            'Return SessionNIK
        End Get
    End Property

    Private Property SetnGetDataTableForSuccessData() As TList(Of MsKepemilikan_ApprovalDetail)
        Get
            Return CType(IIf(Session("MsKepemilikan_upload.TableSuccessUpload") Is Nothing, Nothing, Session("MsKepemilikan_upload.TableSuccessUpload")), TList(Of MsKepemilikan_ApprovalDetail))
        End Get
        Set(ByVal value As TList(Of MsKepemilikan_ApprovalDetail))
            Session("MsKepemilikan_upload.TableSuccessUpload") = value
        End Set
    End Property

    Private Property SetnGetDataTableForAnomalyData() As Data.DataTable
        Get
            Return CType(IIf(Session("MsKepemilikan_upload.TableAnomalyUpload") Is Nothing, Nothing, Session("MsKepemilikan_upload.TableAnomalyUpload")), Data.DataTable)
        End Get
        Set(ByVal value As Data.DataTable)
            Session("MsKepemilikan_upload.TableAnomalyUpload") = value
        End Set
    End Property

    Private Property DSexcelTemp() As Data.DataSet
        Get
            Return CType(IIf(Session("MsKepemilikan_upload.DSexcelTemp") Is Nothing, Nothing, Session("MsKepemilikan_upload.DSexcelTemp")), DataSet)
        End Get
        Set(ByVal value As Data.DataSet)
            Session("MsKepemilikan_upload.DSexcelTemp") = value
        End Set
    End Property

#End Region

#Region "Property Tabel Temporary"
    Private Property DT_AllMsKepemilikan() As Data.DataTable
        Get
            Return CType(IIf(Session("MsKepemilikan_upload.DT_MsKepemilikan") Is Nothing, Nothing, Session("MsKepemilikan_upload.DT_MsKepemilikan")), Data.DataTable)
        End Get
        Set(ByVal value As Data.DataTable)
            Session("MsKepemilikan_upload.DT_MsKepemilikan") = value
        End Set
    End Property

    Private Property DT_AllMsKepemilikanNCBS_ApprovalDetail() As Data.DataTable
        Get
            Return CType(IIf(Session("MsKepemilikan_upload.DT_MsKepemilikanNCBS_ApprovalDetail") Is Nothing, Nothing, Session("MsKepemilikan_upload.DT_MsKepemilikanNCBS_ApprovalDetail")), Data.DataTable)
        End Get
        Set(ByVal value As Data.DataTable)
            Session("MsKepemilikan_upload.DT_MsKepemilikanNCBS_ApprovalDetail") = value
        End Set
    End Property

#End Region

#Region "Property Table Sukses Data"

    Private Property TableSuccessMsKepemilikan() As TList(Of MsKepemilikan_ApprovalDetail)
        Get
            Return Session("TableSuccessMsKepemilikan")
        End Get
        Set(ByVal value As TList(Of MsKepemilikan_ApprovalDetail))
            Session("TableSuccessMsKepemilikan") = value
        End Set
    End Property

    Private Property TableSuccessMsKepemilikanNCBS_ApprovalDetail() As TList(Of MsKepemilikanNCBS_ApprovalDetail)
        Get
            Return Session("TableSuccessMsKepemilikanNCBS_ApprovalDetail")
        End Get
        Set(ByVal value As TList(Of MsKepemilikanNCBS_ApprovalDetail))
            Session("TableSuccessMsKepemilikanNCBS_ApprovalDetail") = value
        End Set
    End Property


#End Region

#Region "Property Table Anomaly Data"

    Private Property TableAnomalyMsKepemilikan() As Data.DataTable
        Get
            Return Session("TableAnomalyMsKepemilikan")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("TableAnomalyMsKepemilikan") = value
        End Set
    End Property

    Private Property TableAnomalyMsKepemilikanNCBS_ApprovalDetail() As Data.DataTable
        Get
            Return Session("TableAnomalyMsKepemilikanNCBS_ApprovalDetail")
        End Get
        Set(ByVal value As Data.DataTable)
            Session("TableAnomalyMsKepemilikanNCBS_ApprovalDetail") = value
        End Set
    End Property

#End Region

#Region "validasi.."

    Private Function ValidateInput(ByVal MsKepemilikan As AMLBLL.MsKepemilikanBll.DT_Group_MsKepemilikan) As List(Of String)
        Dim temp As New List(Of String)
        Dim msgError As New StringBuilder
        Dim errorline As New StringBuilder
        Try
            'validasi
            ValidateMsKepemilikan(MsKepemilikan.MsKepemilikan, msgError, errorline)
            If msgError.ToString = "" Then
                For Each ci As DataRow In MsKepemilikan.L_MsKepemilikanNCBS_ApprovalDetail.Rows
                    'validasi NCBS
                    ValidateMsKepemilikanNCBS_ApprovalDetail(ci, msgError, errorline)
                Next
            End If
            temp.Add(msgError.ToString)
            temp.Add(errorline.ToString)
        Catch ex As Exception
            msgError.Append(ex.Message)
            temp.Add(msgError.ToString)
            temp.Add(errorline.ToString)
        End Try
        Return temp
    End Function

    Private Shared Sub ValidateMsKepemilikanNCBS_ApprovalDetail(ByVal DTR As DataRow, ByVal msgError As StringBuilder, ByVal errorLine As StringBuilder)
        Try

            '======================== Validasi textbox :  IDKepemilikanNCBS canot Null (#)====================================================
            If ObjectAntiNull(DTR("IDKepemilikanBank")) = False Then
                msgError.AppendLine(" &bull; IDKepemilikanBank must be filled <BR/>")
                errorLine.AppendLine(" &bull; MsKepemilikanBank : Line " & DTR("NO") & "<BR/>")
            End If

            '======================== Validasi textbox :  NamaKepemilikanNCBS canot Null (#)====================================================
            If ObjectAntiNull(DTR("NamaKepemilikanBank")) = False Then
                msgError.AppendLine(" &bull; NamaKepemilikanBank must be filled <BR/>")
                errorLine.AppendLine(" &bull; MsKepemilikanBank : Line " & DTR("NO") & "<BR/>")
            End If

        Catch ex As Exception
            msgError.AppendLine(" &bull;" & ex.Message & "<BR/>")
            errorLine.AppendLine(" &bull;MsKepemilikanBank : Line " & DTR("NO") & "<BR/>")
        End Try
    End Sub

    Private Shared Sub ValidateMsKepemilikan(ByVal DTR As DataRow, ByVal msgError As StringBuilder, ByVal errorline As StringBuilder)
        Try

            Using checkBlocking As TList(Of MsKepemilikan_ApprovalDetail) = DataRepository.MsKepemilikan_ApprovalDetailProvider.GetPaged("IDKepemilikan=" & DTR("IDKepemilikan"), "", 0, 1, Nothing)
                If checkBlocking.Count > 0 Then
                    msgError.AppendLine(" &bull;MsKepemilikan is waiting for approval <BR/>")
                    errorline.AppendLine(" &bull;MsKepemilikan : Line " & DTR("NO") & "<BR/>")
                End If
            End Using

            '======================== Validasi textbox :  IDKepemilikan   canot Null (#)====================================================
            If ObjectAntiNull(DTR("IDKepemilikan")) = False Then
                msgError.AppendLine(" &bull; ID must be filled <BR/>")
                errorline.AppendLine(" &bull; MsKepemilikan : Line " & DTR("NO") & "<BR/>")
            End If
            If ObjectAntiNull(DTR("IDKepemilikan")) Then
                If objectNumOnly(DTR("IDKepemilikan")) = False Then
                    msgError.AppendLine(" &bull; ID must be Numeric <BR/>")
                    errorline.AppendLine(" &bull; MsKepemilikan : Line " & DTR("NO") & "<BR/>")
                End If
            End If
            '======================== Validasi textbox :  NamaKepemilikan   canot Null (#)====================================================
            If ObjectAntiNull(DTR("NamaKepemilikan")) = False Then
                msgError.AppendLine(" &bull; Nama must be filled <BR/>")
                errorline.AppendLine(" &bull; MsKepemilikan : Line " & DTR("NO") & "<BR/>")
            End If

        Catch ex As Exception
            msgError.AppendLine(" &bull;" & ex.Message & "<BR/>")
            errorline.AppendLine(" &bull;MsKepemilikan  : Line " & DTR("NO") & "<BR/>")
            LogError(ex)
        End Try
    End Sub

#End Region

#Region "Upload File"

    Public Function SaveFileImportToSave() As String
        Dim filePath As String
        Dim FileNameToGetData As String = ""
        Try
            If FileUploadKPI.PostedFile.ContentLength = 0 Then
                Return FileNameToGetData
            End If

            If Not FileUploadKPI.HasFile Then
                Return FileNameToGetData
            End If

            filePath = Path.Combine(Server.MapPath("Upload"), FileUploadKPI.FileName)

            FileNameToGetData = FileUploadKPI.FileName
            If System.IO.File.Exists(filePath) Then
                Dim bexist As Boolean = True
                Dim i As Integer = 1
                While bexist
                    filePath = Path.Combine(Server.MapPath("Upload"), FileUploadKPI.FileName.Split(CChar("."))(0) & "-" & i.ToString & "." & FileUploadKPI.FileName.Split(CChar("."))(1))
                    FileNameToGetData = FileUploadKPI.FileName.Split(CChar("."))(0) & "-" & i.ToString & "." & Me.FileUploadKPI.FileName.Split(CChar("."))(1)
                    If Not System.IO.File.Exists(filePath) Then
                        bexist = False
                        Exit While
                    End If
                    i = i + 1
                End While
            End If
            Me.FileUploadKPI.PostedFile.SaveAs(filePath)
            Session("Filepath") = filePath
            Return FileNameToGetData
        Catch
            Throw
        End Try
    End Function

    ReadOnly Property FilePath() As String
        Get
            Return Session("FilePath")
        End Get
    End Property

    Private Sub ProcessExcelFile(ByVal FullPathFileNameToExportExcel As String)

        Using stream As FileStream = File.Open(FullPathFileNameToExportExcel, FileMode.Open, FileAccess.Read)
            Using excelReader As IExcelDataReader = ExcelReaderFactory.CreateBinaryReader(stream)
                excelReader.IsFirstRowAsColumnNames = True
                DSexcelTemp = excelReader.AsDataSet()
                If DSexcelTemp.Tables("MsKepemilikan").Columns(0).ColumnName Like "*Column*" Then
                    DSexcelTemp.Tables("MsKepemilikan").Rows.Clear()
                End If

            End Using
        End Using

    End Sub


#End Region

#Region "Function..."

    Function getErrorNo(ByVal Id As String, ByVal nama As String) As String
        Dim temp As String = ""
        Try
            Dim DV As New DataView(DT_AllMsKepemilikanNCBS_ApprovalDetail)
            'jika Null maka harus is null bukan kosong disini belum di handle
            DV.RowFilter = "IDKepemilikan = '" & Id & "' and NamaKepemilikan = '" & nama & "'"

            Dim DT As Data.DataTable = DV.ToTable
            If DT.Rows.Count > 0 Then
                temp = Safe(DT.Rows(0)("NO"))
            End If
        Catch ex As Exception
            temp = ex.Message
        End Try

        Return temp
    End Function
    ''' <summary>
    ''' Merubah excel ke Object Databale sekaligus memecahnya
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ImportExcelTempToDTproperty()
        Dim DT As New Data.DataTable
        'DT_MsKepemilikan = DSexcelTemp.Tables("MsKepemilikan")
        DT = DSexcelTemp.Tables("MsKepemilikan")
        Dim DV As New DataView(DT)
        DT_AllMsKepemilikan = DV.ToTable(True, "IDKepemilikan", "NamaKepemilikan", "activation")
        DT_AllMsKepemilikanNCBS_ApprovalDetail = DSexcelTemp.Tables("MsKepemilikan")
        DT_AllMsKepemilikan.Columns.Add("NO")
        For Each i As DataRow In DT_AllMsKepemilikan.Rows
            Dim PK As String = Safe(i("IDKepemilikan"))
            Dim nama As String = Safe(i("NamaKepemilikan"))
            i("NO") = getErrorNo(PK, nama)
        Next
    End Sub

    Private Sub ClearThisPageSessions()
        DT_AllMsKepemilikan = Nothing
        DT_AllMsKepemilikanNCBS_ApprovalDetail = Nothing
        GroupTableSuccessData = Nothing
        GroupTableAnomalyData = Nothing
        DSexcelTemp = Nothing
        SetnGetDataTableForAnomalyData = Nothing
        SetnGetDataTableForSuccessData = Nothing
    End Sub


    Function ConvertDTtoMsKepemilikanNCBS_ApprovalDetail(ByVal DT As Data.DataTable) As TList(Of MsKepemilikanNCBS_ApprovalDetail)
        Dim temp As New TList(Of MsKepemilikanNCBS_ApprovalDetail)
        For Each DTR As DataRow In DT.Rows
            Using MsKepemilikanNCBS_ApprovalDetail As New MsKepemilikanNCBS_ApprovalDetail
                With MsKepemilikanNCBS_ApprovalDetail
                    FillOrNothing(.IDKepemilikanNCBS, DTR("IDKepemilikanBank"), True, oInt)
                    FillOrNothing(.NamaKepemilikanNCBS, DTR("NamaKepemilikanBank"), True, oString)
                    FillOrNothing(.Activation, 1, True, oBit)
                    FillOrNothing(.CreatedBy, GET_UserName, True, oString)
                    FillOrNothing(.CreatedDate, Now, True, oDate)
                    temp.Add(MsKepemilikanNCBS_ApprovalDetail)
                End With
            End Using

        Next

        Return temp
    End Function



    Function ConvertDTtoMsKepemilikan(ByVal DTR As DataRow) As MsKepemilikan_ApprovalDetail
        Dim temp As New MsKepemilikan_ApprovalDetail
        Using MsKepemilikan As New MsKepemilikan_ApprovalDetail()
            With MsKepemilikan
                FillOrNothing(.IDKepemilikan, DTR("IDKepemilikan"), True, oInt)
                FillOrNothing(.NamaKepemilikan, DTR("NamaKepemilikan"), True, Ovarchar)

                FillOrNothing(.Activation, DTR("Activation"), True, oBit)
                FillOrNothing(.CreatedBy, GET_UserName, True, oString)
                FillOrNothing(.CreatedDate, Now, True, oDate)
            End With
            temp = MsKepemilikan
        End Using

        Return temp
    End Function

    Function ConvertDTtoMsKepemilikan(ByVal DT As Data.DataTable) As TList(Of MsKepemilikan_ApprovalDetail)
        Dim temp As New TList(Of MsKepemilikan_ApprovalDetail)
        For Each DTR As DataRow In DT.Rows
            Using MsKepemilikan As New MsKepemilikan_ApprovalDetail()
                With MsKepemilikan
                    FillOrNothing(.IDKepemilikan, DTR("IDKepemilikan"), True, oInt)
                    FillOrNothing(.NamaKepemilikan, DTR("NamaKepemilikan"), True, Ovarchar)

                    FillOrNothing(.Activation, DTR("Activation"), True, oBit)
                    FillOrNothing(.CreatedBy, GET_UserName, True, oString)
                    FillOrNothing(.CreatedDate, Now, True, oDate)
                    temp.Add(MsKepemilikan)
                End With
            End Using
        Next
        Return temp
    End Function

    Private Sub ChangeMultiView(ByVal index As Integer)
        MultiViewKPIOvertimeUpload.ActiveViewIndex = index
        If index = 0 Then
            Imgbtnsave.Visible = False
            Imgbtncancel.Visible = False
        ElseIf index = 1 Then
            Imgbtncancel.Visible = True
        ElseIf index = 2 Then
            Imgbtncancel.Visible = False
            Imgbtnsave.Visible = False
        End If
    End Sub

    Private Function GenerateGroupingMsKepemilikan() As List(Of AMLBLL.MsKepemilikanBll.DT_Group_MsKepemilikan)
        Dim Ltemp As New List(Of AMLBLL.MsKepemilikanBll.DT_Group_MsKepemilikan)
        For i As Integer = 0 To DT_AllMsKepemilikan.Rows.Count - 1
            Dim MsKepemilikan As New AMLBLL.MsKepemilikanBll.DT_Group_MsKepemilikan
            With MsKepemilikan
                .MsKepemilikan = DT_AllMsKepemilikan.Rows(i)
                .L_MsKepemilikanNCBS_ApprovalDetail = DT_AllMsKepemilikanNCBS_ApprovalDetail.Clone

                'Insert MsKepemilikanNCBS_ApprovalDetail
                Dim MsKepemilikan_ID As String = Safe(.MsKepemilikan("IDKepemilikan"))
                Dim MsKepemilikan_nama As String = Safe(.MsKepemilikan("NamaKepemilikan"))
                Dim MsKepemilikan_Activation As String = Safe(.MsKepemilikan("Activation"))
                If DT_AllMsKepemilikanNCBS_ApprovalDetail.Rows.Count > 0 Then
                    Using DV As New DataView(DT_AllMsKepemilikanNCBS_ApprovalDetail)
                        DV.RowFilter = "IDKepemilikan='" & MsKepemilikan_ID & "' and " & _
                                                  "NamaKepemilikan='" & MsKepemilikan_nama & "' and " & _
                                                  "activation='" & MsKepemilikan_Activation & "'"
                        If DV.Count > 0 Then
                            Dim temp As Data.DataTable = DV.ToTable
                            For Each H As DataRow In temp.Rows
                                .L_MsKepemilikanNCBS_ApprovalDetail.ImportRow(H)
                            Next
                        End If
                    End Using
                End If




            End With
            Ltemp.Add(MsKepemilikan)
        Next
        Return Ltemp
    End Function

    Public Sub BindGrid()
        lblValueFailedData.Text = SetnGetDataTableForAnomalyData.Rows.Count
        GridAnomalyList.DataSource = SetnGetDataTableForAnomalyData
        GridAnomalyList.DataBind()
        lblValueSuccessData.Text = SetnGetDataTableForSuccessData.Count
        GridSuccessList.DataSource = SetnGetDataTableForSuccessData
        lblValueuploadeddata.Text = DT_AllMsKepemilikan.Rows.Count
        GridSuccessList.DataBind()
    End Sub

#End Region

#Region "events..."


    Protected Sub ImgBtnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imgbtncancel.Click
        ClearThisPageSessions()
        ChangeMultiView(0)
        LinkButtonExportExcel.Visible = True
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Try
            If IsPostBack Then
                Return
            End If

            ClearThisPageSessions()
            SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
            ChangeMultiView(0)
        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub


    Protected Sub ImgBtnAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAdd.Click
        ChangeMultiView(0)
    End Sub

    Protected Sub GridAnomalyList_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles GridAnomalyList.PageIndexChanged
        GridAnomalyList.CurrentPageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub GridSuccessList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridSuccessList.ItemDataBound
        Try
            Dim CollCount As Integer = e.Item.Cells.Count - 1
            If e.Item.ItemType <> ListItemType.Footer And e.Item.ItemType <> ListItemType.Header Then

                'Show mapping item
                Dim StringMaping As New StringBuilder
                Dim DV As New DataView(DT_AllMsKepemilikanNCBS_ApprovalDetail)
                Dim DT As New Data.DataTable
                DV.RowFilter = "IDKepemilikan='" & e.Item.Cells(0).Text & "' and " & "NamaKepemilikan='" & e.Item.Cells(1).Text & "'"
                DT = DV.ToTable
                If DT.Rows.Count > 0 Then
                    Dim countField As Integer = 7
                    Dim lengthField As Integer = 0
                    For Each idx As DataRow In DT.Rows
                        With idx
                            If lengthField > countField Then
                                StringMaping.AppendLine(idx("NamaKepemilikanBank") & "(" & idx("IDKepemilikanBank") & ");")
                                lengthField = 0
                            Else
                                StringMaping.AppendLine(idx("NamaKepemilikanBank") & "(" & idx("IDKepemilikanBank") & ");")
                                lengthField += 1
                            End If

                        End With
                    Next
                Else
                    StringMaping.Append("-")
                End If
                e.Item.Cells(CollCount).Text = StringMaping.ToString

            End If
        Catch ex As Exception
            LogError(ex)
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub GridSuccessList_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles GridSuccessList.PageIndexChanged
        Dim GridTarget As DataGrid = CType(source, DataGrid)
        Try
            GridSuccessList.CurrentPageIndex = e.NewPageIndex
            BindGrid()
        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub
    Protected Sub imgSaveSuccessData_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imgbtnsave.Click
        Try
            'File belum diupload
            If DT_AllMsKepemilikan Is Nothing Then
                Throw New Exception("Load data failed")
            End If

            'Save MsKepemilikan Approval (Header)
            Dim KeyApp As String
            Using Aproval As New MsKepemilikan_Approval()
                With Aproval
                    .RequestedBy = GET_PKUserID
                    .RequestedDate = Date.Now
                    .FK_MsMode_Id = 2 'Edit
                    .IsUpload = True
                End With
                DataRepository.MsKepemilikan_ApprovalProvider.Save(Aproval)
                'ambil key approval
                KeyApp = Aproval.PK_MsKepemilikan_Approval_Id
            End Using

            For k As Integer = 0 To GroupTableSuccessData.Count - 1

                Dim MsKepemilikan As GrouP_MsKepemilikan_success = GroupTableSuccessData(k)


                With MsKepemilikan
                    'save MsKepemilikan approval detil
                    .MsKepemilikan.FK_MsKepemilikan_Approval_Id = KeyApp
                    DataRepository.MsKepemilikan_ApprovalDetailProvider.Save(.MsKepemilikan)
                    'ambil key MsKepemilikan approval detil
                    Dim IdMsKepemilikan As String = .MsKepemilikan.IDKepemilikan
                    Dim L_MappingMsKepemilikanNCBSPPATK_Approval_Detail As New TList(Of MappingMsKepemilikanNCBSPPATK_Approval_Detail)
                    'save mapping dan NCBS
                    For Each OMsKepemilikanNCBS_ApprovalDetail_ApprovalDetail As MsKepemilikanNCBS_ApprovalDetail In .L_MsKepemilikanNCBS_ApprovalDetail
                        OMsKepemilikanNCBS_ApprovalDetail_ApprovalDetail.PK_MsKepemilikan_Approval_Id = KeyApp
                        Using OMappingMsKepemilikanNCBSPPATK_Approval_Detail As New MappingMsKepemilikanNCBSPPATK_Approval_Detail
                            With OMappingMsKepemilikanNCBSPPATK_Approval_Detail
                                FillOrNothing(.IDKepemilikanNCBS, OMsKepemilikanNCBS_ApprovalDetail_ApprovalDetail.IDKepemilikanNCBS, True, oInt)
                                FillOrNothing(.IDKepemilikan, IdMsKepemilikan, True, oInt)
                                FillOrNothing(.PK_MsKepemilikan_Approval_Id, KeyApp, True, oInt)
                                FillOrNothing(.Nama, OMsKepemilikanNCBS_ApprovalDetail_ApprovalDetail.NamaKepemilikanNCBS, True, oString)
                            End With
                            L_MappingMsKepemilikanNCBSPPATK_Approval_Detail.Add(OMappingMsKepemilikanNCBSPPATK_Approval_Detail)
                        End Using
                    Next
                    'save maping dan NCBS
                    DataRepository.MappingMsKepemilikanNCBSPPATK_Approval_DetailProvider.Save(L_MappingMsKepemilikanNCBSPPATK_Approval_Detail)
                    DataRepository.MsKepemilikanNCBS_ApprovalDetailProvider.Save(.L_MsKepemilikanNCBS_ApprovalDetail)

                End With
            Next

            LblConfirmation.Text = "MsKepemilikan data has edited (" & SetnGetDataTableForSuccessData.Count & " Rows / MsKepemilikan)"
            ChangeMultiView(2)

        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgUpload_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgUpload.Click
        Dim CombinedFileName As String
        GroupTableSuccessData = New List(Of GrouP_MsKepemilikan_success)
        GroupTableAnomalyData = New List(Of AMLBLL.MsKepemilikanBll.DT_Group_MsKepemilikan)
        Try
            'validasi File
            If FileUploadKPI.FileName = "" Then
                Throw New Exception("Data required")
            End If

            If Path.GetExtension(FileUploadKPI.FileName) <> ".xls" Then
                Throw New Exception("Invalid File Upload Extension")
            End If

            CombinedFileName = SaveFileImportToSave()
            If String.Compare(CombinedFileName, "", False) <> 0 Then
                Try
                    ProcessExcelFile(FilePath)
                Catch ex As Exception
                    Throw New Exception("Invalid File Format")
                End Try
            Else
                Throw New Exception("Invalid File Format")
            End If

            'Import from Excell to property Global
            ImportExcelTempToDTproperty()

            Dim LG_MsKepemilikan As List(Of AMLBLL.MsKepemilikanBll.DT_Group_MsKepemilikan) = GenerateGroupingMsKepemilikan()
            'tambahkan kolom untuk tampilan di grid anomaly
            DT_AllMsKepemilikan.Columns.Add("Description")
            DT_AllMsKepemilikan.Columns.Add("errorline")

            'Membuat tempory untuk yang anomali dan yang sukses
            Dim DTnormal As Data.DataTable = DT_AllMsKepemilikan.Clone
            Dim DTAnomaly As Data.DataTable = DT_AllMsKepemilikan.Clone

            'proses pemilahan data anomaly dan data sukses melalui validasi 
            For i As Integer = 0 To LG_MsKepemilikan.Count - 1
                Dim Lerror As List(Of String) = ValidateInput(LG_MsKepemilikan(i))
                Dim MSG As String = Lerror(0)
                Dim LINE As String = Lerror(1)
                If MSG = "" Then
                    DTnormal.ImportRow(LG_MsKepemilikan(i).MsKepemilikan)
                    Dim GroupMsKepemilikan As New GrouP_MsKepemilikan_success
                    With GroupMsKepemilikan
                        'konversi dari datatable ke Object Nettier
                        .MsKepemilikan = ConvertDTtoMsKepemilikan(LG_MsKepemilikan(i).MsKepemilikan)
                        .L_MsKepemilikanNCBS_ApprovalDetail = ConvertDTtoMsKepemilikanNCBS_ApprovalDetail(LG_MsKepemilikan(i).L_MsKepemilikanNCBS_ApprovalDetail)
                    End With
                    'masukan ke group sukses
                    GroupTableSuccessData.Add(GroupMsKepemilikan)
                Else
                    DT_AllMsKepemilikan.Rows(i)("Description") = MSG
                    DT_AllMsKepemilikan.Rows(i)("errorline") = LINE
                    DTAnomaly.ImportRow(DT_AllMsKepemilikan.Rows(i))
                    GroupTableAnomalyData.Add(LG_MsKepemilikan(i))
                End If

            Next

            SetnGetDataTableForSuccessData = ConvertDTtoMsKepemilikan(DTnormal)
            SetnGetDataTableForAnomalyData = DTAnomaly

            If DTAnomaly.Rows.Count > 0 Then
                Imgbtnsave.Visible = False
                Imgbtncancel.Visible = True
            Else
                Imgbtnsave.Visible = True
                Imgbtncancel.Visible = True
            End If

            BindGrid()
            If GridAnomalyList.Items.Count < 1 Then
                LinkButtonExportExcel.Visible = False
            Else
                LinkButtonExportExcel.Visible = True
            End If
            ChangeMultiView(1)
        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

#End Region

    Protected Sub LinkButtonExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButtonExportExcel.Click
        Try
            Dim Exp As New AMLBLL.MsKepemilikanBll
            Dim FolderPath As String = Server.MapPath("FolderTemplate")

            Dim buffer As Byte() = Exp.ExportDTtoExcel(GroupTableAnomalyData, FolderPath)
            Response.AddHeader("Content-type", "application/vnd.ms-excel")
            Response.AddHeader("Content-Disposition", "attachment; filename=MsKepemilikanUpload.xls")
            Response.BinaryWrite(buffer)
            Response.Flush()
            Response.End()

        Catch ex As Exception
            cvalPageErr.IsValid = False
            cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

        ' Verifies that the control is rendered 

    End Sub

    Protected Sub GridSuccessList_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles GridSuccessList.SelectedIndexChanged

    End Sub
End Class






