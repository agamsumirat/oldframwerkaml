Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Sahassa.AML.TableAdapterHelper


Partial Class GroupManagementApprovalDetail
    Inherits Parent

#Region " Property "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get confirm type
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property ParamType() As Sahassa.AML.Commonly.TypeConfirm
        Get
            Return Me.Request.Params("TypeConfirm")
        End Get
    End Property

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' get param pk Group management
    ''' </summary>
    ''' <value></value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private ReadOnly Property ParamPkGroupManagement() As Int64
        Get
            Return Me.Request.Params("GroupID")
        End Get
    End Property

    ''' <summary>
    ''' get user id
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property GetUserID() As String
        Get
            Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.Group_PendingApprovalTableAdapter
                Return AccessPending.SelectGroup_PendingApprovalUserID(Me.ParamPkGroupManagement)
            End Using
        End Get
    End Property
#End Region

#Region " Load "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' hide all
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub HideAll()
        Dim StrId As String = ""
        Select Case Me.ParamType
            Case Sahassa.AML.Commonly.TypeConfirm.GroupAdd
                StrId = "UserAdd"
            Case Sahassa.AML.Commonly.TypeConfirm.GroupEdit
                StrId = "UserEdi"
            Case Sahassa.AML.Commonly.TypeConfirm.GroupDelete
                StrId = "UserDel"
            Case Else
                Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
        End Select
        For Each ObjRow As HtmlControls.HtmlTableRow In Me.TableDetail.Rows
            ObjRow.Visible = ObjRow.ID = "Button11" Or ObjRow.ID.Substring(0, 7) = StrId
        Next
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load Group add
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub LoadGroupAdd()
        Me.LabelTitle.Text = "Activity: Add Group"
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.Group_ApprovalTableAdapter
            Using ObjTable As Data.DataTable = AccessPending.GetGroupApprovalData(Me.ParamPkGroupManagement)
                'Bila ObjTable.Rows.Count > 0 berarti Group tsb masih ada dlm tabel Group_Approval
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.Group_ApprovalRow = ObjTable.Rows(0)
                    Me.LabelGroupNameAdd.Text = rowData.GroupName
                    Me.TextGroupDescriptionAdd.Text = rowData.GroupDescription
                Else 'Bila ObjTable.Rows.Count = 0 berarti Group tsb sudah tidak lagi berada dlm tabel Group_Approval
                    Throw New Exception("Cannot load data from the following Group: " & Me.LabelGroupNameAdd.Text & " because that Group is no longer in the approval table.")
                End If
            End Using
        End Using
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load Group edit
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub LoadGroupEdit()
        Me.LabelTitle.Text = "Activity: Edit Group"
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.Group_ApprovalTableAdapter
            Using ObjTable As Data.DataTable = AccessPending.GetGroupApprovalData(Me.ParamPkGroupManagement)
                'Bila ObjTable.Rows.Count > 0 berarti Group tsb masih ada dlm tabel Group_Approval
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.Group_ApprovalRow = ObjTable.Rows(0)
                    Me.LabelGroupIDEditNewGroupID.Text = rowData.GroupID
                    Me.LabelGroupEditNewGroupName.Text = rowData.GroupName
                    Me.TextGroupEditNewGroupDescription.Text = rowData.GroupDescription
                    Me.LabelGroupIDEditOldGroupID.Text = rowData.GroupID_Old
                    Me.LabelGroupNameEditOldGroupName.Text = rowData.GroupName_Old
                    Me.TextGroupDescriptionEditOldGroupDescription.Text = rowData.GroupDescription_Old
                Else 'Bila ObjTable.Rows.Count = 0 berarti Group tsb sudah tidak lagi berada dlm tabel Group_Approval
                    Throw New Exception("Cannot load data from the following Group: " & Me.LabelGroupNameEditOldGroupName.Text & " because that Group is no longer in the approval table.")
                End If
            End Using
        End Using
    End Sub
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' load Group delete
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub LoadGroupDelete()
        Me.LabelTitle.Text = "Activity: Delete Group"
        Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.Group_ApprovalTableAdapter
            Using ObjTable As Data.DataTable = AccessPending.GetGroupApprovalData(Me.ParamPkGroupManagement)
                'Bila ObjTable.Rows.Count > 0 berarti Group tsb masih ada dlm tabel Group_Approval
                If ObjTable.Rows.Count > 0 Then
                    Dim rowData As AMLDAL.AMLDataSet.Group_ApprovalRow = ObjTable.Rows(0)
                    Me.LabelGroupDeleteGroupID.Text = rowData.GroupID
                    Me.LabelGroupDeleteGroupName.Text = rowData.GroupName
                    Me.TextGroupDeleteGroupDescription.Text = rowData.GroupDescription
                Else 'Bila ObjTable.Rows.Count = 0 berarti Group tsb sudah tidak lagi berada dlm tabel Group_Approval
                    Throw New Exception("Cannot load data from the following Group: " & Me.LabelGroupDeleteGroupName.Text & " because that Group is no longer in the approval table.")
                End If
            End Using
        End Using
    End Sub
#End Region

#Region " Accept "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' Group add accept
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptGroupAdd()
        Dim oSQLTrans As SqlTransaction = Nothing

        Try
            'Using TranScope As New Transactions.TransactionScope
            Using AccessGroup As New AMLDAL.AMLDataSetTableAdapters.GroupTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessGroup, Data.IsolationLevel.ReadUncommitted)
                Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.Group_ApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, oSQLTrans)
                    Dim ObjTable As Data.DataTable = AccessPending.GetGroupApprovalData(Me.ParamPkGroupManagement)

                    'Bila ObjTable.Rows.Count > 0 berarti Group tsb masih ada dlm tabel Group_Approval
                    If ObjTable.Rows.Count > 0 Then
                        Dim rowData As AMLDAL.AMLDataSet.Group_ApprovalRow = ObjTable.Rows(0)

                        'Periksa apakah GroupName yg baru tsb sudah ada dlm tabel Group atau belum. 
                        Dim counter As Int32 = AccessGroup.CountMatchingGroup(rowData.GroupName)

                        'Bila counter = 0 berarti GroupName tsb belum ada dlm tabel Group, maka boleh ditambahkan
                        If counter = 0 Then
                            'tambahkan item tersebut dalam tabel Group
                            AccessGroup.Insert(rowData.GroupName, rowData.GroupDescription, rowData.GroupCreatedDate, rowData.isCreatedBySU)
                            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(3)
                            'catat aktifitas dalam tabel Audit Trail
                            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Group", "GroupName", "Add", "", rowData.GroupName, "Accepted")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Group", "GroupDescription", "Add", "", rowData.GroupDescription, "Accepted")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Group", "GroupCreatedDate", "Add", "", rowData.GroupCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")

                                'hapus item tersebut dalam tabel Group_Approval
                                AccessPending.DeleteGroupApproval(rowData.Group_PendingApprovalID)

                                'hapus item tersebut dalam tabel Group_PendingApproval
                                Using AccessPendingGroup As New AMLDAL.AMLDataSetTableAdapters.Group_PendingApprovalTableAdapter
                                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPendingGroup, oSQLTrans)
                                    AccessPendingGroup.DeleteGroupPendingApproval(rowData.Group_PendingApprovalID)
                                End Using

                                oSQLTrans.Commit()
                            End Using
                        Else 'Bila counter != 0 berarti GroupName tsb sudah ada dlm tabel Group, maka AcceptGroupAdd gagal
                            Throw New Exception("Cannot add the following Group: " & rowData.GroupName & " because that Group Name already exists in the database.")
                        End If
                    Else 'Bila ObjTable.Rows.Count = 0 berarti Group tsb sudah tidak lagi berada dlm tabel Group_Approval
                        Throw New Exception("Cannot add the following Group: " & Me.LabelGroupNameAdd.Text & " because that Group is no longer in the approval table.")
                    End If
                End Using
            End Using
            'End Using
        Catch
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()

            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If

        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' Group edit accept
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptGroupEdit()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            'Using TranScope As New Transactions.TransactionScope
            Using AccessGroup As New AMLDAL.AMLDataSetTableAdapters.GroupTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessGroup, Data.IsolationLevel.ReadUncommitted)
                Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.Group_ApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, oSQLTrans)
                    Dim ObjTable As Data.DataTable = AccessPending.GetGroupApprovalData(Me.ParamPkGroupManagement)

                    'Bila ObjTable.Rows.Count > 0 berarti User tsb masih ada dlm tabel Group_Approval
                    If ObjTable.Rows.Count > 0 Then
                        Dim rowData As AMLDAL.AMLDataSet.Group_ApprovalRow = ObjTable.Rows(0)

                        'Periksa apakah GroupName yg baru tsb sudah ada dlm tabel Group atau belum. 
                        Dim counter As Int32 = AccessGroup.CountMatchingGroup(rowData.GroupName)

                        'Bila tidak ada perubahan GroupName
                        If rowData.GroupName = rowData.GroupName_Old Then
                            GoTo Edit
                        Else 'Bila ada perubahan GroupName
                            'Counter = 0 berarti GroupName tersebut tidak ada dalam tabel Group
                            If counter = 0 Then
Edit:
                                'update item tersebut dalam tabel Group
                                AccessGroup.UpdateGroup(rowData.GroupID, rowData.GroupName, rowData.GroupDescription, rowData.isCreatedBySU)

                                Sahassa.AML.AuditTrailAlert.AuditTrailChecking(4)
                                'catat aktifitas dalam tabel Audit Trail
                                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Group", "GroupName", "Edit", rowData.GroupName_Old, rowData.GroupName, "Accepted")
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Group", "GroupDescription", "Edit", rowData.GroupDescription_Old, rowData.GroupDescription, "Accepted")
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Group", "GroupID", "Edit", rowData.GroupID_Old, rowData.GroupID, "Accepted")
                                    AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Group", "GroupCreatedDate", "Edit", rowData.GroupCreatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), rowData.GroupCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")

                                    'hapus item tersebut dalam tabel Group_Approval
                                    AccessPending.DeleteGroupApproval(rowData.Group_PendingApprovalID)

                                    'hapus item tersebut dalam tabel Group_PendingApproval
                                    Using AccessPendingGroup As New AMLDAL.AMLDataSetTableAdapters.Group_PendingApprovalTableAdapter
                                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPendingGroup, oSQLTrans)
                                        AccessPendingGroup.DeleteGroupPendingApproval(rowData.Group_PendingApprovalID)
                                    End Using
                                    oSQLTrans.Commit()
                                End Using
                            Else 'Bila counter != 0 berarti GroupName tsb telah ada dlm tabel Group, maka AcceptGroupEdit gagal
                                Throw New Exception("Cannot change to the following Group Name: " & rowData.GroupName & " because that Group Name already exists in the database.")
                            End If
                        End If
                    Else 'Bila ObjTable.Rows.Count = 0 berarti Group tsb sudah tidak lagi berada dlm tabel Group_Approval
                        Throw New Exception("Cannot edit the following Group: " & Me.LabelGroupNameEditOldGroupName.Text & " because that Group is no longer in the approval table.")
                    End If
                End Using
            End Using
            'End Using
        Catch
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' Group delete accept
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub AcceptGroupDelete()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            'Using TranScope As New Transactions.TransactionScope
            Using AccessGroup As New AMLDAL.AMLDataSetTableAdapters.GroupTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessGroup, Data.IsolationLevel.ReadUncommitted)
                Using AccessPending As New AMLDAL.AMLDataSetTableAdapters.Group_ApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPending, oSQLTrans)
                    Dim ObjTable As Data.DataTable = AccessPending.GetGroupApprovalData(Me.ParamPkGroupManagement)

                    'Bila ObjTable.Rows.Count > 0 berarti Group tsb masih ada dlm tabel Group_Approval
                    If ObjTable.Rows.Count > 0 Then
                        Dim rowData As AMLDAL.AMLDataSet.Group_ApprovalRow = ObjTable.Rows(0)

                        'Periksa apakah Group yg hendak didelete tsb ada dlm tabelnya atau tidak. 
                        Dim counter As Int32 = AccessGroup.CountMatchingGroup(rowData.GroupName)

                        'Bila counter = 0 berarti Group tsb tidak ada dlm tabel Group, maka AcceptGroupDelete gagal
                        If counter = 0 Then
                            Throw New Exception("Cannot delete the following Group: " & rowData.GroupName & " because that Group does not exist in the database anymore.")
                        Else 'Bila counter != 0, maka Group tsb bisa didelete
                            'hapus item tersebut dari tabel Group
                            AccessGroup.DeleteGroup(rowData.GroupID)

                            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(4)
                            'catat aktifitas dalam tabel Audit Trail
                            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                                Sahassa.AML.TableAdapterHelper.SetTransaction(AccessAudit, oSQLTrans)
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Group", "GroupName", "Delete", rowData.GroupName_Old, rowData.GroupName, "Accepted")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Group", "GroupDescription", "Delete", rowData.GroupDescription_Old, rowData.GroupDescription, "Accepted")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Group", "GroupID", "Delete", rowData.GroupID_Old, rowData.GroupID, "Accepted")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Group", "GroupCreatedDate", "Delete", rowData.GroupCreatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), rowData.GroupCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Accepted")

                                'hapus item tersebut dalam tabel Group_Approval
                                AccessPending.DeleteGroupApproval(rowData.Group_PendingApprovalID)

                                'hapus item tersebut dalam tabel Group_PendingApproval
                                Using AccessPendingGroup As New AMLDAL.AMLDataSetTableAdapters.Group_PendingApprovalTableAdapter
                                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessPendingGroup, oSQLTrans)
                                    AccessPendingGroup.DeleteGroupPendingApproval(rowData.Group_PendingApprovalID)
                                End Using

                                oSQLTrans.Commit()
                            End Using
                        End If
                    Else 'Bila ObjTable.Rows.Count = 0 berarti Group tsb sudah tidak lagi berada dlm tabel Group_Approval
                        Throw New Exception("Cannot delete the following Group: " & Me.LabelGroupNameAdd.Text & " because that Group is no longer in the approval table.")
                    End If
                End Using
            End Using
            'End Using
        Catch
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub

#End Region

#Region " Reject "
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' reject Group add
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectGroupAdd()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            'Using TranScope As New Transactions.TransactionScope
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAudit, Data.IsolationLevel.ReadUncommitted)
                Using AccessGroup As New AMLDAL.AMLDataSetTableAdapters.Group_ApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessGroup, oSQLTrans)
                    Using AccessGroupPending As New AMLDAL.AMLDataSetTableAdapters.Group_PendingApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessGroupPending, oSQLTrans)
                        Using TableGroup As Data.DataTable = AccessGroup.GetGroupApprovalData(Me.ParamPkGroupManagement)

                            'Bila TableGroup.Rows.Count > 0 berarti Group tsb masih ada dlm tabel Group_Approval
                            If TableGroup.Rows.Count > 0 Then
                                Dim ObjRow As AMLDAL.AMLDataSet.Group_ApprovalRow = TableGroup.Rows(0)

                                Sahassa.AML.AuditTrailAlert.AuditTrailChecking(3)
                                'catat aktifitas dalam tabel Audit Trail
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Group", "GroupName", "Add", "", ObjRow.GroupName, "Rejected")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Group", "GroupDescription", "Add", "", ObjRow.GroupDescription, "Rejected")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Group", "GroupCreatedDate", "Add", "", ObjRow.GroupCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")

                                'hapus item tersebut dalam tabel Group_Approval
                                AccessGroup.DeleteGroupApproval(Me.ParamPkGroupManagement)

                                'hapus item tersebut dalam tabel Group_PendingApproval
                                AccessGroupPending.DeleteGroupPendingApproval(Me.ParamPkGroupManagement)
                                oSQLTrans.Commit()
                            Else 'Bila TableGroup.Rows.Count = 0 berarti Group tsb sudah tidak lagi berada dlm tabel Group_Approval
                                Throw New Exception("Operation failed. The following Group: " & Me.LabelGroupNameAdd.Text & " is no longer in the approval table.")
                            End If
                        End Using
                    End Using
                End Using
            End Using
            'End Using
        Catch
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' reject Group edit
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectGroupEdit()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            'Using TranScope As New Transactions.TransactionScope
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAudit, Data.IsolationLevel.ReadUncommitted)
                Using AccessGroup As New AMLDAL.AMLDataSetTableAdapters.Group_ApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessGroup, oSQLTrans)
                    Using AccessGroupPending As New AMLDAL.AMLDataSetTableAdapters.Group_PendingApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessGroupPending, oSQLTrans)
                        Using TableGroup As Data.DataTable = AccessGroup.GetGroupApprovalData(Me.ParamPkGroupManagement)
                            'Bila TableGroup.Rows.Count > 0 berarti Group tsb masih ada dlm tabel Group_Approval
                            If TableGroup.Rows.Count > 0 Then
                                Dim ObjRow As AMLDAL.AMLDataSet.Group_ApprovalRow = TableGroup.Rows(0)

                                Sahassa.AML.AuditTrailAlert.AuditTrailChecking(4)
                                'catat aktifitas dalam tabel Audit Trail
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Group", "GroupName", "Edit", ObjRow.GroupName_Old, ObjRow.GroupName, "Rejected")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Group", "GroupDescription", "Edit", ObjRow.GroupDescription_Old, ObjRow.GroupDescription, "Rejected")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Group", "GroupID", "Edit", ObjRow.GroupID_Old, ObjRow.GroupID, "Rejected")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Group", "GroupCreatedDate", "Edit", ObjRow.GroupCreatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), ObjRow.GroupCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")

                                'hapus item tersebut dalam tabel Group_Approval
                                AccessGroup.DeleteGroupApproval(Me.ParamPkGroupManagement)

                                'hapus item tersebut dalam tabel Group_PendingApproval
                                AccessGroupPending.DeleteGroupPendingApproval(Me.ParamPkGroupManagement)
                                oSQLTrans.Commit()
                            Else 'Bila TableGroup.Rows.Count = 0 berarti Group tsb sudah tidak lagi berada dlm tabel Group_Approval
                                Throw New Exception("Operation failed. The following Group: " & Me.LabelGroupNameEditOldGroupName.Text & " is no longer in the approval table.")
                            End If
                        End Using
                    End Using
                End Using
            End Using
            'End Using
        Catch
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' reject Group delete
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Ariwibawa]	12/06/2006	Created
    '''     [Hendry]    01/05/2007 Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub RejectGroupDelete()
        Dim oSQLTrans As SqlTransaction = Nothing
        Try
            'Using TranScope As New Transactions.TransactionScope
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                oSQLTrans = Sahassa.AML.TableAdapterHelper.BeginTransaction(AccessAudit, Data.IsolationLevel.ReadUncommitted)
                Using AccessGroup As New AMLDAL.AMLDataSetTableAdapters.Group_ApprovalTableAdapter
                    Sahassa.AML.TableAdapterHelper.SetTransaction(AccessGroup, oSQLTrans)
                    Using AccessGroupPending As New AMLDAL.AMLDataSetTableAdapters.Group_PendingApprovalTableAdapter
                        Sahassa.AML.TableAdapterHelper.SetTransaction(AccessGroupPending, oSQLTrans)
                        Using TableGroup As Data.DataTable = AccessGroup.GetGroupApprovalData(Me.ParamPkGroupManagement)
                            'Bila TableGroup.Rows.Count > 0 berarti Group tsb masih ada dlm tabel Group_Approval
                            If TableGroup.Rows.Count > 0 Then
                                Dim ObjRow As AMLDAL.AMLDataSet.Group_ApprovalRow = TableGroup.Rows(0)

                                Sahassa.AML.AuditTrailAlert.AuditTrailChecking(4)
                                'catat aktifitas dalam tabel Audit Trail
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Group", "GroupName", "Delete", ObjRow.GroupName_Old, ObjRow.GroupName, "Rejected")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Group", "GroupDescription", "Delete", ObjRow.GroupDescription_Old, ObjRow.GroupDescription, "Rejected")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Group", "GroupID", "Delete", ObjRow.GroupID_Old, ObjRow.GroupID, "Rejected")
                                AccessAudit.Insert(Now, Me.GetUserID, Sahassa.AML.Commonly.SessionUserId, "Group", "GroupCreatedDate", "Delete", ObjRow.GroupCreatedDate_Old.ToString("dd-MMMM-yyyy HH:mm"), ObjRow.GroupCreatedDate.ToString("dd-MMMM-yyyy HH:mm"), "Rejected")

                                'hapus item tersebut dalam tabel Group_Approval
                                AccessGroup.DeleteGroupApproval(Me.ParamPkGroupManagement)

                                'hapus item tersebut dalam tabel Group_PendingApproval
                                AccessGroupPending.DeleteGroupPendingApproval(Me.ParamPkGroupManagement)
                                oSQLTrans.Commit()
                            Else 'Bila TableGroup.Rows.Count = 0 berarti Group tsb sudah tidak lagi berada dlm tabel Group_Approval
                                Throw New Exception("Operation failed. The following Group: " & Me.LabelGroupDeleteGroupName.Text & " is no longer in the approval table.")
                            End If
                        End Using
                    End Using
                End Using
            End Using
            'End Using
        Catch
            If Not oSQLTrans Is Nothing Then oSQLTrans.Rollback()
            Throw
        Finally
            If Not oSQLTrans Is Nothing Then
                oSQLTrans.Dispose()
                oSQLTrans = Nothing
            End If
        End Try
    End Sub
#End Region

    ''' <summary>
    ''' page load event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.IsPostBack Then
                Me.ImageAccept.Attributes.Add("onclick", "javascript: if(confirm('Do you want to accept this activity ?')== false) return false;")
                Me.ImageReject.Attributes.Add("onclick", "javascript: if(confirm('Do you want to reject this activity ?')== false) return false;")
                Me.HideAll()
                Select Case Me.ParamType
                    Case Sahassa.AML.Commonly.TypeConfirm.GroupAdd
                        Me.LoadGroupAdd()
                    Case Sahassa.AML.Commonly.TypeConfirm.GroupEdit
                        Me.LoadGroupEdit()
                    Case Sahassa.AML.Commonly.TypeConfirm.GroupDelete
                        Me.LoadGroupDelete()
                    Case Else
                        Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
                End Select

                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)

                'Using Transcope As New Transactions.TransactionScope
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)

                    'Transcope.Complete()
                End Using
                'End Using
            End If
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' accept button handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageAccept_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageAccept.Click
        Try
            Select Case Me.ParamType
                Case Sahassa.AML.Commonly.TypeConfirm.GroupAdd
                    Me.AcceptGroupAdd()
                Case Sahassa.AML.Commonly.TypeConfirm.GroupEdit
                    Me.AcceptGroupEdit()
                Case Sahassa.AML.Commonly.TypeConfirm.GroupDelete
                    Me.AcceptGroupDelete()
                Case Else
                    Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
            End Select

            Sahassa.AML.Commonly.SessionIntendedPage = "GroupManagementApproval.aspx"

            Me.Response.Redirect("GroupManagementApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' reject button handlert
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageReject_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageReject.Click
        Try
            Select Case Me.ParamType
                Case Sahassa.AML.Commonly.TypeConfirm.GroupAdd
                    Me.RejectGroupAdd()
                Case Sahassa.AML.Commonly.TypeConfirm.GroupEdit
                    Me.RejectGroupEdit()
                Case Sahassa.AML.Commonly.TypeConfirm.GroupDelete
                    Me.RejectGroupDelete()
                Case Else
                    Throw New Exception("Type not supported type:" & Me.ParamType.ToString)
            End Select

            Sahassa.AML.Commonly.SessionIntendedPage = "GroupManagementApproval.aspx"

            Me.Response.Redirect("GroupManagementApproval.aspx", False)
        Catch ex As Exception
            Me.cvalPageErr.IsValid = False
            Me.cvalPageErr.ErrorMessage = ex.Message
            LogError(ex)
        End Try
    End Sub

    ''' <summary>
    ''' back button handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ImageBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageBack.Click
        Sahassa.AML.Commonly.SessionIntendedPage = "GroupManagementApproval.aspx"

        Me.Response.Redirect("GroupManagementApproval.aspx", False)
    End Sub
End Class