<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="UserManagementApprovalDetail.aspx.vb" Inherits="UserManagementApprovalDetail" title="User Management Approval Detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cpContent" Runat="Server">
	<table cellSpacing="4" cellPadding="0" width="100%">
		<tr>
			<td vAlign="bottom" align="left"><IMG height="15" src="images/dot_title.gif" width="15"></td>
			<td class="maintitle" vAlign="bottom" width="99%"><asp:label id="LabelTitle" runat="server"></asp:label></td>
		</tr>
	</table>
    <TABLE id="TableDetail" borderColor="#ffffff" cellSpacing="1" cellPadding="2" width="100%"
	    bgColor="#dddddd" border="2" runat="server">
	    <TR class="formText" id="UserAdd1">
		    <TD width="5" bgColor="#ffffff" height="24">&nbsp;</TD>
		    <TD width="20%" bgColor="#ffffff">User ID</TD>
		    <TD width="5" bgColor="#ffffff">:</TD>
		    <TD width="80%" bgColor="#ffffff"><asp:label id="LabelUserIdAdd" runat="server"></asp:label></TD>
	    </TR>
	    <tr class="formText" id="UserAdd2">
		    <td bgcolor="#ffffff" height="24">&nbsp;</td>
		    <td bgcolor="#ffffff">
                User Name</td>
		    <td bgcolor="#ffffff">:</td>
		    <td bgcolor="#ffffff"><asp:label id="LabelUserNameAdd" runat="server"></asp:label></td>
	    </tr>
	    <tr class="formText" id="UserAdd3">
		    <td bgcolor="#ffffff" height="24">&nbsp;</td>
		    <td bgcolor="#ffffff">
                Email Address</td>
		    <td bgcolor="#ffffff">:</td>
		    <td bgcolor="#ffffff"><asp:label id="LabelEmailAddressAdd" runat="server"></asp:label></td>
	    </tr>
	    <tr class="formText" id="UserAdd4">
		    <td bgcolor="#ffffff" height="24">&nbsp;</td>
		    <td bgcolor="#ffffff">Mobile Phone</td>
		    <td bgcolor="#ffffff">:</td>
		    <td bgcolor="#ffffff"><asp:label id="LabelMobilePhoneAdd" runat="server"></asp:label></td>
	    </tr>
	    <tr class="formText" id="UserAdd5">
		    <td bgcolor="#ffffff" height="24">&nbsp;</td>
		    <td bgcolor="#ffffff">Group</td>
		    <td bgcolor="#ffffff">:</td>
		    <td bgcolor="#ffffff"><asp:label id="LabelGroupNameAdd" runat="server"></asp:label></td>
	    </tr>
        <tr class="formText" id="UserAdd6">
			<td bgColor="#ffffff" height="24">&nbsp;</td>
			<td bgColor="#ffffff">Teller ID</td>
			<td bgColor="#ffffff">:</td>
			<td bgcolor="#ffffff"><asp:label id="LabelTellerIDAdd" runat="server"></asp:label></td>
		</tr>
	    <tr class="formText" id="UserEdi1">
		    <td height="24" colspan="4" bgcolor="#ffffcc">
                Old Values</td>
		    <td colspan="4" bgcolor="#ffffcc">&nbsp;New Values</td>
	    </tr>
	    <tr class="formText" id="UserEdi2">
		    <td width="5" bgcolor="#ffffff" height="24">&nbsp;</td>
		    <td width="10%" bgcolor="#ffffff">User ID</td>
		    <td width="5" bgcolor="#ffffff">:</td>
		    <td width="40%" bgcolor="#ffffff"><asp:label id="LabelUserIdOld" runat="server"></asp:label></td>
		    <td width="5" bgcolor="#ffffff" height="24">&nbsp;</td>
		    <td width="10%" bgcolor="#ffffff">User ID</td>
		    <td width="5" bgcolor="#ffffff">:</td>
		    <td width="40%" bgcolor="#ffffff"><asp:label id="LabelUserIdNew" runat="server"></asp:label></td>
	    </tr>
	    <tr class="formText" id="UserEdi3">
		    <td bgcolor="#ffffff">&nbsp;</td>
		    <td bgcolor="#ffffff">
                User Name</td>
		    <td bgcolor="#ffffff">:</td>
		    <td bgcolor="#ffffff"><asp:label id="LabelUserNameOld" runat="server"></asp:label></td>
		    <td bgcolor="#ffffff">&nbsp;</td>
		    <td bgcolor="#ffffff">
                User Name</td>
		    <td bgcolor="#ffffff">:</td>
		    <td bgcolor="#ffffff"><asp:label id="LabelUserNameNew" runat="server"></asp:label></td>
	    </tr>
        <tr class="formText" id="UserEdi4">
		    <td bgcolor="#ffffff">&nbsp;</td>
		    <td bgcolor="#ffffff">
                Email Address</td>
		    <td bgcolor="#ffffff">:</td>
		    <td bgcolor="#ffffff"><asp:label id="LabelEmailAddressOld" runat="server"></asp:label></td>
		    <td bgcolor="#ffffff">&nbsp;</td>
		    <td bgcolor="#ffffff">
                Email Address</td>
		    <td bgcolor="#ffffff">:</td>
		    <td bgcolor="#ffffff"><asp:label id="LabelEmailAddressNew" runat="server"></asp:label></td>
	    </tr>
	    <tr class="formText" id="UserEdi5">
		    <td bgcolor="#ffffff">&nbsp;</td>
		    <td bgcolor="#ffffff">
                Mobile Phone</td>
		    <td bgcolor="#ffffff">:</td>
		    <td bgcolor="#ffffff"><asp:label id="LabelMobilePhoneOld" runat="server"></asp:label></td>
		    <td bgcolor="#ffffff">&nbsp;</td>
		    <td bgcolor="#ffffff">
                Mobile Phone</td>
		    <td bgcolor="#ffffff">:</td>
		    <td bgcolor="#ffffff"><asp:label id="LabelMobilePhoneNew" runat="server"></asp:label></td>
	    </tr>
	    <tr class="formText" id="UserEdi6">
		    <td bgcolor="#ffffff">&nbsp;</td>
		    <td bgcolor="#ffffff">Group</td>
		    <td bgcolor="#ffffff">:</td>
		    <td bgcolor="#ffffff"><asp:label id="LabelGroupNameOld" runat="server"></asp:label></td>
		    <td bgcolor="#ffffff">&nbsp;</td>
		    <td bgcolor="#ffffff">Group</td>
		    <td bgcolor="#ffffff">:</td>
		    <td bgcolor="#ffffff"><asp:label id="LabelGroupNameNew" runat="server"></asp:label></td>
	    </tr>
        <tr class="formText" id="UserEdi7">
			<td bgcolor="#ffffff">&nbsp;</td>
			<td bgColor="#ffffff">Teller ID</td>
			<td bgColor="#ffffff">:</td>
			<td bgcolor="#ffffff"><asp:label id="LabelTellerIDOld" runat="server"></asp:label></td>
            <td bgcolor="#ffffff">&nbsp;</td>
			<td bgColor="#ffffff">Teller ID</td>
			<td bgColor="#ffffff">:</td>
			<td bgcolor="#ffffff"><asp:label id="LabelTellerIDNew" runat="server"></asp:label></td>
		</tr>
	    <TR class="formText" id="UserDel1">
		    <TD width="5" bgColor="#ffffff" style="height: 23px">&nbsp;</TD>
		    <TD width="20%" bgColor="#ffffff" style="height: 23px">User ID</TD>
		    <TD width="5" bgColor="#ffffff" style="height: 23px">:</TD>
		    <TD width="80%" bgColor="#ffffff" style="height: 23px"><asp:label id="LabelUserIdDelete" runat="server"></asp:label></TD>
	    </TR>
	    <tr class="formText" id="UserDel2">
		    <td bgcolor="#ffffff" height="24">&nbsp;</td>
		    <td bgcolor="#ffffff">
                User Name</td>
		    <td bgcolor="#ffffff">:</td>
		    <td bgcolor="#ffffff"><asp:label id="LabelUserNameDelete" runat="server"></asp:label></td>
	    </tr>     
	    <tr class="formText" id="UserDel3">
		    <td bgcolor="#ffffff" height="24">&nbsp;</td>
		    <td bgcolor="#ffffff">
                Email Address</td>
		    <td bgcolor="#ffffff">:</td>
		    <td bgcolor="#ffffff"><asp:label id="LabelEmailAddressDelete" runat="server"></asp:label></td>
	    </tr>
	    <tr class="formText" id="UserDel4">
		    <td bgcolor="#ffffff" height="24">&nbsp;</td>
		    <td bgcolor="#ffffff">Mobile Phone</td>
		    <td bgcolor="#ffffff">:</td>
		    <td bgcolor="#ffffff"><asp:label id="LabelMobilePhoneDelete" runat="server"></asp:label></td>
	    </tr>
	    <tr class="formText" id="UserDel5">
		    <td bgcolor="#ffffff" height="24">&nbsp;</td>
		    <td bgcolor="#ffffff">Group</td>
		    <td bgcolor="#ffffff">:</td>
		    <td bgcolor="#ffffff"><asp:label id="LabelGroupNameDelete" runat="server"></asp:label></td>
	    </tr>
        <tr class="formText" id="UserDel6">
			<td bgColor="#ffffff" height="24">&nbsp;</td>
			<td bgColor="#ffffff">Teller ID</td>
			<td bgColor="#ffffff">:</td>
			<td bgcolor="#ffffff"><asp:label id="LabelTellerIDDelete" runat="server"></asp:label></td>
		</tr>
	    <TR class="formText" id="Button11" bgColor="#dddddd" height="30">
		    <TD width="15"><IMG height="15" src="images/arrow.gif" width="15"></TD>
		    <TD colSpan="7">
			    <TABLE cellSpacing="0" cellPadding="3" border="0">
				    <TR>
					    <TD><asp:imagebutton id="ImageAccept" runat="server" CausesValidation="True" SkinID="AcceptButton"></asp:imagebutton></TD>
					    <TD><asp:imagebutton id="ImageReject" runat="server" CausesValidation="False" SkinID="RejectButton"></asp:imagebutton></TD>
					    <td><asp:imagebutton id="ImageBack" runat="server" CausesValidation="False" SkinID="BackButton"></asp:imagebutton></td>
				    </TR>
			    </TABLE>
                <asp:CustomValidator ID="cvalPageErr" runat="server" Display="none"></asp:CustomValidator></TD>
	    </TR>
    </TABLE>
</asp:Content>