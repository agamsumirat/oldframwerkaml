#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports Sahassa.AML
Imports System.Collections.Generic
#End Region

Partial Class MsPekerjaan_APPROVAL_Detail
	Inherits Parent

#Region "Function"

	Sub HideControl()
		imgReject.Visible = False
		imgApprove.Visible = False
		ImageCancel.Visible = False
	End Sub

	Sub ChangeMultiView(index As Integer)
		MtvMsUser.ActiveViewIndex = index
	End Sub

#End Region

#Region "events..."

	Protected Sub ImageCancel_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
		Response.Redirect("MsPekerjaan_Approval_View.aspx")
	End Sub

	Protected Sub imgReject_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles imgReject.Click
		Try
            Rejected()
            ChangeMultiView(1)
            LblConfirmation.Text = "Data has been rejected"
            HideControl()
		Catch ex As Exception
			LogError(ex)
			CvalPageErr.IsValid = False
			CvalPageErr.ErrorMessage = ex.Message
		End Try
	End Sub

	Protected Sub imgApprove_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles imgApprove.Click
		Try
			Using ObjMsPekerjaan_Approval As MsPekerjaan_Approval = DataRepository.MsPekerjaan_ApprovalProvider.GetByPK_MsPekerjaan_Approval_Id(parID)
				If ObjMsPekerjaan_Approval.FK_MsMode_Id.GetValueOrDefault = 1 Then
					Inserdata(ObjMsPekerjaan_Approval)
				ElseIf ObjMsPekerjaan_Approval.FK_MsMode_Id.GetValueOrDefault = 2 Then
					UpdateData(ObjMsPekerjaan_Approval)
				ElseIf ObjMsPekerjaan_Approval.FK_MsMode_Id.GetValueOrDefault = 3 Then
					DeleteData(ObjMsPekerjaan_Approval)
				End If
			End Using
			ChangeMultiView(1)
			LblConfirmation.Text = "Data approved successful"
            HideControl()
		Catch ex As Exception
			LogError(ex)
			CvalPageErr.IsValid = False
			CvalPageErr.ErrorMessage = ex.Message
		End Try

	End Sub


	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Not Page.IsPostBack Then
			Try
                'AuditTrailBLL.InsertAuditTrailUserAccess(SessionCurrentPage)
				ListmapingNew = New TList(Of MappingMsPekerjaanNCBSPPATK_Approval_Detail)
				ListmapingOld = New TList(Of MappingMsPekerjaanNCBSPPATK)
				LoadData()
			Catch ex As Exception
				LogError(ex)
				CvalPageErr.IsValid = False
				CvalPageErr.ErrorMessage = ex.Message
			End Try
		End If

	End Sub

	Private Sub LoadData()
		Dim PkObject As String
		'Load new data
		Using ObjMsPekerjaan_ApprovalDetail As MsPekerjaan_ApprovalDetail = DataRepository.MsPekerjaan_ApprovalDetailProvider.GetPaged(MsPekerjaan_ApprovalDetailColumn.FK_MsPekerjaan_Approval_Id.ToString & _
			"=" & _
			parID, "", 0, 1, Nothing)(0)
            Dim CekMode As MsPekerjaan_Approval = DataRepository.MsPekerjaan_ApprovalProvider.GetByPK_MsPekerjaan_Approval_Id(ObjMsPekerjaan_ApprovalDetail.FK_MsPekerjaan_Approval_Id)
            Dim nama As String = CekMode.FK_MsMode_Id
            If nama = 1 Then
                Baru.Visible = True
                lama.Visible = False
                baruNew.Visible = True
                lamaOld.Visible = False
            ElseIf nama = 2 Then
                Baru.Visible = True
                lama.Visible = True
                baruNew.Visible = True
                lamaOld.Visible = True
            ElseIf nama = 3 Then
                Baru.Visible = True
                lama.Visible = False
                baruNew.Visible = True
                lamaOld.Visible = False
            ElseIf nama = 4 Then
                Baru.Visible = True
                lama.Visible = False
                baruNew.Visible = True
                lamaOld.Visible = False
            End If
			With ObjMsPekerjaan_ApprovalDetail
				SafeDefaultValue = "-"
			   txtIDPekerjaanNew.Text = Safe(.IDPekerjaan)
txtNamaPekerjaanNew.Text = Safe(.NamaPekerjaan)


			       'other info
                Dim Omsuser As TList(Of User)
                Omsuser = DataRepository.UserProvider.GetByUserID(.CreatedBy)
                If Omsuser.Count > 0 Then
                    lblCreatedByNew.Text = Omsuser(0).UserName
                Else
                    lblCreatedByNew.Text = SafeDefaultValue
                End If
                lblCreatedDateNew.Text = FormatDate(.CreatedDate)
                Omsuser = DataRepository.UserProvider.GetByUserID(.LastUpdatedBy)
                If Omsuser.Count > 0 Then
                    lblUpdatedbyNew.Text = Omsuser(0).UserName
                Else
                    lblUpdatedbyNew.Text = SafeDefaultValue
                End If
			  lblUpdatedDateNew.Text = FormatDate(.LastUpdatedDate)
               txtActivationNew.Text = SafeActiveInactive(.activation)
				Dim L_objMappingMsPekerjaanNCBSPPATK_Approval_Detail As TList(Of MappingMsPekerjaanNCBSPPATK_Approval_Detail)
				L_objMappingMsPekerjaanNCBSPPATK_Approval_Detail = DataRepository.MappingMsPekerjaanNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsPekerjaanNCBSPPATK_Approval_DetailColumn.PK_MsPekerjaan_Approval_Id.ToString & _
					"=" & _
					parID, "", 0, Integer.MaxValue, Nothing)
                'Add mapping
                ListmapingNew.AddRange(L_oBJMappingMsPekerjaanNCBSPPATK_Approval_Detail)
			
			End With
			PkObject = ObjMsPekerjaan_ApprovalDetail.IDPekerjaan
		End Using

		'Load Old Data
		Using ObjMsPekerjaan As MsPekerjaan = DataRepository.MsPekerjaanProvider.GetByIDPekerjaan(PkObject)
			If ObjMsPekerjaan Is Nothing Then Return
			With ObjMsPekerjaan
				SafeDefaultValue = "-"
			    txtIDPekerjaanOld.Text = Safe(.IDPekerjaan)
txtNamaPekerjaanOld.Text = Safe(.NamaPekerjaan)


			 'other info
                Dim Omsuser As TList(Of User)
                Omsuser = DataRepository.UserProvider.GetByUserID(.CreatedBy)
                If Omsuser.Count > 0 Then
                    lblCreatedByOld.Text = Omsuser(0).UserName
                Else
                    lblCreatedByOld.Text = SafeDefaultValue
                End If
                lblCreatedDateNew.Text = FormatDate(.CreatedDate)
                Omsuser = DataRepository.UserProvider.GetByUserID(.LastUpdatedBy)
                If Omsuser.Count > 0 Then
                    lblUpdatedbyOld.Text = Omsuser(0).UserName
                Else
                    lblUpdatedbyOld.Text = SafeDefaultValue
                End If
			  lblUpdatedDateOld.Text = FormatDate(.LastUpdatedDate)
				Dim L_objMappingMsPekerjaanNCBSPPATK As TList(Of MappingMsPekerjaanNCBSPPATK)
                 txtActivationoLD.Text = SafeActiveInactive(.activation)
              L_ObjMappingMsPekerjaanNCBSPPATK = DataRepository.MappingMsPekerjaanNCBSPPATKProvider.GetPaged(MappingMsPekerjaanNCBSPPATKColumn.IDPekerjaan.ToString & _
                 "=" & _
                 PkObject, "", 0, Integer.MaxValue, Nothing)
                ListmapingOld.AddRange(L_ObjMappingMsPekerjaanNCBSPPATK)
            End With
        End Using
    End Sub



	Protected Sub imgBtnConfirmation_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnConfirmation.Click
		Try
			Response.Redirect("MsPekerjaan_approval_view.aspx")
		Catch ex As Exception
			LogError(ex)
			CvalPageErr.IsValid = False
			CvalPageErr.ErrorMessage = ex.Message
		End Try
	End Sub


	Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
		Try
			BindListMapping()
		Catch ex As Exception
			LogError(ex)
			CvalPageErr.IsValid = False
			CvalPageErr.ErrorMessage = ex.Message
		End Try
	End Sub

    Private Sub BindListMapping()
        'bind new value
        LBMappingNew.DataSource = ListMappingDisplayNew
        LBMappingNew.DataBind()
        'Bind OldValue
        LBmappingOld.DataSource = ListMappingDisplayOld
        LBmappingOld.DataBind()
    End Sub

#End Region

#Region "Property..."

	Public ReadOnly Property parID As String
		Get
			Return Request.Item("ID")
		End Get
	End Property
	''' <summary>
	''' Menyimpan Item untuk mapping New sementara
	''' </summary>
	''' <value></value>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Property ListmapingNew As TList(Of MappingMsPekerjaanNCBSPPATK_Approval_Detail)
		Get
			Return Session("ListmapingNew.data")
		End Get
		Set(value As TList(Of MappingMsPekerjaanNCBSPPATK_Approval_Detail))
			Session("ListmapingNew.data") = value
		End Set
	End Property

	''' <summary>
	''' Menyimpan Item untuk mapping Old sementara
	''' </summary>
	''' <value></value>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Property ListmapingOld As TList(Of MappingMsPekerjaanNCBSPPATK)
		Get
			Return Session("ListmapingOld.data")
		End Get
		Set(value As TList(Of MappingMsPekerjaanNCBSPPATK))
			Session("ListmapingOld.data") = value
		End Set
	End Property

    ''' <summary>
    ''' List yang tampil di ListBox
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplayNew() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingMsPekerjaanNCBSPPATK_Approval_Detail In ListmapingNew.FindAllDistinct("IDPekerjaanNCBS")
                Temp.Add(i.IDPekerjaanNCBS.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property

    ''' <summary>
    ''' List yang tampil di ListBox
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplayOld() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingMsPekerjaanNCBSPPATK In ListmapingOld.FindAllDistinct("IDPekerjaanNCBS")
                Temp.Add(i.IDPekerjaanNCBS.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property

#End Region

#Region "Action Approval..."

	Sub Rejected()
		Dim ObjMsPekerjaan_Approval As MsPekerjaan_Approval = DataRepository.MsPekerjaan_ApprovalProvider.GetByPK_MsPekerjaan_Approval_Id(parID)
		Dim ObjMsPekerjaan_ApprovalDetail As MsPekerjaan_ApprovalDetail = DataRepository.MsPekerjaan_ApprovalDetailProvider.GetPaged(MsPekerjaan_ApprovalDetailColumn.FK_MsPekerjaan_Approval_Id.ToString & "=" & ObjMsPekerjaan_Approval.PK_MsPekerjaan_Approval_Id, "", 0, 1, Nothing)(0)
		Dim L_ObjMappingMsPekerjaanNCBSPPATK_Approval_Detail As TList(Of MappingMsPekerjaanNCBSPPATK_Approval_Detail) = DataRepository.MappingMsPekerjaanNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsPekerjaanNCBSPPATK_Approval_DetailColumn.PK_MsPekerjaan_Approval_Id.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)
		DeleteApproval(ObjMsPekerjaan_Approval, ObjMsPekerjaan_ApprovalDetail, L_ObjMappingMsPekerjaanNCBSPPATK_Approval_Detail)
	End Sub

	Sub DeleteApproval(ByRef objMsPekerjaan_Approval As MsPekerjaan_Approval, ByRef ObjMsPekerjaan_ApprovalDetail As MsPekerjaan_ApprovalDetail, ByRef L_ObjMappingMsPekerjaanNCBSPPATK_Approval_Detail As TList(Of MappingMsPekerjaanNCBSPPATK_Approval_Detail))
		DataRepository.MsPekerjaan_ApprovalProvider.Delete(objMsPekerjaan_Approval)
		DataRepository.MsPekerjaan_ApprovalDetailProvider.Delete(ObjMsPekerjaan_ApprovalDetail)
		DataRepository.MappingMsPekerjaanNCBSPPATK_Approval_DetailProvider.Delete(L_ObjMappingMsPekerjaanNCBSPPATK_Approval_Detail)
	End Sub

	''' <summary>
	''' Delete Data dari  asli dari approval
	''' </summary>
	''' <remarks></remarks>
	Private Sub DeleteData(objMsPekerjaan_Approval As MsPekerjaan_Approval)
		'   '================= Delete Header ===========================================================
		Dim LObjMsPekerjaan_ApprovalDetail As TList(Of MsPekerjaan_ApprovalDetail) = DataRepository.MsPekerjaan_ApprovalDetailProvider.GetPaged(MsPekerjaan_ApprovalDetailColumn.FK_MsPekerjaan_Approval_Id.ToString & "=" & objMsPekerjaan_Approval.PK_MsPekerjaan_Approval_Id, "", 0, 1, Nothing)
		If LObjMsPekerjaan_ApprovalDetail.Count = 0 Then Throw New Exception("Data Not Found")
		Dim ObjMsPekerjaan_ApprovalDetail As MsPekerjaan_ApprovalDetail = LObjMsPekerjaan_ApprovalDetail(0)
		Using ObjNewMsPekerjaan As MsPekerjaan = DataRepository.MsPekerjaanProvider.GetByIDPekerjaan(ObjMsPekerjaan_ApprovalDetail.IDPekerjaan)
			DataRepository.MsPekerjaanProvider.Delete(ObjNewMsPekerjaan)
		End Using

		'======== Delete MAPPING =========================================
		Dim L_ObjMappingMsPekerjaanNCBSPPATK_Approval_Detail As TList(Of MappingMsPekerjaanNCBSPPATK_Approval_Detail) = DataRepository.MappingMsPekerjaanNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsPekerjaanNCBSPPATK_Approval_DetailColumn.PK_MsPekerjaan_Approval_Id.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)


		For Each c As MappingMsPekerjaanNCBSPPATK_Approval_Detail In L_ObjMappingMsPekerjaanNCBSPPATK_Approval_Detail
			Using ObjNewMappingMsPekerjaanNCBSPPATK As MappingMsPekerjaanNCBSPPATK = DataRepository.MappingMsPekerjaanNCBSPPATKProvider.GetByPK_MappingMsPekerjaanNCBSPPATK_Id(c.PK_MappingMsPekerjaanNCBSPPATK_Id)
				DataRepository.MappingMsPekerjaanNCBSPPATKProvider.Delete(ObjNewMappingMsPekerjaanNCBSPPATK)
			End Using
		Next

		'======== Delete Approval data ======================================
		DeleteApproval(objMsPekerjaan_Approval, ObjMsPekerjaan_ApprovalDetail, L_ObjMappingMsPekerjaanNCBSPPATK_Approval_Detail)
	End Sub

	''' <summary>
	''' Update Data dari approval ke table asli 
	''' </summary>
	''' <remarks></remarks>
	Private Sub UpdateData(objMsPekerjaan_Approval As MsPekerjaan_Approval)
        'Dim objAuditTrailType As MsAuditTrailEntityType = DataRepository.MsAuditTrailEntityTypeProvider.GetByPk_MsAuditTrailEntityTypeId(18)
		'   '================= Update Header ===========================================================
		Dim LObjMsPekerjaan_ApprovalDetail As TList(Of MsPekerjaan_ApprovalDetail) = DataRepository.MsPekerjaan_ApprovalDetailProvider.GetPaged(MsPekerjaan_ApprovalDetailColumn.FK_MsPekerjaan_Approval_Id.ToString & "=" & objMsPekerjaan_Approval.PK_MsPekerjaan_Approval_Id, "", 0, 1, Nothing)
		If LObjMsPekerjaan_ApprovalDetail.Count = 0 Then Throw New Exception("Data Not Found")
		Dim ObjMsPekerjaan_ApprovalDetail As MsPekerjaan_ApprovalDetail = LObjMsPekerjaan_ApprovalDetail(0)
		Dim ObjMsPekerjaanNew As MsPekerjaan
		Using ObjMsPekerjaanOld As MsPekerjaan = DataRepository.MsPekerjaanProvider.GetByIDPekerjaan(ObjMsPekerjaan_ApprovalDetail.IDPekerjaan)
			If ObjMsPekerjaanOld Is Nothing Then Throw New Exception("Data Not Found")
			ObjMsPekerjaanNew = ObjMsPekerjaanOld.Clone
			With ObjMsPekerjaanNew
				FillOrNothing(.NamaPekerjaan, ObjMsPekerjaan_ApprovalDetail.NamaPekerjaan)
				FillOrNothing(.CreatedBy, ObjMsPekerjaan_ApprovalDetail.CreatedBy)
				FillOrNothing(.CreatedDate, ObjMsPekerjaan_ApprovalDetail.CreatedDate)
				FillOrNothing(.ApprovedBy, SessionUserId, True, oString)
				FillOrNothing(.ApprovedDate, Date.Now)
			End With
			DataRepository.MsPekerjaanProvider.Save(ObjMsPekerjaanNew)
			'Insert auditrail
            'AuditTrailBLL.InsertAuditTrail(AuditTrailOperation.Add, objAuditTrailType, Now(), Sahassa.AML.Commonly.SessionNIK, Sahassa.AML.Commonly.SessionStaffName, "MsBentukBidangUsaha has Approved to Update data", ObjMsBentukBidangUsahaNew, ObjMsBentukBidangUsahaOld)
			'======== Update MAPPING =========================================
			'delete mapping item
			Using L_ObjMappingMsPekerjaanNCBSPPATK As TList(Of MappingMsPekerjaanNCBSPPATK) = DataRepository.MappingMsPekerjaanNCBSPPATKProvider.GetPaged(MappingMsPekerjaanNCBSPPATKColumn.IDPekerjaan.ToString & _
				"=" & _
				ObjMsPekerjaanNew.IDPekerjaan, "", 0, Integer.MaxValue, Nothing)
				DataRepository.MappingMsPekerjaanNCBSPPATKProvider.Delete(L_ObjMappingMsPekerjaanNCBSPPATK)
			End Using
			'Insert mapping item
			Dim L_ObjMappingMsPekerjaanNCBSPPATK_Approval_Detail As TList(Of MappingMsPekerjaanNCBSPPATK_Approval_Detail) = DataRepository.MappingMsPekerjaanNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsPekerjaanNCBSPPATK_Approval_DetailColumn.PK_MsPekerjaan_Approval_Id.ToString & "=" & parID, "", 0, Integer.MaxValue, Nothing)

			Dim L_ObjNewMappingMsPekerjaanNCBSPPATK As New TList(Of MappingMsPekerjaanNCBSPPATK)
			For Each c As MappingMsPekerjaanNCBSPPATK_Approval_Detail In L_ObjMappingMsPekerjaanNCBSPPATK_Approval_Detail
				Dim ObjNewMappingMsPekerjaanNCBSPPATK As New MappingMsPekerjaanNCBSPPATK '= DataRepository.MappingMsPekerjaanNCBSPPATKProvider.GetByPK_MappingMsPekerjaanNCBSPPATK_Id(c.PK_MappingMsPekerjaanNCBSPPATK_Id.GetValueOrDefault)
				With ObjNewMappingMsPekerjaanNCBSPPATK
					FillOrNothing(.IDPekerjaan, ObjMsPekerjaanNew.IDPekerjaan)
					FillOrNothing(.Nama, c.Nama)
					FillOrNothing(.IDPekerjaanNCBS, c.IDPekerjaanNCBS)
					L_ObjNewMappingMsPekerjaanNCBSPPATK.Add(ObjNewMappingMsPekerjaanNCBSPPATK)
				End With
			Next
			DataRepository.MappingMsPekerjaanNCBSPPATKProvider.Save(L_ObjNewMappingMsPekerjaanNCBSPPATK)
			'======== Delete Approval data ======================================
			DeleteApproval(objMsPekerjaan_Approval, ObjMsPekerjaan_ApprovalDetail, L_ObjMappingMsPekerjaanNCBSPPATK_Approval_Detail)
		End Using
	End Sub
	''' <summary>
	''' Insert Data dari approval ke table asli 
	''' </summary>
	''' <remarks></remarks>
	Private Sub Inserdata(objMsPekerjaan_Approval As MsPekerjaan_Approval)
        'Dim objAuditTrailType As MsAuditTrailEntityType = DataRepository.MsAuditTrailEntityTypeProvider.GetByPk_MsAuditTrailEntityTypeId(18)
		'   '================= Save Header ===========================================================
		Dim LObjMsPekerjaan_ApprovalDetail As TList(Of MsPekerjaan_ApprovalDetail) = DataRepository.MsPekerjaan_ApprovalDetailProvider.GetPaged(MsPekerjaan_ApprovalDetailColumn.FK_MsPekerjaan_Approval_Id.ToString & "=" & objMsPekerjaan_Approval.PK_MsPekerjaan_Approval_Id, "", 0, 1, Nothing)
		'jika data tidak ada di database
		If LObjMsPekerjaan_ApprovalDetail.Count = 0 Then Throw New Exception("Data Not Found")
		'binding approval ke table asli
		Dim ObjMsPekerjaan_ApprovalDetail As MsPekerjaan_ApprovalDetail = LObjMsPekerjaan_ApprovalDetail(0)
		Using ObjNewMsPekerjaan As New MsPekerjaan()
			With ObjNewMsPekerjaan
				FillOrNothing(.IDPekerjaan, ObjMsPekerjaan_ApprovalDetail.IDPekerjaan)
				FillOrNothing(.NamaPekerjaan, ObjMsPekerjaan_ApprovalDetail.NamaPekerjaan)
				FillOrNothing(.CreatedBy, ObjMsPekerjaan_ApprovalDetail.CreatedBy)
				FillOrNothing(.CreatedDate, ObjMsPekerjaan_ApprovalDetail.CreatedDate)
				FillOrNothing(.ApprovedBy, SessionUserId, True, oString)
				FillOrNothing(.ApprovedDate, Date.Now)
                .activation = True
			End With
			DataRepository.MsPekerjaanProvider.Save(ObjNewMsPekerjaan)
			'Insert auditrail
            'AuditTrailBLL.InsertAuditTrail(AuditTrailOperation.Add, objAuditTrailType, Now(), Sahassa.AML.Commonly.SessionNIK, Sahassa.AML.Commonly.SessionStaffName, "MsBentukBidangUsaha has Approved to Insert data", ObjNewMsBentukBidangUsaha, Nothing)
			'======== Insert MAPPING =========================================
			Dim L_ObjMappingMsPekerjaanNCBSPPATK_Approval_Detail As TList(Of MappingMsPekerjaanNCBSPPATK_Approval_Detail) = DataRepository.MappingMsPekerjaanNCBSPPATK_Approval_DetailProvider.GetPaged(MappingMsPekerjaanNCBSPPATK_Approval_DetailColumn.PK_MsPekerjaan_Approval_Id.ToString & _
				"=" & _
				parID, "", 0, Integer.MaxValue, Nothing)
			Dim L_ObjNewMappingMsPekerjaanNCBSPPATK As New TList(Of MappingMsPekerjaanNCBSPPATK)()
			For Each c As MappingMsPekerjaanNCBSPPATK_Approval_Detail In L_ObjMappingMsPekerjaanNCBSPPATK_Approval_Detail
				Dim ObjNewMappingMsPekerjaanNCBSPPATK As New MappingMsPekerjaanNCBSPPATK()
				With ObjNewMappingMsPekerjaanNCBSPPATK
					FillOrNothing(.IDPekerjaan, ObjNewMsPekerjaan.IDPekerjaan)
 					FillOrNothing(.Nama, c.Nama)
					FillOrNothing(.IDPekerjaanNCBS, c.IDPekerjaanNCBS)
					L_ObjNewMappingMsPekerjaanNCBSPPATK.Add(ObjNewMappingMsPekerjaanNCBSPPATK)
				End With
			Next
			DataRepository.MappingMsPekerjaanNCBSPPATKProvider.Save(L_ObjNewMappingMsPekerjaanNCBSPPATK)
			'======== Delete Approval data ======================================
			DeleteApproval(objMsPekerjaan_Approval, ObjMsPekerjaan_ApprovalDetail, L_ObjMappingMsPekerjaanNCBSPPATK_Approval_Detail)
		End Using
	End Sub

#End Region

End Class


