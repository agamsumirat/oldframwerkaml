#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports System.Collections.Generic
#End Region

Partial Class MsNegara_add
    Inherits Parent



#Region "Function"

    ''' <summary>
    ''' Validasi men-handle seluruh syarat2 data valid
    ''' data yang lolos dari validasi dipastikan lolos ke database 
    ''' karena menggunakan FillorNothing
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ValidasiControl()

        '======================== Validasi textbox :  IDNegara canot Null ====================================================
        If ObjectAntiNull(txtIDNegara.Text) = False Then Throw New Exception("IDNegara  must be filled  ")
        '======================== Validasi textbox :  Nama Negara canot Null ====================================================
        If ObjectAntiNull(txtNamaNegara.Text) = False Then Throw New Exception("Nama Negara  must be filled  ")

        If Not DataRepository.MsNegaraProvider.GetByIDNegara(txtIDNegara.Text) Is Nothing Then
            Throw New Exception("ID already exist in the database")
        End If

        If DataRepository.MsNegara_ApprovalDetailProvider.GetPaged(MsNegara_ApprovalDetailColumn.IDNegara.ToString & "=" & txtIDNegara.Text, "", 0, 1, Nothing).Count > 0 Then
            Throw New Exception("Data exist in List Waiting Approval")
        End If


        'If LBMapping.Items.Count = 0 Then Throw New Exception("data doesnt have list Mapping")
    End Sub

#End Region
    Private Sub InsertAuditTrail()
        Try
            Sahassa.AML.AuditTrailAlert.AuditTrailChecking(3)
            Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrailTableAdapter
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "MsNegara", "ID", "Add", "", txtIDNegara.Text, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "MsNegara", "Nama", "Add", "", txtNamaNegara.Text, "Accepted")
                AccessAudit.Insert(Now, Sahassa.AML.Commonly.SessionUserId, Sahassa.AML.Commonly.SessionUserId, "MsNegara", "Mapping Item", "Add", "", LBMapping.Text, "Accepted")
            End Using
        Catch
            Throw
        End Try
    End Sub

#Region "events..."
    Protected Sub ImgBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBack.Click
        Response.Redirect("MsNegara_View.aspx")
    End Sub
    Protected Sub imgBrowse_click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBrowse.Click
        Try
            If Not Get_DataFromPicker Is Nothing Then
                Listmaping.AddRange(Get_DataFromPicker)
            End If
        Catch ex As Exception
            LogError(ex)
            Me.CvalPageErr.IsValid = False
            Me.CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub imgDeleteItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgDeleteItem.Click
        Try
            If LBMapping.SelectedIndex < 0 Then
                Throw New Exception("No data Selected")
            End If
            Listmaping.RemoveAt(LBMapping.SelectedIndex)

        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
                Listmaping = New TList(Of MappingMsNegaraNCBSPPATK)
            Catch ex As Exception
                LogError(ex)
                CvalPageErr.IsValid = False
                CvalPageErr.ErrorMessage = ex.Message
            End Try
        End If

    End Sub



    Protected Sub ImgBtnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnSave.Click
        Try
            Page.Validate()
            If Page.IsValid Then
                ValidasiControl()
                'create audittrailtype
                'Dim objAuditTrailType As MsAuditTrailEntityType = DataRepository.MsAuditTrailEntityTypeProvider.GetByPk_MsAuditTrailEntityTypeId(18)
                ' =========== Insert Header Approval
                Dim KeyHeaderApproval As Integer
                Using ObjMsNamaNegara_Approval As New MsNegara_Approval
                    With ObjMsNamaNegara_Approval
                        FillOrNothing(.FK_MsMode_Id, 1, True, oInt)
                        FillOrNothing(.RequestedBy, SessionPkUserId)
                        FillOrNothing(.RequestedDate, Date.Now)
                        .IsUpload = False
                    End With
                    DataRepository.MsNegara_ApprovalProvider.Save(ObjMsNamaNegara_Approval)
                    KeyHeaderApproval = ObjMsNamaNegara_Approval.PK_MsNegara_Approval_Id
                End Using
                '============ Insert Detail Approval
                Using objMsNegara_ApprovalDetail As New MsNegara_ApprovalDetail()
                    With objMsNegara_ApprovalDetail
                        FillOrNothing(.IDNegara, txtIDNegara.Text, True, oInt)
                        FillOrNothing(.NamaNegara, txtNamaNegara.Text, True, Ovarchar)

                        FillOrNothing(.Activation, 1, True, oBit)
                        FillOrNothing(.CreatedDate, Date.Now)
                        FillOrNothing(.CreatedBy, SessionUserId)
                        FillOrNothing(.FK_MsNegara_Approval_Id, KeyHeaderApproval)
                    End With
                    DataRepository.MsNegara_ApprovalDetailProvider.Save(objMsNegara_ApprovalDetail)
                    Dim keyHeaderApprovalDetail As Integer = objMsNegara_ApprovalDetail.PK_MsNegara_ApprovalDetail_Id
                    'Insert auditrail
                    InsertAuditTrail()
                    'AuditTrailBLL.InsertAuditTrail(AuditTrailOperation.Add, objAuditTrailType, Now(), Sahassa.CommonCTRWeb.Commonly.SessionNIK, Sahassa.CommonCTRWeb.Commonly.SessionStaffName, "Request for approval inserted  data MsNamaNegara", objMsNamaNegara_ApprovalDetail, Nothing)
                    '========= Insert mapping item 
                    Dim LobjMappingMsNamaNegaraPPATK_Approval_Detail As New TList(Of MappingMsNegaraNCBSPPATK_Approval_Detail)
                    For Each objMappingMsNamaNegaraPPATK As MappingMsNegaraNCBSPPATK In Listmaping
                        Dim Mapping As New MappingMsNegaraNCBSPPATK_Approval_Detail
                        With Mapping
                            FillOrNothing(.IDNegara, objMsNegara_ApprovalDetail.IDNegara)
                            FillOrNothing(.IDNegaraNCBS, objMappingMsNamaNegaraPPATK.IDNegaraNCBS)
                            FillOrNothing(.PK_MsNegara_Approval_Id, KeyHeaderApproval)
                            FillOrNothing(.PK_MappingMsNegaraNCBSPPATK_approval_detail_Id, keyHeaderApprovalDetail)
                            FillOrNothing(.Nama, objMappingMsNamaNegaraPPATK.Nama)
                            LobjMappingMsNamaNegaraPPATK_Approval_Detail.Add(Mapping)
                        End With
                    Next
                    DataRepository.MappingMsNegaraNCBSPPATK_Approval_DetailProvider.Save(LobjMappingMsNamaNegaraPPATK_Approval_Detail)
                    'session Maping item Clear
                    Listmaping.Clear()
                    LblConfirmation.Text = "MsNegara data is waiting approval for Insert"
                    MtvMsUser.ActiveViewIndex = 1
                    ImgBtnSave.Visible = False
                    ImgBackAdd.Visible = False
                End Using
            End If
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try

    End Sub

    Protected Sub ImgBtnAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBtnAdd.Click
        Try
            ClearAllControl(VwAdd)
            MtvMsUser.ActiveViewIndex = 0
            ImgBtnSave.Visible = True
            ImgBackAdd.Visible = True
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Protected Sub ImgBackAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBackAdd.Click
        Response.Redirect("MsNegara_View.aspx")
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            BindListMapping()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub BindListMapping()
        LBMapping.DataSource = ListMappingDisplay
        LBMapping.DataBind()
    End Sub

#End Region

#Region "Property..."
    ''' <summary>
    ''' Mengambil Data dari Picker
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Get_DataFromPicker() As TList(Of MappingMsNegaraNCBSPPATK)
        Get
            Dim Temp As New TList(Of MappingMsNegaraNCBSPPATK)
            Dim L_MsNegaraNCBS As TList(Of MsNegaraNCBS)
            Try
                If Session("PickerMsNegaraNCBS.Data") Is Nothing Then
                    Throw New Exception
                End If
                L_MsNegaraNCBS = Session("PickerMsNegaraNCBS.Data")
                For Each i As MsNegaraNCBS In L_MsNegaraNCBS
                    Dim Tempmapping As New MappingMsNegaraNCBSPPATK
                    With Tempmapping
                        .IDNegaraNCBS = i.IDNegaraNCBS
                        .Nama = i.NamaNegara
                    End With
                    Temp.Add(Tempmapping)
                Next
            Catch ex As Exception
                L_MsNegaraNCBS = Nothing
            End Try
            Return Temp
        End Get
    End Property
    ''' <summary>
    ''' Menyimpan Item untuk mapping sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Listmaping() As TList(Of MappingMsNegaraNCBSPPATK)
        Get
            Return Session("Listmaping.data")
        End Get
        Set(ByVal value As TList(Of MappingMsNegaraNCBSPPATK))
            Session("Listmaping.data") = value
        End Set
    End Property
    ''' <summary>
    ''' List yang tampil di ListBox
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplay() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingMsNegaraNCBSPPATK In Listmaping.FindAllDistinct("IDNegaraNCBS")
                Temp.Add(i.IDNegaraNCBS.ToString & "-" & i.Nama)
            Next
            Return Temp
        End Get

    End Property

#End Region

End Class



