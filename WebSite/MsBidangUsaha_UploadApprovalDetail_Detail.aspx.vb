#Region "Imports..."
Option Explicit On
Imports AMLBLL
Imports AMLBLL.ValidateBLL
Imports Sahassanettier.Entities
Imports Sahassanettier.Data
Imports AMLBLL.DataType
Imports Sahassa.AML.Commonly
Imports Sahassa.AML
Imports System.Collections.Generic
#End Region

Partial Class MsBidangUsaha_UploadApprovalDetail_Detail
    Inherits Parent

#Region "Function"

    Sub HideControl()
        ImageCancel.Visible = False
    End Sub

    Sub ChangeMultiView(ByVal index As Integer)
        MtvMsUser.ActiveViewIndex = index
    End Sub

#End Region

#Region "events..."

    Protected Sub ImageCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageCancel.Click
        Dim LngId As Long = 0
        Using ObjMsBidangUsaha_ApprovalDetail As TList(Of MsBidangUsaha_ApprovalDetail) = DataRepository.MsBidangUsaha_ApprovalDetailProvider.GetPaged("PK_MsBidangUsaha_ApprovalDetail_Id = " & parID, "", 0, Integer.MaxValue, 0)
            If ObjMsBidangUsaha_ApprovalDetail.Count > 0 Then
                LngId = ObjMsBidangUsaha_ApprovalDetail(0).FK_MsBidangUsaha_Approval_Id
            End If
        End Using
        Response.Redirect("MsBidangUsaha_UploadApprovalDetail_View.aspx?ID=" & LngId)
    End Sub






    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                Sahassa.AML.Commonly.SessionCurrentPage = Page.Request.Path.Substring(Page.Request.Path.LastIndexOf("/") + 1)
                Using AccessAudit As New AMLDAL.AMLDataSetTableAdapters.AuditTrail_UserAccessTableAdapter
                    Dim UserAccessAction As String = "Accesssing page " & Sahassa.AML.Commonly.SessionCurrentPage & " succeeded"
                    AccessAudit.Insert(Sahassa.AML.Commonly.SessionUserId, Now, UserAccessAction)
                End Using
                ListmapingNew = New TList(Of MappingBidangUsahaNCBS_Approval_Detail)
                ListmapingOld = New TList(Of MappingBidangUsahaNCBS_Approval_Detail)
                LoadData()
            Catch ex As Exception
                LogError(ex)
                CvalPageErr.IsValid = False
                CvalPageErr.ErrorMessage = ex.Message
            End Try
        End If

    End Sub

    Private Sub LoadData()
        Dim PkObject As String
        'Load new data
        Using ObjMsBidangUsaha_ApprovalDetail As MsBidangUsaha_ApprovalDetail = DataRepository.MsBidangUsaha_ApprovalDetailProvider.GetPaged(MsBidangUsaha_ApprovalDetailColumn.PK_MsBidangUsaha_ApprovalDetail_Id.ToString & _
            "=" & _
            parID, "", 0, 1, Nothing)(0)
            With ObjMsBidangUsaha_ApprovalDetail
                SafeDefaultValue = "-"
                txtIdBidangUsahanew.Text = Safe(.IdBidangUsaha)
                txtNamaBidangUsahanew.Text = Safe(.NamaBidangUsaha)

                'other info
                Dim Omsuser As TList(Of User)
                Omsuser = DataRepository.UserProvider.GetByUserID(.CreatedBy)
                If Omsuser.Count > 0 Then
                    lblCreatedByNew.Text = Omsuser(0).UserName
                Else
                    lblCreatedByNew.Text = SafeDefaultValue
                End If
                lblCreatedDateNew.Text = FormatDate(.CreatedDate)
                Omsuser = DataRepository.UserProvider.GetByUserID(.LastUpdatedBy)
                If Omsuser.Count > 0 Then
                    lblUpdatedbyNew.Text = Omsuser(0).UserName
                Else
                    lblUpdatedbyNew.Text = SafeDefaultValue
                End If
                lblUpdatedDateNew.Text = FormatDate(.LastUpdatedDate)
                'txtActivationNew.Text = SafeActiveInactive(.Activation)
                Dim L_objMappingBidangUsahaNCBS_Approval_Detail_Approval_Detail As TList(Of MappingBidangUsahaNCBS_Approval_Detail)
                L_objMappingBidangUsahaNCBS_Approval_Detail_Approval_Detail = DataRepository.MappingBidangUsahaNCBS_Approval_DetailProvider.GetPaged(MappingBidangUsahaNCBS_Approval_DetailColumn.BidangUsahaId.ToString & _
                 "=" & _
                 ObjMsBidangUsaha_ApprovalDetail.IdBidangUsaha, "", 0, Integer.MaxValue, Nothing)

                'Add mapping
                ListmapingNew.AddRange(L_objMappingBidangUsahaNCBS_Approval_Detail_Approval_Detail)

            End With
            PkObject = ObjMsBidangUsaha_ApprovalDetail.IdBidangUsaha
        End Using



        'Load Old Data
        Using objMsBidangUsaha As MsBidangUsaha = DataRepository.MsBidangUsahaProvider.GetByIdBidangUsaha(PkObject)
            If objMsBidangUsaha Is Nothing Then
                lamaOld.Visible = False
                lama.Visible = False
                Return
            End If

            With objMsBidangUsaha
                SafeDefaultValue = "-"
                txtIdBidangUsahaOld.Text = Safe(.IdBidangUsaha)
                txtNamaBidangUsahaOld.Text = Safe(.NamaBidangUsaha)


                'other info
                Dim Omsuser As TList(Of User)
                Omsuser = DataRepository.UserProvider.GetByUserID(.CreatedBy)
                If Omsuser.Count > 0 Then
                    lblCreatedByOld.Text = Omsuser(0).UserName
                Else
                    lblCreatedByOld.Text = SafeDefaultValue
                End If
                lblCreatedDateNew.Text = FormatDate(.CreatedDate)
                Omsuser = DataRepository.UserProvider.GetByUserID(.LastUpdatedBy)
                If Omsuser.Count > 0 Then
                    lblUpdatedbyOld.Text = Omsuser(0).UserName
                Else
                    lblUpdatedbyOld.Text = SafeDefaultValue
                End If
                lblUpdatedDateOld.Text = FormatDate(.LastUpdatedDate)
                Dim l_objMappingBidangUsahaNCBS_Approval_Detail As TList(Of MappingBidangUsahaNCBS_Approval_Detail)
                'txtNamaBidangUsahaOld.Text = Safe(.NamaBidangUsaha)
                l_objMappingBidangUsahaNCBS_Approval_Detail = DataRepository.MappingBidangUsahaNCBS_Approval_DetailProvider.GetPaged(MappingBidangUsahaNCBS_Approval_DetailColumn.BidangUsahaId.ToString & _
                   "=" & _
                   PkObject, "", 0, Integer.MaxValue, Nothing)

                ListmapingOld.AddRange(l_objMappingBidangUsahaNCBS_Approval_Detail)
            End With
        End Using
    End Sub






    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            BindListMapping()
        Catch ex As Exception
            LogError(ex)
            CvalPageErr.IsValid = False
            CvalPageErr.ErrorMessage = ex.Message
        End Try
    End Sub

    Private Sub BindListMapping()
        'bind new value
        LBMappingNew.DataSource = ListMappingDisplayNew '(0).PK_MappingBidangUsahaNCBS_Approval_detail_id
        LBMappingNew.DataBind()
        'Bind OldValue
        LBmappingOld.DataSource = ListMappingDisplayOld
        LBmappingOld.DataBind()
    End Sub

#End Region

#Region "Property..."

    Public ReadOnly Property parID() As String
        Get
            Return Request.Item("ID")
        End Get
    End Property
    ''' <summary>
    ''' Menyimpan Item untuk mapping New sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ListmapingNew() As TList(Of MappingBidangUsahaNCBS_Approval_Detail)
        Get
            Return Session("ListmapingNew.data")
        End Get
        Set(ByVal value As TList(Of MappingBidangUsahaNCBS_Approval_Detail))
            Session("ListmapingNew.data") = value
        End Set
    End Property

    ''' <summary>
    ''' Menyimpan Item untuk mapping Old sementara
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ListmapingOld() As TList(Of MappingBidangUsahaNCBS_Approval_Detail)
        Get
            Return Session("ListmapingOld.data")
        End Get
        Set(ByVal value As TList(Of MappingBidangUsahaNCBS_Approval_Detail))
            Session("ListmapingOld.data") = value
        End Set
    End Property

    ''' <summary>
    ''' List yang tampil di ListBox New
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplayNew() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingBidangUsahaNCBS_Approval_Detail In ListmapingNew.FindAllDistinct("IDBidangUsahaNCBS")
                Temp.Add(i.IDBidangUsahaNCBS.ToString & "-" & i.NAMA)
            Next
            Return Temp
        End Get

    End Property

    ''' <summary>
    ''' List yang tampil di ListBox Old
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private ReadOnly Property ListMappingDisplayOld() As List(Of String)
        Get
            Dim Temp As New List(Of String)
            For Each i As MappingBidangUsahaNCBS_Approval_Detail In ListmapingOld.FindAllDistinct("IDBidangUsahaNCBS")
                Temp.Add(i.IDBidangUsahaNCBS.ToString & "-" & i.NAMA)
            Next
            Return Temp
        End Get

    End Property
#End Region



End Class




